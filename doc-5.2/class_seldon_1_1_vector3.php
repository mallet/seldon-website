<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_vector3.php">Vector3</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Vector3" -->
<p>Vector of vectors of vectors.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_vector3_8hxx_source.php">Vector3.hxx</a>&gt;</code></p>

<p><a href="class_seldon_1_1_vector3-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a432f82e0b3e08fd9ed3ab63231ee5d5f"></a><!-- doxytag: member="Seldon::Vector3::value_type" ref="a432f82e0b3e08fd9ed3ab63231ee5d5f" args="" -->
typedef T&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a30f987d32a45d5aa34e1e4afe97657dd"></a><!-- doxytag: member="Seldon::Vector3::pointer" ref="a30f987d32a45d5aa34e1e4afe97657dd" args="" -->
typedef T *&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a41a82d7781dcec627be04cb423c3d454"></a><!-- doxytag: member="Seldon::Vector3::const_pointer" ref="a41a82d7781dcec627be04cb423c3d454" args="" -->
typedef const T *&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac9d5535212538964cd99cedca2a25be2"></a><!-- doxytag: member="Seldon::Vector3::reference" ref="ac9d5535212538964cd99cedca2a25be2" args="" -->
typedef T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac5593e6ccaf9b35a8146c4773e4ac32f"></a><!-- doxytag: member="Seldon::Vector3::const_reference" ref="ac5593e6ccaf9b35a8146c4773e4ac32f" args="" -->
typedef const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a3425eec2b0400057adeda33a005a188d">Vector3</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#a3425eec2b0400057adeda33a005a188d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a8384e50e394357e4b9a6c0699f39f241">Vector3</a> (int)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Constructor.  <a href="#a8384e50e394357e4b9a6c0699f39f241"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#ae42d9b0d42feb5459c7efea52c2f47ac">Vector3</a> (<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt; &amp;length)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Constructor.  <a href="#ae42d9b0d42feb5459c7efea52c2f47ac"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Allocator &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a9fff1c8ce72eb5dc5c002b6f1c0e7b86">Vector3</a> (<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt;, Vect_Full, Allocator &gt; &amp;length)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Constructor.  <a href="#a9fff1c8ce72eb5dc5c002b6f1c0e7b86"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#ae85c3b686a4100a5c05dc7d82a4b6b80">~Vector3</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Destructor.  <a href="#ae85c3b686a4100a5c05dc7d82a4b6b80"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a4a694febe8dba0a1203fd628cd1e3167">GetLength</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns size along dimension 1.  <a href="#a4a694febe8dba0a1203fd628cd1e3167"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a66946f1764cc14abec8a51d4e310f6b5">GetSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns size along dimension 1.  <a href="#a66946f1764cc14abec8a51d4e310f6b5"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a89eac88f9d58edbf81884b2f8645219c">GetLength</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the size of the inner vector of vectors #<em>i</em>.  <a href="#a89eac88f9d58edbf81884b2f8645219c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a6b29a36baf352da31f2a97ec972fd761">GetSize</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the size of the inner vector of vectors #<em>i</em>.  <a href="#a6b29a36baf352da31f2a97ec972fd761"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a51d4b3778ff8220b1c80377852826d7b">GetLength</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the size of the inner vector #<em>i</em> #<em>j</em>.  <a href="#a51d4b3778ff8220b1c80377852826d7b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a81b5f3374df5a7f46ff50d60840eb81b">GetSize</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the size of the inner vector #<em>i</em> #<em>j</em>.  <a href="#a81b5f3374df5a7f46ff50d60840eb81b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a497379d28c87524052ee202702db9c91">GetNelement</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the total number of elements in the inner vectors.  <a href="#a497379d28c87524052ee202702db9c91"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#ab83e9c4434cdc9aeda454890ae557662">GetNelement</a> (int beg, int end) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the total number of elements in a range of inner vectors.  <a href="#ab83e9c4434cdc9aeda454890ae557662"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a0d7115fd1e3802ad9417a97f0a1fcad3">GetNelement</a> (int beg0, int end0, int beg1, int end1) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the total number of elements in a range of inner vectors.  <a href="#a0d7115fd1e3802ad9417a97f0a1fcad3"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a5772120ddd32015c71c56ac2e44413e0">GetShape</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the shape of a vector of vectors.  <a href="#a5772120ddd32015c71c56ac2e44413e0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a2520f6f6ecf4d5853eaefe38904a4fc7">GetShape</a> (int i, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt; &amp;shape) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the shape of a vector of vectors.  <a href="#a2520f6f6ecf4d5853eaefe38904a4fc7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a21e22b8d942820eb4c60e3e34c2afcd6">Reallocate</a> (int N)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates the vector of vectors of vectors.  <a href="#a21e22b8d942820eb4c60e3e34c2afcd6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a5d5b76dbcaa0a445f089f2bdf341aba5">Reallocate</a> (int i, int N)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates the inner vector of vectors #<em>i</em>.  <a href="#a5d5b76dbcaa0a445f089f2bdf341aba5"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a67835c87beb5c2539ffe8af0d4a0b49c">Reallocate</a> (int i, int j, int N)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates the inner vector #<em>i</em> #<em>j</em>.  <a href="#a67835c87beb5c2539ffe8af0d4a0b49c"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Td , class Allocatord &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a1f70de4b1edeb1a64078ad3113014a8e">Flatten</a> (<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; Td, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocatord &gt; &amp;data) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns all values in a vector.  <a href="#a1f70de4b1edeb1a64078ad3113014a8e"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Td , class Allocatord &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a84d542b134e8f98e944932b154c1d48d">Flatten</a> (int beg, int end, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; Td, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocatord &gt; &amp;data) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns in a vector all values from a range of inner vectors of vectors.  <a href="#a84d542b134e8f98e944932b154c1d48d"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Td , class Allocatord &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a84310d341a922124b1b6c9f5a541e998">Flatten</a> (int beg0, int end0, int beg1, int end1, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; Td, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocatord &gt; &amp;data) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns in a vector all values from a range of inner vectors and of inner vectors of vectors.  <a href="#a84310d341a922124b1b6c9f5a541e998"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd">PushBack</a> (int i, int j, const T &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Appends an element at the end of the inner vector #<em>i</em> #<em>j</em>.  <a href="#a9bd517dbe558026e398730fd0c604edd"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a71fd6304ae029838b323c8092a2a109f">PushBack</a> (int i, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, Allocator0 &gt; &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Appends an inner vector at the end of the inner vector of vectors #<em>i</em>.  <a href="#a71fd6304ae029838b323c8092a2a109f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a99bb83b050637e5e1d2aaedd540a33f4">PushBack</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, Allocator0 &gt;, Vect_Full, Allocator1 &gt; &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Appends an inner vector of vectors at the end of the vector.  <a href="#a99bb83b050637e5e1d2aaedd540a33f4"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#aabfc0ec2c12884196ebe4ce2ba4638da">PushBack</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, Allocator0 &gt;, Vect_Full, Allocator1 &gt;, Vect_Full, Allocator2 &gt; &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Appends a vector of vectors of vectors.  <a href="#aabfc0ec2c12884196ebe4ce2ba4638da"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a96ad43bc29d35bf3a1b1bffc507d8d46">PushBack</a> (const <a class="el" href="class_seldon_1_1_vector3.php">Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt; &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Appends a vector of vectors of vectors.  <a href="#a96ad43bc29d35bf3a1b1bffc507d8d46"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0e178a2f8fad92f0929f4ea1b1dd1604"></a><!-- doxytag: member="Seldon::Vector3::Clear" ref="a0e178a2f8fad92f0929f4ea1b1dd1604" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a0e178a2f8fad92f0929f4ea1b1dd1604">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the vector. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a173d2e7734fb3098785523c63488a577">Clear</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears a given inner vector of vectors.  <a href="#a173d2e7734fb3098785523c63488a577"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a3a1c6051634fc7ec05f011779b855f5e">Clear</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears a given inner vector.  <a href="#a3a1c6051634fc7ec05f011779b855f5e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a0d6388a07fda225152eef87cdab2ab5f">Fill</a> (const T &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the vector with a given value.  <a href="#a0d6388a07fda225152eef87cdab2ab5f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <br class="typebreak"/>
Vect_Full, Allocator0 &gt;<br class="typebreak"/>
, Vect_Full, Allocator1 &gt;<br class="typebreak"/>
, Vect_Full, Allocator2 &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17">GetVector</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the vector of vectors of vectors.  <a href="#aadaf82b78ca64f7278c081b5b17b6c17"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a><br class="typebreak"/>
&lt; T, Vect_Full, Allocator0 &gt;<br class="typebreak"/>
, Vect_Full, Allocator1 &gt;<br class="typebreak"/>
, Vect_Full, Allocator2 &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#acc38f6e8f4660dc1c98999b08a467454">GetVector</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the vector of vectors of vectors.  <a href="#acc38f6e8f4660dc1c98999b08a467454"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, <br class="typebreak"/>
Allocator0 &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
Allocator1 &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a356bbfa79f98b46d264aaf80def94eb0">GetVector</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a given inner vector of vectors.  <a href="#a356bbfa79f98b46d264aaf80def94eb0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <br class="typebreak"/>
Vect_Full, Allocator0 &gt;<br class="typebreak"/>
, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator1 &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a0d684b2dc3e14d471e6af816d3d11e36">GetVector</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a given inner vector of vectors.  <a href="#a0d684b2dc3e14d471e6af816d3d11e36"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, <br class="typebreak"/>
Allocator0 &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a1770eeb944cb8eb3d8600a9e5fdba6b5">GetVector</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a given inner vector.  <a href="#a1770eeb944cb8eb3d8600a9e5fdba6b5"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, <br class="typebreak"/>
Allocator0 &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#ad5fd1f0b6dac7bd14faf617633a3a364">GetVector</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a given inner vector.  <a href="#ad5fd1f0b6dac7bd14faf617633a3a364"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <br class="typebreak"/>
Vect_Full, Allocator0 &gt;<br class="typebreak"/>
, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator1 &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#aaacba77cabdd3d80b2809778ee76206b">operator()</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a given inner vector of vectors.  <a href="#aaacba77cabdd3d80b2809778ee76206b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, <br class="typebreak"/>
Allocator0 &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
Allocator1 &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a3cc0d7b9283c1069cee2770860c39133">operator()</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a given inner vector of vectors.  <a href="#a3cc0d7b9283c1069cee2770860c39133"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, <br class="typebreak"/>
Allocator0 &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a1298a6ec16e6a70ce7de2e456a0c56aa">operator()</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a given inner vector.  <a href="#a1298a6ec16e6a70ce7de2e456a0c56aa"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, <br class="typebreak"/>
Allocator0 &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#ac1ba1e37b274b6a1522a5db89b1e5c7a">operator()</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a given inner vector.  <a href="#ac1ba1e37b274b6a1522a5db89b1e5c7a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#afc9bb303c83fd61af66cb9f2b7ea9842">operator()</a> (int i, int j, int k) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns an element of a given inner vector of a given vector of vectors.  <a href="#afc9bb303c83fd61af66cb9f2b7ea9842"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#ad67a55c6828c310590dae6b671ff2d07">operator()</a> (int i, int j, int k)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns an element of a given inner vector of a given vector of vectors.  <a href="#ad67a55c6828c310590dae6b671ff2d07"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9b3c87591141123ac43759d6b9ca9531"></a><!-- doxytag: member="Seldon::Vector3::Print" ref="a9b3c87591141123ac43759d6b9ca9531" args="() const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a9b3c87591141123ac43759d6b9ca9531">Print</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays the vector. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#ad0372ca6e3d3dbee249dddd5eb4c4363">Write</a> (string file_name, bool with_size=true) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the instance in a binary file.  <a href="#ad0372ca6e3d3dbee249dddd5eb4c4363"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a05d48fe1fd82579d1138e9677852ccca">Write</a> (ostream &amp;file_stream, bool with_size=true) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the instance in a stream in a binary format.  <a href="#a05d48fe1fd82579d1138e9677852ccca"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#ac28c5fe01b2ac97d21e560e9282444b5">Read</a> (string file_name, bool with_size=true)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the <a class="el" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3</a> from a file.  <a href="#ac28c5fe01b2ac97d21e560e9282444b5"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector3.php#a96c8cfd79eaa900690893540509562f6">Read</a> (istream &amp;file_stream, bool with_size=true)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the <a class="el" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3</a> from a stream.  <a href="#a96c8cfd79eaa900690893540509562f6"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2f3334676d096fe0ed5bf28911c36679"></a><!-- doxytag: member="Seldon::Vector3::data_" ref="a2f3334676d096fe0ed5bf28911c36679" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <br class="typebreak"/>
Vect_Full, Allocator0 &gt;<br class="typebreak"/>
, Vect_Full, Allocator1 &gt;<br class="typebreak"/>
, Vect_Full, Allocator2 &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Allocator0 = SELDON_VECTOR3_DEFAULT_ALLOCATOR_0&lt;T&gt;, class Allocator1 = SELDON_VECTOR3_DEFAULT_ALLOCATOR_1&lt;	      Vector&lt;T, Vect_Full, Allocator0&gt; &gt;, class Allocator2 = SELDON_VECTOR3_DEFAULT_ALLOCATOR_2&lt;	      Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;,		     Vect_Full, Allocator1&gt; &gt;&gt;<br/>
 class Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</h3>

<p>Vector of vectors of vectors. </p>
<p><a class="el" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3</a> is a structure that acts like a vector of vectors of vectors. Both inner vectors and inner vectors of vectors can be of any dimension, so that this structure is more flexible than an <a class="el" href="class_seldon_1_1_array3_d.php" title="3D array.">Array3D</a>. </p>
<dl><dt><b>Template Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>T</em>&nbsp;</td><td>numerical type of the inner vectors. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>Allocator0</em>&nbsp;</td><td>allocator for the inner vectors. The default allocator is SELDON_DEFAULT_ALLOCATOR. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>Allocator1</em>&nbsp;</td><td>allocator for the vector of vectors. It is recommended to choose <a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a> or, for more efficient in reallocations, <a class="el" href="class_seldon_1_1_malloc_object.php">MallocObject</a> (default allocator here): these allocators can manage an array of inner vectors. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>Allocator2</em>&nbsp;</td><td>allocator for the vector of vectors of vectors. It is recommended to choose <a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a> or, for more efficient in reallocations, <a class="el" href="class_seldon_1_1_malloc_object.php">MallocObject</a> (default allocator here). </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8hxx_source.php#l00072">72</a> of file <a class="el" href="_vector3_8hxx_source.php">Vector3.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="a3425eec2b0400057adeda33a005a188d"></a><!-- doxytag: member="Seldon::Vector3::Vector3" ref="a3425eec2b0400057adeda33a005a188d" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::<a class="el" href="class_seldon_1_1_vector3.php">Vector3</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>Nothing is allocated. </p>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00043">43</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a8384e50e394357e4b9a6c0699f39f241"></a><!-- doxytag: member="Seldon::Vector3::Vector3" ref="a8384e50e394357e4b9a6c0699f39f241" args="(int)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::<a class="el" href="class_seldon_1_1_vector3.php">Vector3</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>length</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Constructor. </p>
<p>The vector of vectors of vectors is allocated with <em>length</em> empty vectors of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>length</em>&nbsp;</td><td>the length of the vector of vectors of vectors. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00054">54</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae42d9b0d42feb5459c7efea52c2f47ac"></a><!-- doxytag: member="Seldon::Vector3::Vector3" ref="ae42d9b0d42feb5459c7efea52c2f47ac" args="(Vector&lt; int &gt; &amp;length)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::<a class="el" href="class_seldon_1_1_vector3.php">Vector3</a> </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>length</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Constructor. </p>
<p>The vector of vectors of vectors and the inner vectors of vectors are allocated. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>length</em>&nbsp;</td><td>the lengths of the inner vectors of vectors. The vector of vectors of vectors will obviously have as many elements as <em>length</em> has. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00069">69</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9fff1c8ce72eb5dc5c002b6f1c0e7b86"></a><!-- doxytag: member="Seldon::Vector3::Vector3" ref="a9fff1c8ce72eb5dc5c002b6f1c0e7b86" args="(Vector&lt; Vector&lt; int &gt;, Vect_Full, Allocator &gt; &amp;length)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
<div class="memtemplate">
template&lt;class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::<a class="el" href="class_seldon_1_1_vector3.php">Vector3</a> </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt;, Vect_Full, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>length</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Constructor. </p>
<p>The vector of vectors of vectors, the inner vectors of vectors and the inner vectors are allocated. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>length</em>&nbsp;</td><td>the lengths of the inner vectors of vectors and the inner vectors. The vector of vectors of vectors will obviously have as many elements as <em>length</em> has. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00092">92</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae85c3b686a4100a5c05dc7d82a4b6b80"></a><!-- doxytag: member="Seldon::Vector3::~Vector3" ref="ae85c3b686a4100a5c05dc7d82a4b6b80" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::~<a class="el" href="class_seldon_1_1_vector3.php">Vector3</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Destructor. </p>
<p>The vector of vectors of vectors, the inner vectors of vectors and the inner vectors are deallocated. </p>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00117">117</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="a173d2e7734fb3098785523c63488a577"></a><!-- doxytag: member="Seldon::Vector3::Clear" ref="a173d2e7734fb3098785523c63488a577" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::Clear </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears a given inner vector of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the vector of vectors to be cleared. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00575">575</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3a1c6051634fc7ec05f011779b855f5e"></a><!-- doxytag: member="Seldon::Vector3::Clear" ref="a3a1c6051634fc7ec05f011779b855f5e" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::Clear </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears a given inner vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the vector to be cleared. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>index of the vector to be cleared. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00587">587</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0d6388a07fda225152eef87cdab2ab5f"></a><!-- doxytag: member="Seldon::Vector3::Fill" ref="a0d6388a07fda225152eef87cdab2ab5f" args="(const T &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::Fill </td>
          <td>(</td>
          <td class="paramtype">const T &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the vector with a given value. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>x</em>&nbsp;</td><td>value to fill the vector with. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00598">598</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1f70de4b1edeb1a64078ad3113014a8e"></a><!-- doxytag: member="Seldon::Vector3::Flatten" ref="a1f70de4b1edeb1a64078ad3113014a8e" args="(Vector&lt; Td, VectFull, Allocatord &gt; &amp;data) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
<div class="memtemplate">
template&lt;class Td , class Allocatord &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::Flatten </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; Td, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocatord &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>data</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns all values in a vector. </p>
<p>The output vector <em>data</em> contains all inner vectors concatenated in the same order as they appear in the current <a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> instance. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[out]</tt>&nbsp;</td><td valign="top"><em>data</em>&nbsp;</td><td>all values from the current <a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> instance. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00384">384</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a84310d341a922124b1b6c9f5a541e998"></a><!-- doxytag: member="Seldon::Vector3::Flatten" ref="a84310d341a922124b1b6c9f5a541e998" args="(int beg0, int end0, int beg1, int end1, Vector&lt; Td, VectFull, Allocatord &gt; &amp;data) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
<div class="memtemplate">
template&lt;class Td , class Allocatord &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::Flatten </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>beg0</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>end0</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>beg1</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>end1</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; Td, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocatord &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>data</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns in a vector all values from a range of inner vectors and of inner vectors of vectors. </p>
<p>The output vector <em>data</em> contains part of the inner vectors of vectors, in the index range [<em>beg0</em>, <em>end0</em>[, concatenated in the same order as they appear in the current <a class="el" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3</a> instance. For each inner vector of vectors, the range [<em>beg1</em>, <em>end1</em>[ of vectors is selected. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>beg0</em>&nbsp;</td><td>inclusive lower-bound for the indexes of inner vectors of vectors. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>end0</em>&nbsp;</td><td>exclusive upper-bound for the indexes of inner vectors of vectors. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>beg1</em>&nbsp;</td><td>inclusive lower-bound for the indexes of vectors to be selected in the inner vectors of vectors. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>end1</em>&nbsp;</td><td>exclusive upper-bound for the indexes of vectors to be selected in the inner vectors of vectors. </td></tr>
    <tr><td valign="top"><tt>[out]</tt>&nbsp;</td><td valign="top"><em>data</em>&nbsp;</td><td>the values contained in the inner vectors of vectors [<em>beg0</em>, <em>end0</em>[, in which the vectors [<em>beg1</em>, <em>end1</em>[ were selected. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00450">450</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a84d542b134e8f98e944932b154c1d48d"></a><!-- doxytag: member="Seldon::Vector3::Flatten" ref="a84d542b134e8f98e944932b154c1d48d" args="(int beg, int end, Vector&lt; Td, VectFull, Allocatord &gt; &amp;data) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
<div class="memtemplate">
template&lt;class Td , class Allocatord &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::Flatten </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>beg</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>end</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; Td, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocatord &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>data</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns in a vector all values from a range of inner vectors of vectors. </p>
<p>The output vector <em>data</em> contains all inner vectors, in the index range [<em>beg</em>, <em>end</em>[, concatenated in the same order as they appear in the current <a class="el" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3</a> instance. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>beg</em>&nbsp;</td><td>inclusive lower-bound for the indexes. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>end</em>&nbsp;</td><td>exclusive upper-bound for the indexes. </td></tr>
    <tr><td valign="top"><tt>[out]</tt>&nbsp;</td><td valign="top"><em>data</em>&nbsp;</td><td>the values contained in the inner vectors [<em>beg</em>, <em>end</em>[. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00407">407</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a51d4b3778ff8220b1c80377852826d7b"></a><!-- doxytag: member="Seldon::Vector3::GetLength" ref="a51d4b3778ff8220b1c80377852826d7b" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::GetLength </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the size of the inner vector #<em>i</em> #<em>j</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The size of the inner vector #<em>i</em> #<em>j</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00181">181</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a4a694febe8dba0a1203fd628cd1e3167"></a><!-- doxytag: member="Seldon::Vector3::GetLength" ref="a4a694febe8dba0a1203fd628cd1e3167" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::GetLength </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns size along dimension 1. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The size along dimension 1. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00132">132</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a89eac88f9d58edbf81884b2f8645219c"></a><!-- doxytag: member="Seldon::Vector3::GetLength" ref="a89eac88f9d58edbf81884b2f8645219c" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::GetLength </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the size of the inner vector of vectors #<em>i</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The size of the inner vector of vectors #<em>i</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00155">155</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab83e9c4434cdc9aeda454890ae557662"></a><!-- doxytag: member="Seldon::Vector3::GetNelement" ref="ab83e9c4434cdc9aeda454890ae557662" args="(int beg, int end) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::GetNelement </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>beg</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>end</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the total number of elements in a range of inner vectors. </p>
<p>Returns the total number of elements in the range [<em>beg</em>, <em>end</em>[ of inner vectors of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>beg</em>&nbsp;</td><td>inclusive lower-bound for the indexes. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>end</em>&nbsp;</td><td>exclusive upper-bound for the indexes. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The sum of the lengths of the inner vectors of vectors with index <em>beg</em> to <em>end-1</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00226">226</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0d7115fd1e3802ad9417a97f0a1fcad3"></a><!-- doxytag: member="Seldon::Vector3::GetNelement" ref="a0d7115fd1e3802ad9417a97f0a1fcad3" args="(int beg0, int end0, int beg1, int end1) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::GetNelement </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>beg0</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>end0</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>beg1</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>end1</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the total number of elements in a range of inner vectors. </p>
<p>Returns the total number of elements in the range [<em>beg0</em>, <em>end0</em>[ of inner vectors of vectors, in which only the vectors in [<em>beg1</em>, <em>end1</em>[ are selected. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>beg0</em>&nbsp;</td><td>inclusive lower-bound for the indexes of inner vectors of vectors. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>end0</em>&nbsp;</td><td>exclusive upper-bound for the indexes of inner vectors of vectors. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>beg1</em>&nbsp;</td><td>inclusive lower-bound for the indexes of vectors to be selected in the inner vectors of vectors. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>end1</em>&nbsp;</td><td>exclusive upper-bound for the indexes of vectors to be selected in the inner vectors of vectors. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The sum of the lengths of the inner vectors of vectors with index <em>beg</em> to <em>end-1</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00265">265</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a497379d28c87524052ee202702db9c91"></a><!-- doxytag: member="Seldon::Vector3::GetNelement" ref="a497379d28c87524052ee202702db9c91" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::GetNelement </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the total number of elements in the inner vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The sum of the lengths of the inner vectors. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00206">206</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5772120ddd32015c71c56ac2e44413e0"></a><!-- doxytag: member="Seldon::Vector3::GetShape" ref="a5772120ddd32015c71c56ac2e44413e0" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt; <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::GetShape </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the shape of a vector of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the vector of vectors. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>A vector with the lengths of the inner vectors of the <em>i</em> th vector of vectors. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00313">313</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2520f6f6ecf4d5853eaefe38904a4fc7"></a><!-- doxytag: member="Seldon::Vector3::GetShape" ref="a2520f6f6ecf4d5853eaefe38904a4fc7" args="(int i, Vector&lt; int &gt; &amp;shape) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::GetShape </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>shape</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the shape of a vector of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the vector of vectors. </td></tr>
    <tr><td valign="top"><tt>[out]</tt>&nbsp;</td><td valign="top"><em>shape</em>&nbsp;</td><td>the lengths of the inner vectors of the <em>i</em> th vector of vectors. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00330">330</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6b29a36baf352da31f2a97ec972fd761"></a><!-- doxytag: member="Seldon::Vector3::GetSize" ref="a6b29a36baf352da31f2a97ec972fd761" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::GetSize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the size of the inner vector of vectors #<em>i</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The size of the inner vector of vectors #<em>i</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00167">167</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a66946f1764cc14abec8a51d4e310f6b5"></a><!-- doxytag: member="Seldon::Vector3::GetSize" ref="a66946f1764cc14abec8a51d4e310f6b5" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::GetSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns size along dimension 1. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The size along dimension 1. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00143">143</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a81b5f3374df5a7f46ff50d60840eb81b"></a><!-- doxytag: member="Seldon::Vector3::GetSize" ref="a81b5f3374df5a7f46ff50d60840eb81b" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::GetSize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the size of the inner vector #<em>i</em> #<em>j</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The size of the inner vector #<em>i</em> #<em>j</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00195">195</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad5fd1f0b6dac7bd14faf617633a3a364"></a><!-- doxytag: member="Seldon::Vector3::GetVector" ref="ad5fd1f0b6dac7bd14faf617633a3a364" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, Allocator0 &gt; &amp; <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::GetVector </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a given inner vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>index of the inner vector. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The inner vector #<em>i</em> #<em>j</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00680">680</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1770eeb944cb8eb3d8600a9e5fdba6b5"></a><!-- doxytag: member="Seldon::Vector3::GetVector" ref="a1770eeb944cb8eb3d8600a9e5fdba6b5" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, Allocator0 &gt; &amp; <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::GetVector </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a given inner vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>index of the inner vector. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The inner vector #<em>i</em> #<em>j</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00666">666</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aadaf82b78ca64f7278c081b5b17b6c17"></a><!-- doxytag: member="Seldon::Vector3::GetVector" ref="aadaf82b78ca64f7278c081b5b17b6c17" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, Allocator0 &gt;, Vect_Full, Allocator1 &gt;, Vect_Full, Allocator2 &gt; &amp; <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::GetVector </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the vector of vectors of vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The vector of vectors of vectors. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00613">613</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="acc38f6e8f4660dc1c98999b08a467454"></a><!-- doxytag: member="Seldon::Vector3::GetVector" ref="acc38f6e8f4660dc1c98999b08a467454" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, Allocator0 &gt;, Vect_Full, Allocator1 &gt;, Vect_Full, Allocator2 &gt; &amp; <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::GetVector </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the vector of vectors of vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The vector of vectors of vectors. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00626">626</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a356bbfa79f98b46d264aaf80def94eb0"></a><!-- doxytag: member="Seldon::Vector3::GetVector" ref="a356bbfa79f98b46d264aaf80def94eb0" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, Allocator0 &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator1 &gt; &amp; <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::GetVector </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a given inner vector of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector of vectors. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The inner vector of vectors i. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00639">639</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0d684b2dc3e14d471e6af816d3d11e36"></a><!-- doxytag: member="Seldon::Vector3::GetVector" ref="a0d684b2dc3e14d471e6af816d3d11e36" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, Allocator0 &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator1 &gt; &amp; <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::GetVector </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a given inner vector of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector of vectors. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The inner vector of vectors i. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00652">652</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aaacba77cabdd3d80b2809778ee76206b"></a><!-- doxytag: member="Seldon::Vector3::operator()" ref="aaacba77cabdd3d80b2809778ee76206b" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, Allocator0 &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator1 &gt; &amp; <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a given inner vector of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector of vectors. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The inner vector of vectors i. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00700">700</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad67a55c6828c310590dae6b671ff2d07"></a><!-- doxytag: member="Seldon::Vector3::operator()" ref="ad67a55c6828c310590dae6b671ff2d07" args="(int i, int j, int k)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector3.php">Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::reference <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>k</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns an element of a given inner vector of a given vector of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector of vectors. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>index of the inner vector in the vector of vectors #<em>i</em>. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>k</em>&nbsp;</td><td>index of the element in the inner vector #<em>j</em>. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The element #<em>k</em> of vector #<em>j</em> of vector of vectors #<em>i</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00774">774</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3cc0d7b9283c1069cee2770860c39133"></a><!-- doxytag: member="Seldon::Vector3::operator()" ref="a3cc0d7b9283c1069cee2770860c39133" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, Allocator0 &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator1 &gt; &amp; <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a given inner vector of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector of vectors. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The inner vector of vectors i. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00713">713</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="afc9bb303c83fd61af66cb9f2b7ea9842"></a><!-- doxytag: member="Seldon::Vector3::operator()" ref="afc9bb303c83fd61af66cb9f2b7ea9842" args="(int i, int j, int k) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector3.php">Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::const_reference <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>k</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns an element of a given inner vector of a given vector of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector of vectors. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>index of the inner vector in the vector of vectors #<em>i</em>. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>k</em>&nbsp;</td><td>index of the element in the inner vector #<em>j</em>. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The element #<em>k</em> of vector #<em>j</em> of vector of vectors #<em>i</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00758">758</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac1ba1e37b274b6a1522a5db89b1e5c7a"></a><!-- doxytag: member="Seldon::Vector3::operator()" ref="ac1ba1e37b274b6a1522a5db89b1e5c7a" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, Allocator0 &gt; &amp; <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a given inner vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>index of the inner vector. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The inner vector #<em>i</em> #<em>j</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00742">742</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1298a6ec16e6a70ce7de2e456a0c56aa"></a><!-- doxytag: member="Seldon::Vector3::operator()" ref="a1298a6ec16e6a70ce7de2e456a0c56aa" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, Allocator0 &gt; &amp; <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a given inner vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>index of the inner vector. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The inner vector #<em>i</em> #<em>j</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00728">728</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9bd517dbe558026e398730fd0c604edd"></a><!-- doxytag: member="Seldon::Vector3::PushBack" ref="a9bd517dbe558026e398730fd0c604edd" args="(int i, int j, const T &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::PushBack </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const T &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Appends an element at the end of the inner vector #<em>i</em> #<em>j</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector of vectors. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>index of the inner vector to which <em>x</em> should be appended. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>x</em>&nbsp;</td><td>element to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00500">500</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a99bb83b050637e5e1d2aaedd540a33f4"></a><!-- doxytag: member="Seldon::Vector3::PushBack" ref="a99bb83b050637e5e1d2aaedd540a33f4" args="(const Vector&lt; Vector&lt; T, Vect_Full, Allocator0 &gt;, Vect_Full, Allocator1 &gt; &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Allocator0, class Allocator1, class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::PushBack </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, Allocator0 &gt;, Vect_Full, Allocator1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Appends an inner vector of vectors at the end of the vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>X</em>&nbsp;</td><td>inner vector of vectors to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00525">525</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aabfc0ec2c12884196ebe4ce2ba4638da"></a><!-- doxytag: member="Seldon::Vector3::PushBack" ref="aabfc0ec2c12884196ebe4ce2ba4638da" args="(const Vector&lt; Vector&lt; Vector&lt; T, Vect_Full, Allocator0 &gt;, Vect_Full, Allocator1 &gt;, Vect_Full, Allocator2 &gt; &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Allocator0, class Allocator1, class Allocator2&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::PushBack </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, Allocator0 &gt;, Vect_Full, Allocator1 &gt;, Vect_Full, Allocator2 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Appends a vector of vectors of vectors. </p>
<p>The inner vectors of vectors of <em>X</em> are appended to the current instance, in the same order as they appear in <em>X</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>X</em>&nbsp;</td><td>vector of vectors of vectors to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00539">539</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a71fd6304ae029838b323c8092a2a109f"></a><!-- doxytag: member="Seldon::Vector3::PushBack" ref="a71fd6304ae029838b323c8092a2a109f" args="(int i, const Vector&lt; T, Vect_Full, Allocator0 &gt; &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Allocator0, class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::PushBack </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Appends an inner vector at the end of the inner vector of vectors #<em>i</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector of vectors. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>X</em>&nbsp;</td><td>inner vector to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00513">513</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a96ad43bc29d35bf3a1b1bffc507d8d46"></a><!-- doxytag: member="Seldon::Vector3::PushBack" ref="a96ad43bc29d35bf3a1b1bffc507d8d46" args="(const Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt; &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Allocator0, class Allocator1, class Allocator2&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::PushBack </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector3.php">Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Appends a vector of vectors of vectors. </p>
<p>The inner vectors of vectors of <em>X</em> are appended to the current instance, in the same order as they appear in <em>X</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>X</em>&nbsp;</td><td>vector of vectors of vectors to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00554">554</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac28c5fe01b2ac97d21e560e9282444b5"></a><!-- doxytag: member="Seldon::Vector3::Read" ref="ac28c5fe01b2ac97d21e560e9282444b5" args="(string file_name, bool with_size=true)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>file_name</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the <a class="el" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3</a> from a file. </p>
<p>Sets the current <a class="el" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3</a> instance according to a binary file. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>file_name</em>&nbsp;</td><td>file name. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the shape is not available in the file, the shape of the current instance is thus unchanged and the values of the elements are directly read in the file. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00886">886</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a96c8cfd79eaa900690893540509562f6"></a><!-- doxytag: member="Seldon::Vector3::Read" ref="a96c8cfd79eaa900690893540509562f6" args="(istream &amp;file_stream, bool with_size=true)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>stream</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the <a class="el" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3</a> from a stream. </p>
<p>Sets the current <a class="el" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3</a> instance according to a binary stream. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in,out]</tt>&nbsp;</td><td valign="top"><em>stream</em>&nbsp;</td><td>input stream. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the shape is not available in the stream, the shape of the current instance is thus unchanged and the values of the elements are directly read in the stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00914">914</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a21e22b8d942820eb4c60e3e34c2afcd6"></a><!-- doxytag: member="Seldon::Vector3::Reallocate" ref="a21e22b8d942820eb4c60e3e34c2afcd6" args="(int N)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::Reallocate </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>N</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates the vector of vectors of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>N</em>&nbsp;</td><td>the new size of the vector of vectors of vectors. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00343">343</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a67835c87beb5c2539ffe8af0d4a0b49c"></a><!-- doxytag: member="Seldon::Vector3::Reallocate" ref="a67835c87beb5c2539ffe8af0d4a0b49c" args="(int i, int j, int N)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::Reallocate </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>N</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates the inner vector #<em>i</em> #<em>j</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector of vectors. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>index of the inner vector to be reallocated. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>N</em>&nbsp;</td><td>the new vector size. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00370">370</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5d5b76dbcaa0a445f089f2bdf341aba5"></a><!-- doxytag: member="Seldon::Vector3::Reallocate" ref="a5d5b76dbcaa0a445f089f2bdf341aba5" args="(int i, int N)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::Reallocate </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>N</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates the inner vector of vectors #<em>i</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector of vectors to be reallocated. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>N</em>&nbsp;</td><td>the new size of the inner vector of vectors #<em>i</em>. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00356">356</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad0372ca6e3d3dbee249dddd5eb4c4363"></a><!-- doxytag: member="Seldon::Vector3::Write" ref="ad0372ca6e3d3dbee249dddd5eb4c4363" args="(string file_name, bool with_size=true) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>file_name</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the instance in a binary file. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>file_name</em>&nbsp;</td><td>file name. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the sizes are not saved so that the shape of the instance is lost. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00811">811</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a05d48fe1fd82579d1138e9677852ccca"></a><!-- doxytag: member="Seldon::Vector3::Write" ref="a05d48fe1fd82579d1138e9677852ccca" args="(ostream &amp;file_stream, bool with_size=true) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3</a>&lt; T, Allocator0, Allocator1, Allocator2 &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>stream</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the instance in a stream in a binary format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in,out]</tt>&nbsp;</td><td valign="top"><em>stream</em>&nbsp;</td><td>output stream. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the sizes are not saved so that the shape of the instance is lost. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector3_8cxx_source.php#l00837">837</a> of file <a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>vector/<a class="el" href="_vector3_8hxx_source.php">Vector3.hxx</a></li>
<li>vector/<a class="el" href="_vector3_8cxx_source.php">Vector3.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
