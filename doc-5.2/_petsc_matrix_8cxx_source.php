<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix/PetscMatrix.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2012 INRIA</span>
<a name="l00002"></a>00002 <span class="comment">// Author(s): Marc Fragu</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_PETSCMATRIX_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;PetscMatrix.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 
<a name="l00026"></a>00026 <span class="keyword">namespace </span>Seldon
<a name="l00027"></a>00027 {
<a name="l00028"></a>00028 
<a name="l00029"></a>00029 
<a name="l00031"></a>00031 
<a name="l00034"></a>00034   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00035"></a>00035   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#aff9053f274de65aa8e0f69af8bd5491c" title="Default constructor.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;::PetscMatrix</a>():
<a name="l00036"></a>00036     Matrix_Base&lt;T, Allocator&gt;()
<a name="l00037"></a>00037   {
<a name="l00038"></a>00038     mpi_communicator_ = MPI_COMM_WORLD;
<a name="l00039"></a>00039     MatCreate(PETSC_COMM_WORLD, &amp;petsc_matrix_);
<a name="l00040"></a>00040     petsc_matrix_deallocated_ = <span class="keyword">false</span>;
<a name="l00041"></a>00041   }
<a name="l00042"></a>00042 
<a name="l00043"></a>00043 
<a name="l00045"></a>00045 
<a name="l00049"></a>00049   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00050"></a>00050   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#aff9053f274de65aa8e0f69af8bd5491c" title="Default constructor.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;::PetscMatrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l00051"></a>00051     Matrix_Base&lt;T, Allocator&gt;(i, j)
<a name="l00052"></a>00052   {
<a name="l00053"></a>00053     <span class="keywordtype">int</span> ierr;
<a name="l00054"></a>00054     mpi_communicator_ = MPI_COMM_WORLD;
<a name="l00055"></a>00055     MatCreate(mpi_communicator_, &amp;petsc_matrix_);
<a name="l00056"></a>00056     petsc_matrix_deallocated_ = <span class="keyword">false</span>;
<a name="l00057"></a>00057   }
<a name="l00058"></a>00058 
<a name="l00059"></a>00059 
<a name="l00061"></a>00061   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00062"></a>00062   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#aff9053f274de65aa8e0f69af8bd5491c" title="Default constructor.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00063"></a>00063 <a class="code" href="class_seldon_1_1_petsc_matrix.php#aff9053f274de65aa8e0f69af8bd5491c" title="Default constructor.">  ::PetscMatrix</a>(Mat&amp; A): Matrix_Base&lt;T, Allocator&gt;()
<a name="l00064"></a>00064   {
<a name="l00065"></a>00065     Copy(A);
<a name="l00066"></a>00066   }
<a name="l00067"></a>00067 
<a name="l00068"></a>00068 
<a name="l00070"></a>00070 
<a name="l00073"></a>00073   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00074"></a>00074   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#ab0fa102ad451a796f30de474ee7c036e" title="Sets the MPI communicator.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00075"></a>00075 <a class="code" href="class_seldon_1_1_petsc_matrix.php#ab0fa102ad451a796f30de474ee7c036e" title="Sets the MPI communicator.">  ::SetCommunicator</a>(MPI_Comm mpi_communicator)
<a name="l00076"></a>00076   {
<a name="l00077"></a>00077     mpi_communicator_ = mpi_communicator;
<a name="l00078"></a>00078   }
<a name="l00079"></a>00079 
<a name="l00080"></a>00080 
<a name="l00082"></a>00082 
<a name="l00085"></a>00085   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00086"></a>00086   <span class="keyword">inline</span> MPI_Comm <a class="code" href="class_seldon_1_1_petsc_matrix.php#a290f3593cb516534a86c0b9f5c1646c8" title="Returns the MPI communicator of the current PETSc matrix.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00087"></a>00087 <a class="code" href="class_seldon_1_1_petsc_matrix.php#a290f3593cb516534a86c0b9f5c1646c8" title="Returns the MPI communicator of the current PETSc matrix.">  ::GetCommunicator</a>()<span class="keyword"> const</span>
<a name="l00088"></a>00088 <span class="keyword">  </span>{
<a name="l00089"></a>00089     <span class="keywordflow">return</span> mpi_communicator_;
<a name="l00090"></a>00090   }
<a name="l00091"></a>00091 
<a name="l00092"></a>00092 
<a name="l00094"></a>00094   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00095"></a>00095   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#a791babbea4d834e8559e316e7605dbbc" title="Destructor.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00096"></a>00096 <a class="code" href="class_seldon_1_1_petsc_matrix.php#a791babbea4d834e8559e316e7605dbbc" title="Destructor.">  ::~PetscMatrix</a>()
<a name="l00097"></a>00097   {
<a name="l00098"></a>00098     Clear();
<a name="l00099"></a>00099   }
<a name="l00100"></a>00100 
<a name="l00101"></a>00101 
<a name="l00103"></a>00103 
<a name="l00107"></a>00107   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00108"></a>00108   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#a5344decb2cb632f0db5b2bb1204afcbe" title="Clears the matrix.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00109"></a>00109 <a class="code" href="class_seldon_1_1_petsc_matrix.php#a5344decb2cb632f0db5b2bb1204afcbe" title="Clears the matrix.">  ::Clear</a>()
<a name="l00110"></a>00110   {
<a name="l00111"></a>00111     <span class="keywordflow">if</span> (petsc_matrix_deallocated_)
<a name="l00112"></a>00112       <span class="keywordflow">return</span>;
<a name="l00113"></a>00113     <span class="keywordtype">int</span> ierr;
<a name="l00114"></a>00114     ierr = MatDestroy(&amp;petsc_matrix_);
<a name="l00115"></a>00115     CHKERRABORT(mpi_communicator_, ierr);
<a name="l00116"></a>00116     petsc_matrix_deallocated_ = <span class="keyword">true</span>;
<a name="l00117"></a>00117   }
<a name="l00118"></a>00118 
<a name="l00119"></a>00119 
<a name="l00121"></a>00121 
<a name="l00126"></a>00126   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00127"></a>00127   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#aec50a5001f0de562e2e20b1c9e166e29" title="Clears the matrix without releasing memory.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00128"></a>00128 <a class="code" href="class_seldon_1_1_petsc_matrix.php#aec50a5001f0de562e2e20b1c9e166e29" title="Clears the matrix without releasing memory.">  ::Nullify</a>()
<a name="l00129"></a>00129   {
<a name="l00130"></a>00130     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;PetscMatrix&lt;T, Prop, Storage, Allocator&gt;&quot;</span>
<a name="l00131"></a>00131                     <span class="stringliteral">&quot;::Nullify()&quot;</span>);
<a name="l00132"></a>00132   }
<a name="l00133"></a>00133 
<a name="l00134"></a>00134 
<a name="l00136"></a>00136 
<a name="l00139"></a>00139   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00140"></a>00140   <span class="keyword">inline</span> Mat&amp; <a class="code" href="class_seldon_1_1_petsc_matrix.php#a0fa3793829d97bf29211e30ab72384bf" title="Returns a reference on the inner petsc matrix.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00141"></a>00141 <a class="code" href="class_seldon_1_1_petsc_matrix.php#a0fa3793829d97bf29211e30ab72384bf" title="Returns a reference on the inner petsc matrix.">  ::GetPetscMatrix</a>()
<a name="l00142"></a>00142   {
<a name="l00143"></a>00143     <span class="keywordflow">return</span> petsc_matrix_;
<a name="l00144"></a>00144   }
<a name="l00145"></a>00145 
<a name="l00146"></a>00146 
<a name="l00148"></a>00148 
<a name="l00151"></a>00151   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00152"></a>00152   <span class="keyword">inline</span> <span class="keyword">const</span> Mat&amp; <a class="code" href="class_seldon_1_1_petsc_matrix.php#a0fa3793829d97bf29211e30ab72384bf" title="Returns a reference on the inner petsc matrix.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00153"></a>00153 <a class="code" href="class_seldon_1_1_petsc_matrix.php#a0fa3793829d97bf29211e30ab72384bf" title="Returns a reference on the inner petsc matrix.">  ::GetPetscMatrix</a>()<span class="keyword"> const</span>
<a name="l00154"></a>00154 <span class="keyword">  </span>{
<a name="l00155"></a>00155     <span class="keywordflow">return</span> petsc_matrix_;
<a name="l00156"></a>00156   }
<a name="l00157"></a>00157 
<a name="l00158"></a>00158 
<a name="l00160"></a>00160 
<a name="l00167"></a>00167   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00168"></a>00168   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#a0cfea8ec472b6f1b44fb04eae709f8f8" title="Reallocates memory to resize the matrix and keeps previous entries.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00169"></a>00169 <a class="code" href="class_seldon_1_1_petsc_matrix.php#a0cfea8ec472b6f1b44fb04eae709f8f8" title="Reallocates memory to resize the matrix and keeps previous entries.">  ::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00170"></a>00170   {
<a name="l00171"></a>00171     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;void PetscMatrix&lt;T, Prop, Storage, Allocator&gt;&quot;</span>
<a name="l00172"></a>00172                     <span class="stringliteral">&quot;::Resize(int i, int j)&quot;</span>);
<a name="l00173"></a>00173   }
<a name="l00174"></a>00174 
<a name="l00175"></a>00175 
<a name="l00178"></a>00178 
<a name="l00192"></a>00192   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00193"></a>00193   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#af9571d45be248575d07b866c490c1ba2" title="Changes the size of the matrix and sets its data array // (low level method).">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00194"></a>00194 <a class="code" href="class_seldon_1_1_petsc_matrix.php#af9571d45be248575d07b866c490c1ba2" title="Changes the size of the matrix and sets its data array // (low level method).">  ::SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, pointer data)
<a name="l00195"></a>00195   {
<a name="l00196"></a>00196     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;void PetscMatrix&lt;T, Prop, Storage, Allocator&gt;&quot;</span>
<a name="l00197"></a>00197                     <span class="stringliteral">&quot;::SetData(int i, int j, pointer data)&quot;</span>);
<a name="l00198"></a>00198   }
<a name="l00199"></a>00199 
<a name="l00200"></a>00200 
<a name="l00202"></a>00202 
<a name="l00208"></a>00208   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00209"></a>00209   <span class="keyword">inline</span> <span class="keyword">typename</span> PetscMatrix&lt;T, Prop, Storage, Allocator&gt;::const_reference
<a name="l00210"></a>00210   <a class="code" href="class_seldon_1_1_petsc_matrix.php#a0353d3a236f3f6f3a688b5e422a5e568" title="Access operator.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00211"></a>00211 <span class="keyword">  </span>{
<a name="l00212"></a>00212     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;PetscMatrix::Val(int i, int j) const&quot;</span>);
<a name="l00213"></a>00213   }
<a name="l00214"></a>00214 
<a name="l00215"></a>00215 
<a name="l00217"></a>00217 
<a name="l00223"></a>00223   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00224"></a>00224   <span class="keyword">inline</span> <span class="keyword">typename</span> PetscMatrix&lt;T, Prop, Storage, Allocator&gt;::reference
<a name="l00225"></a>00225   <a class="code" href="class_seldon_1_1_petsc_matrix.php#a0353d3a236f3f6f3a688b5e422a5e568" title="Access operator.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00226"></a>00226   {
<a name="l00227"></a>00227     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;PetscMatrix::Val(int i, int j)&quot;</span>);
<a name="l00228"></a>00228   }
<a name="l00229"></a>00229 
<a name="l00230"></a>00230 
<a name="l00232"></a>00232 
<a name="l00237"></a>00237   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00238"></a>00238   <span class="keyword">inline</span> <span class="keyword">typename</span> PetscMatrix&lt;T, Prop, Storage, Allocator&gt;::reference
<a name="l00239"></a>00239   <a class="code" href="class_seldon_1_1_petsc_matrix.php#ae4f22eb628e0e160dcf3789673748861" title="Access to elements of the data array.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;::operator[] </a>(<span class="keywordtype">int</span> i)
<a name="l00240"></a>00240   {
<a name="l00241"></a>00241     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;PetscMatrix::operator[] (int i)&quot;</span>);
<a name="l00242"></a>00242   }
<a name="l00243"></a>00243 
<a name="l00244"></a>00244 
<a name="l00246"></a>00246 
<a name="l00251"></a>00251   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00252"></a>00252   <span class="keyword">inline</span> <span class="keyword">typename</span> PetscMatrix&lt;T, Prop, Storage, Allocator&gt;::const_reference
<a name="l00253"></a>00253   <a class="code" href="class_seldon_1_1_petsc_matrix.php#ae4f22eb628e0e160dcf3789673748861" title="Access to elements of the data array.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;::operator[] </a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00254"></a>00254 <span class="keyword">  </span>{
<a name="l00255"></a>00255     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;PetscMatrix::operator[] (int i) const&quot;</span>);
<a name="l00256"></a>00256   }
<a name="l00257"></a>00257 
<a name="l00258"></a>00258 
<a name="l00259"></a>00259   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00260"></a>00260   <span class="keywordtype">void</span> PetscMatrix&lt;T, Prop, Storage, Allocator&gt;::Set(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, T value)
<a name="l00261"></a>00261   {
<a name="l00262"></a>00262     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;PetscMatrix::Set(int i, int j, T value)&quot;</span>);
<a name="l00263"></a>00263   }
<a name="l00264"></a>00264 
<a name="l00265"></a>00265 
<a name="l00267"></a>00267 
<a name="l00276"></a>00276   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00277"></a>00277   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#ad3cc1f14b9ba6ac1b459ce448d6adfd9" title="Inserts or adds values into certain locations of a matrix.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00278"></a>00278 <a class="code" href="class_seldon_1_1_petsc_matrix.php#ad3cc1f14b9ba6ac1b459ce448d6adfd9" title="Inserts or adds values into certain locations of a matrix.">  ::SetBuffer</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, T value, InsertMode insert_mode = INSERT_VALUES)
<a name="l00279"></a>00279   {
<a name="l00280"></a>00280     <span class="keywordtype">int</span> ierr;
<a name="l00281"></a>00281     ierr = MatSetValues(petsc_matrix_, 1, &amp;i, 1,
<a name="l00282"></a>00282                         &amp;j, &amp;value, insert_mode);
<a name="l00283"></a>00283     CHKERRABORT(mpi_communicator_, ierr);
<a name="l00284"></a>00284   }
<a name="l00285"></a>00285 
<a name="l00286"></a><a class="code" href="class_seldon_1_1_petsc_matrix.php#a2f608b718871d65dc96806c4f5867a60">00286</a> 
<a name="l00288"></a>00288   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00289"></a>00289   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#a2f608b718871d65dc96806c4f5867a60" title="Assembles the PETSc matrix.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;::Flush</a>()<span class="keyword"> const</span>
<a name="l00290"></a>00290 <span class="keyword">  </span>{
<a name="l00291"></a>00291     <span class="keywordtype">int</span> ierr;
<a name="l00292"></a>00292     ierr = MatAssemblyBegin(<a class="code" href="class_seldon_1_1_petsc_matrix.php#a472c2776d1ddbe98b586a6260ea9dcfc" title="Encapsulated PETSc matrix.">petsc_matrix_</a>, MAT_FINAL_ASSEMBLY);
<a name="l00293"></a>00293     CHKERRABORT(<a class="code" href="class_seldon_1_1_petsc_matrix.php#a71d0b850860f9c228b6efc8ea83a2898" title="The MPI communicator to use.">mpi_communicator_</a>, ierr);
<a name="l00294"></a>00294     ierr = MatAssemblyEnd(<a class="code" href="class_seldon_1_1_petsc_matrix.php#a472c2776d1ddbe98b586a6260ea9dcfc" title="Encapsulated PETSc matrix.">petsc_matrix_</a>, MAT_FINAL_ASSEMBLY);
<a name="l00295"></a>00295     CHKERRABORT(<a class="code" href="class_seldon_1_1_petsc_matrix.php#a71d0b850860f9c228b6efc8ea83a2898" title="The MPI communicator to use.">mpi_communicator_</a>, ierr);
<a name="l00296"></a>00296   }
<a name="l00297"></a>00297 
<a name="l00298"></a>00298 
<a name="l00300"></a>00300 
<a name="l00308"></a>00308   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00309"></a>00309   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#afb8978d1bd4774d18b4815ad454b62e9" title="Returns the range of row indices owned by this processor.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00310"></a>00310 <a class="code" href="class_seldon_1_1_petsc_matrix.php#afb8978d1bd4774d18b4815ad454b62e9" title="Returns the range of row indices owned by this processor.">  ::GetProcessorRowRange</a>(<span class="keywordtype">int</span>&amp; i, <span class="keywordtype">int</span>&amp; j)<span class="keyword"> const</span>
<a name="l00311"></a>00311 <span class="keyword">  </span>{
<a name="l00312"></a>00312     <span class="keywordtype">int</span> ierr;
<a name="l00313"></a>00313     ierr = MatGetOwnershipRange(petsc_matrix_, &amp;i, &amp;j);
<a name="l00314"></a>00314     CHKERRABORT(mpi_communicator_, ierr);
<a name="l00315"></a>00315   }
<a name="l00316"></a>00316 
<a name="l00317"></a>00317 
<a name="l00319"></a>00319 
<a name="l00324"></a>00324   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00325"></a>00325   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#a4b04405490e02061ddb68a4e33037106" title="Duplicates a matrix.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00326"></a>00326 <a class="code" href="class_seldon_1_1_petsc_matrix.php#a4b04405490e02061ddb68a4e33037106" title="Duplicates a matrix.">  ::Copy</a>(<span class="keyword">const</span> Mat&amp; A)
<a name="l00327"></a>00327   {
<a name="l00328"></a>00328     Clear();
<a name="l00329"></a>00329     <span class="keywordtype">int</span> ierr;
<a name="l00330"></a>00330     ierr = MatDuplicate(A, MAT_COPY_VALUES, &amp;petsc_matrix_);
<a name="l00331"></a>00331     CHKERRABORT(mpi_communicator_, ierr);
<a name="l00332"></a>00332     MatGetSize(A, &amp;this-&gt;m_, &amp;this-&gt;n_);
<a name="l00333"></a>00333     mpi_communicator_ = MPI_COMM_WORLD;
<a name="l00334"></a>00334     petsc_matrix_deallocated_ = <span class="keyword">false</span>;
<a name="l00335"></a>00335   }
<a name="l00336"></a>00336 
<a name="l00337"></a>00337 
<a name="l00339"></a>00339 
<a name="l00343"></a>00343   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00344"></a>00344   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#a452a9f334888ccca27951ce53e4b2cf9" title="Sets all elements to zero.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;::Zero</a>()
<a name="l00345"></a>00345   {
<a name="l00346"></a>00346     MatScale(<a class="code" href="class_seldon_1_1_petsc_matrix.php#a472c2776d1ddbe98b586a6260ea9dcfc" title="Encapsulated PETSc matrix.">petsc_matrix_</a>, T(0));
<a name="l00347"></a>00347   }
<a name="l00348"></a>00348 
<a name="l00349"></a><a class="code" href="class_seldon_1_1_petsc_matrix.php#a177616dc93ea00fc5a570cbc1cedf421">00349</a> 
<a name="l00351"></a>00351   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00352"></a>00352   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#a177616dc93ea00fc5a570cbc1cedf421" title="Sets the matrix to the identity.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;::SetIdentity</a>()
<a name="l00353"></a>00353   {
<a name="l00354"></a>00354     <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;void PetscMatrix&lt;T, Prop, Storage, Allocator&gt;&quot;</span>
<a name="l00355"></a>00355                     <span class="stringliteral">&quot;::SetIdentity()&quot;</span>);
<a name="l00356"></a>00356   }
<a name="l00357"></a>00357 
<a name="l00358"></a>00358 
<a name="l00360"></a>00360 
<a name="l00364"></a>00364   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00365"></a>00365   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#a3a3b27f67d957f4a7cce1708f8e1c8b8" title="Fills the matrix with 0, 1, 2, ...">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;::Fill</a>()
<a name="l00366"></a>00366   {
<a name="l00367"></a>00367     <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;void PetscMatrix&lt;T, Prop, Storage, Allocator&gt;&quot;</span>
<a name="l00368"></a>00368                     <span class="stringliteral">&quot;::Fill()&quot;</span>);
<a name="l00369"></a>00369   }
<a name="l00370"></a>00370 
<a name="l00371"></a>00371 
<a name="l00373"></a>00373 
<a name="l00376"></a>00376   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00377"></a>00377   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00378"></a>00378   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#a3a3b27f67d957f4a7cce1708f8e1c8b8" title="Fills the matrix with 0, 1, 2, ...">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;::Fill</a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00379"></a>00379   {
<a name="l00380"></a>00380     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;void PetscMatrix&lt;T, Prop, Storage, Allocator&gt;&quot;</span>
<a name="l00381"></a>00381                     <span class="stringliteral">&quot;::Fill(const T0&amp; x)&quot;</span>);
<a name="l00382"></a>00382   }
<a name="l00383"></a>00383 
<a name="l00384"></a>00384 
<a name="l00386"></a>00386 
<a name="l00389"></a>00389   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00390"></a>00390   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#a7a6c8b901b09261c340d88500d7066e0" title="Fills a matrix randomly.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;::FillRand</a>()
<a name="l00391"></a>00391   {
<a name="l00392"></a>00392     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;void PetscMatrix&lt;T, Prop, Storage, Allocator&gt;&quot;</span>
<a name="l00393"></a>00393                     <span class="stringliteral">&quot;::FillRand()&quot;</span>);
<a name="l00394"></a>00394   }
<a name="l00395"></a>00395 
<a name="l00396"></a>00396 
<a name="l00398"></a>00398 
<a name="l00409"></a>00409   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00410"></a>00410   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#a4b5bb6ff820bb4b06605fc367ee9b12c" title="Displays a sub-matrix on the standard output.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00411"></a>00411 <a class="code" href="class_seldon_1_1_petsc_matrix.php#a4b5bb6ff820bb4b06605fc367ee9b12c" title="Displays a sub-matrix on the standard output.">  ::Print</a>(<span class="keywordtype">int</span> a, <span class="keywordtype">int</span> b, <span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n)<span class="keyword"> const</span>
<a name="l00412"></a>00412 <span class="keyword">  </span>{
<a name="l00413"></a>00413     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;void PetscMatrix&lt;T, Prop, Storage, Allocator&gt;&quot;</span>
<a name="l00414"></a>00414                     <span class="stringliteral">&quot;::Print(int a, int b, int m, int n) const&quot;</span>);
<a name="l00415"></a>00415   }
<a name="l00416"></a>00416 
<a name="l00417"></a>00417 
<a name="l00419"></a>00419 
<a name="l00427"></a>00427   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00428"></a>00428   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#a4b5bb6ff820bb4b06605fc367ee9b12c" title="Displays a sub-matrix on the standard output.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;::Print</a>(<span class="keywordtype">int</span> l)<span class="keyword"> const</span>
<a name="l00429"></a>00429 <span class="keyword">  </span>{
<a name="l00430"></a>00430     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;void PetscMatrix&lt;T, Prop, Storage, Allocator&gt;&quot;</span>
<a name="l00431"></a>00431                     <span class="stringliteral">&quot;::Print(int l) const&quot;</span>);
<a name="l00432"></a>00432   }
<a name="l00433"></a>00433 
<a name="l00434"></a>00434 
<a name="l00435"></a>00435   <span class="comment">/**************************</span>
<a name="l00436"></a>00436 <span class="comment">   * INPUT/OUTPUT FUNCTIONS *</span>
<a name="l00437"></a>00437 <span class="comment">   **************************/</span>
<a name="l00438"></a>00438 
<a name="l00439"></a>00439 
<a name="l00441"></a>00441 
<a name="l00448"></a>00448   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00449"></a>00449   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#a87f057e43012523ccd9f2087f688aa11" title="Writes the matrix in a file.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00450"></a>00450 <a class="code" href="class_seldon_1_1_petsc_matrix.php#a87f057e43012523ccd9f2087f688aa11" title="Writes the matrix in a file.">  ::Write</a>(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size)<span class="keyword"> const</span>
<a name="l00451"></a>00451 <span class="keyword">  </span>{
<a name="l00452"></a>00452     ofstream FileStream;
<a name="l00453"></a>00453     FileStream.open(FileName.c_str());
<a name="l00454"></a>00454 
<a name="l00455"></a>00455 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00456"></a>00456 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00457"></a>00457     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00458"></a>00458       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;PetscMatrix::Write(string FileName)&quot;</span>,
<a name="l00459"></a>00459                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00460"></a>00460 <span class="preprocessor">#endif</span>
<a name="l00461"></a>00461 <span class="preprocessor"></span>
<a name="l00462"></a>00462     this-&gt;Write(FileStream, with_size);
<a name="l00463"></a>00463 
<a name="l00464"></a>00464     FileStream.close();
<a name="l00465"></a>00465   }
<a name="l00466"></a>00466 
<a name="l00467"></a>00467 
<a name="l00469"></a>00469 
<a name="l00476"></a>00476   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00477"></a>00477   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#a87f057e43012523ccd9f2087f688aa11" title="Writes the matrix in a file.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00478"></a>00478 <a class="code" href="class_seldon_1_1_petsc_matrix.php#a87f057e43012523ccd9f2087f688aa11" title="Writes the matrix in a file.">  ::Write</a>(ostream&amp; FileStream, <span class="keywordtype">bool</span> with_size)<span class="keyword"> const</span>
<a name="l00479"></a>00479 <span class="keyword">  </span>{
<a name="l00480"></a>00480     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;void PetscMatrix&lt;T, Prop, Storage, Allocator&gt;&quot;</span>
<a name="l00481"></a>00481                     <span class="stringliteral">&quot;::Write(ostream&amp; FileStream, bool with_size) const&quot;</span>);
<a name="l00482"></a>00482   }
<a name="l00483"></a>00483 
<a name="l00484"></a>00484 
<a name="l00486"></a>00486 
<a name="l00493"></a>00493   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00494"></a>00494   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#a88ebd49e399e37027904bce13e7f63c4" title="Writes the matrix in a file.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00495"></a>00495 <a class="code" href="class_seldon_1_1_petsc_matrix.php#a88ebd49e399e37027904bce13e7f63c4" title="Writes the matrix in a file.">  ::WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00496"></a>00496 <span class="keyword">  </span>{
<a name="l00497"></a>00497     ofstream FileStream;
<a name="l00498"></a>00498     FileStream.precision(cout.precision());
<a name="l00499"></a>00499     FileStream.flags(cout.flags());
<a name="l00500"></a>00500     FileStream.open(FileName.c_str());
<a name="l00501"></a>00501 
<a name="l00502"></a>00502 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00503"></a>00503 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00504"></a>00504     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00505"></a>00505       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;PetscMatrix::WriteText(string FileName)&quot;</span>,
<a name="l00506"></a>00506                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00507"></a>00507 <span class="preprocessor">#endif</span>
<a name="l00508"></a>00508 <span class="preprocessor"></span>
<a name="l00509"></a>00509     this-&gt;WriteText(FileStream);
<a name="l00510"></a>00510 
<a name="l00511"></a>00511     FileStream.close();
<a name="l00512"></a>00512   }
<a name="l00513"></a>00513 
<a name="l00514"></a>00514 
<a name="l00516"></a>00516 
<a name="l00523"></a>00523   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00524"></a>00524   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#a88ebd49e399e37027904bce13e7f63c4" title="Writes the matrix in a file.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00525"></a>00525 <a class="code" href="class_seldon_1_1_petsc_matrix.php#a88ebd49e399e37027904bce13e7f63c4" title="Writes the matrix in a file.">  ::WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00526"></a>00526 <span class="keyword">  </span>{
<a name="l00527"></a>00527     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;void PetscMatrix&lt;T, Prop, Storage, Allocator&gt;&quot;</span>
<a name="l00528"></a>00528                     <span class="stringliteral">&quot;::WriteText(ostream&amp; FileStream) const&quot;</span>);
<a name="l00529"></a>00529   }
<a name="l00530"></a>00530 
<a name="l00531"></a>00531 
<a name="l00533"></a>00533 
<a name="l00540"></a>00540   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00541"></a>00541   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#a2b5cb5cb3784d8754b5a2f3b73d256aa" title="Reads the matrix from a file.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;::Read</a>(<span class="keywordtype">string</span> FileName)
<a name="l00542"></a>00542   {
<a name="l00543"></a>00543     ifstream FileStream;
<a name="l00544"></a>00544     FileStream.open(FileName.c_str());
<a name="l00545"></a>00545 
<a name="l00546"></a>00546 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00547"></a>00547 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00548"></a>00548     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00549"></a>00549       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;PetscMatrix::Read(string FileName)&quot;</span>,
<a name="l00550"></a>00550                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00551"></a>00551 <span class="preprocessor">#endif</span>
<a name="l00552"></a>00552 <span class="preprocessor"></span>
<a name="l00553"></a>00553     this-&gt;<a class="code" href="class_seldon_1_1_petsc_matrix.php#a2b5cb5cb3784d8754b5a2f3b73d256aa" title="Reads the matrix from a file.">Read</a>(FileStream);
<a name="l00554"></a>00554 
<a name="l00555"></a>00555     FileStream.close();
<a name="l00556"></a>00556   }
<a name="l00557"></a>00557 
<a name="l00558"></a>00558 
<a name="l00560"></a>00560 
<a name="l00567"></a>00567   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00568"></a>00568   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#a2b5cb5cb3784d8754b5a2f3b73d256aa" title="Reads the matrix from a file.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00569"></a>00569 <a class="code" href="class_seldon_1_1_petsc_matrix.php#a2b5cb5cb3784d8754b5a2f3b73d256aa" title="Reads the matrix from a file.">  ::Read</a>(istream&amp; FileStream)
<a name="l00570"></a>00570   {
<a name="l00571"></a>00571     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;void PetscMatrix&lt;T, Prop, Storage, Allocator&gt;&quot;</span>
<a name="l00572"></a>00572                     <span class="stringliteral">&quot;::Read(istream&amp; FileStream) const&quot;</span>);
<a name="l00573"></a>00573   }
<a name="l00574"></a>00574 
<a name="l00575"></a>00575 
<a name="l00577"></a>00577 
<a name="l00581"></a>00581   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00582"></a>00582   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#a72c784f22fcba80ba6538f19bf29a9c1" title="Reads the matrix from a file.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;::ReadText</a>(<span class="keywordtype">string</span> FileName)
<a name="l00583"></a>00583   {
<a name="l00584"></a>00584     ifstream FileStream;
<a name="l00585"></a>00585     FileStream.open(FileName.c_str());
<a name="l00586"></a>00586 
<a name="l00587"></a>00587 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00588"></a>00588 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00589"></a>00589     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00590"></a>00590       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(string FileName)&quot;</span>,
<a name="l00591"></a>00591                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00592"></a>00592 <span class="preprocessor">#endif</span>
<a name="l00593"></a>00593 <span class="preprocessor"></span>
<a name="l00594"></a>00594     this-&gt;<a class="code" href="class_seldon_1_1_petsc_matrix.php#a72c784f22fcba80ba6538f19bf29a9c1" title="Reads the matrix from a file.">ReadText</a>(FileStream);
<a name="l00595"></a>00595 
<a name="l00596"></a>00596     FileStream.close();
<a name="l00597"></a>00597   }
<a name="l00598"></a>00598 
<a name="l00599"></a>00599 
<a name="l00601"></a>00601 
<a name="l00605"></a>00605   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00606"></a>00606   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_petsc_matrix.php#a72c784f22fcba80ba6538f19bf29a9c1" title="Reads the matrix from a file.">PetscMatrix&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00607"></a>00607 <a class="code" href="class_seldon_1_1_petsc_matrix.php#a72c784f22fcba80ba6538f19bf29a9c1" title="Reads the matrix from a file.">  ::ReadText</a>(istream&amp; FileStream)
<a name="l00608"></a>00608   {
<a name="l00609"></a>00609     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;void PetscMatrix&lt;T, Prop, Storage, Allocator&gt;&quot;</span>
<a name="l00610"></a>00610                     <span class="stringliteral">&quot;::ReadText(istream&amp; FileStream)&quot;</span>);
<a name="l00611"></a>00611   }
<a name="l00612"></a>00612 
<a name="l00613"></a>00613 
<a name="l00615"></a>00615   <span class="comment">// Matrix&lt;PETScSeqDense&gt; //</span>
<a name="l00617"></a>00617 <span class="comment"></span>
<a name="l00618"></a>00618 
<a name="l00619"></a>00619   <span class="comment">/****************</span>
<a name="l00620"></a>00620 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00621"></a>00621 <span class="comment">   ****************/</span>
<a name="l00622"></a>00622 
<a name="l00623"></a>00623 
<a name="l00625"></a>00625 
<a name="l00628"></a>00628   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00629"></a>00629   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_seq_dense_00_01_allocator_01_4.php#a6211ff52f749c50d5f935257e53a69fc" title="Default constructor.">Matrix&lt;T, Prop, PETScSeqDense, Allocator&gt;::Matrix</a>():
<a name="l00630"></a>00630     PetscMatrix&lt;T, Prop, RowMajor, Allocator&gt;()
<a name="l00631"></a>00631   {
<a name="l00632"></a>00632   }
<a name="l00633"></a>00633 
<a name="l00634"></a>00634 
<a name="l00636"></a>00636 
<a name="l00640"></a>00640   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00641"></a>00641   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_seq_dense_00_01_allocator_01_4.php#a6211ff52f749c50d5f935257e53a69fc" title="Default constructor.">Matrix&lt;T, Prop, PETScSeqDense, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l00642"></a>00642     PetscMatrix&lt;T, Prop, RowMajor, Allocator&gt;(i, j)
<a name="l00643"></a>00643   {
<a name="l00644"></a>00644     MatSetType(this-&gt;<a class="code" href="class_seldon_1_1_petsc_matrix.php#a472c2776d1ddbe98b586a6260ea9dcfc" title="Encapsulated PETSc matrix.">petsc_matrix_</a>, MATSEQDENSE);
<a name="l00645"></a>00645     MatSetSizes(this-&gt;<a class="code" href="class_seldon_1_1_petsc_matrix.php#a472c2776d1ddbe98b586a6260ea9dcfc" title="Encapsulated PETSc matrix.">petsc_matrix_</a>, PETSC_DECIDE, PETSC_DECIDE, i, j);
<a name="l00646"></a>00646   }
<a name="l00647"></a>00647 
<a name="l00648"></a>00648 
<a name="l00650"></a>00650 
<a name="l00653"></a>00653   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00654"></a>00654   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_seq_dense_00_01_allocator_01_4.php#a6211ff52f749c50d5f935257e53a69fc" title="Default constructor.">Matrix&lt;T, Prop, PETScSeqDense, Allocator&gt;::Matrix</a>(Mat&amp; A):
<a name="l00655"></a>00655   PetscMatrix&lt;T, Prop, RowMajor, Allocator&gt;(A)
<a name="l00656"></a>00656   {
<a name="l00657"></a>00657   }
<a name="l00658"></a>00658 
<a name="l00659"></a>00659 
<a name="l00661"></a>00661 
<a name="l00664"></a>00664   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00665"></a>00665   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_seq_dense_00_01_allocator_01_4.php#a6211ff52f749c50d5f935257e53a69fc" title="Default constructor.">Matrix&lt;T, Prop, PETScSeqDense, Allocator&gt;</a>
<a name="l00666"></a>00666 <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_seq_dense_00_01_allocator_01_4.php#a6211ff52f749c50d5f935257e53a69fc" title="Default constructor.">  ::Matrix</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, PETScSeqDense, Allocator&gt;&amp; A)
<a name="l00667"></a>00667   {
<a name="l00668"></a>00668     this-&gt;petsc_matrix_deallocated_ = <span class="keyword">true</span>;
<a name="l00669"></a>00669     Copy(A);
<a name="l00670"></a>00670   }
<a name="l00671"></a>00671 
<a name="l00672"></a>00672 
<a name="l00674"></a>00674 
<a name="l00680"></a>00680   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00681"></a>00681   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_seq_dense_00_01_allocator_01_4.php#a33e12a6614cab92e3955cee408371b40" title="Reallocates memory to resize the matrix.">Matrix&lt;T, Prop, PETScSeqDense, Allocator&gt;</a>
<a name="l00682"></a>00682 <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_seq_dense_00_01_allocator_01_4.php#a33e12a6614cab92e3955cee408371b40" title="Reallocates memory to resize the matrix.">  ::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00683"></a>00683   {
<a name="l00684"></a>00684     this-&gt;Clear();
<a name="l00685"></a>00685     <span class="keywordtype">int</span> ierr;
<a name="l00686"></a>00686     MatCreate(MPI_COMM_SELF, &amp;this-&gt;petsc_matrix_);
<a name="l00687"></a>00687     MatSetSizes(this-&gt;petsc_matrix_, i, j, i, j);
<a name="l00688"></a>00688     MatSetType(this-&gt;petsc_matrix_, MATSEQDENSE);
<a name="l00689"></a>00689     this-&gt;petsc_matrix_deallocated_ = <span class="keyword">false</span>;
<a name="l00690"></a>00690     this-&gt;Flush();
<a name="l00691"></a>00691   }
<a name="l00692"></a>00692 
<a name="l00693"></a>00693 
<a name="l00695"></a>00695 
<a name="l00701"></a>00701   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00702"></a>00702   <span class="keyword">inline</span> <span class="keyword">typename</span> Matrix&lt;T, Prop, PETScSeqDense, Allocator&gt;::value_type
<a name="l00703"></a>00703   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_seq_dense_00_01_allocator_01_4.php#a8046107e554eb4a16820332a34d9844f" title="Access operator.">Matrix&lt;T, Prop, PETScSeqDense, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00704"></a>00704   {
<a name="l00705"></a>00705 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00706"></a>00706 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00707"></a>00707       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;PetscMatrix&lt;PETScSeqDense&gt;::operator()&quot;</span>,
<a name="l00708"></a>00708                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00709"></a>00709                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00710"></a>00710                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00711"></a>00711     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00712"></a>00712       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;PetscMatrix&lt;PETScSeqDense&gt;::operator()&quot;</span>,
<a name="l00713"></a>00713                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00714"></a>00714                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00715"></a>00715 <span class="preprocessor">#endif</span>
<a name="l00716"></a>00716 <span class="preprocessor"></span>    PetscInt idxm[1] = {i};
<a name="l00717"></a>00717     PetscInt idxn[1] = {j};
<a name="l00718"></a>00718     PetscScalar v[1];
<a name="l00719"></a>00719     MatGetValues(this-&gt;petsc_matrix_, 1, idxm, 1, idxn, v);
<a name="l00720"></a>00720     <span class="keywordflow">return</span> v[0];
<a name="l00721"></a>00721   }
<a name="l00722"></a>00722 
<a name="l00723"></a>00723 
<a name="l00725"></a>00725 
<a name="l00731"></a>00731   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00732"></a>00732   <span class="keyword">inline</span> <span class="keyword">typename</span> Matrix&lt;T, Prop, PETScSeqDense, Allocator&gt;::value_type
<a name="l00733"></a>00733   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_seq_dense_00_01_allocator_01_4.php#a8046107e554eb4a16820332a34d9844f" title="Access operator.">Matrix&lt;T, Prop, PETScSeqDense, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00734"></a>00734 <span class="keyword">  </span>{
<a name="l00735"></a>00735 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00736"></a>00736 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00737"></a>00737       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;PetscMatrix&lt;PETScSeqDense&gt;::operator()&quot;</span>,
<a name="l00738"></a>00738                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00739"></a>00739                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00740"></a>00740                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00741"></a>00741     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00742"></a>00742       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;PetscMatrix&lt;PETScSeqDense&gt;::operator()&quot;</span>,
<a name="l00743"></a>00743                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00744"></a>00744                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00745"></a>00745                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00746"></a>00746 <span class="preprocessor">#endif</span>
<a name="l00747"></a>00747 <span class="preprocessor"></span>    PetscInt idxm[1] = {i};
<a name="l00748"></a>00748     PetscInt idxn[1] = {j};
<a name="l00749"></a>00749     PetscScalar v[1];
<a name="l00750"></a>00750     MatGetValues(this-&gt;petsc_matrix_, 1, idxm, 1, idxn, v);
<a name="l00751"></a>00751     <span class="keywordflow">return</span> v[0];
<a name="l00752"></a>00752   }
<a name="l00753"></a>00753 
<a name="l00754"></a>00754 
<a name="l00756"></a>00756 
<a name="l00761"></a>00761   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00762"></a>00762   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_seq_dense_00_01_allocator_01_4.php#ae697ed66617699a7ebf8467cf2d11bfc" title="Duplicates a matrix.">Matrix&lt;T, Prop, PETScSeqDense, Allocator&gt;</a>
<a name="l00763"></a>00763 <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_seq_dense_00_01_allocator_01_4.php#ae697ed66617699a7ebf8467cf2d11bfc" title="Duplicates a matrix.">  ::Copy</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, PETScSeqDense, Allocator&gt;&amp; A)
<a name="l00764"></a>00764   {
<a name="l00765"></a>00765     this-&gt;mpi_communicator_ = A.mpi_communicator_;
<a name="l00766"></a>00766     <a class="code" href="class_seldon_1_1_petsc_matrix.php#a4b04405490e02061ddb68a4e33037106" title="Duplicates a matrix.">PetscMatrix&lt;T, Prop, RowMajor, Allocator&gt;::Copy</a>(A.GetPetscMatrix());
<a name="l00767"></a>00767     this-&gt;petsc_matrix_deallocated_ = <span class="keyword">false</span>;
<a name="l00768"></a>00768   }
<a name="l00769"></a>00769 
<a name="l00770"></a>00770 
<a name="l00772"></a>00772 
<a name="l00777"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_seq_dense_00_01_allocator_01_4.php#a61b75cfc936d2263df0d8c451b18b025">00777</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00778"></a>00778   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_seq_dense_00_01_allocator_01_4.php">Matrix&lt;T, Prop, PETScSeqDense, Allocator&gt;</a>&amp;
<a name="l00779"></a>00779   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, PETScSeqDense, Allocator&gt;</a>
<a name="l00780"></a>00780 <a class="code" href="class_seldon_1_1_matrix.php">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_seq_dense_00_01_allocator_01_4.php">Matrix&lt;T, Prop, PETScSeqDense, Allocator&gt;</a>&amp; A)
<a name="l00781"></a>00781   {
<a name="l00782"></a>00782     this-&gt;Copy(A);
<a name="l00783"></a>00783     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00784"></a>00784   }
<a name="l00785"></a>00785 
<a name="l00786"></a>00786 
<a name="l00788"></a>00788 
<a name="l00793"></a>00793   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00794"></a>00794   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, PETScSeqDense, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00795"></a>00795 <span class="keyword">  </span>{
<a name="l00796"></a>00796     <span class="keywordtype">int</span> ierr;
<a name="l00797"></a>00797     ierr = MatView(this-&gt;petsc_matrix_, PETSC_VIEWER_STDOUT_SELF);
<a name="l00798"></a>00798     CHKERRABORT(this-&gt;mpi_communicator_, ierr);
<a name="l00799"></a>00799   }
<a name="l00800"></a>00800 
<a name="l00801"></a>00801 
<a name="l00803"></a>00803   <span class="comment">// Matrix&lt;PETScMPIDense&gt; //</span>
<a name="l00805"></a>00805 <span class="comment"></span>
<a name="l00806"></a>00806 
<a name="l00807"></a>00807   <span class="comment">/****************</span>
<a name="l00808"></a>00808 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00809"></a>00809 <span class="comment">   ****************/</span>
<a name="l00810"></a>00810 
<a name="l00811"></a>00811 
<a name="l00813"></a>00813 
<a name="l00816"></a>00816   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00817"></a>00817   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#a228557019fa6c0df6cb022dc79af77a2" title="Default constructor.">Matrix&lt;T, Prop, PETScMPIDense, Allocator&gt;::Matrix</a>():
<a name="l00818"></a>00818     PetscMatrix&lt;T, Prop, RowMajor, Allocator&gt;()
<a name="l00819"></a>00819   {
<a name="l00820"></a>00820   }
<a name="l00821"></a>00821 
<a name="l00822"></a>00822 
<a name="l00824"></a>00824 
<a name="l00828"></a>00828   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00829"></a>00829   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#a228557019fa6c0df6cb022dc79af77a2" title="Default constructor.">Matrix&lt;T, Prop, PETScMPIDense, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l00830"></a>00830     PetscMatrix&lt;T, Prop, RowMajor, Allocator&gt;(i, j)
<a name="l00831"></a>00831   {
<a name="l00832"></a>00832     MatSetType(this-&gt;petsc_matrix_, MATMPIDENSE);
<a name="l00833"></a>00833     MatSetSizes(this-&gt;petsc_matrix_, PETSC_DECIDE, PETSC_DECIDE, i, j);
<a name="l00834"></a>00834   }
<a name="l00835"></a>00835 
<a name="l00836"></a>00836 
<a name="l00838"></a>00838 
<a name="l00841"></a>00841   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00842"></a>00842   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#a228557019fa6c0df6cb022dc79af77a2" title="Default constructor.">Matrix&lt;T, Prop, PETScMPIDense, Allocator&gt;::Matrix</a>(Mat&amp; A):
<a name="l00843"></a>00843   PetscMatrix&lt;T, Prop, RowMajor, Allocator&gt;(A)
<a name="l00844"></a>00844   {
<a name="l00845"></a>00845   }
<a name="l00846"></a>00846 
<a name="l00847"></a>00847 
<a name="l00849"></a>00849 
<a name="l00852"></a>00852   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00853"></a>00853   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#a228557019fa6c0df6cb022dc79af77a2" title="Default constructor.">Matrix&lt;T, Prop, PETScMPIDense, Allocator&gt;</a>
<a name="l00854"></a>00854 <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#a228557019fa6c0df6cb022dc79af77a2" title="Default constructor.">  ::Matrix</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, PETScMPIDense, Allocator&gt;&amp; A)
<a name="l00855"></a>00855   {
<a name="l00856"></a>00856     this-&gt;petsc_matrix_deallocated_ = <span class="keyword">true</span>;
<a name="l00857"></a>00857     Copy(A);
<a name="l00858"></a>00858   }
<a name="l00859"></a>00859 
<a name="l00860"></a>00860 
<a name="l00862"></a>00862 
<a name="l00868"></a>00868   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00869"></a>00869   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#aaa6382ddfa0297d691e01634036ccdc3" title="Reallocates memory to resize the matrix.">Matrix&lt;T, Prop, PETScMPIDense, Allocator&gt;</a>
<a name="l00870"></a>00870 <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#aaa6382ddfa0297d691e01634036ccdc3" title="Reallocates memory to resize the matrix.">  ::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> local_i, <span class="keywordtype">int</span> local_j)
<a name="l00871"></a>00871   {
<a name="l00872"></a>00872     this-&gt;Clear();
<a name="l00873"></a>00873     <span class="keywordtype">int</span> ierr;
<a name="l00874"></a>00874     MatCreate(this-&gt;mpi_communicator_, &amp;this-&gt;petsc_matrix_);
<a name="l00875"></a>00875     MatSetType(this-&gt;petsc_matrix_, MATMPIDENSE);
<a name="l00876"></a>00876     MatSetSizes(this-&gt;petsc_matrix_, local_i, local_j, i, j);
<a name="l00877"></a>00877     this-&gt;m_ = i;
<a name="l00878"></a>00878     this-&gt;n_ = j;
<a name="l00879"></a>00879     this-&gt;petsc_matrix_deallocated_ = <span class="keyword">false</span>;
<a name="l00880"></a>00880     this-&gt;Flush();
<a name="l00881"></a>00881   }
<a name="l00882"></a>00882 
<a name="l00883"></a>00883 
<a name="l00885"></a>00885 
<a name="l00891"></a>00891   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00892"></a>00892   <span class="keyword">inline</span> <span class="keyword">typename</span> Matrix&lt;T, Prop, PETScMPIDense, Allocator&gt;::value_type
<a name="l00893"></a>00893   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#ad0b79cf0b9c6a74045a3aa3c94f5e467" title="Access operator.">Matrix&lt;T, Prop, PETScMPIDense, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00894"></a>00894   {
<a name="l00895"></a>00895 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00896"></a>00896 <span class="preprocessor"></span>    <span class="keywordtype">int</span> start, end;
<a name="l00897"></a>00897     this-&gt;GetProcessorRowRange(start, end);
<a name="l00898"></a>00898     <span class="keywordflow">if</span> (i &lt; start || i &gt;= end)
<a name="l00899"></a>00899       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;PetscMatrix&lt;PETScMPIDense&gt;::operator()&quot;</span>,
<a name="l00900"></a>00900                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [&quot;</span>)
<a name="l00901"></a>00901                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(start)
<a name="l00902"></a>00902                      + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end - 1)
<a name="l00903"></a>00903                      + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00904"></a>00904                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00905"></a>00905     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00906"></a>00906       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;PetscMatrix&lt;PETScMPIDense&gt;::operator()&quot;</span>,
<a name="l00907"></a>00907                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00908"></a>00908                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00909"></a>00909 <span class="preprocessor">#endif</span>
<a name="l00910"></a>00910 <span class="preprocessor"></span>    PetscInt idxm[1] = {i};
<a name="l00911"></a>00911     PetscInt idxn[1] = {j};
<a name="l00912"></a>00912     PetscScalar v[1];
<a name="l00913"></a>00913     MatGetValues(this-&gt;petsc_matrix_, 1, idxm, 1, idxn, v);
<a name="l00914"></a>00914     <span class="keywordflow">return</span> v[0];
<a name="l00915"></a>00915   }
<a name="l00916"></a>00916 
<a name="l00917"></a>00917 
<a name="l00919"></a>00919 
<a name="l00925"></a>00925   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00926"></a>00926   <span class="keyword">inline</span> <span class="keyword">typename</span> Matrix&lt;T, Prop, PETScMPIDense, Allocator&gt;::value_type
<a name="l00927"></a>00927   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#ad0b79cf0b9c6a74045a3aa3c94f5e467" title="Access operator.">Matrix&lt;T, Prop, PETScMPIDense, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00928"></a>00928 <span class="keyword">  </span>{
<a name="l00929"></a>00929 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00930"></a>00930 <span class="preprocessor"></span>    <span class="keywordtype">int</span> start, end;
<a name="l00931"></a>00931     this-&gt;GetProcessorRowRange(start, end);
<a name="l00932"></a>00932     <span class="keywordflow">if</span> (i &lt; start || i &gt;= end)
<a name="l00933"></a>00933       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;PetscMatrix&lt;PETScMPIDense&gt;::operator()&quot;</span>,
<a name="l00934"></a>00934                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [&quot;</span>)
<a name="l00935"></a>00935                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(start)
<a name="l00936"></a>00936                      + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00937"></a>00937                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00938"></a>00938     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00939"></a>00939       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;PetscMatrix&lt;PETScMPIDense&gt;::operator()&quot;</span>,
<a name="l00940"></a>00940                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00941"></a>00941                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00942"></a>00942                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00943"></a>00943 <span class="preprocessor">#endif</span>
<a name="l00944"></a>00944 <span class="preprocessor"></span>    PetscInt idxm[1] = {i};
<a name="l00945"></a>00945     PetscInt idxn[1] = {j};
<a name="l00946"></a>00946     PetscScalar v[1];
<a name="l00947"></a>00947     MatGetValues(this-&gt;petsc_matrix_, 1, idxm, 1, idxn, v);
<a name="l00948"></a>00948     <span class="keywordflow">return</span> v[0];
<a name="l00949"></a>00949   }
<a name="l00950"></a>00950 
<a name="l00951"></a>00951 
<a name="l00953"></a>00953 
<a name="l00958"></a>00958   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00959"></a>00959   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#aa713b223ff87d400513f920934592bfa" title="Duplicates a matrix.">Matrix&lt;T, Prop, PETScMPIDense, Allocator&gt;</a>
<a name="l00960"></a>00960 <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#aa713b223ff87d400513f920934592bfa" title="Duplicates a matrix.">  ::Copy</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, PETScMPIDense, Allocator&gt;&amp; A)
<a name="l00961"></a>00961   {
<a name="l00962"></a>00962     this-&gt;mpi_communicator_ = A.mpi_communicator_;
<a name="l00963"></a>00963     <a class="code" href="class_seldon_1_1_petsc_matrix.php#a4b04405490e02061ddb68a4e33037106" title="Duplicates a matrix.">PetscMatrix&lt;T, Prop, RowMajor, Allocator&gt;::Copy</a>(A.GetPetscMatrix());
<a name="l00964"></a>00964     this-&gt;petsc_matrix_deallocated_ = <span class="keyword">false</span>;
<a name="l00965"></a>00965   }
<a name="l00966"></a>00966 
<a name="l00967"></a>00967 
<a name="l00969"></a>00969 
<a name="l00974"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#a73133bbeda0d426fbdb7b59edeb09e5c">00974</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00975"></a>00975   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php">Matrix&lt;T, Prop, PETScMPIDense, Allocator&gt;</a>&amp;
<a name="l00976"></a>00976   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, PETScMPIDense, Allocator&gt;</a>
<a name="l00977"></a>00977 <a class="code" href="class_seldon_1_1_matrix.php">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php">Matrix&lt;T, Prop, PETScMPIDense, Allocator&gt;</a>&amp; A)
<a name="l00978"></a>00978   {
<a name="l00979"></a>00979     this-&gt;Copy(A);
<a name="l00980"></a>00980     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00981"></a>00981   }
<a name="l00982"></a>00982 
<a name="l00983"></a>00983 
<a name="l00985"></a>00985 
<a name="l00990"></a>00990   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00991"></a>00991   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, PETScMPIDense, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00992"></a>00992 <span class="keyword">  </span>{
<a name="l00993"></a>00993     <span class="keywordtype">int</span> ierr;
<a name="l00994"></a>00994     ierr = MatView(this-&gt;petsc_matrix_, PETSC_VIEWER_STDOUT_WORLD);
<a name="l00995"></a>00995     CHKERRABORT(this-&gt;mpi_communicator_, ierr);
<a name="l00996"></a>00996   }
<a name="l00997"></a>00997 
<a name="l00998"></a>00998 
<a name="l01000"></a>01000   <span class="comment">// Matrix&lt;PETScMPIAIJ&gt; //</span>
<a name="l01002"></a>01002 <span class="comment"></span>
<a name="l01003"></a>01003 
<a name="l01004"></a>01004   <span class="comment">/****************</span>
<a name="l01005"></a>01005 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01006"></a>01006 <span class="comment">   ****************/</span>
<a name="l01007"></a>01007 
<a name="l01008"></a>01008 
<a name="l01010"></a>01010 
<a name="l01013"></a>01013   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01014"></a>01014   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#ae1fb6818eb7e3f325d6f6f2d915217af" title="Default constructor.">Matrix&lt;T, Prop, PETScMPIAIJ, Allocator&gt;::Matrix</a>():
<a name="l01015"></a>01015     PetscMatrix&lt;T, Prop, RowMajor, Allocator&gt;()
<a name="l01016"></a>01016   {
<a name="l01017"></a>01017   }
<a name="l01018"></a>01018 
<a name="l01019"></a>01019 
<a name="l01021"></a>01021 
<a name="l01025"></a>01025   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01026"></a>01026   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#ae1fb6818eb7e3f325d6f6f2d915217af" title="Default constructor.">Matrix&lt;T, Prop, PETScMPIAIJ, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01027"></a>01027     PetscMatrix&lt;T, Prop, RowMajor, Allocator&gt;(i, j)
<a name="l01028"></a>01028   {
<a name="l01029"></a>01029     MatSetType(this-&gt;petsc_matrix_, MATMPIAIJ);
<a name="l01030"></a>01030     MatSetSizes(this-&gt;petsc_matrix_, PETSC_DECIDE, PETSC_DECIDE, i, j);
<a name="l01031"></a>01031   }
<a name="l01032"></a>01032 
<a name="l01033"></a>01033 
<a name="l01035"></a>01035 
<a name="l01038"></a>01038   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01039"></a>01039   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#ae1fb6818eb7e3f325d6f6f2d915217af" title="Default constructor.">Matrix&lt;T, Prop, PETScMPIAIJ, Allocator&gt;::Matrix</a>(Mat&amp; A):
<a name="l01040"></a>01040   PetscMatrix&lt;T, Prop, RowMajor, Allocator&gt;(A)
<a name="l01041"></a>01041   {
<a name="l01042"></a>01042   }
<a name="l01043"></a>01043 
<a name="l01044"></a>01044 
<a name="l01046"></a>01046 
<a name="l01049"></a>01049   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01050"></a>01050   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#ae1fb6818eb7e3f325d6f6f2d915217af" title="Default constructor.">Matrix&lt;T, Prop, PETScMPIAIJ, Allocator&gt;</a>
<a name="l01051"></a>01051 <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#ae1fb6818eb7e3f325d6f6f2d915217af" title="Default constructor.">  ::Matrix</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, PETScMPIAIJ, Allocator&gt;&amp; A)
<a name="l01052"></a>01052   {
<a name="l01053"></a>01053     this-&gt;petsc_matrix_deallocated_ = <span class="keyword">true</span>;
<a name="l01054"></a>01054     Copy(A);
<a name="l01055"></a>01055   }
<a name="l01056"></a>01056 
<a name="l01057"></a>01057 
<a name="l01059"></a>01059 
<a name="l01065"></a>01065   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01066"></a>01066   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#abdf3b65f08de384fd0527e89a5478998" title="Reallocates memory to resize the matrix.">Matrix&lt;T, Prop, PETScMPIAIJ, Allocator&gt;</a>
<a name="l01067"></a>01067 <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#abdf3b65f08de384fd0527e89a5478998" title="Reallocates memory to resize the matrix.">  ::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> local_i, <span class="keywordtype">int</span> local_j)
<a name="l01068"></a>01068   {
<a name="l01069"></a>01069     this-&gt;Clear();
<a name="l01070"></a>01070     <span class="keywordtype">int</span> ierr;
<a name="l01071"></a>01071     MatCreate(this-&gt;mpi_communicator_, &amp;this-&gt;petsc_matrix_);
<a name="l01072"></a>01072     MatSetType(this-&gt;petsc_matrix_, MATMPIAIJ);
<a name="l01073"></a>01073     MatSetSizes(this-&gt;petsc_matrix_, local_i, local_j, i, j);
<a name="l01074"></a>01074     this-&gt;m_ = i;
<a name="l01075"></a>01075     this-&gt;n_ = j;
<a name="l01076"></a>01076     this-&gt;petsc_matrix_deallocated_ = <span class="keyword">false</span>;
<a name="l01077"></a>01077     this-&gt;Flush();
<a name="l01078"></a>01078   }
<a name="l01079"></a>01079 
<a name="l01080"></a>01080 
<a name="l01082"></a>01082 
<a name="l01088"></a>01088   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01089"></a>01089   <span class="keyword">inline</span> <span class="keyword">typename</span> Matrix&lt;T, Prop, PETScMPIAIJ, Allocator&gt;::value_type
<a name="l01090"></a>01090   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#aac091fbb0b9613110e38a4f94da20ba3" title="Access operator.">Matrix&lt;T, Prop, PETScMPIAIJ, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01091"></a>01091   {
<a name="l01092"></a>01092 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01093"></a>01093 <span class="preprocessor"></span>    <span class="keywordtype">int</span> start, end;
<a name="l01094"></a>01094     this-&gt;GetProcessorRowRange(start, end);
<a name="l01095"></a>01095     <span class="keywordflow">if</span> (i &lt; start || i &gt;= end)
<a name="l01096"></a>01096       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;PetscMatrix&lt;PETScMPIAIJ&gt;::operator()&quot;</span>,
<a name="l01097"></a>01097                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [&quot;</span>)
<a name="l01098"></a>01098                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(start)
<a name="l01099"></a>01099                      + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end - 1)
<a name="l01100"></a>01100                      + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01101"></a>01101                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01102"></a>01102     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01103"></a>01103       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;PetscMatrix&lt;PETScMPIAIJ&gt;::operator()&quot;</span>,
<a name="l01104"></a>01104                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l01105"></a>01105                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01106"></a>01106 <span class="preprocessor">#endif</span>
<a name="l01107"></a>01107 <span class="preprocessor"></span>    PetscInt idxm[1] = {i};
<a name="l01108"></a>01108     PetscInt idxn[1] = {j};
<a name="l01109"></a>01109     PetscScalar v[1];
<a name="l01110"></a>01110     MatGetValues(this-&gt;petsc_matrix_, 1, idxm, 1, idxn, v);
<a name="l01111"></a>01111     <span class="keywordflow">return</span> v[0];
<a name="l01112"></a>01112   }
<a name="l01113"></a>01113 
<a name="l01114"></a>01114 
<a name="l01116"></a>01116 
<a name="l01122"></a>01122   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01123"></a>01123   <span class="keyword">inline</span> <span class="keyword">typename</span> Matrix&lt;T, Prop, PETScMPIAIJ, Allocator&gt;::value_type
<a name="l01124"></a>01124   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#aac091fbb0b9613110e38a4f94da20ba3" title="Access operator.">Matrix&lt;T, Prop, PETScMPIAIJ, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l01125"></a>01125 <span class="keyword">  </span>{
<a name="l01126"></a>01126 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01127"></a>01127 <span class="preprocessor"></span>    <span class="keywordtype">int</span> start, end;
<a name="l01128"></a>01128     this-&gt;GetProcessorRowRange(start, end);
<a name="l01129"></a>01129     <span class="keywordflow">if</span> (i &lt; start || i &gt;= end)
<a name="l01130"></a>01130       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;PetscMatrix&lt;PETScMPIAIJ&gt;::operator()&quot;</span>,
<a name="l01131"></a>01131                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [&quot;</span>)
<a name="l01132"></a>01132                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(start)
<a name="l01133"></a>01133                      + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01134"></a>01134                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01135"></a>01135     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01136"></a>01136       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;PetscMatrix&lt;PETScMPIAIJ&gt;::operator()&quot;</span>,
<a name="l01137"></a>01137                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l01138"></a>01138                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l01139"></a>01139                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01140"></a>01140 <span class="preprocessor">#endif</span>
<a name="l01141"></a>01141 <span class="preprocessor"></span>    PetscInt idxm[1] = {i};
<a name="l01142"></a>01142     PetscInt idxn[1] = {j};
<a name="l01143"></a>01143     PetscScalar v[1];
<a name="l01144"></a>01144     MatGetValues(this-&gt;petsc_matrix_, 1, idxm, 1, idxn, v);
<a name="l01145"></a>01145     <span class="keywordflow">return</span> v[0];
<a name="l01146"></a>01146   }
<a name="l01147"></a>01147 
<a name="l01148"></a>01148 
<a name="l01150"></a>01150 
<a name="l01155"></a>01155   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01156"></a>01156   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#ab542e6e5403f75d41b6d24d42400e16a" title="Duplicates a matrix.">Matrix&lt;T, Prop, PETScMPIAIJ, Allocator&gt;</a>
<a name="l01157"></a>01157 <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#ab542e6e5403f75d41b6d24d42400e16a" title="Duplicates a matrix.">  ::Copy</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, PETScMPIAIJ, Allocator&gt;&amp; A)
<a name="l01158"></a>01158   {
<a name="l01159"></a>01159     this-&gt;mpi_communicator_ = A.mpi_communicator_;
<a name="l01160"></a>01160     <a class="code" href="class_seldon_1_1_petsc_matrix.php#a4b04405490e02061ddb68a4e33037106" title="Duplicates a matrix.">PetscMatrix&lt;T, Prop, RowMajor, Allocator&gt;::Copy</a>(A.GetPetscMatrix());
<a name="l01161"></a>01161     this-&gt;petsc_matrix_deallocated_ = <span class="keyword">false</span>;
<a name="l01162"></a>01162   }
<a name="l01163"></a>01163 
<a name="l01164"></a>01164 
<a name="l01166"></a>01166 
<a name="l01171"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#a58810530bac6b6b662fdb4b9509ab431">01171</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01172"></a>01172   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Matrix&lt;T, Prop, PETScMPIAIJ, Allocator&gt;</a>&amp;
<a name="l01173"></a>01173   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, PETScMPIAIJ, Allocator&gt;</a>
<a name="l01174"></a>01174 <a class="code" href="class_seldon_1_1_matrix.php">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Matrix&lt;T, Prop, PETScMPIAIJ, Allocator&gt;</a>&amp; A)
<a name="l01175"></a>01175   {
<a name="l01176"></a>01176     this-&gt;Copy(A);
<a name="l01177"></a>01177     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01178"></a>01178   }
<a name="l01179"></a>01179 
<a name="l01180"></a>01180 
<a name="l01182"></a>01182 
<a name="l01187"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#a6c592a94514945ad3c1ea77dc2ef6f76">01187</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01188"></a>01188   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0,  <span class="keyword">class</span> Allocator0&gt;
<a name="l01189"></a>01189   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, PETScMPIAIJ, Allocator&gt;</a>
<a name="l01190"></a>01190 <a class="code" href="class_seldon_1_1_matrix.php">  ::Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, General, RowSparse, Allocator0&gt;</a>&amp; A)
<a name="l01191"></a>01191   {
<a name="l01192"></a>01192     this-&gt;Clear();
<a name="l01193"></a>01193 
<a name="l01194"></a>01194     <span class="keywordtype">int</span> ierr;
<a name="l01195"></a>01195     ierr = MatCreate(this-&gt;mpi_communicator_, &amp;this-&gt;petsc_matrix_);
<a name="l01196"></a>01196     CHKERRABORT(this-&gt;mpi_communicator_, ierr);
<a name="l01197"></a>01197 
<a name="l01198"></a>01198     <span class="keywordtype">int</span> ma = A.GetM();
<a name="l01199"></a>01199     <span class="keywordtype">int</span> na = A.GetN();
<a name="l01200"></a>01200     <span class="keywordtype">int</span> nnz = A.GetDataSize();
<a name="l01201"></a>01201     <span class="keywordtype">double</span> *value = A.GetData();
<a name="l01202"></a>01202     <span class="keywordtype">int</span> *column = A.GetInd();
<a name="l01203"></a>01203     <span class="keywordtype">int</span> *ptr = A.GetPtr();
<a name="l01204"></a>01204 
<a name="l01205"></a>01205     this-&gt;m_ = ma;
<a name="l01206"></a>01206     this-&gt;n_ = na;
<a name="l01207"></a>01207 
<a name="l01208"></a>01208     ierr = MatSetType(this-&gt;petsc_matrix_, MATMPIAIJ);
<a name="l01209"></a>01209     CHKERRABORT(this-&gt;mpi_communicator_, ierr);
<a name="l01210"></a>01210     ierr = MatSetSizes(this-&gt;petsc_matrix_, PETSC_DECIDE, PETSC_DECIDE,
<a name="l01211"></a>01211                        this-&gt;m_, this-&gt;n_);
<a name="l01212"></a>01212     CHKERRABORT(this-&gt;mpi_communicator_, ierr);
<a name="l01213"></a>01213     <span class="keywordtype">int</span> loc_start, loc_end;
<a name="l01214"></a>01214     ierr = MatGetOwnershipRange(this-&gt;petsc_matrix_,
<a name="l01215"></a>01215                                 &amp;loc_start, &amp;loc_end);
<a name="l01216"></a>01216     CHKERRABORT(this-&gt;mpi_communicator_, ierr);
<a name="l01217"></a>01217     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = loc_start; i &lt; loc_end; i++)
<a name="l01218"></a>01218       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = ptr[i]; j &lt; ptr[i + 1]; j++)
<a name="l01219"></a>01219         ierr = MatSetValues(this-&gt;petsc_matrix_, 1, &amp;i, 1, &amp;column[j],
<a name="l01220"></a>01220                             &amp;value[j], INSERT_VALUES);
<a name="l01221"></a>01221     ierr = MatAssemblyBegin(this-&gt;petsc_matrix_,MAT_FINAL_ASSEMBLY);
<a name="l01222"></a>01222     CHKERRABORT(this-&gt;mpi_communicator_, ierr);
<a name="l01223"></a>01223     ierr = MatAssemblyEnd(this-&gt;petsc_matrix_,MAT_FINAL_ASSEMBLY);
<a name="l01224"></a>01224     CHKERRABORT(this-&gt;mpi_communicator_, ierr);
<a name="l01225"></a>01225   }
<a name="l01226"></a>01226 
<a name="l01227"></a>01227 
<a name="l01229"></a>01229 
<a name="l01232"></a>01232   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01233"></a>01233   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, PETScMPIAIJ, Allocator&gt;::GetLocalM</a>()<span class="keyword"> const</span>
<a name="l01234"></a>01234 <span class="keyword">  </span>{
<a name="l01235"></a>01235     <span class="keywordtype">int</span> ierr;
<a name="l01236"></a>01236     <span class="keywordtype">int</span> m;
<a name="l01237"></a>01237     ierr = MatGetLocalSize(this-&gt;petsc_matrix_, &amp;m, PETSC_NULL);
<a name="l01238"></a>01238     CHKERRABORT(this-&gt;mpi_communicator_, ierr);
<a name="l01239"></a>01239     <span class="keywordflow">return</span> m;
<a name="l01240"></a>01240   }
<a name="l01241"></a>01241 
<a name="l01242"></a>01242 
<a name="l01244"></a>01244 
<a name="l01247"></a>01247   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01248"></a>01248   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#a695d0eadf5e3bec6a94a0155d3627fbc" title="Gets the number of local columns of the inner PETSc matrix.">Matrix&lt;T, Prop, PETScMPIAIJ, Allocator&gt;::GetLocalN</a>()<span class="keyword"> const</span>
<a name="l01249"></a>01249 <span class="keyword">  </span>{
<a name="l01250"></a>01250     <span class="keywordtype">int</span> ierr;
<a name="l01251"></a>01251     <span class="keywordtype">int</span> n;
<a name="l01252"></a>01252     ierr = MatGetLocalSize(this-&gt;petsc_matrix_, PETSC_NULL, &amp;n);
<a name="l01253"></a>01253     CHKERRABORT(this-&gt;mpi_communicator_, ierr);
<a name="l01254"></a>01254     <span class="keywordflow">return</span> n;
<a name="l01255"></a>01255   }
<a name="l01256"></a>01256 
<a name="l01257"></a>01257 
<a name="l01259"></a>01259 
<a name="l01264"></a>01264   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01265"></a>01265   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#ae9b1d53be394fd8ba4a8e58f9b3b0957" title="Displays the matrix on the standard output.">Matrix&lt;T, Prop, PETScMPIAIJ, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l01266"></a>01266 <span class="keyword">  </span>{
<a name="l01267"></a>01267     <span class="keywordtype">int</span> ierr;
<a name="l01268"></a>01268     ierr = MatView(this-&gt;petsc_matrix_, PETSC_VIEWER_STDOUT_WORLD);
<a name="l01269"></a>01269     CHKERRABORT(this-&gt;mpi_communicator_, ierr);
<a name="l01270"></a>01270   }
<a name="l01271"></a>01271 
<a name="l01272"></a>01272 
<a name="l01273"></a>01273 }
<a name="l01274"></a>01274 
<a name="l01275"></a>01275 
<a name="l01276"></a>01276 <span class="preprocessor">#define SELDON_FILE_MATRIX_PETSCMATRIX_CXX</span>
<a name="l01277"></a>01277 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
