<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>nlopt::SeldonOpt Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>, including all inherited members.<table>
  <tr bgcolor="#f0f0f0"><td><b>add_equality_constraint</b>(func f, void *f_data, double tol=0) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>add_equality_constraint</b>(svfunc vf, void *f_data, double tol=0) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>add_equality_constraint</b>(func f, void *f_data, nlopt_munge md, nlopt_munge mc, double tol=0) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>add_equality_mconstraint</b>(mfunc mf, void *f_data, const Seldon::Vector&lt; double &gt; &amp;tol) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>add_equality_mconstraint</b>(mfunc mf, void *f_data, nlopt_munge md, nlopt_munge mc, const Seldon::Vector&lt; double &gt; &amp;tol) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>add_inequality_constraint</b>(func f, void *f_data, double tol=0) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>add_inequality_constraint</b>(svfunc vf, void *f_data, double tol=0) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>add_inequality_constraint</b>(func f, void *f_data, nlopt_munge md, nlopt_munge mc, double tol=0) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>add_inequality_mconstraint</b>(mfunc mf, void *f_data, const Seldon::Vector&lt; double &gt; &amp;tol) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>add_inequality_mconstraint</b>(mfunc mf, void *f_data, nlopt_munge md, nlopt_munge mc, const Seldon::Vector&lt; double &gt; &amp;tol) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>force_stop</b>() (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>get_algorithm</b>() const  (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>get_algorithm_name</b>() const  (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>get_dimension</b>() const  (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>get_initial_step</b>(const Seldon::Vector&lt; double &gt; &amp;x, Seldon::Vector&lt; double &gt; &amp;dx) const  (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>get_initial_step_</b>(const Seldon::Vector&lt; double &gt; &amp;x) const  (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>last_optimize_result</b>() const  (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>last_optimum_value</b>() const  (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>operator=</b>(SeldonOpt const &amp;f) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>optimize</b>(Seldon::Vector&lt; double &gt; &amp;x, double &amp;opt_f) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>optimize</b>(const Seldon::Vector&lt; double &gt; &amp;x0) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>remove_equality_constraints</b>() (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>remove_inequality_constraints</b>() (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SeldonOpt</b>() (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SeldonOpt</b>(algorithm a, unsigned n) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SeldonOpt</b>(const SeldonOpt &amp;f) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>set_default_initial_step</b>(const Seldon::Vector&lt; double &gt; &amp;x) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>set_local_optimizer</b>(const SeldonOpt &amp;lo) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>set_max_objective</b>(func f, void *f_data) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>set_max_objective</b>(svfunc vf, void *f_data) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>set_max_objective</b>(func f, void *f_data, nlopt_munge md, nlopt_munge mc) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>set_min_objective</b>(func f, void *f_data) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>set_min_objective</b>(svfunc vf, void *f_data) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>set_min_objective</b>(func f, void *f_data, nlopt_munge md, nlopt_munge mc) (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>~SeldonOpt</b>() (defined in <a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>)</td><td><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td><code> [inline]</code></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
