<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a> &#124;
<a href="#pro-static-attribs">Static Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Matrix_ComplexSparse" --><!-- doxytag: inherits="Seldon::Matrix_Base" -->
<p>Complex sparse-matrix class.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_matrix___complex_sparse_8hxx_source.php">Matrix_ComplexSparse.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_matrix___complex_sparse.png" usemap="#Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;_map" alt=""/>
  <map id="Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;_map" name="Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;_map">
<area href="class_seldon_1_1_matrix___base.php" alt="Seldon::Matrix_Base&lt; T, Allocator &gt;" shape="rect" coords="0,0,367,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_matrix___complex_sparse-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5471d27b72c23536e7c5929780d8f086"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::value_type" ref="a5471d27b72c23536e7c5929780d8f086" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2c5f90bfb9957b51ed711ea4348fab5e"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::pointer" ref="a2c5f90bfb9957b51ed711ea4348fab5e" args="" -->
typedef Allocator::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6f73c61c200e6ff51cf252f9f879df7c"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::const_pointer" ref="a6f73c61c200e6ff51cf252f9f879df7c" args="" -->
typedef Allocator::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab45e8d6f696bdec29746e22bd3c45949"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::reference" ref="ab45e8d6f696bdec29746e22bd3c45949" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aef4005169f486bba0e6def7f2debd684"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::const_reference" ref="aef4005169f486bba0e6def7f2debd684" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a80848c730013088a9ebe491698ab3304"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::entry_type" ref="a80848c730013088a9ebe491698ab3304" args="" -->
typedef complex&lt; value_type &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>entry_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad38add04e1855655be79e6eb1317025d"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::access_type" ref="ad38add04e1855655be79e6eb1317025d" args="" -->
typedef complex&lt; value_type &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>access_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="addd99f24257e88e1a5ff0b3e794f6ba8"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::const_access_type" ref="addd99f24257e88e1a5ff0b3e794f6ba8" args="" -->
typedef complex&lt; value_type &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_access_type</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9">Matrix_ComplexSparse</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#ad680112fe370f8ce4621a70bbc5ce2d9"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a0105ca0b54784973e1272931f9c9e420">Matrix_ComplexSparse</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Constructor.  <a href="#a0105ca0b54784973e1272931f9c9e420"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a728b0dd58af72224fc92a6ab299ad274">Matrix_ComplexSparse</a> (int i, int j, int real_nz, int imag_nz)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Constructor.  <a href="#a728b0dd58af72224fc92a6ab299ad274"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Storage0 , class Allocator0 , class Storage1 , class Allocator1 , class Storage2 , class Allocator2 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a079f7006a23ebdc5e63c2337f88f9bfc">Matrix_ComplexSparse</a> (int i, int j, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;real_values, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;real_ptr, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;real_ind, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;imag_values, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;imag_ptr, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;imag_ind)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Constructor.  <a href="#a079f7006a23ebdc5e63c2337f88f9bfc"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3df0b27785c83bf1b8809cdd263c48c5"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::Matrix_ComplexSparse" ref="a3df0b27785c83bf1b8809cdd263c48c5" args="(const Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt; &amp;A)" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a3df0b27785c83bf1b8809cdd263c48c5">Matrix_ComplexSparse</a> (const <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copy constructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3b785e50c55d9c0e00180bc7d3332d75"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::~Matrix_ComplexSparse" ref="a3b785e50c55d9c0e00180bc7d3332d75" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a3b785e50c55d9c0e00180bc7d3332d75">~Matrix_ComplexSparse</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Destructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a8940cdf1faedc44051919b9774132fa9">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the matrix.  <a href="#a8940cdf1faedc44051919b9774132fa9"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Storage0 , class Allocator0 , class Storage1 , class Allocator1 , class Storage2 , class Allocator2 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a3729058de11ead808a09e98a10702d85">SetData</a> (int i, int j, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;real_values, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;real_ptr, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;real_ind, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;imag_values, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;imag_ptr, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;imag_ind)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Redefines the matrix.  <a href="#a3729058de11ead808a09e98a10702d85"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8828989769a80a0b86defd0f9b3322e3"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::SetData" ref="a8828989769a80a0b86defd0f9b3322e3" args="(int i, int j, int real_nz, pointer real_values, int *real_ptr, int *real_ind, int imag_nz, pointer imag_values, int *imag_ptr, int *imag_ind)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetData</b> (int i, int j, int real_nz, pointer real_values, int *real_ptr, int *real_ind, int imag_nz, pointer imag_values, int *imag_ptr, int *imag_ind)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a10672a9a570060c9bd8f17eb54dc46fa">Nullify</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the matrix without releasing memory.  <a href="#a10672a9a570060c9bd8f17eb54dc46fa"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a72602182671e7f970959772510eed557"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::Reallocate" ref="a72602182671e7f970959772510eed557" args="(int i, int j)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a72602182671e7f970959772510eed557">Reallocate</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Initialization of an empty matrix i x j. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8e14d527038311367db8ebfd79418995"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::Reallocate" ref="a8e14d527038311367db8ebfd79418995" args="(int i, int j, int real_nz, int imag_nz)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a8e14d527038311367db8ebfd79418995">Reallocate</a> (int i, int j, int real_nz, int imag_nz)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Initialization of a matrix i x j with n non-zero entries. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#aa53b17b254435486a52c058490abb0fb">Resize</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Changing the number of rows and columns.  <a href="#aa53b17b254435486a52c058490abb0fb"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a17e9f9857a46c900b33ab803ca584c2f">Resize</a> (int i, int j, int real_nz, int imag_nz)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Changing the number of rows, columns and non-zero entries.  <a href="#a17e9f9857a46c900b33ab803ca584c2f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae583202802b373763ce3780fe2382f4f"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::Copy" ref="ae583202802b373763ce3780fe2382f4f" args="(const Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt; &amp;A)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ae583202802b373763ce3780fe2382f4f">Copy</a> (const <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copies a matrix. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7a9910c17b34f71b34c8a95486774ad4"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetNonZeros" ref="a7a9910c17b34f71b34c8a95486774ad4" args="() const " -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetNonZeros</b> () const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a543d8834abfbd7888c6d2516612bde7b">GetDataSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored in memory.  <a href="#a543d8834abfbd7888c6d2516612bde7b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#afa783bff3e823f8ad2480a50d4d28929">GetRealPtr</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns (row or column) start indices for the real part.  <a href="#afa783bff3e823f8ad2480a50d4d28929"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a6766b6db59aa35b4fa7458a12e531e9b">GetImagPtr</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns (row or column) start indices for the imaginary part.  <a href="#a6766b6db59aa35b4fa7458a12e531e9b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a4eb0b0535cb51c9c0886da147f6e0cb0">GetRealInd</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns (row or column) indices of non-zero entries for the real part.  <a href="#a4eb0b0535cb51c9c0886da147f6e0cb0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a08f3d91609557b1650066765343909db">GetImagInd</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns (row or column) indices of non-zero entries //! for the imaginary part.  <a href="#a08f3d91609557b1650066765343909db"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a3be4bf190b7c5957d81ddf7e7a7b7882">GetRealPtrSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the length of the array of start indices for the real part.  <a href="#a3be4bf190b7c5957d81ddf7e7a7b7882"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ae97357d4afa167ca95d28e8897e44ef2">GetImagPtrSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the length of the array of start indices for the imaginary part.  <a href="#ae97357d4afa167ca95d28e8897e44ef2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ae573e32219c095276106254a84e7f684">GetRealIndSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the length of the array of (column or row) indices //! for the real part.  <a href="#ae573e32219c095276106254a84e7f684"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ad61069ad96f1bfd7b57f6a4e9fe85764">GetImagIndSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the length of the array of (column or row) indices //! for the imaginary part.  <a href="#ad61069ad96f1bfd7b57f6a4e9fe85764"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a00680b1689e564a1ed51dd34c4dcd0c9">GetRealDataSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the length of the array of (column or row) indices //! for the real part.  <a href="#a00680b1689e564a1ed51dd34c4dcd0c9"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a2aa0e0d1430941ccfecb8e2947801570">GetImagDataSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the length of the array of (column or row) indices //! for the imaginary part.  <a href="#a2aa0e0d1430941ccfecb8e2947801570"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a74325c4f424c0043682b5cb3ad3df501">GetRealData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the array of values of the real part.  <a href="#a74325c4f424c0043682b5cb3ad3df501"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a5637e2dd95b0e62a7f42c0f3fd1383b1">GetImagData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the array of values of the imaginary part.  <a href="#a5637e2dd95b0e62a7f42c0f3fd1383b1"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">complex&lt; value_type &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ad9e481e5ea6de45867d8952dd9676bea">operator()</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#ad9e481e5ea6de45867d8952dd9676bea"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">value_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a928e2aa74c9ad336e77ab76593b3c8da">ValReal</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#a928e2aa74c9ad336e77ab76593b3c8da"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const value_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a3a5cde7a78f4fea30cf13090eba0892f">ValReal</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#a3a5cde7a78f4fea30cf13090eba0892f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">value_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a9d49fb0a1544b3eda52ee5e002d0822d">ValImag</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#a9d49fb0a1544b3eda52ee5e002d0822d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const value_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a7b8f83d193758434bcb1cfe959518f91">ValImag</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#a7b8f83d193758434bcb1cfe959518f91"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">value_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a703b6043d3ad7c59cc36e9c5fd0a7d77">GetReal</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#a703b6043d3ad7c59cc36e9c5fd0a7d77"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const value_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ae8796f2d633623f1179a19a38d7f68e1">GetReal</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#ae8796f2d633623f1179a19a38d7f68e1"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">value_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a54303525013c8dc63f7064c4720b9348">GetImag</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#a54303525013c8dc63f7064c4720b9348"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const value_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a9fec8163944b4af720f89473c7675a34">GetImag</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#a9fec8163944b4af720f89473c7675a34"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ad297118a9c4023d96a871e6b95d78aa6">Set</a> (int i, int j, const complex&lt; T &gt; &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets an element (i, j) to a value.  <a href="#ad297118a9c4023d96a871e6b95d78aa6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ade592203fcc968e928f0083cd5e21644">AddInteraction</a> (int i, int j, const complex&lt; T &gt; &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Add a value to a non-zero entry.  <a href="#ade592203fcc968e928f0083cd5e21644"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a>&lt; T, Prop, <br class="typebreak"/>
Storage, Allocator &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a3e54cf340623d3c2c03f4926d9846383">operator=</a> (const <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a matrix (assignment operator).  <a href="#a3e54cf340623d3c2c03f4926d9846383"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#acdb7d1d285d2601b1ef970aecc861178">Zero</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Resets all non-zero entries to 0-value.  <a href="#acdb7d1d285d2601b1ef970aecc861178"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a7ca605152313a38a14014b92fb1947a5">SetIdentity</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the matrix to identity.  <a href="#a7ca605152313a38a14014b92fb1947a5"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a4f936ebae0d6d37d4ef7e71022ad0490">Fill</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the non-zero entries with 0, 1, 2, ...  <a href="#a4f936ebae0d6d37d4ef7e71022ad0490"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a1b17bf0f1b4be60ae64a51564737f834">Fill</a> (const complex&lt; T &gt; &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the non-zero entries with a given value.  <a href="#a1b17bf0f1b4be60ae64a51564737f834"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a99688ae138cf3093f657a1673869d2ba">FillRand</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the non-zero entries randomly.  <a href="#a99688ae138cf3093f657a1673869d2ba"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a3bf9cba0b13332169dd7bf67810387dd">Print</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays the matrix on the standard output.  <a href="#a3bf9cba0b13332169dd7bf67810387dd"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#afb6419e497671ea7dd09027df0d43bc7">Write</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix in a file.  <a href="#afb6419e497671ea7dd09027df0d43bc7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a6bda3b0c8892f7cb59b59ab9ec043980">Write</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix to an output stream.  <a href="#a6bda3b0c8892f7cb59b59ab9ec043980"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ab2a9ab616ba78d7a0c63f23f58a3e70f">WriteText</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix in a file.  <a href="#ab2a9ab616ba78d7a0c63f23f58a3e70f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ac2f20995f37def4157cf2b363b908f86">WriteText</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix to an output stream.  <a href="#ac2f20995f37def4157cf2b363b908f86"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a0ba7bb9f087c978b3f528803136ffad7">Read</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from a file.  <a href="#a0ba7bb9f087c978b3f528803136ffad7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a1b8c684b65f487412ba7d437adfc2195">Read</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from an input stream.  <a href="#a1b8c684b65f487412ba7d437adfc2195"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a2ae7d8f121dd314d8fab599225028432">ReadText</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from a file.  <a href="#a2ae7d8f121dd314d8fab599225028432"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#aa33b08968aee2a8a9fca8e823db4274c">ReadText</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from an input stream.  <a href="#aa33b08968aee2a8a9fca8e823db4274c"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T , class Prop , class Storage , class Allocator &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a50bafb538cfd1b8e6bb18bba62451dff">SetData</a> (int i, int j, int real_nz, typename <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::pointer real_values, int *real_ptr, int *real_ind, int imag_nz, typename <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::pointer imag_values, int *imag_ptr, int *imag_ind)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Redefines the matrix.  <a href="#a50bafb538cfd1b8e6bb18bba62451dff"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">GetM</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows.  <a href="#a65e9c3f0db9c7c8c3fa10ead777dd927"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows of the matrix possibly transposed.  <a href="#ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">GetN</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns.  <a href="#a6b134070d1b890ba6b7eb72fee170984"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns of the matrix possibly transposed.  <a href="#a7d27412bc9384a5ce0c0db1ee310d31c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">GetSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements in the matrix.  <a href="#a0fbc3f7030583174eaa69429f655a1b0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer to the data array.  <a href="#a453a269dfe7fadba249064363d5ab92a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a const pointer to the data array.  <a href="#a0f2796430deb08e565df8ced656097bf"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "void*" to the data array.  <a href="#a505cdfee34741c463e0dc1943337bedc"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "const void*" to the data array.  <a href="#a5979450cd8801810229f4a24c36e6639"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">Allocator &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the allocator of the matrix.  <a href="#af88748a55208349367d6860ad76fd691"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0c26f06038a9e56d60bcb50318280020"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::real_nz_" ref="a0c26f06038a9e56d60bcb50318280020" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>real_nz_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a48432663ef5598efa8400efa2b2d74d2"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::imag_nz_" ref="a48432663ef5598efa8400efa2b2d74d2" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>imag_nz_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac2c6047ef3a7965c253645ca7cabb854"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::real_ptr_" ref="ac2c6047ef3a7965c253645ca7cabb854" args="" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>real_ptr_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aea4d687cb74076d2830fe9436305f339"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::imag_ptr_" ref="aea4d687cb74076d2830fe9436305f339" args="" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>imag_ptr_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac206ea18c12aa8a73a01e1c202ef5cf4"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::real_ind_" ref="ac206ea18c12aa8a73a01e1c202ef5cf4" args="" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>real_ind_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a43b598929036da9689830972edbe7372"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::imag_ind_" ref="a43b598929036da9689830972edbe7372" args="" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>imag_ind_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acf7794353872c8a30f8619ad02730a31"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::real_data_" ref="acf7794353872c8a30f8619ad02730a31" args="" -->
T *&nbsp;</td><td class="memItemRight" valign="bottom"><b>real_data_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a70891d4e4b14eda27d452ce5d00079aa"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::imag_data_" ref="a70891d4e4b14eda27d452ce5d00079aa" args="" -->
T *&nbsp;</td><td class="memItemRight" valign="bottom"><b>imag_data_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a96b39ff494d4b7d3e30f320a1c7b4aba"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::m_" ref="a96b39ff494d4b7d3e30f320a1c7b4aba" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>m_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a11cb5d502050e5f14482ff0c9cef5a76"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::n_" ref="a11cb5d502050e5f14482ff0c9cef5a76" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>n_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acb12a8b74699fae7ae3b2b67376795a1"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::data_" ref="acb12a8b74699fae7ae3b2b67376795a1" args="" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-static-attribs"></a>
Static Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a132fa01ce120f352d434fd920e5ad9b4"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::allocator_" ref="a132fa01ce120f352d434fd920e5ad9b4" args="" -->
static Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>allocator_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Prop, class Storage, class Allocator = SELDON_DEFAULT_ALLOCATOR&lt;T&gt;&gt;<br/>
 class Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</h3>

<p>Complex sparse-matrix class. </p>
<p>Sparse matrices are defined by: (1) the number of rows and columns; (2) the number of non-zero entries; (3) an array 'ptr_' of start indices (i.e. indices of the first element of each row or column, depending on the storage); (4) an array 'ind_' of column or row indices of each non-zero entry; (5) values of non-zero entries.</p>
<dl class="user"><dt><b></b></dt><dd>Complex sparse matrices are defined in the same way except that real and imaginary parts are splitted. It is as if two matrices were stored. There are therefore 6 arrays: 'real_ptr_', 'real_ind_', 'real_data_', 'imag_ptr_', 'imag_ind_' and 'imag_data_'. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8hxx_source.php#l00048">48</a> of file <a class="el" href="_matrix___complex_sparse_8hxx_source.php">Matrix_ComplexSparse.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="ad680112fe370f8ce4621a70bbc5ce2d9"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::Matrix_ComplexSparse" ref="ad680112fe370f8ce4621a70bbc5ce2d9" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>Builds an empty 0x0 matrix. </p>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l00038">38</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0105ca0b54784973e1272931f9c9e420"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::Matrix_ComplexSparse" ref="a0105ca0b54784973e1272931f9c9e420" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Constructor. </p>
<p>Builds a i by j sparse matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l00059">59</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a728b0dd58af72224fc92a6ab299ad274"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::Matrix_ComplexSparse" ref="a728b0dd58af72224fc92a6ab299ad274" args="(int i, int j, int real_nz, int imag_nz)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>real_nz</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>imag_nz</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Constructor. </p>
<p>Builds a sparse matrix of size i by j , with real_nz non-zero elements in the real part of the matrix and imag_nz non-zero elements in the imaginary part of the matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_nz</em>&nbsp;</td><td>number of non-zero elements in the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_nz</em>&nbsp;</td><td>number of non-zero elements in the imaginary part. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> values are not initialized. Indices of non-zero entries are not initialized either. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l00087">87</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a079f7006a23ebdc5e63c2337f88f9bfc"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::Matrix_ComplexSparse" ref="a079f7006a23ebdc5e63c2337f88f9bfc" args="(int i, int j, Vector&lt; T, Storage0, Allocator0 &gt; &amp;real_values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;real_ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;real_ind, Vector&lt; T, Storage0, Allocator0 &gt; &amp;imag_values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;imag_ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;imag_ind)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Storage0 , class Allocator0 , class Storage1 , class Allocator1 , class Storage2 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>real_values</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>real_ptr</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>real_ind</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>imag_values</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>imag_ptr</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>imag_ind</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Constructor. </p>
<p>Builds a i by j sparse matrix with non-zero values and indices provided by 'real_values' (values of the real part), 'real_ptr' (pointers for the real part), 'real_ind' (indices for the real part), 'imag_values' (values of the imaginary part), 'imag_ptr' (pointers for the imaginary part) and 'imag_ind' (indices for the imaginary part). Input vectors are released and are empty on exit. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_values</em>&nbsp;</td><td>values of non-zero entries for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_ptr</em>&nbsp;</td><td>row or column start indices for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_ind</em>&nbsp;</td><td>row or column indices for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_values</em>&nbsp;</td><td>values of non-zero entries for the imaginary part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_ptr</em>&nbsp;</td><td>row or column start indices for the imaginary part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_ind</em>&nbsp;</td><td>row or column indices for the imaginary part. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Input vectors 'real_values', 'real_ptr' and 'real_ind', 'imag_values', 'imag_ptr' and 'imag_ind' are empty on exit. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l00127">127</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="ade592203fcc968e928f0083cd5e21644"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::AddInteraction" ref="ade592203fcc968e928f0083cd5e21644" args="(int i, int j, const complex&lt; T &gt; &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::AddInteraction </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const complex&lt; T &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Add a value to a non-zero entry. </p>
<p>This function adds <em>val</em> to the element (<em>i</em>, <em>j</em>), provided that this element is already a non-zero entry. Otherwise a non-zero entry is inserted equal to <em>val</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>value to be added to the element (<em>i</em>, <em>j</em>). </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02471">2471</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a8940cdf1faedc44051919b9774132fa9"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::Clear" ref="a8940cdf1faedc44051919b9774132fa9" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Clear </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the matrix. </p>
<p>This methods is equivalent to the destructor. On exit, the matrix is empty (0x0). </p>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l00447">447</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a4f936ebae0d6d37d4ef7e71022ad0490"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::Fill" ref="a4f936ebae0d6d37d4ef7e71022ad0490" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Fill </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the non-zero entries with 0, 1, 2, ... </p>
<p>On exit, the non-zero entries are 0, 1, 2, 3, ... The order of the numbers depends on the storage. </p>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02573">2573</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1b17bf0f1b4be60ae64a51564737f834"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::Fill" ref="a1b17bf0f1b4be60ae64a51564737f834" args="(const complex&lt; T &gt; &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Fill </td>
          <td>(</td>
          <td class="paramtype">const complex&lt; T &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the non-zero entries with a given value. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>the value to set the non-zero entries to. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02589">2589</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a99688ae138cf3093f657a1673869d2ba"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::FillRand" ref="a99688ae138cf3093f657a1673869d2ba" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::FillRand </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the non-zero entries randomly. </p>
<dl class="note"><dt><b>Note:</b></dt><dd>The random generator is very basic. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02604">2604</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af88748a55208349367d6860ad76fd691"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetAllocator" ref="af88748a55208349367d6860ad76fd691" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">Allocator &amp; <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetAllocator </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the allocator of the matrix. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The allocator. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00258">258</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a453a269dfe7fadba249064363d5ab92a"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetData" ref="a453a269dfe7fadba249064363d5ab92a" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer to the data array. </p>
<p>Returns a pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00208">208</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0f2796430deb08e565df8ced656097bf"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetDataConst" ref="a0f2796430deb08e565df8ced656097bf" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::const_pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConst </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a const pointer to the data array. </p>
<p>Returns a const pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00221">221</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5979450cd8801810229f4a24c36e6639"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetDataConstVoid" ref="a5979450cd8801810229f4a24c36e6639" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConstVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "const void*" to the data array. </p>
<p>Returns a pointer of type "const void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00247">247</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a543d8834abfbd7888c6d2516612bde7b"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetDataSize" ref="a543d8834abfbd7888c6d2516612bde7b" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetDataSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored in memory. </p>
<p>Returns the number of elements stored in memory, i.e. the cumulated number of non-zero entries of both the real and the imaginary part. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements stored in memory. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l01814">1814</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a505cdfee34741c463e0dc1943337bedc"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetDataVoid" ref="a505cdfee34741c463e0dc1943337bedc" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "void*" to the data array. </p>
<p>Returns a pointer of type "void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00234">234</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9fec8163944b4af720f89473c7675a34"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetImag" ref="a9fec8163944b4af720f89473c7675a34" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &amp; <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the imaginary part of element (<em>i</em>, <em>j</em>) if it can be returned as a reference. If the non-zero entry does not exit, it is created </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02455">2455</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a54303525013c8dc63f7064c4720b9348"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetImag" ref="a54303525013c8dc63f7064c4720b9348" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &amp; <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the imaginary part of element (<em>i</em>, <em>j</em>) if it can be returned as a reference. If the non-zero entry does not exit, it is created </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02390">2390</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5637e2dd95b0e62a7f42c0f3fd1383b1"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetImagData" ref="a5637e2dd95b0e62a7f42c0f3fd1383b1" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T * <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImagData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the array of values of the imaginary part. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The array 'imag_data_' of values of the imaginary part.. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l01991">1991</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2aa0e0d1430941ccfecb8e2947801570"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetImagDataSize" ref="a2aa0e0d1430941ccfecb8e2947801570" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImagDataSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the length of the array of (column or row) indices //! for the imaginary part. </p>
<p>Returns the length of the array ('ind_') of (row or column) indices of non-zero entries for the imaginary part. This array defines non-zero entries indices if coupled with (column or row) start indices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the array of (column or row) indices for the imaginary part. </dd></dl>
<dl class="note"><dt><b>Note:</b></dt><dd>The length of the array of (column or row) indices is the number of non-zero entries. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l01969">1969</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a08f3d91609557b1650066765343909db"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetImagInd" ref="a08f3d91609557b1650066765343909db" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int * <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImagInd </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns (row or column) indices of non-zero entries //! for the imaginary part. </p>
<p>Returns the array ('ind_') of (row or column) indices of non-zero entries for the imaginary part. This array defines non-zero entries indices if coupled with (column or row) start indices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The array of (row or column) indices of non-zero entries for the imaginary part. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l01869">1869</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad61069ad96f1bfd7b57f6a4e9fe85764"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetImagIndSize" ref="ad61069ad96f1bfd7b57f6a4e9fe85764" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImagIndSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the length of the array of (column or row) indices //! for the imaginary part. </p>
<p>Returns the length of the array ('ind_') of (row or column) indices of non-zero entries for the imaginary part. This array defines non-zero entries indices if coupled with (column or row) start indices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the array of (column or row) indices for the imaginary part. </dd></dl>
<dl class="note"><dt><b>Note:</b></dt><dd>The length of the array of (column or row) indices is the number of non-zero entries. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l01931">1931</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6766b6db59aa35b4fa7458a12e531e9b"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetImagPtr" ref="a6766b6db59aa35b4fa7458a12e531e9b" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int * <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImagPtr </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns (row or column) start indices for the imaginary part. </p>
<p>Returns the array ('ptr_') of start indices for the imaginary part. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The array of start indices for the imaginary part. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l01838">1838</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae97357d4afa167ca95d28e8897e44ef2"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetImagPtrSize" ref="ae97357d4afa167ca95d28e8897e44ef2" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImagPtrSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the length of the array of start indices for the imaginary part. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the array of start indices for the imaginary part. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l01893">1893</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a65e9c3f0db9c7c8c3fa10ead777dd927"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetM" ref="a65e9c3f0db9c7c8c3fa10ead777dd927" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a8c781e99310aae01786eac0e8e5aa50c">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00107">107</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetM" ref="ab6f6c5bba065ca82dad7c3abdbc8ea79" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#aecf3e8c636dbb83335ea588afe2558a8">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00130">130</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6b134070d1b890ba6b7eb72fee170984"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetN" ref="a6b134070d1b890ba6b7eb72fee170984" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a40580a92202044416e379c1304ff0220">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00118">118</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7d27412bc9384a5ce0c0db1ee310d31c"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetN" ref="a7d27412bc9384a5ce0c0db1ee310d31c" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a867066f7bf531bf7ad15bdcd034dc3c0">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00145">145</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a703b6043d3ad7c59cc36e9c5fd0a7d77"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetReal" ref="a703b6043d3ad7c59cc36e9c5fd0a7d77" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &amp; <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the real part of element (<em>i</em>, <em>j</em>) if it can be returned as a reference. If the non-zero entry does not exit, it is created </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02308">2308</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae8796f2d633623f1179a19a38d7f68e1"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetReal" ref="ae8796f2d633623f1179a19a38d7f68e1" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &amp; <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the real part of element (<em>i</em>, <em>j</em>) if it can be returned as a reference. If the non-zero entry does not exit, it is created </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02373">2373</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a74325c4f424c0043682b5cb3ad3df501"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetRealData" ref="a74325c4f424c0043682b5cb3ad3df501" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T * <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetRealData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the array of values of the real part. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The array 'real_data_' of values of the real part.. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l01980">1980</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a00680b1689e564a1ed51dd34c4dcd0c9"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetRealDataSize" ref="a00680b1689e564a1ed51dd34c4dcd0c9" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetRealDataSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the length of the array of (column or row) indices //! for the real part. </p>
<p>Returns the length of the array ('ind_') of (row or column) indices of non-zero entries for the real part. This array defines non-zero entries indices if coupled with (column or row) start indices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the array of (column or row) indices for the real part. </dd></dl>
<dl class="note"><dt><b>Note:</b></dt><dd>The length of the array of (column or row) indices is the number of non-zero entries. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l01950">1950</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a4eb0b0535cb51c9c0886da147f6e0cb0"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetRealInd" ref="a4eb0b0535cb51c9c0886da147f6e0cb0" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int * <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetRealInd </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns (row or column) indices of non-zero entries for the real part. </p>
<p>Returns the array ('ind_') of (row or column) indices of non-zero entries for the real part. This array defines non-zero entries indices if coupled with (column or row) start indices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The array of (row or column) indices of non-zero entries for the real part. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l01853">1853</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae573e32219c095276106254a84e7f684"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetRealIndSize" ref="ae573e32219c095276106254a84e7f684" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetRealIndSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the length of the array of (column or row) indices //! for the real part. </p>
<p>Returns the length of the array ('ind_') of (row or column) indices of non-zero entries for the real part. This array defines non-zero entries indices if coupled with (column or row) start indices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the array of (column or row) indices for the real part. </dd></dl>
<dl class="note"><dt><b>Note:</b></dt><dd>The length of the array of (column or row) indices is the number of non-zero entries. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l01912">1912</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="afa783bff3e823f8ad2480a50d4d28929"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetRealPtr" ref="afa783bff3e823f8ad2480a50d4d28929" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int * <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetRealPtr </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns (row or column) start indices for the real part. </p>
<p>Returns the array ('ptr_') of start indices for the real part. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The array of start indices for the real part. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l01826">1826</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3be4bf190b7c5957d81ddf7e7a7b7882"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetRealPtrSize" ref="a3be4bf190b7c5957d81ddf7e7a7b7882" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetRealPtrSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the length of the array of start indices for the real part. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the array of start indices for the real part. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l01881">1881</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0fbc3f7030583174eaa69429f655a1b0"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::GetSize" ref="a0fbc3f7030583174eaa69429f655a1b0" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements in the matrix. </p>
<p>Returns the number of elements in the matrix, i.e. the number of rows multiplied by the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements in the matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00195">195</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a10672a9a570060c9bd8f17eb54dc46fa"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::Nullify" ref="a10672a9a570060c9bd8f17eb54dc46fa" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Nullify </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the matrix without releasing memory. </p>
<p>On exit, the matrix is empty and the memory has not been released. It is useful for low level manipulations on a <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> instance. </p>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l00692">692</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad9e481e5ea6de45867d8952dd9676bea"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::operator()" ref="ad9e481e5ea6de45867d8952dd9676bea" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">complex&lt; typename <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &gt; <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02013">2013</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3e54cf340623d3c2c03f4926d9846383"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::operator=" ref="a3e54cf340623d3c2c03f4926d9846383" args="(const Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop, class Storage, class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a matrix (assignment operator). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>A</em>&nbsp;</td><td>matrix to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'A' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02505">2505</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3bf9cba0b13332169dd7bf67810387dd"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::Print" ref="a3bf9cba0b13332169dd7bf67810387dd" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Print </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Displays the matrix on the standard output. </p>
<p>Displays elements on the standard output, in text format. Each row is displayed on a single line and elements of a row are delimited by tabulations. </p>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02622">2622</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1b8c684b65f487412ba7d437adfc2195"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::Read" ref="a1b8c684b65f487412ba7d437adfc2195" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from an input stream. </p>
<p>Reads a matrix in binary format from an input stream. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>input stream </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02791">2791</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0ba7bb9f087c978b3f528803136ffad7"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::Read" ref="a0ba7bb9f087c978b3f528803136ffad7" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from a file. </p>
<p>Reads a matrix stored in binary format in a file. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>input file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02766">2766</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa33b08968aee2a8a9fca8e823db4274c"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::ReadText" ref="aa33b08968aee2a8a9fca8e823db4274c" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from an input stream. </p>
<p>Reads a matrix from a stream in text format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>input stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02863">2863</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2ae7d8f121dd314d8fab599225028432"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::ReadText" ref="a2ae7d8f121dd314d8fab599225028432" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from a file. </p>
<p>Reads the matrix from a file in text format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>input file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02838">2838</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a17e9f9857a46c900b33ab803ca584c2f"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::Resize" ref="a17e9f9857a46c900b33ab803ca584c2f" args="(int i, int j, int real_nz, int imag_nz)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Resize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>real_nz</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>imag_nz</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Changing the number of rows, columns and non-zero entries. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns Previous entries are kept during the operation </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l01155">1155</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa53b17b254435486a52c058490abb0fb"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::Resize" ref="aa53b17b254435486a52c058490abb0fb" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Resize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Changing the number of rows and columns. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l01137">1137</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad297118a9c4023d96a871e6b95d78aa6"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::Set" ref="ad297118a9c4023d96a871e6b95d78aa6" args="(int i, int j, const complex&lt; T &gt; &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Set </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const complex&lt; T &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets an element (i, j) to a value. </p>
<p>This function sets <em>val</em> to the element (<em>i</em>, <em>j</em>) </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>A(i, j) = val </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02489">2489</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3729058de11ead808a09e98a10702d85"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::SetData" ref="a3729058de11ead808a09e98a10702d85" args="(int i, int j, Vector&lt; T, Storage0, Allocator0 &gt; &amp;real_values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;real_ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;real_ind, Vector&lt; T, Storage0, Allocator0 &gt; &amp;imag_values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;imag_ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;imag_ind)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Storage0 , class Allocator0 , class Storage1 , class Allocator1 , class Storage2 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::SetData </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>real_values</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>real_ptr</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>real_ind</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>imag_values</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>imag_ptr</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>imag_ind</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Redefines the matrix. </p>
<p>It clears the matrix and sets it to a new matrix defined by 'real_values' (values of the real part), 'real_ptr' (pointers for the real part), 'real_ind' (indices for the real part), 'imag_values' (values of the imaginary part), 'imag_ptr' (pointers for the imaginary part) and 'imag_ind' (indices for the imaginary part). Input vectors are released and are empty on exit. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_values</em>&nbsp;</td><td>values of non-zero entries for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_ptr</em>&nbsp;</td><td>row or column start indices for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_ind</em>&nbsp;</td><td>row or column indices for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_values</em>&nbsp;</td><td>values of non-zero entries for the imaginary part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_ptr</em>&nbsp;</td><td>row or column start indices for the imaginary part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_ind</em>&nbsp;</td><td>row or column indices for the imaginary part. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Input vectors 'real_values', 'real_ptr' and 'real_ind', 'imag_values', 'imag_ptr' and 'imag_ind' are empty on exit. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l00481">481</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a50bafb538cfd1b8e6bb18bba62451dff"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::SetData" ref="a50bafb538cfd1b8e6bb18bba62451dff" args="(int i, int j, int real_nz, typename Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;::pointer real_values, int *real_ptr, int *real_ind, int imag_nz, typename Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;::pointer imag_values, int *imag_ptr, int *imag_ind)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop, class Storage, class Allocator = SELDON_DEFAULT_ALLOCATOR&lt;T&gt;&gt; </div>
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::SetData </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>real_nz</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">typename <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::pointer&nbsp;</td>
          <td class="paramname"> <em>real_values</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int *&nbsp;</td>
          <td class="paramname"> <em>real_ptr</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int *&nbsp;</td>
          <td class="paramname"> <em>real_ind</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>imag_nz</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">typename <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::pointer&nbsp;</td>
          <td class="paramname"> <em>imag_values</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int *&nbsp;</td>
          <td class="paramname"> <em>imag_ptr</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int *&nbsp;</td>
          <td class="paramname"> <em>imag_ind</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Redefines the matrix. </p>
<p>It clears the matrix and sets it to a new matrix defined by arrays 'real_values' (values of the real part), 'real_ptr' (pointers for the real part), 'real_ind' (indices for the real part), 'imag_values' (values of the imaginary part), 'imag_ptr' (pointers for the imaginary part) and 'imag_ind' (indices for the imaginary part). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_nz</em>&nbsp;</td><td>number of non-zero entries (real part). </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_values</em>&nbsp;</td><td>values of non-zero entries for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_ptr</em>&nbsp;</td><td>row or column start indices for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_ind</em>&nbsp;</td><td>row or column indices for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_nz</em>&nbsp;</td><td>number of non-zero entries (imaginary part). </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_values</em>&nbsp;</td><td>values of non-zero entries for the imaginary part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_ptr</em>&nbsp;</td><td>row or column start indices for the imaginary part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_ind</em>&nbsp;</td><td>row or column indices for the imaginary part. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>On exit, arrays 'real_values', 'real_ptr', 'real_ind', 'imag_values', 'imag_ptr' and 'imag_ind' are managed by the matrix. For example, it means that the destructor will release those arrays; therefore, the user mustn't release those arrays. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l00661">661</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7ca605152313a38a14014b92fb1947a5"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::SetIdentity" ref="a7ca605152313a38a14014b92fb1947a5" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::SetIdentity </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the matrix to identity. </p>
<p>This method fills the diagonal of the matrix with ones. It can be applied to non square matrix. </p>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02536">2536</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7b8f83d193758434bcb1cfe959518f91"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::ValImag" ref="a7b8f83d193758434bcb1cfe959518f91" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &amp; <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ValImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the imaginary part of element (<em>i</em>, <em>j</em>) if it can be returned as a reference. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. </dd></dl>
<dl><dt><b>Exceptions:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em><a class="el" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a></em>&nbsp;</td><td>No reference can be returned because the element is a zero entry (not stored in the matrix). </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02257">2257</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9d49fb0a1544b3eda52ee5e002d0822d"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::ValImag" ref="a9d49fb0a1544b3eda52ee5e002d0822d" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &amp; <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ValImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the imaginary part of element (<em>i</em>, <em>j</em>) if it can be returned as a reference. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. </dd></dl>
<dl><dt><b>Exceptions:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em><a class="el" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a></em>&nbsp;</td><td>No reference can be returned because the element is a zero entry (not stored in the matrix). </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02205">2205</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a928e2aa74c9ad336e77ab76593b3c8da"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::ValReal" ref="a928e2aa74c9ad336e77ab76593b3c8da" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &amp; <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ValReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the real value of element (<em>i</em>, <em>j</em>) if it can be returned as a reference. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. </dd></dl>
<dl><dt><b>Exceptions:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em><a class="el" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a></em>&nbsp;</td><td>No reference can be returned because the element is a zero entry (not stored in the matrix). </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02101">2101</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3a5cde7a78f4fea30cf13090eba0892f"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::ValReal" ref="a3a5cde7a78f4fea30cf13090eba0892f" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &amp; <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ValReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the real value of element (<em>i</em>, <em>j</em>) if it can be returned as a reference. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. </dd></dl>
<dl><dt><b>Exceptions:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em><a class="el" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a></em>&nbsp;</td><td>No reference can be returned because the element is a zero entry (not stored in the matrix). </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02153">2153</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="afb6419e497671ea7dd09027df0d43bc7"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::Write" ref="afb6419e497671ea7dd09027df0d43bc7" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix in a file. </p>
<p>Stores the matrix in a file in binary format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02640">2640</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6bda3b0c8892f7cb59b59ab9ec043980"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::Write" ref="a6bda3b0c8892f7cb59b59ab9ec043980" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix to an output stream. </p>
<p>Stores the matrix in an output stream in binary format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02665">2665</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab2a9ab616ba78d7a0c63f23f58a3e70f"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::WriteText" ref="ab2a9ab616ba78d7a0c63f23f58a3e70f" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix in a file. </p>
<p>Stores the matrix in a file in ascii format. The entries are written in coordinate format (row column value) 1-index convention is used </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02708">2708</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac2f20995f37def4157cf2b363b908f86"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::WriteText" ref="ac2f20995f37def4157cf2b363b908f86" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix to an output stream. </p>
<p>Stores the matrix in a file in ascii format. The entries are written in coordinate format (row column value) 1-index convention is used </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02735">2735</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="acdb7d1d285d2601b1ef970aecc861178"></a><!-- doxytag: member="Seldon::Matrix_ComplexSparse::Zero" ref="acdb7d1d285d2601b1ef970aecc861178" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Zero </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Resets all non-zero entries to 0-value. </p>
<p>The sparsity pattern remains unchanged. </p>

<p>Definition at line <a class="el" href="_matrix___complex_sparse_8cxx_source.php#l02521">2521</a> of file <a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>matrix_sparse/<a class="el" href="_matrix___complex_sparse_8hxx_source.php">Matrix_ComplexSparse.hxx</a></li>
<li>matrix_sparse/<a class="el" href="_matrix___complex_sparse_8cxx_source.php">Matrix_ComplexSparse.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
