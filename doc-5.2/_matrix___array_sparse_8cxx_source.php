<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix_sparse/Matrix_ArraySparse.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2011 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_ARRAY_SPARSE_CXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 <span class="preprocessor">#include &quot;Matrix_ArraySparse.hxx&quot;</span>
<a name="l00023"></a>00023 
<a name="l00024"></a>00024 <span class="keyword">namespace </span>Seldon
<a name="l00025"></a>00025 {
<a name="l00026"></a>00026 
<a name="l00028"></a>00028 
<a name="l00031"></a>00031   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00032"></a>00032   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a6d460ce1bd8388adce95c8ad99b24f1d" title="Default constructor.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Matrix_ArraySparse</a>()
<a name="l00033"></a>00033     : val_()
<a name="l00034"></a>00034   {
<a name="l00035"></a>00035     this-&gt;m_ = 0;
<a name="l00036"></a>00036     this-&gt;n_ = 0;
<a name="l00037"></a>00037   }
<a name="l00038"></a>00038 
<a name="l00039"></a>00039 
<a name="l00041"></a>00041 
<a name="l00046"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ad3363505ad6df902486f4496ac0e7ea7">00046</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00047"></a>00047   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a6d460ce1bd8388adce95c8ad99b24f1d" title="Default constructor.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00048"></a>00048 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a6d460ce1bd8388adce95c8ad99b24f1d" title="Default constructor.">  Matrix_ArraySparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) :
<a name="l00049"></a>00049     val_(Storage::GetFirst(i, j))
<a name="l00050"></a>00050   {
<a name="l00051"></a>00051     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a> = i;
<a name="l00052"></a>00052     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a> = j;
<a name="l00053"></a>00053   }
<a name="l00054"></a>00054 
<a name="l00055"></a>00055 
<a name="l00057"></a>00057   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocat&gt;
<a name="l00058"></a>00058   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#adf298ef8550ee9681a384a539802736f" title="Destructor.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocat&gt;::~Matrix_ArraySparse</a>()
<a name="l00059"></a>00059   {
<a name="l00060"></a>00060     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a> = 0;
<a name="l00061"></a>00061     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a> = 0;
<a name="l00062"></a>00062   }
<a name="l00063"></a>00063 
<a name="l00064"></a>00064 
<a name="l00066"></a>00066 
<a name="l00069"></a>00069   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00070"></a>00070   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a61227beea7612dd22bf1f46546bb5cde" title="Clears the matrix.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Clear</a>()
<a name="l00071"></a>00071   {
<a name="l00072"></a>00072     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#adf298ef8550ee9681a384a539802736f" title="Destructor.">~Matrix_ArraySparse</a>();
<a name="l00073"></a>00073   }
<a name="l00074"></a>00074 
<a name="l00075"></a>00075 
<a name="l00076"></a>00076   <span class="comment">/*********************</span>
<a name="l00077"></a>00077 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00078"></a>00078 <span class="comment">   *********************/</span>
<a name="l00079"></a>00079 
<a name="l00080"></a>00080 
<a name="l00082"></a>00082 
<a name="l00088"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#afcd0d4612b325249c24407627b3dd568">00088</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00089"></a>00089   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#afcd0d4612b325249c24407627b3dd568" title="Reallocates memory to resize the matrix.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00090"></a>00090 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#afcd0d4612b325249c24407627b3dd568" title="Reallocates memory to resize the matrix.">  Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00091"></a>00091   {
<a name="l00092"></a>00092     <span class="comment">// Clears previous entries.</span>
<a name="l00093"></a>00093     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a61227beea7612dd22bf1f46546bb5cde" title="Clears the matrix.">Clear</a>();
<a name="l00094"></a>00094 
<a name="l00095"></a>00095     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a> = i;
<a name="l00096"></a>00096     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a> = j;
<a name="l00097"></a>00097 
<a name="l00098"></a>00098     <span class="keywordtype">int</span> n = Storage::GetFirst(i, j);
<a name="l00099"></a>00099     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.Reallocate(n);
<a name="l00100"></a>00100   }
<a name="l00101"></a>00101 
<a name="l00102"></a>00102 
<a name="l00104"></a>00104 
<a name="l00110"></a>00110   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00111"></a>00111   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a2c6f9586424529c8b06b6110c5a01efa" title="Reallocates additional memory to resize the matrix.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00112"></a>00112   {
<a name="l00113"></a>00113     <span class="keywordtype">int</span> n = Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>, <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>);
<a name="l00114"></a>00114     <span class="keywordtype">int</span> new_n = Storage::GetFirst(i, j);
<a name="l00115"></a>00115     <span class="keywordflow">if</span> (n != new_n)
<a name="l00116"></a>00116       {
<a name="l00117"></a>00117         <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;T, VectSparse, Allocator&gt;</a>, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>,
<a name="l00118"></a>00118           <a class="code" href="class_seldon_1_1_new_alloc.php">NewAlloc&lt;Vector&lt;T, VectSparse, Allocator&gt;</a> &gt; &gt; new_val;
<a name="l00119"></a>00119 
<a name="l00120"></a>00120         new_val.Reallocate(new_n);
<a name="l00121"></a>00121 
<a name="l00122"></a>00122         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0 ; k &lt; min(n, new_n) ; k++)
<a name="l00123"></a>00123           Swap(new_val(k), this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(k));
<a name="l00124"></a>00124 
<a name="l00125"></a>00125         <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.SetData(new_n, new_val.GetData());
<a name="l00126"></a>00126         new_val.Nullify();
<a name="l00127"></a>00127 
<a name="l00128"></a>00128       }
<a name="l00129"></a>00129 
<a name="l00130"></a>00130     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a> = i;
<a name="l00131"></a>00131     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a> = j;
<a name="l00132"></a>00132   }
<a name="l00133"></a>00133 
<a name="l00134"></a>00134 
<a name="l00135"></a>00135   <span class="comment">/*******************</span>
<a name="l00136"></a>00136 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00137"></a>00137 <span class="comment">   *******************/</span>
<a name="l00138"></a>00138 
<a name="l00139"></a>00139 
<a name="l00141"></a>00141 
<a name="l00144"></a>00144   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00145"></a>00145   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::GetM</a>()<span class="keyword"> const</span>
<a name="l00146"></a>00146 <span class="keyword">  </span>{
<a name="l00147"></a>00147     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>;
<a name="l00148"></a>00148   }
<a name="l00149"></a>00149 
<a name="l00150"></a>00150 
<a name="l00152"></a>00152 
<a name="l00155"></a>00155   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00156"></a>00156   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3ae65d56c548233051a15054643f6753" title="Returns the number of columns.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::GetN</a>()<span class="keyword"> const</span>
<a name="l00157"></a>00157 <span class="keyword">  </span>{
<a name="l00158"></a>00158     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>;
<a name="l00159"></a>00159   }
<a name="l00160"></a>00160 
<a name="l00161"></a>00161 
<a name="l00163"></a>00163 
<a name="l00167"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#aeb55b9de8946ee89fb40c18612f9f700">00167</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00168"></a>00168   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00169"></a>00169 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">  ::GetM</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; status)<span class="keyword"> const</span>
<a name="l00170"></a>00170 <span class="keyword">  </span>{
<a name="l00171"></a>00171     <span class="keywordflow">if</span> (status.NoTrans())
<a name="l00172"></a>00172       <span class="keywordflow">return</span> m_;
<a name="l00173"></a>00173     <span class="keywordflow">else</span>
<a name="l00174"></a>00174       <span class="keywordflow">return</span> n_;
<a name="l00175"></a>00175   }
<a name="l00176"></a>00176 
<a name="l00177"></a>00177 
<a name="l00179"></a>00179 
<a name="l00183"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a537eebc1c692724958852a83df609036">00183</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00184"></a>00184   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3ae65d56c548233051a15054643f6753" title="Returns the number of columns.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00185"></a>00185 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3ae65d56c548233051a15054643f6753" title="Returns the number of columns.">  ::GetN</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; status)<span class="keyword"> const</span>
<a name="l00186"></a>00186 <span class="keyword">  </span>{
<a name="l00187"></a>00187     <span class="keywordflow">if</span> (status.NoTrans())
<a name="l00188"></a>00188       <span class="keywordflow">return</span> n_;
<a name="l00189"></a>00189     <span class="keywordflow">else</span>
<a name="l00190"></a>00190       <span class="keywordflow">return</span> m_;
<a name="l00191"></a>00191   }
<a name="l00192"></a>00192 
<a name="l00193"></a>00193 
<a name="l00195"></a>00195 
<a name="l00198"></a>00198   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00199"></a>00199   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ac58573fd27b4f0345ae196b1a6c69d0d" title="Returns the number of non-zero entries.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::GetNonZeros</a>()<span class="keyword"></span>
<a name="l00200"></a>00200 <span class="keyword">    const</span>
<a name="l00201"></a>00201 <span class="keyword">  </span>{
<a name="l00202"></a>00202     <span class="keywordtype">int</span> nnz = 0;
<a name="l00203"></a>00203     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.GetM(); i++)
<a name="l00204"></a>00204       nnz += this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).GetM();
<a name="l00205"></a>00205 
<a name="l00206"></a>00206     <span class="keywordflow">return</span> nnz;
<a name="l00207"></a>00207   }
<a name="l00208"></a>00208 
<a name="l00209"></a>00209 
<a name="l00211"></a>00211 
<a name="l00216"></a>00216   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00217"></a>00217   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a01787b806fc996c2ff3c1dcf9b2d54de" title="Returns the number of elements stored in memory.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::GetDataSize</a>()<span class="keyword"></span>
<a name="l00218"></a>00218 <span class="keyword">    const</span>
<a name="l00219"></a>00219 <span class="keyword">  </span>{
<a name="l00220"></a>00220     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ac58573fd27b4f0345ae196b1a6c69d0d" title="Returns the number of non-zero entries.">GetNonZeros</a>();
<a name="l00221"></a>00221   }
<a name="l00222"></a>00222 
<a name="l00223"></a>00223 
<a name="l00225"></a>00225 
<a name="l00230"></a>00230   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00231"></a>00231   <span class="keyword">inline</span> <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ac3d6a23829eb932f41de04c13de22f6b" title="Returns (row or column) indices of non-zero entries in row.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::GetIndex</a>(<span class="keywordtype">int</span> i)<span class="keyword"></span>
<a name="l00232"></a>00232 <span class="keyword">    const</span>
<a name="l00233"></a>00233 <span class="keyword">  </span>{
<a name="l00234"></a>00234     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).GetIndex();
<a name="l00235"></a>00235   }
<a name="l00236"></a>00236 
<a name="l00237"></a>00237 
<a name="l00239"></a>00239 
<a name="l00243"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0c18c6428cc11d175b58205727a4c67d">00243</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00244"></a>00244   <span class="keyword">inline</span> T*
<a name="l00245"></a>00245   <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a550784008495b254998193d94f75bff9" title="Returns values of non-zero entries.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::GetData</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00246"></a>00246 <span class="keyword">  </span>{
<a name="l00247"></a>00247     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).GetData();
<a name="l00248"></a>00248   }
<a name="l00249"></a>00249 
<a name="l00250"></a>00250 
<a name="l00252"></a>00252 
<a name="l00256"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a550784008495b254998193d94f75bff9">00256</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocat&gt;
<a name="l00257"></a>00257   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocat&gt;</a>*
<a name="l00258"></a>00258   <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a550784008495b254998193d94f75bff9" title="Returns values of non-zero entries.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocat&gt;::GetData</a>()<span class="keyword"> const</span>
<a name="l00259"></a>00259 <span class="keyword">  </span>{
<a name="l00260"></a>00260     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.GetData();
<a name="l00261"></a>00261   }
<a name="l00262"></a>00262 
<a name="l00263"></a>00263 
<a name="l00264"></a>00264   <span class="comment">/**********************************</span>
<a name="l00265"></a>00265 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l00266"></a>00266 <span class="comment">   **********************************/</span>
<a name="l00267"></a>00267 
<a name="l00268"></a>00268 
<a name="l00270"></a>00270 
<a name="l00276"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a41e1ef18aa87fce8062d36be5e634dfd">00276</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00277"></a>00277   <span class="keyword">inline</span> T
<a name="l00278"></a>00278   <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a41e1ef18aa87fce8062d36be5e634dfd" title="Access operator.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"></span>
<a name="l00279"></a>00279 <span class="keyword">    const</span>
<a name="l00280"></a>00280 <span class="keyword">  </span>{
<a name="l00281"></a>00281 
<a name="l00282"></a>00282 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00283"></a>00283 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>)
<a name="l00284"></a>00284       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l00285"></a>00285                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>-1) +
<a name="l00286"></a>00286                      <span class="stringliteral">&quot;],but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00287"></a>00287 
<a name="l00288"></a>00288     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>)
<a name="l00289"></a>00289       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l00290"></a>00290                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>-1) +
<a name="l00291"></a>00291                      <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00292"></a>00292 <span class="preprocessor">#endif</span>
<a name="l00293"></a>00293 <span class="preprocessor"></span>
<a name="l00294"></a>00294     <span class="keywordflow">return</span> this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(Storage::GetFirst(i, j))(Storage::GetSecond(i, j));
<a name="l00295"></a>00295   }
<a name="l00296"></a>00296 
<a name="l00297"></a>00297 
<a name="l00299"></a>00299 
<a name="l00305"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a93c303a4312317ac5f90c125ef3c5a97">00305</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00306"></a>00306   <span class="keyword">inline</span> T&amp;
<a name="l00307"></a>00307   <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a93c303a4312317ac5f90c125ef3c5a97" title="Access operator.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00308"></a>00308   {
<a name="l00309"></a>00309 
<a name="l00310"></a>00310 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00311"></a>00311 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>)
<a name="l00312"></a>00312       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l00313"></a>00313                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>-1) +
<a name="l00314"></a>00314                      <span class="stringliteral">&quot;],but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00315"></a>00315 
<a name="l00316"></a>00316     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>)
<a name="l00317"></a>00317       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l00318"></a>00318                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>-1) +
<a name="l00319"></a>00319                      <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00320"></a>00320 <span class="preprocessor">#endif</span>
<a name="l00321"></a>00321 <span class="preprocessor"></span>
<a name="l00322"></a>00322     <span class="keywordflow">return</span> this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(Storage::GetFirst(i, j)).Get(Storage::GetSecond(i, j));
<a name="l00323"></a>00323   }
<a name="l00324"></a>00324 
<a name="l00325"></a>00325 
<a name="l00327"></a>00327 
<a name="l00333"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ae918a08d4cf98cfbeb63000724321a43">00333</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00334"></a>00334   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp;
<a name="l00335"></a>00335   <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a93c303a4312317ac5f90c125ef3c5a97" title="Access operator.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00336"></a>00336 <span class="keyword">  </span>{
<a name="l00337"></a>00337 
<a name="l00338"></a>00338 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00339"></a>00339 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>)
<a name="l00340"></a>00340       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l00341"></a>00341                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>-1) +
<a name="l00342"></a>00342                      <span class="stringliteral">&quot;],but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00343"></a>00343 
<a name="l00344"></a>00344     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>)
<a name="l00345"></a>00345       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l00346"></a>00346                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>-1) +
<a name="l00347"></a>00347                      <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00348"></a>00348 <span class="preprocessor">#endif</span>
<a name="l00349"></a>00349 <span class="preprocessor"></span>
<a name="l00350"></a>00350     <span class="keywordflow">return</span> this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(Storage::GetFirst(i, j)).Get(Storage::GetSecond(i, j));
<a name="l00351"></a>00351   }
<a name="l00352"></a>00352 
<a name="l00353"></a>00353 
<a name="l00355"></a>00355 
<a name="l00361"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ad114c8c6e002a7a0def70f2413a32659">00361</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00362"></a>00362   <span class="keyword">inline</span> T&amp;
<a name="l00363"></a>00363   <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ad114c8c6e002a7a0def70f2413a32659" title="Access operator.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00364"></a>00364   {
<a name="l00365"></a>00365 
<a name="l00366"></a>00366 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00367"></a>00367 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>)
<a name="l00368"></a>00368       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l00369"></a>00369                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>-1) +
<a name="l00370"></a>00370                      <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00371"></a>00371 
<a name="l00372"></a>00372     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>)
<a name="l00373"></a>00373       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l00374"></a>00374                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>-1) +
<a name="l00375"></a>00375                      <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00376"></a>00376 <span class="preprocessor">#endif</span>
<a name="l00377"></a>00377 <span class="preprocessor"></span>
<a name="l00378"></a>00378     <span class="keywordflow">return</span>
<a name="l00379"></a>00379       this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(Storage::GetFirst(i, j)).Val(Storage::GetSecond(i, j));
<a name="l00380"></a>00380   }
<a name="l00381"></a>00381 
<a name="l00382"></a>00382 
<a name="l00384"></a>00384 
<a name="l00390"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a130c7307de87672e3878362386481775">00390</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00391"></a>00391   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp;
<a name="l00392"></a>00392   <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ad114c8c6e002a7a0def70f2413a32659" title="Access operator.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00393"></a>00393 <span class="keyword">  </span>{
<a name="l00394"></a>00394 
<a name="l00395"></a>00395 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00396"></a>00396 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>)
<a name="l00397"></a>00397       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l00398"></a>00398                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>-1) +
<a name="l00399"></a>00399                      <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00400"></a>00400 
<a name="l00401"></a>00401     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>)
<a name="l00402"></a>00402       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l00403"></a>00403                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>-1) +
<a name="l00404"></a>00404                      <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00405"></a>00405 <span class="preprocessor">#endif</span>
<a name="l00406"></a>00406 <span class="preprocessor"></span>
<a name="l00407"></a>00407     <span class="keywordflow">return</span>
<a name="l00408"></a>00408       this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(Storage::GetFirst(i, j)).Val(Storage::GetSecond(i, j));
<a name="l00409"></a>00409   }
<a name="l00410"></a>00410 
<a name="l00411"></a>00411 
<a name="l00413"></a>00413 
<a name="l00418"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a7efa3a40752d1506b63504aaf65775b2">00418</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00419"></a>00419   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a7efa3a40752d1506b63504aaf65775b2" title="Sets an element of the matrix.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00420"></a>00420 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a7efa3a40752d1506b63504aaf65775b2" title="Sets an element of the matrix.">  ::Set</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; x)
<a name="l00421"></a>00421   {
<a name="l00422"></a>00422     this-&gt;Get(i, j) = x;
<a name="l00423"></a>00423   }
<a name="l00424"></a>00424 
<a name="l00425"></a>00425 
<a name="l00427"></a>00427 
<a name="l00432"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6">00432</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00433"></a>00433   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00434"></a>00434 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">  Value</a> (<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00435"></a>00435 <span class="keyword">  </span>{
<a name="l00436"></a>00436 
<a name="l00437"></a>00437 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00438"></a>00438 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>))
<a name="l00439"></a>00439       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00440"></a>00440                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>)-1)
<a name="l00441"></a>00441                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00442"></a>00442 
<a name="l00443"></a>00443     <span class="keywordflow">if</span> ((j &lt; 0)||(j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>()))
<a name="l00444"></a>00444       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00445"></a>00445                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00446"></a>00446                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00447"></a>00447 <span class="preprocessor">#endif</span>
<a name="l00448"></a>00448 <span class="preprocessor"></span>
<a name="l00449"></a>00449     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Value(j);
<a name="l00450"></a>00450   }
<a name="l00451"></a>00451 
<a name="l00452"></a>00452 
<a name="l00454"></a>00454 
<a name="l00459"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#afce6920acaa6e6ca7ce340c2db8991ea">00459</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00460"></a>00460   <span class="keyword">inline</span> T&amp;
<a name="l00461"></a>00461   <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Value</a> (<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00462"></a>00462   {
<a name="l00463"></a>00463 
<a name="l00464"></a>00464 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00465"></a>00465 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>))
<a name="l00466"></a>00466       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00467"></a>00467                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>)-1)
<a name="l00468"></a>00468                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00469"></a>00469 
<a name="l00470"></a>00470     <span class="keywordflow">if</span> ((j &lt; 0)||(j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>()))
<a name="l00471"></a>00471       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00472"></a>00472                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00473"></a>00473                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00474"></a>00474 <span class="preprocessor">#endif</span>
<a name="l00475"></a>00475 <span class="preprocessor"></span>
<a name="l00476"></a>00476     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Value(j);
<a name="l00477"></a>00477   }
<a name="l00478"></a>00478 
<a name="l00479"></a>00479 
<a name="l00481"></a>00481 
<a name="l00486"></a>00486   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l00487"></a>00487   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3534fb2c57f2640c9b8cd5a59e9da0e2" title="Returns column/row number of j-th non-zero value of row/column i.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Index</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"></span>
<a name="l00488"></a>00488 <span class="keyword">    const</span>
<a name="l00489"></a>00489 <span class="keyword">  </span>{
<a name="l00490"></a>00490 
<a name="l00491"></a>00491 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00492"></a>00492 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>))
<a name="l00493"></a>00493       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00494"></a>00494                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>)-1)
<a name="l00495"></a>00495                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00496"></a>00496 
<a name="l00497"></a>00497     <span class="keywordflow">if</span> ((j &lt; 0)||(j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>()))
<a name="l00498"></a>00498       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00499"></a>00499                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00500"></a>00500                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00501"></a>00501 <span class="preprocessor">#endif</span>
<a name="l00502"></a>00502 <span class="preprocessor"></span>
<a name="l00503"></a>00503     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Index(j);
<a name="l00504"></a>00504   }
<a name="l00505"></a>00505 
<a name="l00506"></a>00506 
<a name="l00508"></a>00508 
<a name="l00513"></a>00513   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l00514"></a>00514   <span class="keywordtype">int</span>&amp; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3534fb2c57f2640c9b8cd5a59e9da0e2" title="Returns column/row number of j-th non-zero value of row/column i.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Index</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00515"></a>00515   {
<a name="l00516"></a>00516 
<a name="l00517"></a>00517 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00518"></a>00518 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>))
<a name="l00519"></a>00519       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00520"></a>00520                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>)-1)
<a name="l00521"></a>00521                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00522"></a>00522 
<a name="l00523"></a>00523     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>())
<a name="l00524"></a>00524       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00525"></a>00525                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00526"></a>00526                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00527"></a>00527 <span class="preprocessor">#endif</span>
<a name="l00528"></a>00528 <span class="preprocessor"></span>
<a name="l00529"></a>00529     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Index(j);
<a name="l00530"></a>00530   }
<a name="l00531"></a>00531 
<a name="l00532"></a>00532 
<a name="l00534"></a>00534 
<a name="l00540"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a6e429aff71786b06033b047e6c277fab">00540</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00541"></a>00541   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a9fb98c93d5bee7a4be62e1a3e1bb5a46" title="Redefines the matrix.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00542"></a>00542 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a9fb98c93d5bee7a4be62e1a3e1bb5a46" title="Redefines the matrix.">  SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> n, T* val, <span class="keywordtype">int</span>* ind)
<a name="l00543"></a>00543   {
<a name="l00544"></a>00544     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).SetData(n, val, ind);
<a name="l00545"></a>00545   }
<a name="l00546"></a>00546 
<a name="l00547"></a>00547 
<a name="l00549"></a>00549 
<a name="l00553"></a>00553   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00554"></a>00554   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0cceba81597165b0e23ec0c20daed462" title="Clears the matrix without releasing memory.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Nullify</a>(<span class="keywordtype">int</span> i)
<a name="l00555"></a>00555   {
<a name="l00556"></a>00556     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Nullify();
<a name="l00557"></a>00557   }
<a name="l00558"></a>00558 
<a name="l00559"></a>00559 
<a name="l00561"></a>00561 
<a name="l00566"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a9fb98c93d5bee7a4be62e1a3e1bb5a46">00566</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00567"></a>00567   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a9fb98c93d5bee7a4be62e1a3e1bb5a46" title="Redefines the matrix.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00568"></a>00568 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a9fb98c93d5bee7a4be62e1a3e1bb5a46" title="Redefines the matrix.">  SetData</a>(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n, <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php" title="Sparse vector class.">Vector&lt;T, VectSparse, Allocator&gt;</a>* val)
<a name="l00569"></a>00569   {
<a name="l00570"></a>00570     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a> = m;
<a name="l00571"></a>00571     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a> = n;
<a name="l00572"></a>00572     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.SetData(Storage::GetFirst(m, n), val);
<a name="l00573"></a>00573   }
<a name="l00574"></a>00574 
<a name="l00575"></a>00575 
<a name="l00577"></a>00577 
<a name="l00581"></a>00581   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00582"></a>00582   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0cceba81597165b0e23ec0c20daed462" title="Clears the matrix without releasing memory.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Nullify</a>()
<a name="l00583"></a>00583   {
<a name="l00584"></a>00584     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a> = 0;
<a name="l00585"></a>00585     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a> = 0;
<a name="l00586"></a>00586     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.Nullify();
<a name="l00587"></a>00587   }
<a name="l00588"></a>00588 
<a name="l00589"></a>00589 
<a name="l00590"></a>00590   <span class="comment">/************************</span>
<a name="l00591"></a>00591 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l00592"></a>00592 <span class="comment">   ************************/</span>
<a name="l00593"></a>00593 
<a name="l00594"></a>00594 
<a name="l00596"></a>00596 
<a name="l00601"></a>00601   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00602"></a>00602   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a61ff8d09ea73a05eff4fb21e16fb1118" title="Displays the matrix on the standard output.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00603"></a>00603 <span class="keyword">  </span>{
<a name="l00604"></a>00604     <span class="keywordflow">if</span> (Storage::GetFirst(1, 0) == 1)
<a name="l00605"></a>00605       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>; i++)
<a name="l00606"></a>00606         {
<a name="l00607"></a>00607           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).GetM(); j++)
<a name="l00608"></a>00608             cout &lt;&lt; (i+1) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Index(j)+1
<a name="l00609"></a>00609                  &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Value(j) &lt;&lt; endl;
<a name="l00610"></a>00610         }
<a name="l00611"></a>00611     <span class="keywordflow">else</span>
<a name="l00612"></a>00612       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>; i++)
<a name="l00613"></a>00613         {
<a name="l00614"></a>00614           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).GetM(); j++)
<a name="l00615"></a>00615             cout &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Index(j)+1 &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; i+1
<a name="l00616"></a>00616                  &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Value(j) &lt;&lt; endl;
<a name="l00617"></a>00617         }
<a name="l00618"></a>00618   }
<a name="l00619"></a>00619 
<a name="l00620"></a>00620 
<a name="l00622"></a>00622 
<a name="l00628"></a>00628   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00629"></a>00629   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ac469920c36d5a57516a3e53c0c4e3126" title="Assembles the matrix.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Assemble</a>()
<a name="l00630"></a>00630   {
<a name="l00631"></a>00631     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.GetM(); i++)
<a name="l00632"></a>00632       <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Assemble();
<a name="l00633"></a>00633   }
<a name="l00634"></a>00634 
<a name="l00635"></a>00635 
<a name="l00637"></a>00637 
<a name="l00640"></a>00640   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00641"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a4b87eb77f9690ec605e9a1e7e1d155c5">00641</a>   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l00642"></a>00642   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a4b87eb77f9690ec605e9a1e7e1d155c5" title="Removes small coefficients from entries.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00643"></a>00643 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a4b87eb77f9690ec605e9a1e7e1d155c5" title="Removes small coefficients from entries.">  RemoveSmallEntry</a>(<span class="keyword">const</span> T0&amp; epsilon)
<a name="l00644"></a>00644   {
<a name="l00645"></a>00645     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.GetM(); i++)
<a name="l00646"></a>00646       <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).RemoveSmallEntry(epsilon);
<a name="l00647"></a>00647   }
<a name="l00648"></a>00648 
<a name="l00649"></a>00649 
<a name="l00651"></a>00651   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00652"></a>00652   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#af92baadfc393d70b6e3f3ecaa25d16c7" title="Matrix is initialized to the identity matrix.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::SetIdentity</a>()
<a name="l00653"></a>00653   {
<a name="l00654"></a>00654     this-&gt;n_ = this-&gt;m_;
<a name="l00655"></a>00655     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00656"></a>00656       {
<a name="l00657"></a>00657         <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Reallocate(1);
<a name="l00658"></a>00658         <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Index(0) = i;
<a name="l00659"></a>00659         <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Value(0) = T(1);
<a name="l00660"></a>00660       }
<a name="l00661"></a>00661   }
<a name="l00662"></a>00662 
<a name="l00663"></a>00663 
<a name="l00665"></a>00665   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00666"></a>00666   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a06fc483908e9219787dfc9331b07d828" title="Non-zero entries are set to 0 (but not removed).">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Zero</a>()
<a name="l00667"></a>00667   {
<a name="l00668"></a>00668     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.GetM(); i++)
<a name="l00669"></a>00669       <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Zero();
<a name="l00670"></a>00670   }
<a name="l00671"></a>00671 
<a name="l00672"></a>00672 
<a name="l00674"></a>00674   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00675"></a>00675   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a806a12c725733434b08b4276de7fbcee" title="Non-zero entries are filled with values 0, 1, 2, 3 ...">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Fill</a>()
<a name="l00676"></a>00676   {
<a name="l00677"></a>00677     <span class="keywordtype">int</span> value = 0;
<a name="l00678"></a>00678     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.GetM(); i++)
<a name="l00679"></a>00679       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).GetM(); j++)
<a name="l00680"></a>00680         <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Value(j) = value++;
<a name="l00681"></a>00681   }
<a name="l00682"></a>00682 
<a name="l00683"></a>00683 
<a name="l00685"></a>00685   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allo&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l00686"></a>00686   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a806a12c725733434b08b4276de7fbcee" title="Non-zero entries are filled with values 0, 1, 2, 3 ...">Matrix_ArraySparse&lt;T, Prop, Storage, Allo&gt;::Fill</a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00687"></a>00687   {
<a name="l00688"></a>00688     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.GetM(); i++)
<a name="l00689"></a>00689       <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Fill(x);
<a name="l00690"></a>00690   }
<a name="l00691"></a>00691 
<a name="l00692"></a>00692 
<a name="l00694"></a>00694   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00695"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a28d0b06c82e4bb29f36c872f71fe7b48">00695</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00696"></a>00696   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php" title="Sparse Array-matrix class.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00697"></a>00697   <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a28d0b06c82e4bb29f36c872f71fe7b48" title="Non-zero entries are set to a given value x.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00698"></a>00698   {
<a name="l00699"></a>00699     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a806a12c725733434b08b4276de7fbcee" title="Non-zero entries are filled with values 0, 1, 2, 3 ...">Fill</a>(x);
<a name="l00700"></a>00700   }
<a name="l00701"></a>00701 
<a name="l00702"></a>00702 
<a name="l00704"></a>00704   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00705"></a>00705   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ae7161fac73344a98f937abbf957fdbb3" title="Non-zero entries take a random value.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::FillRand</a>()
<a name="l00706"></a>00706   {
<a name="l00707"></a>00707     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.GetM(); i++)
<a name="l00708"></a>00708       <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).FillRand();
<a name="l00709"></a>00709   }
<a name="l00710"></a>00710 
<a name="l00711"></a>00711 
<a name="l00713"></a>00713 
<a name="l00720"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a014e4dca632c4955660ad849db2cb363">00720</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00721"></a>00721   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a014e4dca632c4955660ad849db2cb363" title="Writes the matrix in a file.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00722"></a>00722 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a014e4dca632c4955660ad849db2cb363" title="Writes the matrix in a file.">  Write</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00723"></a>00723 <span class="keyword">  </span>{
<a name="l00724"></a>00724     ofstream FileStream;
<a name="l00725"></a>00725     FileStream.open(FileName.c_str(), ofstream::binary);
<a name="l00726"></a>00726 
<a name="l00727"></a>00727 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00728"></a>00728 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00729"></a>00729     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00730"></a>00730       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::Write(string FileName)&quot;</span>,
<a name="l00731"></a>00731                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00732"></a>00732 <span class="preprocessor">#endif</span>
<a name="l00733"></a>00733 <span class="preprocessor"></span>
<a name="l00734"></a>00734     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a014e4dca632c4955660ad849db2cb363" title="Writes the matrix in a file.">Write</a>(FileStream);
<a name="l00735"></a>00735 
<a name="l00736"></a>00736     FileStream.close();
<a name="l00737"></a>00737   }
<a name="l00738"></a>00738 
<a name="l00739"></a>00739 
<a name="l00741"></a>00741 
<a name="l00748"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#afaaf09b0ad951e8b82603e2e6f4d79c4">00748</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00749"></a>00749   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a014e4dca632c4955660ad849db2cb363" title="Writes the matrix in a file.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00750"></a>00750 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a014e4dca632c4955660ad849db2cb363" title="Writes the matrix in a file.">  Write</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00751"></a>00751 <span class="keyword">  </span>{
<a name="l00752"></a>00752 
<a name="l00753"></a>00753 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00754"></a>00754 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00755"></a>00755     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00756"></a>00756       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l00757"></a>00757                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00758"></a>00758 <span class="preprocessor">#endif</span>
<a name="l00759"></a>00759 <span class="preprocessor"></span>
<a name="l00760"></a>00760     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l00761"></a>00761                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00762"></a>00762     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;n_)),
<a name="l00763"></a>00763                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00764"></a>00764 
<a name="l00765"></a>00765     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.GetM(); i++)
<a name="l00766"></a>00766       <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Write(FileStream);
<a name="l00767"></a>00767   }
<a name="l00768"></a>00768 
<a name="l00769"></a>00769 
<a name="l00771"></a>00771 
<a name="l00775"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#adf0d3ec9d5ffeb1b3d46f523051d1412">00775</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00776"></a>00776   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#adf0d3ec9d5ffeb1b3d46f523051d1412" title="Writes the matrix in a file.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00777"></a>00777 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#adf0d3ec9d5ffeb1b3d46f523051d1412" title="Writes the matrix in a file.">  WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00778"></a>00778 <span class="keyword">  </span>{
<a name="l00779"></a>00779     ofstream FileStream; FileStream.precision(14);
<a name="l00780"></a>00780     FileStream.open(FileName.c_str());
<a name="l00781"></a>00781 
<a name="l00782"></a>00782 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00783"></a>00783 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00784"></a>00784     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00785"></a>00785       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::Write(string FileName)&quot;</span>,
<a name="l00786"></a>00786                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00787"></a>00787 <span class="preprocessor">#endif</span>
<a name="l00788"></a>00788 <span class="preprocessor"></span>
<a name="l00789"></a>00789     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#adf0d3ec9d5ffeb1b3d46f523051d1412" title="Writes the matrix in a file.">WriteText</a>(FileStream);
<a name="l00790"></a>00790 
<a name="l00791"></a>00791     FileStream.close();
<a name="l00792"></a>00792   }
<a name="l00793"></a>00793 
<a name="l00794"></a>00794 
<a name="l00796"></a>00796 
<a name="l00800"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a6037e1907164259fa78d2abaa1380577">00800</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00801"></a>00801   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#adf0d3ec9d5ffeb1b3d46f523051d1412" title="Writes the matrix in a file.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00802"></a>00802 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#adf0d3ec9d5ffeb1b3d46f523051d1412" title="Writes the matrix in a file.">  WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00803"></a>00803 <span class="keyword">  </span>{
<a name="l00804"></a>00804 
<a name="l00805"></a>00805 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00806"></a>00806 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00807"></a>00807     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00808"></a>00808       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l00809"></a>00809                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00810"></a>00810 <span class="preprocessor">#endif</span>
<a name="l00811"></a>00811 <span class="preprocessor"></span>
<a name="l00812"></a>00812     <span class="comment">// conversion in coordinate format (1-index convention)</span>
<a name="l00813"></a>00813     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> IndRow, IndCol; <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> Value;
<a name="l00814"></a>00814     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; leaf_class =
<a name="l00815"></a>00815       <span class="keyword">static_cast&lt;</span><span class="keyword">const </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; <span class="keyword">&gt;</span>(*this);
<a name="l00816"></a>00816 
<a name="l00817"></a>00817     <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(leaf_class, IndRow, IndCol,
<a name="l00818"></a>00818                                  Value, 1, <span class="keyword">true</span>);
<a name="l00819"></a>00819 
<a name="l00820"></a>00820     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; IndRow.GetM(); i++)
<a name="l00821"></a>00821       FileStream &lt;&lt; IndRow(i) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; IndCol(i) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">Value</a>(i) &lt;&lt; <span class="charliteral">&#39;\n&#39;</span>;
<a name="l00822"></a>00822 
<a name="l00823"></a>00823     <span class="comment">// If the last element a_{m,n} does not exist, we add a zero.</span>
<a name="l00824"></a>00824     <span class="keywordtype">int</span> m = Storage::GetFirst(this-&gt;m_, this-&gt;n_);
<a name="l00825"></a>00825     <span class="keywordtype">int</span> n = Storage::GetSecond(this-&gt;m_, this-&gt;n_);
<a name="l00826"></a>00826     <span class="keywordflow">if</span> (m &gt; 0 &amp;&amp; n &gt; 0)
<a name="l00827"></a>00827       {
<a name="l00828"></a>00828         <span class="keywordtype">bool</span> presence_last_elt = <span class="keyword">false</span>;
<a name="l00829"></a>00829         <span class="keywordflow">if</span> (this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(m-1).<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>() &gt; 0)
<a name="l00830"></a>00830           {
<a name="l00831"></a>00831             <span class="keywordtype">int</span> p = this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(m-1).GetM();
<a name="l00832"></a>00832             <span class="keywordflow">if</span> (this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(m-1).<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3534fb2c57f2640c9b8cd5a59e9da0e2" title="Returns column/row number of j-th non-zero value of row/column i.">Index</a>(p-1) == n-1)
<a name="l00833"></a>00833               presence_last_elt = <span class="keyword">true</span>;
<a name="l00834"></a>00834           }
<a name="l00835"></a>00835 
<a name="l00836"></a>00836         <span class="keywordflow">if</span> (!presence_last_elt)
<a name="l00837"></a>00837           {
<a name="l00838"></a>00838             T zero;
<a name="l00839"></a>00839             SetComplexZero(zero);
<a name="l00840"></a>00840             FileStream &lt;&lt; this-&gt;m_ &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; this-&gt;n_ &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; zero &lt;&lt; <span class="charliteral">&#39;\n&#39;</span>;
<a name="l00841"></a>00841           }
<a name="l00842"></a>00842       }
<a name="l00843"></a>00843   }
<a name="l00844"></a>00844 
<a name="l00845"></a>00845 
<a name="l00847"></a>00847 
<a name="l00854"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a2e644c704eba4e3c531c403952189d86">00854</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00855"></a>00855   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a2e644c704eba4e3c531c403952189d86" title="Reads the matrix from a file.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00856"></a>00856 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a2e644c704eba4e3c531c403952189d86" title="Reads the matrix from a file.">  Read</a>(<span class="keywordtype">string</span> FileName)
<a name="l00857"></a>00857   {
<a name="l00858"></a>00858     ifstream FileStream;
<a name="l00859"></a>00859     FileStream.open(FileName.c_str(), ifstream::binary);
<a name="l00860"></a>00860 
<a name="l00861"></a>00861 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00862"></a>00862 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00863"></a>00863     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00864"></a>00864       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::Read(string FileName)&quot;</span>,
<a name="l00865"></a>00865                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00866"></a>00866 <span class="preprocessor">#endif</span>
<a name="l00867"></a>00867 <span class="preprocessor"></span>
<a name="l00868"></a>00868     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a2e644c704eba4e3c531c403952189d86" title="Reads the matrix from a file.">Read</a>(FileStream);
<a name="l00869"></a>00869 
<a name="l00870"></a>00870     FileStream.close();
<a name="l00871"></a>00871   }
<a name="l00872"></a>00872 
<a name="l00873"></a>00873 
<a name="l00875"></a>00875 
<a name="l00882"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#abeb6e31b268538d3c43fc8f795e994d4">00882</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00883"></a>00883   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a2e644c704eba4e3c531c403952189d86" title="Reads the matrix from a file.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00884"></a>00884 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a2e644c704eba4e3c531c403952189d86" title="Reads the matrix from a file.">  Read</a>(istream&amp; FileStream)
<a name="l00885"></a>00885   {
<a name="l00886"></a>00886 
<a name="l00887"></a>00887 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00888"></a>00888 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00889"></a>00889     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00890"></a>00890       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::Read(ofstream&amp; FileStream)&quot;</span>,
<a name="l00891"></a>00891                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00892"></a>00892 <span class="preprocessor">#endif</span>
<a name="l00893"></a>00893 <span class="preprocessor"></span>
<a name="l00894"></a>00894     FileStream.read(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l00895"></a>00895                     <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00896"></a>00896     FileStream.read(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;n_)),
<a name="l00897"></a>00897                     <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00898"></a>00898 
<a name="l00899"></a>00899     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.Reallocate(Storage::GetFirst(this-&gt;m_, this-&gt;n_));
<a name="l00900"></a>00900     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.GetM(); i++)
<a name="l00901"></a>00901       <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Read(FileStream);
<a name="l00902"></a>00902 
<a name="l00903"></a>00903 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00904"></a>00904 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l00905"></a>00905     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00906"></a>00906       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::Read(istream&amp; FileStream)&quot;</span>,
<a name="l00907"></a>00907                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Input operation failed.&quot;</span>)
<a name="l00908"></a>00908                     + string(<span class="stringliteral">&quot; The input file may have been removed&quot;</span>)
<a name="l00909"></a>00909                     + <span class="stringliteral">&quot; or may not contain enough data.&quot;</span>);
<a name="l00910"></a>00910 <span class="preprocessor">#endif</span>
<a name="l00911"></a>00911 <span class="preprocessor"></span>
<a name="l00912"></a>00912   }
<a name="l00913"></a>00913 
<a name="l00914"></a>00914 
<a name="l00916"></a>00916 
<a name="l00920"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a22e85b66114b00ba89b1615bc1839b0c">00920</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00921"></a>00921   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a22e85b66114b00ba89b1615bc1839b0c" title="Reads the matrix from a file.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00922"></a>00922 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a22e85b66114b00ba89b1615bc1839b0c" title="Reads the matrix from a file.">  ReadText</a>(<span class="keywordtype">string</span> FileName)
<a name="l00923"></a>00923   {
<a name="l00924"></a>00924     ifstream FileStream;
<a name="l00925"></a>00925     FileStream.open(FileName.c_str());
<a name="l00926"></a>00926 
<a name="l00927"></a>00927 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00928"></a>00928 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00929"></a>00929     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00930"></a>00930       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::ReadText(string FileName)&quot;</span>,
<a name="l00931"></a>00931                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00932"></a>00932 <span class="preprocessor">#endif</span>
<a name="l00933"></a>00933 <span class="preprocessor"></span>
<a name="l00934"></a>00934     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a22e85b66114b00ba89b1615bc1839b0c" title="Reads the matrix from a file.">ReadText</a>(FileStream);
<a name="l00935"></a>00935 
<a name="l00936"></a>00936     FileStream.close();
<a name="l00937"></a>00937   }
<a name="l00938"></a>00938 
<a name="l00939"></a>00939 
<a name="l00941"></a>00941 
<a name="l00945"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a4bac922eaed55ed72c4fcfac89b405ca">00945</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00946"></a>00946   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a22e85b66114b00ba89b1615bc1839b0c" title="Reads the matrix from a file.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00947"></a>00947 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a22e85b66114b00ba89b1615bc1839b0c" title="Reads the matrix from a file.">  ReadText</a>(istream&amp; FileStream)
<a name="l00948"></a>00948   {
<a name="l00949"></a>00949     <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; leaf_class =
<a name="l00950"></a>00950       <span class="keyword">static_cast&lt;</span><a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; <span class="keyword">&gt;</span>(*this);
<a name="l00951"></a>00951 
<a name="l00952"></a>00952     T zero; <span class="keywordtype">int</span> index = 1;
<a name="l00953"></a>00953     <a class="code" href="namespace_seldon.php#af8428b13721ad15b95a74ecf07d23668" title="Reading of matrix in coordinate format.">ReadCoordinateMatrix</a>(leaf_class, FileStream, zero, index);
<a name="l00954"></a>00954   }
<a name="l00955"></a>00955 
<a name="l00956"></a>00956 
<a name="l00958"></a>00958   <span class="comment">// MATRIX&lt;ARRAY_COLSPARSE&gt; //</span>
<a name="l00960"></a>00960 <span class="comment"></span>
<a name="l00961"></a>00961 
<a name="l00963"></a>00963 
<a name="l00966"></a>00966   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00967"></a>00967   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::Matrix</a>():
<a name="l00968"></a>00968     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php" title="Sparse Array-matrix class.">Matrix_ArraySparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_col_sparse.php">ArrayColSparse</a>, Allocator&gt;()
<a name="l00969"></a>00969   {
<a name="l00970"></a>00970   }
<a name="l00971"></a>00971 
<a name="l00972"></a>00972 
<a name="l00974"></a>00974 
<a name="l00979"></a>00979   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00980"></a>00980   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#a06100dad6cbda6411aeb3e87665175da" title="Default constructor.">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l00981"></a>00981     Matrix_ArraySparse&lt;T, Prop, ArrayColSparse, Allocator&gt;(i, j)
<a name="l00982"></a>00982   {
<a name="l00983"></a>00983   }
<a name="l00984"></a>00984 
<a name="l00985"></a>00985 
<a name="l00987"></a>00987   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00988"></a>00988   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#a00b55880d103fffa13cd1eaa1e49dac9" title="Clears column i.">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::ClearColumn</a>(<span class="keywordtype">int</span> i)
<a name="l00989"></a>00989   {
<a name="l00990"></a>00990     this-&gt;val_(i).Clear();
<a name="l00991"></a>00991   }
<a name="l00992"></a>00992 
<a name="l00993"></a>00993 
<a name="l00995"></a>00995 
<a name="l00999"></a>00999   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Alloc&gt; <span class="keyword">inline</span>
<a name="l01000"></a>01000   <span class="keywordtype">void</span> Matrix&lt;T, Prop, ArrayColSparse, Alloc&gt;::ReallocateColumn(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01001"></a>01001   {
<a name="l01002"></a>01002     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Reallocate(j);
<a name="l01003"></a>01003   }
<a name="l01004"></a>01004 
<a name="l01005"></a>01005 
<a name="l01007"></a>01007 
<a name="l01011"></a>01011   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01012"></a>01012   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#ad92aa8f008beddbec6d8a41711409655" title="Reallocates column i.">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::ResizeColumn</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01013"></a>01013   {
<a name="l01014"></a>01014     this-&gt;val_(i).Resize(j);
<a name="l01015"></a>01015   }
<a name="l01016"></a>01016 
<a name="l01017"></a>01017 
<a name="l01019"></a>01019 
<a name="l01023"></a>01023   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01024"></a>01024   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#a9007597f6b3c40534bd422990e75da7f" title="Swaps two columns.">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::SwapColumn</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01025"></a>01025   {
<a name="l01026"></a>01026     Swap(this-&gt;val_(i), this-&gt;val_(j));
<a name="l01027"></a>01027   }
<a name="l01028"></a>01028 
<a name="l01029"></a>01029 
<a name="l01031"></a>01031 
<a name="l01035"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#aff85ed83508cf4e0bf75079ae2e73b4f">01035</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01036"></a>01036   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::</a>
<a name="l01037"></a>01037 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceIndexColumn</a>(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l01038"></a>01038   {
<a name="l01039"></a>01039     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_(i).GetM(); j++)
<a name="l01040"></a>01040       this-&gt;val_(i).Index(j) = new_index(j);
<a name="l01041"></a>01041   }
<a name="l01042"></a>01042 
<a name="l01043"></a>01043 
<a name="l01045"></a>01045 
<a name="l01049"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#a6db90999f5bd69e71afcf585b11a00e7">01049</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01050"></a>01050   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::</a>
<a name="l01051"></a>01051 <a class="code" href="class_seldon_1_1_matrix.php">  GetColumnSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01052"></a>01052 <span class="keyword">  </span>{
<a name="l01053"></a>01053     <span class="keywordflow">return</span> this-&gt;val_(i).GetSize();
<a name="l01054"></a>01054   }
<a name="l01055"></a>01055 
<a name="l01056"></a>01056 
<a name="l01058"></a>01058   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01059"></a>01059   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::PrintColumn</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01060"></a>01060 <span class="keyword">  </span>{
<a name="l01061"></a>01061     this-&gt;val_(i).Print();
<a name="l01062"></a>01062   }
<a name="l01063"></a>01063 
<a name="l01064"></a>01064 
<a name="l01066"></a>01066 
<a name="l01071"></a>01071   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01072"></a>01072   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#aa16ced801acf542fc6a0d2588c226500" title="Assembles a column.">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::AssembleColumn</a>(<span class="keywordtype">int</span> i)
<a name="l01073"></a>01073   {
<a name="l01074"></a>01074     this-&gt;val_(i).Assemble();
<a name="l01075"></a>01075   }
<a name="l01076"></a>01076 
<a name="l01077"></a>01077 
<a name="l01079"></a>01079 
<a name="l01084"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#ab2b0465e1e4a7323bff043c74f95b7f3">01084</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01085"></a>01085   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::</a>
<a name="l01086"></a>01086 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; val)
<a name="l01087"></a>01087   {
<a name="l01088"></a>01088     this-&gt;val_(j).AddInteraction(i, val);
<a name="l01089"></a>01089   }
<a name="l01090"></a>01090 
<a name="l01091"></a>01091 
<a name="l01093"></a>01093 
<a name="l01099"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#a62961435a3e36a3b89d8f1d452a26ee7">01099</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01100"></a>01100   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::</a>
<a name="l01101"></a>01101 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keywordtype">int</span>* col_, T* value_)
<a name="l01102"></a>01102   {
<a name="l01103"></a>01103     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> col;
<a name="l01104"></a>01104     col.SetData(nb, col_);
<a name="l01105"></a>01105     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val;
<a name="l01106"></a>01106     val.SetData(nb, value_);
<a name="l01107"></a>01107     AddInteractionRow(i, nb, col, val);
<a name="l01108"></a>01108     col.Nullify();
<a name="l01109"></a>01109     val.Nullify();
<a name="l01110"></a>01110   }
<a name="l01111"></a>01111 
<a name="l01112"></a>01112 
<a name="l01114"></a>01114 
<a name="l01120"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#a5f71f557c041e0d4ee09f656c23e4719">01120</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01121"></a>01121   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::</a>
<a name="l01122"></a>01122 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keywordtype">int</span>* row_, T* value_)
<a name="l01123"></a>01123   {
<a name="l01124"></a>01124     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> row;
<a name="l01125"></a>01125     row.SetData(nb, row_);
<a name="l01126"></a>01126     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val;
<a name="l01127"></a>01127     val.SetData(nb, value_);
<a name="l01128"></a>01128     AddInteractionColumn(i, nb, row, val);
<a name="l01129"></a>01129     row.Nullify();
<a name="l01130"></a>01130     val.Nullify();
<a name="l01131"></a>01131   }
<a name="l01132"></a>01132 
<a name="l01133"></a>01133 
<a name="l01135"></a>01135 
<a name="l01141"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#a3015e7a923f01ee2581975650294ad6e">01141</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01142"></a>01142   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::</a>
<a name="l01143"></a>01143 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l01144"></a>01144                     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val)
<a name="l01145"></a>01145   {
<a name="l01146"></a>01146     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nb; j++)
<a name="l01147"></a>01147       this-&gt;val_(col(j)).AddInteraction(i, val(j));
<a name="l01148"></a>01148   }
<a name="l01149"></a>01149 
<a name="l01150"></a>01150 
<a name="l01152"></a>01152 
<a name="l01158"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#ac658b9f8e33a4656b1eda205677218db">01158</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01159"></a>01159   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::</a>
<a name="l01160"></a>01160 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l01161"></a>01161                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val)
<a name="l01162"></a>01162   {
<a name="l01163"></a>01163     this-&gt;val_(i).AddInteractionRow(nb, row, val);
<a name="l01164"></a>01164   }
<a name="l01165"></a>01165 
<a name="l01166"></a>01166 
<a name="l01168"></a>01168   <span class="comment">// MATRIX&lt;ARRAY_ROWSPARSE&gt; //</span>
<a name="l01170"></a>01170 <span class="comment"></span>
<a name="l01171"></a>01171 
<a name="l01173"></a>01173 
<a name="l01176"></a>01176   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01177"></a>01177   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::Matrix</a>():
<a name="l01178"></a>01178     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php" title="Sparse Array-matrix class.">Matrix_ArraySparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_row_sparse.php">ArrayRowSparse</a>, Allocator&gt;()
<a name="l01179"></a>01179   {
<a name="l01180"></a>01180   }
<a name="l01181"></a>01181 
<a name="l01182"></a>01182 
<a name="l01184"></a>01184 
<a name="l01189"></a>01189   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01190"></a>01190   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a45d914bbc71d7f861330e804296906c2" title="Default constructor.">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01191"></a>01191     Matrix_ArraySparse&lt;T, Prop, ArrayRowSparse, Allocator&gt;(i, j)
<a name="l01192"></a>01192   {
<a name="l01193"></a>01193   }
<a name="l01194"></a>01194 
<a name="l01195"></a>01195 
<a name="l01197"></a>01197   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01198"></a>01198   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#ad7e78da914d3e0d3818b8c21ec8e7bbf" title="Clears a row.">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::ClearRow</a>(<span class="keywordtype">int</span> i)
<a name="l01199"></a>01199   {
<a name="l01200"></a>01200     this-&gt;val_(i).Clear();
<a name="l01201"></a>01201   }
<a name="l01202"></a>01202 
<a name="l01203"></a>01203 
<a name="l01205"></a>01205 
<a name="l01210"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a141cfc9e4bacfcb106564df3154a104b">01210</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01211"></a>01211   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::</a>
<a name="l01212"></a>01212 <a class="code" href="class_seldon_1_1_matrix.php">  ReallocateRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01213"></a>01213   {
<a name="l01214"></a>01214     this-&gt;val_(i).Reallocate(j);
<a name="l01215"></a>01215   }
<a name="l01216"></a>01216 
<a name="l01217"></a>01217 
<a name="l01219"></a>01219 
<a name="l01224"></a>01224   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01225"></a>01225   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::ResizeRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01226"></a>01226   {
<a name="l01227"></a>01227     this-&gt;val_(i).Resize(j);
<a name="l01228"></a>01228   }
<a name="l01229"></a>01229 
<a name="l01230"></a>01230 
<a name="l01232"></a>01232 
<a name="l01236"></a>01236   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01237"></a>01237   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a6a476c1d342322cb6b0d16d19de837dc" title="Swaps two rows.">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::SwapRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01238"></a>01238   {
<a name="l01239"></a>01239     Swap(this-&gt;val_(i), this-&gt;val_(j));
<a name="l01240"></a>01240   }
<a name="l01241"></a>01241 
<a name="l01242"></a>01242 
<a name="l01244"></a>01244 
<a name="l01248"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a29c3376f6702a15782a46c240c7ce8bd">01248</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01249"></a>01249   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::</a>
<a name="l01250"></a>01250 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceIndexRow</a>(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l01251"></a>01251   {
<a name="l01252"></a>01252     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_(i).GetM(); j++)
<a name="l01253"></a>01253       this-&gt;val_(i).Index(j) = new_index(j);
<a name="l01254"></a>01254   }
<a name="l01255"></a>01255 
<a name="l01256"></a>01256 
<a name="l01258"></a>01258 
<a name="l01262"></a>01262   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01263"></a>01263   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::GetRowSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01264"></a>01264 <span class="keyword">  </span>{
<a name="l01265"></a>01265     <span class="keywordflow">return</span> this-&gt;val_(i).GetSize();
<a name="l01266"></a>01266   }
<a name="l01267"></a>01267 
<a name="l01268"></a>01268 
<a name="l01270"></a>01270   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01271"></a>01271   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#aedf0c511638d6bcc7f5b8e4cfddf6e93" title="Displays non-zero values of a row.">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::PrintRow</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01272"></a>01272 <span class="keyword">  </span>{
<a name="l01273"></a>01273     this-&gt;val_(i).Print();
<a name="l01274"></a>01274   }
<a name="l01275"></a>01275 
<a name="l01276"></a>01276 
<a name="l01278"></a>01278 
<a name="l01283"></a>01283   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01284"></a>01284   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a73b4fcf4fa577bbe4a88cf0a2f9a0bbc" title="Assembles a row.">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::AssembleRow</a>(<span class="keywordtype">int</span> i)
<a name="l01285"></a>01285   {
<a name="l01286"></a>01286     this-&gt;val_(i).Assemble();
<a name="l01287"></a>01287   }
<a name="l01288"></a>01288 
<a name="l01290"></a>01290 
<a name="l01295"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a4bed47faffc46ab4a08c0869e5f7cdad">01295</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01296"></a>01296   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::</a>
<a name="l01297"></a>01297 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; val)
<a name="l01298"></a>01298   {
<a name="l01299"></a>01299     this-&gt;val_(i).AddInteraction(j, val);
<a name="l01300"></a>01300   }
<a name="l01301"></a>01301 
<a name="l01302"></a>01302 
<a name="l01304"></a>01304 
<a name="l01310"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a6428579a6d7521fb171b28c12f48a030">01310</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01311"></a>01311   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::</a>
<a name="l01312"></a>01312 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keywordtype">int</span>* col_, T* value_)
<a name="l01313"></a>01313   {
<a name="l01314"></a>01314     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> col;
<a name="l01315"></a>01315     col.SetData(nb, col_);
<a name="l01316"></a>01316     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val;
<a name="l01317"></a>01317     val.SetData(nb, value_);
<a name="l01318"></a>01318     AddInteractionRow(i, nb, col, val);
<a name="l01319"></a>01319     col.Nullify();
<a name="l01320"></a>01320     val.Nullify();
<a name="l01321"></a>01321   }
<a name="l01322"></a>01322 
<a name="l01323"></a>01323 
<a name="l01325"></a>01325 
<a name="l01331"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#aba0481a22dc304dc647d1ff3efdf9916">01331</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01332"></a>01332   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::</a>
<a name="l01333"></a>01333 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keywordtype">int</span>* row_, T* value_)
<a name="l01334"></a>01334   {
<a name="l01335"></a>01335     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> row;
<a name="l01336"></a>01336     row.SetData(nb, row_);
<a name="l01337"></a>01337     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val;
<a name="l01338"></a>01338     val.SetData(nb, value_);
<a name="l01339"></a>01339     AddInteractionColumn(i, nb, row, val);
<a name="l01340"></a>01340     row.Nullify();
<a name="l01341"></a>01341     val.Nullify();
<a name="l01342"></a>01342   }
<a name="l01343"></a>01343 
<a name="l01344"></a>01344 
<a name="l01346"></a>01346 
<a name="l01352"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a0563d3c1193b5095374765262957006f">01352</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01353"></a>01353   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::</a>
<a name="l01354"></a>01354 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l01355"></a>01355                     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val)
<a name="l01356"></a>01356   {
<a name="l01357"></a>01357     this-&gt;val_(i).AddInteractionRow(nb, col, val);
<a name="l01358"></a>01358   }
<a name="l01359"></a>01359 
<a name="l01360"></a>01360 
<a name="l01362"></a>01362 
<a name="l01368"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a1f7b4b524875b008fc0aa212d022ac37">01368</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01369"></a>01369   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::</a>
<a name="l01370"></a>01370 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l01371"></a>01371                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val)
<a name="l01372"></a>01372   {
<a name="l01373"></a>01373     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nb; j++)
<a name="l01374"></a>01374       this-&gt;val_(row(j)).AddInteraction(i, val(j));
<a name="l01375"></a>01375   }
<a name="l01376"></a>01376 
<a name="l01377"></a>01377 
<a name="l01379"></a>01379   <span class="comment">// MATRIX&lt;ARRAY_COLSYMSPARSE&gt; //</span>
<a name="l01381"></a>01381 <span class="comment"></span>
<a name="l01382"></a>01382 
<a name="l01384"></a>01384 
<a name="l01387"></a>01387   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01388"></a>01388   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::Matrix</a>():
<a name="l01389"></a>01389     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php" title="Sparse Array-matrix class.">Matrix_ArraySparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_col_sym_sparse.php">ArrayColSymSparse</a>, Allocator&gt;()
<a name="l01390"></a>01390   {
<a name="l01391"></a>01391   }
<a name="l01392"></a>01392 
<a name="l01393"></a>01393 
<a name="l01395"></a>01395 
<a name="l01400"></a>01400   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01401"></a>01401   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a49019378c18567a9a3544f5801dd9b33" title="Default constructor.">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01402"></a>01402     Matrix_ArraySparse&lt;T, Prop, ArrayColSymSparse, Allocator&gt;(i, j)
<a name="l01403"></a>01403   {
<a name="l01404"></a>01404   }
<a name="l01405"></a>01405 
<a name="l01406"></a>01406 
<a name="l01407"></a>01407   <span class="comment">/**********************************</span>
<a name="l01408"></a>01408 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l01409"></a>01409 <span class="comment">   **********************************/</span>
<a name="l01410"></a>01410 
<a name="l01411"></a>01411 
<a name="l01413"></a>01413 
<a name="l01419"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a1437cb081d496b576b36ae061cd9c51c">01419</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01420"></a>01420   <span class="keyword">inline</span> T
<a name="l01421"></a>01421   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"></span>
<a name="l01422"></a>01422 <span class="keyword">    const</span>
<a name="l01423"></a>01423 <span class="keyword">  </span>{
<a name="l01424"></a>01424 
<a name="l01425"></a>01425 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01426"></a>01426 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01427"></a>01427       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01428"></a>01428                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01429"></a>01429                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01430"></a>01430     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01431"></a>01431       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01432"></a>01432                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01433"></a>01433                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01434"></a>01434 <span class="preprocessor">#endif</span>
<a name="l01435"></a>01435 <span class="preprocessor"></span>
<a name="l01436"></a>01436     <span class="keywordflow">if</span> (i &lt; j)
<a name="l01437"></a>01437       <span class="keywordflow">return</span> this-&gt;val_(j)(i);
<a name="l01438"></a>01438 
<a name="l01439"></a>01439     <span class="keywordflow">return</span> this-&gt;val_(i)(j);
<a name="l01440"></a>01440   }
<a name="l01441"></a>01441 
<a name="l01442"></a>01442 
<a name="l01444"></a>01444 
<a name="l01450"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a5496356b5784b7c6e45fccbe44e51a00">01450</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01451"></a>01451   <span class="keyword">inline</span> T&amp;
<a name="l01452"></a>01452   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01453"></a>01453   {
<a name="l01454"></a>01454 
<a name="l01455"></a>01455 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01456"></a>01456 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01457"></a>01457       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01458"></a>01458                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01459"></a>01459                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01460"></a>01460     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01461"></a>01461       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01462"></a>01462                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01463"></a>01463                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01464"></a>01464 <span class="preprocessor">#endif</span>
<a name="l01465"></a>01465 <span class="preprocessor"></span>
<a name="l01466"></a>01466     <span class="keywordflow">if</span> (i &lt; j)
<a name="l01467"></a>01467       <span class="keywordflow">return</span> this-&gt;val_(j).Get(i);
<a name="l01468"></a>01468 
<a name="l01469"></a>01469     <span class="keywordflow">return</span> this-&gt;val_(i).Get(j);
<a name="l01470"></a>01470   }
<a name="l01471"></a>01471 
<a name="l01472"></a>01472 
<a name="l01474"></a>01474 
<a name="l01480"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a5c9e6737ed3577bc734948740f733e26">01480</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01481"></a>01481   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp;
<a name="l01482"></a>01482   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l01483"></a>01483 <span class="keyword">  </span>{
<a name="l01484"></a>01484 
<a name="l01485"></a>01485 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01486"></a>01486 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01487"></a>01487       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01488"></a>01488                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01489"></a>01489                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01490"></a>01490     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01491"></a>01491       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01492"></a>01492                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01493"></a>01493                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01494"></a>01494 <span class="preprocessor">#endif</span>
<a name="l01495"></a>01495 <span class="preprocessor"></span>
<a name="l01496"></a>01496     <span class="keywordflow">if</span> (i &lt; j)
<a name="l01497"></a>01497       <span class="keywordflow">return</span> this-&gt;val_(j).Get(i);
<a name="l01498"></a>01498 
<a name="l01499"></a>01499     <span class="keywordflow">return</span> this-&gt;val_(i).Get(j);
<a name="l01500"></a>01500   }
<a name="l01501"></a>01501 
<a name="l01502"></a>01502 
<a name="l01504"></a>01504 
<a name="l01510"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a7a12553a1f4b81044e5def6cab89a7a1">01510</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01511"></a>01511   <span class="keyword">inline</span> T&amp;
<a name="l01512"></a>01512   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01513"></a>01513   {
<a name="l01514"></a>01514 
<a name="l01515"></a>01515 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01516"></a>01516 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01517"></a>01517       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01518"></a>01518                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01519"></a>01519                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01520"></a>01520     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01521"></a>01521       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01522"></a>01522                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01523"></a>01523                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01524"></a>01524     <span class="keywordflow">if</span> (i &gt; j)
<a name="l01525"></a>01525       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix::Val()&quot;</span>, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;With this function, you &quot;</span>)
<a name="l01526"></a>01526                           + <span class="stringliteral">&quot;can only access upper part of matrix.&quot;</span>);
<a name="l01527"></a>01527 <span class="preprocessor">#endif</span>
<a name="l01528"></a>01528 <span class="preprocessor"></span>
<a name="l01529"></a>01529     <span class="keywordflow">return</span> this-&gt;val_(j).Val(i);
<a name="l01530"></a>01530   }
<a name="l01531"></a>01531 
<a name="l01532"></a>01532 
<a name="l01534"></a>01534 
<a name="l01540"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a51307b7343833f1754ad2f359449d202">01540</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01541"></a>01541   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp;
<a name="l01542"></a>01542   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l01543"></a>01543 <span class="keyword">  </span>{
<a name="l01544"></a>01544 
<a name="l01545"></a>01545 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01546"></a>01546 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01547"></a>01547       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01548"></a>01548                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01549"></a>01549                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01550"></a>01550     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01551"></a>01551       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01552"></a>01552                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01553"></a>01553                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01554"></a>01554 <span class="preprocessor">#endif</span>
<a name="l01555"></a>01555 <span class="preprocessor"></span>
<a name="l01556"></a>01556     <span class="keywordflow">return</span> this-&gt;val_(j).Val(i);
<a name="l01557"></a>01557   }
<a name="l01558"></a>01558 
<a name="l01559"></a>01559 
<a name="l01561"></a>01561 
<a name="l01566"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a567450d65dd57f4dc43838547fae50a2">01566</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01567"></a>01567   <span class="keyword">inline</span> <span class="keywordtype">void</span>
<a name="l01568"></a>01568   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::Set</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; x)
<a name="l01569"></a>01569   {
<a name="l01570"></a>01570     <span class="keywordflow">if</span> (i &lt; j)
<a name="l01571"></a>01571       this-&gt;val_(j).Get(i) = x;
<a name="l01572"></a>01572     <span class="keywordflow">else</span>
<a name="l01573"></a>01573       this-&gt;val_(i).Get(j) = x;
<a name="l01574"></a>01574   }
<a name="l01575"></a>01575 
<a name="l01576"></a>01576 
<a name="l01578"></a>01578   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01579"></a>01579   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::ClearColumn</a>(<span class="keywordtype">int</span> i)
<a name="l01580"></a>01580   {
<a name="l01581"></a>01581     this-&gt;val_(i).Clear();
<a name="l01582"></a>01582   }
<a name="l01583"></a>01583 
<a name="l01584"></a>01584 
<a name="l01586"></a>01586 
<a name="l01591"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a2f50f94ea485c1c81aecdedd76e09116">01591</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01592"></a>01592   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01593"></a>01593 <a class="code" href="class_seldon_1_1_matrix.php">  ReallocateColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01594"></a>01594   {
<a name="l01595"></a>01595     this-&gt;val_(i).Reallocate(j);
<a name="l01596"></a>01596   }
<a name="l01597"></a>01597 
<a name="l01598"></a>01598 
<a name="l01600"></a>01600 
<a name="l01604"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a7350581df0edd83fe9d180db302afd56">01604</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01605"></a>01605   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01606"></a>01606 <a class="code" href="class_seldon_1_1_matrix.php">  ResizeColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01607"></a>01607   {
<a name="l01608"></a>01608     this-&gt;val_(i).Resize(j);
<a name="l01609"></a>01609   }
<a name="l01610"></a>01610 
<a name="l01611"></a>01611 
<a name="l01613"></a>01613 
<a name="l01617"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#ab9de78a2a24aa5fa8cec25f93232c4ff">01617</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01618"></a>01618   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01619"></a>01619 <a class="code" href="class_seldon_1_1_matrix.php">  SwapColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01620"></a>01620   {
<a name="l01621"></a>01621     Swap(this-&gt;val_(i), this-&gt;val_(j));
<a name="l01622"></a>01622   }
<a name="l01623"></a>01623 
<a name="l01624"></a>01624 
<a name="l01626"></a>01626 
<a name="l01630"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#aece56a1f1bdc20ffc86e29d92e8d0c41">01630</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01631"></a>01631   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01632"></a>01632 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceIndexColumn</a>(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l01633"></a>01633   {
<a name="l01634"></a>01634     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_(i).GetM(); j++)
<a name="l01635"></a>01635       this-&gt;val_(i).Index(j) = new_index(j);
<a name="l01636"></a>01636   }
<a name="l01637"></a>01637 
<a name="l01638"></a>01638 
<a name="l01640"></a>01640 
<a name="l01644"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#accfeb4e792ac6f354c2f9c40827e8c3a">01644</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01645"></a>01645   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01646"></a>01646 <a class="code" href="class_seldon_1_1_matrix.php">  GetColumnSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01647"></a>01647 <span class="keyword">  </span>{
<a name="l01648"></a>01648     <span class="keywordflow">return</span> this-&gt;val_(i).GetSize();
<a name="l01649"></a>01649   }
<a name="l01650"></a>01650 
<a name="l01651"></a>01651 
<a name="l01653"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a875318957a4ebbeabb1c01f24ea9e9cf">01653</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01654"></a>01654   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01655"></a>01655 <a class="code" href="class_seldon_1_1_matrix.php">  PrintColumn</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01656"></a>01656 <span class="keyword">  </span>{
<a name="l01657"></a>01657     this-&gt;val_(i).Print();
<a name="l01658"></a>01658   }
<a name="l01659"></a>01659 
<a name="l01660"></a>01660 
<a name="l01662"></a>01662 
<a name="l01667"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a6dc03b72ecdd09350ba69ea8f0188691">01667</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01668"></a>01668   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01669"></a>01669 <a class="code" href="class_seldon_1_1_matrix.php">  AssembleColumn</a>(<span class="keywordtype">int</span> i)
<a name="l01670"></a>01670   {
<a name="l01671"></a>01671     this-&gt;val_(i).Assemble();
<a name="l01672"></a>01672   }
<a name="l01673"></a>01673 
<a name="l01674"></a>01674 
<a name="l01676"></a>01676 
<a name="l01682"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a60c5f3ef5501776c7e57256d2d96d578">01682</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01683"></a>01683   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01684"></a>01684 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keywordtype">int</span>* col_, T* value_)
<a name="l01685"></a>01685   {
<a name="l01686"></a>01686     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> col;
<a name="l01687"></a>01687     col.SetData(nb, col_);
<a name="l01688"></a>01688     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val;
<a name="l01689"></a>01689     val.SetData(nb, value_);
<a name="l01690"></a>01690     AddInteractionRow(i, nb, col, val);
<a name="l01691"></a>01691     col.Nullify();
<a name="l01692"></a>01692     val.Nullify();
<a name="l01693"></a>01693   }
<a name="l01694"></a>01694 
<a name="l01695"></a>01695 
<a name="l01697"></a>01697 
<a name="l01703"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#ae52a1b46f4454ee1591cd6b71c83574c">01703</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01704"></a>01704   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01705"></a>01705 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keywordtype">int</span>* row_, T* value_)
<a name="l01706"></a>01706   {
<a name="l01707"></a>01707     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> row;
<a name="l01708"></a>01708     row.SetData(nb, row_);
<a name="l01709"></a>01709     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val;
<a name="l01710"></a>01710     val.SetData(nb, value_);
<a name="l01711"></a>01711     AddInteractionColumn(i, nb, row, val);
<a name="l01712"></a>01712     row.Nullify();
<a name="l01713"></a>01713     val.Nullify();
<a name="l01714"></a>01714   }
<a name="l01715"></a>01715 
<a name="l01716"></a>01716 
<a name="l01718"></a>01718 
<a name="l01723"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a3d27b66507e6d6fe9e35684eb69e6a3b">01723</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01724"></a>01724   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01725"></a>01725 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; val)
<a name="l01726"></a>01726   {
<a name="l01727"></a>01727     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l01728"></a>01728       this-&gt;val_(j).AddInteraction(i, val);
<a name="l01729"></a>01729   }
<a name="l01730"></a>01730 
<a name="l01731"></a>01731 
<a name="l01733"></a>01733 
<a name="l01739"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#ac128b73917745e8cf0e543abbae95ea6">01739</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01740"></a>01740   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01741"></a>01741 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l01742"></a>01742                     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val)
<a name="l01743"></a>01743   {
<a name="l01744"></a>01744     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nb; j++)
<a name="l01745"></a>01745       <span class="keywordflow">if</span> (i &lt;= col(j))
<a name="l01746"></a>01746         this-&gt;val_(col(j)).AddInteraction(i, val(j));
<a name="l01747"></a>01747   }
<a name="l01748"></a>01748 
<a name="l01749"></a>01749 
<a name="l01751"></a>01751 
<a name="l01757"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a3b1e5feec5c47a50c828ff3709380c4d">01757</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01758"></a>01758   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01759"></a>01759 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l01760"></a>01760                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val)
<a name="l01761"></a>01761   {
<a name="l01762"></a>01762     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> new_row(nb);
<a name="l01763"></a>01763     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a> new_val(nb);
<a name="l01764"></a>01764     nb = 0;
<a name="l01765"></a>01765     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; new_row.GetM(); j++)
<a name="l01766"></a>01766       <span class="keywordflow">if</span> (row(j) &lt;= i)
<a name="l01767"></a>01767         {
<a name="l01768"></a>01768           new_row(nb) = row(j);
<a name="l01769"></a>01769           new_val(nb) = val(j); nb++;
<a name="l01770"></a>01770         }
<a name="l01771"></a>01771 
<a name="l01772"></a>01772     this-&gt;val_(i).AddInteractionRow(nb, new_row, new_val);
<a name="l01773"></a>01773   }
<a name="l01774"></a>01774 
<a name="l01775"></a>01775 
<a name="l01777"></a>01777   <span class="comment">// MATRIX&lt;ARRAY_ROWSYMSPARSE&gt; //</span>
<a name="l01779"></a>01779 <span class="comment"></span>
<a name="l01780"></a>01780 
<a name="l01782"></a>01782 
<a name="l01785"></a>01785   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01786"></a>01786   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::Matrix</a>():
<a name="l01787"></a>01787     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php" title="Sparse Array-matrix class.">Matrix_ArraySparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_row_sym_sparse.php">ArrayRowSymSparse</a>, Allocator&gt;()
<a name="l01788"></a>01788   {
<a name="l01789"></a>01789   }
<a name="l01790"></a>01790 
<a name="l01791"></a>01791 
<a name="l01793"></a>01793 
<a name="l01798"></a>01798   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01799"></a>01799   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a9bd699fbd7fe4a62e4216883a322b623" title="Default constructor.">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01800"></a>01800     Matrix_ArraySparse&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;(i, j)
<a name="l01801"></a>01801   {
<a name="l01802"></a>01802   }
<a name="l01803"></a>01803 
<a name="l01804"></a>01804 
<a name="l01805"></a>01805   <span class="comment">/**********************************</span>
<a name="l01806"></a>01806 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l01807"></a>01807 <span class="comment">   **********************************/</span>
<a name="l01808"></a>01808 
<a name="l01809"></a>01809 
<a name="l01811"></a>01811 
<a name="l01817"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a8d872a0e035a2afcc8e40736e8556c44">01817</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01818"></a>01818   <span class="keyword">inline</span> T
<a name="l01819"></a>01819   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"></span>
<a name="l01820"></a>01820 <span class="keyword">    const</span>
<a name="l01821"></a>01821 <span class="keyword">  </span>{
<a name="l01822"></a>01822 
<a name="l01823"></a>01823 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01824"></a>01824 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01825"></a>01825       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l01826"></a>01826                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01827"></a>01827                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01828"></a>01828                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01829"></a>01829     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01830"></a>01830       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l01831"></a>01831                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01832"></a>01832                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01833"></a>01833                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01834"></a>01834 <span class="preprocessor">#endif</span>
<a name="l01835"></a>01835 <span class="preprocessor"></span>
<a name="l01836"></a>01836     <span class="keywordflow">if</span> (i &lt; j)
<a name="l01837"></a>01837       <span class="keywordflow">return</span> this-&gt;val_(i)(j);
<a name="l01838"></a>01838 
<a name="l01839"></a>01839     <span class="keywordflow">return</span> this-&gt;val_(j)(i);
<a name="l01840"></a>01840   }
<a name="l01841"></a>01841 
<a name="l01842"></a>01842 
<a name="l01844"></a>01844 
<a name="l01850"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a6be1645f7e70dd978da6edba42969b8a">01850</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01851"></a>01851   <span class="keyword">inline</span> T&amp;
<a name="l01852"></a>01852   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01853"></a>01853   {
<a name="l01854"></a>01854 
<a name="l01855"></a>01855 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01856"></a>01856 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01857"></a>01857       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01858"></a>01858                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01859"></a>01859                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01860"></a>01860     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01861"></a>01861       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01862"></a>01862                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01863"></a>01863                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01864"></a>01864 <span class="preprocessor">#endif</span>
<a name="l01865"></a>01865 <span class="preprocessor"></span>
<a name="l01866"></a>01866     <span class="keywordflow">if</span> (i &lt; j)
<a name="l01867"></a>01867       <span class="keywordflow">return</span> this-&gt;val_(i).Get(j);
<a name="l01868"></a>01868 
<a name="l01869"></a>01869     <span class="keywordflow">return</span> this-&gt;val_(j).Get(i);
<a name="l01870"></a>01870   }
<a name="l01871"></a>01871 
<a name="l01872"></a>01872 
<a name="l01874"></a>01874 
<a name="l01880"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#ab55cfb6520eb9645918c7e0a161b1f0d">01880</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01881"></a>01881   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp;
<a name="l01882"></a>01882   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l01883"></a>01883 <span class="keyword">  </span>{
<a name="l01884"></a>01884 
<a name="l01885"></a>01885 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01886"></a>01886 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01887"></a>01887       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01888"></a>01888                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01889"></a>01889                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01890"></a>01890     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01891"></a>01891       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01892"></a>01892                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01893"></a>01893                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01894"></a>01894 <span class="preprocessor">#endif</span>
<a name="l01895"></a>01895 <span class="preprocessor"></span>
<a name="l01896"></a>01896     <span class="keywordflow">if</span> (i &lt; j)
<a name="l01897"></a>01897       <span class="keywordflow">return</span> this-&gt;val_(i).Get(j);
<a name="l01898"></a>01898 
<a name="l01899"></a>01899     <span class="keywordflow">return</span> this-&gt;val_(j).Get(i);
<a name="l01900"></a>01900   }
<a name="l01901"></a>01901 
<a name="l01902"></a>01902 
<a name="l01904"></a>01904 
<a name="l01910"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#ae0e5492ee09498fcd376ec76469c1034">01910</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01911"></a>01911   <span class="keyword">inline</span> T&amp;
<a name="l01912"></a>01912   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01913"></a>01913   {
<a name="l01914"></a>01914 
<a name="l01915"></a>01915 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01916"></a>01916 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01917"></a>01917       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01918"></a>01918                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01919"></a>01919                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01920"></a>01920     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01921"></a>01921       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01922"></a>01922                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01923"></a>01923                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01924"></a>01924     <span class="keywordflow">if</span> (i &gt; j)
<a name="l01925"></a>01925       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix::Val()&quot;</span>, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;With this function, you &quot;</span>)
<a name="l01926"></a>01926                           + <span class="stringliteral">&quot;can only access upper part of matrix.&quot;</span>);
<a name="l01927"></a>01927 <span class="preprocessor">#endif</span>
<a name="l01928"></a>01928 <span class="preprocessor"></span>
<a name="l01929"></a>01929     <span class="keywordflow">return</span> this-&gt;val_(i).Val(j);
<a name="l01930"></a>01930   }
<a name="l01931"></a>01931 
<a name="l01932"></a>01932 
<a name="l01934"></a>01934 
<a name="l01940"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a97036f8d9ca0c12cf1bb95d8aafce4a0">01940</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01941"></a>01941   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp;
<a name="l01942"></a>01942   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l01943"></a>01943 <span class="keyword">  </span>{
<a name="l01944"></a>01944 
<a name="l01945"></a>01945 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01946"></a>01946 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01947"></a>01947       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01948"></a>01948                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01949"></a>01949                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01950"></a>01950     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01951"></a>01951       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01952"></a>01952                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01953"></a>01953                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01954"></a>01954     <span class="keywordflow">if</span> (i &gt; j)
<a name="l01955"></a>01955       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix::Val()&quot;</span>, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;With this function, you &quot;</span>)
<a name="l01956"></a>01956                           + <span class="stringliteral">&quot;can only access upper part of matrix.&quot;</span>);
<a name="l01957"></a>01957 <span class="preprocessor">#endif</span>
<a name="l01958"></a>01958 <span class="preprocessor"></span>
<a name="l01959"></a>01959     <span class="keywordflow">return</span> this-&gt;val_(i).Val(j);
<a name="l01960"></a>01960   }
<a name="l01961"></a>01961 
<a name="l01962"></a>01962 
<a name="l01964"></a>01964 
<a name="l01969"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#ac3340bdc4a5377e34c3e7e3b3ef5fb86">01969</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01970"></a>01970   <span class="keyword">inline</span> <span class="keywordtype">void</span>
<a name="l01971"></a>01971   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::Set</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; x)
<a name="l01972"></a>01972   {
<a name="l01973"></a>01973     <span class="keywordflow">if</span> (i &lt; j)
<a name="l01974"></a>01974       this-&gt;val_(i).Get(j) = x;
<a name="l01975"></a>01975     <span class="keywordflow">else</span>
<a name="l01976"></a>01976       this-&gt;val_(j).Get(i) = x;
<a name="l01977"></a>01977   }
<a name="l01978"></a>01978 
<a name="l01979"></a>01979 
<a name="l01981"></a>01981   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01982"></a>01982   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::ClearRow</a>(<span class="keywordtype">int</span> i)
<a name="l01983"></a>01983   {
<a name="l01984"></a>01984     this-&gt;val_(i).Clear();
<a name="l01985"></a>01985   }
<a name="l01986"></a>01986 
<a name="l01987"></a>01987 
<a name="l01989"></a>01989 
<a name="l01994"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a02d5086adf71d37c259c1a94a375bd57">01994</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01995"></a>01995   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::</a>
<a name="l01996"></a>01996 <a class="code" href="class_seldon_1_1_matrix.php">  ReallocateRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01997"></a>01997   {
<a name="l01998"></a>01998     this-&gt;val_(i).Reallocate(j);
<a name="l01999"></a>01999   }
<a name="l02000"></a>02000 
<a name="l02001"></a>02001 
<a name="l02003"></a>02003 
<a name="l02007"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#ad70cf7b6b6aadd9c1e51bc1944550d91">02007</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02008"></a>02008   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::</a>
<a name="l02009"></a>02009 <a class="code" href="class_seldon_1_1_matrix.php">  ResizeRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l02010"></a>02010   {
<a name="l02011"></a>02011     this-&gt;val_(i).Resize(j);
<a name="l02012"></a>02012   }
<a name="l02013"></a>02013 
<a name="l02014"></a>02014 
<a name="l02016"></a>02016 
<a name="l02020"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#abb74a0467a0295eb8964ac30c9e4922c">02020</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02021"></a>02021   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::</a>
<a name="l02022"></a>02022 <a class="code" href="class_seldon_1_1_matrix.php">  SwapRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l02023"></a>02023   {
<a name="l02024"></a>02024     Swap(this-&gt;val_(i), this-&gt;val_(j));
<a name="l02025"></a>02025   }
<a name="l02026"></a>02026 
<a name="l02027"></a>02027 
<a name="l02029"></a>02029 
<a name="l02033"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#af70fc4f3c8267b2a4f528e1fd3056020">02033</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02034"></a>02034   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::</a>
<a name="l02035"></a>02035 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceIndexRow</a>(<span class="keywordtype">int</span> i,<a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l02036"></a>02036   {
<a name="l02037"></a>02037     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_(i).GetM(); j++)
<a name="l02038"></a>02038       this-&gt;val_(i).Index(j) = new_index(j);
<a name="l02039"></a>02039   }
<a name="l02040"></a>02040 
<a name="l02041"></a>02041 
<a name="l02043"></a>02043 
<a name="l02047"></a>02047   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02048"></a>02048   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::GetRowSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"></span>
<a name="l02049"></a>02049 <span class="keyword">    const</span>
<a name="l02050"></a>02050 <span class="keyword">  </span>{
<a name="l02051"></a>02051     <span class="keywordflow">return</span> this-&gt;val_(i).GetSize();
<a name="l02052"></a>02052   }
<a name="l02053"></a>02053 
<a name="l02054"></a>02054 
<a name="l02056"></a>02056   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02057"></a>02057   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a10ca8f311170cfa8b11f6caa63d42635" title="Displays non-zero values of a column.">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::PrintRow</a>(<span class="keywordtype">int</span> i)<span class="keyword"></span>
<a name="l02058"></a>02058 <span class="keyword">    const</span>
<a name="l02059"></a>02059 <span class="keyword">  </span>{
<a name="l02060"></a>02060     this-&gt;val_(i).Print();
<a name="l02061"></a>02061   }
<a name="l02062"></a>02062 
<a name="l02063"></a>02063 
<a name="l02065"></a>02065 
<a name="l02070"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a94a7c41aa9fb70bc83fbadb66e0e7709">02070</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02071"></a>02071   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;</a>
<a name="l02072"></a>02072 <a class="code" href="class_seldon_1_1_matrix.php">  ::AssembleRow</a>(<span class="keywordtype">int</span> i)
<a name="l02073"></a>02073   {
<a name="l02074"></a>02074     this-&gt;val_(i).Assemble();
<a name="l02075"></a>02075   }
<a name="l02076"></a>02076 
<a name="l02077"></a>02077 
<a name="l02079"></a>02079 
<a name="l02084"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#ac8bf415466c0dd580e635a66ae4110a6">02084</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02085"></a>02085   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::</a>
<a name="l02086"></a>02086 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; val)
<a name="l02087"></a>02087   {
<a name="l02088"></a>02088     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l02089"></a>02089       this-&gt;val_(i).AddInteraction(j, val);
<a name="l02090"></a>02090   }
<a name="l02091"></a>02091 
<a name="l02092"></a>02092 
<a name="l02094"></a>02094 
<a name="l02100"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a3c0b4b2b2f82744e655eb119080c309f">02100</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02101"></a>02101   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::</a>
<a name="l02102"></a>02102 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keywordtype">int</span>* col_, T* value_)
<a name="l02103"></a>02103   {
<a name="l02104"></a>02104     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> col;
<a name="l02105"></a>02105     col.SetData(nb, col_);
<a name="l02106"></a>02106     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val;
<a name="l02107"></a>02107     val.SetData(nb, value_);
<a name="l02108"></a>02108     AddInteractionRow(i, nb, col, val);
<a name="l02109"></a>02109     col.Nullify();
<a name="l02110"></a>02110     val.Nullify();
<a name="l02111"></a>02111   }
<a name="l02112"></a>02112 
<a name="l02113"></a>02113 
<a name="l02115"></a>02115 
<a name="l02121"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#ab4d1a9abf1e07714f5652eab9862475c">02121</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02122"></a>02122   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::</a>
<a name="l02123"></a>02123 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keywordtype">int</span>* row_, T* value_)
<a name="l02124"></a>02124   {
<a name="l02125"></a>02125     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> row;
<a name="l02126"></a>02126     row.SetData(nb, row_);
<a name="l02127"></a>02127     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val;
<a name="l02128"></a>02128     val.SetData(nb, value_);
<a name="l02129"></a>02129     AddInteractionColumn(i, nb, row, val);
<a name="l02130"></a>02130     row.Nullify();
<a name="l02131"></a>02131     val.Nullify();
<a name="l02132"></a>02132   }
<a name="l02133"></a>02133 
<a name="l02134"></a>02134 
<a name="l02136"></a>02136 
<a name="l02142"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#af99cb559be02dcf98137b4dc5c4b29d1">02142</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l02143"></a>02143   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::</a>
<a name="l02144"></a>02144 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l02145"></a>02145                     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val)
<a name="l02146"></a>02146   {
<a name="l02147"></a>02147     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> new_col(nb);
<a name="l02148"></a>02148     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a> new_val(nb);
<a name="l02149"></a>02149     nb = 0;
<a name="l02150"></a>02150     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; new_col.GetM(); j++)
<a name="l02151"></a>02151       <span class="keywordflow">if</span> (i &lt;= col(j))
<a name="l02152"></a>02152         {
<a name="l02153"></a>02153           new_col(nb) = col(j);
<a name="l02154"></a>02154           new_val(nb) = val(j); nb++;
<a name="l02155"></a>02155         }
<a name="l02156"></a>02156 
<a name="l02157"></a>02157     this-&gt;val_(i).AddInteractionRow(nb, new_col, new_val);
<a name="l02158"></a>02158   }
<a name="l02159"></a>02159 
<a name="l02160"></a>02160 
<a name="l02162"></a>02162 
<a name="l02168"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a8f6797869ae81e18c8c81aa00fbcdf82">02168</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l02169"></a>02169   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::</a>
<a name="l02170"></a>02170 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l02171"></a>02171                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T,VectFull,Alloc1&gt;</a>&amp; val)
<a name="l02172"></a>02172   {
<a name="l02173"></a>02173     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nb; j++)
<a name="l02174"></a>02174       <span class="keywordflow">if</span> (row(j) &lt;= i)
<a name="l02175"></a>02175         this-&gt;val_(row(j)).AddInteraction(i, val(j));
<a name="l02176"></a>02176   }
<a name="l02177"></a>02177 
<a name="l02178"></a>02178 
<a name="l02179"></a>02179   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02180"></a>02180   ostream&amp; <a class="code" href="namespace_seldon.php#a5bd4a6d6f3c83048663d57d2fc032107" title="operator&amp;lt;&amp;lt; overloaded for a 3D array.">operator &lt;&lt;</a>(ostream&amp; out,
<a name="l02181"></a>02181                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php" title="Row-major sparse-matrix class.">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;</a>&amp; A)
<a name="l02182"></a>02182   {
<a name="l02183"></a>02183     A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#adf0d3ec9d5ffeb1b3d46f523051d1412" title="Writes the matrix in a file.">WriteText</a>(out);
<a name="l02184"></a>02184 
<a name="l02185"></a>02185     <span class="keywordflow">return</span> out;
<a name="l02186"></a>02186   }
<a name="l02187"></a>02187 
<a name="l02188"></a>02188 
<a name="l02189"></a>02189   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02190"></a>02190   ostream&amp; <a class="code" href="namespace_seldon.php#a5bd4a6d6f3c83048663d57d2fc032107" title="operator&amp;lt;&amp;lt; overloaded for a 3D array.">operator &lt;&lt;</a>(ostream&amp; out,
<a name="l02191"></a>02191                        <span class="keyword">const</span> Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;&amp; A)
<a name="l02192"></a>02192   {
<a name="l02193"></a>02193     A.WriteText(out);
<a name="l02194"></a>02194 
<a name="l02195"></a>02195     <span class="keywordflow">return</span> out;
<a name="l02196"></a>02196   }
<a name="l02197"></a>02197 
<a name="l02198"></a>02198 
<a name="l02199"></a>02199   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02200"></a>02200   ostream&amp; <a class="code" href="namespace_seldon.php#a5bd4a6d6f3c83048663d57d2fc032107" title="operator&amp;lt;&amp;lt; overloaded for a 3D array.">operator &lt;&lt;</a>(ostream&amp; out,
<a name="l02201"></a>02201                        <span class="keyword">const</span> Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;&amp; A)
<a name="l02202"></a>02202   {
<a name="l02203"></a>02203     A.WriteText(out);
<a name="l02204"></a>02204 
<a name="l02205"></a>02205     <span class="keywordflow">return</span> out;
<a name="l02206"></a>02206   }
<a name="l02207"></a>02207 
<a name="l02208"></a>02208 
<a name="l02209"></a>02209   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02210"></a>02210   ostream&amp; <a class="code" href="namespace_seldon.php#a5bd4a6d6f3c83048663d57d2fc032107" title="operator&amp;lt;&amp;lt; overloaded for a 3D array.">operator &lt;&lt;</a>(ostream&amp; out,
<a name="l02211"></a>02211                        <span class="keyword">const</span> Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;&amp; A)
<a name="l02212"></a>02212   {
<a name="l02213"></a>02213     A.WriteText(out);
<a name="l02214"></a>02214 
<a name="l02215"></a>02215     <span class="keywordflow">return</span> out;
<a name="l02216"></a>02216   }
<a name="l02217"></a>02217 
<a name="l02218"></a>02218 
<a name="l02219"></a>02219 } <span class="comment">// namespace Seldon</span>
<a name="l02220"></a>02220 
<a name="l02221"></a>02221 <span class="preprocessor">#define SELDON_FILE_MATRIX_ARRAY_SPARSE_CXX</span>
<a name="l02222"></a>02222 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
