<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Class List</h1>  </div>
</div>
<div class="contents">
Here are the classes, structs, unions and interfaces with brief descriptions:<table>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1__object.php">seldon::_object</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td class="indexvalue">3D array </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_array_col_complex_sparse.php">Seldon::ArrayColComplexSparse</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_array_col_sparse.php">Seldon::ArrayColSparse</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">Seldon::ArrayColSymComplexSparse</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_array_col_sym_sparse.php">Seldon::ArrayColSymSparse</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_array_row_complex_sparse.php">Seldon::ArrayRowComplexSparse</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_array_row_sparse.php">Seldon::ArrayRowSparse</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">Seldon::ArrayRowSymComplexSparse</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_array_row_sym_sparse.php">Seldon::ArrayRowSymSparse</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_base_matrix_sparse_double.php">seldon::BaseMatrixSparseDouble</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_base_seldon_vector_double.php">seldon::BaseSeldonVectorDouble</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_base_seldon_vector_int.php">seldon::BaseSeldonVectorInt</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_calloc_alloc.php">Seldon::CallocAlloc&lt; T &gt;</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1class___seldon_conj_trans.php">Seldon::class_SeldonConjTrans</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1class___seldon_left.php">Seldon::class_SeldonLeft</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1class___seldon_non_unit.php">Seldon::class_SeldonNonUnit</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1class___seldon_no_trans.php">Seldon::class_SeldonNoTrans</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1class___seldon_right.php">Seldon::class_SeldonRight</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1class___seldon_trans.php">Seldon::class_SeldonTrans</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1class___seldon_unit.php">Seldon::class_SeldonUnit</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_col_complex_sparse.php">Seldon::ColComplexSparse</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_col_herm.php">Seldon::ColHerm</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_col_herm_packed.php">Seldon::ColHermPacked</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_collection.php">Seldon::Collection</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_col_lo_triang.php">Seldon::ColLoTriang</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_col_lo_triang_packed.php">Seldon::ColLoTriangPacked</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_col_major.php">Seldon::ColMajor</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_col_major_collection.php">Seldon::ColMajorCollection</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_col_major_collection.php">seldon::ColMajorCollection</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_col_sparse.php">Seldon::ColSparse</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_col_sym.php">Seldon::ColSym</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_col_sym_complex_sparse.php">Seldon::ColSymComplexSparse</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_col_sym_packed.php">Seldon::ColSymPacked</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_col_sym_packed_collection.php">Seldon::ColSymPackedCollection</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_col_sym_packed_collection.php">seldon::ColSymPackedCollection</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_col_sym_sparse.php">Seldon::ColSymSparse</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_col_up_triang.php">Seldon::ColUpTriang</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_col_up_triang_packed.php">Seldon::ColUpTriangPacked</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_col_up_triang_packed_collection.php">Seldon::ColUpTriangPackedCollection</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_col_up_triang_packed_collection.php">seldon::ColUpTriangPackedCollection</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_dense_sparse_collection.php">Seldon::DenseSparseCollection</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_double_malloc.php">seldon::DoubleMalloc</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_error.php">Seldon::Error</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_error.php">seldon::Error</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_float_double.php">Seldon::FloatDouble</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_float_double.php">seldon::FloatDouble</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_general.php">Seldon::General</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_general.php">seldon::General</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a></td><td class="indexvalue"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> class made of an heterogeneous collection of matrices </td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1ifstream.php">seldon::ifstream</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_ilut_preconditioning.php">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_int_malloc.php">seldon::IntMalloc</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_i_o_error.php">Seldon::IOError</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_i_o_error.php">seldon::IOError</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1iostream.php">seldon::iostream</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1istream.php">seldon::istream</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td class="indexvalue">Class containing parameters for an iterative resolution </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_lapack_error.php">Seldon::LapackError</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_lapack_error.php">seldon::LapackError</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_lapack_info.php">Seldon::LapackInfo</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_lapack_info.php">seldon::LapackInfo</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_malloc_alloc.php">Seldon::MallocAlloc&lt; T &gt;</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_malloc_object.php">Seldon::MallocObject&lt; T &gt;</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix&lt; T, Prop, Storage, Allocator &gt;</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_float_double_00_01_general_00_01_dense_sparse_collection_00_01_allocator_3_01double_01_4_01_4.php">Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;</a></td><td class="indexvalue">Heterogeneous matrix collection class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayColComplexSparse, Allocator &gt;</a></td><td class="indexvalue">Column-major sparse-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayColSparse, Allocator &gt;</a></td><td class="indexvalue">Column-major sparse-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a></td><td class="indexvalue">Column-major symmetric sparse-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayColSymSparse, Allocator &gt;</a></td><td class="indexvalue">Column-major symmetric sparse-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td class="indexvalue">Row-major sparse-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td class="indexvalue">Row-major sparse-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a></td><td class="indexvalue">Row-major symmetric sparse-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</a></td><td class="indexvalue">Row-major symmetric sparse-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColComplexSparse, Allocator &gt;</a></td><td class="indexvalue">Column-major sparse-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColHerm, Allocator &gt;</a></td><td class="indexvalue">Column-major hermitian full-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td class="indexvalue">Column-major hermitian packed matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColLoTriang, Allocator &gt;</a></td><td class="indexvalue">Column-major lower-triangular full-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColLoTriangPacked, Allocator &gt;</a></td><td class="indexvalue">Column-major lower-triangular packed matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColMajor, Allocator &gt;</a></td><td class="indexvalue">Column-major full-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_collection_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColMajorCollection, Allocator &gt;</a></td><td class="indexvalue">Column-major matrix collection class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSparse, Allocator &gt;</a></td><td class="indexvalue">Column-major sparse-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt;</a></td><td class="indexvalue">Column-major symmetric full-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSymComplexSparse, Allocator &gt;</a></td><td class="indexvalue">Column-major complex sparse-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSymPacked, Allocator &gt;</a></td><td class="indexvalue">Column-major symmetric packed matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_packed_collection_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSymPackedCollection, Allocator &gt;</a></td><td class="indexvalue">Symetric column-major matrix collection class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSymSparse, Allocator &gt;</a></td><td class="indexvalue">Column-major sparse-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColUpTriang, Allocator &gt;</a></td><td class="indexvalue">Column-major upper-triangular full-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColUpTriangPacked, Allocator &gt;</a></td><td class="indexvalue">Column-major upper-triangular packed matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_seq_dense_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScSeqDense, Allocator &gt;</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowComplexSparse, Allocator &gt;</a></td><td class="indexvalue">Row-major sparse-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowHerm, Allocator &gt;</a></td><td class="indexvalue">Row-major hermitian full-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowHermPacked, Allocator &gt;</a></td><td class="indexvalue">Row-major hermitian packed matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowLoTriang, Allocator &gt;</a></td><td class="indexvalue">Row-major lower-triangular full-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td class="indexvalue">Row-major lower-triangular packed matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td class="indexvalue">Row-major full-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_collection_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowMajorCollection, Allocator &gt;</a></td><td class="indexvalue">Row-major matrix collection class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSparse, Allocator &gt;</a></td><td class="indexvalue">Row-major sparse-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSym, Allocator &gt;</a></td><td class="indexvalue">Row-major symmetric full-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td class="indexvalue">Row-major complex sparse-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymPacked, Allocator &gt;</a></td><td class="indexvalue">Row-major symmetric packed matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_collection_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;</a></td><td class="indexvalue">Symetric row-major matrix collection class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td class="indexvalue">Row-major sparse-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td class="indexvalue">Row-major upper-triangular full-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowUpTriangPacked, Allocator &gt;</a></td><td class="indexvalue">Row-major upper-triangular packed matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_sub_storage_3_01_m_01_4_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, SubStorage&lt; M &gt;, Allocator &gt;</a></td><td class="indexvalue">Sub-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td class="indexvalue">Sparse Array-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td class="indexvalue">Sparse Array-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td class="indexvalue">Base class for all matrices </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td class="indexvalue">Complex sparse-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix___hermitian.php">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a></td><td class="indexvalue">Hermitian matrix stored in a full matrix </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td class="indexvalue">Hermitian packed matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td class="indexvalue">Full matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td class="indexvalue">Sparse-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td class="indexvalue"><a class="el" href="class_seldon_1_1_symmetric.php">Symmetric</a> complex sparse-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a></td><td class="indexvalue"><a class="el" href="class_seldon_1_1_symmetric.php">Symmetric</a> matrix stored in a full matrix </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td class="indexvalue"><a class="el" href="class_seldon_1_1_symmetric.php">Symmetric</a> packed matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td class="indexvalue"><a class="el" href="class_seldon_1_1_symmetric.php">Symmetric</a> sparse-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td class="indexvalue">Triangular packed matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a></td><td class="indexvalue">Triangular matrix stored in a full matrix </td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_matrix_base_double.php">seldon::MatrixBaseDouble</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_matrix_base_int.php">seldon::MatrixBaseInt</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_cholmod.php">Seldon::MatrixCholmod</a></td><td class="indexvalue">Object containing Cholesky factorization </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a></td><td class="indexvalue"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> class made of a collection of matrices </td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_matrix_double.php">seldon::MatrixDouble</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_matrix_int.php">seldon::MatrixInt</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td class="indexvalue">Object used to solve linear system by calling mumps subroutines </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_matrix_pointers_double.php">seldon::MatrixPointersDouble</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_matrix_pointers_int.php">seldon::MatrixPointersInt</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_matrix_sparse_double.php">seldon::MatrixSparseDouble</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_super_l_u.php">Seldon::MatrixSuperLU&lt; T &gt;</a></td><td class="indexvalue">Empty matrix </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_super_l_u_3_01complex_3_01double_01_4_01_4.php">Seldon::MatrixSuperLU&lt; complex&lt; double &gt; &gt;</a></td><td class="indexvalue">Class interfacing SuperLU functions in complex double precision </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_super_l_u_3_01double_01_4.php">Seldon::MatrixSuperLU&lt; double &gt;</a></td><td class="indexvalue">Class interfacing SuperLU functions in double precision </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; T &gt;</a></td><td class="indexvalue">Class interfacing SuperLU functions </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_umf_pack.php">Seldon::MatrixUmfPack&lt; T &gt;</a></td><td class="indexvalue">Empty class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php">Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;</a></td><td class="indexvalue">Class to solve linear system in complex double precision with UmfPack </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php">Seldon::MatrixUmfPack&lt; double &gt;</a></td><td class="indexvalue">Class to solve linear system in double precision with UmfPack </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_matrix_umf_pack___base.php">Seldon::MatrixUmfPack_Base&lt; T &gt;</a></td><td class="indexvalue">&lt; base class to solve linear system by using UmfPack </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_na_n_alloc.php">Seldon::NaNAlloc&lt; T &gt;</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_new_alloc.php">Seldon::NewAlloc&lt; T &gt;</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td class="indexvalue">NLopt optimization </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_no_memory.php">seldon::NoMemory</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1ofstream.php">seldon::ofstream</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1ostream.php">seldon::ostream</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, Storage, Allocator &gt;</a></td><td class="indexvalue"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> class based on PETSc matrix </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_a_i_j.php">Seldon::PETScMPIAIJ</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">Seldon::PETScMPIDense</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">Seldon::PETScPar</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_p_e_t_sc_seq.php">Seldon::PETScSeq</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_p_e_t_sc_seq_dense.php">Seldon::PETScSeqDense</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_preconditioner___base.php">Seldon::Preconditioner_Base</a></td><td class="indexvalue">Base class for preconditioners </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_row_complex_sparse.php">Seldon::RowComplexSparse</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_row_herm.php">Seldon::RowHerm</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_row_herm_packed.php">Seldon::RowHermPacked</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_row_lo_triang.php">Seldon::RowLoTriang</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_row_lo_triang_packed.php">Seldon::RowLoTriangPacked</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_row_major.php">Seldon::RowMajor</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_row_major.php">seldon::RowMajor</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_row_major_collection.php">seldon::RowMajorCollection</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_row_major_collection.php">Seldon::RowMajorCollection</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_row_sparse.php">Seldon::RowSparse</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_row_sparse.php">seldon::RowSparse</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_row_sym.php">Seldon::RowSym</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_row_sym_complex_sparse.php">Seldon::RowSymComplexSparse</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_row_sym_packed.php">Seldon::RowSymPacked</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_row_sym_packed_collection.php">Seldon::RowSymPackedCollection</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_row_sym_packed_collection.php">seldon::RowSymPackedCollection</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_row_sym_sparse.php">Seldon::RowSymSparse</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_row_up_triang.php">Seldon::RowUpTriang</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_row_up_triang_packed.php">Seldon::RowUpTriangPacked</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_row_up_triang_packed_collection.php">Seldon::RowUpTriangPackedCollection</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_row_up_triang_packed_collection.php">seldon::RowUpTriangPackedCollection</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_seldon_conjugate.php">Seldon::SeldonConjugate</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_seldon_diag.php">Seldon::SeldonDiag</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_seldon_norm.php">Seldon::SeldonNorm</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_seldon_side.php">Seldon::SeldonSide</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_seldon_uplo.php">Seldon::SeldonUplo</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_sor_preconditioner.php">Seldon::SorPreconditioner&lt; T &gt;</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td class="indexvalue">Class grouping different Cholesky solvers </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php">Seldon::SparseDirectSolver&lt; T &gt;</a></td><td class="indexvalue">Class grouping different direct solvers </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_sparse_matrix_ordering.php">Seldon::SparseMatrixOrdering</a></td><td class="indexvalue">Basic class grouping different ordering strategies </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_sparse_seldon_solver.php">Seldon::SparseSeldonSolver&lt; T, Allocator &gt;</a></td><td class="indexvalue">Default solver in <a class="el" href="namespace_seldon.php" title="Seldon namespace.">Seldon</a> </td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_str.php">seldon::Str</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_str.php">Seldon::Str</a></td><td class="indexvalue">This class helps formatting C++ strings on the fly </td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1string.php">seldon::string</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_sub_matrix.php">Seldon::SubMatrix&lt; M &gt;</a></td><td class="indexvalue">Sub-matrix class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_sub_matrix___base.php">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a></td><td class="indexvalue">Sub-matrix base class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_sub_storage.php">Seldon::SubStorage&lt; M &gt;</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_swig_py_iterator.php">seldon::SwigPyIterator</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_symmetric.php">Seldon::Symmetric</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_symmetric.php">seldon::Symmetric</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_type_mumps.php">Seldon::TypeMumps&lt; T &gt;</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_type_mumps_3_01complex_3_01double_01_4_01_4.php">Seldon::TypeMumps&lt; complex&lt; double &gt; &gt;</a></td><td class="indexvalue">Class containing MUMPS data structure </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_type_mumps_3_01double_01_4.php">Seldon::TypeMumps&lt; double &gt;</a></td><td class="indexvalue">Class containing MUMPS data structure </td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_undefined.php">seldon::Undefined</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_undefined.php">Seldon::Undefined</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_vect_full.php">Seldon::VectFull</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector&lt; T, Storage, Allocator &gt;</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td class="indexvalue">Vector of vectors </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td class="indexvalue">Vector of vectors of vectors </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td class="indexvalue">Structure for distributed vectors </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php">Seldon::Vector&lt; T, Collection, Allocator &gt;</a></td><td class="indexvalue">Structure for distributed vectors </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScPar, Allocator &gt;</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td class="indexvalue">Full vector class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td class="indexvalue">Sparse vector class </td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td class="indexvalue">Base structure for all vectors </td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_vector_double.php">seldon::VectorDouble</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_vector_int.php">seldon::VectorInt</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_vector_sparse_double.php">seldon::VectorSparseDouble</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_vect_sparse.php">Seldon::VectSparse</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_wrong_argument.php">Seldon::WrongArgument</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_wrong_argument.php">seldon::WrongArgument</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_wrong_col.php">seldon::WrongCol</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_wrong_col.php">Seldon::WrongCol</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_wrong_dim.php">Seldon::WrongDim</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_wrong_dim.php">seldon::WrongDim</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_wrong_index.php">Seldon::WrongIndex</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_wrong_index.php">seldon::WrongIndex</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="classseldon_1_1_wrong_row.php">seldon::WrongRow</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><a class="el" href="class_seldon_1_1_wrong_row.php">Seldon::WrongRow</a></td><td class="indexvalue"></td></tr>
</table>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
