<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix/HeterogeneousMatrixCollection.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2010 INRIA</span>
<a name="l00002"></a>00002 <span class="comment">// Author(s): Marc Fragu</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_HETEROGENEOUS_MATRIX_COLLECTION_HXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 
<a name="l00024"></a>00024 <span class="preprocessor">#include &quot;../share/Common.hxx&quot;</span>
<a name="l00025"></a>00025 <span class="preprocessor">#include &quot;../share/Properties.hxx&quot;</span>
<a name="l00026"></a>00026 <span class="preprocessor">#include &quot;../share/Storage.hxx&quot;</span>
<a name="l00027"></a>00027 <span class="preprocessor">#include &quot;../share/Errors.hxx&quot;</span>
<a name="l00028"></a>00028 <span class="preprocessor">#include &quot;../share/Allocator.hxx&quot;</span>
<a name="l00029"></a>00029 
<a name="l00030"></a>00030 <span class="keyword">namespace </span>Seldon
<a name="l00031"></a>00031 {
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00035"></a>00035 
<a name="l00039"></a>00039   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00040"></a>00040             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00041"></a>00041             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt;
<a name="l00042"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php">00042</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection</a>:
<a name="l00043"></a>00043     <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;double, Allocator&lt;double&gt; &gt;
<a name="l00044"></a>00044   {
<a name="l00045"></a>00045 
<a name="l00046"></a>00046     <span class="comment">// Typedef declarations.</span>
<a name="l00047"></a>00047   <span class="keyword">public</span>:
<a name="l00048"></a>00048     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;float, Prop0, Storage0, Allocator&lt;float&gt;</a> &gt; <a class="code" href="class_seldon_1_1_matrix.php">float_dense_m</a>;
<a name="l00049"></a>00049     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;float, Prop1, Storage1, Allocator&lt;float&gt;</a> &gt; <a class="code" href="class_seldon_1_1_matrix.php">float_sparse_m</a>;
<a name="l00050"></a>00050     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, Prop0, Storage0, Allocator&lt;double&gt;</a> &gt;
<a name="l00051"></a>00051     <a class="code" href="class_seldon_1_1_matrix.php">double_dense_m</a>;
<a name="l00052"></a>00052     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, Prop1, Storage1, Allocator&lt;double&gt;</a> &gt;
<a name="l00053"></a>00053     <a class="code" href="class_seldon_1_1_matrix.php">double_sparse_m</a>;
<a name="l00054"></a>00054 
<a name="l00055"></a>00055     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;<a class="code" href="class_seldon_1_1_matrix.php">float_dense_m</a>, <a class="code" href="class_seldon_1_1_general.php">General</a>, <a class="code" href="class_seldon_1_1_row_major_collection.php">RowMajorCollection</a>,
<a name="l00056"></a>00056                    <a class="code" href="class_seldon_1_1_new_alloc.php">NewAlloc&lt;float_dense_m&gt;</a> &gt; <a class="code" href="class_seldon_1_1_matrix.php">float_dense_c</a>;
<a name="l00057"></a>00057     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;<a class="code" href="class_seldon_1_1_matrix.php">float_sparse_m</a>, <a class="code" href="class_seldon_1_1_general.php">General</a>, <a class="code" href="class_seldon_1_1_row_major_collection.php">RowMajorCollection</a>,
<a name="l00058"></a>00058                    <a class="code" href="class_seldon_1_1_new_alloc.php">NewAlloc&lt;float_sparse_m&gt;</a> &gt; <a class="code" href="class_seldon_1_1_matrix.php">float_sparse_c</a>;
<a name="l00059"></a>00059     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;<a class="code" href="class_seldon_1_1_matrix.php">double_dense_m</a>, <a class="code" href="class_seldon_1_1_general.php">General</a>, <a class="code" href="class_seldon_1_1_row_major_collection.php">RowMajorCollection</a>,
<a name="l00060"></a>00060                    <a class="code" href="class_seldon_1_1_new_alloc.php">NewAlloc&lt;double_dense_m&gt;</a> &gt; <a class="code" href="class_seldon_1_1_matrix.php">double_dense_c</a>;
<a name="l00061"></a>00061     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;<a class="code" href="class_seldon_1_1_matrix.php">double_sparse_m</a>, <a class="code" href="class_seldon_1_1_general.php">General</a>, <a class="code" href="class_seldon_1_1_row_major_collection.php">RowMajorCollection</a>,
<a name="l00062"></a>00062                    <a class="code" href="class_seldon_1_1_new_alloc.php">NewAlloc&lt;double_sparse_m&gt;</a> &gt; <a class="code" href="class_seldon_1_1_matrix.php">double_sparse_c</a>;
<a name="l00063"></a>00063 
<a name="l00064"></a>00064     <span class="comment">// Attributes.</span>
<a name="l00065"></a>00065   <span class="keyword">protected</span>:
<a name="l00067"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3137de2c1fe161a746dbde1f10c9128e">00067</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3137de2c1fe161a746dbde1f10c9128e" title="Number of non-zero elements.">nz_</a>;
<a name="l00069"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a67cb0eb87ea633caabecdc58714723ec">00069</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a67cb0eb87ea633caabecdc58714723ec" title="Number of rows of matrices.">Mmatrix_</a>;
<a name="l00071"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a37c185f5d163c0f8a32828828fde7252">00071</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a37c185f5d163c0f8a32828828fde7252" title="Number of columns of matrices.">Nmatrix_</a>;
<a name="l00073"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a48d1d3d9da13f7923a6356e49815fe66">00073</a>     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a48d1d3d9da13f7923a6356e49815fe66" title="Number of rows in the underlying matrices.">Mlocal_</a>;
<a name="l00075"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#af934beebf115742be0a29cb452017a7b">00075</a>     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#af934beebf115742be0a29cb452017a7b" title="Cumulative number of rows in the underlying matrices.">Mlocal_sum_</a>;
<a name="l00077"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a6946bf5a2c2a2dbaf307ce2ed1feea09">00077</a>     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a6946bf5a2c2a2dbaf307ce2ed1feea09" title="Number of columns in the underlying matrices.">Nlocal_</a>;
<a name="l00079"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a0d31be2a9c37790f5e6ec86ef556ac82">00079</a>     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a0d31be2a9c37790f5e6ec86ef556ac82" title="Cumulative number of columns in the underlying matrices.">Nlocal_sum_</a>;
<a name="l00080"></a>00080 
<a name="l00082"></a>00082 
<a name="l00088"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a068b8212aa98d09c129bb1952aeee722">00088</a>     <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;int, General, RowMajor, CallocAlloc&lt;int&gt;</a> &gt; <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a068b8212aa98d09c129bb1952aeee722" title="Type of the underlying matrices.">collection_</a>;
<a name="l00089"></a>00089 
<a name="l00091"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a9ecb39a2c309d9cec57f7e97b448771d">00091</a>     <a class="code" href="class_seldon_1_1_matrix.php">float_dense_c</a> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a9ecb39a2c309d9cec57f7e97b448771d" title="Pointers of the underlying float dense matrices.">float_dense_c_</a>;
<a name="l00093"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a66e2ad59a6007f1dd4be614a8ade4b85">00093</a>     <a class="code" href="class_seldon_1_1_matrix.php">float_sparse_c</a> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a66e2ad59a6007f1dd4be614a8ade4b85" title="Pointers of the underlying float sparse matrices.">float_sparse_c_</a>;
<a name="l00095"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aedc9e7d9e1d59170fce9cf501899b0f6">00095</a>     <a class="code" href="class_seldon_1_1_matrix.php">double_dense_c</a> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aedc9e7d9e1d59170fce9cf501899b0f6" title="Pointers of the underlying double dense matrices.">double_dense_c_</a>;
<a name="l00097"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a8743b791c26f3513a2e911812083e482">00097</a>     <a class="code" href="class_seldon_1_1_matrix.php">double_sparse_c</a> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a8743b791c26f3513a2e911812083e482" title="Pointers of the underlying double sparse matrices.">double_sparse_c_</a>;
<a name="l00098"></a>00098 
<a name="l00099"></a>00099 
<a name="l00100"></a>00100     <span class="comment">// Methods.</span>
<a name="l00101"></a>00101   <span class="keyword">public</span>:
<a name="l00102"></a>00102     <span class="comment">// Constructor.</span>
<a name="l00103"></a>00103     <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aa958b2bb3412cda25f49f24938294553" title="Default constructor.">HeterogeneousMatrixCollection</a>();
<a name="l00104"></a>00104     <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aa958b2bb3412cda25f49f24938294553" title="Default constructor.">HeterogeneousMatrixCollection</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00105"></a>00105     <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aa958b2bb3412cda25f49f24938294553" title="Default constructor.">HeterogeneousMatrixCollection</a>
<a name="l00106"></a>00106     (<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection</a>&lt;Prop0, Storage0, Prop1,
<a name="l00107"></a>00107      Storage1, Allocator&gt;&amp; A);
<a name="l00108"></a>00108 
<a name="l00109"></a>00109     <span class="comment">// Destructor.</span>
<a name="l00110"></a>00110     <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aeec9a8f4343637e860a9dc627d60ed89" title="Destructor.">~HeterogeneousMatrixCollection</a>();
<a name="l00111"></a>00111     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a0324574cd55a0799aa9746b36b9b20a1" title="Clears the matrix collection without releasing memory.">Clear</a>();
<a name="l00112"></a>00112     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3e48adb67f58b6355dc7ce39611585cc" title="Clears the matrix collection without releasing memory.">Nullify</a>();
<a name="l00113"></a>00113     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3e48adb67f58b6355dc7ce39611585cc" title="Clears the matrix collection without releasing memory.">Nullify</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00114"></a>00114     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a34264880bdeb3febb8c73b2cad0b4588" title="Deallocates underlying the matrices.">Deallocate</a>();
<a name="l00115"></a>00115 
<a name="l00116"></a>00116     <span class="comment">// Basic methods.</span>
<a name="l00117"></a>00117     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e" title="Returns the number of rows.">GetM</a>() <span class="keyword">const</span>;
<a name="l00118"></a>00118     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a38820efd26db52feaeb8998d2fd43daa" title="Returns the number of rows.">GetMmatrix</a>() <span class="keyword">const</span>;
<a name="l00119"></a>00119     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e" title="Returns the number of rows.">GetM</a>(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00120"></a>00120     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d" title="Returns the number of columns.">GetN</a>() <span class="keyword">const</span>;
<a name="l00121"></a>00121     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a6c59c77733659c863477c9a34fc927fe" title="Returns the number of columns.">GetNmatrix</a>() <span class="keyword">const</span>;
<a name="l00122"></a>00122     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d" title="Returns the number of columns.">GetN</a>(<span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00123"></a>00123     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8" title="Returns the number of elements stored in memory.">GetSize</a>() <span class="keyword">const</span>;
<a name="l00124"></a>00124     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a2a36130a12ef3b4e4a4971f5898ca0bd" title="Returns the number of elements stored in memory.">GetDataSize</a>() <span class="keyword">const</span>;
<a name="l00125"></a>00125     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a69858677f0823951f46671248091be2e" title="Returns the type of a given underlying matrix.">GetType</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00126"></a>00126 
<a name="l00127"></a>00127     <a class="code" href="class_seldon_1_1_matrix.php">float_dense_c</a>&amp; <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a9bd6c070c0738b3425e5e9b3e0118ec6" title="Returns the collection of float dense underlying matrices.">GetFloatDense</a>();
<a name="l00128"></a>00128     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">float_dense_c</a>&amp; <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a9bd6c070c0738b3425e5e9b3e0118ec6" title="Returns the collection of float dense underlying matrices.">GetFloatDense</a>() <span class="keyword">const</span>;
<a name="l00129"></a>00129     <a class="code" href="class_seldon_1_1_matrix.php">float_sparse_c</a>&amp; <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a69cf73dbc6d9055bc8ad28633de18f24" title="Returns the collection of float sparse underlying matrices.">GetFloatSparse</a>();
<a name="l00130"></a>00130     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">float_sparse_c</a>&amp; <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a69cf73dbc6d9055bc8ad28633de18f24" title="Returns the collection of float sparse underlying matrices.">GetFloatSparse</a>() <span class="keyword">const</span>;
<a name="l00131"></a>00131     <a class="code" href="class_seldon_1_1_matrix.php">double_dense_c</a>&amp; <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a289e9f762721738e42b9907450e72e9f" title="Returns the collection of double dense underlying matrices.">GetDoubleDense</a>();
<a name="l00132"></a>00132     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">double_dense_c</a>&amp; <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a289e9f762721738e42b9907450e72e9f" title="Returns the collection of double dense underlying matrices.">GetDoubleDense</a>() <span class="keyword">const</span>;
<a name="l00133"></a>00133     <a class="code" href="class_seldon_1_1_matrix.php">double_sparse_c</a>&amp; <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a39301f5774586f67d31f64a3d4acb0af" title="Returns the collection of double sparse underlying matrices.">GetDoubleSparse</a>();
<a name="l00134"></a>00134     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">double_sparse_c</a>&amp; <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a39301f5774586f67d31f64a3d4acb0af" title="Returns the collection of double sparse underlying matrices.">GetDoubleSparse</a>() <span class="keyword">const</span>;
<a name="l00135"></a>00135 
<a name="l00136"></a>00136     <span class="comment">// Memory management.</span>
<a name="l00137"></a>00137     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae600c754d08ba0af893d118cd3a38c60" title="Reallocates memory to resize the matrix collection.">Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00138"></a>00138 
<a name="l00139"></a>00139     <span class="comment">// Management of the matrices.</span>
<a name="l00140"></a>00140     <span class="keywordtype">void</span> SetMatrix(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">float_dense_m</a>&amp;);
<a name="l00141"></a>00141     <span class="keywordtype">void</span> SetMatrix(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">float_sparse_m</a>&amp;);
<a name="l00142"></a>00142     <span class="keywordtype">void</span> SetMatrix(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">double_dense_m</a>&amp;);
<a name="l00143"></a>00143     <span class="keywordtype">void</span> SetMatrix(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">double_sparse_m</a>&amp;);
<a name="l00144"></a>00144 
<a name="l00145"></a>00145     <span class="comment">// Element access and affectation.</span>
<a name="l00146"></a>00146     <span class="keywordtype">void</span> GetMatrix(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n, <a class="code" href="class_seldon_1_1_matrix.php">float_dense_m</a>&amp;) <span class="keyword">const</span>;
<a name="l00147"></a>00147     <span class="keywordtype">void</span> GetMatrix(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n, <a class="code" href="class_seldon_1_1_matrix.php">float_sparse_m</a>&amp;) <span class="keyword">const</span>;
<a name="l00148"></a>00148     <span class="keywordtype">void</span> GetMatrix(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n, <a class="code" href="class_seldon_1_1_matrix.php">double_dense_m</a>&amp;) <span class="keyword">const</span>;
<a name="l00149"></a>00149     <span class="keywordtype">void</span> GetMatrix(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n, <a class="code" href="class_seldon_1_1_matrix.php">double_sparse_m</a>&amp;) <span class="keyword">const</span>;
<a name="l00150"></a>00150 
<a name="l00151"></a>00151 
<a name="l00152"></a>00152     <span class="keywordtype">double</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#accc5a22477245a42cd0e017157f7d17d" title="Access operator.">operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00153"></a>00153 
<a name="l00154"></a>00154     <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection</a>&lt;Prop0, Storage0, Prop1,
<a name="l00155"></a>00155                                   Storage1, Allocator&gt;&amp;
<a name="l00156"></a>00156     <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a1f383b28bd0289fc1f00c9e06e597c9e" title="Duplicates a matrix collection (assignment operator).">operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection</a>&lt;Prop0, Storage0,
<a name="l00157"></a>00157                Prop1, Storage1, Allocator&gt;&amp; A);
<a name="l00158"></a>00158 
<a name="l00159"></a>00159     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a91100b6eb7a2cea5e86d191ee58749bf" title="Duplicates a matrix collection (assignment operator).">Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection</a>&lt;Prop0, Storage0,
<a name="l00160"></a>00160               Prop1, Storage1, Allocator&gt;&amp; A);
<a name="l00161"></a>00161 
<a name="l00162"></a>00162     <span class="comment">// Convenient functions.</span>
<a name="l00163"></a>00163     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a788108f4822afc37c6ef4054b37a03bc" title="Displays the matrix collection on the standard output.">Print</a>() <span class="keyword">const</span>;
<a name="l00164"></a>00164 
<a name="l00165"></a>00165     <span class="comment">// Input/output functions.</span>
<a name="l00166"></a>00166     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3d95d8a80c5fdd1d5aea9decf6d4e544" title="Writes the matrix collection in a file.">Write</a>(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size) <span class="keyword">const</span>;
<a name="l00167"></a>00167     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3d95d8a80c5fdd1d5aea9decf6d4e544" title="Writes the matrix collection in a file.">Write</a>(ostream&amp; FileStream, <span class="keywordtype">bool</span> with_size) <span class="keyword">const</span>;
<a name="l00168"></a>00168     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ac56a7ef93f6f0a217795ce3166b9edae" title="Writes the matrix collection in a file.">WriteText</a>(<span class="keywordtype">string</span> FileName) <span class="keyword">const</span>;
<a name="l00169"></a>00169     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ac56a7ef93f6f0a217795ce3166b9edae" title="Writes the matrix collection in a file.">WriteText</a>(ostream&amp; FileStream) <span class="keyword">const</span>;
<a name="l00170"></a>00170 
<a name="l00171"></a>00171     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a11761a3a59b566c4e2d4ae2e020d1b24" title="Reads the matrix collection from a file.">Read</a>(<span class="keywordtype">string</span> FileName);
<a name="l00172"></a>00172     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a11761a3a59b566c4e2d4ae2e020d1b24" title="Reads the matrix collection from a file.">Read</a>(istream&amp; FileStream);
<a name="l00173"></a>00173 
<a name="l00174"></a>00174   };
<a name="l00175"></a>00175 
<a name="l00176"></a>00176 
<a name="l00178"></a>00178   <span class="keyword">template</span> &lt;<span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt;
<a name="l00179"></a><a class="code" href="class_seldon_1_1_matrix_3_01_float_double_00_01_general_00_01_dense_sparse_collection_00_01_allocator_3_01double_01_4_01_4.php">00179</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;<a class="code" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="code" href="class_seldon_1_1_general.php">General</a>,
<a name="l00180"></a>00180                <a class="code" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt;double&gt; &gt;:
<a name="l00181"></a>00181     <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection</a>&lt;General, RowMajor, General,
<a name="l00182"></a>00182                                          RowSparse, Allocator &gt;
<a name="l00183"></a>00183   {
<a name="l00184"></a>00184     <span class="comment">// typedef declaration.</span>
<a name="l00185"></a>00185   <span class="keyword">public</span>:
<a name="l00186"></a>00186     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_general.php">General</a> <a class="code" href="class_seldon_1_1_general.php">property</a>;
<a name="l00187"></a>00187     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a> <a class="code" href="class_seldon_1_1_dense_sparse_collection.php">storage</a>;
<a name="l00188"></a>00188     <span class="keyword">typedef</span> Allocator&lt;double&gt; allocator;
<a name="l00189"></a>00189 
<a name="l00190"></a>00190   <span class="keyword">public</span>:
<a name="l00191"></a>00191     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>();
<a name="l00192"></a>00192     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00193"></a>00193   };
<a name="l00194"></a>00194 
<a name="l00195"></a>00195 
<a name="l00196"></a>00196 } <span class="comment">// namespace Seldon.</span>
<a name="l00197"></a>00197 
<a name="l00198"></a>00198 
<a name="l00199"></a>00199 <span class="preprocessor">#define SELDON_FILE_HETEROGENEOUS_MATRIX_COLLECTION_HXX</span>
<a name="l00200"></a>00200 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
