<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix_sparse/Matrix_Sparse.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_SPARSE_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;Matrix_Sparse.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="preprocessor">#include &quot;../vector/Functions_Arrays.cxx&quot;</span>
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 <span class="preprocessor">#include &lt;set&gt;</span>
<a name="l00028"></a>00028 
<a name="l00029"></a>00029 
<a name="l00030"></a>00030 <span class="keyword">namespace </span>Seldon
<a name="l00031"></a>00031 {
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00034"></a>00034   <span class="comment">/****************</span>
<a name="l00035"></a>00035 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00036"></a>00036 <span class="comment">   ****************/</span>
<a name="l00037"></a>00037 
<a name="l00038"></a>00038 
<a name="l00040"></a>00040 
<a name="l00043"></a>00043   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00044"></a>00044   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#af4624e0ed00838ec34b030103f9dd383" title="Default constructor.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Matrix_Sparse</a>():
<a name="l00045"></a>00045     Matrix_Base&lt;T, Allocator&gt;()
<a name="l00046"></a>00046   {
<a name="l00047"></a>00047     nz_ = 0;
<a name="l00048"></a>00048     ptr_ = NULL;
<a name="l00049"></a>00049     ind_ = NULL;
<a name="l00050"></a>00050   }
<a name="l00051"></a>00051 
<a name="l00052"></a>00052 
<a name="l00054"></a>00054 
<a name="l00059"></a>00059   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00060"></a>00060   <span class="keyword">inline</span> Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Matrix_Sparse(<span class="keywordtype">int</span> i,
<a name="l00061"></a>00061                                                                    <span class="keywordtype">int</span> j):
<a name="l00062"></a>00062     Matrix_Base&lt;T, Allocator&gt;()
<a name="l00063"></a>00063   {
<a name="l00064"></a>00064     nz_ = 0;
<a name="l00065"></a>00065     ptr_ = NULL;
<a name="l00066"></a>00066     ind_ = NULL;
<a name="l00067"></a>00067 
<a name="l00068"></a>00068     Reallocate(i, j);
<a name="l00069"></a>00069   }
<a name="l00070"></a>00070 
<a name="l00071"></a>00071 
<a name="l00073"></a>00073 
<a name="l00080"></a>00080   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00081"></a>00081   <span class="keyword">inline</span> Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::
<a name="l00082"></a>00082   Matrix_Sparse(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> nz):
<a name="l00083"></a>00083     Matrix_Base&lt;T, Allocator&gt;()
<a name="l00084"></a>00084   {
<a name="l00085"></a>00085     this-&gt;nz_ = 0;
<a name="l00086"></a>00086     ind_ = NULL;
<a name="l00087"></a>00087     ptr_ = NULL;
<a name="l00088"></a>00088 
<a name="l00089"></a>00089     Reallocate(i, j, nz);
<a name="l00090"></a>00090   }
<a name="l00091"></a>00091 
<a name="l00092"></a>00092 
<a name="l00094"></a>00094 
<a name="l00105"></a><a class="code" href="class_seldon_1_1_matrix___sparse.php#a2ff242fb46f84f041e4709a7eff70cd3">00105</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00106"></a>00106   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00107"></a>00107             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00108"></a>00108             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00109"></a>00109   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00110"></a>00110 <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">  Matrix_Sparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00111"></a>00111                 <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; values,
<a name="l00112"></a>00112                 <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; ptr,
<a name="l00113"></a>00113                 <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; ind):
<a name="l00114"></a>00114     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, j)
<a name="l00115"></a>00115   {
<a name="l00116"></a>00116     nz_ = values.GetLength();
<a name="l00117"></a>00117 
<a name="l00118"></a>00118 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00119"></a>00119 <span class="preprocessor"></span>    <span class="comment">// Checks whether vector sizes are acceptable.</span>
<a name="l00120"></a>00120 
<a name="l00121"></a>00121     <span class="keywordflow">if</span> (ind.GetLength() != nz_)
<a name="l00122"></a>00122       {
<a name="l00123"></a>00123         this-&gt;m_ = 0;
<a name="l00124"></a>00124         this-&gt;n_ = 0;
<a name="l00125"></a>00125         nz_ = 0;
<a name="l00126"></a>00126         ptr_ = NULL;
<a name="l00127"></a>00127         ind_ = NULL;
<a name="l00128"></a>00128         this-&gt;data_ = NULL;
<a name="l00129"></a>00129         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, &quot;</span>)
<a name="l00130"></a>00130                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00131"></a>00131                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz_) + <span class="stringliteral">&quot; values but &quot;</span>
<a name="l00132"></a>00132                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(ind.GetLength()) + <span class="stringliteral">&quot; row or column indices.&quot;</span>);
<a name="l00133"></a>00133       }
<a name="l00134"></a>00134 
<a name="l00135"></a>00135     <span class="keywordflow">if</span> (ptr.GetLength()-1 != Storage::GetFirst(i, j))
<a name="l00136"></a>00136       {
<a name="l00137"></a>00137         this-&gt;m_ = 0;
<a name="l00138"></a>00138         this-&gt;n_ = 0;
<a name="l00139"></a>00139         nz_ = 0;
<a name="l00140"></a>00140         ptr_ = NULL;
<a name="l00141"></a>00141         ind_ = NULL;
<a name="l00142"></a>00142         this-&gt;data_ = NULL;
<a name="l00143"></a>00143         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, &quot;</span>)
<a name="l00144"></a>00144                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00145"></a>00145                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The vector of start indices contains &quot;</span>)
<a name="l00146"></a>00146                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(ptr.GetLength()-1) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column&quot;</span>)
<a name="l00147"></a>00147                        + string(<span class="stringliteral">&quot; start indices (plus the number&quot;</span>)
<a name="l00148"></a>00148                        + <span class="stringliteral">&quot; of non-zero entries) but there are &quot;</span>
<a name="l00149"></a>00149                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j))
<a name="l00150"></a>00150                        + <span class="stringliteral">&quot; rows or columns (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span>
<a name="l00151"></a>00151                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix).&quot;</span>);
<a name="l00152"></a>00152       }
<a name="l00153"></a>00153 
<a name="l00154"></a>00154     <span class="keywordflow">if</span> (nz_ &gt; 0
<a name="l00155"></a>00155         &amp;&amp; (j == 0
<a name="l00156"></a>00156             || static_cast&lt;long int&gt;(nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l00157"></a>00157             &gt;= static_cast&lt;long int&gt;(i)))
<a name="l00158"></a>00158       {
<a name="l00159"></a>00159         this-&gt;m_ = 0;
<a name="l00160"></a>00160         this-&gt;n_ = 0;
<a name="l00161"></a>00161         nz_ = 0;
<a name="l00162"></a>00162         ptr_ = NULL;
<a name="l00163"></a>00163         ind_ = NULL;
<a name="l00164"></a>00164         this-&gt;data_ = NULL;
<a name="l00165"></a>00165         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, &quot;</span>)
<a name="l00166"></a>00166                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00167"></a>00167                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>)
<a name="l00168"></a>00168                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(values.GetLength())
<a name="l00169"></a>00169                        + <span class="stringliteral">&quot; values) than elements in the matrix (&quot;</span>
<a name="l00170"></a>00170                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00171"></a>00171       }
<a name="l00172"></a>00172 <span class="preprocessor">#endif</span>
<a name="l00173"></a>00173 <span class="preprocessor"></span>
<a name="l00174"></a>00174     this-&gt;ptr_ = ptr.GetData();
<a name="l00175"></a>00175     this-&gt;ind_ = ind.GetData();
<a name="l00176"></a>00176     this-&gt;data_ = values.GetData();
<a name="l00177"></a>00177 
<a name="l00178"></a>00178     ptr.Nullify();
<a name="l00179"></a>00179     ind.Nullify();
<a name="l00180"></a>00180     values.Nullify();
<a name="l00181"></a>00181   }
<a name="l00182"></a><a class="code" href="class_seldon_1_1_matrix___sparse.php#a49f2012fc254ff9e960b1dfee766ce0f">00182</a> 
<a name="l00183"></a>00183 
<a name="l00185"></a>00185   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00186"></a>00186   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#af4624e0ed00838ec34b030103f9dd383" title="Default constructor.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00187"></a>00187 <a class="code" href="class_seldon_1_1_matrix___sparse.php#af4624e0ed00838ec34b030103f9dd383" title="Default constructor.">  Matrix_Sparse</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A):
<a name="l00188"></a>00188     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(A)
<a name="l00189"></a>00189   {
<a name="l00190"></a>00190     this-&gt;m_ = 0;
<a name="l00191"></a>00191     this-&gt;n_ = 0;
<a name="l00192"></a>00192     this-&gt;nz_ = 0;
<a name="l00193"></a>00193     ptr_ = NULL;
<a name="l00194"></a>00194     ind_ = NULL;
<a name="l00195"></a>00195     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sparse.php#a54604e7e696124d278a4aa198edcab77" title="Copies a matrix.">Copy</a>(A);
<a name="l00196"></a>00196   }
<a name="l00197"></a>00197 
<a name="l00198"></a>00198 
<a name="l00199"></a>00199   <span class="comment">/**************</span>
<a name="l00200"></a>00200 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00201"></a><a class="code" href="class_seldon_1_1_matrix___sparse.php#a805ce704fffb3c509d908ec3ac45db28">00201</a> <span class="comment">   **************/</span>
<a name="l00202"></a>00202 
<a name="l00203"></a>00203 
<a name="l00205"></a>00205   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00206"></a>00206   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a805ce704fffb3c509d908ec3ac45db28" title="Destructor.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::~Matrix_Sparse</a>()
<a name="l00207"></a>00207   {
<a name="l00208"></a>00208     this-&gt;m_ = 0;
<a name="l00209"></a>00209     this-&gt;n_ = 0;
<a name="l00210"></a>00210 
<a name="l00211"></a>00211 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00212"></a>00212 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00213"></a>00213       {
<a name="l00214"></a>00214 <span class="preprocessor">#endif</span>
<a name="l00215"></a>00215 <span class="preprocessor"></span>
<a name="l00216"></a>00216         <span class="keywordflow">if</span> (ptr_ != NULL)
<a name="l00217"></a>00217           {
<a name="l00218"></a>00218             free(ptr_);
<a name="l00219"></a>00219             ptr_ = NULL;
<a name="l00220"></a>00220           }
<a name="l00221"></a>00221 
<a name="l00222"></a>00222 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00223"></a>00223 <span class="preprocessor"></span>      }
<a name="l00224"></a>00224     <span class="keywordflow">catch</span> (...)
<a name="l00225"></a>00225       {
<a name="l00226"></a>00226         ptr_ = NULL;
<a name="l00227"></a>00227       }
<a name="l00228"></a>00228 <span class="preprocessor">#endif</span>
<a name="l00229"></a>00229 <span class="preprocessor"></span>
<a name="l00230"></a>00230 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00231"></a>00231 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00232"></a>00232       {
<a name="l00233"></a>00233 <span class="preprocessor">#endif</span>
<a name="l00234"></a>00234 <span class="preprocessor"></span>
<a name="l00235"></a>00235         <span class="keywordflow">if</span> (ind_ != NULL)
<a name="l00236"></a>00236           {
<a name="l00237"></a>00237             free(ind_);
<a name="l00238"></a>00238             ind_ = NULL;
<a name="l00239"></a>00239           }
<a name="l00240"></a>00240 
<a name="l00241"></a>00241 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00242"></a>00242 <span class="preprocessor"></span>      }
<a name="l00243"></a>00243     <span class="keywordflow">catch</span> (...)
<a name="l00244"></a>00244       {
<a name="l00245"></a>00245         ind_ = NULL;
<a name="l00246"></a>00246       }
<a name="l00247"></a>00247 <span class="preprocessor">#endif</span>
<a name="l00248"></a>00248 <span class="preprocessor"></span>
<a name="l00249"></a>00249 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00250"></a>00250 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00251"></a>00251       {
<a name="l00252"></a>00252 <span class="preprocessor">#endif</span>
<a name="l00253"></a>00253 <span class="preprocessor"></span>
<a name="l00254"></a>00254         <span class="keywordflow">if</span> (this-&gt;data_ != NULL)
<a name="l00255"></a>00255           {
<a name="l00256"></a>00256             this-&gt;allocator_.deallocate(this-&gt;data_, nz_);
<a name="l00257"></a>00257             this-&gt;data_ = NULL;
<a name="l00258"></a>00258           }
<a name="l00259"></a>00259 
<a name="l00260"></a>00260 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00261"></a>00261 <span class="preprocessor"></span>      }
<a name="l00262"></a>00262     <span class="keywordflow">catch</span> (...)
<a name="l00263"></a>00263       {
<a name="l00264"></a>00264         this-&gt;nz_ = 0;
<a name="l00265"></a>00265         this-&gt;data_ = NULL;
<a name="l00266"></a>00266       }
<a name="l00267"></a>00267 <span class="preprocessor">#endif</span>
<a name="l00268"></a>00268 <span class="preprocessor"></span>
<a name="l00269"></a>00269     this-&gt;nz_ = 0;
<a name="l00270"></a>00270   }
<a name="l00271"></a>00271 
<a name="l00272"></a>00272 
<a name="l00274"></a>00274 
<a name="l00277"></a>00277   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00278"></a>00278   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#ab63b8d9aee1a38054cd525d7fa708f1a" title="Clears the matrix.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Clear</a>()
<a name="l00279"></a>00279   {
<a name="l00280"></a>00280     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sparse.php#a805ce704fffb3c509d908ec3ac45db28" title="Destructor.">~Matrix_Sparse</a>();
<a name="l00281"></a>00281   }
<a name="l00282"></a>00282 
<a name="l00283"></a>00283 
<a name="l00284"></a>00284   <span class="comment">/*********************</span>
<a name="l00285"></a>00285 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00286"></a>00286 <span class="comment">   *********************/</span>
<a name="l00287"></a>00287 
<a name="l00288"></a>00288 
<a name="l00290"></a>00290 
<a name="l00300"></a><a class="code" href="class_seldon_1_1_matrix___sparse.php#a5b411ce16d320a1442588af33adefe09">00300</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00301"></a>00301   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00302"></a>00302             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00303"></a>00303             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00304"></a>00304   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a5b411ce16d320a1442588af33adefe09" title="Redefines the matrix.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00305"></a>00305 <a class="code" href="class_seldon_1_1_matrix___sparse.php#a5b411ce16d320a1442588af33adefe09" title="Redefines the matrix.">  SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00306"></a>00306           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; values,
<a name="l00307"></a>00307           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; ptr,
<a name="l00308"></a>00308           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; ind)
<a name="l00309"></a>00309   {
<a name="l00310"></a>00310     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sparse.php#ab63b8d9aee1a38054cd525d7fa708f1a" title="Clears the matrix.">Clear</a>();
<a name="l00311"></a>00311     this-&gt;m_ = i;
<a name="l00312"></a>00312     this-&gt;n_ = j;
<a name="l00313"></a>00313     this-&gt;nz_ = values.GetLength();
<a name="l00314"></a>00314 
<a name="l00315"></a>00315 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00316"></a>00316 <span class="preprocessor"></span>    <span class="comment">// Checks whether vector sizes are acceptable.</span>
<a name="l00317"></a>00317 
<a name="l00318"></a>00318     <span class="keywordflow">if</span> (ind.GetLength() != nz_)
<a name="l00319"></a>00319       {
<a name="l00320"></a>00320         this-&gt;m_ = 0;
<a name="l00321"></a>00321         this-&gt;n_ = 0;
<a name="l00322"></a>00322         nz_ = 0;
<a name="l00323"></a>00323         ptr_ = NULL;
<a name="l00324"></a>00324         ind_ = NULL;
<a name="l00325"></a>00325         this-&gt;data_ = NULL;
<a name="l00326"></a>00326         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_Sparse::SetData(int, int, &quot;</span>)
<a name="l00327"></a>00327                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00328"></a>00328                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz_) + <span class="stringliteral">&quot; values but &quot;</span>
<a name="l00329"></a>00329                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(ind.GetLength()) + <span class="stringliteral">&quot; row or column indices.&quot;</span>);
<a name="l00330"></a>00330       }
<a name="l00331"></a>00331 
<a name="l00332"></a>00332     <span class="keywordflow">if</span> (ptr.GetLength()-1 != Storage::GetFirst(i, j))
<a name="l00333"></a>00333       {
<a name="l00334"></a>00334         this-&gt;m_ = 0;
<a name="l00335"></a>00335         this-&gt;n_ = 0;
<a name="l00336"></a>00336         nz_ = 0;
<a name="l00337"></a>00337         ptr_ = NULL;
<a name="l00338"></a>00338         ind_ = NULL;
<a name="l00339"></a>00339         this-&gt;data_ = NULL;
<a name="l00340"></a>00340         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_Sparse::SetData(int, int, &quot;</span>)
<a name="l00341"></a>00341                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00342"></a>00342                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The vector of start indices contains &quot;</span>)
<a name="l00343"></a>00343                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(ptr.GetLength()-1) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column&quot;</span>)
<a name="l00344"></a>00344                        + string(<span class="stringliteral">&quot; start indices (plus the number&quot;</span>)
<a name="l00345"></a>00345                        + <span class="stringliteral">&quot; of non-zero entries) but there are &quot;</span>
<a name="l00346"></a>00346                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j))
<a name="l00347"></a>00347                        + <span class="stringliteral">&quot; rows or columns (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span>
<a name="l00348"></a>00348                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix).&quot;</span>);
<a name="l00349"></a>00349       }
<a name="l00350"></a>00350 
<a name="l00351"></a>00351     <span class="keywordflow">if</span> (nz_ &gt; 0
<a name="l00352"></a>00352         &amp;&amp; (j == 0
<a name="l00353"></a>00353             || static_cast&lt;long int&gt;(nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l00354"></a>00354             &gt;= static_cast&lt;long int&gt;(i)))
<a name="l00355"></a>00355       {
<a name="l00356"></a>00356         this-&gt;m_ = 0;
<a name="l00357"></a>00357         this-&gt;n_ = 0;
<a name="l00358"></a>00358         nz_ = 0;
<a name="l00359"></a>00359         ptr_ = NULL;
<a name="l00360"></a>00360         ind_ = NULL;
<a name="l00361"></a>00361         this-&gt;data_ = NULL;
<a name="l00362"></a>00362         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_Sparse::SetData(int, int, &quot;</span>)
<a name="l00363"></a>00363                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00364"></a>00364                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>)
<a name="l00365"></a>00365                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(values.GetLength())
<a name="l00366"></a>00366                        + <span class="stringliteral">&quot; values) than elements in the matrix (&quot;</span>
<a name="l00367"></a>00367                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00368"></a>00368       }
<a name="l00369"></a>00369 <span class="preprocessor">#endif</span>
<a name="l00370"></a>00370 <span class="preprocessor"></span>
<a name="l00371"></a>00371     this-&gt;ptr_ = ptr.GetData();
<a name="l00372"></a>00372     this-&gt;ind_ = ind.GetData();
<a name="l00373"></a>00373     this-&gt;data_ = values.GetData();
<a name="l00374"></a>00374 
<a name="l00375"></a>00375     ptr.Nullify();
<a name="l00376"></a>00376     ind.Nullify();
<a name="l00377"></a>00377     values.Nullify();
<a name="l00378"></a>00378   }
<a name="l00379"></a>00379 
<a name="l00380"></a>00380 
<a name="l00382"></a>00382 
<a name="l00395"></a>00395   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00396"></a>00396   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a5b411ce16d320a1442588af33adefe09" title="Redefines the matrix.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00397"></a>00397 <a class="code" href="class_seldon_1_1_matrix___sparse.php#a5b411ce16d320a1442588af33adefe09" title="Redefines the matrix.">  ::SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> nz,
<a name="l00398"></a>00398             <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00399"></a>00399             ::pointer values,
<a name="l00400"></a>00400             <span class="keywordtype">int</span>* ptr, <span class="keywordtype">int</span>* ind)
<a name="l00401"></a>00401   {
<a name="l00402"></a>00402     this-&gt;Clear();
<a name="l00403"></a>00403 
<a name="l00404"></a>00404     this-&gt;m_ = i;
<a name="l00405"></a>00405     this-&gt;n_ = j;
<a name="l00406"></a>00406 
<a name="l00407"></a>00407     this-&gt;nz_ = nz;
<a name="l00408"></a>00408 
<a name="l00409"></a>00409     this-&gt;data_ = values;
<a name="l00410"></a>00410     ind_ = ind;
<a name="l00411"></a>00411     ptr_ = ptr;
<a name="l00412"></a>00412   }
<a name="l00413"></a>00413 
<a name="l00414"></a>00414 
<a name="l00416"></a><a class="code" href="class_seldon_1_1_matrix___sparse.php#a805231f8ec04efc5a081b21c9f65d5c9">00416</a> 
<a name="l00420"></a>00420   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00421"></a>00421   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a805231f8ec04efc5a081b21c9f65d5c9" title="Clears the matrix without releasing memory.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Nullify</a>()
<a name="l00422"></a>00422   {
<a name="l00423"></a>00423     this-&gt;data_ = NULL;
<a name="l00424"></a>00424     this-&gt;m_ = 0;
<a name="l00425"></a>00425     this-&gt;n_ = 0;
<a name="l00426"></a>00426     nz_ = 0;
<a name="l00427"></a>00427     ptr_ = NULL;
<a name="l00428"></a>00428     ind_ = NULL;
<a name="l00429"></a>00429   }
<a name="l00430"></a>00430 
<a name="l00431"></a>00431 
<a name="l00433"></a><a class="code" href="class_seldon_1_1_matrix___sparse.php#a68315328fe25c14b9d8bef181c1c2f49">00433</a> 
<a name="l00437"></a>00437   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00438"></a>00438   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a68315328fe25c14b9d8bef181c1c2f49" title="Initialization of an empty sparse matrix with i rows and j columns.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00439"></a>00439   {
<a name="l00440"></a>00440     <span class="comment">// clearing previous entries</span>
<a name="l00441"></a>00441     <a class="code" href="class_seldon_1_1_matrix___sparse.php#ab63b8d9aee1a38054cd525d7fa708f1a" title="Clears the matrix.">Clear</a>();
<a name="l00442"></a>00442 
<a name="l00443"></a>00443     this-&gt;m_ = i;
<a name="l00444"></a>00444     this-&gt;n_ = j;
<a name="l00445"></a>00445 
<a name="l00446"></a>00446     <span class="comment">// we try to allocate ptr_</span>
<a name="l00447"></a>00447 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00448"></a>00448 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00449"></a>00449       {
<a name="l00450"></a>00450 <span class="preprocessor">#endif</span>
<a name="l00451"></a>00451 <span class="preprocessor"></span>
<a name="l00452"></a>00452         ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(Storage::GetFirst(i, j)+1,
<a name="l00453"></a>00453                                               <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00454"></a>00454 
<a name="l00455"></a>00455 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00456"></a>00456 <span class="preprocessor"></span>      }
<a name="l00457"></a>00457     <span class="keywordflow">catch</span> (...)
<a name="l00458"></a>00458       {
<a name="l00459"></a>00459         this-&gt;m_ = 0;
<a name="l00460"></a>00460         this-&gt;n_ = 0;
<a name="l00461"></a>00461         nz_ = 0;
<a name="l00462"></a>00462         ptr_ = NULL;
<a name="l00463"></a>00463         ind_ = NULL;
<a name="l00464"></a>00464         this-&gt;data_ = NULL;
<a name="l00465"></a>00465       }
<a name="l00466"></a>00466     <span class="keywordflow">if</span> (ptr_ == NULL)
<a name="l00467"></a>00467       {
<a name="l00468"></a>00468         this-&gt;m_ = 0;
<a name="l00469"></a>00469         this-&gt;n_ = 0;
<a name="l00470"></a>00470         nz_ = 0;
<a name="l00471"></a>00471         ind_ = NULL;
<a name="l00472"></a>00472         this-&gt;data_ = NULL;
<a name="l00473"></a>00473       }
<a name="l00474"></a>00474     <span class="keywordflow">if</span> (ptr_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00475"></a>00475       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Sparse::Reallocate(int, int)&quot;</span>,
<a name="l00476"></a>00476                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00477"></a>00477                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (Storage::GetFirst(i, j)+1) )
<a name="l00478"></a>00478                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j)+1)
<a name="l00479"></a>00479                      + <span class="stringliteral">&quot; row or column start indices, for a &quot;</span>
<a name="l00480"></a>00480                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00481"></a>00481 <span class="preprocessor">#endif</span>
<a name="l00482"></a>00482 <span class="preprocessor"></span>
<a name="l00483"></a>00483     <span class="comment">// then filing ptr_ with 0</span>
<a name="l00484"></a>00484     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt;= Storage::GetFirst(i, j); k++)
<a name="l00485"></a>00485       ptr_[k] = 0;
<a name="l00486"></a>00486 
<a name="l00487"></a>00487   }
<a name="l00488"></a>00488 
<a name="l00489"></a>00489 
<a name="l00491"></a>00491 
<a name="l00496"></a>00496   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00497"></a>00497   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a68315328fe25c14b9d8bef181c1c2f49" title="Initialization of an empty sparse matrix with i rows and j columns.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> nz)
<a name="l00498"></a>00498   {
<a name="l00499"></a>00499     <span class="comment">// clearing previous entries</span>
<a name="l00500"></a>00500     <a class="code" href="class_seldon_1_1_matrix___sparse.php#ab63b8d9aee1a38054cd525d7fa708f1a" title="Clears the matrix.">Clear</a>();
<a name="l00501"></a>00501 
<a name="l00502"></a>00502     this-&gt;nz_ = nz;
<a name="l00503"></a>00503     this-&gt;m_ = i;
<a name="l00504"></a>00504     this-&gt;n_ = j;
<a name="l00505"></a>00505 
<a name="l00506"></a>00506 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00507"></a>00507 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (nz_ &lt; 0)
<a name="l00508"></a>00508       {
<a name="l00509"></a>00509         this-&gt;m_ = 0;
<a name="l00510"></a>00510         this-&gt;n_ = 0;
<a name="l00511"></a>00511         nz_ = 0;
<a name="l00512"></a>00512         ptr_ = NULL;
<a name="l00513"></a>00513         ind_ = NULL;
<a name="l00514"></a>00514         this-&gt;data_ = NULL;
<a name="l00515"></a>00515         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, int)&quot;</span>,
<a name="l00516"></a>00516                        <span class="stringliteral">&quot;Invalid number of non-zero elements: &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00517"></a>00517                        + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00518"></a>00518       }
<a name="l00519"></a>00519     <span class="keywordflow">if</span> (nz_ &gt; 0
<a name="l00520"></a>00520         &amp;&amp; (j == 0
<a name="l00521"></a>00521             || static_cast&lt;long int&gt;(nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l00522"></a>00522             &gt;= static_cast&lt;long int&gt;(i)))
<a name="l00523"></a>00523       {
<a name="l00524"></a>00524         this-&gt;m_ = 0;
<a name="l00525"></a>00525         this-&gt;n_ = 0;
<a name="l00526"></a>00526         nz_ = 0;
<a name="l00527"></a>00527         ptr_ = NULL;
<a name="l00528"></a>00528         ind_ = NULL;
<a name="l00529"></a>00529         this-&gt;data_ = NULL;
<a name="l00530"></a>00530         <span class="keywordflow">throw</span> WrongDim(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, int)&quot;</span>,
<a name="l00531"></a>00531                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00532"></a>00532                        + <span class="stringliteral">&quot; values) than elements in the matrix (&quot;</span>
<a name="l00533"></a>00533                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00534"></a>00534       }
<a name="l00535"></a>00535 <span class="preprocessor">#endif</span>
<a name="l00536"></a>00536 <span class="preprocessor"></span>
<a name="l00537"></a>00537 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00538"></a>00538 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00539"></a>00539       {
<a name="l00540"></a>00540 <span class="preprocessor">#endif</span>
<a name="l00541"></a>00541 <span class="preprocessor"></span>
<a name="l00542"></a>00542         ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(Storage::GetFirst(i, j)+1,
<a name="l00543"></a>00543                                               <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00544"></a>00544 
<a name="l00545"></a>00545 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00546"></a>00546 <span class="preprocessor"></span>      }
<a name="l00547"></a>00547     <span class="keywordflow">catch</span> (...)
<a name="l00548"></a>00548       {
<a name="l00549"></a>00549         this-&gt;m_ = 0;
<a name="l00550"></a>00550         this-&gt;n_ = 0;
<a name="l00551"></a>00551         nz_ = 0;
<a name="l00552"></a>00552         ptr_ = NULL;
<a name="l00553"></a>00553         ind_ = NULL;
<a name="l00554"></a>00554         this-&gt;data_ = NULL;
<a name="l00555"></a>00555       }
<a name="l00556"></a>00556     <span class="keywordflow">if</span> (ptr_ == NULL)
<a name="l00557"></a>00557       {
<a name="l00558"></a>00558         this-&gt;m_ = 0;
<a name="l00559"></a>00559         this-&gt;n_ = 0;
<a name="l00560"></a>00560         nz_ = 0;
<a name="l00561"></a>00561         ind_ = NULL;
<a name="l00562"></a>00562         this-&gt;data_ = NULL;
<a name="l00563"></a>00563       }
<a name="l00564"></a>00564     <span class="keywordflow">if</span> (ptr_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00565"></a>00565       <span class="keywordflow">throw</span> NoMemory(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, int)&quot;</span>,
<a name="l00566"></a>00566                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00567"></a>00567                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (Storage::GetFirst(i, j)+1) )
<a name="l00568"></a>00568                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j)+1)
<a name="l00569"></a>00569                      + <span class="stringliteral">&quot; row or column start indices, for a &quot;</span>
<a name="l00570"></a>00570                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00571"></a>00571 <span class="preprocessor">#endif</span>
<a name="l00572"></a>00572 <span class="preprocessor"></span>
<a name="l00573"></a>00573 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00574"></a>00574 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00575"></a>00575       {
<a name="l00576"></a>00576 <span class="preprocessor">#endif</span>
<a name="l00577"></a>00577 <span class="preprocessor"></span>
<a name="l00578"></a>00578         ind_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(nz_, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00579"></a>00579 
<a name="l00580"></a>00580 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00581"></a>00581 <span class="preprocessor"></span>      }
<a name="l00582"></a>00582     <span class="keywordflow">catch</span> (...)
<a name="l00583"></a>00583       {
<a name="l00584"></a>00584         this-&gt;m_ = 0;
<a name="l00585"></a>00585         this-&gt;n_ = 0;
<a name="l00586"></a>00586         nz_ = 0;
<a name="l00587"></a>00587         free(ptr_);
<a name="l00588"></a>00588         ptr_ = NULL;
<a name="l00589"></a>00589         ind_ = NULL;
<a name="l00590"></a>00590         this-&gt;data_ = NULL;
<a name="l00591"></a>00591       }
<a name="l00592"></a>00592     <span class="keywordflow">if</span> (ind_ == NULL)
<a name="l00593"></a>00593       {
<a name="l00594"></a>00594         this-&gt;m_ = 0;
<a name="l00595"></a>00595         this-&gt;n_ = 0;
<a name="l00596"></a>00596         nz_ = 0;
<a name="l00597"></a>00597         free(ptr_);
<a name="l00598"></a>00598         ptr_ = NULL;
<a name="l00599"></a>00599         this-&gt;data_ = NULL;
<a name="l00600"></a>00600       }
<a name="l00601"></a>00601     <span class="keywordflow">if</span> (ind_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00602"></a>00602       <span class="keywordflow">throw</span> NoMemory(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, int)&quot;</span>,
<a name="l00603"></a>00603                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * nz)
<a name="l00604"></a>00604                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00605"></a>00605                      + <span class="stringliteral">&quot; row or column indices, for a &quot;</span>
<a name="l00606"></a>00606                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00607"></a>00607 <span class="preprocessor">#endif</span>
<a name="l00608"></a>00608 <span class="preprocessor"></span>
<a name="l00609"></a>00609 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00610"></a>00610 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00611"></a>00611       {
<a name="l00612"></a>00612 <span class="preprocessor">#endif</span>
<a name="l00613"></a>00613 <span class="preprocessor"></span>
<a name="l00614"></a>00614         this-&gt;data_ = this-&gt;allocator_.allocate(nz_, <span class="keyword">this</span>);
<a name="l00615"></a>00615 
<a name="l00616"></a>00616 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00617"></a>00617 <span class="preprocessor"></span>      }
<a name="l00618"></a>00618     <span class="keywordflow">catch</span> (...)
<a name="l00619"></a>00619       {
<a name="l00620"></a>00620         this-&gt;m_ = 0;
<a name="l00621"></a>00621         this-&gt;n_ = 0;
<a name="l00622"></a>00622         free(ptr_);
<a name="l00623"></a>00623         ptr_ = NULL;
<a name="l00624"></a>00624         free(ind_);
<a name="l00625"></a>00625         ind_ = NULL;
<a name="l00626"></a>00626         this-&gt;data_ = NULL;
<a name="l00627"></a>00627       }
<a name="l00628"></a>00628     <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00629"></a>00629       {
<a name="l00630"></a>00630         this-&gt;m_ = 0;
<a name="l00631"></a>00631         this-&gt;n_ = 0;
<a name="l00632"></a>00632         free(ptr_);
<a name="l00633"></a>00633         ptr_ = NULL;
<a name="l00634"></a>00634         free(ind_);
<a name="l00635"></a>00635         ind_ = NULL;
<a name="l00636"></a>00636       }
<a name="l00637"></a>00637     <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00638"></a>00638       <span class="keywordflow">throw</span> NoMemory(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, int)&quot;</span>,
<a name="l00639"></a>00639                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * nz)
<a name="l00640"></a>00640                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz) + <span class="stringliteral">&quot; values, for a &quot;</span>
<a name="l00641"></a>00641                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00642"></a>00642 <span class="preprocessor">#endif</span>
<a name="l00643"></a>00643 <span class="preprocessor"></span>  }
<a name="l00644"></a>00644 
<a name="l00645"></a>00645 
<a name="l00647"></a>00647 
<a name="l00652"></a>00652   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00653"></a>00653   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a84a417a78587e38b7e9caeecb766aa84" title="Reallocates memory to resize the matrix and keeps previous entries.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00654"></a>00654   {
<a name="l00655"></a>00655     <span class="keywordflow">if</span> (Storage::GetFirst(i, j) &lt; Storage::GetFirst(this-&gt;m_, this-&gt;n_))
<a name="l00656"></a>00656       <a class="code" href="class_seldon_1_1_matrix___sparse.php#a84a417a78587e38b7e9caeecb766aa84" title="Reallocates memory to resize the matrix and keeps previous entries.">Resize</a>(i, j, ptr_[Storage::GetFirst(i, j)]);
<a name="l00657"></a>00657     <span class="keywordflow">else</span>
<a name="l00658"></a>00658       <a class="code" href="class_seldon_1_1_matrix___sparse.php#a84a417a78587e38b7e9caeecb766aa84" title="Reallocates memory to resize the matrix and keeps previous entries.">Resize</a>(i, j, nz_);
<a name="l00659"></a>00659   }
<a name="l00660"></a>00660 
<a name="l00661"></a>00661 
<a name="l00663"></a>00663 
<a name="l00669"></a>00669   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00670"></a>00670   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a84a417a78587e38b7e9caeecb766aa84" title="Reallocates memory to resize the matrix and keeps previous entries.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00671"></a>00671 <a class="code" href="class_seldon_1_1_matrix___sparse.php#a84a417a78587e38b7e9caeecb766aa84" title="Reallocates memory to resize the matrix and keeps previous entries.">  ::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> nz)
<a name="l00672"></a>00672   {
<a name="l00673"></a>00673 
<a name="l00674"></a>00674 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00675"></a>00675 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (nz &lt; 0)
<a name="l00676"></a>00676       {
<a name="l00677"></a>00677         this-&gt;m_ = 0;
<a name="l00678"></a>00678         this-&gt;n_ = 0;
<a name="l00679"></a>00679         nz_ = 0;
<a name="l00680"></a>00680         ptr_ = NULL;
<a name="l00681"></a>00681         ind_ = NULL;
<a name="l00682"></a>00682         this-&gt;data_ = NULL;
<a name="l00683"></a>00683         <span class="keywordflow">throw</span> WrongDim(<span class="stringliteral">&quot;Matrix_Sparse::Resize(int, int, int)&quot;</span>,
<a name="l00684"></a>00684                        <span class="stringliteral">&quot;Invalid number of non-zero elements: &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00685"></a>00685                        + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00686"></a>00686       }
<a name="l00687"></a>00687     <span class="keywordflow">if</span> (nz &gt; 0
<a name="l00688"></a>00688         &amp;&amp; (j == 0
<a name="l00689"></a>00689             || static_cast&lt;long int&gt;(nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l00690"></a>00690             &gt;= static_cast&lt;long int&gt;(i)))
<a name="l00691"></a>00691       {
<a name="l00692"></a>00692         this-&gt;m_ = 0;
<a name="l00693"></a>00693         this-&gt;n_ = 0;
<a name="l00694"></a>00694         nz_ = 0;
<a name="l00695"></a>00695         ptr_ = NULL;
<a name="l00696"></a>00696         ind_ = NULL;
<a name="l00697"></a>00697         this-&gt;data_ = NULL;
<a name="l00698"></a>00698         <span class="keywordflow">throw</span> WrongDim(<span class="stringliteral">&quot;Matrix_Sparse::Resize(int, int, int)&quot;</span>,
<a name="l00699"></a>00699                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00700"></a>00700                        + <span class="stringliteral">&quot; values) than elements in the matrix (&quot;</span>
<a name="l00701"></a>00701                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00702"></a>00702       }
<a name="l00703"></a>00703 <span class="preprocessor">#endif</span>
<a name="l00704"></a>00704 <span class="preprocessor"></span>
<a name="l00705"></a>00705     <span class="keywordflow">if</span> (nz != nz_)
<a name="l00706"></a>00706       {
<a name="l00707"></a>00707         <span class="comment">// trying to resize ind_ and data_</span>
<a name="l00708"></a>00708 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00709"></a>00709 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l00710"></a>00710           {
<a name="l00711"></a>00711 <span class="preprocessor">#endif</span>
<a name="l00712"></a>00712 <span class="preprocessor"></span>
<a name="l00713"></a>00713             ind_
<a name="l00714"></a>00714               = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>(realloc(reinterpret_cast&lt;void*&gt;(ind_),
<a name="l00715"></a>00715                                                nz*<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)));
<a name="l00716"></a>00716 
<a name="l00717"></a>00717 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00718"></a>00718 <span class="preprocessor"></span>          }
<a name="l00719"></a>00719         <span class="keywordflow">catch</span> (...)
<a name="l00720"></a>00720           {
<a name="l00721"></a>00721             this-&gt;m_ = 0;
<a name="l00722"></a>00722             this-&gt;n_ = 0;
<a name="l00723"></a>00723             nz_ = 0;
<a name="l00724"></a>00724             free(ptr_);
<a name="l00725"></a>00725             ptr_ = NULL;
<a name="l00726"></a>00726             ind_ = NULL;
<a name="l00727"></a>00727             this-&gt;data_ = NULL;
<a name="l00728"></a>00728           }
<a name="l00729"></a>00729         <span class="keywordflow">if</span> (ind_ == NULL)
<a name="l00730"></a>00730           {
<a name="l00731"></a>00731             this-&gt;m_ = 0;
<a name="l00732"></a>00732             this-&gt;n_ = 0;
<a name="l00733"></a>00733             nz_ = 0;
<a name="l00734"></a>00734             free(ptr_);
<a name="l00735"></a>00735             ptr_ = NULL;
<a name="l00736"></a>00736             this-&gt;data_ = NULL;
<a name="l00737"></a>00737           }
<a name="l00738"></a>00738         <span class="keywordflow">if</span> (ind_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00739"></a>00739           <span class="keywordflow">throw</span> NoMemory(<span class="stringliteral">&quot;Matrix_Sparse::Resize(int, int, int)&quot;</span>,
<a name="l00740"></a>00740                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * nz)
<a name="l00741"></a>00741                          + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00742"></a>00742                          + <span class="stringliteral">&quot; row or column indices, for a &quot;</span>
<a name="l00743"></a>00743                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00744"></a>00744 <span class="preprocessor">#endif</span>
<a name="l00745"></a>00745 <span class="preprocessor"></span>
<a name="l00746"></a>00746         Vector&lt;T, VectFull, Allocator&gt; val;
<a name="l00747"></a>00747         val.SetData(nz_, this-&gt;data_);
<a name="l00748"></a>00748         val.Resize(nz);
<a name="l00749"></a>00749 
<a name="l00750"></a>00750         this-&gt;data_ = val.GetData();
<a name="l00751"></a>00751         nz_ = nz;
<a name="l00752"></a>00752         val.Nullify();
<a name="l00753"></a>00753       }
<a name="l00754"></a>00754 
<a name="l00755"></a>00755 
<a name="l00756"></a>00756     <span class="keywordflow">if</span> (Storage::GetFirst(this-&gt;m_, this-&gt;n_) != Storage::GetFirst(i, j))
<a name="l00757"></a>00757       {
<a name="l00758"></a>00758 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00759"></a>00759 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l00760"></a>00760           {
<a name="l00761"></a>00761 <span class="preprocessor">#endif</span>
<a name="l00762"></a>00762 <span class="preprocessor"></span>            <span class="comment">// trying to resize ptr_</span>
<a name="l00763"></a>00763             ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( realloc(ptr_, (Storage::GetFirst(i, j)+1)*
<a name="l00764"></a>00764                                                    <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00765"></a>00765 
<a name="l00766"></a>00766 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00767"></a>00767 <span class="preprocessor"></span>          }
<a name="l00768"></a>00768         <span class="keywordflow">catch</span> (...)
<a name="l00769"></a>00769           {
<a name="l00770"></a>00770             this-&gt;m_ = 0;
<a name="l00771"></a>00771             this-&gt;n_ = 0;
<a name="l00772"></a>00772             nz_ = 0;
<a name="l00773"></a>00773             ptr_ = NULL;
<a name="l00774"></a>00774             ind_ = NULL;
<a name="l00775"></a>00775             this-&gt;data_ = NULL;
<a name="l00776"></a>00776           }
<a name="l00777"></a>00777         <span class="keywordflow">if</span> (ptr_ == NULL)
<a name="l00778"></a>00778           {
<a name="l00779"></a>00779             this-&gt;m_ = 0;
<a name="l00780"></a>00780             this-&gt;n_ = 0;
<a name="l00781"></a>00781             nz_ = 0;
<a name="l00782"></a>00782             ind_ = NULL;
<a name="l00783"></a>00783             this-&gt;data_ = NULL;
<a name="l00784"></a>00784           }
<a name="l00785"></a>00785         <span class="keywordflow">if</span> (ptr_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00786"></a>00786           <span class="keywordflow">throw</span> NoMemory(<span class="stringliteral">&quot;Matrix_Sparse::Resize(int, int)&quot;</span>,
<a name="l00787"></a>00787                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00788"></a>00788                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (Storage::GetFirst(i, j)+1) )
<a name="l00789"></a>00789                          + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j)+1)
<a name="l00790"></a>00790                          + <span class="stringliteral">&quot; row or column start indices, for a &quot;</span>
<a name="l00791"></a>00791                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00792"></a>00792 <span class="preprocessor">#endif</span>
<a name="l00793"></a>00793 <span class="preprocessor"></span>
<a name="l00794"></a>00794         <span class="comment">// then filing last values of ptr_ with nz_</span>
<a name="l00795"></a>00795         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = Storage::GetFirst(this-&gt;m_, this-&gt;n_);
<a name="l00796"></a>00796              k &lt;= Storage::GetFirst(i, j); k++)
<a name="l00797"></a>00797           ptr_[k] = this-&gt;nz_;
<a name="l00798"></a>00798       }
<a name="l00799"></a>00799 
<a name="l00800"></a>00800     this-&gt;m_ = i;
<a name="l00801"></a>00801     this-&gt;n_ = j;
<a name="l00802"></a>00802   }
<a name="l00803"></a><a class="code" href="class_seldon_1_1_matrix___sparse.php#a54604e7e696124d278a4aa198edcab77">00803</a> 
<a name="l00804"></a>00804 
<a name="l00806"></a>00806   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00807"></a>00807   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a54604e7e696124d278a4aa198edcab77" title="Copies a matrix.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00808"></a>00808 <a class="code" href="class_seldon_1_1_matrix___sparse.php#a54604e7e696124d278a4aa198edcab77" title="Copies a matrix.">  Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00809"></a>00809   {
<a name="l00810"></a>00810     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sparse.php#ab63b8d9aee1a38054cd525d7fa708f1a" title="Clears the matrix.">Clear</a>();
<a name="l00811"></a>00811     <span class="keywordtype">int</span> nz = A.nz_;
<a name="l00812"></a>00812     <span class="keywordtype">int</span> i = A.m_;
<a name="l00813"></a>00813     <span class="keywordtype">int</span> j = A.n_;
<a name="l00814"></a>00814     this-&gt;nz_ = nz;
<a name="l00815"></a>00815     this-&gt;m_ = i;
<a name="l00816"></a>00816     this-&gt;n_ = j;
<a name="l00817"></a>00817     <span class="keywordflow">if</span> ((i == 0)||(j == 0))
<a name="l00818"></a>00818       {
<a name="l00819"></a>00819         this-&gt;m_ = 0;
<a name="l00820"></a>00820         this-&gt;n_ = 0;
<a name="l00821"></a>00821         this-&gt;nz_ = 0;
<a name="l00822"></a>00822         <span class="keywordflow">return</span>;
<a name="l00823"></a>00823       }
<a name="l00824"></a>00824 
<a name="l00825"></a>00825 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00826"></a>00826 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (nz_ &gt; 0
<a name="l00827"></a>00827         &amp;&amp; (j == 0
<a name="l00828"></a>00828             || static_cast&lt;long int&gt;(nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l00829"></a>00829             &gt;= static_cast&lt;long int&gt;(i)))
<a name="l00830"></a>00830       {
<a name="l00831"></a>00831         this-&gt;m_ = 0;
<a name="l00832"></a>00832         this-&gt;n_ = 0;
<a name="l00833"></a>00833         nz_ = 0;
<a name="l00834"></a>00834         ptr_ = NULL;
<a name="l00835"></a>00835         ind_ = NULL;
<a name="l00836"></a>00836         this-&gt;data_ = NULL;
<a name="l00837"></a>00837         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, int)&quot;</span>,
<a name="l00838"></a>00838                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00839"></a>00839                        + <span class="stringliteral">&quot; values) than elements in the matrix (&quot;</span>
<a name="l00840"></a>00840                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00841"></a>00841       }
<a name="l00842"></a>00842 <span class="preprocessor">#endif</span>
<a name="l00843"></a>00843 <span class="preprocessor"></span>
<a name="l00844"></a>00844 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00845"></a>00845 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00846"></a>00846       {
<a name="l00847"></a>00847 <span class="preprocessor">#endif</span>
<a name="l00848"></a>00848 <span class="preprocessor"></span>
<a name="l00849"></a>00849         ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(Storage::GetFirst(i, j)+1,
<a name="l00850"></a>00850                                               <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00851"></a>00851         memcpy(this-&gt;ptr_, A.ptr_,
<a name="l00852"></a>00852                (Storage::GetFirst(i, j) + 1) * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00853"></a>00853 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00854"></a>00854 <span class="preprocessor"></span>      }
<a name="l00855"></a>00855     <span class="keywordflow">catch</span> (...)
<a name="l00856"></a>00856       {
<a name="l00857"></a>00857         this-&gt;m_ = 0;
<a name="l00858"></a>00858         this-&gt;n_ = 0;
<a name="l00859"></a>00859         nz_ = 0;
<a name="l00860"></a>00860         ptr_ = NULL;
<a name="l00861"></a>00861         ind_ = NULL;
<a name="l00862"></a>00862         this-&gt;data_ = NULL;
<a name="l00863"></a>00863       }
<a name="l00864"></a>00864     <span class="keywordflow">if</span> (ptr_ == NULL)
<a name="l00865"></a>00865       {
<a name="l00866"></a>00866         this-&gt;m_ = 0;
<a name="l00867"></a>00867         this-&gt;n_ = 0;
<a name="l00868"></a>00868         nz_ = 0;
<a name="l00869"></a>00869         ind_ = NULL;
<a name="l00870"></a>00870         this-&gt;data_ = NULL;
<a name="l00871"></a>00871       }
<a name="l00872"></a>00872     <span class="keywordflow">if</span> (ptr_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00873"></a>00873       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, int)&quot;</span>,
<a name="l00874"></a>00874                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00875"></a>00875                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (Storage::GetFirst(i, j)+1) )
<a name="l00876"></a>00876                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j)+1)
<a name="l00877"></a>00877                      + <span class="stringliteral">&quot; row or column start indices, for a &quot;</span>
<a name="l00878"></a>00878                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00879"></a>00879 <span class="preprocessor">#endif</span>
<a name="l00880"></a>00880 <span class="preprocessor"></span>
<a name="l00881"></a>00881 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00882"></a>00882 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00883"></a>00883       {
<a name="l00884"></a>00884 <span class="preprocessor">#endif</span>
<a name="l00885"></a>00885 <span class="preprocessor"></span>
<a name="l00886"></a>00886         ind_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(nz_, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00887"></a>00887         memcpy(this-&gt;ind_, A.ind_, nz_ * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00888"></a>00888 
<a name="l00889"></a>00889 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00890"></a>00890 <span class="preprocessor"></span>      }
<a name="l00891"></a>00891     <span class="keywordflow">catch</span> (...)
<a name="l00892"></a>00892       {
<a name="l00893"></a>00893         this-&gt;m_ = 0;
<a name="l00894"></a>00894         this-&gt;n_ = 0;
<a name="l00895"></a>00895         nz_ = 0;
<a name="l00896"></a>00896         free(ptr_);
<a name="l00897"></a>00897         ptr_ = NULL;
<a name="l00898"></a>00898         ind_ = NULL;
<a name="l00899"></a>00899         this-&gt;data_ = NULL;
<a name="l00900"></a>00900       }
<a name="l00901"></a>00901     <span class="keywordflow">if</span> (ind_ == NULL)
<a name="l00902"></a>00902       {
<a name="l00903"></a>00903         this-&gt;m_ = 0;
<a name="l00904"></a>00904         this-&gt;n_ = 0;
<a name="l00905"></a>00905         nz_ = 0;
<a name="l00906"></a>00906         free(ptr_);
<a name="l00907"></a>00907         ptr_ = NULL;
<a name="l00908"></a>00908         this-&gt;data_ = NULL;
<a name="l00909"></a>00909       }
<a name="l00910"></a>00910     <span class="keywordflow">if</span> (ind_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00911"></a>00911       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, int)&quot;</span>,
<a name="l00912"></a>00912                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * nz)
<a name="l00913"></a>00913                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00914"></a>00914                      + <span class="stringliteral">&quot; row or column indices, for a &quot;</span>
<a name="l00915"></a>00915                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00916"></a>00916 <span class="preprocessor">#endif</span>
<a name="l00917"></a>00917 <span class="preprocessor"></span>
<a name="l00918"></a>00918 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00919"></a>00919 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00920"></a>00920       {
<a name="l00921"></a>00921 <span class="preprocessor">#endif</span>
<a name="l00922"></a>00922 <span class="preprocessor"></span>
<a name="l00923"></a>00923         this-&gt;data_ = this-&gt;allocator_.allocate(nz_, <span class="keyword">this</span>);
<a name="l00924"></a>00924         this-&gt;allocator_.memorycpy(this-&gt;data_, A.data_, nz_);
<a name="l00925"></a>00925 
<a name="l00926"></a>00926 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00927"></a>00927 <span class="preprocessor"></span>      }
<a name="l00928"></a>00928     <span class="keywordflow">catch</span> (...)
<a name="l00929"></a>00929       {
<a name="l00930"></a>00930         this-&gt;m_ = 0;
<a name="l00931"></a>00931         this-&gt;n_ = 0;
<a name="l00932"></a>00932         free(ptr_);
<a name="l00933"></a>00933         ptr_ = NULL;
<a name="l00934"></a>00934         free(ind_);
<a name="l00935"></a>00935         ind_ = NULL;
<a name="l00936"></a>00936         this-&gt;data_ = NULL;
<a name="l00937"></a>00937       }
<a name="l00938"></a>00938     <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00939"></a>00939       {
<a name="l00940"></a>00940         this-&gt;m_ = 0;
<a name="l00941"></a>00941         this-&gt;n_ = 0;
<a name="l00942"></a>00942         free(ptr_);
<a name="l00943"></a>00943         ptr_ = NULL;
<a name="l00944"></a>00944         free(ind_);
<a name="l00945"></a>00945         ind_ = NULL;
<a name="l00946"></a>00946       }
<a name="l00947"></a>00947     <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00948"></a>00948       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, int)&quot;</span>,
<a name="l00949"></a>00949                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * nz)
<a name="l00950"></a>00950                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz) + <span class="stringliteral">&quot; values, for a &quot;</span>
<a name="l00951"></a>00951                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00952"></a>00952 <span class="preprocessor">#endif</span>
<a name="l00953"></a>00953 <span class="preprocessor"></span>
<a name="l00954"></a>00954   }
<a name="l00955"></a>00955 
<a name="l00956"></a>00956 
<a name="l00957"></a>00957   <span class="comment">/*******************</span>
<a name="l00958"></a>00958 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00959"></a>00959 <span class="comment">   *******************/</span>
<a name="l00960"></a>00960 
<a name="l00961"></a>00961 
<a name="l00963"></a>00963 
<a name="l00966"></a>00966   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00967"></a>00967   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a0a0f412562d73bb7c9361da17565e7fb" title="Returns the number of non-zero elements.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::GetNonZeros</a>()<span class="keyword"> const</span>
<a name="l00968"></a>00968 <span class="keyword">  </span>{
<a name="l00969"></a>00969     <span class="keywordflow">return</span> nz_;
<a name="l00970"></a>00970   }
<a name="l00971"></a>00971 
<a name="l00972"></a>00972 
<a name="l00974"></a>00974 
<a name="l00979"></a>00979   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00980"></a>00980   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#aac6b06ab623602d24a80d12adf495cf2" title="Returns the number of elements stored in memory.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::GetDataSize</a>()<span class="keyword"> const</span>
<a name="l00981"></a>00981 <span class="keyword">  </span>{
<a name="l00982"></a>00982     <span class="keywordflow">return</span> nz_;
<a name="l00983"></a>00983   }
<a name="l00984"></a>00984 
<a name="l00985"></a>00985 
<a name="l00987"></a><a class="code" href="class_seldon_1_1_matrix___sparse.php#a9126980f0a760e8a73c2d0f480c69342">00987</a> 
<a name="l00991"></a>00991   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00992"></a>00992   <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___sparse.php#a9126980f0a760e8a73c2d0f480c69342" title="Returns (row or column) start indices.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::GetPtr</a>()<span class="keyword"> const</span>
<a name="l00993"></a>00993 <span class="keyword">  </span>{
<a name="l00994"></a>00994     <span class="keywordflow">return</span> ptr_;
<a name="l00995"></a>00995   }
<a name="l00996"></a>00996 
<a name="l00997"></a>00997 
<a name="l00999"></a>00999 
<a name="l01006"></a>01006   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01007"></a>01007   <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___sparse.php#a1378a5e93b30065896f0d84b07b18caa" title="Returns (row or column) indices of non-zero entries.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::GetInd</a>()<span class="keyword"> const</span>
<a name="l01008"></a>01008 <span class="keyword">  </span>{
<a name="l01009"></a>01009     <span class="keywordflow">return</span> ind_;
<a name="l01010"></a>01010   }
<a name="l01011"></a>01011 
<a name="l01012"></a>01012 
<a name="l01014"></a>01014 
<a name="l01017"></a>01017   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01018"></a>01018   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a0c3409f0fec69fb357c493154e5d763b" title="Returns the length of the array of start indices.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::GetPtrSize</a>()<span class="keyword"> const</span>
<a name="l01019"></a>01019 <span class="keyword">  </span>{
<a name="l01020"></a>01020     <span class="keywordflow">return</span> (Storage::GetFirst(this-&gt;m_, this-&gt;n_) + 1);
<a name="l01021"></a>01021   }
<a name="l01022"></a>01022 
<a name="l01023"></a>01023 
<a name="l01025"></a>01025 
<a name="l01033"></a>01033   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01034"></a>01034   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a7b136934dfbe364dd016ac33e4a5dcc6" title="Returns the length of the array of (column or row) indices.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::GetIndSize</a>()<span class="keyword"> const</span>
<a name="l01035"></a>01035 <span class="keyword">  </span>{
<a name="l01036"></a>01036     <span class="keywordflow">return</span> nz_;
<a name="l01037"></a>01037   }
<a name="l01038"></a>01038 
<a name="l01039"></a>01039 
<a name="l01040"></a>01040   <span class="comment">/**********************************</span>
<a name="l01041"></a>01041 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l01042"></a>01042 <span class="comment">   **********************************/</span>
<a name="l01043"></a>01043 
<a name="l01044"></a>01044 
<a name="l01046"></a>01046 
<a name="l01052"></a>01052   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01053"></a>01053   <span class="keyword">inline</span> <span class="keyword">typename</span> Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::value_type
<a name="l01054"></a>01054   <a class="code" href="class_seldon_1_1_matrix___sparse.php#a019d20b720889f75c4c2314b40e86954" title="Access operator.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l01055"></a>01055 <span class="keyword">  </span>{
<a name="l01056"></a>01056 
<a name="l01057"></a>01057 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01058"></a>01058 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01059"></a>01059       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;Matrix_Sparse::operator()&quot;</span>,
<a name="l01060"></a>01060                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l01061"></a>01061                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01062"></a>01062     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01063"></a>01063       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;Matrix_Sparse::operator()&quot;</span>,
<a name="l01064"></a>01064                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l01065"></a>01065                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01066"></a>01066 <span class="preprocessor">#endif</span>
<a name="l01067"></a>01067 <span class="preprocessor"></span>
<a name="l01068"></a>01068     <span class="keywordtype">int</span> k,l;
<a name="l01069"></a>01069     <span class="keywordtype">int</span> a,b;
<a name="l01070"></a>01070 
<a name="l01071"></a>01071     a = ptr_[Storage::GetFirst(i, j)];
<a name="l01072"></a>01072     b = ptr_[Storage::GetFirst(i, j) + 1];
<a name="l01073"></a>01073 
<a name="l01074"></a>01074     <span class="keywordflow">if</span> (a == b)
<a name="l01075"></a>01075       <span class="keywordflow">return</span> T(0);
<a name="l01076"></a>01076 
<a name="l01077"></a>01077     l = Storage::GetSecond(i, j);
<a name="l01078"></a>01078 
<a name="l01079"></a>01079     <span class="keywordflow">for</span> (k = a; (k &lt; b-1) &amp;&amp; (ind_[k] &lt; l); k++);
<a name="l01080"></a>01080 
<a name="l01081"></a>01081     <span class="keywordflow">if</span> (ind_[k] == l)
<a name="l01082"></a>01082       <span class="keywordflow">return</span> this-&gt;data_[k];
<a name="l01083"></a>01083     <span class="keywordflow">else</span>
<a name="l01084"></a>01084       <span class="keywordflow">return</span> T(0);
<a name="l01085"></a>01085   }
<a name="l01086"></a>01086 
<a name="l01087"></a>01087 
<a name="l01089"></a>01089 
<a name="l01097"></a>01097   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01098"></a>01098   <span class="keyword">inline</span> <span class="keyword">typename</span> Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::value_type&amp;
<a name="l01099"></a>01099   <a class="code" href="class_seldon_1_1_matrix___sparse.php#a7f416147b6680860cd489dfa6cb08b58" title="Access method.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01100"></a>01100   {
<a name="l01101"></a>01101 
<a name="l01102"></a>01102 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01103"></a>01103 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01104"></a>01104       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;Matrix_Sparse::Val(int, int)&quot;</span>,
<a name="l01105"></a>01105                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l01106"></a>01106                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01107"></a>01107     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01108"></a>01108       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;Matrix_Sparse::Val(int, int)&quot;</span>,
<a name="l01109"></a>01109                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l01110"></a>01110                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01111"></a>01111 <span class="preprocessor">#endif</span>
<a name="l01112"></a>01112 <span class="preprocessor"></span>
<a name="l01113"></a>01113     <span class="keywordtype">int</span> k, l;
<a name="l01114"></a>01114     <span class="keywordtype">int</span> a, b;
<a name="l01115"></a>01115 
<a name="l01116"></a>01116     a = ptr_[Storage::GetFirst(i, j)];
<a name="l01117"></a>01117     b = ptr_[Storage::GetFirst(i, j) + 1];
<a name="l01118"></a>01118 
<a name="l01119"></a>01119     <span class="keywordflow">if</span> (a == b)
<a name="l01120"></a>01120       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Matrix_Sparse::Val(int, int)&quot;</span>,
<a name="l01121"></a>01121                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l01122"></a>01122                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l01123"></a>01123                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l01124"></a>01124 
<a name="l01125"></a>01125     l = Storage::GetSecond(i, j);
<a name="l01126"></a>01126 
<a name="l01127"></a>01127     <span class="keywordflow">for</span> (k = a; (k &lt; b-1) &amp;&amp; (ind_[k] &lt; l); k++);
<a name="l01128"></a>01128 
<a name="l01129"></a>01129     <span class="keywordflow">if</span> (ind_[k] == l)
<a name="l01130"></a>01130       <span class="keywordflow">return</span> this-&gt;data_[k];
<a name="l01131"></a>01131     <span class="keywordflow">else</span>
<a name="l01132"></a>01132       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Matrix_Sparse::Val(int, int)&quot;</span>,
<a name="l01133"></a>01133                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l01134"></a>01134                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l01135"></a>01135                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l01136"></a>01136   }
<a name="l01137"></a>01137 
<a name="l01138"></a>01138 
<a name="l01140"></a>01140 
<a name="l01148"></a>01148   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01149"></a>01149   <span class="keyword">inline</span>
<a name="l01150"></a>01150   <span class="keyword">const</span> <span class="keyword">typename</span> Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::value_type&amp;
<a name="l01151"></a>01151   <a class="code" href="class_seldon_1_1_matrix___sparse.php#a7f416147b6680860cd489dfa6cb08b58" title="Access method.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l01152"></a>01152 <span class="keyword">  </span>{
<a name="l01153"></a>01153 
<a name="l01154"></a>01154 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01155"></a>01155 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01156"></a>01156       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;Matrix_Sparse::Val(int, int)&quot;</span>,
<a name="l01157"></a>01157                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l01158"></a>01158                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01159"></a>01159     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01160"></a>01160       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;Matrix_Sparse::Val(int, int)&quot;</span>,
<a name="l01161"></a>01161                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l01162"></a>01162                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01163"></a>01163 <span class="preprocessor">#endif</span>
<a name="l01164"></a>01164 <span class="preprocessor"></span>
<a name="l01165"></a>01165     <span class="keywordtype">int</span> k, l;
<a name="l01166"></a>01166     <span class="keywordtype">int</span> a, b;
<a name="l01167"></a>01167 
<a name="l01168"></a>01168     a = ptr_[Storage::GetFirst(i, j)];
<a name="l01169"></a>01169     b = ptr_[Storage::GetFirst(i, j) + 1];
<a name="l01170"></a>01170 
<a name="l01171"></a>01171     <span class="keywordflow">if</span> (a == b)
<a name="l01172"></a>01172       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Matrix_Sparse::Val(int, int)&quot;</span>,
<a name="l01173"></a>01173                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l01174"></a>01174                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l01175"></a>01175                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l01176"></a>01176 
<a name="l01177"></a>01177     l = Storage::GetSecond(i, j);
<a name="l01178"></a>01178 
<a name="l01179"></a>01179     <span class="keywordflow">for</span> (k = a; (k &lt; b-1) &amp;&amp; (ind_[k] &lt; l); k++);
<a name="l01180"></a>01180 
<a name="l01181"></a>01181     <span class="keywordflow">if</span> (ind_[k] == l)
<a name="l01182"></a>01182       <span class="keywordflow">return</span> this-&gt;data_[k];
<a name="l01183"></a>01183     <span class="keywordflow">else</span>
<a name="l01184"></a>01184       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Matrix_Sparse::Val(int, int)&quot;</span>,
<a name="l01185"></a>01185                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l01186"></a>01186                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l01187"></a>01187                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l01188"></a>01188   }
<a name="l01189"></a>01189 
<a name="l01190"></a>01190 
<a name="l01192"></a>01192 
<a name="l01199"></a>01199   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01200"></a>01200   <span class="keyword">inline</span> <span class="keyword">typename</span> Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::value_type&amp;
<a name="l01201"></a>01201   <a class="code" href="class_seldon_1_1_matrix___sparse.php#afe93d17275283fe4740428f833e633e0" title="Access method.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01202"></a>01202   {
<a name="l01203"></a>01203 
<a name="l01204"></a>01204 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01205"></a>01205 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01206"></a>01206       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;Matrix_Sparse::Get(int, int)&quot;</span>,
<a name="l01207"></a>01207                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l01208"></a>01208                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01209"></a>01209     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01210"></a>01210       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;Matrix_Sparse::Get(int, int)&quot;</span>,
<a name="l01211"></a>01211                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l01212"></a>01212                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01213"></a>01213 <span class="preprocessor">#endif</span>
<a name="l01214"></a>01214 <span class="preprocessor"></span>
<a name="l01215"></a>01215     <span class="keywordtype">int</span> k, l;
<a name="l01216"></a>01216     <span class="keywordtype">int</span> a, b;
<a name="l01217"></a>01217 
<a name="l01218"></a>01218     a = ptr_[Storage::GetFirst(i, j)];
<a name="l01219"></a>01219     b = ptr_[Storage::GetFirst(i, j) + 1];
<a name="l01220"></a>01220 
<a name="l01221"></a>01221     <span class="keywordflow">if</span> (a &lt; b)
<a name="l01222"></a>01222       {
<a name="l01223"></a>01223         l = Storage::GetSecond(i, j);
<a name="l01224"></a>01224 
<a name="l01225"></a>01225         <span class="keywordflow">for</span> (k = a; (k &lt; b) &amp;&amp; (ind_[k] &lt; l); k++);
<a name="l01226"></a>01226 
<a name="l01227"></a>01227         <span class="keywordflow">if</span> ( (k &lt; b) &amp;&amp; (ind_[k] == l))
<a name="l01228"></a>01228           <span class="keywordflow">return</span> this-&gt;data_[k];
<a name="l01229"></a>01229       }
<a name="l01230"></a>01230     <span class="keywordflow">else</span>
<a name="l01231"></a>01231       k = a;
<a name="l01232"></a>01232 
<a name="l01233"></a>01233     <span class="comment">// adding a non-zero entry</span>
<a name="l01234"></a>01234     <a class="code" href="class_seldon_1_1_matrix___sparse.php#a84a417a78587e38b7e9caeecb766aa84" title="Reallocates memory to resize the matrix and keeps previous entries.">Resize</a>(this-&gt;m_, this-&gt;n_, nz_+1);
<a name="l01235"></a>01235 
<a name="l01236"></a>01236     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> m = Storage::GetFirst(i, j)+1;
<a name="l01237"></a>01237          m &lt;= Storage::GetFirst(this-&gt;m_, this-&gt;n_); m++)
<a name="l01238"></a>01238       ptr_[m]++;
<a name="l01239"></a>01239 
<a name="l01240"></a>01240     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> m = nz_-1; m &gt;= k+1; m--)
<a name="l01241"></a>01241       {
<a name="l01242"></a>01242         ind_[m] = ind_[m-1];
<a name="l01243"></a>01243         this-&gt;data_[m] = this-&gt;data_[m-1];
<a name="l01244"></a>01244       }
<a name="l01245"></a>01245 
<a name="l01246"></a>01246     ind_[k] = Storage::GetSecond(i, j);
<a name="l01247"></a>01247 
<a name="l01248"></a>01248     <span class="comment">// value of new non-zero entry is set to 0</span>
<a name="l01249"></a>01249     SetComplexZero(this-&gt;data_[k]);
<a name="l01250"></a>01250 
<a name="l01251"></a>01251     <span class="keywordflow">return</span> this-&gt;data_[k];
<a name="l01252"></a>01252   }
<a name="l01253"></a>01253 
<a name="l01254"></a>01254 
<a name="l01256"></a>01256 
<a name="l01261"></a>01261   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01262"></a>01262   <span class="keyword">inline</span> <span class="keyword">const</span> <span class="keyword">typename</span> Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::value_type&amp;
<a name="l01263"></a>01263   <a class="code" href="class_seldon_1_1_matrix___sparse.php#afe93d17275283fe4740428f833e633e0" title="Access method.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l01264"></a>01264 <span class="keyword">  </span>{
<a name="l01265"></a>01265     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a7f416147b6680860cd489dfa6cb08b58" title="Access method.">Val</a>(i, j);
<a name="l01266"></a>01266   }
<a name="l01267"></a>01267 
<a name="l01268"></a>01268 
<a name="l01270"></a>01270 
<a name="l01277"></a>01277   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01278"></a>01278   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a6ede24fc389f58952986463da449bfb0" title="Add a value to a non-zero entry.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01279"></a>01279 <a class="code" href="class_seldon_1_1_matrix___sparse.php#a6ede24fc389f58952986463da449bfb0" title="Add a value to a non-zero entry.">  ::AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; val)
<a name="l01280"></a>01280   {
<a name="l01281"></a>01281     Get(i, j) += val;
<a name="l01282"></a>01282   }
<a name="l01283"></a>01283 
<a name="l01284"></a>01284 
<a name="l01286"></a>01286 
<a name="l01291"></a>01291   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01292"></a>01292   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a059694445d9f150c96fa35c53f05dbe8" title="Sets an element (i, j) to a value.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01293"></a>01293 <a class="code" href="class_seldon_1_1_matrix___sparse.php#a059694445d9f150c96fa35c53f05dbe8" title="Sets an element (i, j) to a value.">  ::Set</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; val)
<a name="l01294"></a>01294   {
<a name="l01295"></a>01295     Get(i, j) = val;
<a name="l01296"></a>01296   }
<a name="l01297"></a>01297 
<a name="l01298"></a>01298 
<a name="l01300"></a>01300 
<a name="l01305"></a>01305   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01306"></a>01306   <span class="keyword">inline</span> Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;&amp;
<a name="l01307"></a>01307   <a class="code" href="class_seldon_1_1_matrix___sparse.php#a58f047d2d62a3e5e7f7feaae98cd7d7f" title="Duplicates a matrix (assignment operator).">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01308"></a>01308 <a class="code" href="class_seldon_1_1_matrix___sparse.php#a58f047d2d62a3e5e7f7feaae98cd7d7f" title="Duplicates a matrix (assignment operator).">  ::operator= </a>(<span class="keyword">const</span> Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;&amp; A)
<a name="l01309"></a>01309   {
<a name="l01310"></a>01310     this-&gt;Copy(A);
<a name="l01311"></a>01311 
<a name="l01312"></a>01312     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01313"></a>01313   }
<a name="l01314"></a>01314 
<a name="l01315"></a>01315 
<a name="l01316"></a>01316   <span class="comment">/************************</span>
<a name="l01317"></a>01317 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l01318"></a>01318 <span class="comment">   ************************/</span>
<a name="l01319"></a><a class="code" href="class_seldon_1_1_matrix___sparse.php#a812985d3c66421787a6dbe687620ea1a">01319</a> 
<a name="l01320"></a>01320 
<a name="l01322"></a>01322 
<a name="l01323"></a>01323   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01324"></a>01324   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a812985d3c66421787a6dbe687620ea1a" title="Resets all non-zero entries to 0-value.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Zero</a>()
<a name="l01325"></a>01325   {
<a name="l01326"></a>01326     this-&gt;allocator_.memoryset(this-&gt;data_, <span class="keywordtype">char</span>(0),
<a name="l01327"></a>01327                                this-&gt;nz_ * <span class="keyword">sizeof</span>(value_type));
<a name="l01328"></a>01328   }
<a name="l01329"></a>01329 
<a name="l01330"></a>01330 
<a name="l01332"></a>01332 
<a name="l01335"></a>01335   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01336"></a>01336   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a657c33784aee4b940c6a635c33f2eb5e" title="Sets the matrix to identity.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::SetIdentity</a>()
<a name="l01337"></a>01337   {
<a name="l01338"></a>01338     <span class="keywordtype">int</span> m = this-&gt;m_;
<a name="l01339"></a>01339     <span class="keywordtype">int</span> n = this-&gt;n_;
<a name="l01340"></a>01340     <span class="keywordtype">int</span> nz = min(m, n);
<a name="l01341"></a>01341 
<a name="l01342"></a>01342     <span class="keywordflow">if</span> (nz == 0)
<a name="l01343"></a>01343       <span class="keywordflow">return</span>;
<a name="l01344"></a>01344 
<a name="l01345"></a>01345     <a class="code" href="class_seldon_1_1_matrix___sparse.php#ab63b8d9aee1a38054cd525d7fa708f1a" title="Clears the matrix.">Clear</a>();
<a name="l01346"></a>01346 
<a name="l01347"></a>01347     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> values(nz);
<a name="l01348"></a>01348     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; ptr(Storage::GetFirst(m, n) + 1);
<a name="l01349"></a>01349     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; ind(nz);
<a name="l01350"></a>01350 
<a name="l01351"></a>01351     T one; SetComplexOne(one);
<a name="l01352"></a>01352     values.Fill(one);
<a name="l01353"></a>01353     ind.Fill();
<a name="l01354"></a>01354     <span class="keywordtype">int</span> i;
<a name="l01355"></a>01355     <span class="keywordflow">for</span> (i = 0; i &lt; nz + 1; i++)
<a name="l01356"></a>01356       ptr(i) = i;
<a name="l01357"></a>01357     <span class="keywordflow">for</span> (i = nz + 1; i &lt; ptr.GetLength(); i++)
<a name="l01358"></a>01358       ptr(i) = nz;
<a name="l01359"></a>01359 
<a name="l01360"></a>01360     <a class="code" href="class_seldon_1_1_matrix___sparse.php#a5b411ce16d320a1442588af33adefe09" title="Redefines the matrix.">SetData</a>(m, n, values, ptr, ind);
<a name="l01361"></a>01361   }
<a name="l01362"></a>01362 
<a name="l01363"></a>01363 
<a name="l01365"></a>01365 
<a name="l01368"></a>01368   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01369"></a>01369   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#abe41180630623f201ea254e009931abd" title="Fills the non-zero entries with 0, 1, 2, ...">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Fill</a>()
<a name="l01370"></a>01370   {
<a name="l01371"></a>01371     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___sparse.php#aac6b06ab623602d24a80d12adf495cf2" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l01372"></a>01372       this-&gt;data_[i] = i;
<a name="l01373"></a>01373   }
<a name="l01374"></a>01374 
<a name="l01375"></a>01375 
<a name="l01377"></a><a class="code" href="class_seldon_1_1_matrix___sparse.php#aa2721edc471c0603919afa9365bee419">01377</a> 
<a name="l01380"></a>01380   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01381"></a>01381   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01382"></a>01382   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#abe41180630623f201ea254e009931abd" title="Fills the non-zero entries with 0, 1, 2, ...">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Fill</a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01383"></a>01383   {
<a name="l01384"></a>01384     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___sparse.php#aac6b06ab623602d24a80d12adf495cf2" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l01385"></a>01385       this-&gt;data_[i] = x;
<a name="l01386"></a>01386   }
<a name="l01387"></a>01387 
<a name="l01388"></a>01388 
<a name="l01390"></a>01390 
<a name="l01393"></a>01393   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01394"></a>01394   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a27b33a9915ce2922d2f3f0a0270b467f" title="Fills the non-zero entries randomly.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::FillRand</a>()
<a name="l01395"></a>01395   {
<a name="l01396"></a>01396     srand(time(NULL));
<a name="l01397"></a>01397     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___sparse.php#aac6b06ab623602d24a80d12adf495cf2" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l01398"></a>01398       this-&gt;data_[i] = rand();
<a name="l01399"></a>01399   }
<a name="l01400"></a>01400 
<a name="l01401"></a>01401 
<a name="l01403"></a>01403 
<a name="l01411"></a>01411   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01412"></a>01412   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a27b33a9915ce2922d2f3f0a0270b467f" title="Fills the non-zero entries randomly.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01413"></a>01413 <a class="code" href="class_seldon_1_1_matrix___sparse.php#a27b33a9915ce2922d2f3f0a0270b467f" title="Fills the non-zero entries randomly.">  ::FillRand</a>(<span class="keywordtype">int</span> Nelement)
<a name="l01414"></a>01414   {
<a name="l01415"></a>01415     <span class="keywordflow">if</span> (this-&gt;m_ == 0 || this-&gt;n_ == 0)
<a name="l01416"></a>01416       <span class="keywordflow">return</span>;
<a name="l01417"></a>01417 
<a name="l01418"></a>01418     Vector&lt;int&gt; i(Nelement), j(Nelement);
<a name="l01419"></a>01419     Vector&lt;T&gt; value(Nelement);
<a name="l01420"></a>01420 
<a name="l01421"></a>01421     set&lt;pair&lt;int, int&gt; &gt; skeleton;
<a name="l01422"></a>01422     set&lt;pair&lt;int, int&gt; &gt;::iterator it;
<a name="l01423"></a>01423 
<a name="l01424"></a>01424     srand(time(NULL));
<a name="l01425"></a>01425 
<a name="l01426"></a>01426     <span class="comment">// generation of triplet (i, j, value)</span>
<a name="l01427"></a>01427     <span class="keywordflow">while</span> (static_cast&lt;int&gt;(skeleton.size()) != Nelement)
<a name="l01428"></a>01428       skeleton.insert(make_pair(rand() % this-&gt;m_, rand() % this-&gt;n_));
<a name="l01429"></a>01429 
<a name="l01430"></a>01430     <span class="keywordtype">int</span> l = 0;
<a name="l01431"></a>01431     <span class="keywordflow">for</span> (it = skeleton.begin(); it != skeleton.end(); it++)
<a name="l01432"></a>01432       {
<a name="l01433"></a>01433         i(l) = it-&gt;first;
<a name="l01434"></a>01434         j(l) = it-&gt;second;
<a name="l01435"></a>01435         value(l) = double(rand());
<a name="l01436"></a>01436         l++;
<a name="l01437"></a>01437       }
<a name="l01438"></a>01438 
<a name="l01439"></a>01439     <span class="comment">// then conversion to current sparse matrix</span>
<a name="l01440"></a>01440     Matrix&lt;T, Prop, Storage, Allocator&gt;&amp; leaf_class =
<a name="l01441"></a>01441       <span class="keyword">static_cast&lt;</span>Matrix&lt;T, Prop, Storage, Allocator&gt;&amp; <span class="keyword">&gt;</span>(*this);
<a name="l01442"></a>01442 
<a name="l01443"></a>01443     <a class="code" href="namespace_seldon.php#ad4977fcff081edc62cc482b1aedaf0d9" title="Conversion from coordinate format to RowSparse.">ConvertMatrix_from_Coordinates</a>(i, j, value, leaf_class, 0);
<a name="l01444"></a>01444   }
<a name="l01445"></a>01445 
<a name="l01446"></a>01446 
<a name="l01448"></a>01448 
<a name="l01458"></a>01458   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01459"></a>01459   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a27b33a9915ce2922d2f3f0a0270b467f" title="Fills the non-zero entries randomly.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01460"></a>01460 <a class="code" href="class_seldon_1_1_matrix___sparse.php#a27b33a9915ce2922d2f3f0a0270b467f" title="Fills the non-zero entries randomly.">  ::FillRand</a>(<span class="keywordtype">int</span> Nelement, <span class="keyword">const</span> T&amp; x)
<a name="l01461"></a>01461   {
<a name="l01462"></a>01462     <span class="keywordflow">if</span> (this-&gt;m_ == 0 || this-&gt;n_ == 0)
<a name="l01463"></a>01463       <span class="keywordflow">return</span>;
<a name="l01464"></a>01464 
<a name="l01465"></a>01465     Vector&lt;int&gt; i(Nelement), j(Nelement);
<a name="l01466"></a>01466     Vector&lt;T&gt; value(Nelement);
<a name="l01467"></a>01467     value.Fill(x);
<a name="l01468"></a>01468 
<a name="l01469"></a>01469     srand(time(NULL));
<a name="l01470"></a>01470     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> l = 0; l &lt; Nelement; l++)
<a name="l01471"></a>01471       {
<a name="l01472"></a>01472         i(l) = rand() % this-&gt;m_;
<a name="l01473"></a>01473         j(l) = rand() % this-&gt;n_;
<a name="l01474"></a>01474       }
<a name="l01475"></a>01475 
<a name="l01476"></a>01476     <span class="comment">// then conversion to current sparse matrix</span>
<a name="l01477"></a>01477     Matrix&lt;T, Prop, Storage, Allocator&gt;&amp; leaf_class =
<a name="l01478"></a>01478       <span class="keyword">static_cast&lt;</span>Matrix&lt;T, Prop, Storage, Allocator&gt;&amp; <span class="keyword">&gt;</span>(*this);
<a name="l01479"></a>01479 
<a name="l01480"></a>01480     <a class="code" href="namespace_seldon.php#ad4977fcff081edc62cc482b1aedaf0d9" title="Conversion from coordinate format to RowSparse.">ConvertMatrix_from_Coordinates</a>(i, j, value, leaf_class, 0);
<a name="l01481"></a>01481   }
<a name="l01482"></a>01482 
<a name="l01483"></a>01483 
<a name="l01485"></a>01485 
<a name="l01490"></a>01490   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01491"></a>01491   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a8d6d48b6f0277f1df2d52b798975407e" title="Displays the matrix on the standard output.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l01492"></a>01492 <span class="keyword">  </span>{
<a name="l01493"></a>01493     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l01494"></a>01494       {
<a name="l01495"></a>01495         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;n_; j++)
<a name="l01496"></a>01496           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l01497"></a>01497         cout &lt;&lt; endl;
<a name="l01498"></a>01498       }
<a name="l01499"></a>01499   }
<a name="l01500"></a>01500 
<a name="l01501"></a>01501 
<a name="l01503"></a>01503 
<a name="l01507"></a>01507   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01508"></a>01508   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#aa0120b7bbfbe15d0a24a1b05b304bc2d" title="Writes the matrix in a file.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01509"></a>01509 <a class="code" href="class_seldon_1_1_matrix___sparse.php#aa0120b7bbfbe15d0a24a1b05b304bc2d" title="Writes the matrix in a file.">  ::Write</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l01510"></a>01510 <span class="keyword">  </span>{
<a name="l01511"></a>01511     ofstream FileStream;
<a name="l01512"></a>01512     FileStream.open(FileName.c_str());
<a name="l01513"></a>01513 
<a name="l01514"></a>01514 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01515"></a>01515 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01516"></a>01516     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01517"></a>01517       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Matrix_Sparse::Write(string FileName)&quot;</span>,
<a name="l01518"></a>01518                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01519"></a>01519 <span class="preprocessor">#endif</span>
<a name="l01520"></a>01520 <span class="preprocessor"></span>
<a name="l01521"></a>01521     this-&gt;Write(FileStream);
<a name="l01522"></a>01522 
<a name="l01523"></a>01523     FileStream.close();
<a name="l01524"></a>01524   }
<a name="l01525"></a>01525 
<a name="l01526"></a>01526 
<a name="l01528"></a>01528 
<a name="l01532"></a>01532   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01533"></a>01533   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#aa0120b7bbfbe15d0a24a1b05b304bc2d" title="Writes the matrix in a file.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01534"></a>01534 <a class="code" href="class_seldon_1_1_matrix___sparse.php#aa0120b7bbfbe15d0a24a1b05b304bc2d" title="Writes the matrix in a file.">  ::Write</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l01535"></a>01535 <span class="keyword">  </span>{
<a name="l01536"></a>01536 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01537"></a>01537 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01538"></a>01538     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01539"></a>01539       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Matrix_Sparse::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l01540"></a>01540                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l01541"></a>01541 <span class="preprocessor">#endif</span>
<a name="l01542"></a>01542 <span class="preprocessor"></span>
<a name="l01543"></a>01543     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l01544"></a>01544                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01545"></a>01545     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;n_)),
<a name="l01546"></a>01546                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01547"></a>01547     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;nz_)),
<a name="l01548"></a>01548                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01549"></a>01549 
<a name="l01550"></a>01550     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;ptr_),
<a name="l01551"></a>01551                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)*(Storage::GetFirst(this-&gt;m_, this-&gt;n_)+1));
<a name="l01552"></a>01552     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;ind_),
<a name="l01553"></a>01553                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)*this-&gt;nz_);
<a name="l01554"></a>01554     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l01555"></a>01555                      <span class="keyword">sizeof</span>(T)*this-&gt;nz_);
<a name="l01556"></a>01556   }
<a name="l01557"></a>01557 
<a name="l01558"></a>01558 
<a name="l01560"></a>01560 
<a name="l01566"></a>01566   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01567"></a>01567   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a45ac6acb158f9b49af3d50a2da34b708" title="Writes the matrix in a file.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l01568"></a>01568 <a class="code" href="class_seldon_1_1_matrix___sparse.php#a45ac6acb158f9b49af3d50a2da34b708" title="Writes the matrix in a file.">  WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l01569"></a>01569 <span class="keyword">  </span>{
<a name="l01570"></a>01570     ofstream FileStream; FileStream.precision(14);
<a name="l01571"></a>01571     FileStream.open(FileName.c_str());
<a name="l01572"></a>01572 
<a name="l01573"></a>01573 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01574"></a>01574 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01575"></a>01575     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01576"></a>01576       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Matrix_Sparse::Write(string FileName)&quot;</span>,
<a name="l01577"></a>01577                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01578"></a>01578 <span class="preprocessor">#endif</span>
<a name="l01579"></a>01579 <span class="preprocessor"></span>
<a name="l01580"></a>01580     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sparse.php#a45ac6acb158f9b49af3d50a2da34b708" title="Writes the matrix in a file.">WriteText</a>(FileStream);
<a name="l01581"></a>01581 
<a name="l01582"></a>01582     FileStream.close();
<a name="l01583"></a>01583   }
<a name="l01584"></a>01584 
<a name="l01585"></a>01585 
<a name="l01587"></a>01587 
<a name="l01593"></a>01593   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01594"></a>01594   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a45ac6acb158f9b49af3d50a2da34b708" title="Writes the matrix in a file.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l01595"></a>01595 <a class="code" href="class_seldon_1_1_matrix___sparse.php#a45ac6acb158f9b49af3d50a2da34b708" title="Writes the matrix in a file.">  WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l01596"></a>01596 <span class="keyword">  </span>{
<a name="l01597"></a>01597 
<a name="l01598"></a>01598 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01599"></a>01599 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01600"></a>01600     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01601"></a>01601       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Matrix_Sparse::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l01602"></a>01602                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l01603"></a>01603 <span class="preprocessor">#endif</span>
<a name="l01604"></a>01604 <span class="preprocessor"></span>
<a name="l01605"></a>01605     <span class="comment">// conversion in coordinate format (1-index convention)</span>
<a name="l01606"></a>01606     IVect IndRow, IndCol; Vector&lt;T&gt; Value;
<a name="l01607"></a>01607     <span class="keyword">const</span> Matrix&lt;T, Prop, Storage, Allocator&gt;&amp; leaf_class =
<a name="l01608"></a>01608       <span class="keyword">static_cast&lt;</span><span class="keyword">const </span>Matrix&lt;T, Prop, Storage, Allocator&gt;&amp; <span class="keyword">&gt;</span>(*this);
<a name="l01609"></a>01609 
<a name="l01610"></a>01610     <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(leaf_class, IndRow, IndCol,
<a name="l01611"></a>01611                                  Value, 1, <span class="keyword">true</span>);
<a name="l01612"></a>01612 
<a name="l01613"></a>01613     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; IndRow.GetM(); i++)
<a name="l01614"></a>01614       FileStream &lt;&lt; IndRow(i) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; IndCol(i) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; Value(i) &lt;&lt; <span class="charliteral">&#39;\n&#39;</span>;
<a name="l01615"></a>01615 
<a name="l01616"></a>01616   }
<a name="l01617"></a>01617 
<a name="l01618"></a>01618 
<a name="l01620"></a>01620 
<a name="l01624"></a>01624   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01625"></a>01625   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#ab3898fc51b20fa37a13cd69a2db3e5a1" title="Reads the matrix from a file.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l01626"></a>01626 <a class="code" href="class_seldon_1_1_matrix___sparse.php#ab3898fc51b20fa37a13cd69a2db3e5a1" title="Reads the matrix from a file.">  Read</a>(<span class="keywordtype">string</span> FileName)
<a name="l01627"></a>01627   {
<a name="l01628"></a>01628     ifstream FileStream;
<a name="l01629"></a>01629     FileStream.open(FileName.c_str());
<a name="l01630"></a>01630 
<a name="l01631"></a>01631 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01632"></a>01632 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01633"></a>01633     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01634"></a>01634       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Matrix_Sparse::Read(string FileName)&quot;</span>,
<a name="l01635"></a>01635                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01636"></a>01636 <span class="preprocessor">#endif</span>
<a name="l01637"></a>01637 <span class="preprocessor"></span>
<a name="l01638"></a>01638     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sparse.php#ab3898fc51b20fa37a13cd69a2db3e5a1" title="Reads the matrix from a file.">Read</a>(FileStream);
<a name="l01639"></a>01639 
<a name="l01640"></a>01640     FileStream.close();
<a name="l01641"></a>01641   }
<a name="l01642"></a>01642 
<a name="l01643"></a>01643 
<a name="l01645"></a>01645 
<a name="l01649"></a>01649   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01650"></a>01650   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#ab3898fc51b20fa37a13cd69a2db3e5a1" title="Reads the matrix from a file.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l01651"></a>01651 <a class="code" href="class_seldon_1_1_matrix___sparse.php#ab3898fc51b20fa37a13cd69a2db3e5a1" title="Reads the matrix from a file.">  Read</a>(istream&amp; FileStream)
<a name="l01652"></a>01652   {
<a name="l01653"></a>01653 
<a name="l01654"></a>01654 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01655"></a>01655 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01656"></a>01656     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01657"></a>01657       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Matrix_Sparse::Read(istream&amp; FileStream)&quot;</span>,
<a name="l01658"></a>01658                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l01659"></a>01659 <span class="preprocessor">#endif</span>
<a name="l01660"></a>01660 <span class="preprocessor"></span>
<a name="l01661"></a>01661     <span class="keywordtype">int</span> m, n, nz;
<a name="l01662"></a>01662     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;m), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01663"></a>01663     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;n), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01664"></a>01664     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;nz), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01665"></a>01665 
<a name="l01666"></a>01666     <a class="code" href="class_seldon_1_1_matrix___sparse.php#a68315328fe25c14b9d8bef181c1c2f49" title="Initialization of an empty sparse matrix with i rows and j columns.">Reallocate</a>(m, n, nz);
<a name="l01667"></a>01667 
<a name="l01668"></a>01668     FileStream.read(reinterpret_cast&lt;char*&gt;(ptr_),
<a name="l01669"></a>01669                     <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)*(Storage::GetFirst(m, n)+1));
<a name="l01670"></a>01670     FileStream.read(reinterpret_cast&lt;char*&gt;(ind_), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)*nz);
<a name="l01671"></a>01671     FileStream.read(reinterpret_cast&lt;char*&gt;(this-&gt;data_), <span class="keyword">sizeof</span>(T)*nz);
<a name="l01672"></a>01672 
<a name="l01673"></a>01673 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01674"></a>01674 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l01675"></a>01675     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01676"></a>01676       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Matrix_Sparse::Read(istream&amp; FileStream)&quot;</span>,
<a name="l01677"></a>01677                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Input operation failed.&quot;</span>)
<a name="l01678"></a>01678                     + string(<span class="stringliteral">&quot; The input file may have been removed&quot;</span>)
<a name="l01679"></a>01679                     + <span class="stringliteral">&quot; or may not contain enough data.&quot;</span>);
<a name="l01680"></a>01680 <span class="preprocessor">#endif</span>
<a name="l01681"></a>01681 <span class="preprocessor"></span>
<a name="l01682"></a>01682   }
<a name="l01683"></a>01683 
<a name="l01684"></a>01684 
<a name="l01686"></a>01686 
<a name="l01690"></a>01690   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01691"></a>01691   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a6a2bad0b67f1410249057b24a75afcea" title="Reads the matrix from a file.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l01692"></a>01692 <a class="code" href="class_seldon_1_1_matrix___sparse.php#a6a2bad0b67f1410249057b24a75afcea" title="Reads the matrix from a file.">  ReadText</a>(<span class="keywordtype">string</span> FileName)
<a name="l01693"></a>01693   {
<a name="l01694"></a>01694     ifstream FileStream;
<a name="l01695"></a>01695     FileStream.open(FileName.c_str());
<a name="l01696"></a>01696 
<a name="l01697"></a>01697 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01698"></a>01698 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01699"></a>01699     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01700"></a>01700       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Matrix_Sparse::ReadText(string FileName)&quot;</span>,
<a name="l01701"></a>01701                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01702"></a>01702 <span class="preprocessor">#endif</span>
<a name="l01703"></a>01703 <span class="preprocessor"></span>
<a name="l01704"></a>01704     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sparse.php#a6a2bad0b67f1410249057b24a75afcea" title="Reads the matrix from a file.">ReadText</a>(FileStream);
<a name="l01705"></a>01705 
<a name="l01706"></a>01706     FileStream.close();
<a name="l01707"></a>01707   }
<a name="l01708"></a>01708 
<a name="l01709"></a>01709 
<a name="l01711"></a>01711 
<a name="l01715"></a>01715   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01716"></a>01716   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a6a2bad0b67f1410249057b24a75afcea" title="Reads the matrix from a file.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l01717"></a>01717 <a class="code" href="class_seldon_1_1_matrix___sparse.php#a6a2bad0b67f1410249057b24a75afcea" title="Reads the matrix from a file.">  ReadText</a>(istream&amp; FileStream)
<a name="l01718"></a>01718   {
<a name="l01719"></a>01719     Matrix&lt;T, Prop, Storage, Allocator&gt;&amp; leaf_class =
<a name="l01720"></a>01720       <span class="keyword">static_cast&lt;</span>Matrix&lt;T, Prop, Storage, Allocator&gt;&amp; <span class="keyword">&gt;</span>(*this);
<a name="l01721"></a>01721 
<a name="l01722"></a>01722     T zero; <span class="keywordtype">int</span> index = 1;
<a name="l01723"></a>01723     <a class="code" href="namespace_seldon.php#af8428b13721ad15b95a74ecf07d23668" title="Reading of matrix in coordinate format.">ReadCoordinateMatrix</a>(leaf_class, FileStream, zero, index);
<a name="l01724"></a>01724   }
<a name="l01725"></a>01725 
<a name="l01726"></a>01726 
<a name="l01728"></a>01728   <span class="comment">// MATRIX&lt;COLSPARSE&gt; //</span>
<a name="l01730"></a>01730 <span class="comment"></span>
<a name="l01731"></a>01731 
<a name="l01732"></a>01732   <span class="comment">/****************</span>
<a name="l01733"></a>01733 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01734"></a>01734 <span class="comment">   ****************/</span>
<a name="l01735"></a>01735 
<a name="l01737"></a>01737 
<a name="l01740"></a>01740   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01741"></a>01741   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sparse_00_01_allocator_01_4.php#a02ccefd04db099e355bf432daf2b8d0b" title="Default constructor.">Matrix&lt;T, Prop, ColSparse, Allocator&gt;::Matrix</a>():
<a name="l01742"></a>01742     Matrix_Sparse&lt;T, Prop, ColSparse, Allocator&gt;()
<a name="l01743"></a>01743   {
<a name="l01744"></a>01744   }
<a name="l01745"></a>01745 
<a name="l01746"></a>01746 
<a name="l01748"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sparse_00_01_allocator_01_4.php#a6b27ce89da3078a901e6b3123c78b9a0">01748</a> 
<a name="l01752"></a>01752   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01753"></a>01753   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01754"></a>01754     <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">Matrix_Sparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_sparse.php">ColSparse</a>, Allocator&gt;(i, j, 0)
<a name="l01755"></a>01755   {
<a name="l01756"></a>01756   }
<a name="l01757"></a>01757 
<a name="l01758"></a>01758 
<a name="l01760"></a>01760 
<a name="l01766"></a>01766   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01767"></a>01767   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> nz):
<a name="l01768"></a>01768     <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">Matrix_Sparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_sparse.php">ColSparse</a>, Allocator&gt;(i, j, nz)
<a name="l01769"></a>01769   {
<a name="l01770"></a>01770   }
<a name="l01771"></a>01771 
<a name="l01772"></a>01772 
<a name="l01774"></a>01774 
<a name="l01785"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sparse_00_01_allocator_01_4.php#aeb00bb87b12cfa5a7982da11100ef81f">01785</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01786"></a>01786   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01787"></a>01787             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01788"></a>01788             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01789"></a>01789   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSparse, Allocator&gt;::</a>
<a name="l01790"></a>01790 <a class="code" href="class_seldon_1_1_matrix.php">  Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l01791"></a>01791          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; values,
<a name="l01792"></a>01792          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; ptr,
<a name="l01793"></a>01793          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; ind):
<a name="l01794"></a>01794     <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">Matrix_Sparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_sparse.php">ColSparse</a>, Allocator&gt;(i, j, values, ptr, ind)
<a name="l01795"></a>01795   {
<a name="l01796"></a>01796   }
<a name="l01797"></a>01797 
<a name="l01798"></a>01798 
<a name="l01799"></a>01799 
<a name="l01801"></a>01801   <span class="comment">// MATRIX&lt;ROWSPARSE&gt; //</span>
<a name="l01803"></a>01803 <span class="comment"></span>
<a name="l01804"></a>01804 
<a name="l01805"></a>01805   <span class="comment">/****************</span>
<a name="l01806"></a>01806 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01807"></a>01807 <span class="comment">   ****************/</span>
<a name="l01808"></a>01808 
<a name="l01810"></a>01810 
<a name="l01813"></a>01813   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01814"></a>01814   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSparse, Allocator&gt;::Matrix</a>():
<a name="l01815"></a>01815     <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">Matrix_Sparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator&gt;()
<a name="l01816"></a>01816   {
<a name="l01817"></a>01817   }
<a name="l01818"></a>01818 
<a name="l01819"></a>01819 
<a name="l01821"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sparse_00_01_allocator_01_4.php#a710d1a0338e3a1f80b9d4d75664abe38">01821</a> 
<a name="l01825"></a>01825   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01826"></a>01826   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01827"></a>01827     <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">Matrix_Sparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator&gt;(i, j, 0)
<a name="l01828"></a>01828   {
<a name="l01829"></a>01829   }
<a name="l01830"></a>01830 
<a name="l01831"></a>01831 
<a name="l01833"></a>01833 
<a name="l01839"></a>01839   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01840"></a>01840   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> nz):
<a name="l01841"></a>01841     <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">Matrix_Sparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator&gt;(i, j, nz)
<a name="l01842"></a>01842   {
<a name="l01843"></a>01843   }
<a name="l01844"></a>01844 
<a name="l01845"></a>01845 
<a name="l01847"></a>01847 
<a name="l01858"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sparse_00_01_allocator_01_4.php#a4d328f6bb80941304ae3f1948155ff6e">01858</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01859"></a>01859   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01860"></a>01860             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01861"></a>01861             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01862"></a>01862   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSparse, Allocator&gt;::</a>
<a name="l01863"></a>01863 <a class="code" href="class_seldon_1_1_matrix.php">  Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l01864"></a>01864          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; values,
<a name="l01865"></a>01865          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; ptr,
<a name="l01866"></a>01866          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; ind):
<a name="l01867"></a>01867     <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">Matrix_Sparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator&gt;(i, j, values, ptr, ind)
<a name="l01868"></a>01868   {
<a name="l01869"></a>01869   }
<a name="l01870"></a>01870 
<a name="l01871"></a>01871 
<a name="l01873"></a>01873   <span class="comment">// ReadCoordinateMatrix //</span>
<a name="l01875"></a>01875 <span class="comment"></span>
<a name="l01876"></a>01876 
<a name="l01878"></a>01878 
<a name="l01884"></a>01884   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T<span class="keywordtype">int</span>, <span class="keyword">class</span> AllocInt, <span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l01885"></a>01885   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#af8428b13721ad15b95a74ecf07d23668" title="Reading of matrix in coordinate format.">ReadCoordinateMatrix</a>(istream&amp; FileStream,
<a name="l01886"></a>01886                             <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Tint, VectFull, AllocInt&gt;</a>&amp; row_numbers,
<a name="l01887"></a>01887                             <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Tint, VectFull, AllocInt&gt;</a>&amp; col_numbers,
<a name="l01888"></a>01888                             <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a>&amp; values)
<a name="l01889"></a>01889   {
<a name="l01890"></a>01890 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01891"></a>01891 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01892"></a>01892     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01893"></a>01893       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;ReadCoordinateMatrix&quot;</span>, <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l01894"></a>01894 <span class="preprocessor">#endif</span>
<a name="l01895"></a>01895 <span class="preprocessor"></span>
<a name="l01896"></a>01896     T entry; <span class="keywordtype">int</span> row = 0, col = 0;
<a name="l01897"></a>01897     <span class="keywordtype">int</span> nb_elt = 0;
<a name="l01898"></a>01898     SetComplexZero(entry);
<a name="l01899"></a>01899 
<a name="l01900"></a>01900     <span class="keywordflow">while</span> (!FileStream.eof())
<a name="l01901"></a>01901       {
<a name="l01902"></a>01902         <span class="comment">// new entry is read (1-index)</span>
<a name="l01903"></a>01903         FileStream &gt;&gt; row &gt;&gt; col &gt;&gt; entry;
<a name="l01904"></a>01904 
<a name="l01905"></a>01905         <span class="keywordflow">if</span> (FileStream.fail())
<a name="l01906"></a>01906           <span class="keywordflow">break</span>;
<a name="l01907"></a>01907         <span class="keywordflow">else</span>
<a name="l01908"></a>01908           {
<a name="l01909"></a>01909 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01910"></a>01910 <span class="preprocessor"></span>            <span class="keywordflow">if</span> (row &lt; 1)
<a name="l01911"></a>01911               <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;ReadCoordinateMatrix&quot;</span>),
<a name="l01912"></a>01912                             <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Error : Row number should be greater &quot;</span>)
<a name="l01913"></a>01913                             + <span class="stringliteral">&quot;than 0 but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(row));
<a name="l01914"></a>01914 
<a name="l01915"></a>01915             <span class="keywordflow">if</span> (col &lt; 1)
<a name="l01916"></a>01916               <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;ReadCoordinateMatrix&quot;</span>),
<a name="l01917"></a>01917                             <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Error : Column number should be greater&quot;</span>)
<a name="l01918"></a>01918                             + <span class="stringliteral">&quot; than 0 but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(col));
<a name="l01919"></a>01919 <span class="preprocessor">#endif</span>
<a name="l01920"></a>01920 <span class="preprocessor"></span>
<a name="l01921"></a>01921             nb_elt++;
<a name="l01922"></a>01922 
<a name="l01923"></a>01923             <span class="comment">// inserting new element</span>
<a name="l01924"></a>01924             <span class="keywordflow">if</span> (nb_elt &gt; values.<a class="code" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae" title="Returns the number of elements.">GetM</a>())
<a name="l01925"></a>01925               {
<a name="l01926"></a>01926                 values.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a99dda1b202ce562645f890897c593515" title="Changes the length of the vector, and keeps previous values.">Resize</a>(2*nb_elt);
<a name="l01927"></a>01927                 row_numbers.Resize(2*nb_elt);
<a name="l01928"></a>01928                 col_numbers.Resize(2*nb_elt);
<a name="l01929"></a>01929               }
<a name="l01930"></a>01930 
<a name="l01931"></a>01931             values(nb_elt-1) = entry;
<a name="l01932"></a>01932             row_numbers(nb_elt-1) = row;
<a name="l01933"></a>01933             col_numbers(nb_elt-1) = col;
<a name="l01934"></a>01934           }
<a name="l01935"></a>01935       }
<a name="l01936"></a>01936 
<a name="l01937"></a>01937     <span class="keywordflow">if</span> (nb_elt &gt; 0)
<a name="l01938"></a>01938       {
<a name="l01939"></a>01939         row_numbers.Resize(nb_elt);
<a name="l01940"></a>01940         col_numbers.Resize(nb_elt);
<a name="l01941"></a>01941         values.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a99dda1b202ce562645f890897c593515" title="Changes the length of the vector, and keeps previous values.">Resize</a>(nb_elt);
<a name="l01942"></a>01942       }
<a name="l01943"></a>01943   }
<a name="l01944"></a>01944 
<a name="l01945"></a>01945 
<a name="l01947"></a>01947 
<a name="l01956"></a>01956   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Matrix1, <span class="keyword">class</span> T&gt;
<a name="l01957"></a>01957   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#af8428b13721ad15b95a74ecf07d23668" title="Reading of matrix in coordinate format.">ReadCoordinateMatrix</a>(Matrix1&amp; A, istream&amp; FileStream, T&amp; zero,
<a name="l01958"></a>01958                             <span class="keywordtype">int</span> index, <span class="keywordtype">int</span> nnz)
<a name="l01959"></a>01959   {
<a name="l01960"></a>01960     <span class="comment">// previous elements are removed</span>
<a name="l01961"></a>01961     A.Clear();
<a name="l01962"></a>01962 
<a name="l01963"></a>01963     Vector&lt;int&gt; row_numbers, col_numbers;
<a name="l01964"></a>01964     Vector&lt;T&gt; values;
<a name="l01965"></a>01965     <span class="keywordflow">if</span> (nnz &gt;= 0)
<a name="l01966"></a>01966       {
<a name="l01967"></a>01967         values.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a6c1667ecf1265c0870ba2828b33a2d79" title="Vector reallocation.">Reallocate</a>(nnz);
<a name="l01968"></a>01968         row_numbers.Reallocate(nnz);
<a name="l01969"></a>01969         col_numbers.Reallocate(nnz);
<a name="l01970"></a>01970       }
<a name="l01971"></a>01971 
<a name="l01972"></a>01972     <a class="code" href="namespace_seldon.php#af8428b13721ad15b95a74ecf07d23668" title="Reading of matrix in coordinate format.">ReadCoordinateMatrix</a>(FileStream, row_numbers, col_numbers, values);
<a name="l01973"></a>01973 
<a name="l01974"></a>01974     <span class="keywordflow">if</span> (row_numbers.GetM() &gt; 0)
<a name="l01975"></a>01975       <a class="code" href="namespace_seldon.php#ad4977fcff081edc62cc482b1aedaf0d9" title="Conversion from coordinate format to RowSparse.">ConvertMatrix_from_Coordinates</a>(row_numbers, col_numbers, values,
<a name="l01976"></a>01976                                      A, index);
<a name="l01977"></a>01977 
<a name="l01978"></a>01978   }
<a name="l01979"></a>01979 
<a name="l01980"></a>01980 } <span class="comment">// namespace Seldon.</span>
<a name="l01981"></a>01981 
<a name="l01982"></a>01982 <span class="preprocessor">#define SELDON_FILE_MATRIX_SPARSE_CXX</span>
<a name="l01983"></a>01983 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
