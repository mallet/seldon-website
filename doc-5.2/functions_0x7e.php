<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="tabs2">
    <ul class="tablist">
      <li class="current"><a href="functions.php"><span>All</span></a></li>
      <li><a href="functions_func.php"><span>Functions</span></a></li>
      <li><a href="functions_vars.php"><span>Variables</span></a></li>
      <li><a href="functions_type.php"><span>Typedefs</span></a></li>
    </ul>
  </div>
  <div class="tabs3">
    <ul class="tablist">
      <li><a href="functions.php#index_a"><span>a</span></a></li>
      <li><a href="functions_0x63.php#index_c"><span>c</span></a></li>
      <li><a href="functions_0x64.php#index_d"><span>d</span></a></li>
      <li><a href="functions_0x65.php#index_e"><span>e</span></a></li>
      <li><a href="functions_0x66.php#index_f"><span>f</span></a></li>
      <li><a href="functions_0x67.php#index_g"><span>g</span></a></li>
      <li><a href="functions_0x68.php#index_h"><span>h</span></a></li>
      <li><a href="functions_0x69.php#index_i"><span>i</span></a></li>
      <li><a href="functions_0x6c.php#index_l"><span>l</span></a></li>
      <li><a href="functions_0x6d.php#index_m"><span>m</span></a></li>
      <li><a href="functions_0x6e.php#index_n"><span>n</span></a></li>
      <li><a href="functions_0x6f.php#index_o"><span>o</span></a></li>
      <li><a href="functions_0x70.php#index_p"><span>p</span></a></li>
      <li><a href="functions_0x72.php#index_r"><span>r</span></a></li>
      <li><a href="functions_0x73.php#index_s"><span>s</span></a></li>
      <li><a href="functions_0x74.php#index_t"><span>t</span></a></li>
      <li><a href="functions_0x75.php#index_u"><span>u</span></a></li>
      <li><a href="functions_0x76.php#index_v"><span>v</span></a></li>
      <li><a href="functions_0x77.php#index_w"><span>w</span></a></li>
      <li><a href="functions_0x78.php#index_x"><span>x</span></a></li>
      <li><a href="functions_0x7a.php#index_z"><span>z</span></a></li>
      <li class="current"><a href="functions_0x7e.php#index_~"><span>~</span></a></li>
    </ul>
  </div>
<div class="contents">
Here is a list of all documented class members with links to the class documentation for each member:

<h3><a class="anchor" id="index_0x7e"></a>- ~ -</h3><ul>
<li>~Array3D()
: <a class="el" href="class_seldon_1_1_array3_d.php#a0acf50b4c510976bdde062f03787f06b">Seldon::Array3D&lt; T, Allocator &gt;</a>
</li>
<li>~Error()
: <a class="el" href="class_seldon_1_1_error.php#a82007af97b6bc0fd14de04f165e07497">Seldon::Error</a>
</li>
<li>~HeterogeneousMatrixCollection()
: <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aeec9a8f4343637e860a9dc627d60ed89">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
</li>
<li>~Matrix()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_sub_storage_3_01_m_01_4_00_01_allocator_01_4.php#ad5b939c348576cbd5f2bf1e6168dfa05">Seldon::Matrix&lt; T, Prop, SubStorage&lt; M &gt;, Allocator &gt;</a>
</li>
<li>~Matrix_ArrayComplexSparse()
: <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#acec6f5d4aa6488c9389a2065944568b0">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>~Matrix_ArraySparse()
: <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#adf298ef8550ee9681a384a539802736f">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>~Matrix_Base()
: <a class="el" href="class_seldon_1_1_matrix___base.php#af34a03c0cc56f757a83cd56f97c74ed8">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>
</li>
<li>~Matrix_ComplexSparse()
: <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a3b785e50c55d9c0e00180bc7d3332d75">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>~Matrix_Hermitian()
: <a class="el" href="class_seldon_1_1_matrix___hermitian.php#aa31430150e01ad4acc8b2a8e5e55e5c3">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>~Matrix_HermPacked()
: <a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a2d9134dbc8606170ccaaf6aed472df26">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>~Matrix_Pointers()
: <a class="el" href="class_seldon_1_1_matrix___pointers.php#adc705f0d17fc4d5676e39b1b8f2b08ca">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>~Matrix_Sparse()
: <a class="el" href="class_seldon_1_1_matrix___sparse.php#a805ce704fffb3c509d908ec3ac45db28">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>~Matrix_SymComplexSparse()
: <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#afcf6a5d81a59ad003dfa7f905e0b5aaa">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>~Matrix_Symmetric()
: <a class="el" href="class_seldon_1_1_matrix___symmetric.php#ae3c7bcbdfae4e4240505bd3ad72df274">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>~Matrix_SymPacked()
: <a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a5b43b96986b649ecab6baa564f58068f">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>~Matrix_SymSparse()
: <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a6f46818c1d832ff9be950a5b6f0fc9c7">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>~Matrix_TriangPacked()
: <a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a3e4295fa0a1e9a2f3eba8bdda1221565">Seldon::Matrix_TriangPacked&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>~Matrix_Triangular()
: <a class="el" href="class_seldon_1_1_matrix___triangular.php#a1bf20e38fe0a64a4db93991819fba679">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>~MatrixCollection()
: <a class="el" href="class_seldon_1_1_matrix_collection.php#acd85176ecf48145fcb40621d4e7401e8">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>~MatrixMumps()
: <a class="el" href="class_seldon_1_1_matrix_mumps.php#af9f7189c4f700303976d3f2ffd13945a">Seldon::MatrixMumps&lt; T &gt;</a>
</li>
<li>~MatrixPastix()
: <a class="el" href="class_seldon_1_1_matrix_pastix.php#a06e7866292ac5b30289cf566d56b0caf">Seldon::MatrixPastix&lt; T &gt;</a>
</li>
<li>~MatrixSuperLU_Base()
: <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a8f0a2adf458ec4f187f3936fea612106">Seldon::MatrixSuperLU_Base&lt; T &gt;</a>
</li>
<li>~MatrixUmfPack()
: <a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#aa660d4563cf769365f1121761f7db67f">Seldon::MatrixUmfPack&lt; double &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a2cc80beb8d893f7b1b8e68872de1290a">Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;</a>
</li>
<li>~NLoptSolver()
: <a class="el" href="class_seldon_1_1_n_lopt_solver.php#adcf818c843283a20eef4d17a974fd6cc">Seldon::NLoptSolver</a>
</li>
<li>~PetscMatrix()
: <a class="el" href="class_seldon_1_1_petsc_matrix.php#a791babbea4d834e8559e316e7605dbbc">Seldon::PetscMatrix&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>~PETScVector()
: <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#ae416e60119dccc2e4f8d2ee949290ccc">Seldon::PETScVector&lt; T, Allocator &gt;</a>
</li>
<li>~SubMatrix_Base()
: <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a2142c411e3e6b939ef7bc99ed6b4a7f9">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>
</li>
<li>~Vector()
: <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#af0eb42557b7578cfefabbf4d15ad2073">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a89672db2ad89fd1e27987790ec0e0561">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#aea88a28edc2030920f93f83ac1f4952f">Seldon::Vector&lt; T, PETScPar, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#afe4b7f7be7d9dedb8e1275f283706c43">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#aeee7fe1b1be642339c63037d1245334b">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a8305af2f1806473a28c7fb71d8189cd2">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a>
</li>
<li>~Vector2()
: <a class="el" href="class_seldon_1_1_vector2.php#a07190b6e16f512ee68feddf636c2698d">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
</li>
<li>~Vector3()
: <a class="el" href="class_seldon_1_1_vector3.php#ae85c3b686a4100a5c05dc7d82a4b6b80">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
</li>
<li>~Vector_Base()
: <a class="el" href="class_seldon_1_1_vector___base.php#a6da510796ebef867765ea3a037debb1b">Seldon::Vector_Base&lt; T, Allocator &gt;</a>
</li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
