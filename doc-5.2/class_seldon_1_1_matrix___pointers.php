<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a> &#124;
<a href="#pro-static-attribs">Static Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Matrix_Pointers" --><!-- doxytag: inherits="Seldon::Matrix_Base" -->
<p>Full matrix class.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_matrix___pointers.png" usemap="#Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;_map" alt=""/>
  <map id="Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;_map" name="Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;_map">
<area href="class_seldon_1_1_matrix___base.php" alt="Seldon::Matrix_Base&lt; T, Allocator &gt;" shape="rect" coords="0,0,325,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_matrix___pointers-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4a16358af4507cc560f34353cfe26b5a"></a><!-- doxytag: member="Seldon::Matrix_Pointers::value_type" ref="a4a16358af4507cc560f34353cfe26b5a" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a91492e35ee7cd936e2b9db024c3a994d"></a><!-- doxytag: member="Seldon::Matrix_Pointers::pointer" ref="a91492e35ee7cd936e2b9db024c3a994d" args="" -->
typedef Allocator::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a90820ccdb63acabd4f2e0bcdf1b81bff"></a><!-- doxytag: member="Seldon::Matrix_Pointers::const_pointer" ref="a90820ccdb63acabd4f2e0bcdf1b81bff" args="" -->
typedef Allocator::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afd9fad8a481da28a1e0f8d679206fe03"></a><!-- doxytag: member="Seldon::Matrix_Pointers::reference" ref="afd9fad8a481da28a1e0f8d679206fe03" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7d8aac45bb068ca6560103564b3d9aa1"></a><!-- doxytag: member="Seldon::Matrix_Pointers::const_reference" ref="a7d8aac45bb068ca6560103564b3d9aa1" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1f247aba29d840e5cceed5561c12be2d"></a><!-- doxytag: member="Seldon::Matrix_Pointers::entry_type" ref="a1f247aba29d840e5cceed5561c12be2d" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>entry_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5736f8e9940b1a0b770ee53eab406fb0"></a><!-- doxytag: member="Seldon::Matrix_Pointers::access_type" ref="a5736f8e9940b1a0b770ee53eab406fb0" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>access_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3c5dec5d3b1695386ab9e1a255dd3238"></a><!-- doxytag: member="Seldon::Matrix_Pointers::const_access_type" ref="a3c5dec5d3b1695386ab9e1a255dd3238" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_access_type</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a5f99e5a5a2f9a8f8e7873f829d82ed33">Matrix_Pointers</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#a5f99e5a5a2f9a8f8e7873f829d82ed33"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a70433b5aff5b052203136dd43783ac7d">Matrix_Pointers</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Main constructor.  <a href="#a70433b5aff5b052203136dd43783ac7d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad8f9783bed7ab3a8a3c781b8bab3ce62"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Matrix_Pointers" ref="ad8f9783bed7ab3a8a3c781b8bab3ce62" args="(const Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt; &amp;A)" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#ad8f9783bed7ab3a8a3c781b8bab3ce62">Matrix_Pointers</a> (const <a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copy constructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adc705f0d17fc4d5676e39b1b8f2b08ca"></a><!-- doxytag: member="Seldon::Matrix_Pointers::~Matrix_Pointers" ref="adc705f0d17fc4d5676e39b1b8f2b08ca" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#adc705f0d17fc4d5676e39b1b8f2b08ca">~Matrix_Pointers</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Destructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a361729faf34aaedf14ad0b3f044e093c">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the matrix.  <a href="#a361729faf34aaedf14ad0b3f044e093c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a36ac8e7e9bee92ae266201dfb60bf32e">GetDataSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored in memory.  <a href="#a36ac8e7e9bee92ae266201dfb60bf32e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">pointer *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#adf9f21c9fda6a0761549aee70f555f43">GetMe</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the pointer 'me_'.  <a href="#adf9f21c9fda6a0761549aee70f555f43"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a782143339736b347e5a1c7ecd4d0c793">Reallocate</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates memory to resize the matrix.  <a href="#a782143339736b347e5a1c7ecd4d0c793"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4553ae986c158b88f6677d6c588eaabc"></a><!-- doxytag: member="Seldon::Matrix_Pointers::SetData" ref="a4553ae986c158b88f6677d6c588eaabc" args="(int i, int j, pointer data)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetData</b> (int i, int j, pointer data)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a2898e791b8d8e9b282bc36a59da5a0ec">Nullify</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the matrix without releasing memory.  <a href="#a2898e791b8d8e9b282bc36a59da5a0ec"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#ae5cee7b46956ee19dbbb3308613378ec">Resize</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates memory to resize the matrix and keeps previous entries.  <a href="#ae5cee7b46956ee19dbbb3308613378ec"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a859b5a76ca8ff32246c610145151ebac">operator()</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a859b5a76ca8ff32246c610145151ebac"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a1152a779c807df6022715b8635be7bf7">operator()</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a1152a779c807df6022715b8635be7bf7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#afca09e0690510a4a40fe022e62677b6e">Val</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#afca09e0690510a4a40fe022e62677b6e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#ab39472ce33509eee84f5f9b078d38a96">Get</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#ab39472ce33509eee84f5f9b078d38a96"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a3d83977c9b496a6c1745a8e41f79af5c">Val</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a3d83977c9b496a6c1745a8e41f79af5c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a17902c9305fb18c9a40749340945f830">Get</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a17902c9305fb18c9a40749340945f830"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#ac3be1736d3b8f78244483b879b72b255">operator[]</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access to elements of the data array.  <a href="#ac3be1736d3b8f78244483b879b72b255"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#af98cb1a88c5cd8675ce73ccff9f37db9">operator[]</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access to elements of the data array.  <a href="#af98cb1a88c5cd8675ce73ccff9f37db9"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a>&lt; T, Prop, <br class="typebreak"/>
Storage, Allocator &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#ab52557ba17dd1288794a2e6d472ebdc3">operator=</a> (const <a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a matrix (assignment operator).  <a href="#ab52557ba17dd1288794a2e6d472ebdc3"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a2f4238595987c707ac784f437b5e04e3">Set</a> (int i, int j, const T &amp;val)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets an element of the matrix.  <a href="#a2f4238595987c707ac784f437b5e04e3"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a043929510eae4908750348b4862e190c">Copy</a> (const <a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a matrix.  <a href="#a043929510eae4908750348b4862e190c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#af93c5e73fc737a2f6e4f0b15164dd46d">GetLD</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the leading dimension.  <a href="#af93c5e73fc737a2f6e4f0b15164dd46d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#add248e51552e9cec9a6214a8c1328ed0">Zero</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets all elements to zero.  <a href="#add248e51552e9cec9a6214a8c1328ed0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a35fc7bad6fe6cf252737916dfafae0ff"></a><!-- doxytag: member="Seldon::Matrix_Pointers::SetIdentity" ref="a35fc7bad6fe6cf252737916dfafae0ff" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a35fc7bad6fe6cf252737916dfafae0ff">SetIdentity</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the current matrix to the identity. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a2d3aeda0fa734518dd700f8568aaccde">Fill</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the matrix the matrix with 0, 1, 2, ...  <a href="#a2d3aeda0fa734518dd700f8568aaccde"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#aff7bbc1161aeb1ffcf7894e4b21a270f">Fill</a> (const T0 &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the matrix with a given value.  <a href="#aff7bbc1161aeb1ffcf7894e4b21a270f"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a>&lt; T, Prop, <br class="typebreak"/>
Storage, Allocator &gt; &amp;&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#ac18163af8e9002a336f71ad38fd69230">operator=</a> (const T0 &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the matrix with a given value.  <a href="#ac18163af8e9002a336f71ad38fd69230"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a656390ed0505de78a077a6533586428e">FillRand</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the matrix randomly.  <a href="#a656390ed0505de78a077a6533586428e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a648346709f078652cb0a76f4001c901e">Print</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays the matrix on the standard output.  <a href="#a648346709f078652cb0a76f4001c901e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#ac26e9b3c26a513df732755cec5821a6d">Print</a> (int a, int b, int m, int n) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays a sub-matrix on the standard output.  <a href="#ac26e9b3c26a513df732755cec5821a6d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#ab905f1761772bdd8607e212860d3b25d">Print</a> (int l) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays a square sub-matrix on the standard output.  <a href="#ab905f1761772bdd8607e212860d3b25d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a05252b9ce76816127365574721a97e44">Write</a> (string FileName, bool with_size=true) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix in a file.  <a href="#a05252b9ce76816127365574721a97e44"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a150457071742a4d3a4bfd503a0e10c29">Write</a> (ostream &amp;FileStream, bool with_size=true) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix to an output stream.  <a href="#a150457071742a4d3a4bfd503a0e10c29"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#aa1b41e8fe89480269acdc9b8cd36c21e">WriteText</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix in a file.  <a href="#aa1b41e8fe89480269acdc9b8cd36c21e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#af1aa0dd65f43e9ab7fc47dddc38d2cd6">WriteText</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix to an output stream.  <a href="#af1aa0dd65f43e9ab7fc47dddc38d2cd6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a45b2cbff13b8da2a247def9e87e910a6">Read</a> (string FileName, bool with_size=true)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from a file.  <a href="#a45b2cbff13b8da2a247def9e87e910a6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a697168859e965ebfe3e0c722544217de">Read</a> (istream &amp;FileStream, bool with_size=true)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from an input stream.  <a href="#a697168859e965ebfe3e0c722544217de"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a2ec5770914f507080dac5452b8ddaee5">ReadText</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from a file.  <a href="#a2ec5770914f507080dac5452b8ddaee5"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___pointers.php#a96dc6a908187241ae7fea78ed2f12e93">ReadText</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from an input stream.  <a href="#a96dc6a908187241ae7fea78ed2f12e93"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">GetM</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows.  <a href="#a65e9c3f0db9c7c8c3fa10ead777dd927"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows of the matrix possibly transposed.  <a href="#ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">GetN</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns.  <a href="#a6b134070d1b890ba6b7eb72fee170984"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns of the matrix possibly transposed.  <a href="#a7d27412bc9384a5ce0c0db1ee310d31c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">GetSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements in the matrix.  <a href="#a0fbc3f7030583174eaa69429f655a1b0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer to the data array.  <a href="#a453a269dfe7fadba249064363d5ab92a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a const pointer to the data array.  <a href="#a0f2796430deb08e565df8ced656097bf"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "void*" to the data array.  <a href="#a505cdfee34741c463e0dc1943337bedc"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "const void*" to the data array.  <a href="#a5979450cd8801810229f4a24c36e6639"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">Allocator &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the allocator of the matrix.  <a href="#af88748a55208349367d6860ad76fd691"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad3b518bdaa349ab872ad6610e11785ce"></a><!-- doxytag: member="Seldon::Matrix_Pointers::me_" ref="ad3b518bdaa349ab872ad6610e11785ce" args="" -->
pointer *&nbsp;</td><td class="memItemRight" valign="bottom"><b>me_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a96b39ff494d4b7d3e30f320a1c7b4aba"></a><!-- doxytag: member="Seldon::Matrix_Pointers::m_" ref="a96b39ff494d4b7d3e30f320a1c7b4aba" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>m_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a11cb5d502050e5f14482ff0c9cef5a76"></a><!-- doxytag: member="Seldon::Matrix_Pointers::n_" ref="a11cb5d502050e5f14482ff0c9cef5a76" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>n_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acb12a8b74699fae7ae3b2b67376795a1"></a><!-- doxytag: member="Seldon::Matrix_Pointers::data_" ref="acb12a8b74699fae7ae3b2b67376795a1" args="" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-static-attribs"></a>
Static Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a132fa01ce120f352d434fd920e5ad9b4"></a><!-- doxytag: member="Seldon::Matrix_Pointers::allocator_" ref="a132fa01ce120f352d434fd920e5ad9b4" args="" -->
static Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>allocator_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Prop, class Storage, class Allocator = SELDON_DEFAULT_ALLOCATOR&lt;T&gt;&gt;<br/>
 class Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</h3>

<p>Full matrix class. </p>

<p>Definition at line <a class="el" href="_matrix___pointers_8hxx_source.php#l00037">37</a> of file <a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="a5f99e5a5a2f9a8f8e7873f829d82ed33"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Matrix_Pointers" ref="a5f99e5a5a2f9a8f8e7873f829d82ed33" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>On exit, the matrix is an empty 0x0 matrix. </p>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00037">37</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a70433b5aff5b052203136dd43783ac7d"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Matrix_Pointers" ref="a70433b5aff5b052203136dd43783ac7d" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Main constructor. </p>
<p>Builds a i x j full matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00051">51</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="a361729faf34aaedf14ad0b3f044e093c"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Clear" ref="a361729faf34aaedf14ad0b3f044e093c" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::Clear </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the matrix. </p>
<p>Destructs the matrix. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>On exit, the matrix is an empty 0x0 matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00190">190</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a043929510eae4908750348b4862e190c"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Copy" ref="a043929510eae4908750348b4862e190c" args="(const Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop, class Storage, class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::Copy </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>A</em>&nbsp;</td><td>matrix to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'A' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00694">694</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2d3aeda0fa734518dd700f8568aaccde"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Fill" ref="a2d3aeda0fa734518dd700f8568aaccde" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::Fill </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the matrix the matrix with 0, 1, 2, ... </p>
<p>On exit, the matrix is filled with 0, 1, 2, 3, ... The order of those numbers depends on the storage. </p>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00749">749</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aff7bbc1161aeb1ffcf7894e4b21a270f"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Fill" ref="aff7bbc1161aeb1ffcf7894e4b21a270f" args="(const T0 &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::Fill </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the matrix with a given value. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>the value to fill the matrix with. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00762">762</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a656390ed0505de78a077a6533586428e"></a><!-- doxytag: member="Seldon::Matrix_Pointers::FillRand" ref="a656390ed0505de78a077a6533586428e" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::FillRand </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the matrix randomly. </p>
<dl class="note"><dt><b>Note:</b></dt><dd>The random generator is very basic. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00789">789</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab39472ce33509eee84f5f9b078d38a96"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Get" ref="ab39472ce33509eee84f5f9b078d38a96" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::reference <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::Get </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00558">558</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a17902c9305fb18c9a40749340945f830"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Get" ref="a17902c9305fb18c9a40749340945f830" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::const_reference <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::Get </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00602">602</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af88748a55208349367d6860ad76fd691"></a><!-- doxytag: member="Seldon::Matrix_Pointers::GetAllocator" ref="af88748a55208349367d6860ad76fd691" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">Allocator &amp; <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetAllocator </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the allocator of the matrix. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The allocator. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00258">258</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a453a269dfe7fadba249064363d5ab92a"></a><!-- doxytag: member="Seldon::Matrix_Pointers::GetData" ref="a453a269dfe7fadba249064363d5ab92a" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer to the data array. </p>
<p>Returns a pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00208">208</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0f2796430deb08e565df8ced656097bf"></a><!-- doxytag: member="Seldon::Matrix_Pointers::GetDataConst" ref="a0f2796430deb08e565df8ced656097bf" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::const_pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConst </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a const pointer to the data array. </p>
<p>Returns a const pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00221">221</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5979450cd8801810229f4a24c36e6639"></a><!-- doxytag: member="Seldon::Matrix_Pointers::GetDataConstVoid" ref="a5979450cd8801810229f4a24c36e6639" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConstVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "const void*" to the data array. </p>
<p>Returns a pointer of type "const void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00247">247</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a36ac8e7e9bee92ae266201dfb60bf32e"></a><!-- doxytag: member="Seldon::Matrix_Pointers::GetDataSize" ref="a36ac8e7e9bee92ae266201dfb60bf32e" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::GetDataSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored in memory. </p>
<p>Returns the number of elements stored in memory, i.e. the number of rows multiplied by the number of columns because the matrix is full. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements stored in memory. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00211">211</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a505cdfee34741c463e0dc1943337bedc"></a><!-- doxytag: member="Seldon::Matrix_Pointers::GetDataVoid" ref="a505cdfee34741c463e0dc1943337bedc" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "void*" to the data array. </p>
<p>Returns a pointer of type "void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00234">234</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af93c5e73fc737a2f6e4f0b15164dd46d"></a><!-- doxytag: member="Seldon::Matrix_Pointers::GetLD" ref="af93c5e73fc737a2f6e4f0b15164dd46d" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::GetLD </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the leading dimension. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The leading dimension. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00712">712</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a65e9c3f0db9c7c8c3fa10ead777dd927"></a><!-- doxytag: member="Seldon::Matrix_Pointers::GetM" ref="a65e9c3f0db9c7c8c3fa10ead777dd927" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a8c781e99310aae01786eac0e8e5aa50c">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00107">107</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><!-- doxytag: member="Seldon::Matrix_Pointers::GetM" ref="ab6f6c5bba065ca82dad7c3abdbc8ea79" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#aecf3e8c636dbb83335ea588afe2558a8">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00130">130</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="adf9f21c9fda6a0761549aee70f555f43"></a><!-- doxytag: member="Seldon::Matrix_Pointers::GetMe" ref="adf9f21c9fda6a0761549aee70f555f43" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::pointer * <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::GetMe </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the pointer 'me_'. </p>
<p>Returns the pointer 'me_' that defines an array pointing to the first row or column elements, so that 'me_[1]' points to the first element of the second row or column. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The pointer 'me_'. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00226">226</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6b134070d1b890ba6b7eb72fee170984"></a><!-- doxytag: member="Seldon::Matrix_Pointers::GetN" ref="a6b134070d1b890ba6b7eb72fee170984" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a40580a92202044416e379c1304ff0220">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00118">118</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7d27412bc9384a5ce0c0db1ee310d31c"></a><!-- doxytag: member="Seldon::Matrix_Pointers::GetN" ref="a7d27412bc9384a5ce0c0db1ee310d31c" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a867066f7bf531bf7ad15bdcd034dc3c0">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00145">145</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0fbc3f7030583174eaa69429f655a1b0"></a><!-- doxytag: member="Seldon::Matrix_Pointers::GetSize" ref="a0fbc3f7030583174eaa69429f655a1b0" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements in the matrix. </p>
<p>Returns the number of elements in the matrix, i.e. the number of rows multiplied by the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements in the matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00195">195</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2898e791b8d8e9b282bc36a59da5a0ec"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Nullify" ref="a2898e791b8d8e9b282bc36a59da5a0ec" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::Nullify </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the matrix without releasing memory. </p>
<p>On exit, the matrix is empty and the memory has not been released. It is useful for low level manipulations on a <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> instance. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Memory is not released except for me_. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00392">392</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1152a779c807df6022715b8635be7bf7"></a><!-- doxytag: member="Seldon::Matrix_Pointers::operator()" ref="a1152a779c807df6022715b8635be7bf7" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::const_reference <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00502">502</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a859b5a76ca8ff32246c610145151ebac"></a><!-- doxytag: member="Seldon::Matrix_Pointers::operator()" ref="a859b5a76ca8ff32246c610145151ebac" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::reference <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00472">472</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab52557ba17dd1288794a2e6d472ebdc3"></a><!-- doxytag: member="Seldon::Matrix_Pointers::operator=" ref="ab52557ba17dd1288794a2e6d472ebdc3" args="(const Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop, class Storage, class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a matrix (assignment operator). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>A</em>&nbsp;</td><td>matrix to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'A' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00678">678</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac18163af8e9002a336f71ad38fd69230"></a><!-- doxytag: member="Seldon::Matrix_Pointers::operator=" ref="ac18163af8e9002a336f71ad38fd69230" args="(const T0 &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the matrix with a given value. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>the value to fill the matrix with. </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php#a5f16442782b3155c49889aee1a6df7e1">Seldon::Matrix&lt; T, Prop, ColMajor, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#a3727350f7283b6e8e08ccb2f6733362b">Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00776">776</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac3be1736d3b8f78244483b879b72b255"></a><!-- doxytag: member="Seldon::Matrix_Pointers::operator[]" ref="ac3be1736d3b8f78244483b879b72b255" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::reference <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::operator[] </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access to elements of the data array. </p>
<p>Provides a direct access to the data array. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>i-th element of the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00616">616</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af98cb1a88c5cd8675ce73ccff9f37db9"></a><!-- doxytag: member="Seldon::Matrix_Pointers::operator[]" ref="af98cb1a88c5cd8675ce73ccff9f37db9" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::const_reference <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::operator[] </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access to elements of the data array. </p>
<p>Provides a direct access to the data array. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>i-th element of the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00640">640</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac26e9b3c26a513df732755cec5821a6d"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Print" ref="ac26e9b3c26a513df732755cec5821a6d" args="(int a, int b, int m, int n) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::Print </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>a</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>b</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>m</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>n</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Displays a sub-matrix on the standard output. </p>
<p>The sub-matrix is defined by its upper-left corner (a, b) and its bottom-right corner (m, n). So, elements with indices in [a, m] x [b, n] are displayed on the standard output, in text format. Each row is displayed on a single line and elements of a row are delimited by tabulations. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>a</em>&nbsp;</td><td>row index of the upper-left corner. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>b</em>&nbsp;</td><td>column index of the upper-left corner. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>m</em>&nbsp;</td><td>row index of the bottom-right corner. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>n</em>&nbsp;</td><td>column index of the bottom-right corner. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00828">828</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a648346709f078652cb0a76f4001c901e"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Print" ref="a648346709f078652cb0a76f4001c901e" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::Print </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Displays the matrix on the standard output. </p>
<p>Displays elements on the standard output, in text format. Each row is displayed on a single line and elements of a row are delimited by tabulations. </p>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00804">804</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab905f1761772bdd8607e212860d3b25d"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Print" ref="ab905f1761772bdd8607e212860d3b25d" args="(int l) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::Print </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>l</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Displays a square sub-matrix on the standard output. </p>
<p>The sub-matrix is defined by its bottom-right corner (l, l). So, elements with indices in [0, 0] x [l, l] are displayed on the standard output, in text format. Each row is displayed on a single line and elements of a row are delimited by tabulations. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>l</em>&nbsp;</td><td>dimension of the square matrix to be displayed. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00850">850</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a45b2cbff13b8da2a247def9e87e910a6"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Read" ref="a45b2cbff13b8da2a247def9e87e910a6" args="(string FileName, bool with_size=true)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from a file. </p>
<p>Reads a matrix stored in binary format in a file. The number of rows (integer) and the number of columns (integer) are read, and matrix elements are then read in the same order as it should be in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>input file name. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the dimensions of the matrix are not available in the file. In this case, the current dimensions (M, N) of the matrix are unchanged, and MxN elements are read in the file. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l01033">1033</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a697168859e965ebfe3e0c722544217de"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Read" ref="a697168859e965ebfe3e0c722544217de" args="(istream &amp;FileStream, bool with_size=true)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from an input stream. </p>
<p>Reads a matrix in binary format from an input stream. The number of rows (integer) and the number of columns (integer) are read, and matrix elements are then read in the same order as it should be in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>input stream. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the dimensions of the matrix are not available in the stream. In this case, the current dimensions (M, N) of the matrix are unchanged, and MxN elements are read in the stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l01064">1064</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2ec5770914f507080dac5452b8ddaee5"></a><!-- doxytag: member="Seldon::Matrix_Pointers::ReadText" ref="a2ec5770914f507080dac5452b8ddaee5" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from a file. </p>
<p>Reads a matrix from a file in text format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>input file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l01101">1101</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a96dc6a908187241ae7fea78ed2f12e93"></a><!-- doxytag: member="Seldon::Matrix_Pointers::ReadText" ref="a96dc6a908187241ae7fea78ed2f12e93" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from an input stream. </p>
<p>Reads a matrix in text format from an input stream. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>input stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l01126">1126</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a782143339736b347e5a1c7ecd4d0c793"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Reallocate" ref="a782143339736b347e5a1c7ecd4d0c793" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::Reallocate </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates memory to resize the matrix. </p>
<p>On exit, the matrix is a i x j matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>new number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>new number of columns. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Depending on your allocator, data may be lost. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00246">246</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae5cee7b46956ee19dbbb3308613378ec"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Resize" ref="ae5cee7b46956ee19dbbb3308613378ec" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::Resize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates memory to resize the matrix and keeps previous entries. </p>
<p>On exit, the matrix is a i x j matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>new number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>new number of columns. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>The previous entries are kept, extra-entries may not be initialized (depending of the allocator). </dd></dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00432">432</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2f4238595987c707ac784f437b5e04e3"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Set" ref="a2f4238595987c707ac784f437b5e04e3" args="(int i, int j, const T &amp;val)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::Set </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const T &amp;&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets an element of the matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>new value for the matrix element (<em>i</em>, <em>j</em>). </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00663">663</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3d83977c9b496a6c1745a8e41f79af5c"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Val" ref="a3d83977c9b496a6c1745a8e41f79af5c" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::const_reference <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::Val </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00574">574</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="afca09e0690510a4a40fe022e62677b6e"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Val" ref="afca09e0690510a4a40fe022e62677b6e" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::reference <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::Val </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00531">531</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a150457071742a4d3a4bfd503a0e10c29"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Write" ref="a150457071742a4d3a4bfd503a0e10c29" args="(ostream &amp;FileStream, bool with_size=true) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix to an output stream. </p>
<p>Writes the matrix to an output stream in binary format. The number of rows (integer) and the number of columns (integer) are written, and matrix elements are then written in the same order as in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output stream. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the dimensions of the matrix are not saved. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00921">921</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a05252b9ce76816127365574721a97e44"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Write" ref="a05252b9ce76816127365574721a97e44" args="(string FileName, bool with_size=true) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix in a file. </p>
<p>Stores the matrix in a file in binary format. The number of rows (integer) and the number of columns (integer) are written, and matrix elements are then written in the same order as in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the dimensions of the matrix are not saved. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00891">891</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af1aa0dd65f43e9ab7fc47dddc38d2cd6"></a><!-- doxytag: member="Seldon::Matrix_Pointers::WriteText" ref="af1aa0dd65f43e9ab7fc47dddc38d2cd6" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix to an output stream. </p>
<p>Writes the matrix to an output stream in text format. Only matrix elements are written (not dimensions). Each row is written on a single line and elements of a row are delimited by tabulations. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00992">992</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa1b41e8fe89480269acdc9b8cd36c21e"></a><!-- doxytag: member="Seldon::Matrix_Pointers::WriteText" ref="aa1b41e8fe89480269acdc9b8cd36c21e" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix in a file. </p>
<p>Stores the matrix in a file in text format. Only matrix elements are written (not dimensions). Each row is written on a single line and elements of a row are delimited by tabulations. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00962">962</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="add248e51552e9cec9a6214a8c1328ed0"></a><!-- doxytag: member="Seldon::Matrix_Pointers::Zero" ref="add248e51552e9cec9a6214a8c1328ed0" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers</a>&lt; T, Prop, Storage, Allocator &gt;::Zero </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets all elements to zero. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>It fills the memory with zeros. If the matrix stores complex structures, use 'Fill' instead. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l00724">724</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>matrix/<a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a></li>
<li>matrix/<a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
