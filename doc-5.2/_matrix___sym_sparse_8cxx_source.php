<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix_sparse/Matrix_SymSparse.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_SYMSPARSE_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;Matrix_SymSparse.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="keyword">namespace </span>Seldon
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 
<a name="l00029"></a>00029   <span class="comment">/****************</span>
<a name="l00030"></a>00030 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00031"></a>00031 <span class="comment">   ****************/</span>
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00035"></a>00035 
<a name="l00038"></a>00038   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00039"></a>00039   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaffa73e2f7d10d2f2dfff4a701e72405" title="Default constructor.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::Matrix_SymSparse</a>():
<a name="l00040"></a>00040     Matrix_Base&lt;T, Allocator&gt;()
<a name="l00041"></a>00041   {
<a name="l00042"></a>00042     nz_ = 0;
<a name="l00043"></a>00043     ptr_ = NULL;
<a name="l00044"></a>00044     ind_ = NULL;
<a name="l00045"></a>00045   }
<a name="l00046"></a>00046 
<a name="l00047"></a>00047 
<a name="l00049"></a>00049 
<a name="l00055"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#afa0d65cf9cf27bd033588a54dae20f09">00055</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00056"></a>00056   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaffa73e2f7d10d2f2dfff4a701e72405" title="Default constructor.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00057"></a>00057 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaffa73e2f7d10d2f2dfff4a701e72405" title="Default constructor.">  ::Matrix_SymSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j): <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;()
<a name="l00058"></a>00058   {
<a name="l00059"></a>00059     nz_ = 0;
<a name="l00060"></a>00060     ptr_ = NULL;
<a name="l00061"></a>00061     ind_ = NULL;
<a name="l00062"></a>00062 
<a name="l00063"></a>00063     Reallocate(i, j);
<a name="l00064"></a>00064   }
<a name="l00065"></a>00065 
<a name="l00066"></a>00066 
<a name="l00068"></a>00068 
<a name="l00077"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a14fd7e8b9ecea1af9d25faf494dc6431">00077</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00078"></a>00078   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaffa73e2f7d10d2f2dfff4a701e72405" title="Default constructor.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00079"></a>00079 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaffa73e2f7d10d2f2dfff4a701e72405" title="Default constructor.">  Matrix_SymSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> nz):
<a name="l00080"></a>00080     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;()
<a name="l00081"></a>00081   {
<a name="l00082"></a>00082     this-&gt;nz_ = 0;
<a name="l00083"></a>00083     ind_ = NULL;
<a name="l00084"></a>00084     ptr_ = NULL;
<a name="l00085"></a>00085 
<a name="l00086"></a>00086     <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a03e2a2b861d5e2df212db3aeaf144426" title="Initialization of an empty sparse matrix with i rows and j columns.">Reallocate</a>(i, j, nz);
<a name="l00087"></a>00087   }
<a name="l00088"></a>00088 
<a name="l00089"></a>00089 
<a name="l00091"></a>00091 
<a name="l00103"></a>00103   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00104"></a>00104   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00105"></a>00105             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00106"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a626e591de87845d39c44796cd9b19bc9">00106</a>             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00107"></a>00107   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaffa73e2f7d10d2f2dfff4a701e72405" title="Default constructor.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00108"></a>00108 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaffa73e2f7d10d2f2dfff4a701e72405" title="Default constructor.">  Matrix_SymSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00109"></a>00109                    <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; values,
<a name="l00110"></a>00110                    <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; ptr,
<a name="l00111"></a>00111                    <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; ind):
<a name="l00112"></a>00112     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, j)
<a name="l00113"></a>00113   {
<a name="l00114"></a>00114     nz_ = values.GetLength();
<a name="l00115"></a>00115 
<a name="l00116"></a>00116 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00117"></a>00117 <span class="preprocessor"></span>    <span class="comment">// Checks whether vector sizes are acceptable.</span>
<a name="l00118"></a>00118 
<a name="l00119"></a>00119     <span class="keywordflow">if</span> (ind.GetLength() != nz_)
<a name="l00120"></a>00120       {
<a name="l00121"></a>00121         this-&gt;m_ = 0;
<a name="l00122"></a>00122         this-&gt;n_ = 0;
<a name="l00123"></a>00123         nz_ = 0;
<a name="l00124"></a>00124         ptr_ = NULL;
<a name="l00125"></a>00125         ind_ = NULL;
<a name="l00126"></a>00126         this-&gt;data_ = NULL;
<a name="l00127"></a>00127         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, &quot;</span>)
<a name="l00128"></a>00128                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00129"></a>00129                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz_) + <span class="stringliteral">&quot; values but &quot;</span>
<a name="l00130"></a>00130                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(ind.GetLength()) + <span class="stringliteral">&quot; row or column indices.&quot;</span>);
<a name="l00131"></a>00131       }
<a name="l00132"></a>00132 
<a name="l00133"></a>00133     <span class="keywordflow">if</span> (ptr.GetLength() - 1 != i)
<a name="l00134"></a>00134       {
<a name="l00135"></a>00135         this-&gt;m_ = 0;
<a name="l00136"></a>00136         this-&gt;n_ = 0;
<a name="l00137"></a>00137         nz_ = 0;
<a name="l00138"></a>00138         ptr_ = NULL;
<a name="l00139"></a>00139         ind_ = NULL;
<a name="l00140"></a>00140         this-&gt;data_ = NULL;
<a name="l00141"></a>00141         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, &quot;</span>)
<a name="l00142"></a>00142                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00143"></a>00143                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The vector of start indices contains &quot;</span>)
<a name="l00144"></a>00144                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(ptr.GetLength()-1) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column &quot;</span>)
<a name="l00145"></a>00145                        + string(<span class="stringliteral">&quot;start  indices (plus the number of non-zero&quot;</span>)
<a name="l00146"></a>00146                        + <span class="stringliteral">&quot; entries) but there are &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i)
<a name="l00147"></a>00147                        + <span class="stringliteral">&quot; rows or columns (&quot;</span>
<a name="l00148"></a>00148                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix).&quot;</span>);
<a name="l00149"></a>00149       }
<a name="l00150"></a>00150 
<a name="l00151"></a>00151     <span class="keywordflow">if</span> (static_cast&lt;long int&gt;(2 * nz_ - 2) / <span class="keyword">static_cast&lt;</span><span class="keywordtype">long</span> <span class="keywordtype">int</span><span class="keyword">&gt;</span>(i + 1)
<a name="l00152"></a>00152         &gt;= static_cast&lt;long int&gt;(i))
<a name="l00153"></a>00153       {
<a name="l00154"></a>00154         this-&gt;m_ = 0;
<a name="l00155"></a>00155         this-&gt;n_ = 0;
<a name="l00156"></a>00156         nz_ = 0;
<a name="l00157"></a>00157         ptr_ = NULL;
<a name="l00158"></a>00158         ind_ = NULL;
<a name="l00159"></a>00159         this-&gt;data_ = NULL;
<a name="l00160"></a>00160         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, &quot;</span>)
<a name="l00161"></a>00161                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00162"></a>00162                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>)
<a name="l00163"></a>00163                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(values.GetLength())
<a name="l00164"></a>00164                        + <span class="stringliteral">&quot; values) than elements in the matrix (&quot;</span>
<a name="l00165"></a>00165                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00166"></a>00166       }
<a name="l00167"></a>00167 <span class="preprocessor">#endif</span>
<a name="l00168"></a>00168 <span class="preprocessor"></span>
<a name="l00169"></a>00169     this-&gt;ptr_ = ptr.GetData();
<a name="l00170"></a>00170     this-&gt;ind_ = ind.GetData();
<a name="l00171"></a>00171     this-&gt;data_ = values.GetData();
<a name="l00172"></a>00172 
<a name="l00173"></a>00173     ptr.Nullify();
<a name="l00174"></a>00174     ind.Nullify();
<a name="l00175"></a>00175     values.Nullify();
<a name="l00176"></a>00176   }
<a name="l00177"></a>00177 
<a name="l00178"></a>00178 
<a name="l00180"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a6895a30236bf5f7ec632cba1916c2e4b">00180</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00181"></a>00181   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaffa73e2f7d10d2f2dfff4a701e72405" title="Default constructor.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00182"></a>00182 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaffa73e2f7d10d2f2dfff4a701e72405" title="Default constructor.">  Matrix_SymSparse</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00183"></a>00183   {
<a name="l00184"></a>00184     this-&gt;m_ = 0;
<a name="l00185"></a>00185     this-&gt;n_ = 0;
<a name="l00186"></a>00186     this-&gt;nz_ = 0;
<a name="l00187"></a>00187     ptr_ = NULL;
<a name="l00188"></a>00188     ind_ = NULL;
<a name="l00189"></a>00189     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a13715cb4e64fbe58e24fa86897131e25" title="Copies a matrix.">Copy</a>(A);
<a name="l00190"></a>00190   }
<a name="l00191"></a>00191 
<a name="l00192"></a>00192 
<a name="l00193"></a>00193   <span class="comment">/**************</span>
<a name="l00194"></a>00194 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00195"></a>00195 <span class="comment">   **************/</span>
<a name="l00196"></a>00196 
<a name="l00197"></a>00197 
<a name="l00199"></a>00199   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00200"></a>00200   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a6f46818c1d832ff9be950a5b6f0fc9c7" title="Destructor.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::~Matrix_SymSparse</a>()
<a name="l00201"></a>00201   {
<a name="l00202"></a>00202     this-&gt;m_ = 0;
<a name="l00203"></a>00203     this-&gt;n_ = 0;
<a name="l00204"></a>00204 
<a name="l00205"></a>00205 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00206"></a>00206 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00207"></a>00207       {
<a name="l00208"></a>00208 <span class="preprocessor">#endif</span>
<a name="l00209"></a>00209 <span class="preprocessor"></span>
<a name="l00210"></a>00210         <span class="keywordflow">if</span> (ptr_ != NULL)
<a name="l00211"></a>00211           {
<a name="l00212"></a>00212             free(ptr_);
<a name="l00213"></a>00213             ptr_ = NULL;
<a name="l00214"></a>00214           }
<a name="l00215"></a>00215 
<a name="l00216"></a>00216 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00217"></a>00217 <span class="preprocessor"></span>      }
<a name="l00218"></a>00218     <span class="keywordflow">catch</span> (...)
<a name="l00219"></a>00219       {
<a name="l00220"></a>00220         ptr_ = NULL;
<a name="l00221"></a>00221       }
<a name="l00222"></a>00222 <span class="preprocessor">#endif</span>
<a name="l00223"></a>00223 <span class="preprocessor"></span>
<a name="l00224"></a>00224 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00225"></a>00225 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00226"></a>00226       {
<a name="l00227"></a>00227 <span class="preprocessor">#endif</span>
<a name="l00228"></a>00228 <span class="preprocessor"></span>
<a name="l00229"></a>00229         <span class="keywordflow">if</span> (ind_ != NULL)
<a name="l00230"></a>00230           {
<a name="l00231"></a>00231             free(ind_);
<a name="l00232"></a>00232             ind_ = NULL;
<a name="l00233"></a>00233           }
<a name="l00234"></a>00234 
<a name="l00235"></a>00235 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00236"></a>00236 <span class="preprocessor"></span>      }
<a name="l00237"></a>00237     <span class="keywordflow">catch</span> (...)
<a name="l00238"></a>00238       {
<a name="l00239"></a>00239         ind_ = NULL;
<a name="l00240"></a>00240       }
<a name="l00241"></a>00241 <span class="preprocessor">#endif</span>
<a name="l00242"></a>00242 <span class="preprocessor"></span>
<a name="l00243"></a>00243 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00244"></a>00244 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00245"></a>00245       {
<a name="l00246"></a>00246 <span class="preprocessor">#endif</span>
<a name="l00247"></a>00247 <span class="preprocessor"></span>
<a name="l00248"></a>00248         <span class="keywordflow">if</span> (this-&gt;data_ != NULL)
<a name="l00249"></a>00249           {
<a name="l00250"></a>00250             this-&gt;allocator_.deallocate(this-&gt;data_, nz_);
<a name="l00251"></a>00251             this-&gt;data_ = NULL;
<a name="l00252"></a>00252           }
<a name="l00253"></a>00253 
<a name="l00254"></a>00254 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00255"></a>00255 <span class="preprocessor"></span>      }
<a name="l00256"></a>00256     <span class="keywordflow">catch</span> (...)
<a name="l00257"></a>00257       {
<a name="l00258"></a>00258         this-&gt;nz_ = 0;
<a name="l00259"></a>00259         this-&gt;data_ = NULL;
<a name="l00260"></a>00260       }
<a name="l00261"></a>00261 <span class="preprocessor">#endif</span>
<a name="l00262"></a>00262 <span class="preprocessor"></span>
<a name="l00263"></a>00263     this-&gt;nz_ = 0;
<a name="l00264"></a>00264   }
<a name="l00265"></a>00265 
<a name="l00266"></a>00266 
<a name="l00268"></a>00268 
<a name="l00271"></a>00271   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00272"></a>00272   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a71256dab229c463457ff8848aa6d1fa7" title="Clears the matrix.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::Clear</a>()
<a name="l00273"></a>00273   {
<a name="l00274"></a>00274     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a6f46818c1d832ff9be950a5b6f0fc9c7" title="Destructor.">~Matrix_SymSparse</a>();
<a name="l00275"></a>00275   }
<a name="l00276"></a>00276 
<a name="l00277"></a>00277 
<a name="l00278"></a>00278   <span class="comment">/*********************</span>
<a name="l00279"></a>00279 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00280"></a>00280 <span class="comment">   *********************/</span>
<a name="l00281"></a>00281 
<a name="l00282"></a>00282 
<a name="l00284"></a>00284 
<a name="l00295"></a>00295   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00296"></a>00296   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00297"></a>00297             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00298"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aeb3fdc796d3c31e4b4729d3e9e15834c">00298</a>             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00299"></a>00299   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aeb3fdc796d3c31e4b4729d3e9e15834c" title="Redefines the matrix.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00300"></a>00300 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aeb3fdc796d3c31e4b4729d3e9e15834c" title="Redefines the matrix.">  SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00301"></a>00301           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; values,
<a name="l00302"></a>00302           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; ptr,
<a name="l00303"></a>00303           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; ind)
<a name="l00304"></a>00304   {
<a name="l00305"></a>00305     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a71256dab229c463457ff8848aa6d1fa7" title="Clears the matrix.">Clear</a>();
<a name="l00306"></a>00306     this-&gt;m_ = i;
<a name="l00307"></a>00307     this-&gt;n_ = i;
<a name="l00308"></a>00308     this-&gt;nz_ = values.GetLength();
<a name="l00309"></a>00309 
<a name="l00310"></a>00310 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00311"></a>00311 <span class="preprocessor"></span>    <span class="comment">// Checks whether vector sizes are acceptable.</span>
<a name="l00312"></a>00312 
<a name="l00313"></a>00313     <span class="keywordflow">if</span> (ind.GetLength() != nz_)
<a name="l00314"></a>00314       {
<a name="l00315"></a>00315         this-&gt;m_ = 0;
<a name="l00316"></a>00316         this-&gt;n_ = 0;
<a name="l00317"></a>00317         nz_ = 0;
<a name="l00318"></a>00318         ptr_ = NULL;
<a name="l00319"></a>00319         ind_ = NULL;
<a name="l00320"></a>00320         this-&gt;data_ = NULL;
<a name="l00321"></a>00321         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymSparse::Reallocate(int, int, &quot;</span>)
<a name="l00322"></a>00322                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00323"></a>00323                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz_) + <span class="stringliteral">&quot; values but &quot;</span>
<a name="l00324"></a>00324                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(ind.GetLength()) + <span class="stringliteral">&quot; row or column indices.&quot;</span>);
<a name="l00325"></a>00325       }
<a name="l00326"></a>00326 
<a name="l00327"></a>00327     <span class="keywordflow">if</span> (ptr.GetLength()-1 != i)
<a name="l00328"></a>00328       {
<a name="l00329"></a>00329         this-&gt;m_ = 0;
<a name="l00330"></a>00330         this-&gt;n_ = 0;
<a name="l00331"></a>00331         nz_ = 0;
<a name="l00332"></a>00332         ptr_ = NULL;
<a name="l00333"></a>00333         ind_ = NULL;
<a name="l00334"></a>00334         this-&gt;data_ = NULL;
<a name="l00335"></a>00335         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymSparse::Reallocate(int, int, &quot;</span>)
<a name="l00336"></a>00336                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00337"></a>00337                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The vector of start indices contains &quot;</span>)
<a name="l00338"></a>00338                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(ptr.GetLength()-1) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column&quot;</span>)
<a name="l00339"></a>00339                        + string(<span class="stringliteral">&quot; start indices (plus the number of&quot;</span>)
<a name="l00340"></a>00340                        + string(<span class="stringliteral">&quot; non-zero entries) but there are &quot;</span>)
<a name="l00341"></a>00341                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; rows or columns (&quot;</span>
<a name="l00342"></a>00342                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix).&quot;</span>);
<a name="l00343"></a>00343       }
<a name="l00344"></a>00344 
<a name="l00345"></a>00345     <span class="keywordflow">if</span> (static_cast&lt;long int&gt;(2 * nz_ - 2) / <span class="keyword">static_cast&lt;</span><span class="keywordtype">long</span> <span class="keywordtype">int</span><span class="keyword">&gt;</span>(i + 1)
<a name="l00346"></a>00346         &gt;= static_cast&lt;long int&gt;(i))
<a name="l00347"></a>00347       {
<a name="l00348"></a>00348         this-&gt;m_ = 0;
<a name="l00349"></a>00349         this-&gt;n_ = 0;
<a name="l00350"></a>00350         nz_ = 0;
<a name="l00351"></a>00351         ptr_ = NULL;
<a name="l00352"></a>00352         ind_ = NULL;
<a name="l00353"></a>00353         this-&gt;data_ = NULL;
<a name="l00354"></a>00354         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymSparse::Reallocate(int, int, &quot;</span>)
<a name="l00355"></a>00355                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00356"></a>00356                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>)
<a name="l00357"></a>00357                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(values.GetLength())
<a name="l00358"></a>00358                        + <span class="stringliteral">&quot; values) than elements in the matrix (&quot;</span>
<a name="l00359"></a>00359                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00360"></a>00360       }
<a name="l00361"></a>00361 <span class="preprocessor">#endif</span>
<a name="l00362"></a>00362 <span class="preprocessor"></span>
<a name="l00363"></a>00363     this-&gt;ptr_ = ptr.GetData();
<a name="l00364"></a>00364     this-&gt;ind_ = ind.GetData();
<a name="l00365"></a>00365     this-&gt;data_ = values.GetData();
<a name="l00366"></a>00366 
<a name="l00367"></a>00367     ptr.Nullify();
<a name="l00368"></a>00368     ind.Nullify();
<a name="l00369"></a>00369     values.Nullify();
<a name="l00370"></a>00370   }
<a name="l00371"></a>00371 
<a name="l00372"></a>00372 
<a name="l00374"></a>00374 
<a name="l00388"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#ad647993f653414c5a34b00052aab3c78">00388</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00389"></a>00389   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aeb3fdc796d3c31e4b4729d3e9e15834c" title="Redefines the matrix.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00390"></a>00390 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aeb3fdc796d3c31e4b4729d3e9e15834c" title="Redefines the matrix.">  ::SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> nz,
<a name="l00391"></a>00391             <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00392"></a>00392             ::pointer values,
<a name="l00393"></a>00393             <span class="keywordtype">int</span>* ptr,
<a name="l00394"></a>00394             <span class="keywordtype">int</span>* ind)
<a name="l00395"></a>00395   {
<a name="l00396"></a>00396     this-&gt;Clear();
<a name="l00397"></a>00397 
<a name="l00398"></a>00398     this-&gt;m_ = i;
<a name="l00399"></a>00399     this-&gt;n_ = i;
<a name="l00400"></a>00400 
<a name="l00401"></a>00401     this-&gt;nz_ = nz;
<a name="l00402"></a>00402 
<a name="l00403"></a>00403     this-&gt;data_ = values;
<a name="l00404"></a>00404     ind_ = ind;
<a name="l00405"></a>00405     ptr_ = ptr;
<a name="l00406"></a>00406   }
<a name="l00407"></a>00407 
<a name="l00408"></a>00408 
<a name="l00410"></a>00410 
<a name="l00414"></a>00414   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00415"></a>00415   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a99f9900aa0c5bd7c6040686a53d39a85" title="Clears the matrix without releasing memory.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::Nullify</a>()
<a name="l00416"></a>00416   {
<a name="l00417"></a>00417     this-&gt;data_ = NULL;
<a name="l00418"></a>00418     this-&gt;m_ = 0;
<a name="l00419"></a>00419     this-&gt;n_ = 0;
<a name="l00420"></a>00420     nz_ = 0;
<a name="l00421"></a>00421     ptr_ = NULL;
<a name="l00422"></a>00422     ind_ = NULL;
<a name="l00423"></a>00423   }
<a name="l00424"></a>00424 
<a name="l00425"></a>00425 
<a name="l00427"></a>00427 
<a name="l00431"></a>00431   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00432"></a>00432   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a03e2a2b861d5e2df212db3aeaf144426" title="Initialization of an empty sparse matrix with i rows and j columns.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00433"></a>00433   {
<a name="l00434"></a>00434     <span class="comment">// clearing previous entries</span>
<a name="l00435"></a>00435     <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a71256dab229c463457ff8848aa6d1fa7" title="Clears the matrix.">Clear</a>();
<a name="l00436"></a>00436 
<a name="l00437"></a>00437     this-&gt;m_ = i;
<a name="l00438"></a>00438     this-&gt;n_ = i;
<a name="l00439"></a>00439 
<a name="l00440"></a>00440     <span class="comment">// we try to allocate ptr_</span>
<a name="l00441"></a>00441 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00442"></a>00442 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00443"></a>00443       {
<a name="l00444"></a>00444 <span class="preprocessor">#endif</span>
<a name="l00445"></a>00445 <span class="preprocessor"></span>
<a name="l00446"></a>00446         ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(i+1, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00447"></a>00447 
<a name="l00448"></a>00448 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00449"></a>00449 <span class="preprocessor"></span>      }
<a name="l00450"></a>00450     <span class="keywordflow">catch</span> (...)
<a name="l00451"></a>00451       {
<a name="l00452"></a>00452         this-&gt;m_ = 0;
<a name="l00453"></a>00453         this-&gt;n_ = 0;
<a name="l00454"></a>00454         nz_ = 0;
<a name="l00455"></a>00455         ptr_ = NULL;
<a name="l00456"></a>00456         ind_ = NULL;
<a name="l00457"></a>00457         this-&gt;data_ = NULL;
<a name="l00458"></a>00458       }
<a name="l00459"></a>00459     <span class="keywordflow">if</span> (ptr_ == NULL)
<a name="l00460"></a>00460       {
<a name="l00461"></a>00461         this-&gt;m_ = 0;
<a name="l00462"></a>00462         this-&gt;n_ = 0;
<a name="l00463"></a>00463         nz_ = 0;
<a name="l00464"></a>00464         ind_ = NULL;
<a name="l00465"></a>00465         this-&gt;data_ = NULL;
<a name="l00466"></a>00466       }
<a name="l00467"></a>00467     <span class="keywordflow">if</span> (ptr_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00468"></a>00468       <span class="keywordflow">throw</span> NoMemory(<span class="stringliteral">&quot;Matrix_SymSparse::Reallocate(int, int)&quot;</span>,
<a name="l00469"></a>00469                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00470"></a>00470                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (i+1) )
<a name="l00471"></a>00471                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i+1)
<a name="l00472"></a>00472                      + <span class="stringliteral">&quot; row or column start indices, for a &quot;</span>
<a name="l00473"></a>00473                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00474"></a>00474 <span class="preprocessor">#endif</span>
<a name="l00475"></a>00475 <span class="preprocessor"></span>
<a name="l00476"></a>00476     <span class="comment">// then filing ptr_ with 0</span>
<a name="l00477"></a>00477     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt;= i; k++)
<a name="l00478"></a>00478       ptr_[k] = 0;
<a name="l00479"></a>00479   }
<a name="l00480"></a>00480 
<a name="l00481"></a>00481 
<a name="l00483"></a>00483 
<a name="l00488"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a42b7c2729769fb7ffbb4be5f5214f582">00488</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00489"></a>00489   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a03e2a2b861d5e2df212db3aeaf144426" title="Initialization of an empty sparse matrix with i rows and j columns.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00490"></a>00490 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a03e2a2b861d5e2df212db3aeaf144426" title="Initialization of an empty sparse matrix with i rows and j columns.">  ::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> nz)
<a name="l00491"></a>00491   {
<a name="l00492"></a>00492     <span class="comment">// clearing previous entries</span>
<a name="l00493"></a>00493     Clear();
<a name="l00494"></a>00494 
<a name="l00495"></a>00495     this-&gt;nz_ = nz;
<a name="l00496"></a>00496     this-&gt;m_ = i;
<a name="l00497"></a>00497     this-&gt;n_ = i;
<a name="l00498"></a>00498 
<a name="l00499"></a>00499 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00500"></a>00500 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (static_cast&lt;long int&gt;(2 * nz_ - 2) / static_cast&lt;long int&gt;(i + 1)
<a name="l00501"></a>00501         &gt;= static_cast&lt;long int&gt;(i))
<a name="l00502"></a>00502       {
<a name="l00503"></a>00503         this-&gt;m_ = 0;
<a name="l00504"></a>00504         this-&gt;n_ = 0;
<a name="l00505"></a>00505         nz_ = 0;
<a name="l00506"></a>00506         ptr_ = NULL;
<a name="l00507"></a>00507         ind_ = NULL;
<a name="l00508"></a>00508         this-&gt;data_ = NULL;
<a name="l00509"></a>00509         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, int)&quot;</span>,
<a name="l00510"></a>00510                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00511"></a>00511                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; values) than elements in the upper&quot;</span>)
<a name="l00512"></a>00512                        + <span class="stringliteral">&quot; part of the matrix (&quot;</span>
<a name="l00513"></a>00513                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00514"></a>00514       }
<a name="l00515"></a>00515 <span class="preprocessor">#endif</span>
<a name="l00516"></a>00516 <span class="preprocessor"></span>
<a name="l00517"></a>00517 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00518"></a>00518 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00519"></a>00519       {
<a name="l00520"></a>00520 <span class="preprocessor">#endif</span>
<a name="l00521"></a>00521 <span class="preprocessor"></span>
<a name="l00522"></a>00522         ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(i + 1, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00523"></a>00523 
<a name="l00524"></a>00524 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00525"></a>00525 <span class="preprocessor"></span>      }
<a name="l00526"></a>00526     <span class="keywordflow">catch</span> (...)
<a name="l00527"></a>00527       {
<a name="l00528"></a>00528         this-&gt;m_ = 0;
<a name="l00529"></a>00529         this-&gt;n_ = 0;
<a name="l00530"></a>00530         nz_ = 0;
<a name="l00531"></a>00531         ptr_ = NULL;
<a name="l00532"></a>00532         ind_ = NULL;
<a name="l00533"></a>00533         this-&gt;data_ = NULL;
<a name="l00534"></a>00534       }
<a name="l00535"></a>00535     <span class="keywordflow">if</span> (ptr_ == NULL)
<a name="l00536"></a>00536       {
<a name="l00537"></a>00537         this-&gt;m_ = 0;
<a name="l00538"></a>00538         this-&gt;n_ = 0;
<a name="l00539"></a>00539         nz_ = 0;
<a name="l00540"></a>00540         ind_ = NULL;
<a name="l00541"></a>00541         this-&gt;data_ = NULL;
<a name="l00542"></a>00542       }
<a name="l00543"></a>00543     <span class="keywordflow">if</span> (ptr_ == NULL &amp;&amp; i != 0)
<a name="l00544"></a>00544       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, int)&quot;</span>,
<a name="l00545"></a>00545                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00546"></a>00546                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (i+1) ) + <span class="stringliteral">&quot; bytes to store &quot;</span>
<a name="l00547"></a>00547                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i+1) + <span class="stringliteral">&quot; row or column start indices, for a &quot;</span>
<a name="l00548"></a>00548                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00549"></a>00549 <span class="preprocessor">#endif</span>
<a name="l00550"></a>00550 <span class="preprocessor"></span>
<a name="l00551"></a>00551 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00552"></a>00552 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00553"></a>00553       {
<a name="l00554"></a>00554 <span class="preprocessor">#endif</span>
<a name="l00555"></a>00555 <span class="preprocessor"></span>
<a name="l00556"></a>00556         ind_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(nz_, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00557"></a>00557 
<a name="l00558"></a>00558 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00559"></a>00559 <span class="preprocessor"></span>      }
<a name="l00560"></a>00560     <span class="keywordflow">catch</span> (...)
<a name="l00561"></a>00561       {
<a name="l00562"></a>00562         this-&gt;m_ = 0;
<a name="l00563"></a>00563         this-&gt;n_ = 0;
<a name="l00564"></a>00564         nz_ = 0;
<a name="l00565"></a>00565         free(ptr_);
<a name="l00566"></a>00566         ptr_ = NULL;
<a name="l00567"></a>00567         ind_ = NULL;
<a name="l00568"></a>00568         this-&gt;data_ = NULL;
<a name="l00569"></a>00569       }
<a name="l00570"></a>00570     <span class="keywordflow">if</span> (ind_ == NULL)
<a name="l00571"></a>00571       {
<a name="l00572"></a>00572         this-&gt;m_ = 0;
<a name="l00573"></a>00573         this-&gt;n_ = 0;
<a name="l00574"></a>00574         nz_ = 0;
<a name="l00575"></a>00575         free(ptr_);
<a name="l00576"></a>00576         ptr_ = NULL;
<a name="l00577"></a>00577         this-&gt;data_ = NULL;
<a name="l00578"></a>00578       }
<a name="l00579"></a>00579     <span class="keywordflow">if</span> (ind_ == NULL &amp;&amp; i != 0)
<a name="l00580"></a>00580       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, int)&quot;</span>,
<a name="l00581"></a>00581                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * nz)
<a name="l00582"></a>00582                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00583"></a>00583                      + <span class="stringliteral">&quot; row or column indices, for a &quot;</span>
<a name="l00584"></a>00584                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00585"></a>00585 <span class="preprocessor">#endif</span>
<a name="l00586"></a>00586 <span class="preprocessor"></span>
<a name="l00587"></a>00587 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00588"></a>00588 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00589"></a>00589       {
<a name="l00590"></a>00590 <span class="preprocessor">#endif</span>
<a name="l00591"></a>00591 <span class="preprocessor"></span>
<a name="l00592"></a>00592         this-&gt;data_ = this-&gt;allocator_.allocate(nz_, <span class="keyword">this</span>);
<a name="l00593"></a>00593 
<a name="l00594"></a>00594 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00595"></a>00595 <span class="preprocessor"></span>      }
<a name="l00596"></a>00596     <span class="keywordflow">catch</span> (...)
<a name="l00597"></a>00597       {
<a name="l00598"></a>00598         this-&gt;m_ = 0;
<a name="l00599"></a>00599         this-&gt;n_ = 0;
<a name="l00600"></a>00600         free(ptr_);
<a name="l00601"></a>00601         ptr_ = NULL;
<a name="l00602"></a>00602         free(ind_);
<a name="l00603"></a>00603         ind_ = NULL;
<a name="l00604"></a>00604         this-&gt;data_ = NULL;
<a name="l00605"></a>00605       }
<a name="l00606"></a>00606     <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00607"></a>00607       {
<a name="l00608"></a>00608         this-&gt;m_ = 0;
<a name="l00609"></a>00609         this-&gt;n_ = 0;
<a name="l00610"></a>00610         free(ptr_);
<a name="l00611"></a>00611         ptr_ = NULL;
<a name="l00612"></a>00612         free(ind_);
<a name="l00613"></a>00613         ind_ = NULL;
<a name="l00614"></a>00614       }
<a name="l00615"></a>00615     <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; i != 0)
<a name="l00616"></a>00616       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, int)&quot;</span>,
<a name="l00617"></a>00617                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * nz)
<a name="l00618"></a>00618                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz) + <span class="stringliteral">&quot; values, for a &quot;</span>
<a name="l00619"></a>00619                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00620"></a>00620 <span class="preprocessor">#endif</span>
<a name="l00621"></a>00621 <span class="preprocessor"></span>  }
<a name="l00622"></a>00622 
<a name="l00623"></a>00623 
<a name="l00625"></a>00625 
<a name="l00629"></a>00629   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00630"></a>00630   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a00809c7e5168b170c34e6e9c60d03674" title="Changing the number of rows and columns.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00631"></a>00631   {
<a name="l00632"></a>00632     <span class="keywordflow">if</span> (i &lt; this-&gt;m_)
<a name="l00633"></a>00633       <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a00809c7e5168b170c34e6e9c60d03674" title="Changing the number of rows and columns.">Resize</a>(i, i, ptr_[i]);
<a name="l00634"></a>00634     <span class="keywordflow">else</span>
<a name="l00635"></a>00635       <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a00809c7e5168b170c34e6e9c60d03674" title="Changing the number of rows and columns.">Resize</a>(i, i, nz_);
<a name="l00636"></a>00636   }
<a name="l00637"></a>00637 
<a name="l00638"></a>00638 
<a name="l00640"></a>00640 
<a name="l00645"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a43f12ba2de0fb8d0ed79449b99e3c9a0">00645</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00646"></a>00646   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a00809c7e5168b170c34e6e9c60d03674" title="Changing the number of rows and columns.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00647"></a>00647 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a00809c7e5168b170c34e6e9c60d03674" title="Changing the number of rows and columns.">  ::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> nz)
<a name="l00648"></a>00648   {
<a name="l00649"></a>00649 
<a name="l00650"></a>00650 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00651"></a>00651 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (static_cast&lt;long int&gt;(2 * nz - 2) / static_cast&lt;long int&gt;(i + 1)
<a name="l00652"></a>00652         &gt;= static_cast&lt;long int&gt;(i))
<a name="l00653"></a>00653       {
<a name="l00654"></a>00654         this-&gt;m_ = 0;
<a name="l00655"></a>00655         this-&gt;n_ = 0;
<a name="l00656"></a>00656         nz_ = 0;
<a name="l00657"></a>00657         ptr_ = NULL;
<a name="l00658"></a>00658         ind_ = NULL;
<a name="l00659"></a>00659         this-&gt;data_ = NULL;
<a name="l00660"></a>00660         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, int)&quot;</span>,
<a name="l00661"></a>00661                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00662"></a>00662                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; values) than elements in the upper&quot;</span>)
<a name="l00663"></a>00663                        + <span class="stringliteral">&quot; part of the matrix (&quot;</span>
<a name="l00664"></a>00664                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00665"></a>00665       }
<a name="l00666"></a>00666 <span class="preprocessor">#endif</span>
<a name="l00667"></a>00667 <span class="preprocessor"></span>
<a name="l00668"></a>00668     <span class="keywordflow">if</span> (nz != nz_)
<a name="l00669"></a>00669       {
<a name="l00670"></a>00670         <span class="comment">// trying to resize ind_ and data_</span>
<a name="l00671"></a>00671 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00672"></a>00672 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l00673"></a>00673           {
<a name="l00674"></a>00674 <span class="preprocessor">#endif</span>
<a name="l00675"></a>00675 <span class="preprocessor"></span>
<a name="l00676"></a>00676             ind_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( realloc(reinterpret_cast&lt;void*&gt;(ind_),
<a name="l00677"></a>00677                                                    nz*<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00678"></a>00678 
<a name="l00679"></a>00679 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00680"></a>00680 <span class="preprocessor"></span>          }
<a name="l00681"></a>00681         <span class="keywordflow">catch</span> (...)
<a name="l00682"></a>00682           {
<a name="l00683"></a>00683             this-&gt;m_ = 0;
<a name="l00684"></a>00684             this-&gt;n_ = 0;
<a name="l00685"></a>00685             nz_ = 0;
<a name="l00686"></a>00686             free(ptr_);
<a name="l00687"></a>00687             ptr_ = NULL;
<a name="l00688"></a>00688             ind_ = NULL;
<a name="l00689"></a>00689             this-&gt;data_ = NULL;
<a name="l00690"></a>00690           }
<a name="l00691"></a>00691         <span class="keywordflow">if</span> (ind_ == NULL)
<a name="l00692"></a>00692           {
<a name="l00693"></a>00693             this-&gt;m_ = 0;
<a name="l00694"></a>00694             this-&gt;n_ = 0;
<a name="l00695"></a>00695             nz_ = 0;
<a name="l00696"></a>00696             free(ptr_);
<a name="l00697"></a>00697             ptr_ = NULL;
<a name="l00698"></a>00698             this-&gt;data_ = NULL;
<a name="l00699"></a>00699           }
<a name="l00700"></a>00700         <span class="keywordflow">if</span> (ind_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00701"></a>00701           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Resize(int, int, int)&quot;</span>,
<a name="l00702"></a>00702                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * nz)
<a name="l00703"></a>00703                          + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00704"></a>00704                          + <span class="stringliteral">&quot; row or column indices, for a &quot;</span>
<a name="l00705"></a>00705                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00706"></a>00706 <span class="preprocessor">#endif</span>
<a name="l00707"></a>00707 <span class="preprocessor"></span>
<a name="l00708"></a>00708         <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> val;
<a name="l00709"></a>00709         val.SetData(nz_, this-&gt;data_);
<a name="l00710"></a>00710         val.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a99dda1b202ce562645f890897c593515" title="Changes the length of the vector, and keeps previous values.">Resize</a>(nz);
<a name="l00711"></a>00711 
<a name="l00712"></a>00712         this-&gt;data_ = val.<a class="code" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a" title="Returns a pointer to data_ (stored data).">GetData</a>();
<a name="l00713"></a>00713         nz_ = nz;
<a name="l00714"></a>00714         val.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a151dc47113d07ab24775733d0c46dc90" title="Clears the vector without releasing memory.">Nullify</a>();
<a name="l00715"></a>00715       }
<a name="l00716"></a>00716 
<a name="l00717"></a>00717 
<a name="l00718"></a>00718     <span class="keywordflow">if</span> (this-&gt;m_ != i)
<a name="l00719"></a>00719       {
<a name="l00720"></a>00720 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00721"></a>00721 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l00722"></a>00722           {
<a name="l00723"></a>00723 <span class="preprocessor">#endif</span>
<a name="l00724"></a>00724 <span class="preprocessor"></span>            <span class="comment">// trying to resize ptr_</span>
<a name="l00725"></a>00725             ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( realloc(ptr_, (i+1)*
<a name="l00726"></a>00726                                                    <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00727"></a>00727 
<a name="l00728"></a>00728 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00729"></a>00729 <span class="preprocessor"></span>          }
<a name="l00730"></a>00730         <span class="keywordflow">catch</span> (...)
<a name="l00731"></a>00731           {
<a name="l00732"></a>00732             this-&gt;m_ = 0;
<a name="l00733"></a>00733             this-&gt;n_ = 0;
<a name="l00734"></a>00734             nz_ = 0;
<a name="l00735"></a>00735             ptr_ = NULL;
<a name="l00736"></a>00736             ind_ = NULL;
<a name="l00737"></a>00737             this-&gt;data_ = NULL;
<a name="l00738"></a>00738           }
<a name="l00739"></a>00739         <span class="keywordflow">if</span> (ptr_ == NULL)
<a name="l00740"></a>00740           {
<a name="l00741"></a>00741             this-&gt;m_ = 0;
<a name="l00742"></a>00742             this-&gt;n_ = 0;
<a name="l00743"></a>00743             nz_ = 0;
<a name="l00744"></a>00744             ind_ = NULL;
<a name="l00745"></a>00745             this-&gt;data_ = NULL;
<a name="l00746"></a>00746           }
<a name="l00747"></a>00747         <span class="keywordflow">if</span> (ptr_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00748"></a>00748           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Resize(int, int)&quot;</span>,
<a name="l00749"></a>00749                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00750"></a>00750                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (i+1) )
<a name="l00751"></a>00751                          + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i+1)
<a name="l00752"></a>00752                          + <span class="stringliteral">&quot; row or column start indices, for a &quot;</span>
<a name="l00753"></a>00753                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00754"></a>00754 <span class="preprocessor">#endif</span>
<a name="l00755"></a>00755 <span class="preprocessor"></span>
<a name="l00756"></a>00756         <span class="comment">// then filing last values of ptr_ with nz_</span>
<a name="l00757"></a>00757         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = this-&gt;m_; k &lt;= i; k++)
<a name="l00758"></a>00758           ptr_[k] = this-&gt;nz_;
<a name="l00759"></a>00759       }
<a name="l00760"></a>00760 
<a name="l00761"></a>00761     this-&gt;m_ = i;
<a name="l00762"></a>00762     this-&gt;n_ = i;
<a name="l00763"></a>00763   }
<a name="l00764"></a>00764 
<a name="l00765"></a>00765 
<a name="l00767"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a13715cb4e64fbe58e24fa86897131e25">00767</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00768"></a>00768   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a13715cb4e64fbe58e24fa86897131e25" title="Copies a matrix.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00769"></a>00769 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a13715cb4e64fbe58e24fa86897131e25" title="Copies a matrix.">  Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00770"></a>00770   {
<a name="l00771"></a>00771     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a71256dab229c463457ff8848aa6d1fa7" title="Clears the matrix.">Clear</a>();
<a name="l00772"></a>00772     <span class="keywordtype">int</span> nz = A.<a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a4be3dd5024addbf8bfa332193c4030cd" title="Returns the number of elements stored in memory.">GetNonZeros</a>();
<a name="l00773"></a>00773     <span class="keywordtype">int</span> i = A.<a class="code" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927" title="Returns the number of rows.">GetM</a>();
<a name="l00774"></a>00774     <span class="keywordtype">int</span> j = A.<a class="code" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984" title="Returns the number of columns.">GetN</a>();
<a name="l00775"></a>00775     this-&gt;nz_ = nz;
<a name="l00776"></a>00776     this-&gt;m_ = i;
<a name="l00777"></a>00777     this-&gt;n_ = j;
<a name="l00778"></a>00778     <span class="keywordflow">if</span> ((i == 0)||(j == 0))
<a name="l00779"></a>00779       {
<a name="l00780"></a>00780         this-&gt;m_ = 0;
<a name="l00781"></a>00781         this-&gt;n_ = 0;
<a name="l00782"></a>00782         this-&gt;nz_ = 0;
<a name="l00783"></a>00783         <span class="keywordflow">return</span>;
<a name="l00784"></a>00784       }
<a name="l00785"></a>00785 
<a name="l00786"></a>00786 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00787"></a>00787 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (static_cast&lt;long int&gt;(2 * nz_ - 2) / static_cast&lt;long int&gt;(i + 1)
<a name="l00788"></a>00788         &gt;= static_cast&lt;long int&gt;(i))
<a name="l00789"></a>00789       {
<a name="l00790"></a>00790         this-&gt;m_ = 0;
<a name="l00791"></a>00791         this-&gt;n_ = 0;
<a name="l00792"></a>00792         nz_ = 0;
<a name="l00793"></a>00793         ptr_ = NULL;
<a name="l00794"></a>00794         ind_ = NULL;
<a name="l00795"></a>00795         this-&gt;data_ = NULL;
<a name="l00796"></a>00796         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, int)&quot;</span>,
<a name="l00797"></a>00797                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00798"></a>00798                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; values) than elements in the upper&quot;</span>)
<a name="l00799"></a>00799                        + <span class="stringliteral">&quot; part of the matrix (&quot;</span>
<a name="l00800"></a>00800                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00801"></a>00801       }
<a name="l00802"></a>00802 <span class="preprocessor">#endif</span>
<a name="l00803"></a>00803 <span class="preprocessor"></span>
<a name="l00804"></a>00804 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00805"></a>00805 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00806"></a>00806       {
<a name="l00807"></a>00807 <span class="preprocessor">#endif</span>
<a name="l00808"></a>00808 <span class="preprocessor"></span>
<a name="l00809"></a>00809         ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(i + 1, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00810"></a>00810         memcpy(this-&gt;ptr_, A.ptr_, (i + 1) * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00811"></a>00811 
<a name="l00812"></a>00812 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00813"></a>00813 <span class="preprocessor"></span>      }
<a name="l00814"></a>00814     <span class="keywordflow">catch</span> (...)
<a name="l00815"></a>00815       {
<a name="l00816"></a>00816         this-&gt;m_ = 0;
<a name="l00817"></a>00817         this-&gt;n_ = 0;
<a name="l00818"></a>00818         nz_ = 0;
<a name="l00819"></a>00819         ptr_ = NULL;
<a name="l00820"></a>00820         ind_ = NULL;
<a name="l00821"></a>00821         this-&gt;data_ = NULL;
<a name="l00822"></a>00822       }
<a name="l00823"></a>00823     <span class="keywordflow">if</span> (ptr_ == NULL)
<a name="l00824"></a>00824       {
<a name="l00825"></a>00825         this-&gt;m_ = 0;
<a name="l00826"></a>00826         this-&gt;n_ = 0;
<a name="l00827"></a>00827         nz_ = 0;
<a name="l00828"></a>00828         ind_ = NULL;
<a name="l00829"></a>00829         this-&gt;data_ = NULL;
<a name="l00830"></a>00830       }
<a name="l00831"></a>00831     <span class="keywordflow">if</span> (ptr_ == NULL &amp;&amp; i != 0)
<a name="l00832"></a>00832       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, int)&quot;</span>,
<a name="l00833"></a>00833                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00834"></a>00834                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (i+1) ) + <span class="stringliteral">&quot; bytes to store &quot;</span>
<a name="l00835"></a>00835                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i+1) + <span class="stringliteral">&quot; row or column start indices, for a &quot;</span>
<a name="l00836"></a>00836                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00837"></a>00837 <span class="preprocessor">#endif</span>
<a name="l00838"></a>00838 <span class="preprocessor"></span>
<a name="l00839"></a>00839 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00840"></a>00840 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00841"></a>00841       {
<a name="l00842"></a>00842 <span class="preprocessor">#endif</span>
<a name="l00843"></a>00843 <span class="preprocessor"></span>
<a name="l00844"></a>00844         ind_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(nz_, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00845"></a>00845         memcpy(this-&gt;ind_, A.ind_, nz_ * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00846"></a>00846 
<a name="l00847"></a>00847 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00848"></a>00848 <span class="preprocessor"></span>      }
<a name="l00849"></a>00849     <span class="keywordflow">catch</span> (...)
<a name="l00850"></a>00850       {
<a name="l00851"></a>00851         this-&gt;m_ = 0;
<a name="l00852"></a>00852         this-&gt;n_ = 0;
<a name="l00853"></a>00853         nz_ = 0;
<a name="l00854"></a>00854         free(ptr_);
<a name="l00855"></a>00855         ptr_ = NULL;
<a name="l00856"></a>00856         ind_ = NULL;
<a name="l00857"></a>00857         this-&gt;data_ = NULL;
<a name="l00858"></a>00858       }
<a name="l00859"></a>00859     <span class="keywordflow">if</span> (ind_ == NULL)
<a name="l00860"></a>00860       {
<a name="l00861"></a>00861         this-&gt;m_ = 0;
<a name="l00862"></a>00862         this-&gt;n_ = 0;
<a name="l00863"></a>00863         nz_ = 0;
<a name="l00864"></a>00864         free(ptr_);
<a name="l00865"></a>00865         ptr_ = NULL;
<a name="l00866"></a>00866         this-&gt;data_ = NULL;
<a name="l00867"></a>00867       }
<a name="l00868"></a>00868     <span class="keywordflow">if</span> (ind_ == NULL &amp;&amp; i != 0)
<a name="l00869"></a>00869       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, int)&quot;</span>,
<a name="l00870"></a>00870                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * nz)
<a name="l00871"></a>00871                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00872"></a>00872                      + <span class="stringliteral">&quot; row or column indices, for a &quot;</span>
<a name="l00873"></a>00873                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00874"></a>00874 <span class="preprocessor">#endif</span>
<a name="l00875"></a>00875 <span class="preprocessor"></span>
<a name="l00876"></a>00876 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00877"></a>00877 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00878"></a>00878       {
<a name="l00879"></a>00879 <span class="preprocessor">#endif</span>
<a name="l00880"></a>00880 <span class="preprocessor"></span>
<a name="l00881"></a>00881         this-&gt;data_ = this-&gt;allocator_.allocate(nz_, <span class="keyword">this</span>);
<a name="l00882"></a>00882         this-&gt;allocator_.memorycpy(this-&gt;data_, A.data_, nz_);
<a name="l00883"></a>00883 
<a name="l00884"></a>00884 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00885"></a>00885 <span class="preprocessor"></span>      }
<a name="l00886"></a>00886     <span class="keywordflow">catch</span> (...)
<a name="l00887"></a>00887       {
<a name="l00888"></a>00888         this-&gt;m_ = 0;
<a name="l00889"></a>00889         this-&gt;n_ = 0;
<a name="l00890"></a>00890         free(ptr_);
<a name="l00891"></a>00891         ptr_ = NULL;
<a name="l00892"></a>00892         free(ind_);
<a name="l00893"></a>00893         ind_ = NULL;
<a name="l00894"></a>00894         this-&gt;data_ = NULL;
<a name="l00895"></a>00895       }
<a name="l00896"></a>00896     <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00897"></a>00897       {
<a name="l00898"></a>00898         this-&gt;m_ = 0;
<a name="l00899"></a>00899         this-&gt;n_ = 0;
<a name="l00900"></a>00900         free(ptr_);
<a name="l00901"></a>00901         ptr_ = NULL;
<a name="l00902"></a>00902         free(ind_);
<a name="l00903"></a>00903         ind_ = NULL;
<a name="l00904"></a>00904       }
<a name="l00905"></a>00905     <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; i != 0)
<a name="l00906"></a>00906       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, int)&quot;</span>,
<a name="l00907"></a>00907                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * nz)
<a name="l00908"></a>00908                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz) + <span class="stringliteral">&quot; values, for a &quot;</span>
<a name="l00909"></a>00909                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00910"></a>00910 <span class="preprocessor">#endif</span>
<a name="l00911"></a>00911 <span class="preprocessor"></span>
<a name="l00912"></a>00912   }
<a name="l00913"></a>00913 
<a name="l00914"></a>00914 
<a name="l00915"></a>00915   <span class="comment">/*******************</span>
<a name="l00916"></a>00916 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00917"></a>00917 <span class="comment">   *******************/</span>
<a name="l00918"></a>00918 
<a name="l00919"></a>00919 
<a name="l00921"></a>00921 
<a name="l00925"></a>00925   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00926"></a>00926   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a4be3dd5024addbf8bfa332193c4030cd" title="Returns the number of elements stored in memory.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::GetNonZeros</a>()<span class="keyword"> const</span>
<a name="l00927"></a>00927 <span class="keyword">  </span>{
<a name="l00928"></a>00928     <span class="keywordflow">return</span> nz_;
<a name="l00929"></a>00929   }
<a name="l00930"></a>00930 
<a name="l00931"></a>00931 
<a name="l00933"></a>00933 
<a name="l00937"></a>00937   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00938"></a>00938   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a8950827192f33de71909f783032c9f5f" title="Returns the number of elements stored in memory.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::GetDataSize</a>()<span class="keyword"> const</span>
<a name="l00939"></a>00939 <span class="keyword">  </span>{
<a name="l00940"></a>00940     <span class="keywordflow">return</span> nz_;
<a name="l00941"></a>00941   }
<a name="l00942"></a>00942 
<a name="l00943"></a>00943 
<a name="l00945"></a>00945 
<a name="l00949"></a>00949   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00950"></a>00950   <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#ae6576503580320224c84cc46d675781d" title="Returns (row or column) start indices.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::GetPtr</a>()<span class="keyword"> const</span>
<a name="l00951"></a>00951 <span class="keyword">  </span>{
<a name="l00952"></a>00952     <span class="keywordflow">return</span> ptr_;
<a name="l00953"></a>00953   }
<a name="l00954"></a>00954 
<a name="l00955"></a>00955 
<a name="l00957"></a>00957 
<a name="l00964"></a>00964   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00965"></a>00965   <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a9d4ddb6035ed47fadc9f35df03c3764e" title="Returns (row or column) indices of non-zero entries.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::GetInd</a>()<span class="keyword"> const</span>
<a name="l00966"></a>00966 <span class="keyword">  </span>{
<a name="l00967"></a>00967     <span class="keywordflow">return</span> ind_;
<a name="l00968"></a>00968   }
<a name="l00969"></a>00969 
<a name="l00970"></a>00970 
<a name="l00972"></a>00972 
<a name="l00975"></a>00975   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00976"></a>00976   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a046fce2e283dc38a9ed0ce5b570e6f11" title="Returns the length of the array of start indices.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::GetPtrSize</a>()<span class="keyword"> const</span>
<a name="l00977"></a>00977 <span class="keyword">  </span>{
<a name="l00978"></a>00978     <span class="keywordflow">return</span> (this-&gt;m_ + 1);
<a name="l00979"></a>00979   }
<a name="l00980"></a>00980 
<a name="l00981"></a>00981 
<a name="l00983"></a>00983 
<a name="l00992"></a>00992   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00993"></a>00993   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a6ffb37230f5629d9f85a6b962bc8127d" title="Returns the length of the array of (column or row) indices.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::GetIndSize</a>()<span class="keyword"> const</span>
<a name="l00994"></a>00994 <span class="keyword">  </span>{
<a name="l00995"></a>00995     <span class="keywordflow">return</span> nz_;
<a name="l00996"></a>00996   }
<a name="l00997"></a>00997 
<a name="l00998"></a>00998 
<a name="l00999"></a>00999   <span class="comment">/**********************************</span>
<a name="l01000"></a>01000 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l01001"></a>01001 <span class="comment">   **********************************/</span>
<a name="l01002"></a>01002 
<a name="l01003"></a>01003 
<a name="l01005"></a>01005 
<a name="l01011"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aa899e8ece6a4210b5199a37e3e60b401">01011</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01012"></a>01012   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::value_type</a>
<a name="l01013"></a>01013   <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aa899e8ece6a4210b5199a37e3e60b401" title="Access operator.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i,
<a name="l01014"></a>01014                                                              <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l01015"></a>01015 <span class="keyword">  </span>{
<a name="l01016"></a>01016 
<a name="l01017"></a>01017 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01018"></a>01018 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01019"></a>01019       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_SymSparse::operator()&quot;</span>,
<a name="l01020"></a>01020                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l01021"></a>01021                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01022"></a>01022     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01023"></a>01023       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_SymSparse::operator()&quot;</span>,
<a name="l01024"></a>01024                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l01025"></a>01025                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01026"></a>01026 <span class="preprocessor">#endif</span>
<a name="l01027"></a>01027 <span class="preprocessor"></span>
<a name="l01028"></a>01028     <span class="keywordtype">int</span> k, l;
<a name="l01029"></a>01029     <span class="keywordtype">int</span> a, b;
<a name="l01030"></a>01030 
<a name="l01031"></a>01031     <span class="comment">// Only the upper part is stored.</span>
<a name="l01032"></a>01032     <span class="keywordflow">if</span> (i &gt; j)
<a name="l01033"></a>01033       {
<a name="l01034"></a>01034         l = i;
<a name="l01035"></a>01035         i = j;
<a name="l01036"></a>01036         j = l;
<a name="l01037"></a>01037       }
<a name="l01038"></a>01038 
<a name="l01039"></a>01039     a = ptr_[Storage::GetFirst(i, j)];
<a name="l01040"></a>01040     b = ptr_[Storage::GetFirst(i, j) + 1];
<a name="l01041"></a>01041 
<a name="l01042"></a>01042     <span class="keywordflow">if</span> (a == b)
<a name="l01043"></a>01043       <span class="keywordflow">return</span> T(0);
<a name="l01044"></a>01044 
<a name="l01045"></a>01045     l = Storage::GetSecond(i, j);
<a name="l01046"></a>01046 
<a name="l01047"></a>01047     <span class="keywordflow">for</span> (k = a; (k&lt;b-1) &amp;&amp; (ind_[k]&lt;l); k++);
<a name="l01048"></a>01048 
<a name="l01049"></a>01049     <span class="keywordflow">if</span> (ind_[k] == l)
<a name="l01050"></a>01050       <span class="keywordflow">return</span> this-&gt;data_[k];
<a name="l01051"></a>01051     <span class="keywordflow">else</span>
<a name="l01052"></a>01052       <span class="keywordflow">return</span> T(0);
<a name="l01053"></a>01053   }
<a name="l01054"></a>01054 
<a name="l01055"></a>01055 
<a name="l01057"></a>01057 
<a name="l01065"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaca494c5d6a5c03b5a76c3cfbfc412fe">01065</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01066"></a>01066   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::value_type</a>&amp;
<a name="l01067"></a>01067   <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaca494c5d6a5c03b5a76c3cfbfc412fe" title="Access method.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01068"></a>01068   {
<a name="l01069"></a>01069 
<a name="l01070"></a>01070 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01071"></a>01071 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01072"></a>01072       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Val(int, int)&quot;</span>,
<a name="l01073"></a>01073                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l01074"></a>01074                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01075"></a>01075     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01076"></a>01076       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Val(int, int)&quot;</span>,
<a name="l01077"></a>01077                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l01078"></a>01078                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01079"></a>01079 <span class="preprocessor">#endif</span>
<a name="l01080"></a>01080 <span class="preprocessor"></span>
<a name="l01081"></a>01081     <span class="keywordtype">int</span> k, l;
<a name="l01082"></a>01082     <span class="keywordtype">int</span> a, b;
<a name="l01083"></a>01083 
<a name="l01084"></a>01084     <span class="comment">// Only the upper part is stored.</span>
<a name="l01085"></a>01085     <span class="keywordflow">if</span> (i &gt; j)
<a name="l01086"></a>01086       {
<a name="l01087"></a>01087         l = i;
<a name="l01088"></a>01088         i = j;
<a name="l01089"></a>01089         j = l;
<a name="l01090"></a>01090       }
<a name="l01091"></a>01091 
<a name="l01092"></a>01092     a = ptr_[Storage::GetFirst(i, j)];
<a name="l01093"></a>01093     b = ptr_[Storage::GetFirst(i, j) + 1];
<a name="l01094"></a>01094 
<a name="l01095"></a>01095     <span class="keywordflow">if</span> (a == b)
<a name="l01096"></a>01096       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Val(int, int)&quot;</span>,
<a name="l01097"></a>01097                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l01098"></a>01098                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l01099"></a>01099                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l01100"></a>01100 
<a name="l01101"></a>01101     l = Storage::GetSecond(i, j);
<a name="l01102"></a>01102 
<a name="l01103"></a>01103     <span class="keywordflow">for</span> (k = a; (k&lt;b-1) &amp;&amp; (ind_[k]&lt;l); k++);
<a name="l01104"></a>01104 
<a name="l01105"></a>01105     <span class="keywordflow">if</span> (ind_[k] == l)
<a name="l01106"></a>01106       <span class="keywordflow">return</span> this-&gt;data_[k];
<a name="l01107"></a>01107     <span class="keywordflow">else</span>
<a name="l01108"></a>01108       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Val(int, int)&quot;</span>,
<a name="l01109"></a>01109                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l01110"></a>01110                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l01111"></a>01111                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l01112"></a>01112   }
<a name="l01113"></a>01113 
<a name="l01114"></a>01114 
<a name="l01116"></a>01116 
<a name="l01124"></a>01124   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01125"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a30ac9be8e0a8f70f3c2eae3f2e6657bb">01125</a>   <span class="keyword">inline</span>
<a name="l01126"></a>01126   <span class="keyword">const</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::value_type</a>&amp;
<a name="l01127"></a>01127   <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaca494c5d6a5c03b5a76c3cfbfc412fe" title="Access method.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l01128"></a>01128 <span class="keyword">  </span>{
<a name="l01129"></a>01129 
<a name="l01130"></a>01130 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01131"></a>01131 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01132"></a>01132       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Val(int, int)&quot;</span>,
<a name="l01133"></a>01133                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l01134"></a>01134                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01135"></a>01135     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01136"></a>01136       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Val(int, int)&quot;</span>,
<a name="l01137"></a>01137                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l01138"></a>01138                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01139"></a>01139 <span class="preprocessor">#endif</span>
<a name="l01140"></a>01140 <span class="preprocessor"></span>
<a name="l01141"></a>01141     <span class="keywordtype">int</span> k, l;
<a name="l01142"></a>01142     <span class="keywordtype">int</span> a, b;
<a name="l01143"></a>01143 
<a name="l01144"></a>01144     <span class="comment">// Only the upper part is stored.</span>
<a name="l01145"></a>01145     <span class="keywordflow">if</span> (i &gt; j)
<a name="l01146"></a>01146       {
<a name="l01147"></a>01147         l = i;
<a name="l01148"></a>01148         i = j;
<a name="l01149"></a>01149         j = l;
<a name="l01150"></a>01150       }
<a name="l01151"></a>01151 
<a name="l01152"></a>01152     a = ptr_[Storage::GetFirst(i, j)];
<a name="l01153"></a>01153     b = ptr_[Storage::GetFirst(i, j) + 1];
<a name="l01154"></a>01154 
<a name="l01155"></a>01155     <span class="keywordflow">if</span> (a == b)
<a name="l01156"></a>01156       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Val(int, int)&quot;</span>,
<a name="l01157"></a>01157                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l01158"></a>01158                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l01159"></a>01159                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l01160"></a>01160 
<a name="l01161"></a>01161     l = Storage::GetSecond(i, j);
<a name="l01162"></a>01162 
<a name="l01163"></a>01163     <span class="keywordflow">for</span> (k = a; (k&lt;b-1) &amp;&amp; (ind_[k]&lt;l); k++);
<a name="l01164"></a>01164 
<a name="l01165"></a>01165     <span class="keywordflow">if</span> (ind_[k] == l)
<a name="l01166"></a>01166       <span class="keywordflow">return</span> this-&gt;data_[k];
<a name="l01167"></a>01167     <span class="keywordflow">else</span>
<a name="l01168"></a>01168       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Val(int, int)&quot;</span>,
<a name="l01169"></a>01169                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l01170"></a>01170                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l01171"></a>01171                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l01172"></a>01172   }
<a name="l01173"></a>01173 
<a name="l01174"></a>01174 
<a name="l01176"></a>01176 
<a name="l01183"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a07a75313d8d3b10eaf6a9363327f0d5e">01183</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01184"></a>01184   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::value_type</a>&amp;
<a name="l01185"></a>01185   <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a07a75313d8d3b10eaf6a9363327f0d5e" title="Access method.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01186"></a>01186   {
<a name="l01187"></a>01187 
<a name="l01188"></a>01188 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01189"></a>01189 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01190"></a>01190       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Get(int, int)&quot;</span>,
<a name="l01191"></a>01191                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l01192"></a>01192                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01193"></a>01193     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01194"></a>01194       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Get(int, int)&quot;</span>,
<a name="l01195"></a>01195                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l01196"></a>01196                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01197"></a>01197 <span class="preprocessor">#endif</span>
<a name="l01198"></a>01198 <span class="preprocessor"></span>
<a name="l01199"></a>01199     <span class="keywordtype">int</span> k, l;
<a name="l01200"></a>01200     <span class="keywordtype">int</span> a, b;
<a name="l01201"></a>01201     <span class="comment">// Only the upper part is stored.</span>
<a name="l01202"></a>01202     <span class="keywordflow">if</span> (i &gt; j)
<a name="l01203"></a>01203       {
<a name="l01204"></a>01204         l = i;
<a name="l01205"></a>01205         i = j;
<a name="l01206"></a>01206         j = l;
<a name="l01207"></a>01207       }
<a name="l01208"></a>01208 
<a name="l01209"></a>01209     a = ptr_[Storage::GetFirst(i, j)];
<a name="l01210"></a>01210     b = ptr_[Storage::GetFirst(i, j) + 1];
<a name="l01211"></a>01211 
<a name="l01212"></a>01212     <span class="keywordflow">if</span> (a &lt; b)
<a name="l01213"></a>01213       {
<a name="l01214"></a>01214         l = Storage::GetSecond(i, j);
<a name="l01215"></a>01215 
<a name="l01216"></a>01216         <span class="keywordflow">for</span> (k = a; (k &lt; b) &amp;&amp; (ind_[k] &lt; l); k++);
<a name="l01217"></a>01217 
<a name="l01218"></a>01218         <span class="keywordflow">if</span> ( (k &lt; b) &amp;&amp; (ind_[k] == l))
<a name="l01219"></a>01219           <span class="keywordflow">return</span> this-&gt;data_[k];
<a name="l01220"></a>01220       }
<a name="l01221"></a>01221     <span class="keywordflow">else</span>
<a name="l01222"></a>01222       k = a;
<a name="l01223"></a>01223 
<a name="l01224"></a>01224     <span class="comment">// adding a non-zero entry</span>
<a name="l01225"></a>01225     <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a00809c7e5168b170c34e6e9c60d03674" title="Changing the number of rows and columns.">Resize</a>(this-&gt;m_, this-&gt;n_, nz_+1);
<a name="l01226"></a>01226 
<a name="l01227"></a>01227     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> m = Storage::GetFirst(i, j)+1; m &lt;= this-&gt;m_; m++)
<a name="l01228"></a>01228       ptr_[m]++;
<a name="l01229"></a>01229 
<a name="l01230"></a>01230     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> m = nz_-1; m &gt;= k+1; m--)
<a name="l01231"></a>01231       {
<a name="l01232"></a>01232         ind_[m] = ind_[m-1];
<a name="l01233"></a>01233         this-&gt;data_[m] = this-&gt;data_[m-1];
<a name="l01234"></a>01234       }
<a name="l01235"></a>01235 
<a name="l01236"></a>01236     ind_[k] = Storage::GetSecond(i, j);
<a name="l01237"></a>01237 
<a name="l01238"></a>01238     <span class="comment">// value of new non-zero entry is set to 0</span>
<a name="l01239"></a>01239     SetComplexZero(this-&gt;data_[k]);
<a name="l01240"></a>01240 
<a name="l01241"></a>01241     <span class="keywordflow">return</span> this-&gt;data_[k];
<a name="l01242"></a>01242   }
<a name="l01243"></a>01243 
<a name="l01244"></a>01244 
<a name="l01246"></a>01246 
<a name="l01251"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a60b6afbb8ec16173531ff2aa190b7afb">01251</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01252"></a>01252   <span class="keyword">inline</span> <span class="keyword">const</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::value_type</a>&amp;
<a name="l01253"></a>01253   <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a07a75313d8d3b10eaf6a9363327f0d5e" title="Access method.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l01254"></a>01254 <span class="keyword">  </span>{
<a name="l01255"></a>01255     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaca494c5d6a5c03b5a76c3cfbfc412fe" title="Access method.">Val</a>(i, j);
<a name="l01256"></a>01256   }
<a name="l01257"></a>01257 
<a name="l01258"></a>01258 
<a name="l01260"></a>01260 
<a name="l01267"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a044259b03537794337e0c05c55af5075">01267</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01268"></a>01268   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a044259b03537794337e0c05c55af5075" title="Add a value to a non-zero entry.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01269"></a>01269 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a044259b03537794337e0c05c55af5075" title="Add a value to a non-zero entry.">  ::AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; val)
<a name="l01270"></a>01270   {
<a name="l01271"></a>01271     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l01272"></a>01272       Get(i, j) += val;
<a name="l01273"></a>01273   }
<a name="l01274"></a>01274 
<a name="l01275"></a>01275 
<a name="l01277"></a>01277 
<a name="l01282"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a04a627a376c55f6afd831018805ba9a7">01282</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01283"></a>01283   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a04a627a376c55f6afd831018805ba9a7" title="Sets an element (i, j) to a value.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01284"></a>01284 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a04a627a376c55f6afd831018805ba9a7" title="Sets an element (i, j) to a value.">  ::Set</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; val)
<a name="l01285"></a>01285   {
<a name="l01286"></a>01286     Get(i, j) = val;
<a name="l01287"></a>01287   }
<a name="l01288"></a>01288 
<a name="l01289"></a>01289 
<a name="l01291"></a>01291 
<a name="l01296"></a>01296   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01297"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#ad9e277dce0daf0cfb76e81e6087bcb75">01297</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l01298"></a>01298   <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#ad9e277dce0daf0cfb76e81e6087bcb75" title="Duplicates a matrix (assignment operator).">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01299"></a>01299 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#ad9e277dce0daf0cfb76e81e6087bcb75" title="Duplicates a matrix (assignment operator).">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l01300"></a>01300   {
<a name="l01301"></a>01301     this-&gt;Copy(A);
<a name="l01302"></a>01302 
<a name="l01303"></a>01303     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01304"></a>01304   }
<a name="l01305"></a>01305 
<a name="l01306"></a>01306 
<a name="l01307"></a>01307   <span class="comment">/************************</span>
<a name="l01308"></a>01308 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l01309"></a>01309 <span class="comment">   ************************/</span>
<a name="l01310"></a>01310 
<a name="l01311"></a>01311 
<a name="l01313"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a5366839188776ddfed4053241a42e010">01313</a> 
<a name="l01314"></a>01314   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01315"></a>01315   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a5366839188776ddfed4053241a42e010" title="Resets all non-zero entries to 0-value.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::Zero</a>()
<a name="l01316"></a>01316   {
<a name="l01317"></a>01317     this-&gt;allocator_.memoryset(this-&gt;data_, <span class="keywordtype">char</span>(0),
<a name="l01318"></a>01318                                this-&gt;nz_ * <span class="keyword">sizeof</span>(value_type));
<a name="l01319"></a>01319   }
<a name="l01320"></a>01320 
<a name="l01321"></a>01321 
<a name="l01323"></a>01323 
<a name="l01325"></a>01325   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01326"></a>01326   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aa10b71d5e4eca83a9c61ad352d99a369" title="Sets the matrix to identity.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::SetIdentity</a>()
<a name="l01327"></a>01327   {
<a name="l01328"></a>01328     <span class="keywordtype">int</span> m = this-&gt;m_;
<a name="l01329"></a>01329     <span class="keywordtype">int</span> nz = this-&gt;m_;
<a name="l01330"></a>01330 
<a name="l01331"></a>01331     <span class="keywordflow">if</span> (nz == 0)
<a name="l01332"></a>01332       <span class="keywordflow">return</span>;
<a name="l01333"></a>01333 
<a name="l01334"></a>01334     <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a71256dab229c463457ff8848aa6d1fa7" title="Clears the matrix.">Clear</a>();
<a name="l01335"></a>01335 
<a name="l01336"></a>01336     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> values(nz);
<a name="l01337"></a>01337     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; ptr(m + 1);
<a name="l01338"></a>01338     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; ind(nz);
<a name="l01339"></a>01339 
<a name="l01340"></a>01340     values.Fill(T(1));
<a name="l01341"></a>01341     ind.Fill();
<a name="l01342"></a>01342     ptr.Fill();
<a name="l01343"></a>01343 
<a name="l01344"></a>01344     <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aeb3fdc796d3c31e4b4729d3e9e15834c" title="Redefines the matrix.">SetData</a>(m, m, values, ptr, ind);
<a name="l01345"></a>01345   }
<a name="l01346"></a>01346 
<a name="l01347"></a>01347 
<a name="l01349"></a>01349 
<a name="l01352"></a>01352   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01353"></a>01353   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a4bf9e5c00324310123384c8c890cbe69" title="Fills the non-zero entries with 0, 1, 2, ...">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::Fill</a>()
<a name="l01354"></a>01354   {
<a name="l01355"></a>01355     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a8950827192f33de71909f783032c9f5f" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l01356"></a>01356       this-&gt;data_[i] = i;
<a name="l01357"></a>01357   }
<a name="l01358"></a>01358 
<a name="l01359"></a>01359 
<a name="l01361"></a>01361 
<a name="l01364"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#afca6e6228ab0a0fe3a89e0570e9a6769">01364</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01365"></a>01365   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01366"></a>01366   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a4bf9e5c00324310123384c8c890cbe69" title="Fills the non-zero entries with 0, 1, 2, ...">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::Fill</a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01367"></a>01367   {
<a name="l01368"></a>01368     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a8950827192f33de71909f783032c9f5f" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l01369"></a>01369       this-&gt;data_[i] = x;
<a name="l01370"></a>01370   }
<a name="l01371"></a>01371 
<a name="l01372"></a>01372 
<a name="l01374"></a>01374 
<a name="l01377"></a>01377   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01378"></a>01378   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aa3d98ae0ee6651e491219b309b6edc32" title="Fills the non-zero entries randomly.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::FillRand</a>()
<a name="l01379"></a>01379   {
<a name="l01380"></a>01380     srand(time(NULL));
<a name="l01381"></a>01381     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a8950827192f33de71909f783032c9f5f" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l01382"></a>01382       this-&gt;data_[i] = rand();
<a name="l01383"></a>01383   }
<a name="l01384"></a>01384 
<a name="l01385"></a>01385 
<a name="l01387"></a>01387 
<a name="l01392"></a>01392   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01393"></a>01393   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a20b7ac2a821a2f54f4e325c07846b6cd" title="Displays the matrix on the standard output.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l01394"></a>01394 <span class="keyword">  </span>{
<a name="l01395"></a>01395     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l01396"></a>01396       {
<a name="l01397"></a>01397         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;n_; j++)
<a name="l01398"></a>01398           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l01399"></a>01399         cout &lt;&lt; endl;
<a name="l01400"></a>01400       }
<a name="l01401"></a>01401   }
<a name="l01402"></a>01402 
<a name="l01403"></a>01403 
<a name="l01405"></a>01405 
<a name="l01409"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aa2fef9c935ab0c5b76dce130efc41399">01409</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01410"></a>01410   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aa2fef9c935ab0c5b76dce130efc41399" title="Writes the matrix in a file.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01411"></a>01411 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aa2fef9c935ab0c5b76dce130efc41399" title="Writes the matrix in a file.">  ::Write</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l01412"></a>01412 <span class="keyword">  </span>{
<a name="l01413"></a>01413     ofstream FileStream;
<a name="l01414"></a>01414     FileStream.open(FileName.c_str());
<a name="l01415"></a>01415 
<a name="l01416"></a>01416 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01417"></a>01417 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01418"></a>01418     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01419"></a>01419       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Write(string FileName)&quot;</span>,
<a name="l01420"></a>01420                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01421"></a>01421 <span class="preprocessor">#endif</span>
<a name="l01422"></a>01422 <span class="preprocessor"></span>
<a name="l01423"></a>01423     this-&gt;Write(FileStream);
<a name="l01424"></a>01424 
<a name="l01425"></a>01425     FileStream.close();
<a name="l01426"></a>01426   }
<a name="l01427"></a>01427 
<a name="l01428"></a>01428 
<a name="l01430"></a>01430 
<a name="l01434"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#ae50c3f85534149cc86221ca81e3a8338">01434</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01435"></a>01435   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aa2fef9c935ab0c5b76dce130efc41399" title="Writes the matrix in a file.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01436"></a>01436 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aa2fef9c935ab0c5b76dce130efc41399" title="Writes the matrix in a file.">  ::Write</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l01437"></a>01437 <span class="keyword">  </span>{
<a name="l01438"></a>01438 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01439"></a>01439 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01440"></a>01440     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01441"></a>01441       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l01442"></a>01442                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l01443"></a>01443 <span class="preprocessor">#endif</span>
<a name="l01444"></a>01444 <span class="preprocessor"></span>
<a name="l01445"></a>01445     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l01446"></a>01446                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01447"></a>01447     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l01448"></a>01448                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01449"></a>01449     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;nz_)),
<a name="l01450"></a>01450                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01451"></a>01451 
<a name="l01452"></a>01452     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;ptr_),
<a name="l01453"></a>01453                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)*(this-&gt;m_+1));
<a name="l01454"></a>01454     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;ind_),
<a name="l01455"></a>01455                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)*this-&gt;nz_);
<a name="l01456"></a>01456     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l01457"></a>01457                      <span class="keyword">sizeof</span>(T)*this-&gt;nz_);
<a name="l01458"></a>01458   }
<a name="l01459"></a>01459 
<a name="l01460"></a>01460 
<a name="l01462"></a>01462 
<a name="l01467"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#ab1a9893cc7b8823fe6a2a0eee9520460">01467</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01468"></a>01468   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#ab1a9893cc7b8823fe6a2a0eee9520460" title="Writes the matrix in a file.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01469"></a>01469 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#ab1a9893cc7b8823fe6a2a0eee9520460" title="Writes the matrix in a file.">  ::WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l01470"></a>01470 <span class="keyword">  </span>{
<a name="l01471"></a>01471     ofstream FileStream; FileStream.precision(14);
<a name="l01472"></a>01472     FileStream.open(FileName.c_str());
<a name="l01473"></a>01473 
<a name="l01474"></a>01474 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01475"></a>01475 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01476"></a>01476     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01477"></a>01477       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_SymSparse::WriteText(string FileName)&quot;</span>,
<a name="l01478"></a>01478                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01479"></a>01479 <span class="preprocessor">#endif</span>
<a name="l01480"></a>01480 <span class="preprocessor"></span>
<a name="l01481"></a>01481     this-&gt;WriteText(FileStream);
<a name="l01482"></a>01482 
<a name="l01483"></a>01483     FileStream.close();
<a name="l01484"></a>01484   }
<a name="l01485"></a>01485 
<a name="l01486"></a>01486 
<a name="l01488"></a>01488 
<a name="l01493"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aa28e902cd297fe56f7a88cea69c154d5">01493</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01494"></a>01494   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#ab1a9893cc7b8823fe6a2a0eee9520460" title="Writes the matrix in a file.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01495"></a>01495 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#ab1a9893cc7b8823fe6a2a0eee9520460" title="Writes the matrix in a file.">  ::WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l01496"></a>01496 <span class="keyword">  </span>{
<a name="l01497"></a>01497 
<a name="l01498"></a>01498 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01499"></a>01499 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01500"></a>01500     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01501"></a>01501       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_SymSparse::WriteText(ofstream&amp; FileStream)&quot;</span>,
<a name="l01502"></a>01502                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l01503"></a>01503 <span class="preprocessor">#endif</span>
<a name="l01504"></a>01504 <span class="preprocessor"></span>
<a name="l01505"></a>01505     <span class="comment">// Conversion to coordinate format (1-index convention).</span>
<a name="l01506"></a>01506     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> IndRow, IndCol;
<a name="l01507"></a>01507     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> Value;
<a name="l01508"></a>01508     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; leaf_class =
<a name="l01509"></a>01509       <span class="keyword">static_cast&lt;</span><span class="keyword">const </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; <span class="keyword">&gt;</span>(*this);
<a name="l01510"></a>01510 
<a name="l01511"></a>01511     <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(leaf_class, IndRow, IndCol,
<a name="l01512"></a>01512                                  Value, 1, <span class="keyword">true</span>);
<a name="l01513"></a>01513 
<a name="l01514"></a>01514     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; IndRow.GetM(); i++)
<a name="l01515"></a>01515       FileStream &lt;&lt; IndRow(i) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; IndCol(i) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; Value(i) &lt;&lt; <span class="charliteral">&#39;\n&#39;</span>;
<a name="l01516"></a>01516   }
<a name="l01517"></a>01517 
<a name="l01518"></a>01518 
<a name="l01520"></a>01520 
<a name="l01524"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a882ab4d19bdff682493063a7a98e7631">01524</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01525"></a>01525   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a882ab4d19bdff682493063a7a98e7631" title="Reads the matrix from a file.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01526"></a>01526 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a882ab4d19bdff682493063a7a98e7631" title="Reads the matrix from a file.">  ::Read</a>(<span class="keywordtype">string</span> FileName)
<a name="l01527"></a>01527   {
<a name="l01528"></a>01528     ifstream FileStream;
<a name="l01529"></a>01529     FileStream.open(FileName.c_str());
<a name="l01530"></a>01530 
<a name="l01531"></a>01531 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01532"></a>01532 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01533"></a>01533     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01534"></a>01534       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Read(string FileName)&quot;</span>,
<a name="l01535"></a>01535                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01536"></a>01536 <span class="preprocessor">#endif</span>
<a name="l01537"></a>01537 <span class="preprocessor"></span>
<a name="l01538"></a>01538     this-&gt;Read(FileStream);
<a name="l01539"></a>01539 
<a name="l01540"></a>01540     FileStream.close();
<a name="l01541"></a>01541   }
<a name="l01542"></a>01542 
<a name="l01543"></a>01543 
<a name="l01545"></a>01545 
<a name="l01549"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a4f3318c78df19c1a44d5f934c5707095">01549</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01550"></a>01550   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a882ab4d19bdff682493063a7a98e7631" title="Reads the matrix from a file.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l01551"></a>01551 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a882ab4d19bdff682493063a7a98e7631" title="Reads the matrix from a file.">  Read</a>(istream&amp; FileStream)
<a name="l01552"></a>01552   {
<a name="l01553"></a>01553 
<a name="l01554"></a>01554 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01555"></a>01555 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01556"></a>01556     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01557"></a>01557       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Read(ofstream&amp; FileStream)&quot;</span>,
<a name="l01558"></a>01558                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l01559"></a>01559 <span class="preprocessor">#endif</span>
<a name="l01560"></a>01560 <span class="preprocessor"></span>
<a name="l01561"></a>01561     <span class="keywordtype">int</span> m, n, nz;
<a name="l01562"></a>01562     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;m), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01563"></a>01563     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;n), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01564"></a>01564     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;nz), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01565"></a>01565 
<a name="l01566"></a>01566     <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a03e2a2b861d5e2df212db3aeaf144426" title="Initialization of an empty sparse matrix with i rows and j columns.">Reallocate</a>(m, m, nz);
<a name="l01567"></a>01567 
<a name="l01568"></a>01568     FileStream.read(reinterpret_cast&lt;char*&gt;(ptr_),
<a name="l01569"></a>01569                     <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)*(m+1));
<a name="l01570"></a>01570     FileStream.read(reinterpret_cast&lt;char*&gt;(ind_), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)*nz);
<a name="l01571"></a>01571     FileStream.read(reinterpret_cast&lt;char*&gt;(this-&gt;data_), <span class="keyword">sizeof</span>(T)*nz);
<a name="l01572"></a>01572 
<a name="l01573"></a>01573 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01574"></a>01574 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l01575"></a>01575     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01576"></a>01576       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Read(istream&amp; FileStream)&quot;</span>,
<a name="l01577"></a>01577                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Input operation failed.&quot;</span>)
<a name="l01578"></a>01578                     + string(<span class="stringliteral">&quot; The input file may have been removed&quot;</span>)
<a name="l01579"></a>01579                     + <span class="stringliteral">&quot; or may not contain enough data.&quot;</span>);
<a name="l01580"></a>01580 <span class="preprocessor">#endif</span>
<a name="l01581"></a>01581 <span class="preprocessor"></span>
<a name="l01582"></a>01582   }
<a name="l01583"></a>01583 
<a name="l01584"></a>01584 
<a name="l01586"></a>01586 
<a name="l01590"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#af55e946912a9936394784f9fd2968b74">01590</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01591"></a>01591   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#af55e946912a9936394784f9fd2968b74" title="Reads the matrix from a file.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l01592"></a>01592 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#af55e946912a9936394784f9fd2968b74" title="Reads the matrix from a file.">  ReadText</a>(<span class="keywordtype">string</span> FileName)
<a name="l01593"></a>01593   {
<a name="l01594"></a>01594     ifstream FileStream;
<a name="l01595"></a>01595     FileStream.open(FileName.c_str());
<a name="l01596"></a>01596 
<a name="l01597"></a>01597 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01598"></a>01598 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01599"></a>01599     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01600"></a>01600       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_SymSparse::ReadText(string FileName)&quot;</span>,
<a name="l01601"></a>01601                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01602"></a>01602 <span class="preprocessor">#endif</span>
<a name="l01603"></a>01603 <span class="preprocessor"></span>
<a name="l01604"></a>01604     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#af55e946912a9936394784f9fd2968b74" title="Reads the matrix from a file.">ReadText</a>(FileStream);
<a name="l01605"></a>01605 
<a name="l01606"></a>01606     FileStream.close();
<a name="l01607"></a>01607   }
<a name="l01608"></a>01608 
<a name="l01609"></a>01609 
<a name="l01611"></a>01611 
<a name="l01615"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaa7ab4ddb5c00c5ba6582661d8920e63">01615</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01616"></a>01616   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#af55e946912a9936394784f9fd2968b74" title="Reads the matrix from a file.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l01617"></a>01617 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#af55e946912a9936394784f9fd2968b74" title="Reads the matrix from a file.">  ReadText</a>(istream&amp; FileStream)
<a name="l01618"></a>01618   {
<a name="l01619"></a>01619     <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; leaf_class =
<a name="l01620"></a>01620       <span class="keyword">static_cast&lt;</span><a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; <span class="keyword">&gt;</span>(*this);
<a name="l01621"></a>01621 
<a name="l01622"></a>01622     T zero; <span class="keywordtype">int</span> index = 1;
<a name="l01623"></a>01623     <a class="code" href="namespace_seldon.php#af8428b13721ad15b95a74ecf07d23668" title="Reading of matrix in coordinate format.">ReadCoordinateMatrix</a>(leaf_class, FileStream, zero, index);
<a name="l01624"></a>01624   }
<a name="l01625"></a>01625 
<a name="l01626"></a>01626 
<a name="l01628"></a>01628   <span class="comment">// MATRIX&lt;COLSYMSPARSE&gt; //</span>
<a name="l01630"></a>01630 <span class="comment"></span>
<a name="l01631"></a>01631 
<a name="l01632"></a>01632   <span class="comment">/****************</span>
<a name="l01633"></a>01633 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01634"></a>01634 <span class="comment">   ****************/</span>
<a name="l01635"></a>01635 
<a name="l01637"></a>01637 
<a name="l01640"></a>01640   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01641"></a>01641   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSymSparse, Allocator&gt;::Matrix</a>():
<a name="l01642"></a>01642     <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_sym_sparse.php">ColSymSparse</a>, Allocator&gt;()
<a name="l01643"></a>01643   {
<a name="l01644"></a>01644   }
<a name="l01645"></a>01645 
<a name="l01646"></a>01646 
<a name="l01648"></a>01648 
<a name="l01652"></a>01652   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01653"></a>01653   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_sparse_00_01_allocator_01_4.php#ab59d8490dc7405a4fc91b4e019bfa10f" title="Default constructor.">Matrix&lt;T, Prop, ColSymSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01654"></a>01654     Matrix_SymSparse&lt;T, Prop, ColSymSparse, Allocator&gt;(i, j, 0)
<a name="l01655"></a>01655   {
<a name="l01656"></a>01656   }
<a name="l01657"></a>01657 
<a name="l01658"></a>01658 
<a name="l01660"></a>01660 
<a name="l01667"></a>01667   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01668"></a>01668   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_sparse_00_01_allocator_01_4.php#ab59d8490dc7405a4fc91b4e019bfa10f" title="Default constructor.">Matrix&lt;T, Prop, ColSymSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> nz):
<a name="l01669"></a>01669     Matrix_SymSparse&lt;T, Prop, ColSymSparse, Allocator&gt;(i, j, nz)
<a name="l01670"></a>01670   {
<a name="l01671"></a>01671   }
<a name="l01672"></a>01672 
<a name="l01673"></a>01673 
<a name="l01675"></a>01675 
<a name="l01687"></a>01687   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01688"></a>01688   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01689"></a>01689             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01690"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_sparse_00_01_allocator_01_4.php#a496b0a427faaea22d2d342b1263e4d00">01690</a>             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01691"></a>01691   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSymSparse, Allocator&gt;::</a>
<a name="l01692"></a>01692 <a class="code" href="class_seldon_1_1_matrix.php">  Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l01693"></a>01693          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; values,
<a name="l01694"></a>01694          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; ptr,
<a name="l01695"></a>01695          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; ind):
<a name="l01696"></a>01696     <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_sym_sparse.php">ColSymSparse</a>, Allocator&gt;(i, j, values, ptr, ind)
<a name="l01697"></a>01697   {
<a name="l01698"></a>01698   }
<a name="l01699"></a>01699 
<a name="l01700"></a>01700 
<a name="l01701"></a>01701 
<a name="l01703"></a>01703   <span class="comment">// MATRIX&lt;ROWSYMSPARSE&gt; //</span>
<a name="l01705"></a>01705 <span class="comment"></span>
<a name="l01706"></a>01706 
<a name="l01707"></a>01707   <span class="comment">/****************</span>
<a name="l01708"></a>01708 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01709"></a>01709 <span class="comment">   ****************/</span>
<a name="l01710"></a>01710 
<a name="l01711"></a>01711 
<a name="l01713"></a>01713 
<a name="l01716"></a>01716   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01717"></a>01717   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSymSparse, Allocator&gt;::Matrix</a>():
<a name="l01718"></a>01718     <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_sym_sparse.php">RowSymSparse</a>, Allocator&gt;()
<a name="l01719"></a>01719   {
<a name="l01720"></a>01720   }
<a name="l01721"></a>01721 
<a name="l01722"></a>01722 
<a name="l01724"></a>01724 
<a name="l01728"></a>01728   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01729"></a>01729   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php#a11e9550e00334880bb869d69aaf76546" title="Default constructor.">Matrix&lt;T, Prop, RowSymSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01730"></a>01730     Matrix_SymSparse&lt;T, Prop, RowSymSparse, Allocator&gt;(i, j, 0)
<a name="l01731"></a>01731   {
<a name="l01732"></a>01732   }
<a name="l01733"></a>01733 
<a name="l01734"></a>01734 
<a name="l01736"></a>01736 
<a name="l01743"></a>01743   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01744"></a>01744   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php#a11e9550e00334880bb869d69aaf76546" title="Default constructor.">Matrix&lt;T, Prop, RowSymSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> nz):
<a name="l01745"></a>01745     Matrix_SymSparse&lt;T, Prop, RowSymSparse, Allocator&gt;(i, j, nz)
<a name="l01746"></a>01746   {
<a name="l01747"></a>01747   }
<a name="l01748"></a>01748 
<a name="l01749"></a>01749 
<a name="l01751"></a>01751 
<a name="l01763"></a>01763   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01764"></a>01764   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01765"></a>01765             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01766"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php#aca58932742ad99038531a482692d83f7">01766</a>             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01767"></a>01767   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSymSparse, Allocator&gt;::</a>
<a name="l01768"></a>01768 <a class="code" href="class_seldon_1_1_matrix.php">  Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l01769"></a>01769          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; values,
<a name="l01770"></a>01770          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; ptr,
<a name="l01771"></a>01771          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; ind):
<a name="l01772"></a>01772     <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_sym_sparse.php">RowSymSparse</a>, Allocator&gt;(i, j, values, ptr, ind)
<a name="l01773"></a>01773   {
<a name="l01774"></a>01774   }
<a name="l01775"></a>01775 
<a name="l01776"></a>01776 
<a name="l01777"></a>01777 } <span class="comment">// namespace Seldon.</span>
<a name="l01778"></a>01778 
<a name="l01779"></a>01779 <span class="preprocessor">#define SELDON_FILE_MATRIX_SYMSPARSE_CXX</span>
<a name="l01780"></a>01780 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
