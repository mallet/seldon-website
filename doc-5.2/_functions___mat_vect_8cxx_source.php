<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/basic_functions/Functions_MatVect.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_FUNCTIONS_MATVECT_CXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span><span class="preprocessor">#define SELDON_FILE_FUNCTIONS_MATVECT_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 
<a name="l00024"></a>00024 <span class="preprocessor">#include &quot;Functions_MatVect.hxx&quot;</span>
<a name="l00025"></a>00025 
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 <span class="comment">/*</span>
<a name="l00028"></a>00028 <span class="comment">  Functions defined in this file:</span>
<a name="l00029"></a>00029 <span class="comment"></span>
<a name="l00030"></a>00030 <span class="comment">  M X -&gt; Y</span>
<a name="l00031"></a>00031 <span class="comment">  Mlt(M, X, Y)</span>
<a name="l00032"></a>00032 <span class="comment"></span>
<a name="l00033"></a>00033 <span class="comment">  alpha M X -&gt; Y</span>
<a name="l00034"></a>00034 <span class="comment">  Mlt(alpha, M, X, Y)</span>
<a name="l00035"></a>00035 <span class="comment"></span>
<a name="l00036"></a>00036 <span class="comment">  M X -&gt; Y</span>
<a name="l00037"></a>00037 <span class="comment">  M^T X -&gt; Y</span>
<a name="l00038"></a>00038 <span class="comment">  Mlt(Trans, M, X, Y)</span>
<a name="l00039"></a>00039 <span class="comment"></span>
<a name="l00040"></a>00040 <span class="comment">  alpha M X + beta Y -&gt; Y</span>
<a name="l00041"></a>00041 <span class="comment">  MltAdd(alpha, M, X, beta, Y)</span>
<a name="l00042"></a>00042 <span class="comment"></span>
<a name="l00043"></a>00043 <span class="comment">  Gauss(M, X)</span>
<a name="l00044"></a>00044 <span class="comment"></span>
<a name="l00045"></a>00045 <span class="comment">  GaussSeidel(M, X, Y, iter)</span>
<a name="l00046"></a>00046 <span class="comment"></span>
<a name="l00047"></a>00047 <span class="comment">  SOR(M, X, Y, omega, iter)</span>
<a name="l00048"></a>00048 <span class="comment"></span>
<a name="l00049"></a>00049 <span class="comment">  SolveLU(M, Y)</span>
<a name="l00050"></a>00050 <span class="comment"></span>
<a name="l00051"></a>00051 <span class="comment">  Solve(M, Y)</span>
<a name="l00052"></a>00052 <span class="comment">*/</span>
<a name="l00053"></a>00053 
<a name="l00054"></a>00054 
<a name="l00055"></a>00055 <span class="keyword">namespace </span>Seldon
<a name="l00056"></a>00056 {
<a name="l00057"></a>00057 
<a name="l00058"></a>00058 
<a name="l00060"></a>00060   <span class="comment">// MLT //</span>
<a name="l00061"></a>00061 
<a name="l00062"></a>00062 
<a name="l00064"></a>00064 
<a name="l00072"></a>00072   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00073"></a>00073             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00074"></a>00074             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00075"></a>00075   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l00076"></a>00076            <span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00077"></a>00077            Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y)
<a name="l00078"></a>00078   {
<a name="l00079"></a>00079     Y.Fill(T2(0));
<a name="l00080"></a>00080     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(T2(1), M, X, T2(0), Y);
<a name="l00081"></a>00081   }
<a name="l00082"></a>00082 
<a name="l00083"></a>00083 
<a name="l00085"></a>00085 
<a name="l00096"></a>00096   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00097"></a>00097             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00098"></a>00098             <span class="keyword">class </span>T3, <span class="keyword">class </span>Storage3, <span class="keyword">class </span>Allocator3&gt;
<a name="l00099"></a>00099   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(<span class="keyword">const</span> T3&amp; alpha,
<a name="l00100"></a>00100            <span class="keyword">const</span> Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;&amp; M,
<a name="l00101"></a>00101            <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00102"></a>00102            Vector&lt;T3, Storage3, Allocator3&gt;&amp; Y)
<a name="l00103"></a>00103   {
<a name="l00104"></a>00104     Y.Fill(T2(0));
<a name="l00105"></a>00105     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, M, X, T3(0), Y);
<a name="l00106"></a>00106   }
<a name="l00107"></a>00107 
<a name="l00108"></a>00108 
<a name="l00110"></a>00110 
<a name="l00120"></a>00120   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00121"></a>00121             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00122"></a>00122             <span class="keyword">class </span>T3, <span class="keyword">class </span>Storage3, <span class="keyword">class </span>Allocator3&gt;
<a name="l00123"></a>00123   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(<span class="keyword">const</span> SeldonTranspose&amp; Trans,
<a name="l00124"></a>00124            <span class="keyword">const</span> Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;&amp; M,
<a name="l00125"></a>00125            <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00126"></a>00126            Vector&lt;T3, Storage3, Allocator3&gt;&amp; Y)
<a name="l00127"></a>00127   {
<a name="l00128"></a>00128     Y.Fill(T2(0));
<a name="l00129"></a>00129     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(T2(1), Trans, M, X, T3(0), Y);
<a name="l00130"></a>00130   }
<a name="l00131"></a>00131 
<a name="l00132"></a>00132 
<a name="l00133"></a>00133   <span class="comment">// MLT //</span>
<a name="l00135"></a>00135 <span class="comment"></span>
<a name="l00136"></a>00136 
<a name="l00138"></a>00138   <span class="comment">// MLTADD //</span>
<a name="l00139"></a>00139 
<a name="l00140"></a>00140 
<a name="l00141"></a>00141   <span class="comment">/*** PETSc matrices ***/</span>
<a name="l00142"></a>00142 
<a name="l00143"></a>00143 
<a name="l00144"></a>00144   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00145"></a>00145             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00146"></a>00146             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2,
<a name="l00147"></a>00147             <span class="keyword">class </span>T3,
<a name="l00148"></a>00148             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00149"></a>00149   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00150"></a>00150               <span class="keyword">const</span> Matrix&lt;T1, Prop1, PETScMPIAIJ, Allocator1&gt;&amp; M,
<a name="l00151"></a>00151               <span class="keyword">const</span> Vector&lt;T2, PETScSeq, Allocator2&gt;&amp; X,
<a name="l00152"></a>00152               <span class="keyword">const</span> T3 beta, Vector&lt;T4, PETScSeq, Allocator4&gt;&amp; Y)
<a name="l00153"></a>00153   {
<a name="l00154"></a>00154 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00155"></a>00155 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l00156"></a>00156 <span class="preprocessor">#endif</span>
<a name="l00157"></a>00157 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (beta == T3(0))
<a name="l00158"></a>00158       <span class="keywordflow">if</span> (alpha == T0(0))
<a name="l00159"></a>00159         {
<a name="l00160"></a>00160           Y.Fill(T4(0));
<a name="l00161"></a>00161           <span class="keywordflow">return</span>;
<a name="l00162"></a>00162         }
<a name="l00163"></a>00163       <span class="keywordflow">else</span>
<a name="l00164"></a>00164         {
<a name="l00165"></a>00165           MatMult(M.GetPetscMatrix(), X.GetPetscVector(), Y.GetPetscVector());
<a name="l00166"></a>00166           <span class="keywordflow">if</span> (alpha != T0(1))
<a name="l00167"></a>00167             VecScale(Y.GetPetscVector(), alpha);
<a name="l00168"></a>00168           <span class="keywordflow">return</span>;
<a name="l00169"></a>00169         }
<a name="l00170"></a>00170     <span class="keywordflow">if</span> (alpha == T0(1))
<a name="l00171"></a>00171       {
<a name="l00172"></a>00172         <span class="keywordflow">if</span> (beta != T3(1))
<a name="l00173"></a>00173           VecScale(Y.GetPetscVector(), beta);
<a name="l00174"></a>00174         MatMultAdd(M.GetPetscMatrix(), X.GetPetscVector(),
<a name="l00175"></a>00175                    Y.GetPetscVector(),Y.GetPetscVector());
<a name="l00176"></a>00176         <span class="keywordflow">return</span>;
<a name="l00177"></a>00177       }
<a name="l00178"></a>00178     Vector&lt;T2, PETScSeq, Allocator2&gt; tmp;
<a name="l00179"></a>00179     tmp.Copy(Y);
<a name="l00180"></a>00180     MatMult(M.GetPetscMatrix(), X.GetPetscVector(), tmp.GetPetscVector());
<a name="l00181"></a>00181     VecAXPBY(Y.GetPetscVector(), alpha, beta, tmp.GetPetscVector());
<a name="l00182"></a>00182     <span class="keywordflow">return</span>;
<a name="l00183"></a>00183   }
<a name="l00184"></a>00184 
<a name="l00185"></a>00185 
<a name="l00186"></a>00186   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00187"></a>00187             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00188"></a>00188             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2,
<a name="l00189"></a>00189             <span class="keyword">class </span>T3,
<a name="l00190"></a>00190             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00191"></a>00191   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00192"></a>00192               <span class="keyword">const</span> Matrix&lt;T1, Prop1, PETScMPIAIJ, Allocator1&gt;&amp; M,
<a name="l00193"></a>00193               <span class="keyword">const</span> Vector&lt;T2, PETScPar, Allocator2&gt;&amp; X,
<a name="l00194"></a>00194               <span class="keyword">const</span> T3 beta, Vector&lt;T4, PETScPar, Allocator4&gt;&amp; Y)
<a name="l00195"></a>00195   {
<a name="l00196"></a>00196 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00197"></a>00197 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l00198"></a>00198 <span class="preprocessor">#endif</span>
<a name="l00199"></a>00199 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (beta == T3(0))
<a name="l00200"></a>00200       <span class="keywordflow">if</span> (alpha == T0(0))
<a name="l00201"></a>00201         {
<a name="l00202"></a>00202           Y.Fill(T4(0));
<a name="l00203"></a>00203           <span class="keywordflow">return</span>;
<a name="l00204"></a>00204         }
<a name="l00205"></a>00205       <span class="keywordflow">else</span>
<a name="l00206"></a>00206         {
<a name="l00207"></a>00207           MatMult(M.GetPetscMatrix(), X.GetPetscVector(), Y.GetPetscVector());
<a name="l00208"></a>00208           <span class="keywordflow">if</span> (alpha != T0(1))
<a name="l00209"></a>00209             VecScale(Y.GetPetscVector(), alpha);
<a name="l00210"></a>00210           <span class="keywordflow">return</span>;
<a name="l00211"></a>00211         }
<a name="l00212"></a>00212     <span class="keywordflow">if</span> (alpha == T0(1))
<a name="l00213"></a>00213       {
<a name="l00214"></a>00214         <span class="keywordflow">if</span> (beta != T3(1))
<a name="l00215"></a>00215           VecScale(Y.GetPetscVector(), beta);
<a name="l00216"></a>00216         MatMultAdd(M.GetPetscMatrix(), X.GetPetscVector(),
<a name="l00217"></a>00217                    Y.GetPetscVector(),Y.GetPetscVector());
<a name="l00218"></a>00218         <span class="keywordflow">return</span>;
<a name="l00219"></a>00219       }
<a name="l00220"></a>00220     Vector&lt;T2, PETScPar, Allocator2&gt; tmp;
<a name="l00221"></a>00221     tmp.Copy(Y);
<a name="l00222"></a>00222     MatMult(M.GetPetscMatrix(), X.GetPetscVector(), tmp.GetPetscVector());
<a name="l00223"></a>00223     VecAXPBY(Y.GetPetscVector(), alpha, beta, tmp.GetPetscVector());
<a name="l00224"></a>00224     <span class="keywordflow">return</span>;
<a name="l00225"></a>00225   }
<a name="l00226"></a>00226 
<a name="l00227"></a>00227 
<a name="l00228"></a>00228   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00229"></a>00229             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00230"></a>00230             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2,
<a name="l00231"></a>00231             <span class="keyword">class </span>T3,
<a name="l00232"></a>00232             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00233"></a>00233   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00234"></a>00234               <span class="keyword">const</span> Matrix&lt;T1, Prop1, PETScMPIAIJ, Allocator1&gt;&amp; M,
<a name="l00235"></a>00235               <span class="keyword">const</span> Vector&lt;T2, VectFull, Allocator2&gt;&amp; X,
<a name="l00236"></a>00236               <span class="keyword">const</span> T3 beta, Vector&lt;T4, PETScSeq, Allocator4&gt;&amp; Y)
<a name="l00237"></a>00237   {
<a name="l00238"></a>00238 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00239"></a>00239 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l00240"></a>00240 <span class="preprocessor">#endif</span>
<a name="l00241"></a>00241 <span class="preprocessor"></span>
<a name="l00242"></a>00242     Vector&lt;T4, PETScSeq, Allocator4&gt; X_Petsc;
<a name="l00243"></a>00243     X_Petsc.Reallocate(X.GetM());
<a name="l00244"></a>00244     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetM(); i++)
<a name="l00245"></a>00245       X_Petsc.SetBuffer(i, X(i));
<a name="l00246"></a>00246     X_Petsc.Flush();
<a name="l00247"></a>00247 
<a name="l00248"></a>00248     <span class="keywordflow">if</span> (beta == T3(0))
<a name="l00249"></a>00249       <span class="keywordflow">if</span> (alpha == T0(0))
<a name="l00250"></a>00250         {
<a name="l00251"></a>00251           Y.Fill(T4(0));
<a name="l00252"></a>00252           <span class="keywordflow">return</span>;
<a name="l00253"></a>00253         }
<a name="l00254"></a>00254       <span class="keywordflow">else</span>
<a name="l00255"></a>00255         {
<a name="l00256"></a>00256           MatMult(M.GetPetscMatrix(), X_Petsc.GetPetscVector(),
<a name="l00257"></a>00257                   Y.GetPetscVector());
<a name="l00258"></a>00258           <span class="keywordflow">if</span> (alpha != T0(1))
<a name="l00259"></a>00259             VecScale(Y.GetPetscVector(), alpha);
<a name="l00260"></a>00260           <span class="keywordflow">return</span>;
<a name="l00261"></a>00261         }
<a name="l00262"></a>00262     <span class="keywordflow">if</span> (alpha == T0(1))
<a name="l00263"></a>00263       {
<a name="l00264"></a>00264         <span class="keywordflow">if</span> (beta != T3(1))
<a name="l00265"></a>00265           VecScale(Y.GetPetscVector(), beta);
<a name="l00266"></a>00266         MatMultAdd(M.GetPetscMatrix(), X_Petsc.GetPetscVector(),
<a name="l00267"></a>00267                    Y.GetPetscVector(),Y.GetPetscVector());
<a name="l00268"></a>00268         <span class="keywordflow">return</span>;
<a name="l00269"></a>00269       }
<a name="l00270"></a>00270     Vector&lt;T2, PETScSeq, Allocator2&gt; tmp;
<a name="l00271"></a>00271     tmp.Copy(Y);
<a name="l00272"></a>00272     MatMult(M.GetPetscMatrix(), X_Petsc.GetPetscVector(),
<a name="l00273"></a>00273             tmp.GetPetscVector());
<a name="l00274"></a>00274     VecAXPBY(Y.GetPetscVector(), alpha, beta, tmp.GetPetscVector());
<a name="l00275"></a>00275     <span class="keywordflow">return</span>;
<a name="l00276"></a>00276   }
<a name="l00277"></a>00277 
<a name="l00278"></a>00278 
<a name="l00279"></a>00279   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00280"></a>00280             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00281"></a>00281             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2,
<a name="l00282"></a>00282             <span class="keyword">class </span>T3,
<a name="l00283"></a>00283             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00284"></a>00284   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00285"></a>00285               <span class="keyword">const</span> Matrix&lt;T1, Prop1, PETScMPIAIJ, Allocator1&gt;&amp; M,
<a name="l00286"></a>00286               <span class="keyword">const</span> Vector&lt;T2, VectFull, Allocator2&gt;&amp; X,
<a name="l00287"></a>00287               <span class="keyword">const</span> T3 beta, Vector&lt;T4, PETScPar, Allocator4&gt;&amp; Y)
<a name="l00288"></a>00288   {
<a name="l00289"></a>00289 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00290"></a>00290 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l00291"></a>00291 <span class="preprocessor">#endif</span>
<a name="l00292"></a>00292 <span class="preprocessor"></span>
<a name="l00293"></a>00293     Vector&lt;T4, PETScPar, Allocator4&gt; X_Petsc;
<a name="l00294"></a>00294     X_Petsc.SetCommunicator(M.GetCommunicator());
<a name="l00295"></a>00295     X_Petsc.Reallocate(X.GetM());
<a name="l00296"></a>00296     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetM(); i++)
<a name="l00297"></a>00297       X_Petsc.SetBuffer(i, X(i));
<a name="l00298"></a>00298     X_Petsc.Flush();
<a name="l00299"></a>00299 
<a name="l00300"></a>00300     <span class="keywordflow">if</span> (beta == T3(0))
<a name="l00301"></a>00301       <span class="keywordflow">if</span> (alpha == T0(0))
<a name="l00302"></a>00302         {
<a name="l00303"></a>00303           Y.Fill(T4(0));
<a name="l00304"></a>00304           <span class="keywordflow">return</span>;
<a name="l00305"></a>00305         }
<a name="l00306"></a>00306       <span class="keywordflow">else</span>
<a name="l00307"></a>00307         {
<a name="l00308"></a>00308           MatMult(M.GetPetscMatrix(), X_Petsc.GetPetscVector(),
<a name="l00309"></a>00309                   Y.GetPetscVector());
<a name="l00310"></a>00310           <span class="keywordflow">if</span> (alpha != T0(1))
<a name="l00311"></a>00311             VecScale(Y.GetPetscVector(), alpha);
<a name="l00312"></a>00312           <span class="keywordflow">return</span>;
<a name="l00313"></a>00313         }
<a name="l00314"></a>00314     <span class="keywordflow">if</span> (alpha == T0(1))
<a name="l00315"></a>00315       {
<a name="l00316"></a>00316         <span class="keywordflow">if</span> (beta != T3(1))
<a name="l00317"></a>00317           VecScale(Y.GetPetscVector(), beta);
<a name="l00318"></a>00318         MatMultAdd(M.GetPetscMatrix(), X_Petsc.GetPetscVector(),
<a name="l00319"></a>00319                    Y.GetPetscVector(),Y.GetPetscVector());
<a name="l00320"></a>00320         <span class="keywordflow">return</span>;
<a name="l00321"></a>00321       }
<a name="l00322"></a>00322     Vector&lt;T2, PETScPar, Allocator2&gt; tmp;
<a name="l00323"></a>00323     tmp.Copy(Y);
<a name="l00324"></a>00324     MatMult(M.GetPetscMatrix(), X_Petsc.GetPetscVector(),
<a name="l00325"></a>00325             tmp.GetPetscVector());
<a name="l00326"></a>00326     VecAXPBY(Y.GetPetscVector(), alpha, beta, tmp.GetPetscVector());
<a name="l00327"></a>00327     <span class="keywordflow">return</span>;
<a name="l00328"></a>00328   }
<a name="l00329"></a>00329 
<a name="l00330"></a>00330 
<a name="l00331"></a>00331   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00332"></a>00332             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00333"></a>00333             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2,
<a name="l00334"></a>00334             <span class="keyword">class </span>T3,
<a name="l00335"></a>00335             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00336"></a>00336   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00337"></a>00337               <span class="keyword">const</span> Matrix&lt;T1, Prop1, PETScMPIDense, Allocator1&gt;&amp; M,
<a name="l00338"></a>00338               <span class="keyword">const</span> Vector&lt;T2, VectFull, Allocator2&gt;&amp; X,
<a name="l00339"></a>00339               <span class="keyword">const</span> T3 beta, Vector&lt;T4, PETScPar, Allocator4&gt;&amp; Y)
<a name="l00340"></a>00340   {
<a name="l00341"></a>00341 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00342"></a>00342 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l00343"></a>00343 <span class="preprocessor">#endif</span>
<a name="l00344"></a>00344 <span class="preprocessor"></span>
<a name="l00345"></a>00345     Vector&lt;T4, PETScPar, Allocator4&gt; X_Petsc;
<a name="l00346"></a>00346      X_Petsc.SetCommunicator(M.GetCommunicator());
<a name="l00347"></a>00347     X_Petsc.Reallocate(X.GetM());
<a name="l00348"></a>00348     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetM(); i++)
<a name="l00349"></a>00349       X_Petsc.SetBuffer(i, X(i));
<a name="l00350"></a>00350     X_Petsc.Flush();
<a name="l00351"></a>00351 
<a name="l00352"></a>00352     <span class="keywordflow">if</span> (beta == T3(0))
<a name="l00353"></a>00353       <span class="keywordflow">if</span> (alpha == T0(0))
<a name="l00354"></a>00354         {
<a name="l00355"></a>00355           Y.Fill(T4(0));
<a name="l00356"></a>00356           <span class="keywordflow">return</span>;
<a name="l00357"></a>00357         }
<a name="l00358"></a>00358       <span class="keywordflow">else</span>
<a name="l00359"></a>00359         {
<a name="l00360"></a>00360           MatMult(M.GetPetscMatrix(), X_Petsc.GetPetscVector(),
<a name="l00361"></a>00361                   Y.GetPetscVector());
<a name="l00362"></a>00362           <span class="keywordflow">if</span> (alpha != T0(1))
<a name="l00363"></a>00363             VecScale(Y.GetPetscVector(), alpha);
<a name="l00364"></a>00364           <span class="keywordflow">return</span>;
<a name="l00365"></a>00365         }
<a name="l00366"></a>00366     <span class="keywordflow">if</span> (alpha == T0(1))
<a name="l00367"></a>00367       {
<a name="l00368"></a>00368         <span class="keywordflow">if</span> (beta != T3(1))
<a name="l00369"></a>00369           VecScale(Y.GetPetscVector(), beta);
<a name="l00370"></a>00370         MatMultAdd(M.GetPetscMatrix(), X_Petsc.GetPetscVector(),
<a name="l00371"></a>00371                    Y.GetPetscVector(),Y.GetPetscVector());
<a name="l00372"></a>00372         <span class="keywordflow">return</span>;
<a name="l00373"></a>00373       }
<a name="l00374"></a>00374     Vector&lt;T2, PETScPar, Allocator2&gt; tmp;
<a name="l00375"></a>00375     tmp.Copy(Y);
<a name="l00376"></a>00376     tmp.SetCommunicator(M.GetCommunicator());
<a name="l00377"></a>00377     MatMult(M.GetPetscMatrix(), X_Petsc.GetPetscVector(),
<a name="l00378"></a>00378             tmp.GetPetscVector());
<a name="l00379"></a>00379     VecAXPBY(Y.GetPetscVector(), alpha, beta, tmp.GetPetscVector());
<a name="l00380"></a>00380     <span class="keywordflow">return</span>;
<a name="l00381"></a>00381   }
<a name="l00382"></a>00382 
<a name="l00383"></a>00383 
<a name="l00384"></a>00384 
<a name="l00385"></a>00385   <span class="comment">/*** Sparse matrices ***/</span>
<a name="l00386"></a>00386 
<a name="l00387"></a>00387 
<a name="l00388"></a>00388   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00389"></a>00389             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00390"></a>00390             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00391"></a>00391             <span class="keyword">class </span>T3,
<a name="l00392"></a>00392             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00393"></a>00393   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00394"></a>00394               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;&amp; M,
<a name="l00395"></a>00395               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00396"></a>00396               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00397"></a>00397   {
<a name="l00398"></a>00398     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00399"></a>00399 
<a name="l00400"></a>00400 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00401"></a>00401 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l00402"></a>00402 <span class="preprocessor">#endif</span>
<a name="l00403"></a>00403 <span class="preprocessor"></span>
<a name="l00404"></a>00404     <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(beta, Y);
<a name="l00405"></a>00405 
<a name="l00406"></a>00406     T4 zero(0);
<a name="l00407"></a>00407     T4 temp;
<a name="l00408"></a>00408 
<a name="l00409"></a>00409     <span class="keywordtype">int</span>* ptr = M.GetPtr();
<a name="l00410"></a>00410     <span class="keywordtype">int</span>* ind = M.GetInd();
<a name="l00411"></a>00411     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;::pointer
<a name="l00412"></a>00412       data = M.GetData();
<a name="l00413"></a>00413 
<a name="l00414"></a>00414     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; ma; i++)
<a name="l00415"></a>00415       {
<a name="l00416"></a>00416         temp = zero;
<a name="l00417"></a>00417         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = ptr[i]; j &lt; ptr[i+1]; j++)
<a name="l00418"></a>00418           temp += data[j] * X(ind[j]);
<a name="l00419"></a>00419         Y(i) += alpha * temp;
<a name="l00420"></a>00420       }
<a name="l00421"></a>00421   }
<a name="l00422"></a>00422 
<a name="l00423"></a>00423 
<a name="l00424"></a>00424   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00425"></a>00425             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00426"></a>00426             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00427"></a>00427             <span class="keyword">class </span>T3,
<a name="l00428"></a>00428             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00429"></a>00429   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00430"></a>00430               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;&amp; M,
<a name="l00431"></a>00431               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00432"></a>00432               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Collection, Allocator4&gt;&amp; Y)
<a name="l00433"></a>00433   {
<a name="l00434"></a>00434     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00435"></a>00435 
<a name="l00436"></a>00436 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00437"></a>00437 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l00438"></a>00438 <span class="preprocessor">#endif</span>
<a name="l00439"></a>00439 <span class="preprocessor"></span>
<a name="l00440"></a>00440     <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(beta, Y);
<a name="l00441"></a>00441 
<a name="l00442"></a>00442     <span class="keyword">typename</span> T4::value_type zero(0);
<a name="l00443"></a>00443     <span class="keyword">typename</span> T4::value_type temp;
<a name="l00444"></a>00444 
<a name="l00445"></a>00445     <span class="keywordtype">int</span>* ptr = M.GetPtr();
<a name="l00446"></a>00446     <span class="keywordtype">int</span>* ind = M.GetInd();
<a name="l00447"></a>00447     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;::pointer
<a name="l00448"></a>00448       data = M.GetData();
<a name="l00449"></a>00449 
<a name="l00450"></a>00450     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; ma; i++)
<a name="l00451"></a>00451       {
<a name="l00452"></a>00452         temp = zero;
<a name="l00453"></a>00453         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = ptr[i]; j &lt; ptr[i+1]; j++)
<a name="l00454"></a>00454           temp += data[j] * X(ind[j]);
<a name="l00455"></a>00455         Y(i) += alpha * temp;
<a name="l00456"></a>00456       }
<a name="l00457"></a>00457   }
<a name="l00458"></a>00458 
<a name="l00459"></a>00459 
<a name="l00460"></a>00460   <span class="comment">/*** Complex sparse matrices ***/</span>
<a name="l00461"></a>00461 
<a name="l00462"></a>00462 
<a name="l00463"></a>00463   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00464"></a>00464             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00465"></a>00465             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00466"></a>00466             <span class="keyword">class </span>T3,
<a name="l00467"></a>00467             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00468"></a>00468   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00469"></a>00469               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;&amp; M,
<a name="l00470"></a>00470               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00471"></a>00471               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00472"></a>00472   {
<a name="l00473"></a>00473     <span class="keywordtype">int</span> i, j;
<a name="l00474"></a>00474 
<a name="l00475"></a>00475     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00476"></a>00476 
<a name="l00477"></a>00477 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00478"></a>00478 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l00479"></a>00479 <span class="preprocessor">#endif</span>
<a name="l00480"></a>00480 <span class="preprocessor"></span>
<a name="l00481"></a>00481     <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(beta, Y);
<a name="l00482"></a>00482 
<a name="l00483"></a>00483     complex&lt;T1&gt; zero(0);
<a name="l00484"></a>00484     complex&lt;T1&gt; temp;
<a name="l00485"></a>00485 
<a name="l00486"></a>00486     <span class="keywordtype">int</span>* real_ptr = M.GetRealPtr();
<a name="l00487"></a>00487     <span class="keywordtype">int</span>* imag_ptr = M.GetImagPtr();
<a name="l00488"></a>00488     <span class="keywordtype">int</span>* real_ind = M.GetRealInd();
<a name="l00489"></a>00489     <span class="keywordtype">int</span>* imag_ind = M.GetImagInd();
<a name="l00490"></a>00490     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;::pointer
<a name="l00491"></a>00491       real_data = M.GetRealData();
<a name="l00492"></a>00492     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;::pointer
<a name="l00493"></a>00493       imag_data = M.GetImagData();
<a name="l00494"></a>00494 
<a name="l00495"></a>00495     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00496"></a>00496       {
<a name="l00497"></a>00497         temp = zero;
<a name="l00498"></a>00498         <span class="keywordflow">for</span> (j = real_ptr[i]; j &lt; real_ptr[i + 1]; j++)
<a name="l00499"></a>00499           temp += real_data[j] * X(real_ind[j]);
<a name="l00500"></a>00500         Y(i) += alpha * temp;
<a name="l00501"></a>00501       }
<a name="l00502"></a>00502 
<a name="l00503"></a>00503     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00504"></a>00504       {
<a name="l00505"></a>00505         temp = zero;
<a name="l00506"></a>00506         <span class="keywordflow">for</span> (j = imag_ptr[i]; j &lt; imag_ptr[i + 1]; j++)
<a name="l00507"></a>00507           temp += complex&lt;T1&gt;(T1(0), imag_data[j]) * X(imag_ind[j]);
<a name="l00508"></a>00508         Y(i) += alpha * temp;
<a name="l00509"></a>00509       }
<a name="l00510"></a>00510   }
<a name="l00511"></a>00511 
<a name="l00512"></a>00512 
<a name="l00513"></a>00513   <span class="comment">/*** Symmetric sparse matrices ***/</span>
<a name="l00514"></a>00514 
<a name="l00515"></a>00515 
<a name="l00516"></a>00516   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00517"></a>00517             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00518"></a>00518             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00519"></a>00519             <span class="keyword">class </span>T3,
<a name="l00520"></a>00520             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00521"></a>00521   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00522"></a>00522               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymSparse, Allocator1&gt;&amp; M,
<a name="l00523"></a>00523               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00524"></a>00524               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00525"></a>00525   {
<a name="l00526"></a>00526     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00527"></a>00527 
<a name="l00528"></a>00528 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00529"></a>00529 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l00530"></a>00530 <span class="preprocessor">#endif</span>
<a name="l00531"></a>00531 <span class="preprocessor"></span>
<a name="l00532"></a>00532     <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(beta, Y);
<a name="l00533"></a>00533 
<a name="l00534"></a>00534     <span class="keywordtype">int</span> i, j;
<a name="l00535"></a>00535     T4 zero(0);
<a name="l00536"></a>00536     T4 temp;
<a name="l00537"></a>00537 
<a name="l00538"></a>00538     <span class="keywordtype">int</span>* ptr = M.GetPtr();
<a name="l00539"></a>00539     <span class="keywordtype">int</span>* ind = M.GetInd();
<a name="l00540"></a>00540     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowSymSparse, Allocator1&gt;::pointer
<a name="l00541"></a>00541       data = M.GetData();
<a name="l00542"></a>00542 
<a name="l00543"></a>00543     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00544"></a>00544       {
<a name="l00545"></a>00545         temp = zero;
<a name="l00546"></a>00546         <span class="keywordflow">for</span> (j = ptr[i]; j &lt; ptr[i + 1]; j++)
<a name="l00547"></a>00547           temp += data[j] * X(ind[j]);
<a name="l00548"></a>00548         Y(i) += alpha * temp;
<a name="l00549"></a>00549       }
<a name="l00550"></a>00550     <span class="keywordflow">for</span> (i = 0; i &lt; ma-1; i++)
<a name="l00551"></a>00551       <span class="keywordflow">for</span> (j = ptr[i]; j &lt; ptr[i + 1]; j++)
<a name="l00552"></a>00552         <span class="keywordflow">if</span> (ind[j] != i)
<a name="l00553"></a>00553           Y(ind[j]) += alpha * data[j] * X(i);
<a name="l00554"></a>00554   }
<a name="l00555"></a>00555 
<a name="l00556"></a>00556 
<a name="l00557"></a>00557   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00558"></a>00558             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00559"></a>00559             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2,
<a name="l00560"></a>00560             <span class="keyword">class </span>T3,
<a name="l00561"></a>00561             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00562"></a>00562   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00563"></a>00563               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymSparse, Allocator1&gt;&amp; M,
<a name="l00564"></a>00564               <span class="keyword">const</span> Vector&lt;T2, Collection, Allocator2&gt;&amp; X,
<a name="l00565"></a>00565               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Collection, Allocator4&gt;&amp; Y)
<a name="l00566"></a>00566   {
<a name="l00567"></a>00567     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00568"></a>00568 
<a name="l00569"></a>00569 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00570"></a>00570 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l00571"></a>00571 <span class="preprocessor">#endif</span>
<a name="l00572"></a>00572 <span class="preprocessor"></span>
<a name="l00573"></a>00573     <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(beta, Y);
<a name="l00574"></a>00574 
<a name="l00575"></a>00575     <span class="keywordtype">int</span> i, j;
<a name="l00576"></a>00576     <span class="keyword">typename</span> T4::value_type zero(0);
<a name="l00577"></a>00577     <span class="keyword">typename</span> T4::value_type temp;
<a name="l00578"></a>00578 
<a name="l00579"></a>00579     <span class="keywordtype">int</span>* ptr = M.GetPtr();
<a name="l00580"></a>00580     <span class="keywordtype">int</span>* ind = M.GetInd();
<a name="l00581"></a>00581     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowSymSparse, Allocator1&gt;::pointer
<a name="l00582"></a>00582       data = M.GetData();
<a name="l00583"></a>00583 
<a name="l00584"></a>00584     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00585"></a>00585       {
<a name="l00586"></a>00586         temp = zero;
<a name="l00587"></a>00587         <span class="keywordflow">for</span> (j = ptr[i]; j &lt; ptr[i + 1]; j++)
<a name="l00588"></a>00588           temp += data[j] * X(ind[j]);
<a name="l00589"></a>00589         Y(i) += alpha * temp;
<a name="l00590"></a>00590       }
<a name="l00591"></a>00591     <span class="keywordflow">for</span> (i = 0; i &lt; ma-1; i++)
<a name="l00592"></a>00592       <span class="keywordflow">for</span> (j = ptr[i]; j &lt; ptr[i + 1]; j++)
<a name="l00593"></a>00593         <span class="keywordflow">if</span> (ind[j] != i)
<a name="l00594"></a>00594           Y(ind[j]) += alpha * data[j] * X(i);
<a name="l00595"></a>00595   }
<a name="l00596"></a>00596 
<a name="l00597"></a>00597 
<a name="l00598"></a>00598   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00599"></a>00599             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00600"></a>00600             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00601"></a>00601             <span class="keyword">class </span>T3,
<a name="l00602"></a>00602             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00603"></a>00603   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00604"></a>00604               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymSparse, Allocator1&gt;&amp; M,
<a name="l00605"></a>00605               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00606"></a>00606               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Collection, Allocator4&gt;&amp; Y)
<a name="l00607"></a>00607   {
<a name="l00608"></a>00608     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00609"></a>00609 
<a name="l00610"></a>00610 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00611"></a>00611 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l00612"></a>00612 <span class="preprocessor">#endif</span>
<a name="l00613"></a>00613 <span class="preprocessor"></span>
<a name="l00614"></a>00614     <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(beta, Y);
<a name="l00615"></a>00615 
<a name="l00616"></a>00616     <span class="keywordtype">int</span> i, j;
<a name="l00617"></a>00617     <span class="keyword">typename</span> T4::value_type zero(0);
<a name="l00618"></a>00618     <span class="keyword">typename</span> T4::value_type temp;
<a name="l00619"></a>00619 
<a name="l00620"></a>00620     <span class="keywordtype">int</span>* ptr = M.GetPtr();
<a name="l00621"></a>00621     <span class="keywordtype">int</span>* ind = M.GetInd();
<a name="l00622"></a>00622     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowSymSparse, Allocator1&gt;::pointer
<a name="l00623"></a>00623       data = M.GetData();
<a name="l00624"></a>00624 
<a name="l00625"></a>00625     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00626"></a>00626       {
<a name="l00627"></a>00627         temp = zero;
<a name="l00628"></a>00628         <span class="keywordflow">for</span> (j = ptr[i]; j &lt; ptr[i + 1]; j++)
<a name="l00629"></a>00629           temp += data[j] * X(ind[j]);
<a name="l00630"></a>00630         Y(i) += alpha * temp;
<a name="l00631"></a>00631       }
<a name="l00632"></a>00632     <span class="keywordflow">for</span> (i = 0; i &lt; ma-1; i++)
<a name="l00633"></a>00633       <span class="keywordflow">for</span> (j = ptr[i]; j &lt; ptr[i + 1]; j++)
<a name="l00634"></a>00634         <span class="keywordflow">if</span> (ind[j] != i)
<a name="l00635"></a>00635           Y(ind[j]) += alpha * data[j] * X(i);
<a name="l00636"></a>00636   }
<a name="l00637"></a>00637 
<a name="l00638"></a>00638 
<a name="l00639"></a>00639   <span class="comment">/*** Symmetric complex sparse matrices ***/</span>
<a name="l00640"></a>00640 
<a name="l00641"></a>00641 
<a name="l00642"></a>00642   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00643"></a>00643             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00644"></a>00644             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00645"></a>00645             <span class="keyword">class </span>T3,
<a name="l00646"></a>00646             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00647"></a>00647   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00648"></a>00648               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymComplexSparse, Allocator1&gt;&amp; M,
<a name="l00649"></a>00649               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00650"></a>00650               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00651"></a>00651   {
<a name="l00652"></a>00652     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00653"></a>00653 
<a name="l00654"></a>00654 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00655"></a>00655 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l00656"></a>00656 <span class="preprocessor">#endif</span>
<a name="l00657"></a>00657 <span class="preprocessor"></span>
<a name="l00658"></a>00658     <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(beta, Y);
<a name="l00659"></a>00659 
<a name="l00660"></a>00660     <span class="keywordtype">int</span> i, j;
<a name="l00661"></a>00661     complex&lt;T1&gt; zero(0);
<a name="l00662"></a>00662     complex&lt;T1&gt; temp;
<a name="l00663"></a>00663 
<a name="l00664"></a>00664     <span class="keywordtype">int</span>* real_ptr = M.GetRealPtr();
<a name="l00665"></a>00665     <span class="keywordtype">int</span>* imag_ptr = M.GetImagPtr();
<a name="l00666"></a>00666     <span class="keywordtype">int</span>* real_ind = M.GetRealInd();
<a name="l00667"></a>00667     <span class="keywordtype">int</span>* imag_ind = M.GetImagInd();
<a name="l00668"></a>00668     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowSymComplexSparse, Allocator1&gt;::pointer
<a name="l00669"></a>00669       real_data = M.GetRealData();
<a name="l00670"></a>00670     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowSymComplexSparse, Allocator1&gt;::pointer
<a name="l00671"></a>00671       imag_data = M.GetImagData();
<a name="l00672"></a>00672 
<a name="l00673"></a>00673     <span class="keywordflow">for</span> (i = 0; i&lt;ma; i++)
<a name="l00674"></a>00674       {
<a name="l00675"></a>00675         temp = zero;
<a name="l00676"></a>00676         <span class="keywordflow">for</span> (j = real_ptr[i]; j &lt; real_ptr[i + 1]; j++)
<a name="l00677"></a>00677           temp += real_data[j] * X(real_ind[j]);
<a name="l00678"></a>00678         Y(i) += alpha * temp;
<a name="l00679"></a>00679       }
<a name="l00680"></a>00680     <span class="keywordflow">for</span> (i = 0; i&lt;ma-1; i++)
<a name="l00681"></a>00681       <span class="keywordflow">for</span> (j = real_ptr[i]; j &lt; real_ptr[i + 1]; j++)
<a name="l00682"></a>00682         <span class="keywordflow">if</span> (real_ind[j] != i)
<a name="l00683"></a>00683           Y(real_ind[j]) += alpha * real_data[j] * X(i);
<a name="l00684"></a>00684 
<a name="l00685"></a>00685     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00686"></a>00686       {
<a name="l00687"></a>00687         temp = zero;
<a name="l00688"></a>00688         <span class="keywordflow">for</span> (j = imag_ptr[i]; j &lt; imag_ptr[i + 1]; j++)
<a name="l00689"></a>00689           temp += complex&lt;T1&gt;(T1(0), imag_data[j]) * X(imag_ind[j]);
<a name="l00690"></a>00690         Y(i) += alpha * temp;
<a name="l00691"></a>00691       }
<a name="l00692"></a>00692     <span class="keywordflow">for</span> (i = 0; i&lt;ma-1; i++)
<a name="l00693"></a>00693       <span class="keywordflow">for</span> (j = imag_ptr[i]; j &lt; imag_ptr[i + 1]; j++)
<a name="l00694"></a>00694         <span class="keywordflow">if</span> (imag_ind[j] != i)
<a name="l00695"></a>00695           Y(imag_ind[j]) += alpha * complex&lt;T1&gt;(T1(0), imag_data[j]) * X(i);
<a name="l00696"></a>00696   }
<a name="l00697"></a>00697 
<a name="l00698"></a>00698 
<a name="l00699"></a>00699   <span class="comment">/*** Sparse matrices, *Trans ***/</span>
<a name="l00700"></a>00700 
<a name="l00701"></a>00701 
<a name="l00702"></a>00702   <span class="comment">// NoTrans.</span>
<a name="l00703"></a>00703   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00704"></a>00704             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00705"></a>00705             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00706"></a>00706             <span class="keyword">class </span>T3,
<a name="l00707"></a>00707             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00708"></a>00708   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00709"></a>00709               <span class="keyword">const</span> class_SeldonNoTrans&amp; Trans,
<a name="l00710"></a>00710               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;&amp; M,
<a name="l00711"></a>00711               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00712"></a>00712               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00713"></a>00713   {
<a name="l00714"></a>00714     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, M, X, beta, Y);
<a name="l00715"></a>00715   }
<a name="l00716"></a>00716 
<a name="l00717"></a>00717 
<a name="l00718"></a>00718   <span class="comment">// Trans.</span>
<a name="l00719"></a>00719   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00720"></a>00720             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00721"></a>00721             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00722"></a>00722             <span class="keyword">class </span>T3,
<a name="l00723"></a>00723             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00724"></a>00724   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00725"></a>00725               <span class="keyword">const</span> class_SeldonTrans&amp; Trans,
<a name="l00726"></a>00726               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;&amp; M,
<a name="l00727"></a>00727               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00728"></a>00728               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00729"></a>00729   {
<a name="l00730"></a>00730     <span class="keywordtype">int</span> i, j;
<a name="l00731"></a>00731 
<a name="l00732"></a>00732     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00733"></a>00733 
<a name="l00734"></a>00734 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00735"></a>00735 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(Trans, M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, SeldonTrans, M, X, beta, Y)&quot;</span>);
<a name="l00736"></a>00736 <span class="preprocessor">#endif</span>
<a name="l00737"></a>00737 <span class="preprocessor"></span>
<a name="l00738"></a>00738     <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(beta, Y);
<a name="l00739"></a>00739 
<a name="l00740"></a>00740     <span class="keywordtype">int</span>* ptr = M.GetPtr();
<a name="l00741"></a>00741     <span class="keywordtype">int</span>* ind = M.GetInd();
<a name="l00742"></a>00742     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;::pointer
<a name="l00743"></a>00743       data = M.GetData();
<a name="l00744"></a>00744 
<a name="l00745"></a>00745     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00746"></a>00746       <span class="keywordflow">for</span> (j = ptr[i]; j &lt; ptr[i + 1]; j++)
<a name="l00747"></a>00747         Y(ind[j]) += alpha * data[j] * X(i);
<a name="l00748"></a>00748   }
<a name="l00749"></a>00749 
<a name="l00750"></a>00750 
<a name="l00751"></a>00751   <span class="comment">// ConjTrans.</span>
<a name="l00752"></a>00752   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00753"></a>00753             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00754"></a>00754             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00755"></a>00755             <span class="keyword">class </span>T3,
<a name="l00756"></a>00756             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00757"></a>00757   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00758"></a>00758               <span class="keyword">const</span> class_SeldonConjTrans&amp; Trans,
<a name="l00759"></a>00759               <span class="keyword">const</span> Matrix&lt;complex&lt;T1&gt;, Prop1, RowSparse, Allocator1&gt;&amp; M,
<a name="l00760"></a>00760               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00761"></a>00761               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00762"></a>00762   {
<a name="l00763"></a>00763     <span class="keywordtype">int</span> i, j;
<a name="l00764"></a>00764 
<a name="l00765"></a>00765     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00766"></a>00766 
<a name="l00767"></a>00767 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00768"></a>00768 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(Trans, M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, SeldonConjTrans, M, X, beta, Y)&quot;</span>);
<a name="l00769"></a>00769 <span class="preprocessor">#endif</span>
<a name="l00770"></a>00770 <span class="preprocessor"></span>
<a name="l00771"></a>00771     <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(beta, Y);
<a name="l00772"></a>00772 
<a name="l00773"></a>00773     <span class="keywordtype">int</span>* ptr = M.GetPtr();
<a name="l00774"></a>00774     <span class="keywordtype">int</span>* ind = M.GetInd();
<a name="l00775"></a>00775     <span class="keyword">typename</span> Matrix&lt;complex&lt;T1&gt;, Prop1, RowSparse, Allocator1&gt;::pointer
<a name="l00776"></a>00776       data = M.GetData();
<a name="l00777"></a>00777 
<a name="l00778"></a>00778     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00779"></a>00779       <span class="keywordflow">for</span> (j = ptr[i]; j &lt; ptr[i + 1]; j++)
<a name="l00780"></a>00780         Y(ind[j]) += alpha * conj(data[j]) * X(i);
<a name="l00781"></a>00781   }
<a name="l00782"></a>00782 
<a name="l00783"></a>00783 
<a name="l00784"></a>00784   <span class="comment">/*** Complex sparse matrices, *Trans ***/</span>
<a name="l00785"></a>00785 
<a name="l00786"></a>00786 
<a name="l00787"></a>00787   <span class="comment">// NoTrans.</span>
<a name="l00788"></a>00788   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00789"></a>00789             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00790"></a>00790             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00791"></a>00791             <span class="keyword">class </span>T3,
<a name="l00792"></a>00792             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00793"></a>00793   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00794"></a>00794               <span class="keyword">const</span> class_SeldonNoTrans&amp; Trans,
<a name="l00795"></a>00795               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;&amp; M,
<a name="l00796"></a>00796               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00797"></a>00797               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00798"></a>00798   {
<a name="l00799"></a>00799     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, M, X, beta, Y);
<a name="l00800"></a>00800   }
<a name="l00801"></a>00801 
<a name="l00802"></a>00802 
<a name="l00803"></a>00803   <span class="comment">// Trans.</span>
<a name="l00804"></a>00804   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00805"></a>00805             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00806"></a>00806             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00807"></a>00807             <span class="keyword">class </span>T3,
<a name="l00808"></a>00808             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00809"></a>00809   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00810"></a>00810               <span class="keyword">const</span> class_SeldonTrans&amp; Trans,
<a name="l00811"></a>00811               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;&amp; M,
<a name="l00812"></a>00812               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00813"></a>00813               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00814"></a>00814   {
<a name="l00815"></a>00815     <span class="keywordtype">int</span> i, j;
<a name="l00816"></a>00816 
<a name="l00817"></a>00817     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00818"></a>00818 
<a name="l00819"></a>00819 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00820"></a>00820 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(Trans, M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, SeldonTrans, M, X, beta, Y)&quot;</span>);
<a name="l00821"></a>00821 <span class="preprocessor">#endif</span>
<a name="l00822"></a>00822 <span class="preprocessor"></span>
<a name="l00823"></a>00823     <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(beta, Y);
<a name="l00824"></a>00824 
<a name="l00825"></a>00825     <span class="keywordtype">int</span>* real_ptr = M.GetRealPtr();
<a name="l00826"></a>00826     <span class="keywordtype">int</span>* imag_ptr = M.GetImagPtr();
<a name="l00827"></a>00827     <span class="keywordtype">int</span>* real_ind = M.GetRealInd();
<a name="l00828"></a>00828     <span class="keywordtype">int</span>* imag_ind = M.GetImagInd();
<a name="l00829"></a>00829     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;::pointer
<a name="l00830"></a>00830       real_data = M.GetRealData();
<a name="l00831"></a>00831     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;::pointer
<a name="l00832"></a>00832       imag_data = M.GetImagData();
<a name="l00833"></a>00833 
<a name="l00834"></a>00834     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00835"></a>00835       <span class="keywordflow">for</span> (j = real_ptr[i]; j &lt; real_ptr[i + 1]; j++)
<a name="l00836"></a>00836         Y(real_ind[j]) += alpha * real_data[j] * X(i);
<a name="l00837"></a>00837 
<a name="l00838"></a>00838     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00839"></a>00839       <span class="keywordflow">for</span> (j = imag_ptr[i]; j &lt; imag_ptr[i + 1]; j++)
<a name="l00840"></a>00840         Y(imag_ind[j]) += alpha * complex&lt;T1&gt;(T1(0), imag_data[j]) * X(i);
<a name="l00841"></a>00841   }
<a name="l00842"></a>00842 
<a name="l00843"></a>00843 
<a name="l00844"></a>00844   <span class="comment">// ConjTrans.</span>
<a name="l00845"></a>00845   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00846"></a>00846             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00847"></a>00847             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00848"></a>00848             <span class="keyword">class </span>T3,
<a name="l00849"></a>00849             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00850"></a>00850   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00851"></a>00851               <span class="keyword">const</span> class_SeldonConjTrans&amp; Trans,
<a name="l00852"></a>00852               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;&amp; M,
<a name="l00853"></a>00853               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00854"></a>00854               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00855"></a>00855   {
<a name="l00856"></a>00856     <span class="keywordtype">int</span> i, j;
<a name="l00857"></a>00857 
<a name="l00858"></a>00858     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00859"></a>00859 
<a name="l00860"></a>00860 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00861"></a>00861 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(Trans, M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, SeldonConjTrans, M, X, beta, Y)&quot;</span>);
<a name="l00862"></a>00862 <span class="preprocessor">#endif</span>
<a name="l00863"></a>00863 <span class="preprocessor"></span>
<a name="l00864"></a>00864     <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(beta, Y);
<a name="l00865"></a>00865 
<a name="l00866"></a>00866     <span class="keywordtype">int</span>* real_ptr = M.GetRealPtr();
<a name="l00867"></a>00867     <span class="keywordtype">int</span>* imag_ptr = M.GetImagPtr();
<a name="l00868"></a>00868     <span class="keywordtype">int</span>* real_ind = M.GetRealInd();
<a name="l00869"></a>00869     <span class="keywordtype">int</span>* imag_ind = M.GetImagInd();
<a name="l00870"></a>00870     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;::pointer
<a name="l00871"></a>00871       real_data = M.GetRealData();
<a name="l00872"></a>00872     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;::pointer
<a name="l00873"></a>00873       imag_data = M.GetImagData();
<a name="l00874"></a>00874 
<a name="l00875"></a>00875     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00876"></a>00876       <span class="keywordflow">for</span> (j = real_ptr[i]; j &lt; real_ptr[i + 1]; j++)
<a name="l00877"></a>00877         Y(real_ind[j]) += alpha * real_data[j] * X(i);
<a name="l00878"></a>00878 
<a name="l00879"></a>00879     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00880"></a>00880       <span class="keywordflow">for</span> (j = imag_ptr[i]; j &lt; imag_ptr[i + 1]; j++)
<a name="l00881"></a>00881         Y(imag_ind[j]) += alpha * complex&lt;T1&gt;(T1(0), - imag_data[j]) * X(i);
<a name="l00882"></a>00882   }
<a name="l00883"></a>00883 
<a name="l00884"></a>00884 
<a name="l00885"></a>00885   <span class="comment">/*** Symmetric sparse matrices, *Trans ***/</span>
<a name="l00886"></a>00886 
<a name="l00887"></a>00887 
<a name="l00888"></a>00888   <span class="comment">// NoTrans.</span>
<a name="l00889"></a>00889   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00890"></a>00890             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00891"></a>00891             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00892"></a>00892             <span class="keyword">class </span>T3,
<a name="l00893"></a>00893             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00894"></a>00894   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00895"></a>00895               <span class="keyword">const</span> class_SeldonNoTrans&amp; Trans,
<a name="l00896"></a>00896               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymSparse, Allocator1&gt;&amp; M,
<a name="l00897"></a>00897               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00898"></a>00898               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00899"></a>00899   {
<a name="l00900"></a>00900     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, M, X, beta, Y);
<a name="l00901"></a>00901   }
<a name="l00902"></a>00902 
<a name="l00903"></a>00903 
<a name="l00904"></a>00904   <span class="comment">// Trans.</span>
<a name="l00905"></a>00905   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00906"></a>00906             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00907"></a>00907             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00908"></a>00908             <span class="keyword">class </span>T3,
<a name="l00909"></a>00909             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00910"></a>00910   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00911"></a>00911               <span class="keyword">const</span> class_SeldonTrans&amp; Trans,
<a name="l00912"></a>00912               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymSparse, Allocator1&gt;&amp; M,
<a name="l00913"></a>00913               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00914"></a>00914               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00915"></a>00915   {
<a name="l00916"></a>00916     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, M, X, beta, Y);
<a name="l00917"></a>00917   }
<a name="l00918"></a>00918 
<a name="l00919"></a>00919 
<a name="l00920"></a>00920   <span class="comment">// ConjTrans.</span>
<a name="l00921"></a>00921   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00922"></a>00922             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00923"></a>00923             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00924"></a>00924             <span class="keyword">class </span>T3,
<a name="l00925"></a>00925             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00926"></a>00926   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00927"></a>00927               <span class="keyword">const</span> class_SeldonConjTrans&amp; Trans,
<a name="l00928"></a>00928               <span class="keyword">const</span> Matrix&lt;complex&lt;T1&gt;, Prop1, RowSymSparse, Allocator1&gt;&amp; M,
<a name="l00929"></a>00929               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00930"></a>00930               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00931"></a>00931   {
<a name="l00932"></a>00932     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00933"></a>00933 
<a name="l00934"></a>00934 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00935"></a>00935 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(Trans, M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, SeldonConjTrans, M, X, beta, Y)&quot;</span>);
<a name="l00936"></a>00936 <span class="preprocessor">#endif</span>
<a name="l00937"></a>00937 <span class="preprocessor"></span>
<a name="l00938"></a>00938     <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(beta, Y);
<a name="l00939"></a>00939 
<a name="l00940"></a>00940     <span class="keywordtype">int</span> i, j;
<a name="l00941"></a>00941     complex&lt;T1&gt; zero(0);
<a name="l00942"></a>00942     complex&lt;T1&gt; temp;
<a name="l00943"></a>00943 
<a name="l00944"></a>00944     <span class="keywordtype">int</span>* ptr = M.GetPtr();
<a name="l00945"></a>00945     <span class="keywordtype">int</span>* ind = M.GetInd();
<a name="l00946"></a>00946     <span class="keyword">typename</span> Matrix&lt;complex&lt;T1&gt;, Prop1, RowSymSparse, Allocator1&gt;::pointer
<a name="l00947"></a>00947       data = M.GetData();
<a name="l00948"></a>00948 
<a name="l00949"></a>00949     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00950"></a>00950       {
<a name="l00951"></a>00951         temp = zero;
<a name="l00952"></a>00952         <span class="keywordflow">for</span> (j = ptr[i]; j &lt; ptr[i + 1]; j++)
<a name="l00953"></a>00953           temp += conj(data[j]) * X(ind[j]);
<a name="l00954"></a>00954         Y(i) += temp;
<a name="l00955"></a>00955       }
<a name="l00956"></a>00956     <span class="keywordflow">for</span> (i = 0; i &lt; ma - 1; i++)
<a name="l00957"></a>00957       <span class="keywordflow">for</span> (j = ptr[i]; j &lt; ptr[i + 1]; j++)
<a name="l00958"></a>00958         <span class="keywordflow">if</span> (ind[j] != i)
<a name="l00959"></a>00959           Y(ind[j]) += conj(data[j]) * X(i);
<a name="l00960"></a>00960   }
<a name="l00961"></a>00961 
<a name="l00962"></a>00962 
<a name="l00963"></a>00963   <span class="comment">/*** Symmetric complex sparse matrices, *Trans ***/</span>
<a name="l00964"></a>00964 
<a name="l00965"></a>00965 
<a name="l00966"></a>00966   <span class="comment">// NoTrans.</span>
<a name="l00967"></a>00967   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00968"></a>00968             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00969"></a>00969             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00970"></a>00970             <span class="keyword">class </span>T3,
<a name="l00971"></a>00971             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00972"></a>00972   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00973"></a>00973               <span class="keyword">const</span> class_SeldonNoTrans&amp; Trans,
<a name="l00974"></a>00974               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymComplexSparse, Allocator1&gt;&amp; M,
<a name="l00975"></a>00975               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00976"></a>00976               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00977"></a>00977   {
<a name="l00978"></a>00978     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, M, X, beta, Y);
<a name="l00979"></a>00979   }
<a name="l00980"></a>00980 
<a name="l00981"></a>00981 
<a name="l00982"></a>00982   <span class="comment">// Trans.</span>
<a name="l00983"></a>00983   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00984"></a>00984             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00985"></a>00985             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00986"></a>00986             <span class="keyword">class </span>T3,
<a name="l00987"></a>00987             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00988"></a>00988   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00989"></a>00989               <span class="keyword">const</span> class_SeldonTrans&amp; Trans,
<a name="l00990"></a>00990               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymComplexSparse, Allocator1&gt;&amp; M,
<a name="l00991"></a>00991               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00992"></a>00992               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00993"></a>00993   {
<a name="l00994"></a>00994     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, M, X, beta, Y);
<a name="l00995"></a>00995   }
<a name="l00996"></a>00996 
<a name="l00997"></a>00997 
<a name="l00998"></a>00998   <span class="comment">// ConjTrans.</span>
<a name="l00999"></a>00999   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l01000"></a>01000             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l01001"></a>01001             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l01002"></a>01002             <span class="keyword">class </span>T3,
<a name="l01003"></a>01003             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l01004"></a>01004   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l01005"></a>01005               <span class="keyword">const</span> class_SeldonConjTrans&amp; Trans,
<a name="l01006"></a>01006               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymComplexSparse, Allocator1&gt;&amp; M,
<a name="l01007"></a>01007               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l01008"></a>01008               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l01009"></a>01009   {
<a name="l01010"></a>01010     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l01011"></a>01011 
<a name="l01012"></a>01012 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l01013"></a>01013 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(Trans, M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, SeldonConjTrans, M, X, beta, Y)&quot;</span>);
<a name="l01014"></a>01014 <span class="preprocessor">#endif</span>
<a name="l01015"></a>01015 <span class="preprocessor"></span>
<a name="l01016"></a>01016     <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(beta, Y);
<a name="l01017"></a>01017 
<a name="l01018"></a>01018     <span class="keywordtype">int</span> i, j;
<a name="l01019"></a>01019     complex&lt;T1&gt; zero(0);
<a name="l01020"></a>01020     complex&lt;T1&gt; temp;
<a name="l01021"></a>01021 
<a name="l01022"></a>01022     <span class="keywordtype">int</span>* real_ptr = M.GetRealPtr();
<a name="l01023"></a>01023     <span class="keywordtype">int</span>* imag_ptr = M.GetImagPtr();
<a name="l01024"></a>01024     <span class="keywordtype">int</span>* real_ind = M.GetRealInd();
<a name="l01025"></a>01025     <span class="keywordtype">int</span>* imag_ind = M.GetImagInd();
<a name="l01026"></a>01026     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowSymComplexSparse, Allocator1&gt;::pointer
<a name="l01027"></a>01027       real_data = M.GetRealData();
<a name="l01028"></a>01028     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowSymComplexSparse, Allocator1&gt;::pointer
<a name="l01029"></a>01029       imag_data = M.GetImagData();
<a name="l01030"></a>01030 
<a name="l01031"></a>01031     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l01032"></a>01032       {
<a name="l01033"></a>01033         temp = zero;
<a name="l01034"></a>01034         <span class="keywordflow">for</span> (j = real_ptr[i]; j &lt; real_ptr[i + 1]; j++)
<a name="l01035"></a>01035           temp += real_data[j] * X(real_ind[j]);
<a name="l01036"></a>01036         Y(i) += temp;
<a name="l01037"></a>01037       }
<a name="l01038"></a>01038     <span class="keywordflow">for</span> (i = 0; i &lt; ma - 1; i++)
<a name="l01039"></a>01039       <span class="keywordflow">for</span> (j = real_ptr[i]; j &lt; real_ptr[i + 1]; j++)
<a name="l01040"></a>01040         <span class="keywordflow">if</span> (real_ind[j] != i)
<a name="l01041"></a>01041           Y(real_ind[j]) += real_data[j] * X(i);
<a name="l01042"></a>01042 
<a name="l01043"></a>01043     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l01044"></a>01044       {
<a name="l01045"></a>01045         temp = zero;
<a name="l01046"></a>01046         <span class="keywordflow">for</span> (j = imag_ptr[i]; j &lt; imag_ptr[i + 1]; j++)
<a name="l01047"></a>01047           temp += complex&lt;T1&gt;(T1(0), - imag_data[j]) * X(imag_ind[j]);
<a name="l01048"></a>01048         Y(i) += temp;
<a name="l01049"></a>01049       }
<a name="l01050"></a>01050     <span class="keywordflow">for</span> (i = 0; i &lt; ma - 1; i++)
<a name="l01051"></a>01051       <span class="keywordflow">for</span> (j = imag_ptr[i]; j &lt; imag_ptr[i + 1]; j++)
<a name="l01052"></a>01052         <span class="keywordflow">if</span> (imag_ind[j] != i)
<a name="l01053"></a>01053           Y(imag_ind[j]) += complex&lt;T1&gt;(T1(0), - imag_data[j]) * X(i);
<a name="l01054"></a>01054   }
<a name="l01055"></a>01055 
<a name="l01056"></a>01056 
<a name="l01057"></a>01057   <span class="comment">// MLTADD //</span>
<a name="l01059"></a>01059 <span class="comment"></span>
<a name="l01060"></a>01060 
<a name="l01062"></a>01062   <span class="comment">// MLTADD //</span>
<a name="l01063"></a>01063 
<a name="l01064"></a>01064 
<a name="l01079"></a>01079   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l01080"></a>01080             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01081"></a>01081             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l01082"></a>01082             <span class="keyword">class </span>T3,
<a name="l01083"></a>01083             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l01084"></a>01084   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l01085"></a>01085               <span class="keyword">const</span> Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;&amp; M,
<a name="l01086"></a>01086               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l01087"></a>01087               <span class="keyword">const</span> T3 beta,
<a name="l01088"></a>01088               Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l01089"></a>01089   {
<a name="l01090"></a>01090     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l01091"></a>01091     <span class="keywordtype">int</span> na = M.GetN();
<a name="l01092"></a>01092 
<a name="l01093"></a>01093 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l01094"></a>01094 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l01095"></a>01095 <span class="preprocessor">#endif</span>
<a name="l01096"></a>01096 <span class="preprocessor"></span>
<a name="l01097"></a>01097     <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(beta, Y);
<a name="l01098"></a>01098 
<a name="l01099"></a>01099     T4 zero(0);
<a name="l01100"></a>01100     T4 temp;
<a name="l01101"></a>01101     T4 alpha_(alpha);
<a name="l01102"></a>01102 
<a name="l01103"></a>01103     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; ma; i++)
<a name="l01104"></a>01104       {
<a name="l01105"></a>01105         temp = zero;
<a name="l01106"></a>01106         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; na; j++)
<a name="l01107"></a>01107           temp += M(i, j) * X(j);
<a name="l01108"></a>01108         Y(i) += alpha_ * temp;
<a name="l01109"></a>01109       }
<a name="l01110"></a>01110   }
<a name="l01111"></a>01111 
<a name="l01112"></a>01112 
<a name="l01127"></a>01127   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l01128"></a>01128             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01129"></a>01129             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l01130"></a>01130             <span class="keyword">class </span>T3,
<a name="l01131"></a>01131             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l01132"></a>01132   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l01133"></a>01133               <span class="keyword">const</span> Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;&amp; M,
<a name="l01134"></a>01134               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l01135"></a>01135               <span class="keyword">const</span> T3 beta,
<a name="l01136"></a>01136               Vector&lt;T4, Collection, Allocator4&gt;&amp; Y)
<a name="l01137"></a>01137   {
<a name="l01138"></a>01138     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l01139"></a>01139     <span class="keywordtype">int</span> na = M.GetN();
<a name="l01140"></a>01140 
<a name="l01141"></a>01141 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l01142"></a>01142 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l01143"></a>01143 <span class="preprocessor">#endif</span>
<a name="l01144"></a>01144 <span class="preprocessor"></span>
<a name="l01145"></a>01145     <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(beta, Y);
<a name="l01146"></a>01146 
<a name="l01147"></a>01147     <span class="keyword">typename</span> T4::value_type zero(0);
<a name="l01148"></a>01148     <span class="keyword">typename</span> T4::value_type temp;
<a name="l01149"></a>01149     <span class="keyword">typename</span> T4::value_type alpha_(alpha);
<a name="l01150"></a>01150 
<a name="l01151"></a>01151     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; ma; i++)
<a name="l01152"></a>01152       {
<a name="l01153"></a>01153         temp = zero;
<a name="l01154"></a>01154         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; na; j++)
<a name="l01155"></a>01155           temp += M(i, j) * X(j);
<a name="l01156"></a>01156         Y(i) += alpha_ * temp;
<a name="l01157"></a>01157       }
<a name="l01158"></a>01158   }
<a name="l01159"></a>01159 
<a name="l01160"></a>01160 
<a name="l01175"></a>01175   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l01176"></a>01176             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l01177"></a>01177             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2,
<a name="l01178"></a>01178             <span class="keyword">class </span>T3,
<a name="l01179"></a>01179             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l01180"></a>01180   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l01181"></a>01181               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowMajorCollection, Allocator1&gt;&amp; M,
<a name="l01182"></a>01182               <span class="keyword">const</span> Vector&lt;T2, Collection, Allocator2&gt;&amp; X,
<a name="l01183"></a>01183               <span class="keyword">const</span> T3 beta,
<a name="l01184"></a>01184               Vector&lt;T4, Collection, Allocator4&gt;&amp; Y)
<a name="l01185"></a>01185   {
<a name="l01186"></a>01186     <span class="keywordtype">int</span> ma = M.GetMmatrix();
<a name="l01187"></a>01187     <span class="keywordtype">int</span> na = M.GetNmatrix();
<a name="l01188"></a>01188 
<a name="l01189"></a>01189 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l01190"></a>01190 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l01191"></a>01191 <span class="preprocessor">#endif</span>
<a name="l01192"></a>01192 <span class="preprocessor"></span>    <span class="keyword">typedef</span> <span class="keyword">typename</span> T4::value_type value_type;
<a name="l01193"></a>01193 
<a name="l01194"></a>01194     <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(value_type(beta), Y);
<a name="l01195"></a>01195 
<a name="l01196"></a>01196     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; ma; i++)
<a name="l01197"></a>01197       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; na; j++)
<a name="l01198"></a>01198         <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, M.GetMatrix(i, j), X.GetVector(j), value_type(1.),
<a name="l01199"></a>01199                Y.GetVector(i));
<a name="l01200"></a>01200   }
<a name="l01201"></a>01201 
<a name="l01202"></a>01202 
<a name="l01217"></a>01217   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l01218"></a>01218             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l01219"></a>01219             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2,
<a name="l01220"></a>01220             <span class="keyword">class </span>T3,
<a name="l01221"></a>01221             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l01222"></a>01222   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l01223"></a>01223               <span class="keyword">const</span> Matrix&lt;T1, Prop1, ColMajorCollection, Allocator1&gt;&amp; M,
<a name="l01224"></a>01224               <span class="keyword">const</span> Vector&lt;T2, Collection, Allocator2&gt;&amp; X,
<a name="l01225"></a>01225               <span class="keyword">const</span> T3 beta,
<a name="l01226"></a>01226               Vector&lt;T4, Collection, Allocator4&gt;&amp; Y)
<a name="l01227"></a>01227   {
<a name="l01228"></a>01228     <span class="keywordtype">int</span> ma = M.GetMmatrix();
<a name="l01229"></a>01229     <span class="keywordtype">int</span> na = M.GetNmatrix();
<a name="l01230"></a>01230 
<a name="l01231"></a>01231 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l01232"></a>01232 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l01233"></a>01233 <span class="preprocessor">#endif</span>
<a name="l01234"></a>01234 <span class="preprocessor"></span>    <span class="keyword">typedef</span> <span class="keyword">typename</span> T4::value_type value_type;
<a name="l01235"></a>01235 
<a name="l01236"></a>01236     <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(value_type(beta), Y);
<a name="l01237"></a>01237 
<a name="l01238"></a>01238     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; ma; i++)
<a name="l01239"></a>01239       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; na; j++)
<a name="l01240"></a>01240         <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, M.GetMatrix(i, j), X.GetVector(j), value_type(1.),
<a name="l01241"></a>01241                Y.GetVector(i));
<a name="l01242"></a>01242   }
<a name="l01243"></a>01243 
<a name="l01244"></a>01244 
<a name="l01264"></a>01264   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l01265"></a>01265             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01266"></a>01266             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l01267"></a>01267             <span class="keyword">class </span>T3,
<a name="l01268"></a>01268             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l01269"></a>01269   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l01270"></a>01270               <span class="keyword">const</span> SeldonTranspose&amp; Trans,
<a name="l01271"></a>01271               <span class="keyword">const</span> Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;&amp; M,
<a name="l01272"></a>01272               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l01273"></a>01273               <span class="keyword">const</span> T3 beta,
<a name="l01274"></a>01274               Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l01275"></a>01275   {
<a name="l01276"></a>01276     <span class="keywordflow">if</span> (Trans.NoTrans())
<a name="l01277"></a>01277       {
<a name="l01278"></a>01278         <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, M, X, beta, Y);
<a name="l01279"></a>01279         <span class="keywordflow">return</span>;
<a name="l01280"></a>01280       }
<a name="l01281"></a>01281     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (Trans.ConjTrans())
<a name="l01282"></a>01282       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;MltAdd(alpha, trans, M, X, beta, Y)&quot;</span>,
<a name="l01283"></a>01283                           <span class="stringliteral">&quot;Complex conjugation not supported.&quot;</span>);
<a name="l01284"></a>01284 
<a name="l01285"></a>01285     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l01286"></a>01286     <span class="keywordtype">int</span> na = M.GetN();
<a name="l01287"></a>01287 
<a name="l01288"></a>01288 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l01289"></a>01289 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(Trans, M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, trans, M, X, beta, Y)&quot;</span>);
<a name="l01290"></a>01290 <span class="preprocessor">#endif</span>
<a name="l01291"></a>01291 <span class="preprocessor"></span>
<a name="l01292"></a>01292     <span class="keywordflow">if</span> (beta == T3(0))
<a name="l01293"></a>01293       Y.Fill(T4(0));
<a name="l01294"></a>01294     <span class="keywordflow">else</span>
<a name="l01295"></a>01295       <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(beta, Y);
<a name="l01296"></a>01296 
<a name="l01297"></a>01297     T4 zero(0);
<a name="l01298"></a>01298     T4 temp;
<a name="l01299"></a>01299     T4 alpha_(alpha);
<a name="l01300"></a>01300 
<a name="l01301"></a>01301     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; na; i++)
<a name="l01302"></a>01302       {
<a name="l01303"></a>01303         temp = zero;
<a name="l01304"></a>01304         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ma; j++)
<a name="l01305"></a>01305           temp += M(j, i) * X(j);
<a name="l01306"></a>01306         Y(i) += alpha_ * temp;
<a name="l01307"></a>01307       }
<a name="l01308"></a>01308   }
<a name="l01309"></a>01309 
<a name="l01310"></a>01310 
<a name="l01311"></a>01311   <span class="comment">// MLTADD //</span>
<a name="l01313"></a>01313 <span class="comment"></span>
<a name="l01314"></a>01314 
<a name="l01316"></a>01316   <span class="comment">// GAUSS //</span>
<a name="l01317"></a>01317 
<a name="l01318"></a>01318 
<a name="l01319"></a>01319   <span class="comment">// Solve X = M*Y with Gauss method.</span>
<a name="l01320"></a>01320   <span class="comment">// Warning: M is modified. The results are stored in X.</span>
<a name="l01321"></a>01321   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01322"></a>01322             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l01323"></a>01323   <span class="keyword">inline</span> <span class="keywordtype">void</span> Gauss(Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l01324"></a>01324                     Vector&lt;T1, Storage1, Allocator1&gt;&amp; X)
<a name="l01325"></a>01325   {
<a name="l01326"></a>01326     <span class="keywordtype">int</span> i, j, k;
<a name="l01327"></a>01327     T1 r, S;
<a name="l01328"></a>01328 
<a name="l01329"></a>01329     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l01330"></a>01330     <span class="keywordtype">int</span> na = M.GetN();
<a name="l01331"></a>01331 
<a name="l01332"></a>01332 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l01333"></a>01333 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (na != ma)
<a name="l01334"></a>01334       <span class="keywordflow">throw</span> WrongDim(<span class="stringliteral">&quot;Gauss(M, X)&quot;</span>,
<a name="l01335"></a>01335                      <span class="stringliteral">&quot;The matrix must be squared.&quot;</span>);
<a name="l01336"></a>01336 
<a name="l01337"></a>01337     <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, <span class="stringliteral">&quot;Gauss(M, X)&quot;</span>);
<a name="l01338"></a>01338 <span class="preprocessor">#endif</span>
<a name="l01339"></a>01339 <span class="preprocessor"></span>
<a name="l01340"></a>01340     <span class="keywordflow">for</span> (k = 0; k &lt; ma - 1; k++)
<a name="l01341"></a>01341       <span class="keywordflow">for</span> (i = k + 1; i &lt; ma; i++)
<a name="l01342"></a>01342         {
<a name="l01343"></a>01343           r = M(i, k) / M(k, k);
<a name="l01344"></a>01344           <span class="keywordflow">for</span> (j = k + 1; j &lt; ma; j++)
<a name="l01345"></a>01345             M(i, j) -= r * M(k, j);
<a name="l01346"></a>01346           X(i) -= r *= X(k);
<a name="l01347"></a>01347         }
<a name="l01348"></a>01348 
<a name="l01349"></a>01349     X(ma - 1) = X(ma - 1) / M(ma - 1, ma - 1);
<a name="l01350"></a>01350     <span class="keywordflow">for</span> (k = ma - 2; k &gt; -1; k--)
<a name="l01351"></a>01351       {
<a name="l01352"></a>01352         S = X(k);
<a name="l01353"></a>01353         <span class="keywordflow">for</span> (j = k + 1; j &lt; ma; j++)
<a name="l01354"></a>01354           S -= M(k, j) * X(j);
<a name="l01355"></a>01355         X(k) = S / M(k, k);
<a name="l01356"></a>01356       }
<a name="l01357"></a>01357   }
<a name="l01358"></a>01358 
<a name="l01359"></a>01359 
<a name="l01360"></a>01360   <span class="comment">// GAUSS //</span>
<a name="l01362"></a>01362 <span class="comment"></span>
<a name="l01363"></a>01363 
<a name="l01365"></a>01365   <span class="comment">// GAUSS-SEIDEL //</span>
<a name="l01366"></a>01366 
<a name="l01367"></a>01367 
<a name="l01368"></a>01368   <span class="comment">// Solve X = M*Y with Gauss-Seidel method.</span>
<a name="l01369"></a>01369   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01370"></a>01370             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01371"></a>01371             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01372"></a>01372   <span class="keyword">inline</span> <span class="keywordtype">void</span> GaussSeidel(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l01373"></a>01373                           <span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l01374"></a>01374                           Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y,
<a name="l01375"></a>01375                           <span class="keywordtype">int</span> iter)
<a name="l01376"></a>01376   {
<a name="l01377"></a>01377     <span class="keywordtype">int</span> i, j, k;
<a name="l01378"></a>01378     T1 temp;
<a name="l01379"></a>01379 
<a name="l01380"></a>01380     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l01381"></a>01381     <span class="keywordtype">int</span> na = M.GetN();
<a name="l01382"></a>01382 
<a name="l01383"></a>01383 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l01384"></a>01384 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (na != ma)
<a name="l01385"></a>01385       <span class="keywordflow">throw</span> WrongDim(<span class="stringliteral">&quot;GaussSeidel(M, X, Y, iter)&quot;</span>,
<a name="l01386"></a>01386                      <span class="stringliteral">&quot;The matrix must be squared.&quot;</span>);
<a name="l01387"></a>01387 
<a name="l01388"></a>01388     <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;GaussSeidel(M, X, Y, iter)&quot;</span>);
<a name="l01389"></a>01389 <span class="preprocessor">#endif</span>
<a name="l01390"></a>01390 <span class="preprocessor"></span>
<a name="l01391"></a>01391     <span class="keywordflow">for</span> (i = 0; i &lt; iter; i++)
<a name="l01392"></a>01392       <span class="keywordflow">for</span> (j = 0; j &lt; na; j++)
<a name="l01393"></a>01393         {
<a name="l01394"></a>01394           temp = 0;
<a name="l01395"></a>01395           <span class="keywordflow">for</span> (k = 0; k &lt; j; k++)
<a name="l01396"></a>01396             temp -= M(j, k) * Y(k);
<a name="l01397"></a>01397           <span class="keywordflow">for</span> (k = j + 1; k &lt; na; k++)
<a name="l01398"></a>01398             temp -= M(j, k) * Y(k);
<a name="l01399"></a>01399           Y(j) = (X(j) + temp) / M(j, j);
<a name="l01400"></a>01400         }
<a name="l01401"></a>01401   }
<a name="l01402"></a>01402 
<a name="l01403"></a>01403 
<a name="l01404"></a>01404   <span class="comment">// GAUSS-SEIDEL //</span>
<a name="l01406"></a>01406 <span class="comment"></span>
<a name="l01407"></a>01407 
<a name="l01409"></a>01409   <span class="comment">// S.O.R. METHOD //</span>
<a name="l01410"></a>01410 
<a name="l01411"></a>01411 
<a name="l01412"></a>01412   <span class="comment">// Solve X = M*Y with S.O.R. method.</span>
<a name="l01413"></a>01413   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01414"></a>01414             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01415"></a>01415             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l01416"></a>01416             <span class="keyword">class </span>T3&gt;
<a name="l01417"></a>01417   <span class="keyword">inline</span> <span class="keywordtype">void</span> SOR(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l01418"></a>01418                   <span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l01419"></a>01419                   Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y,
<a name="l01420"></a>01420                   T3 omega,
<a name="l01421"></a>01421                   <span class="keywordtype">int</span> iter)
<a name="l01422"></a>01422   {
<a name="l01423"></a>01423     <span class="keywordtype">int</span> i, j, k;
<a name="l01424"></a>01424     T1 temp;
<a name="l01425"></a>01425 
<a name="l01426"></a>01426     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l01427"></a>01427     <span class="keywordtype">int</span> na = M.GetN();
<a name="l01428"></a>01428 
<a name="l01429"></a>01429 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l01430"></a>01430 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (na != ma)
<a name="l01431"></a>01431       <span class="keywordflow">throw</span> WrongDim(<span class="stringliteral">&quot;SOR(M, X, Y, omega, iter)&quot;</span>,
<a name="l01432"></a>01432                      <span class="stringliteral">&quot;The matrix must be squared.&quot;</span>);
<a name="l01433"></a>01433 
<a name="l01434"></a>01434     <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;SOR(M, X, Y, omega, iter)&quot;</span>);
<a name="l01435"></a>01435 <span class="preprocessor">#endif</span>
<a name="l01436"></a>01436 <span class="preprocessor"></span>
<a name="l01437"></a>01437     <span class="keywordflow">for</span> (i = 0; i &lt; iter; i++)
<a name="l01438"></a>01438       <span class="keywordflow">for</span> (j = 0; j &lt; na; j++)
<a name="l01439"></a>01439         {
<a name="l01440"></a>01440           temp = 0;
<a name="l01441"></a>01441           <span class="keywordflow">for</span> (k = 0; k &lt; j; k++)
<a name="l01442"></a>01442             temp -= M(j, k) * Y(k);
<a name="l01443"></a>01443           <span class="keywordflow">for</span> (k = j + 1; k &lt; na; k++)
<a name="l01444"></a>01444             temp -= M(j, k) * Y(k);
<a name="l01445"></a>01445           Y(j) = (T3(1) - omega) * Y(j) + omega * (X(j) + temp) / M(j, j);
<a name="l01446"></a>01446         }
<a name="l01447"></a>01447   }
<a name="l01448"></a>01448 
<a name="l01449"></a>01449 
<a name="l01450"></a>01450   <span class="comment">// S.O.R. METHOD //</span>
<a name="l01452"></a>01452 <span class="comment"></span>
<a name="l01453"></a>01453 
<a name="l01454"></a>01454 
<a name="l01456"></a>01456   <span class="comment">// SOLVELU //</span>
<a name="l01457"></a>01457 
<a name="l01458"></a>01458 
<a name="l01460"></a>01460 
<a name="l01472"></a>01472   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01473"></a>01473             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l01474"></a>01474   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l01475"></a>01475                Vector&lt;T1, Storage1, Allocator1&gt;&amp; Y)
<a name="l01476"></a>01476   {
<a name="l01477"></a>01477     <span class="keywordtype">int</span> i, k;
<a name="l01478"></a>01478     T1 temp;
<a name="l01479"></a>01479 
<a name="l01480"></a>01480     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l01481"></a>01481 
<a name="l01482"></a>01482 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l01483"></a>01483 <span class="preprocessor"></span>    <span class="keywordtype">int</span> na = M.GetN();
<a name="l01484"></a>01484     <span class="keywordflow">if</span> (na != ma)
<a name="l01485"></a>01485       <span class="keywordflow">throw</span> WrongDim(<span class="stringliteral">&quot;SolveLU(M, Y)&quot;</span>,
<a name="l01486"></a>01486                      <span class="stringliteral">&quot;The matrix must be squared.&quot;</span>);
<a name="l01487"></a>01487 
<a name="l01488"></a>01488     <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, Y, <span class="stringliteral">&quot;SolveLU(M, Y)&quot;</span>);
<a name="l01489"></a>01489 <span class="preprocessor">#endif</span>
<a name="l01490"></a>01490 <span class="preprocessor"></span>
<a name="l01491"></a>01491     <span class="comment">// Forward substitution.</span>
<a name="l01492"></a>01492     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l01493"></a>01493       {
<a name="l01494"></a>01494         temp = 0;
<a name="l01495"></a>01495         <span class="keywordflow">for</span> (k = 0; k &lt; i; k++)
<a name="l01496"></a>01496           temp += M(i, k) * Y(k);
<a name="l01497"></a>01497         Y(i) = (Y(i) - temp) / M(i, i);
<a name="l01498"></a>01498       }
<a name="l01499"></a>01499     <span class="comment">// Back substitution.</span>
<a name="l01500"></a>01500     <span class="keywordflow">for</span> (i = ma - 2; i &gt; -1; i--)
<a name="l01501"></a>01501       {
<a name="l01502"></a>01502         temp = 0;
<a name="l01503"></a>01503         <span class="keywordflow">for</span> (k = i + 1; k &lt; ma; k++)
<a name="l01504"></a>01504           temp += M(i, k) * Y(k);
<a name="l01505"></a>01505         Y(i) -= temp;
<a name="l01506"></a>01506       }
<a name="l01507"></a>01507   }
<a name="l01508"></a>01508 
<a name="l01509"></a>01509 
<a name="l01510"></a>01510   <span class="comment">// SOLVELU //</span>
<a name="l01512"></a>01512 <span class="comment"></span>
<a name="l01513"></a>01513 
<a name="l01515"></a>01515   <span class="comment">// SOLVE //</span>
<a name="l01516"></a>01516 
<a name="l01517"></a>01517 
<a name="l01519"></a>01519 
<a name="l01526"></a>01526   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01527"></a>01527             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l01528"></a>01528   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ac173624f2f8a8e737e4f9c7b29ad95af" title="Solves a linear system using LU factorization.">GetAndSolveLU</a>(Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l01529"></a>01529                      Vector&lt;T1, Storage1, Allocator1&gt;&amp; Y)
<a name="l01530"></a>01530   {
<a name="l01531"></a>01531 <span class="preprocessor">#ifdef SELDON_WITH_LAPACK</span>
<a name="l01532"></a>01532 <span class="preprocessor"></span>    Vector&lt;int&gt; P;
<a name="l01533"></a>01533     <a class="code" href="namespace_seldon.php#a76d3f187a877c6c58245b0716e1adc00" title="Returns the LU factorization of a matrix.">GetLU</a>(M, P);
<a name="l01534"></a>01534     <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(M, P, Y);
<a name="l01535"></a>01535 <span class="preprocessor">#else</span>
<a name="l01536"></a>01536 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#a76d3f187a877c6c58245b0716e1adc00" title="Returns the LU factorization of a matrix.">GetLU</a>(M);
<a name="l01537"></a>01537     <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(M, Y);
<a name="l01538"></a>01538 <span class="preprocessor">#endif</span>
<a name="l01539"></a>01539 <span class="preprocessor"></span>  }
<a name="l01540"></a>01540 
<a name="l01541"></a>01541 
<a name="l01542"></a>01542   <span class="comment">// SOLVE //</span>
<a name="l01544"></a>01544 <span class="comment"></span>
<a name="l01545"></a>01545 
<a name="l01547"></a>01547   <span class="comment">// CHECKDIM //</span>
<a name="l01548"></a>01548 
<a name="l01549"></a>01549 
<a name="l01551"></a>01551 
<a name="l01560"></a>01560   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01561"></a>01561             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01562"></a>01562             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01563"></a>01563   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l01564"></a>01564                 <span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l01565"></a>01565                 <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y,
<a name="l01566"></a>01566                 <span class="keywordtype">string</span> function)
<a name="l01567"></a>01567   {
<a name="l01568"></a>01568     <span class="keywordflow">if</span> (X.GetLength() != M.GetN() || Y.GetLength() != M.GetM())
<a name="l01569"></a>01569       <span class="keywordflow">throw</span> WrongDim(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation M X + Y -&gt; Y not permitted:&quot;</span>)
<a name="l01570"></a>01570                      + string(<span class="stringliteral">&quot;\n     M (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;M) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l01571"></a>01571                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetM()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetN())
<a name="l01572"></a>01572                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X)
<a name="l01573"></a>01573                      + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01574"></a>01574                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetLength()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>)
<a name="l01575"></a>01575                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01576"></a>01576                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetLength()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l01577"></a>01577   }
<a name="l01578"></a>01578 
<a name="l01579"></a>01579 
<a name="l01581"></a>01581 
<a name="l01590"></a>01590   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l01591"></a>01591             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l01592"></a>01592             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01593"></a>01593   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, RowMajorCollection, Allocator0&gt;&amp; M,
<a name="l01594"></a>01594                 <span class="keyword">const</span> Vector&lt;T1, Collection, Allocator1&gt;&amp; X,
<a name="l01595"></a>01595                 <span class="keyword">const</span> Vector&lt;T2, Collection, Allocator2&gt;&amp; Y,
<a name="l01596"></a>01596                 <span class="keywordtype">string</span> function)
<a name="l01597"></a>01597   {
<a name="l01598"></a>01598     <span class="keywordflow">if</span> (X.GetNvector() != M.GetNmatrix() || Y.GetNvector() != M.GetMmatrix())
<a name="l01599"></a>01599       <span class="keywordflow">throw</span> WrongDim(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation M X + Y -&gt; Y not permitted:&quot;</span>)
<a name="l01600"></a>01600                      + string(<span class="stringliteral">&quot;\n     M (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;M) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l01601"></a>01601                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetM()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetN())
<a name="l01602"></a>01602                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X)
<a name="l01603"></a>01603                      + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01604"></a>01604                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetNvector()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>)
<a name="l01605"></a>01605                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01606"></a>01606                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetNvector()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l01607"></a>01607   }
<a name="l01608"></a>01608 
<a name="l01609"></a>01609 
<a name="l01611"></a>01611 
<a name="l01620"></a>01620   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l01621"></a>01621             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l01622"></a>01622             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01623"></a>01623   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, ColMajorCollection, Allocator0&gt;&amp; M,
<a name="l01624"></a>01624                 <span class="keyword">const</span> Vector&lt;T1, Collection, Allocator1&gt;&amp; X,
<a name="l01625"></a>01625                 <span class="keyword">const</span> Vector&lt;T2, Collection, Allocator2&gt;&amp; Y,
<a name="l01626"></a>01626                 <span class="keywordtype">string</span> function)
<a name="l01627"></a>01627   {
<a name="l01628"></a>01628     <span class="keywordflow">if</span> (X.GetNvector() != M.GetNmatrix() || Y.GetNvector() != M.GetMmatrix())
<a name="l01629"></a>01629       <span class="keywordflow">throw</span> WrongDim(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation M X + Y -&gt; Y not permitted:&quot;</span>)
<a name="l01630"></a>01630                      + string(<span class="stringliteral">&quot;\n     M (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;M) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l01631"></a>01631                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetM()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetN())
<a name="l01632"></a>01632                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X)
<a name="l01633"></a>01633                      + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01634"></a>01634                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetNvector()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>)
<a name="l01635"></a>01635                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01636"></a>01636                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetNvector()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l01637"></a>01637   }
<a name="l01638"></a>01638 
<a name="l01639"></a>01639 
<a name="l01641"></a>01641 
<a name="l01650"></a>01650   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01651"></a>01651             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l01652"></a>01652             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01653"></a>01653   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l01654"></a>01654                 <span class="keyword">const</span> Vector&lt;T1, Collection, Allocator1&gt;&amp; X,
<a name="l01655"></a>01655                 <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y,
<a name="l01656"></a>01656                 <span class="keywordtype">string</span> function)
<a name="l01657"></a>01657   {
<a name="l01658"></a>01658     <span class="keywordflow">if</span> (X.GetLength() != M.GetN() || Y.GetLength() != M.GetM())
<a name="l01659"></a>01659       <span class="keywordflow">throw</span> WrongDim(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation M X + Y -&gt; Y not permitted:&quot;</span>)
<a name="l01660"></a>01660                      + string(<span class="stringliteral">&quot;\n     M (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;M) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l01661"></a>01661                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetM()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetN())
<a name="l01662"></a>01662                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X)
<a name="l01663"></a>01663                      + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01664"></a>01664                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetLength()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>)
<a name="l01665"></a>01665                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01666"></a>01666                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetLength()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l01667"></a>01667   }
<a name="l01668"></a>01668 
<a name="l01669"></a>01669 
<a name="l01671"></a>01671 
<a name="l01683"></a>01683   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01684"></a>01684             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01685"></a>01685             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01686"></a>01686   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> SeldonTranspose&amp; trans,
<a name="l01687"></a>01687                 <span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l01688"></a>01688                 <span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l01689"></a>01689                 <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y,
<a name="l01690"></a>01690                 <span class="keywordtype">string</span> function, <span class="keywordtype">string</span> op)
<a name="l01691"></a>01691   {
<a name="l01692"></a>01692     <span class="keywordflow">if</span> (op == <span class="stringliteral">&quot;M X + Y -&gt; Y&quot;</span>)
<a name="l01693"></a>01693       <span class="keywordflow">if</span> (trans.Trans())
<a name="l01694"></a>01694         op = <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation M&#39; X + Y -&gt; Y not permitted:&quot;</span>);
<a name="l01695"></a>01695       <span class="keywordflow">else</span> <span class="keywordflow">if</span> (trans.ConjTrans())
<a name="l01696"></a>01696         op = <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation M* X + Y -&gt; Y not permitted:&quot;</span>);
<a name="l01697"></a>01697       <span class="keywordflow">else</span>
<a name="l01698"></a>01698         op = string(<span class="stringliteral">&quot;Operation M X + Y -&gt; Y not permitted:&quot;</span>);
<a name="l01699"></a>01699     <span class="keywordflow">else</span>
<a name="l01700"></a>01700       op = string(<span class="stringliteral">&quot;Operation &quot;</span>) + op + string(<span class="stringliteral">&quot; not permitted:&quot;</span>);
<a name="l01701"></a>01701     <span class="keywordflow">if</span> (X.GetLength() != M.GetN(trans) || Y.GetLength() != M.GetM(trans))
<a name="l01702"></a>01702       <span class="keywordflow">throw</span> WrongDim(function, op + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;\n     M (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;M)
<a name="l01703"></a>01703                      + string(<span class="stringliteral">&quot;) is a &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetM()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>)
<a name="l01704"></a>01704                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetN()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     X (&quot;</span>)
<a name="l01705"></a>01705                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X) + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01706"></a>01706                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetLength()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>)
<a name="l01707"></a>01707                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01708"></a>01708                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetLength()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l01709"></a>01709   }
<a name="l01710"></a>01710 
<a name="l01711"></a>01711 
<a name="l01713"></a>01713 
<a name="l01722"></a>01722   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01723"></a>01723             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l01724"></a>01724   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l01725"></a>01725                 <span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l01726"></a>01726                 <span class="keywordtype">string</span> function, <span class="keywordtype">string</span> op)
<a name="l01727"></a>01727   {
<a name="l01728"></a>01728     <span class="keywordflow">if</span> (X.GetLength() != M.GetN())
<a name="l01729"></a>01729       <span class="keywordflow">throw</span> WrongDim(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation &quot;</span>) + op + <span class="stringliteral">&quot; not permitted:&quot;</span>
<a name="l01730"></a>01730                      + string(<span class="stringliteral">&quot;\n     M (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;M) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l01731"></a>01731                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetM()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetN())
<a name="l01732"></a>01732                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X)
<a name="l01733"></a>01733                      + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01734"></a>01734                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetLength()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l01735"></a>01735   }
<a name="l01736"></a>01736 
<a name="l01737"></a>01737 
<a name="l01738"></a>01738   <span class="comment">// CHECKDIM //</span>
<a name="l01740"></a>01740 <span class="comment"></span>
<a name="l01741"></a>01741 
<a name="l01742"></a>01742 }  <span class="comment">// namespace Seldon.</span>
<a name="l01743"></a>01743 
<a name="l01744"></a>01744 
<a name="l01745"></a>01745 <span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
