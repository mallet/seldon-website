<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/solver/iterative/Iterative.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_ITERATIVE_CXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 <span class="preprocessor">#include &lt;vector&gt;</span>
<a name="l00023"></a>00023 
<a name="l00024"></a>00024 <span class="comment">// headers of class Iteration and Preconditioner_Base</span>
<a name="l00025"></a>00025 <span class="preprocessor">#include &quot;Iterative.hxx&quot;</span>
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 <span class="comment">// and all iterative solvers</span>
<a name="l00028"></a>00028 <span class="preprocessor">#include &quot;Cg.cxx&quot;</span>
<a name="l00029"></a>00029 <span class="preprocessor">#include &quot;Cgne.cxx&quot;</span>
<a name="l00030"></a>00030 <span class="preprocessor">#include &quot;Lsqr.cxx&quot;</span>
<a name="l00031"></a>00031 <span class="preprocessor">#include &quot;Cgs.cxx&quot;</span>
<a name="l00032"></a>00032 <span class="preprocessor">#include &quot;BiCg.cxx&quot;</span>
<a name="l00033"></a>00033 <span class="preprocessor">#include &quot;BiCgStab.cxx&quot;</span>
<a name="l00034"></a>00034 <span class="preprocessor">#include &quot;BiCgStabl.cxx&quot;</span>
<a name="l00035"></a>00035 <span class="preprocessor">#include &quot;BiCgcr.cxx&quot;</span>
<a name="l00036"></a>00036 <span class="preprocessor">#include &quot;Gcr.cxx&quot;</span>
<a name="l00037"></a>00037 <span class="preprocessor">#include &quot;CoCg.cxx&quot;</span>
<a name="l00038"></a>00038 <span class="preprocessor">#include &quot;Gmres.cxx&quot;</span>
<a name="l00039"></a>00039 <span class="preprocessor">#include &quot;MinRes.cxx&quot;</span>
<a name="l00040"></a>00040 <span class="preprocessor">#include &quot;Qmr.cxx&quot;</span>
<a name="l00041"></a>00041 <span class="preprocessor">#include &quot;QmrSym.cxx&quot;</span>
<a name="l00042"></a>00042 <span class="preprocessor">#include &quot;QCgs.cxx&quot;</span>
<a name="l00043"></a>00043 <span class="preprocessor">#include &quot;TfQmr.cxx&quot;</span>
<a name="l00044"></a>00044 <span class="preprocessor">#include &quot;Symmlq.cxx&quot;</span>
<a name="l00045"></a>00045 
<a name="l00046"></a>00046 
<a name="l00047"></a>00047 <span class="keyword">namespace </span>Seldon
<a name="l00048"></a><a class="code" href="class_seldon_1_1_preconditioner___base.php#a2845a80350be45add812148062be20e9">00048</a> {
<a name="l00049"></a>00049 
<a name="l00051"></a>00051   <a class="code" href="class_seldon_1_1_preconditioner___base.php#a2845a80350be45add812148062be20e9" title="Default constructor.">Preconditioner_Base::Preconditioner_Base</a>()
<a name="l00052"></a>00052   {
<a name="l00053"></a>00053   }
<a name="l00054"></a>00054 
<a name="l00055"></a>00055 
<a name="l00057"></a>00057 
<a name="l00060"></a>00060   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Matrix1, <span class="keyword">class</span> Vector1&gt;
<a name="l00061"></a>00061   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_preconditioner___base.php#ae0cef5d8a4c53cc5a5025ccdc8712fad" title="Solves M z = r.">Preconditioner_Base::Solve</a>(<span class="keyword">const</span> Matrix1&amp; A, <span class="keyword">const</span> Vector1&amp; r,
<a name="l00062"></a>00062                                   Vector1&amp; z)
<a name="l00063"></a>00063   {
<a name="l00064"></a>00064     Copy(r,z);
<a name="l00065"></a>00065   }
<a name="l00066"></a>00066 
<a name="l00067"></a>00067 
<a name="l00069"></a>00069 
<a name="l00072"></a>00072   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Matrix1, <span class="keyword">class</span> Vector1&gt;
<a name="l00073"></a>00073   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_preconditioner___base.php#ac7ddd68847e7c895ac63866742138852" title="Solves M^t z = r.">Preconditioner_Base::</a>
<a name="l00074"></a>00074 <a class="code" href="class_seldon_1_1_preconditioner___base.php#ac7ddd68847e7c895ac63866742138852" title="Solves M^t z = r.">  TransSolve</a>(<span class="keyword">const</span> Matrix1&amp; A, <span class="keyword">const</span> Vector1 &amp; r, Vector1 &amp; z)
<a name="l00075"></a>00075   {
<a name="l00076"></a>00076     <a class="code" href="class_seldon_1_1_preconditioner___base.php#ae0cef5d8a4c53cc5a5025ccdc8712fad" title="Solves M z = r.">Solve</a>(A, r, z);
<a name="l00077"></a>00077   }
<a name="l00078"></a>00078 
<a name="l00079"></a><a class="code" href="class_seldon_1_1_iteration.php#a9c42765a055d9dd271b2b8a52a0baa57">00079</a> 
<a name="l00081"></a>00081   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00082"></a>00082   <a class="code" href="class_seldon_1_1_iteration.php#a9c42765a055d9dd271b2b8a52a0baa57" title="Default constructor.">Iteration&lt;Titer&gt;::Iteration</a>()
<a name="l00083"></a>00083   {
<a name="l00084"></a>00084     <a class="code" href="class_seldon_1_1_iteration.php#acf9550c0ec96e51fc5d395446342c4e8" title="stopping criterion">tolerance</a> = 1e-6;
<a name="l00085"></a>00085     <a class="code" href="class_seldon_1_1_iteration.php#a75359d82e80a14fa10907b203fc5a2ff" title="maximum number of iterations">max_iter</a> = 100;
<a name="l00086"></a>00086     <a class="code" href="class_seldon_1_1_iteration.php#a4ff83b0b358c1574c6c0ababbbe04745" title="number of iterations">nb_iter</a> = 0;
<a name="l00087"></a>00087     <a class="code" href="class_seldon_1_1_iteration.php#a493984d8abaec3ac0d4f73c29d2a0e60" title="error code returned by iterative solver">error_code</a> = 0;
<a name="l00088"></a>00088     <a class="code" href="class_seldon_1_1_iteration.php#afea45227a5d730ac1464b3c33dabfc9e" title="true if the iterative solver has converged //! print level">fail_convergence</a> = <span class="keyword">false</span>;
<a name="l00089"></a>00089     <a class="code" href="class_seldon_1_1_iteration.php#a82adaef364a9a20a7525ab9299b6bfd5">print_level</a> = 1;
<a name="l00090"></a>00090     <a class="code" href="class_seldon_1_1_iteration.php#a363306d1242c5a2b78737ace82ea465b" title="true if initial guess is null">init_guess_null</a> = <span class="keyword">true</span>;
<a name="l00091"></a>00091     <a class="code" href="class_seldon_1_1_iteration.php#aced354c0bf1e3868ae5b77283982b8fa" title="iterative solver used">type_solver</a> = 0; <a class="code" href="class_seldon_1_1_iteration.php#a5c4d36f6bff1c44d6e7b31bb3c70b603" title="restart parameter (for Gmres and Gcr)">parameter_restart</a> = 10;
<a name="l00092"></a>00092     <a class="code" href="class_seldon_1_1_iteration.php#a19f60dd0946c1a5a212b0e28b5be55a9" title="preconditioner used">type_preconditioning</a> = 0;
<a name="l00093"></a>00093   }
<a name="l00094"></a>00094 
<a name="l00095"></a><a class="code" href="class_seldon_1_1_iteration.php#a7631992edbb680c8840bc586410a016a">00095</a> 
<a name="l00097"></a>00097   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00098"></a>00098   <a class="code" href="class_seldon_1_1_iteration.php#a9c42765a055d9dd271b2b8a52a0baa57" title="Default constructor.">Iteration&lt;Titer&gt;::Iteration</a>(<span class="keywordtype">int</span> max_iteration, <span class="keyword">const</span> Titer&amp; tol)
<a name="l00099"></a>00099   {
<a name="l00100"></a>00100     <a class="code" href="class_seldon_1_1_iteration.php#a75359d82e80a14fa10907b203fc5a2ff" title="maximum number of iterations">max_iter</a> = max_iteration;
<a name="l00101"></a>00101     <a class="code" href="class_seldon_1_1_iteration.php#acf9550c0ec96e51fc5d395446342c4e8" title="stopping criterion">tolerance</a> = tol;
<a name="l00102"></a>00102     <a class="code" href="class_seldon_1_1_iteration.php#a4ff83b0b358c1574c6c0ababbbe04745" title="number of iterations">nb_iter</a> = 0;
<a name="l00103"></a>00103     <a class="code" href="class_seldon_1_1_iteration.php#a493984d8abaec3ac0d4f73c29d2a0e60" title="error code returned by iterative solver">error_code</a> = 0;
<a name="l00104"></a>00104     <a class="code" href="class_seldon_1_1_iteration.php#afea45227a5d730ac1464b3c33dabfc9e" title="true if the iterative solver has converged //! print level">fail_convergence</a> = <span class="keyword">false</span>;
<a name="l00105"></a>00105     <a class="code" href="class_seldon_1_1_iteration.php#a82adaef364a9a20a7525ab9299b6bfd5">print_level</a> = 1;
<a name="l00106"></a>00106     <a class="code" href="class_seldon_1_1_iteration.php#a363306d1242c5a2b78737ace82ea465b" title="true if initial guess is null">init_guess_null</a> = <span class="keyword">true</span>;
<a name="l00107"></a>00107     <a class="code" href="class_seldon_1_1_iteration.php#aced354c0bf1e3868ae5b77283982b8fa" title="iterative solver used">type_solver</a> = 0; <a class="code" href="class_seldon_1_1_iteration.php#a5c4d36f6bff1c44d6e7b31bb3c70b603" title="restart parameter (for Gmres and Gcr)">parameter_restart</a> = 10;
<a name="l00108"></a>00108     <a class="code" href="class_seldon_1_1_iteration.php#a19f60dd0946c1a5a212b0e28b5be55a9" title="preconditioner used">type_preconditioning</a> = 0;
<a name="l00109"></a>00109   }
<a name="l00110"></a>00110 
<a name="l00111"></a><a class="code" href="class_seldon_1_1_iteration.php#a6463cf08af3be211d0c5c397b27b177d">00111</a> 
<a name="l00113"></a>00113   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00114"></a>00114   <a class="code" href="class_seldon_1_1_iteration.php#a9c42765a055d9dd271b2b8a52a0baa57" title="Default constructor.">Iteration&lt;Titer&gt;::Iteration</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_iteration.php" title="Class containing parameters for an iterative resolution.">Iteration&lt;Titer&gt;</a>&amp; outer)
<a name="l00115"></a>00115   {
<a name="l00116"></a>00116     <a class="code" href="class_seldon_1_1_iteration.php#acf9550c0ec96e51fc5d395446342c4e8" title="stopping criterion">tolerance</a> = outer.<a class="code" href="class_seldon_1_1_iteration.php#acf9550c0ec96e51fc5d395446342c4e8" title="stopping criterion">tolerance</a>; <a class="code" href="class_seldon_1_1_iteration.php#adf29aa77b9dd1ecdc57c132712b02811" title="inverse of norm of first residual">facteur_reste</a> = outer.<a class="code" href="class_seldon_1_1_iteration.php#adf29aa77b9dd1ecdc57c132712b02811" title="inverse of norm of first residual">facteur_reste</a>;
<a name="l00117"></a>00117     <a class="code" href="class_seldon_1_1_iteration.php#a75359d82e80a14fa10907b203fc5a2ff" title="maximum number of iterations">max_iter</a> = outer.<a class="code" href="class_seldon_1_1_iteration.php#a75359d82e80a14fa10907b203fc5a2ff" title="maximum number of iterations">max_iter</a>;
<a name="l00118"></a>00118     <a class="code" href="class_seldon_1_1_iteration.php#a4ff83b0b358c1574c6c0ababbbe04745" title="number of iterations">nb_iter</a> = 0;
<a name="l00119"></a>00119     <a class="code" href="class_seldon_1_1_iteration.php#a82adaef364a9a20a7525ab9299b6bfd5">print_level</a> = outer.<a class="code" href="class_seldon_1_1_iteration.php#a82adaef364a9a20a7525ab9299b6bfd5">print_level</a>; <a class="code" href="class_seldon_1_1_iteration.php#a493984d8abaec3ac0d4f73c29d2a0e60" title="error code returned by iterative solver">error_code</a> = outer.<a class="code" href="class_seldon_1_1_iteration.php#a493984d8abaec3ac0d4f73c29d2a0e60" title="error code returned by iterative solver">error_code</a>;
<a name="l00120"></a>00120     <a class="code" href="class_seldon_1_1_iteration.php#a363306d1242c5a2b78737ace82ea465b" title="true if initial guess is null">init_guess_null</a> = <span class="keyword">true</span>;
<a name="l00121"></a>00121     <a class="code" href="class_seldon_1_1_iteration.php#aced354c0bf1e3868ae5b77283982b8fa" title="iterative solver used">type_solver</a> = outer.<a class="code" href="class_seldon_1_1_iteration.php#aced354c0bf1e3868ae5b77283982b8fa" title="iterative solver used">type_solver</a>;
<a name="l00122"></a>00122     <a class="code" href="class_seldon_1_1_iteration.php#a5c4d36f6bff1c44d6e7b31bb3c70b603" title="restart parameter (for Gmres and Gcr)">parameter_restart</a> = outer.<a class="code" href="class_seldon_1_1_iteration.php#a5c4d36f6bff1c44d6e7b31bb3c70b603" title="restart parameter (for Gmres and Gcr)">parameter_restart</a>;
<a name="l00123"></a>00123     <a class="code" href="class_seldon_1_1_iteration.php#a19f60dd0946c1a5a212b0e28b5be55a9" title="preconditioner used">type_preconditioning</a> = outer.<a class="code" href="class_seldon_1_1_iteration.php#a19f60dd0946c1a5a212b0e28b5be55a9" title="preconditioner used">type_preconditioning</a>;
<a name="l00124"></a>00124   }
<a name="l00125"></a>00125 
<a name="l00126"></a><a class="code" href="class_seldon_1_1_iteration.php#ad57613fa7b74a7a7a4e758262957ca0f">00126</a> 
<a name="l00128"></a>00128   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00129"></a>00129   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_iteration.php#ad57613fa7b74a7a7a4e758262957ca0f" title="Returns the type of solver.">Iteration&lt;Titer&gt;::GetTypeSolver</a>()<span class="keyword"> const</span>
<a name="l00130"></a>00130 <span class="keyword">  </span>{
<a name="l00131"></a>00131     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_iteration.php#aced354c0bf1e3868ae5b77283982b8fa" title="iterative solver used">type_solver</a>;
<a name="l00132"></a>00132   }
<a name="l00133"></a>00133 
<a name="l00134"></a><a class="code" href="class_seldon_1_1_iteration.php#ae383df46b41aff26fe97418cab8a070f">00134</a> 
<a name="l00136"></a>00136   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00137"></a>00137   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_iteration.php#ae383df46b41aff26fe97418cab8a070f" title="Returns the restart parameter.">Iteration&lt;Titer&gt;::GetRestart</a>()<span class="keyword"> const</span>
<a name="l00138"></a>00138 <span class="keyword">  </span>{
<a name="l00139"></a>00139     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_iteration.php#a5c4d36f6bff1c44d6e7b31bb3c70b603" title="restart parameter (for Gmres and Gcr)">parameter_restart</a>;
<a name="l00140"></a>00140   }
<a name="l00141"></a>00141 
<a name="l00142"></a><a class="code" href="class_seldon_1_1_iteration.php#a617c6d99afeb2b5a2d18144f367dba88">00142</a> 
<a name="l00144"></a>00144   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00145"></a>00145   Titer <a class="code" href="class_seldon_1_1_iteration.php#a617c6d99afeb2b5a2d18144f367dba88" title="Returns used coefficient to compute relative residual.">Iteration&lt;Titer&gt;::GetFactor</a>()<span class="keyword"> const</span>
<a name="l00146"></a>00146 <span class="keyword">  </span>{
<a name="l00147"></a>00147     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_iteration.php#adf29aa77b9dd1ecdc57c132712b02811" title="inverse of norm of first residual">facteur_reste</a>;
<a name="l00148"></a>00148   }
<a name="l00149"></a>00149 
<a name="l00150"></a><a class="code" href="class_seldon_1_1_iteration.php#af31105a0169bb625d24a4ba76183b3a5">00150</a> 
<a name="l00152"></a>00152   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00153"></a>00153   Titer <a class="code" href="class_seldon_1_1_iteration.php#af31105a0169bb625d24a4ba76183b3a5" title="Returns stopping criterion.">Iteration&lt;Titer&gt;::GetTolerance</a>()<span class="keyword"> const</span>
<a name="l00154"></a>00154 <span class="keyword">  </span>{
<a name="l00155"></a>00155     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_iteration.php#acf9550c0ec96e51fc5d395446342c4e8" title="stopping criterion">tolerance</a>;
<a name="l00156"></a>00156   }
<a name="l00157"></a>00157 
<a name="l00158"></a><a class="code" href="class_seldon_1_1_iteration.php#ab426008d4bfc40842d86c8c6042a6594">00158</a> 
<a name="l00160"></a>00160   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00161"></a>00161   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_iteration.php#ab426008d4bfc40842d86c8c6042a6594" title="Returns the number of iterations.">Iteration&lt;Titer&gt;::GetNumberIteration</a>()<span class="keyword"> const</span>
<a name="l00162"></a>00162 <span class="keyword">  </span>{
<a name="l00163"></a>00163     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_iteration.php#a4ff83b0b358c1574c6c0ababbbe04745" title="number of iterations">nb_iter</a>;
<a name="l00164"></a>00164   }
<a name="l00165"></a>00165 
<a name="l00166"></a><a class="code" href="class_seldon_1_1_iteration.php#a7906239fb42957439f236258d4423465">00166</a> 
<a name="l00168"></a>00168   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00169"></a>00169   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_iteration.php#a7906239fb42957439f236258d4423465" title="Changes the type of solver and preconditioning.">Iteration&lt;Titer&gt;::SetSolver</a>(<span class="keywordtype">int</span> type_resolution,
<a name="l00170"></a>00170                                    <span class="keywordtype">int</span> param_restart, <span class="keywordtype">int</span> type_prec)
<a name="l00171"></a>00171   {
<a name="l00172"></a>00172     <a class="code" href="class_seldon_1_1_iteration.php#aced354c0bf1e3868ae5b77283982b8fa" title="iterative solver used">type_solver</a> = type_resolution;
<a name="l00173"></a>00173     <a class="code" href="class_seldon_1_1_iteration.php#a5c4d36f6bff1c44d6e7b31bb3c70b603" title="restart parameter (for Gmres and Gcr)">parameter_restart</a> = param_restart;
<a name="l00174"></a>00174     <a class="code" href="class_seldon_1_1_iteration.php#a19f60dd0946c1a5a212b0e28b5be55a9" title="preconditioner used">type_preconditioning</a> = type_prec;
<a name="l00175"></a>00175   }
<a name="l00176"></a>00176 
<a name="l00177"></a><a class="code" href="class_seldon_1_1_iteration.php#a3a5874b5b76075698ceeb99944261311">00177</a> 
<a name="l00179"></a>00179   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00180"></a>00180   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_iteration.php#a3a5874b5b76075698ceeb99944261311" title="Changes the restart parameter.">Iteration&lt;Titer&gt;::SetRestart</a>(<span class="keywordtype">int</span> m)
<a name="l00181"></a>00181   {
<a name="l00182"></a>00182     <a class="code" href="class_seldon_1_1_iteration.php#a5c4d36f6bff1c44d6e7b31bb3c70b603" title="restart parameter (for Gmres and Gcr)">parameter_restart</a> = m;
<a name="l00183"></a>00183   }
<a name="l00184"></a>00184 
<a name="l00185"></a><a class="code" href="class_seldon_1_1_iteration.php#a1393846a1b9090c4bb3516b28e70561b">00185</a> 
<a name="l00187"></a>00187   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00188"></a>00188   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_iteration.php#a1393846a1b9090c4bb3516b28e70561b" title="Changes the stopping criterion.">Iteration&lt;Titer&gt;::SetTolerance</a>(Titer stopping_criterion)
<a name="l00189"></a>00189   {
<a name="l00190"></a>00190     <a class="code" href="class_seldon_1_1_iteration.php#acf9550c0ec96e51fc5d395446342c4e8" title="stopping criterion">tolerance</a> = stopping_criterion;
<a name="l00191"></a>00191   }
<a name="l00192"></a>00192 
<a name="l00193"></a><a class="code" href="class_seldon_1_1_iteration.php#afed55f067231a1defcf50acd0755aef1">00193</a> 
<a name="l00195"></a>00195   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00196"></a>00196   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_iteration.php#afed55f067231a1defcf50acd0755aef1" title="Changes the maximum number of iterations.">Iteration&lt;Titer&gt;::SetMaxNumberIteration</a>(<span class="keywordtype">int</span> max_iteration)
<a name="l00197"></a>00197   {
<a name="l00198"></a>00198     <a class="code" href="class_seldon_1_1_iteration.php#a75359d82e80a14fa10907b203fc5a2ff" title="maximum number of iterations">max_iter</a>=max_iteration;
<a name="l00199"></a>00199   }
<a name="l00200"></a>00200 
<a name="l00201"></a><a class="code" href="class_seldon_1_1_iteration.php#a24c69ffb62504d708354f8874a5f0f00">00201</a> 
<a name="l00203"></a>00203   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00204"></a>00204   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_iteration.php#a24c69ffb62504d708354f8874a5f0f00" title="Changes the number of iterations.">Iteration&lt;Titer&gt;::SetNumberIteration</a>(<span class="keywordtype">int</span> nb)
<a name="l00205"></a>00205   {
<a name="l00206"></a>00206     <a class="code" href="class_seldon_1_1_iteration.php#a4ff83b0b358c1574c6c0ababbbe04745" title="number of iterations">nb_iter</a> = nb;
<a name="l00207"></a>00207   }
<a name="l00208"></a>00208 
<a name="l00209"></a><a class="code" href="class_seldon_1_1_iteration.php#a18f2bdb38c22716d640422ead8998e1f">00209</a> 
<a name="l00211"></a>00211   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00212"></a>00212   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_iteration.php#a18f2bdb38c22716d640422ead8998e1f" title="Sets to a normal display (residual each 100 iterations).">Iteration&lt;Titer&gt;::ShowMessages</a>()
<a name="l00213"></a>00213   {
<a name="l00214"></a>00214     <a class="code" href="class_seldon_1_1_iteration.php#a82adaef364a9a20a7525ab9299b6bfd5">print_level</a> = 1;
<a name="l00215"></a>00215   }
<a name="l00216"></a>00216 
<a name="l00217"></a><a class="code" href="class_seldon_1_1_iteration.php#a69000baf75577546e3d78e8bcfe1576a">00217</a> 
<a name="l00219"></a>00219   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00220"></a>00220   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_iteration.php#a69000baf75577546e3d78e8bcfe1576a" title="Sets to a complete display (residual each iteration).">Iteration&lt;Titer&gt;::ShowFullHistory</a>()
<a name="l00221"></a>00221   {
<a name="l00222"></a>00222     <a class="code" href="class_seldon_1_1_iteration.php#a82adaef364a9a20a7525ab9299b6bfd5">print_level</a> = 6;
<a name="l00223"></a>00223   }
<a name="l00224"></a>00224 
<a name="l00225"></a><a class="code" href="class_seldon_1_1_iteration.php#a305d9d618a978830c3d2e36953c001c3">00225</a> 
<a name="l00227"></a>00227   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00228"></a>00228   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_iteration.php#a305d9d618a978830c3d2e36953c001c3" title="Doesn&amp;#39;t display any information.">Iteration&lt;Titer&gt;::HideMessages</a>()
<a name="l00229"></a>00229   {
<a name="l00230"></a>00230     <a class="code" href="class_seldon_1_1_iteration.php#a82adaef364a9a20a7525ab9299b6bfd5">print_level</a> = 0;
<a name="l00231"></a>00231   }
<a name="l00232"></a>00232 
<a name="l00233"></a><a class="code" href="class_seldon_1_1_iteration.php#a047b05d60ecc8b6f8f0789255472bfe3">00233</a> 
<a name="l00235"></a>00235   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> Vector1&gt;
<a name="l00236"></a>00236   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_iteration.php#a047b05d60ecc8b6f8f0789255472bfe3" title="Initialization with the right hand side.">Iteration&lt;Titer&gt;::Init</a>(<span class="keyword">const</span> Vector1&amp; r)
<a name="l00237"></a>00237   {
<a name="l00238"></a>00238     Titer norme_rhs = Titer(Norm2(r));
<a name="l00239"></a>00239     <span class="comment">// test of a null right hand side</span>
<a name="l00240"></a>00240     <span class="keywordflow">if</span> (norme_rhs == Titer(0))
<a name="l00241"></a>00241       <span class="keywordflow">return</span> -1;
<a name="l00242"></a>00242 
<a name="l00243"></a>00243     <span class="comment">// coefficient used later to compute relative residual</span>
<a name="l00244"></a>00244     <a class="code" href="class_seldon_1_1_iteration.php#adf29aa77b9dd1ecdc57c132712b02811" title="inverse of norm of first residual">facteur_reste</a> = Titer(1)/norme_rhs;
<a name="l00245"></a>00245 
<a name="l00246"></a>00246     <span class="comment">// initialization of iterations</span>
<a name="l00247"></a>00247     <a class="code" href="class_seldon_1_1_iteration.php#a4ff83b0b358c1574c6c0ababbbe04745" title="number of iterations">nb_iter</a> = 0;
<a name="l00248"></a>00248     <span class="keywordflow">return</span> 0; <span class="comment">// successful initialization</span>
<a name="l00249"></a>00249   }
<a name="l00250"></a>00250 
<a name="l00251"></a><a class="code" href="class_seldon_1_1_iteration.php#adaeada1a17ad527435684f5f8ad33f33">00251</a> 
<a name="l00253"></a>00253   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00254"></a>00254   <span class="keyword">inline</span> <span class="keywordtype">bool</span> <a class="code" href="class_seldon_1_1_iteration.php#adaeada1a17ad527435684f5f8ad33f33" title="Returns true if it is the first iteration.">Iteration&lt;Titer&gt;::First</a>()<span class="keyword"> const</span>
<a name="l00255"></a>00255 <span class="keyword">  </span>{
<a name="l00256"></a>00256     <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_iteration.php#a4ff83b0b358c1574c6c0ababbbe04745" title="number of iterations">nb_iter</a> == 0)
<a name="l00257"></a>00257       <span class="keywordflow">return</span> <span class="keyword">true</span>;
<a name="l00258"></a>00258 
<a name="l00259"></a>00259     <span class="keywordflow">return</span> <span class="keyword">false</span>;
<a name="l00260"></a>00260   }
<a name="l00261"></a>00261 
<a name="l00262"></a><a class="code" href="class_seldon_1_1_iteration.php#aa02c78783891fabcb4f7517f60f4912e">00262</a> 
<a name="l00264"></a>00264   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00265"></a>00265   <span class="keyword">inline</span> <span class="keywordtype">bool</span> <a class="code" href="class_seldon_1_1_iteration.php#aa02c78783891fabcb4f7517f60f4912e" title="Returns true if the initial guess is null.">Iteration&lt;Titer&gt;::IsInitGuess_Null</a>()<span class="keyword"> const</span>
<a name="l00266"></a>00266 <span class="keyword">  </span>{
<a name="l00267"></a>00267     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_iteration.php#a363306d1242c5a2b78737ace82ea465b" title="true if initial guess is null">init_guess_null</a>;
<a name="l00268"></a>00268   }
<a name="l00269"></a>00269 
<a name="l00270"></a>00270 
<a name="l00272"></a>00272   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> Vector1&gt;
<a name="l00273"></a>00273   <span class="keyword">inline</span> <span class="keywordtype">bool</span> <a class="code" href="class_seldon_1_1_iteration.php#a07c3b0af0412cfc7fe6fd3ed5476c4e0" title="Returns true if the iterative solver has reached its end.">Iteration&lt;Titer&gt;::</a>
<a name="l00274"></a>00274 <a class="code" href="class_seldon_1_1_iteration.php#a07c3b0af0412cfc7fe6fd3ed5476c4e0" title="Returns true if the iterative solver has reached its end.">  Finished</a>(<span class="keyword">const</span> Vector1&amp; r)<span class="keyword"> const</span>
<a name="l00275"></a>00275 <span class="keyword">  </span>{
<a name="l00276"></a>00276     <span class="comment">// absolute residual</span>
<a name="l00277"></a>00277     Titer reste = Norm2(r);
<a name="l00278"></a>00278     <span class="comment">// computation of relative residual</span>
<a name="l00279"></a>00279     reste = <a class="code" href="class_seldon_1_1_iteration.php#adf29aa77b9dd1ecdc57c132712b02811" title="inverse of norm of first residual">facteur_reste</a>*reste;
<a name="l00280"></a>00280 
<a name="l00281"></a>00281     <span class="comment">// displaying residual if required</span>
<a name="l00282"></a>00282     <span class="keywordflow">if</span> ((<a class="code" href="class_seldon_1_1_iteration.php#a82adaef364a9a20a7525ab9299b6bfd5">print_level</a> &gt;= 1)&amp;&amp;(<a class="code" href="class_seldon_1_1_iteration.php#a4ff83b0b358c1574c6c0ababbbe04745" title="number of iterations">nb_iter</a>%100 == 0))
<a name="l00283"></a>00283       cout&lt;&lt;<span class="stringliteral">&quot;Residu at iteration number &quot;</span>&lt;&lt;
<a name="l00284"></a>00284         <a class="code" href="class_seldon_1_1_iteration.php#ab426008d4bfc40842d86c8c6042a6594" title="Returns the number of iterations.">GetNumberIteration</a>()&lt;&lt;<span class="stringliteral">&quot;  &quot;</span>&lt;&lt;reste&lt;&lt;endl;
<a name="l00285"></a>00285     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_iteration.php#a82adaef364a9a20a7525ab9299b6bfd5">print_level</a> &gt;= 6)
<a name="l00286"></a>00286       cout&lt;&lt;<span class="stringliteral">&quot;Residu at iteration number &quot;</span>&lt;&lt;
<a name="l00287"></a>00287         <a class="code" href="class_seldon_1_1_iteration.php#ab426008d4bfc40842d86c8c6042a6594" title="Returns the number of iterations.">GetNumberIteration</a>()&lt;&lt;<span class="stringliteral">&quot;  &quot;</span>&lt;&lt;reste&lt;&lt;endl;
<a name="l00288"></a>00288 
<a name="l00289"></a>00289     <span class="comment">// end of iterative solver when residual is small enough</span>
<a name="l00290"></a>00290     <span class="comment">// or when the number of iterations is too high</span>
<a name="l00291"></a>00291     <span class="keywordflow">if</span> ((reste &lt; <a class="code" href="class_seldon_1_1_iteration.php#acf9550c0ec96e51fc5d395446342c4e8" title="stopping criterion">tolerance</a>)||(<a class="code" href="class_seldon_1_1_iteration.php#a4ff83b0b358c1574c6c0ababbbe04745" title="number of iterations">nb_iter</a> &gt;= <a class="code" href="class_seldon_1_1_iteration.php#a75359d82e80a14fa10907b203fc5a2ff" title="maximum number of iterations">max_iter</a>))
<a name="l00292"></a>00292       <span class="keywordflow">return</span> <span class="keyword">true</span>;
<a name="l00293"></a>00293 
<a name="l00294"></a>00294     <span class="keywordflow">return</span> <span class="keyword">false</span>;
<a name="l00295"></a>00295   }
<a name="l00296"></a>00296 
<a name="l00297"></a><a class="code" href="class_seldon_1_1_iteration.php#af217fa4792dee453e14b48f5f6cc30a4">00297</a> 
<a name="l00299"></a>00299   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00300"></a>00300   <span class="keyword">inline</span> <span class="keywordtype">bool</span> <a class="code" href="class_seldon_1_1_iteration.php#a07c3b0af0412cfc7fe6fd3ed5476c4e0" title="Returns true if the iterative solver has reached its end.">Iteration&lt;Titer&gt;::Finished</a>(<span class="keyword">const</span> Titer&amp; r)<span class="keyword"> const</span>
<a name="l00301"></a>00301 <span class="keyword">  </span>{
<a name="l00302"></a>00302     <span class="comment">// relative residual</span>
<a name="l00303"></a>00303     Titer reste = <a class="code" href="class_seldon_1_1_iteration.php#adf29aa77b9dd1ecdc57c132712b02811" title="inverse of norm of first residual">facteur_reste</a>*r;
<a name="l00304"></a>00304 
<a name="l00305"></a>00305     <span class="comment">// displaying residual if required</span>
<a name="l00306"></a>00306     <span class="keywordflow">if</span> ((<a class="code" href="class_seldon_1_1_iteration.php#a82adaef364a9a20a7525ab9299b6bfd5">print_level</a> &gt;= 1)&amp;&amp;(<a class="code" href="class_seldon_1_1_iteration.php#a4ff83b0b358c1574c6c0ababbbe04745" title="number of iterations">nb_iter</a>%100 == 0))
<a name="l00307"></a>00307       cout&lt;&lt;<span class="stringliteral">&quot;Residu at iteration number &quot;</span>&lt;&lt;
<a name="l00308"></a>00308         <a class="code" href="class_seldon_1_1_iteration.php#ab426008d4bfc40842d86c8c6042a6594" title="Returns the number of iterations.">GetNumberIteration</a>()&lt;&lt;<span class="stringliteral">&quot;  &quot;</span>&lt;&lt;reste&lt;&lt;endl;
<a name="l00309"></a>00309     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_iteration.php#a82adaef364a9a20a7525ab9299b6bfd5">print_level</a> &gt;= 6)
<a name="l00310"></a>00310       cout&lt;&lt;<span class="stringliteral">&quot;Residu at iteration number &quot;</span>&lt;&lt;
<a name="l00311"></a>00311         <a class="code" href="class_seldon_1_1_iteration.php#ab426008d4bfc40842d86c8c6042a6594" title="Returns the number of iterations.">GetNumberIteration</a>()&lt;&lt;<span class="stringliteral">&quot;  &quot;</span>&lt;&lt;reste&lt;&lt;endl;
<a name="l00312"></a>00312 
<a name="l00313"></a>00313     <span class="comment">// end of iterative solver when residual is small enough</span>
<a name="l00314"></a>00314     <span class="comment">// or when the number of iterations is too high</span>
<a name="l00315"></a>00315     <span class="keywordflow">if</span> ((reste &lt; <a class="code" href="class_seldon_1_1_iteration.php#acf9550c0ec96e51fc5d395446342c4e8" title="stopping criterion">tolerance</a>)||(<a class="code" href="class_seldon_1_1_iteration.php#a4ff83b0b358c1574c6c0ababbbe04745" title="number of iterations">nb_iter</a> &gt;= <a class="code" href="class_seldon_1_1_iteration.php#a75359d82e80a14fa10907b203fc5a2ff" title="maximum number of iterations">max_iter</a>))
<a name="l00316"></a>00316       <span class="keywordflow">return</span> <span class="keyword">true</span>;
<a name="l00317"></a>00317 
<a name="l00318"></a>00318     <span class="keywordflow">return</span> <span class="keyword">false</span>;
<a name="l00319"></a>00319   }
<a name="l00320"></a>00320 
<a name="l00321"></a><a class="code" href="class_seldon_1_1_iteration.php#a32dd5ec2e2b6216c50fbc1d0e27607a7">00321</a> 
<a name="l00323"></a>00323   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00324"></a>00324   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_iteration.php#a32dd5ec2e2b6216c50fbc1d0e27607a7" title="Informs of a failure in the iterative solver.">Iteration&lt;Titer&gt;::Fail</a>(<span class="keywordtype">int</span> i, <span class="keyword">const</span> <span class="keywordtype">string</span>&amp; s)
<a name="l00325"></a>00325   {
<a name="l00326"></a>00326     <a class="code" href="class_seldon_1_1_iteration.php#afea45227a5d730ac1464b3c33dabfc9e" title="true if the iterative solver has converged //! print level">fail_convergence</a> = <span class="keyword">true</span>;
<a name="l00327"></a>00327     <a class="code" href="class_seldon_1_1_iteration.php#a493984d8abaec3ac0d4f73c29d2a0e60" title="error code returned by iterative solver">error_code</a> = i;
<a name="l00328"></a>00328     <span class="comment">// displays information if required</span>
<a name="l00329"></a>00329     <span class="keywordflow">if</span> ((<a class="code" href="class_seldon_1_1_iteration.php#a82adaef364a9a20a7525ab9299b6bfd5">print_level</a> &gt;= 1)&amp;&amp;(<a class="code" href="class_seldon_1_1_iteration.php#a4ff83b0b358c1574c6c0ababbbe04745" title="number of iterations">nb_iter</a>%100==0))
<a name="l00330"></a>00330       cout&lt;&lt;<span class="stringliteral">&quot;Error during resolution : &quot;</span>&lt;&lt;s&lt;&lt;endl;
<a name="l00331"></a>00331   }
<a name="l00332"></a>00332 
<a name="l00333"></a><a class="code" href="class_seldon_1_1_iteration.php#a105cecfc4c5d1c1c27278eaa5807e32d">00333</a> 
<a name="l00335"></a>00335   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00336"></a>00336   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_iteration.php" title="Class containing parameters for an iterative resolution.">Iteration&lt;Titer&gt;</a>&amp; <a class="code" href="class_seldon_1_1_iteration.php#a105cecfc4c5d1c1c27278eaa5807e32d" title="Increment the number of iterations.">Iteration&lt;Titer&gt;::operator ++ </a>(<span class="keywordtype">void</span>)
<a name="l00337"></a>00337   {
<a name="l00338"></a>00338     ++<a class="code" href="class_seldon_1_1_iteration.php#a4ff83b0b358c1574c6c0ababbbe04745" title="number of iterations">nb_iter</a>;
<a name="l00339"></a>00339     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00340"></a>00340   }
<a name="l00341"></a>00341 
<a name="l00342"></a><a class="code" href="class_seldon_1_1_iteration.php#aa84736520dcca50ab5b8c72449ef8ed4">00342</a> 
<a name="l00344"></a>00344   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00345"></a>00345   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_iteration.php#aa84736520dcca50ab5b8c72449ef8ed4" title="Returns the error code (if an error occured).">Iteration&lt;Titer&gt;::ErrorCode</a>()<span class="keyword"> const</span>
<a name="l00346"></a>00346 <span class="keyword">  </span>{
<a name="l00347"></a>00347     <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_iteration.php#a4ff83b0b358c1574c6c0ababbbe04745" title="number of iterations">nb_iter</a> &gt;= <a class="code" href="class_seldon_1_1_iteration.php#a75359d82e80a14fa10907b203fc5a2ff" title="maximum number of iterations">max_iter</a>)
<a name="l00348"></a>00348       <span class="keywordflow">return</span> -2;
<a name="l00349"></a>00349 
<a name="l00350"></a>00350     <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_iteration.php#afea45227a5d730ac1464b3c33dabfc9e" title="true if the iterative solver has converged //! print level">fail_convergence</a>)
<a name="l00351"></a>00351       <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_iteration.php#a493984d8abaec3ac0d4f73c29d2a0e60" title="error code returned by iterative solver">error_code</a>;
<a name="l00352"></a>00352 
<a name="l00353"></a>00353     <span class="keywordflow">return</span> 0;
<a name="l00354"></a>00354   }
<a name="l00355"></a>00355 
<a name="l00356"></a>00356 } <span class="comment">// end namespace</span>
<a name="l00357"></a>00357 
<a name="l00358"></a>00358 <span class="preprocessor">#define SELDON_FILE_ITERATIVE_CXX</span>
<a name="l00359"></a>00359 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
<a name="l00360"></a>00360 <span class="preprocessor"></span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
