<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php">Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a> &#124;
<a href="#pro-static-attribs">Static Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;" --><!-- doxytag: inherits="PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;" --><div class="dynheader">
Inheritance diagram for Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.png" usemap="#Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;_map" alt=""/>
  <map id="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;_map" name="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;_map">
<area href="class_seldon_1_1_petsc_matrix.php" alt="Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;" shape="rect" coords="0,56,324,80"/>
<area href="class_seldon_1_1_matrix___base.php" alt="Seldon::Matrix_Base&lt; T, Allocator &gt;" shape="rect" coords="0,0,324,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab19afa27cb73e1cf105a67da8e235885"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::value_type" ref="ab19afa27cb73e1cf105a67da8e235885" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a62003a13e41e63a687b38642485785b1"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::pointer" ref="a62003a13e41e63a687b38642485785b1" args="" -->
typedef Allocator::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2ca028288a69b0c328245c7163a36139"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::const_pointer" ref="a2ca028288a69b0c328245c7163a36139" args="" -->
typedef Allocator::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af05b0aab1cfee05d317d3699c24eeab7"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::reference" ref="af05b0aab1cfee05d317d3699c24eeab7" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aabee0d27f2d0d5b2a56e31cedd37c8ba"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::const_reference" ref="aabee0d27f2d0d5b2a56e31cedd37c8ba" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4a16459af1b747a55a4a81a64fd552b9"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::entry_type" ref="a4a16459af1b747a55a4a81a64fd552b9" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>entry_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae4bacce889ca58bbef660db0190d65c7"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::access_type" ref="ae4bacce889ca58bbef660db0190d65c7" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>access_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1ac3d8d99f5c17e629d5b09539daac49"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::const_access_type" ref="a1ac3d8d99f5c17e629d5b09539daac49" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_access_type</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#a228557019fa6c0df6cb022dc79af77a2">Matrix</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#a228557019fa6c0df6cb022dc79af77a2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#a730afea2dd9473d44b883e5b9ca55cd8">Matrix</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Main constructor.  <a href="#a730afea2dd9473d44b883e5b9ca55cd8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#a87dac24bcb5ca5a5b845919620a23b97">Matrix</a> (Mat &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copy constructor.  <a href="#a87dac24bcb5ca5a5b845919620a23b97"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#a553cb7bfe9b6c465d85c4e2f79515f06">Matrix</a> (const <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">PETScMPIDense</a>, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copy constructor.  <a href="#a553cb7bfe9b6c465d85c4e2f79515f06"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#aaa6382ddfa0297d691e01634036ccdc3">Reallocate</a> (int i, int j, int local_i=PETSC_DECIDE, int local_j=PETSC_DECIDE)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates memory to resize the matrix.  <a href="#aaa6382ddfa0297d691e01634036ccdc3"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">value_type&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#ad0b79cf0b9c6a74045a3aa3c94f5e467">operator()</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#ad0b79cf0b9c6a74045a3aa3c94f5e467"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">value_type&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#a0852e34952627d2b2e2596002c5b2b85">operator()</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a0852e34952627d2b2e2596002c5b2b85"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#aa713b223ff87d400513f920934592bfa">Copy</a> (const <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">PETScMPIDense</a>, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a matrix.  <a href="#aa713b223ff87d400513f920934592bfa"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">PETScMPIDense</a>, <br class="typebreak"/>
Allocator &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#a73133bbeda0d426fbdb7b59edeb09e5c">operator=</a> (const <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">PETScMPIDense</a>, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a matrix (assignment operator).  <a href="#a73133bbeda0d426fbdb7b59edeb09e5c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#aada3ed094eefa207ce84db9f69e6b3f3">Print</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays the matrix on the standard output.  <a href="#aada3ed094eefa207ce84db9f69e6b3f3"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab0fa102ad451a796f30de474ee7c036e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::SetCommunicator" ref="ab0fa102ad451a796f30de474ee7c036e" args="(MPI_Comm mpi_communicator)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetCommunicator</b> (MPI_Comm mpi_communicator)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a290f3593cb516534a86c0b9f5c1646c8"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::GetCommunicator" ref="a290f3593cb516534a86c0b9f5c1646c8" args="() const" -->
MPI_Comm&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetCommunicator</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5344decb2cb632f0db5b2bb1204afcbe"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Clear" ref="a5344decb2cb632f0db5b2bb1204afcbe" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Clear</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aec50a5001f0de562e2e20b1c9e166e29"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Nullify" ref="aec50a5001f0de562e2e20b1c9e166e29" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Nullify</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0fa3793829d97bf29211e30ab72384bf"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::GetPetscMatrix" ref="a0fa3793829d97bf29211e30ab72384bf" args="()" -->
Mat &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetPetscMatrix</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a23c632e3a5c2e5710ac4429ca2fdc726"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::GetPetscMatrix" ref="a23c632e3a5c2e5710ac4429ca2fdc726" args="() const" -->
const Mat &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetPetscMatrix</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0cfea8ec472b6f1b44fb04eae709f8f8"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Resize" ref="a0cfea8ec472b6f1b44fb04eae709f8f8" args="(int i, int j)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Resize</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af9571d45be248575d07b866c490c1ba2"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::SetData" ref="af9571d45be248575d07b866c490c1ba2" args="(int i, int j, pointer data)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetData</b> (int i, int j, pointer data)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0353d3a236f3f6f3a688b5e422a5e568"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Val" ref="a0353d3a236f3f6f3a688b5e422a5e568" args="(int i, int j) const" -->
const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>Val</b> (int i, int j) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad780213ae6138a9a9532c02806985625"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Val" ref="ad780213ae6138a9a9532c02806985625" args="(int i, int j)" -->
reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>Val</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae4f22eb628e0e160dcf3789673748861"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::operator[]" ref="ae4f22eb628e0e160dcf3789673748861" args="(int i)" -->
reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>operator[]</b> (int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5ade50e9ecdaaa8854d22f16b800a265"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::operator[]" ref="a5ade50e9ecdaaa8854d22f16b800a265" args="(int i) const" -->
const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>operator[]</b> (int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2d1db5dbf312a68b3bc3c7c44e48b7e4"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Set" ref="a2d1db5dbf312a68b3bc3c7c44e48b7e4" args="(int, int, T)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Set</b> (int, int, T)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad3cc1f14b9ba6ac1b459ce448d6adfd9"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::SetBuffer" ref="ad3cc1f14b9ba6ac1b459ce448d6adfd9" args="(int, int, T, InsertMode)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetBuffer</b> (int, int, T, InsertMode)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2f608b718871d65dc96806c4f5867a60"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Flush" ref="a2f608b718871d65dc96806c4f5867a60" args="() const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Flush</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afb8978d1bd4774d18b4815ad454b62e9"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::GetProcessorRowRange" ref="afb8978d1bd4774d18b4815ad454b62e9" args="(int &amp;i, int &amp;j) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetProcessorRowRange</b> (int &amp;i, int &amp;j) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4b04405490e02061ddb68a4e33037106"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Copy" ref="a4b04405490e02061ddb68a4e33037106" args="(const Mat &amp;A)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Copy</b> (const Mat &amp;A)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a452a9f334888ccca27951ce53e4b2cf9"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Zero" ref="a452a9f334888ccca27951ce53e4b2cf9" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Zero</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a177616dc93ea00fc5a570cbc1cedf421"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::SetIdentity" ref="a177616dc93ea00fc5a570cbc1cedf421" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetIdentity</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3a3b27f67d957f4a7cce1708f8e1c8b8"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Fill" ref="a3a3b27f67d957f4a7cce1708f8e1c8b8" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Fill</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4b35bb1419e705d671180eb2afa31e02"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Fill" ref="a4b35bb1419e705d671180eb2afa31e02" args="(const T0 &amp;x)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Fill</b> (const T0 &amp;x)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7a6c8b901b09261c340d88500d7066e0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::FillRand" ref="a7a6c8b901b09261c340d88500d7066e0" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>FillRand</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4b5bb6ff820bb4b06605fc367ee9b12c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Print" ref="a4b5bb6ff820bb4b06605fc367ee9b12c" args="(int a, int b, int m, int n) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Print</b> (int a, int b, int m, int n) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adb57f345a20c0f85f5bb20b8341c49cb"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Print" ref="adb57f345a20c0f85f5bb20b8341c49cb" args="(int l) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Print</b> (int l) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a87f057e43012523ccd9f2087f688aa11"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Write" ref="a87f057e43012523ccd9f2087f688aa11" args="(string FileName, bool with_size) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Write</b> (string FileName, bool with_size) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a33e2b2d752293700486568ef8257fac5"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Write" ref="a33e2b2d752293700486568ef8257fac5" args="(ostream &amp;FileStream, bool with_size) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Write</b> (ostream &amp;FileStream, bool with_size) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a88ebd49e399e37027904bce13e7f63c4"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::WriteText" ref="a88ebd49e399e37027904bce13e7f63c4" args="(string FileName) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>WriteText</b> (string FileName) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab4b190fca03d36f2b5a3de31680be4c0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::WriteText" ref="ab4b190fca03d36f2b5a3de31680be4c0" args="(ostream &amp;FileStream) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>WriteText</b> (ostream &amp;FileStream) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2b5cb5cb3784d8754b5a2f3b73d256aa"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Read" ref="a2b5cb5cb3784d8754b5a2f3b73d256aa" args="(string FileName)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Read</b> (string FileName)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2c6835f0aaac0d2a61e834f32e5bc046"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Read" ref="a2c6835f0aaac0d2a61e834f32e5bc046" args="(istream &amp;FileStream)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Read</b> (istream &amp;FileStream)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a72c784f22fcba80ba6538f19bf29a9c1"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::ReadText" ref="a72c784f22fcba80ba6538f19bf29a9c1" args="(string FileName)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>ReadText</b> (string FileName)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad06f7c6ed48a9b52800a67b77d4d2fb4"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::ReadText" ref="ad06f7c6ed48a9b52800a67b77d4d2fb4" args="(istream &amp;FileStream)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>ReadText</b> (istream &amp;FileStream)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">GetM</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows.  <a href="#a65e9c3f0db9c7c8c3fa10ead777dd927"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows of the matrix possibly transposed.  <a href="#ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">GetN</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns.  <a href="#a6b134070d1b890ba6b7eb72fee170984"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns of the matrix possibly transposed.  <a href="#a7d27412bc9384a5ce0c0db1ee310d31c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">GetSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements in the matrix.  <a href="#a0fbc3f7030583174eaa69429f655a1b0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer to the data array.  <a href="#a453a269dfe7fadba249064363d5ab92a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a const pointer to the data array.  <a href="#a0f2796430deb08e565df8ced656097bf"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "void*" to the data array.  <a href="#a505cdfee34741c463e0dc1943337bedc"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "const void*" to the data array.  <a href="#a5979450cd8801810229f4a24c36e6639"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">Allocator &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the allocator of the matrix.  <a href="#af88748a55208349367d6860ad76fd691"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a472c2776d1ddbe98b586a6260ea9dcfc"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::petsc_matrix_" ref="a472c2776d1ddbe98b586a6260ea9dcfc" args="" -->
Mat&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_petsc_matrix.php#a472c2776d1ddbe98b586a6260ea9dcfc">petsc_matrix_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Encapsulated PETSc matrix. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a71d0b850860f9c228b6efc8ea83a2898"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::mpi_communicator_" ref="a71d0b850860f9c228b6efc8ea83a2898" args="" -->
MPI_Comm&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_petsc_matrix.php#a71d0b850860f9c228b6efc8ea83a2898">mpi_communicator_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">The MPI communicator to use. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a06e38862e1dd29739d952dc62ee40c9d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::petsc_matrix_deallocated_" ref="a06e38862e1dd29739d952dc62ee40c9d" args="" -->
bool&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_petsc_matrix.php#a06e38862e1dd29739d952dc62ee40c9d">petsc_matrix_deallocated_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Boolean to indicate if the inner PETSc matrix is destroyed or not. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a96b39ff494d4b7d3e30f320a1c7b4aba"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::m_" ref="a96b39ff494d4b7d3e30f320a1c7b4aba" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>m_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a11cb5d502050e5f14482ff0c9cef5a76"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::n_" ref="a11cb5d502050e5f14482ff0c9cef5a76" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>n_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acb12a8b74699fae7ae3b2b67376795a1"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::data_" ref="acb12a8b74699fae7ae3b2b67376795a1" args="" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-static-attribs"></a>
Static Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a132fa01ce120f352d434fd920e5ad9b4"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::allocator_" ref="a132fa01ce120f352d434fd920e5ad9b4" args="" -->
static Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>allocator_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Prop, class Allocator&gt;<br/>
 class Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;</h3>


<p>Definition at line <a class="el" href="_petsc_matrix_8hxx_source.php#l00152">152</a> of file <a class="el" href="_petsc_matrix_8hxx_source.php">PetscMatrix.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="a228557019fa6c0df6cb022dc79af77a2"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Matrix" ref="a228557019fa6c0df6cb022dc79af77a2" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">PETScMPIDense</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>On exit, the matrix is an empty 0x0 matrix. </p>

<p>Definition at line <a class="el" href="_petsc_matrix_8cxx_source.php#l00814">814</a> of file <a class="el" href="_petsc_matrix_8cxx_source.php">PetscMatrix.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a730afea2dd9473d44b883e5b9ca55cd8"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Matrix" ref="a730afea2dd9473d44b883e5b9ca55cd8" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">PETScMPIDense</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Main constructor. </p>
<p>Builds a i x j collection matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows of matrices. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns of matrices. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_matrix_8cxx_source.php#l00826">826</a> of file <a class="el" href="_petsc_matrix_8cxx_source.php">PetscMatrix.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a87dac24bcb5ca5a5b845919620a23b97"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Matrix" ref="a87dac24bcb5ca5a5b845919620a23b97" args="(Mat &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">PETScMPIDense</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramtype">Mat &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Copy constructor. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>A</em>&nbsp;</td><td>PETCs matrix to be copied. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_matrix_8cxx_source.php#l00839">839</a> of file <a class="el" href="_petsc_matrix_8cxx_source.php">PetscMatrix.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a553cb7bfe9b6c465d85c4e2f79515f06"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Matrix" ref="a553cb7bfe9b6c465d85c4e2f79515f06" args="(const Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">PETScMPIDense</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">PETScMPIDense</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Copy constructor. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>A</em>&nbsp;</td><td>matrix to be copied. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_matrix_8cxx_source.php#l00851">851</a> of file <a class="el" href="_petsc_matrix_8cxx_source.php">PetscMatrix.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="aa713b223ff87d400513f920934592bfa"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Copy" ref="aa713b223ff87d400513f920934592bfa" args="(const Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">PETScMPIDense</a>, Allocator &gt;::Copy </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">PETScMPIDense</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>A</em>&nbsp;</td><td>matrix to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'A' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_petsc_matrix_8cxx_source.php#l00957">957</a> of file <a class="el" href="_petsc_matrix_8cxx_source.php">PetscMatrix.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af88748a55208349367d6860ad76fd691"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::GetAllocator" ref="af88748a55208349367d6860ad76fd691" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">Allocator &amp; <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetAllocator </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the allocator of the matrix. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The allocator. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00258">258</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a453a269dfe7fadba249064363d5ab92a"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::GetData" ref="a453a269dfe7fadba249064363d5ab92a" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer to the data array. </p>
<p>Returns a pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00208">208</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0f2796430deb08e565df8ced656097bf"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::GetDataConst" ref="a0f2796430deb08e565df8ced656097bf" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::const_pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConst </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a const pointer to the data array. </p>
<p>Returns a const pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00221">221</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5979450cd8801810229f4a24c36e6639"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::GetDataConstVoid" ref="a5979450cd8801810229f4a24c36e6639" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConstVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "const void*" to the data array. </p>
<p>Returns a pointer of type "const void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00247">247</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a505cdfee34741c463e0dc1943337bedc"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::GetDataVoid" ref="a505cdfee34741c463e0dc1943337bedc" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "void*" to the data array. </p>
<p>Returns a pointer of type "void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00234">234</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a65e9c3f0db9c7c8c3fa10ead777dd927"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::GetM" ref="a65e9c3f0db9c7c8c3fa10ead777dd927" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a8c781e99310aae01786eac0e8e5aa50c">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00107">107</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::GetM" ref="ab6f6c5bba065ca82dad7c3abdbc8ea79" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#aecf3e8c636dbb83335ea588afe2558a8">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00130">130</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6b134070d1b890ba6b7eb72fee170984"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::GetN" ref="a6b134070d1b890ba6b7eb72fee170984" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a40580a92202044416e379c1304ff0220">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00118">118</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7d27412bc9384a5ce0c0db1ee310d31c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::GetN" ref="a7d27412bc9384a5ce0c0db1ee310d31c" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a867066f7bf531bf7ad15bdcd034dc3c0">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00145">145</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0fbc3f7030583174eaa69429f655a1b0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::GetSize" ref="a0fbc3f7030583174eaa69429f655a1b0" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements in the matrix. </p>
<p>Returns the number of elements in the matrix, i.e. the number of rows multiplied by the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements in the matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00195">195</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad0b79cf0b9c6a74045a3aa3c94f5e467"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::operator()" ref="ad0b79cf0b9c6a74045a3aa3c94f5e467" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">PETScMPIDense</a>, Allocator &gt;::value_type <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">PETScMPIDense</a>, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_petsc_matrix_8cxx_source.php#l00890">890</a> of file <a class="el" href="_petsc_matrix_8cxx_source.php">PetscMatrix.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0852e34952627d2b2e2596002c5b2b85"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::operator()" ref="a0852e34952627d2b2e2596002c5b2b85" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">PETScMPIDense</a>, Allocator &gt;::value_type <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">PETScMPIDense</a>, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_petsc_matrix_8cxx_source.php#l00924">924</a> of file <a class="el" href="_petsc_matrix_8cxx_source.php">PetscMatrix.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a73133bbeda0d426fbdb7b59edeb09e5c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::operator=" ref="a73133bbeda0d426fbdb7b59edeb09e5c" args="(const Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">PETScMPIDense</a>, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">PETScMPIDense</a>, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">PETScMPIDense</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a matrix (assignment operator). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>A</em>&nbsp;</td><td>matrix to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'A' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_petsc_matrix_8cxx_source.php#l00974">974</a> of file <a class="el" href="_petsc_matrix_8cxx_source.php">PetscMatrix.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aada3ed094eefa207ce84db9f69e6b3f3"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Print" ref="aada3ed094eefa207ce84db9f69e6b3f3" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">PETScMPIDense</a>, Allocator &gt;::Print </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Displays the matrix on the standard output. </p>
<p>Displays elements on the standard output, in text format. Each row is displayed on a single line and elements of a row are delimited by tabulations. </p>

<p>Definition at line <a class="el" href="_petsc_matrix_8cxx_source.php#l00988">988</a> of file <a class="el" href="_petsc_matrix_8cxx_source.php">PetscMatrix.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aaa6382ddfa0297d691e01634036ccdc3"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;::Reallocate" ref="aaa6382ddfa0297d691e01634036ccdc3" args="(int i, int j, int local_i=PETSC_DECIDE, int local_j=PETSC_DECIDE)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">PETScMPIDense</a>, Allocator &gt;::Reallocate </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>local_i</em> = <code>PETSC_DECIDE</code>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>local_j</em> = <code>PETSC_DECIDE</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates memory to resize the matrix. </p>
<p>On exit, the matrix is a i x i matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Depending on your allocator, data may be lost. </dd></dl>

<p>Definition at line <a class="el" href="_petsc_matrix_8cxx_source.php#l00867">867</a> of file <a class="el" href="_petsc_matrix_8cxx_source.php">PetscMatrix.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>matrix/<a class="el" href="_petsc_matrix_8hxx_source.php">PetscMatrix.hxx</a></li>
<li>matrix/<a class="el" href="_petsc_matrix_8cxx_source.php">PetscMatrix.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
