<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="tabs2">
    <ul class="tablist">
      <li class="current"><a href="functions.php"><span>All</span></a></li>
      <li><a href="functions_func.php"><span>Functions</span></a></li>
      <li><a href="functions_vars.php"><span>Variables</span></a></li>
      <li><a href="functions_type.php"><span>Typedefs</span></a></li>
    </ul>
  </div>
  <div class="tabs3">
    <ul class="tablist">
      <li><a href="functions.php#index_a"><span>a</span></a></li>
      <li><a href="functions_0x63.php#index_c"><span>c</span></a></li>
      <li><a href="functions_0x64.php#index_d"><span>d</span></a></li>
      <li><a href="functions_0x65.php#index_e"><span>e</span></a></li>
      <li><a href="functions_0x66.php#index_f"><span>f</span></a></li>
      <li><a href="functions_0x67.php#index_g"><span>g</span></a></li>
      <li><a href="functions_0x68.php#index_h"><span>h</span></a></li>
      <li><a href="functions_0x69.php#index_i"><span>i</span></a></li>
      <li><a href="functions_0x6c.php#index_l"><span>l</span></a></li>
      <li><a href="functions_0x6d.php#index_m"><span>m</span></a></li>
      <li><a href="functions_0x6e.php#index_n"><span>n</span></a></li>
      <li><a href="functions_0x6f.php#index_o"><span>o</span></a></li>
      <li><a href="functions_0x70.php#index_p"><span>p</span></a></li>
      <li><a href="functions_0x72.php#index_r"><span>r</span></a></li>
      <li class="current"><a href="functions_0x73.php#index_s"><span>s</span></a></li>
      <li><a href="functions_0x74.php#index_t"><span>t</span></a></li>
      <li><a href="functions_0x75.php#index_u"><span>u</span></a></li>
      <li><a href="functions_0x76.php#index_v"><span>v</span></a></li>
      <li><a href="functions_0x77.php#index_w"><span>w</span></a></li>
      <li><a href="functions_0x78.php#index_x"><span>x</span></a></li>
      <li><a href="functions_0x7a.php#index_z"><span>z</span></a></li>
      <li><a href="functions_0x7e.php#index_~"><span>~</span></a></li>
    </ul>
  </div>
<div class="contents">
Here is a list of all documented class members with links to the class documentation for each member:

<h3><a class="anchor" id="index_s"></a>- s -</h3><ul>
<li>Select()
: <a class="el" href="class_seldon_1_1_vector2.php#a2cf9e05eacb905603b0f0cfc8e7cdc5a">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
</li>
<li>SelectDirectSolver()
: <a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#ae580e7293920e4bde686f3dbe4ebb379">Seldon::SparseCholeskySolver&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_sparse_direct_solver.php#aee5b1ccbb25a3dfb21a6ea3ed00b037e">Seldon::SparseDirectSolver&lt; T &gt;</a>
</li>
<li>SelectOrdering()
: <a class="el" href="class_seldon_1_1_matrix_mumps.php#a752caecc1023fe5da2a71fc83ae8cbf9">Seldon::MatrixMumps&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#a12507e190d0320641997052c66039c93">Seldon::SparseCholeskySolver&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_sparse_direct_solver.php#adc649792b3da7574a6340563a9bc7378">Seldon::SparseDirectSolver&lt; T &gt;</a>
</li>
<li>Set()
: <a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a7f2f874c147e1ee426c29439bc6b6708">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a49ac0164990312cc9485abe5de3a1d77">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a7efa3a40752d1506b63504aaf65775b2">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___pointers.php#a2f4238595987c707ac784f437b5e04e3">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a567450d65dd57f4dc43838547fae50a2">Seldon::Matrix&lt; T, Prop, ArrayColSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#ac3340bdc4a5377e34c3e7e3b3ef5fb86">Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___symmetric.php#a66c6a02ed5d50cbf204f7bd145d13880">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ad297118a9c4023d96a871e6b95d78aa6">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sparse.php#a059694445d9f150c96fa35c53f05dbe8">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a0db0736203c7e574e0fcdd931f889c41">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ab4c3a8968e2c79248fc7d1add27f3433">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a04a627a376c55f6afd831018805ba9a7">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a600ae1e3aaa10e9e1d4732d69a027ae1">Seldon::Matrix_TriangPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triangular.php#ad81a65ada70f935d76a59e4f199222d9">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___hermitian.php#a7e89af78ff1d41c34b1ad69993817155">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2dfca2deb2a823fc64dc51b7c0728f49">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a6f67461ccf260c8bb9e9fd1fc40811e7">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>
</li>
<li>SetBuffer()
: <a class="el" href="class_seldon_1_1_petsc_matrix.php#ad3cc1f14b9ba6ac1b459ce448d6adfd9">Seldon::PetscMatrix&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#aa9c4710028825f7e8d4c4b38e28d77bc">Seldon::PETScVector&lt; T, Allocator &gt;</a>
</li>
<li>SetCommunicator()
: <a class="el" href="class_seldon_1_1_petsc_matrix.php#ab0fa102ad451a796f30de474ee7c036e">Seldon::PetscMatrix&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#ac6b7117d6550912d70c1b929be557f27">Seldon::PETScVector&lt; T, Allocator &gt;</a>
</li>
<li>SetCostFunctionTolerance()
: <a class="el" href="class_seldon_1_1_n_lopt_solver.php#a44ed740163ffdc82a1eda9a47001e58a">Seldon::NLoptSolver</a>
</li>
<li>SetData()
: <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a3729058de11ead808a09e98a10702d85">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sparse.php#a5b411ce16d320a1442588af33adefe09">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a3f202fe96b9289912e1cb6acfe24f18b">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#aeb3fdc796d3c31e4b4729d3e9e15834c">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a73034fa2fb67eec70a2d25684f2a0c8c">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ab12cedc7bee632a7b210312f1e7ef28b">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a91805dfa095ef095a620209201cdc252">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a50bafb538cfd1b8e6bb18bba62451dff">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sparse.php#acf57d5d2ab0730c882b331ce89d4768a">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a288b0c3b764e1db7eff81cb5e56e7c63">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#ad647993f653414c5a34b00052aab3c78">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_petsc_matrix.php#af9571d45be248575d07b866c490c1ba2">Seldon::PetscMatrix&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a9fb98c93d5bee7a4be62e1a3e1bb5a46">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>SetIdentity()
: <a class="el" href="class_seldon_1_1_matrix___hermitian.php#aa71e81ad230567c0624e278ae7e0a9cb">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a5e62a678cb672277c536d1ad9aad96f1">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___pointers.php#a35fc7bad6fe6cf252737916dfafae0ff">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___symmetric.php#a7cab27ba8e4ee1d46cb8d5eca3123d42">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a86973f10c1197e1a62c2df95049f5c69">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a888b8dc8faeb5654fda9a0517cea0c7b">Seldon::Matrix_TriangPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triangular.php#a17b39f20590f545d3dce529aeaab9d5f">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_petsc_matrix.php#a177616dc93ea00fc5a570cbc1cedf421">Seldon::PetscMatrix&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a19b48691b03953dffd387ace5cfddd37">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#af92baadfc393d70b6e3f3ecaa25d16c7">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a7ca605152313a38a14014b92fb1947a5">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sparse.php#a657c33784aee4b940c6a635c33f2eb5e">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a2116e2afc727a7c2ecf920fe41811c97">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#aa10b71d5e4eca83a9c61ad352d99a369">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>SetImagData()
: <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a8e3c2dfa0ea5aa57cd8607cca15562f2">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>SetLowerBound()
: <a class="el" href="class_seldon_1_1_n_lopt_solver.php#a6f92bbc59849ce8489da5210dfd6d994">Seldon::NLoptSolver</a>
</li>
<li>SetMatrix()
: <a class="el" href="class_seldon_1_1_matrix_collection.php#a42828d0fb580934560080b2cf15eaa78">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>SetMaxNumberIteration()
: <a class="el" href="class_seldon_1_1_iteration.php#afed55f067231a1defcf50acd0755aef1">Seldon::Iteration&lt; Titer &gt;</a>
</li>
<li>SetName()
: <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a60874f5b7285ceaaad8c2d8a9e99d7f0">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a9727114f35983dec2b486d65bf40644f">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
</li>
<li>SetNiterationMax()
: <a class="el" href="class_seldon_1_1_n_lopt_solver.php#ae8d18863be9cbbf79bc14e5fdc905a39">Seldon::NLoptSolver</a>
</li>
<li>SetNonSymmetricIlut()
: <a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a706d99d20b79c014b676cd91074860ee">Seldon::SparseDirectSolver&lt; T &gt;</a>
</li>
<li>SetNumberIteration()
: <a class="el" href="class_seldon_1_1_iteration.php#a24c69ffb62504d708354f8874a5f0f00">Seldon::Iteration&lt; Titer &gt;</a>
</li>
<li>SetNumberThreadPerNode()
: <a class="el" href="class_seldon_1_1_matrix_pastix.php#ac3ebb0a32a3aa30164f54dbaa75bff4a">Seldon::MatrixPastix&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_sparse_direct_solver.php#ad1d61abb463cc28f50e0acce15a41ce7">Seldon::SparseDirectSolver&lt; T &gt;</a>
</li>
<li>SetOrdering()
: <a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#aa08d8f448a3408e05f75ae0268b90739">Seldon::SparseCholeskySolver&lt; T &gt;</a>
</li>
<li>SetParameter()
: <a class="el" href="class_seldon_1_1_n_lopt_solver.php#adb63bff470ea8bbc8df7cc78207a597f">Seldon::NLoptSolver</a>
</li>
<li>SetParameterTolerance()
: <a class="el" href="class_seldon_1_1_n_lopt_solver.php#aa74cb3aca730ca6444fff8bd600f3be7">Seldon::NLoptSolver</a>
</li>
<li>SetPermutation()
: <a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a42cd77074d9e29b54946cc2e99826f71">Seldon::SparseDirectSolver&lt; T &gt;</a>
</li>
<li>SetRealData()
: <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aab6c31240cd6dcbd442f9a29658f51ea">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>SetRestart()
: <a class="el" href="class_seldon_1_1_iteration.php#a3a5874b5b76075698ceeb99944261311">Seldon::Iteration&lt; Titer &gt;</a>
</li>
<li>SetSolver()
: <a class="el" href="class_seldon_1_1_iteration.php#a7906239fb42957439f236258d4423465">Seldon::Iteration&lt; Titer &gt;</a>
</li>
<li>SetTolerance()
: <a class="el" href="class_seldon_1_1_iteration.php#a1393846a1b9090c4bb3516b28e70561b">Seldon::Iteration&lt; Titer &gt;</a>
</li>
<li>SetUpperBound()
: <a class="el" href="class_seldon_1_1_n_lopt_solver.php#a46fee6721c1a4172bae0ce10870d4442">Seldon::NLoptSolver</a>
</li>
<li>SetVector()
: <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a2718fc033e69dd6e16f88848d6b02ed5">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a8d14b3a597d38152461d2de34baa0156">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#adf98501c23f89625bb971e10e44b605f">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
</li>
<li>ShowFullHistory()
: <a class="el" href="class_seldon_1_1_iteration.php#a69000baf75577546e3d78e8bcfe1576a">Seldon::Iteration&lt; Titer &gt;</a>
, <a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#a24dc3be1fb9e8f479913b74f940384ed">Seldon::SparseCholeskySolver&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a70a07b6d4ada2aa3b63e71edb71675a6">Seldon::SparseDirectSolver&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_pastix.php#adc33ae8c7f93e468abd3ade499bea393">Seldon::MatrixPastix&lt; T &gt;</a>
</li>
<li>ShowMessages()
: <a class="el" href="class_seldon_1_1_sparse_direct_solver.php#ad4b15cbf1fc0faec2354e0beaa4678eb">Seldon::SparseDirectSolver&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_pastix.php#a0ca315759b1dd24114ff47958c37cb96">Seldon::MatrixPastix&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#af5322d1293240d5d3ab6aeb93183acc7">Seldon::MatrixSuperLU_Base&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_umf_pack___base.php#af5a60fc35fefccab49c6888cdb148c94">Seldon::MatrixUmfPack_Base&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#ab8b96c5cc5065e333d3b79c32cdebe92">Seldon::SparseCholeskySolver&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_mumps.php#a9375b89f5cbe3908db7120d22f033beb">Seldon::MatrixMumps&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_iteration.php#a18f2bdb38c22716d640422ead8998e1f">Seldon::Iteration&lt; Titer &gt;</a>
</li>
<li>Solve()
: <a class="el" href="class_seldon_1_1_sor_preconditioner.php#aba3a4a6c4b451382b4a850bde82a71a9">Seldon::SorPreconditioner&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_super_l_u_3_01complex_3_01double_01_4_01_4.php#a5a30b60114255c945211912168deea85">Seldon::MatrixSuperLU&lt; complex&lt; double &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a80c4aabe4215400d7e7c3d3964bf4545">Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_preconditioner___base.php#ae0cef5d8a4c53cc5a5025ccdc8712fad">Seldon::Preconditioner_Base</a>
, <a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#a4f4e2a42ec974a69b46a91d5f68003ad">Seldon::MatrixUmfPack&lt; double &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_pastix.php#a9f48e18656ea0d118f8e4d60bed06718">Seldon::MatrixPastix&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_super_l_u_3_01double_01_4.php#a983be1d0a4172a365493da4752757317">Seldon::MatrixSuperLU&lt; double &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_super_l_u_3_01complex_3_01double_01_4_01_4.php#a13625fddc750bb908d481b2c789344eb">Seldon::MatrixSuperLU&lt; complex&lt; double &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a278d2d4b4dd170ae821b7057385c2807">Seldon::SparseDirectSolver&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_pastix.php#a74f16146dfc8aa871028cfa6086a184a">Seldon::MatrixPastix&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_mumps.php#a5863d614bd5110fda7cc3d1dcc7c15f4">Seldon::MatrixMumps&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_super_l_u_3_01double_01_4.php#ab9f8ba83a82b29ee87e8ec8fd623c5bc">Seldon::MatrixSuperLU&lt; double &gt;</a>
, <a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a63f7c474a05a6034c08ab44af925f55b">Seldon::SparseDirectSolver&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_cholmod.php#accc98021ee559ca7e77c22d95acddd69">Seldon::MatrixCholmod</a>
, <a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a2e24772a5a8588f8fa826bc060fd8cbb">Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#a34cf4c23c1a2fbd266d3f0466956e171">Seldon::SparseCholeskySolver&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_mumps.php#ac2ed47d3a9f1f5c99e9a01a8c6f58083">Seldon::MatrixMumps&lt; T &gt;</a>
</li>
<li>SorPreconditioner()
: <a class="el" href="class_seldon_1_1_sor_preconditioner.php#ad1755eda6403d0cc33f8cc30ffc038ae">Seldon::SorPreconditioner&lt; T &gt;</a>
</li>
<li>SparseCholeskySolver()
: <a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#a9f1966f5fe27659ea3d479df51c43c81">Seldon::SparseCholeskySolver&lt; T &gt;</a>
</li>
<li>SparseDirectSolver()
: <a class="el" href="class_seldon_1_1_sparse_direct_solver.php#aef26a5c7fcbe65069bca7967c73f7ab5">Seldon::SparseDirectSolver&lt; T &gt;</a>
</li>
<li>stat
: <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a51a947858f30367b812df38f97ec3aa4">Seldon::MatrixSuperLU_Base&lt; T &gt;</a>
</li>
<li>status_facto
: <a class="el" href="class_seldon_1_1_matrix_umf_pack___base.php#a53020b345510801107c5119544865ea9">Seldon::MatrixUmfPack_Base&lt; T &gt;</a>
</li>
<li>Str()
: <a class="el" href="class_seldon_1_1_str.php#a965cff7019460030db60fc04a8de07ea">Seldon::Str</a>
</li>
<li>SubMatrix()
: <a class="el" href="class_seldon_1_1_sub_matrix.php#af3c69aa848cf1b11697ebf3ce81c4731">Seldon::SubMatrix&lt; M &gt;</a>
</li>
<li>SubMatrix_Base()
: <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a495411ec39b561f75a7fecbf589e4f2c">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>
</li>
<li>subvector_
: <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a0e24041339f74050c878a52404373649">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
</li>
<li>SwapColumn()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#ab9de78a2a24aa5fa8cec25f93232c4ff">Seldon::Matrix&lt; T, Prop, ArrayColSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#a9007597f6b3c40534bd422990e75da7f">Seldon::Matrix&lt; T, Prop, ArrayColSparse, Allocator &gt;</a>
</li>
<li>SwapImagColumn()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a7b4cf92d8be151c14508ada2b6b0b72d">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a77edc2353b0c31b171a3721d9c514dac">Seldon::Matrix&lt; T, Prop, ArrayColComplexSparse, Allocator &gt;</a>
</li>
<li>SwapImagRow()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a4b6b31fa7468c32bddf17fd1a22888ae">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#abd914e374842533215ebeb12ae0fd5e6">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>
</li>
<li>SwapRealColumn()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#af2f1c1dc9a20bfed30e186f8d4c7dc44">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a88a77017d1aedf2ecc8c371e475f0d15">Seldon::Matrix&lt; T, Prop, ArrayColComplexSparse, Allocator &gt;</a>
</li>
<li>SwapRealRow()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a8ae984b95031fd72c6ecab86418d60ac">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#adf10d81ed34502289b1d928626921d91">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>
</li>
<li>SwapRow()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#abb74a0467a0295eb8964ac30c9e4922c">Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a6a476c1d342322cb6b0d16d19de837dc">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a>
</li>
<li>symmetric_algorithm
: <a class="el" href="class_seldon_1_1_ilut_preconditioning.php#a87e3208fe6fed093679b21210726be46">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a>
</li>
<li>symmetric_matrix
: <a class="el" href="class_seldon_1_1_sparse_seldon_solver.php#abe4e7e238d8d5e74265a70f46e23c88b">Seldon::SparseSeldonSolver&lt; T, Allocator &gt;</a>
</li>
<li>symmetric_precond
: <a class="el" href="class_seldon_1_1_sor_preconditioner.php#a4e58db67067834701f1080f81fb2442d">Seldon::SorPreconditioner&lt; T &gt;</a>
</li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
