<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><b>seldon</b>      </li>
      <li><a class="el" href="classseldon_1_1ifstream.php">ifstream</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pub-attribs">Public Attributes</a> &#124;
<a href="#pub-static-attribs">Static Public Attributes</a>  </div>
  <div class="headertitle">
<h1>seldon::ifstream Class Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="seldon::ifstream" --><!-- doxytag: inherits="seldon::istream" --><div class="dynheader">
Inheritance diagram for seldon::ifstream:</div>
<div class="dyncontent">
 <div class="center">
  <img src="classseldon_1_1ifstream.png" usemap="#seldon::ifstream_map" alt=""/>
  <map id="seldon::ifstream_map" name="seldon::ifstream_map">
<area href="classseldon_1_1istream.php" alt="seldon::istream" shape="rect" coords="0,168,105,192"/>
<area href="classseldon_1_1ios.php" alt="seldon::ios" shape="rect" coords="0,112,105,136"/>
<area href="classseldon_1_1ios__base.php" alt="seldon::ios_base" shape="rect" coords="0,56,105,80"/>
<area href="classseldon_1_1__object.php" alt="seldon::_object" shape="rect" coords="0,0,105,24"/>
</map>
</div>

<p><a href="classseldon_1_1ifstream-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a040ce5ad19d2334b00442d79e4e4666a"></a><!-- doxytag: member="seldon::ifstream::__init__" ref="a040ce5ad19d2334b00442d79e4e4666a" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__init__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad53e86b19a904f45d7d853bdb6cff3aa"></a><!-- doxytag: member="seldon::ifstream::is_open" ref="ad53e86b19a904f45d7d853bdb6cff3aa" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>is_open</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7e5194c5555ef2eca0c9ed807225d3d5"></a><!-- doxytag: member="seldon::ifstream::close" ref="a7e5194c5555ef2eca0c9ed807225d3d5" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>close</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a21a78e39d5277faea40b5625d9c094d5"></a><!-- doxytag: member="seldon::ifstream::__init__" ref="a21a78e39d5277faea40b5625d9c094d5" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__init__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5e7f092ba390aa54c41b12d256e41171"></a><!-- doxytag: member="seldon::ifstream::__rshift__" ref="a5e7f092ba390aa54c41b12d256e41171" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__rshift__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae50925f55abde3c919bd63858ac4ea3f"></a><!-- doxytag: member="seldon::ifstream::gcount" ref="ae50925f55abde3c919bd63858ac4ea3f" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>gcount</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aee303ebd3cc61ffefec6b4252333ae3f"></a><!-- doxytag: member="seldon::ifstream::get" ref="aee303ebd3cc61ffefec6b4252333ae3f" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>get</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae36e7d0ba82f9a1393382d78577ca08c"></a><!-- doxytag: member="seldon::ifstream::getline" ref="ae36e7d0ba82f9a1393382d78577ca08c" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>getline</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6c67d5586cde875ba8dc7df7c0ab4bbb"></a><!-- doxytag: member="seldon::ifstream::ignore" ref="a6c67d5586cde875ba8dc7df7c0ab4bbb" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>ignore</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6d052e408b7c5ca48f03708b1f3ca66e"></a><!-- doxytag: member="seldon::ifstream::peek" ref="a6d052e408b7c5ca48f03708b1f3ca66e" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>peek</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aeb452218824a936e28adef95c9eb2b87"></a><!-- doxytag: member="seldon::ifstream::read" ref="aeb452218824a936e28adef95c9eb2b87" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>read</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a89986ce894df98133b95511a4d8dc833"></a><!-- doxytag: member="seldon::ifstream::readsome" ref="a89986ce894df98133b95511a4d8dc833" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>readsome</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa08c00bcc12b8cfe2850480fce846b1d"></a><!-- doxytag: member="seldon::ifstream::putback" ref="aa08c00bcc12b8cfe2850480fce846b1d" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>putback</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afcecb7ccedbad6dfec3f53e0ebbff669"></a><!-- doxytag: member="seldon::ifstream::unget" ref="afcecb7ccedbad6dfec3f53e0ebbff669" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>unget</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a432a1f71b32703b675b179d115433372"></a><!-- doxytag: member="seldon::ifstream::sync" ref="a432a1f71b32703b675b179d115433372" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>sync</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5d05fa5550cc0b4238925f968be32272"></a><!-- doxytag: member="seldon::ifstream::tellg" ref="a5d05fa5550cc0b4238925f968be32272" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>tellg</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7628b5372e00757830376da4ce5a9a4d"></a><!-- doxytag: member="seldon::ifstream::seekg" ref="a7628b5372e00757830376da4ce5a9a4d" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>seekg</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a678a94928eb876b50ad1287078172915"></a><!-- doxytag: member="seldon::ifstream::rdstate" ref="a678a94928eb876b50ad1287078172915" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>rdstate</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aec6d7d998c176e6298fb8b7cf46e736b"></a><!-- doxytag: member="seldon::ifstream::clear" ref="aec6d7d998c176e6298fb8b7cf46e736b" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>clear</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7c5dac98f5bb493c6f57da1ccbe6ff3f"></a><!-- doxytag: member="seldon::ifstream::setstate" ref="a7c5dac98f5bb493c6f57da1ccbe6ff3f" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>setstate</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3e63517a623b9429d69c5ed2eff2b3f8"></a><!-- doxytag: member="seldon::ifstream::good" ref="a3e63517a623b9429d69c5ed2eff2b3f8" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>good</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a92c326bb5bc190c93c13edc97f14f22a"></a><!-- doxytag: member="seldon::ifstream::eof" ref="a92c326bb5bc190c93c13edc97f14f22a" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>eof</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9f5c4aae249124e87ed9c5fe380ecdf6"></a><!-- doxytag: member="seldon::ifstream::fail" ref="a9f5c4aae249124e87ed9c5fe380ecdf6" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>fail</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2e362b04abfdc51772b3d8f231a8aaf7"></a><!-- doxytag: member="seldon::ifstream::bad" ref="a2e362b04abfdc51772b3d8f231a8aaf7" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>bad</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aff1e82ffa19bc755537af9d2cbf78d19"></a><!-- doxytag: member="seldon::ifstream::exceptions" ref="aff1e82ffa19bc755537af9d2cbf78d19" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>exceptions</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a15ba001738553b6cf4106b2ecc95b6c1"></a><!-- doxytag: member="seldon::ifstream::tie" ref="a15ba001738553b6cf4106b2ecc95b6c1" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>tie</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac249cb29c5966ee8c72cceba6b72e7f7"></a><!-- doxytag: member="seldon::ifstream::rdbuf" ref="ac249cb29c5966ee8c72cceba6b72e7f7" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>rdbuf</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a01e722f8d653f520345072974fbd58d7"></a><!-- doxytag: member="seldon::ifstream::copyfmt" ref="a01e722f8d653f520345072974fbd58d7" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>copyfmt</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acf8d3506ec9f1f50f12f0c4b32ae454e"></a><!-- doxytag: member="seldon::ifstream::fill" ref="acf8d3506ec9f1f50f12f0c4b32ae454e" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>fill</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a030d88f05d5b5f4df35bceebaab463cd"></a><!-- doxytag: member="seldon::ifstream::imbue" ref="a030d88f05d5b5f4df35bceebaab463cd" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>imbue</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a28b4f6e187362360e57b2f3661a9a14a"></a><!-- doxytag: member="seldon::ifstream::narrow" ref="a28b4f6e187362360e57b2f3661a9a14a" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>narrow</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0475ad29ab83371c1c8eeb8f8421e7f5"></a><!-- doxytag: member="seldon::ifstream::widen" ref="a0475ad29ab83371c1c8eeb8f8421e7f5" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>widen</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a79c824472f079470603496be66a4a228"></a><!-- doxytag: member="seldon::ifstream::register_callback" ref="a79c824472f079470603496be66a4a228" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>register_callback</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="abe2d97bc10dbebbef7f448f4ac3ae661"></a><!-- doxytag: member="seldon::ifstream::flags" ref="abe2d97bc10dbebbef7f448f4ac3ae661" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>flags</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afad76f0141c49ed816f2592374bedddd"></a><!-- doxytag: member="seldon::ifstream::setf" ref="afad76f0141c49ed816f2592374bedddd" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>setf</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4102412d5a3824e0f9b57389805d93a9"></a><!-- doxytag: member="seldon::ifstream::unsetf" ref="a4102412d5a3824e0f9b57389805d93a9" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>unsetf</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae029ba7a4fdc1aac0b86362887b9e022"></a><!-- doxytag: member="seldon::ifstream::precision" ref="ae029ba7a4fdc1aac0b86362887b9e022" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>precision</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae6eb750f835d233eed128bd75a22282a"></a><!-- doxytag: member="seldon::ifstream::width" ref="ae6eb750f835d233eed128bd75a22282a" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>width</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af38faa6bb107e35e373977312d9585e5"></a><!-- doxytag: member="seldon::ifstream::getloc" ref="af38faa6bb107e35e373977312d9585e5" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>getloc</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afa1e3d0c3d344006af1a10f5e6762022"></a><!-- doxytag: member="seldon::ifstream::iword" ref="afa1e3d0c3d344006af1a10f5e6762022" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>iword</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7cbef9a3b6a2c82c4451c9eaf59a5d3a"></a><!-- doxytag: member="seldon::ifstream::pword" ref="a7cbef9a3b6a2c82c4451c9eaf59a5d3a" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>pword</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-attribs"></a>
Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a29343639ba12d077d48337fcd9ee259d"></a><!-- doxytag: member="seldon::ifstream::this" ref="a29343639ba12d077d48337fcd9ee259d" args="" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><b>this</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-static-attribs"></a>
Static Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a673fe40b5a7bc9162a174bdf75f50375"></a><!-- doxytag: member="seldon::ifstream::erase_event" ref="a673fe40b5a7bc9162a174bdf75f50375" args="" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><b>erase_event</b> = _seldon.ios_base_erase_event</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac14f2ad710a3c7defae6badafb5b434b"></a><!-- doxytag: member="seldon::ifstream::imbue_event" ref="ac14f2ad710a3c7defae6badafb5b434b" args="" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><b>imbue_event</b> = _seldon.ios_base_imbue_event</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2623bdfc3f6270db8f57cc3e82f946ff"></a><!-- doxytag: member="seldon::ifstream::copyfmt_event" ref="a2623bdfc3f6270db8f57cc3e82f946ff" args="" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><b>copyfmt_event</b> = _seldon.ios_base_copyfmt_event</td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>

<p>Definition at line <a class="el" href="seldon_8py_source.php#l00353">353</a> of file <a class="el" href="seldon_8py_source.php">seldon.py</a>.</p>
<hr/>The documentation for this class was generated from the following file:<ul>
<li><a class="el" href="seldon_8py_source.php">seldon.py</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
