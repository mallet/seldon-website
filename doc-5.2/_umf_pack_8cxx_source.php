<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/interfaces/direct/UmfPack.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_UMFPACK_CXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 <span class="preprocessor">#include &quot;UmfPack.hxx&quot;</span>
<a name="l00023"></a>00023 
<a name="l00024"></a>00024 <span class="keyword">namespace </span>Seldon
<a name="l00025"></a>00025 {
<a name="l00026"></a>00026 
<a name="l00028"></a>00028   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00029"></a>00029   <a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a50e9165bfdf52428e6f405d959f9f863" title="constructor">MatrixUmfPack_Base&lt;T&gt;::MatrixUmfPack_Base</a>()
<a name="l00030"></a>00030   {
<a name="l00031"></a>00031     Symbolic = NULL;
<a name="l00032"></a>00032     Numeric = NULL;
<a name="l00033"></a>00033     <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a3a2831b9dac4fa42c7815015627f5f38" title="number of rows">n</a> = 0;
<a name="l00034"></a>00034 
<a name="l00035"></a>00035     <span class="comment">// allocation of arrays Control and Info</span>
<a name="l00036"></a>00036     Control.Reallocate(UMFPACK_CONTROL);
<a name="l00037"></a>00037     Info.Reallocate(UMFPACK_INFO);
<a name="l00038"></a>00038 
<a name="l00039"></a>00039     print_level = -1;
<a name="l00040"></a>00040     transpose = <span class="keyword">false</span>;
<a name="l00041"></a>00041     status_facto = 0;
<a name="l00042"></a>00042   }
<a name="l00043"></a>00043 
<a name="l00044"></a>00044 
<a name="l00046"></a>00046   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00047"></a>00047   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#ad88543986d6cf6273c9f56654820b2f1" title="no message will be displayed by UmfPack">MatrixUmfPack_Base&lt;T&gt;::HideMessages</a>()
<a name="l00048"></a>00048   {
<a name="l00049"></a>00049     print_level = -1;
<a name="l00050"></a>00050     Control(UMFPACK_PRL) = 0;
<a name="l00051"></a>00051   }
<a name="l00052"></a>00052 
<a name="l00053"></a>00053 
<a name="l00055"></a>00055   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00056"></a>00056   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#af5a60fc35fefccab49c6888cdb148c94" title="normal amount of message displayed by UmfPack">MatrixUmfPack_Base&lt;T&gt;::ShowMessages</a>()
<a name="l00057"></a>00057   {
<a name="l00058"></a>00058     print_level = 1;
<a name="l00059"></a>00059     Control(UMFPACK_PRL) = 2;
<a name="l00060"></a>00060   }
<a name="l00061"></a>00061 
<a name="l00062"></a>00062 
<a name="l00063"></a>00063   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00064"></a>00064   <span class="keywordtype">void</span> MatrixUmfPack_Base&lt;T&gt;::ShowFullHistory()
<a name="l00065"></a>00065   {
<a name="l00066"></a>00066     print_level = 2;
<a name="l00067"></a>00067   }
<a name="l00068"></a>00068 
<a name="l00069"></a>00069 
<a name="l00070"></a>00070   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00071"></a>00071   <span class="keywordtype">int</span> MatrixUmfPack_Base&lt;T&gt;::GetInfoFactorization()<span class="keyword"> const</span>
<a name="l00072"></a>00072 <span class="keyword">  </span>{
<a name="l00073"></a>00073     <span class="keywordflow">return</span> status_facto;
<a name="l00074"></a>00074   }
<a name="l00075"></a>00075 
<a name="l00076"></a>00076 
<a name="l00077"></a>00077   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00078"></a>00078   <span class="keywordtype">void</span> MatrixUmfPack_Base&lt;T&gt;::SelectOrdering(<span class="keywordtype">int</span> type)
<a name="l00079"></a>00079   {
<a name="l00080"></a>00080     Control(UMFPACK_ORDERING) = type;
<a name="l00081"></a>00081   }
<a name="l00082"></a>00082 
<a name="l00083"></a>00083 
<a name="l00084"></a>00084   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00085"></a>00085   <span class="keywordtype">void</span> MatrixUmfPack_Base&lt;T&gt;::SetPermutation(<span class="keyword">const</span> IVect&amp; permut)
<a name="l00086"></a>00086   {
<a name="l00087"></a>00087     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;MatrixUmfPack_Base::SetPermutation(const Vector&amp;)&quot;</span>);
<a name="l00088"></a>00088   }
<a name="l00089"></a>00089 
<a name="l00090"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#a70dd6831eed0355caedc69e8d3c99920">00090</a> 
<a name="l00092"></a>00092   <a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack&lt;double&gt;::MatrixUmfPack</a>() : <a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php" title="&amp;lt; base class to solve linear system by using UmfPack">MatrixUmfPack_Base</a>&lt;double&gt;()
<a name="l00093"></a>00093   {
<a name="l00094"></a>00094     ptr_ = NULL;
<a name="l00095"></a>00095     ind_ = NULL;
<a name="l00096"></a>00096     data_ = NULL;
<a name="l00097"></a>00097     umfpack_di_defaults(this-&gt;Control.GetData());
<a name="l00098"></a>00098     this-&gt;<a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#ad88543986d6cf6273c9f56654820b2f1" title="no message will be displayed by UmfPack">HideMessages</a>();
<a name="l00099"></a>00099   }
<a name="l00100"></a>00100 
<a name="l00101"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a83049b296e185003b45f3d02d664adb2">00101</a> 
<a name="l00103"></a>00103   <a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack&lt;complex&lt;double&gt;</a> &gt;<a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">::MatrixUmfPack</a>()
<a name="l00104"></a>00104     : <a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php" title="&amp;lt; base class to solve linear system by using UmfPack">MatrixUmfPack_Base</a>&lt;complex&lt;double&gt; &gt;()
<a name="l00105"></a>00105   {
<a name="l00106"></a>00106     ptr_ = NULL;
<a name="l00107"></a>00107     ind_ = NULL;
<a name="l00108"></a>00108     data_real_ = NULL;
<a name="l00109"></a>00109     data_imag_ = NULL;
<a name="l00110"></a>00110     umfpack_zi_defaults(this-&gt;Control.GetData());
<a name="l00111"></a>00111     this-&gt;<a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#ad88543986d6cf6273c9f56654820b2f1" title="no message will be displayed by UmfPack">HideMessages</a>();
<a name="l00112"></a>00112   }
<a name="l00113"></a>00113 
<a name="l00114"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#aa660d4563cf769365f1121761f7db67f">00114</a> 
<a name="l00116"></a>00116   <a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack&lt;double&gt;::~MatrixUmfPack</a>()
<a name="l00117"></a>00117   {
<a name="l00118"></a>00118     Clear();
<a name="l00119"></a>00119   }
<a name="l00120"></a>00120 
<a name="l00121"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#a24db3569fa50301554e9ca73ebd167e3">00121</a> 
<a name="l00123"></a>00123   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack&lt;double&gt;::Clear</a>()
<a name="l00124"></a>00124   {
<a name="l00125"></a>00125     <span class="keywordflow">if</span> (this-&gt;<a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#aa442e0754359877423d73f857aa75731" title="number of rows in the matrix">n</a> &gt; 0)
<a name="l00126"></a>00126       {
<a name="l00127"></a>00127         <span class="comment">// memory used for matrix is released</span>
<a name="l00128"></a>00128         free(ptr_);
<a name="l00129"></a>00129         free(ind_);
<a name="l00130"></a>00130         free(data_);
<a name="l00131"></a>00131 
<a name="l00132"></a>00132         <span class="comment">// memory for numbering scheme is released</span>
<a name="l00133"></a>00133         umfpack_di_free_symbolic(&amp;this-&gt;Symbolic) ;
<a name="l00134"></a>00134 
<a name="l00135"></a>00135         <span class="comment">// memory used by LU factorization is released</span>
<a name="l00136"></a>00136         umfpack_di_free_numeric(&amp;this-&gt;<a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a7a91b3ba9d23982fc5b75ebf8f4d2eea" title="pointers of UmfPack objects">Numeric</a>) ;
<a name="l00137"></a>00137 
<a name="l00138"></a>00138         this-&gt;<a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#aa442e0754359877423d73f857aa75731" title="number of rows in the matrix">n</a> = 0;
<a name="l00139"></a>00139         this-&gt;Symbolic = NULL;
<a name="l00140"></a>00140         this-&gt;<a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a7a91b3ba9d23982fc5b75ebf8f4d2eea" title="pointers of UmfPack objects">Numeric</a> = NULL;
<a name="l00141"></a>00141         ptr_ = NULL;
<a name="l00142"></a>00142         ind_ = NULL;
<a name="l00143"></a>00143         data_ = NULL;
<a name="l00144"></a>00144       }
<a name="l00145"></a>00145   }
<a name="l00146"></a>00146 
<a name="l00147"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a2cc80beb8d893f7b1b8e68872de1290a">00147</a> 
<a name="l00149"></a>00149   <a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack&lt;complex&lt;double&gt;</a> &gt;::~MatrixUmfPack()
<a name="l00150"></a>00150   {
<a name="l00151"></a>00151     Clear();
<a name="l00152"></a>00152   }
<a name="l00153"></a>00153 
<a name="l00154"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a490fccbcfd57c7e3ad07c8ea3739afab">00154</a> 
<a name="l00156"></a>00156   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack&lt;complex&lt;double&gt;</a> &gt;::Clear()
<a name="l00157"></a>00157   {
<a name="l00158"></a>00158     <span class="keywordflow">if</span> (this-&gt;n &gt; 0)
<a name="l00159"></a>00159       {
<a name="l00160"></a>00160         <span class="comment">// memory used for matrix is released</span>
<a name="l00161"></a>00161         free(ptr_);
<a name="l00162"></a>00162         free(ind_);
<a name="l00163"></a>00163         free(data_real_);
<a name="l00164"></a>00164         free(data_imag_);
<a name="l00165"></a>00165 
<a name="l00166"></a>00166         <span class="comment">// memory for numbering scheme is released</span>
<a name="l00167"></a>00167         umfpack_zi_free_symbolic(&amp;this-&gt;Symbolic) ;
<a name="l00168"></a>00168 
<a name="l00169"></a>00169         <span class="comment">// memory used by LU factorization is released</span>
<a name="l00170"></a>00170         umfpack_zi_free_numeric(&amp;this-&gt;Numeric) ;
<a name="l00171"></a>00171 
<a name="l00172"></a>00172         this-&gt;n = 0;
<a name="l00173"></a>00173         this-&gt;Symbolic = NULL;
<a name="l00174"></a>00174         this-&gt;Numeric = NULL;
<a name="l00175"></a>00175 
<a name="l00176"></a>00176         ptr_ = NULL;
<a name="l00177"></a>00177         ind_ = NULL;
<a name="l00178"></a>00178         data_real_ = NULL;
<a name="l00179"></a>00179         data_imag_ = NULL;
<a name="l00180"></a>00180       }
<a name="l00181"></a>00181   }
<a name="l00182"></a>00182 
<a name="l00183"></a>00183 
<a name="l00185"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#aa2dd343b5e23a0d46704531f201e1f24">00185</a>   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00186"></a>00186   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack&lt;double&gt;::</a>
<a name="l00187"></a>00187 <a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">  FactorizeMatrix</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, Prop, Storage, Allocator&gt;</a> &amp; mat,
<a name="l00188"></a>00188                   <span class="keywordtype">bool</span> keep_matrix)
<a name="l00189"></a>00189   {
<a name="l00190"></a>00190     <span class="comment">// we clear previous factorization</span>
<a name="l00191"></a>00191     Clear();
<a name="l00192"></a>00192 
<a name="l00193"></a>00193     <span class="comment">// conversion to unsymmetric matrix in Column Sparse Column Format</span>
<a name="l00194"></a>00194     <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, General, ColSparse, MallocAlloc&lt;double&gt;</a> &gt; Acsc;
<a name="l00195"></a>00195     transpose = <span class="keyword">false</span>;
<a name="l00196"></a>00196 
<a name="l00197"></a>00197     this-&gt;<a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#aa442e0754359877423d73f857aa75731" title="number of rows in the matrix">n</a> = mat.GetM();
<a name="l00198"></a>00198     Copy(mat, Acsc);
<a name="l00199"></a>00199     <span class="keywordflow">if</span> (!keep_matrix)
<a name="l00200"></a>00200       mat.Clear();
<a name="l00201"></a>00201 
<a name="l00202"></a>00202     <span class="comment">// we retrieve pointers of Acsc and nullify this object</span>
<a name="l00203"></a>00203     ptr_ = Acsc.GetPtr();
<a name="l00204"></a>00204     ind_ = Acsc.GetInd();
<a name="l00205"></a>00205     data_ = Acsc.GetData();
<a name="l00206"></a>00206     Acsc.Nullify();
<a name="l00207"></a>00207 
<a name="l00208"></a>00208     <span class="comment">// factorization with UmfPack</span>
<a name="l00209"></a>00209     umfpack_di_symbolic(this-&gt;<a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#aa442e0754359877423d73f857aa75731" title="number of rows in the matrix">n</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#aa442e0754359877423d73f857aa75731" title="number of rows in the matrix">n</a>, ptr_, ind_, data_, &amp;this-&gt;Symbolic,
<a name="l00210"></a>00210                         this-&gt;Control.GetData(), this-&gt;<a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a4a6b504d370b37ef4951626d2a3ce439" title="parameters for UmfPack">Info</a>.GetData());
<a name="l00211"></a>00211 
<a name="l00212"></a>00212     <span class="comment">// we display informations about the performed operation</span>
<a name="l00213"></a>00213     <a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a53020b345510801107c5119544865ea9" title="transpose system to solve ?">status_facto</a> =
<a name="l00214"></a>00214       umfpack_di_numeric(ptr_, ind_, data_,
<a name="l00215"></a>00215                          this-&gt;Symbolic, &amp;this-&gt;<a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a7a91b3ba9d23982fc5b75ebf8f4d2eea" title="pointers of UmfPack objects">Numeric</a>,
<a name="l00216"></a>00216                          this-&gt;Control.GetData(), this-&gt;<a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a4a6b504d370b37ef4951626d2a3ce439" title="parameters for UmfPack">Info</a>.GetData());
<a name="l00217"></a>00217 
<a name="l00218"></a>00218     <span class="comment">// we display informations about the performed operation</span>
<a name="l00219"></a>00219     <span class="keywordflow">if</span> (print_level &gt; 1)
<a name="l00220"></a>00220       {
<a name="l00221"></a>00221         umfpack_di_report_status(this-&gt;Control.GetData(), status_facto);
<a name="l00222"></a>00222         umfpack_di_report_info(this-&gt;Control.GetData(),this-&gt;<a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a4a6b504d370b37ef4951626d2a3ce439" title="parameters for UmfPack">Info</a>.GetData());
<a name="l00223"></a>00223       }
<a name="l00224"></a>00224 
<a name="l00225"></a>00225     <span class="keywordflow">if</span> (print_level &gt; 0)
<a name="l00226"></a>00226       {
<a name="l00227"></a>00227         <span class="keywordtype">int</span> size_mem = (this-&gt;<a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a4a6b504d370b37ef4951626d2a3ce439" title="parameters for UmfPack">Info</a>(UMFPACK_SYMBOLIC_SIZE)
<a name="l00228"></a>00228                         + this-&gt;<a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a4a6b504d370b37ef4951626d2a3ce439" title="parameters for UmfPack">Info</a>(UMFPACK_NUMERIC_SIZE_ESTIMATE))
<a name="l00229"></a>00229           *this-&gt;<a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a4a6b504d370b37ef4951626d2a3ce439" title="parameters for UmfPack">Info</a>(UMFPACK_SIZE_OF_UNIT);
<a name="l00230"></a>00230 
<a name="l00231"></a>00231         cout &lt;&lt; <span class="stringliteral">&quot;Memory used to store LU factors: &quot;</span>
<a name="l00232"></a>00232              &lt;&lt; double(size_mem)/(1024*1024) &lt;&lt; <span class="stringliteral">&quot; MB&quot;</span> &lt;&lt; endl;
<a name="l00233"></a>00233       }
<a name="l00234"></a>00234   }
<a name="l00235"></a>00235 
<a name="l00236"></a>00236 
<a name="l00238"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#a182a7c745c3d05a8fbe6733aa837087d">00238</a>   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00239"></a>00239   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack&lt;double&gt;</a>
<a name="l00240"></a>00240 <a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">  ::PerformAnalysis</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, Prop, RowSparse, Allocator&gt;</a> &amp; mat)
<a name="l00241"></a>00241   {
<a name="l00242"></a>00242     <span class="comment">// we clear previous factorization</span>
<a name="l00243"></a>00243     Clear();
<a name="l00244"></a>00244 
<a name="l00245"></a>00245     <span class="comment">// conversion to unsymmetric matrix in Column Sparse Row Format</span>
<a name="l00246"></a>00246     <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, General, RowSparse, MallocAlloc&lt;double&gt;</a> &gt; Acsr;
<a name="l00247"></a>00247     transpose = <span class="keyword">true</span>;
<a name="l00248"></a>00248 
<a name="l00249"></a>00249     Copy(mat, Acsr);
<a name="l00250"></a>00250 
<a name="l00251"></a>00251     <span class="comment">// we retrieve pointers of Acsc and nullify this object</span>
<a name="l00252"></a>00252     this-&gt;n = mat.GetM();
<a name="l00253"></a>00253     ptr_ = Acsr.GetPtr();
<a name="l00254"></a>00254     ind_ = Acsr.GetInd();
<a name="l00255"></a>00255     data_ = Acsr.GetData();
<a name="l00256"></a>00256     Acsr.Nullify();
<a name="l00257"></a>00257 
<a name="l00258"></a>00258     <span class="comment">// factorization with UmfPack</span>
<a name="l00259"></a>00259     umfpack_di_symbolic(this-&gt;n, this-&gt;n, ptr_, ind_, data_, &amp;this-&gt;Symbolic,
<a name="l00260"></a>00260                         this-&gt;Control.GetData(), this-&gt;Info.GetData());
<a name="l00261"></a>00261 
<a name="l00262"></a>00262   }
<a name="l00263"></a>00263 
<a name="l00264"></a>00264 
<a name="l00266"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#affb8a911794ad355a183469ed2e42480">00266</a>   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00267"></a>00267   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack&lt;double&gt;</a>
<a name="l00268"></a>00268 <a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">  ::PerformFactorization</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, Prop, RowSparse, Allocator&gt;</a> &amp; mat)
<a name="l00269"></a>00269   {
<a name="l00270"></a>00270     <span class="comment">// we copy values</span>
<a name="l00271"></a>00271     <span class="keywordtype">double</span>* data = mat.GetData();
<a name="l00272"></a>00272     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; mat.GetDataSize(); i++)
<a name="l00273"></a>00273       data_[i] = data[i];
<a name="l00274"></a>00274 
<a name="l00275"></a>00275     status_facto =
<a name="l00276"></a>00276       umfpack_di_numeric(ptr_, ind_, data_,
<a name="l00277"></a>00277                          this-&gt;Symbolic, &amp;this-&gt;Numeric,
<a name="l00278"></a>00278                          this-&gt;Control.GetData(), this-&gt;Info.GetData());
<a name="l00279"></a>00279 
<a name="l00280"></a>00280     <span class="comment">// we display informations about the performed operation</span>
<a name="l00281"></a>00281     <span class="keywordflow">if</span> (print_level &gt; 1)
<a name="l00282"></a>00282       {
<a name="l00283"></a>00283         umfpack_di_report_status(this-&gt;Control.GetData(), status_facto);
<a name="l00284"></a>00284         umfpack_di_report_info(this-&gt;Control.GetData(),this-&gt;Info.GetData());
<a name="l00285"></a>00285       }
<a name="l00286"></a>00286   }
<a name="l00287"></a>00287 
<a name="l00288"></a>00288 
<a name="l00290"></a>00290   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2&gt;
<a name="l00291"></a>00291   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack&lt;double&gt;::Solve</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double, VectFull, Allocator2&gt;</a>&amp; x)
<a name="l00292"></a>00292   {
<a name="l00293"></a>00293     <span class="comment">// local copy of x</span>
<a name="l00294"></a>00294     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double, VectFull, Allocator2&gt;</a> b(x);
<a name="l00295"></a>00295 
<a name="l00296"></a>00296     <span class="keywordtype">int</span> sys = UMFPACK_A;
<a name="l00297"></a>00297     <span class="keywordflow">if</span> (transpose)
<a name="l00298"></a>00298       sys = UMFPACK_Aat;
<a name="l00299"></a>00299 
<a name="l00300"></a>00300     <span class="keywordtype">int</span> status
<a name="l00301"></a>00301       = umfpack_di_solve(sys, ptr_, ind_, data_, x.GetData(),
<a name="l00302"></a>00302                          b.GetData(), this-&gt;<a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a7a91b3ba9d23982fc5b75ebf8f4d2eea" title="pointers of UmfPack objects">Numeric</a>, this-&gt;Control.GetData(),
<a name="l00303"></a>00303                          this-&gt;<a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a4a6b504d370b37ef4951626d2a3ce439" title="parameters for UmfPack">Info</a>.GetData());
<a name="l00304"></a>00304 
<a name="l00305"></a>00305     <span class="comment">// we display informations about the performed operation</span>
<a name="l00306"></a>00306     <span class="keywordflow">if</span> (print_level &gt; 1)
<a name="l00307"></a>00307       umfpack_di_report_status(this-&gt;Control.GetData(), status);
<a name="l00308"></a>00308   }
<a name="l00309"></a>00309 
<a name="l00310"></a>00310 
<a name="l00311"></a>00311   <span class="keyword">template</span>&lt;<span class="keyword">class</span> StatusTrans, <span class="keyword">class</span> Allocator2&gt;
<a name="l00312"></a>00312   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#a4f4e2a42ec974a69b46a91d5f68003ad" title="resolution of A y = x (result is overwritten in x)">MatrixUmfPack&lt;double&gt;::Solve</a>(<span class="keyword">const</span> StatusTrans&amp; TransA,
<a name="l00313"></a>00313                                     Vector&lt;double, VectFull, Allocator2&gt;&amp; x)
<a name="l00314"></a>00314   {
<a name="l00315"></a>00315     <span class="keywordflow">if</span> (TransA.NoTrans())
<a name="l00316"></a>00316       {
<a name="l00317"></a>00317         Solve(x);
<a name="l00318"></a>00318         <span class="keywordflow">return</span>;
<a name="l00319"></a>00319       }
<a name="l00320"></a>00320 
<a name="l00321"></a>00321     <span class="comment">// local copy of x</span>
<a name="l00322"></a>00322     Vector&lt;double, VectFull, Allocator2&gt; b(x);
<a name="l00323"></a>00323 
<a name="l00324"></a>00324     <span class="keywordtype">int</span> sys = UMFPACK_Aat;
<a name="l00325"></a>00325     <span class="keywordflow">if</span> (transpose)
<a name="l00326"></a>00326       sys = UMFPACK_A;
<a name="l00327"></a>00327 
<a name="l00328"></a>00328     <span class="keywordtype">int</span> status
<a name="l00329"></a>00329       = umfpack_di_solve(sys, ptr_, ind_, data_, x.GetData(),
<a name="l00330"></a>00330                          b.GetData(), this-&gt;<a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a7a91b3ba9d23982fc5b75ebf8f4d2eea" title="pointers of UmfPack objects">Numeric</a>, this-&gt;Control.GetData(),
<a name="l00331"></a>00331                          this-&gt;<a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a4a6b504d370b37ef4951626d2a3ce439" title="parameters for UmfPack">Info</a>.GetData());
<a name="l00332"></a>00332 
<a name="l00333"></a>00333     <span class="comment">// We display information about the performed operation.</span>
<a name="l00334"></a>00334     <span class="keywordflow">if</span> (print_level &gt; 1)
<a name="l00335"></a>00335       umfpack_di_report_status(this-&gt;Control.GetData(), status);
<a name="l00336"></a>00336   }
<a name="l00337"></a>00337 
<a name="l00338"></a>00338 
<a name="l00340"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a0efe83ef21a51ee358dec89fc0086ea4">00340</a>   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage,<span class="keyword">class</span> Allocator&gt;
<a name="l00341"></a>00341   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack&lt;complex&lt;double&gt;</a> &gt;::
<a name="l00342"></a>00342   FactorizeMatrix(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;complex&lt;double&gt;</a>, Prop, Storage, Allocator&gt; &amp; mat,
<a name="l00343"></a>00343                   <span class="keywordtype">bool</span> keep_matrix)
<a name="l00344"></a>00344   {
<a name="l00345"></a>00345     Clear();
<a name="l00346"></a>00346 
<a name="l00347"></a>00347     this-&gt;n = mat.GetM();
<a name="l00348"></a>00348     <span class="comment">// conversion to CSC format</span>
<a name="l00349"></a>00349     <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;complex&lt;double&gt;</a>, <a class="code" href="class_seldon_1_1_general.php">General</a>, <a class="code" href="class_seldon_1_1_col_sparse.php">ColSparse</a>,
<a name="l00350"></a>00350       <a class="code" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc&lt;complex&lt;double&gt;</a> &gt; &gt; Acsc;
<a name="l00351"></a>00351     transpose = <span class="keyword">false</span>;
<a name="l00352"></a>00352 
<a name="l00353"></a>00353     Copy(mat, Acsc);
<a name="l00354"></a>00354     <span class="keywordflow">if</span> (!keep_matrix)
<a name="l00355"></a>00355       mat.Clear();
<a name="l00356"></a>00356 
<a name="l00357"></a>00357     <span class="keywordtype">int</span> nnz = Acsc.GetDataSize();
<a name="l00358"></a>00358     complex&lt;double&gt;* data = Acsc.GetData();
<a name="l00359"></a>00359     <span class="keywordtype">int</span>* ptr = Acsc.GetPtr();
<a name="l00360"></a>00360     <span class="keywordtype">int</span>* ind = Acsc.GetInd();
<a name="l00361"></a>00361     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double, VectFull, MallocAlloc&lt;double&gt;</a> &gt;
<a name="l00362"></a>00362       ValuesReal(nnz), ValuesImag(nnz);
<a name="l00363"></a>00363 
<a name="l00364"></a>00364     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, MallocAlloc&lt;int&gt;</a> &gt; Ptr(this-&gt;n+1), Ind(nnz);
<a name="l00365"></a>00365 
<a name="l00366"></a>00366     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; nnz; i++)
<a name="l00367"></a>00367       {
<a name="l00368"></a>00368         ValuesReal(i) = real(data[i]);
<a name="l00369"></a>00369         ValuesImag(i) = imag(data[i]);
<a name="l00370"></a>00370         Ind(i) = ind[i];
<a name="l00371"></a>00371       }
<a name="l00372"></a>00372 
<a name="l00373"></a>00373     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt;= this-&gt;n; i++)
<a name="l00374"></a>00374       Ptr(i) = ptr[i];
<a name="l00375"></a>00375 
<a name="l00376"></a>00376     <span class="comment">// we clear intermediary matrix Acsc</span>
<a name="l00377"></a>00377     Acsc.Clear();
<a name="l00378"></a>00378 
<a name="l00379"></a>00379     <span class="comment">// retrieve pointers and nullify Seldon vectors</span>
<a name="l00380"></a>00380     data_real_ = ValuesReal.GetData();
<a name="l00381"></a>00381     data_imag_ = ValuesImag.GetData();
<a name="l00382"></a>00382     ptr_ = Ptr.GetData();
<a name="l00383"></a>00383     ind_ = Ind.GetData();
<a name="l00384"></a>00384     ValuesReal.Nullify(); ValuesImag.Nullify();
<a name="l00385"></a>00385     Ptr.Nullify(); Ind.Nullify();
<a name="l00386"></a>00386 
<a name="l00387"></a>00387     <span class="comment">// we call UmfPack</span>
<a name="l00388"></a>00388     umfpack_zi_symbolic(this-&gt;n, this-&gt;n, ptr_, ind_,
<a name="l00389"></a>00389                         data_real_, data_imag_,
<a name="l00390"></a>00390                         &amp;this-&gt;Symbolic, this-&gt;Control.GetData(),
<a name="l00391"></a>00391                         this-&gt;Info.GetData());
<a name="l00392"></a>00392 
<a name="l00393"></a>00393     status_facto
<a name="l00394"></a>00394       = umfpack_zi_numeric(ptr_, ind_, data_real_, data_imag_,
<a name="l00395"></a>00395                            this-&gt;Symbolic, &amp;this-&gt;Numeric,
<a name="l00396"></a>00396                            this-&gt;Control.GetData(), this-&gt;Info.GetData());
<a name="l00397"></a>00397 
<a name="l00398"></a>00398     <span class="keywordflow">if</span> (print_level &gt; 1)
<a name="l00399"></a>00399       {
<a name="l00400"></a>00400         umfpack_zi_report_status(this-&gt;Control.GetData(), status_facto);
<a name="l00401"></a>00401         umfpack_zi_report_info(this-&gt;Control.GetData(), this-&gt;Info.GetData());
<a name="l00402"></a>00402       }
<a name="l00403"></a>00403 
<a name="l00404"></a>00404     <span class="keywordflow">if</span> (print_level &gt; 0)
<a name="l00405"></a>00405       {
<a name="l00406"></a>00406         <span class="keywordtype">int</span> size_mem = (this-&gt;Info(UMFPACK_SYMBOLIC_SIZE)
<a name="l00407"></a>00407                         + this-&gt;Info(UMFPACK_NUMERIC_SIZE_ESTIMATE))
<a name="l00408"></a>00408           *this-&gt;Info(UMFPACK_SIZE_OF_UNIT);
<a name="l00409"></a>00409 
<a name="l00410"></a>00410         cout &lt;&lt; <span class="stringliteral">&quot;Estimated memory used to store LU factors: &quot;</span>
<a name="l00411"></a>00411              &lt;&lt; double(size_mem)/(1024*1024) &lt;&lt; <span class="stringliteral">&quot; MB&quot;</span> &lt;&lt; endl;
<a name="l00412"></a>00412       }
<a name="l00413"></a>00413   }
<a name="l00414"></a>00414 
<a name="l00415"></a>00415 
<a name="l00417"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a2e24772a5a8588f8fa826bc060fd8cbb">00417</a>   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2&gt;
<a name="l00418"></a>00418   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack&lt;complex&lt;double&gt;</a> &gt;::
<a name="l00419"></a>00419   Solve(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;complex&lt;double&gt;</a>, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator2&gt;&amp; x)
<a name="l00420"></a>00420   {
<a name="l00421"></a>00421     Solve(SeldonNoTrans, x);
<a name="l00422"></a>00422   }
<a name="l00423"></a>00423 
<a name="l00424"></a>00424 
<a name="l00426"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a80c4aabe4215400d7e7c3d3964bf4545">00426</a>   <span class="keyword">template</span>&lt;<span class="keyword">class</span> StatusTrans, <span class="keyword">class</span> Allocator2&gt;
<a name="l00427"></a>00427   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack&lt;complex&lt;double&gt;</a> &gt;::
<a name="l00428"></a>00428   Solve(<span class="keyword">const</span> StatusTrans&amp; TransA,
<a name="l00429"></a>00429         <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;complex&lt;double&gt;</a>, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator2&gt;&amp; x)
<a name="l00430"></a>00430   {
<a name="l00431"></a>00431     <span class="keywordtype">int</span> m = x.GetM();
<a name="l00432"></a>00432     <span class="comment">// creation of vectors</span>
<a name="l00433"></a>00433     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double&gt;</a> b_real(m), b_imag(m);
<a name="l00434"></a>00434     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l00435"></a>00435       {
<a name="l00436"></a>00436         b_real(i) = real(x(i));
<a name="l00437"></a>00437         b_imag(i) = imag(x(i));
<a name="l00438"></a>00438       }
<a name="l00439"></a>00439 
<a name="l00440"></a>00440     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double&gt;</a> x_real(m), x_imag(m);
<a name="l00441"></a>00441     x_real.Zero();
<a name="l00442"></a>00442     x_imag.Zero();
<a name="l00443"></a>00443 
<a name="l00444"></a>00444     <span class="keywordtype">int</span> sys = UMFPACK_A;
<a name="l00445"></a>00445     <span class="keywordflow">if</span> (TransA.Trans())
<a name="l00446"></a>00446       sys = UMFPACK_Aat;
<a name="l00447"></a>00447 
<a name="l00448"></a>00448     <span class="keywordtype">int</span> status
<a name="l00449"></a>00449       = umfpack_zi_solve(sys, ptr_, ind_, data_real_, data_imag_,
<a name="l00450"></a>00450                          x_real.GetData(), x_imag.GetData(),
<a name="l00451"></a>00451                          b_real.GetData(), b_imag.GetData(),
<a name="l00452"></a>00452                          this-&gt;Numeric,
<a name="l00453"></a>00453                          this-&gt;Control.GetData(), this-&gt;Info.GetData());
<a name="l00454"></a>00454     b_real.Clear();
<a name="l00455"></a>00455     b_imag.Clear();
<a name="l00456"></a>00456 
<a name="l00457"></a>00457     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l00458"></a>00458       x(i) = complex&lt;double&gt;(x_real(i), x_imag(i));
<a name="l00459"></a>00459 
<a name="l00460"></a>00460     <span class="keywordflow">if</span> (print_level &gt; 1)
<a name="l00461"></a>00461       umfpack_zi_report_status(this-&gt;Control.GetData(), status);
<a name="l00462"></a>00462   }
<a name="l00463"></a>00463 
<a name="l00464"></a>00464 
<a name="l00466"></a>00466   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00467"></a>00467   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a76d3f187a877c6c58245b0716e1adc00" title="Returns the LU factorization of a matrix.">GetLU</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A, <a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack&lt;T&gt;</a>&amp; mat_lu,
<a name="l00468"></a>00468              <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>)
<a name="l00469"></a>00469   {
<a name="l00470"></a>00470     mat_lu.FactorizeMatrix(A, keep_matrix);
<a name="l00471"></a>00471   }
<a name="l00472"></a>00472 
<a name="l00473"></a>00473 
<a name="l00475"></a>00475   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00476"></a>00476   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(MatrixUmfPack&lt;T&gt;&amp; mat_lu, Vector&lt;T, VectFull, Allocator&gt;&amp; x)
<a name="l00477"></a>00477   {
<a name="l00478"></a>00478     mat_lu.Solve(x);
<a name="l00479"></a>00479   }
<a name="l00480"></a>00480 
<a name="l00481"></a>00481 
<a name="l00482"></a>00482   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00483"></a>00483   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(<span class="keyword">const</span> SeldonTranspose&amp; TransA,
<a name="l00484"></a>00484                MatrixUmfPack&lt;T&gt;&amp; mat_lu, Vector&lt;T, VectFull, Allocator&gt;&amp; x)
<a name="l00485"></a>00485   {
<a name="l00486"></a>00486     mat_lu.Solve(TransA, x);
<a name="l00487"></a>00487   }
<a name="l00488"></a>00488 
<a name="l00489"></a>00489 }
<a name="l00490"></a>00490 
<a name="l00491"></a>00491 <span class="preprocessor">#define SELDON_FILE_UMFPACK_CXX</span>
<a name="l00492"></a>00492 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
