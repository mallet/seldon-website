<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>seldon::iostream Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="classseldon_1_1iostream.php">seldon::iostream</a>, including all inherited members.<table>
  <tr bgcolor="#f0f0f0"><td><b>__init__</b> (defined in <a class="el" href="classseldon_1_1iostream.php">seldon::iostream</a>)</td><td><a class="el" href="classseldon_1_1iostream.php">seldon::iostream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>__init__</b> (defined in <a class="el" href="classseldon_1_1istream.php">seldon::istream</a>)</td><td><a class="el" href="classseldon_1_1istream.php">seldon::istream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>__init__</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>__init__</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>__lshift__</b> (defined in <a class="el" href="classseldon_1_1ostream.php">seldon::ostream</a>)</td><td><a class="el" href="classseldon_1_1ostream.php">seldon::ostream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>__rshift__</b> (defined in <a class="el" href="classseldon_1_1istream.php">seldon::istream</a>)</td><td><a class="el" href="classseldon_1_1istream.php">seldon::istream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>bad</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>bad</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>clear</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>clear</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>copyfmt</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>copyfmt</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>copyfmt_event</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td><code> [static]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>copyfmt_event</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td><code> [static]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>eof</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>eof</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>erase_event</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td><code> [static]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>erase_event</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td><code> [static]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>exceptions</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>exceptions</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>fail</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>fail</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>fill</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>fill</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>flags</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>flags</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>flush</b> (defined in <a class="el" href="classseldon_1_1ostream.php">seldon::ostream</a>)</td><td><a class="el" href="classseldon_1_1ostream.php">seldon::ostream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>gcount</b> (defined in <a class="el" href="classseldon_1_1istream.php">seldon::istream</a>)</td><td><a class="el" href="classseldon_1_1istream.php">seldon::istream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>get</b> (defined in <a class="el" href="classseldon_1_1istream.php">seldon::istream</a>)</td><td><a class="el" href="classseldon_1_1istream.php">seldon::istream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>getline</b> (defined in <a class="el" href="classseldon_1_1istream.php">seldon::istream</a>)</td><td><a class="el" href="classseldon_1_1istream.php">seldon::istream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>getloc</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>getloc</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>good</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>good</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>ignore</b> (defined in <a class="el" href="classseldon_1_1istream.php">seldon::istream</a>)</td><td><a class="el" href="classseldon_1_1istream.php">seldon::istream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>imbue</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>imbue</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>imbue_event</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td><code> [static]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>imbue_event</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td><code> [static]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>iword</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>iword</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>narrow</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>narrow</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>peek</b> (defined in <a class="el" href="classseldon_1_1istream.php">seldon::istream</a>)</td><td><a class="el" href="classseldon_1_1istream.php">seldon::istream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>precision</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>precision</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>put</b> (defined in <a class="el" href="classseldon_1_1ostream.php">seldon::ostream</a>)</td><td><a class="el" href="classseldon_1_1ostream.php">seldon::ostream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>putback</b> (defined in <a class="el" href="classseldon_1_1istream.php">seldon::istream</a>)</td><td><a class="el" href="classseldon_1_1istream.php">seldon::istream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pword</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pword</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>rdbuf</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>rdbuf</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>rdstate</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>rdstate</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>read</b> (defined in <a class="el" href="classseldon_1_1istream.php">seldon::istream</a>)</td><td><a class="el" href="classseldon_1_1istream.php">seldon::istream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>readsome</b> (defined in <a class="el" href="classseldon_1_1istream.php">seldon::istream</a>)</td><td><a class="el" href="classseldon_1_1istream.php">seldon::istream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>register_callback</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>register_callback</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>seekg</b> (defined in <a class="el" href="classseldon_1_1istream.php">seldon::istream</a>)</td><td><a class="el" href="classseldon_1_1istream.php">seldon::istream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>seekp</b> (defined in <a class="el" href="classseldon_1_1ostream.php">seldon::ostream</a>)</td><td><a class="el" href="classseldon_1_1ostream.php">seldon::ostream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>setf</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>setf</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>setstate</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>setstate</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>sync</b> (defined in <a class="el" href="classseldon_1_1istream.php">seldon::istream</a>)</td><td><a class="el" href="classseldon_1_1istream.php">seldon::istream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>tellg</b> (defined in <a class="el" href="classseldon_1_1istream.php">seldon::istream</a>)</td><td><a class="el" href="classseldon_1_1istream.php">seldon::istream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>tellp</b> (defined in <a class="el" href="classseldon_1_1ostream.php">seldon::ostream</a>)</td><td><a class="el" href="classseldon_1_1ostream.php">seldon::ostream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>this</b> (defined in <a class="el" href="classseldon_1_1iostream.php">seldon::iostream</a>)</td><td><a class="el" href="classseldon_1_1iostream.php">seldon::iostream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>this</b> (defined in <a class="el" href="classseldon_1_1istream.php">seldon::istream</a>)</td><td><a class="el" href="classseldon_1_1istream.php">seldon::istream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>tie</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>tie</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>unget</b> (defined in <a class="el" href="classseldon_1_1istream.php">seldon::istream</a>)</td><td><a class="el" href="classseldon_1_1istream.php">seldon::istream</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>unsetf</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>unsetf</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>widen</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>widen</b> (defined in <a class="el" href="classseldon_1_1ios.php">seldon::ios</a>)</td><td><a class="el" href="classseldon_1_1ios.php">seldon::ios</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>width</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>width</b> (defined in <a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a>)</td><td><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>write</b> (defined in <a class="el" href="classseldon_1_1ostream.php">seldon::ostream</a>)</td><td><a class="el" href="classseldon_1_1ostream.php">seldon::ostream</a></td><td></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
