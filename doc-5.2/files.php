<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>File List</h1>  </div>
</div>
<div class="contents">
Here is a list of all documented files with brief descriptions:<table>
  <tr><td class="indexkey"><b>Seldon.cpp</b> <a href="_seldon_8cpp_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><b>Seldon.hxx</b> <a href="_seldon_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><b>seldon.py</b> <a href="seldon_8py_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><b>SeldonHeader.hxx</b> <a href="_seldon_header_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><b>SeldonPreconditioner.hxx</b> <a href="_seldon_preconditioner_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey"><b>SeldonSolver.hxx</b> <a href="_seldon_solver_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">.sconf_temp/<b>conftest_0.c</b> <a href="conftest__0_8c_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">array3d/<b>Array3D.cxx</b> <a href="_array3_d_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">array3d/<b>Array3D.hxx</b> <a href="_array3_d_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/basic_functions/<b>Functions_Matrix.cxx</b> <a href="_functions___matrix_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/basic_functions/<b>Functions_Matrix.hxx</b> <a href="_functions___matrix_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/basic_functions/<b>Functions_MatVect.cxx</b> <a href="_functions___mat_vect_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/basic_functions/<b>Functions_MatVect.hxx</b> <a href="_functions___mat_vect_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/basic_functions/<b>Functions_Vector.cxx</b> <a href="_functions___vector_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/basic_functions/<b>Functions_Vector.hxx</b> <a href="_functions___vector_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/<b>Blas_1.cxx</b> <a href="_blas__1_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/<b>Blas_1.hxx</b> <a href="_blas__1_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/<b>Blas_2.cxx</b> <a href="_blas__2_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/<b>Blas_2.hxx</b> <a href="_blas__2_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/<b>Blas_3.cxx</b> <a href="_blas__3_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/<b>Blas_3.hxx</b> <a href="_blas__3_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/<b>cblas.h</b> <a href="cblas_8h_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/<b>clapack.h</b> <a href="clapack_8h_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/<b>Lapack_Eigenvalues.cxx</b> <a href="_lapack___eigenvalues_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/<b>Lapack_Eigenvalues.hxx</b> <a href="_lapack___eigenvalues_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/<b>Lapack_LeastSquares.cxx</b> <a href="_lapack___least_squares_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/<b>Lapack_LeastSquares.hxx</b> <a href="_lapack___least_squares_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/<b>Lapack_LinearEquations.cxx</b> <a href="_lapack___linear_equations_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/<b>Lapack_LinearEquations.hxx</b> <a href="_lapack___linear_equations_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/direct/<b>Cholmod.cxx</b> <a href="_cholmod_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/direct/<b>Cholmod.hxx</b> <a href="_cholmod_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/direct/<b>Mumps.cxx</b> <a href="_mumps_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/direct/<b>Mumps.hxx</b> <a href="_mumps_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/direct/<b>Pastix.cxx</b> <a href="_pastix_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/direct/<b>Pastix.hxx</b> <a href="_pastix_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/direct/<b>SuperLU.cxx</b> <a href="_super_l_u_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/direct/<b>SuperLU.hxx</b> <a href="_super_l_u_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/direct/<b>superlu_interface.h</b> <a href="superlu__interface_8h_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/direct/<b>UmfPack.cxx</b> <a href="_umf_pack_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/interfaces/direct/<b>UmfPack.hxx</b> <a href="_umf_pack_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/optimization/<b>NLopt.hxx</b> <a href="_n_lopt_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/optimization/<b>NLoptSolver.cxx</b> <a href="_n_lopt_solver_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/optimization/<b>NLoptSolver.hxx</b> <a href="_n_lopt_solver_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/<b>Ordering.cxx</b> <a href="_ordering_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/<b>Ordering.hxx</b> <a href="_ordering_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/<b>SparseCholeskyFactorisation.cxx</b> <a href="_sparse_cholesky_factorisation_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/<b>SparseCholeskyFactorisation.hxx</b> <a href="_sparse_cholesky_factorisation_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/<b>SparseSolver.cxx</b> <a href="_sparse_solver_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/<b>SparseSolver.hxx</b> <a href="_sparse_solver_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/iterative/<b>BiCg.cxx</b> <a href="_bi_cg_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/iterative/<b>BiCgcr.cxx</b> <a href="_bi_cgcr_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/iterative/<b>BiCgStab.cxx</b> <a href="_bi_cg_stab_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/iterative/<b>BiCgStabl.cxx</b> <a href="_bi_cg_stabl_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/iterative/<b>Cg.cxx</b> <a href="_cg_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/iterative/<b>Cgne.cxx</b> <a href="_cgne_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/iterative/<b>Cgs.cxx</b> <a href="_cgs_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/iterative/<b>CoCg.cxx</b> <a href="_co_cg_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/iterative/<b>Gcr.cxx</b> <a href="_gcr_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/iterative/<b>Gmres.cxx</b> <a href="_gmres_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/iterative/<b>Iterative.cxx</b> <a href="_iterative_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/iterative/<b>Iterative.hxx</b> <a href="_iterative_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/iterative/<b>Lsqr.cxx</b> <a href="_lsqr_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/iterative/<b>MinRes.cxx</b> <a href="_min_res_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/iterative/<b>QCgs.cxx</b> <a href="_q_cgs_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/iterative/<b>Qmr.cxx</b> <a href="_qmr_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/iterative/<b>QmrSym.cxx</b> <a href="_qmr_sym_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/iterative/<b>Symmlq.cxx</b> <a href="_symmlq_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/iterative/<b>TfQmr.cxx</b> <a href="_tf_qmr_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/preconditioner/<b>IlutPreconditioning.cxx</b> <a href="_ilut_preconditioning_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/preconditioner/<b>IlutPreconditioning.hxx</b> <a href="_ilut_preconditioning_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/preconditioner/<b>Precond_Ssor.cxx</b> <a href="_precond___ssor_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">computation/solver/preconditioner/<b>SymmetricIlutPreconditioning.cxx</b> <a href="_symmetric_ilut_preconditioning_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">doc/example/<b>basic_example.cpp</b> <a href="basic__example_8cpp_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">doc/example/<b>basic_example_exception.cpp</b> <a href="basic__example__exception_8cpp_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">doc/example/<b>matrix_market.cpp</b> <a href="matrix__market_8cpp_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">doc/example/<b>submatrix.cpp</b> <a href="submatrix_8cpp_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">doc/example/<b>vector2.cpp</b> <a href="vector2_8cpp_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">doc/guide/<b>footer.php</b> <a href="footer_8php_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">doc/guide/<b>header.php</b> <a href="header_8php_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">lib/<b>template-Common.cpp</b> <a href="template-_common_8cpp_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">lib/<b>template-MatrixHermSymTriang.cpp</b> <a href="template-_matrix_herm_sym_triang_8cpp_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">lib/<b>template-MatrixPacked.cpp</b> <a href="template-_matrix_packed_8cpp_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">lib/<b>template-MatrixPointers.cpp</b> <a href="template-_matrix_pointers_8cpp_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">lib/<b>template-Vector.cpp</b> <a href="template-_vector_8cpp_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>Functions.cxx</b> <a href="_functions_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>Functions.hxx</b> <a href="_functions_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>HeterogeneousMatrixCollection.cxx</b> <a href="_heterogeneous_matrix_collection_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>HeterogeneousMatrixCollection.hxx</b> <a href="_heterogeneous_matrix_collection_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>Matrix_Base.cxx</b> <a href="_matrix___base_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>Matrix_Base.hxx</b> <a href="_matrix___base_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>Matrix_Hermitian.cxx</b> <a href="_matrix___hermitian_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>Matrix_Hermitian.hxx</b> <a href="_matrix___hermitian_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>Matrix_HermPacked.cxx</b> <a href="_matrix___herm_packed_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>Matrix_HermPacked.hxx</b> <a href="_matrix___herm_packed_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>Matrix_Pointers.cxx</b> <a href="_matrix___pointers_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>Matrix_Pointers.hxx</b> <a href="_matrix___pointers_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>Matrix_Symmetric.cxx</b> <a href="_matrix___symmetric_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>Matrix_Symmetric.hxx</b> <a href="_matrix___symmetric_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>Matrix_SymPacked.cxx</b> <a href="_matrix___sym_packed_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>Matrix_SymPacked.hxx</b> <a href="_matrix___sym_packed_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>Matrix_TriangPacked.cxx</b> <a href="_matrix___triang_packed_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>Matrix_TriangPacked.hxx</b> <a href="_matrix___triang_packed_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>Matrix_Triangular.cxx</b> <a href="_matrix___triangular_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>Matrix_Triangular.hxx</b> <a href="_matrix___triangular_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>MatrixCollection.cxx</b> <a href="_matrix_collection_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>MatrixCollection.hxx</b> <a href="_matrix_collection_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>PetscMatrix.cxx</b> <a href="_petsc_matrix_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>PetscMatrix.hxx</b> <a href="_petsc_matrix_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>SubMatrix.cxx</b> <a href="_sub_matrix_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>SubMatrix.hxx</b> <a href="_sub_matrix_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>SubMatrix_Base.cxx</b> <a href="_sub_matrix___base_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix/<b>SubMatrix_Base.hxx</b> <a href="_sub_matrix___base_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix_sparse/<b>Functions_MatrixArray.cxx</b> <a href="_functions___matrix_array_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix_sparse/<b>IOMatrixMarket.cxx</b> <a href="_i_o_matrix_market_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix_sparse/<b>IOMatrixMarket.hxx</b> <a href="_i_o_matrix_market_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix_sparse/<b>Matrix_ArrayComplexSparse.cxx</b> <a href="_matrix___array_complex_sparse_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix_sparse/<b>Matrix_ArrayComplexSparse.hxx</b> <a href="_matrix___array_complex_sparse_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix_sparse/<b>Matrix_ArraySparse.cxx</b> <a href="_matrix___array_sparse_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix_sparse/<b>Matrix_ArraySparse.hxx</b> <a href="_matrix___array_sparse_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix_sparse/<b>Matrix_ComplexSparse.cxx</b> <a href="_matrix___complex_sparse_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix_sparse/<b>Matrix_ComplexSparse.hxx</b> <a href="_matrix___complex_sparse_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix_sparse/<b>Matrix_Conversions.cxx</b> <a href="_matrix___conversions_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix_sparse/<b>Matrix_Conversions.hxx</b> <a href="_matrix___conversions_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix_sparse/<b>Matrix_Sparse.cxx</b> <a href="_matrix___sparse_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix_sparse/<b>Matrix_Sparse.hxx</b> <a href="_matrix___sparse_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix_sparse/<b>Matrix_SymComplexSparse.cxx</b> <a href="_matrix___sym_complex_sparse_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix_sparse/<b>Matrix_SymComplexSparse.hxx</b> <a href="_matrix___sym_complex_sparse_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix_sparse/<b>Matrix_SymSparse.cxx</b> <a href="_matrix___sym_sparse_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix_sparse/<b>Matrix_SymSparse.hxx</b> <a href="_matrix___sym_sparse_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix_sparse/<b>Permutation_ScalingMatrix.cxx</b> <a href="_permutation___scaling_matrix_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">matrix_sparse/<b>Relaxation_MatVect.cxx</b> <a href="_relaxation___mat_vect_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">share/<b>Allocator.cxx</b> <a href="_allocator_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">share/<b>Allocator.hxx</b> <a href="_allocator_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">share/<b>Common.cxx</b> <a href="_common_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">share/<b>Common.hxx</b> <a href="_common_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">share/<b>Errors.cxx</b> <a href="_errors_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">share/<b>Errors.hxx</b> <a href="_errors_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">share/<b>expand.py</b> <a href="expand_8py_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">share/<b>MatrixFlag.cxx</b> <a href="_matrix_flag_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">share/<b>MatrixFlag.hxx</b> <a href="_matrix_flag_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">share/<b>Properties.hxx</b> <a href="_properties_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">share/<b>Storage.cxx</b> <a href="_storage_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">share/<b>Storage.hxx</b> <a href="_storage_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">vector/<b>Functions_Arrays.cxx</b> <a href="_functions___arrays_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">vector/<b>HeterogeneousCollection.cxx</b> <a href="_heterogeneous_collection_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">vector/<b>HeterogeneousCollection.hxx</b> <a href="_heterogeneous_collection_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">vector/<b>PetscVector.cxx</b> <a href="_petsc_vector_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">vector/<b>PetscVector.hxx</b> <a href="_petsc_vector_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">vector/<b>SparseVector.cxx</b> <a href="_sparse_vector_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">vector/<b>SparseVector.hxx</b> <a href="_sparse_vector_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">vector/<b>Vector.cxx</b> <a href="_vector_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">vector/<b>Vector.hxx</b> <a href="_vector_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">vector/<b>Vector2.cxx</b> <a href="_vector2_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">vector/<b>Vector2.hxx</b> <a href="_vector2_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">vector/<b>Vector3.cxx</b> <a href="_vector3_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">vector/<b>Vector3.hxx</b> <a href="_vector3_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">vector/<b>VectorCollection.cxx</b> <a href="_vector_collection_8cxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
  <tr><td class="indexkey">vector/<b>VectorCollection.hxx</b> <a href="_vector_collection_8hxx_source.php">[code]</a></td><td class="indexvalue"></td></tr>
</table>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
