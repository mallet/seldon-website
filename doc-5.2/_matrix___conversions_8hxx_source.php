<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix_sparse/Matrix_Conversions.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2001-2010 Vivien Mallet</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_CONVERSIONS_HXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 
<a name="l00024"></a>00024 <span class="keyword">namespace </span>Seldon
<a name="l00025"></a>00025 {
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 
<a name="l00028"></a>00028   <span class="comment">/*</span>
<a name="l00029"></a>00029 <span class="comment">    From CSR formats to &quot;Matlab&quot; coordinate format.</span>
<a name="l00030"></a>00030 <span class="comment">  */</span>
<a name="l00031"></a>00031 
<a name="l00032"></a>00032 
<a name="l00033"></a>00033   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00034"></a>00034            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00035"></a>00035   <span class="keywordtype">void</span>
<a name="l00036"></a>00036   <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, RowSparse,
<a name="l00037"></a>00037                                Allocator1&gt;&amp; A,
<a name="l00038"></a>00038                                Vector&lt;Tint, VectFull, Allocator2&gt;&amp; IndRow,
<a name="l00039"></a>00039                                Vector&lt;Tint, VectFull, Allocator3&gt;&amp; IndCol,
<a name="l00040"></a>00040                                Vector&lt;T, VectFull, Allocator4&gt;&amp; Val,
<a name="l00041"></a>00041                                <span class="keywordtype">int</span> index = 0, <span class="keywordtype">bool</span> sym = <span class="keyword">false</span>);
<a name="l00042"></a>00042 
<a name="l00043"></a>00043 
<a name="l00044"></a>00044   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00045"></a>00045            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00046"></a>00046   <span class="keywordtype">void</span>
<a name="l00047"></a>00047   <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ColSparse,
<a name="l00048"></a>00048                                Allocator1&gt;&amp; A,
<a name="l00049"></a>00049                                Vector&lt;Tint, VectFull, Allocator2&gt;&amp; IndRow,
<a name="l00050"></a>00050                                Vector&lt;Tint, VectFull, Allocator3&gt;&amp; IndCol,
<a name="l00051"></a>00051                                Vector&lt;T, VectFull, Allocator4&gt;&amp; Val,
<a name="l00052"></a>00052                                <span class="keywordtype">int</span> index = 0, <span class="keywordtype">bool</span> sym = <span class="keyword">false</span>);
<a name="l00053"></a>00053 
<a name="l00054"></a>00054 
<a name="l00055"></a>00055   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00056"></a>00056            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00057"></a>00057   <span class="keywordtype">void</span>
<a name="l00058"></a>00058   <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, RowSymSparse,
<a name="l00059"></a>00059                                Allocator1&gt;&amp; A,
<a name="l00060"></a>00060                                Vector&lt;Tint, VectFull, Allocator2&gt;&amp; IndRow,
<a name="l00061"></a>00061                                Vector&lt;Tint, VectFull, Allocator3&gt;&amp; IndCol,
<a name="l00062"></a>00062                                Vector&lt;T, VectFull, Allocator4&gt;&amp; Val,
<a name="l00063"></a>00063                                <span class="keywordtype">int</span> index = 0, <span class="keywordtype">bool</span> sym = <span class="keyword">false</span>);
<a name="l00064"></a>00064 
<a name="l00065"></a>00065 
<a name="l00066"></a>00066   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00067"></a>00067            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00068"></a>00068   <span class="keywordtype">void</span>
<a name="l00069"></a>00069   <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ColSymSparse,
<a name="l00070"></a>00070                                Allocator1&gt;&amp; A,
<a name="l00071"></a>00071                                Vector&lt;Tint, VectFull, Allocator2&gt;&amp; IndRow,
<a name="l00072"></a>00072                                Vector&lt;Tint, VectFull, Allocator3&gt;&amp; IndCol,
<a name="l00073"></a>00073                                Vector&lt;T, VectFull, Allocator4&gt;&amp; Val,
<a name="l00074"></a>00074                                <span class="keywordtype">int</span> index = 0, <span class="keywordtype">bool</span> sym = <span class="keyword">false</span>);
<a name="l00075"></a>00075 
<a name="l00076"></a>00076 
<a name="l00077"></a>00077   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00078"></a>00078            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00079"></a>00079   <span class="keywordtype">void</span>
<a name="l00080"></a>00080   <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, RowComplexSparse,
<a name="l00081"></a>00081                                Allocator1&gt;&amp; A,
<a name="l00082"></a>00082                                Vector&lt;Tint, VectFull, Allocator2&gt;&amp; IndRow,
<a name="l00083"></a>00083                                Vector&lt;Tint, VectFull, Allocator3&gt;&amp; IndCol,
<a name="l00084"></a>00084                                Vector&lt;complex&lt;T&gt;, VectFull, Allocator4&gt;&amp; Val,
<a name="l00085"></a>00085                                <span class="keywordtype">int</span> index = 0, <span class="keywordtype">bool</span> sym = <span class="keyword">false</span>);
<a name="l00086"></a>00086 
<a name="l00087"></a>00087 
<a name="l00088"></a>00088   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00089"></a>00089            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00090"></a>00090   <span class="keywordtype">void</span>
<a name="l00091"></a>00091   <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ColComplexSparse,
<a name="l00092"></a>00092                                Allocator1&gt;&amp; A,
<a name="l00093"></a>00093                                Vector&lt;Tint, VectFull, Allocator2&gt;&amp; IndRow,
<a name="l00094"></a>00094                                Vector&lt;Tint, VectFull, Allocator3&gt;&amp; IndCol,
<a name="l00095"></a>00095                                Vector&lt;complex&lt;T&gt;, VectFull, Allocator4&gt;&amp; Val,
<a name="l00096"></a>00096                                <span class="keywordtype">int</span> index = 0, <span class="keywordtype">bool</span> sym = <span class="keyword">false</span>);
<a name="l00097"></a>00097 
<a name="l00098"></a>00098 
<a name="l00099"></a>00099   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00100"></a>00100            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00101"></a>00101   <span class="keywordtype">void</span>
<a name="l00102"></a>00102   <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, RowSymComplexSparse,
<a name="l00103"></a>00103                                Allocator1&gt;&amp; A,
<a name="l00104"></a>00104                                Vector&lt;Tint, VectFull, Allocator2&gt;&amp; IndRow,
<a name="l00105"></a>00105                                Vector&lt;Tint, VectFull, Allocator3&gt;&amp; IndCol,
<a name="l00106"></a>00106                                Vector&lt;complex&lt;T&gt;, VectFull, Allocator4&gt;&amp; Val,
<a name="l00107"></a>00107                                <span class="keywordtype">int</span> index = 0, <span class="keywordtype">bool</span> sym = <span class="keyword">false</span>);
<a name="l00108"></a>00108 
<a name="l00109"></a>00109 
<a name="l00110"></a>00110   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00111"></a>00111            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00112"></a>00112   <span class="keywordtype">void</span>
<a name="l00113"></a>00113   <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ColSymComplexSparse,
<a name="l00114"></a>00114                                Allocator1&gt;&amp; A,
<a name="l00115"></a>00115                                Vector&lt;Tint, VectFull, Allocator2&gt;&amp; IndRow,
<a name="l00116"></a>00116                                Vector&lt;Tint, VectFull, Allocator3&gt;&amp; IndCol,
<a name="l00117"></a>00117                                Vector&lt;complex&lt;T&gt;, VectFull, Allocator4&gt;&amp; Val,
<a name="l00118"></a>00118                                <span class="keywordtype">int</span> index = 0, <span class="keywordtype">bool</span> sym = <span class="keyword">false</span>);
<a name="l00119"></a>00119 
<a name="l00120"></a>00120 
<a name="l00121"></a>00121   <span class="comment">/*</span>
<a name="l00122"></a>00122 <span class="comment">    From Sparse Array formats to &quot;Matlab&quot; coordinate format.</span>
<a name="l00123"></a>00123 <span class="comment">  */</span>
<a name="l00124"></a>00124 
<a name="l00125"></a>00125 
<a name="l00126"></a>00126   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00127"></a>00127            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00128"></a>00128   <span class="keywordtype">void</span>
<a name="l00129"></a>00129   <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ArrayRowSparse,
<a name="l00130"></a>00130                                Allocator1&gt;&amp; A,
<a name="l00131"></a>00131                                Vector&lt;Tint, VectFull, Allocator2&gt;&amp; IndRow,
<a name="l00132"></a>00132                                Vector&lt;Tint, VectFull, Allocator3&gt;&amp; IndCol,
<a name="l00133"></a>00133                                Vector&lt;T, VectFull, Allocator4&gt;&amp; Val,
<a name="l00134"></a>00134                                <span class="keywordtype">int</span> index = 0, <span class="keywordtype">bool</span> sym = <span class="keyword">false</span>);
<a name="l00135"></a>00135 
<a name="l00136"></a>00136 
<a name="l00137"></a>00137   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00138"></a>00138            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00139"></a>00139   <span class="keywordtype">void</span>
<a name="l00140"></a>00140   <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ArrayColSparse,
<a name="l00141"></a>00141                                Allocator1&gt;&amp; A,
<a name="l00142"></a>00142                                Vector&lt;Tint, VectFull, Allocator2&gt;&amp; IndRow,
<a name="l00143"></a>00143                                Vector&lt;Tint, VectFull, Allocator3&gt;&amp; IndCol,
<a name="l00144"></a>00144                                Vector&lt;T, VectFull, Allocator4&gt;&amp; Val,
<a name="l00145"></a>00145                                <span class="keywordtype">int</span> index = 0, <span class="keywordtype">bool</span> sym = <span class="keyword">false</span>);
<a name="l00146"></a>00146 
<a name="l00147"></a>00147 
<a name="l00148"></a>00148   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00149"></a>00149            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00150"></a>00150   <span class="keywordtype">void</span>
<a name="l00151"></a>00151   <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ArrayRowComplexSparse,
<a name="l00152"></a>00152                                Allocator1&gt;&amp; A,
<a name="l00153"></a>00153                                Vector&lt;Tint, VectFull, Allocator2&gt;&amp; IndRow,
<a name="l00154"></a>00154                                Vector&lt;Tint, VectFull, Allocator3&gt;&amp; IndCol,
<a name="l00155"></a>00155                                Vector&lt;complex&lt;T&gt;, VectFull, Allocator4&gt;&amp; Val,
<a name="l00156"></a>00156                                <span class="keywordtype">int</span> index = 0, <span class="keywordtype">bool</span> sym = <span class="keyword">false</span>);
<a name="l00157"></a>00157 
<a name="l00158"></a>00158 
<a name="l00159"></a>00159   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00160"></a>00160            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00161"></a>00161   <span class="keywordtype">void</span>
<a name="l00162"></a>00162   <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ArrayRowSymSparse,
<a name="l00163"></a>00163                                Allocator1&gt;&amp; A,
<a name="l00164"></a>00164                                Vector&lt;Tint, VectFull, Allocator2&gt;&amp; IndRow,
<a name="l00165"></a>00165                                Vector&lt;Tint, VectFull, Allocator3&gt;&amp; IndCol,
<a name="l00166"></a>00166                                Vector&lt;T, VectFull, Allocator4&gt;&amp; Val,
<a name="l00167"></a>00167                                <span class="keywordtype">int</span> index = 0, <span class="keywordtype">bool</span> sym = <span class="keyword">false</span>);
<a name="l00168"></a>00168 
<a name="l00169"></a>00169 
<a name="l00170"></a>00170   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00171"></a>00171            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00172"></a>00172   <span class="keywordtype">void</span>
<a name="l00173"></a>00173   <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ArrayColSymSparse,
<a name="l00174"></a>00174                                Allocator1&gt;&amp; A,
<a name="l00175"></a>00175                                Vector&lt;Tint, VectFull, Allocator2&gt;&amp; IndRow,
<a name="l00176"></a>00176                                Vector&lt;Tint, VectFull, Allocator3&gt;&amp; IndCol,
<a name="l00177"></a>00177                                Vector&lt;T, VectFull, Allocator4&gt;&amp; Val,
<a name="l00178"></a>00178                                <span class="keywordtype">int</span> index = 0, <span class="keywordtype">bool</span> sym = <span class="keyword">false</span>);
<a name="l00179"></a>00179 
<a name="l00180"></a>00180 
<a name="l00181"></a>00181   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00182"></a>00182            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00183"></a>00183   <span class="keywordtype">void</span>
<a name="l00184"></a>00184   <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ArrayRowSymComplexSparse,
<a name="l00185"></a>00185                                Allocator1&gt;&amp; A,
<a name="l00186"></a>00186                                Vector&lt;Tint, VectFull, Allocator2&gt;&amp; IndRow,
<a name="l00187"></a>00187                                Vector&lt;Tint, VectFull, Allocator3&gt;&amp; IndCol,
<a name="l00188"></a>00188                                Vector&lt;complex&lt;T&gt;, VectFull, Allocator4&gt;&amp; Val,
<a name="l00189"></a>00189                                <span class="keywordtype">int</span> index = 0, <span class="keywordtype">bool</span> sym = <span class="keyword">false</span>);
<a name="l00190"></a>00190 
<a name="l00191"></a>00191 
<a name="l00192"></a>00192   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00193"></a>00193            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00194"></a>00194   <span class="keywordtype">void</span>
<a name="l00195"></a>00195   <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ArrayColSymComplexSparse,
<a name="l00196"></a>00196                                Allocator1&gt;&amp; A,
<a name="l00197"></a>00197                                Vector&lt;Tint, VectFull, Allocator2&gt;&amp; IndRow,
<a name="l00198"></a>00198                                Vector&lt;Tint, VectFull, Allocator3&gt;&amp; IndCol,
<a name="l00199"></a>00199                                Vector&lt;complex&lt;T&gt;, VectFull, Allocator4&gt;&amp; Val,
<a name="l00200"></a>00200                                <span class="keywordtype">int</span> index = 0, <span class="keywordtype">bool</span> sym = <span class="keyword">false</span>);
<a name="l00201"></a>00201 
<a name="l00202"></a>00202 
<a name="l00203"></a>00203   <span class="comment">/*</span>
<a name="l00204"></a>00204 <span class="comment">    From &quot;Matlab&quot; coordinate format to CSR formats.</span>
<a name="l00205"></a>00205 <span class="comment">  */</span>
<a name="l00206"></a>00206 
<a name="l00207"></a>00207 
<a name="l00208"></a>00208   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1,
<a name="l00209"></a>00209            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00210"></a>00210   <span class="keywordtype">void</span>
<a name="l00211"></a>00211   <a class="code" href="namespace_seldon.php#ad4977fcff081edc62cc482b1aedaf0d9" title="Conversion from coordinate format to RowSparse.">ConvertMatrix_from_Coordinates</a>(Vector&lt;int, VectFull, Allocator1&gt;&amp; IndRow_,
<a name="l00212"></a>00212                                  Vector&lt;int, VectFull, Allocator2&gt;&amp; IndCol_,
<a name="l00213"></a>00213                                  Vector&lt;T, VectFull, Allocator3&gt;&amp; Val,
<a name="l00214"></a>00214                                  Matrix&lt;T, Prop, RowSparse, Allocator3&gt;&amp; A,
<a name="l00215"></a>00215                                  <span class="keywordtype">int</span> index = 0);
<a name="l00216"></a>00216 
<a name="l00217"></a>00217 
<a name="l00218"></a>00218   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1,
<a name="l00219"></a>00219            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00220"></a>00220   <span class="keywordtype">void</span>
<a name="l00221"></a>00221   <a class="code" href="namespace_seldon.php#ad4977fcff081edc62cc482b1aedaf0d9" title="Conversion from coordinate format to RowSparse.">ConvertMatrix_from_Coordinates</a>(Vector&lt;int, VectFull, Allocator1&gt;&amp; IndRow_,
<a name="l00222"></a>00222                                  Vector&lt;int, VectFull, Allocator2&gt;&amp; IndCol_,
<a name="l00223"></a>00223                                  Vector&lt;T, VectFull, Allocator3&gt;&amp; Val,
<a name="l00224"></a>00224                                  Matrix&lt;T, Prop, ColSparse, Allocator3&gt;&amp; A,
<a name="l00225"></a>00225                                  <span class="keywordtype">int</span> index = 0);
<a name="l00226"></a>00226 
<a name="l00227"></a>00227 
<a name="l00228"></a>00228   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1,
<a name="l00229"></a>00229            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00230"></a>00230   <span class="keywordtype">void</span>
<a name="l00231"></a>00231   <a class="code" href="namespace_seldon.php#ad4977fcff081edc62cc482b1aedaf0d9" title="Conversion from coordinate format to RowSparse.">ConvertMatrix_from_Coordinates</a>(Vector&lt;int, VectFull, Allocator1&gt;&amp; IndRow_,
<a name="l00232"></a>00232                                  Vector&lt;int, VectFull, Allocator2&gt;&amp; IndCol_,
<a name="l00233"></a>00233                                  Vector&lt;T, VectFull, Allocator3&gt;&amp; Val,
<a name="l00234"></a>00234                                  Matrix&lt;T, Prop, RowSymSparse, Allocator3&gt;&amp; A,
<a name="l00235"></a>00235                                  <span class="keywordtype">int</span> index = 0);
<a name="l00236"></a>00236 
<a name="l00237"></a>00237 
<a name="l00238"></a>00238   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1,
<a name="l00239"></a>00239            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00240"></a>00240   <span class="keywordtype">void</span>
<a name="l00241"></a>00241   <a class="code" href="namespace_seldon.php#ad4977fcff081edc62cc482b1aedaf0d9" title="Conversion from coordinate format to RowSparse.">ConvertMatrix_from_Coordinates</a>(Vector&lt;int, VectFull, Allocator1&gt;&amp; IndRow_,
<a name="l00242"></a>00242                                  Vector&lt;int, VectFull, Allocator2&gt;&amp; IndCol_,
<a name="l00243"></a>00243                                  Vector&lt;T, VectFull, Allocator3&gt;&amp; Val,
<a name="l00244"></a>00244                                  Matrix&lt;T, Prop, ColSymSparse, Allocator3&gt;&amp; A,
<a name="l00245"></a>00245                                  <span class="keywordtype">int</span> index = 0);
<a name="l00246"></a>00246 
<a name="l00247"></a>00247 
<a name="l00248"></a>00248   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1,
<a name="l00249"></a>00249            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00250"></a>00250   <span class="keywordtype">void</span>
<a name="l00251"></a>00251   <a class="code" href="namespace_seldon.php#ad4977fcff081edc62cc482b1aedaf0d9" title="Conversion from coordinate format to RowSparse.">ConvertMatrix_from_Coordinates</a>
<a name="l00252"></a>00252   (Vector&lt;int, VectFull, Allocator1&gt;&amp; IndRow,
<a name="l00253"></a>00253    Vector&lt;int, VectFull, Allocator2&gt;&amp; IndCol,
<a name="l00254"></a>00254    Vector&lt;complex&lt;T&gt;, VectFull, Allocator3&gt;&amp; Val,
<a name="l00255"></a>00255    Matrix&lt;T, Prop, RowComplexSparse,
<a name="l00256"></a>00256    Allocator4&gt;&amp; A,
<a name="l00257"></a>00257    <span class="keywordtype">int</span> index = 0);
<a name="l00258"></a>00258 
<a name="l00259"></a>00259 
<a name="l00260"></a>00260   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1,
<a name="l00261"></a>00261            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00262"></a>00262   <span class="keywordtype">void</span>
<a name="l00263"></a>00263   <a class="code" href="namespace_seldon.php#ad4977fcff081edc62cc482b1aedaf0d9" title="Conversion from coordinate format to RowSparse.">ConvertMatrix_from_Coordinates</a>
<a name="l00264"></a>00264   (Vector&lt;int, VectFull, Allocator1&gt;&amp; IndRow,
<a name="l00265"></a>00265    Vector&lt;int, VectFull, Allocator2&gt;&amp; IndCol,
<a name="l00266"></a>00266    Vector&lt;complex&lt;T&gt;, VectFull, Allocator3&gt;&amp; Val,
<a name="l00267"></a>00267    Matrix&lt;T, Prop, ColComplexSparse,
<a name="l00268"></a>00268    Allocator4&gt;&amp; A,
<a name="l00269"></a>00269    <span class="keywordtype">int</span> index = 0);
<a name="l00270"></a>00270 
<a name="l00271"></a>00271 
<a name="l00272"></a>00272   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1,
<a name="l00273"></a>00273            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00274"></a>00274   <span class="keywordtype">void</span>
<a name="l00275"></a>00275   <a class="code" href="namespace_seldon.php#ad4977fcff081edc62cc482b1aedaf0d9" title="Conversion from coordinate format to RowSparse.">ConvertMatrix_from_Coordinates</a>
<a name="l00276"></a>00276   (Vector&lt;int, VectFull, Allocator1&gt;&amp; IndRow,
<a name="l00277"></a>00277    Vector&lt;int, VectFull, Allocator2&gt;&amp; IndCol,
<a name="l00278"></a>00278    Vector&lt;complex&lt;T&gt;, VectFull, Allocator3&gt;&amp; Val,
<a name="l00279"></a>00279    Matrix&lt;T, Prop, RowSymComplexSparse,
<a name="l00280"></a>00280    Allocator4&gt;&amp; A,
<a name="l00281"></a>00281    <span class="keywordtype">int</span> index = 0);
<a name="l00282"></a>00282 
<a name="l00283"></a>00283 
<a name="l00284"></a>00284   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1,
<a name="l00285"></a>00285            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00286"></a>00286   <span class="keywordtype">void</span>
<a name="l00287"></a>00287   <a class="code" href="namespace_seldon.php#ad4977fcff081edc62cc482b1aedaf0d9" title="Conversion from coordinate format to RowSparse.">ConvertMatrix_from_Coordinates</a>
<a name="l00288"></a>00288   (Vector&lt;int, VectFull, Allocator1&gt;&amp; IndRow,
<a name="l00289"></a>00289    Vector&lt;int, VectFull, Allocator2&gt;&amp; IndCol,
<a name="l00290"></a>00290    Vector&lt;complex&lt;T&gt;, VectFull, Allocator3&gt;&amp; Val,
<a name="l00291"></a>00291    Matrix&lt;T, Prop, ColSymComplexSparse,
<a name="l00292"></a>00292    Allocator4&gt;&amp; A,
<a name="l00293"></a>00293    <span class="keywordtype">int</span> index = 0);
<a name="l00294"></a>00294 
<a name="l00295"></a>00295 
<a name="l00296"></a>00296   <span class="comment">/*</span>
<a name="l00297"></a>00297 <span class="comment">    From Sparse Array formats to &quot;Matlab&quot; coordinate format.</span>
<a name="l00298"></a>00298 <span class="comment">  */</span>
<a name="l00299"></a>00299 
<a name="l00300"></a>00300 
<a name="l00301"></a>00301   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1,
<a name="l00302"></a>00302            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00303"></a>00303   <span class="keywordtype">void</span>
<a name="l00304"></a>00304   <a class="code" href="namespace_seldon.php#ad4977fcff081edc62cc482b1aedaf0d9" title="Conversion from coordinate format to RowSparse.">ConvertMatrix_from_Coordinates</a>(Vector&lt;int, VectFull, Allocator1&gt;&amp; IndRow_,
<a name="l00305"></a>00305                                  Vector&lt;int, VectFull, Allocator2&gt;&amp; IndCol_,
<a name="l00306"></a>00306                                  Vector&lt;T, VectFull, Allocator3&gt;&amp; Val,
<a name="l00307"></a>00307                                  Matrix&lt;T, Prop, ArrayRowSparse,
<a name="l00308"></a>00308                                  Allocator3&gt;&amp; A,
<a name="l00309"></a>00309                                  <span class="keywordtype">int</span> index = 0);
<a name="l00310"></a>00310 
<a name="l00311"></a>00311 
<a name="l00312"></a>00312   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1,
<a name="l00313"></a>00313            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00314"></a>00314   <span class="keywordtype">void</span>
<a name="l00315"></a>00315   <a class="code" href="namespace_seldon.php#ad4977fcff081edc62cc482b1aedaf0d9" title="Conversion from coordinate format to RowSparse.">ConvertMatrix_from_Coordinates</a>(Vector&lt;int, VectFull, Allocator1&gt;&amp; IndRow_,
<a name="l00316"></a>00316                                  Vector&lt;int, VectFull, Allocator2&gt;&amp; IndCol_,
<a name="l00317"></a>00317                                  Vector&lt;T, VectFull, Allocator3&gt;&amp; Val,
<a name="l00318"></a>00318                                  Matrix&lt;T, Prop, ArrayColSparse,
<a name="l00319"></a>00319                                  Allocator3&gt;&amp; A,
<a name="l00320"></a>00320                                  <span class="keywordtype">int</span> index = 0);
<a name="l00321"></a>00321 
<a name="l00322"></a>00322 
<a name="l00323"></a>00323   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1,
<a name="l00324"></a>00324            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00325"></a>00325   <span class="keywordtype">void</span>
<a name="l00326"></a>00326   <a class="code" href="namespace_seldon.php#ad4977fcff081edc62cc482b1aedaf0d9" title="Conversion from coordinate format to RowSparse.">ConvertMatrix_from_Coordinates</a>(Vector&lt;int, VectFull, Allocator1&gt;&amp; IndRow_,
<a name="l00327"></a>00327                                  Vector&lt;int, VectFull, Allocator2&gt;&amp; IndCol_,
<a name="l00328"></a>00328                                  Vector&lt;T, VectFull, Allocator3&gt;&amp; Val,
<a name="l00329"></a>00329                                  Matrix&lt;T, Prop, ArrayRowSymSparse,
<a name="l00330"></a>00330                                  Allocator3&gt;&amp; A,
<a name="l00331"></a>00331                                  <span class="keywordtype">int</span> index = 0);
<a name="l00332"></a>00332 
<a name="l00333"></a>00333 
<a name="l00334"></a>00334   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1,
<a name="l00335"></a>00335            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00336"></a>00336   <span class="keywordtype">void</span>
<a name="l00337"></a>00337   <a class="code" href="namespace_seldon.php#ad4977fcff081edc62cc482b1aedaf0d9" title="Conversion from coordinate format to RowSparse.">ConvertMatrix_from_Coordinates</a>(Vector&lt;int, VectFull, Allocator1&gt;&amp; IndRow_,
<a name="l00338"></a>00338                                  Vector&lt;int, VectFull, Allocator2&gt;&amp; IndCol_,
<a name="l00339"></a>00339                                  Vector&lt;T, VectFull, Allocator3&gt;&amp; Val,
<a name="l00340"></a>00340                                  Matrix&lt;T, Prop, ArrayColSymSparse,
<a name="l00341"></a>00341                                  Allocator3&gt;&amp; A,
<a name="l00342"></a>00342                                  <span class="keywordtype">int</span> index = 0);
<a name="l00343"></a>00343 
<a name="l00344"></a>00344 
<a name="l00345"></a>00345   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1,
<a name="l00346"></a>00346            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00347"></a>00347   <span class="keywordtype">void</span>
<a name="l00348"></a>00348   <a class="code" href="namespace_seldon.php#ad4977fcff081edc62cc482b1aedaf0d9" title="Conversion from coordinate format to RowSparse.">ConvertMatrix_from_Coordinates</a>
<a name="l00349"></a>00349     (Vector&lt;int, VectFull, Allocator1&gt;&amp; IndRow,
<a name="l00350"></a>00350      Vector&lt;int, VectFull, Allocator2&gt;&amp; IndCol,
<a name="l00351"></a>00351      Vector&lt;complex&lt;T&gt;, VectFull, Allocator3&gt;&amp; Val,
<a name="l00352"></a>00352      Matrix&lt;T, Prop, ArrayRowComplexSparse,
<a name="l00353"></a>00353      Allocator4&gt;&amp; A,
<a name="l00354"></a>00354      <span class="keywordtype">int</span> index = 0);
<a name="l00355"></a>00355 
<a name="l00356"></a>00356 
<a name="l00357"></a>00357   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1,
<a name="l00358"></a>00358            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00359"></a>00359   <span class="keywordtype">void</span>
<a name="l00360"></a>00360   <a class="code" href="namespace_seldon.php#ad4977fcff081edc62cc482b1aedaf0d9" title="Conversion from coordinate format to RowSparse.">ConvertMatrix_from_Coordinates</a>
<a name="l00361"></a>00361   (Vector&lt;int, VectFull, Allocator1&gt;&amp; IndRow,
<a name="l00362"></a>00362    Vector&lt;int, VectFull, Allocator2&gt;&amp; IndCol,
<a name="l00363"></a>00363    Vector&lt;complex&lt;T&gt;, VectFull, Allocator3&gt;&amp; Val,
<a name="l00364"></a>00364    Matrix&lt;T, Prop, ArrayColComplexSparse,
<a name="l00365"></a>00365    Allocator4&gt;&amp; A,
<a name="l00366"></a>00366    <span class="keywordtype">int</span> index = 0);
<a name="l00367"></a>00367 
<a name="l00368"></a>00368 
<a name="l00369"></a>00369   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1,
<a name="l00370"></a>00370            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00371"></a>00371   <span class="keywordtype">void</span>
<a name="l00372"></a>00372   <a class="code" href="namespace_seldon.php#ad4977fcff081edc62cc482b1aedaf0d9" title="Conversion from coordinate format to RowSparse.">ConvertMatrix_from_Coordinates</a>
<a name="l00373"></a>00373   (Vector&lt;int, VectFull, Allocator1&gt;&amp; IndRow,
<a name="l00374"></a>00374    Vector&lt;int, VectFull, Allocator2&gt;&amp; IndCol,
<a name="l00375"></a>00375    Vector&lt;complex&lt;T&gt;, VectFull, Allocator3&gt;&amp; Val,
<a name="l00376"></a>00376    Matrix&lt;T, Prop, ArrayRowSymComplexSparse,
<a name="l00377"></a>00377    Allocator4&gt;&amp; A,
<a name="l00378"></a>00378    <span class="keywordtype">int</span> index = 0);
<a name="l00379"></a>00379 
<a name="l00380"></a>00380 
<a name="l00381"></a>00381   <span class="comment">/*</span>
<a name="l00382"></a>00382 <span class="comment">    From Sparse formats to CSC format</span>
<a name="l00383"></a>00383 <span class="comment">  */</span>
<a name="l00384"></a>00384 
<a name="l00385"></a>00385 
<a name="l00386"></a>00386   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Alloc1,
<a name="l00387"></a>00387            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Alloc2, <span class="keyword">class </span>Alloc3, <span class="keyword">class </span>Alloc4&gt;
<a name="l00388"></a>00388   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#af590e7294696236e540099421ca518d7" title="Conversion from RowSparse to CSC format.">ConvertToCSC</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, RowSparse, Alloc1&gt;&amp; A,
<a name="l00389"></a>00389                     General&amp; sym, Vector&lt;Tint, VectFull, Alloc2&gt;&amp; Ptr,
<a name="l00390"></a>00390                     Vector&lt;Tint, VectFull, Alloc3&gt;&amp; IndRow,
<a name="l00391"></a>00391                     Vector&lt;T, VectFull, Alloc4&gt;&amp; Val, <span class="keywordtype">bool</span> sym_pat = <span class="keyword">false</span>);
<a name="l00392"></a>00392 
<a name="l00393"></a>00393 
<a name="l00394"></a>00394   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Alloc1,
<a name="l00395"></a>00395            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Alloc2, <span class="keyword">class </span>Alloc3, <span class="keyword">class </span>Alloc4&gt;
<a name="l00396"></a>00396   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#af590e7294696236e540099421ca518d7" title="Conversion from RowSparse to CSC format.">ConvertToCSC</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ArrayRowSparse, Alloc1&gt;&amp; A,
<a name="l00397"></a>00397                     General&amp; sym, Vector&lt;Tint, VectFull, Alloc2&gt;&amp; Ptr,
<a name="l00398"></a>00398                     Vector&lt;Tint, VectFull, Alloc3&gt;&amp; IndRow,
<a name="l00399"></a>00399                     Vector&lt;T, VectFull, Alloc4&gt;&amp; Val, <span class="keywordtype">bool</span> sym_pat = <span class="keyword">false</span>);
<a name="l00400"></a>00400 
<a name="l00401"></a>00401 
<a name="l00402"></a>00402   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Alloc1,
<a name="l00403"></a>00403            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Alloc2, <span class="keyword">class </span>Alloc3, <span class="keyword">class </span>Alloc4&gt;
<a name="l00404"></a>00404   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#af590e7294696236e540099421ca518d7" title="Conversion from RowSparse to CSC format.">ConvertToCSC</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ColSparse, Alloc1&gt;&amp; A,
<a name="l00405"></a>00405                     General&amp; sym, Vector&lt;Tint, VectFull, Alloc2&gt;&amp; Ptr,
<a name="l00406"></a>00406                     Vector&lt;Tint, VectFull, Alloc3&gt;&amp; IndRow,
<a name="l00407"></a>00407                     Vector&lt;T, VectFull, Alloc4&gt;&amp; Val, <span class="keywordtype">bool</span> sym_pat = <span class="keyword">false</span>);
<a name="l00408"></a>00408 
<a name="l00409"></a>00409 
<a name="l00410"></a>00410   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Alloc1,
<a name="l00411"></a>00411            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Alloc2, <span class="keyword">class </span>Alloc3, <span class="keyword">class </span>Alloc4&gt;
<a name="l00412"></a>00412   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#af590e7294696236e540099421ca518d7" title="Conversion from RowSparse to CSC format.">ConvertToCSC</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ArrayColSparse, Alloc1&gt;&amp; A,
<a name="l00413"></a>00413                     General&amp; sym, Vector&lt;Tint, VectFull, Alloc2&gt;&amp; Ptr,
<a name="l00414"></a>00414                     Vector&lt;Tint, VectFull, Alloc3&gt;&amp; IndRow,
<a name="l00415"></a>00415                     Vector&lt;T, VectFull, Alloc4&gt;&amp; Val, <span class="keywordtype">bool</span> sym_pat = <span class="keyword">false</span>);
<a name="l00416"></a>00416 
<a name="l00417"></a>00417 
<a name="l00418"></a>00418   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Alloc1,
<a name="l00419"></a>00419            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Alloc2, <span class="keyword">class </span>Alloc3, <span class="keyword">class </span>Alloc4&gt;
<a name="l00420"></a>00420   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#af590e7294696236e540099421ca518d7" title="Conversion from RowSparse to CSC format.">ConvertToCSC</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ColSymSparse, Alloc1&gt;&amp; A,
<a name="l00421"></a>00421                     Symmetric&amp; sym, Vector&lt;Tint, VectFull, Alloc2&gt;&amp; Ptr,
<a name="l00422"></a>00422                     Vector&lt;Tint, VectFull, Alloc3&gt;&amp; Ind,
<a name="l00423"></a>00423                     Vector&lt;T, VectFull, Alloc4&gt;&amp; Value, <span class="keywordtype">bool</span> sym_pat = <span class="keyword">false</span>);
<a name="l00424"></a>00424 
<a name="l00425"></a>00425 
<a name="l00426"></a>00426   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Alloc1,
<a name="l00427"></a>00427            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Alloc2, <span class="keyword">class </span>Alloc3, <span class="keyword">class </span>Alloc4&gt;
<a name="l00428"></a>00428   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#af590e7294696236e540099421ca518d7" title="Conversion from RowSparse to CSC format.">ConvertToCSC</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ColSymSparse, Alloc1&gt;&amp; A,
<a name="l00429"></a>00429                     General&amp; sym, Vector&lt;Tint, VectFull, Alloc2&gt;&amp; Ptr,
<a name="l00430"></a>00430                     Vector&lt;Tint, VectFull, Alloc3&gt;&amp; IndRow,
<a name="l00431"></a>00431                     Vector&lt;T, VectFull, Alloc4&gt;&amp; Value, <span class="keywordtype">bool</span> sym_pat = <span class="keyword">false</span>);
<a name="l00432"></a>00432 
<a name="l00433"></a>00433 
<a name="l00434"></a>00434   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Alloc1,
<a name="l00435"></a>00435            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Alloc2, <span class="keyword">class </span>Alloc3, <span class="keyword">class </span>Alloc4&gt;
<a name="l00436"></a>00436   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#af590e7294696236e540099421ca518d7" title="Conversion from RowSparse to CSC format.">ConvertToCSC</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ArrayColSymSparse, Alloc1&gt;&amp; A,
<a name="l00437"></a>00437                     Symmetric&amp; sym, Vector&lt;Tint, VectFull, Alloc2&gt;&amp; Ptr,
<a name="l00438"></a>00438                     Vector&lt;Tint, VectFull, Alloc3&gt;&amp; Ind,
<a name="l00439"></a>00439                     Vector&lt;T, VectFull, Alloc4&gt;&amp; Value, <span class="keywordtype">bool</span> sym_pat = <span class="keyword">false</span>);
<a name="l00440"></a>00440 
<a name="l00441"></a>00441 
<a name="l00442"></a>00442   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Alloc1,
<a name="l00443"></a>00443            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Alloc2, <span class="keyword">class </span>Alloc3, <span class="keyword">class </span>Alloc4&gt;
<a name="l00444"></a>00444   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#af590e7294696236e540099421ca518d7" title="Conversion from RowSparse to CSC format.">ConvertToCSC</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ArrayColSymSparse, Alloc1&gt;&amp; A,
<a name="l00445"></a>00445                     General&amp; sym, Vector&lt;Tint, VectFull, Alloc2&gt;&amp; Ptr,
<a name="l00446"></a>00446                     Vector&lt;Tint, VectFull, Alloc3&gt;&amp; IndRow,
<a name="l00447"></a>00447                     Vector&lt;T, VectFull, Alloc4&gt;&amp; Value, <span class="keywordtype">bool</span> sym_pat = <span class="keyword">false</span>);
<a name="l00448"></a>00448 
<a name="l00449"></a>00449 
<a name="l00450"></a>00450   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Alloc1,
<a name="l00451"></a>00451            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Alloc2, <span class="keyword">class </span>Alloc3, <span class="keyword">class </span>Alloc4&gt;
<a name="l00452"></a>00452   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#af590e7294696236e540099421ca518d7" title="Conversion from RowSparse to CSC format.">ConvertToCSC</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, RowSymSparse, Alloc1&gt;&amp; A,
<a name="l00453"></a>00453                     Symmetric&amp; sym, Vector&lt;Tint, VectFull, Alloc2&gt;&amp; Ptr,
<a name="l00454"></a>00454                     Vector&lt;Tint, VectFull, Alloc3&gt;&amp; IndRow,
<a name="l00455"></a>00455                     Vector&lt;T, VectFull, Alloc4&gt;&amp; Value, <span class="keywordtype">bool</span> sym_pat = <span class="keyword">false</span>);
<a name="l00456"></a>00456 
<a name="l00457"></a>00457 
<a name="l00458"></a>00458   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Alloc1,
<a name="l00459"></a>00459            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Alloc2, <span class="keyword">class </span>Alloc3, <span class="keyword">class </span>Alloc4&gt;
<a name="l00460"></a>00460   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#af590e7294696236e540099421ca518d7" title="Conversion from RowSparse to CSC format.">ConvertToCSC</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, RowSymSparse, Alloc1&gt;&amp; A,
<a name="l00461"></a>00461                     General&amp; sym, Vector&lt;Tint, VectFull, Alloc2&gt;&amp; Ptr,
<a name="l00462"></a>00462                     Vector&lt;Tint, VectFull, Alloc3&gt;&amp; IndRow,
<a name="l00463"></a>00463                     Vector&lt;T, VectFull, Alloc4&gt;&amp; Value, <span class="keywordtype">bool</span> sym_pat = <span class="keyword">false</span>);
<a name="l00464"></a>00464 
<a name="l00465"></a>00465 
<a name="l00466"></a>00466   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Alloc1,
<a name="l00467"></a>00467            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Alloc2, <span class="keyword">class </span>Alloc3, <span class="keyword">class </span>Alloc4&gt;
<a name="l00468"></a>00468   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#af590e7294696236e540099421ca518d7" title="Conversion from RowSparse to CSC format.">ConvertToCSC</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ArrayRowSymSparse, Alloc1&gt;&amp; A,
<a name="l00469"></a>00469                     Symmetric&amp; sym, Vector&lt;Tint, VectFull, Alloc2&gt;&amp; Ptr,
<a name="l00470"></a>00470                     Vector&lt;Tint, VectFull, Alloc3&gt;&amp; IndRow,
<a name="l00471"></a>00471                     Vector&lt;T, VectFull, Alloc4&gt;&amp; Value, <span class="keywordtype">bool</span> sym_pat = <span class="keyword">false</span>);
<a name="l00472"></a>00472 
<a name="l00473"></a>00473 
<a name="l00474"></a>00474   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Alloc1,
<a name="l00475"></a>00475            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Alloc2, <span class="keyword">class </span>Alloc3, <span class="keyword">class </span>Alloc4&gt;
<a name="l00476"></a>00476   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#af590e7294696236e540099421ca518d7" title="Conversion from RowSparse to CSC format.">ConvertToCSC</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ArrayRowSymSparse, Alloc1&gt;&amp; A,
<a name="l00477"></a>00477                     General&amp; sym, Vector&lt;Tint, VectFull, Alloc2&gt;&amp; Ptr,
<a name="l00478"></a>00478                     Vector&lt;Tint, VectFull, Alloc3&gt;&amp; Ind,
<a name="l00479"></a>00479                     Vector&lt;T, VectFull, Alloc4&gt;&amp; AllVal, <span class="keywordtype">bool</span> sym_pat = <span class="keyword">false</span>);
<a name="l00480"></a>00480 
<a name="l00481"></a>00481 
<a name="l00482"></a>00482   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00483"></a>00483   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Matrix&lt;T, Prop, Storage, Allocator&gt;&amp; A,
<a name="l00484"></a>00484             Matrix&lt;T, Prop, Storage, Allocator&gt;&amp; B);
<a name="l00485"></a>00485 
<a name="l00486"></a>00486 
<a name="l00487"></a>00487   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00488"></a>00488            <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00489"></a>00489   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Matrix&lt;T0, Prop0, ArrayColSparse, Allocator0&gt;&amp; mat_array,
<a name="l00490"></a>00490             Matrix&lt;T1, Prop1, ColSparse, Allocator1&gt;&amp; mat_csc);
<a name="l00491"></a>00491 
<a name="l00492"></a>00492 
<a name="l00493"></a>00493   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Alloc1, <span class="keyword">class</span> Alloc2&gt;
<a name="l00494"></a>00494   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Matrix&lt;T, Prop, RowSparse, Alloc1&gt;&amp; A,
<a name="l00495"></a>00495             Matrix&lt;T, Prop, ColSparse, Alloc2&gt;&amp; B);
<a name="l00496"></a>00496 
<a name="l00497"></a>00497 
<a name="l00498"></a>00498   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00499"></a>00499            <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00500"></a>00500   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Matrix&lt;T0, Prop0, ArrayRowSparse,Allocator0&gt;&amp; mat_array,
<a name="l00501"></a>00501             Matrix&lt;T1, Prop1, ColSparse, Allocator1&gt;&amp; mat_csr);
<a name="l00502"></a>00502 
<a name="l00503"></a>00503 
<a name="l00504"></a>00504   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop1, <span class="keyword">class</span> Prop2, <span class="keyword">class</span> Alloc1, <span class="keyword">class</span> Alloc2&gt;
<a name="l00505"></a>00505   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Matrix&lt;T, Prop1, RowSymSparse, Alloc1&gt;&amp; A,
<a name="l00506"></a>00506             Matrix&lt;T, Prop2, ColSparse, Alloc2&gt;&amp; B);
<a name="l00507"></a>00507 
<a name="l00508"></a>00508 
<a name="l00509"></a>00509   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00510"></a>00510            <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00511"></a>00511   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Matrix&lt;T0, Prop0, ArrayRowSymSparse, Allocator0&gt;&amp; A,
<a name="l00512"></a>00512             Matrix&lt;T1, Prop1, ColSparse, Allocator1&gt;&amp; B);
<a name="l00513"></a>00513 
<a name="l00514"></a>00514 
<a name="l00515"></a>00515   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop1, <span class="keyword">class</span> Prop2, <span class="keyword">class</span> Alloc1, <span class="keyword">class</span> Alloc2&gt;
<a name="l00516"></a>00516   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Matrix&lt;T, Prop1, ColSymSparse, Alloc1&gt;&amp; A,
<a name="l00517"></a>00517             Matrix&lt;T, Prop2, ColSparse, Alloc2&gt;&amp; B);
<a name="l00518"></a>00518 
<a name="l00519"></a>00519 
<a name="l00520"></a>00520   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop1, <span class="keyword">class</span> Prop2, <span class="keyword">class</span> Alloc1, <span class="keyword">class</span> Alloc2&gt;
<a name="l00521"></a>00521   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Matrix&lt;T, Prop1, ArrayColSymSparse, Alloc1&gt;&amp; A,
<a name="l00522"></a>00522             Matrix&lt;T, Prop2, ColSparse, Alloc2&gt;&amp; B);
<a name="l00523"></a>00523 
<a name="l00524"></a>00524   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop1, <span class="keyword">class</span> Prop2, <span class="keyword">class</span> Alloc1, <span class="keyword">class</span> Alloc2&gt;
<a name="l00525"></a>00525   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Matrix&lt;T, Prop1, PETScMPIDense, Alloc1&gt;&amp; A,
<a name="l00526"></a>00526             Matrix&lt;T, Prop2, RowMajor, Alloc2&gt;&amp; B);
<a name="l00527"></a>00527 
<a name="l00528"></a>00528   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop1, <span class="keyword">class</span> Prop2, <span class="keyword">class</span> Alloc1, <span class="keyword">class</span> Alloc2&gt;
<a name="l00529"></a>00529   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Matrix&lt;T, Prop1, RowMajor, Alloc1&gt;&amp; A,
<a name="l00530"></a>00530             Matrix&lt;T, Prop2, PETScMPIDense, Alloc2&gt;&amp; B);
<a name="l00531"></a>00531 
<a name="l00532"></a>00532 
<a name="l00533"></a>00533   <span class="comment">/*</span>
<a name="l00534"></a>00534 <span class="comment">    From Sparse formats to CSR format</span>
<a name="l00535"></a>00535 <span class="comment">  */</span>
<a name="l00536"></a>00536 
<a name="l00537"></a>00537 
<a name="l00538"></a>00538   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Alloc1,
<a name="l00539"></a>00539            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Alloc2, <span class="keyword">class </span>Alloc3, <span class="keyword">class </span>Alloc4&gt;
<a name="l00540"></a>00540   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#afaf5d0022d0430daa355ed1def309e07" title="Conversion from ColSparse to CSR.">ConvertToCSR</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ColSymSparse, Alloc1&gt;&amp; A,
<a name="l00541"></a>00541                     Symmetric&amp; sym, Vector&lt;Tint, VectFull, Alloc2&gt;&amp; Ptr,
<a name="l00542"></a>00542                     Vector&lt;Tint, VectFull, Alloc3&gt;&amp; IndCol,
<a name="l00543"></a>00543                     Vector&lt;T, VectFull, Alloc4&gt;&amp; Value);
<a name="l00544"></a>00544 
<a name="l00545"></a>00545 
<a name="l00546"></a>00546   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Alloc1,
<a name="l00547"></a>00547            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Alloc2, <span class="keyword">class </span>Alloc3, <span class="keyword">class </span>Alloc4&gt;
<a name="l00548"></a>00548   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#afaf5d0022d0430daa355ed1def309e07" title="Conversion from ColSparse to CSR.">ConvertToCSR</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ColSymSparse, Alloc1&gt;&amp; A,
<a name="l00549"></a>00549                     General&amp; sym, Vector&lt;Tint, VectFull, Alloc2&gt;&amp; Ptr,
<a name="l00550"></a>00550                     Vector&lt;Tint, VectFull, Alloc3&gt;&amp; IndCol,
<a name="l00551"></a>00551                     Vector&lt;T, VectFull, Alloc4&gt;&amp; Value);
<a name="l00552"></a>00552 
<a name="l00553"></a>00553 
<a name="l00554"></a>00554   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Alloc1,
<a name="l00555"></a>00555            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Alloc2, <span class="keyword">class </span>Alloc3, <span class="keyword">class </span>Alloc4&gt;
<a name="l00556"></a>00556   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#afaf5d0022d0430daa355ed1def309e07" title="Conversion from ColSparse to CSR.">ConvertToCSR</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ArrayColSymSparse, Alloc1&gt;&amp; A,
<a name="l00557"></a>00557                     Symmetric&amp; sym, Vector&lt;Tint, VectFull, Alloc2&gt;&amp; Ptr,
<a name="l00558"></a>00558                     Vector&lt;Tint, VectFull, Alloc3&gt;&amp; IndCol,
<a name="l00559"></a>00559                     Vector&lt;T, VectFull, Alloc4&gt;&amp; Value);
<a name="l00560"></a>00560 
<a name="l00561"></a>00561 
<a name="l00562"></a>00562   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Alloc1,
<a name="l00563"></a>00563            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Alloc2, <span class="keyword">class </span>Alloc3, <span class="keyword">class </span>Alloc4&gt;
<a name="l00564"></a>00564   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#afaf5d0022d0430daa355ed1def309e07" title="Conversion from ColSparse to CSR.">ConvertToCSR</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ArrayColSymSparse, Alloc1&gt;&amp; A,
<a name="l00565"></a>00565                     General&amp; sym, Vector&lt;Tint, VectFull, Alloc2&gt;&amp; Ptr,
<a name="l00566"></a>00566                     Vector&lt;Tint, VectFull, Alloc3&gt;&amp; IndCol,
<a name="l00567"></a>00567                     Vector&lt;T, VectFull, Alloc4&gt;&amp; Value);
<a name="l00568"></a>00568 
<a name="l00569"></a>00569 
<a name="l00570"></a>00570   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Alloc1,
<a name="l00571"></a>00571            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Alloc2, <span class="keyword">class </span>Alloc3, <span class="keyword">class </span>Alloc4&gt;
<a name="l00572"></a>00572   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#afaf5d0022d0430daa355ed1def309e07" title="Conversion from ColSparse to CSR.">ConvertToCSR</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ArrayRowSparse, Alloc1&gt;&amp; A,
<a name="l00573"></a>00573                     General&amp; sym, Vector&lt;Tint, VectFull, Alloc2&gt;&amp; IndRow,
<a name="l00574"></a>00574                     Vector&lt;Tint, VectFull, Alloc3&gt;&amp; IndCol,
<a name="l00575"></a>00575                     Vector&lt;T, VectFull, Alloc4&gt;&amp; Val);
<a name="l00576"></a>00576 
<a name="l00577"></a>00577 
<a name="l00578"></a>00578   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Alloc1,
<a name="l00579"></a>00579            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Alloc2, <span class="keyword">class </span>Alloc3, <span class="keyword">class </span>Alloc4&gt;
<a name="l00580"></a>00580   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#afaf5d0022d0430daa355ed1def309e07" title="Conversion from ColSparse to CSR.">ConvertToCSR</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, RowSymSparse, Alloc1&gt;&amp; A,
<a name="l00581"></a>00581                     Symmetric&amp; sym, Vector&lt;Tint, VectFull, Alloc2&gt;&amp; IndRow,
<a name="l00582"></a>00582                     Vector&lt;Tint, VectFull, Alloc3&gt;&amp; IndCol,
<a name="l00583"></a>00583                     Vector&lt;T, VectFull, Alloc4&gt;&amp; Val);
<a name="l00584"></a>00584 
<a name="l00585"></a>00585 
<a name="l00586"></a>00586   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Alloc1,
<a name="l00587"></a>00587            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Alloc2, <span class="keyword">class </span>Alloc3, <span class="keyword">class </span>Alloc4&gt;
<a name="l00588"></a>00588   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#afaf5d0022d0430daa355ed1def309e07" title="Conversion from ColSparse to CSR.">ConvertToCSR</a>(<span class="keyword">const</span> Matrix&lt;T, Prop, ArrayRowSymSparse, Alloc1&gt;&amp; A,
<a name="l00589"></a>00589                     Symmetric&amp; sym, Vector&lt;Tint, VectFull, Alloc2&gt;&amp; IndRow,
<a name="l00590"></a>00590                     Vector&lt;Tint, VectFull, Alloc3&gt;&amp; IndCol,
<a name="l00591"></a>00591                     Vector&lt;T, VectFull, Alloc4&gt;&amp; Val);
<a name="l00592"></a>00592 
<a name="l00593"></a>00593 
<a name="l00594"></a>00594   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Alloc1, <span class="keyword">class</span> Alloc2&gt;
<a name="l00595"></a>00595   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Matrix&lt;T, Prop, ColSymSparse, Alloc1&gt;&amp; A,
<a name="l00596"></a>00596             Matrix&lt;T, Prop, RowSymSparse, Alloc2&gt;&amp; B);
<a name="l00597"></a>00597 
<a name="l00598"></a>00598 
<a name="l00599"></a>00599   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>T2, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2,
<a name="l00600"></a>00600            <span class="keyword">class </span>Alloc1, <span class="keyword">class </span>Alloc2&gt;
<a name="l00601"></a>00601   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Matrix&lt;T1, Prop1, ColSparse, Alloc1&gt;&amp; A,
<a name="l00602"></a>00602             Matrix&lt;T2, Prop2, RowSparse, Alloc2&gt;&amp; B);
<a name="l00603"></a>00603 
<a name="l00604"></a>00604 
<a name="l00605"></a>00605   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00606"></a>00606            <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00607"></a>00607   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Matrix&lt;T0, Prop0, ArrayRowSparse,Allocator0&gt;&amp; mat_array,
<a name="l00608"></a>00608             Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;&amp; mat_csr);
<a name="l00609"></a>00609 
<a name="l00610"></a>00610 
<a name="l00611"></a>00611   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00612"></a>00612            <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00613"></a>00613   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Matrix&lt;T0, Prop0, ArrayRowSymSparse,Allocator0&gt;&amp; mat_array,
<a name="l00614"></a>00614             Matrix&lt;T1, Prop1, RowSymSparse, Allocator1&gt;&amp; mat_csr);
<a name="l00615"></a>00615 
<a name="l00616"></a>00616 
<a name="l00617"></a>00617   <span class="comment">/***********************************</span>
<a name="l00618"></a>00618 <span class="comment">   * From ArraySparse to ArraySparse *</span>
<a name="l00619"></a>00619 <span class="comment">   ***********************************/</span>
<a name="l00620"></a>00620 
<a name="l00621"></a>00621 
<a name="l00622"></a>00622   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00623"></a>00623            <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00624"></a>00624   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Matrix&lt;T0, Prop0, ArrayRowSymSparse, Allocator0&gt;&amp; A,
<a name="l00625"></a>00625             Matrix&lt;T1, Prop1, ArrayRowSparse, Allocator1&gt;&amp; B);
<a name="l00626"></a>00626 
<a name="l00627"></a>00627 
<a name="l00628"></a>00628   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00629"></a>00629            <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00630"></a>00630   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Matrix&lt;T0, Prop0, ArrayRowSymSparse, Allocator0&gt;&amp; A,
<a name="l00631"></a>00631             Matrix&lt;T1, Prop1, ArrayColSparse, Allocator1&gt;&amp; B);
<a name="l00632"></a>00632 
<a name="l00633"></a>00633 
<a name="l00634"></a>00634   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00635"></a>00635            <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00636"></a>00636   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Matrix&lt;T0, Prop0, ArrayRowSparse,Allocator0&gt;&amp; A,
<a name="l00637"></a>00637             Matrix&lt;T1, Prop1, ArrayColSparse, Allocator1&gt;&amp; B);
<a name="l00638"></a>00638 
<a name="l00639"></a>00639 
<a name="l00640"></a>00640   <span class="comment">/**************************</span>
<a name="l00641"></a>00641 <span class="comment">   * ComplexSparse matrices *</span>
<a name="l00642"></a>00642 <span class="comment">   **************************/</span>
<a name="l00643"></a>00643 
<a name="l00644"></a>00644 
<a name="l00645"></a>00645   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00646"></a>00646            <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00647"></a>00647   <span class="keywordtype">void</span>
<a name="l00648"></a>00648   Copy(<span class="keyword">const</span> Matrix&lt;T0, Prop0, ArrayRowComplexSparse, Allocator0&gt;&amp; mat_array,
<a name="l00649"></a>00649        Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;&amp; mat_csr);
<a name="l00650"></a>00650 
<a name="l00651"></a>00651 
<a name="l00652"></a>00652   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00653"></a>00653            <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00654"></a>00654   <span class="keywordtype">void</span>
<a name="l00655"></a>00655   Copy(<span class="keyword">const</span> Matrix&lt;T0, Prop0,
<a name="l00656"></a>00656        ArrayRowSymComplexSparse, Allocator0&gt;&amp; mat_array,
<a name="l00657"></a>00657        Matrix&lt;T1, Prop1, RowSymComplexSparse, Allocator1&gt;&amp; mat_csr);
<a name="l00658"></a>00658 
<a name="l00659"></a>00659 
<a name="l00660"></a>00660   <span class="comment">/***********************</span>
<a name="l00661"></a>00661 <span class="comment">   * GetSymmetricPattern *</span>
<a name="l00662"></a>00662 <span class="comment">   ***********************/</span>
<a name="l00663"></a>00663 
<a name="l00664"></a>00664 
<a name="l00665"></a>00665   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Storage, <span class="keyword">class </span>Allocator,
<a name="l00666"></a>00666            <span class="keyword">class </span>Tint, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00667"></a>00667   <span class="keywordtype">void</span> GetSymmetricPattern(<span class="keyword">const</span> Matrix&lt;T, Prop, Storage, Allocator&gt;&amp; A,
<a name="l00668"></a>00668                            Vector&lt;Tint, VectFull, Allocator2&gt;&amp; Ptr,
<a name="l00669"></a>00669                            Vector&lt;Tint, VectFull, Allocator3&gt;&amp; Ind);
<a name="l00670"></a>00670 
<a name="l00671"></a>00671 
<a name="l00672"></a>00672   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator, <span class="keyword">class</span> AllocI&gt;
<a name="l00673"></a>00673   <span class="keywordtype">void</span> GetSymmetricPattern(<span class="keyword">const</span> Matrix&lt;T, Prop, Storage, Allocator&gt;&amp; A,
<a name="l00674"></a>00674                            Matrix&lt;int, Symmetric, RowSymSparse, AllocI&gt;&amp; B);
<a name="l00675"></a>00675 
<a name="l00676"></a>00676 
<a name="l00677"></a>00677   <span class="comment">/*****************************************************</span>
<a name="l00678"></a>00678 <span class="comment">   * Conversion from sparse matrices to dense matrices *</span>
<a name="l00679"></a>00679 <span class="comment">   *****************************************************/</span>
<a name="l00680"></a>00680 
<a name="l00681"></a>00681 
<a name="l00682"></a>00682   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00683"></a>00683   <span class="keywordtype">void</span> Copy(Matrix&lt;T, Prop, RowSparse, Allocator1&gt;&amp; A,
<a name="l00684"></a>00684             Matrix&lt;T, Prop, RowMajor, Allocator2&gt;&amp; B);
<a name="l00685"></a>00685 
<a name="l00686"></a>00686 
<a name="l00687"></a>00687   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00688"></a>00688   <span class="keywordtype">void</span> Copy(Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator1&gt;&amp; A,
<a name="l00689"></a>00689             Matrix&lt;T, Prop, RowSymPacked, Allocator2&gt;&amp; B);
<a name="l00690"></a>00690 
<a name="l00691"></a>00691 
<a name="l00692"></a>00692   <span class="comment">/*****************************************************</span>
<a name="l00693"></a>00693 <span class="comment">   * Conversion from dense matrices to sparse matrices *</span>
<a name="l00694"></a>00694 <span class="comment">   *****************************************************/</span>
<a name="l00695"></a>00695 
<a name="l00696"></a>00696 
<a name="l00697"></a>00697   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00698"></a>00698   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a0e6c51ffa6a93517fa6300e693c8f810" title="Conversion from RowSymPacked to RowSymSparse.">ConvertToSparse</a>(<span class="keyword">const</span> Matrix&lt;T, Symmetric, RowSymPacked&gt;&amp; A,
<a name="l00699"></a>00699                        Matrix&lt;T, Symmetric, RowSymSparse&gt;&amp; B,
<a name="l00700"></a>00700                        <span class="keyword">const</span> T&amp; threshold);
<a name="l00701"></a>00701 
<a name="l00702"></a>00702 
<a name="l00703"></a>00703   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00704"></a>00704   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a0e6c51ffa6a93517fa6300e693c8f810" title="Conversion from RowSymPacked to RowSymSparse.">ConvertToSparse</a>(<span class="keyword">const</span> Matrix&lt;T, General, RowMajor&gt;&amp; A,
<a name="l00705"></a>00705                        Matrix&lt;T, General, ArrayRowSparse&gt;&amp; B,
<a name="l00706"></a>00706                        <span class="keyword">const</span> T&amp; threshold);
<a name="l00707"></a>00707 
<a name="l00708"></a>00708 
<a name="l00709"></a>00709   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00710"></a>00710   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a0e6c51ffa6a93517fa6300e693c8f810" title="Conversion from RowSymPacked to RowSymSparse.">ConvertToSparse</a>(<span class="keyword">const</span> Matrix&lt;T, General, RowMajor&gt;&amp; A,
<a name="l00711"></a>00711                        Matrix&lt;T, General, RowSparse&gt;&amp; B,
<a name="l00712"></a>00712                        <span class="keyword">const</span> T&amp; threshold);
<a name="l00713"></a>00713 
<a name="l00714"></a>00714 } <span class="comment">// namespace Seldon.</span>
<a name="l00715"></a>00715 
<a name="l00716"></a>00716 
<a name="l00717"></a>00717 <span class="preprocessor">#define SELDON_FILE_MATRIX_CONVERSIONS_HXX</span>
<a name="l00718"></a>00718 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
