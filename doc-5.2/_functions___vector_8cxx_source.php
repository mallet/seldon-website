<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/basic_functions/Functions_Vector.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00003"></a>00003 <span class="comment">// Copyright (C) 2010 INRIA</span>
<a name="l00004"></a>00004 <span class="comment">// Author(s): Marc Fragu</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00007"></a>00007 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00008"></a>00008 <span class="comment">//</span>
<a name="l00009"></a>00009 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00010"></a>00010 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00011"></a>00011 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00012"></a>00012 <span class="comment">// any later version.</span>
<a name="l00013"></a>00013 <span class="comment">//</span>
<a name="l00014"></a>00014 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00015"></a>00015 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00016"></a>00016 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00017"></a>00017 <span class="comment">// more details.</span>
<a name="l00018"></a>00018 <span class="comment">//</span>
<a name="l00019"></a>00019 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00020"></a>00020 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00021"></a>00021 
<a name="l00022"></a>00022 
<a name="l00023"></a>00023 <span class="preprocessor">#ifndef SELDON_FILE_FUNCTIONS_VECTOR_CXX</span>
<a name="l00024"></a>00024 <span class="preprocessor"></span><span class="preprocessor">#define SELDON_FILE_FUNCTIONS_VECTOR_CXX</span>
<a name="l00025"></a>00025 <span class="preprocessor"></span>
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 <span class="preprocessor">#include &quot;Functions_Vector.hxx&quot;</span>
<a name="l00028"></a>00028 
<a name="l00029"></a>00029 
<a name="l00030"></a>00030 <span class="comment">/*</span>
<a name="l00031"></a>00031 <span class="comment">  Functions defined in this file:</span>
<a name="l00032"></a>00032 <span class="comment"></span>
<a name="l00033"></a>00033 <span class="comment">  alpha X -&gt; X</span>
<a name="l00034"></a>00034 <span class="comment">  Mlt(alpha, X)</span>
<a name="l00035"></a>00035 <span class="comment"></span>
<a name="l00036"></a>00036 <span class="comment">  alpha X + Y -&gt; Y</span>
<a name="l00037"></a>00037 <span class="comment">  Add(alpha, X, Y)</span>
<a name="l00038"></a>00038 <span class="comment"></span>
<a name="l00039"></a>00039 <span class="comment">  X -&gt; Y</span>
<a name="l00040"></a>00040 <span class="comment">  Copy(X, Y)</span>
<a name="l00041"></a>00041 <span class="comment"></span>
<a name="l00042"></a>00042 <span class="comment">  X &lt;-&gt; Y</span>
<a name="l00043"></a>00043 <span class="comment">  Swap(X, Y)</span>
<a name="l00044"></a>00044 <span class="comment"></span>
<a name="l00045"></a>00045 <span class="comment">  X.Y</span>
<a name="l00046"></a>00046 <span class="comment">  DotProd(X, Y)</span>
<a name="l00047"></a>00047 <span class="comment">  DotProdConj(X, Y)</span>
<a name="l00048"></a>00048 <span class="comment"></span>
<a name="l00049"></a>00049 <span class="comment">  ||X||</span>
<a name="l00050"></a>00050 <span class="comment">  Norm1(X)</span>
<a name="l00051"></a>00051 <span class="comment">  Norm2(X)</span>
<a name="l00052"></a>00052 <span class="comment">  GetMaxAbsIndex(X)</span>
<a name="l00053"></a>00053 <span class="comment"></span>
<a name="l00054"></a>00054 <span class="comment">  Omega X</span>
<a name="l00055"></a>00055 <span class="comment">  GenRot(x, y, cos, sin)</span>
<a name="l00056"></a>00056 <span class="comment">  ApplyRot(x, y, cos, sin)</span>
<a name="l00057"></a>00057 <span class="comment"></span>
<a name="l00058"></a>00058 <span class="comment">*/</span>
<a name="l00059"></a>00059 
<a name="l00060"></a>00060 
<a name="l00061"></a>00061 <span class="keyword">namespace </span>Seldon
<a name="l00062"></a>00062 {
<a name="l00063"></a>00063 
<a name="l00064"></a>00064 
<a name="l00066"></a>00066   <span class="comment">// MLT //</span>
<a name="l00067"></a>00067 
<a name="l00068"></a>00068 
<a name="l00069"></a>00069   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00070"></a>00070             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00071"></a>00071   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(<span class="keyword">const</span> T0 alpha, Vector&lt;T1, Storage1, Allocator1&gt;&amp; X)
<a name="l00072"></a>00072   {
<a name="l00073"></a>00073     X *= alpha;
<a name="l00074"></a>00074   }
<a name="l00075"></a>00075 
<a name="l00076"></a>00076 
<a name="l00077"></a>00077   <span class="comment">// MLT //</span>
<a name="l00079"></a>00079 <span class="comment"></span>
<a name="l00080"></a>00080 
<a name="l00082"></a>00082   <span class="comment">// ADD //</span>
<a name="l00083"></a>00083 
<a name="l00084"></a>00084 
<a name="l00085"></a>00085   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00086"></a>00086             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00087"></a>00087             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00088"></a>00088   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00089"></a>00089            <span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00090"></a>00090            Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y)
<a name="l00091"></a>00091   {
<a name="l00092"></a>00092     <span class="keywordflow">if</span> (alpha != T0(0))
<a name="l00093"></a>00093       {
<a name="l00094"></a>00094         T1 alpha_ = alpha;
<a name="l00095"></a>00095 
<a name="l00096"></a>00096         <span class="keywordtype">int</span> ma = X.GetM();
<a name="l00097"></a>00097 
<a name="l00098"></a>00098 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00099"></a>00099 <span class="preprocessor"></span>        <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(X, Y, <span class="stringliteral">&quot;Add(alpha, X, Y)&quot;</span>);
<a name="l00100"></a>00100 <span class="preprocessor">#endif</span>
<a name="l00101"></a>00101 <span class="preprocessor"></span>
<a name="l00102"></a>00102         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; ma; i++)
<a name="l00103"></a>00103           Y(i) += alpha_ * X(i);
<a name="l00104"></a>00104       }
<a name="l00105"></a>00105   }
<a name="l00106"></a>00106 
<a name="l00107"></a>00107 
<a name="l00108"></a>00108   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00109"></a>00109             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00110"></a>00110             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00111"></a>00111   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00112"></a>00112            <span class="keyword">const</span> Vector&lt;T1, PETScSeq, Allocator1&gt;&amp; X,
<a name="l00113"></a>00113            Vector&lt;T2, PETScSeq, Allocator2&gt;&amp; Y)
<a name="l00114"></a>00114   {
<a name="l00115"></a>00115     <span class="keywordflow">if</span> (alpha != T0(0))
<a name="l00116"></a>00116       {
<a name="l00117"></a>00117         T1 alpha_ = alpha;
<a name="l00118"></a>00118 
<a name="l00119"></a>00119         <span class="keywordtype">int</span> ma = X.GetM();
<a name="l00120"></a>00120 
<a name="l00121"></a>00121 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00122"></a>00122 <span class="preprocessor"></span>        <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(X, Y, <span class="stringliteral">&quot;Add(alpha, X, Y)&quot;</span>);
<a name="l00123"></a>00123 <span class="preprocessor">#endif</span>
<a name="l00124"></a>00124 <span class="preprocessor"></span>
<a name="l00125"></a>00125         VecAXPY(Y.GetPetscVector(), alpha_, X.GetPetscVector());
<a name="l00126"></a>00126       }
<a name="l00127"></a>00127   }
<a name="l00128"></a>00128 
<a name="l00129"></a>00129 
<a name="l00130"></a>00130   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00131"></a>00131             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00132"></a>00132             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00133"></a>00133   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00134"></a>00134            <span class="keyword">const</span> Vector&lt;T1, PETScPar, Allocator1&gt;&amp; X,
<a name="l00135"></a>00135            Vector&lt;T2, PETScPar, Allocator2&gt;&amp; Y)
<a name="l00136"></a>00136   {
<a name="l00137"></a>00137     <span class="keywordflow">if</span> (alpha != T0(0))
<a name="l00138"></a>00138       {
<a name="l00139"></a>00139         T1 alpha_ = alpha;
<a name="l00140"></a>00140 
<a name="l00141"></a>00141         <span class="keywordtype">int</span> ma = X.GetM();
<a name="l00142"></a>00142 
<a name="l00143"></a>00143 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00144"></a>00144 <span class="preprocessor"></span>        <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(X, Y, <span class="stringliteral">&quot;Add(alpha, X, Y)&quot;</span>);
<a name="l00145"></a>00145 <span class="preprocessor">#endif</span>
<a name="l00146"></a>00146 <span class="preprocessor"></span>
<a name="l00147"></a>00147         VecAXPY(Y.GetPetscVector(), alpha_, X.GetPetscVector());
<a name="l00148"></a>00148       }
<a name="l00149"></a>00149   }
<a name="l00150"></a>00150 
<a name="l00151"></a>00151 
<a name="l00152"></a>00152 
<a name="l00153"></a>00153   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00154"></a>00154             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00155"></a>00155             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00156"></a>00156   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00157"></a>00157            <span class="keyword">const</span> Vector&lt;T1, VectSparse, Allocator1&gt;&amp; X,
<a name="l00158"></a>00158            Vector&lt;T2, VectSparse, Allocator2&gt;&amp; Y)
<a name="l00159"></a>00159   {
<a name="l00160"></a>00160     <span class="keywordflow">if</span> (alpha != T0(0))
<a name="l00161"></a>00161       {
<a name="l00162"></a>00162         Vector&lt;T1, VectSparse, Allocator1&gt; Xalpha = X;
<a name="l00163"></a>00163         Xalpha *= alpha;
<a name="l00164"></a>00164         Y.AddInteractionRow(Xalpha.GetSize(),
<a name="l00165"></a>00165                             Xalpha.GetIndex(), Xalpha.GetData(), <span class="keyword">true</span>);
<a name="l00166"></a>00166       }
<a name="l00167"></a>00167   }
<a name="l00168"></a>00168 
<a name="l00169"></a>00169 
<a name="l00170"></a>00170   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00171"></a>00171             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00172"></a>00172             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00173"></a>00173   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00174"></a>00174            <span class="keyword">const</span> Vector&lt;T1, Collection, Allocator1&gt;&amp; X,
<a name="l00175"></a>00175            Vector&lt;T2, Collection, Allocator2&gt;&amp; Y)
<a name="l00176"></a>00176   {
<a name="l00177"></a>00177 
<a name="l00178"></a>00178 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00179"></a>00179 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(X, Y, <span class="stringliteral">&quot;Add(X, Y)&quot;</span>, <span class="stringliteral">&quot;X + Y&quot;</span>);
<a name="l00180"></a>00180 <span class="preprocessor">#endif</span>
<a name="l00181"></a>00181 <span class="preprocessor"></span>
<a name="l00182"></a>00182     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetNvector(); i++)
<a name="l00183"></a>00183       <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(alpha, X.GetVector(i), Y.GetVector(i));
<a name="l00184"></a>00184   }
<a name="l00185"></a>00185 
<a name="l00186"></a>00186 
<a name="l00187"></a>00187   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00188"></a>00188             <span class="keyword">class </span>T1, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U1&gt; <span class="keyword">class </span>Allocator1,
<a name="l00189"></a>00189             <span class="keyword">class </span>T2, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U2&gt; <span class="keyword">class </span>Allocator2&gt;
<a name="l00190"></a>00190   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00191"></a>00191            <span class="keyword">const</span>
<a name="l00192"></a>00192            Vector&lt;FloatDouble, DenseSparseCollection, Allocator1&lt;T1&gt; &gt;&amp; X,
<a name="l00193"></a>00193            Vector&lt;FloatDouble, DenseSparseCollection, Allocator2&lt;T2&gt; &gt;&amp; Y)
<a name="l00194"></a>00194   {
<a name="l00195"></a>00195 
<a name="l00196"></a>00196 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00197"></a>00197 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(X, Y, <span class="stringliteral">&quot;Add(X, Y)&quot;</span>);
<a name="l00198"></a>00198 <span class="preprocessor">#endif</span>
<a name="l00199"></a>00199 <span class="preprocessor"></span>
<a name="l00200"></a>00200     <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(alpha, X.GetFloatDense(), Y.GetFloatDense());
<a name="l00201"></a>00201     <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(alpha, X.GetFloatSparse(), Y.GetFloatSparse());
<a name="l00202"></a>00202     <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(alpha, X.GetDoubleDense(), Y.GetDoubleDense());
<a name="l00203"></a>00203     <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(alpha, X.GetDoubleSparse(), Y.GetDoubleSparse());
<a name="l00204"></a>00204   }
<a name="l00205"></a>00205 
<a name="l00206"></a>00206 
<a name="l00207"></a>00207   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00208"></a>00208             <span class="keyword">class </span>T1, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U1&gt; <span class="keyword">class </span>Allocator1,
<a name="l00209"></a>00209             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00210"></a>00210   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00211"></a>00211            <span class="keyword">const</span>
<a name="l00212"></a>00212            Vector&lt;FloatDouble, DenseSparseCollection, Allocator1&lt;T1&gt; &gt;&amp; X,
<a name="l00213"></a>00213            Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y)
<a name="l00214"></a>00214   {
<a name="l00215"></a>00215     <span class="keywordflow">if</span> (alpha != T0(0))
<a name="l00216"></a>00216       {
<a name="l00217"></a>00217         <span class="keywordtype">double</span> alpha_ = alpha;
<a name="l00218"></a>00218 
<a name="l00219"></a>00219         <span class="keywordtype">int</span> ma = X.GetM();
<a name="l00220"></a>00220 
<a name="l00221"></a>00221 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00222"></a>00222 <span class="preprocessor"></span>        <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(X, Y, <span class="stringliteral">&quot;Add(alpha, X, Y)&quot;</span>);
<a name="l00223"></a>00223 <span class="preprocessor">#endif</span>
<a name="l00224"></a>00224 <span class="preprocessor"></span>
<a name="l00225"></a>00225         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; ma; i++)
<a name="l00226"></a>00226           Y(i) += alpha_ * X(i);
<a name="l00227"></a>00227 
<a name="l00228"></a>00228       }
<a name="l00229"></a>00229   }
<a name="l00230"></a>00230 
<a name="l00231"></a>00231 
<a name="l00232"></a>00232   <span class="comment">// ADD //</span>
<a name="l00234"></a>00234 <span class="comment"></span>
<a name="l00235"></a>00235 
<a name="l00237"></a>00237   <span class="comment">// COPY //</span>
<a name="l00238"></a>00238 
<a name="l00239"></a>00239 
<a name="l00240"></a>00240   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00241"></a>00241             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00242"></a>00242   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00243"></a>00243             Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y)
<a name="l00244"></a>00244   {
<a name="l00245"></a>00245     Y.Copy(X);
<a name="l00246"></a>00246   }
<a name="l00247"></a>00247 
<a name="l00248"></a>00248 
<a name="l00249"></a>00249   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00250"></a>00250             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00251"></a>00251   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Vector&lt;T1, Collection, Allocator1&gt;&amp; X,
<a name="l00252"></a>00252             Vector&lt;T2, VectFull, Allocator2&gt;&amp; Y)
<a name="l00253"></a>00253   {
<a name="l00254"></a>00254     Y.Clear();
<a name="l00255"></a>00255     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetNvector(); i++)
<a name="l00256"></a>00256       Y.PushBack(X.GetVector(i));
<a name="l00257"></a>00257   }
<a name="l00258"></a>00258 
<a name="l00259"></a>00259 
<a name="l00260"></a>00260   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Alloc1, <span class="keyword">class</span> Alloc2&gt;
<a name="l00261"></a>00261   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Vector&lt;T, PETScPar, Alloc1&gt;&amp; A,
<a name="l00262"></a>00262             Vector&lt;T, VectFull, Alloc2&gt;&amp; B)
<a name="l00263"></a>00263   {
<a name="l00264"></a>00264     B.Reallocate(A.GetLocalM());
<a name="l00265"></a>00265     T *local_data;
<a name="l00266"></a>00266     VecGetArray(A.GetPetscVector(), &amp;local_data);
<a name="l00267"></a>00267     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetLocalM(); i++)
<a name="l00268"></a>00268       B(i) = local_data[i];
<a name="l00269"></a>00269     VecRestoreArray(A.GetPetscVector(), &amp;local_data);
<a name="l00270"></a>00270   }
<a name="l00271"></a>00271 
<a name="l00272"></a>00272 
<a name="l00273"></a>00273   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Alloc1, <span class="keyword">class</span> Alloc2&gt;
<a name="l00274"></a>00274   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Vector&lt;T, VectFull, Alloc1&gt;&amp; A,
<a name="l00275"></a>00275             Vector&lt;T, PETScPar, Alloc2&gt;&amp; B)
<a name="l00276"></a>00276   {
<a name="l00277"></a>00277     T *local_data;
<a name="l00278"></a>00278     VecGetArray(B.GetPetscVector(), &amp;local_data);
<a name="l00279"></a>00279     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetM(); i++)
<a name="l00280"></a>00280       local_data[i] = A(i);
<a name="l00281"></a>00281     VecRestoreArray(B.GetPetscVector(), &amp;local_data);
<a name="l00282"></a>00282   }
<a name="l00283"></a>00283 
<a name="l00284"></a>00284 
<a name="l00285"></a>00285 
<a name="l00286"></a>00286   <span class="comment">// COPY //</span>
<a name="l00288"></a>00288 <span class="comment"></span>
<a name="l00289"></a>00289 
<a name="l00291"></a>00291   <span class="comment">// SWAP //</span>
<a name="l00292"></a>00292 
<a name="l00293"></a>00293 
<a name="l00294"></a>00294   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00295"></a>00295   <span class="keywordtype">void</span> Swap(Vector&lt;T, Storage, Allocator&gt;&amp; X,
<a name="l00296"></a>00296             Vector&lt;T, Storage, Allocator&gt;&amp; Y)
<a name="l00297"></a>00297   {
<a name="l00298"></a>00298     <span class="keywordtype">int</span> nx = X.GetM();
<a name="l00299"></a>00299     T* data = X.GetData();
<a name="l00300"></a>00300     X.Nullify();
<a name="l00301"></a>00301     X.SetData(Y.GetM(), Y.GetData());
<a name="l00302"></a>00302     Y.Nullify();
<a name="l00303"></a>00303     Y.SetData(nx, data);
<a name="l00304"></a>00304   }
<a name="l00305"></a>00305 
<a name="l00306"></a>00306 
<a name="l00307"></a>00307   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00308"></a>00308   <span class="keywordtype">void</span> Swap(Vector&lt;T, VectSparse, Allocator&gt;&amp; X,
<a name="l00309"></a>00309             Vector&lt;T, VectSparse, Allocator&gt;&amp; Y)
<a name="l00310"></a>00310   {
<a name="l00311"></a>00311     <span class="keywordtype">int</span> nx = X.GetM();
<a name="l00312"></a>00312     T* data = X.GetData();
<a name="l00313"></a>00313     <span class="keywordtype">int</span>* index = X.GetIndex();
<a name="l00314"></a>00314     X.Nullify();
<a name="l00315"></a>00315     X.SetData(Y.GetM(), Y.GetData(), Y.GetIndex());
<a name="l00316"></a>00316     Y.Nullify();
<a name="l00317"></a>00317     Y.SetData(nx, data, index);
<a name="l00318"></a>00318   }
<a name="l00319"></a>00319 
<a name="l00320"></a>00320 
<a name="l00321"></a>00321   <span class="comment">// SWAP //</span>
<a name="l00323"></a>00323 <span class="comment"></span>
<a name="l00324"></a>00324 
<a name="l00326"></a>00326   <span class="comment">// DOTPROD //</span>
<a name="l00327"></a>00327 
<a name="l00328"></a>00328 
<a name="l00330"></a>00330   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00331"></a>00331            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00332"></a>00332   T1 <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(<span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00333"></a>00333              <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y)
<a name="l00334"></a>00334   {
<a name="l00335"></a>00335     T1 value;
<a name="l00336"></a>00336     SetComplexZero(value);
<a name="l00337"></a>00337 
<a name="l00338"></a>00338 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00339"></a>00339 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(X, Y, <span class="stringliteral">&quot;DotProd(X, Y)&quot;</span>);
<a name="l00340"></a>00340 <span class="preprocessor">#endif</span>
<a name="l00341"></a>00341 <span class="preprocessor"></span>
<a name="l00342"></a>00342     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetM(); i++)
<a name="l00343"></a>00343       value += X(i) * Y(i);
<a name="l00344"></a>00344 
<a name="l00345"></a>00345     <span class="keywordflow">return</span> value;
<a name="l00346"></a>00346   }
<a name="l00347"></a>00347 
<a name="l00348"></a>00348 
<a name="l00350"></a>00350   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00351"></a>00351            <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00352"></a>00352   <span class="keyword">typename</span> T1::value_type <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(<span class="keyword">const</span> Vector&lt;T1, Collection, Allocator1&gt;&amp; X,
<a name="l00353"></a>00353                                   <span class="keyword">const</span> Vector&lt;T2, Collection, Allocator2&gt;&amp; Y)
<a name="l00354"></a>00354   {
<a name="l00355"></a>00355     <span class="keyword">typename</span> T1::value_type value(0);
<a name="l00356"></a>00356 
<a name="l00357"></a>00357 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00358"></a>00358 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(X, Y, <span class="stringliteral">&quot;DotProd(X, Y)&quot;</span>, <span class="stringliteral">&quot;&lt;X, Y&gt;&quot;</span>);
<a name="l00359"></a>00359 <span class="preprocessor">#endif</span>
<a name="l00360"></a>00360 <span class="preprocessor"></span>
<a name="l00361"></a>00361     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetNvector(); i++)
<a name="l00362"></a>00362       value += <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(X.GetVector(i), Y.GetVector(i));
<a name="l00363"></a>00363     <span class="keywordflow">return</span> value;
<a name="l00364"></a>00364   }
<a name="l00365"></a>00365 
<a name="l00366"></a>00366 
<a name="l00368"></a>00368   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U1&gt; <span class="keyword">class </span>Allocator1,
<a name="l00369"></a>00369            <span class="keyword">class </span>T2, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U2&gt; <span class="keyword">class </span>Allocator2&gt;
<a name="l00370"></a>00370   <span class="keywordtype">double</span>
<a name="l00371"></a>00371   <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(<span class="keyword">const</span>
<a name="l00372"></a>00372           Vector&lt;FloatDouble, DenseSparseCollection, Allocator1&lt;T1&gt; &gt;&amp; X,
<a name="l00373"></a>00373           <span class="keyword">const</span>
<a name="l00374"></a>00374           Vector&lt;FloatDouble, DenseSparseCollection, Allocator2&lt;T2&gt; &gt;&amp; Y)
<a name="l00375"></a>00375   {
<a name="l00376"></a>00376 
<a name="l00377"></a>00377 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00378"></a>00378 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(X, Y, <span class="stringliteral">&quot;DotProd(X, Y)&quot;</span>);
<a name="l00379"></a>00379 <span class="preprocessor">#endif</span>
<a name="l00380"></a>00380 <span class="preprocessor"></span>
<a name="l00381"></a>00381     <span class="keywordtype">double</span> value(0.);
<a name="l00382"></a>00382     value += <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(X.GetFloatDense(), Y.GetFloatDense());
<a name="l00383"></a>00383     value += <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(X.GetFloatSparse(), Y.GetFloatSparse());
<a name="l00384"></a>00384     value += <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(X.GetDoubleDense(), Y.GetDoubleDense());
<a name="l00385"></a>00385     value += <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(X.GetDoubleSparse(), Y.GetDoubleSparse());
<a name="l00386"></a>00386     <span class="keywordflow">return</span> value;
<a name="l00387"></a>00387   }
<a name="l00388"></a>00388 
<a name="l00389"></a>00389 
<a name="l00391"></a>00391   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00392"></a>00392            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00393"></a>00393   T1 <a class="code" href="namespace_seldon.php#afd8cf3dd207cb4727edacda863a3f055" title="Scalar product between two vectors.">DotProdConj</a>(<span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00394"></a>00394                  <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y)
<a name="l00395"></a>00395   {
<a name="l00396"></a>00396     <span class="keywordflow">return</span> <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(X, Y);
<a name="l00397"></a>00397   }
<a name="l00398"></a>00398 
<a name="l00399"></a>00399 
<a name="l00401"></a>00401   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00402"></a>00402            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00403"></a>00403   complex&lt;T1&gt; <a class="code" href="namespace_seldon.php#afd8cf3dd207cb4727edacda863a3f055" title="Scalar product between two vectors.">DotProdConj</a>(<span class="keyword">const</span> Vector&lt;complex&lt;T1&gt;, Storage1, Allocator1&gt;&amp; X,
<a name="l00404"></a>00404                           <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y)
<a name="l00405"></a>00405   {
<a name="l00406"></a>00406     complex&lt;T1&gt; value;
<a name="l00407"></a>00407     SetComplexZero(value);
<a name="l00408"></a>00408 
<a name="l00409"></a>00409 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00410"></a>00410 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(X, Y, <span class="stringliteral">&quot;DotProdConj(X, Y)&quot;</span>);
<a name="l00411"></a>00411 <span class="preprocessor">#endif</span>
<a name="l00412"></a>00412 <span class="preprocessor"></span>
<a name="l00413"></a>00413     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetM(); i++)
<a name="l00414"></a>00414       value += conj(X(i)) * Y(i);
<a name="l00415"></a>00415 
<a name="l00416"></a>00416     <span class="keywordflow">return</span> value;
<a name="l00417"></a>00417   }
<a name="l00418"></a>00418 
<a name="l00419"></a>00419 
<a name="l00421"></a>00421   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00422"></a>00422            <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00423"></a>00423   T1 <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(<span class="keyword">const</span> Vector&lt;T1, VectSparse, Allocator1&gt;&amp; X,
<a name="l00424"></a>00424              <span class="keyword">const</span> Vector&lt;T2, VectSparse, Allocator2&gt;&amp; Y)
<a name="l00425"></a>00425   {
<a name="l00426"></a>00426     T1 value;
<a name="l00427"></a>00427     SetComplexZero(value);
<a name="l00428"></a>00428 
<a name="l00429"></a>00429     <span class="keywordtype">int</span> size_x = X.GetSize();
<a name="l00430"></a>00430     <span class="keywordtype">int</span> size_y = Y.GetSize();
<a name="l00431"></a>00431     <span class="keywordtype">int</span> kx = 0, ky = 0, pos_x;
<a name="l00432"></a>00432     <span class="keywordflow">while</span> (kx &lt; size_x)
<a name="l00433"></a>00433       {
<a name="l00434"></a>00434         pos_x = X.Index(kx);
<a name="l00435"></a>00435         <span class="keywordflow">while</span> (ky &lt; size_y &amp;&amp; Y.Index(ky) &lt; pos_x)
<a name="l00436"></a>00436           ky++;
<a name="l00437"></a>00437 
<a name="l00438"></a>00438         <span class="keywordflow">if</span> (ky &lt; size_y &amp;&amp; Y.Index(ky) == pos_x)
<a name="l00439"></a>00439           value += X.Value(kx) * Y.Value(ky);
<a name="l00440"></a>00440 
<a name="l00441"></a>00441         kx++;
<a name="l00442"></a>00442       }
<a name="l00443"></a>00443 
<a name="l00444"></a>00444     <span class="keywordflow">return</span> value;
<a name="l00445"></a>00445   }
<a name="l00446"></a>00446 
<a name="l00447"></a>00447 
<a name="l00449"></a>00449   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00450"></a>00450            <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00451"></a>00451   complex&lt;T1&gt;
<a name="l00452"></a>00452   <a class="code" href="namespace_seldon.php#afd8cf3dd207cb4727edacda863a3f055" title="Scalar product between two vectors.">DotProdConj</a>(<span class="keyword">const</span> Vector&lt;complex&lt;T1&gt;, VectSparse, Allocator1&gt;&amp; X,
<a name="l00453"></a>00453               <span class="keyword">const</span> Vector&lt;T2, VectSparse, Allocator2&gt;&amp; Y)
<a name="l00454"></a>00454   {
<a name="l00455"></a>00455     complex&lt;T1&gt; value(0, 0);
<a name="l00456"></a>00456 
<a name="l00457"></a>00457     <span class="keywordtype">int</span> size_x = X.GetSize();
<a name="l00458"></a>00458     <span class="keywordtype">int</span> size_y = Y.GetSize();
<a name="l00459"></a>00459     <span class="keywordtype">int</span> kx = 0, ky = 0, pos_x;
<a name="l00460"></a>00460     <span class="keywordflow">while</span> (kx &lt; size_x)
<a name="l00461"></a>00461       {
<a name="l00462"></a>00462         pos_x = X.Index(kx);
<a name="l00463"></a>00463         <span class="keywordflow">while</span> (ky &lt; size_y &amp;&amp; Y.Index(ky) &lt; pos_x)
<a name="l00464"></a>00464           ky++;
<a name="l00465"></a>00465 
<a name="l00466"></a>00466         <span class="keywordflow">if</span> (ky &lt; size_y &amp;&amp; Y.Index(ky) == pos_x)
<a name="l00467"></a>00467           value += conj(X.Value(kx)) * Y.Value(ky);
<a name="l00468"></a>00468 
<a name="l00469"></a>00469         kx++;
<a name="l00470"></a>00470       }
<a name="l00471"></a>00471 
<a name="l00472"></a>00472     <span class="keywordflow">return</span> value;
<a name="l00473"></a>00473   }
<a name="l00474"></a>00474 
<a name="l00475"></a>00475 
<a name="l00476"></a>00476   <span class="comment">// DOTPROD //</span>
<a name="l00478"></a>00478 <span class="comment"></span>
<a name="l00479"></a>00479 
<a name="l00481"></a>00481   <span class="comment">// NORM1 //</span>
<a name="l00482"></a>00482 
<a name="l00483"></a>00483 
<a name="l00484"></a>00484   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Storage1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00485"></a>00485   T1 <a class="code" href="namespace_seldon.php#afefce2ae4cd3147f863955efddc4184c" title="Returns the 1-norm of a matrix.">Norm1</a>(<span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X)
<a name="l00486"></a>00486   {
<a name="l00487"></a>00487     T1 value(0);
<a name="l00488"></a>00488 
<a name="l00489"></a>00489     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetM(); i++)
<a name="l00490"></a>00490       value += abs(X(i));
<a name="l00491"></a>00491 
<a name="l00492"></a>00492     <span class="keywordflow">return</span> value;
<a name="l00493"></a>00493   }
<a name="l00494"></a>00494 
<a name="l00495"></a>00495 
<a name="l00496"></a>00496   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Storage1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00497"></a>00497   T1 <a class="code" href="namespace_seldon.php#afefce2ae4cd3147f863955efddc4184c" title="Returns the 1-norm of a matrix.">Norm1</a>(<span class="keyword">const</span> Vector&lt;complex&lt;T1&gt;, Storage1, Allocator1&gt;&amp; X)
<a name="l00498"></a>00498   {
<a name="l00499"></a>00499     T1 value(0);
<a name="l00500"></a>00500 
<a name="l00501"></a>00501     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetM(); i++)
<a name="l00502"></a>00502       value += abs(X(i));
<a name="l00503"></a>00503 
<a name="l00504"></a>00504     <span class="keywordflow">return</span> value;
<a name="l00505"></a>00505   }
<a name="l00506"></a>00506 
<a name="l00507"></a>00507 
<a name="l00508"></a>00508   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00509"></a>00509   T1 <a class="code" href="namespace_seldon.php#afefce2ae4cd3147f863955efddc4184c" title="Returns the 1-norm of a matrix.">Norm1</a>(<span class="keyword">const</span> Vector&lt;T1, VectSparse, Allocator1&gt;&amp; X)
<a name="l00510"></a>00510   {
<a name="l00511"></a>00511     T1 value(0);
<a name="l00512"></a>00512 
<a name="l00513"></a>00513     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetSize(); i++)
<a name="l00514"></a>00514       value += abs(X.Value(i));
<a name="l00515"></a>00515 
<a name="l00516"></a>00516     <span class="keywordflow">return</span> value;
<a name="l00517"></a>00517   }
<a name="l00518"></a>00518 
<a name="l00519"></a>00519 
<a name="l00520"></a>00520   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00521"></a>00521   T1 <a class="code" href="namespace_seldon.php#afefce2ae4cd3147f863955efddc4184c" title="Returns the 1-norm of a matrix.">Norm1</a>(<span class="keyword">const</span> Vector&lt;complex&lt;T1&gt;, VectSparse, Allocator1&gt;&amp; X)
<a name="l00522"></a>00522   {
<a name="l00523"></a>00523     T1 value(0);
<a name="l00524"></a>00524 
<a name="l00525"></a>00525     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetSize(); i++)
<a name="l00526"></a>00526       value += abs(X.Value(i));
<a name="l00527"></a>00527 
<a name="l00528"></a>00528     <span class="keywordflow">return</span> value;
<a name="l00529"></a>00529   }
<a name="l00530"></a>00530 
<a name="l00531"></a>00531 
<a name="l00532"></a>00532   <span class="comment">// NORM1 //</span>
<a name="l00534"></a>00534 <span class="comment"></span>
<a name="l00535"></a>00535 
<a name="l00537"></a>00537   <span class="comment">// NORM2 //</span>
<a name="l00538"></a>00538 
<a name="l00539"></a>00539 
<a name="l00540"></a>00540   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Storage1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00541"></a>00541   T1 Norm2(<span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X)
<a name="l00542"></a>00542   {
<a name="l00543"></a>00543     T1 value(0);
<a name="l00544"></a>00544 
<a name="l00545"></a>00545     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetM(); i++)
<a name="l00546"></a>00546       value += X(i) * X(i);
<a name="l00547"></a>00547 
<a name="l00548"></a>00548     <span class="keywordflow">return</span> sqrt(value);
<a name="l00549"></a>00549   }
<a name="l00550"></a>00550 
<a name="l00551"></a>00551 
<a name="l00552"></a>00552   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Storage1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00553"></a>00553   T1 Norm2(<span class="keyword">const</span> Vector&lt;complex&lt;T1&gt;, Storage1, Allocator1&gt;&amp; X)
<a name="l00554"></a>00554   {
<a name="l00555"></a>00555     T1 value(0);
<a name="l00556"></a>00556 
<a name="l00557"></a>00557     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetM(); i++)
<a name="l00558"></a>00558       value += real(X(i) * conj(X(i)));
<a name="l00559"></a>00559 
<a name="l00560"></a>00560     <span class="keywordflow">return</span> sqrt(value);
<a name="l00561"></a>00561   }
<a name="l00562"></a>00562 
<a name="l00563"></a>00563 
<a name="l00564"></a>00564   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00565"></a>00565   T1 Norm2(<span class="keyword">const</span> Vector&lt;T1, VectSparse, Allocator1&gt;&amp; X)
<a name="l00566"></a>00566   {
<a name="l00567"></a>00567     T1 value(0);
<a name="l00568"></a>00568 
<a name="l00569"></a>00569     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetSize(); i++)
<a name="l00570"></a>00570       value += X.Value(i) * X.Value(i);
<a name="l00571"></a>00571 
<a name="l00572"></a>00572     <span class="keywordflow">return</span> sqrt(value);
<a name="l00573"></a>00573   }
<a name="l00574"></a>00574 
<a name="l00575"></a>00575 
<a name="l00576"></a>00576   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00577"></a>00577   T1 Norm2(<span class="keyword">const</span> Vector&lt;complex&lt;T1&gt;, VectSparse, Allocator1&gt;&amp; X)
<a name="l00578"></a>00578   {
<a name="l00579"></a>00579     T1 value(0);
<a name="l00580"></a>00580 
<a name="l00581"></a>00581     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetSize(); i++)
<a name="l00582"></a>00582       value += real(X.Value(i) * conj(X.Value(i)));
<a name="l00583"></a>00583 
<a name="l00584"></a>00584     <span class="keywordflow">return</span> sqrt(value);
<a name="l00585"></a>00585   }
<a name="l00586"></a>00586 
<a name="l00587"></a>00587 
<a name="l00588"></a>00588   <span class="comment">// NORM2 //</span>
<a name="l00590"></a>00590 <span class="comment"></span>
<a name="l00591"></a>00591 
<a name="l00593"></a>00593   <span class="comment">// GETMAXABSINDEX //</span>
<a name="l00594"></a>00594 
<a name="l00595"></a>00595 
<a name="l00596"></a>00596   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00597"></a>00597   <span class="keywordtype">int</span> GetMaxAbsIndex(<span class="keyword">const</span> Vector&lt;T, Storage, Allocator&gt;&amp; X)
<a name="l00598"></a>00598   {
<a name="l00599"></a>00599     <span class="keywordflow">return</span> X.GetNormInfIndex();
<a name="l00600"></a>00600   }
<a name="l00601"></a>00601 
<a name="l00602"></a>00602 
<a name="l00603"></a>00603   <span class="comment">// GETMAXABSINDEX //</span>
<a name="l00605"></a>00605 <span class="comment"></span>
<a name="l00606"></a>00606 
<a name="l00608"></a>00608   <span class="comment">// APPLYROT //</span>
<a name="l00609"></a>00609 
<a name="l00610"></a>00610 
<a name="l00612"></a>00612   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00613"></a>00613   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a280476959b456ad5aa0aab55257f5c03" title="Computation of rotation between two points.">GenRot</a>(T&amp; a_in, T&amp; b_in, T&amp; c_, T&amp; s_)
<a name="l00614"></a>00614   {
<a name="l00615"></a>00615     <span class="comment">// Old BLAS version.</span>
<a name="l00616"></a>00616     T roe;
<a name="l00617"></a>00617     <span class="keywordflow">if</span> (abs(a_in) &gt; abs(b_in))
<a name="l00618"></a>00618       roe = a_in;
<a name="l00619"></a>00619     <span class="keywordflow">else</span>
<a name="l00620"></a>00620       roe = b_in;
<a name="l00621"></a>00621 
<a name="l00622"></a>00622     T scal = abs(a_in) + abs(b_in);
<a name="l00623"></a>00623     T r, z;
<a name="l00624"></a>00624     <span class="keywordflow">if</span> (scal != T(0))
<a name="l00625"></a>00625       {
<a name="l00626"></a>00626         T a_scl = a_in / scal;
<a name="l00627"></a>00627         T b_scl = b_in / scal;
<a name="l00628"></a>00628         r = scal * sqrt(a_scl * a_scl + b_scl * b_scl);
<a name="l00629"></a>00629         <span class="keywordflow">if</span> (roe &lt; T(0))
<a name="l00630"></a>00630           r *= T(-1);
<a name="l00631"></a>00631 
<a name="l00632"></a>00632         c_ = a_in / r;
<a name="l00633"></a>00633         s_ = b_in / r;
<a name="l00634"></a>00634         z = T(1);
<a name="l00635"></a>00635         <span class="keywordflow">if</span> (abs(a_in) &gt; abs(b_in))
<a name="l00636"></a>00636           z = s_;
<a name="l00637"></a>00637         <span class="keywordflow">else</span> <span class="keywordflow">if</span> (abs(b_in) &gt;= abs(a_in) &amp;&amp; c_ != T(0))
<a name="l00638"></a>00638           z = T(1) / c_;
<a name="l00639"></a>00639       }
<a name="l00640"></a>00640     <span class="keywordflow">else</span>
<a name="l00641"></a>00641       {
<a name="l00642"></a>00642         c_ = T(1);
<a name="l00643"></a>00643         s_ = T(0);
<a name="l00644"></a>00644         r = T(0);
<a name="l00645"></a>00645         z = T(0);
<a name="l00646"></a>00646       }
<a name="l00647"></a>00647     a_in = r;
<a name="l00648"></a>00648     b_in = z;
<a name="l00649"></a>00649   }
<a name="l00650"></a>00650 
<a name="l00651"></a>00651 
<a name="l00653"></a>00653   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00654"></a>00654   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a280476959b456ad5aa0aab55257f5c03" title="Computation of rotation between two points.">GenRot</a>(complex&lt;T&gt;&amp; a_in, complex&lt;T&gt;&amp; b_in, T&amp; c_, complex&lt;T&gt;&amp; s_)
<a name="l00655"></a>00655   {
<a name="l00656"></a>00656 
<a name="l00657"></a>00657     T a = abs(a_in), b = abs(b_in);
<a name="l00658"></a>00658     <span class="keywordflow">if</span> (a == T(0))
<a name="l00659"></a>00659       {
<a name="l00660"></a>00660         c_ = T(0);
<a name="l00661"></a>00661         s_ = complex&lt;T&gt;(1, 0);
<a name="l00662"></a>00662         a_in = b_in;
<a name="l00663"></a>00663       }
<a name="l00664"></a>00664     <span class="keywordflow">else</span>
<a name="l00665"></a>00665       {
<a name="l00666"></a>00666         T scale = a + b;
<a name="l00667"></a>00667         T a_scal = abs(a_in / scale);
<a name="l00668"></a>00668         T b_scal = abs(b_in / scale);
<a name="l00669"></a>00669         T norm = sqrt(a_scal * a_scal + b_scal * b_scal) * scale;
<a name="l00670"></a>00670 
<a name="l00671"></a>00671         c_ = a / norm;
<a name="l00672"></a>00672         complex&lt;T&gt; alpha = a_in / a;
<a name="l00673"></a>00673         s_ = alpha * conj(b_in) / norm;
<a name="l00674"></a>00674         a_in = alpha * norm;
<a name="l00675"></a>00675       }
<a name="l00676"></a>00676     b_in = complex&lt;T&gt;(0, 0);
<a name="l00677"></a>00677   }
<a name="l00678"></a>00678 
<a name="l00679"></a>00679 
<a name="l00681"></a>00681   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00682"></a>00682   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a5cb9b0f1114a738c5ecb66e2629a3155" title="Rotation of a point in 2-D.">ApplyRot</a>(T&amp; x, T&amp; y, <span class="keyword">const</span> T c_, <span class="keyword">const</span> T s_)
<a name="l00683"></a>00683   {
<a name="l00684"></a>00684     T temp = c_ * x + s_ * y;
<a name="l00685"></a>00685     y = c_ * y - s_ * x;
<a name="l00686"></a>00686     x = temp;
<a name="l00687"></a>00687   }
<a name="l00688"></a>00688 
<a name="l00689"></a>00689 
<a name="l00691"></a>00691   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00692"></a>00692   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a5cb9b0f1114a738c5ecb66e2629a3155" title="Rotation of a point in 2-D.">ApplyRot</a>(complex&lt;T&gt;&amp; x, complex&lt;T&gt;&amp; y,
<a name="l00693"></a>00693                 <span class="keyword">const</span> T&amp; c_, <span class="keyword">const</span> complex&lt;T&gt;&amp; s_)
<a name="l00694"></a>00694   {
<a name="l00695"></a>00695     complex&lt;T&gt; temp = s_ * y + c_ * x;
<a name="l00696"></a>00696     y = -conj(s_) * x + c_ * y;
<a name="l00697"></a>00697     x = temp;
<a name="l00698"></a>00698   }
<a name="l00699"></a>00699 
<a name="l00700"></a>00700 
<a name="l00701"></a>00701   <span class="comment">// APPLYROT //</span>
<a name="l00703"></a>00703 <span class="comment"></span>
<a name="l00704"></a>00704 
<a name="l00706"></a>00706   <span class="comment">// CHECKDIM //</span>
<a name="l00707"></a>00707 
<a name="l00708"></a>00708 
<a name="l00710"></a>00710 
<a name="l00720"></a>00720   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00721"></a>00721             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00722"></a>00722   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> Vector&lt;T0, Storage0, Allocator0&gt;&amp; X,
<a name="l00723"></a>00723                 <span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; Y,
<a name="l00724"></a>00724                 <span class="keywordtype">string</span> function, <span class="keywordtype">string</span> op)
<a name="l00725"></a>00725   {
<a name="l00726"></a>00726     <span class="keywordflow">if</span> (X.GetLength() != Y.GetLength())
<a name="l00727"></a>00727       <span class="keywordflow">throw</span> WrongDim(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation &quot;</span>) + op
<a name="l00728"></a>00728                      + string(<span class="stringliteral">&quot; not permitted:&quot;</span>)
<a name="l00729"></a>00729                      + string(<span class="stringliteral">&quot;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00730"></a>00730                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetLength())
<a name="l00731"></a>00731                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00732"></a>00732                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetLength())
<a name="l00733"></a>00733                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l00734"></a>00734   }
<a name="l00735"></a>00735 
<a name="l00736"></a>00736 
<a name="l00738"></a>00738 
<a name="l00748"></a>00748   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Allocator0,
<a name="l00749"></a>00749             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00750"></a>00750   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> Vector&lt;T0, Vect_Sparse, Allocator0&gt;&amp; X,
<a name="l00751"></a>00751                 <span class="keyword">const</span> Vector&lt;T1, Vect_Sparse, Allocator1&gt;&amp; Y,
<a name="l00752"></a>00752                 <span class="keywordtype">string</span> function, <span class="keywordtype">string</span> op)
<a name="l00753"></a>00753   {
<a name="l00754"></a>00754     <span class="comment">// The dimension of a Vector&lt;Vect_Sparse&gt; is infinite,</span>
<a name="l00755"></a>00755     <span class="comment">// so no vector dimension checking has to be done.</span>
<a name="l00756"></a>00756   }
<a name="l00757"></a>00757 
<a name="l00758"></a>00758 
<a name="l00760"></a>00760 
<a name="l00770"></a>00770   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Allocator0,
<a name="l00771"></a>00771             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00772"></a>00772   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> Vector&lt;T0, Collection, Allocator0&gt;&amp; X,
<a name="l00773"></a>00773                 <span class="keyword">const</span> Vector&lt;T1, Collection, Allocator1&gt;&amp; Y,
<a name="l00774"></a>00774                 <span class="keywordtype">string</span> function, <span class="keywordtype">string</span> op)
<a name="l00775"></a>00775   {
<a name="l00776"></a>00776     <span class="keywordflow">if</span> (X.GetLength() != Y.GetLength())
<a name="l00777"></a>00777       <span class="keywordflow">throw</span> WrongDim(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation &quot;</span>) + op
<a name="l00778"></a>00778                      + string(<span class="stringliteral">&quot; not permitted:&quot;</span>)
<a name="l00779"></a>00779                      + string(<span class="stringliteral">&quot;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00780"></a>00780                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetLength())
<a name="l00781"></a>00781                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00782"></a>00782                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetLength())
<a name="l00783"></a>00783                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l00784"></a>00784 
<a name="l00785"></a>00785     <span class="keywordflow">if</span> (X.GetNvector() != Y.GetNvector())
<a name="l00786"></a>00786       <span class="keywordflow">throw</span> WrongDim(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation &quot;</span>) + op
<a name="l00787"></a>00787                      + string(<span class="stringliteral">&quot; not permitted:&quot;</span>)
<a name="l00788"></a>00788                      + string(<span class="stringliteral">&quot;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00789"></a>00789                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetNvector())
<a name="l00790"></a>00790                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00791"></a>00791                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetNvector())
<a name="l00792"></a>00792                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l00793"></a>00793   }
<a name="l00794"></a>00794 
<a name="l00795"></a>00795 
<a name="l00797"></a>00797 
<a name="l00807"></a>00807   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Allocator0, <span class="keyword">class </span>Allocator00,
<a name="l00808"></a>00808             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator11&gt;
<a name="l00809"></a>00809   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> Vector&lt;Vector&lt;T0, Vect_Sparse, Allocator0&gt;,
<a name="l00810"></a>00810                 Collection, Allocator00&gt;&amp; X,
<a name="l00811"></a>00811                 <span class="keyword">const</span> Vector&lt;Vector&lt;T1, Vect_Sparse, Allocator1&gt;,
<a name="l00812"></a>00812                 Collection, Allocator11&gt;&amp; Y,
<a name="l00813"></a>00813                 <span class="keywordtype">string</span> function, <span class="keywordtype">string</span> op)
<a name="l00814"></a>00814   {
<a name="l00815"></a>00815     <span class="keywordflow">if</span> (X.GetNvector() != Y.GetNvector())
<a name="l00816"></a>00816       <span class="keywordflow">throw</span> WrongDim(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation &quot;</span>) + op
<a name="l00817"></a>00817                      + string(<span class="stringliteral">&quot; not permitted:&quot;</span>)
<a name="l00818"></a>00818                      + string(<span class="stringliteral">&quot;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00819"></a>00819                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetNvector())
<a name="l00820"></a>00820                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00821"></a>00821                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetNvector())
<a name="l00822"></a>00822                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l00823"></a>00823   }
<a name="l00824"></a>00824 
<a name="l00825"></a>00825 
<a name="l00827"></a>00827 
<a name="l00837"></a>00837   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Allocator0,
<a name="l00838"></a>00838             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00839"></a>00839   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> Vector&lt;T0, VectFull, Allocator0&gt;&amp; X,
<a name="l00840"></a>00840                 Vector&lt;T1, Collection, Allocator1&gt;&amp; Y,
<a name="l00841"></a>00841                 <span class="keywordtype">string</span> function, <span class="keywordtype">string</span> op)
<a name="l00842"></a>00842   {
<a name="l00843"></a>00843     <span class="keywordflow">if</span> (X.GetLength() != Y.GetM())
<a name="l00844"></a>00844       <span class="keywordflow">throw</span> WrongDim(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation &quot;</span>) + op
<a name="l00845"></a>00845                      + string(<span class="stringliteral">&quot; not permitted:&quot;</span>)
<a name="l00846"></a>00846                      + string(<span class="stringliteral">&quot;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00847"></a>00847                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetLength())
<a name="l00848"></a>00848                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00849"></a>00849                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetM())
<a name="l00850"></a>00850                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l00851"></a>00851   }
<a name="l00852"></a>00852 
<a name="l00853"></a>00853 
<a name="l00855"></a>00855 
<a name="l00865"></a>00865   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U0&gt; <span class="keyword">class </span>Allocator0,
<a name="l00866"></a>00866             <span class="keyword">class </span>T1, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U1&gt; <span class="keyword">class </span>Allocator1&gt;
<a name="l00867"></a>00867   <span class="keywordtype">void</span>
<a name="l00868"></a>00868   <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span>
<a name="l00869"></a>00869            Vector&lt;FloatDouble, DenseSparseCollection, Allocator0&lt;T0&gt; &gt;&amp; X,
<a name="l00870"></a>00870            <span class="keyword">const</span>
<a name="l00871"></a>00871            Vector&lt;FloatDouble, DenseSparseCollection, Allocator1&lt;T1&gt; &gt;&amp; Y,
<a name="l00872"></a>00872            <span class="keywordtype">string</span> function, <span class="keywordtype">string</span> op)
<a name="l00873"></a>00873   {
<a name="l00874"></a>00874     <span class="keywordflow">if</span> (X.GetNvector() != Y.GetNvector())
<a name="l00875"></a>00875       <span class="keywordflow">throw</span> WrongDim(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation &quot;</span>) + op
<a name="l00876"></a>00876                      + string(<span class="stringliteral">&quot; not permitted:&quot;</span>)
<a name="l00877"></a>00877                      + string(<span class="stringliteral">&quot;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00878"></a>00878                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetNvector())
<a name="l00879"></a>00879                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00880"></a>00880                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetNvector())
<a name="l00881"></a>00881                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l00882"></a>00882   }
<a name="l00883"></a>00883 
<a name="l00884"></a>00884 
<a name="l00885"></a>00885   <span class="comment">// CHECKDIM //</span>
<a name="l00887"></a>00887 <span class="comment"></span>
<a name="l00888"></a>00888 
<a name="l00890"></a>00890   <span class="comment">// CONJUGATE //</span>
<a name="l00891"></a>00891 
<a name="l00892"></a>00892 
<a name="l00894"></a>00894   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00895"></a>00895   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a90119cfc81b2e157c522a426a0167ccb" title="Sets a vector to its conjugate.">Conjugate</a>(Vector&lt;T, Prop, Allocator&gt;&amp; X)
<a name="l00896"></a>00896   {
<a name="l00897"></a>00897     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetM(); i++)
<a name="l00898"></a>00898       X(i) = conj(X(i));
<a name="l00899"></a>00899   }
<a name="l00900"></a>00900 
<a name="l00901"></a>00901 
<a name="l00903"></a>00903   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00904"></a>00904   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a90119cfc81b2e157c522a426a0167ccb" title="Sets a vector to its conjugate.">Conjugate</a>(Vector&lt;T, VectSparse, Allocator&gt;&amp; X)
<a name="l00905"></a>00905   {
<a name="l00906"></a>00906     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetSize(); i++)
<a name="l00907"></a>00907       X.Value(i) = conj(X.Value(i));
<a name="l00908"></a>00908   }
<a name="l00909"></a>00909 
<a name="l00910"></a>00910 
<a name="l00911"></a>00911   <span class="comment">// CONJUGATE //</span>
<a name="l00913"></a>00913 <span class="comment"></span>
<a name="l00914"></a>00914 
<a name="l00915"></a>00915 } <span class="comment">// namespace Seldon.</span>
<a name="l00916"></a>00916 
<a name="l00917"></a>00917 
<a name="l00918"></a>00918 <span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
