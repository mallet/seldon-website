<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><b>seldon</b>      </li>
      <li><a class="el" href="classseldon_1_1_matrix_pointers_double.php">MatrixPointersDouble</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pub-attribs">Public Attributes</a>  </div>
  <div class="headertitle">
<h1>seldon::MatrixPointersDouble Class Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="seldon::MatrixPointersDouble" --><!-- doxytag: inherits="seldon::MatrixBaseDouble" --><div class="dynheader">
Inheritance diagram for seldon::MatrixPointersDouble:</div>
<div class="dyncontent">
 <div class="center">
  <img src="classseldon_1_1_matrix_pointers_double.png" usemap="#seldon::MatrixPointersDouble_map" alt=""/>
  <map id="seldon::MatrixPointersDouble_map" name="seldon::MatrixPointersDouble_map">
<area href="classseldon_1_1_matrix_base_double.php" alt="seldon::MatrixBaseDouble" shape="rect" coords="0,56,176,80"/>
<area href="classseldon_1_1__object.php" alt="seldon::_object" shape="rect" coords="0,0,176,24"/>
<area href="classseldon_1_1_matrix_double.php" alt="seldon::MatrixDouble" shape="rect" coords="0,168,176,192"/>
</map>
</div>

<p><a href="classseldon_1_1_matrix_pointers_double-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a27b9b0299913ecc0d1e0b2c61cd91df5"></a><!-- doxytag: member="seldon::MatrixPointersDouble::__init__" ref="a27b9b0299913ecc0d1e0b2c61cd91df5" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__init__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aff17fd20323c94725aca88b7586384da"></a><!-- doxytag: member="seldon::MatrixPointersDouble::Clear" ref="aff17fd20323c94725aca88b7586384da" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Clear</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af982038160a825ee856b0d078df05b86"></a><!-- doxytag: member="seldon::MatrixPointersDouble::GetDataSize" ref="af982038160a825ee856b0d078df05b86" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataSize</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af3528e95e492fb1313c9cd9c976d6a04"></a><!-- doxytag: member="seldon::MatrixPointersDouble::GetMe" ref="af3528e95e492fb1313c9cd9c976d6a04" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetMe</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a66ea613c2e43192f439a456d9bf26689"></a><!-- doxytag: member="seldon::MatrixPointersDouble::Reallocate" ref="a66ea613c2e43192f439a456d9bf26689" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Reallocate</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0d653f3990f0fb4364839a6f33f135d7"></a><!-- doxytag: member="seldon::MatrixPointersDouble::SetData" ref="a0d653f3990f0fb4364839a6f33f135d7" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetData</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a993963b8c39a76953ed95cdd725dca9d"></a><!-- doxytag: member="seldon::MatrixPointersDouble::Nullify" ref="a993963b8c39a76953ed95cdd725dca9d" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Nullify</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aac23375339915752a5b295f41cf130a5"></a><!-- doxytag: member="seldon::MatrixPointersDouble::Resize" ref="aac23375339915752a5b295f41cf130a5" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Resize</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a960f0cee943cd9dc37b4f16978db12b5"></a><!-- doxytag: member="seldon::MatrixPointersDouble::__call__" ref="a960f0cee943cd9dc37b4f16978db12b5" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__call__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1ada38d65b9e97e5c786350ac2cb7642"></a><!-- doxytag: member="seldon::MatrixPointersDouble::Val" ref="a1ada38d65b9e97e5c786350ac2cb7642" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Val</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a31d9db1d21741d454f5bba349f9fdaa0"></a><!-- doxytag: member="seldon::MatrixPointersDouble::Get" ref="a31d9db1d21741d454f5bba349f9fdaa0" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Get</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab85b47c462634d2ee45d4551404e46df"></a><!-- doxytag: member="seldon::MatrixPointersDouble::Set" ref="ab85b47c462634d2ee45d4551404e46df" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Set</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a71e3ae4d33ac07a8af2d91adabd2893b"></a><!-- doxytag: member="seldon::MatrixPointersDouble::Copy" ref="a71e3ae4d33ac07a8af2d91adabd2893b" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Copy</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a95cc5e455373994d87124afe29d7d7d7"></a><!-- doxytag: member="seldon::MatrixPointersDouble::GetLD" ref="a95cc5e455373994d87124afe29d7d7d7" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetLD</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6dfb6c90689ae2b8b19293ceaa476b03"></a><!-- doxytag: member="seldon::MatrixPointersDouble::Zero" ref="a6dfb6c90689ae2b8b19293ceaa476b03" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Zero</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a94623c854cae66bbb6ce0a0db1df7b6e"></a><!-- doxytag: member="seldon::MatrixPointersDouble::SetIdentity" ref="a94623c854cae66bbb6ce0a0db1df7b6e" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetIdentity</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8ba84b662ad60591021f3d6655a2c3d9"></a><!-- doxytag: member="seldon::MatrixPointersDouble::Fill" ref="a8ba84b662ad60591021f3d6655a2c3d9" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Fill</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab18995ba01c9c3fa27e1d2f57573fe6a"></a><!-- doxytag: member="seldon::MatrixPointersDouble::FillRand" ref="ab18995ba01c9c3fa27e1d2f57573fe6a" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>FillRand</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac6fdf6082b96697eec2f23eaef1f41e6"></a><!-- doxytag: member="seldon::MatrixPointersDouble::Print" ref="ac6fdf6082b96697eec2f23eaef1f41e6" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Print</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aea181c23a69e5b9048a39d31b0a95d76"></a><!-- doxytag: member="seldon::MatrixPointersDouble::Write" ref="aea181c23a69e5b9048a39d31b0a95d76" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Write</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a53c365195fd0238dfb20922f98c2c0f0"></a><!-- doxytag: member="seldon::MatrixPointersDouble::WriteText" ref="a53c365195fd0238dfb20922f98c2c0f0" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>WriteText</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a99bd57160e6fdbf2aa36e87311deaa25"></a><!-- doxytag: member="seldon::MatrixPointersDouble::Read" ref="a99bd57160e6fdbf2aa36e87311deaa25" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Read</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a58bcded5e2b35a05a9854f45952b33e7"></a><!-- doxytag: member="seldon::MatrixPointersDouble::ReadText" ref="a58bcded5e2b35a05a9854f45952b33e7" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>ReadText</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a10557b0ca5e128907518343d38f9d430"></a><!-- doxytag: member="seldon::MatrixPointersDouble::GetM" ref="a10557b0ca5e128907518343d38f9d430" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetM</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a556688675412c7c5d063cf313ad4997f"></a><!-- doxytag: member="seldon::MatrixPointersDouble::GetN" ref="a556688675412c7c5d063cf313ad4997f" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetN</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a569343e9fe5001da21c874c4ce1a11f8"></a><!-- doxytag: member="seldon::MatrixPointersDouble::GetSize" ref="a569343e9fe5001da21c874c4ce1a11f8" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetSize</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac25edf930e9247880287294c0e9d4b54"></a><!-- doxytag: member="seldon::MatrixPointersDouble::GetData" ref="ac25edf930e9247880287294c0e9d4b54" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetData</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a475019abd6e2a17c673efa0fb9bc2643"></a><!-- doxytag: member="seldon::MatrixPointersDouble::GetDataConst" ref="a475019abd6e2a17c673efa0fb9bc2643" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataConst</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a100846ac7e1a2610cf1167a654328de4"></a><!-- doxytag: member="seldon::MatrixPointersDouble::GetDataVoid" ref="a100846ac7e1a2610cf1167a654328de4" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataVoid</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6a8f262d641f7d43ffebcf4980941b78"></a><!-- doxytag: member="seldon::MatrixPointersDouble::GetDataConstVoid" ref="a6a8f262d641f7d43ffebcf4980941b78" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataConstVoid</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9d1c2383fbdcdb9afabe800635103663"></a><!-- doxytag: member="seldon::MatrixPointersDouble::GetAllocator" ref="a9d1c2383fbdcdb9afabe800635103663" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetAllocator</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-attribs"></a>
Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7e03f2e9e0d41a0ccc0e26349d748bc2"></a><!-- doxytag: member="seldon::MatrixPointersDouble::this" ref="a7e03f2e9e0d41a0ccc0e26349d748bc2" args="" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><b>this</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>

<p>Definition at line <a class="el" href="seldon_8py_source.php#l01054">1054</a> of file <a class="el" href="seldon_8py_source.php">seldon.py</a>.</p>
<hr/>The documentation for this class was generated from the following file:<ul>
<li><a class="el" href="seldon_8py_source.php">seldon.py</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
