<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><b>seldon</b>      </li>
      <li><a class="el" href="classseldon_1_1_matrix_pointers_int.php">MatrixPointersInt</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pub-attribs">Public Attributes</a>  </div>
  <div class="headertitle">
<h1>seldon::MatrixPointersInt Class Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="seldon::MatrixPointersInt" --><!-- doxytag: inherits="seldon::MatrixBaseInt" --><div class="dynheader">
Inheritance diagram for seldon::MatrixPointersInt:</div>
<div class="dyncontent">
 <div class="center">
  <img src="classseldon_1_1_matrix_pointers_int.png" usemap="#seldon::MatrixPointersInt_map" alt=""/>
  <map id="seldon::MatrixPointersInt_map" name="seldon::MatrixPointersInt_map">
<area href="classseldon_1_1_matrix_base_int.php" alt="seldon::MatrixBaseInt" shape="rect" coords="0,56,149,80"/>
<area href="classseldon_1_1__object.php" alt="seldon::_object" shape="rect" coords="0,0,149,24"/>
<area href="classseldon_1_1_matrix_int.php" alt="seldon::MatrixInt" shape="rect" coords="0,168,149,192"/>
</map>
</div>

<p><a href="classseldon_1_1_matrix_pointers_int-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7f2f66392c84c95d853e479ab5e75829"></a><!-- doxytag: member="seldon::MatrixPointersInt::__init__" ref="a7f2f66392c84c95d853e479ab5e75829" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__init__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a69ccfd19838a8adc0e3dcfd0708df98c"></a><!-- doxytag: member="seldon::MatrixPointersInt::Clear" ref="a69ccfd19838a8adc0e3dcfd0708df98c" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Clear</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9153b62cd6df0df2debbaaed6d3cbb4f"></a><!-- doxytag: member="seldon::MatrixPointersInt::GetDataSize" ref="a9153b62cd6df0df2debbaaed6d3cbb4f" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataSize</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2d9b582dc501d5d7a074a9fd47ffda39"></a><!-- doxytag: member="seldon::MatrixPointersInt::GetMe" ref="a2d9b582dc501d5d7a074a9fd47ffda39" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetMe</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aec5ffae04b16db1f69aadc3891f0d1be"></a><!-- doxytag: member="seldon::MatrixPointersInt::Reallocate" ref="aec5ffae04b16db1f69aadc3891f0d1be" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Reallocate</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6d1e69131c75619350f92be2e1c67e48"></a><!-- doxytag: member="seldon::MatrixPointersInt::SetData" ref="a6d1e69131c75619350f92be2e1c67e48" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetData</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a03a80d36d206abd1f2384a15896e1fea"></a><!-- doxytag: member="seldon::MatrixPointersInt::Nullify" ref="a03a80d36d206abd1f2384a15896e1fea" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Nullify</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3068e3fdfd6ea241b0f163684c4c6111"></a><!-- doxytag: member="seldon::MatrixPointersInt::Resize" ref="a3068e3fdfd6ea241b0f163684c4c6111" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Resize</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a397e69c53c468403d1b52c7683e0b30b"></a><!-- doxytag: member="seldon::MatrixPointersInt::__call__" ref="a397e69c53c468403d1b52c7683e0b30b" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__call__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa9704da2abcfbf9bfa7e27f2fbf299b5"></a><!-- doxytag: member="seldon::MatrixPointersInt::Val" ref="aa9704da2abcfbf9bfa7e27f2fbf299b5" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Val</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af52dc44bc8d7609b7184b81327a84213"></a><!-- doxytag: member="seldon::MatrixPointersInt::Get" ref="af52dc44bc8d7609b7184b81327a84213" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Get</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a18088a7f7a0f96ceced838c802aa7f45"></a><!-- doxytag: member="seldon::MatrixPointersInt::Set" ref="a18088a7f7a0f96ceced838c802aa7f45" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Set</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3b9eeaaae646163c62d2ab7ca89ed147"></a><!-- doxytag: member="seldon::MatrixPointersInt::Copy" ref="a3b9eeaaae646163c62d2ab7ca89ed147" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Copy</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae2f805b39ad7b5cba54e2558e3892600"></a><!-- doxytag: member="seldon::MatrixPointersInt::GetLD" ref="ae2f805b39ad7b5cba54e2558e3892600" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetLD</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4c8f89bb923a50abfeb68a2b50827c60"></a><!-- doxytag: member="seldon::MatrixPointersInt::Zero" ref="a4c8f89bb923a50abfeb68a2b50827c60" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Zero</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac388cfb0d6402af9c3d2527f4a494527"></a><!-- doxytag: member="seldon::MatrixPointersInt::SetIdentity" ref="ac388cfb0d6402af9c3d2527f4a494527" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetIdentity</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2ad4e36ecd6ad2d3545e283a57c18a36"></a><!-- doxytag: member="seldon::MatrixPointersInt::Fill" ref="a2ad4e36ecd6ad2d3545e283a57c18a36" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Fill</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a01984680a29561264cf5f2d3bbed9b27"></a><!-- doxytag: member="seldon::MatrixPointersInt::FillRand" ref="a01984680a29561264cf5f2d3bbed9b27" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>FillRand</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a86e4b7a242041a0b0e2bf0d62efb78d3"></a><!-- doxytag: member="seldon::MatrixPointersInt::Print" ref="a86e4b7a242041a0b0e2bf0d62efb78d3" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Print</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1f7c6457202f0e98fe2e190ebde1f17c"></a><!-- doxytag: member="seldon::MatrixPointersInt::Write" ref="a1f7c6457202f0e98fe2e190ebde1f17c" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Write</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8cbaf002e88c8d984cbb2c76852915ce"></a><!-- doxytag: member="seldon::MatrixPointersInt::WriteText" ref="a8cbaf002e88c8d984cbb2c76852915ce" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>WriteText</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6fe9eb8cbfced540714255c44e23c583"></a><!-- doxytag: member="seldon::MatrixPointersInt::Read" ref="a6fe9eb8cbfced540714255c44e23c583" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Read</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a92969b6644acd4133bab1d5b2bf104d5"></a><!-- doxytag: member="seldon::MatrixPointersInt::ReadText" ref="a92969b6644acd4133bab1d5b2bf104d5" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>ReadText</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7d8cdd66465381d843d299604950f6a0"></a><!-- doxytag: member="seldon::MatrixPointersInt::GetM" ref="a7d8cdd66465381d843d299604950f6a0" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetM</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5519809640f319a51c0d39fdeb539167"></a><!-- doxytag: member="seldon::MatrixPointersInt::GetN" ref="a5519809640f319a51c0d39fdeb539167" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetN</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afa1bcc2cc4851d9ad1badcc4d5cecaa0"></a><!-- doxytag: member="seldon::MatrixPointersInt::GetSize" ref="afa1bcc2cc4851d9ad1badcc4d5cecaa0" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetSize</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="abc834ad7c7cebeff263999cc243712b4"></a><!-- doxytag: member="seldon::MatrixPointersInt::GetData" ref="abc834ad7c7cebeff263999cc243712b4" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetData</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afc040a289ae0b850a2facac5bfcd55ea"></a><!-- doxytag: member="seldon::MatrixPointersInt::GetDataConst" ref="afc040a289ae0b850a2facac5bfcd55ea" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataConst</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae020f3a2bdca6915c17eee565c8c0a6d"></a><!-- doxytag: member="seldon::MatrixPointersInt::GetDataVoid" ref="ae020f3a2bdca6915c17eee565c8c0a6d" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataVoid</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afd29e24fa5407d1adf4e64526427cda5"></a><!-- doxytag: member="seldon::MatrixPointersInt::GetDataConstVoid" ref="afd29e24fa5407d1adf4e64526427cda5" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataConstVoid</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aff3200e043c71d68559cfb1daae0bcd6"></a><!-- doxytag: member="seldon::MatrixPointersInt::GetAllocator" ref="aff3200e043c71d68559cfb1daae0bcd6" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetAllocator</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-attribs"></a>
Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adba0202a7843453becf9633ec4d95541"></a><!-- doxytag: member="seldon::MatrixPointersInt::this" ref="adba0202a7843453becf9633ec4d95541" args="" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><b>this</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>

<p>Definition at line <a class="el" href="seldon_8py_source.php#l00972">972</a> of file <a class="el" href="seldon_8py_source.php">seldon.py</a>.</p>
<hr/>The documentation for this class was generated from the following file:<ul>
<li><a class="el" href="seldon_8py_source.php">seldon.py</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
