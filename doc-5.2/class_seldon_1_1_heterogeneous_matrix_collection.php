<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">HeterogeneousMatrixCollection</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a> &#124;
<a href="#pro-static-attribs">Static Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::HeterogeneousMatrixCollection" --><!-- doxytag: inherits="Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;" -->
<p><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> class made of an heterogeneous collection of matrices.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_heterogeneous_matrix_collection_8hxx_source.php">HeterogeneousMatrixCollection.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_heterogeneous_matrix_collection.png" usemap="#Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;_map" alt=""/>
  <map id="Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;_map" name="Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;_map">
<area href="class_seldon_1_1_matrix___base.php" alt="Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;" shape="rect" coords="0,0,515,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_heterogeneous_matrix_collection-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac35261f04ec95ce8324dcdfca25002f5"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::float_dense_m" ref="ac35261f04ec95ce8324dcdfca25002f5" args="" -->
typedef <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; float, Prop0, <br class="typebreak"/>
Storage0, Allocator&lt; float &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>float_dense_m</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3a449c59e72ebe42f62f67eadd82db3f"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::float_sparse_m" ref="a3a449c59e72ebe42f62f67eadd82db3f" args="" -->
typedef <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; float, Prop1, <br class="typebreak"/>
Storage1, Allocator&lt; float &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>float_sparse_m</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1529a24b3c2dbe8eb1669962fc1884d6"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::double_dense_m" ref="a1529a24b3c2dbe8eb1669962fc1884d6" args="" -->
typedef <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; double, Prop0, <br class="typebreak"/>
Storage0, Allocator&lt; double &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>double_dense_m</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afa02c20e34846d05e6c0d5cd5280cc3d"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::double_sparse_m" ref="afa02c20e34846d05e6c0d5cd5280cc3d" args="" -->
typedef <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; double, Prop1, <br class="typebreak"/>
Storage1, Allocator&lt; double &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>double_sparse_m</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a33b3985bd348f768034c562fd16a4f3c"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::float_dense_c" ref="a33b3985bd348f768034c562fd16a4f3c" args="" -->
typedef <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; <a class="el" href="class_seldon_1_1_matrix.php">float_dense_m</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_general.php">General</a>, <a class="el" href="class_seldon_1_1_row_major_collection.php">RowMajorCollection</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a>&lt; <a class="el" href="class_seldon_1_1_matrix.php">float_dense_m</a> &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>float_dense_c</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a28f73456cd9a9b5816aa0c80095b7884"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::float_sparse_c" ref="a28f73456cd9a9b5816aa0c80095b7884" args="" -->
typedef <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; <a class="el" href="class_seldon_1_1_matrix.php">float_sparse_m</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_general.php">General</a>, <a class="el" href="class_seldon_1_1_row_major_collection.php">RowMajorCollection</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a>&lt; <a class="el" href="class_seldon_1_1_matrix.php">float_sparse_m</a> &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>float_sparse_c</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7c20722ed8cad089e383b332b7ec30d9"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::double_dense_c" ref="a7c20722ed8cad089e383b332b7ec30d9" args="" -->
typedef <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; <a class="el" href="class_seldon_1_1_matrix.php">double_dense_m</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_general.php">General</a>, <a class="el" href="class_seldon_1_1_row_major_collection.php">RowMajorCollection</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a>&lt; <a class="el" href="class_seldon_1_1_matrix.php">double_dense_m</a> &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>double_dense_c</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab2d2280f2f97f03d80f67e14ec5981a7"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::double_sparse_c" ref="ab2d2280f2f97f03d80f67e14ec5981a7" args="" -->
typedef <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a><br class="typebreak"/>
&lt; <a class="el" href="class_seldon_1_1_matrix.php">double_sparse_m</a>, <a class="el" href="class_seldon_1_1_general.php">General</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_row_major_collection.php">RowMajorCollection</a>, <a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a><br class="typebreak"/>
&lt; <a class="el" href="class_seldon_1_1_matrix.php">double_sparse_m</a> &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>double_sparse_c</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a09596a54396d6e21a9848fe2e8554efd"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::value_type" ref="a09596a54396d6e21a9848fe2e8554efd" args="" -->
typedef Allocator&lt; double &gt;<br class="typebreak"/>
::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9c13a3062dcae2e438ad6845e8920a97"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::pointer" ref="a9c13a3062dcae2e438ad6845e8920a97" args="" -->
typedef Allocator&lt; double &gt;<br class="typebreak"/>
::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adac539c8208073ffd6214e9cefbed0f6"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::const_pointer" ref="adac539c8208073ffd6214e9cefbed0f6" args="" -->
typedef Allocator&lt; double &gt;<br class="typebreak"/>
::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5ed3e9a1cc5071fdfcde5ce231932324"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::reference" ref="a5ed3e9a1cc5071fdfcde5ce231932324" args="" -->
typedef Allocator&lt; double &gt;<br class="typebreak"/>
::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0a4a4810c1d0a16655843bdd62bc8f60"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::const_reference" ref="a0a4a4810c1d0a16655843bdd62bc8f60" args="" -->
typedef Allocator&lt; double &gt;<br class="typebreak"/>
::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aa958b2bb3412cda25f49f24938294553">HeterogeneousMatrixCollection</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#aa958b2bb3412cda25f49f24938294553"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a686bd977ef7310cb3339289ec07eefc2">HeterogeneousMatrixCollection</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Main constructor.  <a href="#a686bd977ef7310cb3339289ec07eefc2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aa473473c04fea0716d88de40c87b282b">HeterogeneousMatrixCollection</a> (const <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copy constructor.  <a href="#aa473473c04fea0716d88de40c87b282b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aeec9a8f4343637e860a9dc627d60ed89"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::~HeterogeneousMatrixCollection" ref="aeec9a8f4343637e860a9dc627d60ed89" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aeec9a8f4343637e860a9dc627d60ed89">~HeterogeneousMatrixCollection</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Destructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0324574cd55a0799aa9746b36b9b20a1"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::Clear" ref="a0324574cd55a0799aa9746b36b9b20a1" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a0324574cd55a0799aa9746b36b9b20a1">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the matrix collection without releasing memory. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3e48adb67f58b6355dc7ce39611585cc"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::Nullify" ref="a3e48adb67f58b6355dc7ce39611585cc" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3e48adb67f58b6355dc7ce39611585cc">Nullify</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the matrix collection without releasing memory. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a19f1fe843dbb872c589db2116cd204f9">Nullify</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears a given underlying matrix.  <a href="#a19f1fe843dbb872c589db2116cd204f9"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a34264880bdeb3febb8c73b2cad0b4588"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::Deallocate" ref="a34264880bdeb3febb8c73b2cad0b4588" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a34264880bdeb3febb8c73b2cad0b4588">Deallocate</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Deallocates underlying the matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">GetM</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows.  <a href="#a7b22f363d1535f911ee271f1e0743c6e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a38820efd26db52feaeb8998d2fd43daa">GetMmatrix</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows.  <a href="#a38820efd26db52feaeb8998d2fd43daa"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ac368f84e127c6738f7f06c74bbc6e7da">GetM</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows in an underlying matrix.  <a href="#ac368f84e127c6738f7f06c74bbc6e7da"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">GetN</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns.  <a href="#a03dbde09b4beb73bd66628b4db00df0d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a6c59c77733659c863477c9a34fc927fe">GetNmatrix</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns.  <a href="#a6c59c77733659c863477c9a34fc927fe"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3b788a9ac72e2f32842dcc9251c6ca0f">GetN</a> (int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns in an underlying matrix.  <a href="#a3b788a9ac72e2f32842dcc9251c6ca0f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">GetSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored in memory.  <a href="#ae5570f5b9a4dac52024ced4061cfe5a8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a2a36130a12ef3b4e4a4971f5898ca0bd">GetDataSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored in memory.  <a href="#a2a36130a12ef3b4e4a4971f5898ca0bd"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a69858677f0823951f46671248091be2e">GetType</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the type of a given underlying matrix.  <a href="#a69858677f0823951f46671248091be2e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix.php">float_dense_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a9bd6c070c0738b3425e5e9b3e0118ec6">GetFloatDense</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the collection of float dense underlying matrices.  <a href="#a9bd6c070c0738b3425e5e9b3e0118ec6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_matrix.php">float_dense_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aa53a233cf9ea59c8711247cc013d75b7">GetFloatDense</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the collection of float dense underlying matrices.  <a href="#aa53a233cf9ea59c8711247cc013d75b7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix.php">float_sparse_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a69cf73dbc6d9055bc8ad28633de18f24">GetFloatSparse</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the collection of float sparse underlying matrices.  <a href="#a69cf73dbc6d9055bc8ad28633de18f24"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_matrix.php">float_sparse_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#afbdb423cb862ab27779c8e526f4f05e6">GetFloatSparse</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the collection of float sparse underlying matrices.  <a href="#afbdb423cb862ab27779c8e526f4f05e6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix.php">double_dense_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a289e9f762721738e42b9907450e72e9f">GetDoubleDense</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the collection of double dense underlying matrices.  <a href="#a289e9f762721738e42b9907450e72e9f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_matrix.php">double_dense_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a9bc398080b4454e0bcbf968fd25fbd78">GetDoubleDense</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the collection of double dense underlying matrices.  <a href="#a9bc398080b4454e0bcbf968fd25fbd78"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix.php">double_sparse_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a39301f5774586f67d31f64a3d4acb0af">GetDoubleSparse</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the collection of double sparse underlying matrices.  <a href="#a39301f5774586f67d31f64a3d4acb0af"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_matrix.php">double_sparse_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a1a4f404541bb2b70a36cc1a193ee5cb7">GetDoubleSparse</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the collection of double sparse underlying matrices.  <a href="#a1a4f404541bb2b70a36cc1a193ee5cb7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae600c754d08ba0af893d118cd3a38c60">Reallocate</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates memory to resize the matrix collection.  <a href="#ae600c754d08ba0af893d118cd3a38c60"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3f15b1b92a4f05a353a553597b18a718"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::SetMatrix" ref="a3f15b1b92a4f05a353a553597b18a718" args="(int m, int n, const float_dense_m &amp;)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetMatrix</b> (int m, int n, const <a class="el" href="class_seldon_1_1_matrix.php">float_dense_m</a> &amp;)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa14610a44bc6121227676f7f8a4d3092"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::SetMatrix" ref="aa14610a44bc6121227676f7f8a4d3092" args="(int m, int n, const float_sparse_m &amp;)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetMatrix</b> (int m, int n, const <a class="el" href="class_seldon_1_1_matrix.php">float_sparse_m</a> &amp;)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aba9733f51050379e7d6511a423ed18a2"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::SetMatrix" ref="aba9733f51050379e7d6511a423ed18a2" args="(int m, int n, const double_dense_m &amp;)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetMatrix</b> (int m, int n, const <a class="el" href="class_seldon_1_1_matrix.php">double_dense_m</a> &amp;)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae7b70308ce05f06619b770261c2ec3b4"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::SetMatrix" ref="ae7b70308ce05f06619b770261c2ec3b4" args="(int m, int n, const double_sparse_m &amp;)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetMatrix</b> (int m, int n, const <a class="el" href="class_seldon_1_1_matrix.php">double_sparse_m</a> &amp;)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a652965504371cdec893ace70c4a66502"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetMatrix" ref="a652965504371cdec893ace70c4a66502" args="(int m, int n, float_dense_m &amp;) const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetMatrix</b> (int m, int n, <a class="el" href="class_seldon_1_1_matrix.php">float_dense_m</a> &amp;) const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab1fe3083dfb9e0e0c55f4ab0551aaeaa"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetMatrix" ref="ab1fe3083dfb9e0e0c55f4ab0551aaeaa" args="(int m, int n, float_sparse_m &amp;) const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetMatrix</b> (int m, int n, <a class="el" href="class_seldon_1_1_matrix.php">float_sparse_m</a> &amp;) const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad97ac078f1e871776f18f27486e8f1f1"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetMatrix" ref="ad97ac078f1e871776f18f27486e8f1f1" args="(int m, int n, double_dense_m &amp;) const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetMatrix</b> (int m, int n, <a class="el" href="class_seldon_1_1_matrix.php">double_dense_m</a> &amp;) const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9efe3f3c96b6afca9bcc1c8d60871531"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetMatrix" ref="a9efe3f3c96b6afca9bcc1c8d60871531" args="(int m, int n, double_sparse_m &amp;) const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetMatrix</b> (int m, int n, <a class="el" href="class_seldon_1_1_matrix.php">double_sparse_m</a> &amp;) const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top">double&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#accc5a22477245a42cd0e017157f7d17d">operator()</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#accc5a22477245a42cd0e017157f7d17d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">HeterogeneousMatrixCollection</a><br class="typebreak"/>
&lt; Prop0, Storage0, Prop1, <br class="typebreak"/>
Storage1, Allocator &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a1f383b28bd0289fc1f00c9e06e597c9e">operator=</a> (const <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a matrix collection (assignment operator).  <a href="#a1f383b28bd0289fc1f00c9e06e597c9e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a91100b6eb7a2cea5e86d191ee58749bf">Copy</a> (const <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a matrix collection (assignment operator).  <a href="#a91100b6eb7a2cea5e86d191ee58749bf"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a788108f4822afc37c6ef4054b37a03bc">Print</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays the matrix collection on the standard output.  <a href="#a788108f4822afc37c6ef4054b37a03bc"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3d95d8a80c5fdd1d5aea9decf6d4e544">Write</a> (string FileName, bool with_size) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix collection in a file.  <a href="#a3d95d8a80c5fdd1d5aea9decf6d4e544"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a0c289b98c4bd4ea56dab00a13b88761b">Write</a> (ostream &amp;FileStream, bool with_size) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix collection to an output stream.  <a href="#a0c289b98c4bd4ea56dab00a13b88761b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ac56a7ef93f6f0a217795ce3166b9edae">WriteText</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix collection in a file.  <a href="#ac56a7ef93f6f0a217795ce3166b9edae"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a85ddf0c1055330e18f77175da318432e">WriteText</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix collection to an output stream.  <a href="#a85ddf0c1055330e18f77175da318432e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a11761a3a59b566c4e2d4ae2e020d1b24">Read</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix collection from a file.  <a href="#a11761a3a59b566c4e2d4ae2e020d1b24"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a2c15862a4b7347bff455204fab3ea161">Read</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix collection from an input stream.  <a href="#a2c15862a4b7347bff455204fab3ea161"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetM" ref="ab6f6c5bba065ca82dad7c3abdbc8ea79" args="(const Seldon::SeldonTranspose &amp;status) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetM</b> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7d27412bc9384a5ce0c0db1ee310d31c"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetN" ref="a7d27412bc9384a5ce0c0db1ee310d31c" args="(const Seldon::SeldonTranspose &amp;status) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetN</b> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a453a269dfe7fadba249064363d5ab92a"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetData" ref="a453a269dfe7fadba249064363d5ab92a" args="() const" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetData</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0f2796430deb08e565df8ced656097bf"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetDataConst" ref="a0f2796430deb08e565df8ced656097bf" args="() const" -->
const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataConst</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a505cdfee34741c463e0dc1943337bedc"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetDataVoid" ref="a505cdfee34741c463e0dc1943337bedc" args="() const" -->
void *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataVoid</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5979450cd8801810229f4a24c36e6639"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetDataConstVoid" ref="a5979450cd8801810229f4a24c36e6639" args="() const" -->
const void *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataConstVoid</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af88748a55208349367d6860ad76fd691"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetAllocator" ref="af88748a55208349367d6860ad76fd691" args="()" -->
Allocator&lt; double &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetAllocator</b> ()</td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3137de2c1fe161a746dbde1f10c9128e"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::nz_" ref="a3137de2c1fe161a746dbde1f10c9128e" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3137de2c1fe161a746dbde1f10c9128e">nz_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of non-zero elements. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a67cb0eb87ea633caabecdc58714723ec"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::Mmatrix_" ref="a67cb0eb87ea633caabecdc58714723ec" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a67cb0eb87ea633caabecdc58714723ec">Mmatrix_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of rows of matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a37c185f5d163c0f8a32828828fde7252"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::Nmatrix_" ref="a37c185f5d163c0f8a32828828fde7252" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a37c185f5d163c0f8a32828828fde7252">Nmatrix_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of columns of matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a48d1d3d9da13f7923a6356e49815fe66"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::Mlocal_" ref="a48d1d3d9da13f7923a6356e49815fe66" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_calloc_alloc.php">CallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a48d1d3d9da13f7923a6356e49815fe66">Mlocal_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of rows in the underlying matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af934beebf115742be0a29cb452017a7b"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::Mlocal_sum_" ref="af934beebf115742be0a29cb452017a7b" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_calloc_alloc.php">CallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#af934beebf115742be0a29cb452017a7b">Mlocal_sum_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Cumulative number of rows in the underlying matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6946bf5a2c2a2dbaf307ce2ed1feea09"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::Nlocal_" ref="a6946bf5a2c2a2dbaf307ce2ed1feea09" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_calloc_alloc.php">CallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a6946bf5a2c2a2dbaf307ce2ed1feea09">Nlocal_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of columns in the underlying matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0d31be2a9c37790f5e6ec86ef556ac82"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::Nlocal_sum_" ref="a0d31be2a9c37790f5e6ec86ef556ac82" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_calloc_alloc.php">CallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a0d31be2a9c37790f5e6ec86ef556ac82">Nlocal_sum_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Cumulative number of columns in the underlying matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; int, <a class="el" href="class_seldon_1_1_general.php">General</a>, <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_calloc_alloc.php">CallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a068b8212aa98d09c129bb1952aeee722">collection_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Type of the underlying matrices.  <a href="#a068b8212aa98d09c129bb1952aeee722"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9ecb39a2c309d9cec57f7e97b448771d"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::float_dense_c_" ref="a9ecb39a2c309d9cec57f7e97b448771d" args="" -->
<a class="el" href="class_seldon_1_1_matrix.php">float_dense_c</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a9ecb39a2c309d9cec57f7e97b448771d">float_dense_c_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Pointers of the underlying float dense matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a66e2ad59a6007f1dd4be614a8ade4b85"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::float_sparse_c_" ref="a66e2ad59a6007f1dd4be614a8ade4b85" args="" -->
<a class="el" href="class_seldon_1_1_matrix.php">float_sparse_c</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a66e2ad59a6007f1dd4be614a8ade4b85">float_sparse_c_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Pointers of the underlying float sparse matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aedc9e7d9e1d59170fce9cf501899b0f6"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::double_dense_c_" ref="aedc9e7d9e1d59170fce9cf501899b0f6" args="" -->
<a class="el" href="class_seldon_1_1_matrix.php">double_dense_c</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aedc9e7d9e1d59170fce9cf501899b0f6">double_dense_c_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Pointers of the underlying double dense matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8743b791c26f3513a2e911812083e482"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::double_sparse_c_" ref="a8743b791c26f3513a2e911812083e482" args="" -->
<a class="el" href="class_seldon_1_1_matrix.php">double_sparse_c</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a8743b791c26f3513a2e911812083e482">double_sparse_c_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Pointers of the underlying double sparse matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a96b39ff494d4b7d3e30f320a1c7b4aba"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::m_" ref="a96b39ff494d4b7d3e30f320a1c7b4aba" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>m_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a11cb5d502050e5f14482ff0c9cef5a76"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::n_" ref="a11cb5d502050e5f14482ff0c9cef5a76" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>n_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acb12a8b74699fae7ae3b2b67376795a1"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::data_" ref="acb12a8b74699fae7ae3b2b67376795a1" args="" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-static-attribs"></a>
Static Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a132fa01ce120f352d434fd920e5ad9b4"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::allocator_" ref="a132fa01ce120f352d434fd920e5ad9b4" args="" -->
static Allocator&lt; double &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>allocator_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class Prop0, class Storage0, class Prop1, class Storage1, template&lt; class U &gt; class Allocator&gt;<br/>
 class Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</h3>

<p><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> class made of an heterogeneous collection of matrices. </p>
<p>A collection can refer to matrices of different types : float, double, dense and sparse. </p>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8hxx_source.php#l00042">42</a> of file <a class="el" href="_heterogeneous_matrix_collection_8hxx_source.php">HeterogeneousMatrixCollection.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="aa958b2bb3412cda25f49f24938294553"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::HeterogeneousMatrixCollection" ref="aa958b2bb3412cda25f49f24938294553" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::<a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">HeterogeneousMatrixCollection</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>On exit, the matrix is an empty 0x0 matrix. </p>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00045">45</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a686bd977ef7310cb3339289ec07eefc2"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::HeterogeneousMatrixCollection" ref="a686bd977ef7310cb3339289ec07eefc2" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::<a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">HeterogeneousMatrixCollection</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Main constructor. </p>
<p>Builds a i x j collection matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows of matrices. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns of matrices. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00068">68</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa473473c04fea0716d88de40c87b282b"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::HeterogeneousMatrixCollection" ref="aa473473c04fea0716d88de40c87b282b" args="(const HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0, class Storage0, class Prop1, class Storage1, template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::<a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">HeterogeneousMatrixCollection</a> </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Copy constructor. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>A</em>&nbsp;</td><td>matrix collection to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: <em>A</em> is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00095">95</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="a91100b6eb7a2cea5e86d191ee58749bf"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::Copy" ref="a91100b6eb7a2cea5e86d191ee58749bf" args="(const HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0, class Storage0, class Prop1, class Storage1, template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::Copy </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a matrix collection (assignment operator). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>A</em>&nbsp;</td><td>matrix collection to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: <em>A</em> is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l01188">1188</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2a36130a12ef3b4e4a4971f5898ca0bd"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetDataSize" ref="a2a36130a12ef3b4e4a4971f5898ca0bd" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::GetDataSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored in memory. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements stored in memory. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00362">362</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a289e9f762721738e42b9907450e72e9f"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetDoubleDense" ref="a289e9f762721738e42b9907450e72e9f" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">double_dense_c</a> &amp; <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::GetDoubleDense </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the collection of double dense underlying matrices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the collection of double dense underlying matrices. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00476">476</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9bc398080b4454e0bcbf968fd25fbd78"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetDoubleDense" ref="a9bc398080b4454e0bcbf968fd25fbd78" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">double_dense_c</a> &amp; <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::GetDoubleDense </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the collection of double dense underlying matrices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the collection of double dense underlying matrices. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00492">492</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a39301f5774586f67d31f64a3d4acb0af"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetDoubleSparse" ref="a39301f5774586f67d31f64a3d4acb0af" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">double_sparse_c</a> &amp; <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::GetDoubleSparse </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the collection of double sparse underlying matrices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the collection of double sparse underlying matrices. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00508">508</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1a4f404541bb2b70a36cc1a193ee5cb7"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetDoubleSparse" ref="a1a4f404541bb2b70a36cc1a193ee5cb7" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">double_sparse_c</a> &amp; <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::GetDoubleSparse </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the collection of double sparse underlying matrices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the collection of double sparse underlying matrices. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00524">524</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9bd6c070c0738b3425e5e9b3e0118ec6"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetFloatDense" ref="a9bd6c070c0738b3425e5e9b3e0118ec6" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">float_dense_c</a> &amp; <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::GetFloatDense </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the collection of float dense underlying matrices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the collection of float dense underlying matrices. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00412">412</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa53a233cf9ea59c8711247cc013d75b7"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetFloatDense" ref="aa53a233cf9ea59c8711247cc013d75b7" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">float_dense_c</a> &amp; <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::GetFloatDense </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the collection of float dense underlying matrices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the collection of float dense underlying matrices. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00428">428</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="afbdb423cb862ab27779c8e526f4f05e6"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetFloatSparse" ref="afbdb423cb862ab27779c8e526f4f05e6" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">float_sparse_c</a> &amp; <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::GetFloatSparse </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the collection of float sparse underlying matrices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the collection of float sparse underlying matrices. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00460">460</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a69cf73dbc6d9055bc8ad28633de18f24"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetFloatSparse" ref="a69cf73dbc6d9055bc8ad28633de18f24" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">float_sparse_c</a> &amp; <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::GetFloatSparse </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the collection of float sparse underlying matrices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the collection of float sparse underlying matrices. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00444">444</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7b22f363d1535f911ee271f1e0743c6e"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetM" ref="a7b22f363d1535f911ee271f1e0743c6e" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the total number of rows. It is the sum of the number of rows in the underlying matrices. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a>.</p>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00241">241</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac368f84e127c6738f7f06c74bbc6e7da"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetM" ref="ac368f84e127c6738f7f06c74bbc6e7da" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows in an underlying matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index of the underlying matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows in the underlying matrices with row index <em>i</em>. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00271">271</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a38820efd26db52feaeb8998d2fd43daa"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetMmatrix" ref="a38820efd26db52feaeb8998d2fd43daa" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::GetMmatrix </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the total number of rows. It is the sum of the number of rows in the underlying matrices. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00256">256</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3b788a9ac72e2f32842dcc9251c6ca0f"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetN" ref="a3b788a9ac72e2f32842dcc9251c6ca0f" args="(int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns in an underlying matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index of the underlying matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns in the underlying matrices with column index <em>j</em>. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00325">325</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a03dbde09b4beb73bd66628b4db00df0d"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetN" ref="a03dbde09b4beb73bd66628b4db00df0d" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the total number of columns. It is the sum of the number of columns in the underlying matrices. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a>.</p>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00294">294</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6c59c77733659c863477c9a34fc927fe"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetNmatrix" ref="a6c59c77733659c863477c9a34fc927fe" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::GetNmatrix </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the total number of columns. It is the sum of the number of columns in the underlying matrices. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00309">309</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae5570f5b9a4dac52024ced4061cfe5a8"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetSize" ref="ae5570f5b9a4dac52024ced4061cfe5a8" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::GetSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored in memory. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements stored in memory. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a>.</p>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00347">347</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a69858677f0823951f46671248091be2e"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::GetType" ref="a69858677f0823951f46671248091be2e" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::GetType </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the type of a given underlying matrix. </p>
<p>Type 0 refers to a float dense matrice. Type 1 refers to a float sparse matrice. Type 2 refers to a double dense matrice. Type 3 refers to a double sparse matrice. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row of the given underlying matrix. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column of the given underlying matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The type of the underlying matrix. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00383">383</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a19f1fe843dbb872c589db2116cd204f9"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::Nullify" ref="a19f1fe843dbb872c589db2116cd204f9" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::Nullify </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears a given underlying matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row of the underlying matrix to be nullified. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column of the underlying matrix to be nullified. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00174">174</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="accc5a22477245a42cd0e017157f7d17d"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::operator()" ref="accc5a22477245a42cd0e017157f7d17d" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">double <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l01100">1100</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1f383b28bd0289fc1f00c9e06e597c9e"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::operator=" ref="a1f383b28bd0289fc1f00c9e06e597c9e" args="(const HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0, class Storage0, class Prop1, class Storage1, template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a matrix collection (assignment operator). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>A</em>&nbsp;</td><td>matrix collection to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: <em>A</em> is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l01169">1169</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a788108f4822afc37c6ef4054b37a03bc"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::Print" ref="a788108f4822afc37c6ef4054b37a03bc" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::Print </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Displays the matrix collection on the standard output. </p>
<p>Displays elements on the standard output, in text format. </p>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l01263">1263</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a11761a3a59b566c4e2d4ae2e020d1b24"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::Read" ref="a11761a3a59b566c4e2d4ae2e020d1b24" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix collection from a file. </p>
<p>Reads a matrix collection stored in binary format in a file. The number of rows of matrices (integer) and the number of columns of matrices (integer) are read, and the underlying matrices are then read in the same order as it should be in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>FileName</em>&nbsp;</td><td>input file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l01507">1507</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2c15862a4b7347bff455204fab3ea161"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::Read" ref="a2c15862a4b7347bff455204fab3ea161" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix collection from an input stream. </p>
<p>Reads a matrix collection stored in binary format from a stream. The number of rows of matrices (integer) and the number of columns of matrices (integer) are read, and the underlying matrices are then read in the same order as it should be in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in,out]</tt>&nbsp;</td><td valign="top"><em>FileStream</em>&nbsp;</td><td>input stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l01537">1537</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae600c754d08ba0af893d118cd3a38c60"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::Reallocate" ref="ae600c754d08ba0af893d118cd3a38c60" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::Reallocate </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates memory to resize the matrix collection. </p>
<p>On exit, the matrix is a matrix collection with <em>i</em> x <em>j</em> underlying matrices. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows of matrices. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns of matrices. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Depending on your allocator, data may be lost. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l00546">546</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3d95d8a80c5fdd1d5aea9decf6d4e544"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::Write" ref="a3d95d8a80c5fdd1d5aea9decf6d4e544" args="(string FileName, bool with_size) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix collection in a file. </p>
<p>Stores the matrix collection in a file in binary format. The number of rows of matrices (integer) and the number of columns of matrices (integer) are written, and the underlying matrices are then written in the same order as in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the dimensions of the matrix are not saved. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l01287">1287</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0c289b98c4bd4ea56dab00a13b88761b"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::Write" ref="a0c289b98c4bd4ea56dab00a13b88761b" args="(ostream &amp;FileStream, bool with_size) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix collection to an output stream. </p>
<p>Writes the matrix collection to an output stream in binary format. The number of rows of matrices (integer) and the number of columns of matrices (integer) are written, and the underlying matrices are then written in the same order as in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in,out]</tt>&nbsp;</td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output stream. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the dimensions of the matrix are not saved. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l01318">1318</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a85ddf0c1055330e18f77175da318432e"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::WriteText" ref="a85ddf0c1055330e18f77175da318432e" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix collection to an output stream. </p>
<p>Stores the matrix to an output stream in text format. Only the underlying matrices are written, without the dimensions. Each row is written on a single line and elements of a row are delimited by tabulations. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in,out]</tt>&nbsp;</td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l01433">1433</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac56a7ef93f6f0a217795ce3166b9edae"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::WriteText" ref="ac56a7ef93f6f0a217795ce3166b9edae" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0 , class Storage0 , class Prop1 , class Storage1 , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix collection in a file. </p>
<p>Stores the matrix in a file in text format. Only the underlying matrices are written, without the dimensions. Each row is written on a single line and elements of a row are delimited by tabulations. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l01401">1401</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Data Documentation</h2>
<a class="anchor" id="a068b8212aa98d09c129bb1952aeee722"></a><!-- doxytag: member="Seldon::HeterogeneousMatrixCollection::collection_" ref="a068b8212aa98d09c129bb1952aeee722" args="" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class Prop0, class Storage0, class Prop1, class Storage1, template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;int, <a class="el" href="class_seldon_1_1_general.php">General</a>, <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, <a class="el" href="class_seldon_1_1_calloc_alloc.php">CallocAlloc</a>&lt;int&gt; &gt; <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;::<a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a068b8212aa98d09c129bb1952aeee722">collection_</a><code> [protected]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Type of the underlying matrices. </p>
<p>Type 0 refers to float dense matrices. Type 1 refers to float sparse matrices. Type 2 refers to double dense matrices. Type 3 refers to double sparse matrices. </p>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8hxx_source.php#l00088">88</a> of file <a class="el" href="_heterogeneous_matrix_collection_8hxx_source.php">HeterogeneousMatrixCollection.hxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>matrix/<a class="el" href="_heterogeneous_matrix_collection_8hxx_source.php">HeterogeneousMatrixCollection.hxx</a></li>
<li>matrix/<a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
