<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Eigenvalue Solvers </h1>  </div>
</div>
<div class="contents">
<p>Seldon is interfaced with libraries performing the research of eigenvalues and eigenvectors of very large sparse linear systems : <b><a href="http://www.caam.rice.edu/software/ARPACK/">Arpack</a></b> and <b><a href="http://trilinos.sandia.gov/packages/anasazi/">Anasazi</a></b>. An example file is located in test/program/eigenvalue_test.cpp. For an efficient computation, the user should compile with one of the direct solvers interfaced by <a class="el" href="namespace_seldon.php" title="Seldon namespace.">Seldon</a> (ie. Mumps, Pastix, SuperLU or UmfPack). For example, if you are compiling with SuperLU and Arpack, you have to type. </p>
<pre class="fragment">g++ -DSELDON_WITH_SUPERLU -DSELDON_WITH_ARPACK eigenvalue_test.cpp -ISuperLUdir/SRC -LSuperLUdir -lsuperlu -LArpackdir -larpack </pre><p>where <b>Arpackdir</b> denotes the directory where Arpack has been installed. </p>
<h2>Syntax</h2>
<p>The syntax of all eigenvalue solvers is similar </p>
<pre class="syntax-box">
void GetEigenvaluesEigenvectors(EigenPb&amp;, Vector&amp; lambda, Vector&amp; lambda_imag, Matrix&amp; eigen_vec);
</pre><p>The interface has been done only for double precision (real or complex numbers), since single precision is not accurate enough when seeking eigenvalues of very large sparse linear systems.</p>
<h2>Basic use</h2>
<p>We provide an example of eigenvalue resolution with a sparse matrix.</p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// first we construct a sparse matrix
int n = 1000; // size of linear system
// we assume that you will construct A correctly
Matrix&lt;double, General, ArrayRowSparse&gt; A(n, n);
// if A is symmetric, prefer a symmetric storage
// so that dsaupd routine will be called (more efficient)

// then we declare the eigenproblem K x = lambda M x
// where K is the stiffness matrix, and M mass matrix
// the syntax is SparseEigenProblem&lt;T, MatrixStiff&gt; 
// where T is double or complex&lt;double&gt;
// and MatrixStiff the type of the stiffness matrix K
SparseEigenProblem&lt;double, Matrix&lt;double, General, ArrayRowSparse&gt; &gt; var_eig;

// SparseEigenProblem is devoted to the research of eigenvalues for sparse
// matrices (using Seldon format). If you want to consider dense matrices
// you can DenseEigenProblem&lt;T, Prop, Storage&gt; var_eig;
// If you have your own class of Matrix (in which only matrix vector product
// has been defined), use MatrixFreeEigenProblem&lt;T, MatStiff&gt; var_eig;
// where MatStiff is the type of the stiffness matrix

// standard eigenvalue problem =&gt; K = A, and M = I
var_eig.InitMatrix(A);

// setting parameters of eigenproblem
var_eig.SetStoppingCriterion(1e-12);
var_eig.SetNbAskedEigenvalues(10);
// you can ask largest eigenvalues of M^-1 K, smallest eigenvalues
// or eigenvalues closest to a shift sigma
var_eig.SetTypeSpectrum(var_eig.LARGE_EIGENVALUES, 0);
// for small eigenvalues
var_eig.SetTypeSpectrum(var_eig.SMALL_EIGENVALUES, 0);
// eigenvalues clustered around a shift sigma :
double sigma = 0.5;
var_eig.SetTypeSpectrum(var_eig.CENTERED_EIGENVALUES, sigma);

// then you select the computational mode
// REGULAR =&gt; you only need of matrix vector product K x (if M = I)
// SHIFTED =&gt; the matrix (K - sigma M) will be factorized by
//            using an available direct solver
var_eig.SetComputationalMode(var_eig.REGULAR_MODE);

// declaring arrays that will contains eigenvalues and eigenvectors
Vector&lt;double&gt; lambda, lambda_imag;
Matrix&lt;double&gt; eigen_vec;
GetEigenvaluesEigenvectors(var_eig, lambda, lambda_imag, eigen_vec);


// for a generalized eigenvalue problem, you provide both K and M
// default type of M is Matrix&lt;double, Symmetric, ArrayRowSymSparse&gt;
Matrix&lt;double, Symmetric, ArrayRowSymSparse&gt; M(n, n);
Matrix&lt;double, General, ArrayRowSparse&gt; K(n, n);

var_eig.InitMatrix(K, M);

// then you can compute eigenvalues as usual
GetEigenvaluesEigenvectors(var_eig, lambda, lambda_imag, eigen_vec);

// you can also give a specific type for mass Matrix :
SparseEigenProblem&lt;double, Matrix&lt;double, General, RowSparse&gt;, Matrix&lt;double, Symmetric, RowSymSparse&gt; &gt; var_eig2;

// then perform the same operations on var_eig2

</pre></div>  </pre><h2>Advanced use</h2>
<p>It may sometimes be useful to compute eigenvalues by writing only a matrix-vector product. It could happen when the stiffness matrix is not effectively stored, and you wish to know large or small eigenvalues of this matrix with only this matrix-vector product. Here an example how to do that : </p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">


// basic class defining a matrix
// we take here the discretization of 1-D laplacien
// with second-order finite difference method
template&lt;class T&gt;
class Matrix_Laplacian1D
{
protected :
  int n;
  double L;
  
public :
  double dx;
  
  // this method is mandatory
  int GetM() const
  {
    return n;
  }

  // this method is mandatory
  int GetN() const
  {
    return n;
  }

  //  this method is not needed, it is placed here
  // to initializa attributes of this specific class
  void Init(int n_, double L_)
  {
    n = n_;
    L = L_;
    dx = L/(n+1);
  }
  
};

// matrix vector product Y = A*X
// mandatory function and main function
template&lt;class T1, class T2, class T3&gt;
void Mlt(const Matrix_Laplacian1D&lt;T1&gt;&amp; A,
         const Vector&lt;T2&gt;&amp; X, Vector&lt;T3&gt;&amp; Y)
{
  int n = A.GetM();
  Y(0) = 2.0*X(0) - X(1);
  Y(n-1) = 2.0*X(n-1) - X(n-2);
  for (int i = 1; i &lt; n-1; i++)
    Y(i) = 2.0*X(i) - X(i-1) - X(i+1);
  
  Mlt(1.0/(A.dx*A.dx), Y);
}


// returns true if the matrix is complex
// mandatory function
template&lt;class T&gt;
bool IsComplexMatrix(const Matrix_Laplacian1D&lt;T&gt;&amp; A)
{
  return false;
}


// returns true if the matrix is symmetric
// mandatory function so that eigensolver knows if the matrix is symmetric or not
template&lt;class T&gt;
bool IsSymmetricMatrix(const Matrix_Laplacian1D&lt;T&gt;&amp; A)
{
  return true;
}


int main()
{
    // testing matrix-free class (defined by the user)
    Matrix_Laplacian1D&lt;double&gt; K;
    K.Init(200, 2.0);
    
    // setting eigenvalue problem
    MatrixFreeEigenProblem&lt;double, Matrix_Laplacian1D&lt;double&gt; &gt; var_eig;
    var_eig.SetStoppingCriterion(1e-12);
    var_eig.SetNbAskedEigenvalues(5);
    var_eig.SetComputationalMode(var_eig.REGULAR_MODE);
    var_eig.SetTypeSpectrum(var_eig.SMALL_EIGENVALUES, 0, var_eig.SORTED_MODULUS);

    // finding large eigenvalues of K
    var_eig.InitMatrix(K);
    Vector&lt;double&gt; lambda, lambda_imag;
    Matrix&lt;double, General, ColMajor&gt; eigen_vec;

    // effective computation of eigenvalues and eigenvectors    
    GetEigenvaluesEigenvectors(var_eig, lambda, lambda_imag, eigen_vec);    
  }

</pre></div>  </pre><p>Classes DenseEigenProblem, SparseEigenProblem and MatrixFreeEigenProblem are deriving from base class EigenProblem_Base, and are overloading some methods of this base class. Therefore if you need to define a new class of eigenproblems, it could be a good idea to write a derived class. In order to help you out, we have detailed all the methods of EigenProblem_Base : </p>
<table  class="category-table">
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#Init">Init </a> </td><td class="category-table-td">Initialization of the eigenvalue problem   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#InitMatrix">InitMatrix </a> </td><td class="category-table-td">Stiffness matrix (and optionall mass matrix) is given   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#GetComputationalMode">GetComputationalMode </a> </td><td class="category-table-td">Returns the computational mode used (regular, shifted, ...)   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#GetComputationalMode">SetComputationalMode </a> </td><td class="category-table-td">Sets the computational mode used (regular, shifted, ...)   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#GetNbAskedEigenvalues">GetNbAskedEigenvalues </a> </td><td class="category-table-td">Returns the number of wanted eigenvalues   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#GetNbAskedEigenvalues">SetNbAskedEigenvalues </a> </td><td class="category-table-td">Sets the number of wanted eigenvalues   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#GetTypeSpectrum">GetTypeSpectrum </a> </td><td class="category-table-td">Returns the type of spectrum wanted by the user   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#GetTypeSpectrum">SetTypeSpectrum </a> </td><td class="category-table-td">Sets the of spectrum wanted by the user   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#GetTypeSorting">GetTypeSorting </a> </td><td class="category-table-td">Returns how eigenvalues are sorted (real part, modulus, etc)   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#GetShiftValue">GetShiftValue </a> </td><td class="category-table-td">Returns the shift   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#GetShiftValue">GetImagShiftValue </a> </td><td class="category-table-td">Returns imaginary part of the shift (real unsymmetric problem)   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#SetCholeskyFactoForMass">SetCholeskyFactoForMass </a> </td><td class="category-table-td">Tells to find eigenvalues of L<sup>-1</sup> K L<sup>-T</sup> if M = L L<sup>T</sup>   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#SetCholeskyFactoForMass">UseCholeskyFactoForMass </a> </td><td class="category-table-td">Returns true if eigenvalues of L<sup>-1</sup> K L<sup>-T</sup>are searched (M = L L<sup>T</sup>)   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#SetDiagonalMass">SetDiagonalMass </a> </td><td class="category-table-td">Tells to find eigenvalues of M<sup>-1/2</sup> K M<sup>-1/2</sup>are searched (M diagonal)   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#SetDiagonalMass">DiagonalMass </a> </td><td class="category-table-td">Returns true if eigenvalues of M<sup>-1/2</sup> K M<sup>-1/2</sup>are searched (M diagonal)   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#SetStoppingCriterion">SetStoppingCriterion </a> </td><td class="category-table-td">Sets the stopping criterion used by iterative process   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#SetStoppingCriterion">GetStoppingCriterion </a> </td><td class="category-table-td">Returns the stopping criterion used by iterative process   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#SetNbMaximumIterations">SetNbMaximumIterations </a> </td><td class="category-table-td">Sets the maximum number of iterations allowed for the iterative process   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#SetNbMaximumIterations">GetNbMaximumIterations </a> </td><td class="category-table-td">Returns the maximum number of iterations allowed for the iterative process   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#GetNbMatrixVectorProducts">GetNbMatrixVectorProducts </a> </td><td class="category-table-td">Returns the number of iterations performed by the iterative process   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#GetNbArnoldiVectors">GetNbArnoldiVectors </a> </td><td class="category-table-td">Returns the number of Arnoldi vectors   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#GetNbArnoldiVectors">SetNbArnoldiVectors </a> </td><td class="category-table-td">Sets the number of Arnoldi vectors   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#GetM">GetM </a> </td><td class="category-table-td">Returns the number of rows of the stiffness matrix   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#GetM">GetN </a> </td><td class="category-table-td">Returns the number of columns of the stiffness matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#GetPrintLevel">GetPrintLevel </a> </td><td class="category-table-td">Returns the print level   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#GetPrintLevel">SetPrintLevel </a> </td><td class="category-table-td">Sets the print level   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#IncrementProdMatVect">IncrementProdMatVect </a> </td><td class="category-table-td">Increments the number of iterations performed by the iterative process   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#PrintErrorInit">PrintErrorInit </a> </td><td class="category-table-td">Prints an error message if InitMatrix has not been called   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#IsSymmetricProblem">IsSymmetricProblem </a> </td><td class="category-table-td">Returns true if the stiffness matrix is symmetric   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#FactorizeDiagonalMass">FactorizeDiagonalMass </a> </td><td class="category-table-td">Computation of M<sup>1/2</sup>, once M is known   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#MltSqrtDiagonalMass">MltInvSqrtDiagonalMass </a> </td><td class="category-table-td">Multiplication by M<sup>-1/2</sup>   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#MltSqrtDiagonalMass">MltSqrtDiagonalMass </a> </td><td class="category-table-td">Multiplication by M<sup>1/2</sup>   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#ComputeDiagonalMass">ComputeDiagonalMass </a> </td><td class="category-table-td">Computation of diagonal of M  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#ComputeMassForCholesky">ComputeMassForCholesky </a> </td><td class="category-table-td">Multiplication by M<sup>-1/2</sup>   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#ComputeMassMatrix">ComputeMassMatrix </a> </td><td class="category-table-td">Computation of mass matrix M   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#MltMass">MltMass </a> </td><td class="category-table-td">Multiplication by mass matrix M   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#ComputeStiffnessMatrix">ComputeStiffnessMatrix </a> </td><td class="category-table-td">Computation of stiffness matrix K   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#MltStiffness">MltStiffness </a> </td><td class="category-table-td">Multiplication by stiffness matrix K   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#ComputeAndFactorizeStiffnessMatrix">ComputeAndFactorizeStiffnessMatrix </a> </td><td class="category-table-td">Computation and factorization of a M + b K   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#ComputeSolution">ComputeSolution </a> </td><td class="category-table-td">Computation and factorization of a M + b K   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#FactorizeCholeskyMass">FactorizeCholeskyMass </a> </td><td class="category-table-td">Computation of Cholesky factor L of mass matrix (M = L L<sup>T</sup>   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#MltCholeskyMass">MltCholeskyMass </a> </td><td class="category-table-td">Multiplication by L or L<sup>T</sup>   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#MltCholeskyMass">SolveCholeskyMass </a> </td><td class="category-table-td">Resolution of L x = b or L<sup>T</sup> x = b   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#Clear">Clear </a> </td><td class="category-table-td">Clears memory used by factorizations (if any present)  </td></tr>
</table>
<p>The choice of eigensolver is made when calling function <a href="#GetEigenvaluesEigenvectors">GetEigenvaluesEigenvectors</a> by providing an optional argument. If this argument is not provided, default eigenvalue solver will be used (Arpack). </p>
<p><br/>
 <br/>
</p>
<div class="separator"><a class="anchor" id="Init"></a></div><h3>Init</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void Init(int n);
</pre><p>This method is actually called by each eigenvalue solver before starting the research of eigenvalues. For example, it resets the number of iterations. This method should not be overloaded, neither called by the user. </p>
<h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="InitMatrix"></a></div><h3>InitMatrix</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void InitMatrix(Matrix&amp; K);
  void InitMatrix(Matrix&amp; K, Matrix&amp; M);
</pre><p>This method allows the initialization of pointers for the stiffness matrix and mass matrix. It is mandatory to call it when using SparseEigenProblem, DenseEigenProblem or MatrixFreeEigenProblem. </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">

{

// declaration of the eigenproblem
DenseEigenProblem&lt;double, General, RowMajor&gt; var_eig;
Vector&lt;double&gt; eigen_values, eigen_imag;
Matrix&lt;double&gt; eigen_vec;

// you can set some parameters
var_eig.SetNbAskedEigenvalues(5);
var_eig.SetComputationalMode(var_eig.REGULAR_MODE);
var_eig.SetSpectrum(var_eig.LARGE_EIGENVALUES, 0);

// then you can construct the stiffness matrix
int n = 20;
Matrix&lt;double, General, RowMajor&gt; K(n, n);
K.FillRand();

// and you provide this matrix to the eigenproblem
// standard eigenvalue problem K x = lambda x
var_eig.InitMatrix(K);

// then you can call GetEigenvaluesEigenvectors
GetEigenvaluesEigenvectors(var_eig, lambda, lambda_imag, eigen_vec);

// for a generalized eigenvalue problem, provide M
Matrix&lt;double, Symmetric, RowSymPacked&gt; M(n, n);
M.SetIdentity();
// searching K x = lambda M x
var_eig.InitMatrix(K, M);

// then you can call GetEigenvaluesEigenvectors
GetEigenvaluesEigenvectors(var_eig, lambda, lambda_imag, eigen_vec);

// if the type of M is different from Matrix&lt;double, Symmetric, RowSymPacked&gt;
// and is Matrix&lt;Tm, PropM, StorageM&gt;
// use DenseEigenProblem&lt;double, General, RowMajor, Tm, PropM, StorageM&gt; var_eig;
}

{
// for a sparse eigenproblem, similar stuff
// declaration of the eigenproblem
SparseEigenProblem&lt;double, Matrix&lt;double, General, ArrayRowSparse&gt; &gt; var_eig;
Vector&lt;double&gt; eigen_values, eigen_imag;
Matrix&lt;double&gt; eigen_vec;

// you can set some parameters
var_eig.SetNbAskedEigenvalues(5);
var_eig.SetComputationalMode(var_eig.REGULAR_MODE);
var_eig.SetSpectrum(var_eig.LARGE_EIGENVALUES, 0);

// then you can construct the stiffness matrix
int n = 20;
Matrix&lt;double, General, ArrayRowSparse&gt; K(n, n);
K.FillRand();

// and you provide this matrix to the eigenproblem
// standard eigenvalue problem K x = lambda x
var_eig.InitMatrix(K);

// then you can call GetEigenvaluesEigenvectors
GetEigenvaluesEigenvectors(var_eig, lambda, lambda_imag, eigen_vec);

// for a generalized eigenvalue problem, provide M
Matrix&lt;double, Symmetric, ArrayRowSymSparse&gt; M(n, n);
M.SetIdentity();
// searching K x = lambda M x
var_eig.InitMatrix(K, M);

// then you can call GetEigenvaluesEigenvectors
GetEigenvaluesEigenvectors(var_eig, lambda, lambda_imag, eigen_vec);

// if the type of M is different from Matrix&lt;double, Symmetric, ArrayRowSymSparse&gt;
// and is Matrix&lt;Tm, PropM, StorageM&gt;
// use SparseEigenProblem&lt;double, Matrix&lt;double, General, ArrayRowSparse&gt;,
//                                Matrix&lt;Tm, PropM, StorageM&gt; &gt; var_eig;
}

{
  // matrix-free eigenproblem
  // stiffness matrix has type MyMatrixClass
  MatrixFreeEigenProblem&lt;double, MyMatrixClass&gt; var_eig;

  // you can set some parameters
  var_eig.SetNbAskedEigenvalues(5);
  var_eig.SetComputationalMode(var_eig.REGULAR_MODE);
  var_eig.SetSpectrum(var_eig.LARGE_EIGENVALUES, 0);
 
  // then you can construct the stiffness matrix
  MyMatrixClass K;

  // and you provide this matrix to the eigenproblem
  // standard eigenvalue problem K x = lambda x
  var_eig.InitMatrix(K);

  // then you can call GetEigenvaluesEigenvectors
  GetEigenvaluesEigenvectors(var_eig, lambda, lambda_imag, eigen_vec);

  // no generalized problem for MatrixFreeEigenProblem
}


</pre></div>  </pre><h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="GetComputationalMode"></a></div><h3>GetComputationalMode, SetComputationalMode</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void SetComputationalMode(int);
  int GetComputationalMode();
</pre><p><b>SetComputationalMode</b> sets the computational mode to use for the research of eigenvalues and eigenvectors, whereas <b>GetComputationalMode</b> returns the computation mode used. The default computational mode is REGULAR_MODE. This mode is particularly well suited if you are looking for largest eigenvalues of the matrix. However, it can induce a lot of iterations to converge if smallest eigenvalues are researched, and you can't compute eigenvalues closest to the shift with that mode. The following available modes are (we put equivalent mode numbers in Arpack) : </p>
<ul>
<li>
REGULAR_MODE : regular mode (1 for standard eigenproblem, 2 for generalized one) : K x =  M x  </li>
<li>
SHIFTED_MODE : shifted mode (1 for standard eigenproblem, 3 for generalized one) : (K -  M)<sup>-1</sup> M x =  x &lt; </li>
<li>
IMAG_SHIFTED_MODE : shifted mode and use of imaginary part (4 for generalized eigenproblem, only for real unsymmetric matrices) : Imag{ (K-  M)<sup>-1</sup> M} x =  x  </li>
<li>
INVERT_MODE : multiplication by inverse of M to have a standard eigenvalue problem (only for generalized eigenproblem) : (K -  M)<sup>-1</sup> M x =  x  </li>
<li>
BUCKLING_MODE : available for real generalized symmetric eigenproblems (4 for Arpack) : (K -  M)<sup>-1</sup> K x =  x  </li>
<li>
CAYLEY_MODE : available for real generalized symmetric eigenproblems (5 for Arpack) : (K -  M)<sup>-1</sup> (K +  M) x =  x  </li>
</ul>
<p>INVERT_MODE is not a computational mode of Arpack, but rather a trick in order to use mode 1 for generalized eigenvalue problems. Similarly, if you inform that the mass matrix is diagonal, or if you ask to perform a Cholesky factorization of the mass matrix, GetEigenvaluesEigenvectors will use mode 1 (a regular mode on K or (K -  M)<sup>-1</sup> M ) to compute eigenvalues. However for diagonal mass and Cholesky factorization, the eigenproblem stays symmetric, while INVERT_MODE breaks the symmetry, and the computation should be less efficient. </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">

// declaration of the eigenproblem
SparseEigenProblem&lt;double, Matrix&lt;double, General, ArrayRowSparse&gt; &gt; var_eig;
Vector&lt;double&gt; eigen_values, eigen_imag;
Matrix&lt;double&gt; eigen_vec;

// you can set some parameters
var_eig.SetNbAskedEigenvalues(5);
var_eig.SetSpectrum(var_eig.LARGE_EIGENVALUES, 0);

// and choose the computational mode relevant with researched spectrum
var_eig.SetComputationalMode(var_eig.REGULAR_MODE);

// then you can construct the stiffness matrix
int n = 20;
Matrix&lt;double, General, ArrayRowSparse&gt; K(n, n);
K.FillRand();

// and you provide this matrix to the eigenproblem
// standard eigenvalue problem K x = lambda x
var_eig.InitMatrix(K);

// then you can call GetEigenvaluesEigenvectors
GetEigenvaluesEigenvectors(var_eig, lambda, lambda_imag, eigen_vec);

// for a generalized eigenvalue problem, provide M
Matrix&lt;double, Symmetric, ArrayRowSymSparse&gt; M(n, n);
M.SetIdentity();
// searching K x = lambda M x
var_eig.InitMatrix(K, M);

// searching eigenvalues close to a shift
var_eig.SetTypeSpectrum(var_eig.CENTERED_EIGENVALUES, 0.5);
var_eig.SetComputationalMode(var_eig.SHIFTED_MODE);

// then you can call GetEigenvaluesEigenvectors
GetEigenvaluesEigenvectors(var_eig, lambda, lambda_imag, eigen_vec);

</pre></div>  </pre><h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="GetNbAskedEigenvalues"></a></div><h3>GetNbAskedEigenvalues</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void SetNbAskedEigenvalues(int nev);
  int GetNbAskedEigenvalues();
</pre><p><b>SetNbAskedEigenvalues</b> sets the number of converged eigenvalues you desire to know, whereas <b>GetNbAskedEigenvaluse</b> returns the number of desired eigenvalues. This method is mandatory since the default number of eigenvalues is equal to 0. </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">

// declaration of the eigenproblem
SparseEigenProblem&lt;double, Matrix&lt;double, General, ArrayRowSparse&gt; &gt; var_eig;
Vector&lt;double&gt; eigen_values, eigen_imag;
Matrix&lt;double&gt; eigen_vec;

// you can set some parameters
var_eig.SetNbAskedEigenvalues(5);
var_eig.SetSpectrum(var_eig.LARGE_EIGENVALUES, 0);

// and choose the computational mode relevant with researched spectrum
var_eig.SetComputationalMode(var_eig.REGULAR_MODE);

// then you can construct the stiffness matrix
int n = 20;
Matrix&lt;double, General, ArrayRowSparse&gt; K(n, n);
K.FillRand();

// and you provide this matrix to the eigenproblem
// standard eigenvalue problem K x = lambda x
var_eig.InitMatrix(K);

// then you can call GetEigenvaluesEigenvectors
GetEigenvaluesEigenvectors(var_eig, lambda, lambda_imag, eigen_vec);

</pre></div>  </pre><h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="GetTypeSpectrum"></a></div><h3>GetTypeSpectrum, SetTypeSpectrum</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void SetTypeSpectrum(int type, T sigma);
  void SetTypeSpectrum(int type, T sigma, int type_sort);
  int GetTypeSpectrum();
</pre><p><b>SetTypeSpectrum</b> sets the part of the spectrum you desire to know, whereas <b>GetTypeSpectrum</b> returns the type of researched spectrum. You can ask LARGE_EIGENVALUES, SMALL_EIGENVALUES and CENTERED_EIGENVALUES, to research largest eigenvalues, smallest eigenvalues and eigenvalues closest to the shift sigma. If you search largest or smallest eigenvalues, the shift won't be used, and you can only use REGULAR_MODE. In the case of small eigenvalues, this mode may induce a high number of iterations, it can be a good idea to search eigenvalues close to a small value. type_sort can be used to specify how values are sorted, you can search largest eigenvalues by their modulus, real part or imaginary part (SORTED_MODULUS, SORTED_REAL and SORTED_IMAG). SORTED_MODULUS is the default sorting strategy. </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">

// declaration of the eigenproblem
SparseEigenProblem&lt;double, Matrix&lt;double, General, ArrayRowSparse&gt; &gt; var_eig;
Vector&lt;double&gt; eigen_values, eigen_imag;
Matrix&lt;double&gt; eigen_vec;

// you can set some parameters
var_eig.SetNbAskedEigenvalues(5);
var_eig.SetSpectrum(var_eig.LARGE_EIGENVALUES, 0);

// for eigenvalues closest to 0.25+0.3i
var_eig.SetSpectrum(var_eig.CENTERED_EIGENVALUES, complex&lt;double&gt;(0.25, 0.3), SORTED_IMAG);

if (var_eig.GetTypeSpectrum() != var_eig.CENTERED_EIGENVALUES)
  cout &lt;&lt; "not possible" &lt;&lt; endl;

// and choose the computational mode relevant with researched spectrum
var_eig.SetComputationalMode(var_eig.SHIFTED_MODE);

// then you can construct the stiffness matrix
int n = 20;
Matrix&lt;double, General, ArrayRowSparse&gt; K(n, n);
K.FillRand();

// and you provide this matrix to the eigenproblem
// standard eigenvalue problem K x = lambda x
var_eig.InitMatrix(K);

// then you can call GetEigenvaluesEigenvectors
GetEigenvaluesEigenvectors(var_eig, lambda, lambda_imag, eigen_vec);

</pre></div>  </pre><h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="GetTypeSorting"></a></div><h3>GetTypeSorting</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int GetTypeSorting();
</pre><p>This methods returns the sorting strategy used (SORTED_MODULUS, SORTED_REAL or SORTED_IMAG). </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">

// declaration of the eigenproblem
SparseEigenProblem&lt;double, Matrix&lt;double, General, ArrayRowSparse&gt; &gt; var_eig;
Vector&lt;double&gt; eigen_values, eigen_imag;
Matrix&lt;double&gt; eigen_vec;

// you can set some parameters
var_eig.SetNbAskedEigenvalues(5);
var_eig.SetSpectrum(var_eig.LARGE_EIGENVALUES, 0);

if (var_eig.GetTypeSorting() != var_eig.SORTED_MODULUS)
  cout &lt;&lt; "not possible" &lt;&lt; endl;

// and choose the computational mode relevant with researched spectrum
var_eig.SetComputationalMode(var_eig.REGULAR_MODE);

// then you can construct the stiffness matrix
int n = 20;
Matrix&lt;double, General, ArrayRowSparse&gt; K(n, n);
K.FillRand();

// and you provide this matrix to the eigenproblem
// standard eigenvalue problem K x = lambda x
var_eig.InitMatrix(K);

// then you can call GetEigenvaluesEigenvectors
GetEigenvaluesEigenvectors(var_eig, lambda, lambda_imag, eigen_vec);

</pre></div>  </pre><div class="separator"><a class="anchor" id="GetShiftValue"></a></div><h3>GetShiftValue, GetImagShiftValue</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  T GetShiftValue();
  T GetImagShiftValue();
</pre><p>This methods returns the shift. For real unsymmetric matrices, you can also retrieve imaginary part of the shift </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">

// declaration of the eigenproblem
SparseEigenProblem&lt;double, Matrix&lt;double, General, ArrayRowSparse&gt; &gt; var_eig;
Vector&lt;double&gt; eigen_values, eigen_imag;
Matrix&lt;double&gt; eigen_vec;

// you can set some parameters
var_eig.SetNbAskedEigenvalues(5);

// for eigenvalues closest to 0.25+0.3i
var_eig.SetSpectrum(var_eig.CENTERED_EIGENVALUES, complex&lt;double&gt;(0.25, 0.3), SORTED_IMAG);

// shift_r = 0.25 and shift_i = 0.3
double shift_r = var_eig.GetShiftValue();
double shift_i = var_eig.GetImagShiftValue();

</pre></div>  </pre><div class="separator"><a class="anchor" id="SetCholeskyFactoForMass"></a></div><h3>SetCholeskyFactoForMass, UseCholeskyFactoForMass</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void SetCholeskyFactoForMass(bool);
  void SetCholeskyFactoForMass();
  bool UseCholeskyFactoForMass();
</pre><p>When solving generalized eigenvalue problems K x =  M x, if K and M are symmetric, it can be attractive to perform a Cholesky factorization of M = L L<sup>T</sup>, and consider the standard problem L<sup>-1</sup> K L<sup>-T</sup> x =  x. If you want to use that strategy, <b>SetCholeskyFactoForMass</b> has to be called. <b>UseCholeskyFactoForMass</b> returns true if this strategy is used. </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">

// declaration of the eigenproblem
SparseEigenProblem&lt;double, Matrix&lt;double, General, ArrayRowSparse&gt; &gt; var_eig;
Vector&lt;double&gt; eigen_values, eigen_imag;
Matrix&lt;double&gt; eigen_vec;

// you can set some parameters
var_eig.SetNbAskedEigenvalues(5);
var_eig.SetSpectrum(var_eig.CENTERED_EIGENVALUES, 0.4);

// then use Cholesky factorization
var_eig.SetCholeskyFactoForMass();
if (var_eig.UseCholekyFactoForMass())
    cout &lt;&lt; "Searching eigenvalues of L^-1  K L^-T

// if you want to cancel that, or solve another problem without Cholesky
var_eig.SetCholeskyFactoForMass(false);

// then declare a generalized problem
var_eig.InitMatrix(K, M);

</pre></div>  </pre><div class="separator"><a class="anchor" id="SetDiagonalMass"></a></div><h3>SetDiagonalMass, DiagonalMass</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void SetDiagonalMass();
  void SetDiagonalMass(bool);
  bool DiagonalMass();
</pre><p>When solving generalized eigenvalue problems K x =  M x, if K and M are symmetric and M diagonal, it can be attractive to consider the standard problem M<sup>-1/2</sup> K M<sup>-1/2</sup> x =  x. If you want to use that strategy, <b>SetDiagonalMass</b> has to be called. <b>DiagonalMass</b> returns true if this strategy is used. </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">

// declaration of the eigenproblem
SparseEigenProblem&lt;double, Matrix&lt;double, General, ArrayRowSparse&gt; &gt; var_eig;
Vector&lt;double&gt; eigen_values, eigen_imag;
Matrix&lt;double&gt; eigen_vec;

// you can set some parameters
var_eig.SetNbAskedEigenvalues(5);
var_eig.SetSpectrum(var_eig.CENTERED_EIGENVALUES, 0.4);

// then inform that mass matrix is diagonal
var_eig.SetDiagonalMass();
if (var_eig.DiagonalMass())
    cout &lt;&lt; "Searching eigenvalues of M^-1/2  K M^-1/2

// if you want to cancel that, or solve another problem without diagonal mass
var_eig.SetDiagonalMass(false);

// then declare a generalized problem
var_eig.InitMatrix(K, M);

</pre></div>  </pre><h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="SetStoppingCriterion"></a></div><h3>SetStoppingCriterion, GetStoppingCriterion</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void SetStoppingCriterion(double);
  double GetStoppingCriterion();
</pre><p>With those methods, you can modify and retrieve the stopping criterion used by the iterative algorithm. The default value is equal to 1e-6. </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">

// declaration of the eigenproblem
SparseEigenProblem&lt;double, Matrix&lt;double, General, ArrayRowSparse&gt; &gt; var_eig;
Vector&lt;double&gt; eigen_values, eigen_imag;
Matrix&lt;double&gt; eigen_vec;

// you can set some parameters
var_eig.SetNbAskedEigenvalues(5);
var_eig.SetStoppingCriterion(1e-12);
var_eig.SetSpectrum(var_eig.CENTERED_EIGENVALUES, 0.4);

</pre></div>  </pre><h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="SetNbMaximumIterations"></a></div><h3>SetNbMaximumIterations, GetNbMaximumIterations</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void SetNbMaximumIterations(int);
  int GetNbMaximumIterations();
</pre><p>With those methods, you can modify and retrieve the maximal number of iterations completed by the iterative algorithm. If the iterative algorithm spends more iterations, it is stopped. The default value is equal to 1000. </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">

// declaration of the eigenproblem
SparseEigenProblem&lt;double, Matrix&lt;double, General, ArrayRowSparse&gt; &gt; var_eig;
Vector&lt;double&gt; eigen_values, eigen_imag;
Matrix&lt;double&gt; eigen_vec;

// you can set some parameters
var_eig.SetNbAskedEigenvalues(5);
var_eig.SetStoppingCriterion(1e-12);
var_eig.SetNbMaximumIterations(10000);
var_eig.SetSpectrum(var_eig.CENTERED_EIGENVALUES, 0.4);

</pre></div>  </pre><h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="GetNbMatrixVectorProducts"></a></div><h3>GetNbMatrixVectorProducts</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int GetNbMatrixVectorProducts();
</pre><p>This method returns the number of matrix vector products (for shifted mode, it would be the number of resolutions by operator K - sigma M) performed by the iterative process. </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">

// declaration of the eigenproblem
MatrixFreeEigenProblem&lt;double, MyMatrixClass&gt; var_eig;
Vector&lt;double&gt; eigen_values, eigen_imag;
Matrix&lt;double&gt; eigen_vec;

// you can set some parameters
var_eig.SetNbAskedEigenvalues(5);
var_eig.SetSpectrum(var_eig.LARGE_EIGENVALUES, 0.0);

var_eig.InitMatrix(K);

// research of eigenvalues
GetEigenvaluesEigenvectors(var_eig, eigen_values, eigen_imag, eigen_vec);

// then displaying the number of matrix vector products performed :
cout &lt;&lt; "Number of times K x has been done : " &lt;&lt; var_eig.GetNbMatrixVectorProducts() &lt;&lt; endl;

</pre></div>  </pre><h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="GetNbArnoldiVectors"></a></div><h3>GetNbArnoldiVectors, SetNbArnoldiVectors</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int GetNbArnoldiVectors();
  void SetNbArnoldiVectors(int n);
</pre><p>You can modify and retrieve the number of Arnoldi vectors used by the iterative algorithm. It can be a good idea to increase the number of Arnolid vectors in order to improve the convergence. By default, the number of Arnoldi vectors is set to 2 nev + 2 where nev is the number of asked eigenvalues. </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">

// declaration of the eigenproblem
MatrixFreeEigenProblem&lt;double, MyMatrixClass&gt; var_eig;
Vector&lt;double&gt; eigen_values, eigen_imag;
Matrix&lt;double&gt; eigen_vec;

// you can set some parameters
var_eig.SetNbAskedEigenvalues(5);
var_eig.SetSpectrum(var_eig.LARGE_EIGENVALUES, 0.0);

// and require a larger number of Arnoldi vectors
var_eig.SetNbArnoldiVectors(20);

var_eig.InitMatrix(K);

// research of eigenvalues
GetEigenvaluesEigenvectors(var_eig, eigen_values, eigen_imag, eigen_vec);

cout &lt;&lt; "Number of Arnoldi vectors used by simulation" &lt;&lt; var_eig.GetNbArnoldiVectors() &lt;&lt; endl;

</pre></div>  </pre><h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="GetM"></a></div><h3>GetM, GetN</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int GetM();
  int GetM();
</pre><p><b>GetM</b> returns the number of rows of the stiffness matrix, while <b>GetN</b> returns the number of columns. </p>
<h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="GetPrintLevel"></a></div><h3>GetPrintLevel</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int GetPrintLevel();
  void SetPrintLevel(int n);
</pre><p>By increasing print level, more messages should be displayed on the standard output. </p>
<h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="IncrementProdMatVect"></a></div><h3>IncrementProdMatVect</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
void IncrementProdMatVect();
</pre><p>This method is used internally to increment the variable containing the number of matrix vector products. It should not be called by the user. </p>
<h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="PrintErrorInit"></a></div><h3>PrintErrorInit</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
void PrintErrorInit();
</pre><p>This method is used internally to display an error message and stop the program if InitMatrix has not been called. </p>
<h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="IsSymmetricProblem"></a></div><h3>IsSymmetricProblem</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
bool IsSymmetricProblem();
</pre><p>This method returns true if the eigenvalue problem involves symmetrix mass and stiffness matrices. </p>
<h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="FactorizeDiagonalMass"></a></div><h3>FactorizeDiagonalMass</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
void FactorizeDiagonalMass(Vector&amp;);
</pre><p>This method computes the square root of diagonal mass matrix. It is used internally, and should not be called. However, if you derive a class for your own eigenproblem, and you want to change how diagonal mass matrices are handled, you can overload this method. Then, look at the example detailed in <a href="#MltSqrtDiagonalMass">MltSqrtDiagonalMass</a>. </p>
<h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="MltSqrtDiagonalMass"></a></div><h3>MltSqrtDiagonalMass, MltInvSqrtDiagonalMass</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
void MltSqrtDiagonalMass(Vector&amp;);
void MltInvSqrtDiagonalMass(Vector&amp;);
</pre><p>These methods apply M<sup>1/2</sup> or M<sup>-1/2</sup> to a given vector. It is used internally, and should not be called. However, if you derive a class for your own eigenproblem, and you want to change how diagonal mass matrices are handled, you can overload this method as shown in the example below. </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
template&lt;class T, class MatStiff, class MatMass&gt;
class MyOwnEigenProblem : EigenProblem_Base&lt;T, MatStiff, MatMass&gt;
{
  protected:
  // you can add attributs to handle diagonal mass matrix
  // for example, you could consider a diagonal with only
  // two coefficient D = [alpha I, 0; 0, beta I]
  double alpha, beta, sqrt_alpha, sqrt_beta;

  public :
  
  // computation of diagonal mass matrix
  void ComputeDiagonalMass(Vector&lt;double&gt;&amp; D)
  {
    // you may use Mh
    if (this-&gt;Mh != NULL)
      {
         alpha = (*this-&gt;Mh)(0, 0);
         beta = (*this-&gt;Mh)(1, 1);
      }  
    else
      {
        // or not
        alpha = 1.5; beta = 3.0;
      }
  }  

  // computation of sqrt(D)
  void FactorizeDiagonalMass(Vector&lt;double&gt;&amp; D)
  {
    sqrt_alpha = sqrt(alpha);
    sqrt_beta = sqrt(beta);
  }
  
  // application of sqrt(D)
  void MltSqrtDiagonalMass(Vector&lt;double&gt;&amp; X)
  {
    for (int i = 0; i &lt; X.GetM(); i+=2)
     {
       X(i) *= sqrt_alpha;
       X(i+1) *= sqrt_beta;
     }
  }

  // application of 1/sqrt(D)
  void MltInvSqrtDiagonalMass(Vector&lt;double&gt;&amp; X)
  {
    for (int i = 0; i &lt; X.GetM(); i+=2)
     {
       X(i) /= sqrt_alpha;
       X(i+1) /= sqrt_beta;
     }
  }
};

int main()
{

// then you can use this class :
MyOwnEigenProblem&lt;double, MyStiffMatrix, MyMassMatrix&gt; var_eig;
Vector&lt;double&gt; lambda, lambda_imag;
Matrix&lt;double&gt; eigen_vec;

// set some parameters
var_eig.SetNbAskedEigenvalues(5);
// specify that mass matrix is actually diagonal
var_eig.SetDiagonalMass();

// initialized pointers to matrices
var_eig.InitMatrix(K, M);

// then call computation of eigenvalues
GetEigenvaluesEigenvectors(var_eig, lambda, lambda_imag, eigen_vec);

} 
</pre></div>  </pre><h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="ComputeDiagonalMass"></a></div><h3>ComputeDiagonalMass</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
void ComputeDiagonalMass(Vector&amp;);
</pre><p>This method computes the diagonal of mass matrix. It is used internally, it should not be called directly by the user. However, if you derive a class for your own eigenproblem, and you want to change how diagonal mass matrices are handled, you can overload this method. Then, look at the example detailed in <a href="#MltSqrtDiagonalMass">MltSqrtDiagonalMass</a>. An overload of this single method may be necessary if the extraction of the diagonal from the mass matrix is not standard. </p>
<h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="ComputeMassForCholesky"></a></div><h3>ComputeMassForCholesky</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
void ComputeMassForCholesky();
</pre><p>This method is assumed to compute the mass matrix when a Cholesky factorization is required. It can differ from the computation of the mass matrix when a matrix vector product is required, since that in that latter case, the matrix may be not stored. This method is used internally and should not be called directly by the user. However, if you need to change how Cholesky factorization is handled, you can overload this function as shown in the example of member function <a href="#FactorizeCholeskyMass">FactorizeCholeskyMass</a>. </p>
<h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="ComputeMassMatrix"></a></div><h3>ComputeMassMatrix</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
void ComputeMassMatrix();
</pre><p>This method is assumed to compute the mass matrix when a matrix vector product is required. This method is called internally, and the user should not call it directly. However, if you have your own storage and matrix-vector product with mass matrix, you can overload this function as shown in the example of member function <a href="#MltMass">MltMass</a>. </p>
<h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="MltMass"></a></div><h3>MltMass</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
void MltMass(const Vector&amp; X, Vector&amp; Y);
</pre><p>This method is assumed to compute Y = M X, where M is the mass matrix. This method is called internally, and the user should not call it directly. However, if you have your own storage and matrix-vector product with mass matrix, you can overload this function as shown below : </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
template&lt;class T, class MatStiff, class MatMass&gt;
class MyOwnEigenProblem : EigenProblem_Base&lt;T, MatStiff, MatMass&gt;
{
  protected:
  // you can add attributs to handle mass matrix
  double coef;

  public :
  
  // computation of mass matrix
  void ComputeMassMatrix()
  {
    coef = 2.5;
  }  

  // application of mass matrix (tridiagonal here)
  void MltMass(const Vector&lt;T&gt;&amp; X, Vector&lt;T&gt;&amp; Y)
  {
    int n = X.GetM();
    Y(0) = 4.0*X(0) + X(1);
    Y(n-1) = 4.0*X(n-1) + X(n-2);
    for (int i = 1; i &lt; n-1; i++)
      Y(i) = 4.0*X(i) + X(i-1) + X(i+1);
    
    Mlt(coef, Y);
  }
};

int main()
{

// then you can use this class :
MyOwnEigenProblem&lt;double, MyStiffMatrix, MyMassMatrix&gt; var_eig;
Vector&lt;double&gt; lambda, lambda_imag;
Matrix&lt;double&gt; eigen_vec;

// set some parameters
var_eig.SetNbAskedEigenvalues(5);

// initializing pointers to matrices
var_eig.InitMatrix(K, M);

// then calling computation of eigenvalues
GetEigenvaluesEigenvectors(var_eig, lambda, lambda_imag, eigen_vec);

} 
</pre></div>  </pre><h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="ComputeStiffnessMatrix"></a></div><h3>ComputeStiffnessMatrix</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
void ComputeStiffnessMatrix();
void ComputeStiffnessMatrix(const T&amp; a, const T&amp; b);
</pre><p>This method is assumed to compute the stiffness matrix when a matrix vector product is required. This method is called internally, and the user should not call it directly. However, if you have your own storage and matrix-vector product with stiffness matrix, you can overload this function as shown in the example of member function <a href="#MltStiffness">MltStiffness</a>. One other option is to write your own class for stiffness matrix and overload function Mlt with this stiffness as shown in the section "Advanced Use", then you can use MatrixFreeEigenProblem class. Nevertheless, this class is not appropriate for shift-invert mode (only regular mode can be used). </p>
<h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="MltStiffness"></a></div><h3>MltStiffness</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
void MltStiffness(const Vector&amp; X, Vector&amp; Y);
void MltStiffness(const T&amp; a, const T&amp; b, const Vector&amp; X, Vector&amp; Y);
</pre><p>This method is assumed to compute Y = K X (or Y = (a M + b K) X ), where M is the mass matrix and K the stiffness matrix. This method is called internally, and the user should not call it directly. However, if you have your own storage and matrix-vector product with stiffness matrix, you can overload this function as shown below : </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
template&lt;class T, class MatStiff, class MatMass&gt;
class MyOwnEigenProblem : EigenProblem_Base&lt;T, MatStiff, MatMass&gt;
{
  protected:
  // you can add attributs to handle stiffness matrix
  double alpha, beta, gamma;

  public :
  
  // computation of stiffness matrix
  void ComputeStiffnessMatrix()
  {
    alpha = 1.4;
    beta = 2.5;
    gamma = 0.8;
  }  

  // application of stiffness matrix (tridiagonal here)
  void MltStiffness(const Vector&lt;T&gt;&amp; X, Vector&lt;T&gt;&amp; Y)
  {
    int n = X.GetM();
    Y(0) = beta*X(0) + gamma*X(1);
    Y(n-1) = beta*X(n-1) + alpha*X(n-2);
    for (int i = 1; i &lt; n-1; i++)
      Y(i) = beta*X(i) + alpha*X(i-1) + gamma*X(i+1);
    
  }


  // computation of Y = (a M + b K) X
  void MltStiffness(const T&amp; a, const T&amp; b, const Vector&lt;T&gt;&amp; X, Vector&lt;T&gt;&amp; Y)
  { 
    // to write only for Cayley mode actually ...
    abort();    
  }

  
};

int main()
{

// then you can use this class :
MyOwnEigenProblem&lt;double, MyStiffMatrix, MyMassMatrix&gt; var_eig;
Vector&lt;double&gt; lambda, lambda_imag;
Matrix&lt;double&gt; eigen_vec;

// set some parameters
var_eig.SetNbAskedEigenvalues(5);

// initializing pointers to matrices
var_eig.InitMatrix(K, M);

// then calling computation of eigenvalues
GetEigenvaluesEigenvectors(var_eig, lambda, lambda_imag, eigen_vec);

} 
</pre></div>  </pre><h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="ComputeAndFactorizeStiffnessMatrix"></a></div><h3>ComputeAndFactorizeStiffnessMatrix</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
void ComputeAndFactorizeStiffnessMatrix(const T&amp; a, const T&amp; b);
void ComputeAndFactorizeStiffnessMatrix(const compplex&lt;T&gt;&amp; a, const complex&lt;T&gt;&amp; b, bool real_part);
</pre><p>This method is assumed to compute matrix a M + b K and factorize this matrix. If this matrix is solved by an iterative algorithm, the factorization can consist of constructing a preconditioning. This method is called internally, and the user should not call it directly. However, if you have your own resolution of linear system (a M + b K) y = x , you can overload this function as shown in the example of member function <a href="#ComputeSolution">ComputeSolution</a>. For real unsymmetric matrices, coefficients a and b can be complex and we require real part or imaginary part of the factorization of a M + b K. </p>
<h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="ComputeSolution"></a></div><h3>ComputeSolution</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
void ComputeSolution(const Vector&amp; X, Vector&amp; Y);
</pre><p>This method is assumed to solve (a M + b K) Y = X, where M is the mass matrix and K the stiffness matrix. Coefficients a and b have been given when calling the function ComputeAndFactorizeStiffnessMatrix. This method is called internally, and the user should not call it directly. However, if you have your own resolution of linear system (a M + b K) y = x , you can overload this function as shown below : </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
template&lt;class T, class MatStiff, class MatMass&gt;
class MyOwnEigenProblem : EigenProblem_Base&lt;T, MatStiff, MatMass&gt;
{
  protected:
  // you can add attributs to handle factorization
  double alpha, beta;
  Vector&lt;double&gt; precond;

  public :
  
  // computation of  a M + b K and factorization
  void ComputeAndFactorizeStiffnessMatrix(const T&amp; a, const T&amp; b)
  {
    alpha = a;
    beta = b;
    // iterative process, constructing preconditioning
    // example of diagonal preconditioning
    precond.Reallocate(this-&gt;n_);
    for (int i = 0; i &lt; this-&gt;n_; i++)
      precond(i) = 1.0 / (a*(*this-&gt;Mh)(i, i) + b*(*this-&gt;Kh)(i, i));
  }  

  void ComputeAndFactorizeStiffnessMatrix(const complex&lt;T&gt;&amp; a, const complex&lt;T&gt;&amp; b, bool real_p = true)
  {
    // to implement if real unsymmetric stiffness matrix
    abort();
  }


  // resolution of (a M + b K) y = x
  void ComputeSolution(const Vector&lt;T&gt;&amp; X, Vector&lt;T&gt;&amp; Y)
  {
    // use of conjugate gradient to solve the linear system
    Iteration&lt;double&gt; iter(10000, 1e-12); 
    Cg(*this, Y, X, *this, iter);
  }

  void ApplyFct(const T&amp; a, const Vector&lt;T&gt;&amp; x, const T&amp; b, Vector&lt;T&gt;&amp; y)
  {
    MltAdd(a*alpha, *this-&gt;Mh, x, b, y);
    MltAdd(a*beta, *this-&gt;Kh, x, T(1), y);
  }
 
  // application of preconditioning
  template&lt;class Matrix1&gt;
  void Solve(Matrix1&amp; A, const Vector&lt;T&gt;&amp; r, Vector&lt;T&gt;&amp; z)
  {
    for (int i = 0; i &lt; this-&gt;n_; i++)
      z(i) = r(i)*precond(i);
  }
 
};

template&lt;class T, class MatStiff, class MatMass&gt;
void MltAdd(const T&amp; alpha, MyOwnEigenProblem&lt;T, MatStiff, MatMass&gt;&amp; var,
            const Vector&lt;T&gt;&amp; X, const T&amp; beta, Vector&lt;T&gt;&amp; Y)
{
  var.ApplyFct(alpha, x, beta, y);
}

int main()
{

// then you can use this class :
MyOwnEigenProblem&lt;double, MyStiffMatrix, MyMassMatrix&gt; var_eig;
Vector&lt;double&gt; lambda, lambda_imag;
Matrix&lt;double&gt; eigen_vec;

// set some parameters
var_eig.SetNbAskedEigenvalues(5);
var_eig.SetTypeSpectrum(var_eig.CENTERED_EIGENVALUES, 0.1);
var_eig.SetComputationalMode(var_eig.SHIFTED_MODE);

// initializing pointers to matrices
var_eig.InitMatrix(K, M);

// then calling computation of eigenvalues
GetEigenvaluesEigenvectors(var_eig, lambda, lambda_imag, eigen_vec);

} 
</pre></div>  </pre><h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="FactorizeCholeskyMass"></a></div><h3>FactorizeCholeskyMass</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
void FactorizeCholeskyMass();
</pre><p>This method computes a Cholesky factorization of mass matrix. It is used internally, and should not be called directly. However, if you derive a class for your own eigenproblem, and you want to change how Cholesky factorization is handled, you can overload this method. Then, look at the example detailed in <a href="#MltCholeskyMass">MltCholeskyMass</a>. </p>
<h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="MltCholeskyMass"></a></div><h3>SolveCholeskyMass, MltCholeskyMass</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
void MltCholeskyMass(SeldonTrans, Vector&amp; x);
void SolveCholeskyMass(SeldonTrans, Vector&amp; x);
</pre><p>These methods apply L, L<sup>T</sup>, L<sup>-1</sup> or L<sup>-T</sup> to a given vector. It is used internally, and should not be called directly. However, if you derive a class for your own eigenproblem, and you want to change how Cholesky factorization is handled, you can overload this method as shown in the example below. </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
template&lt;class T, class MatStiff, class MatMass&gt;
class MyOwnEigenProblem : EigenProblem_Base&lt;T, MatStiff, MatMass&gt;
{
  protected:
  // you can add attributs to handle Cholesky mass matrix
  // for example, you could consider a diagonal with only
  // two coefficient D = [alpha I, 0; 0, beta I]
  double alpha, beta, sqrt_alpha, sqrt_beta;

  public :
  
  // computation of mass in preparation of Cholesky factorization
  void ComputeMassForCholesky()
  {
    // you may use Mh
    if (this-&gt;Mh != NULL)
      {
         alpha = (*this-&gt;Mh)(0, 0);
         beta = (*this-&gt;Mh)(1, 1);
      }  
    else
      {
        // or not
        alpha = 1.5; beta = 3.0;
      }
  }  

  // computation of Cholesky factor
  void FactorizeCholeskyMass()
  {
    sqrt_alpha = sqrt(alpha);
    sqrt_beta = sqrt(beta);
  }
  
  // application of L or L^T
  template&lt;class TransposeStatus&gt;
  void MltCholesyMass(const TransposeStatus&amp; TransA, Vector&lt;double&gt;&amp; X)
  {
    for (int i = 0; i &lt; X.GetM(); i+=2)
     {
       X(i) *= sqrt_alpha;
       X(i+1) *= sqrt_beta;
     }
  }

  // application of L^-1 or L^-T
  template&lt;class TransposeStatus&gt;
  void SolveCholeskyMass(const TransposeStatus&amp; TransA, Vector&lt;double&gt;&amp; X)
  {
    for (int i = 0; i &lt; X.GetM(); i+=2)
     {
       X(i) /= sqrt_alpha;
       X(i+1) /= sqrt_beta;
     }
  }
};

int main()
{

// then you can use this class :
MyOwnEigenProblem&lt;double, MyStiffMatrix, MyMassMatrix&gt; var_eig;
Vector&lt;double&gt; lambda, lambda_imag;
Matrix&lt;double&gt; eigen_vec;

// set some parameters
var_eig.SetNbAskedEigenvalues(5);
// specify that you want to use Cholesky factors of mass matrix
var_eig.SetCholeskyFactoForMass();

// initialized pointers to matrices
var_eig.InitMatrix(K, M);

// then call computation of eigenvalues
GetEigenvaluesEigenvectors(var_eig, lambda, lambda_imag, eigen_vec);

} 
</pre></div>  </pre><h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="Clear"></a></div><h3>Clear</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
void Clear();
</pre><p>This method releases memory used for the factorization of mass matrix and/or stiffness matrix. </p>
<h4>Location :</h4>
<p>EigenvalueSolver.cxx</p>
<div class="separator"><a class="anchor" id="GetEigenvaluesEigenvectors"></a></div><h3>GetEigenvaluesEigenvectors</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
template&lt;class EigenPb&gt;
void GetEigenvaluesEigenvectors(EigenPb&amp; var, Vector&amp; lambda, Vector&amp; lambda_imag, Matrix&amp; eigen_vec);</pre><pre class="syntax-box">template&lt;class EigenPb&gt;
void GetEigenvaluesEigenvectors(EigenPb&amp; var, Vector&amp; lambda, Vector&amp; lambda_imag, Matrix&amp; eigen_vec, int type_solver);
</pre><p>This function compute some eigenvalues and eigenvectors of a matrix (dense, sparse or defined by the user) : K x =  x, or a generalized eigenvalue problem : K x =  M x, K being called stiffness matrix and M mass matrix. For dense matrices, you can use DenseEigenProblem. For sparse matrices (<a class="el" href="namespace_seldon.php" title="Seldon namespace.">Seldon</a> format), you can use SparseEigenProblem, it will use direct solvers interfaced by <a class="el" href="namespace_seldon.php" title="Seldon namespace.">Seldon</a> to solve intermediary linear systems (if needed). For matrices defined by the user, you can evaluate largest eigenvalues with only the matrix vector product thanks to the class MatrixFreeEigenProblem. Eigenvectors are placed in a dense matrix, each column being associated with the corresponding eigenvalue. In the case of real unsymmetric eigenvalue problems, the imaginary part of eigenvalues is placed in vector lambda_imag, and the column i of eigen_vec represents the real part u of the eigenvector, the column i+1 stores the imaginary part v (eigenvectors are then equal to u + i v and u - i v ). An optional last argument can be provided in order to select the eigenvalue solver you prefer. </p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">

// declaration of the eigenvalue problem
DenseEigenProblem&lt;double, Symmetric, RowSymPacked&gt; var_eig;

// setting parameters of eigenproblem
var_eig.SetStoppingCriterion(1e-12);
var_eig.SetNbAskedEigenvalues(10);
// you can ask largest eigenvalues of M^-1 K, smallest 
// or eigenvalues closest to a shift sigma
var_eig.SetTypeSpectrum(var_eig.LARGE_EIGENVALUES, 0);

// then you select the computational mode
var_eig.SetComputationalMode(var_eig.REGULAR_MODE);

// giving matrices K and M to the eigenvalue problem
int n = 20;
Matrix&lt;double, Symmetric, RowSymPacked&gt; K(n, n), M(n, n);
K.FillRand(); M.FillRand();
var_eig.InitMatrix(K, M);

// declaring arrays that will contains eigenvalues and eigenvectors
Vector&lt;double&gt; lambda, lambda_imag;
Matrix&lt;double&gt; eigen_vec;
GetEigenvaluesEigenvectors(var_eig, lambda, lambda_imag, eigen_vec);

</pre></div>  </pre><h4>Location :</h4>
<p>EigenvalueSolver.cxx </p>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
