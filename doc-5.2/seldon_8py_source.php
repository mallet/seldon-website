<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>seldon.py</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment"># This file was automatically generated by SWIG (http://www.swig.org).</span>
<a name="l00002"></a>00002 <span class="comment"># Version 2.0.4</span>
<a name="l00003"></a>00003 <span class="comment">#</span>
<a name="l00004"></a>00004 <span class="comment"># Do not make changes to this file unless you know what you are doing--modify</span>
<a name="l00005"></a>00005 <span class="comment"># the SWIG interface file instead.</span>
<a name="l00006"></a>00006 
<a name="l00007"></a>00007 
<a name="l00008"></a>00008 
<a name="l00009"></a>00009 <span class="keyword">from</span> sys <span class="keyword">import</span> version_info
<a name="l00010"></a>00010 <span class="keywordflow">if</span> version_info &gt;= (2,6,0):
<a name="l00011"></a>00011     <span class="keyword">def </span>swig_import_helper():
<a name="l00012"></a>00012         <span class="keyword">from</span> os.path <span class="keyword">import</span> dirname
<a name="l00013"></a>00013         <span class="keyword">import</span> imp
<a name="l00014"></a>00014         fp = <span class="keywordtype">None</span>
<a name="l00015"></a>00015         <span class="keywordflow">try</span>:
<a name="l00016"></a>00016             fp, pathname, description = imp.find_module(<span class="stringliteral">&#39;_seldon&#39;</span>, [dirname(__file__)])
<a name="l00017"></a>00017         <span class="keywordflow">except</span> ImportError:
<a name="l00018"></a>00018             <span class="keyword">import</span> _seldon
<a name="l00019"></a>00019             <span class="keywordflow">return</span> _seldon
<a name="l00020"></a>00020         <span class="keywordflow">if</span> fp <span class="keywordflow">is</span> <span class="keywordflow">not</span> <span class="keywordtype">None</span>:
<a name="l00021"></a>00021             <span class="keywordflow">try</span>:
<a name="l00022"></a>00022                 _mod = imp.load_module(<span class="stringliteral">&#39;_seldon&#39;</span>, fp, pathname, description)
<a name="l00023"></a>00023             <span class="keywordflow">finally</span>:
<a name="l00024"></a>00024                 fp.close()
<a name="l00025"></a>00025             <span class="keywordflow">return</span> _mod
<a name="l00026"></a>00026     _seldon = swig_import_helper()
<a name="l00027"></a>00027     del swig_import_helper
<a name="l00028"></a>00028 <span class="keywordflow">else</span>:
<a name="l00029"></a>00029     <span class="keyword">import</span> _seldon
<a name="l00030"></a>00030 del version_info
<a name="l00031"></a>00031 <span class="keywordflow">try</span>:
<a name="l00032"></a>00032     _swig_property = property
<a name="l00033"></a>00033 <span class="keywordflow">except</span> NameError:
<a name="l00034"></a>00034     <span class="keywordflow">pass</span> <span class="comment"># Python &lt; 2.2 doesn&#39;t have &#39;property&#39;.</span>
<a name="l00035"></a>00035 <span class="keyword">def </span>_swig_setattr_nondynamic(self,class_type,name,value,static=1):
<a name="l00036"></a>00036     <span class="keywordflow">if</span> (name == <span class="stringliteral">&quot;thisown&quot;</span>): <span class="keywordflow">return</span> self.this.own(value)
<a name="l00037"></a>00037     <span class="keywordflow">if</span> (name == <span class="stringliteral">&quot;this&quot;</span>):
<a name="l00038"></a>00038         <span class="keywordflow">if</span> type(value).__name__ == <span class="stringliteral">&#39;SwigPyObject&#39;</span>:
<a name="l00039"></a>00039             self.__dict__[name] = value
<a name="l00040"></a>00040             <span class="keywordflow">return</span>
<a name="l00041"></a>00041     method = class_type.__swig_setmethods__.get(name,<span class="keywordtype">None</span>)
<a name="l00042"></a>00042     <span class="keywordflow">if</span> method: <span class="keywordflow">return</span> method(self,value)
<a name="l00043"></a>00043     <span class="keywordflow">if</span> (<span class="keywordflow">not</span> static):
<a name="l00044"></a>00044         self.__dict__[name] = value
<a name="l00045"></a>00045     <span class="keywordflow">else</span>:
<a name="l00046"></a>00046         <span class="keywordflow">raise</span> AttributeError(<span class="stringliteral">&quot;You cannot add attributes to %s&quot;</span> % self)
<a name="l00047"></a>00047 
<a name="l00048"></a>00048 <span class="keyword">def </span>_swig_setattr(self,class_type,name,value):
<a name="l00049"></a>00049     <span class="keywordflow">return</span> _swig_setattr_nondynamic(self,class_type,name,value,0)
<a name="l00050"></a>00050 
<a name="l00051"></a>00051 <span class="keyword">def </span>_swig_getattr(self,class_type,name):
<a name="l00052"></a>00052     <span class="keywordflow">if</span> (name == <span class="stringliteral">&quot;thisown&quot;</span>): <span class="keywordflow">return</span> self.this.own()
<a name="l00053"></a>00053     method = class_type.__swig_getmethods__.get(name,<span class="keywordtype">None</span>)
<a name="l00054"></a>00054     <span class="keywordflow">if</span> method: <span class="keywordflow">return</span> method(self)
<a name="l00055"></a>00055     <span class="keywordflow">raise</span> AttributeError(name)
<a name="l00056"></a>00056 
<a name="l00057"></a>00057 <span class="keyword">def </span>_swig_repr(self):
<a name="l00058"></a>00058     <span class="keywordflow">try</span>: strthis = <span class="stringliteral">&quot;proxy of &quot;</span> + self.this.__repr__()
<a name="l00059"></a>00059     <span class="keywordflow">except</span>: strthis = <span class="stringliteral">&quot;&quot;</span>
<a name="l00060"></a>00060     <span class="keywordflow">return</span> <span class="stringliteral">&quot;&lt;%s.%s; %s &gt;&quot;</span> % (self.__class__.__module__, self.__class__.__name__, strthis,)
<a name="l00061"></a>00061 
<a name="l00062"></a>00062 <span class="keywordflow">try</span>:
<a name="l00063"></a>00063     _object = object
<a name="l00064"></a>00064     _newclass = 1
<a name="l00065"></a>00065 <span class="keywordflow">except</span> AttributeError:
<a name="l00066"></a><a class="code" href="classseldon_1_1__object.php">00066</a>     <span class="keyword">class </span><a class="code" href="classseldon_1_1__object.php">_object</a> : <span class="keywordflow">pass</span>
<a name="l00067"></a>00067     _newclass = 0
<a name="l00068"></a>00068 
<a name="l00069"></a>00069 
<a name="l00070"></a><a class="code" href="classseldon_1_1_swig_py_iterator.php">00070</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_swig_py_iterator.php">SwigPyIterator</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00071"></a>00071     __swig_setmethods__ = {}
<a name="l00072"></a>00072     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, SwigPyIterator, name, value)
<a name="l00073"></a>00073     __swig_getmethods__ = {}
<a name="l00074"></a>00074     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, SwigPyIterator, name)
<a name="l00075"></a>00075     <span class="keyword">def </span>__init__(self, *args, **kwargs): <span class="keywordflow">raise</span> AttributeError(<span class="stringliteral">&quot;No constructor defined - class is abstract&quot;</span>)
<a name="l00076"></a>00076     __repr__ = _swig_repr
<a name="l00077"></a>00077     __swig_destroy__ = _seldon.delete_SwigPyIterator
<a name="l00078"></a>00078     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00079"></a>00079     <span class="keyword">def </span>value(self): <span class="keywordflow">return</span> _seldon.SwigPyIterator_value(self)
<a name="l00080"></a>00080     <span class="keyword">def </span>incr(self, n = 1): <span class="keywordflow">return</span> _seldon.SwigPyIterator_incr(self, n)
<a name="l00081"></a>00081     <span class="keyword">def </span>decr(self, n = 1): <span class="keywordflow">return</span> _seldon.SwigPyIterator_decr(self, n)
<a name="l00082"></a>00082     <span class="keyword">def </span>distance(self, *args): <span class="keywordflow">return</span> _seldon.SwigPyIterator_distance(self, *args)
<a name="l00083"></a>00083     <span class="keyword">def </span>equal(self, *args): <span class="keywordflow">return</span> _seldon.SwigPyIterator_equal(self, *args)
<a name="l00084"></a>00084     <span class="keyword">def </span>copy(self): <span class="keywordflow">return</span> _seldon.SwigPyIterator_copy(self)
<a name="l00085"></a>00085     <span class="keyword">def </span>next(self): <span class="keywordflow">return</span> _seldon.SwigPyIterator_next(self)
<a name="l00086"></a>00086     <span class="keyword">def </span>__next__(self): <span class="keywordflow">return</span> _seldon.SwigPyIterator___next__(self)
<a name="l00087"></a>00087     <span class="keyword">def </span>previous(self): <span class="keywordflow">return</span> _seldon.SwigPyIterator_previous(self)
<a name="l00088"></a>00088     <span class="keyword">def </span>advance(self, *args): <span class="keywordflow">return</span> _seldon.SwigPyIterator_advance(self, *args)
<a name="l00089"></a>00089     <span class="keyword">def </span>__eq__(self, *args): <span class="keywordflow">return</span> _seldon.SwigPyIterator___eq__(self, *args)
<a name="l00090"></a>00090     <span class="keyword">def </span>__ne__(self, *args): <span class="keywordflow">return</span> _seldon.SwigPyIterator___ne__(self, *args)
<a name="l00091"></a>00091     <span class="keyword">def </span>__iadd__(self, *args): <span class="keywordflow">return</span> _seldon.SwigPyIterator___iadd__(self, *args)
<a name="l00092"></a>00092     <span class="keyword">def </span>__isub__(self, *args): <span class="keywordflow">return</span> _seldon.SwigPyIterator___isub__(self, *args)
<a name="l00093"></a>00093     <span class="keyword">def </span>__add__(self, *args): <span class="keywordflow">return</span> _seldon.SwigPyIterator___add__(self, *args)
<a name="l00094"></a>00094     <span class="keyword">def </span>__sub__(self, *args): <span class="keywordflow">return</span> _seldon.SwigPyIterator___sub__(self, *args)
<a name="l00095"></a>00095     <span class="keyword">def </span>__iter__(self): <span class="keywordflow">return</span> self
<a name="l00096"></a>00096 SwigPyIterator_swigregister = _seldon.SwigPyIterator_swigregister
<a name="l00097"></a>00097 SwigPyIterator_swigregister(SwigPyIterator)
<a name="l00098"></a>00098 
<a name="l00099"></a><a class="code" href="classseldon_1_1ios__base.php">00099</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1ios__base.php">ios_base</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00100"></a>00100     __swig_setmethods__ = {}
<a name="l00101"></a>00101     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, ios_base, name, value)
<a name="l00102"></a>00102     __swig_getmethods__ = {}
<a name="l00103"></a>00103     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, ios_base, name)
<a name="l00104"></a>00104     <span class="keyword">def </span>__init__(self, *args, **kwargs): <span class="keywordflow">raise</span> AttributeError(<span class="stringliteral">&quot;No constructor defined&quot;</span>)
<a name="l00105"></a>00105     __repr__ = _swig_repr
<a name="l00106"></a>00106     erase_event = _seldon.ios_base_erase_event
<a name="l00107"></a>00107     imbue_event = _seldon.ios_base_imbue_event
<a name="l00108"></a>00108     copyfmt_event = _seldon.ios_base_copyfmt_event
<a name="l00109"></a>00109     <span class="keyword">def </span>register_callback(self, *args): <span class="keywordflow">return</span> _seldon.ios_base_register_callback(self, *args)
<a name="l00110"></a>00110     <span class="keyword">def </span>flags(self, *args): <span class="keywordflow">return</span> _seldon.ios_base_flags(self, *args)
<a name="l00111"></a>00111     <span class="keyword">def </span>setf(self, *args): <span class="keywordflow">return</span> _seldon.ios_base_setf(self, *args)
<a name="l00112"></a>00112     <span class="keyword">def </span>unsetf(self, *args): <span class="keywordflow">return</span> _seldon.ios_base_unsetf(self, *args)
<a name="l00113"></a>00113     <span class="keyword">def </span>precision(self, *args): <span class="keywordflow">return</span> _seldon.ios_base_precision(self, *args)
<a name="l00114"></a>00114     <span class="keyword">def </span>width(self, *args): <span class="keywordflow">return</span> _seldon.ios_base_width(self, *args)
<a name="l00115"></a>00115     __swig_getmethods__[<span class="stringliteral">&quot;sync_with_stdio&quot;</span>] = <span class="keyword">lambda</span> x: _seldon.ios_base_sync_with_stdio
<a name="l00116"></a>00116     <span class="keywordflow">if</span> _newclass:sync_with_stdio = staticmethod(_seldon.ios_base_sync_with_stdio)
<a name="l00117"></a>00117     <span class="keyword">def </span>imbue(self, *args): <span class="keywordflow">return</span> _seldon.ios_base_imbue(self, *args)
<a name="l00118"></a>00118     <span class="keyword">def </span>getloc(self): <span class="keywordflow">return</span> _seldon.ios_base_getloc(self)
<a name="l00119"></a>00119     __swig_getmethods__[<span class="stringliteral">&quot;xalloc&quot;</span>] = <span class="keyword">lambda</span> x: _seldon.ios_base_xalloc
<a name="l00120"></a>00120     <span class="keywordflow">if</span> _newclass:xalloc = staticmethod(_seldon.ios_base_xalloc)
<a name="l00121"></a>00121     <span class="keyword">def </span>iword(self, *args): <span class="keywordflow">return</span> _seldon.ios_base_iword(self, *args)
<a name="l00122"></a>00122     <span class="keyword">def </span>pword(self, *args): <span class="keywordflow">return</span> _seldon.ios_base_pword(self, *args)
<a name="l00123"></a>00123     __swig_destroy__ = _seldon.delete_ios_base
<a name="l00124"></a>00124     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00125"></a>00125 ios_base_swigregister = _seldon.ios_base_swigregister
<a name="l00126"></a>00126 ios_base_swigregister(ios_base)
<a name="l00127"></a>00127 cvar = _seldon.cvar
<a name="l00128"></a>00128 ios_base.boolalpha = _seldon.cvar.ios_base_boolalpha
<a name="l00129"></a>00129 ios_base.dec = _seldon.cvar.ios_base_dec
<a name="l00130"></a>00130 ios_base.fixed = _seldon.cvar.ios_base_fixed
<a name="l00131"></a>00131 ios_base.hex = _seldon.cvar.ios_base_hex
<a name="l00132"></a>00132 ios_base.internal = _seldon.cvar.ios_base_internal
<a name="l00133"></a>00133 ios_base.left = _seldon.cvar.ios_base_left
<a name="l00134"></a>00134 ios_base.oct = _seldon.cvar.ios_base_oct
<a name="l00135"></a>00135 ios_base.right = _seldon.cvar.ios_base_right
<a name="l00136"></a>00136 ios_base.scientific = _seldon.cvar.ios_base_scientific
<a name="l00137"></a>00137 ios_base.showbase = _seldon.cvar.ios_base_showbase
<a name="l00138"></a>00138 ios_base.showpoint = _seldon.cvar.ios_base_showpoint
<a name="l00139"></a>00139 ios_base.showpos = _seldon.cvar.ios_base_showpos
<a name="l00140"></a>00140 ios_base.skipws = _seldon.cvar.ios_base_skipws
<a name="l00141"></a>00141 ios_base.unitbuf = _seldon.cvar.ios_base_unitbuf
<a name="l00142"></a>00142 ios_base.uppercase = _seldon.cvar.ios_base_uppercase
<a name="l00143"></a>00143 ios_base.adjustfield = _seldon.cvar.ios_base_adjustfield
<a name="l00144"></a>00144 ios_base.basefield = _seldon.cvar.ios_base_basefield
<a name="l00145"></a>00145 ios_base.floatfield = _seldon.cvar.ios_base_floatfield
<a name="l00146"></a>00146 ios_base.badbit = _seldon.cvar.ios_base_badbit
<a name="l00147"></a>00147 ios_base.eofbit = _seldon.cvar.ios_base_eofbit
<a name="l00148"></a>00148 ios_base.failbit = _seldon.cvar.ios_base_failbit
<a name="l00149"></a>00149 ios_base.goodbit = _seldon.cvar.ios_base_goodbit
<a name="l00150"></a>00150 ios_base.app = _seldon.cvar.ios_base_app
<a name="l00151"></a>00151 ios_base.ate = _seldon.cvar.ios_base_ate
<a name="l00152"></a>00152 ios_base.binary = _seldon.cvar.ios_base_binary
<a name="l00153"></a>00153 ios_base.ios_base_in = _seldon.cvar.ios_base_ios_base_in
<a name="l00154"></a>00154 ios_base.out = _seldon.cvar.ios_base_out
<a name="l00155"></a>00155 ios_base.trunc = _seldon.cvar.ios_base_trunc
<a name="l00156"></a>00156 ios_base.beg = _seldon.cvar.ios_base_beg
<a name="l00157"></a>00157 ios_base.cur = _seldon.cvar.ios_base_cur
<a name="l00158"></a>00158 ios_base.end = _seldon.cvar.ios_base_end
<a name="l00159"></a>00159 
<a name="l00160"></a>00160 <span class="keyword">def </span>ios_base_sync_with_stdio(__sync = True):
<a name="l00161"></a>00161   <span class="keywordflow">return</span> _seldon.ios_base_sync_with_stdio(__sync)
<a name="l00162"></a>00162 ios_base_sync_with_stdio = _seldon.ios_base_sync_with_stdio
<a name="l00163"></a>00163 
<a name="l00164"></a>00164 <span class="keyword">def </span>ios_base_xalloc():
<a name="l00165"></a>00165   <span class="keywordflow">return</span> _seldon.ios_base_xalloc()
<a name="l00166"></a>00166 ios_base_xalloc = _seldon.ios_base_xalloc
<a name="l00167"></a>00167 
<a name="l00168"></a><a class="code" href="classseldon_1_1ios.php">00168</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1ios.php">ios</a>(<a class="code" href="classseldon_1_1ios__base.php">ios_base</a>):
<a name="l00169"></a>00169     __swig_setmethods__ = {}
<a name="l00170"></a>00170     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [ios_base]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l00171"></a>00171     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, ios, name, value)
<a name="l00172"></a>00172     __swig_getmethods__ = {}
<a name="l00173"></a>00173     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [ios_base]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l00174"></a>00174     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, ios, name)
<a name="l00175"></a>00175     __repr__ = _swig_repr
<a name="l00176"></a>00176     <span class="keyword">def </span>rdstate(self): <span class="keywordflow">return</span> _seldon.ios_rdstate(self)
<a name="l00177"></a>00177     <span class="keyword">def </span>clear(self, *args): <span class="keywordflow">return</span> _seldon.ios_clear(self, *args)
<a name="l00178"></a>00178     <span class="keyword">def </span>setstate(self, *args): <span class="keywordflow">return</span> _seldon.ios_setstate(self, *args)
<a name="l00179"></a>00179     <span class="keyword">def </span>good(self): <span class="keywordflow">return</span> _seldon.ios_good(self)
<a name="l00180"></a>00180     <span class="keyword">def </span>eof(self): <span class="keywordflow">return</span> _seldon.ios_eof(self)
<a name="l00181"></a>00181     <span class="keyword">def </span>fail(self): <span class="keywordflow">return</span> _seldon.ios_fail(self)
<a name="l00182"></a>00182     <span class="keyword">def </span>bad(self): <span class="keywordflow">return</span> _seldon.ios_bad(self)
<a name="l00183"></a>00183     <span class="keyword">def </span>exceptions(self, *args): <span class="keywordflow">return</span> _seldon.ios_exceptions(self, *args)
<a name="l00184"></a>00184     <span class="keyword">def </span>__init__(self, *args): 
<a name="l00185"></a>00185         this = _seldon.new_ios(*args)
<a name="l00186"></a>00186         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1ios.php#a9a82c4b6138df245ec05975510e4f184">this</a>.append(this)
<a name="l00187"></a>00187         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1ios.php#a9a82c4b6138df245ec05975510e4f184">this</a> = this
<a name="l00188"></a>00188     __swig_destroy__ = _seldon.delete_ios
<a name="l00189"></a>00189     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00190"></a>00190     <span class="keyword">def </span>tie(self, *args): <span class="keywordflow">return</span> _seldon.ios_tie(self, *args)
<a name="l00191"></a>00191     <span class="keyword">def </span>rdbuf(self, *args): <span class="keywordflow">return</span> _seldon.ios_rdbuf(self, *args)
<a name="l00192"></a>00192     <span class="keyword">def </span>copyfmt(self, *args): <span class="keywordflow">return</span> _seldon.ios_copyfmt(self, *args)
<a name="l00193"></a>00193     <span class="keyword">def </span>fill(self, *args): <span class="keywordflow">return</span> _seldon.ios_fill(self, *args)
<a name="l00194"></a>00194     <span class="keyword">def </span>imbue(self, *args): <span class="keywordflow">return</span> _seldon.ios_imbue(self, *args)
<a name="l00195"></a>00195     <span class="keyword">def </span>narrow(self, *args): <span class="keywordflow">return</span> _seldon.ios_narrow(self, *args)
<a name="l00196"></a>00196     <span class="keyword">def </span>widen(self, *args): <span class="keywordflow">return</span> _seldon.ios_widen(self, *args)
<a name="l00197"></a>00197 ios_swigregister = _seldon.ios_swigregister
<a name="l00198"></a>00198 ios_swigregister(ios)
<a name="l00199"></a>00199 
<a name="l00200"></a><a class="code" href="classseldon_1_1string.php">00200</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1string.php">string</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00201"></a>00201     __swig_setmethods__ = {}
<a name="l00202"></a>00202     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, string, name, value)
<a name="l00203"></a>00203     __swig_getmethods__ = {}
<a name="l00204"></a>00204     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, string, name)
<a name="l00205"></a>00205     __repr__ = _swig_repr
<a name="l00206"></a>00206     <span class="keyword">def </span>length(self): <span class="keywordflow">return</span> _seldon.string_length(self)
<a name="l00207"></a>00207     <span class="keyword">def </span>max_size(self): <span class="keywordflow">return</span> _seldon.string_max_size(self)
<a name="l00208"></a>00208     <span class="keyword">def </span>capacity(self): <span class="keywordflow">return</span> _seldon.string_capacity(self)
<a name="l00209"></a>00209     <span class="keyword">def </span>reserve(self, __res_arg = 0): <span class="keywordflow">return</span> _seldon.string_reserve(self, __res_arg)
<a name="l00210"></a>00210     <span class="keyword">def </span>copy(self, *args): <span class="keywordflow">return</span> _seldon.string_copy(self, *args)
<a name="l00211"></a>00211     <span class="keyword">def </span>c_str(self): <span class="keywordflow">return</span> _seldon.string_c_str(self)
<a name="l00212"></a>00212     <span class="keyword">def </span>find(self, *args): <span class="keywordflow">return</span> _seldon.string_find(self, *args)
<a name="l00213"></a>00213     <span class="keyword">def </span>rfind(self, *args): <span class="keywordflow">return</span> _seldon.string_rfind(self, *args)
<a name="l00214"></a>00214     <span class="keyword">def </span>find_first_of(self, *args): <span class="keywordflow">return</span> _seldon.string_find_first_of(self, *args)
<a name="l00215"></a>00215     <span class="keyword">def </span>find_last_of(self, *args): <span class="keywordflow">return</span> _seldon.string_find_last_of(self, *args)
<a name="l00216"></a>00216     <span class="keyword">def </span>find_first_not_of(self, *args): <span class="keywordflow">return</span> _seldon.string_find_first_not_of(self, *args)
<a name="l00217"></a>00217     <span class="keyword">def </span>find_last_not_of(self, *args): <span class="keywordflow">return</span> _seldon.string_find_last_not_of(self, *args)
<a name="l00218"></a>00218     <span class="keyword">def </span>substr(self, *args): <span class="keywordflow">return</span> _seldon.string_substr(self, *args)
<a name="l00219"></a>00219     <span class="keyword">def </span>empty(self): <span class="keywordflow">return</span> _seldon.string_empty(self)
<a name="l00220"></a>00220     <span class="keyword">def </span>size(self): <span class="keywordflow">return</span> _seldon.string_size(self)
<a name="l00221"></a>00221     <span class="keyword">def </span>swap(self, *args): <span class="keywordflow">return</span> _seldon.string_swap(self, *args)
<a name="l00222"></a>00222     <span class="keyword">def </span>get_allocator(self): <span class="keywordflow">return</span> _seldon.string_get_allocator(self)
<a name="l00223"></a>00223     <span class="keyword">def </span>begin(self): <span class="keywordflow">return</span> _seldon.string_begin(self)
<a name="l00224"></a>00224     <span class="keyword">def </span>end(self): <span class="keywordflow">return</span> _seldon.string_end(self)
<a name="l00225"></a>00225     <span class="keyword">def </span>rbegin(self): <span class="keywordflow">return</span> _seldon.string_rbegin(self)
<a name="l00226"></a>00226     <span class="keyword">def </span>rend(self): <span class="keywordflow">return</span> _seldon.string_rend(self)
<a name="l00227"></a>00227     <span class="keyword">def </span>erase(self, *args): <span class="keywordflow">return</span> _seldon.string_erase(self, *args)
<a name="l00228"></a>00228     <span class="keyword">def </span>__init__(self, *args): 
<a name="l00229"></a>00229         this = _seldon.new_string(*args)
<a name="l00230"></a>00230         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1string.php#af39995dd2532e505b13194af21013ed1">this</a>.append(this)
<a name="l00231"></a>00231         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1string.php#af39995dd2532e505b13194af21013ed1">this</a> = this
<a name="l00232"></a>00232     <span class="keyword">def </span>assign(self, *args): <span class="keywordflow">return</span> _seldon.string_assign(self, *args)
<a name="l00233"></a>00233     <span class="keyword">def </span>resize(self, *args): <span class="keywordflow">return</span> _seldon.string_resize(self, *args)
<a name="l00234"></a>00234     <span class="keyword">def </span>iterator(self): <span class="keywordflow">return</span> _seldon.string_iterator(self)
<a name="l00235"></a>00235     <span class="keyword">def </span>__iter__(self): <span class="keywordflow">return</span> self.iterator()
<a name="l00236"></a>00236     <span class="keyword">def </span>__nonzero__(self): <span class="keywordflow">return</span> _seldon.string___nonzero__(self)
<a name="l00237"></a>00237     <span class="keyword">def </span>__bool__(self): <span class="keywordflow">return</span> _seldon.string___bool__(self)
<a name="l00238"></a>00238     <span class="keyword">def </span>__len__(self): <span class="keywordflow">return</span> _seldon.string___len__(self)
<a name="l00239"></a>00239     <span class="keyword">def </span>__getslice__(self, *args): <span class="keywordflow">return</span> _seldon.string___getslice__(self, *args)
<a name="l00240"></a>00240     <span class="keyword">def </span>__setslice__(self, *args): <span class="keywordflow">return</span> _seldon.string___setslice__(self, *args)
<a name="l00241"></a>00241     <span class="keyword">def </span>__delslice__(self, *args): <span class="keywordflow">return</span> _seldon.string___delslice__(self, *args)
<a name="l00242"></a>00242     <span class="keyword">def </span>__delitem__(self, *args): <span class="keywordflow">return</span> _seldon.string___delitem__(self, *args)
<a name="l00243"></a>00243     <span class="keyword">def </span>__getitem__(self, *args): <span class="keywordflow">return</span> _seldon.string___getitem__(self, *args)
<a name="l00244"></a>00244     <span class="keyword">def </span>__setitem__(self, *args): <span class="keywordflow">return</span> _seldon.string___setitem__(self, *args)
<a name="l00245"></a>00245     <span class="keyword">def </span>insert(self, *args): <span class="keywordflow">return</span> _seldon.string_insert(self, *args)
<a name="l00246"></a>00246     <span class="keyword">def </span>replace(self, *args): <span class="keywordflow">return</span> _seldon.string_replace(self, *args)
<a name="l00247"></a>00247     <span class="keyword">def </span>__iadd__(self, *args): <span class="keywordflow">return</span> _seldon.string___iadd__(self, *args)
<a name="l00248"></a>00248     <span class="keyword">def </span>__add__(self, *args): <span class="keywordflow">return</span> _seldon.string___add__(self, *args)
<a name="l00249"></a>00249     <span class="keyword">def </span>__radd__(self, *args): <span class="keywordflow">return</span> _seldon.string___radd__(self, *args)
<a name="l00250"></a>00250     <span class="keyword">def </span>__str__(self): <span class="keywordflow">return</span> _seldon.string___str__(self)
<a name="l00251"></a>00251     <span class="keyword">def </span>__rlshift__(self, *args): <span class="keywordflow">return</span> _seldon.string___rlshift__(self, *args)
<a name="l00252"></a>00252     <span class="keyword">def </span>__eq__(self, *args): <span class="keywordflow">return</span> _seldon.string___eq__(self, *args)
<a name="l00253"></a>00253     <span class="keyword">def </span>__ne__(self, *args): <span class="keywordflow">return</span> _seldon.string___ne__(self, *args)
<a name="l00254"></a>00254     <span class="keyword">def </span>__gt__(self, *args): <span class="keywordflow">return</span> _seldon.string___gt__(self, *args)
<a name="l00255"></a>00255     <span class="keyword">def </span>__lt__(self, *args): <span class="keywordflow">return</span> _seldon.string___lt__(self, *args)
<a name="l00256"></a>00256     <span class="keyword">def </span>__ge__(self, *args): <span class="keywordflow">return</span> _seldon.string___ge__(self, *args)
<a name="l00257"></a>00257     <span class="keyword">def </span>__le__(self, *args): <span class="keywordflow">return</span> _seldon.string___le__(self, *args)
<a name="l00258"></a>00258     __swig_destroy__ = _seldon.delete_string
<a name="l00259"></a>00259     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00260"></a>00260 string_swigregister = _seldon.string_swigregister
<a name="l00261"></a>00261 string_swigregister(string)
<a name="l00262"></a>00262 string.npos = _seldon.cvar.string_npos
<a name="l00263"></a>00263 
<a name="l00264"></a><a class="code" href="classseldon_1_1ostream.php">00264</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1ostream.php">ostream</a>(<a class="code" href="classseldon_1_1ios.php">ios</a>):
<a name="l00265"></a>00265     __swig_setmethods__ = {}
<a name="l00266"></a>00266     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [ios]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l00267"></a>00267     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, ostream, name, value)
<a name="l00268"></a>00268     __swig_getmethods__ = {}
<a name="l00269"></a>00269     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [ios]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l00270"></a>00270     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, ostream, name)
<a name="l00271"></a>00271     __repr__ = _swig_repr
<a name="l00272"></a>00272     <span class="keyword">def </span>__init__(self, *args): 
<a name="l00273"></a>00273         this = _seldon.new_ostream(*args)
<a name="l00274"></a>00274         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1ostream.php#aa5a703bd0c20f70ed5f7cf43f5f7d7ed">this</a>.append(this)
<a name="l00275"></a>00275         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1ostream.php#aa5a703bd0c20f70ed5f7cf43f5f7d7ed">this</a> = this
<a name="l00276"></a>00276     __swig_destroy__ = _seldon.delete_ostream
<a name="l00277"></a>00277     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00278"></a>00278     <span class="keyword">def </span>__lshift__(self, *args): <span class="keywordflow">return</span> _seldon.ostream___lshift__(self, *args)
<a name="l00279"></a>00279     <span class="keyword">def </span>put(self, *args): <span class="keywordflow">return</span> _seldon.ostream_put(self, *args)
<a name="l00280"></a>00280     <span class="keyword">def </span>write(self, *args): <span class="keywordflow">return</span> _seldon.ostream_write(self, *args)
<a name="l00281"></a>00281     <span class="keyword">def </span>flush(self): <span class="keywordflow">return</span> _seldon.ostream_flush(self)
<a name="l00282"></a>00282     <span class="keyword">def </span>tellp(self): <span class="keywordflow">return</span> _seldon.ostream_tellp(self)
<a name="l00283"></a>00283     <span class="keyword">def </span>seekp(self, *args): <span class="keywordflow">return</span> _seldon.ostream_seekp(self, *args)
<a name="l00284"></a>00284 ostream_swigregister = _seldon.ostream_swigregister
<a name="l00285"></a>00285 ostream_swigregister(ostream)
<a name="l00286"></a>00286 cin = cvar.cin
<a name="l00287"></a>00287 cout = cvar.cout
<a name="l00288"></a>00288 cerr = cvar.cerr
<a name="l00289"></a>00289 clog = cvar.clog
<a name="l00290"></a>00290 
<a name="l00291"></a><a class="code" href="classseldon_1_1istream.php">00291</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1istream.php">istream</a>(<a class="code" href="classseldon_1_1ios.php">ios</a>):
<a name="l00292"></a>00292     __swig_setmethods__ = {}
<a name="l00293"></a>00293     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [ios]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l00294"></a>00294     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, istream, name, value)
<a name="l00295"></a>00295     __swig_getmethods__ = {}
<a name="l00296"></a>00296     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [ios]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l00297"></a>00297     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, istream, name)
<a name="l00298"></a>00298     __repr__ = _swig_repr
<a name="l00299"></a>00299     <span class="keyword">def </span>__init__(self, *args): 
<a name="l00300"></a>00300         this = _seldon.new_istream(*args)
<a name="l00301"></a>00301         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1istream.php#a705ace0296ccf848b964db7ced804993">this</a>.append(this)
<a name="l00302"></a>00302         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1istream.php#a705ace0296ccf848b964db7ced804993">this</a> = this
<a name="l00303"></a>00303     __swig_destroy__ = _seldon.delete_istream
<a name="l00304"></a>00304     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00305"></a>00305     <span class="keyword">def </span>__rshift__(self, *args): <span class="keywordflow">return</span> _seldon.istream___rshift__(self, *args)
<a name="l00306"></a>00306     <span class="keyword">def </span>gcount(self): <span class="keywordflow">return</span> _seldon.istream_gcount(self)
<a name="l00307"></a>00307     <span class="keyword">def </span>get(self, *args): <span class="keywordflow">return</span> _seldon.istream_get(self, *args)
<a name="l00308"></a>00308     <span class="keyword">def </span>getline(self, *args): <span class="keywordflow">return</span> _seldon.istream_getline(self, *args)
<a name="l00309"></a>00309     <span class="keyword">def </span>ignore(self, *args): <span class="keywordflow">return</span> _seldon.istream_ignore(self, *args)
<a name="l00310"></a>00310     <span class="keyword">def </span>peek(self): <span class="keywordflow">return</span> _seldon.istream_peek(self)
<a name="l00311"></a>00311     <span class="keyword">def </span>read(self, *args): <span class="keywordflow">return</span> _seldon.istream_read(self, *args)
<a name="l00312"></a>00312     <span class="keyword">def </span>readsome(self, *args): <span class="keywordflow">return</span> _seldon.istream_readsome(self, *args)
<a name="l00313"></a>00313     <span class="keyword">def </span>putback(self, *args): <span class="keywordflow">return</span> _seldon.istream_putback(self, *args)
<a name="l00314"></a>00314     <span class="keyword">def </span>unget(self): <span class="keywordflow">return</span> _seldon.istream_unget(self)
<a name="l00315"></a>00315     <span class="keyword">def </span>sync(self): <span class="keywordflow">return</span> _seldon.istream_sync(self)
<a name="l00316"></a>00316     <span class="keyword">def </span>tellg(self): <span class="keywordflow">return</span> _seldon.istream_tellg(self)
<a name="l00317"></a>00317     <span class="keyword">def </span>seekg(self, *args): <span class="keywordflow">return</span> _seldon.istream_seekg(self, *args)
<a name="l00318"></a>00318 istream_swigregister = _seldon.istream_swigregister
<a name="l00319"></a>00319 istream_swigregister(istream)
<a name="l00320"></a>00320 
<a name="l00321"></a><a class="code" href="classseldon_1_1iostream.php">00321</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1iostream.php">iostream</a>(<a class="code" href="classseldon_1_1istream.php">istream</a>,<a class="code" href="classseldon_1_1ostream.php">ostream</a>):
<a name="l00322"></a>00322     __swig_setmethods__ = {}
<a name="l00323"></a>00323     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [istream,ostream]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l00324"></a>00324     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, iostream, name, value)
<a name="l00325"></a>00325     __swig_getmethods__ = {}
<a name="l00326"></a>00326     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [istream,ostream]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l00327"></a>00327     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, iostream, name)
<a name="l00328"></a>00328     __repr__ = _swig_repr
<a name="l00329"></a>00329     <span class="keyword">def </span>__init__(self, *args): 
<a name="l00330"></a>00330         this = _seldon.new_iostream(*args)
<a name="l00331"></a>00331         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1iostream.php#ad704f0ba2fd91be99e99c449cb19f82d">this</a>.append(this)
<a name="l00332"></a>00332         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1iostream.php#ad704f0ba2fd91be99e99c449cb19f82d">this</a> = this
<a name="l00333"></a>00333     __swig_destroy__ = _seldon.delete_iostream
<a name="l00334"></a>00334     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00335"></a>00335 iostream_swigregister = _seldon.iostream_swigregister
<a name="l00336"></a>00336 iostream_swigregister(iostream)
<a name="l00337"></a>00337 
<a name="l00338"></a>00338 endl_cb_ptr = _seldon.endl_cb_ptr
<a name="l00339"></a>00339 
<a name="l00340"></a>00340 <span class="keyword">def </span>endl(*args):
<a name="l00341"></a>00341   <span class="keywordflow">return</span> _seldon.endl(*args)
<a name="l00342"></a>00342 endl = _seldon.endl
<a name="l00343"></a>00343 ends_cb_ptr = _seldon.ends_cb_ptr
<a name="l00344"></a>00344 
<a name="l00345"></a>00345 <span class="keyword">def </span>ends(*args):
<a name="l00346"></a>00346   <span class="keywordflow">return</span> _seldon.ends(*args)
<a name="l00347"></a>00347 ends = _seldon.ends
<a name="l00348"></a>00348 flush_cb_ptr = _seldon.flush_cb_ptr
<a name="l00349"></a>00349 
<a name="l00350"></a>00350 <span class="keyword">def </span>flush(*args):
<a name="l00351"></a>00351   <span class="keywordflow">return</span> _seldon.flush(*args)
<a name="l00352"></a>00352 flush = _seldon.flush
<a name="l00353"></a><a class="code" href="classseldon_1_1ifstream.php">00353</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1ifstream.php">ifstream</a>(<a class="code" href="classseldon_1_1istream.php">istream</a>):
<a name="l00354"></a>00354     __swig_setmethods__ = {}
<a name="l00355"></a>00355     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [istream]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l00356"></a>00356     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, ifstream, name, value)
<a name="l00357"></a>00357     __swig_getmethods__ = {}
<a name="l00358"></a>00358     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [istream]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l00359"></a>00359     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, ifstream, name)
<a name="l00360"></a>00360     __repr__ = _swig_repr
<a name="l00361"></a>00361     <span class="keyword">def </span>__init__(self, *args): 
<a name="l00362"></a>00362         this = _seldon.new_ifstream(*args)
<a name="l00363"></a>00363         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1ifstream.php#a29343639ba12d077d48337fcd9ee259d">this</a>.append(this)
<a name="l00364"></a>00364         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1ifstream.php#a29343639ba12d077d48337fcd9ee259d">this</a> = this
<a name="l00365"></a>00365     __swig_destroy__ = _seldon.delete_ifstream
<a name="l00366"></a>00366     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00367"></a>00367     <span class="keyword">def </span>is_open(self): <span class="keywordflow">return</span> _seldon.ifstream_is_open(self)
<a name="l00368"></a>00368     <span class="keyword">def </span>close(self): <span class="keywordflow">return</span> _seldon.ifstream_close(self)
<a name="l00369"></a>00369 ifstream_swigregister = _seldon.ifstream_swigregister
<a name="l00370"></a>00370 ifstream_swigregister(ifstream)
<a name="l00371"></a>00371 
<a name="l00372"></a><a class="code" href="classseldon_1_1ofstream.php">00372</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1ofstream.php">ofstream</a>(<a class="code" href="classseldon_1_1ostream.php">ostream</a>):
<a name="l00373"></a>00373     __swig_setmethods__ = {}
<a name="l00374"></a>00374     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [ostream]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l00375"></a>00375     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, ofstream, name, value)
<a name="l00376"></a>00376     __swig_getmethods__ = {}
<a name="l00377"></a>00377     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [ostream]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l00378"></a>00378     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, ofstream, name)
<a name="l00379"></a>00379     __repr__ = _swig_repr
<a name="l00380"></a>00380     <span class="keyword">def </span>__init__(self, *args): 
<a name="l00381"></a>00381         this = _seldon.new_ofstream(*args)
<a name="l00382"></a>00382         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1ofstream.php#ad41731bc8ce783ad69f092b20f317598">this</a>.append(this)
<a name="l00383"></a>00383         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1ofstream.php#ad41731bc8ce783ad69f092b20f317598">this</a> = this
<a name="l00384"></a>00384     __swig_destroy__ = _seldon.delete_ofstream
<a name="l00385"></a>00385     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00386"></a>00386     <span class="keyword">def </span>is_open(self): <span class="keywordflow">return</span> _seldon.ofstream_is_open(self)
<a name="l00387"></a>00387     <span class="keyword">def </span>close(self): <span class="keywordflow">return</span> _seldon.ofstream_close(self)
<a name="l00388"></a>00388 ofstream_swigregister = _seldon.ofstream_swigregister
<a name="l00389"></a>00389 ofstream_swigregister(ofstream)
<a name="l00390"></a>00390 
<a name="l00391"></a><a class="code" href="classseldon_1_1_error.php">00391</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_error.php">Error</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00392"></a>00392     __swig_setmethods__ = {}
<a name="l00393"></a>00393     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, Error, name, value)
<a name="l00394"></a>00394     __swig_getmethods__ = {}
<a name="l00395"></a>00395     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, Error, name)
<a name="l00396"></a>00396     __repr__ = _swig_repr
<a name="l00397"></a>00397     <span class="keyword">def </span>__init__(self, *args): 
<a name="l00398"></a>00398         this = _seldon.new_Error(*args)
<a name="l00399"></a>00399         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_error.php#a917b2b95119e796615a2aedcbb45c992">this</a>.append(this)
<a name="l00400"></a>00400         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_error.php#a917b2b95119e796615a2aedcbb45c992">this</a> = this
<a name="l00401"></a>00401     __swig_destroy__ = _seldon.delete_Error
<a name="l00402"></a>00402     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00403"></a>00403     <span class="keyword">def </span>What(self): <span class="keywordflow">return</span> _seldon.Error_What(self)
<a name="l00404"></a>00404     <span class="keyword">def </span>CoutWhat(self): <span class="keywordflow">return</span> _seldon.Error_CoutWhat(self)
<a name="l00405"></a>00405 Error_swigregister = _seldon.Error_swigregister
<a name="l00406"></a>00406 Error_swigregister(Error)
<a name="l00407"></a>00407 
<a name="l00408"></a><a class="code" href="classseldon_1_1_undefined.php">00408</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_undefined.php">Undefined</a>(<a class="code" href="classseldon_1_1_error.php">Error</a>):
<a name="l00409"></a>00409     __swig_setmethods__ = {}
<a name="l00410"></a>00410     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [Error]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l00411"></a>00411     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, Undefined, name, value)
<a name="l00412"></a>00412     __swig_getmethods__ = {}
<a name="l00413"></a>00413     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [Error]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l00414"></a>00414     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, Undefined, name)
<a name="l00415"></a>00415     __repr__ = _swig_repr
<a name="l00416"></a>00416     <span class="keyword">def </span>__init__(self, function = &quot;&quot;, comment = &quot;&quot;): 
<a name="l00417"></a>00417         this = _seldon.new_Undefined(function, comment)
<a name="l00418"></a>00418         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_undefined.php#af56e6b163281f9b419b158559243be8a">this</a>.append(this)
<a name="l00419"></a>00419         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_undefined.php#af56e6b163281f9b419b158559243be8a">this</a> = this
<a name="l00420"></a>00420     <span class="keyword">def </span>What(self): <span class="keywordflow">return</span> _seldon.Undefined_What(self)
<a name="l00421"></a>00421     __swig_destroy__ = _seldon.delete_Undefined
<a name="l00422"></a>00422     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00423"></a>00423 Undefined_swigregister = _seldon.Undefined_swigregister
<a name="l00424"></a>00424 Undefined_swigregister(Undefined)
<a name="l00425"></a>00425 
<a name="l00426"></a><a class="code" href="classseldon_1_1_wrong_argument.php">00426</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_wrong_argument.php">WrongArgument</a>(<a class="code" href="classseldon_1_1_error.php">Error</a>):
<a name="l00427"></a>00427     __swig_setmethods__ = {}
<a name="l00428"></a>00428     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [Error]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l00429"></a>00429     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, WrongArgument, name, value)
<a name="l00430"></a>00430     __swig_getmethods__ = {}
<a name="l00431"></a>00431     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [Error]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l00432"></a>00432     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, WrongArgument, name)
<a name="l00433"></a>00433     __repr__ = _swig_repr
<a name="l00434"></a>00434     <span class="keyword">def </span>__init__(self, function = &quot;&quot;, comment = &quot;&quot;): 
<a name="l00435"></a>00435         this = _seldon.new_WrongArgument(function, comment)
<a name="l00436"></a>00436         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_wrong_argument.php#a7749bfd1a38f7be1b373938dd7aaae93">this</a>.append(this)
<a name="l00437"></a>00437         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_wrong_argument.php#a7749bfd1a38f7be1b373938dd7aaae93">this</a> = this
<a name="l00438"></a>00438     <span class="keyword">def </span>What(self): <span class="keywordflow">return</span> _seldon.WrongArgument_What(self)
<a name="l00439"></a>00439     __swig_destroy__ = _seldon.delete_WrongArgument
<a name="l00440"></a>00440     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00441"></a>00441 WrongArgument_swigregister = _seldon.WrongArgument_swigregister
<a name="l00442"></a>00442 WrongArgument_swigregister(WrongArgument)
<a name="l00443"></a>00443 
<a name="l00444"></a><a class="code" href="classseldon_1_1_no_memory.php">00444</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_no_memory.php">NoMemory</a>(<a class="code" href="classseldon_1_1_error.php">Error</a>):
<a name="l00445"></a>00445     __swig_setmethods__ = {}
<a name="l00446"></a>00446     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [Error]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l00447"></a>00447     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, NoMemory, name, value)
<a name="l00448"></a>00448     __swig_getmethods__ = {}
<a name="l00449"></a>00449     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [Error]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l00450"></a>00450     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, NoMemory, name)
<a name="l00451"></a>00451     __repr__ = _swig_repr
<a name="l00452"></a>00452     <span class="keyword">def </span>__init__(self, function = &quot;&quot;, comment = &quot;&quot;): 
<a name="l00453"></a>00453         this = _seldon.new_NoMemory(function, comment)
<a name="l00454"></a>00454         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_no_memory.php#a71e847a6c7a8c177851ab363c4e08ae9">this</a>.append(this)
<a name="l00455"></a>00455         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_no_memory.php#a71e847a6c7a8c177851ab363c4e08ae9">this</a> = this
<a name="l00456"></a>00456     __swig_destroy__ = _seldon.delete_NoMemory
<a name="l00457"></a>00457     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00458"></a>00458 NoMemory_swigregister = _seldon.NoMemory_swigregister
<a name="l00459"></a>00459 NoMemory_swigregister(NoMemory)
<a name="l00460"></a>00460 
<a name="l00461"></a><a class="code" href="classseldon_1_1_wrong_dim.php">00461</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_wrong_dim.php">WrongDim</a>(<a class="code" href="classseldon_1_1_error.php">Error</a>):
<a name="l00462"></a>00462     __swig_setmethods__ = {}
<a name="l00463"></a>00463     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [Error]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l00464"></a>00464     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, WrongDim, name, value)
<a name="l00465"></a>00465     __swig_getmethods__ = {}
<a name="l00466"></a>00466     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [Error]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l00467"></a>00467     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, WrongDim, name)
<a name="l00468"></a>00468     __repr__ = _swig_repr
<a name="l00469"></a>00469     <span class="keyword">def </span>__init__(self, function = &quot;&quot;, comment = &quot;&quot;): 
<a name="l00470"></a>00470         this = _seldon.new_WrongDim(function, comment)
<a name="l00471"></a>00471         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_wrong_dim.php#a0013ddc421bb7355e265ed75f955ec5f">this</a>.append(this)
<a name="l00472"></a>00472         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_wrong_dim.php#a0013ddc421bb7355e265ed75f955ec5f">this</a> = this
<a name="l00473"></a>00473     __swig_destroy__ = _seldon.delete_WrongDim
<a name="l00474"></a>00474     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00475"></a>00475 WrongDim_swigregister = _seldon.WrongDim_swigregister
<a name="l00476"></a>00476 WrongDim_swigregister(WrongDim)
<a name="l00477"></a>00477 
<a name="l00478"></a><a class="code" href="classseldon_1_1_wrong_index.php">00478</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_wrong_index.php">WrongIndex</a>(<a class="code" href="classseldon_1_1_error.php">Error</a>):
<a name="l00479"></a>00479     __swig_setmethods__ = {}
<a name="l00480"></a>00480     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [Error]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l00481"></a>00481     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, WrongIndex, name, value)
<a name="l00482"></a>00482     __swig_getmethods__ = {}
<a name="l00483"></a>00483     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [Error]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l00484"></a>00484     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, WrongIndex, name)
<a name="l00485"></a>00485     __repr__ = _swig_repr
<a name="l00486"></a>00486     <span class="keyword">def </span>__init__(self, function = &quot;&quot;, comment = &quot;&quot;): 
<a name="l00487"></a>00487         this = _seldon.new_WrongIndex(function, comment)
<a name="l00488"></a>00488         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_wrong_index.php#a8b083da4441dd07dfc5d53a25e2682ad">this</a>.append(this)
<a name="l00489"></a>00489         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_wrong_index.php#a8b083da4441dd07dfc5d53a25e2682ad">this</a> = this
<a name="l00490"></a>00490     __swig_destroy__ = _seldon.delete_WrongIndex
<a name="l00491"></a>00491     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00492"></a>00492 WrongIndex_swigregister = _seldon.WrongIndex_swigregister
<a name="l00493"></a>00493 WrongIndex_swigregister(WrongIndex)
<a name="l00494"></a>00494 
<a name="l00495"></a><a class="code" href="classseldon_1_1_wrong_row.php">00495</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_wrong_row.php">WrongRow</a>(<a class="code" href="classseldon_1_1_error.php">Error</a>):
<a name="l00496"></a>00496     __swig_setmethods__ = {}
<a name="l00497"></a>00497     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [Error]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l00498"></a>00498     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, WrongRow, name, value)
<a name="l00499"></a>00499     __swig_getmethods__ = {}
<a name="l00500"></a>00500     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [Error]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l00501"></a>00501     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, WrongRow, name)
<a name="l00502"></a>00502     __repr__ = _swig_repr
<a name="l00503"></a>00503     <span class="keyword">def </span>__init__(self, function = &quot;&quot;, comment = &quot;&quot;): 
<a name="l00504"></a>00504         this = _seldon.new_WrongRow(function, comment)
<a name="l00505"></a>00505         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_wrong_row.php#afa7e9d3fecb898b970fd47448b0dbb7f">this</a>.append(this)
<a name="l00506"></a>00506         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_wrong_row.php#afa7e9d3fecb898b970fd47448b0dbb7f">this</a> = this
<a name="l00507"></a>00507     __swig_destroy__ = _seldon.delete_WrongRow
<a name="l00508"></a>00508     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00509"></a>00509 WrongRow_swigregister = _seldon.WrongRow_swigregister
<a name="l00510"></a>00510 WrongRow_swigregister(WrongRow)
<a name="l00511"></a>00511 
<a name="l00512"></a><a class="code" href="classseldon_1_1_wrong_col.php">00512</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_wrong_col.php">WrongCol</a>(<a class="code" href="classseldon_1_1_error.php">Error</a>):
<a name="l00513"></a>00513     __swig_setmethods__ = {}
<a name="l00514"></a>00514     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [Error]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l00515"></a>00515     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, WrongCol, name, value)
<a name="l00516"></a>00516     __swig_getmethods__ = {}
<a name="l00517"></a>00517     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [Error]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l00518"></a>00518     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, WrongCol, name)
<a name="l00519"></a>00519     __repr__ = _swig_repr
<a name="l00520"></a>00520     <span class="keyword">def </span>__init__(self, function = &quot;&quot;, comment = &quot;&quot;): 
<a name="l00521"></a>00521         this = _seldon.new_WrongCol(function, comment)
<a name="l00522"></a>00522         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_wrong_col.php#a692f8ecf10abd40a2e9f11abeee0efe9">this</a>.append(this)
<a name="l00523"></a>00523         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_wrong_col.php#a692f8ecf10abd40a2e9f11abeee0efe9">this</a> = this
<a name="l00524"></a>00524     __swig_destroy__ = _seldon.delete_WrongCol
<a name="l00525"></a>00525     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00526"></a>00526 WrongCol_swigregister = _seldon.WrongCol_swigregister
<a name="l00527"></a>00527 WrongCol_swigregister(WrongCol)
<a name="l00528"></a>00528 
<a name="l00529"></a><a class="code" href="classseldon_1_1_i_o_error.php">00529</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_i_o_error.php">IOError</a>(<a class="code" href="classseldon_1_1_error.php">Error</a>):
<a name="l00530"></a>00530     __swig_setmethods__ = {}
<a name="l00531"></a>00531     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [Error]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l00532"></a>00532     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, IOError, name, value)
<a name="l00533"></a>00533     __swig_getmethods__ = {}
<a name="l00534"></a>00534     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [Error]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l00535"></a>00535     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, IOError, name)
<a name="l00536"></a>00536     __repr__ = _swig_repr
<a name="l00537"></a>00537     <span class="keyword">def </span>__init__(self, function = &quot;&quot;, comment = &quot;&quot;): 
<a name="l00538"></a>00538         this = _seldon.new_IOError(function, comment)
<a name="l00539"></a>00539         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_i_o_error.php#a4ca93d1bc3e2a3e11deeab03f6c7f3c1">this</a>.append(this)
<a name="l00540"></a>00540         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_i_o_error.php#a4ca93d1bc3e2a3e11deeab03f6c7f3c1">this</a> = this
<a name="l00541"></a>00541     __swig_destroy__ = _seldon.delete_IOError
<a name="l00542"></a>00542     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00543"></a>00543 IOError_swigregister = _seldon.IOError_swigregister
<a name="l00544"></a>00544 IOError_swigregister(IOError)
<a name="l00545"></a>00545 
<a name="l00546"></a><a class="code" href="classseldon_1_1_lapack_error.php">00546</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_lapack_error.php">LapackError</a>(<a class="code" href="classseldon_1_1_error.php">Error</a>):
<a name="l00547"></a>00547     __swig_setmethods__ = {}
<a name="l00548"></a>00548     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [Error]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l00549"></a>00549     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, LapackError, name, value)
<a name="l00550"></a>00550     __swig_getmethods__ = {}
<a name="l00551"></a>00551     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [Error]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l00552"></a>00552     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, LapackError, name)
<a name="l00553"></a>00553     __repr__ = _swig_repr
<a name="l00554"></a>00554     <span class="keyword">def </span>__init__(self, *args): 
<a name="l00555"></a>00555         this = _seldon.new_LapackError(*args)
<a name="l00556"></a>00556         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_lapack_error.php#a670d862536f7699b6ab10ff1182e0670">this</a>.append(this)
<a name="l00557"></a>00557         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_lapack_error.php#a670d862536f7699b6ab10ff1182e0670">this</a> = this
<a name="l00558"></a>00558     <span class="keyword">def </span>What(self): <span class="keywordflow">return</span> _seldon.LapackError_What(self)
<a name="l00559"></a>00559     __swig_destroy__ = _seldon.delete_LapackError
<a name="l00560"></a>00560     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00561"></a>00561 LapackError_swigregister = _seldon.LapackError_swigregister
<a name="l00562"></a>00562 LapackError_swigregister(LapackError)
<a name="l00563"></a>00563 
<a name="l00564"></a><a class="code" href="classseldon_1_1_lapack_info.php">00564</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_lapack_info.php">LapackInfo</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00565"></a>00565     __swig_setmethods__ = {}
<a name="l00566"></a>00566     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, LapackInfo, name, value)
<a name="l00567"></a>00567     __swig_getmethods__ = {}
<a name="l00568"></a>00568     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, LapackInfo, name)
<a name="l00569"></a>00569     __repr__ = _swig_repr
<a name="l00570"></a>00570     <span class="keyword">def </span>__init__(self, *args): 
<a name="l00571"></a>00571         this = _seldon.new_LapackInfo(*args)
<a name="l00572"></a>00572         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_lapack_info.php#afb3bcfef92effc0cf1d678f4795dea08">this</a>.append(this)
<a name="l00573"></a>00573         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_lapack_info.php#afb3bcfef92effc0cf1d678f4795dea08">this</a> = this
<a name="l00574"></a>00574     <span class="keyword">def </span>GetInfo(self): <span class="keywordflow">return</span> _seldon.LapackInfo_GetInfo(self)
<a name="l00575"></a>00575     <span class="keyword">def </span>GetInfoRef(self): <span class="keywordflow">return</span> _seldon.LapackInfo_GetInfoRef(self)
<a name="l00576"></a>00576     __swig_destroy__ = _seldon.delete_LapackInfo
<a name="l00577"></a>00577     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00578"></a>00578 LapackInfo_swigregister = _seldon.LapackInfo_swigregister
<a name="l00579"></a>00579 LapackInfo_swigregister(LapackInfo)
<a name="l00580"></a>00580 
<a name="l00581"></a><a class="code" href="classseldon_1_1_str.php">00581</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_str.php">Str</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00582"></a>00582     __swig_setmethods__ = {}
<a name="l00583"></a>00583     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, Str, name, value)
<a name="l00584"></a>00584     __swig_getmethods__ = {}
<a name="l00585"></a>00585     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, Str, name)
<a name="l00586"></a>00586     __repr__ = _swig_repr
<a name="l00587"></a>00587     <span class="keyword">def </span>__init__(self, *args): 
<a name="l00588"></a>00588         this = _seldon.new_Str(*args)
<a name="l00589"></a>00589         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_str.php#a262715a77bee24f83b78a561f95ec2bb">this</a>.append(this)
<a name="l00590"></a>00590         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_str.php#a262715a77bee24f83b78a561f95ec2bb">this</a> = this
<a name="l00591"></a>00591     __swig_destroy__ = _seldon.delete_Str
<a name="l00592"></a>00592     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00593"></a>00593 Str_swigregister = _seldon.Str_swigregister
<a name="l00594"></a>00594 Str_swigregister(Str)
<a name="l00595"></a>00595 
<a name="l00596"></a><a class="code" href="classseldon_1_1_row_major.php">00596</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_row_major.php">RowMajor</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00597"></a>00597     __swig_setmethods__ = {}
<a name="l00598"></a>00598     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, RowMajor, name, value)
<a name="l00599"></a>00599     __swig_getmethods__ = {}
<a name="l00600"></a>00600     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, RowMajor, name)
<a name="l00601"></a>00601     __repr__ = _swig_repr
<a name="l00602"></a>00602     __swig_getmethods__[<span class="stringliteral">&quot;GetFirst&quot;</span>] = <span class="keyword">lambda</span> x: _seldon.RowMajor_GetFirst
<a name="l00603"></a>00603     <span class="keywordflow">if</span> _newclass:GetFirst = staticmethod(_seldon.RowMajor_GetFirst)
<a name="l00604"></a>00604     __swig_getmethods__[<span class="stringliteral">&quot;GetSecond&quot;</span>] = <span class="keyword">lambda</span> x: _seldon.RowMajor_GetSecond
<a name="l00605"></a>00605     <span class="keywordflow">if</span> _newclass:GetSecond = staticmethod(_seldon.RowMajor_GetSecond)
<a name="l00606"></a>00606     <span class="keyword">def </span>__init__(self): 
<a name="l00607"></a>00607         this = _seldon.new_RowMajor()
<a name="l00608"></a>00608         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_row_major.php#a17c2770e372f269e8f63ef443707d1a4">this</a>.append(this)
<a name="l00609"></a>00609         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_row_major.php#a17c2770e372f269e8f63ef443707d1a4">this</a> = this
<a name="l00610"></a>00610     __swig_destroy__ = _seldon.delete_RowMajor
<a name="l00611"></a>00611     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00612"></a>00612 RowMajor_swigregister = _seldon.RowMajor_swigregister
<a name="l00613"></a>00613 RowMajor_swigregister(RowMajor)
<a name="l00614"></a>00614 
<a name="l00615"></a>00615 <span class="keyword">def </span>RowMajor_GetFirst(*args):
<a name="l00616"></a>00616   <span class="keywordflow">return</span> _seldon.RowMajor_GetFirst(*args)
<a name="l00617"></a>00617 RowMajor_GetFirst = _seldon.RowMajor_GetFirst
<a name="l00618"></a>00618 
<a name="l00619"></a>00619 <span class="keyword">def </span>RowMajor_GetSecond(*args):
<a name="l00620"></a>00620   <span class="keywordflow">return</span> _seldon.RowMajor_GetSecond(*args)
<a name="l00621"></a>00621 RowMajor_GetSecond = _seldon.RowMajor_GetSecond
<a name="l00622"></a>00622 
<a name="l00623"></a><a class="code" href="classseldon_1_1_row_sparse.php">00623</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_row_sparse.php">RowSparse</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00624"></a>00624     __swig_setmethods__ = {}
<a name="l00625"></a>00625     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, RowSparse, name, value)
<a name="l00626"></a>00626     __swig_getmethods__ = {}
<a name="l00627"></a>00627     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, RowSparse, name)
<a name="l00628"></a>00628     __repr__ = _swig_repr
<a name="l00629"></a>00629     __swig_getmethods__[<span class="stringliteral">&quot;GetFirst&quot;</span>] = <span class="keyword">lambda</span> x: _seldon.RowSparse_GetFirst
<a name="l00630"></a>00630     <span class="keywordflow">if</span> _newclass:GetFirst = staticmethod(_seldon.RowSparse_GetFirst)
<a name="l00631"></a>00631     __swig_getmethods__[<span class="stringliteral">&quot;GetSecond&quot;</span>] = <span class="keyword">lambda</span> x: _seldon.RowSparse_GetSecond
<a name="l00632"></a>00632     <span class="keywordflow">if</span> _newclass:GetSecond = staticmethod(_seldon.RowSparse_GetSecond)
<a name="l00633"></a>00633     <span class="keyword">def </span>__init__(self): 
<a name="l00634"></a>00634         this = _seldon.new_RowSparse()
<a name="l00635"></a>00635         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_row_sparse.php#a86142070a136e1401996fcbad648fee9">this</a>.append(this)
<a name="l00636"></a>00636         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_row_sparse.php#a86142070a136e1401996fcbad648fee9">this</a> = this
<a name="l00637"></a>00637     __swig_destroy__ = _seldon.delete_RowSparse
<a name="l00638"></a>00638     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00639"></a>00639 RowSparse_swigregister = _seldon.RowSparse_swigregister
<a name="l00640"></a>00640 RowSparse_swigregister(RowSparse)
<a name="l00641"></a>00641 
<a name="l00642"></a>00642 <span class="keyword">def </span>RowSparse_GetFirst(*args):
<a name="l00643"></a>00643   <span class="keywordflow">return</span> _seldon.RowSparse_GetFirst(*args)
<a name="l00644"></a>00644 RowSparse_GetFirst = _seldon.RowSparse_GetFirst
<a name="l00645"></a>00645 
<a name="l00646"></a>00646 <span class="keyword">def </span>RowSparse_GetSecond(*args):
<a name="l00647"></a>00647   <span class="keywordflow">return</span> _seldon.RowSparse_GetSecond(*args)
<a name="l00648"></a>00648 RowSparse_GetSecond = _seldon.RowSparse_GetSecond
<a name="l00649"></a>00649 
<a name="l00650"></a><a class="code" href="classseldon_1_1_float_double.php">00650</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_float_double.php">FloatDouble</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00651"></a>00651     __swig_setmethods__ = {}
<a name="l00652"></a>00652     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, FloatDouble, name, value)
<a name="l00653"></a>00653     __swig_getmethods__ = {}
<a name="l00654"></a>00654     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, FloatDouble, name)
<a name="l00655"></a>00655     __repr__ = _swig_repr
<a name="l00656"></a>00656     <span class="keyword">def </span>__init__(self): 
<a name="l00657"></a>00657         this = _seldon.new_FloatDouble()
<a name="l00658"></a>00658         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_float_double.php#a14fac85461f91d98a99a0ca95ba3598e">this</a>.append(this)
<a name="l00659"></a>00659         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_float_double.php#a14fac85461f91d98a99a0ca95ba3598e">this</a> = this
<a name="l00660"></a>00660     __swig_destroy__ = _seldon.delete_FloatDouble
<a name="l00661"></a>00661     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00662"></a>00662 FloatDouble_swigregister = _seldon.FloatDouble_swigregister
<a name="l00663"></a>00663 FloatDouble_swigregister(FloatDouble)
<a name="l00664"></a>00664 
<a name="l00665"></a><a class="code" href="classseldon_1_1_col_major_collection.php">00665</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_col_major_collection.php">ColMajorCollection</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00666"></a>00666     __swig_setmethods__ = {}
<a name="l00667"></a>00667     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, ColMajorCollection, name, value)
<a name="l00668"></a>00668     __swig_getmethods__ = {}
<a name="l00669"></a>00669     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, ColMajorCollection, name)
<a name="l00670"></a>00670     __repr__ = _swig_repr
<a name="l00671"></a>00671     <span class="keyword">def </span>__init__(self): 
<a name="l00672"></a>00672         this = _seldon.new_ColMajorCollection()
<a name="l00673"></a>00673         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_col_major_collection.php#af1b63c3ce2764a9ce3421ac3de1e7749">this</a>.append(this)
<a name="l00674"></a>00674         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_col_major_collection.php#af1b63c3ce2764a9ce3421ac3de1e7749">this</a> = this
<a name="l00675"></a>00675     __swig_destroy__ = _seldon.delete_ColMajorCollection
<a name="l00676"></a>00676     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00677"></a>00677 ColMajorCollection_swigregister = _seldon.ColMajorCollection_swigregister
<a name="l00678"></a>00678 ColMajorCollection_swigregister(ColMajorCollection)
<a name="l00679"></a>00679 
<a name="l00680"></a><a class="code" href="classseldon_1_1_row_major_collection.php">00680</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_row_major_collection.php">RowMajorCollection</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00681"></a>00681     __swig_setmethods__ = {}
<a name="l00682"></a>00682     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, RowMajorCollection, name, value)
<a name="l00683"></a>00683     __swig_getmethods__ = {}
<a name="l00684"></a>00684     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, RowMajorCollection, name)
<a name="l00685"></a>00685     __repr__ = _swig_repr
<a name="l00686"></a>00686     <span class="keyword">def </span>__init__(self): 
<a name="l00687"></a>00687         this = _seldon.new_RowMajorCollection()
<a name="l00688"></a>00688         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_row_major_collection.php#a1e3389ec7378d0ad50d8090a0bf9ffb5">this</a>.append(this)
<a name="l00689"></a>00689         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_row_major_collection.php#a1e3389ec7378d0ad50d8090a0bf9ffb5">this</a> = this
<a name="l00690"></a>00690     __swig_destroy__ = _seldon.delete_RowMajorCollection
<a name="l00691"></a>00691     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00692"></a>00692 RowMajorCollection_swigregister = _seldon.RowMajorCollection_swigregister
<a name="l00693"></a>00693 RowMajorCollection_swigregister(RowMajorCollection)
<a name="l00694"></a>00694 
<a name="l00695"></a><a class="code" href="classseldon_1_1_col_sym_packed_collection.php">00695</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_col_sym_packed_collection.php">ColSymPackedCollection</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00696"></a>00696     __swig_setmethods__ = {}
<a name="l00697"></a>00697     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, ColSymPackedCollection, name, value)
<a name="l00698"></a>00698     __swig_getmethods__ = {}
<a name="l00699"></a>00699     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, ColSymPackedCollection, name)
<a name="l00700"></a>00700     __repr__ = _swig_repr
<a name="l00701"></a>00701     <span class="keyword">def </span>__init__(self): 
<a name="l00702"></a>00702         this = _seldon.new_ColSymPackedCollection()
<a name="l00703"></a>00703         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_col_sym_packed_collection.php#a3d53bed94e8fd374822f449efce28818">this</a>.append(this)
<a name="l00704"></a>00704         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_col_sym_packed_collection.php#a3d53bed94e8fd374822f449efce28818">this</a> = this
<a name="l00705"></a>00705     __swig_destroy__ = _seldon.delete_ColSymPackedCollection
<a name="l00706"></a>00706     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00707"></a>00707 ColSymPackedCollection_swigregister = _seldon.ColSymPackedCollection_swigregister
<a name="l00708"></a>00708 ColSymPackedCollection_swigregister(ColSymPackedCollection)
<a name="l00709"></a>00709 
<a name="l00710"></a><a class="code" href="classseldon_1_1_row_sym_packed_collection.php">00710</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_row_sym_packed_collection.php">RowSymPackedCollection</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00711"></a>00711     __swig_setmethods__ = {}
<a name="l00712"></a>00712     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, RowSymPackedCollection, name, value)
<a name="l00713"></a>00713     __swig_getmethods__ = {}
<a name="l00714"></a>00714     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, RowSymPackedCollection, name)
<a name="l00715"></a>00715     __repr__ = _swig_repr
<a name="l00716"></a>00716     <span class="keyword">def </span>__init__(self): 
<a name="l00717"></a>00717         this = _seldon.new_RowSymPackedCollection()
<a name="l00718"></a>00718         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_row_sym_packed_collection.php#af9b61830f0bdfa3b34df17c0c5a2cf48">this</a>.append(this)
<a name="l00719"></a>00719         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_row_sym_packed_collection.php#af9b61830f0bdfa3b34df17c0c5a2cf48">this</a> = this
<a name="l00720"></a>00720     __swig_destroy__ = _seldon.delete_RowSymPackedCollection
<a name="l00721"></a>00721     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00722"></a>00722 RowSymPackedCollection_swigregister = _seldon.RowSymPackedCollection_swigregister
<a name="l00723"></a>00723 RowSymPackedCollection_swigregister(RowSymPackedCollection)
<a name="l00724"></a>00724 
<a name="l00725"></a><a class="code" href="classseldon_1_1_col_up_triang_packed_collection.php">00725</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_col_up_triang_packed_collection.php">ColUpTriangPackedCollection</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00726"></a>00726     __swig_setmethods__ = {}
<a name="l00727"></a>00727     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, ColUpTriangPackedCollection, name, value)
<a name="l00728"></a>00728     __swig_getmethods__ = {}
<a name="l00729"></a>00729     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, ColUpTriangPackedCollection, name)
<a name="l00730"></a>00730     __repr__ = _swig_repr
<a name="l00731"></a>00731     <span class="keyword">def </span>__init__(self): 
<a name="l00732"></a>00732         this = _seldon.new_ColUpTriangPackedCollection()
<a name="l00733"></a>00733         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_col_up_triang_packed_collection.php#a3e306d99402dd445029bba1722a54c4d">this</a>.append(this)
<a name="l00734"></a>00734         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_col_up_triang_packed_collection.php#a3e306d99402dd445029bba1722a54c4d">this</a> = this
<a name="l00735"></a>00735     __swig_destroy__ = _seldon.delete_ColUpTriangPackedCollection
<a name="l00736"></a>00736     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00737"></a>00737 ColUpTriangPackedCollection_swigregister = _seldon.ColUpTriangPackedCollection_swigregister
<a name="l00738"></a>00738 ColUpTriangPackedCollection_swigregister(ColUpTriangPackedCollection)
<a name="l00739"></a>00739 
<a name="l00740"></a><a class="code" href="classseldon_1_1_row_up_triang_packed_collection.php">00740</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_row_up_triang_packed_collection.php">RowUpTriangPackedCollection</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00741"></a>00741     __swig_setmethods__ = {}
<a name="l00742"></a>00742     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, RowUpTriangPackedCollection, name, value)
<a name="l00743"></a>00743     __swig_getmethods__ = {}
<a name="l00744"></a>00744     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, RowUpTriangPackedCollection, name)
<a name="l00745"></a>00745     __repr__ = _swig_repr
<a name="l00746"></a>00746     <span class="keyword">def </span>__init__(self): 
<a name="l00747"></a>00747         this = _seldon.new_RowUpTriangPackedCollection()
<a name="l00748"></a>00748         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_row_up_triang_packed_collection.php#a41b2875d0b822bb5cbb03ed15c519324">this</a>.append(this)
<a name="l00749"></a>00749         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_row_up_triang_packed_collection.php#a41b2875d0b822bb5cbb03ed15c519324">this</a> = this
<a name="l00750"></a>00750     __swig_destroy__ = _seldon.delete_RowUpTriangPackedCollection
<a name="l00751"></a>00751     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00752"></a>00752 RowUpTriangPackedCollection_swigregister = _seldon.RowUpTriangPackedCollection_swigregister
<a name="l00753"></a>00753 RowUpTriangPackedCollection_swigregister(RowUpTriangPackedCollection)
<a name="l00754"></a>00754 
<a name="l00755"></a><a class="code" href="classseldon_1_1_general.php">00755</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_general.php">General</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00756"></a>00756     __swig_setmethods__ = {}
<a name="l00757"></a>00757     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, General, name, value)
<a name="l00758"></a>00758     __swig_getmethods__ = {}
<a name="l00759"></a>00759     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, General, name)
<a name="l00760"></a>00760     __repr__ = _swig_repr
<a name="l00761"></a>00761     <span class="keyword">def </span>__init__(self): 
<a name="l00762"></a>00762         this = _seldon.new_General()
<a name="l00763"></a>00763         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_general.php#afcff0e6a66e371b51e5795f8daac9a88">this</a>.append(this)
<a name="l00764"></a>00764         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_general.php#afcff0e6a66e371b51e5795f8daac9a88">this</a> = this
<a name="l00765"></a>00765     __swig_destroy__ = _seldon.delete_General
<a name="l00766"></a>00766     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00767"></a>00767 General_swigregister = _seldon.General_swigregister
<a name="l00768"></a>00768 General_swigregister(General)
<a name="l00769"></a>00769 
<a name="l00770"></a><a class="code" href="classseldon_1_1_symmetric.php">00770</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_symmetric.php">Symmetric</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00771"></a>00771     __swig_setmethods__ = {}
<a name="l00772"></a>00772     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, Symmetric, name, value)
<a name="l00773"></a>00773     __swig_getmethods__ = {}
<a name="l00774"></a>00774     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, Symmetric, name)
<a name="l00775"></a>00775     __repr__ = _swig_repr
<a name="l00776"></a>00776     <span class="keyword">def </span>__init__(self): 
<a name="l00777"></a>00777         this = _seldon.new_Symmetric()
<a name="l00778"></a>00778         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_symmetric.php#aa69562a433dfcb3231a20b2b011f26a2">this</a>.append(this)
<a name="l00779"></a>00779         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_symmetric.php#aa69562a433dfcb3231a20b2b011f26a2">this</a> = this
<a name="l00780"></a>00780     __swig_destroy__ = _seldon.delete_Symmetric
<a name="l00781"></a>00781     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00782"></a>00782 Symmetric_swigregister = _seldon.Symmetric_swigregister
<a name="l00783"></a>00783 Symmetric_swigregister(Symmetric)
<a name="l00784"></a>00784 
<a name="l00785"></a><a class="code" href="classseldon_1_1_int_malloc.php">00785</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_int_malloc.php">IntMalloc</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00786"></a>00786     __swig_setmethods__ = {}
<a name="l00787"></a>00787     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, IntMalloc, name, value)
<a name="l00788"></a>00788     __swig_getmethods__ = {}
<a name="l00789"></a>00789     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, IntMalloc, name)
<a name="l00790"></a>00790     __repr__ = _swig_repr
<a name="l00791"></a>00791     <span class="keyword">def </span>allocate(self, *args): <span class="keywordflow">return</span> _seldon.IntMalloc_allocate(self, *args)
<a name="l00792"></a>00792     <span class="keyword">def </span>deallocate(self, *args): <span class="keywordflow">return</span> _seldon.IntMalloc_deallocate(self, *args)
<a name="l00793"></a>00793     <span class="keyword">def </span>reallocate(self, *args): <span class="keywordflow">return</span> _seldon.IntMalloc_reallocate(self, *args)
<a name="l00794"></a>00794     <span class="keyword">def </span>memoryset(self, *args): <span class="keywordflow">return</span> _seldon.IntMalloc_memoryset(self, *args)
<a name="l00795"></a>00795     <span class="keyword">def </span>memorycpy(self, *args): <span class="keywordflow">return</span> _seldon.IntMalloc_memorycpy(self, *args)
<a name="l00796"></a>00796     <span class="keyword">def </span>__init__(self): 
<a name="l00797"></a>00797         this = _seldon.new_IntMalloc()
<a name="l00798"></a>00798         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_int_malloc.php#a1b020dd1edd8c661a18715b22590ec26">this</a>.append(this)
<a name="l00799"></a>00799         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_int_malloc.php#a1b020dd1edd8c661a18715b22590ec26">this</a> = this
<a name="l00800"></a>00800     __swig_destroy__ = _seldon.delete_IntMalloc
<a name="l00801"></a>00801     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00802"></a>00802 IntMalloc_swigregister = _seldon.IntMalloc_swigregister
<a name="l00803"></a>00803 IntMalloc_swigregister(IntMalloc)
<a name="l00804"></a>00804 
<a name="l00805"></a><a class="code" href="classseldon_1_1_base_seldon_vector_int.php">00805</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_base_seldon_vector_int.php">BaseSeldonVectorInt</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00806"></a>00806     __swig_setmethods__ = {}
<a name="l00807"></a>00807     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, BaseSeldonVectorInt, name, value)
<a name="l00808"></a>00808     __swig_getmethods__ = {}
<a name="l00809"></a>00809     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, BaseSeldonVectorInt, name)
<a name="l00810"></a>00810     __repr__ = _swig_repr
<a name="l00811"></a>00811     <span class="keyword">def </span>__init__(self, *args): 
<a name="l00812"></a>00812         this = _seldon.new_BaseSeldonVectorInt(*args)
<a name="l00813"></a>00813         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_base_seldon_vector_int.php#a223e6d50223213f219bbb86fd36b8fd2">this</a>.append(this)
<a name="l00814"></a>00814         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_base_seldon_vector_int.php#a223e6d50223213f219bbb86fd36b8fd2">this</a> = this
<a name="l00815"></a>00815     __swig_destroy__ = _seldon.delete_BaseSeldonVectorInt
<a name="l00816"></a>00816     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00817"></a>00817     <span class="keyword">def </span>GetM(self): <span class="keywordflow">return</span> _seldon.BaseSeldonVectorInt_GetM(self)
<a name="l00818"></a>00818     <span class="keyword">def </span>GetLength(self): <span class="keywordflow">return</span> _seldon.BaseSeldonVectorInt_GetLength(self)
<a name="l00819"></a>00819     <span class="keyword">def </span>GetSize(self): <span class="keywordflow">return</span> _seldon.BaseSeldonVectorInt_GetSize(self)
<a name="l00820"></a>00820     <span class="keyword">def </span>GetData(self): <span class="keywordflow">return</span> _seldon.BaseSeldonVectorInt_GetData(self)
<a name="l00821"></a>00821     <span class="keyword">def </span>GetDataConst(self): <span class="keywordflow">return</span> _seldon.BaseSeldonVectorInt_GetDataConst(self)
<a name="l00822"></a>00822     <span class="keyword">def </span>GetDataVoid(self): <span class="keywordflow">return</span> _seldon.BaseSeldonVectorInt_GetDataVoid(self)
<a name="l00823"></a>00823     <span class="keyword">def </span>GetDataConstVoid(self): <span class="keywordflow">return</span> _seldon.BaseSeldonVectorInt_GetDataConstVoid(self)
<a name="l00824"></a>00824 BaseSeldonVectorInt_swigregister = _seldon.BaseSeldonVectorInt_swigregister
<a name="l00825"></a>00825 BaseSeldonVectorInt_swigregister(BaseSeldonVectorInt)
<a name="l00826"></a>00826 
<a name="l00827"></a><a class="code" href="classseldon_1_1_vector_int.php">00827</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_vector_int.php">VectorInt</a>(<a class="code" href="classseldon_1_1_base_seldon_vector_int.php">BaseSeldonVectorInt</a>):
<a name="l00828"></a>00828     __swig_setmethods__ = {}
<a name="l00829"></a>00829     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [BaseSeldonVectorInt]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l00830"></a>00830     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, VectorInt, name, value)
<a name="l00831"></a>00831     __swig_getmethods__ = {}
<a name="l00832"></a>00832     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [BaseSeldonVectorInt]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l00833"></a>00833     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, VectorInt, name)
<a name="l00834"></a>00834     __repr__ = _swig_repr
<a name="l00835"></a>00835     <span class="keyword">def </span>__init__(self, *args): 
<a name="l00836"></a>00836         this = _seldon.new_VectorInt(*args)
<a name="l00837"></a>00837         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_vector_int.php#a5dba37ce2201c0caa5181cb09981212d">this</a>.append(this)
<a name="l00838"></a>00838         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_vector_int.php#a5dba37ce2201c0caa5181cb09981212d">this</a> = this
<a name="l00839"></a>00839     __swig_destroy__ = _seldon.delete_VectorInt
<a name="l00840"></a>00840     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00841"></a>00841     <span class="keyword">def </span>Clear(self): <span class="keywordflow">return</span> _seldon.VectorInt_Clear(self)
<a name="l00842"></a>00842     <span class="keyword">def </span>Reallocate(self, *args): <span class="keywordflow">return</span> _seldon.VectorInt_Reallocate(self, *args)
<a name="l00843"></a>00843     <span class="keyword">def </span>Resize(self, *args): <span class="keywordflow">return</span> _seldon.VectorInt_Resize(self, *args)
<a name="l00844"></a>00844     <span class="keyword">def </span>SetData(self, *args): <span class="keywordflow">return</span> _seldon.VectorInt_SetData(self, *args)
<a name="l00845"></a>00845     <span class="keyword">def </span>Nullify(self): <span class="keywordflow">return</span> _seldon.VectorInt_Nullify(self)
<a name="l00846"></a>00846     <span class="keyword">def </span>__call__(self, *args): <span class="keywordflow">return</span> _seldon.VectorInt___call__(self, *args)
<a name="l00847"></a>00847     <span class="keyword">def </span>Get(self, *args): <span class="keywordflow">return</span> _seldon.VectorInt_Get(self, *args)
<a name="l00848"></a>00848     <span class="keyword">def </span>Copy(self, *args): <span class="keywordflow">return</span> _seldon.VectorInt_Copy(self, *args)
<a name="l00849"></a>00849     <span class="keyword">def </span>Append(self, *args): <span class="keywordflow">return</span> _seldon.VectorInt_Append(self, *args)
<a name="l00850"></a>00850     <span class="keyword">def </span>GetDataSize(self): <span class="keywordflow">return</span> _seldon.VectorInt_GetDataSize(self)
<a name="l00851"></a>00851     <span class="keyword">def </span>Zero(self): <span class="keywordflow">return</span> _seldon.VectorInt_Zero(self)
<a name="l00852"></a>00852     <span class="keyword">def </span>Fill(self): <span class="keywordflow">return</span> _seldon.VectorInt_Fill(self)
<a name="l00853"></a>00853     <span class="keyword">def </span>FillRand(self): <span class="keywordflow">return</span> _seldon.VectorInt_FillRand(self)
<a name="l00854"></a>00854     <span class="keyword">def </span>Print(self): <span class="keywordflow">return</span> _seldon.VectorInt_Print(self)
<a name="l00855"></a>00855     <span class="keyword">def </span>GetNormInf(self): <span class="keywordflow">return</span> _seldon.VectorInt_GetNormInf(self)
<a name="l00856"></a>00856     <span class="keyword">def </span>GetNormInfIndex(self): <span class="keywordflow">return</span> _seldon.VectorInt_GetNormInfIndex(self)
<a name="l00857"></a>00857     <span class="keyword">def </span>Write(self, *args): <span class="keywordflow">return</span> _seldon.VectorInt_Write(self, *args)
<a name="l00858"></a>00858     <span class="keyword">def </span>WriteText(self, *args): <span class="keywordflow">return</span> _seldon.VectorInt_WriteText(self, *args)
<a name="l00859"></a>00859     <span class="keyword">def </span>Read(self, *args): <span class="keywordflow">return</span> _seldon.VectorInt_Read(self, *args)
<a name="l00860"></a>00860     <span class="keyword">def </span>ReadText(self, *args): <span class="keywordflow">return</span> _seldon.VectorInt_ReadText(self, *args)
<a name="l00861"></a>00861     <span class="keyword">def </span>__getitem__(self, *args): <span class="keywordflow">return</span> _seldon.VectorInt___getitem__(self, *args)
<a name="l00862"></a>00862     <span class="keyword">def </span>__setitem__(self, *args): <span class="keywordflow">return</span> _seldon.VectorInt___setitem__(self, *args)
<a name="l00863"></a>00863     <span class="keyword">def </span>__len__(self): <span class="keywordflow">return</span> _seldon.VectorInt___len__(self)
<a name="l00864"></a>00864 VectorInt_swigregister = _seldon.VectorInt_swigregister
<a name="l00865"></a>00865 VectorInt_swigregister(VectorInt)
<a name="l00866"></a>00866 
<a name="l00867"></a><a class="code" href="classseldon_1_1_double_malloc.php">00867</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_double_malloc.php">DoubleMalloc</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00868"></a>00868     __swig_setmethods__ = {}
<a name="l00869"></a>00869     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, DoubleMalloc, name, value)
<a name="l00870"></a>00870     __swig_getmethods__ = {}
<a name="l00871"></a>00871     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, DoubleMalloc, name)
<a name="l00872"></a>00872     __repr__ = _swig_repr
<a name="l00873"></a>00873     <span class="keyword">def </span>allocate(self, *args): <span class="keywordflow">return</span> _seldon.DoubleMalloc_allocate(self, *args)
<a name="l00874"></a>00874     <span class="keyword">def </span>deallocate(self, *args): <span class="keywordflow">return</span> _seldon.DoubleMalloc_deallocate(self, *args)
<a name="l00875"></a>00875     <span class="keyword">def </span>reallocate(self, *args): <span class="keywordflow">return</span> _seldon.DoubleMalloc_reallocate(self, *args)
<a name="l00876"></a>00876     <span class="keyword">def </span>memoryset(self, *args): <span class="keywordflow">return</span> _seldon.DoubleMalloc_memoryset(self, *args)
<a name="l00877"></a>00877     <span class="keyword">def </span>memorycpy(self, *args): <span class="keywordflow">return</span> _seldon.DoubleMalloc_memorycpy(self, *args)
<a name="l00878"></a>00878     <span class="keyword">def </span>__init__(self): 
<a name="l00879"></a>00879         this = _seldon.new_DoubleMalloc()
<a name="l00880"></a>00880         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_double_malloc.php#a7474e243c4e74fa3af5a73e225ba973d">this</a>.append(this)
<a name="l00881"></a>00881         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_double_malloc.php#a7474e243c4e74fa3af5a73e225ba973d">this</a> = this
<a name="l00882"></a>00882     __swig_destroy__ = _seldon.delete_DoubleMalloc
<a name="l00883"></a>00883     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00884"></a>00884 DoubleMalloc_swigregister = _seldon.DoubleMalloc_swigregister
<a name="l00885"></a>00885 DoubleMalloc_swigregister(DoubleMalloc)
<a name="l00886"></a>00886 
<a name="l00887"></a><a class="code" href="classseldon_1_1_base_seldon_vector_double.php">00887</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_base_seldon_vector_double.php">BaseSeldonVectorDouble</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00888"></a>00888     __swig_setmethods__ = {}
<a name="l00889"></a>00889     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, BaseSeldonVectorDouble, name, value)
<a name="l00890"></a>00890     __swig_getmethods__ = {}
<a name="l00891"></a>00891     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, BaseSeldonVectorDouble, name)
<a name="l00892"></a>00892     __repr__ = _swig_repr
<a name="l00893"></a>00893     <span class="keyword">def </span>__init__(self, *args): 
<a name="l00894"></a>00894         this = _seldon.new_BaseSeldonVectorDouble(*args)
<a name="l00895"></a>00895         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_base_seldon_vector_double.php#ae5ae0acbfd282c4924f27591596d25da">this</a>.append(this)
<a name="l00896"></a>00896         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_base_seldon_vector_double.php#ae5ae0acbfd282c4924f27591596d25da">this</a> = this
<a name="l00897"></a>00897     __swig_destroy__ = _seldon.delete_BaseSeldonVectorDouble
<a name="l00898"></a>00898     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00899"></a>00899     <span class="keyword">def </span>GetM(self): <span class="keywordflow">return</span> _seldon.BaseSeldonVectorDouble_GetM(self)
<a name="l00900"></a>00900     <span class="keyword">def </span>GetLength(self): <span class="keywordflow">return</span> _seldon.BaseSeldonVectorDouble_GetLength(self)
<a name="l00901"></a>00901     <span class="keyword">def </span>GetSize(self): <span class="keywordflow">return</span> _seldon.BaseSeldonVectorDouble_GetSize(self)
<a name="l00902"></a>00902     <span class="keyword">def </span>GetData(self): <span class="keywordflow">return</span> _seldon.BaseSeldonVectorDouble_GetData(self)
<a name="l00903"></a>00903     <span class="keyword">def </span>GetDataConst(self): <span class="keywordflow">return</span> _seldon.BaseSeldonVectorDouble_GetDataConst(self)
<a name="l00904"></a>00904     <span class="keyword">def </span>GetDataVoid(self): <span class="keywordflow">return</span> _seldon.BaseSeldonVectorDouble_GetDataVoid(self)
<a name="l00905"></a>00905     <span class="keyword">def </span>GetDataConstVoid(self): <span class="keywordflow">return</span> _seldon.BaseSeldonVectorDouble_GetDataConstVoid(self)
<a name="l00906"></a>00906 BaseSeldonVectorDouble_swigregister = _seldon.BaseSeldonVectorDouble_swigregister
<a name="l00907"></a>00907 BaseSeldonVectorDouble_swigregister(BaseSeldonVectorDouble)
<a name="l00908"></a>00908 
<a name="l00909"></a><a class="code" href="classseldon_1_1_vector_double.php">00909</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_vector_double.php">VectorDouble</a>(<a class="code" href="classseldon_1_1_base_seldon_vector_double.php">BaseSeldonVectorDouble</a>):
<a name="l00910"></a>00910     __swig_setmethods__ = {}
<a name="l00911"></a>00911     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [BaseSeldonVectorDouble]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l00912"></a>00912     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, VectorDouble, name, value)
<a name="l00913"></a>00913     __swig_getmethods__ = {}
<a name="l00914"></a>00914     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [BaseSeldonVectorDouble]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l00915"></a>00915     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, VectorDouble, name)
<a name="l00916"></a>00916     __repr__ = _swig_repr
<a name="l00917"></a>00917     <span class="keyword">def </span>__init__(self, *args): 
<a name="l00918"></a>00918         this = _seldon.new_VectorDouble(*args)
<a name="l00919"></a>00919         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_vector_double.php#a15472a917b372a164f16c4931b44afef">this</a>.append(this)
<a name="l00920"></a>00920         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_vector_double.php#a15472a917b372a164f16c4931b44afef">this</a> = this
<a name="l00921"></a>00921     __swig_destroy__ = _seldon.delete_VectorDouble
<a name="l00922"></a>00922     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00923"></a>00923     <span class="keyword">def </span>Clear(self): <span class="keywordflow">return</span> _seldon.VectorDouble_Clear(self)
<a name="l00924"></a>00924     <span class="keyword">def </span>Reallocate(self, *args): <span class="keywordflow">return</span> _seldon.VectorDouble_Reallocate(self, *args)
<a name="l00925"></a>00925     <span class="keyword">def </span>Resize(self, *args): <span class="keywordflow">return</span> _seldon.VectorDouble_Resize(self, *args)
<a name="l00926"></a>00926     <span class="keyword">def </span>SetData(self, *args): <span class="keywordflow">return</span> _seldon.VectorDouble_SetData(self, *args)
<a name="l00927"></a>00927     <span class="keyword">def </span>Nullify(self): <span class="keywordflow">return</span> _seldon.VectorDouble_Nullify(self)
<a name="l00928"></a>00928     <span class="keyword">def </span>__call__(self, *args): <span class="keywordflow">return</span> _seldon.VectorDouble___call__(self, *args)
<a name="l00929"></a>00929     <span class="keyword">def </span>Get(self, *args): <span class="keywordflow">return</span> _seldon.VectorDouble_Get(self, *args)
<a name="l00930"></a>00930     <span class="keyword">def </span>Copy(self, *args): <span class="keywordflow">return</span> _seldon.VectorDouble_Copy(self, *args)
<a name="l00931"></a>00931     <span class="keyword">def </span>Append(self, *args): <span class="keywordflow">return</span> _seldon.VectorDouble_Append(self, *args)
<a name="l00932"></a>00932     <span class="keyword">def </span>GetDataSize(self): <span class="keywordflow">return</span> _seldon.VectorDouble_GetDataSize(self)
<a name="l00933"></a>00933     <span class="keyword">def </span>Zero(self): <span class="keywordflow">return</span> _seldon.VectorDouble_Zero(self)
<a name="l00934"></a>00934     <span class="keyword">def </span>Fill(self): <span class="keywordflow">return</span> _seldon.VectorDouble_Fill(self)
<a name="l00935"></a>00935     <span class="keyword">def </span>FillRand(self): <span class="keywordflow">return</span> _seldon.VectorDouble_FillRand(self)
<a name="l00936"></a>00936     <span class="keyword">def </span>Print(self): <span class="keywordflow">return</span> _seldon.VectorDouble_Print(self)
<a name="l00937"></a>00937     <span class="keyword">def </span>GetNormInf(self): <span class="keywordflow">return</span> _seldon.VectorDouble_GetNormInf(self)
<a name="l00938"></a>00938     <span class="keyword">def </span>GetNormInfIndex(self): <span class="keywordflow">return</span> _seldon.VectorDouble_GetNormInfIndex(self)
<a name="l00939"></a>00939     <span class="keyword">def </span>Write(self, *args): <span class="keywordflow">return</span> _seldon.VectorDouble_Write(self, *args)
<a name="l00940"></a>00940     <span class="keyword">def </span>WriteText(self, *args): <span class="keywordflow">return</span> _seldon.VectorDouble_WriteText(self, *args)
<a name="l00941"></a>00941     <span class="keyword">def </span>Read(self, *args): <span class="keywordflow">return</span> _seldon.VectorDouble_Read(self, *args)
<a name="l00942"></a>00942     <span class="keyword">def </span>ReadText(self, *args): <span class="keywordflow">return</span> _seldon.VectorDouble_ReadText(self, *args)
<a name="l00943"></a>00943     <span class="keyword">def </span>__getitem__(self, *args): <span class="keywordflow">return</span> _seldon.VectorDouble___getitem__(self, *args)
<a name="l00944"></a>00944     <span class="keyword">def </span>__setitem__(self, *args): <span class="keywordflow">return</span> _seldon.VectorDouble___setitem__(self, *args)
<a name="l00945"></a>00945     <span class="keyword">def </span>__len__(self): <span class="keywordflow">return</span> _seldon.VectorDouble___len__(self)
<a name="l00946"></a>00946 VectorDouble_swigregister = _seldon.VectorDouble_swigregister
<a name="l00947"></a>00947 VectorDouble_swigregister(VectorDouble)
<a name="l00948"></a>00948 
<a name="l00949"></a><a class="code" href="classseldon_1_1_matrix_base_int.php">00949</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_matrix_base_int.php">MatrixBaseInt</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l00950"></a>00950     __swig_setmethods__ = {}
<a name="l00951"></a>00951     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, MatrixBaseInt, name, value)
<a name="l00952"></a>00952     __swig_getmethods__ = {}
<a name="l00953"></a>00953     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, MatrixBaseInt, name)
<a name="l00954"></a>00954     __repr__ = _swig_repr
<a name="l00955"></a>00955     <span class="keyword">def </span>__init__(self, *args): 
<a name="l00956"></a>00956         this = _seldon.new_MatrixBaseInt(*args)
<a name="l00957"></a>00957         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_matrix_base_int.php#a92e3dcb95080141abde6e50f3da41667">this</a>.append(this)
<a name="l00958"></a>00958         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_matrix_base_int.php#a92e3dcb95080141abde6e50f3da41667">this</a> = this
<a name="l00959"></a>00959     __swig_destroy__ = _seldon.delete_MatrixBaseInt
<a name="l00960"></a>00960     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00961"></a>00961     <span class="keyword">def </span>GetM(self, *args): <span class="keywordflow">return</span> _seldon.MatrixBaseInt_GetM(self, *args)
<a name="l00962"></a>00962     <span class="keyword">def </span>GetN(self, *args): <span class="keywordflow">return</span> _seldon.MatrixBaseInt_GetN(self, *args)
<a name="l00963"></a>00963     <span class="keyword">def </span>GetSize(self): <span class="keywordflow">return</span> _seldon.MatrixBaseInt_GetSize(self)
<a name="l00964"></a>00964     <span class="keyword">def </span>GetData(self): <span class="keywordflow">return</span> _seldon.MatrixBaseInt_GetData(self)
<a name="l00965"></a>00965     <span class="keyword">def </span>GetDataConst(self): <span class="keywordflow">return</span> _seldon.MatrixBaseInt_GetDataConst(self)
<a name="l00966"></a>00966     <span class="keyword">def </span>GetDataVoid(self): <span class="keywordflow">return</span> _seldon.MatrixBaseInt_GetDataVoid(self)
<a name="l00967"></a>00967     <span class="keyword">def </span>GetDataConstVoid(self): <span class="keywordflow">return</span> _seldon.MatrixBaseInt_GetDataConstVoid(self)
<a name="l00968"></a>00968     <span class="keyword">def </span>GetAllocator(self): <span class="keywordflow">return</span> _seldon.MatrixBaseInt_GetAllocator(self)
<a name="l00969"></a>00969 MatrixBaseInt_swigregister = _seldon.MatrixBaseInt_swigregister
<a name="l00970"></a>00970 MatrixBaseInt_swigregister(MatrixBaseInt)
<a name="l00971"></a>00971 
<a name="l00972"></a><a class="code" href="classseldon_1_1_matrix_pointers_int.php">00972</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_matrix_pointers_int.php">MatrixPointersInt</a>(<a class="code" href="classseldon_1_1_matrix_base_int.php">MatrixBaseInt</a>):
<a name="l00973"></a>00973     __swig_setmethods__ = {}
<a name="l00974"></a>00974     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [MatrixBaseInt]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l00975"></a>00975     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, MatrixPointersInt, name, value)
<a name="l00976"></a>00976     __swig_getmethods__ = {}
<a name="l00977"></a>00977     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [MatrixBaseInt]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l00978"></a>00978     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, MatrixPointersInt, name)
<a name="l00979"></a>00979     __repr__ = _swig_repr
<a name="l00980"></a>00980     <span class="keyword">def </span>__init__(self, *args): 
<a name="l00981"></a>00981         this = _seldon.new_MatrixPointersInt(*args)
<a name="l00982"></a>00982         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_matrix_pointers_int.php#adba0202a7843453becf9633ec4d95541">this</a>.append(this)
<a name="l00983"></a>00983         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_matrix_pointers_int.php#adba0202a7843453becf9633ec4d95541">this</a> = this
<a name="l00984"></a>00984     __swig_destroy__ = _seldon.delete_MatrixPointersInt
<a name="l00985"></a>00985     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l00986"></a>00986     <span class="keyword">def </span>Clear(self): <span class="keywordflow">return</span> _seldon.MatrixPointersInt_Clear(self)
<a name="l00987"></a>00987     <span class="keyword">def </span>GetDataSize(self): <span class="keywordflow">return</span> _seldon.MatrixPointersInt_GetDataSize(self)
<a name="l00988"></a>00988     <span class="keyword">def </span>GetMe(self): <span class="keywordflow">return</span> _seldon.MatrixPointersInt_GetMe(self)
<a name="l00989"></a>00989     <span class="keyword">def </span>Reallocate(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersInt_Reallocate(self, *args)
<a name="l00990"></a>00990     <span class="keyword">def </span>SetData(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersInt_SetData(self, *args)
<a name="l00991"></a>00991     <span class="keyword">def </span>Nullify(self): <span class="keywordflow">return</span> _seldon.MatrixPointersInt_Nullify(self)
<a name="l00992"></a>00992     <span class="keyword">def </span>Resize(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersInt_Resize(self, *args)
<a name="l00993"></a>00993     <span class="keyword">def </span>__call__(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersInt___call__(self, *args)
<a name="l00994"></a>00994     <span class="keyword">def </span>Val(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersInt_Val(self, *args)
<a name="l00995"></a>00995     <span class="keyword">def </span>Get(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersInt_Get(self, *args)
<a name="l00996"></a>00996     <span class="keyword">def </span>Set(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersInt_Set(self, *args)
<a name="l00997"></a>00997     <span class="keyword">def </span>Copy(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersInt_Copy(self, *args)
<a name="l00998"></a>00998     <span class="keyword">def </span>GetLD(self): <span class="keywordflow">return</span> _seldon.MatrixPointersInt_GetLD(self)
<a name="l00999"></a>00999     <span class="keyword">def </span>Zero(self): <span class="keywordflow">return</span> _seldon.MatrixPointersInt_Zero(self)
<a name="l01000"></a>01000     <span class="keyword">def </span>SetIdentity(self): <span class="keywordflow">return</span> _seldon.MatrixPointersInt_SetIdentity(self)
<a name="l01001"></a>01001     <span class="keyword">def </span>Fill(self): <span class="keywordflow">return</span> _seldon.MatrixPointersInt_Fill(self)
<a name="l01002"></a>01002     <span class="keyword">def </span>FillRand(self): <span class="keywordflow">return</span> _seldon.MatrixPointersInt_FillRand(self)
<a name="l01003"></a>01003     <span class="keyword">def </span>Print(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersInt_Print(self, *args)
<a name="l01004"></a>01004     <span class="keyword">def </span>Write(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersInt_Write(self, *args)
<a name="l01005"></a>01005     <span class="keyword">def </span>WriteText(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersInt_WriteText(self, *args)
<a name="l01006"></a>01006     <span class="keyword">def </span>Read(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersInt_Read(self, *args)
<a name="l01007"></a>01007     <span class="keyword">def </span>ReadText(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersInt_ReadText(self, *args)
<a name="l01008"></a>01008 MatrixPointersInt_swigregister = _seldon.MatrixPointersInt_swigregister
<a name="l01009"></a>01009 MatrixPointersInt_swigregister(MatrixPointersInt)
<a name="l01010"></a>01010 
<a name="l01011"></a><a class="code" href="classseldon_1_1_matrix_int.php">01011</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_matrix_int.php">MatrixInt</a>(<a class="code" href="classseldon_1_1_matrix_pointers_int.php">MatrixPointersInt</a>):
<a name="l01012"></a>01012     __swig_setmethods__ = {}
<a name="l01013"></a>01013     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [MatrixPointersInt]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l01014"></a>01014     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, MatrixInt, name, value)
<a name="l01015"></a>01015     __swig_getmethods__ = {}
<a name="l01016"></a>01016     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [MatrixPointersInt]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l01017"></a>01017     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, MatrixInt, name)
<a name="l01018"></a>01018     __repr__ = _swig_repr
<a name="l01019"></a>01019     <span class="keyword">def </span>__init__(self, *args): 
<a name="l01020"></a>01020         this = _seldon.new_MatrixInt(*args)
<a name="l01021"></a>01021         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_matrix_int.php#a95e2d6726771ab2c9309776b97b058b8">this</a>.append(this)
<a name="l01022"></a>01022         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_matrix_int.php#a95e2d6726771ab2c9309776b97b058b8">this</a> = this
<a name="l01023"></a>01023     <span class="keyword">def </span>__getitem__(self, *args): <span class="keywordflow">return</span> _seldon.MatrixInt___getitem__(self, *args)
<a name="l01024"></a>01024     <span class="keyword">def </span>__setitem__(self, *args): <span class="keywordflow">return</span> _seldon.MatrixInt___setitem__(self, *args)
<a name="l01025"></a>01025     <span class="keyword">def </span>__len__(self): <span class="keywordflow">return</span> _seldon.MatrixInt___len__(self)
<a name="l01026"></a>01026     __swig_destroy__ = _seldon.delete_MatrixInt
<a name="l01027"></a>01027     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l01028"></a>01028 MatrixInt_swigregister = _seldon.MatrixInt_swigregister
<a name="l01029"></a>01029 MatrixInt_swigregister(MatrixInt)
<a name="l01030"></a>01030 
<a name="l01031"></a><a class="code" href="classseldon_1_1_matrix_base_double.php">01031</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_matrix_base_double.php">MatrixBaseDouble</a>(<a class="code" href="classseldon_1_1__object.php">_object</a>):
<a name="l01032"></a>01032     __swig_setmethods__ = {}
<a name="l01033"></a>01033     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, MatrixBaseDouble, name, value)
<a name="l01034"></a>01034     __swig_getmethods__ = {}
<a name="l01035"></a>01035     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, MatrixBaseDouble, name)
<a name="l01036"></a>01036     __repr__ = _swig_repr
<a name="l01037"></a>01037     <span class="keyword">def </span>__init__(self, *args): 
<a name="l01038"></a>01038         this = _seldon.new_MatrixBaseDouble(*args)
<a name="l01039"></a>01039         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_matrix_base_double.php#a2ce11ac1904e65a88d8ed8ac326a8b1a">this</a>.append(this)
<a name="l01040"></a>01040         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_matrix_base_double.php#a2ce11ac1904e65a88d8ed8ac326a8b1a">this</a> = this
<a name="l01041"></a>01041     __swig_destroy__ = _seldon.delete_MatrixBaseDouble
<a name="l01042"></a>01042     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l01043"></a>01043     <span class="keyword">def </span>GetM(self, *args): <span class="keywordflow">return</span> _seldon.MatrixBaseDouble_GetM(self, *args)
<a name="l01044"></a>01044     <span class="keyword">def </span>GetN(self, *args): <span class="keywordflow">return</span> _seldon.MatrixBaseDouble_GetN(self, *args)
<a name="l01045"></a>01045     <span class="keyword">def </span>GetSize(self): <span class="keywordflow">return</span> _seldon.MatrixBaseDouble_GetSize(self)
<a name="l01046"></a>01046     <span class="keyword">def </span>GetData(self): <span class="keywordflow">return</span> _seldon.MatrixBaseDouble_GetData(self)
<a name="l01047"></a>01047     <span class="keyword">def </span>GetDataConst(self): <span class="keywordflow">return</span> _seldon.MatrixBaseDouble_GetDataConst(self)
<a name="l01048"></a>01048     <span class="keyword">def </span>GetDataVoid(self): <span class="keywordflow">return</span> _seldon.MatrixBaseDouble_GetDataVoid(self)
<a name="l01049"></a>01049     <span class="keyword">def </span>GetDataConstVoid(self): <span class="keywordflow">return</span> _seldon.MatrixBaseDouble_GetDataConstVoid(self)
<a name="l01050"></a>01050     <span class="keyword">def </span>GetAllocator(self): <span class="keywordflow">return</span> _seldon.MatrixBaseDouble_GetAllocator(self)
<a name="l01051"></a>01051 MatrixBaseDouble_swigregister = _seldon.MatrixBaseDouble_swigregister
<a name="l01052"></a>01052 MatrixBaseDouble_swigregister(MatrixBaseDouble)
<a name="l01053"></a>01053 
<a name="l01054"></a><a class="code" href="classseldon_1_1_matrix_pointers_double.php">01054</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_matrix_pointers_double.php">MatrixPointersDouble</a>(<a class="code" href="classseldon_1_1_matrix_base_double.php">MatrixBaseDouble</a>):
<a name="l01055"></a>01055     __swig_setmethods__ = {}
<a name="l01056"></a>01056     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [MatrixBaseDouble]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l01057"></a>01057     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, MatrixPointersDouble, name, value)
<a name="l01058"></a>01058     __swig_getmethods__ = {}
<a name="l01059"></a>01059     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [MatrixBaseDouble]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l01060"></a>01060     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, MatrixPointersDouble, name)
<a name="l01061"></a>01061     __repr__ = _swig_repr
<a name="l01062"></a>01062     <span class="keyword">def </span>__init__(self, *args): 
<a name="l01063"></a>01063         this = _seldon.new_MatrixPointersDouble(*args)
<a name="l01064"></a>01064         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_matrix_pointers_double.php#a7e03f2e9e0d41a0ccc0e26349d748bc2">this</a>.append(this)
<a name="l01065"></a>01065         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_matrix_pointers_double.php#a7e03f2e9e0d41a0ccc0e26349d748bc2">this</a> = this
<a name="l01066"></a>01066     __swig_destroy__ = _seldon.delete_MatrixPointersDouble
<a name="l01067"></a>01067     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l01068"></a>01068     <span class="keyword">def </span>Clear(self): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble_Clear(self)
<a name="l01069"></a>01069     <span class="keyword">def </span>GetDataSize(self): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble_GetDataSize(self)
<a name="l01070"></a>01070     <span class="keyword">def </span>GetMe(self): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble_GetMe(self)
<a name="l01071"></a>01071     <span class="keyword">def </span>Reallocate(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble_Reallocate(self, *args)
<a name="l01072"></a>01072     <span class="keyword">def </span>SetData(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble_SetData(self, *args)
<a name="l01073"></a>01073     <span class="keyword">def </span>Nullify(self): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble_Nullify(self)
<a name="l01074"></a>01074     <span class="keyword">def </span>Resize(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble_Resize(self, *args)
<a name="l01075"></a>01075     <span class="keyword">def </span>__call__(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble___call__(self, *args)
<a name="l01076"></a>01076     <span class="keyword">def </span>Val(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble_Val(self, *args)
<a name="l01077"></a>01077     <span class="keyword">def </span>Get(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble_Get(self, *args)
<a name="l01078"></a>01078     <span class="keyword">def </span>Set(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble_Set(self, *args)
<a name="l01079"></a>01079     <span class="keyword">def </span>Copy(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble_Copy(self, *args)
<a name="l01080"></a>01080     <span class="keyword">def </span>GetLD(self): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble_GetLD(self)
<a name="l01081"></a>01081     <span class="keyword">def </span>Zero(self): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble_Zero(self)
<a name="l01082"></a>01082     <span class="keyword">def </span>SetIdentity(self): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble_SetIdentity(self)
<a name="l01083"></a>01083     <span class="keyword">def </span>Fill(self): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble_Fill(self)
<a name="l01084"></a>01084     <span class="keyword">def </span>FillRand(self): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble_FillRand(self)
<a name="l01085"></a>01085     <span class="keyword">def </span>Print(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble_Print(self, *args)
<a name="l01086"></a>01086     <span class="keyword">def </span>Write(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble_Write(self, *args)
<a name="l01087"></a>01087     <span class="keyword">def </span>WriteText(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble_WriteText(self, *args)
<a name="l01088"></a>01088     <span class="keyword">def </span>Read(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble_Read(self, *args)
<a name="l01089"></a>01089     <span class="keyword">def </span>ReadText(self, *args): <span class="keywordflow">return</span> _seldon.MatrixPointersDouble_ReadText(self, *args)
<a name="l01090"></a>01090 MatrixPointersDouble_swigregister = _seldon.MatrixPointersDouble_swigregister
<a name="l01091"></a>01091 MatrixPointersDouble_swigregister(MatrixPointersDouble)
<a name="l01092"></a>01092 
<a name="l01093"></a><a class="code" href="classseldon_1_1_matrix_double.php">01093</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_matrix_double.php">MatrixDouble</a>(<a class="code" href="classseldon_1_1_matrix_pointers_double.php">MatrixPointersDouble</a>):
<a name="l01094"></a>01094     __swig_setmethods__ = {}
<a name="l01095"></a>01095     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [MatrixPointersDouble]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l01096"></a>01096     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, MatrixDouble, name, value)
<a name="l01097"></a>01097     __swig_getmethods__ = {}
<a name="l01098"></a>01098     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [MatrixPointersDouble]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l01099"></a>01099     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, MatrixDouble, name)
<a name="l01100"></a>01100     __repr__ = _swig_repr
<a name="l01101"></a>01101     <span class="keyword">def </span>__init__(self, *args): 
<a name="l01102"></a>01102         this = _seldon.new_MatrixDouble(*args)
<a name="l01103"></a>01103         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_matrix_double.php#a08d20fe9ca8f5f6edd74a35438107fb9">this</a>.append(this)
<a name="l01104"></a>01104         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_matrix_double.php#a08d20fe9ca8f5f6edd74a35438107fb9">this</a> = this
<a name="l01105"></a>01105     <span class="keyword">def </span>__getitem__(self, *args): <span class="keywordflow">return</span> _seldon.MatrixDouble___getitem__(self, *args)
<a name="l01106"></a>01106     <span class="keyword">def </span>__setitem__(self, *args): <span class="keywordflow">return</span> _seldon.MatrixDouble___setitem__(self, *args)
<a name="l01107"></a>01107     <span class="keyword">def </span>__len__(self): <span class="keywordflow">return</span> _seldon.MatrixDouble___len__(self)
<a name="l01108"></a>01108     __swig_destroy__ = _seldon.delete_MatrixDouble
<a name="l01109"></a>01109     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l01110"></a>01110 MatrixDouble_swigregister = _seldon.MatrixDouble_swigregister
<a name="l01111"></a>01111 MatrixDouble_swigregister(MatrixDouble)
<a name="l01112"></a>01112 
<a name="l01113"></a><a class="code" href="classseldon_1_1_vector_sparse_double.php">01113</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_vector_sparse_double.php">VectorSparseDouble</a>(<a class="code" href="classseldon_1_1_vector_double.php">VectorDouble</a>):
<a name="l01114"></a>01114     __swig_setmethods__ = {}
<a name="l01115"></a>01115     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [VectorDouble]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l01116"></a>01116     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, VectorSparseDouble, name, value)
<a name="l01117"></a>01117     __swig_getmethods__ = {}
<a name="l01118"></a>01118     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [VectorDouble]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l01119"></a>01119     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, VectorSparseDouble, name)
<a name="l01120"></a>01120     __repr__ = _swig_repr
<a name="l01121"></a>01121     <span class="keyword">def </span>__init__(self, *args): 
<a name="l01122"></a>01122         this = _seldon.new_VectorSparseDouble(*args)
<a name="l01123"></a>01123         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_vector_sparse_double.php#ab950b9fd032a10a7b737f0d157646243">this</a>.append(this)
<a name="l01124"></a>01124         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_vector_sparse_double.php#ab950b9fd032a10a7b737f0d157646243">this</a> = this
<a name="l01125"></a>01125     __swig_destroy__ = _seldon.delete_VectorSparseDouble
<a name="l01126"></a>01126     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l01127"></a>01127     <span class="keyword">def </span>Clear(self): <span class="keywordflow">return</span> _seldon.VectorSparseDouble_Clear(self)
<a name="l01128"></a>01128     <span class="keyword">def </span>Reallocate(self, *args): <span class="keywordflow">return</span> _seldon.VectorSparseDouble_Reallocate(self, *args)
<a name="l01129"></a>01129     <span class="keyword">def </span>Resize(self, *args): <span class="keywordflow">return</span> _seldon.VectorSparseDouble_Resize(self, *args)
<a name="l01130"></a>01130     <span class="keyword">def </span>SetData(self, *args): <span class="keywordflow">return</span> _seldon.VectorSparseDouble_SetData(self, *args)
<a name="l01131"></a>01131     <span class="keyword">def </span>Nullify(self): <span class="keywordflow">return</span> _seldon.VectorSparseDouble_Nullify(self)
<a name="l01132"></a>01132     <span class="keyword">def </span>Value(self, *args): <span class="keywordflow">return</span> _seldon.VectorSparseDouble_Value(self, *args)
<a name="l01133"></a>01133     <span class="keyword">def </span>Index(self, *args): <span class="keywordflow">return</span> _seldon.VectorSparseDouble_Index(self, *args)
<a name="l01134"></a>01134     <span class="keyword">def </span>__call__(self, *args): <span class="keywordflow">return</span> _seldon.VectorSparseDouble___call__(self, *args)
<a name="l01135"></a>01135     <span class="keyword">def </span>Get(self, *args): <span class="keywordflow">return</span> _seldon.VectorSparseDouble_Get(self, *args)
<a name="l01136"></a>01136     <span class="keyword">def </span>Val(self, *args): <span class="keywordflow">return</span> _seldon.VectorSparseDouble_Val(self, *args)
<a name="l01137"></a>01137     <span class="keyword">def </span>Copy(self, *args): <span class="keywordflow">return</span> _seldon.VectorSparseDouble_Copy(self, *args)
<a name="l01138"></a>01138     <span class="keyword">def </span>GetIndex(self): <span class="keywordflow">return</span> _seldon.VectorSparseDouble_GetIndex(self)
<a name="l01139"></a>01139     <span class="keyword">def </span>Assemble(self): <span class="keywordflow">return</span> _seldon.VectorSparseDouble_Assemble(self)
<a name="l01140"></a>01140     <span class="keyword">def </span>AddInteraction(self, *args): <span class="keywordflow">return</span> _seldon.VectorSparseDouble_AddInteraction(self, *args)
<a name="l01141"></a>01141     <span class="keyword">def </span>AddInteractionRow(self, *args): <span class="keywordflow">return</span> _seldon.VectorSparseDouble_AddInteractionRow(self, *args)
<a name="l01142"></a>01142     <span class="keyword">def </span>Write(self, *args): <span class="keywordflow">return</span> _seldon.VectorSparseDouble_Write(self, *args)
<a name="l01143"></a>01143     <span class="keyword">def </span>WriteText(self, *args): <span class="keywordflow">return</span> _seldon.VectorSparseDouble_WriteText(self, *args)
<a name="l01144"></a>01144     <span class="keyword">def </span>Read(self, *args): <span class="keywordflow">return</span> _seldon.VectorSparseDouble_Read(self, *args)
<a name="l01145"></a>01145     <span class="keyword">def </span>ReadText(self, *args): <span class="keywordflow">return</span> _seldon.VectorSparseDouble_ReadText(self, *args)
<a name="l01146"></a>01146 VectorSparseDouble_swigregister = _seldon.VectorSparseDouble_swigregister
<a name="l01147"></a>01147 VectorSparseDouble_swigregister(VectorSparseDouble)
<a name="l01148"></a>01148 
<a name="l01149"></a><a class="code" href="classseldon_1_1_base_matrix_sparse_double.php">01149</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_base_matrix_sparse_double.php">BaseMatrixSparseDouble</a>(<a class="code" href="classseldon_1_1_matrix_base_double.php">MatrixBaseDouble</a>):
<a name="l01150"></a>01150     __swig_setmethods__ = {}
<a name="l01151"></a>01151     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [MatrixBaseDouble]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l01152"></a>01152     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, BaseMatrixSparseDouble, name, value)
<a name="l01153"></a>01153     __swig_getmethods__ = {}
<a name="l01154"></a>01154     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [MatrixBaseDouble]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l01155"></a>01155     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, BaseMatrixSparseDouble, name)
<a name="l01156"></a>01156     __repr__ = _swig_repr
<a name="l01157"></a>01157     <span class="keyword">def </span>__init__(self, *args): 
<a name="l01158"></a>01158         this = _seldon.new_BaseMatrixSparseDouble(*args)
<a name="l01159"></a>01159         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_base_matrix_sparse_double.php#a9ec6ae7738284600ab7f4f030a6f928b">this</a>.append(this)
<a name="l01160"></a>01160         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_base_matrix_sparse_double.php#a9ec6ae7738284600ab7f4f030a6f928b">this</a> = this
<a name="l01161"></a>01161     __swig_destroy__ = _seldon.delete_BaseMatrixSparseDouble
<a name="l01162"></a>01162     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l01163"></a>01163     <span class="keyword">def </span>Clear(self): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_Clear(self)
<a name="l01164"></a>01164     <span class="keyword">def </span>SetData(self, *args): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_SetData(self, *args)
<a name="l01165"></a>01165     <span class="keyword">def </span>Nullify(self): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_Nullify(self)
<a name="l01166"></a>01166     <span class="keyword">def </span>Reallocate(self, *args): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_Reallocate(self, *args)
<a name="l01167"></a>01167     <span class="keyword">def </span>Resize(self, *args): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_Resize(self, *args)
<a name="l01168"></a>01168     <span class="keyword">def </span>Copy(self, *args): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_Copy(self, *args)
<a name="l01169"></a>01169     <span class="keyword">def </span>GetNonZeros(self): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_GetNonZeros(self)
<a name="l01170"></a>01170     <span class="keyword">def </span>GetDataSize(self): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_GetDataSize(self)
<a name="l01171"></a>01171     <span class="keyword">def </span>GetPtr(self): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_GetPtr(self)
<a name="l01172"></a>01172     <span class="keyword">def </span>GetInd(self): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_GetInd(self)
<a name="l01173"></a>01173     <span class="keyword">def </span>GetPtrSize(self): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_GetPtrSize(self)
<a name="l01174"></a>01174     <span class="keyword">def </span>GetIndSize(self): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_GetIndSize(self)
<a name="l01175"></a>01175     <span class="keyword">def </span>__call__(self, *args): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble___call__(self, *args)
<a name="l01176"></a>01176     <span class="keyword">def </span>Val(self, *args): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_Val(self, *args)
<a name="l01177"></a>01177     <span class="keyword">def </span>Get(self, *args): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_Get(self, *args)
<a name="l01178"></a>01178     <span class="keyword">def </span>AddInteraction(self, *args): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_AddInteraction(self, *args)
<a name="l01179"></a>01179     <span class="keyword">def </span>Set(self, *args): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_Set(self, *args)
<a name="l01180"></a>01180     <span class="keyword">def </span>Zero(self): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_Zero(self)
<a name="l01181"></a>01181     <span class="keyword">def </span>SetIdentity(self): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_SetIdentity(self)
<a name="l01182"></a>01182     <span class="keyword">def </span>Fill(self): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_Fill(self)
<a name="l01183"></a>01183     <span class="keyword">def </span>FillRand(self, *args): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_FillRand(self, *args)
<a name="l01184"></a>01184     <span class="keyword">def </span>Print(self): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_Print(self)
<a name="l01185"></a>01185     <span class="keyword">def </span>Write(self, *args): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_Write(self, *args)
<a name="l01186"></a>01186     <span class="keyword">def </span>WriteText(self, *args): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_WriteText(self, *args)
<a name="l01187"></a>01187     <span class="keyword">def </span>Read(self, *args): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_Read(self, *args)
<a name="l01188"></a>01188     <span class="keyword">def </span>ReadText(self, *args): <span class="keywordflow">return</span> _seldon.BaseMatrixSparseDouble_ReadText(self, *args)
<a name="l01189"></a>01189 BaseMatrixSparseDouble_swigregister = _seldon.BaseMatrixSparseDouble_swigregister
<a name="l01190"></a>01190 BaseMatrixSparseDouble_swigregister(BaseMatrixSparseDouble)
<a name="l01191"></a>01191 
<a name="l01192"></a><a class="code" href="classseldon_1_1_matrix_sparse_double.php">01192</a> <span class="keyword">class </span><a class="code" href="classseldon_1_1_matrix_sparse_double.php">MatrixSparseDouble</a>(<a class="code" href="classseldon_1_1_base_matrix_sparse_double.php">BaseMatrixSparseDouble</a>):
<a name="l01193"></a>01193     __swig_setmethods__ = {}
<a name="l01194"></a>01194     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [BaseMatrixSparseDouble]: __swig_setmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_setmethods__&#39;</span>,{}))
<a name="l01195"></a>01195     __setattr__ = <span class="keyword">lambda</span> self, name, value: _swig_setattr(self, MatrixSparseDouble, name, value)
<a name="l01196"></a>01196     __swig_getmethods__ = {}
<a name="l01197"></a>01197     <span class="keywordflow">for</span> _s <span class="keywordflow">in</span> [BaseMatrixSparseDouble]: __swig_getmethods__.update(getattr(_s,<span class="stringliteral">&#39;__swig_getmethods__&#39;</span>,{}))
<a name="l01198"></a>01198     __getattr__ = <span class="keyword">lambda</span> self, name: _swig_getattr(self, MatrixSparseDouble, name)
<a name="l01199"></a>01199     __repr__ = _swig_repr
<a name="l01200"></a>01200     <span class="keyword">def </span>__init__(self, *args): 
<a name="l01201"></a>01201         this = _seldon.new_MatrixSparseDouble(*args)
<a name="l01202"></a>01202         <span class="keywordflow">try</span>: self.<a class="code" href="classseldon_1_1_matrix_sparse_double.php#a8fd09b52ded227e04a536b702d74c4dc">this</a>.append(this)
<a name="l01203"></a>01203         <span class="keywordflow">except</span>: self.<a class="code" href="classseldon_1_1_matrix_sparse_double.php#a8fd09b52ded227e04a536b702d74c4dc">this</a> = this
<a name="l01204"></a>01204     __swig_destroy__ = _seldon.delete_MatrixSparseDouble
<a name="l01205"></a>01205     __del__ = <span class="keyword">lambda</span> self : <span class="keywordtype">None</span>;
<a name="l01206"></a>01206 MatrixSparseDouble_swigregister = _seldon.MatrixSparseDouble_swigregister
<a name="l01207"></a>01207 MatrixSparseDouble_swigregister(MatrixSparseDouble)
<a name="l01208"></a>01208 
<a name="l01209"></a>01209 
<a name="l01210"></a>01210 <span class="keyword">def </span>skip_vector_double(*args):
<a name="l01211"></a>01211   <span class="keywordflow">return</span> _seldon.skip_vector_double(*args)
<a name="l01212"></a>01212 skip_vector_double = _seldon.skip_vector_double
<a name="l01213"></a>01213 
<a name="l01214"></a>01214 <span class="keyword">def </span>skip_matrix_double(*args):
<a name="l01215"></a>01215   <span class="keywordflow">return</span> _seldon.skip_matrix_double(*args)
<a name="l01216"></a>01216 skip_matrix_double = _seldon.skip_matrix_double
<a name="l01217"></a>01217 <span class="keyword">def </span>load_vector(filename, array = False):
<a name="l01218"></a>01218     <span class="stringliteral">&quot;&quot;&quot;</span>
<a name="l01219"></a>01219 <span class="stringliteral">    Loads a Seldon vector (in double precision) from a file. If &#39;array&#39; is set</span>
<a name="l01220"></a>01220 <span class="stringliteral">    to True, the vector is converted to a numpy array.</span>
<a name="l01221"></a>01221 <span class="stringliteral">    &quot;&quot;&quot;</span>
<a name="l01222"></a>01222     <span class="keyword">import</span> seldon
<a name="l01223"></a>01223     <span class="keywordflow">if</span> isinstance(filename, str):
<a name="l01224"></a>01224         stream = seldon.ifstream(filename)
<a name="l01225"></a>01225     <span class="keywordflow">else</span>: <span class="comment"># assuming &#39;filename&#39; is already a stream.</span>
<a name="l01226"></a>01226         stream = filename
<a name="l01227"></a>01227 
<a name="l01228"></a>01228     vector = seldon.VectorDouble()
<a name="l01229"></a>01229     vector.Read(stream, <span class="keyword">True</span>)
<a name="l01230"></a>01230 
<a name="l01231"></a>01231     <span class="keywordflow">if</span> array:
<a name="l01232"></a>01232         <span class="keyword">import</span> numpy
<a name="l01233"></a>01233         vector = numpy.array(vector)
<a name="l01234"></a>01234 
<a name="l01235"></a>01235     <span class="keywordflow">if</span> isinstance(filename, str):
<a name="l01236"></a>01236         stream.close()
<a name="l01237"></a>01237 
<a name="l01238"></a>01238     <span class="keywordflow">return</span> vector
<a name="l01239"></a>01239 
<a name="l01240"></a>01240 
<a name="l01241"></a>01241 <span class="keyword">def </span>load_vector_list(filename, array = False, begin = 0, N = 0):
<a name="l01242"></a>01242     <span class="stringliteral">&quot;&quot;&quot;</span>
<a name="l01243"></a>01243 <span class="stringliteral">    Loads a list of Seldon vectors (in double precision) from a file, skipping</span>
<a name="l01244"></a>01244 <span class="stringliteral">    the first &#39;begin&#39; vectors. If &#39;array&#39; is set to True, the vectors are</span>
<a name="l01245"></a>01245 <span class="stringliteral">    converted to numpy arrays. &#39;N&#39; is the number of vectors to read; all</span>
<a name="l01246"></a>01246 <span class="stringliteral">    vectors are read if &#39;N&#39; is 0 or negative.</span>
<a name="l01247"></a>01247 <span class="stringliteral">    &quot;&quot;&quot;</span>
<a name="l01248"></a>01248     <span class="keyword">import</span> seldon
<a name="l01249"></a>01249     <span class="keywordflow">if</span> isinstance(filename, str):
<a name="l01250"></a>01250         stream = seldon.ifstream(filename)
<a name="l01251"></a>01251     <span class="keywordflow">else</span>: <span class="comment"># assuming &#39;filename&#39; is already a stream.</span>
<a name="l01252"></a>01252         stream = filename
<a name="l01253"></a>01253 
<a name="l01254"></a>01254     begin = max(begin, 0)
<a name="l01255"></a>01255     count = 0
<a name="l01256"></a>01256     <span class="keywordflow">while</span> stream.peek() != -1 <span class="keywordflow">and</span> count != begin:
<a name="l01257"></a>01257         skip_vector_double(stream)
<a name="l01258"></a>01258         count += 1
<a name="l01259"></a>01259 
<a name="l01260"></a>01260     vector_list = []
<a name="l01261"></a>01261     count = 0
<a name="l01262"></a>01262     <span class="keywordflow">while</span> stream.peek() != -1 <span class="keywordflow">and</span> (N &lt;= 0 <span class="keywordflow">or</span> count &lt; N):
<a name="l01263"></a>01263         vector = seldon.VectorDouble()
<a name="l01264"></a>01264         vector.Read(stream, <span class="keyword">True</span>)
<a name="l01265"></a>01265         vector_list.append(vector)
<a name="l01266"></a>01266         count += 1
<a name="l01267"></a>01267 
<a name="l01268"></a>01268     <span class="keywordflow">if</span> array:
<a name="l01269"></a>01269         <span class="keyword">import</span> numpy
<a name="l01270"></a>01270         vector_list = [numpy.array(x) <span class="keywordflow">for</span> x <span class="keywordflow">in</span> vector_list]
<a name="l01271"></a>01271 
<a name="l01272"></a>01272     <span class="keywordflow">if</span> isinstance(filename, str):
<a name="l01273"></a>01273         stream.close()
<a name="l01274"></a>01274 
<a name="l01275"></a>01275     <span class="keywordflow">return</span> vector_list
<a name="l01276"></a>01276 
<a name="l01277"></a>01277 
<a name="l01278"></a>01278 <span class="keyword">def </span>to_vector(v):
<a name="l01279"></a>01279     <span class="stringliteral">&quot;&quot;&quot;</span>
<a name="l01280"></a>01280 <span class="stringliteral">    Converts a list or a numpy array to a Seldon vector (in double precision).</span>
<a name="l01281"></a>01281 <span class="stringliteral">    &quot;&quot;&quot;</span>
<a name="l01282"></a>01282     <span class="keyword">import</span> seldon
<a name="l01283"></a>01283     out = seldon.VectorDouble(len(v))
<a name="l01284"></a>01284     <span class="keywordflow">for</span> i <span class="keywordflow">in</span> range(len(v)):
<a name="l01285"></a>01285         out[i] = v[i]
<a name="l01286"></a>01286     <span class="keywordflow">return</span> out
<a name="l01287"></a>01287 
<a name="l01288"></a>01288 
<a name="l01289"></a>01289 <span class="keyword">def </span>load_matrix(filename, array = False):
<a name="l01290"></a>01290     <span class="stringliteral">&quot;&quot;&quot;</span>
<a name="l01291"></a>01291 <span class="stringliteral">    Loads a Seldon matrix (in double precision) from a file. If &#39;array&#39; is set</span>
<a name="l01292"></a>01292 <span class="stringliteral">    to True, the matrix is converted to a numpy array.</span>
<a name="l01293"></a>01293 <span class="stringliteral">    &quot;&quot;&quot;</span>
<a name="l01294"></a>01294     <span class="keyword">import</span> seldon
<a name="l01295"></a>01295     <span class="keywordflow">if</span> isinstance(filename, str):
<a name="l01296"></a>01296         stream = seldon.ifstream(filename)
<a name="l01297"></a>01297     <span class="keywordflow">else</span>: <span class="comment"># assuming &#39;filename&#39; is already a stream.</span>
<a name="l01298"></a>01298         stream = filename
<a name="l01299"></a>01299 
<a name="l01300"></a>01300     matrix = seldon.MatrixDouble()
<a name="l01301"></a>01301     matrix.Read(stream, <span class="keyword">True</span>)
<a name="l01302"></a>01302 
<a name="l01303"></a>01303     <span class="keywordflow">if</span> array:
<a name="l01304"></a>01304         <span class="keyword">import</span> numpy
<a name="l01305"></a>01305         matrix = numpy.array(matrix)
<a name="l01306"></a>01306 
<a name="l01307"></a>01307     <span class="keywordflow">if</span> isinstance(filename, str):
<a name="l01308"></a>01308         stream.close()
<a name="l01309"></a>01309 
<a name="l01310"></a>01310     <span class="keywordflow">return</span> matrix
<a name="l01311"></a>01311 
<a name="l01312"></a>01312 
<a name="l01313"></a>01313 <span class="keyword">def </span>load_matrix_list(filename, array = False, begin = 0, N = 0):
<a name="l01314"></a>01314     <span class="stringliteral">&quot;&quot;&quot;</span>
<a name="l01315"></a>01315 <span class="stringliteral">    Loads a list of Seldon matrices (in double precision) from a file,</span>
<a name="l01316"></a>01316 <span class="stringliteral">    skipping the first &#39;begin&#39; matrices.. If &#39;array&#39; is set to True, the</span>
<a name="l01317"></a>01317 <span class="stringliteral">    matrices are converted to numpy arrays. &#39;N&#39; is the number of matrices to</span>
<a name="l01318"></a>01318 <span class="stringliteral">    read; all matrices are read if &#39;N&#39; is 0 or negative.</span>
<a name="l01319"></a>01319 <span class="stringliteral">    &quot;&quot;&quot;</span>
<a name="l01320"></a>01320     <span class="keyword">import</span> seldon
<a name="l01321"></a>01321     <span class="keywordflow">if</span> isinstance(filename, str):
<a name="l01322"></a>01322         stream = seldon.ifstream(filename)
<a name="l01323"></a>01323     <span class="keywordflow">else</span>: <span class="comment"># assuming &#39;filename&#39; is already a stream.</span>
<a name="l01324"></a>01324         stream = filename
<a name="l01325"></a>01325 
<a name="l01326"></a>01326     begin = max(begin, 0)
<a name="l01327"></a>01327     count = 0
<a name="l01328"></a>01328     <span class="keywordflow">while</span> stream.peek() != -1 <span class="keywordflow">and</span> count != begin:
<a name="l01329"></a>01329         skip_matrix_double(stream)
<a name="l01330"></a>01330         count += 1
<a name="l01331"></a>01331 
<a name="l01332"></a>01332     matrix_list = []
<a name="l01333"></a>01333     count = 0
<a name="l01334"></a>01334     <span class="keywordflow">while</span> stream.peek() != -1 <span class="keywordflow">and</span> (N &lt;= 0 <span class="keywordflow">or</span> count &lt; N):
<a name="l01335"></a>01335         matrix = seldon.MatrixDouble()
<a name="l01336"></a>01336         matrix.Read(stream, <span class="keyword">True</span>)
<a name="l01337"></a>01337         matrix_list.append(matrix)
<a name="l01338"></a>01338         count += 1
<a name="l01339"></a>01339 
<a name="l01340"></a>01340     <span class="keywordflow">if</span> array:
<a name="l01341"></a>01341         <span class="keyword">import</span> numpy
<a name="l01342"></a>01342         matrix_list = [numpy.array(x) <span class="keywordflow">for</span> x <span class="keywordflow">in</span> matrix_list]
<a name="l01343"></a>01343 
<a name="l01344"></a>01344     <span class="keywordflow">if</span> isinstance(filename, str):
<a name="l01345"></a>01345         stream.close()
<a name="l01346"></a>01346 
<a name="l01347"></a>01347     <span class="keywordflow">return</span> matrix_list
<a name="l01348"></a>01348 
<a name="l01349"></a>01349 
<a name="l01350"></a>01350 <span class="keyword">def </span>to_matrix(m):
<a name="l01351"></a>01351     <span class="stringliteral">&quot;&quot;&quot;</span>
<a name="l01352"></a>01352 <span class="stringliteral">    Converts a numpy array to a Seldon matrix (in double precision).</span>
<a name="l01353"></a>01353 <span class="stringliteral">    &quot;&quot;&quot;</span>
<a name="l01354"></a>01354     <span class="keyword">import</span> seldon
<a name="l01355"></a>01355     out = seldon.MatrixDouble(m.shape[0], m.shape[1])
<a name="l01356"></a>01356     <span class="keywordflow">for</span> i <span class="keywordflow">in</span> range(m.shape[0]):
<a name="l01357"></a>01357         <span class="keywordflow">for</span> j <span class="keywordflow">in</span> range(m.shape[1]):
<a name="l01358"></a>01358             out[i, j] = m[i, j]
<a name="l01359"></a>01359     <span class="keywordflow">return</span> out
<a name="l01360"></a>01360 
<a name="l01361"></a>01361 <span class="comment"># This file is compatible with both classic and new-style classes.</span>
<a name="l01362"></a>01362 
<a name="l01363"></a>01363 
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
