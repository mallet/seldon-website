<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix_sparse/Matrix_ArrayComplexSparse.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_ARRAY_COMPLEX_SPARSE_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;Matrix_ArrayComplexSparse.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="keyword">namespace </span>Seldon
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 
<a name="l00029"></a>00029   <span class="comment">/****************</span>
<a name="l00030"></a>00030 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00031"></a>00031 <span class="comment">   ****************/</span>
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00035"></a>00035 
<a name="l00038"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a415bb8c2cb2379647933ddc5d9dbcc32">00038</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00039"></a>00039   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a415bb8c2cb2379647933ddc5d9dbcc32" title="Default constructor.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00040"></a>00040 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a415bb8c2cb2379647933ddc5d9dbcc32" title="Default constructor.">  Matrix_ArrayComplexSparse</a>()
<a name="l00041"></a>00041     : val_real_(), val_imag_()
<a name="l00042"></a>00042   {
<a name="l00043"></a>00043     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a> = 0;
<a name="l00044"></a>00044     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a> = 0;
<a name="l00045"></a>00045   }
<a name="l00046"></a>00046 
<a name="l00047"></a>00047 
<a name="l00049"></a>00049 
<a name="l00054"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a410f415f4976918fc7678bc770611043">00054</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00055"></a>00055   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a415bb8c2cb2379647933ddc5d9dbcc32" title="Default constructor.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00056"></a>00056 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a415bb8c2cb2379647933ddc5d9dbcc32" title="Default constructor.">  Matrix_ArrayComplexSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l00057"></a>00057     val_real_(Storage::GetFirst(i, j)), val_imag_(Storage::GetFirst(i, j))
<a name="l00058"></a>00058   {
<a name="l00059"></a>00059     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a> = i;
<a name="l00060"></a>00060     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a> = j;
<a name="l00061"></a>00061   }
<a name="l00062"></a>00062 
<a name="l00063"></a>00063 
<a name="l00064"></a>00064   <span class="comment">/**************</span>
<a name="l00065"></a>00065 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00066"></a>00066 <span class="comment">   **************/</span>
<a name="l00067"></a>00067 
<a name="l00068"></a>00068 
<a name="l00070"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acec6f5d4aa6488c9389a2065944568b0">00070</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00071"></a>00071   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acec6f5d4aa6488c9389a2065944568b0" title="Destructor.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00072"></a>00072 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acec6f5d4aa6488c9389a2065944568b0" title="Destructor.">  ~Matrix_ArrayComplexSparse</a>()
<a name="l00073"></a>00073   {
<a name="l00074"></a>00074     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0ee918c8c04a1a9434e157c15fdeb596" title="Clears the matrix.">Clear</a>();
<a name="l00075"></a>00075   }
<a name="l00076"></a>00076 
<a name="l00077"></a>00077 
<a name="l00079"></a>00079 
<a name="l00082"></a>00082   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00083"></a>00083   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0ee918c8c04a1a9434e157c15fdeb596" title="Clears the matrix.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Clear</a>()
<a name="l00084"></a>00084   {
<a name="l00085"></a>00085     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a> = 0;
<a name="l00086"></a>00086     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a> = 0;
<a name="l00087"></a>00087     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>.Clear();
<a name="l00088"></a>00088     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>.Clear();
<a name="l00089"></a>00089   }
<a name="l00090"></a>00090 
<a name="l00091"></a>00091 
<a name="l00092"></a>00092   <span class="comment">/*********************</span>
<a name="l00093"></a>00093 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00094"></a>00094 <span class="comment">   *********************/</span>
<a name="l00095"></a>00095 
<a name="l00096"></a>00096 
<a name="l00098"></a>00098 
<a name="l00104"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad2348fb25206ef8516c7e835b7ce2287">00104</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00105"></a>00105   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad2348fb25206ef8516c7e835b7ce2287" title="Reallocates memory to resize the matrix.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00106"></a>00106 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad2348fb25206ef8516c7e835b7ce2287" title="Reallocates memory to resize the matrix.">  Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00107"></a>00107   {
<a name="l00108"></a>00108     <span class="comment">// Clears previous entries.</span>
<a name="l00109"></a>00109     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0ee918c8c04a1a9434e157c15fdeb596" title="Clears the matrix.">Clear</a>();
<a name="l00110"></a>00110 
<a name="l00111"></a>00111     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a> = i;
<a name="l00112"></a>00112     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a> = j;
<a name="l00113"></a>00113 
<a name="l00114"></a>00114     <span class="keywordtype">int</span> n = Storage::GetFirst(i,j);
<a name="l00115"></a>00115     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>.Reallocate(n);
<a name="l00116"></a>00116     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>.Reallocate(n);
<a name="l00117"></a>00117   }
<a name="l00118"></a>00118 
<a name="l00119"></a>00119 
<a name="l00121"></a>00121 
<a name="l00127"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a9166689334cc2f731220f76b730d692f">00127</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00128"></a>00128   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a9166689334cc2f731220f76b730d692f" title="Reallocates additional memory to resize the matrix.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00129"></a>00129 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a9166689334cc2f731220f76b730d692f" title="Reallocates additional memory to resize the matrix.">  Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00130"></a>00130   {
<a name="l00131"></a>00131     <span class="keywordtype">int</span> n = Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>, <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a>);
<a name="l00132"></a>00132     <span class="keywordtype">int</span> new_n = Storage::GetFirst(i, j);
<a name="l00133"></a>00133     <span class="keywordflow">if</span> (n != new_n)
<a name="l00134"></a>00134       {
<a name="l00135"></a>00135         <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;T, VectSparse, Allocator&gt;</a>, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>,
<a name="l00136"></a>00136           <a class="code" href="class_seldon_1_1_new_alloc.php">NewAlloc&lt;Vector&lt;T, VectSparse, Allocator&gt;</a> &gt; &gt; new_val_real;
<a name="l00137"></a>00137 
<a name="l00138"></a>00138         <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;T, VectSparse, Allocator&gt;</a>, VectFull,
<a name="l00139"></a>00139           <a class="code" href="class_seldon_1_1_new_alloc.php">NewAlloc&lt;Vector&lt;T, VectSparse, Allocator&gt;</a> &gt; &gt; new_val_imag;
<a name="l00140"></a>00140 
<a name="l00141"></a>00141         new_val_real.Reallocate(new_n);
<a name="l00142"></a>00142         new_val_imag.Reallocate(new_n);
<a name="l00143"></a>00143 
<a name="l00144"></a>00144         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0 ; k &lt; min(n, new_n) ; k++)
<a name="l00145"></a>00145           {
<a name="l00146"></a>00146             Swap(new_val_real(k), this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(k));
<a name="l00147"></a>00147             Swap(new_val_imag(k), this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(k));
<a name="l00148"></a>00148           }
<a name="l00149"></a>00149 
<a name="l00150"></a>00150         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>.SetData(new_n, new_val_real.GetData());
<a name="l00151"></a>00151         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>.SetData(new_n, new_val_imag.GetData());
<a name="l00152"></a>00152         new_val_real.Nullify();
<a name="l00153"></a>00153         new_val_imag.Nullify();
<a name="l00154"></a>00154 
<a name="l00155"></a>00155       }
<a name="l00156"></a>00156 
<a name="l00157"></a>00157     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a> = i;
<a name="l00158"></a>00158     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a> = j;
<a name="l00159"></a>00159   }
<a name="l00160"></a>00160 
<a name="l00161"></a>00161 
<a name="l00162"></a>00162   <span class="comment">/*******************</span>
<a name="l00163"></a>00163 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00164"></a>00164 <span class="comment">   *******************/</span>
<a name="l00165"></a>00165 
<a name="l00166"></a>00166 
<a name="l00168"></a>00168 
<a name="l00171"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821">00171</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00172"></a>00172   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00173"></a>00173 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">  ::GetM</a>()<span class="keyword"> const</span>
<a name="l00174"></a>00174 <span class="keyword">  </span>{
<a name="l00175"></a>00175     <span class="keywordflow">return</span> m_;
<a name="l00176"></a>00176   }
<a name="l00177"></a>00177 
<a name="l00178"></a>00178 
<a name="l00180"></a>00180 
<a name="l00183"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ae413915ed95545b40df8b43737fa43c1">00183</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00184"></a>00184   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ae413915ed95545b40df8b43737fa43c1" title="Returns the number of columns.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00185"></a>00185 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ae413915ed95545b40df8b43737fa43c1" title="Returns the number of columns.">  ::GetN</a>()<span class="keyword"> const</span>
<a name="l00186"></a>00186 <span class="keyword">  </span>{
<a name="l00187"></a>00187     <span class="keywordflow">return</span> n_;
<a name="l00188"></a>00188   }
<a name="l00189"></a>00189 
<a name="l00190"></a>00190 
<a name="l00192"></a>00192 
<a name="l00196"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ab3c6aad66c9dc6ad9addfb7de9a4ebdd">00196</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00197"></a>00197   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00198"></a>00198 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">  ::GetM</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; status)<span class="keyword"> const</span>
<a name="l00199"></a>00199 <span class="keyword">  </span>{
<a name="l00200"></a>00200     <span class="keywordflow">if</span> (status.NoTrans())
<a name="l00201"></a>00201       <span class="keywordflow">return</span> m_;
<a name="l00202"></a>00202     <span class="keywordflow">else</span>
<a name="l00203"></a>00203       <span class="keywordflow">return</span> n_;
<a name="l00204"></a>00204   }
<a name="l00205"></a>00205 
<a name="l00206"></a>00206 
<a name="l00208"></a>00208 
<a name="l00212"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a53e20518a8557211c89ca8ad0d607825">00212</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00213"></a>00213   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ae413915ed95545b40df8b43737fa43c1" title="Returns the number of columns.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00214"></a>00214 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ae413915ed95545b40df8b43737fa43c1" title="Returns the number of columns.">  ::GetN</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; status)<span class="keyword"> const</span>
<a name="l00215"></a>00215 <span class="keyword">  </span>{
<a name="l00216"></a>00216     <span class="keywordflow">if</span> (status.NoTrans())
<a name="l00217"></a>00217       <span class="keywordflow">return</span> n_;
<a name="l00218"></a>00218     <span class="keywordflow">else</span>
<a name="l00219"></a>00219       <span class="keywordflow">return</span> m_;
<a name="l00220"></a>00220   }
<a name="l00221"></a>00221 
<a name="l00222"></a>00222 
<a name="l00224"></a>00224 
<a name="l00227"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a11efc1bee6a95da705238628d7e3d958">00227</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00228"></a>00228   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a11efc1bee6a95da705238628d7e3d958" title="Returns the number of non-zero elements (real part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00229"></a>00229 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a11efc1bee6a95da705238628d7e3d958" title="Returns the number of non-zero elements (real part).">  GetRealNonZeros</a>()<span class="keyword"> const</span>
<a name="l00230"></a>00230 <span class="keyword">  </span>{
<a name="l00231"></a>00231     <span class="keywordtype">int</span> nnz = 0;
<a name="l00232"></a>00232     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>.GetM(); i++)
<a name="l00233"></a>00233       nnz += this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).GetM();
<a name="l00234"></a>00234 
<a name="l00235"></a>00235     <span class="keywordflow">return</span> nnz;
<a name="l00236"></a>00236   }
<a name="l00237"></a>00237 
<a name="l00238"></a>00238 
<a name="l00240"></a>00240 
<a name="l00243"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a33ebac042eda90796d7f674c250e85be">00243</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00244"></a>00244   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a33ebac042eda90796d7f674c250e85be" title="Returns the number of non-zero elements (imaginary part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00245"></a>00245 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a33ebac042eda90796d7f674c250e85be" title="Returns the number of non-zero elements (imaginary part).">  GetImagNonZeros</a>()<span class="keyword"> const</span>
<a name="l00246"></a>00246 <span class="keyword">  </span>{
<a name="l00247"></a>00247     <span class="keywordtype">int</span> nnz = 0;
<a name="l00248"></a>00248     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>.GetM(); i++)
<a name="l00249"></a>00249       nnz += this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).GetM();
<a name="l00250"></a>00250 
<a name="l00251"></a>00251     <span class="keywordflow">return</span> nnz;
<a name="l00252"></a>00252   }
<a name="l00253"></a>00253 
<a name="l00254"></a>00254 
<a name="l00256"></a>00256 
<a name="l00261"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa40775992c738c7fd1e9543bfddb2694">00261</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00262"></a>00262   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa40775992c738c7fd1e9543bfddb2694" title="Returns the number of elements stored in memory (real part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00263"></a>00263 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa40775992c738c7fd1e9543bfddb2694" title="Returns the number of elements stored in memory (real part).">  GetRealDataSize</a>()<span class="keyword"> const</span>
<a name="l00264"></a>00264 <span class="keyword">  </span>{
<a name="l00265"></a>00265     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a11efc1bee6a95da705238628d7e3d958" title="Returns the number of non-zero elements (real part).">GetRealNonZeros</a>();
<a name="l00266"></a>00266   }
<a name="l00267"></a>00267 
<a name="l00268"></a>00268 
<a name="l00270"></a>00270 
<a name="l00275"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad54321edab8e2633985e198f1a576340">00275</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00276"></a>00276   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad54321edab8e2633985e198f1a576340" title="Returns the number of elements stored in memory (imaginary part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00277"></a>00277 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad54321edab8e2633985e198f1a576340" title="Returns the number of elements stored in memory (imaginary part).">  GetImagDataSize</a>()<span class="keyword"> const</span>
<a name="l00278"></a>00278 <span class="keyword">  </span>{
<a name="l00279"></a>00279     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a33ebac042eda90796d7f674c250e85be" title="Returns the number of non-zero elements (imaginary part).">GetImagNonZeros</a>();
<a name="l00280"></a>00280   }
<a name="l00281"></a>00281 
<a name="l00282"></a>00282 
<a name="l00284"></a>00284 
<a name="l00289"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2e5c6c10af220eea29f9c1565b14a93f">00289</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00290"></a>00290   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2e5c6c10af220eea29f9c1565b14a93f" title="Returns the number of elements stored in memory (real+imaginary part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00291"></a>00291 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2e5c6c10af220eea29f9c1565b14a93f" title="Returns the number of elements stored in memory (real+imaginary part).">  GetDataSize</a>()<span class="keyword"> const</span>
<a name="l00292"></a>00292 <span class="keyword">  </span>{
<a name="l00293"></a>00293     <span class="keywordflow">return</span> (<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a11efc1bee6a95da705238628d7e3d958" title="Returns the number of non-zero elements (real part).">GetRealNonZeros</a>()+<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a33ebac042eda90796d7f674c250e85be" title="Returns the number of non-zero elements (imaginary part).">GetImagNonZeros</a>());
<a name="l00294"></a>00294   }
<a name="l00295"></a>00295 
<a name="l00296"></a>00296 
<a name="l00298"></a>00298 
<a name="l00303"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afae45969eda952b43748cf7756f0e6d6">00303</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00304"></a>00304   <span class="keyword">inline</span> <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afae45969eda952b43748cf7756f0e6d6" title="Returns column indices of non-zero entries in row (real part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00305"></a>00305 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afae45969eda952b43748cf7756f0e6d6" title="Returns column indices of non-zero entries in row (real part).">  GetRealInd</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00306"></a>00306 <span class="keyword">  </span>{
<a name="l00307"></a>00307     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).GetIndex();
<a name="l00308"></a>00308   }
<a name="l00309"></a>00309 
<a name="l00310"></a>00310 
<a name="l00312"></a>00312 
<a name="l00316"></a>00316   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span> T*
<a name="l00317"></a>00317   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetRealData</a>(<span class="keywordtype">int</span> i)<span class="keyword"></span>
<a name="l00318"></a>00318 <span class="keyword">    const</span>
<a name="l00319"></a>00319 <span class="keyword">  </span>{
<a name="l00320"></a>00320     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).GetData();
<a name="l00321"></a>00321   }
<a name="l00322"></a>00322 
<a name="l00323"></a>00323 
<a name="l00325"></a>00325 
<a name="l00330"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0a423d7e93a0ad9c19ed6de2b1052dca">00330</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00331"></a>00331   <span class="keyword">inline</span> <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0a423d7e93a0ad9c19ed6de2b1052dca" title="Returns column indices of non-zero entries in row (imaginary part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00332"></a>00332 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0a423d7e93a0ad9c19ed6de2b1052dca" title="Returns column indices of non-zero entries in row (imaginary part).">  GetImagInd</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00333"></a>00333 <span class="keyword">  </span>{
<a name="l00334"></a>00334     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).GetIndex();
<a name="l00335"></a>00335   }
<a name="l00336"></a>00336 
<a name="l00337"></a>00337 
<a name="l00339"></a>00339 
<a name="l00343"></a>00343   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span> T*
<a name="l00344"></a>00344   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetImagData</a>(<span class="keywordtype">int</span> i)<span class="keyword"></span>
<a name="l00345"></a>00345 <span class="keyword">    const</span>
<a name="l00346"></a>00346 <span class="keyword">  </span>{
<a name="l00347"></a>00347     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).GetData();
<a name="l00348"></a>00348   }
<a name="l00349"></a>00349 
<a name="l00350"></a>00350 
<a name="l00351"></a>00351   <span class="comment">/**********************************</span>
<a name="l00352"></a>00352 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l00353"></a>00353 <span class="comment">   **********************************/</span>
<a name="l00354"></a>00354 
<a name="l00355"></a>00355 
<a name="l00357"></a>00357 
<a name="l00363"></a>00363   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00364"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a30be6306dbdd79b0a282ef99289b0c2b">00364</a>   <span class="keyword">inline</span> complex&lt;T&gt;
<a name="l00365"></a>00365   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::operator</a>()
<a name="l00366"></a>00366     (<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>
<a name="l00367"></a>00367   {
<a name="l00368"></a>00368 
<a name="l00369"></a>00369 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00370"></a>00370 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00371"></a>00371       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00372"></a>00372                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00373"></a>00373                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00374"></a>00374 
<a name="l00375"></a>00375     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00376"></a>00376       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00377"></a>00377                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00378"></a>00378                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00379"></a>00379 <span class="preprocessor">#endif</span>
<a name="l00380"></a>00380 <span class="preprocessor"></span>
<a name="l00381"></a>00381     <span class="keywordflow">return</span> complex&lt;T&gt;(this-&gt;val_real_(Storage::GetFirst(i, j))
<a name="l00382"></a>00382                       (Storage::GetSecond(i, j)),
<a name="l00383"></a>00383                       this-&gt;val_imag_(Storage::GetFirst(i, j))
<a name="l00384"></a>00384                       (Storage::GetSecond(i, j)) );
<a name="l00385"></a>00385   }
<a name="l00386"></a>00386 
<a name="l00387"></a>00387 
<a name="l00389"></a>00389 
<a name="l00395"></a>00395   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00396"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7eb05d6902b13cfb7cc4f17093fdc17e">00396</a>   <span class="keyword">inline</span> complex&lt;T&gt;&amp;
<a name="l00397"></a>00397   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7eb05d6902b13cfb7cc4f17093fdc17e" title="Unavailable access method.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00398"></a>00398 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7eb05d6902b13cfb7cc4f17093fdc17e" title="Unavailable access method.">  ::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00399"></a>00399   {
<a name="l00400"></a>00400     <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;Matrix_ArrayComplexSparse::Val(int i, int j)&quot;</span>);
<a name="l00401"></a>00401   }
<a name="l00402"></a>00402 
<a name="l00403"></a>00403 
<a name="l00405"></a>00405 
<a name="l00411"></a>00411   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00412"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a994fbc16dd258d1609f7c7c582205e9f">00412</a>   <span class="keyword">inline</span> <span class="keyword">const</span> complex&lt;T&gt;&amp;
<a name="l00413"></a>00413   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7eb05d6902b13cfb7cc4f17093fdc17e" title="Unavailable access method.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00414"></a>00414 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7eb05d6902b13cfb7cc4f17093fdc17e" title="Unavailable access method.">  ::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00415"></a>00415 <span class="keyword">  </span>{
<a name="l00416"></a>00416     <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;Matrix_ArrayComplexSparse::Val(int i, int j)&quot;</span>);
<a name="l00417"></a>00417   }
<a name="l00418"></a>00418 
<a name="l00419"></a>00419 
<a name="l00421"></a>00421 
<a name="l00427"></a>00427   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00428"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad840efb36621d28aa9726088cfc29a29">00428</a>   <span class="keyword">inline</span> complex&lt;T&gt;&amp;
<a name="l00429"></a>00429   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad840efb36621d28aa9726088cfc29a29" title="Unavailable access method.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00430"></a>00430 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad840efb36621d28aa9726088cfc29a29" title="Unavailable access method.">  ::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00431"></a>00431   {
<a name="l00432"></a>00432     <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;Matrix_ArrayComplexSparse::Get(int i, int j)&quot;</span>);
<a name="l00433"></a>00433   }
<a name="l00434"></a>00434 
<a name="l00435"></a>00435 
<a name="l00437"></a>00437 
<a name="l00443"></a>00443   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00444"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a17a0b2e40258b30da1f0036c2a85c357">00444</a>   <span class="keyword">inline</span> <span class="keyword">const</span> complex&lt;T&gt;&amp;
<a name="l00445"></a>00445   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad840efb36621d28aa9726088cfc29a29" title="Unavailable access method.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00446"></a>00446 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad840efb36621d28aa9726088cfc29a29" title="Unavailable access method.">  ::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00447"></a>00447 <span class="keyword">  </span>{
<a name="l00448"></a>00448     <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;Matrix_ArrayComplexSparse::Get(int i, int j)&quot;</span>);
<a name="l00449"></a>00449   }
<a name="l00450"></a>00450 
<a name="l00451"></a>00451 
<a name="l00453"></a>00453 
<a name="l00458"></a>00458   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00459"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#abdacf01dc354d0449b16b1d9e111ec54">00459</a>   <span class="keyword">inline</span> T&amp;
<a name="l00460"></a>00460   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#abdacf01dc354d0449b16b1d9e111ec54" title="Returns acces to real part of A(i, j).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00461"></a>00461 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#abdacf01dc354d0449b16b1d9e111ec54" title="Returns acces to real part of A(i, j).">  ::ValReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00462"></a>00462   {
<a name="l00463"></a>00463     <span class="keywordflow">return</span> val_real_(Storage::GetFirst(i, j)).Val(Storage::GetSecond(i, j));
<a name="l00464"></a>00464   }
<a name="l00465"></a>00465 
<a name="l00466"></a>00466 
<a name="l00468"></a>00468 
<a name="l00473"></a>00473   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00474"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2fa5f2b8aeb3c848f5749d113f93dce4">00474</a>   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp;
<a name="l00475"></a>00475   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#abdacf01dc354d0449b16b1d9e111ec54" title="Returns acces to real part of A(i, j).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00476"></a>00476 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#abdacf01dc354d0449b16b1d9e111ec54" title="Returns acces to real part of A(i, j).">  ::ValReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00477"></a>00477 <span class="keyword">  </span>{
<a name="l00478"></a>00478     <span class="keywordflow">return</span> val_real_(Storage::GetFirst(i, j)).Val(Storage::GetSecond(i, j));
<a name="l00479"></a>00479   }
<a name="l00480"></a>00480 
<a name="l00481"></a>00481 
<a name="l00483"></a>00483 
<a name="l00488"></a>00488   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00489"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aee8de4c9149d47935bf84fc04727c7f2">00489</a>   <span class="keyword">inline</span> T&amp;
<a name="l00490"></a>00490   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aee8de4c9149d47935bf84fc04727c7f2" title="Returns acces to imaginary part of A(i, j).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00491"></a>00491 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aee8de4c9149d47935bf84fc04727c7f2" title="Returns acces to imaginary part of A(i, j).">  ::ValImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00492"></a>00492   {
<a name="l00493"></a>00493     <span class="keywordflow">return</span> val_imag_(Storage::GetFirst(i, j)).Val(Storage::GetSecond(i, j));
<a name="l00494"></a>00494   }
<a name="l00495"></a>00495 
<a name="l00496"></a>00496 
<a name="l00498"></a>00498 
<a name="l00503"></a>00503   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00504"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0324d6e453ce4c18dbc84bdc48fb4d5f">00504</a>   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp;
<a name="l00505"></a>00505   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aee8de4c9149d47935bf84fc04727c7f2" title="Returns acces to imaginary part of A(i, j).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00506"></a>00506 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aee8de4c9149d47935bf84fc04727c7f2" title="Returns acces to imaginary part of A(i, j).">  ::ValImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00507"></a>00507 <span class="keyword">  </span>{
<a name="l00508"></a>00508     <span class="keywordflow">return</span> val_imag_(Storage::GetFirst(i, j)).Val(Storage::GetSecond(i, j));
<a name="l00509"></a>00509   }
<a name="l00510"></a>00510 
<a name="l00511"></a>00511 
<a name="l00513"></a>00513 
<a name="l00518"></a>00518   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00519"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ac91be8b2868c034acbf161a8b3af9598">00519</a>   <span class="keyword">inline</span> T&amp;
<a name="l00520"></a>00520   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ac91be8b2868c034acbf161a8b3af9598" title="Returns acces to real part of A(i, j).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00521"></a>00521 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ac91be8b2868c034acbf161a8b3af9598" title="Returns acces to real part of A(i, j).">  ::GetReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00522"></a>00522   {
<a name="l00523"></a>00523     <span class="keywordflow">return</span> val_real_(Storage::GetFirst(i, j)).Get(Storage::GetSecond(i, j));
<a name="l00524"></a>00524   }
<a name="l00525"></a>00525 
<a name="l00526"></a>00526 
<a name="l00528"></a>00528 
<a name="l00533"></a>00533   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00534"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ae562065dff1e5df2ec2516bf7f4f56ae">00534</a>   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp;
<a name="l00535"></a>00535   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ac91be8b2868c034acbf161a8b3af9598" title="Returns acces to real part of A(i, j).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00536"></a>00536 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ac91be8b2868c034acbf161a8b3af9598" title="Returns acces to real part of A(i, j).">  ::GetReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00537"></a>00537 <span class="keyword">  </span>{
<a name="l00538"></a>00538     <span class="keywordflow">return</span> val_real_(Storage::GetFirst(i, j)).Get(Storage::GetSecond(i, j));
<a name="l00539"></a>00539   }
<a name="l00540"></a>00540 
<a name="l00541"></a>00541 
<a name="l00543"></a>00543 
<a name="l00548"></a>00548   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00549"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a644eff8b81954e1f43c4f9321a866903">00549</a>   <span class="keyword">inline</span> T&amp;
<a name="l00550"></a>00550   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a644eff8b81954e1f43c4f9321a866903" title="Returns acces to imaginary part of A(i, j).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00551"></a>00551 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a644eff8b81954e1f43c4f9321a866903" title="Returns acces to imaginary part of A(i, j).">  ::GetImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00552"></a>00552   {
<a name="l00553"></a>00553     <span class="keywordflow">return</span> val_imag_(Storage::GetFirst(i, j)).Get(Storage::GetSecond(i, j));
<a name="l00554"></a>00554   }
<a name="l00555"></a>00555 
<a name="l00556"></a>00556 
<a name="l00558"></a>00558 
<a name="l00563"></a>00563   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00564"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a3233d9091b1fe40403f0e1779f260033">00564</a>   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp;
<a name="l00565"></a>00565   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a644eff8b81954e1f43c4f9321a866903" title="Returns acces to imaginary part of A(i, j).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00566"></a>00566 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a644eff8b81954e1f43c4f9321a866903" title="Returns acces to imaginary part of A(i, j).">  ::GetImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00567"></a>00567 <span class="keyword">  </span>{
<a name="l00568"></a>00568     <span class="keywordflow">return</span> val_imag_(Storage::GetFirst(i, j)).Get(Storage::GetSecond(i, j));
<a name="l00569"></a>00569   }
<a name="l00570"></a>00570 
<a name="l00571"></a>00571 
<a name="l00573"></a>00573 
<a name="l00578"></a>00578   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00579"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2dfca2deb2a823fc64dc51b7c0728f49">00579</a>   <span class="keyword">inline</span> <span class="keywordtype">void</span>
<a name="l00580"></a>00580   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2dfca2deb2a823fc64dc51b7c0728f49" title="Sets element (i, j) of matrix.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00581"></a>00581 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2dfca2deb2a823fc64dc51b7c0728f49" title="Sets element (i, j) of matrix.">  ::Set</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; x)
<a name="l00582"></a>00582   {
<a name="l00583"></a>00583     <span class="keywordflow">if</span> (real(x) != T(0))
<a name="l00584"></a>00584       val_real_(Storage::GetFirst(i, j)).Get(Storage::GetSecond(i, j)) = real(x);
<a name="l00585"></a>00585     <span class="keywordflow">else</span>
<a name="l00586"></a>00586       {
<a name="l00587"></a>00587         <span class="keywordflow">if</span> (val_real_(Storage::GetFirst(i, j))(Storage::GetSecond(i, j)) != T(0))
<a name="l00588"></a>00588           val_real_(Storage::GetFirst(i, j)).Get(Storage::GetSecond(i, j)) = T(0);
<a name="l00589"></a>00589       }
<a name="l00590"></a>00590 
<a name="l00591"></a>00591     <span class="keywordflow">if</span> (imag(x) != T(0))
<a name="l00592"></a>00592       val_imag_(Storage::GetFirst(i, j)).Get(Storage::GetSecond(i, j)) = imag(x);
<a name="l00593"></a>00593     <span class="keywordflow">else</span>
<a name="l00594"></a>00594       {
<a name="l00595"></a>00595         <span class="keywordflow">if</span> (val_imag_(Storage::GetFirst(i, j))(Storage::GetSecond(i, j)) != T(0))
<a name="l00596"></a>00596           val_imag_(Storage::GetFirst(i, j)).Get(Storage::GetSecond(i, j)) = T(0);
<a name="l00597"></a>00597       }
<a name="l00598"></a>00598 
<a name="l00599"></a>00599   }
<a name="l00600"></a>00600 
<a name="l00601"></a>00601 
<a name="l00603"></a>00603 
<a name="l00608"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a041c76033f939293aa871f2fe45ef22a">00608</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00609"></a>00609   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a041c76033f939293aa871f2fe45ef22a" title="Returns j-th non-zero value of row i (real part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00610"></a>00610 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a041c76033f939293aa871f2fe45ef22a" title="Returns j-th non-zero value of row i (real part).">  ValueReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00611"></a>00611 <span class="keyword">  </span>{
<a name="l00612"></a>00612 
<a name="l00613"></a>00613 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00614"></a>00614 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a>))
<a name="l00615"></a>00615       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00616"></a>00616                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a>)-1)
<a name="l00617"></a>00617                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00618"></a>00618 
<a name="l00619"></a>00619     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>())
<a name="l00620"></a>00620       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00621"></a>00621                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00622"></a>00622                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00623"></a>00623 <span class="preprocessor">#endif</span>
<a name="l00624"></a>00624 <span class="preprocessor"></span>
<a name="l00625"></a>00625     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Value(j);
<a name="l00626"></a>00626   }
<a name="l00627"></a>00627 
<a name="l00628"></a>00628 
<a name="l00630"></a>00630 
<a name="l00635"></a>00635   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00636"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a11fe8648ddd3e6e339eb81735d7e544e">00636</a>   <span class="keyword">inline</span> T&amp;
<a name="l00637"></a>00637   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a041c76033f939293aa871f2fe45ef22a" title="Returns j-th non-zero value of row i (real part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00638"></a>00638 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a041c76033f939293aa871f2fe45ef22a" title="Returns j-th non-zero value of row i (real part).">  ValueReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00639"></a>00639   {
<a name="l00640"></a>00640 
<a name="l00641"></a>00641 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00642"></a>00642 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a>))
<a name="l00643"></a>00643       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00644"></a>00644                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a>)-1)
<a name="l00645"></a>00645                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00646"></a>00646     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>())
<a name="l00647"></a>00647       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00648"></a>00648                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00649"></a>00649                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00650"></a>00650 <span class="preprocessor">#endif</span>
<a name="l00651"></a>00651 <span class="preprocessor"></span>
<a name="l00652"></a>00652     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Value(j);
<a name="l00653"></a>00653   }
<a name="l00654"></a>00654 
<a name="l00655"></a>00655 
<a name="l00657"></a>00657 
<a name="l00662"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2a3cd76154ac61a43afa42563805ce3b">00662</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00663"></a>00663   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2a3cd76154ac61a43afa42563805ce3b" title="Returns column number of j-th non-zero value of row i (real part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00664"></a>00664 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2a3cd76154ac61a43afa42563805ce3b" title="Returns column number of j-th non-zero value of row i (real part).">  IndexReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00665"></a>00665 <span class="keyword">  </span>{
<a name="l00666"></a>00666 
<a name="l00667"></a>00667 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00668"></a>00668 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a>))
<a name="l00669"></a>00669       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00670"></a>00670                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a>)-1)
<a name="l00671"></a>00671                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00672"></a>00672 
<a name="l00673"></a>00673     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>())
<a name="l00674"></a>00674       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00675"></a>00675                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00676"></a>00676                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00677"></a>00677 <span class="preprocessor">#endif</span>
<a name="l00678"></a>00678 <span class="preprocessor"></span>
<a name="l00679"></a>00679     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Index(j);
<a name="l00680"></a>00680   }
<a name="l00681"></a>00681 
<a name="l00682"></a>00682 
<a name="l00684"></a>00684 
<a name="l00689"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2fdb9482dad6378fb8ced1982b6860b1">00689</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00690"></a>00690   <span class="keyword">inline</span> <span class="keywordtype">int</span>&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2a3cd76154ac61a43afa42563805ce3b" title="Returns column number of j-th non-zero value of row i (real part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00691"></a>00691 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2a3cd76154ac61a43afa42563805ce3b" title="Returns column number of j-th non-zero value of row i (real part).">  IndexReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00692"></a>00692   {
<a name="l00693"></a>00693 
<a name="l00694"></a>00694 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00695"></a>00695 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a>))
<a name="l00696"></a>00696       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00697"></a>00697                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a>)-1)
<a name="l00698"></a>00698                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00699"></a>00699 
<a name="l00700"></a>00700     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>())
<a name="l00701"></a>00701       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00702"></a>00702                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>()-1)
<a name="l00703"></a>00703                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00704"></a>00704 <span class="preprocessor">#endif</span>
<a name="l00705"></a>00705 <span class="preprocessor"></span>
<a name="l00706"></a>00706     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Index(j);
<a name="l00707"></a>00707   }
<a name="l00708"></a>00708 
<a name="l00709"></a>00709 
<a name="l00711"></a>00711 
<a name="l00716"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa2d9a2e69a269bf9d15b62d520cb560d">00716</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00717"></a>00717   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa2d9a2e69a269bf9d15b62d520cb560d" title="Returns j-th non-zero value of row i (imaginary part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00718"></a>00718 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa2d9a2e69a269bf9d15b62d520cb560d" title="Returns j-th non-zero value of row i (imaginary part).">  ValueImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00719"></a>00719 <span class="keyword">  </span>{
<a name="l00720"></a>00720 
<a name="l00721"></a>00721 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00722"></a>00722 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a>))
<a name="l00723"></a>00723       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00724"></a>00724                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a>)-1)
<a name="l00725"></a>00725                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00726"></a>00726 
<a name="l00727"></a>00727     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>())
<a name="l00728"></a>00728       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00729"></a>00729                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00730"></a>00730                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00731"></a>00731 <span class="preprocessor">#endif</span>
<a name="l00732"></a>00732 <span class="preprocessor"></span>
<a name="l00733"></a>00733     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Value(j);
<a name="l00734"></a>00734   }
<a name="l00735"></a>00735 
<a name="l00736"></a>00736 
<a name="l00738"></a>00738 
<a name="l00743"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ab410f72a305938081fbc07e868eb161b">00743</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00744"></a>00744   <span class="keyword">inline</span> T&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa2d9a2e69a269bf9d15b62d520cb560d" title="Returns j-th non-zero value of row i (imaginary part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00745"></a>00745 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa2d9a2e69a269bf9d15b62d520cb560d" title="Returns j-th non-zero value of row i (imaginary part).">  ValueImag</a> (<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00746"></a>00746   {
<a name="l00747"></a>00747 
<a name="l00748"></a>00748 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00749"></a>00749 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a>))
<a name="l00750"></a>00750       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00751"></a>00751                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a>)-1)
<a name="l00752"></a>00752                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00753"></a>00753 
<a name="l00754"></a>00754     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>())
<a name="l00755"></a>00755       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00756"></a>00756                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00757"></a>00757                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00758"></a>00758 <span class="preprocessor">#endif</span>
<a name="l00759"></a>00759 <span class="preprocessor"></span>
<a name="l00760"></a>00760     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Value(j);
<a name="l00761"></a>00761   }
<a name="l00762"></a>00762 
<a name="l00763"></a>00763 
<a name="l00765"></a>00765 
<a name="l00770"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aedba64fa9b01bce6bb75e9e5a14b26c5">00770</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00771"></a>00771   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aedba64fa9b01bce6bb75e9e5a14b26c5" title="Returns column number of j-th non-zero value of row i (imaginary part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00772"></a>00772 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aedba64fa9b01bce6bb75e9e5a14b26c5" title="Returns column number of j-th non-zero value of row i (imaginary part).">  IndexImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00773"></a>00773 <span class="keyword">  </span>{
<a name="l00774"></a>00774 
<a name="l00775"></a>00775 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00776"></a>00776 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a>))
<a name="l00777"></a>00777       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00778"></a>00778                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a>)-1)
<a name="l00779"></a>00779                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00780"></a>00780 
<a name="l00781"></a>00781     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>())
<a name="l00782"></a>00782       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00783"></a>00783                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00784"></a>00784                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00785"></a>00785 <span class="preprocessor">#endif</span>
<a name="l00786"></a>00786 <span class="preprocessor"></span>
<a name="l00787"></a>00787     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Index(j);
<a name="l00788"></a>00788   }
<a name="l00789"></a>00789 
<a name="l00790"></a>00790 
<a name="l00792"></a>00792 
<a name="l00797"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a011b15c1909f712a693de10f2c0c2328">00797</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00798"></a>00798   <span class="keyword">inline</span> <span class="keywordtype">int</span>&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aedba64fa9b01bce6bb75e9e5a14b26c5" title="Returns column number of j-th non-zero value of row i (imaginary part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00799"></a>00799 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aedba64fa9b01bce6bb75e9e5a14b26c5" title="Returns column number of j-th non-zero value of row i (imaginary part).">  IndexImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00800"></a>00800   {
<a name="l00801"></a>00801 
<a name="l00802"></a>00802 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00803"></a>00803 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a>))
<a name="l00804"></a>00804       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArrayComplexSparse::index&quot;</span>,
<a name="l00805"></a>00805                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00806"></a>00806                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>, this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a>)-1)
<a name="l00807"></a>00807                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00808"></a>00808 
<a name="l00809"></a>00809     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>())
<a name="l00810"></a>00810       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00811"></a>00811                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00812"></a>00812                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00813"></a>00813 <span class="preprocessor">#endif</span>
<a name="l00814"></a>00814 <span class="preprocessor"></span>
<a name="l00815"></a>00815     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Index(j);
<a name="l00816"></a>00816   }
<a name="l00817"></a>00817 
<a name="l00818"></a>00818 
<a name="l00820"></a>00820 
<a name="l00826"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a1ebb3dcbf317ec99e6463f0aafea10b9">00826</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00827"></a>00827   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aab6c31240cd6dcbd442f9a29658f51ea" title="Redefines the real part of the matrix.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00828"></a>00828 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aab6c31240cd6dcbd442f9a29658f51ea" title="Redefines the real part of the matrix.">  SetRealData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> n, T* val, <span class="keywordtype">int</span>* ind)
<a name="l00829"></a>00829   {
<a name="l00830"></a>00830     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).SetData(n, val, ind);
<a name="l00831"></a>00831   }
<a name="l00832"></a>00832 
<a name="l00833"></a>00833 
<a name="l00835"></a>00835 
<a name="l00841"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a50622f29a8e112821c68e5a28236258f">00841</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00842"></a>00842   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a8e3c2dfa0ea5aa57cd8607cca15562f2" title="Redefines the imaginary part of the matrix.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00843"></a>00843 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a8e3c2dfa0ea5aa57cd8607cca15562f2" title="Redefines the imaginary part of the matrix.">  SetImagData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> n, T* val, <span class="keywordtype">int</span>* ind)
<a name="l00844"></a>00844   {
<a name="l00845"></a>00845     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).SetData(n, val, ind);
<a name="l00846"></a>00846   }
<a name="l00847"></a>00847 
<a name="l00848"></a>00848 
<a name="l00850"></a>00850 
<a name="l00854"></a>00854   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00855"></a>00855   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aed1192a75fe2c7d19e545ff6054298e2" title="Clears the matrix without releasing memory.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::NullifyReal</a>(<span class="keywordtype">int</span> i)
<a name="l00856"></a>00856   {
<a name="l00857"></a>00857     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Nullify();
<a name="l00858"></a>00858   }
<a name="l00859"></a>00859 
<a name="l00860"></a>00860 
<a name="l00862"></a>00862 
<a name="l00866"></a>00866   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00867"></a>00867   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#adbdb03e02fd8617a23188d6bc3155ec0" title="Clears the matrix without releasing memory.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::NullifyImag</a>(<span class="keywordtype">int</span> i)
<a name="l00868"></a>00868   {
<a name="l00869"></a>00869     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Nullify();
<a name="l00870"></a>00870   }
<a name="l00871"></a>00871 
<a name="l00872"></a>00872 
<a name="l00874"></a>00874 
<a name="l00879"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aab6c31240cd6dcbd442f9a29658f51ea">00879</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00880"></a>00880   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aab6c31240cd6dcbd442f9a29658f51ea" title="Redefines the real part of the matrix.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00881"></a>00881 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aab6c31240cd6dcbd442f9a29658f51ea" title="Redefines the real part of the matrix.">  SetRealData</a>(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n, <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php" title="Sparse vector class.">Vector&lt;T, VectSparse, Allocator&gt;</a>* val)
<a name="l00882"></a>00882   {
<a name="l00883"></a>00883     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a> = m;
<a name="l00884"></a>00884     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a> = n;
<a name="l00885"></a>00885     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>.SetData(Storage::GetFirst(m, n), val);
<a name="l00886"></a>00886   }
<a name="l00887"></a>00887 
<a name="l00888"></a>00888 
<a name="l00890"></a>00890 
<a name="l00895"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a8e3c2dfa0ea5aa57cd8607cca15562f2">00895</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00896"></a>00896   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a8e3c2dfa0ea5aa57cd8607cca15562f2" title="Redefines the imaginary part of the matrix.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00897"></a>00897 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a8e3c2dfa0ea5aa57cd8607cca15562f2" title="Redefines the imaginary part of the matrix.">  SetImagData</a>(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n, <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php" title="Sparse vector class.">Vector&lt;T, VectSparse, Allocator&gt;</a>* val)
<a name="l00898"></a>00898   {
<a name="l00899"></a>00899     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a> = m;
<a name="l00900"></a>00900     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a> = n;
<a name="l00901"></a>00901     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>.SetData(Storage::GetFirst(m, n), val);
<a name="l00902"></a>00902   }
<a name="l00903"></a>00903 
<a name="l00904"></a>00904 
<a name="l00906"></a>00906 
<a name="l00910"></a>00910   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00911"></a>00911   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aed1192a75fe2c7d19e545ff6054298e2" title="Clears the matrix without releasing memory.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::NullifyReal</a>()
<a name="l00912"></a>00912   {
<a name="l00913"></a>00913     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a> = 0;
<a name="l00914"></a>00914     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a> = 0;
<a name="l00915"></a>00915     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>.Nullify();
<a name="l00916"></a>00916   }
<a name="l00917"></a>00917 
<a name="l00918"></a>00918 
<a name="l00920"></a>00920 
<a name="l00924"></a>00924   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00925"></a>00925   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#adbdb03e02fd8617a23188d6bc3155ec0" title="Clears the matrix without releasing memory.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::NullifyImag</a>()
<a name="l00926"></a>00926   {
<a name="l00927"></a>00927     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a> = 0;
<a name="l00928"></a>00928     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a> = 0;
<a name="l00929"></a>00929     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>.Nullify();
<a name="l00930"></a>00930   }
<a name="l00931"></a>00931 
<a name="l00932"></a>00932 
<a name="l00933"></a>00933   <span class="comment">/************************</span>
<a name="l00934"></a>00934 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l00935"></a>00935 <span class="comment">   ************************/</span>
<a name="l00936"></a>00936 
<a name="l00937"></a>00937 
<a name="l00939"></a>00939 
<a name="l00944"></a>00944   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00945"></a>00945   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ac24abb2e047eb8b91728023074b3fb13" title="Displays the matrix on the standard output.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00946"></a>00946 <span class="keyword">  </span>{
<a name="l00947"></a>00947     <span class="keywordflow">if</span> (Storage::GetFirst(1, 0) == 1)
<a name="l00948"></a>00948       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>; i++)
<a name="l00949"></a>00949         {
<a name="l00950"></a>00950           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).GetM(); j++)
<a name="l00951"></a>00951             cout &lt;&lt; (i+1) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Index(j)+1
<a name="l00952"></a>00952                  &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Value(j) &lt;&lt; endl;
<a name="l00953"></a>00953 
<a name="l00954"></a>00954           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).GetM(); j++)
<a name="l00955"></a>00955             cout &lt;&lt; (i+1) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Index(j)+1
<a name="l00956"></a>00956                  &lt;&lt; <span class="stringliteral">&quot; (0, &quot;</span> &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Value(j) &lt;&lt; <span class="stringliteral">&quot;)&quot;</span>&lt;&lt;endl;
<a name="l00957"></a>00957         }
<a name="l00958"></a>00958     <span class="keywordflow">else</span>
<a name="l00959"></a>00959       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a>; i++)
<a name="l00960"></a>00960         {
<a name="l00961"></a>00961           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).GetM(); j++)
<a name="l00962"></a>00962             cout &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Index(j)+1 &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; i+1
<a name="l00963"></a>00963                  &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Value(j) &lt;&lt; endl;
<a name="l00964"></a>00964 
<a name="l00965"></a>00965           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).GetM(); j++)
<a name="l00966"></a>00966             cout &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Index(j)+1 &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; i+1
<a name="l00967"></a>00967                  &lt;&lt; <span class="stringliteral">&quot; (0, &quot;</span> &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Value(j) &lt;&lt; <span class="stringliteral">&quot;)&quot;</span>&lt;&lt;endl;
<a name="l00968"></a>00968         }
<a name="l00969"></a>00969   }
<a name="l00970"></a>00970 
<a name="l00971"></a>00971 
<a name="l00973"></a>00973 
<a name="l00980"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a9b54a14b64f67242fe9367df4142bd7f">00980</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00981"></a>00981   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a9b54a14b64f67242fe9367df4142bd7f" title="Writes the matrix in a file.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00982"></a>00982 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a9b54a14b64f67242fe9367df4142bd7f" title="Writes the matrix in a file.">  ::Write</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00983"></a>00983 <span class="keyword">  </span>{
<a name="l00984"></a>00984     ofstream FileStream;
<a name="l00985"></a>00985     FileStream.open(FileName.c_str());
<a name="l00986"></a>00986 
<a name="l00987"></a>00987 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00988"></a>00988 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00989"></a>00989     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00990"></a>00990       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArrayComplexSparse::Write(string FileName)&quot;</span>,
<a name="l00991"></a>00991                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00992"></a>00992 <span class="preprocessor">#endif</span>
<a name="l00993"></a>00993 <span class="preprocessor"></span>
<a name="l00994"></a>00994     this-&gt;Write(FileStream);
<a name="l00995"></a>00995 
<a name="l00996"></a>00996     FileStream.close();
<a name="l00997"></a>00997   }
<a name="l00998"></a>00998 
<a name="l00999"></a>00999 
<a name="l01001"></a>01001 
<a name="l01008"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0cc7af3e3d677e5cb6bd5f66d7965a6f">01008</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01009"></a>01009   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a9b54a14b64f67242fe9367df4142bd7f" title="Writes the matrix in a file.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01010"></a>01010 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a9b54a14b64f67242fe9367df4142bd7f" title="Writes the matrix in a file.">  ::Write</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l01011"></a>01011 <span class="keyword">  </span>{
<a name="l01012"></a>01012 
<a name="l01013"></a>01013 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01014"></a>01014 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01015"></a>01015     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01016"></a>01016       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArrayComplexSparse::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l01017"></a>01017                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l01018"></a>01018 <span class="preprocessor">#endif</span>
<a name="l01019"></a>01019 <span class="preprocessor"></span>
<a name="l01020"></a>01020     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l01021"></a>01021                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01022"></a>01022 
<a name="l01023"></a>01023     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;n_)),
<a name="l01024"></a>01024                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01025"></a>01025 
<a name="l01026"></a>01026     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; val_real_.GetM(); i++)
<a name="l01027"></a>01027       {
<a name="l01028"></a>01028         val_real_(i).Write(FileStream);
<a name="l01029"></a>01029         val_imag_(i).Write(FileStream);
<a name="l01030"></a>01030       }
<a name="l01031"></a>01031   }
<a name="l01032"></a>01032 
<a name="l01033"></a>01033 
<a name="l01035"></a>01035 
<a name="l01039"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7095c684ecdb9ef6e9fe9b84462e22ce">01039</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01040"></a>01040   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7095c684ecdb9ef6e9fe9b84462e22ce" title="Writes the matrix in a file.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01041"></a>01041 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7095c684ecdb9ef6e9fe9b84462e22ce" title="Writes the matrix in a file.">  ::WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l01042"></a>01042 <span class="keyword">  </span>{
<a name="l01043"></a>01043     ofstream FileStream; FileStream.precision(14);
<a name="l01044"></a>01044     FileStream.open(FileName.c_str());
<a name="l01045"></a>01045 
<a name="l01046"></a>01046 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01047"></a>01047 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01048"></a>01048     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01049"></a>01049       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArrayComplexSparse::WriteText(string FileName)&quot;</span>,
<a name="l01050"></a>01050                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01051"></a>01051 <span class="preprocessor">#endif</span>
<a name="l01052"></a>01052 <span class="preprocessor"></span>
<a name="l01053"></a>01053     this-&gt;WriteText(FileStream);
<a name="l01054"></a>01054 
<a name="l01055"></a>01055     FileStream.close();
<a name="l01056"></a>01056   }
<a name="l01057"></a>01057 
<a name="l01058"></a>01058 
<a name="l01060"></a>01060 
<a name="l01064"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a884e62c07aa29b59b259d232f7771c94">01064</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01065"></a>01065   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7095c684ecdb9ef6e9fe9b84462e22ce" title="Writes the matrix in a file.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01066"></a>01066 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7095c684ecdb9ef6e9fe9b84462e22ce" title="Writes the matrix in a file.">  ::WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l01067"></a>01067 <span class="keyword">  </span>{
<a name="l01068"></a>01068 
<a name="l01069"></a>01069 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01070"></a>01070 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01071"></a>01071     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01072"></a>01072       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArrayComplexSparse::&quot;</span>
<a name="l01073"></a>01073                     <span class="stringliteral">&quot;WriteText(ofstream&amp; FileStream)&quot;</span>,
<a name="l01074"></a>01074                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l01075"></a>01075 <span class="preprocessor">#endif</span>
<a name="l01076"></a>01076 <span class="preprocessor"></span>
<a name="l01077"></a>01077     <span class="comment">// Conversion to coordinate format (1-index convention).</span>
<a name="l01078"></a>01078     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> IndRow, IndCol; <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;complex&lt;T&gt;</a> &gt; Value;
<a name="l01079"></a>01079     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; leaf_class =
<a name="l01080"></a>01080       <span class="keyword">static_cast&lt;</span><span class="keyword">const </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; <span class="keyword">&gt;</span>(*this);
<a name="l01081"></a>01081 
<a name="l01082"></a>01082     <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(leaf_class, IndRow, IndCol,
<a name="l01083"></a>01083                                  Value, 1, <span class="keyword">true</span>);
<a name="l01084"></a>01084 
<a name="l01085"></a>01085     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; IndRow.GetM(); i++)
<a name="l01086"></a>01086       FileStream &lt;&lt; IndRow(i) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; IndCol(i) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; Value(i) &lt;&lt; <span class="charliteral">&#39;\n&#39;</span>;
<a name="l01087"></a>01087 
<a name="l01088"></a>01088     <span class="comment">// If the last element a_{m,n} does not exist, we add a zero.</span>
<a name="l01089"></a>01089     <span class="keywordtype">int</span> m = Storage::GetFirst(this-&gt;m_, this-&gt;n_);
<a name="l01090"></a>01090     <span class="keywordtype">int</span> n = Storage::GetSecond(this-&gt;m_, this-&gt;n_);
<a name="l01091"></a>01091     <span class="keywordtype">bool</span> presence_last_elt = <span class="keyword">false</span>;
<a name="l01092"></a>01092     <span class="keywordflow">if</span> (m &gt; 0 &amp;&amp; n &gt; 0)
<a name="l01093"></a>01093       {
<a name="l01094"></a>01094         <span class="keywordflow">if</span> (this-&gt;val_real_(m-1).GetM() &gt; 0)
<a name="l01095"></a>01095           {
<a name="l01096"></a>01096             <span class="keywordtype">int</span> p = this-&gt;val_real_(m-1).GetM();
<a name="l01097"></a>01097             <span class="keywordflow">if</span> (this-&gt;val_real_(m-1).Index(p-1) == n-1)
<a name="l01098"></a>01098               presence_last_elt = <span class="keyword">true</span>;
<a name="l01099"></a>01099           }
<a name="l01100"></a>01100 
<a name="l01101"></a>01101         <span class="keywordflow">if</span> (this-&gt;val_imag_(m-1).GetM() &gt; 0)
<a name="l01102"></a>01102           {
<a name="l01103"></a>01103             <span class="keywordtype">int</span> p = this-&gt;val_imag_(m-1).GetM();
<a name="l01104"></a>01104             <span class="keywordflow">if</span> (this-&gt;val_imag_(m-1).Index(p-1) == n-1)
<a name="l01105"></a>01105               presence_last_elt = <span class="keyword">true</span>;
<a name="l01106"></a>01106           }
<a name="l01107"></a>01107 
<a name="l01108"></a>01108         <span class="keywordflow">if</span> (!presence_last_elt)
<a name="l01109"></a>01109           {
<a name="l01110"></a>01110             complex&lt;T&gt; zero;
<a name="l01111"></a>01111             SetComplexZero(zero);
<a name="l01112"></a>01112             FileStream &lt;&lt; this-&gt;m_ &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; this-&gt;n_ &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; zero &lt;&lt; <span class="charliteral">&#39;\n&#39;</span>;
<a name="l01113"></a>01113           }
<a name="l01114"></a>01114       }
<a name="l01115"></a>01115   }
<a name="l01116"></a>01116 
<a name="l01117"></a>01117 
<a name="l01119"></a>01119 
<a name="l01123"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a58c2b7013db7765a01181f928833e530">01123</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01124"></a>01124   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a58c2b7013db7765a01181f928833e530" title="Reads the matrix from a file.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01125"></a>01125 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a58c2b7013db7765a01181f928833e530" title="Reads the matrix from a file.">  ::Read</a>(<span class="keywordtype">string</span> FileName)
<a name="l01126"></a>01126   {
<a name="l01127"></a>01127     ifstream FileStream;
<a name="l01128"></a>01128     FileStream.open(FileName.c_str());
<a name="l01129"></a>01129 
<a name="l01130"></a>01130 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01131"></a>01131 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01132"></a>01132     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01133"></a>01133       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArrayComplexSparse::Read(string FileName)&quot;</span>,
<a name="l01134"></a>01134                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01135"></a>01135 <span class="preprocessor">#endif</span>
<a name="l01136"></a>01136 <span class="preprocessor"></span>
<a name="l01137"></a>01137     this-&gt;Read(FileStream);
<a name="l01138"></a>01138 
<a name="l01139"></a>01139     FileStream.close();
<a name="l01140"></a>01140   }
<a name="l01141"></a>01141 
<a name="l01142"></a>01142 
<a name="l01144"></a>01144 
<a name="l01148"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a8743f281d37cc023cd3e628eba4644f0">01148</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01149"></a>01149   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a58c2b7013db7765a01181f928833e530" title="Reads the matrix from a file.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01150"></a>01150 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a58c2b7013db7765a01181f928833e530" title="Reads the matrix from a file.">  ::Read</a>(istream&amp; FileStream)
<a name="l01151"></a>01151   {
<a name="l01152"></a>01152 
<a name="l01153"></a>01153 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01154"></a>01154 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01155"></a>01155     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01156"></a>01156       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::Read(ofstream&amp; FileStream)&quot;</span>,
<a name="l01157"></a>01157                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l01158"></a>01158 <span class="preprocessor">#endif</span>
<a name="l01159"></a>01159 <span class="preprocessor"></span>
<a name="l01160"></a>01160     FileStream.read(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l01161"></a>01161                     <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01162"></a>01162 
<a name="l01163"></a>01163     FileStream.read(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;n_)),
<a name="l01164"></a>01164                     <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01165"></a>01165 
<a name="l01166"></a>01166     val_real_.Reallocate(Storage::GetFirst(this-&gt;m_, this-&gt;n_));
<a name="l01167"></a>01167     val_imag_.Reallocate(Storage::GetFirst(this-&gt;m_, this-&gt;n_));
<a name="l01168"></a>01168     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; val_real_.GetM(); i++)
<a name="l01169"></a>01169       {
<a name="l01170"></a>01170         val_real_(i).Read(FileStream);
<a name="l01171"></a>01171         val_imag_(i).Read(FileStream);
<a name="l01172"></a>01172       }
<a name="l01173"></a>01173 
<a name="l01174"></a>01174 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01175"></a>01175 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l01176"></a>01176     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01177"></a>01177       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::Read(istream&amp; FileStream)&quot;</span>,
<a name="l01178"></a>01178                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Input operation failed.&quot;</span>)
<a name="l01179"></a>01179                     + string(<span class="stringliteral">&quot; The input file may have been removed&quot;</span>)
<a name="l01180"></a>01180                     + <span class="stringliteral">&quot; or may not contain enough data.&quot;</span>);
<a name="l01181"></a>01181 <span class="preprocessor">#endif</span>
<a name="l01182"></a>01182 <span class="preprocessor"></span>
<a name="l01183"></a>01183   }
<a name="l01184"></a>01184 
<a name="l01185"></a>01185 
<a name="l01187"></a>01187 
<a name="l01191"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ab0d4c7f4d291599c0d9cb735dd7d9dc8">01191</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01192"></a>01192   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ab0d4c7f4d291599c0d9cb735dd7d9dc8" title="Reads the matrix from a file.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01193"></a>01193 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ab0d4c7f4d291599c0d9cb735dd7d9dc8" title="Reads the matrix from a file.">  ::ReadText</a>(<span class="keywordtype">string</span> FileName)
<a name="l01194"></a>01194   {
<a name="l01195"></a>01195     ifstream FileStream;
<a name="l01196"></a>01196     FileStream.open(FileName.c_str());
<a name="l01197"></a>01197 
<a name="l01198"></a>01198 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01199"></a>01199 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01200"></a>01200     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01201"></a>01201       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::ReadText(string FileName)&quot;</span>,
<a name="l01202"></a>01202                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01203"></a>01203 <span class="preprocessor">#endif</span>
<a name="l01204"></a>01204 <span class="preprocessor"></span>
<a name="l01205"></a>01205     this-&gt;ReadText(FileStream);
<a name="l01206"></a>01206 
<a name="l01207"></a>01207     FileStream.close();
<a name="l01208"></a>01208   }
<a name="l01209"></a>01209 
<a name="l01210"></a>01210 
<a name="l01212"></a>01212 
<a name="l01216"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a20de6511128d3ac679c885abcd5538d0">01216</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01217"></a>01217   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ab0d4c7f4d291599c0d9cb735dd7d9dc8" title="Reads the matrix from a file.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01218"></a>01218 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ab0d4c7f4d291599c0d9cb735dd7d9dc8" title="Reads the matrix from a file.">  ::ReadText</a>(istream&amp; FileStream)
<a name="l01219"></a>01219   {
<a name="l01220"></a>01220     <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; leaf_class =
<a name="l01221"></a>01221       <span class="keyword">static_cast&lt;</span><a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; <span class="keyword">&gt;</span>(*this);
<a name="l01222"></a>01222 
<a name="l01223"></a>01223     complex&lt;T&gt; zero; <span class="keywordtype">int</span> index = 1;
<a name="l01224"></a>01224     <a class="code" href="namespace_seldon.php#af8428b13721ad15b95a74ecf07d23668" title="Reading of matrix in coordinate format.">ReadCoordinateMatrix</a>(leaf_class, FileStream, zero, index);
<a name="l01225"></a>01225   }
<a name="l01226"></a>01226 
<a name="l01227"></a>01227 
<a name="l01229"></a>01229 
<a name="l01235"></a>01235   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01236"></a>01236   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a889d3baa1c4aa6a96b83ba8ca7b4cf4a" title="Assembles the matrix.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Assemble</a>()
<a name="l01237"></a>01237   {
<a name="l01238"></a>01238     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Storage::GetFirst(this-&gt;m_, this-&gt;n_); i++)
<a name="l01239"></a>01239       {
<a name="l01240"></a>01240         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Assemble();
<a name="l01241"></a>01241         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Assemble();
<a name="l01242"></a>01242       }
<a name="l01243"></a>01243   }
<a name="l01244"></a>01244 
<a name="l01245"></a>01245 
<a name="l01247"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a19b48691b03953dffd387ace5cfddd37">01247</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01248"></a>01248   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a19b48691b03953dffd387ace5cfddd37" title="Matrix is initialized to the identity matrix.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l01249"></a>01249 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a19b48691b03953dffd387ace5cfddd37" title="Matrix is initialized to the identity matrix.">  SetIdentity</a>()
<a name="l01250"></a>01250   {
<a name="l01251"></a>01251     this-&gt;n_ = this-&gt;m_;
<a name="l01252"></a>01252     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l01253"></a>01253       {
<a name="l01254"></a>01254         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Reallocate(1);
<a name="l01255"></a>01255         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Index(0) = i;
<a name="l01256"></a>01256         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Value(0) = T(1);
<a name="l01257"></a>01257       }
<a name="l01258"></a>01258   }
<a name="l01259"></a>01259 
<a name="l01260"></a>01260 
<a name="l01262"></a>01262   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01263"></a>01263   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa6f51cb3c7779c309578058fd34c5d25" title="Non-zero entries are set to 0 (but not removed).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Zero</a>()
<a name="l01264"></a>01264   {
<a name="l01265"></a>01265     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Storage::GetFirst(this-&gt;m_, this-&gt;n_); i++)
<a name="l01266"></a>01266       {
<a name="l01267"></a>01267         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Zero();
<a name="l01268"></a>01268         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Zero();
<a name="l01269"></a>01269       }
<a name="l01270"></a>01270   }
<a name="l01271"></a>01271 
<a name="l01272"></a>01272 
<a name="l01274"></a>01274   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01275"></a>01275   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a1fce8c713bc87ca4e06df819ced39554" title="Non-zero entries are filled with values 0, 1, 2, 3 ...">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Fill</a>()
<a name="l01276"></a>01276   {
<a name="l01277"></a>01277     <span class="keywordtype">int</span> value = 0;
<a name="l01278"></a>01278     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Storage::GetFirst(this-&gt;m_, this-&gt;n_); i++)
<a name="l01279"></a>01279       {
<a name="l01280"></a>01280         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).GetM(); j++)
<a name="l01281"></a>01281           <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Value(j) = value++;
<a name="l01282"></a>01282 
<a name="l01283"></a>01283         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).GetM(); j++)
<a name="l01284"></a>01284           <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Value(j) = value++;
<a name="l01285"></a>01285       }
<a name="l01286"></a>01286   }
<a name="l01287"></a>01287 
<a name="l01288"></a>01288 
<a name="l01290"></a>01290 
<a name="l01294"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a08a5f1de809bbb4565e90d954ca053f6">01294</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allo&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l01295"></a>01295   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a1fce8c713bc87ca4e06df819ced39554" title="Non-zero entries are filled with values 0, 1, 2, 3 ...">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allo&gt;::</a>
<a name="l01296"></a>01296 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a1fce8c713bc87ca4e06df819ced39554" title="Non-zero entries are filled with values 0, 1, 2, 3 ...">  Fill</a>(<span class="keyword">const</span> complex&lt;T0&gt;&amp; x)
<a name="l01297"></a>01297   {
<a name="l01298"></a>01298     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Storage::GetFirst(this-&gt;m_, this-&gt;n_); i++)
<a name="l01299"></a>01299       {
<a name="l01300"></a>01300         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Fill(real(x));
<a name="l01301"></a>01301         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Fill(imag(x));
<a name="l01302"></a>01302       }
<a name="l01303"></a>01303   }
<a name="l01304"></a>01304 
<a name="l01305"></a>01305 
<a name="l01307"></a>01307   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01308"></a>01308   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01309"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a95e0c06209ddfeea67740ecb9c0c1451">01309</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l01310"></a>01310   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::operator</a>=
<a name="l01311"></a>01311   (<span class="keyword">const</span> complex&lt;T0&gt;&amp; x)
<a name="l01312"></a>01312   {
<a name="l01313"></a>01313     this-&gt;Fill(x);
<a name="l01314"></a>01314   }
<a name="l01315"></a>01315 
<a name="l01316"></a>01316 
<a name="l01318"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad1ff9883f030ca9eaada43616865acdb">01318</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01319"></a>01319   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad1ff9883f030ca9eaada43616865acdb" title="Non-zero entries take a random value.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l01320"></a>01320 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad1ff9883f030ca9eaada43616865acdb" title="Non-zero entries take a random value.">  FillRand</a>()
<a name="l01321"></a>01321   {
<a name="l01322"></a>01322     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Storage::GetFirst(this-&gt;m_, this-&gt;n_); i++)
<a name="l01323"></a>01323       {
<a name="l01324"></a>01324         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).FillRand();
<a name="l01325"></a>01325         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).FillRand();
<a name="l01326"></a>01326       }
<a name="l01327"></a>01327   }
<a name="l01328"></a>01328 
<a name="l01329"></a>01329 
<a name="l01331"></a>01331   <span class="comment">// MATRIX&lt;ARRAY_COLCOMPLEXSPARSE&gt; //</span>
<a name="l01333"></a>01333 <span class="comment"></span>
<a name="l01334"></a>01334 
<a name="l01336"></a>01336 
<a name="l01339"></a>01339   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01340"></a>01340   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::Matrix</a>():
<a name="l01341"></a>01341     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_col_complex_sparse.php">ArrayColComplexSparse</a>, Allocator&gt;()
<a name="l01342"></a>01342   {
<a name="l01343"></a>01343   }
<a name="l01344"></a>01344 
<a name="l01345"></a>01345 
<a name="l01347"></a>01347 
<a name="l01352"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a08cf66f2bb613a0345fcaf8a1a5499f5">01352</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01353"></a>01353   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;</a>
<a name="l01354"></a>01354 <a class="code" href="class_seldon_1_1_matrix.php">  ::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) :
<a name="l01355"></a>01355     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_col_complex_sparse.php">ArrayColComplexSparse</a>, Allocator&gt;(i, j)
<a name="l01356"></a>01356   {
<a name="l01357"></a>01357   }
<a name="l01358"></a>01358 
<a name="l01359"></a>01359 
<a name="l01361"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a4962f5db5a03299da3789ff2c7ab653b">01361</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01362"></a>01362   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;</a>
<a name="l01363"></a>01363 <a class="code" href="class_seldon_1_1_matrix.php">  ::ClearRealColumn</a>(<span class="keywordtype">int</span> i)
<a name="l01364"></a>01364   {
<a name="l01365"></a>01365     this-&gt;val_real_(i).Clear();
<a name="l01366"></a>01366   }
<a name="l01367"></a>01367 
<a name="l01368"></a>01368 
<a name="l01370"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a5e616d97f5b568c8adb47ccc15540300">01370</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01371"></a>01371   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;</a>
<a name="l01372"></a>01372 <a class="code" href="class_seldon_1_1_matrix.php">  ::ClearImagColumn</a>(<span class="keywordtype">int</span> i)
<a name="l01373"></a>01373   {
<a name="l01374"></a>01374     this-&gt;val_imag_(i).Clear();
<a name="l01375"></a>01375   }
<a name="l01376"></a>01376 
<a name="l01377"></a>01377 
<a name="l01379"></a>01379 
<a name="l01383"></a>01383   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Alloc&gt; <span class="keyword">inline</span>
<a name="l01384"></a>01384   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Alloc&gt;</a>
<a name="l01385"></a>01385 <a class="code" href="class_seldon_1_1_matrix.php">  ::ReallocateRealColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01386"></a>01386   {
<a name="l01387"></a>01387     this-&gt;val_real_(i).Reallocate(j);
<a name="l01388"></a>01388   }
<a name="l01389"></a>01389 
<a name="l01390"></a>01390 
<a name="l01392"></a>01392 
<a name="l01396"></a>01396   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Alloc&gt; <span class="keyword">inline</span>
<a name="l01397"></a>01397   <span class="keywordtype">void</span> Matrix&lt;T, Prop, ArrayColComplexSparse, Alloc&gt;
<a name="l01398"></a>01398   ::ReallocateImagColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01399"></a>01399   {
<a name="l01400"></a>01400     this-&gt;val_imag_(i).Reallocate(j);
<a name="l01401"></a>01401   }
<a name="l01402"></a>01402 
<a name="l01403"></a>01403 
<a name="l01405"></a>01405 
<a name="l01409"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#ad7da5d2ec6445de44d3a5a81a0b92128">01409</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01410"></a>01410   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;</a>
<a name="l01411"></a>01411 <a class="code" href="class_seldon_1_1_matrix.php">  ::ResizeRealColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01412"></a>01412   {
<a name="l01413"></a>01413     this-&gt;val_real_(i).Resize(j);
<a name="l01414"></a>01414   }
<a name="l01415"></a>01415 
<a name="l01416"></a>01416 
<a name="l01418"></a>01418 
<a name="l01422"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#ac11baddc386e9569864bb21de1282eb0">01422</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01423"></a>01423   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;</a>
<a name="l01424"></a>01424 <a class="code" href="class_seldon_1_1_matrix.php">  ::ResizeImagColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01425"></a>01425   {
<a name="l01426"></a>01426     this-&gt;val_imag_(i).Resize(j);
<a name="l01427"></a>01427   }
<a name="l01428"></a>01428 
<a name="l01429"></a>01429 
<a name="l01431"></a>01431 
<a name="l01435"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a88a77017d1aedf2ecc8c371e475f0d15">01435</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01436"></a>01436   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;</a>
<a name="l01437"></a>01437 <a class="code" href="class_seldon_1_1_matrix.php">  ::SwapRealColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01438"></a>01438   {
<a name="l01439"></a>01439     Swap(this-&gt;val_real_(i), this-&gt;val_real_(j));
<a name="l01440"></a>01440   }
<a name="l01441"></a>01441 
<a name="l01442"></a>01442 
<a name="l01444"></a>01444 
<a name="l01448"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a77edc2353b0c31b171a3721d9c514dac">01448</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01449"></a>01449   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;</a>
<a name="l01450"></a>01450 <a class="code" href="class_seldon_1_1_matrix.php">  ::SwapImagColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01451"></a>01451   {
<a name="l01452"></a>01452     Swap(this-&gt;val_imag_(i), this-&gt;val_imag_(j));
<a name="l01453"></a>01453   }
<a name="l01454"></a>01454 
<a name="l01455"></a>01455 
<a name="l01457"></a>01457 
<a name="l01461"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a618e575e978846f1ff95f9e8e3fe74c0">01461</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01462"></a>01462   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::</a>
<a name="l01463"></a>01463 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceRealIndexColumn</a>(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l01464"></a>01464   {
<a name="l01465"></a>01465     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_real_(i).GetM(); j++)
<a name="l01466"></a>01466       this-&gt;val_real_(i).Index(j) = new_index(j);
<a name="l01467"></a>01467   }
<a name="l01468"></a>01468 
<a name="l01469"></a>01469 
<a name="l01471"></a>01471 
<a name="l01475"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a54da32085a107892efa1828ebb9b43c7">01475</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01476"></a>01476   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::</a>
<a name="l01477"></a>01477 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceImagIndexColumn</a>(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l01478"></a>01478   {
<a name="l01479"></a>01479     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_imag_(i).GetM(); j++)
<a name="l01480"></a>01480       this-&gt;val_imag_(i).Index(j) = new_index(j);
<a name="l01481"></a>01481   }
<a name="l01482"></a>01482 
<a name="l01483"></a>01483 
<a name="l01485"></a>01485 
<a name="l01489"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a790b5cd9f59aed77430dfbceb834a33e">01489</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01490"></a>01490   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::</a>
<a name="l01491"></a>01491 <a class="code" href="class_seldon_1_1_matrix.php">  GetRealColumnSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01492"></a>01492 <span class="keyword">  </span>{
<a name="l01493"></a>01493     <span class="keywordflow">return</span> this-&gt;val_real_(i).GetSize();
<a name="l01494"></a>01494   }
<a name="l01495"></a>01495 
<a name="l01496"></a>01496 
<a name="l01498"></a>01498 
<a name="l01502"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#ad6f24240d54e489ab302c91a10b62276">01502</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01503"></a>01503   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::</a>
<a name="l01504"></a>01504 <a class="code" href="class_seldon_1_1_matrix.php">  GetImagColumnSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01505"></a>01505 <span class="keyword">  </span>{
<a name="l01506"></a>01506     <span class="keywordflow">return</span> this-&gt;val_imag_(i).GetSize();
<a name="l01507"></a>01507   }
<a name="l01508"></a>01508 
<a name="l01509"></a>01509 
<a name="l01511"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#ab9fef30299f95e6c195194f1f0ac3106">01511</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01512"></a>01512   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;</a>
<a name="l01513"></a>01513 <a class="code" href="class_seldon_1_1_matrix.php">  ::PrintRealColumn</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01514"></a>01514 <span class="keyword">  </span>{
<a name="l01515"></a>01515     this-&gt;val_real_(i).Print();
<a name="l01516"></a>01516   }
<a name="l01517"></a>01517 
<a name="l01518"></a>01518 
<a name="l01520"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a1d651709170e5a488fb2f6a2cffa9ece">01520</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01521"></a>01521   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;</a>
<a name="l01522"></a>01522 <a class="code" href="class_seldon_1_1_matrix.php">  ::PrintImagColumn</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01523"></a>01523 <span class="keyword">  </span>{
<a name="l01524"></a>01524     this-&gt;val_imag_(i).Print();
<a name="l01525"></a>01525   }
<a name="l01526"></a>01526 
<a name="l01527"></a>01527 
<a name="l01529"></a>01529 
<a name="l01534"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#ad6e1965a8a55307f92757a12bbe9662d">01534</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01535"></a>01535   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;</a>
<a name="l01536"></a>01536 <a class="code" href="class_seldon_1_1_matrix.php">  ::AssembleRealColumn</a>(<span class="keywordtype">int</span> i)
<a name="l01537"></a>01537   {
<a name="l01538"></a>01538     this-&gt;val_real_(i).Assemble();
<a name="l01539"></a>01539   }
<a name="l01540"></a>01540 
<a name="l01541"></a>01541 
<a name="l01543"></a>01543 
<a name="l01548"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#ac9846afe391f9acb0129f9595d2cf3e2">01548</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01549"></a>01549   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;</a>
<a name="l01550"></a>01550 <a class="code" href="class_seldon_1_1_matrix.php">  ::AssembleImagColumn</a>(<span class="keywordtype">int</span> i)
<a name="l01551"></a>01551   {
<a name="l01552"></a>01552     this-&gt;val_imag_(i).Assemble();
<a name="l01553"></a>01553   }
<a name="l01554"></a>01554 
<a name="l01555"></a>01555 
<a name="l01557"></a>01557 
<a name="l01562"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a3e34290958f86479984c53107df922cf">01562</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01563"></a>01563   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::</a>
<a name="l01564"></a>01564 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; val)
<a name="l01565"></a>01565   {
<a name="l01566"></a>01566     <span class="keywordflow">if</span> (real(val) != T(0))
<a name="l01567"></a>01567       this-&gt;val_real_(j).AddInteraction(i, real(val));
<a name="l01568"></a>01568 
<a name="l01569"></a>01569     <span class="keywordflow">if</span> (imag(val) != T(0))
<a name="l01570"></a>01570       this-&gt;val_imag_(j).AddInteraction(i, imag(val));
<a name="l01571"></a>01571   }
<a name="l01572"></a>01572 
<a name="l01573"></a>01573 
<a name="l01575"></a>01575 
<a name="l01581"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a774a26e19d61dd4a9e7f95de5559368e">01581</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01582"></a>01582   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::</a>
<a name="l01583"></a>01583 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l01584"></a>01584                     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1&gt;&amp; val)
<a name="l01585"></a>01585   {
<a name="l01586"></a>01586     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nb; j++)
<a name="l01587"></a>01587       AddInteraction(i, col(j), val(j));
<a name="l01588"></a>01588   }
<a name="l01589"></a>01589 
<a name="l01590"></a>01590 
<a name="l01592"></a>01592 
<a name="l01598"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a6ecf0924c16cfb04913b70cd03fb7a19">01598</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01599"></a>01599   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::</a>
<a name="l01600"></a>01600 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l01601"></a>01601                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1&gt;&amp; val)
<a name="l01602"></a>01602   {
<a name="l01603"></a>01603     <span class="keywordtype">int</span> nb_real = 0;
<a name="l01604"></a>01604     <span class="keywordtype">int</span> nb_imag = 0;
<a name="l01605"></a>01605     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> row_real(nb), row_imag(nb);
<a name="l01606"></a>01606     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val_real(nb), val_imag(nb);
<a name="l01607"></a>01607     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nb; j++)
<a name="l01608"></a>01608       {
<a name="l01609"></a>01609         <span class="keywordflow">if</span> (real(val(j)) != T(0))
<a name="l01610"></a>01610           {
<a name="l01611"></a>01611             row_real(nb_real) = row(j);
<a name="l01612"></a>01612             val_real(nb_real) = real(val(j));
<a name="l01613"></a>01613             nb_real++;
<a name="l01614"></a>01614           }
<a name="l01615"></a>01615 
<a name="l01616"></a>01616         <span class="keywordflow">if</span> (imag(val(j)) != T(0))
<a name="l01617"></a>01617           {
<a name="l01618"></a>01618             row_imag(nb_imag) = row(j);
<a name="l01619"></a>01619             val_imag(nb_imag) = imag(val(j));
<a name="l01620"></a>01620             nb_imag++;
<a name="l01621"></a>01621           }
<a name="l01622"></a>01622       }
<a name="l01623"></a>01623 
<a name="l01624"></a>01624     this-&gt;val_real_(i).AddInteractionRow(nb_real, row_real, val_real);
<a name="l01625"></a>01625     this-&gt;val_imag_(i).AddInteractionRow(nb_imag, row_imag, val_imag);
<a name="l01626"></a>01626   }
<a name="l01627"></a>01627 
<a name="l01628"></a>01628 
<a name="l01630"></a>01630   <span class="comment">// MATRIX&lt;ARRAY_ROWCOMPLEXSPARSE&gt; //</span>
<a name="l01632"></a>01632 <span class="comment"></span>
<a name="l01633"></a>01633 
<a name="l01635"></a>01635 
<a name="l01638"></a>01638   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01639"></a>01639   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::Matrix</a>():
<a name="l01640"></a>01640     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>, Allocator&gt;()
<a name="l01641"></a>01641   {
<a name="l01642"></a>01642   }
<a name="l01643"></a>01643 
<a name="l01644"></a>01644 
<a name="l01646"></a>01646 
<a name="l01651"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a8bcc8c567a536ff51cba0d9afce34507">01651</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01652"></a>01652   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;</a>
<a name="l01653"></a>01653 <a class="code" href="class_seldon_1_1_matrix.php">  ::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01654"></a>01654     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>, Allocator&gt;(i, j)
<a name="l01655"></a>01655   {
<a name="l01656"></a>01656   }
<a name="l01657"></a>01657 
<a name="l01658"></a>01658 
<a name="l01660"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a480f759c9bdd3e69206f18d81ed44f5a">01660</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01661"></a>01661   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;</a>
<a name="l01662"></a>01662 <a class="code" href="class_seldon_1_1_matrix.php">  ::ClearRealRow</a>(<span class="keywordtype">int</span> i)
<a name="l01663"></a>01663   {
<a name="l01664"></a>01664     this-&gt;val_real_(i).Clear();
<a name="l01665"></a>01665   }
<a name="l01666"></a>01666 
<a name="l01667"></a>01667 
<a name="l01669"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a16289d6a8f38a4303f75113667f8af74">01669</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01670"></a>01670   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;</a>
<a name="l01671"></a>01671 <a class="code" href="class_seldon_1_1_matrix.php">  ::ClearImagRow</a>(<span class="keywordtype">int</span> i)
<a name="l01672"></a>01672   {
<a name="l01673"></a>01673     this-&gt;val_imag_(i).Clear();
<a name="l01674"></a>01674   }
<a name="l01675"></a>01675 
<a name="l01676"></a>01676 
<a name="l01678"></a>01678 
<a name="l01683"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#aefd387926aad7c3609a9f6bc062252e0">01683</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01684"></a>01684   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::</a>
<a name="l01685"></a>01685 <a class="code" href="class_seldon_1_1_matrix.php">  ReallocateRealRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01686"></a>01686   {
<a name="l01687"></a>01687     this-&gt;val_real_(i).Reallocate(j);
<a name="l01688"></a>01688   }
<a name="l01689"></a>01689 
<a name="l01690"></a>01690 
<a name="l01692"></a>01692 
<a name="l01697"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#aa798b00f5793c414777a5e3b421095a0">01697</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01698"></a>01698   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::</a>
<a name="l01699"></a>01699 <a class="code" href="class_seldon_1_1_matrix.php">  ReallocateImagRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01700"></a>01700   {
<a name="l01701"></a>01701     this-&gt;val_imag_(i).Reallocate(j);
<a name="l01702"></a>01702   }
<a name="l01703"></a>01703 
<a name="l01704"></a>01704 
<a name="l01706"></a>01706 
<a name="l01711"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#ac668aac85ed864892bd3f4209ced4cf2">01711</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01712"></a>01712   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;</a>
<a name="l01713"></a>01713 <a class="code" href="class_seldon_1_1_matrix.php">  ::ResizeRealRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01714"></a>01714   {
<a name="l01715"></a>01715     this-&gt;val_real_(i).Resize(j);
<a name="l01716"></a>01716   }
<a name="l01717"></a>01717 
<a name="l01718"></a>01718 
<a name="l01720"></a>01720 
<a name="l01725"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#af89faca49416df1ad0b4262d440adec9">01725</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01726"></a>01726   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;</a>
<a name="l01727"></a>01727 <a class="code" href="class_seldon_1_1_matrix.php">  ::ResizeImagRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01728"></a>01728   {
<a name="l01729"></a>01729     this-&gt;val_imag_(i).Resize(j);
<a name="l01730"></a>01730   }
<a name="l01731"></a>01731 
<a name="l01732"></a>01732 
<a name="l01734"></a>01734 
<a name="l01738"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a8ae984b95031fd72c6ecab86418d60ac">01738</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01739"></a>01739   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;</a>
<a name="l01740"></a>01740 <a class="code" href="class_seldon_1_1_matrix.php">  ::SwapRealRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01741"></a>01741   {
<a name="l01742"></a>01742     Swap(this-&gt;val_real_(i), this-&gt;val_real_(j));
<a name="l01743"></a>01743   }
<a name="l01744"></a>01744 
<a name="l01745"></a>01745 
<a name="l01747"></a>01747 
<a name="l01751"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a4b6b31fa7468c32bddf17fd1a22888ae">01751</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01752"></a>01752   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;</a>
<a name="l01753"></a>01753 <a class="code" href="class_seldon_1_1_matrix.php">  ::SwapImagRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01754"></a>01754   {
<a name="l01755"></a>01755     Swap(this-&gt;val_imag_(i), this-&gt;val_imag_(j));
<a name="l01756"></a>01756   }
<a name="l01757"></a>01757 
<a name="l01758"></a>01758 
<a name="l01760"></a>01760 
<a name="l01764"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a350b6c00041f9e9db6bf111ac431bdf9">01764</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01765"></a>01765   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::</a>
<a name="l01766"></a>01766 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceRealIndexRow</a>(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l01767"></a>01767   {
<a name="l01768"></a>01768     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_real_(i).GetM(); j++)
<a name="l01769"></a>01769       this-&gt;val_real_(i).Index(j) = new_index(j);
<a name="l01770"></a>01770   }
<a name="l01771"></a>01771 
<a name="l01772"></a>01772 
<a name="l01774"></a>01774 
<a name="l01778"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a3e6d41ebaccab9d346d6fd291e28ff5d">01778</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01779"></a>01779   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::</a>
<a name="l01780"></a>01780 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceImagIndexRow</a>(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l01781"></a>01781   {
<a name="l01782"></a>01782     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_imag_(i).GetM(); j++)
<a name="l01783"></a>01783       this-&gt;val_imag_(i).Index(j) = new_index(j);
<a name="l01784"></a>01784   }
<a name="l01785"></a>01785 
<a name="l01786"></a>01786 
<a name="l01788"></a>01788 
<a name="l01792"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a794411e243ead33b91cb750921602cdd">01792</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01793"></a>01793   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;</a>
<a name="l01794"></a>01794 <a class="code" href="class_seldon_1_1_matrix.php">  ::GetRealRowSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01795"></a>01795 <span class="keyword">  </span>{
<a name="l01796"></a>01796     <span class="keywordflow">return</span> this-&gt;val_real_(i).GetSize();
<a name="l01797"></a>01797   }
<a name="l01798"></a>01798 
<a name="l01799"></a>01799 
<a name="l01801"></a>01801 
<a name="l01805"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a5477a0086e76fbee8db94fcfa1cd9703">01805</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01806"></a>01806   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;</a>
<a name="l01807"></a>01807 <a class="code" href="class_seldon_1_1_matrix.php">  ::GetImagRowSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01808"></a>01808 <span class="keyword">  </span>{
<a name="l01809"></a>01809     <span class="keywordflow">return</span> this-&gt;val_imag_(i).GetSize();
<a name="l01810"></a>01810   }
<a name="l01811"></a>01811 
<a name="l01812"></a>01812 
<a name="l01814"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#aeeca29af2514e61e003925875e7f7c33">01814</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01815"></a>01815   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;</a>
<a name="l01816"></a>01816 <a class="code" href="class_seldon_1_1_matrix.php">  ::PrintRealRow</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01817"></a>01817 <span class="keyword">  </span>{
<a name="l01818"></a>01818     this-&gt;val_real_(i).Print();
<a name="l01819"></a>01819   }
<a name="l01820"></a>01820 
<a name="l01821"></a>01821 
<a name="l01823"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#aebbf5322f0709878e15c379391a8aec8">01823</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01824"></a>01824   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;</a>
<a name="l01825"></a>01825 <a class="code" href="class_seldon_1_1_matrix.php">  ::PrintImagRow</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01826"></a>01826 <span class="keyword">  </span>{
<a name="l01827"></a>01827     this-&gt;val_imag_(i).Print();
<a name="l01828"></a>01828   }
<a name="l01829"></a>01829 
<a name="l01830"></a>01830 
<a name="l01832"></a>01832 
<a name="l01837"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a70fa643e503c063be37de261f89aab22">01837</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01838"></a>01838   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;</a>
<a name="l01839"></a>01839 <a class="code" href="class_seldon_1_1_matrix.php">  ::AssembleRealRow</a>(<span class="keywordtype">int</span> i)
<a name="l01840"></a>01840   {
<a name="l01841"></a>01841     this-&gt;val_real_(i).Assemble();
<a name="l01842"></a>01842   }
<a name="l01843"></a>01843 
<a name="l01844"></a>01844 
<a name="l01846"></a>01846 
<a name="l01851"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a48ce7e0631521cc72c564be088917148">01851</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01852"></a>01852   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;</a>
<a name="l01853"></a>01853 <a class="code" href="class_seldon_1_1_matrix.php">  ::AssembleImagRow</a>(<span class="keywordtype">int</span> i)
<a name="l01854"></a>01854   {
<a name="l01855"></a>01855     this-&gt;val_imag_(i).Assemble();
<a name="l01856"></a>01856   }
<a name="l01857"></a>01857 
<a name="l01858"></a>01858 
<a name="l01860"></a>01860 
<a name="l01865"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#ab2d2137e557ad1acd76fed600f5c3584">01865</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01866"></a>01866   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::</a>
<a name="l01867"></a>01867 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; val)
<a name="l01868"></a>01868   {
<a name="l01869"></a>01869     <span class="keywordflow">if</span> (real(val) != T(0))
<a name="l01870"></a>01870       this-&gt;val_real_(i).AddInteraction(j, real(val));
<a name="l01871"></a>01871 
<a name="l01872"></a>01872     <span class="keywordflow">if</span> (imag(val) != T(0))
<a name="l01873"></a>01873       this-&gt;val_imag_(i).AddInteraction(j, imag(val));
<a name="l01874"></a>01874   }
<a name="l01875"></a>01875 
<a name="l01876"></a>01876 
<a name="l01878"></a>01878 
<a name="l01884"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a445de4f86d354e07251b8a9da3eff045">01884</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01885"></a>01885   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::</a>
<a name="l01886"></a>01886 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l01887"></a>01887                     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1&gt;&amp; val)
<a name="l01888"></a>01888   {
<a name="l01889"></a>01889     <span class="keywordflow">if</span> (nb &lt;= 0)
<a name="l01890"></a>01890       <span class="keywordflow">return</span>;
<a name="l01891"></a>01891 
<a name="l01892"></a>01892     <span class="keywordtype">int</span> nb_real = 0;
<a name="l01893"></a>01893     <span class="keywordtype">int</span> nb_imag = 0;
<a name="l01894"></a>01894     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> col_real(nb), col_imag(nb);
<a name="l01895"></a>01895     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val_real(nb), val_imag(nb);
<a name="l01896"></a>01896     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nb; j++)
<a name="l01897"></a>01897       {
<a name="l01898"></a>01898         <span class="keywordflow">if</span> (real(val(j)) != T(0))
<a name="l01899"></a>01899           {
<a name="l01900"></a>01900             col_real(nb_real) = col(j);
<a name="l01901"></a>01901             val_real(nb_real) = real(val(j));
<a name="l01902"></a>01902             nb_real++;
<a name="l01903"></a>01903           }
<a name="l01904"></a>01904 
<a name="l01905"></a>01905         <span class="keywordflow">if</span> (imag(val(j)) != T(0))
<a name="l01906"></a>01906           {
<a name="l01907"></a>01907             col_imag(nb_imag) = col(j);
<a name="l01908"></a>01908             val_imag(nb_imag) = imag(val(j));
<a name="l01909"></a>01909             nb_imag++;
<a name="l01910"></a>01910           }
<a name="l01911"></a>01911       }
<a name="l01912"></a>01912 
<a name="l01913"></a>01913     this-&gt;val_real_(i).AddInteractionRow(nb_real, col_real, val_real);
<a name="l01914"></a>01914     this-&gt;val_imag_(i).AddInteractionRow(nb_imag, col_imag, val_imag);
<a name="l01915"></a>01915   }
<a name="l01916"></a>01916 
<a name="l01917"></a>01917 
<a name="l01919"></a>01919 
<a name="l01925"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a3a9584691f4c544d683819e70202db20">01925</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01926"></a>01926   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::</a>
<a name="l01927"></a>01927 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l01928"></a>01928                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1&gt;&amp; val)
<a name="l01929"></a>01929   {
<a name="l01930"></a>01930     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nb; j++)
<a name="l01931"></a>01931       AddInteraction(row(j), i, val(j));
<a name="l01932"></a>01932   }
<a name="l01933"></a>01933 
<a name="l01934"></a>01934 
<a name="l01936"></a>01936   <span class="comment">// MATRIX&lt;ARRAY_COLSYMCOMPLEXSPARSE&gt; //</span>
<a name="l01938"></a>01938 <span class="comment"></span>
<a name="l01939"></a>01939 
<a name="l01941"></a>01941 
<a name="l01944"></a>01944   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01945"></a>01945   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::Matrix</a>():
<a name="l01946"></a>01946     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator&gt;()
<a name="l01947"></a>01947   {
<a name="l01948"></a>01948   }
<a name="l01949"></a>01949 
<a name="l01950"></a>01950 
<a name="l01952"></a>01952 
<a name="l01957"></a>01957   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01958"></a>01958   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ac0a88fcc183d15c9d86cfdde270a1d30" title="Default constructor.">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01959"></a>01959     Matrix_ArrayComplexSparse&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;(i, j)
<a name="l01960"></a>01960   {
<a name="l01961"></a>01961   }
<a name="l01962"></a>01962 
<a name="l01963"></a>01963 
<a name="l01964"></a>01964   <span class="comment">/**********************************</span>
<a name="l01965"></a>01965 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l01966"></a>01966 <span class="comment">   **********************************/</span>
<a name="l01967"></a>01967 
<a name="l01968"></a>01968 
<a name="l01970"></a>01970 
<a name="l01976"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ace196e6c26320571bf77a782833d7232">01976</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01977"></a>01977   <span class="keyword">inline</span> complex&lt;T&gt;
<a name="l01978"></a>01978   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"></span>
<a name="l01979"></a>01979 <span class="keyword">    const</span>
<a name="l01980"></a>01980 <span class="keyword">  </span>{
<a name="l01981"></a>01981 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01982"></a>01982 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01983"></a>01983       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01984"></a>01984                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01985"></a>01985                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01986"></a>01986     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01987"></a>01987       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01988"></a>01988                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01989"></a>01989                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01990"></a>01990 <span class="preprocessor">#endif</span>
<a name="l01991"></a>01991 <span class="preprocessor"></span>
<a name="l01992"></a>01992     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l01993"></a>01993       <span class="keywordflow">return</span> complex&lt;T&gt;(this-&gt;val_real_(j)(i), this-&gt;val_imag_(j)(i));
<a name="l01994"></a>01994 
<a name="l01995"></a>01995     <span class="keywordflow">return</span> complex&lt;T&gt;(this-&gt;val_real_(i)(j), this-&gt;val_imag_(i)(j));
<a name="l01996"></a>01996   }
<a name="l01997"></a>01997 
<a name="l01998"></a>01998 
<a name="l02000"></a>02000 
<a name="l02005"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a9edb81b7ae21d2118c6cc4d98058d731">02005</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02006"></a>02006   <span class="keyword">inline</span> T&amp;
<a name="l02007"></a>02007   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::ValReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l02008"></a>02008   {
<a name="l02009"></a>02009 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02010"></a>02010 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02011"></a>02011       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::ValReal&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02012"></a>02012                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02013"></a>02013                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02014"></a>02014     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02015"></a>02015       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::ValReal&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02016"></a>02016                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02017"></a>02017                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02018"></a>02018     <span class="keywordflow">if</span> (i &gt; j)
<a name="l02019"></a>02019       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix::ValReal()&quot;</span>,
<a name="l02020"></a>02020                           <span class="keywordtype">string</span>(<span class="stringliteral">&quot;With this function, you &quot;</span>)
<a name="l02021"></a>02021                           + <span class="stringliteral">&quot;can only access upper part of matrix.&quot;</span>);
<a name="l02022"></a>02022 <span class="preprocessor">#endif</span>
<a name="l02023"></a>02023 <span class="preprocessor"></span>
<a name="l02024"></a>02024     <span class="keywordflow">return</span> this-&gt;val_real_(j).Val(i);
<a name="l02025"></a>02025   }
<a name="l02026"></a>02026 
<a name="l02027"></a>02027 
<a name="l02029"></a>02029 
<a name="l02035"></a>02035   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02036"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a69b5391fd7e911f3f3204bf627e1ed24">02036</a>   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp;
<a name="l02037"></a>02037   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;</a>
<a name="l02038"></a>02038 <a class="code" href="class_seldon_1_1_matrix.php">  ::ValReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l02039"></a>02039 <span class="keyword">  </span>{
<a name="l02040"></a>02040 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02041"></a>02041 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02042"></a>02042       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::ValReal&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02043"></a>02043                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02044"></a>02044                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02045"></a>02045     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02046"></a>02046       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::ValReal&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02047"></a>02047                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02048"></a>02048                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02049"></a>02049     <span class="keywordflow">if</span> (i &gt; j)
<a name="l02050"></a>02050       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix::ValReal()&quot;</span>,
<a name="l02051"></a>02051                           <span class="keywordtype">string</span>(<span class="stringliteral">&quot;With this function, you &quot;</span>)
<a name="l02052"></a>02052                           + <span class="stringliteral">&quot;can only access upper part of matrix.&quot;</span>);
<a name="l02053"></a>02053 <span class="preprocessor">#endif</span>
<a name="l02054"></a>02054 <span class="preprocessor"></span>
<a name="l02055"></a>02055     <span class="keywordflow">return</span> this-&gt;val_real_(j).Val(i);
<a name="l02056"></a>02056   }
<a name="l02057"></a>02057 
<a name="l02058"></a>02058 
<a name="l02060"></a>02060 
<a name="l02066"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a9a8f3daf0ad036985c22c4772b1477aa">02066</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02067"></a>02067   <span class="keyword">inline</span> T&amp;
<a name="l02068"></a>02068   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::GetReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l02069"></a>02069   {
<a name="l02070"></a>02070 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02071"></a>02071 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02072"></a>02072       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::GetReal&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02073"></a>02073                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02074"></a>02074                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02075"></a>02075     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02076"></a>02076       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::GetReal&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02077"></a>02077                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02078"></a>02078                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02079"></a>02079 <span class="preprocessor">#endif</span>
<a name="l02080"></a>02080 <span class="preprocessor"></span>
<a name="l02081"></a>02081     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l02082"></a>02082       <span class="keywordflow">return</span> this-&gt;val_real_(j).Get(i);
<a name="l02083"></a>02083 
<a name="l02084"></a>02084     <span class="keywordflow">return</span> this-&gt;val_real_(i).Get(j);
<a name="l02085"></a>02085   }
<a name="l02086"></a>02086 
<a name="l02087"></a>02087 
<a name="l02089"></a>02089 
<a name="l02095"></a>02095   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02096"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a20616f82c194e42a610e3e48e63d5a40">02096</a>   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp;
<a name="l02097"></a>02097   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;</a>
<a name="l02098"></a>02098 <a class="code" href="class_seldon_1_1_matrix.php">  ::GetReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l02099"></a>02099 <span class="keyword">  </span>{
<a name="l02100"></a>02100 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02101"></a>02101 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02102"></a>02102       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::GetReal&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02103"></a>02103                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02104"></a>02104                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02105"></a>02105     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02106"></a>02106       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::GetReal&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02107"></a>02107                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02108"></a>02108                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02109"></a>02109 <span class="preprocessor">#endif</span>
<a name="l02110"></a>02110 <span class="preprocessor"></span>
<a name="l02111"></a>02111     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l02112"></a>02112       <span class="keywordflow">return</span> this-&gt;val_real_(j).Get(i);
<a name="l02113"></a>02113 
<a name="l02114"></a>02114     <span class="keywordflow">return</span> this-&gt;val_real_(i).Get(j);
<a name="l02115"></a>02115   }
<a name="l02116"></a>02116 
<a name="l02117"></a>02117 
<a name="l02119"></a>02119 
<a name="l02125"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a1b6113bbca815576f929be337af47483">02125</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02126"></a>02126   <span class="keyword">inline</span> T&amp;
<a name="l02127"></a>02127   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::ValImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l02128"></a>02128   {
<a name="l02129"></a>02129 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02130"></a>02130 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02131"></a>02131       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::ValImag&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02132"></a>02132                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02133"></a>02133                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02134"></a>02134     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02135"></a>02135       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::ValImag&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02136"></a>02136                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02137"></a>02137                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02138"></a>02138     <span class="keywordflow">if</span> (i &gt; j)
<a name="l02139"></a>02139       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix::ValImag()&quot;</span>,
<a name="l02140"></a>02140                           <span class="keywordtype">string</span>(<span class="stringliteral">&quot;With this function, you &quot;</span>)
<a name="l02141"></a>02141                           + <span class="stringliteral">&quot;can only access upper part of matrix.&quot;</span>);
<a name="l02142"></a>02142 <span class="preprocessor">#endif</span>
<a name="l02143"></a>02143 <span class="preprocessor"></span>
<a name="l02144"></a>02144     <span class="keywordflow">return</span> this-&gt;val_imag_(j).Val(i);
<a name="l02145"></a>02145   }
<a name="l02146"></a>02146 
<a name="l02147"></a>02147 
<a name="l02149"></a>02149 
<a name="l02154"></a>02154   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02155"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a113a4f9a03767b54cb38c34af0d03ff7">02155</a>   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp;
<a name="l02156"></a>02156   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;</a>
<a name="l02157"></a>02157 <a class="code" href="class_seldon_1_1_matrix.php">  ::ValImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l02158"></a>02158 <span class="keyword">  </span>{
<a name="l02159"></a>02159 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02160"></a>02160 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02161"></a>02161       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::ValImag&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02162"></a>02162                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02163"></a>02163                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02164"></a>02164     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02165"></a>02165       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::ValImag&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02166"></a>02166                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02167"></a>02167                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02168"></a>02168     <span class="keywordflow">if</span> (i &gt; j)
<a name="l02169"></a>02169       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix::ValImag()&quot;</span>,
<a name="l02170"></a>02170                           <span class="keywordtype">string</span>(<span class="stringliteral">&quot;With this function, you &quot;</span>)
<a name="l02171"></a>02171                           + <span class="stringliteral">&quot;can only access upper part of matrix.&quot;</span>);
<a name="l02172"></a>02172 <span class="preprocessor">#endif</span>
<a name="l02173"></a>02173 <span class="preprocessor"></span>
<a name="l02174"></a>02174     <span class="keywordflow">return</span> this-&gt;val_imag_(j).Val(i);
<a name="l02175"></a>02175   }
<a name="l02176"></a>02176 
<a name="l02177"></a>02177 
<a name="l02179"></a>02179 
<a name="l02184"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a913307297605f734e98ba7c7e870ae39">02184</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02185"></a>02185   <span class="keyword">inline</span> T&amp;
<a name="l02186"></a>02186   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::GetImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l02187"></a>02187   {
<a name="l02188"></a>02188 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02189"></a>02189 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02190"></a>02190       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::GetImag&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02191"></a>02191                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02192"></a>02192                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02193"></a>02193     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02194"></a>02194       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::GetImag&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02195"></a>02195                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02196"></a>02196                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02197"></a>02197 <span class="preprocessor">#endif</span>
<a name="l02198"></a>02198 <span class="preprocessor"></span>
<a name="l02199"></a>02199     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l02200"></a>02200       <span class="keywordflow">return</span> this-&gt;val_imag_(j).Get(i);
<a name="l02201"></a>02201 
<a name="l02202"></a>02202     <span class="keywordflow">return</span> this-&gt;val_imag_(i).Get(j);
<a name="l02203"></a>02203   }
<a name="l02204"></a>02204 
<a name="l02205"></a>02205 
<a name="l02207"></a>02207 
<a name="l02212"></a>02212   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02213"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#af4a356b6428d68d9890b96494ef89cc0">02213</a>   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp;
<a name="l02214"></a>02214   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;</a>
<a name="l02215"></a>02215 <a class="code" href="class_seldon_1_1_matrix.php">  ::GetImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l02216"></a>02216 <span class="keyword">  </span>{
<a name="l02217"></a>02217 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02218"></a>02218 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02219"></a>02219       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::GetImag&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02220"></a>02220                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02221"></a>02221                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02222"></a>02222     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02223"></a>02223       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::GetImag&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02224"></a>02224                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02225"></a>02225                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02226"></a>02226 <span class="preprocessor">#endif</span>
<a name="l02227"></a>02227 <span class="preprocessor"></span>
<a name="l02228"></a>02228     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l02229"></a>02229       <span class="keywordflow">return</span> this-&gt;val_imag_(j).Val(i);
<a name="l02230"></a>02230 
<a name="l02231"></a>02231     <span class="keywordflow">return</span> this-&gt;val_imag_(i).Val(j);
<a name="l02232"></a>02232   }
<a name="l02233"></a>02233 
<a name="l02234"></a>02234 
<a name="l02236"></a>02236 
<a name="l02241"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a6f67461ccf260c8bb9e9fd1fc40811e7">02241</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02242"></a>02242   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;</a>
<a name="l02243"></a>02243 <a class="code" href="class_seldon_1_1_matrix.php">  ::Set</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; x)
<a name="l02244"></a>02244   {
<a name="l02245"></a>02245     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l02246"></a>02246       {
<a name="l02247"></a>02247         <span class="keywordflow">if</span> (real(x) != T(0))
<a name="l02248"></a>02248           this-&gt;val_real_(j).Get(i) = real(x);
<a name="l02249"></a>02249         <span class="keywordflow">else</span>
<a name="l02250"></a>02250           {
<a name="l02251"></a>02251             <span class="keywordflow">if</span> (this-&gt;val_real_(j)(i) != T(0))
<a name="l02252"></a>02252               this-&gt;val_real_(j).Get(i) = T(0);
<a name="l02253"></a>02253           }
<a name="l02254"></a>02254 
<a name="l02255"></a>02255         <span class="keywordflow">if</span> (imag(x) != T(0))
<a name="l02256"></a>02256           this-&gt;val_imag_(j).Get(i) = imag(x);
<a name="l02257"></a>02257         <span class="keywordflow">else</span>
<a name="l02258"></a>02258           {
<a name="l02259"></a>02259             <span class="keywordflow">if</span> (this-&gt;val_imag_(j)(i) != T(0))
<a name="l02260"></a>02260               this-&gt;val_imag_(j).Get(i) = T(0);
<a name="l02261"></a>02261           }
<a name="l02262"></a>02262       }
<a name="l02263"></a>02263     <span class="keywordflow">else</span>
<a name="l02264"></a>02264       {
<a name="l02265"></a>02265         <span class="keywordflow">if</span> (real(x) != T(0))
<a name="l02266"></a>02266           this-&gt;val_real_(i).Get(j) = real(x);
<a name="l02267"></a>02267         <span class="keywordflow">else</span>
<a name="l02268"></a>02268           {
<a name="l02269"></a>02269             <span class="keywordflow">if</span> (this-&gt;val_real_(i)(j) != T(0))
<a name="l02270"></a>02270               this-&gt;val_real_(i).Get(j) = T(0);
<a name="l02271"></a>02271           }
<a name="l02272"></a>02272 
<a name="l02273"></a>02273         <span class="keywordflow">if</span> (imag(x) != T(0))
<a name="l02274"></a>02274           this-&gt;val_imag_(i).Get(j) = imag(x);
<a name="l02275"></a>02275         <span class="keywordflow">else</span>
<a name="l02276"></a>02276           {
<a name="l02277"></a>02277             <span class="keywordflow">if</span> (this-&gt;val_imag_(i)(j) != T(0))
<a name="l02278"></a>02278               this-&gt;val_imag_(i).Get(j) = T(0);
<a name="l02279"></a>02279           }
<a name="l02280"></a>02280       }
<a name="l02281"></a>02281   }
<a name="l02282"></a>02282 
<a name="l02283"></a>02283 
<a name="l02285"></a>02285   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l02286"></a>02286   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::ClearRealColumn</a>(<span class="keywordtype">int</span> i)
<a name="l02287"></a>02287   {
<a name="l02288"></a>02288     this-&gt;val_real_(i).Clear();
<a name="l02289"></a>02289   }
<a name="l02290"></a>02290 
<a name="l02291"></a>02291 
<a name="l02293"></a>02293   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l02294"></a>02294   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#addf758fad26c1595b8398590e138fa2f" title="Clears a column.">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::ClearImagColumn</a>(<span class="keywordtype">int</span> i)
<a name="l02295"></a>02295   {
<a name="l02296"></a>02296     this-&gt;val_imag_(i).Clear();
<a name="l02297"></a>02297   }
<a name="l02298"></a>02298 
<a name="l02299"></a>02299 
<a name="l02301"></a>02301 
<a name="l02306"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a063f733a818719a8349d0de407a685a0">02306</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02307"></a>02307   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l02308"></a>02308 <a class="code" href="class_seldon_1_1_matrix.php">  ReallocateRealColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l02309"></a>02309   {
<a name="l02310"></a>02310     this-&gt;val_real_(i).Reallocate(j);
<a name="l02311"></a>02311   }
<a name="l02312"></a>02312 
<a name="l02313"></a>02313 
<a name="l02315"></a>02315 
<a name="l02320"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a460d1b0cadc5cf4915ce024be57b1e58">02320</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02321"></a>02321   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l02322"></a>02322 <a class="code" href="class_seldon_1_1_matrix.php">  ReallocateImagColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l02323"></a>02323   {
<a name="l02324"></a>02324     this-&gt;val_imag_(i).Reallocate(j);
<a name="l02325"></a>02325   }
<a name="l02326"></a>02326 
<a name="l02327"></a>02327 
<a name="l02329"></a>02329 
<a name="l02333"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ab21dd3904c7fb5d4673106b83b5099d4">02333</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02334"></a>02334   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l02335"></a>02335 <a class="code" href="class_seldon_1_1_matrix.php">  ResizeRealColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l02336"></a>02336   {
<a name="l02337"></a>02337     this-&gt;val_real_(i).Resize(j);
<a name="l02338"></a>02338   }
<a name="l02339"></a>02339 
<a name="l02340"></a>02340 
<a name="l02342"></a>02342 
<a name="l02346"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a8f954de1b994af747e4e4e6c3bc2d8b9">02346</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02347"></a>02347   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l02348"></a>02348 <a class="code" href="class_seldon_1_1_matrix.php">  ResizeImagColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l02349"></a>02349   {
<a name="l02350"></a>02350     this-&gt;val_imag_(i).Resize(j);
<a name="l02351"></a>02351   }
<a name="l02352"></a>02352 
<a name="l02353"></a>02353 
<a name="l02355"></a>02355 
<a name="l02359"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#af2f1c1dc9a20bfed30e186f8d4c7dc44">02359</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02360"></a>02360   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l02361"></a>02361 <a class="code" href="class_seldon_1_1_matrix.php">  SwapRealColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l02362"></a>02362   {
<a name="l02363"></a>02363     Swap(this-&gt;val_real_(i), this-&gt;val_real_(j));
<a name="l02364"></a>02364   }
<a name="l02365"></a>02365 
<a name="l02366"></a>02366 
<a name="l02368"></a>02368 
<a name="l02372"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a7b4cf92d8be151c14508ada2b6b0b72d">02372</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02373"></a>02373   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l02374"></a>02374 <a class="code" href="class_seldon_1_1_matrix.php">  SwapImagColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l02375"></a>02375   {
<a name="l02376"></a>02376     Swap(this-&gt;val_imag_(i), this-&gt;val_imag_(j));
<a name="l02377"></a>02377   }
<a name="l02378"></a>02378 
<a name="l02379"></a>02379 
<a name="l02381"></a>02381 
<a name="l02385"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#addca588562b05576d4b37711bb4f6820">02385</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02386"></a>02386   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l02387"></a>02387 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceRealIndexColumn</a>(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l02388"></a>02388   {
<a name="l02389"></a>02389     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_real_(i).GetM(); j++)
<a name="l02390"></a>02390       this-&gt;val_real_(i).Index(j) = new_index(j);
<a name="l02391"></a>02391   }
<a name="l02392"></a>02392 
<a name="l02393"></a>02393 
<a name="l02395"></a>02395 
<a name="l02399"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a832f6f65f5c46c0cfaa66c0cb76e922e">02399</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02400"></a>02400   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l02401"></a>02401 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceImagIndexColumn</a>(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l02402"></a>02402   {
<a name="l02403"></a>02403     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_imag_(i).GetM(); j++)
<a name="l02404"></a>02404       this-&gt;val_imag_(i).Index(j) = new_index(j);
<a name="l02405"></a>02405   }
<a name="l02406"></a>02406 
<a name="l02407"></a>02407 
<a name="l02409"></a>02409 
<a name="l02413"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ab488dd8cd29a05f016d63b30e90b4f2d">02413</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02414"></a>02414   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l02415"></a>02415 <a class="code" href="class_seldon_1_1_matrix.php">  GetRealColumnSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l02416"></a>02416 <span class="keyword">  </span>{
<a name="l02417"></a>02417     <span class="keywordflow">return</span> this-&gt;val_real_(i).GetSize();
<a name="l02418"></a>02418   }
<a name="l02419"></a>02419 
<a name="l02420"></a>02420 
<a name="l02422"></a>02422 
<a name="l02426"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a6cebdd4865f5c2c9e90d5b4f6961414a">02426</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02427"></a>02427   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l02428"></a>02428 <a class="code" href="class_seldon_1_1_matrix.php">  GetImagColumnSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l02429"></a>02429 <span class="keyword">  </span>{
<a name="l02430"></a>02430     <span class="keywordflow">return</span> this-&gt;val_imag_(i).GetSize();
<a name="l02431"></a>02431   }
<a name="l02432"></a>02432 
<a name="l02433"></a>02433 
<a name="l02435"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ad080f5381fb2a720b7ecb72b245db852">02435</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02436"></a>02436   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l02437"></a>02437 <a class="code" href="class_seldon_1_1_matrix.php">  PrintRealColumn</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l02438"></a>02438 <span class="keyword">  </span>{
<a name="l02439"></a>02439     this-&gt;val_real_(i).Print();
<a name="l02440"></a>02440   }
<a name="l02441"></a>02441 
<a name="l02442"></a>02442 
<a name="l02444"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#afa56428c069675df01f46fd68d39be47">02444</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02445"></a>02445   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l02446"></a>02446 <a class="code" href="class_seldon_1_1_matrix.php">  PrintImagColumn</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l02447"></a>02447 <span class="keyword">  </span>{
<a name="l02448"></a>02448     this-&gt;val_imag_(i).Print();
<a name="l02449"></a>02449   }
<a name="l02450"></a>02450 
<a name="l02451"></a>02451 
<a name="l02453"></a>02453 
<a name="l02458"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a2ddc33ea923bccadd9b07a7f6b0a8690">02458</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02459"></a>02459   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l02460"></a>02460 <a class="code" href="class_seldon_1_1_matrix.php">  AssembleRealColumn</a>(<span class="keywordtype">int</span> i)
<a name="l02461"></a>02461   {
<a name="l02462"></a>02462     this-&gt;val_real_(i).Assemble();
<a name="l02463"></a>02463   }
<a name="l02464"></a>02464 
<a name="l02465"></a>02465 
<a name="l02467"></a>02467 
<a name="l02472"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a1285786e064b8336ba57b6c77097e4d5">02472</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02473"></a>02473   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l02474"></a>02474 <a class="code" href="class_seldon_1_1_matrix.php">  AssembleImagColumn</a>(<span class="keywordtype">int</span> i)
<a name="l02475"></a>02475   {
<a name="l02476"></a>02476     this-&gt;val_imag_(i).Assemble();
<a name="l02477"></a>02477   }
<a name="l02478"></a>02478 
<a name="l02479"></a>02479 
<a name="l02481"></a>02481 
<a name="l02486"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a96c7b8d7f0e1e862558b9e3039a32878">02486</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02487"></a>02487   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l02488"></a>02488 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; val)
<a name="l02489"></a>02489   {
<a name="l02490"></a>02490     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l02491"></a>02491       {
<a name="l02492"></a>02492         <span class="keywordflow">if</span> (real(val) != T(0))
<a name="l02493"></a>02493           this-&gt;val_real_(j).AddInteraction(i, real(val));
<a name="l02494"></a>02494 
<a name="l02495"></a>02495         <span class="keywordflow">if</span> (imag(val) != T(0))
<a name="l02496"></a>02496           this-&gt;val_imag_(j).AddInteraction(i, imag(val));
<a name="l02497"></a>02497       }
<a name="l02498"></a>02498   }
<a name="l02499"></a>02499 
<a name="l02500"></a>02500 
<a name="l02502"></a>02502 
<a name="l02508"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a6dcf362a6feaf5467c8c64d6be823fda">02508</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l02509"></a>02509   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l02510"></a>02510 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l02511"></a>02511                     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1&gt;&amp; val)
<a name="l02512"></a>02512   {
<a name="l02513"></a>02513     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nb; j++)
<a name="l02514"></a>02514       AddInteraction(i, col(j), val(j));
<a name="l02515"></a>02515   }
<a name="l02516"></a>02516 
<a name="l02517"></a>02517 
<a name="l02519"></a>02519 
<a name="l02525"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a703b16f79696af7a7d18da3c1db72671">02525</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l02526"></a>02526   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l02527"></a>02527 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l02528"></a>02528                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1&gt;&amp; val)
<a name="l02529"></a>02529   {
<a name="l02530"></a>02530     <span class="keywordtype">int</span> nb_real = 0;
<a name="l02531"></a>02531     <span class="keywordtype">int</span> nb_imag = 0;
<a name="l02532"></a>02532     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> row_real(nb), row_imag(nb);
<a name="l02533"></a>02533     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val_real(nb), val_imag(nb);
<a name="l02534"></a>02534     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nb; j++)
<a name="l02535"></a>02535       <span class="keywordflow">if</span> (row(j) &lt;= i)
<a name="l02536"></a>02536         {
<a name="l02537"></a>02537           <span class="keywordflow">if</span> (real(val(j)) != T(0))
<a name="l02538"></a>02538             {
<a name="l02539"></a>02539               row_real(nb_real) = row(j);
<a name="l02540"></a>02540               val_real(nb_real) = real(val(j));
<a name="l02541"></a>02541               nb_real++;
<a name="l02542"></a>02542             }
<a name="l02543"></a>02543 
<a name="l02544"></a>02544           <span class="keywordflow">if</span> (imag(val(j)) != T(0))
<a name="l02545"></a>02545             {
<a name="l02546"></a>02546               row_imag(nb_imag) = row(j);
<a name="l02547"></a>02547               val_imag(nb_imag) = imag(val(j));
<a name="l02548"></a>02548               nb_imag++;
<a name="l02549"></a>02549             }
<a name="l02550"></a>02550         }
<a name="l02551"></a>02551 
<a name="l02552"></a>02552     this-&gt;val_real_(i).AddInteractionRow(nb_real, row_real, val_real);
<a name="l02553"></a>02553     this-&gt;val_imag_(i).AddInteractionRow(nb_imag, row_imag, val_imag);
<a name="l02554"></a>02554   }
<a name="l02555"></a>02555 
<a name="l02556"></a>02556 
<a name="l02558"></a>02558   <span class="comment">// MATRIX&lt;ARRAY_ROWSYMCOMPLEXSPARSE&gt; //</span>
<a name="l02560"></a>02560 <span class="comment"></span>
<a name="l02561"></a>02561 
<a name="l02563"></a>02563 
<a name="l02566"></a>02566   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02567"></a>02567   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::Matrix</a>():
<a name="l02568"></a>02568     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator&gt;()
<a name="l02569"></a>02569   {
<a name="l02570"></a>02570   }
<a name="l02571"></a>02571 
<a name="l02572"></a>02572 
<a name="l02574"></a>02574 
<a name="l02579"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a2a8c65edac40bcf4ba7ecb3b5db27502">02579</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02580"></a>02580   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;</a>
<a name="l02581"></a>02581 <a class="code" href="class_seldon_1_1_matrix.php">  ::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l02582"></a>02582     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator&gt;(i, j)
<a name="l02583"></a>02583   {
<a name="l02584"></a>02584   }
<a name="l02585"></a>02585 
<a name="l02586"></a>02586 
<a name="l02587"></a>02587   <span class="comment">/**********************************</span>
<a name="l02588"></a>02588 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l02589"></a>02589 <span class="comment">   **********************************/</span>
<a name="l02590"></a>02590 
<a name="l02591"></a>02591 
<a name="l02593"></a>02593 
<a name="l02599"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a5fb8630efa21241d2517927eeb80de5e">02599</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02600"></a>02600   <span class="keyword">inline</span> complex&lt;T&gt;
<a name="l02601"></a>02601   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"></span>
<a name="l02602"></a>02602 <span class="keyword">    const</span>
<a name="l02603"></a>02603 <span class="keyword">  </span>{
<a name="l02604"></a>02604 
<a name="l02605"></a>02605 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02606"></a>02606 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02607"></a>02607       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02608"></a>02608                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02609"></a>02609                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02610"></a>02610     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02611"></a>02611       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02612"></a>02612                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02613"></a>02613                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02614"></a>02614 <span class="preprocessor">#endif</span>
<a name="l02615"></a>02615 <span class="preprocessor"></span>
<a name="l02616"></a>02616     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l02617"></a>02617       <span class="keywordflow">return</span> complex&lt;T&gt;(this-&gt;val_real_(i)(j), this-&gt;val_imag_(i)(j));
<a name="l02618"></a>02618 
<a name="l02619"></a>02619     <span class="keywordflow">return</span> complex&lt;T&gt;(this-&gt;val_real_(j)(i), this-&gt;val_imag_(j)(i));
<a name="l02620"></a>02620   }
<a name="l02621"></a>02621 
<a name="l02622"></a>02622 
<a name="l02624"></a>02624 
<a name="l02629"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#abc507579e215d0fcd88b8d2bf8e710b9">02629</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02630"></a>02630   <span class="keyword">inline</span> T&amp;
<a name="l02631"></a>02631   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::ValReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l02632"></a>02632   {
<a name="l02633"></a>02633 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02634"></a>02634 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02635"></a>02635       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::ValReal&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02636"></a>02636                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02637"></a>02637                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02638"></a>02638     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02639"></a>02639       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::ValReal&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02640"></a>02640                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02641"></a>02641                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02642"></a>02642     <span class="keywordflow">if</span> (i &gt; j)
<a name="l02643"></a>02643       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix::ValReal()&quot;</span>,
<a name="l02644"></a>02644                           <span class="keywordtype">string</span>(<span class="stringliteral">&quot;With this function, you &quot;</span>)
<a name="l02645"></a>02645                           + <span class="stringliteral">&quot;can only access upper part of matrix.&quot;</span>);
<a name="l02646"></a>02646 <span class="preprocessor">#endif</span>
<a name="l02647"></a>02647 <span class="preprocessor"></span>
<a name="l02648"></a>02648     <span class="keywordflow">return</span> this-&gt;val_real_(i).Val(j);
<a name="l02649"></a>02649   }
<a name="l02650"></a>02650 
<a name="l02651"></a>02651 
<a name="l02653"></a>02653 
<a name="l02659"></a>02659   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02660"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a2ccbdf02898e8b22ce9b5fe51aad89e1">02660</a>   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp;
<a name="l02661"></a>02661   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;</a>
<a name="l02662"></a>02662 <a class="code" href="class_seldon_1_1_matrix.php">  ::ValReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l02663"></a>02663 <span class="keyword">  </span>{
<a name="l02664"></a>02664 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02665"></a>02665 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02666"></a>02666       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::ValReal&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02667"></a>02667                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02668"></a>02668                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02669"></a>02669     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02670"></a>02670       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::ValReal&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02671"></a>02671                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02672"></a>02672                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02673"></a>02673     <span class="keywordflow">if</span> (i &gt; j)
<a name="l02674"></a>02674       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix::ValReal()&quot;</span>,
<a name="l02675"></a>02675                           <span class="keywordtype">string</span>(<span class="stringliteral">&quot;With this function, you &quot;</span>)
<a name="l02676"></a>02676                           + <span class="stringliteral">&quot;can only access upper part of matrix.&quot;</span>);
<a name="l02677"></a>02677 <span class="preprocessor">#endif</span>
<a name="l02678"></a>02678 <span class="preprocessor"></span>
<a name="l02679"></a>02679     <span class="keywordflow">return</span> this-&gt;val_real_(i).Val(j);
<a name="l02680"></a>02680   }
<a name="l02681"></a>02681 
<a name="l02682"></a>02682 
<a name="l02684"></a>02684 
<a name="l02690"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a85eba6f5bb491fd35b28e5753f48eace">02690</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02691"></a>02691   <span class="keyword">inline</span> T&amp;
<a name="l02692"></a>02692   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::GetReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l02693"></a>02693   {
<a name="l02694"></a>02694 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02695"></a>02695 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02696"></a>02696       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::GetReal&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02697"></a>02697                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02698"></a>02698                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02699"></a>02699     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02700"></a>02700       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::GetReal&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02701"></a>02701                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02702"></a>02702                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02703"></a>02703 <span class="preprocessor">#endif</span>
<a name="l02704"></a>02704 <span class="preprocessor"></span>
<a name="l02705"></a>02705     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l02706"></a>02706       <span class="keywordflow">return</span> this-&gt;val_real_(i).Get(j);
<a name="l02707"></a>02707 
<a name="l02708"></a>02708     <span class="keywordflow">return</span> this-&gt;val_real_(j).Get(i);
<a name="l02709"></a>02709   }
<a name="l02710"></a>02710 
<a name="l02711"></a>02711 
<a name="l02713"></a>02713 
<a name="l02719"></a>02719   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02720"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a39ca8957207e6102cba15d2f0d7f47fb">02720</a>   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp;
<a name="l02721"></a>02721   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;</a>
<a name="l02722"></a>02722 <a class="code" href="class_seldon_1_1_matrix.php">  ::GetReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l02723"></a>02723 <span class="keyword">  </span>{
<a name="l02724"></a>02724 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02725"></a>02725 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02726"></a>02726       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::GetReal&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02727"></a>02727                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02728"></a>02728                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02729"></a>02729     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02730"></a>02730       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::GetReal&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02731"></a>02731                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02732"></a>02732                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02733"></a>02733 <span class="preprocessor">#endif</span>
<a name="l02734"></a>02734 <span class="preprocessor"></span>
<a name="l02735"></a>02735     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l02736"></a>02736       <span class="keywordflow">return</span> this-&gt;val_real_(i).Get(j);
<a name="l02737"></a>02737 
<a name="l02738"></a>02738     <span class="keywordflow">return</span> this-&gt;val_real_(j).Get(i);
<a name="l02739"></a>02739   }
<a name="l02740"></a>02740 
<a name="l02741"></a>02741 
<a name="l02743"></a>02743 
<a name="l02749"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a8584e87cfb5ba75a746ec60a926f23a4">02749</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02750"></a>02750   <span class="keyword">inline</span> T&amp;
<a name="l02751"></a>02751   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::ValImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l02752"></a>02752   {
<a name="l02753"></a>02753 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02754"></a>02754 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02755"></a>02755       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::ValImag&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02756"></a>02756                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02757"></a>02757                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02758"></a>02758     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02759"></a>02759       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::ValImag&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02760"></a>02760                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02761"></a>02761                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02762"></a>02762     <span class="keywordflow">if</span> (i &gt; j)
<a name="l02763"></a>02763       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix::ValImag()&quot;</span>,
<a name="l02764"></a>02764                           <span class="keywordtype">string</span>(<span class="stringliteral">&quot;With this function, you &quot;</span>)
<a name="l02765"></a>02765                           + <span class="stringliteral">&quot;can only access upper part of matrix.&quot;</span>);
<a name="l02766"></a>02766 <span class="preprocessor">#endif</span>
<a name="l02767"></a>02767 <span class="preprocessor"></span>
<a name="l02768"></a>02768     <span class="keywordflow">return</span> this-&gt;val_imag_(i).Val(j);
<a name="l02769"></a>02769   }
<a name="l02770"></a>02770 
<a name="l02771"></a>02771 
<a name="l02773"></a>02773 
<a name="l02778"></a>02778   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02779"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a9798b4ecbfea2a0629c8b484d9be931a">02779</a>   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp;
<a name="l02780"></a>02780   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;</a>
<a name="l02781"></a>02781 <a class="code" href="class_seldon_1_1_matrix.php">  ::ValImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l02782"></a>02782 <span class="keyword">  </span>{
<a name="l02783"></a>02783 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02784"></a>02784 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02785"></a>02785       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::ValImag&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02786"></a>02786                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02787"></a>02787                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02788"></a>02788     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02789"></a>02789       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::ValImag&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02790"></a>02790                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02791"></a>02791                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02792"></a>02792     <span class="keywordflow">if</span> (i &gt; j)
<a name="l02793"></a>02793       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix::ValImag()&quot;</span>,
<a name="l02794"></a>02794                           <span class="keywordtype">string</span>(<span class="stringliteral">&quot;With this function, you &quot;</span>)
<a name="l02795"></a>02795                           + <span class="stringliteral">&quot;can only access upper part of matrix.&quot;</span>);
<a name="l02796"></a>02796 <span class="preprocessor">#endif</span>
<a name="l02797"></a>02797 <span class="preprocessor"></span>
<a name="l02798"></a>02798     <span class="keywordflow">return</span> this-&gt;val_imag_(i).Val(j);
<a name="l02799"></a>02799   }
<a name="l02800"></a>02800 
<a name="l02801"></a>02801 
<a name="l02803"></a>02803 
<a name="l02808"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#affbf4d5e0fb00ec74943e47af3859d87">02808</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02809"></a>02809   <span class="keyword">inline</span> T&amp;
<a name="l02810"></a>02810   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::GetImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l02811"></a>02811   {
<a name="l02812"></a>02812 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02813"></a>02813 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02814"></a>02814       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::GetImag&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02815"></a>02815                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02816"></a>02816                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02817"></a>02817     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02818"></a>02818       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::GetImag&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02819"></a>02819                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02820"></a>02820                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02821"></a>02821 <span class="preprocessor">#endif</span>
<a name="l02822"></a>02822 <span class="preprocessor"></span>
<a name="l02823"></a>02823     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l02824"></a>02824       <span class="keywordflow">return</span> this-&gt;val_imag_(i).Get(j);
<a name="l02825"></a>02825 
<a name="l02826"></a>02826     <span class="keywordflow">return</span> this-&gt;val_imag_(j).Get(i);
<a name="l02827"></a>02827   }
<a name="l02828"></a>02828 
<a name="l02829"></a>02829 
<a name="l02831"></a>02831 
<a name="l02836"></a>02836   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02837"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#af7fa7341f09012e5f0ab244a40ab0ca2">02837</a>   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp;
<a name="l02838"></a>02838   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;</a>
<a name="l02839"></a>02839 <a class="code" href="class_seldon_1_1_matrix.php">  ::GetImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l02840"></a>02840 <span class="keyword">  </span>{
<a name="l02841"></a>02841 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02842"></a>02842 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02843"></a>02843       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::GetImag&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02844"></a>02844                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02845"></a>02845                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02846"></a>02846     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02847"></a>02847       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::GetImag&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l02848"></a>02848                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l02849"></a>02849                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02850"></a>02850 <span class="preprocessor">#endif</span>
<a name="l02851"></a>02851 <span class="preprocessor"></span>
<a name="l02852"></a>02852     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l02853"></a>02853       <span class="keywordflow">return</span> this-&gt;val_imag_(i).Val(j);
<a name="l02854"></a>02854 
<a name="l02855"></a>02855     <span class="keywordflow">return</span> this-&gt;val_imag_(j).Val(i);
<a name="l02856"></a>02856   }
<a name="l02857"></a>02857 
<a name="l02858"></a>02858 
<a name="l02860"></a>02860 
<a name="l02865"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a49ac0164990312cc9485abe5de3a1d77">02865</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02866"></a>02866   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;</a>
<a name="l02867"></a>02867 <a class="code" href="class_seldon_1_1_matrix.php">  ::Set</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; x)
<a name="l02868"></a>02868   {
<a name="l02869"></a>02869     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l02870"></a>02870       {
<a name="l02871"></a>02871         <span class="keywordflow">if</span> (real(x) != T(0))
<a name="l02872"></a>02872           this-&gt;val_real_(i).Get(j) = real(x);
<a name="l02873"></a>02873         <span class="keywordflow">else</span>
<a name="l02874"></a>02874           {
<a name="l02875"></a>02875             <span class="keywordflow">if</span> (this-&gt;val_real_(i)(j) != T(0))
<a name="l02876"></a>02876               this-&gt;val_real_(i).Get(j) = T(0);
<a name="l02877"></a>02877           }
<a name="l02878"></a>02878 
<a name="l02879"></a>02879         <span class="keywordflow">if</span> (imag(x) != T(0))
<a name="l02880"></a>02880           this-&gt;val_imag_(i).Get(j) = imag(x);
<a name="l02881"></a>02881         <span class="keywordflow">else</span>
<a name="l02882"></a>02882           {
<a name="l02883"></a>02883             <span class="keywordflow">if</span> (this-&gt;val_imag_(i)(j) != T(0))
<a name="l02884"></a>02884               this-&gt;val_imag_(i).Get(j) = T(0);
<a name="l02885"></a>02885           }
<a name="l02886"></a>02886       }
<a name="l02887"></a>02887     <span class="keywordflow">else</span>
<a name="l02888"></a>02888       {
<a name="l02889"></a>02889         <span class="keywordflow">if</span> (real(x) != T(0))
<a name="l02890"></a>02890           this-&gt;val_real_(j).Get(i) = real(x);
<a name="l02891"></a>02891         <span class="keywordflow">else</span>
<a name="l02892"></a>02892           {
<a name="l02893"></a>02893             <span class="keywordflow">if</span> (this-&gt;val_real_(j)(i) != T(0))
<a name="l02894"></a>02894               this-&gt;val_real_(j).Get(i) = T(0);
<a name="l02895"></a>02895           }
<a name="l02896"></a>02896 
<a name="l02897"></a>02897         <span class="keywordflow">if</span> (imag(x) != T(0))
<a name="l02898"></a>02898           this-&gt;val_imag_(j).Get(i) = imag(x);
<a name="l02899"></a>02899         <span class="keywordflow">else</span>
<a name="l02900"></a>02900           {
<a name="l02901"></a>02901             <span class="keywordflow">if</span> (this-&gt;val_imag_(j)(i) != T(0))
<a name="l02902"></a>02902               this-&gt;val_imag_(j).Get(i) = T(0);
<a name="l02903"></a>02903           }
<a name="l02904"></a>02904       }
<a name="l02905"></a>02905   }
<a name="l02906"></a>02906 
<a name="l02907"></a>02907 
<a name="l02909"></a>02909   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02910"></a>02910   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::ClearRealRow</a>(<span class="keywordtype">int</span> i)
<a name="l02911"></a>02911   {
<a name="l02912"></a>02912     this-&gt;val_real_(i).Clear();
<a name="l02913"></a>02913   }
<a name="l02914"></a>02914 
<a name="l02915"></a>02915 
<a name="l02917"></a>02917   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02918"></a>02918   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a47212adda8f7586925272cf2e4c620bb" title="Clears a row.">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::ClearImagRow</a>(<span class="keywordtype">int</span> i)
<a name="l02919"></a>02919   {
<a name="l02920"></a>02920     this-&gt;val_imag_(i).Clear();
<a name="l02921"></a>02921   }
<a name="l02922"></a>02922 
<a name="l02923"></a>02923 
<a name="l02925"></a>02925 
<a name="l02930"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a828bff3d9a061706b80cf3cc4a6725eb">02930</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02931"></a>02931   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l02932"></a>02932 <a class="code" href="class_seldon_1_1_matrix.php">  ReallocateRealRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l02933"></a>02933   {
<a name="l02934"></a>02934     this-&gt;val_real_(i).Reallocate(j);
<a name="l02935"></a>02935   }
<a name="l02936"></a>02936 
<a name="l02937"></a>02937 
<a name="l02939"></a>02939 
<a name="l02944"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#add6c25193de11ecc3f405c1ba713e0ef">02944</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02945"></a>02945   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l02946"></a>02946 <a class="code" href="class_seldon_1_1_matrix.php">  ReallocateImagRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l02947"></a>02947   {
<a name="l02948"></a>02948     this-&gt;val_imag_(i).Reallocate(j);
<a name="l02949"></a>02949   }
<a name="l02950"></a>02950 
<a name="l02951"></a>02951 
<a name="l02953"></a>02953 
<a name="l02957"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a9a7d49e83e0838f63c4e7210ccc42218">02957</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02958"></a>02958   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l02959"></a>02959 <a class="code" href="class_seldon_1_1_matrix.php">  ResizeRealRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l02960"></a>02960   {
<a name="l02961"></a>02961     this-&gt;val_real_(i).Resize(j);
<a name="l02962"></a>02962   }
<a name="l02963"></a>02963 
<a name="l02964"></a>02964 
<a name="l02966"></a>02966 
<a name="l02970"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a770cf431a7508fad913ef06bf487f9d6">02970</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02971"></a>02971   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l02972"></a>02972 <a class="code" href="class_seldon_1_1_matrix.php">  ResizeImagRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l02973"></a>02973   {
<a name="l02974"></a>02974     this-&gt;val_imag_(i).Resize(j);
<a name="l02975"></a>02975   }
<a name="l02976"></a>02976 
<a name="l02977"></a>02977 
<a name="l02979"></a>02979 
<a name="l02983"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#adf10d81ed34502289b1d928626921d91">02983</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02984"></a>02984   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l02985"></a>02985 <a class="code" href="class_seldon_1_1_matrix.php">  SwapRealRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l02986"></a>02986   {
<a name="l02987"></a>02987     Swap(this-&gt;val_real_(i), this-&gt;val_real_(j));
<a name="l02988"></a>02988   }
<a name="l02989"></a>02989 
<a name="l02990"></a>02990 
<a name="l02992"></a>02992 
<a name="l02996"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#abd914e374842533215ebeb12ae0fd5e6">02996</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02997"></a>02997   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l02998"></a>02998 <a class="code" href="class_seldon_1_1_matrix.php">  SwapImagRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l02999"></a>02999   {
<a name="l03000"></a>03000     Swap(this-&gt;val_imag_(i), this-&gt;val_imag_(j));
<a name="l03001"></a>03001   }
<a name="l03002"></a>03002 
<a name="l03003"></a>03003 
<a name="l03005"></a>03005 
<a name="l03009"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a5d858213b73625c365f2cf80c955b56c">03009</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l03010"></a>03010   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l03011"></a>03011 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceRealIndexRow</a>(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l03012"></a>03012   {
<a name="l03013"></a>03013     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_real_(i).GetM(); j++)
<a name="l03014"></a>03014       this-&gt;val_real_(i).Index(j) = new_index(j);
<a name="l03015"></a>03015   }
<a name="l03016"></a>03016 
<a name="l03017"></a>03017 
<a name="l03019"></a>03019 
<a name="l03023"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a18e705d5da14b8ae374333f02a1801af">03023</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l03024"></a>03024   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l03025"></a>03025 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceImagIndexRow</a>(<span class="keywordtype">int</span> i,<a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l03026"></a>03026   {
<a name="l03027"></a>03027     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_imag_(i).GetM(); j++)
<a name="l03028"></a>03028       this-&gt;val_imag_(i).Index(j) = new_index(j);
<a name="l03029"></a>03029   }
<a name="l03030"></a>03030 
<a name="l03031"></a>03031 
<a name="l03033"></a>03033 
<a name="l03037"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#afc6d830c2b6290ab90af0733ca2991d9">03037</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l03038"></a>03038   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;</a>
<a name="l03039"></a>03039 <a class="code" href="class_seldon_1_1_matrix.php">  ::GetRealRowSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"></span>
<a name="l03040"></a>03040 <span class="keyword">    const</span>
<a name="l03041"></a>03041 <span class="keyword">  </span>{
<a name="l03042"></a>03042     <span class="keywordflow">return</span> this-&gt;val_real_(i).GetSize();
<a name="l03043"></a>03043   }
<a name="l03044"></a>03044 
<a name="l03045"></a>03045 
<a name="l03047"></a>03047 
<a name="l03051"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#af38c69e307e68125ec3304929b672e95">03051</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l03052"></a>03052   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;</a>
<a name="l03053"></a>03053 <a class="code" href="class_seldon_1_1_matrix.php">  ::GetImagRowSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l03054"></a>03054 <span class="keyword">  </span>{
<a name="l03055"></a>03055     <span class="keywordflow">return</span> this-&gt;val_imag_(i).GetSize();
<a name="l03056"></a>03056   }
<a name="l03057"></a>03057 
<a name="l03058"></a>03058 
<a name="l03060"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a4287416fdfc3c14ca814be6ebc29153c">03060</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l03061"></a>03061   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;</a>
<a name="l03062"></a>03062 <a class="code" href="class_seldon_1_1_matrix.php">  ::PrintRealRow</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l03063"></a>03063 <span class="keyword">  </span>{
<a name="l03064"></a>03064     this-&gt;val_real_(i).Print();
<a name="l03065"></a>03065   }
<a name="l03066"></a>03066 
<a name="l03067"></a>03067 
<a name="l03069"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a6bf23b591a902626ffb78da822bc4c3e">03069</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l03070"></a>03070   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;</a>
<a name="l03071"></a>03071 <a class="code" href="class_seldon_1_1_matrix.php">  ::PrintImagRow</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l03072"></a>03072 <span class="keyword">  </span>{
<a name="l03073"></a>03073     this-&gt;val_imag_(i).Print();
<a name="l03074"></a>03074   }
<a name="l03075"></a>03075 
<a name="l03076"></a>03076 
<a name="l03078"></a>03078 
<a name="l03083"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a0dd4ca70074352d9df0370707dd05127">03083</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l03084"></a>03084   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;</a>
<a name="l03085"></a>03085 <a class="code" href="class_seldon_1_1_matrix.php">  ::AssembleRealRow</a>(<span class="keywordtype">int</span> i)
<a name="l03086"></a>03086   {
<a name="l03087"></a>03087     this-&gt;val_real_(i).Assemble();
<a name="l03088"></a>03088   }
<a name="l03089"></a>03089 
<a name="l03090"></a>03090 
<a name="l03092"></a>03092 
<a name="l03097"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a44db6b452950f6d03227f0b49e8aa427">03097</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l03098"></a>03098   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;</a>
<a name="l03099"></a>03099 <a class="code" href="class_seldon_1_1_matrix.php">  ::AssembleImagRow</a>(<span class="keywordtype">int</span> i)
<a name="l03100"></a>03100   {
<a name="l03101"></a>03101     this-&gt;val_imag_(i).Assemble();
<a name="l03102"></a>03102   }
<a name="l03103"></a>03103 
<a name="l03104"></a>03104 
<a name="l03106"></a>03106 
<a name="l03111"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a9cce706f016372a361f025cf92c3cb25">03111</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l03112"></a>03112   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l03113"></a>03113 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; val)
<a name="l03114"></a>03114   {
<a name="l03115"></a>03115     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l03116"></a>03116       {
<a name="l03117"></a>03117         <span class="keywordflow">if</span> (real(val) != T(0))
<a name="l03118"></a>03118           this-&gt;val_real_(i).AddInteraction(j, real(val));
<a name="l03119"></a>03119 
<a name="l03120"></a>03120         <span class="keywordflow">if</span> (imag(val) != T(0))
<a name="l03121"></a>03121           this-&gt;val_imag_(i).AddInteraction(j, imag(val));
<a name="l03122"></a>03122       }
<a name="l03123"></a>03123   }
<a name="l03124"></a>03124 
<a name="l03125"></a>03125 
<a name="l03127"></a>03127 
<a name="l03133"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#adb9c19345b5e6f57b6d8f3dbec9bad93">03133</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l03134"></a>03134   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l03135"></a>03135 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l03136"></a>03136                     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1&gt;&amp; val)
<a name="l03137"></a>03137   {
<a name="l03138"></a>03138     <span class="keywordtype">int</span> nb_real = 0;
<a name="l03139"></a>03139     <span class="keywordtype">int</span> nb_imag = 0;
<a name="l03140"></a>03140     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> col_real(nb), col_imag(nb);
<a name="l03141"></a>03141     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val_real(nb), val_imag(nb);
<a name="l03142"></a>03142     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nb; j++)
<a name="l03143"></a>03143       <span class="keywordflow">if</span> (i &lt;= col(j))
<a name="l03144"></a>03144         {
<a name="l03145"></a>03145           <span class="keywordflow">if</span> (real(val(j)) != T(0))
<a name="l03146"></a>03146             {
<a name="l03147"></a>03147               col_real(nb_real) = col(j);
<a name="l03148"></a>03148               val_real(nb_real) = real(val(j));
<a name="l03149"></a>03149               nb_real++;
<a name="l03150"></a>03150             }
<a name="l03151"></a>03151 
<a name="l03152"></a>03152           <span class="keywordflow">if</span> (imag(val(j)) != T(0))
<a name="l03153"></a>03153             {
<a name="l03154"></a>03154               col_imag(nb_imag) = col(j);
<a name="l03155"></a>03155               val_imag(nb_imag) = imag(val(j));
<a name="l03156"></a>03156               nb_imag++;
<a name="l03157"></a>03157             }
<a name="l03158"></a>03158         }
<a name="l03159"></a>03159 
<a name="l03160"></a>03160     this-&gt;val_real_(i).AddInteractionRow(nb_real, col_real, val_real);
<a name="l03161"></a>03161     this-&gt;val_imag_(i).AddInteractionRow(nb_imag, col_imag, val_imag);
<a name="l03162"></a>03162   }
<a name="l03163"></a>03163 
<a name="l03164"></a>03164 
<a name="l03166"></a>03166 
<a name="l03172"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a4703da0eae726628352e0e68340c4ae4">03172</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l03173"></a>03173   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l03174"></a>03174 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l03175"></a>03175                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1&gt;&amp; val)
<a name="l03176"></a>03176   {
<a name="l03177"></a>03177     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nb; j++)
<a name="l03178"></a>03178       AddInteraction(row(j), i, val(j));
<a name="l03179"></a>03179   }
<a name="l03180"></a>03180 
<a name="l03181"></a>03181 } <span class="comment">// namespace Seldon</span>
<a name="l03182"></a>03182 
<a name="l03183"></a>03183 <span class="preprocessor">#define SELDON_FILE_MATRIX_ARRAY_COMPLEX_SPARSE_CXX</span>
<a name="l03184"></a>03184 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
