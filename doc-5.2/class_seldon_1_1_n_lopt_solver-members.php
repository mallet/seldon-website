<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::NLoptSolver Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a>, including all inherited members.<table>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#aad0beaa06728577f5f7c39c2989f81ed">algorithm_</a></td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#afb1ea25dcf6a9da4b54f97d5ccf95cc8">cost_</a></td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#afd6a5c3ebbb73a71fbd412a641eca860">cost_function_tolerance_</a></td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>cost_ptr</b> typedef (defined in <a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a>)</td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#aaf4264800dbfb5f24af404ac6019e0b0">GetCost</a>() const </td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#ab585d1ac63039aa34eb41ba3a1548817">GetCostFunctionTolerance</a>(double &amp;) const </td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#ac584f3bed8e3326faa50fff3c7004b62">GetNiterationMax</a>(int &amp;) const </td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#a2de56e3f3de295800e412039ca5b0e69">GetParameter</a>(Vector&lt; double &gt; &amp;parameter) const </td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#a4e7c4cb0c59c99b234c89b23ffd56927">GetParameterTolerance</a>(double &amp;) const </td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#a3d4a7ac50a792cdd899ff11a0cd5f3f3">gradient_</a></td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#ad80720692c1df8718044c24ecbc98372">Initialize</a>(int Nparameter, string algorithm, double parameter_tolerance=1.e-6, double cost_function_tolerance=1.e-6, int Niteration_max=-1)</td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#ac3340df79837a4cdde5560939ed01989">Niteration_max_</a></td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#ad775e71c990c2d8f08feaf0399a1a916">NLoptSolver</a>()</td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#a0236ea3add112a572ed2326a12306ded">opt_</a></td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#a15954e351f26ae09799c07e0009ce864">Optimize</a>(cost_ptr cost, void *argument)</td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#aade135d662e35906b7cae2ecbd1d789c">parameter_</a></td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#adefce6eb95498ffb35690ce4c8c7f637">parameter_tolerance_</a></td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#a44ed740163ffdc82a1eda9a47001e58a">SetCostFunctionTolerance</a>(double)</td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#a6f92bbc59849ce8489da5210dfd6d994">SetLowerBound</a>(const Vector&lt; double &gt; &amp;)</td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#ae8d18863be9cbbf79bc14e5fdc905a39">SetNiterationMax</a>(int)</td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#adb63bff470ea8bbc8df7cc78207a597f">SetParameter</a>(const Vector&lt; double &gt; &amp;parameter)</td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#aa74cb3aca730ca6444fff8bd600f3be7">SetParameterTolerance</a>(double)</td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#a46fee6721c1a4172bae0ce10870d4442">SetUpperBound</a>(const Vector&lt; double &gt; &amp;)</td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php#adcf818c843283a20eef4d17a974fd6cc">~NLoptSolver</a>()</td><td><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></td><td></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
