<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php">Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;" --><!-- doxytag: inherits="Matrix_ArrayComplexSparse&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;" -->
<p>Column-major symmetric sparse-matrix class.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_matrix___array_complex_sparse_8hxx_source.php">Matrix_ArrayComplexSparse.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.png" usemap="#Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;_map" alt=""/>
  <map id="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;_map" name="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;_map">
<area href="class_seldon_1_1_matrix___array_complex_sparse.php" alt="Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;" shape="rect" coords="0,0,520,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a003dd951faace22a221b8af92e950d19"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::value_type" ref="a003dd951faace22a221b8af92e950d19" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae2a5af587e603c4c8e4a1f4424b8b6da"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::property" ref="ae2a5af587e603c4c8e4a1f4424b8b6da" args="" -->
typedef Prop&nbsp;</td><td class="memItemRight" valign="bottom"><b>property</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4462e00047b6f909533876dffaec76f9"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::storage" ref="a4462e00047b6f909533876dffaec76f9" args="" -->
typedef <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>&nbsp;</td><td class="memItemRight" valign="bottom"><b>storage</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9b1b70c519ec1adb7ed51b7bca691760"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::allocator" ref="a9b1b70c519ec1adb7ed51b7bca691760" args="" -->
typedef Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>allocator</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5325ffba5ca6253c1df46e37283142d0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::pointer" ref="a5325ffba5ca6253c1df46e37283142d0" args="" -->
typedef Allocator::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a87436f000442ff05b2fc557a77dae13c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::const_pointer" ref="a87436f000442ff05b2fc557a77dae13c" args="" -->
typedef Allocator::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1ab28bb0ce6a3c1bba3b7680b0d2f6a2"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::reference" ref="a1ab28bb0ce6a3c1bba3b7680b0d2f6a2" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a963d0f91721a1d53fb2b4d5bc322abb8"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::const_reference" ref="a963d0f91721a1d53fb2b4d5bc322abb8" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2b4d88a19897b82ba5d8a37198cf4426"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::entry_type" ref="a2b4d88a19897b82ba5d8a37198cf4426" args="" -->
typedef complex&lt; T &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>entry_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a55a8cb7922d8fc8bbacd2b5548b03359"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::access_type" ref="a55a8cb7922d8fc8bbacd2b5548b03359" args="" -->
typedef complex&lt; T &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>access_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aaa7b45cfa45845f340f819e31f877f34"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::const_access_type" ref="aaa7b45cfa45845f340f819e31f877f34" args="" -->
typedef complex&lt; T &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_access_type</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ac0a88fcc183d15c9d86cfdde270a1d30">Matrix</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#ac0a88fcc183d15c9d86cfdde270a1d30"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ad7e66b7b0a5c63e77a6607545eec07b0">Matrix</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Constructor.  <a href="#ad7e66b7b0a5c63e77a6607545eec07b0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">complex&lt; T &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ace196e6c26320571bf77a782833d7232">operator()</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#ace196e6c26320571bf77a782833d7232"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a9edb81b7ae21d2118c6cc4d98058d731">ValReal</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns access to real part of element (i, j).  <a href="#a9edb81b7ae21d2118c6cc4d98058d731"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a69b5391fd7e911f3f3204bf627e1ed24">ValReal</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns access to real part element (i, j).  <a href="#a69b5391fd7e911f3f3204bf627e1ed24"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a1b6113bbca815576f929be337af47483">ValImag</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns access to imaginary part of element (i, j).  <a href="#a1b6113bbca815576f929be337af47483"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a113a4f9a03767b54cb38c34af0d03ff7">ValImag</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns access to imaginary part of element (i, j).  <a href="#a113a4f9a03767b54cb38c34af0d03ff7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a9a8f3daf0ad036985c22c4772b1477aa">GetReal</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns access to real part element (i, j).  <a href="#a9a8f3daf0ad036985c22c4772b1477aa"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a20616f82c194e42a610e3e48e63d5a40">GetReal</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns access to real part of element (i, j).  <a href="#a20616f82c194e42a610e3e48e63d5a40"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a913307297605f734e98ba7c7e870ae39">GetImag</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns access to imaginary part of element (i, j).  <a href="#a913307297605f734e98ba7c7e870ae39"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#af4a356b6428d68d9890b96494ef89cc0">GetImag</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns access to imaginary part element (i, j).  <a href="#af4a356b6428d68d9890b96494ef89cc0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a6f67461ccf260c8bb9e9fd1fc40811e7">Set</a> (int i, int j, const complex&lt; T &gt; &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets element (i, j) of the matrix.  <a href="#a6f67461ccf260c8bb9e9fd1fc40811e7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab22d91b2a8aa62a3d5d0ea295749b651"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::ClearRealColumn" ref="ab22d91b2a8aa62a3d5d0ea295749b651" args="(int i)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ab22d91b2a8aa62a3d5d0ea295749b651">ClearRealColumn</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears a column. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="addf758fad26c1595b8398590e138fa2f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::ClearImagColumn" ref="addf758fad26c1595b8398590e138fa2f" args="(int i)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#addf758fad26c1595b8398590e138fa2f">ClearImagColumn</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears a column. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a063f733a818719a8349d0de407a685a0">ReallocateRealColumn</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates column i.  <a href="#a063f733a818719a8349d0de407a685a0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a460d1b0cadc5cf4915ce024be57b1e58">ReallocateImagColumn</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates column i.  <a href="#a460d1b0cadc5cf4915ce024be57b1e58"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ab21dd3904c7fb5d4673106b83b5099d4">ResizeRealColumn</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates column i.  <a href="#ab21dd3904c7fb5d4673106b83b5099d4"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a8f954de1b994af747e4e4e6c3bc2d8b9">ResizeImagColumn</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates column i.  <a href="#a8f954de1b994af747e4e4e6c3bc2d8b9"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#af2f1c1dc9a20bfed30e186f8d4c7dc44">SwapRealColumn</a> (int i, int i_)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Swaps two columns.  <a href="#af2f1c1dc9a20bfed30e186f8d4c7dc44"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a7b4cf92d8be151c14508ada2b6b0b72d">SwapImagColumn</a> (int i, int i_)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Swaps two columns.  <a href="#a7b4cf92d8be151c14508ada2b6b0b72d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#addca588562b05576d4b37711bb4f6820">ReplaceRealIndexColumn</a> (int i, <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;new_index)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets row numbers of non-zero entries of a column.  <a href="#addca588562b05576d4b37711bb4f6820"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a832f6f65f5c46c0cfaa66c0cb76e922e">ReplaceImagIndexColumn</a> (int i, <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;new_index)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets row numbers of non-zero entries of a column.  <a href="#a832f6f65f5c46c0cfaa66c0cb76e922e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ab488dd8cd29a05f016d63b30e90b4f2d">GetRealColumnSize</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of non-zero entries of a column.  <a href="#ab488dd8cd29a05f016d63b30e90b4f2d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a6cebdd4865f5c2c9e90d5b4f6961414a">GetImagColumnSize</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of non-zero entries of a column.  <a href="#a6cebdd4865f5c2c9e90d5b4f6961414a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad080f5381fb2a720b7ecb72b245db852"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::PrintRealColumn" ref="ad080f5381fb2a720b7ecb72b245db852" args="(int i) const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ad080f5381fb2a720b7ecb72b245db852">PrintRealColumn</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays non-zero values of a column. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afa56428c069675df01f46fd68d39be47"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::PrintImagColumn" ref="afa56428c069675df01f46fd68d39be47" args="(int i) const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#afa56428c069675df01f46fd68d39be47">PrintImagColumn</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays non-zero values of a column. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a2ddc33ea923bccadd9b07a7f6b0a8690">AssembleRealColumn</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Assembles a column.  <a href="#a2ddc33ea923bccadd9b07a7f6b0a8690"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a1285786e064b8336ba57b6c77097e4d5">AssembleImagColumn</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Assembles a column.  <a href="#a1285786e064b8336ba57b6c77097e4d5"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a96c7b8d7f0e1e862558b9e3039a32878">AddInteraction</a> (int i, int j, const complex&lt; T &gt; &amp;val)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds a coefficient in the matrix.  <a href="#a96c7b8d7f0e1e862558b9e3039a32878"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Alloc1 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a6dcf362a6feaf5467c8c64d6be823fda">AddInteractionRow</a> (int i, int nb, const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;col, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; complex&lt; T &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1 &gt; &amp;val)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds coefficients in a row.  <a href="#a6dcf362a6feaf5467c8c64d6be823fda"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Alloc1 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a703b16f79696af7a7d18da3c1db72671">AddInteractionColumn</a> (int i, int nb, const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;row, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; complex&lt; T &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1 &gt; &amp;val)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds coefficients in a column.  <a href="#a703b16f79696af7a7d18da3c1db72671"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0ee918c8c04a1a9434e157c15fdeb596"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::Clear" ref="a0ee918c8c04a1a9434e157c15fdeb596" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Clear</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad2348fb25206ef8516c7e835b7ce2287"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::Reallocate" ref="ad2348fb25206ef8516c7e835b7ce2287" args="(int i, int j)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Reallocate</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9166689334cc2f731220f76b730d692f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::Resize" ref="a9166689334cc2f731220f76b730d692f" args="(int i, int j)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Resize</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a35ea621d2e5351259ce5bca87febc821"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::GetM" ref="a35ea621d2e5351259ce5bca87febc821" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetM</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab3c6aad66c9dc6ad9addfb7de9a4ebdd"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::GetM" ref="ab3c6aad66c9dc6ad9addfb7de9a4ebdd" args="(const SeldonTranspose &amp;status) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetM</b> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a> &amp;status) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae413915ed95545b40df8b43737fa43c1"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::GetN" ref="ae413915ed95545b40df8b43737fa43c1" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetN</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a53e20518a8557211c89ca8ad0d607825"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::GetN" ref="a53e20518a8557211c89ca8ad0d607825" args="(const SeldonTranspose &amp;status) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetN</b> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a> &amp;status) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a11efc1bee6a95da705238628d7e3d958"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::GetRealNonZeros" ref="a11efc1bee6a95da705238628d7e3d958" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetRealNonZeros</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a33ebac042eda90796d7f674c250e85be"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::GetImagNonZeros" ref="a33ebac042eda90796d7f674c250e85be" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetImagNonZeros</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa40775992c738c7fd1e9543bfddb2694"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::GetRealDataSize" ref="aa40775992c738c7fd1e9543bfddb2694" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetRealDataSize</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad54321edab8e2633985e198f1a576340"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::GetImagDataSize" ref="ad54321edab8e2633985e198f1a576340" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetImagDataSize</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2e5c6c10af220eea29f9c1565b14a93f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::GetDataSize" ref="a2e5c6c10af220eea29f9c1565b14a93f" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataSize</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afae45969eda952b43748cf7756f0e6d6"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::GetRealInd" ref="afae45969eda952b43748cf7756f0e6d6" args="(int i) const" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetRealInd</b> (int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0a423d7e93a0ad9c19ed6de2b1052dca"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::GetImagInd" ref="a0a423d7e93a0ad9c19ed6de2b1052dca" args="(int i) const" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetImagInd</b> (int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aaa912365c7b8c191cf8b3fb2e2da4789"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::GetRealData" ref="aaa912365c7b8c191cf8b3fb2e2da4789" args="(int i) const" -->
T *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetRealData</b> (int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1d33377770e234c75aae39bed6f996ff"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::GetRealData" ref="a1d33377770e234c75aae39bed6f996ff" args="() const" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt; *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetRealData</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a578281c64cbe05a542a6b0e9642b326a"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::GetImagData" ref="a578281c64cbe05a542a6b0e9642b326a" args="(int i) const" -->
T *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetImagData</b> (int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac3940bc9993d64730333c83be0f839ce"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::GetImagData" ref="ac3940bc9993d64730333c83be0f839ce" args="() const" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt; *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetImagData</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7eb05d6902b13cfb7cc4f17093fdc17e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::Val" ref="a7eb05d6902b13cfb7cc4f17093fdc17e" args="(int i, int j)" -->
complex&lt; T &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>Val</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a994fbc16dd258d1609f7c7c582205e9f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::Val" ref="a994fbc16dd258d1609f7c7c582205e9f" args="(int i, int j) const" -->
const complex&lt; T &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>Val</b> (int i, int j) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad840efb36621d28aa9726088cfc29a29"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::Get" ref="ad840efb36621d28aa9726088cfc29a29" args="(int i, int j)" -->
complex&lt; T &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>Get</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a17a0b2e40258b30da1f0036c2a85c357"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::Get" ref="a17a0b2e40258b30da1f0036c2a85c357" args="(int i, int j) const" -->
const complex&lt; T &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>Get</b> (int i, int j) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a041c76033f939293aa871f2fe45ef22a"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::ValueReal" ref="a041c76033f939293aa871f2fe45ef22a" args="(int num_row, int i) const" -->
const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>ValueReal</b> (int num_row, int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a11fe8648ddd3e6e339eb81735d7e544e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::ValueReal" ref="a11fe8648ddd3e6e339eb81735d7e544e" args="(int num_row, int i)" -->
T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>ValueReal</b> (int num_row, int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2a3cd76154ac61a43afa42563805ce3b"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::IndexReal" ref="a2a3cd76154ac61a43afa42563805ce3b" args="(int num_row, int i) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>IndexReal</b> (int num_row, int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2fdb9482dad6378fb8ced1982b6860b1"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::IndexReal" ref="a2fdb9482dad6378fb8ced1982b6860b1" args="(int num_row, int i)" -->
int &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>IndexReal</b> (int num_row, int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa2d9a2e69a269bf9d15b62d520cb560d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::ValueImag" ref="aa2d9a2e69a269bf9d15b62d520cb560d" args="(int num_row, int i) const" -->
const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>ValueImag</b> (int num_row, int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab410f72a305938081fbc07e868eb161b"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::ValueImag" ref="ab410f72a305938081fbc07e868eb161b" args="(int num_row, int i)" -->
T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>ValueImag</b> (int num_row, int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aedba64fa9b01bce6bb75e9e5a14b26c5"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::IndexImag" ref="aedba64fa9b01bce6bb75e9e5a14b26c5" args="(int num_row, int i) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>IndexImag</b> (int num_row, int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a011b15c1909f712a693de10f2c0c2328"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::IndexImag" ref="a011b15c1909f712a693de10f2c0c2328" args="(int num_row, int i)" -->
int &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>IndexImag</b> (int num_row, int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aab6c31240cd6dcbd442f9a29658f51ea"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::SetRealData" ref="aab6c31240cd6dcbd442f9a29658f51ea" args="(int, int, Vector&lt; T, VectSparse, Allocator &gt; *)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetRealData</b> (int, int, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; *)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1ebb3dcbf317ec99e6463f0aafea10b9"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::SetRealData" ref="a1ebb3dcbf317ec99e6463f0aafea10b9" args="(int, int, T *, int *)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetRealData</b> (int, int, T *, int *)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8e3c2dfa0ea5aa57cd8607cca15562f2"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::SetImagData" ref="a8e3c2dfa0ea5aa57cd8607cca15562f2" args="(int, int, Vector&lt; T, VectSparse, Allocator &gt; *)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetImagData</b> (int, int, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; *)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a50622f29a8e112821c68e5a28236258f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::SetImagData" ref="a50622f29a8e112821c68e5a28236258f" args="(int, int, T *, int *)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetImagData</b> (int, int, T *, int *)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9f0922d865cdd4a627912583a9c73151"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::NullifyReal" ref="a9f0922d865cdd4a627912583a9c73151" args="(int i)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>NullifyReal</b> (int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aed1192a75fe2c7d19e545ff6054298e2"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::NullifyReal" ref="aed1192a75fe2c7d19e545ff6054298e2" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>NullifyReal</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad5e87ca37baabfb8cdc6f841f5125d7d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::NullifyImag" ref="ad5e87ca37baabfb8cdc6f841f5125d7d" args="(int i)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>NullifyImag</b> (int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adbdb03e02fd8617a23188d6bc3155ec0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::NullifyImag" ref="adbdb03e02fd8617a23188d6bc3155ec0" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>NullifyImag</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac24abb2e047eb8b91728023074b3fb13"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::Print" ref="ac24abb2e047eb8b91728023074b3fb13" args="() const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Print</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9b54a14b64f67242fe9367df4142bd7f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::Write" ref="a9b54a14b64f67242fe9367df4142bd7f" args="(string FileName) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Write</b> (string FileName) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0cc7af3e3d677e5cb6bd5f66d7965a6f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::Write" ref="a0cc7af3e3d677e5cb6bd5f66d7965a6f" args="(ostream &amp;FileStream) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Write</b> (ostream &amp;FileStream) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7095c684ecdb9ef6e9fe9b84462e22ce"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::WriteText" ref="a7095c684ecdb9ef6e9fe9b84462e22ce" args="(string FileName) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>WriteText</b> (string FileName) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a884e62c07aa29b59b259d232f7771c94"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::WriteText" ref="a884e62c07aa29b59b259d232f7771c94" args="(ostream &amp;FileStream) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>WriteText</b> (ostream &amp;FileStream) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a58c2b7013db7765a01181f928833e530"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::Read" ref="a58c2b7013db7765a01181f928833e530" args="(string FileName)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Read</b> (string FileName)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8743f281d37cc023cd3e628eba4644f0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::Read" ref="a8743f281d37cc023cd3e628eba4644f0" args="(istream &amp;FileStream)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Read</b> (istream &amp;FileStream)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab0d4c7f4d291599c0d9cb735dd7d9dc8"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::ReadText" ref="ab0d4c7f4d291599c0d9cb735dd7d9dc8" args="(string FileName)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>ReadText</b> (string FileName)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a20de6511128d3ac679c885abcd5538d0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::ReadText" ref="a20de6511128d3ac679c885abcd5538d0" args="(istream &amp;FileStream)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>ReadText</b> (istream &amp;FileStream)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a889d3baa1c4aa6a96b83ba8ca7b4cf4a"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::Assemble" ref="a889d3baa1c4aa6a96b83ba8ca7b4cf4a" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Assemble</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa9a81eead891c0ee85f2563dc8be8a99"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::RemoveSmallEntry" ref="aa9a81eead891c0ee85f2563dc8be8a99" args="(const T0 &amp;epsilon)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>RemoveSmallEntry</b> (const T0 &amp;epsilon)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a19b48691b03953dffd387ace5cfddd37"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::SetIdentity" ref="a19b48691b03953dffd387ace5cfddd37" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetIdentity</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa6f51cb3c7779c309578058fd34c5d25"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::Zero" ref="aa6f51cb3c7779c309578058fd34c5d25" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Zero</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1fce8c713bc87ca4e06df819ced39554"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::Fill" ref="a1fce8c713bc87ca4e06df819ced39554" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Fill</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a08a5f1de809bbb4565e90d954ca053f6"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::Fill" ref="a08a5f1de809bbb4565e90d954ca053f6" args="(const complex&lt; T0 &gt; &amp;x)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Fill</b> (const complex&lt; T0 &gt; &amp;x)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad1ff9883f030ca9eaada43616865acdb"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::FillRand" ref="ad1ff9883f030ca9eaada43616865acdb" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>FillRand</b> ()</td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2032848b78edab7ef6721eda9e2784de"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::m_" ref="a2032848b78edab7ef6721eda9e2784de" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de">m_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of rows. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acfa7a2c6c7f77dc8ebcee2dd411ebf98"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::n_" ref="acfa7a2c6c7f77dc8ebcee2dd411ebf98" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98">n_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of columns. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af1766a47a3d45c0b904b07870204e4a6"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::val_real_" ref="af1766a47a3d45c0b904b07870204e4a6" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6">val_real_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">real part rows or columns <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afa77c763d3b3a280dfef2a4410ed0bd8"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::val_imag_" ref="afa77c763d3b3a280dfef2a4410ed0bd8" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8">val_imag_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">imaginary part rows or columns <br/></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Prop, class Allocator&gt;<br/>
 class Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</h3>

<p>Column-major symmetric sparse-matrix class. </p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8hxx_source.php#l00254">254</a> of file <a class="el" href="_matrix___array_complex_sparse_8hxx_source.php">Matrix_ArrayComplexSparse.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="ac0a88fcc183d15c9d86cfdde270a1d30"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::Matrix" ref="ac0a88fcc183d15c9d86cfdde270a1d30" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>Builds an empty matrix. </p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01943">1943</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad7e66b7b0a5c63e77a6607545eec07b0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::Matrix" ref="ad7e66b7b0a5c63e77a6607545eec07b0" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Constructor. </p>
<p>Builds a i by j matrix </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> values are not initialized. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01956">1956</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="a96c7b8d7f0e1e862558b9e3039a32878"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::AddInteraction" ref="a96c7b8d7f0e1e862558b9e3039a32878" args="(int i, int j, const complex&lt; T &gt; &amp;val)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::AddInteraction </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const complex&lt; T &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds a coefficient in the matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>coefficient to add. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02486">2486</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a703b16f79696af7a7d18da3c1db72671"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::AddInteractionColumn" ref="a703b16f79696af7a7d18da3c1db72671" args="(int i, int nb, const IVect &amp;row, const Vector&lt; complex&lt; T &gt;, VectFull, Alloc1 &gt; &amp;val)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Alloc1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::AddInteractionColumn </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nb</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>row</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; complex&lt; T &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds coefficients in a column. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>nb</em>&nbsp;</td><td>number of coefficients to add. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>row</em>&nbsp;</td><td>row numbers of coefficients. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>values of coefficients. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02525">2525</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6dcf362a6feaf5467c8c64d6be823fda"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::AddInteractionRow" ref="a6dcf362a6feaf5467c8c64d6be823fda" args="(int i, int nb, const IVect &amp;col, const Vector&lt; complex&lt; T &gt;, VectFull, Alloc1 &gt; &amp;val)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Alloc1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::AddInteractionRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nb</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>col</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; complex&lt; T &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds coefficients in a row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>nb</em>&nbsp;</td><td>number of coefficients to add. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>col</em>&nbsp;</td><td>column numbers of coefficients. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>values of coefficients. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02508">2508</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1285786e064b8336ba57b6c77097e4d5"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::AssembleImagColumn" ref="a1285786e064b8336ba57b6c77097e4d5" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::AssembleImagColumn </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Assembles a column. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>If you are using the methods AddInteraction, you don't need to call that method. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02472">2472</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2ddc33ea923bccadd9b07a7f6b0a8690"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::AssembleRealColumn" ref="a2ddc33ea923bccadd9b07a7f6b0a8690" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::AssembleRealColumn </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Assembles a column. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>If you are using the methods AddInteraction, you don't need to call that method. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02458">2458</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a913307297605f734e98ba7c7e870ae39"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::GetImag" ref="a913307297605f734e98ba7c7e870ae39" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T &amp; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::GetImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns access to imaginary part of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>imaginary part of element (i, j) of the matrix. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a644eff8b81954e1f43c4f9321a866903">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02184">2184</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af4a356b6428d68d9890b96494ef89cc0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::GetImag" ref="af4a356b6428d68d9890b96494ef89cc0" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const T &amp; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::GetImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns access to imaginary part element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>imaginary part of element (i, j) of the matrix. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a3233d9091b1fe40403f0e1779f260033">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02213">2213</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6cebdd4865f5c2c9e90d5b4f6961414a"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::GetImagColumnSize" ref="a6cebdd4865f5c2c9e90d5b4f6961414a" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::GetImagColumnSize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of non-zero entries of a column. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of non-zero entries of the column i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02426">2426</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9a8f3daf0ad036985c22c4772b1477aa"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::GetReal" ref="a9a8f3daf0ad036985c22c4772b1477aa" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T &amp; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::GetReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns access to real part element (i, j). </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>real part of element (i, j) of the matrix. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ac91be8b2868c034acbf161a8b3af9598">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02066">2066</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a20616f82c194e42a610e3e48e63d5a40"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::GetReal" ref="a20616f82c194e42a610e3e48e63d5a40" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const T &amp; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::GetReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns access to real part of element (i, j). </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>real part of element (i, j) of the matrix. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ae562065dff1e5df2ec2516bf7f4f56ae">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02096">2096</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab488dd8cd29a05f016d63b30e90b4f2d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::GetRealColumnSize" ref="ab488dd8cd29a05f016d63b30e90b4f2d" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::GetRealColumnSize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of non-zero entries of a column. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of non-zero entries of the column i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02413">2413</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ace196e6c26320571bf77a782833d7232"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::operator()" ref="ace196e6c26320571bf77a782833d7232" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">complex&lt; T &gt; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a30be6306dbdd79b0a282ef99289b0c2b">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01976">1976</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a460d1b0cadc5cf4915ce024be57b1e58"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::ReallocateImagColumn" ref="a460d1b0cadc5cf4915ce024be57b1e58" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::ReallocateImagColumn </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates column i. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>new number of non-zero entries in the column. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Data may be lost. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02320">2320</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a063f733a818719a8349d0de407a685a0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::ReallocateRealColumn" ref="a063f733a818719a8349d0de407a685a0" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::ReallocateRealColumn </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates column i. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>new number of non-zero entries in the column. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Data may be lost. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02306">2306</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a832f6f65f5c46c0cfaa66c0cb76e922e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::ReplaceImagIndexColumn" ref="a832f6f65f5c46c0cfaa66c0cb76e922e" args="(int i, IVect &amp;new_index)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::ReplaceImagIndexColumn </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>new_index</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets row numbers of non-zero entries of a column. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>new_index</em>&nbsp;</td><td>new row numbers. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02399">2399</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="addca588562b05576d4b37711bb4f6820"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::ReplaceRealIndexColumn" ref="addca588562b05576d4b37711bb4f6820" args="(int i, IVect &amp;new_index)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::ReplaceRealIndexColumn </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>new_index</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets row numbers of non-zero entries of a column. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>new_index</em>&nbsp;</td><td>new row numbers. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02385">2385</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a8f954de1b994af747e4e4e6c3bc2d8b9"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::ResizeImagColumn" ref="a8f954de1b994af747e4e4e6c3bc2d8b9" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::ResizeImagColumn </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates column i. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>new number of non-zero entries in the column. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02346">2346</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab21dd3904c7fb5d4673106b83b5099d4"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::ResizeRealColumn" ref="ab21dd3904c7fb5d4673106b83b5099d4" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::ResizeRealColumn </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates column i. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>new number of non-zero entries in the column. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02333">2333</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6f67461ccf260c8bb9e9fd1fc40811e7"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::Set" ref="a6f67461ccf260c8bb9e9fd1fc40811e7" args="(int i, int j, const complex&lt; T &gt; &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::Set </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const complex&lt; T &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets element (i, j) of the matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>A(i, j) = x </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2dfca2deb2a823fc64dc51b7c0728f49">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02241">2241</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7b4cf92d8be151c14508ada2b6b0b72d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::SwapImagColumn" ref="a7b4cf92d8be151c14508ada2b6b0b72d" args="(int i, int i_)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::SwapImagColumn </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Swaps two columns. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>first column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>second column number. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02372">2372</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af2f1c1dc9a20bfed30e186f8d4c7dc44"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::SwapRealColumn" ref="af2f1c1dc9a20bfed30e186f8d4c7dc44" args="(int i, int i_)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::SwapRealColumn </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Swaps two columns. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>first column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>second column number. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02359">2359</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a113a4f9a03767b54cb38c34af0d03ff7"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::ValImag" ref="a113a4f9a03767b54cb38c34af0d03ff7" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const T &amp; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::ValImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns access to imaginary part of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>imaginary part of element (i, j) of the matrix. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0324d6e453ce4c18dbc84bdc48fb4d5f">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02155">2155</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1b6113bbca815576f929be337af47483"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::ValImag" ref="a1b6113bbca815576f929be337af47483" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T &amp; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::ValImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns access to imaginary part of element (i, j). </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>imaginary part of element (i, j) of the matrix. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aee8de4c9149d47935bf84fc04727c7f2">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02125">2125</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9edb81b7ae21d2118c6cc4d98058d731"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::ValReal" ref="a9edb81b7ae21d2118c6cc4d98058d731" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T &amp; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::ValReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns access to real part of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>real part of element (i, j) of the matrix. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#abdacf01dc354d0449b16b1d9e111ec54">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02005">2005</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a69b5391fd7e911f3f3204bf627e1ed24"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;::ValReal" ref="a69b5391fd7e911f3f3204bf627e1ed24" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const T &amp; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator &gt;::ValReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns access to real part element (i, j). </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>real part of element (i, j) of the matrix. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2fa5f2b8aeb3c848f5749d113f93dce4">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02036">2036</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>matrix_sparse/<a class="el" href="_matrix___array_complex_sparse_8hxx_source.php">Matrix_ArrayComplexSparse.hxx</a></li>
<li>matrix_sparse/<a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
