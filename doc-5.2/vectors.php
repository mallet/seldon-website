<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Vectors </h1>  </div>
</div>
<div class="contents">
<h2>Definition</h2>
<p>Vectors are instances of the class <code>Vector</code>. Class <code>Vector</code> is a template class: <code>Vector&lt;T, Storage, Allocator&gt;</code>. <code>T</code> is the type of the elements to be stored (e.g. <code>double</code>). <code>Storage</code> defines how the vector is stored. <code>Storage</code> is equal to <code>VectFull</code> by default for full vectors, you can set it to <code>VectSparse</code> for sparse vectors. Finally, <code>Allocator</code> defines the way memory is managed. It is close to STL allocators. See the section "Allocators" for further details. </p>
<h2>Declaration</h2>
<p>There is a default <code>Allocator</code> (see the section "Allocators") and a default <code>Storage</code> (<code>VectFull</code>). It means that these two template parameters may be omitted. Then a vector of integers may be declared thanks to the line: </p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">Vector&lt;int&gt; V;</pre></div>  </pre><p>This defines a vector of size 0, that is to say an empty vector. To define a vector of length 5, one may write: </p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">Vector&lt;int&gt; V(5);</pre></div>  </pre><p>Other declarations may be: </p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">Vector&lt;int, VectFull&gt; V(10);
Vector&lt;int&gt; U(V); // Copy constructor.
Vector&lt;int, VectFull, MallocAlloc&lt;int&gt; &gt; V(10);
Vector&lt;int, VectFull, NewAlloc&lt;int&gt; &gt; V(10);</pre></div>  </pre><h2>Use of vectors</h2>
<p>Access to elements is achieved through the <code>operator(int)</code>, and indices start at 0: </p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">Vector&lt;int, VectFull&gt; V(10);
V(5) = -3;
V(0) = 2 * V(5);</pre></div>  </pre><p>To display vectors, there are two convenient options: </p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
V.Print();
std::cout &lt;&lt; V &lt;&lt; std::endl;
</pre></div>  </pre><p>There are lots of methods that are described in the <a href="class_vector.php">documentation</a>. One may point out:</p>
<ul>
<li>
<p class="startli"><code>Fill</code> fills with 0, 1, 2, 3, etc. or fills the vector with a given value.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>GetLength</code> or <code>GetSize</code> return the length of the vector.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>Reallocate</code> resizes the vector (warning, data may be lost, depending on the allocator).</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>Resize</code> resizes the vector while keeping previous entries. </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code> PushBack </code> inserts an element at the end of the vector. </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>Read</code>, <code>ReadText</code>, <code>Write</code>, <code>WriteText</code> are useful methods for input/ouput operations.</p>
<p class="endli"></p>
</li>
</ul>
<p>A comprehensive test of dense vectors is achieved in file <code>test/program/vector_test.cpp</code>.</p>
<p><br/>
</p>
<h1>Sparse vectors </h1>
<h2>Declaration</h2>
<p>There is a default <code>Allocator</code> (see the section "Allocators"). It means that this template parameter may be omitted. Then a sparse vector of doubles may be declared thanks to the line: </p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">Vector&lt;double, VectSparse&gt; V;</pre></div>  </pre><p>This defines a null vector, that is to say that <code> V(i) </code> will return 0 for all i. To define a vector with 5 non-zero entries, one may write: </p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">Vector&lt;double, VectSparse&gt; V(5);</pre></div>  </pre><p>Other declarations may be: </p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;double, VectSparse&gt; U(V); // Copy constructor.
Vector&lt;int, VectSparse, MallocAlloc&lt;int&gt; &gt; V(10);
Vector&lt;int, VectSparse, NewAlloc&lt;int&gt; &gt; V(10);</pre></div>  </pre><h2>Use of sparse vectors</h2>
<p>A good technique consists of modifying those vectors through methods <code>AddInteraction</code> and <code>AddInteractionRow</code>. These methods insert (or add) the elements at the correct position, so that the row numbers are always sorted in ascending order. You can use them as in the following example :</p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;double, VectSparse&gt; V;
cout &lt;&lt; "Empty vector V = " &lt;&lt; V &lt;&lt; endl;
// non-null value of 1.5 at row 2
V.AddInteraction(2, 1.5);
V.AddInteraction(2, 0.8);
// now V(2) should be equal to 1.5+0.8 = 2.3
cout &lt;&lt; "After AddInteraction, V = " &lt;&lt; V &lt;&lt; endl;

// AddInteractionRow for several rows to add
IVect num(2);
Vector&lt;double&gt; val(2);
num(0) = 1; val(0) = -0.4;
num(1) = 4; val(1) = 0.6;
V.AddInteractionRow(num, val);
cout &lt;&lt; "After AddInteractionRow, V = " &lt;&lt; endl &lt;&lt; V &lt;&lt; endl;
 </pre></div>  </pre><p>The output of this code would read :</p>
<div class="fragment"><pre class="fragment">
Empty vector V =
After AddInteraction, V = 3 2.3
AfterAddInteractionRow, V =
2 -0.4
3  2.3
5  0.6
</pre></div><p>You noticed that the row indices are displayed with indices beginning at 1 instead of 0, this is more convenient for debugging, or exporting datas into Matlab for example. </p>
<p>You can also use the <code>operator(int)</code> to modify directly values.</p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">Vector&lt;double, VectSparse&gt; V;
V(5) = -3;
V(0) = 2 * V(5);</pre></div>  </pre><p>If the non-zero entry doesn't exist, it is created at the correction position as for <code>AddInteraction</code>.</p>
<p>The methods <code>Reallocate</code>, <code>Index</code> and <code>Value</code> can also be used, but they need to be used carefully and often the method <code> Assemble </code> has to be called in order to sort non-zero entries.</p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// 3 non-zero entries
Vector&lt;double, VectSparse&gt; V;
// creation of 3 non-zero entries
V.Reallocate(3);
// initialization of non-zero entries with Index and Value
V.Index(0) = 1; // row number of first entry
V.Value(0) = 1.3; // value of first entry
V.Index(1) = 3;
V.Value(1) = -0.5;
V.Index(2) = 2;
V.Value(2) = 2.7;
// Here the row numbers are not sorted
// you need to call Assemble
V.Assemble();
</pre></div>  </pre><p>In the same way, the method <code>SetData</code> can be used in conjunction with <code>Assemble</code> :</p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;double, VectSparse&gt; V;
IVect row(3);
Vector&lt;double&gt; value(3);
row(0) = 1; // row number of first entry
value(0) = 1.3; // value of first entry
row(1) = 3;
value(1) = -0.5;
row(2) = 2;
value(2) = 2.7;
// feeding V with row and value
V.SetData(value, row);
// Here the row numbers are not sorted
// you need to call Assemble
V.Assemble();
</pre></div>  </pre><p>You may notice that the method SetData empties vectors row and value.</p>
<p>There are lots of methods that are described in the <a href="class_sparse_vector.php">documentation</a>. One may point out:</p>
<ul>
<li>
<p class="startli"><code>Fill</code> give to non-zero entries the value 0, 1, 2, 3, etc. or a given value.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>GetLength</code> or <code>GetSize</code> return the number of non-zero entries.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>Read</code>, <code>ReadText</code>, <code>Write</code>, <code>WriteText</code> are useful methods for input/ouput operations.</p>
<p class="endli"></p>
</li>
</ul>
<p>A comprehensive test of dense vector is achieved in file <code>test/program/sparse_vector_test.cpp</code>. </p>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
