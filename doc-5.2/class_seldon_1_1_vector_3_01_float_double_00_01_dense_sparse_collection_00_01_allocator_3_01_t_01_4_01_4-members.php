<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>, including all inherited members.<table>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#aae905d2dac0834a9761b86e226c8800c">AddVector</a>(const Vector&lt; float, VectFull, Allocator&lt; float &gt; &gt; &amp;)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ae53a4416febae2f6966ada61143039b9">AddVector</a>(const Vector&lt; float, VectSparse, Allocator&lt; float &gt; &gt; &amp;)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#af9abc590c45be62474179bda12c4d6ae">AddVector</a>(const Vector&lt; double, VectFull, Allocator&lt; double &gt; &gt; &amp;)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a0167a495ffd1f473678ed5d9549f7507">AddVector</a>(const Vector&lt; double, VectSparse, Allocator&lt; double &gt; &gt; &amp;)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#afcca9799bf261c112c99e806b8907884">AddVector</a>(const Vector&lt; T0, Storage0, Allocator0 &gt; &amp;, string name)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ab5cd5f6b57ca76ac3659b02fe928d23d">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a5ad322e3511725821e82e14d632b6be1">collection_</a></td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#acec939c56c4c6b8f3db9cb2d47111644">Copy</a>(const Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt; &amp;X)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>data_</b> (defined in <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ade6c65f8a6252305e86b01898eabdcd7">Deallocate</a>()</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>double_dense_c</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a0e75af5dfcebe3aa872890ee596d2120">double_dense_c_</a></td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>double_dense_v</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>double_sparse_c</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a1270cfa610abeaa07dd55863a33cff5c">double_sparse_c_</a></td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>double_sparse_v</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>float_dense_c</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#afc3abf3a8ecbed59428fe26bc70c094a">float_dense_c_</a></td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>float_dense_v</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>float_sparse_c</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a565fcd42d62a4540995fbc6df9938e37">float_sparse_c_</a></td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>float_sparse_v</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a5bdd2a3d741868fc687c2e62388dd348">GetCollectionIndex</a>() const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a">GetData</a>() const</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a454aea78c82c4317dfe4160cc7c95afa">GetDataConst</a>() const</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a0aadc006977de037eb3ee550683e3f19">GetDataConstVoid</a>() const</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#ad0f5184bd9eec6fca70c54e459ced78f">GetDataVoid</a>() const</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a48e3d61c043a065c53b41470e706dfee">GetDoubleDense</a>()</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#abfef62ba1ded1d080a8d384eef37e85f">GetDoubleDense</a>() const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a1add5fecb61b4984003fc903f5e4a71b">GetDoubleSparse</a>()</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ac499cedccfb785f37d1e1aec3869300d">GetDoubleSparse</a>() const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#addef6ea1f486b2b836b2ca54f8273ccb">GetFloatDense</a>()</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#aace5627f0d92e61ec0e99a513f4570c1">GetFloatDense</a>() const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a919fc21cc5754175f976df5e1c62f1aa">GetFloatSparse</a>()</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a93b4ed54472467eeb6529334ee4c90d2">GetFloatSparse</a>() const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ae501c34e51f6559568846b94e24c249c">GetLength</a>() const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a750b9d392301df88e3cdb76cf11f87a0">GetLengthSum</a>() const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a6f40d71a69e016ae3e1d9db92b6538a4">GetM</a>() const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a696c512cf3692f2e7193446373aa8c97">GetNvector</a>() const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a839880a3113c1885ead70d79fade0294">GetSize</a>() const</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a6872e6bac0e998a5b2e7dbf2d9b1807f">GetSubvectorIndex</a>() const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#adc5a4e9e00f74737c6dc4b69e810e26d">GetType</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>GetVector</b>(int i, float_dense_v &amp;vector) const  (defined in <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>GetVector</b>(int i, float_sparse_v &amp;vector) const  (defined in <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>GetVector</b>(int i, double_dense_v &amp;vector) const  (defined in <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>GetVector</b>(int i, double_sparse_v &amp;vector) const  (defined in <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ae15d81d9568c3d4383af9e4cf81427cd">GetVector</a>(string name, Vector&lt; T0, Storage0, Allocator0 &gt; &amp;vector) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a3c948376419d25cb11ada8d39c134c5d">GetVector</a>(int i, typename Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::float_dense_v &amp;vector) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ad2b29185865f3f2e7a4bf7e43b55c434">GetVectorLength</a>() const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ad9ae13022169e5ecb85874a9d0655b37">label_map_</a></td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a5cf22412ddb84b63dae86c18efd1d46e">label_vector_</a></td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a55b6c0b9a6ed0958ccb7414a95b40327">length_</a></td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a1589bd7ba73b1d9e32fa11b96b57d36a">length_sum_</a></td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>m_</b> (defined in <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ada438f06bc34a5a98b630f07e0e80346">Nullify</a>()</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#addbb1b1aa4add4cfc19d53304ea60fb8">Nvector_</a></td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a2bed95a4398789fdbdc0abd96b233c5e">operator()</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a429e20e3d4e359ccbdca47fd89e87d4b">operator*=</a>(const T0 &amp;X)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a5a45815f1db11248f8e963611f41d0b1">operator=</a>(const Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt; &amp;X)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#aff51ef2a12bca16732cd049b84502a87">Print</a>() const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a72272cc506d65af05eaf0d7d9e28ed0a">Read</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#afe242d0f1bdc515d83c5defd16ee4ece">Read</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a60874f5b7285ceaaad8c2d8a9e99d7f0">SetName</a>(int i, string name)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a2718fc033e69dd6e16f88848d6b02ed5">SetVector</a>(int i, const Vector&lt; float, VectFull, Allocator&lt; float &gt; &gt; &amp;)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a80412689e793d552bc92b165eb3115e9">SetVector</a>(int i, const Vector&lt; float, VectSparse, Allocator&lt; float &gt; &gt; &amp;)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a7fe916fddf26f60e915f8541258dec79">SetVector</a>(int i, const Vector&lt; double, VectFull, Allocator&lt; double &gt; &gt; &amp;)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a7f055bcede5e4036e2f06ea3a51b0829">SetVector</a>(int i, const Vector&lt; double, VectSparse, Allocator&lt; double &gt; &gt; &amp;)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a96adbda8dc019e9d87778deb12e93b39">SetVector</a>(int i, const Vector&lt; T0, Storage0, Allocator0 &gt; &amp;, string name)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#adf98501c23f89625bb971e10e44b605f">SetVector</a>(string name, const Vector&lt; T0, Storage0, Allocator0 &gt; &amp;)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>storage</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a0e24041339f74050c878a52404373649">subvector_</a></td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>value_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>vect_allocator_</b> (defined in <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a></td><td><code> [protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a95f3ecfa8b557f41194fe506d0af7d5e">Vector</a>()</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline, explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a7a13cdb2e25c02e63cfdab46413a6f96">Vector</a>(const Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt; &amp;)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#ac8a6cee506f094504138943629b9dcbb">Vector_Base</a>()</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#aa5d9ca198d4175b9baf9a0543f7376be">Vector_Base</a>(int i)</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a></td><td><code> [explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a27796125b93f3a771a5c92e8196c9229">Vector_Base</a>(const Vector_Base&lt; T, Allocator&lt; T &gt; &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#aada9ae0d211979ae3412ac43b6a6ba40">Write</a>(string FileName, bool with_size) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ac655b1bab61066b91e99570fb893f2a9">Write</a>(ostream &amp;FileStream, bool with_size) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#aedd93324292aff59f9b60d60f618e634">WriteText</a>(string FileName) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#afe9e2d5782ba7bc805eb5d47702afec9">WriteText</a>(ostream &amp;FileStream) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a89672db2ad89fd1e27987790ec0e0561">~Vector</a>()</td><td><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a6da510796ebef867765ea3a037debb1b">~Vector_Base</a>()</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a></td><td></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
