<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><b>seldon</b>      </li>
      <li><a class="el" href="classseldon_1_1_matrix_sparse_double.php">MatrixSparseDouble</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pub-attribs">Public Attributes</a>  </div>
  <div class="headertitle">
<h1>seldon::MatrixSparseDouble Class Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="seldon::MatrixSparseDouble" --><!-- doxytag: inherits="seldon::BaseMatrixSparseDouble" --><div class="dynheader">
Inheritance diagram for seldon::MatrixSparseDouble:</div>
<div class="dyncontent">
 <div class="center">
  <img src="classseldon_1_1_matrix_sparse_double.png" usemap="#seldon::MatrixSparseDouble_map" alt=""/>
  <map id="seldon::MatrixSparseDouble_map" name="seldon::MatrixSparseDouble_map">
<area href="classseldon_1_1_base_matrix_sparse_double.php" alt="seldon::BaseMatrixSparseDouble" shape="rect" coords="0,112,198,136"/>
<area href="classseldon_1_1_matrix_base_double.php" alt="seldon::MatrixBaseDouble" shape="rect" coords="0,56,198,80"/>
<area href="classseldon_1_1__object.php" alt="seldon::_object" shape="rect" coords="0,0,198,24"/>
</map>
</div>

<p><a href="classseldon_1_1_matrix_sparse_double-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afc336e6d71a8f6e2c82c930fe20e55a5"></a><!-- doxytag: member="seldon::MatrixSparseDouble::__init__" ref="afc336e6d71a8f6e2c82c930fe20e55a5" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__init__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9ef607281944ba4f95faa472c59763c1"></a><!-- doxytag: member="seldon::MatrixSparseDouble::Clear" ref="a9ef607281944ba4f95faa472c59763c1" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Clear</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ace3da5933ac814dd2256b07f41966445"></a><!-- doxytag: member="seldon::MatrixSparseDouble::SetData" ref="ace3da5933ac814dd2256b07f41966445" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetData</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9a3c9e1eb9137a711ed7c725a8c9b4c0"></a><!-- doxytag: member="seldon::MatrixSparseDouble::Nullify" ref="a9a3c9e1eb9137a711ed7c725a8c9b4c0" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Nullify</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5407c33aff4f4c42318c42286b948f43"></a><!-- doxytag: member="seldon::MatrixSparseDouble::Reallocate" ref="a5407c33aff4f4c42318c42286b948f43" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Reallocate</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3e20a5627fb95972abe8a8ee10d756d9"></a><!-- doxytag: member="seldon::MatrixSparseDouble::Resize" ref="a3e20a5627fb95972abe8a8ee10d756d9" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Resize</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a66adebc2ff20fbcbc355c97437c4341f"></a><!-- doxytag: member="seldon::MatrixSparseDouble::Copy" ref="a66adebc2ff20fbcbc355c97437c4341f" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Copy</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9f69a5bd1a2afd212d687cfc2964b27c"></a><!-- doxytag: member="seldon::MatrixSparseDouble::GetNonZeros" ref="a9f69a5bd1a2afd212d687cfc2964b27c" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetNonZeros</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acf86588cb1a6b8130ab2097b2f7c53e8"></a><!-- doxytag: member="seldon::MatrixSparseDouble::GetDataSize" ref="acf86588cb1a6b8130ab2097b2f7c53e8" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataSize</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a988893703183dcf978e6e4be8ea19511"></a><!-- doxytag: member="seldon::MatrixSparseDouble::GetPtr" ref="a988893703183dcf978e6e4be8ea19511" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetPtr</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aabb66d2be4650f2bf5bd1a6875565577"></a><!-- doxytag: member="seldon::MatrixSparseDouble::GetInd" ref="aabb66d2be4650f2bf5bd1a6875565577" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetInd</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af8826724d8599b013c3d9a3b6f91afa7"></a><!-- doxytag: member="seldon::MatrixSparseDouble::GetPtrSize" ref="af8826724d8599b013c3d9a3b6f91afa7" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetPtrSize</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0c26ec0485a76d9dd42db2c29422cf05"></a><!-- doxytag: member="seldon::MatrixSparseDouble::GetIndSize" ref="a0c26ec0485a76d9dd42db2c29422cf05" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetIndSize</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a38ec73f0effbc402b845c8d0677665b7"></a><!-- doxytag: member="seldon::MatrixSparseDouble::__call__" ref="a38ec73f0effbc402b845c8d0677665b7" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__call__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad9ae8f15317743cbfa878a4a8a0f6e21"></a><!-- doxytag: member="seldon::MatrixSparseDouble::Val" ref="ad9ae8f15317743cbfa878a4a8a0f6e21" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Val</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8cf0324ccb9131adf9beb0161a62ac6a"></a><!-- doxytag: member="seldon::MatrixSparseDouble::Get" ref="a8cf0324ccb9131adf9beb0161a62ac6a" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Get</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7108f4dc15c7496a1b8bb9c975f24e1a"></a><!-- doxytag: member="seldon::MatrixSparseDouble::AddInteraction" ref="a7108f4dc15c7496a1b8bb9c975f24e1a" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>AddInteraction</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a516e9bf39fe8337caa806abcfc581c71"></a><!-- doxytag: member="seldon::MatrixSparseDouble::Set" ref="a516e9bf39fe8337caa806abcfc581c71" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Set</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a329c53c32d2c5d74850cc339113398d5"></a><!-- doxytag: member="seldon::MatrixSparseDouble::Zero" ref="a329c53c32d2c5d74850cc339113398d5" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Zero</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a08ba043e68e94a23be6f7755f3bceaed"></a><!-- doxytag: member="seldon::MatrixSparseDouble::SetIdentity" ref="a08ba043e68e94a23be6f7755f3bceaed" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetIdentity</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0d25bf96291355e9bccc0f0a27d068aa"></a><!-- doxytag: member="seldon::MatrixSparseDouble::Fill" ref="a0d25bf96291355e9bccc0f0a27d068aa" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Fill</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a748a53221fb2e0f7a1d6368588391dfe"></a><!-- doxytag: member="seldon::MatrixSparseDouble::FillRand" ref="a748a53221fb2e0f7a1d6368588391dfe" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>FillRand</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a941886a42195477842e4e56990783bfc"></a><!-- doxytag: member="seldon::MatrixSparseDouble::Print" ref="a941886a42195477842e4e56990783bfc" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Print</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac78200e41eeea3163fc2ace06c373b9e"></a><!-- doxytag: member="seldon::MatrixSparseDouble::Write" ref="ac78200e41eeea3163fc2ace06c373b9e" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Write</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac647db13e23567e6a3db614394772cd6"></a><!-- doxytag: member="seldon::MatrixSparseDouble::WriteText" ref="ac647db13e23567e6a3db614394772cd6" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>WriteText</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a422af1f201820b8f6ec7816093b158a0"></a><!-- doxytag: member="seldon::MatrixSparseDouble::Read" ref="a422af1f201820b8f6ec7816093b158a0" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Read</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aac82e4f03717654a6e15c021f8dbef05"></a><!-- doxytag: member="seldon::MatrixSparseDouble::ReadText" ref="aac82e4f03717654a6e15c021f8dbef05" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>ReadText</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a10557b0ca5e128907518343d38f9d430"></a><!-- doxytag: member="seldon::MatrixSparseDouble::GetM" ref="a10557b0ca5e128907518343d38f9d430" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetM</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a556688675412c7c5d063cf313ad4997f"></a><!-- doxytag: member="seldon::MatrixSparseDouble::GetN" ref="a556688675412c7c5d063cf313ad4997f" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetN</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a569343e9fe5001da21c874c4ce1a11f8"></a><!-- doxytag: member="seldon::MatrixSparseDouble::GetSize" ref="a569343e9fe5001da21c874c4ce1a11f8" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetSize</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac25edf930e9247880287294c0e9d4b54"></a><!-- doxytag: member="seldon::MatrixSparseDouble::GetData" ref="ac25edf930e9247880287294c0e9d4b54" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetData</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a475019abd6e2a17c673efa0fb9bc2643"></a><!-- doxytag: member="seldon::MatrixSparseDouble::GetDataConst" ref="a475019abd6e2a17c673efa0fb9bc2643" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataConst</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a100846ac7e1a2610cf1167a654328de4"></a><!-- doxytag: member="seldon::MatrixSparseDouble::GetDataVoid" ref="a100846ac7e1a2610cf1167a654328de4" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataVoid</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6a8f262d641f7d43ffebcf4980941b78"></a><!-- doxytag: member="seldon::MatrixSparseDouble::GetDataConstVoid" ref="a6a8f262d641f7d43ffebcf4980941b78" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataConstVoid</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9d1c2383fbdcdb9afabe800635103663"></a><!-- doxytag: member="seldon::MatrixSparseDouble::GetAllocator" ref="a9d1c2383fbdcdb9afabe800635103663" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetAllocator</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-attribs"></a>
Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8fd09b52ded227e04a536b702d74c4dc"></a><!-- doxytag: member="seldon::MatrixSparseDouble::this" ref="a8fd09b52ded227e04a536b702d74c4dc" args="" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><b>this</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>

<p>Definition at line <a class="el" href="seldon_8py_source.php#l01192">1192</a> of file <a class="el" href="seldon_8py_source.php">seldon.py</a>.</p>
<hr/>The documentation for this class was generated from the following file:<ul>
<li><a class="el" href="seldon_8py_source.php">seldon.py</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
