<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="tabs2">
    <ul class="tablist">
      <li class="current"><a href="namespacemembers.php"><span>All</span></a></li>
      <li><a href="namespacemembers_func.php"><span>Functions</span></a></li>
    </ul>
  </div>
  <div class="tabs3">
    <ul class="tablist">
      <li><a href="namespacemembers.php#index_a"><span>a</span></a></li>
      <li><a href="namespacemembers_0x62.php#index_b"><span>b</span></a></li>
      <li><a href="namespacemembers_0x63.php#index_c"><span>c</span></a></li>
      <li><a href="namespacemembers_0x64.php#index_d"><span>d</span></a></li>
      <li><a href="namespacemembers_0x66.php#index_f"><span>f</span></a></li>
      <li><a href="namespacemembers_0x67.php#index_g"><span>g</span></a></li>
      <li><a href="namespacemembers_0x69.php#index_i"><span>i</span></a></li>
      <li><a href="namespacemembers_0x6c.php#index_l"><span>l</span></a></li>
      <li><a href="namespacemembers_0x6d.php#index_m"><span>m</span></a></li>
      <li><a href="namespacemembers_0x6e.php#index_n"><span>n</span></a></li>
      <li><a href="namespacemembers_0x6f.php#index_o"><span>o</span></a></li>
      <li><a href="namespacemembers_0x70.php#index_p"><span>p</span></a></li>
      <li><a href="namespacemembers_0x71.php#index_q"><span>q</span></a></li>
      <li><a href="namespacemembers_0x72.php#index_r"><span>r</span></a></li>
      <li class="current"><a href="namespacemembers_0x73.php#index_s"><span>s</span></a></li>
      <li><a href="namespacemembers_0x74.php#index_t"><span>t</span></a></li>
      <li><a href="namespacemembers_0x77.php#index_w"><span>w</span></a></li>
    </ul>
  </div>
<div class="contents">
Here is a list of all documented namespace members with links to the namespaces they belong to:

<h3><a class="anchor" id="index_s"></a>- s -</h3><ul>
<li>ScaleLeftMatrix()
: <a class="el" href="namespace_seldon.php#aac4a905258ca48097ab48b9bc6318dff">Seldon</a>
</li>
<li>ScaleMatrix()
: <a class="el" href="namespace_seldon.php#a5735705cb264df8ef1790ac3197191eb">Seldon</a>
</li>
<li>SetCol()
: <a class="el" href="namespace_seldon.php#a47489289334cd178af06072aac3e091b">Seldon</a>
</li>
<li>SetComplexOne()
: <a class="el" href="namespace_seldon.php#a53eb893740635599ec513f6c8d35921a">Seldon</a>
</li>
<li>SetComplexZero()
: <a class="el" href="namespace_seldon.php#afdb635379904bf2849441b1f322fd7a0">Seldon</a>
</li>
<li>SetRow()
: <a class="el" href="namespace_seldon.php#ae7c5e5e111b10a46344f75f1e8429a88">Seldon</a>
</li>
<li>Solve()
: <a class="el" href="namespace_seldon.php#a0e8686d1c383cedb707daaafeebb0148">Seldon</a>
</li>
<li>SolveCholesky()
: <a class="el" href="namespace_seldon.php#a1e174651f831c4d60da25d9e3e183322">Seldon</a>
</li>
<li>SolveHessenberg()
: <a class="el" href="namespace_seldon.php#adf7ec49ef745f42bdf79df966e7b32fe">Seldon</a>
</li>
<li>SolveHessenbergTwo()
: <a class="el" href="namespace_seldon.php#a843b227c9a3d11624bce323a15ee5554">Seldon</a>
</li>
<li>SolveLU()
: <a class="el" href="namespace_seldon.php#a2c4b7db2f9998750f069c9564e7d6de9">Seldon</a>
</li>
<li>SolveSylvester()
: <a class="el" href="namespace_seldon.php#aa0526895b7a4577316ede5d334d47756">Seldon</a>
</li>
<li>SOR()
: <a class="el" href="namespace_seldon.php#a88ee59bd9998a522815ba8d8d4291a2d">Seldon</a>
</li>
<li>Sort()
: <a class="el" href="namespace_seldon.php#aba061b11a3646614d19b45126af2e9b8">Seldon</a>
</li>
<li>SparseSolve()
: <a class="el" href="namespace_seldon.php#a696a4c34a72d618cf68fde030966f48e">Seldon</a>
</li>
<li>Symmlq()
: <a class="el" href="namespace_seldon.php#abb2f5e218c853b5470c744c920a2d20f">Seldon</a>
</li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
