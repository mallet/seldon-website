<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php">Vector&lt; T, Collection, Allocator &gt;</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a> &#124;
<a href="#pro-static-attribs">Static Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Vector&lt; T, Collection, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Vector&lt; T, Collection, Allocator &gt;" --><!-- doxytag: inherits="Seldon::Vector_Base" -->
<p>Structure for distributed vectors.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_vector_collection_8hxx_source.php">VectorCollection.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::Vector&lt; T, Collection, Allocator &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.png" usemap="#Seldon::Vector&lt; T, Collection, Allocator &gt;_map" alt=""/>
  <map id="Seldon::Vector&lt; T, Collection, Allocator &gt;_map" name="Seldon::Vector&lt; T, Collection, Allocator &gt;_map">
<area href="class_seldon_1_1_vector___base.php" alt="Seldon::Vector_Base&lt; T, Allocator &gt;" shape="rect" coords="0,0,251,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1651fdaf96b6730b8a016edefdf8cb43"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::value_type" ref="a1651fdaf96b6730b8a016edefdf8cb43" args="" -->
typedef T::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9aa259f7a9c7f479635bd19e68edba30"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::pointer" ref="a9aa259f7a9c7f479635bd19e68edba30" args="" -->
typedef T::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2a361cb285113aa9d0e1da58c0af8698"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::const_pointer" ref="a2a361cb285113aa9d0e1da58c0af8698" args="" -->
typedef T::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad258822209895aafd246c65d2ab0e3a5"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::reference" ref="ad258822209895aafd246c65d2ab0e3a5" args="" -->
typedef T::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aca6d14c541f951546272e2874c349bd0"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::const_reference" ref="aca6d14c541f951546272e2874c349bd0" args="" -->
typedef T::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af5c8bd2fb5c493a406ea6ef2f95706a8"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::vector_type" ref="af5c8bd2fb5c493a406ea6ef2f95706a8" args="" -->
typedef T&nbsp;</td><td class="memItemRight" valign="bottom"><b>vector_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a30eff9989a79cedf37c2c8f1dd110e84"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::vector_pointer" ref="a30eff9989a79cedf37c2c8f1dd110e84" args="" -->
typedef vector_type *&nbsp;</td><td class="memItemRight" valign="bottom"><b>vector_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a14cc687404cd911df00cf31ae7cf806c"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::const_vector_pointer" ref="a14cc687404cd911df00cf31ae7cf806c" args="" -->
typedef const vector_type *&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_vector_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab3b47f4b43acb0a1576f9e4f708c7249"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::vector_reference" ref="ab3b47f4b43acb0a1576f9e4f708c7249" args="" -->
typedef vector_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>vector_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a899c78305fe6c94cd0fb8f23ae03f6f4"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::const_vector_reference" ref="a899c78305fe6c94cd0fb8f23ae03f6f4" args="" -->
typedef const vector_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_vector_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aefee86fdac819ba912a96618756bf96c"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::collection_type" ref="aefee86fdac819ba912a96618756bf96c" args="" -->
typedef <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; vector_type, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a><br class="typebreak"/>
&lt; vector_type &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>collection_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae8fcc40cb9ca4ea5cabe92febbb54dfe"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::const_collection_type" ref="ae8fcc40cb9ca4ea5cabe92febbb54dfe" args="" -->
typedef const <a class="el" href="class_seldon_1_1_vector.php">collection_type</a>&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_collection_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab7be0e942b675da8b7f79ac93859c8bb"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::collection_reference" ref="ab7be0e942b675da8b7f79ac93859c8bb" args="" -->
typedef <a class="el" href="class_seldon_1_1_vector.php">collection_type</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>collection_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a12986d88f61f382398ac5f1ed7215b41"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::const_collection_reference" ref="a12986d88f61f382398ac5f1ed7215b41" args="" -->
typedef const <a class="el" href="class_seldon_1_1_vector.php">collection_type</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_collection_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a25009753060a0ef103d9a9a7d7b300c6"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::storage" ref="a25009753060a0ef103d9a9a7d7b300c6" args="" -->
typedef <a class="el" href="class_seldon_1_1_collection.php">Collection</a>&nbsp;</td><td class="memItemRight" valign="bottom"><b>storage</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a150f79bf5e3a4c93eb6c3c13ac8e2ea8">Vector</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Methods.  <a href="#a150f79bf5e3a4c93eb6c3c13ac8e2ea8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a3a58591088ec52b8952f40033de08a60">Vector</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Main constructor.  <a href="#a3a58591088ec52b8952f40033de08a60"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a5742670006ef46408c215455d8da7b8d">Vector</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copy constructor.  <a href="#a5742670006ef46408c215455d8da7b8d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#aeee7fe1b1be642339c63037d1245334b">~Vector</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Destructor.  <a href="#aeee7fe1b1be642339c63037d1245334b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a44d2428beb7f6d5a26921ba0158ca415">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the vector collection.  <a href="#a44d2428beb7f6d5a26921ba0158ca415"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#aa704b2926c5f2671c31b7fa9f4f85d44">Reallocate</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates the vector collection.  <a href="#aa704b2926c5f2671c31b7fa9f4f85d44"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a0110a69b7655a03280256be0746908eb">Deallocate</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the vector collection.  <a href="#a0110a69b7655a03280256be0746908eb"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 , class Storage0 , class Allocator0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#afe3074bb2edb8497f015fb06a1c32295">AddVector</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T0, Storage0, Allocator0 &gt; &amp;vector)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds a vector to the list of vectors.  <a href="#afe3074bb2edb8497f015fb06a1c32295"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 , class Storage0 , class Allocator0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a13ab25ec5db56e7dea05f4ee5cbf4cd3">AddVector</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T0, Storage0, Allocator0 &gt; &amp;vector, string name)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds a vector to the list of vectors.  <a href="#a13ab25ec5db56e7dea05f4ee5cbf4cd3"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 , class Storage0 , class Allocator0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a8d14b3a597d38152461d2de34baa0156">SetVector</a> (int i, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T0, Storage0, Allocator0 &gt; &amp;vector)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets a vector in the list of vectors.  <a href="#a8d14b3a597d38152461d2de34baa0156"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 , class Storage0 , class Allocator0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a4329365598c63f82a9426826c82bfd51">SetVector</a> (int i, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T0, Storage0, Allocator0 &gt; &amp;vector, string name)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets a vector in the list of vectors.  <a href="#a4329365598c63f82a9426826c82bfd51"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 , class Storage0 , class Allocator0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#aa82e55d926a52594fa5c1445f4a86ce2">SetVector</a> (string name, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T0, Storage0, Allocator0 &gt; &amp;vector)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets a vector in the list of vectors.  <a href="#aa82e55d926a52594fa5c1445f4a86ce2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a9727114f35983dec2b486d65bf40644f">SetName</a> (int i, string name)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the name of a given underlying vector.  <a href="#a9727114f35983dec2b486d65bf40644f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a91805dfa095ef095a620209201cdc252">SetData</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt; &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Changes the length of the collection and sets its data array.  <a href="#a91805dfa095ef095a620209201cdc252"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab2675e90bf57700a64dd1a11f6a3cda8"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::Nullify" ref="ab2675e90bf57700a64dd1a11f6a3cda8" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ab2675e90bf57700a64dd1a11f6a3cda8">Nullify</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Nullifies vectors of the collection without memory deallocation. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a9b0a8be2fb500d4f6c2edb4c1a5247ef">GetM</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the total number of elements.  <a href="#a9b0a8be2fb500d4f6c2edb4c1a5247ef"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a2a2b8d5ad39c79bdcdcd75fa20ae5ae8">GetLength</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the total number of elements.  <a href="#a2a2b8d5ad39c79bdcdcd75fa20ae5ae8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a35ec50dd32f08ecf3a8ce073a6b4bfff">GetNvector</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of aggregated vectors.  <a href="#a35ec50dd32f08ecf3a8ce073a6b4bfff"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#af8b3604aebbc0b77cb6be1548cbd7be4">GetVectorLength</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the length vector of the underlying vectors.  <a href="#af8b3604aebbc0b77cb6be1548cbd7be4"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a947fa0953ce1de93ef0de93d23ffba38">GetLengthSum</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the cumulative sum of the lengths of the underlying vectors.  <a href="#a947fa0953ce1de93ef0de93d23ffba38"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a466dd1c9fb55adfd2e40e612c8aabcdf">GetVectorIndex</a> (string name) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the vector index of the aggregated vector named <em>name</em>.  <a href="#a466dd1c9fb55adfd2e40e612c8aabcdf"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a94d2d95a9f264156a295c3b8c4181223">GetIndex</a> (string name) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the index of the first element of the aggregated vector named <em>name</em>.  <a href="#a94d2d95a9f264156a295c3b8c4181223"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">collection_reference</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ab47050d774f3ae4f7133c71de7a6f5fb">GetVector</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the list of vectors.  <a href="#ab47050d774f3ae4f7133c71de7a6f5fb"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">const_collection_reference</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#acd6707f21f36322b042319d4f34ee4c0">GetVector</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the list of vectors.  <a href="#acd6707f21f36322b042319d4f34ee4c0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">vector_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a0c8af251372300095fbba02ab95276d5">GetVector</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns one of the aggregated vectors.  <a href="#a0c8af251372300095fbba02ab95276d5"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_vector_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ab6992616430aa5bcf17bb251885c0219">GetVector</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns one of the aggregated vectors.  <a href="#ab6992616430aa5bcf17bb251885c0219"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">vector_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a303b32b6b47ed190c93988264fdc7a43">GetVector</a> (string name)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns one of the aggregated vectors.  <a href="#a303b32b6b47ed190c93988264fdc7a43"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_vector_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a00b1884bfdd116ca248368c26ef9299d">GetVector</a> (string name) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns one of the aggregated vectors.  <a href="#a00b1884bfdd116ca248368c26ef9299d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ae10c68e40c5383c9d822e8cfd2647e8f">operator()</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#ae10c68e40c5383c9d822e8cfd2647e8f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a04a16b1d67e0bc02e32d83f31eb572b4">operator()</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a04a16b1d67e0bc02e32d83f31eb572b4"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, <br class="typebreak"/>
Allocator &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ab33349edcc4ad9275540eaebc0279cc5">operator=</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt; &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a vector collection (assignment operator).  <a href="#ab33349edcc4ad9275540eaebc0279cc5"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#afc9462324875b41fae1a8ff1645d831d">Copy</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt; &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a vector collection.  <a href="#afc9462324875b41fae1a8ff1645d831d"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 , class Allocator0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#aba56b2cb47c985c93c721b7adcecf5a5">Copy</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T0, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt; &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copies the values of a full vector into the current vector.  <a href="#aba56b2cb47c985c93c721b7adcecf5a5"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, <br class="typebreak"/>
Allocator &gt; &amp;&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a2279bc596ff24069515312c1332f2d8e">operator*=</a> (const T0 &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Multiplies a vector collection by a scalar.  <a href="#a2279bc596ff24069515312c1332f2d8e"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a14ff161d14eeb23d738566ef511fa489">Fill</a> (const T0 &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the collection with a given value.  <a href="#a14ff161d14eeb23d738566ef511fa489"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7539ec920e545a23259b7fc5bcda7b25"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::Print" ref="a7539ec920e545a23259b7fc5bcda7b25" args="() const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a7539ec920e545a23259b7fc5bcda7b25">Print</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays the vector. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#adb07339ebab8b744c708221a567488f4">Write</a> (string FileName, bool with_size=true) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the inner vectors in a file.  <a href="#adb07339ebab8b744c708221a567488f4"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ac3dd17539f54c1d357d66c7587bb5bab">Write</a> (ostream &amp;FileStream, bool with_size=true) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the vector in a file stream.  <a href="#ac3dd17539f54c1d357d66c7587bb5bab"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a371264abfa98af555c5aacd43426aec2">WriteText</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the vector in a file.  <a href="#a371264abfa98af555c5aacd43426aec2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a5958fbfbb410275d2185b5b076a3e87b">WriteText</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the vector in a file stream.  <a href="#a5958fbfbb410275d2185b5b076a3e87b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a1457672aa96e240a6d32a5374f7aeffd">Read</a> (string FileName, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt; &amp;length_)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the vector from a file.  <a href="#a1457672aa96e240a6d32a5374f7aeffd"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a0ae05e39f5b40baddab006c19cda76e3">Read</a> (istream &amp;FileStream, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt; &amp;length_)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the vector from a file stream.  <a href="#a0ae05e39f5b40baddab006c19cda76e3"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a839880a3113c1885ead70d79fade0294">GetSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored.  <a href="#a839880a3113c1885ead70d79fade0294"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a">GetData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer to data_ (stored data).  <a href="#a4128ad4898e42211d22a7531c9f5f80a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a454aea78c82c4317dfe4160cc7c95afa">GetDataConst</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a const pointer to data_ (stored data).  <a href="#a454aea78c82c4317dfe4160cc7c95afa"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#ad0f5184bd9eec6fca70c54e459ced78f">GetDataVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "void*" to the data array (data_).  <a href="#ad0f5184bd9eec6fca70c54e459ced78f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a0aadc006977de037eb3ee550683e3f19">GetDataConstVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "const void*" to the data array (data_).  <a href="#a0aadc006977de037eb3ee550683e3f19"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6329bc3ea331aef38836948bc9f99e30"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::Nvector_" ref="a6329bc3ea331aef38836948bc9f99e30" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>Nvector_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a462dac93b39206c41bea643eea95a10a"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::length_" ref="a462dac93b39206c41bea643eea95a10a" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>length_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad0702f8492654a9c346d499c6c4e0a60"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::length_sum_" ref="ad0702f8492654a9c346d499c6c4e0a60" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>length_sum_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="add07245e21e95d1cfcb2de84f3d1b987"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::vector_" ref="add07245e21e95d1cfcb2de84f3d1b987" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">collection_type</a>&nbsp;</td><td class="memItemRight" valign="bottom"><b>vector_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a03d0c667f3a6eb4034ab75a1dd133a58"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::label_map_" ref="a03d0c667f3a6eb4034ab75a1dd133a58" args="" -->
map&lt; string, int &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a03d0c667f3a6eb4034ab75a1dd133a58">label_map_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Indexes of the inner vectors that have a name. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad0185be3cff90de10f820787aafe58ba"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::label_vector_" ref="ad0185be3cff90de10f820787aafe58ba" args="" -->
vector&lt; string &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ad0185be3cff90de10f820787aafe58ba">label_vector_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Names associated with the inner vectors. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac58514f2c122d7538dfc4db4ce0d4da9"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::m_" ref="ac58514f2c122d7538dfc4db4ce0d4da9" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>m_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1a6ae822c00f9ee2a798d54a36602430"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::data_" ref="a1a6ae822c00f9ee2a798d54a36602430" args="" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-static-attribs"></a>
Static Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6c958ad29a5f7eabd1f085e50046ab85"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::vect_allocator_" ref="a6c958ad29a5f7eabd1f085e50046ab85" args="" -->
static Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>vect_allocator_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Allocator&gt;<br/>
 class Seldon::Vector&lt; T, Collection, Allocator &gt;</h3>

<p>Structure for distributed vectors. </p>

<p>Definition at line <a class="el" href="_vector_collection_8hxx_source.php#l00038">38</a> of file <a class="el" href="_vector_collection_8hxx_source.php">VectorCollection.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="a150f79bf5e3a4c93eb6c3c13ac8e2ea8"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::Vector" ref="a150f79bf5e3a4c93eb6c3c13ac8e2ea8" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_vector.php">Vector</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline, explicit]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Methods. </p>
<p>Default constructor.</p>
<p>Nothing is allocated. The vector length is set to zero. </p>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00046">46</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3a58591088ec52b8952f40033de08a60"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::Vector" ref="a3a58591088ec52b8952f40033de08a60" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_vector.php">Vector</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [explicit]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Main constructor. </p>
<p>Builds a vector collection of a given size. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>length of the vector. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00058">58</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5742670006ef46408c215455d8da7b8d"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::Vector" ref="a5742670006ef46408c215455d8da7b8d" args="(const Vector&lt; T, Collection, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_vector.php">Vector</a> </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>V</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Copy constructor. </p>
<p>Builds a copy of a vector collection. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>V</em>&nbsp;</td><td>vector collection to be copied. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00074">74</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aeee7fe1b1be642339c63037d1245334b"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::~Vector" ref="aeee7fe1b1be642339c63037d1245334b" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::~<a class="el" href="class_seldon_1_1_vector.php">Vector</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Destructor. </p>
<p>The inner vectors are nullified so that their memory blocks should not be deallocated. </p>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00091">91</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="afe3074bb2edb8497f015fb06a1c32295"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::AddVector" ref="afe3074bb2edb8497f015fb06a1c32295" args="(const Vector&lt; T0, Storage0, Allocator0 &gt; &amp;vector)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 , class Storage0 , class Allocator0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::AddVector </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T0, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>vector</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds a vector to the list of vectors. </p>
<p>The vector is "appended" to the existing data. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>vector</em>&nbsp;</td><td>vector to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00167">167</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a13ab25ec5db56e7dea05f4ee5cbf4cd3"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::AddVector" ref="a13ab25ec5db56e7dea05f4ee5cbf4cd3" args="(const Vector&lt; T0, Storage0, Allocator0 &gt; &amp;vector, string name)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 , class Storage0 , class Allocator0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::AddVector </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T0, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>vector</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>name</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds a vector to the list of vectors. </p>
<p>The vector is "appended" to the existing data. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>vector</em>&nbsp;</td><td>vector to be appended. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>name</em>&nbsp;</td><td>name of the vector to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00198">198</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a44d2428beb7f6d5a26921ba0158ca415"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::Clear" ref="a44d2428beb7f6d5a26921ba0158ca415" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::Clear </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the vector collection. </p>
<p>The inner vectors are nullified so that their memory blocks should not be deallocated. </p>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00105">105</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="afc9462324875b41fae1a8ff1645d831d"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::Copy" ref="afc9462324875b41fae1a8ff1645d831d" args="(const Vector&lt; T, Collection, Allocator &gt; &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::Copy </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a vector collection. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>X</em>&nbsp;</td><td>vector collection to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'X' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00586">586</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aba56b2cb47c985c93c721b7adcecf5a5"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::Copy" ref="aba56b2cb47c985c93c721b7adcecf5a5" args="(const Vector&lt; T0, VectFull, Allocator0 &gt; &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 , class Allocator0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::Copy </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T0, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Copies the values of a full vector into the current vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>X</em>&nbsp;</td><td>full vector to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'X' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00605">605</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0110a69b7655a03280256be0746908eb"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::Deallocate" ref="a0110a69b7655a03280256be0746908eb" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::Deallocate </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the vector collection. </p>
<p>The inner vectors are cleared and the memory blocks are deallocated. </p>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00141">141</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a14ff161d14eeb23d738566ef511fa489"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::Fill" ref="a14ff161d14eeb23d738566ef511fa489" args="(const T0 &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::Fill </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the collection with a given value. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>value to fill the collection with. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00647">647</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a4128ad4898e42211d22a7531c9f5f80a"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::GetData" ref="a4128ad4898e42211d22a7531c9f5f80a" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector___base.php">Vector_Base</a>&lt; T, Allocator &gt;::pointer <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer to data_ (stored data). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer to the data_, i.e. the data array. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00156">156</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a454aea78c82c4317dfe4160cc7c95afa"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::GetDataConst" ref="a454aea78c82c4317dfe4160cc7c95afa" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector___base.php">Vector_Base</a>&lt; T, Allocator &gt;::const_pointer <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetDataConst </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a const pointer to data_ (stored data). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer to the data_, i.e. the data array. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00168">168</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0aadc006977de037eb3ee550683e3f19"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::GetDataConstVoid" ref="a0aadc006977de037eb3ee550683e3f19" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const void * <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetDataConstVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "const void*" to the data array (data_). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer of type "const void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00191">191</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad0f5184bd9eec6fca70c54e459ced78f"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::GetDataVoid" ref="ad0f5184bd9eec6fca70c54e459ced78f" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void * <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetDataVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "void*" to the data array (data_). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00180">180</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a94d2d95a9f264156a295c3b8c4181223"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::GetIndex" ref="a94d2d95a9f264156a295c3b8c4181223" args="(string name) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::GetIndex </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>name</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the index of the first element of the aggregated vector named <em>name</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>name</em>&nbsp;</td><td>name of the aggregated vector. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The index of the first element of the aggregated vector. </dd></dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00410">410</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2a2b8d5ad39c79bdcdcd75fa20ae5ae8"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::GetLength" ref="a2a2b8d5ad39c79bdcdcd75fa20ae5ae8" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::GetLength </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the total number of elements. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The total length of the vector. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_vector___base.php#a075daae3607a4767a77a3de60ab30ba7">Seldon::Vector_Base&lt; T, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00345">345</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a947fa0953ce1de93ef0de93d23ffba38"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::GetLengthSum" ref="a947fa0953ce1de93ef0de93d23ffba38" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt; &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::GetLengthSum </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the cumulative sum of the lengths of the underlying vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The cumulative sum of the lengths of the underlying vectors. </dd></dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00380">380</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9b0a8be2fb500d4f6c2edb4c1a5247ef"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::GetM" ref="a9b0a8be2fb500d4f6c2edb4c1a5247ef" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the total number of elements. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The total length of the vector. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae">Seldon::Vector_Base&lt; T, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00334">334</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a35ec50dd32f08ecf3a8ce073a6b4bfff"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::GetNvector" ref="a35ec50dd32f08ecf3a8ce073a6b4bfff" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::GetNvector </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of aggregated vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The total number of aggregated vectors. </dd></dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00356">356</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a839880a3113c1885ead70d79fade0294"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::GetSize" ref="a839880a3113c1885ead70d79fade0294" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the vector stored. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00144">144</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0c8af251372300095fbba02ab95276d5"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::GetVector" ref="a0c8af251372300095fbba02ab95276d5" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::vector_reference <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::GetVector </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns one of the aggregated vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>the index of the vector to be returned. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The <em>i</em> th aggregated vector. </dd></dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00452">452</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab6992616430aa5bcf17bb251885c0219"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::GetVector" ref="ab6992616430aa5bcf17bb251885c0219" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::const_vector_reference <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::GetVector </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns one of the aggregated vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>the index of the vector to be returned. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The <em>i</em> th aggregated vector. </dd></dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00466">466</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a303b32b6b47ed190c93988264fdc7a43"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::GetVector" ref="a303b32b6b47ed190c93988264fdc7a43" args="(string name)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::vector_reference <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::GetVector </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>name</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns one of the aggregated vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>name</em>&nbsp;</td><td>the name of the vector to be returned. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The aggregated vector named <em>name</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00479">479</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a00b1884bfdd116ca248368c26ef9299d"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::GetVector" ref="a00b1884bfdd116ca248368c26ef9299d" args="(string name) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::const_vector_reference <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::GetVector </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>name</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns one of the aggregated vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>name</em>&nbsp;</td><td>the name of the vector to be returned. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The aggregated vector named <em>name</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00498">498</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab47050d774f3ae4f7133c71de7a6f5fb"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::GetVector" ref="ab47050d774f3ae4f7133c71de7a6f5fb" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_vector.php">collection_reference</a> <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::GetVector </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the list of vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The list of the aggregated vectors. </dd></dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00427">427</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="acd6707f21f36322b042319d4f34ee4c0"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::GetVector" ref="acd6707f21f36322b042319d4f34ee4c0" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_vector.php">const_collection_reference</a> <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::GetVector </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the list of vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The list of the aggregated vectors. </dd></dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00439">439</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a466dd1c9fb55adfd2e40e612c8aabcdf"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::GetVectorIndex" ref="a466dd1c9fb55adfd2e40e612c8aabcdf" args="(string name) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::GetVectorIndex </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>name</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the vector index of the aggregated vector named <em>name</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>name</em>&nbsp;</td><td>name of the aggregated vector. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The vector index of the aggregated vector. </dd></dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00393">393</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af8b3604aebbc0b77cb6be1548cbd7be4"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::GetVectorLength" ref="af8b3604aebbc0b77cb6be1548cbd7be4" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt; &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::GetVectorLength </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the length vector of the underlying vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The lengths of the underlying vectors. </dd></dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00368">368</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae10c68e40c5383c9d822e8cfd2647e8f"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::operator()" ref="ae10c68e40c5383c9d822e8cfd2647e8f" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::const_reference <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The value of the vector at 'i'. </dd></dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00545">545</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a04a16b1d67e0bc02e32d83f31eb572b4"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::operator()" ref="a04a16b1d67e0bc02e32d83f31eb572b4" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::reference <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The value of the vector at 'i'. </dd></dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00521">521</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2279bc596ff24069515312c1332f2d8e"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::operator*=" ref="a2279bc596ff24069515312c1332f2d8e" args="(const T0 &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::operator*= </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>alpha</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Multiplies a vector collection by a scalar. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>alpha</em>&nbsp;</td><td>scalar. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00627">627</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab33349edcc4ad9275540eaebc0279cc5"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::operator=" ref="ab33349edcc4ad9275540eaebc0279cc5" args="(const Vector&lt; T, Collection, Allocator &gt; &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a vector collection (assignment operator). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>X</em>&nbsp;</td><td>vector collection to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'X' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00571">571</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1457672aa96e240a6d32a5374f7aeffd"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::Read" ref="a1457672aa96e240a6d32a5374f7aeffd" args="(string FileName, Vector&lt; int, VectFull, MallocAlloc&lt; int &gt; &gt; &amp;length_)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>length</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the vector from a file. </p>
<p>Sets the vector according to a binary file that stores the length of the vector (integer) and all elements. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00798">798</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0ae05e39f5b40baddab006c19cda76e3"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::Read" ref="a0ae05e39f5b40baddab006c19cda76e3" args="(istream &amp;FileStream, Vector&lt; int, VectFull, MallocAlloc&lt; int &gt; &gt; &amp;length_)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>length</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the vector from a file stream. </p>
<p>Sets the vector according to a binary file stream that stores the length of the vector (integer) and all elements. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>file stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00824">824</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa704b2926c5f2671c31b7fa9f4f85d44"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::Reallocate" ref="aa704b2926c5f2671c31b7fa9f4f85d44" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::Reallocate </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates the vector collection. </p>
<p>This method first clears the collection. Then it allocates a new vector of size <em>i</em>, and puts this vector in the collection. On exit, the collection is only composed of this vector of size <em>i</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>new size. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00126">126</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a91805dfa095ef095a620209201cdc252"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::SetData" ref="a91805dfa095ef095a620209201cdc252" args="(const Vector&lt; T, Collection, Allocator &gt; &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::SetData </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Changes the length of the collection and sets its data array. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>X</em>&nbsp;</td><td>collection with the vectors to which the current collection will point on exit. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00303">303</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9727114f35983dec2b486d65bf40644f"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::SetName" ref="a9727114f35983dec2b486d65bf40644f" args="(int i, string name)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::SetName </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>name</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the name of a given underlying vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>a given index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>name</em>&nbsp;</td><td>name of the underlying vector. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00274">274</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a4329365598c63f82a9426826c82bfd51"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::SetVector" ref="a4329365598c63f82a9426826c82bfd51" args="(int i, const Vector&lt; T0, Storage0, Allocator0 &gt; &amp;vector, string name)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 , class Storage0 , class Allocator0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::SetVector </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T0, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>vector</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>name</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets a vector in the list of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the vector to be set. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>vector</em>&nbsp;</td><td>vector to which the <em>i</em> th vector is set. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>name</em>&nbsp;</td><td>new name of the <em>i</em> th vector. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00239">239</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a8d14b3a597d38152461d2de34baa0156"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::SetVector" ref="a8d14b3a597d38152461d2de34baa0156" args="(int i, const Vector&lt; T0, Storage0, Allocator0 &gt; &amp;vector)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 , class Storage0 , class Allocator0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::SetVector </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T0, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>vector</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets a vector in the list of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the vector to be set. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>vector</em>&nbsp;</td><td>new value of the vector. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00215">215</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa82e55d926a52594fa5c1445f4a86ce2"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::SetVector" ref="aa82e55d926a52594fa5c1445f4a86ce2" args="(string name, const Vector&lt; T0, Storage0, Allocator0 &gt; &amp;vector)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 , class Storage0 , class Allocator0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::SetVector </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>name</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T0, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>vector</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets a vector in the list of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>name</em>&nbsp;</td><td>name of the vector to be set. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>vector</em>&nbsp;</td><td>new value of the vector. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00255">255</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac3dd17539f54c1d357d66c7587bb5bab"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::Write" ref="ac3dd17539f54c1d357d66c7587bb5bab" args="(ostream &amp;FileStream, bool with_size=true) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the vector in a file stream. </p>
<p>The length of the vector (integer) and all elements of the vector are stored in binary format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>file stream. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the length of the vector is not saved. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00706">706</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="adb07339ebab8b744c708221a567488f4"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::Write" ref="adb07339ebab8b744c708221a567488f4" args="(string FileName, bool with_size=true) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the inner vectors in a file. </p>
<p>The length of the vector (integer) and all elements of the vector are stored in binary format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>file name. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the length of the vector is not saved. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00679">679</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5958fbfbb410275d2185b5b076a3e87b"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::WriteText" ref="a5958fbfbb410275d2185b5b076a3e87b" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the vector in a file stream. </p>
<p>All elements of the vector are stored in text format. The length is not stored. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>file stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00768">768</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a371264abfa98af555c5aacd43426aec2"></a><!-- doxytag: member="Seldon::Vector&lt; T, Collection, Allocator &gt;::WriteText" ref="a371264abfa98af555c5aacd43426aec2" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the vector in a file. </p>
<p>All elements of the vector are stored in text format. The length is not stored. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_collection_8cxx_source.php#l00740">740</a> of file <a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>vector/<a class="el" href="_vector_collection_8hxx_source.php">VectorCollection.hxx</a></li>
<li>vector/<a class="el" href="_vector_collection_8cxx_source.php">VectorCollection.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
