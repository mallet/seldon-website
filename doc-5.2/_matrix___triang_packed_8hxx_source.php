<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix/Matrix_TriangPacked.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="comment">// To be included by Seldon.hxx</span>
<a name="l00022"></a>00022 
<a name="l00023"></a>00023 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_TRIANGPACKED_HXX</span>
<a name="l00024"></a>00024 <span class="preprocessor"></span>
<a name="l00025"></a>00025 <span class="preprocessor">#include &quot;../share/Common.hxx&quot;</span>
<a name="l00026"></a>00026 <span class="preprocessor">#include &quot;../share/Properties.hxx&quot;</span>
<a name="l00027"></a>00027 <span class="preprocessor">#include &quot;../share/Storage.hxx&quot;</span>
<a name="l00028"></a>00028 <span class="preprocessor">#include &quot;../share/Errors.hxx&quot;</span>
<a name="l00029"></a>00029 <span class="preprocessor">#include &quot;../share/Allocator.hxx&quot;</span>
<a name="l00030"></a>00030 
<a name="l00031"></a>00031 <span class="keyword">namespace </span>Seldon
<a name="l00032"></a>00032 {
<a name="l00033"></a>00033 
<a name="l00034"></a>00034 
<a name="l00036"></a>00036   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Storage,
<a name="l00037"></a>00037             <span class="keyword">class </span>Allocator = SELDON_DEFAULT_ALLOCATOR&lt;T&gt; &gt;
<a name="l00038"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php">00038</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked</a>: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;
<a name="l00039"></a>00039   {
<a name="l00040"></a>00040     <span class="comment">// typedef declaration.</span>
<a name="l00041"></a>00041   <span class="keyword">public</span>:
<a name="l00042"></a>00042     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00043"></a>00043     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::pointer pointer;
<a name="l00044"></a>00044     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_pointer const_pointer;
<a name="l00045"></a>00045     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::reference reference;
<a name="l00046"></a>00046     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_reference const_reference;
<a name="l00047"></a>00047     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type entry_type;
<a name="l00048"></a>00048     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::reference access_type;
<a name="l00049"></a>00049     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_reference const_access_type;
<a name="l00050"></a>00050 
<a name="l00051"></a>00051     <span class="comment">// Attributes.</span>
<a name="l00052"></a>00052   <span class="keyword">protected</span>:
<a name="l00053"></a>00053 
<a name="l00054"></a>00054     <span class="comment">// Methods.</span>
<a name="l00055"></a>00055   <span class="keyword">public</span>:
<a name="l00056"></a>00056     <span class="comment">// Constructor.</span>
<a name="l00057"></a>00057     <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ad9698cd3163185b5794550f516224d65" title="Default constructor.">Matrix_TriangPacked</a>();
<a name="l00058"></a>00058     <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ad9698cd3163185b5794550f516224d65" title="Default constructor.">Matrix_TriangPacked</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j = 0);
<a name="l00059"></a>00059     <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ad9698cd3163185b5794550f516224d65" title="Default constructor.">Matrix_TriangPacked</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked</a>&lt;T, Prop, Storage,
<a name="l00060"></a>00060                         Allocator&gt;&amp; A);
<a name="l00061"></a>00061 
<a name="l00062"></a>00062     <span class="comment">// Destructor.</span>
<a name="l00063"></a>00063     <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a3e4295fa0a1e9a2f3eba8bdda1221565" title="Destructor.">~Matrix_TriangPacked</a>();
<a name="l00064"></a>00064     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a6f0db73087115429868eaf0a35cf213b" title="Clears the matrix.">Clear</a>();
<a name="l00065"></a>00065 
<a name="l00066"></a>00066     <span class="comment">// Basic methods.</span>
<a name="l00067"></a>00067     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a9d331415858c046a3a72699d2bbd8ceb" title="Returns the number of elements stored in memory.">GetDataSize</a>() <span class="keyword">const</span>;
<a name="l00068"></a>00068 
<a name="l00069"></a>00069     <span class="comment">// Memory management.</span>
<a name="l00070"></a>00070     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ab0a798e9e9eac81e73e3176e8a4f20ac" title="Reallocates memory to resize the matrix.">Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00071"></a>00071     <span class="keywordtype">void</span> SetData(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, pointer data);
<a name="l00072"></a>00072     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a9aa8d70640c7e035ca45aa406c8e6bd1" title="Clears the matrix without releasing memory.">Nullify</a>();
<a name="l00073"></a>00073 
<a name="l00074"></a>00074     <span class="comment">// Element access and affectation.</span>
<a name="l00075"></a>00075     value_type <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#aa72fab293552e87ee528d11b04d361bf" title="Access operator.">operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00076"></a>00076     reference <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a0d4ea94aa72456c633fd26385f28f7a2" title="Direct access method.">Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00077"></a>00077     const_reference <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a0d4ea94aa72456c633fd26385f28f7a2" title="Direct access method.">Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00078"></a>00078     reference <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#aa47392b447a0c718d29050d204d5d465" title="Returns the element (i, j).">Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00079"></a>00079     const_reference <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#aa47392b447a0c718d29050d204d5d465" title="Returns the element (i, j).">Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00080"></a>00080     reference <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a086500c5914460971f1a7c4d48c5f5b4" title="Access to elements of the data array.">operator[] </a>(<span class="keywordtype">int</span> i);
<a name="l00081"></a>00081     const_reference <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a086500c5914460971f1a7c4d48c5f5b4" title="Access to elements of the data array.">operator[] </a>(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00082"></a>00082     <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00083"></a>00083     <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ac8f8519e62ace1acd0e0c514d080471a" title="Duplicates a matrix (assignment operator).">operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A);
<a name="l00084"></a>00084     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a600ae1e3aaa10e9e1d4732d69a027ae1" title="Sets an element of the matrix.">Set</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; x);
<a name="l00085"></a>00085     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ac3fcc459520d0ec52a599a8def09c4fc" title="Duplicates a matrix.">Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A);
<a name="l00086"></a>00086 
<a name="l00087"></a>00087     <span class="comment">// Convenient functions.</span>
<a name="l00088"></a>00088     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a9944e918b0b65d7fc51c5b03ac772635" title="Sets all elements to zero.">Zero</a>();
<a name="l00089"></a>00089     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a888b8dc8faeb5654fda9a0517cea0c7b" title="Sets the matrix to the identity.">SetIdentity</a>();
<a name="l00090"></a>00090     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a5e66e9e50e3ab69c85be605d69f287e3" title="Fills the matrix with 0, 1, 2, ...">Fill</a>();
<a name="l00091"></a>00091     <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00092"></a>00092     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a5e66e9e50e3ab69c85be605d69f287e3" title="Fills the matrix with 0, 1, 2, ...">Fill</a>(<span class="keyword">const</span> T0&amp; x);
<a name="l00093"></a>00093     <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00094"></a>00094     <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>&amp; <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ac8f8519e62ace1acd0e0c514d080471a" title="Duplicates a matrix (assignment operator).">operator= </a>(<span class="keyword">const</span> T0&amp; x);
<a name="l00095"></a>00095     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ae77c0cd9d596c959b2be5e9b78750e65" title="Fills the matrix randomly.">FillRand</a>();
<a name="l00096"></a>00096     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a931e21546d023c42baf9c2d6cfddb3ee" title="Displays the matrix on the standard output.">Print</a>() <span class="keyword">const</span>;
<a name="l00097"></a>00097     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a931e21546d023c42baf9c2d6cfddb3ee" title="Displays the matrix on the standard output.">Print</a>(<span class="keywordtype">int</span> a, <span class="keywordtype">int</span> b, <span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n) <span class="keyword">const</span>;
<a name="l00098"></a>00098     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a931e21546d023c42baf9c2d6cfddb3ee" title="Displays the matrix on the standard output.">Print</a>(<span class="keywordtype">int</span> l) <span class="keyword">const</span>;
<a name="l00099"></a>00099 
<a name="l00100"></a>00100     <span class="comment">// Input/output functions.</span>
<a name="l00101"></a>00101     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ae5e50dae49d4cc0a56532151206184c8" title="Writes the matrix in a file.">Write</a>(<span class="keywordtype">string</span> FileName) <span class="keyword">const</span>;
<a name="l00102"></a>00102     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ae5e50dae49d4cc0a56532151206184c8" title="Writes the matrix in a file.">Write</a>(ostream&amp; FileStream) <span class="keyword">const</span>;
<a name="l00103"></a>00103     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ad2d7a4bde7fea4f6dbbaabb4adb8c21c" title="Writes the matrix in a file.">WriteText</a>(<span class="keywordtype">string</span> FileName) <span class="keyword">const</span>;
<a name="l00104"></a>00104     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ad2d7a4bde7fea4f6dbbaabb4adb8c21c" title="Writes the matrix in a file.">WriteText</a>(ostream&amp; FileStream) <span class="keyword">const</span>;
<a name="l00105"></a>00105     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a96f1f584b5d04f9b6c92647c1dc2b353" title="Reads the matrix from a file.">Read</a>(<span class="keywordtype">string</span> FileName);
<a name="l00106"></a>00106     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a96f1f584b5d04f9b6c92647c1dc2b353" title="Reads the matrix from a file.">Read</a>(istream&amp; FileStream);
<a name="l00107"></a>00107     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ae204d32dd619e2241945cc3bfbebc018" title="Reads the matrix from a file.">ReadText</a>(<span class="keywordtype">string</span> FileName);
<a name="l00108"></a>00108     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ae204d32dd619e2241945cc3bfbebc018" title="Reads the matrix from a file.">ReadText</a>(istream&amp; FileStream);
<a name="l00109"></a>00109 
<a name="l00110"></a>00110   };
<a name="l00111"></a>00111 
<a name="l00112"></a>00112 
<a name="l00114"></a>00114   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00115"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_packed_00_01_allocator_01_4.php">00115</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_up_triang_packed.php">ColUpTriangPacked</a>, Allocator&gt;:
<a name="l00116"></a>00116     <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked</a>&lt;T, Prop, ColUpTriangPacked, Allocator&gt;
<a name="l00117"></a>00117   {
<a name="l00118"></a>00118     <span class="comment">// typedef declaration.</span>
<a name="l00119"></a>00119   <span class="keyword">public</span>:
<a name="l00120"></a>00120     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00121"></a>00121     <span class="keyword">typedef</span> Prop property;
<a name="l00122"></a>00122     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_col_up_triang_packed.php">ColUpTriangPacked</a> <a class="code" href="class_seldon_1_1_col_up_triang_packed.php">storage</a>;
<a name="l00123"></a>00123     <span class="keyword">typedef</span> Allocator allocator;
<a name="l00124"></a>00124 
<a name="l00125"></a>00125   <span class="keyword">public</span>:
<a name="l00126"></a>00126     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>();
<a name="l00127"></a>00127     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j = 0);
<a name="l00128"></a>00128     <span class="keywordtype">void</span> Resize(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00129"></a>00129 
<a name="l00130"></a>00130     <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00131"></a>00131     <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_packed_00_01_allocator_01_4.php" title="Column-major upper-triangular packed matrix class.">Matrix&lt;T, Prop, ColUpTriangPacked, Allocator&gt;</a>&amp; operator= (<span class="keyword">const</span> T0&amp; x);
<a name="l00132"></a>00132     <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_packed_00_01_allocator_01_4.php" title="Column-major upper-triangular packed matrix class.">Matrix&lt;T, Prop, ColUpTriangPacked, Allocator&gt;</a>&amp;
<a name="l00133"></a>00133     operator= (<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_packed_00_01_allocator_01_4.php" title="Column-major upper-triangular packed matrix class.">Matrix&lt;T, Prop, ColUpTriangPacked, Allocator&gt;</a>&amp; A);
<a name="l00134"></a>00134     <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00135"></a>00135     <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_packed_00_01_allocator_01_4.php" title="Column-major upper-triangular packed matrix class.">Matrix&lt;T, Prop, ColUpTriangPacked, Allocator&gt;</a>&amp; operator*= (<span class="keyword">const</span> T0&amp; x);
<a name="l00136"></a>00136   };
<a name="l00137"></a>00137 
<a name="l00138"></a>00138 
<a name="l00140"></a>00140   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00141"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_packed_00_01_allocator_01_4.php">00141</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_lo_triang_packed.php">ColLoTriangPacked</a>, Allocator&gt;:
<a name="l00142"></a>00142     <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked</a>&lt;T, Prop, ColLoTriangPacked, Allocator&gt;
<a name="l00143"></a>00143   {
<a name="l00144"></a>00144     <span class="comment">// typedef declaration.</span>
<a name="l00145"></a>00145   <span class="keyword">public</span>:
<a name="l00146"></a>00146     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00147"></a>00147     <span class="keyword">typedef</span> Prop property;
<a name="l00148"></a>00148     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_col_lo_triang_packed.php">ColLoTriangPacked</a> <a class="code" href="class_seldon_1_1_col_lo_triang_packed.php">storage</a>;
<a name="l00149"></a>00149     <span class="keyword">typedef</span> Allocator allocator;
<a name="l00150"></a>00150 
<a name="l00151"></a>00151   <span class="keyword">public</span>:
<a name="l00152"></a>00152     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>();
<a name="l00153"></a>00153     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j = 0);
<a name="l00154"></a>00154     <span class="keywordtype">void</span> Resize(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00155"></a>00155 
<a name="l00156"></a>00156     <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00157"></a>00157     <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_packed_00_01_allocator_01_4.php" title="Column-major lower-triangular packed matrix class.">Matrix&lt;T, Prop, ColLoTriangPacked, Allocator&gt;</a>&amp; operator= (<span class="keyword">const</span> T0&amp; x);
<a name="l00158"></a>00158     <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_packed_00_01_allocator_01_4.php" title="Column-major lower-triangular packed matrix class.">Matrix&lt;T, Prop, ColLoTriangPacked, Allocator&gt;</a>&amp;
<a name="l00159"></a>00159     operator= (<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_packed_00_01_allocator_01_4.php" title="Column-major lower-triangular packed matrix class.">Matrix&lt;T, Prop, ColLoTriangPacked, Allocator&gt;</a>&amp; A);
<a name="l00160"></a>00160     <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l00161"></a>00161     <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_packed_00_01_allocator_01_4.php" title="Column-major lower-triangular packed matrix class.">Matrix&lt;T, Prop, ColLoTriangPacked, Allocator&gt;</a>&amp; operator*= (<span class="keyword">const</span> T0&amp; x);
<a name="l00162"></a>00162   };
<a name="l00163"></a>00163 
<a name="l00164"></a>00164 
<a name="l00166"></a>00166   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00167"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_packed_00_01_allocator_01_4.php">00167</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_up_triang_packed.php">RowUpTriangPacked</a>, Allocator&gt;:
<a name="l00168"></a>00168     <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked</a>&lt;T, Prop, RowUpTriangPacked, Allocator&gt;
<a name="l00169"></a>00169   {
<a name="l00170"></a>00170     <span class="comment">// typedef declaration.</span>
<a name="l00171"></a>00171   <span class="keyword">public</span>:
<a name="l00172"></a>00172     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00173"></a>00173     <span class="keyword">typedef</span> Prop property;
<a name="l00174"></a>00174     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_row_up_triang_packed.php">RowUpTriangPacked</a> <a class="code" href="class_seldon_1_1_row_up_triang_packed.php">storage</a>;
<a name="l00175"></a>00175     <span class="keyword">typedef</span> Allocator allocator;
<a name="l00176"></a>00176 
<a name="l00177"></a>00177   <span class="keyword">public</span>:
<a name="l00178"></a>00178     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>();
<a name="l00179"></a>00179     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j = 0);
<a name="l00180"></a>00180     <span class="keywordtype">void</span> Resize(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00181"></a>00181 
<a name="l00182"></a>00182     <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00183"></a>00183     <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_packed_00_01_allocator_01_4.php" title="Row-major upper-triangular packed matrix class.">Matrix&lt;T, Prop, RowUpTriangPacked, Allocator&gt;</a>&amp; operator= (<span class="keyword">const</span> T0&amp; x);
<a name="l00184"></a>00184     <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_packed_00_01_allocator_01_4.php" title="Row-major upper-triangular packed matrix class.">Matrix&lt;T, Prop, RowUpTriangPacked, Allocator&gt;</a>&amp;
<a name="l00185"></a>00185     operator= (<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_packed_00_01_allocator_01_4.php" title="Row-major upper-triangular packed matrix class.">Matrix&lt;T, Prop, RowUpTriangPacked, Allocator&gt;</a>&amp; A);
<a name="l00186"></a>00186     <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l00187"></a>00187     <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_packed_00_01_allocator_01_4.php" title="Row-major upper-triangular packed matrix class.">Matrix&lt;T, Prop, RowUpTriangPacked, Allocator&gt;</a>&amp; operator*= (<span class="keyword">const</span> T0&amp; x);
<a name="l00188"></a>00188   };
<a name="l00189"></a>00189 
<a name="l00190"></a>00190 
<a name="l00192"></a>00192   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00193"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php">00193</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_lo_triang_packed.php">RowLoTriangPacked</a>, Allocator&gt;:
<a name="l00194"></a>00194     <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked</a>&lt;T, Prop, RowLoTriangPacked, Allocator&gt;
<a name="l00195"></a>00195   {
<a name="l00196"></a>00196     <span class="comment">// typedef declaration.</span>
<a name="l00197"></a>00197   <span class="keyword">public</span>:
<a name="l00198"></a>00198     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00199"></a>00199     <span class="keyword">typedef</span> Prop property;
<a name="l00200"></a>00200     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_row_lo_triang_packed.php">RowLoTriangPacked</a> <a class="code" href="class_seldon_1_1_row_lo_triang_packed.php">storage</a>;
<a name="l00201"></a>00201     <span class="keyword">typedef</span> Allocator allocator;
<a name="l00202"></a>00202 
<a name="l00203"></a>00203   <span class="keyword">public</span>:
<a name="l00204"></a>00204     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>();
<a name="l00205"></a>00205     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j = 0);
<a name="l00206"></a>00206     <span class="keywordtype">void</span> Resize(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00207"></a>00207 
<a name="l00208"></a>00208     <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00209"></a>00209     <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php" title="Row-major lower-triangular packed matrix class.">Matrix&lt;T, Prop, RowLoTriangPacked, Allocator&gt;</a>&amp; operator= (<span class="keyword">const</span> T0&amp; x);
<a name="l00210"></a>00210     <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php" title="Row-major lower-triangular packed matrix class.">Matrix&lt;T, Prop, RowLoTriangPacked, Allocator&gt;</a>&amp;
<a name="l00211"></a>00211     operator= (<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php" title="Row-major lower-triangular packed matrix class.">Matrix&lt;T, Prop, RowLoTriangPacked, Allocator&gt;</a>&amp; A);
<a name="l00212"></a>00212     <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l00213"></a>00213     <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php" title="Row-major lower-triangular packed matrix class.">Matrix&lt;T, Prop, RowLoTriangPacked, Allocator&gt;</a>&amp; operator*= (<span class="keyword">const</span> T0&amp; x);
<a name="l00214"></a>00214   };
<a name="l00215"></a>00215 
<a name="l00216"></a>00216 
<a name="l00217"></a>00217 } <span class="comment">// namespace Seldon.</span>
<a name="l00218"></a>00218 
<a name="l00219"></a>00219 <span class="preprocessor">#define SELDON_FILE_MATRIX_TRIANGPACKED_HXX</span>
<a name="l00220"></a>00220 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
