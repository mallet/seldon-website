<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-methods">Protected Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a> &#124;
<a href="#pro-static-attribs">Static Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;" --><!-- doxytag: inherits="Vector_Base&lt; T, Allocator&lt; T &gt; &gt;" -->
<p>Structure for distributed vectors.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_heterogeneous_collection_8hxx_source.php">HeterogeneousCollection.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.png" usemap="#Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;_map" alt=""/>
  <map id="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;_map" name="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;_map">
<area href="class_seldon_1_1_vector___base.php" alt="Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;" shape="rect" coords="0,0,419,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5cf0db1f5639087ac74a0cdc1fc07e10"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::float_dense_v" ref="a5cf0db1f5639087ac74a0cdc1fc07e10" args="" -->
typedef <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; float, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator&lt; float &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>float_dense_v</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae91d032f838de9ab5fe0410f793c4801"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::float_sparse_v" ref="ae91d032f838de9ab5fe0410f793c4801" args="" -->
typedef <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; float, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator&lt; float &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>float_sparse_v</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a41d596dc1a545265760af5bf22d26599"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::double_dense_v" ref="a41d596dc1a545265760af5bf22d26599" args="" -->
typedef <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator&lt; double &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>double_dense_v</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afb3a558768472a5e2ae73f517fb5747d"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::double_sparse_v" ref="afb3a558768472a5e2ae73f517fb5747d" args="" -->
typedef <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator&lt; double &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>double_sparse_v</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad9b1bd894f2acb91e888f8d1c596eddb"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::float_dense_c" ref="ad9b1bd894f2acb91e888f8d1c596eddb" args="" -->
typedef <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">float_dense_v</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_collection.php">Collection</a>, <br class="typebreak"/>
SELDON_DEFAULT_COLLECTION_ALLOCATOR<br class="typebreak"/>
&lt; <a class="el" href="class_seldon_1_1_vector.php">float_dense_v</a> &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>float_dense_c</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adc622d3eb12713961d40c0e42c9a53c1"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::float_sparse_c" ref="adc622d3eb12713961d40c0e42c9a53c1" args="" -->
typedef <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">float_sparse_v</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_collection.php">Collection</a>, <br class="typebreak"/>
SELDON_DEFAULT_COLLECTION_ALLOCATOR<br class="typebreak"/>
&lt; <a class="el" href="class_seldon_1_1_vector.php">float_sparse_v</a> &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>float_sparse_c</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa75556fb7c856c3fa50344793244ce3c"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::double_dense_c" ref="aa75556fb7c856c3fa50344793244ce3c" args="" -->
typedef <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">double_dense_v</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_collection.php">Collection</a>, <br class="typebreak"/>
SELDON_DEFAULT_COLLECTION_ALLOCATOR<br class="typebreak"/>
&lt; <a class="el" href="class_seldon_1_1_vector.php">double_dense_v</a> &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>double_dense_c</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1132b154e9a4d4ff4a268b12ff04a6d2"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::double_sparse_c" ref="a1132b154e9a4d4ff4a268b12ff04a6d2" args="" -->
typedef <a class="el" href="class_seldon_1_1_vector.php">Vector</a><br class="typebreak"/>
&lt; <a class="el" href="class_seldon_1_1_vector.php">double_sparse_v</a>, <a class="el" href="class_seldon_1_1_collection.php">Collection</a>, <br class="typebreak"/>
SELDON_DEFAULT_COLLECTION_ALLOCATOR<br class="typebreak"/>
&lt; <a class="el" href="class_seldon_1_1_vector.php">double_sparse_v</a> &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>double_sparse_c</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4817d775df71168ee21a15778ff02b88"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::storage" ref="a4817d775df71168ee21a15778ff02b88" args="" -->
typedef <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>&nbsp;</td><td class="memItemRight" valign="bottom"><b>storage</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adbc0377ca27b759613c52091fce99baf"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::value_type" ref="adbc0377ca27b759613c52091fce99baf" args="" -->
typedef Allocator&lt; T &gt;::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a58ff8f594b30f3073ca819d5777931e9"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::pointer" ref="a58ff8f594b30f3073ca819d5777931e9" args="" -->
typedef Allocator&lt; T &gt;::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a10d82dbcf34ac7fd64a5e83a9a91523b"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::const_pointer" ref="a10d82dbcf34ac7fd64a5e83a9a91523b" args="" -->
typedef Allocator&lt; T &gt;<br class="typebreak"/>
::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a950d1ee76291b86035b7eee7e4871314"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::reference" ref="a950d1ee76291b86035b7eee7e4871314" args="" -->
typedef Allocator&lt; T &gt;::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7b843d8515b1ee847c919303bde64770"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::const_reference" ref="a7b843d8515b1ee847c919303bde64770" args="" -->
typedef Allocator&lt; T &gt;<br class="typebreak"/>
::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a95f3ecfa8b557f41194fe506d0af7d5e">Vector</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#a95f3ecfa8b557f41194fe506d0af7d5e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a7a13cdb2e25c02e63cfdab46413a6f96">Vector</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt; &amp;)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copy constructor.  <a href="#a7a13cdb2e25c02e63cfdab46413a6f96"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a89672db2ad89fd1e27987790ec0e0561">~Vector</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Destructor.  <a href="#a89672db2ad89fd1e27987790ec0e0561"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ab5cd5f6b57ca76ac3659b02fe928d23d">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the vector collection.  <a href="#ab5cd5f6b57ca76ac3659b02fe928d23d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ade6c65f8a6252305e86b01898eabdcd7">Deallocate</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the vector collection.  <a href="#ade6c65f8a6252305e86b01898eabdcd7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#aae905d2dac0834a9761b86e226c8800c">AddVector</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; float, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator&lt; float &gt; &gt; &amp;)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds a vector to the list of vectors.  <a href="#aae905d2dac0834a9761b86e226c8800c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ae53a4416febae2f6966ada61143039b9">AddVector</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; float, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator&lt; float &gt; &gt; &amp;)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds a vector to the list of vectors.  <a href="#ae53a4416febae2f6966ada61143039b9"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#af9abc590c45be62474179bda12c4d6ae">AddVector</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator&lt; double &gt; &gt; &amp;)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds a vector to the list of vectors.  <a href="#af9abc590c45be62474179bda12c4d6ae"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a0167a495ffd1f473678ed5d9549f7507">AddVector</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator&lt; double &gt; &gt; &amp;)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds a vector to the list of vectors.  <a href="#a0167a495ffd1f473678ed5d9549f7507"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 , class Storage0 , class Allocator0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#afcca9799bf261c112c99e806b8907884">AddVector</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T0, Storage0, Allocator0 &gt; &amp;, string name)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds a vector to the list of vectors.  <a href="#afcca9799bf261c112c99e806b8907884"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a2718fc033e69dd6e16f88848d6b02ed5">SetVector</a> (int i, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; float, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator&lt; float &gt; &gt; &amp;)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets a vector in the list of vectors.  <a href="#a2718fc033e69dd6e16f88848d6b02ed5"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a80412689e793d552bc92b165eb3115e9">SetVector</a> (int i, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; float, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator&lt; float &gt; &gt; &amp;)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets a vector in the list of vectors.  <a href="#a80412689e793d552bc92b165eb3115e9"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a7fe916fddf26f60e915f8541258dec79">SetVector</a> (int i, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator&lt; double &gt; &gt; &amp;)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets a vector in the list of vectors.  <a href="#a7fe916fddf26f60e915f8541258dec79"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a7f055bcede5e4036e2f06ea3a51b0829">SetVector</a> (int i, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator&lt; double &gt; &gt; &amp;)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets a vector in the list of vectors.  <a href="#a7f055bcede5e4036e2f06ea3a51b0829"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 , class Storage0 , class Allocator0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a96adbda8dc019e9d87778deb12e93b39">SetVector</a> (int i, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T0, Storage0, Allocator0 &gt; &amp;, string name)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets a vector in the list of vectors.  <a href="#a96adbda8dc019e9d87778deb12e93b39"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 , class Storage0 , class Allocator0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#adf98501c23f89625bb971e10e44b605f">SetVector</a> (string name, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T0, Storage0, Allocator0 &gt; &amp;)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets a vector in the list of vectors.  <a href="#adf98501c23f89625bb971e10e44b605f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a60874f5b7285ceaaad8c2d8a9e99d7f0">SetName</a> (int i, string name)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets a vector in the list of vectors.  <a href="#a60874f5b7285ceaaad8c2d8a9e99d7f0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ada438f06bc34a5a98b630f07e0e80346"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::Nullify" ref="ada438f06bc34a5a98b630f07e0e80346" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ada438f06bc34a5a98b630f07e0e80346">Nullify</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Nullifies vectors of the collection without memory deallocation. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a6f40d71a69e016ae3e1d9db92b6538a4">GetM</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the total number of elements.  <a href="#a6f40d71a69e016ae3e1d9db92b6538a4"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ae501c34e51f6559568846b94e24c249c">GetLength</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the total number of elements.  <a href="#ae501c34e51f6559568846b94e24c249c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a696c512cf3692f2e7193446373aa8c97">GetNvector</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of aggregated vectors.  <a href="#a696c512cf3692f2e7193446373aa8c97"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ad2b29185865f3f2e7a4bf7e43b55c434">GetVectorLength</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the length vector of the underlying vectors.  <a href="#ad2b29185865f3f2e7a4bf7e43b55c434"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a750b9d392301df88e3cdb76cf11f87a0">GetLengthSum</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the cumulative sum of the lengths of the underlying vectors.  <a href="#a750b9d392301df88e3cdb76cf11f87a0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a5bdd2a3d741868fc687c2e62388dd348">GetCollectionIndex</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the collection indexes of the underlying vectors.  <a href="#a5bdd2a3d741868fc687c2e62388dd348"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a6872e6bac0e998a5b2e7dbf2d9b1807f">GetSubvectorIndex</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the index of the underlying vectors in the inner collection.  <a href="#a6872e6bac0e998a5b2e7dbf2d9b1807f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">float_dense_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#addef6ea1f486b2b836b2ca54f8273ccb">GetFloatDense</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the list of float dense vectors.  <a href="#addef6ea1f486b2b836b2ca54f8273ccb"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_vector.php">float_dense_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#aace5627f0d92e61ec0e99a513f4570c1">GetFloatDense</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the list of float dense vectors.  <a href="#aace5627f0d92e61ec0e99a513f4570c1"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">float_sparse_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a919fc21cc5754175f976df5e1c62f1aa">GetFloatSparse</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the list of float sparse vectors.  <a href="#a919fc21cc5754175f976df5e1c62f1aa"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_vector.php">float_sparse_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a93b4ed54472467eeb6529334ee4c90d2">GetFloatSparse</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the list of float sparse vectors.  <a href="#a93b4ed54472467eeb6529334ee4c90d2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">double_dense_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a48e3d61c043a065c53b41470e706dfee">GetDoubleDense</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the list of double dense vectors.  <a href="#a48e3d61c043a065c53b41470e706dfee"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_vector.php">double_dense_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#abfef62ba1ded1d080a8d384eef37e85f">GetDoubleDense</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the list of double dense vectors.  <a href="#abfef62ba1ded1d080a8d384eef37e85f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">double_sparse_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a1add5fecb61b4984003fc903f5e4a71b">GetDoubleSparse</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the list of double sparse vectors.  <a href="#a1add5fecb61b4984003fc903f5e4a71b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_vector.php">double_sparse_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ac499cedccfb785f37d1e1aec3869300d">GetDoubleSparse</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the list of double sparse vectors.  <a href="#ac499cedccfb785f37d1e1aec3869300d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a51857984d975eaac6c6f723de3f91094"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetVector" ref="a51857984d975eaac6c6f723de3f91094" args="(int i, float_dense_v &amp;vector) const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetVector</b> (int i, <a class="el" href="class_seldon_1_1_vector.php">float_dense_v</a> &amp;vector) const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a834df71d9c1169ee880f4c3fd21de72f"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetVector" ref="a834df71d9c1169ee880f4c3fd21de72f" args="(int i, float_sparse_v &amp;vector) const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetVector</b> (int i, <a class="el" href="class_seldon_1_1_vector.php">float_sparse_v</a> &amp;vector) const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a259c299ee19569c45a41a1c95c87c195"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetVector" ref="a259c299ee19569c45a41a1c95c87c195" args="(int i, double_dense_v &amp;vector) const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetVector</b> (int i, <a class="el" href="class_seldon_1_1_vector.php">double_dense_v</a> &amp;vector) const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab536f6f65af1c82febc004b0c2334e08"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetVector" ref="ab536f6f65af1c82febc004b0c2334e08" args="(int i, double_sparse_v &amp;vector) const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetVector</b> (int i, <a class="el" href="class_seldon_1_1_vector.php">double_sparse_v</a> &amp;vector) const </td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 , class Storage0 , class Allocator0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ae15d81d9568c3d4383af9e4cf81427cd">GetVector</a> (string name, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T0, Storage0, Allocator0 &gt; &amp;vector) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns one of the aggregated vectors.  <a href="#ae15d81d9568c3d4383af9e4cf81427cd"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">double&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a2bed95a4398789fdbdc0abd96b233c5e">operator()</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a2bed95a4398789fdbdc0abd96b233c5e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, <br class="typebreak"/>
Allocator&lt; T &gt; &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a5a45815f1db11248f8e963611f41d0b1">operator=</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt; &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a vector collection (assignment operator).  <a href="#a5a45815f1db11248f8e963611f41d0b1"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#acec939c56c4c6b8f3db9cb2d47111644">Copy</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt; &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a vector collection.  <a href="#acec939c56c4c6b8f3db9cb2d47111644"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, <br class="typebreak"/>
Allocator&lt; T &gt; &gt; &amp;&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a429e20e3d4e359ccbdca47fd89e87d4b">operator*=</a> (const T0 &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Multiplies a vector collection by a scalar.  <a href="#a429e20e3d4e359ccbdca47fd89e87d4b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aff51ef2a12bca16732cd049b84502a87"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::Print" ref="aff51ef2a12bca16732cd049b84502a87" args="() const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#aff51ef2a12bca16732cd049b84502a87">Print</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays the vector. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#aada9ae0d211979ae3412ac43b6a6ba40">Write</a> (string FileName, bool with_size) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the inner vectors in a file.  <a href="#aada9ae0d211979ae3412ac43b6a6ba40"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ac655b1bab61066b91e99570fb893f2a9">Write</a> (ostream &amp;FileStream, bool with_size) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the vector in a file stream.  <a href="#ac655b1bab61066b91e99570fb893f2a9"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#aedd93324292aff59f9b60d60f618e634">WriteText</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the vector in a file.  <a href="#aedd93324292aff59f9b60d60f618e634"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#afe9e2d5782ba7bc805eb5d47702afec9">WriteText</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the vector in a file stream.  <a href="#afe9e2d5782ba7bc805eb5d47702afec9"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a72272cc506d65af05eaf0d7d9e28ed0a">Read</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the vector from a file.  <a href="#a72272cc506d65af05eaf0d7d9e28ed0a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#afe242d0f1bdc515d83c5defd16ee4ece">Read</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the vector from a file stream.  <a href="#afe242d0f1bdc515d83c5defd16ee4ece"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T , template&lt; class U &gt; class Allocator&gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a3c948376419d25cb11ada8d39c134c5d">GetVector</a> (int i, typename <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::<a class="el" href="class_seldon_1_1_vector.php">float_dense_v</a> &amp;vector) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns one of the aggregated vectors.  <a href="#a3c948376419d25cb11ada8d39c134c5d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a839880a3113c1885ead70d79fade0294"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetSize" ref="a839880a3113c1885ead70d79fade0294" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetSize</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4128ad4898e42211d22a7531c9f5f80a"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetData" ref="a4128ad4898e42211d22a7531c9f5f80a" args="() const" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetData</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a454aea78c82c4317dfe4160cc7c95afa"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetDataConst" ref="a454aea78c82c4317dfe4160cc7c95afa" args="() const" -->
const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataConst</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad0f5184bd9eec6fca70c54e459ced78f"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetDataVoid" ref="ad0f5184bd9eec6fca70c54e459ced78f" args="() const" -->
void *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataVoid</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0aadc006977de037eb3ee550683e3f19"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetDataConstVoid" ref="a0aadc006977de037eb3ee550683e3f19" args="() const" -->
const void *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataConstVoid</b> () const</td></tr>
<tr><td colspan="2"><h2><a name="pro-methods"></a>
Protected Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">string&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#adc5a4e9e00f74737c6dc4b69e810e26d">GetType</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the type of an inner vector in a string.  <a href="#adc5a4e9e00f74737c6dc4b69e810e26d"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="addbb1b1aa4add4cfc19d53304ea60fb8"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::Nvector_" ref="addbb1b1aa4add4cfc19d53304ea60fb8" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#addbb1b1aa4add4cfc19d53304ea60fb8">Nvector_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Total number of vectors. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5ad322e3511725821e82e14d632b6be1"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::collection_" ref="a5ad322e3511725821e82e14d632b6be1" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a5ad322e3511725821e82e14d632b6be1">collection_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">For each underlying vectors, index of the corresponding collection. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0e24041339f74050c878a52404373649"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::subvector_" ref="a0e24041339f74050c878a52404373649" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a0e24041339f74050c878a52404373649">subvector_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Index of the underlying vectors in the inner collection. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a55b6c0b9a6ed0958ccb7414a95b40327"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::length_" ref="a55b6c0b9a6ed0958ccb7414a95b40327" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a55b6c0b9a6ed0958ccb7414a95b40327">length_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Lengths of the underlying vectors. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1589bd7ba73b1d9e32fa11b96b57d36a"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::length_sum_" ref="a1589bd7ba73b1d9e32fa11b96b57d36a" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a1589bd7ba73b1d9e32fa11b96b57d36a">length_sum_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Cumulative sum of the lengths of the underlying vectors. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afc3abf3a8ecbed59428fe26bc70c094a"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::float_dense_c_" ref="afc3abf3a8ecbed59428fe26bc70c094a" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">float_dense_c</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#afc3abf3a8ecbed59428fe26bc70c094a">float_dense_c_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Pointers of the underlying float dense vectors. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a565fcd42d62a4540995fbc6df9938e37"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::float_sparse_c_" ref="a565fcd42d62a4540995fbc6df9938e37" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">float_sparse_c</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a565fcd42d62a4540995fbc6df9938e37">float_sparse_c_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Pointers of the underlying float sparse vectors. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0e75af5dfcebe3aa872890ee596d2120"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::double_dense_c_" ref="a0e75af5dfcebe3aa872890ee596d2120" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">double_dense_c</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a0e75af5dfcebe3aa872890ee596d2120">double_dense_c_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Pointers of the underlying double dense vectors. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1270cfa610abeaa07dd55863a33cff5c"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::double_sparse_c_" ref="a1270cfa610abeaa07dd55863a33cff5c" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">double_sparse_c</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a1270cfa610abeaa07dd55863a33cff5c">double_sparse_c_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Pointers of the underlying float sparse vectors. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad9ae13022169e5ecb85874a9d0655b37"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::label_map_" ref="ad9ae13022169e5ecb85874a9d0655b37" args="" -->
map&lt; string, int &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ad9ae13022169e5ecb85874a9d0655b37">label_map_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Indexes of the inner vectors that have a name. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5cf22412ddb84b63dae86c18efd1d46e"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::label_vector_" ref="a5cf22412ddb84b63dae86c18efd1d46e" args="" -->
vector&lt; string &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a5cf22412ddb84b63dae86c18efd1d46e">label_vector_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Names associated with the inner vectors. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac58514f2c122d7538dfc4db4ce0d4da9"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::m_" ref="ac58514f2c122d7538dfc4db4ce0d4da9" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>m_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1a6ae822c00f9ee2a798d54a36602430"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::data_" ref="a1a6ae822c00f9ee2a798d54a36602430" args="" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-static-attribs"></a>
Static Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6c958ad29a5f7eabd1f085e50046ab85"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::vect_allocator_" ref="a6c958ad29a5f7eabd1f085e50046ab85" args="" -->
static Allocator&lt; T &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>vect_allocator_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, template&lt; class U &gt; class Allocator&gt;<br/>
 class Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</h3>

<p>Structure for distributed vectors. </p>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8hxx_source.php#l00040">40</a> of file <a class="el" href="_heterogeneous_collection_8hxx_source.php">HeterogeneousCollection.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="a95f3ecfa8b557f41194fe506d0af7d5e"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::Vector" ref="a95f3ecfa8b557f41194fe506d0af7d5e" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::<a class="el" href="class_seldon_1_1_vector.php">Vector</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline, explicit]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>Nothing is allocated. The number of vectors is set to zero. </p>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00044">44</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7a13cdb2e25c02e63cfdab46413a6f96"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::Vector" ref="a7a13cdb2e25c02e63cfdab46413a6f96" args="(const Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt; &amp;)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::<a class="el" href="class_seldon_1_1_vector.php">Vector</a> </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>V</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Copy constructor. </p>
<p>Builds a copy of a vector collection. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>V</em>&nbsp;</td><td>vector collection to be copied. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00057">57</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a89672db2ad89fd1e27987790ec0e0561"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::~Vector" ref="a89672db2ad89fd1e27987790ec0e0561" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::~<a class="el" href="class_seldon_1_1_vector.php">Vector</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Destructor. </p>
<p>The inner vectors are nullified so that their memory blocks should not be deallocated. </p>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00075">75</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="aae905d2dac0834a9761b86e226c8800c"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::AddVector" ref="aae905d2dac0834a9761b86e226c8800c" args="(const Vector&lt; float, VectFull, Allocator&lt; float &gt; &gt; &amp;)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::AddVector </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; float, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator&lt; float &gt; &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>vector</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds a vector to the list of vectors. </p>
<p>The vector is "appended" to the existing data. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>vector</em>&nbsp;</td><td>vector to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00149">149</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae53a4416febae2f6966ada61143039b9"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::AddVector" ref="ae53a4416febae2f6966ada61143039b9" args="(const Vector&lt; float, VectSparse, Allocator&lt; float &gt; &gt; &amp;)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::AddVector </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; float, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator&lt; float &gt; &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>vector</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds a vector to the list of vectors. </p>
<p>The vector is "appended" to the existing data. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>vector</em>&nbsp;</td><td>vector to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00168">168</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af9abc590c45be62474179bda12c4d6ae"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::AddVector" ref="af9abc590c45be62474179bda12c4d6ae" args="(const Vector&lt; double, VectFull, Allocator&lt; double &gt; &gt; &amp;)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::AddVector </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator&lt; double &gt; &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>vector</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds a vector to the list of vectors. </p>
<p>The vector is "appended" to the existing data. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>vector</em>&nbsp;</td><td>vector to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00187">187</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0167a495ffd1f473678ed5d9549f7507"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::AddVector" ref="a0167a495ffd1f473678ed5d9549f7507" args="(const Vector&lt; double, VectSparse, Allocator&lt; double &gt; &gt; &amp;)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::AddVector </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator&lt; double &gt; &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>vector</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds a vector to the list of vectors. </p>
<p>The vector is "appended" to the existing data. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>vector</em>&nbsp;</td><td>vector to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00206">206</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="afcca9799bf261c112c99e806b8907884"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::AddVector" ref="afcca9799bf261c112c99e806b8907884" args="(const Vector&lt; T0, Storage0, Allocator0 &gt; &amp;, string name)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
<div class="memtemplate">
template&lt;class T0 , class Storage0 , class Allocator0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::AddVector </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T0, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>vector</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>name</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds a vector to the list of vectors. </p>
<p>The vector is "appended" to the existing data. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>vector</em>&nbsp;</td><td>vector to be appended. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>name</em>&nbsp;</td><td>name of the vector to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00227">227</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab5cd5f6b57ca76ac3659b02fe928d23d"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::Clear" ref="ab5cd5f6b57ca76ac3659b02fe928d23d" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::Clear </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the vector collection. </p>
<p>The inner vectors are nullified so that their memory blocks should not be deallocated. </p>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00098">98</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="acec939c56c4c6b8f3db9cb2d47111644"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::Copy" ref="acec939c56c4c6b8f3db9cb2d47111644" args="(const Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt; &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::Copy </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a vector collection. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>X</em>&nbsp;</td><td>vector collection to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'X' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00870">870</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ade6c65f8a6252305e86b01898eabdcd7"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::Deallocate" ref="ade6c65f8a6252305e86b01898eabdcd7" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::Deallocate </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the vector collection. </p>
<p>The inner vectors are cleared and the memory blocks are deallocated. </p>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00121">121</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5bdd2a3d741868fc687c2e62388dd348"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetCollectionIndex" ref="a5bdd2a3d741868fc687c2e62388dd348" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt; &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::GetCollectionIndex </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the collection indexes of the underlying vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The collection index vector of the underlying vectors. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00525">525</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a48e3d61c043a065c53b41470e706dfee"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetDoubleDense" ref="a48e3d61c043a065c53b41470e706dfee" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::<a class="el" href="class_seldon_1_1_vector.php">double_dense_c</a> &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::GetDoubleDense </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the list of double dense vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The list of the aggregated vectors. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00608">608</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="abfef62ba1ded1d080a8d384eef37e85f"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetDoubleDense" ref="abfef62ba1ded1d080a8d384eef37e85f" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::<a class="el" href="class_seldon_1_1_vector.php">double_dense_c</a> &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::GetDoubleDense </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the list of double dense vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The list of the aggregated vectors. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00623">623</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1add5fecb61b4984003fc903f5e4a71b"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetDoubleSparse" ref="a1add5fecb61b4984003fc903f5e4a71b" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::<a class="el" href="class_seldon_1_1_vector.php">double_sparse_c</a> &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::GetDoubleSparse </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the list of double sparse vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The list of the aggregated vectors. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00636">636</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac499cedccfb785f37d1e1aec3869300d"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetDoubleSparse" ref="ac499cedccfb785f37d1e1aec3869300d" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::<a class="el" href="class_seldon_1_1_vector.php">double_sparse_c</a> &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::GetDoubleSparse </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the list of double sparse vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The list of the aggregated vectors. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00651">651</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="addef6ea1f486b2b836b2ca54f8273ccb"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetFloatDense" ref="addef6ea1f486b2b836b2ca54f8273ccb" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::<a class="el" href="class_seldon_1_1_vector.php">float_dense_c</a> &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::GetFloatDense </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the list of float dense vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The list of the aggregated vectors. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00552">552</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aace5627f0d92e61ec0e99a513f4570c1"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetFloatDense" ref="aace5627f0d92e61ec0e99a513f4570c1" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::<a class="el" href="class_seldon_1_1_vector.php">float_dense_c</a> &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::GetFloatDense </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the list of float dense vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The list of the aggregated vectors. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00567">567</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a919fc21cc5754175f976df5e1c62f1aa"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetFloatSparse" ref="a919fc21cc5754175f976df5e1c62f1aa" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::<a class="el" href="class_seldon_1_1_vector.php">float_sparse_c</a> &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::GetFloatSparse </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the list of float sparse vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The list of the aggregated vectors. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00580">580</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a93b4ed54472467eeb6529334ee4c90d2"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetFloatSparse" ref="a93b4ed54472467eeb6529334ee4c90d2" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::<a class="el" href="class_seldon_1_1_vector.php">float_sparse_c</a> &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::GetFloatSparse </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the list of float sparse vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The list of the aggregated vectors. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00595">595</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae501c34e51f6559568846b94e24c249c"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetLength" ref="ae501c34e51f6559568846b94e24c249c" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::GetLength </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the total number of elements. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The total length of the vector. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_vector___base.php#a075daae3607a4767a77a3de60ab30ba7">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a>.</p>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00474">474</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a750b9d392301df88e3cdb76cf11f87a0"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetLengthSum" ref="a750b9d392301df88e3cdb76cf11f87a0" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt; &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::GetLengthSum </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the cumulative sum of the lengths of the underlying vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The cumulative sum of the lengths of the underlying vectors. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00512">512</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6f40d71a69e016ae3e1d9db92b6538a4"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetM" ref="a6f40d71a69e016ae3e1d9db92b6538a4" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::GetM </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the total number of elements. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The total length of the vector. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a>.</p>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00461">461</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a696c512cf3692f2e7193446373aa8c97"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetNvector" ref="a696c512cf3692f2e7193446373aa8c97" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::GetNvector </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of aggregated vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The total number of aggregated vectors. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00486">486</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6872e6bac0e998a5b2e7dbf2d9b1807f"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetSubvectorIndex" ref="a6872e6bac0e998a5b2e7dbf2d9b1807f" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt; &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::GetSubvectorIndex </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the index of the underlying vectors in the inner collection. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The index vector of the underlying vectors in the inner collection. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00539">539</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="adc5a4e9e00f74737c6dc4b69e810e26d"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetType" ref="adc5a4e9e00f74737c6dc4b69e810e26d" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">string <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::GetType </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [protected]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the type of an inner vector in a string. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the vector. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The type in format "Vector&lt;{float,double},
    {VectFull,VectSparse}&gt;". </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l01260">1260</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae15d81d9568c3d4383af9e4cf81427cd"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetVector" ref="ae15d81d9568c3d4383af9e4cf81427cd" args="(string name, Vector&lt; T0, Storage0, Allocator0 &gt; &amp;vector) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
<div class="memtemplate">
template&lt;class T0 , class Storage0 , class Allocator0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::GetVector </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>name</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T0, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>vector</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns one of the aggregated vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>the index of the vector to be returned. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The <em>i</em> th aggregated vector. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00785">785</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3c948376419d25cb11ada8d39c134c5d"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetVector" ref="a3c948376419d25cb11ada8d39c134c5d" args="(int i, typename Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::float_dense_v &amp;vector) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::GetVector </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">typename <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::<a class="el" href="class_seldon_1_1_vector.php">float_dense_v</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>vector</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns one of the aggregated vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>the index of the vector to be returned. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The <em>i</em> th aggregated vector. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00664">664</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad2b29185865f3f2e7a4bf7e43b55c434"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::GetVectorLength" ref="ad2b29185865f3f2e7a4bf7e43b55c434" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <a class="el" href="class_seldon_1_1_malloc_alloc.php">MallocAlloc</a>&lt; int &gt; &gt; &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::GetVectorLength </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the length vector of the underlying vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The lengths of the underlying vectors. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00499">499</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2bed95a4398789fdbdc0abd96b233c5e"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::operator()" ref="a2bed95a4398789fdbdc0abd96b233c5e" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">double <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The value of the vector at 'i'. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00810">810</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a429e20e3d4e359ccbdca47fd89e87d4b"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::operator*=" ref="a429e20e3d4e359ccbdca47fd89e87d4b" args="(const T0 &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt; &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::operator*= </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>alpha</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Multiplies a vector collection by a scalar. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>alpha</em>&nbsp;</td><td>scalar. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00897">897</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5a45815f1db11248f8e963611f41d0b1"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::operator=" ref="a5a45815f1db11248f8e963611f41d0b1" args="(const Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt; &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt; &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a vector collection (assignment operator). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>X</em>&nbsp;</td><td>vector collection to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'X' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00855">855</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a72272cc506d65af05eaf0d7d9e28ed0a"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::Read" ref="a72272cc506d65af05eaf0d7d9e28ed0a" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the vector from a file. </p>
<p>Sets the vector according to a binary file that stores the length of the vector (integer) and all elements. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l01111">1111</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="afe242d0f1bdc515d83c5defd16ee4ece"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::Read" ref="afe242d0f1bdc515d83c5defd16ee4ece" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the vector from a file stream. </p>
<p>Sets the vector according to a binary file stream that stores the length of the vector (integer) and all elements. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>file stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l01138">1138</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a60874f5b7285ceaaad8c2d8a9e99d7f0"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::SetName" ref="a60874f5b7285ceaaad8c2d8a9e99d7f0" args="(int i, string name)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::SetName </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>name</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets a vector in the list of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>a given index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>name</em>&nbsp;</td><td>name of the underlying vector. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00421">421</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="adf98501c23f89625bb971e10e44b605f"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::SetVector" ref="adf98501c23f89625bb971e10e44b605f" args="(string name, const Vector&lt; T0, Storage0, Allocator0 &gt; &amp;)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
<div class="memtemplate">
template&lt;class T0 , class Storage0 , class Allocator0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::SetVector </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>name</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T0, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>vector</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets a vector in the list of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>name</em>&nbsp;</td><td>name of the vector to be set. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>vector</em>&nbsp;</td><td>new value of the vector. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00402">402</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7f055bcede5e4036e2f06ea3a51b0829"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::SetVector" ref="a7f055bcede5e4036e2f06ea3a51b0829" args="(int i, const Vector&lt; double, VectSparse, Allocator&lt; double &gt; &gt; &amp;)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::SetVector </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator&lt; double &gt; &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>vector</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets a vector in the list of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>a given index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>vector</em>&nbsp;</td><td>vector to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00348">348</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2718fc033e69dd6e16f88848d6b02ed5"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::SetVector" ref="a2718fc033e69dd6e16f88848d6b02ed5" args="(int i, const Vector&lt; float, VectFull, Allocator&lt; float &gt; &gt; &amp;)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::SetVector </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; float, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator&lt; float &gt; &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>vector</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets a vector in the list of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>a given index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>vector</em>&nbsp;</td><td>vector to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00241">241</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7fe916fddf26f60e915f8541258dec79"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::SetVector" ref="a7fe916fddf26f60e915f8541258dec79" args="(int i, const Vector&lt; double, VectFull, Allocator&lt; double &gt; &gt; &amp;)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::SetVector </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator&lt; double &gt; &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>vector</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets a vector in the list of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>a given index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>vector</em>&nbsp;</td><td>vector to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00312">312</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a80412689e793d552bc92b165eb3115e9"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::SetVector" ref="a80412689e793d552bc92b165eb3115e9" args="(int i, const Vector&lt; float, VectSparse, Allocator&lt; float &gt; &gt; &amp;)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::SetVector </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; float, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator&lt; float &gt; &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>vector</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets a vector in the list of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>a given index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>vector</em>&nbsp;</td><td>vector to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00276">276</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a96adbda8dc019e9d87778deb12e93b39"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::SetVector" ref="a96adbda8dc019e9d87778deb12e93b39" args="(int i, const Vector&lt; T0, Storage0, Allocator0 &gt; &amp;, string name)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
<div class="memtemplate">
template&lt;class T0 , class Storage0 , class Allocator0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::SetVector </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T0, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>vector</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>name</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets a vector in the list of vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the vector to be set. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>vector</em>&nbsp;</td><td>new value of the vector. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>name</em>&nbsp;</td><td>new name of the vector. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00386">386</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aada9ae0d211979ae3412ac43b6a6ba40"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::Write" ref="aada9ae0d211979ae3412ac43b6a6ba40" args="(string FileName, bool with_size) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the inner vectors in a file. </p>
<p>The length of the vector (integer) and all elements of the vector are stored in binary format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>file name. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the length of the vector is not saved. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00952">952</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac655b1bab61066b91e99570fb893f2a9"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::Write" ref="ac655b1bab61066b91e99570fb893f2a9" args="(ostream &amp;FileStream, bool with_size) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the vector in a file stream. </p>
<p>The length of the vector (integer) and all elements of the vector are stored in binary format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>file stream. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the length of the vector is not saved. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l00980">980</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="afe9e2d5782ba7bc805eb5d47702afec9"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::WriteText" ref="afe9e2d5782ba7bc805eb5d47702afec9" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the vector in a file stream. </p>
<p>All elements of the vector are stored in text format. The length is not stored. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>file stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l01063">1063</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aedd93324292aff59f9b60d60f618e634"></a><!-- doxytag: member="Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;::WriteText" ref="aedd93324292aff59f9b60d60f618e634" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; T &gt; &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the vector in a file. </p>
<p>All elements of the vector are stored in text format. The length is not stored. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_collection_8cxx_source.php#l01034">1034</a> of file <a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>vector/<a class="el" href="_heterogeneous_collection_8hxx_source.php">HeterogeneousCollection.hxx</a></li>
<li>vector/<a class="el" href="_heterogeneous_collection_8cxx_source.php">HeterogeneousCollection.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
