<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><b>seldon</b>      </li>
      <li><a class="el" href="classseldon_1_1string.php">string</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pub-attribs">Public Attributes</a>  </div>
  <div class="headertitle">
<h1>seldon::string Class Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="seldon::string" --><!-- doxytag: inherits="seldon::_object" --><div class="dynheader">
Inheritance diagram for seldon::string:</div>
<div class="dyncontent">
 <div class="center">
  <img src="classseldon_1_1string.png" usemap="#seldon::string_map" alt=""/>
  <map id="seldon::string_map" name="seldon::string_map">
<area href="classseldon_1_1__object.php" alt="seldon::_object" shape="rect" coords="0,0,96,24"/>
</map>
</div>

<p><a href="classseldon_1_1string-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aba48a1d640c181b1db3c5e537b191b86"></a><!-- doxytag: member="seldon::string::length" ref="aba48a1d640c181b1db3c5e537b191b86" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>length</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3588e75dd58bd902ac9d9a3a6ada406f"></a><!-- doxytag: member="seldon::string::max_size" ref="a3588e75dd58bd902ac9d9a3a6ada406f" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>max_size</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab603799d7f784a31e7cc45bcf13588b7"></a><!-- doxytag: member="seldon::string::capacity" ref="ab603799d7f784a31e7cc45bcf13588b7" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>capacity</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0f1a68e93cf73e4dea7f38c3fe71244b"></a><!-- doxytag: member="seldon::string::reserve" ref="a0f1a68e93cf73e4dea7f38c3fe71244b" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>reserve</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad8bf1a7eef74528e89607a1d5469b0bc"></a><!-- doxytag: member="seldon::string::copy" ref="ad8bf1a7eef74528e89607a1d5469b0bc" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>copy</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a498dbc7213dcf3fa2a427eaef53892b1"></a><!-- doxytag: member="seldon::string::c_str" ref="a498dbc7213dcf3fa2a427eaef53892b1" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>c_str</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a536790f7ada11635619cd1024f14af1d"></a><!-- doxytag: member="seldon::string::find" ref="a536790f7ada11635619cd1024f14af1d" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>find</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8251d04a7db3569e1dfb2c7bde990b21"></a><!-- doxytag: member="seldon::string::rfind" ref="a8251d04a7db3569e1dfb2c7bde990b21" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>rfind</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0d647f6a4484a4055e32f28e3a43b0e6"></a><!-- doxytag: member="seldon::string::find_first_of" ref="a0d647f6a4484a4055e32f28e3a43b0e6" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>find_first_of</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac1e8e7a1bce751937294d0c71c158f8c"></a><!-- doxytag: member="seldon::string::find_last_of" ref="ac1e8e7a1bce751937294d0c71c158f8c" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>find_last_of</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4ed87d10fd862132931af1a3a40aeca0"></a><!-- doxytag: member="seldon::string::find_first_not_of" ref="a4ed87d10fd862132931af1a3a40aeca0" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>find_first_not_of</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a62fe82c5b4b3ca440e17bf52d8ae6856"></a><!-- doxytag: member="seldon::string::find_last_not_of" ref="a62fe82c5b4b3ca440e17bf52d8ae6856" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>find_last_not_of</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="abecbaa54f54084f8d462b39426e439e5"></a><!-- doxytag: member="seldon::string::substr" ref="abecbaa54f54084f8d462b39426e439e5" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>substr</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab8f9ef4a9c3bcb9dc8734c62f110af5c"></a><!-- doxytag: member="seldon::string::empty" ref="ab8f9ef4a9c3bcb9dc8734c62f110af5c" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>empty</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa456a3d7e704e2bad37e27b6cbc317e2"></a><!-- doxytag: member="seldon::string::size" ref="aa456a3d7e704e2bad37e27b6cbc317e2" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>size</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6b47675aa9d5fa8b266b91f3c8faaa77"></a><!-- doxytag: member="seldon::string::swap" ref="a6b47675aa9d5fa8b266b91f3c8faaa77" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>swap</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7483a659cdccbcedca1bd6e495a8a4f3"></a><!-- doxytag: member="seldon::string::get_allocator" ref="a7483a659cdccbcedca1bd6e495a8a4f3" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>get_allocator</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aaffa451860a29d6bedbd0f9730e221f4"></a><!-- doxytag: member="seldon::string::begin" ref="aaffa451860a29d6bedbd0f9730e221f4" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>begin</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae40ded49ee1d8960357cf029e23f3bbf"></a><!-- doxytag: member="seldon::string::end" ref="ae40ded49ee1d8960357cf029e23f3bbf" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>end</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0f20efa7fbd56833fe50c0772a07cef5"></a><!-- doxytag: member="seldon::string::rbegin" ref="a0f20efa7fbd56833fe50c0772a07cef5" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>rbegin</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3a6c1bc0ce72d8f1e884928bb2d0c532"></a><!-- doxytag: member="seldon::string::rend" ref="a3a6c1bc0ce72d8f1e884928bb2d0c532" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>rend</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a86780bb0c111376a1228370119c34ac5"></a><!-- doxytag: member="seldon::string::erase" ref="a86780bb0c111376a1228370119c34ac5" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>erase</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa34357010a45cb6be61716d0c728db1e"></a><!-- doxytag: member="seldon::string::__init__" ref="aa34357010a45cb6be61716d0c728db1e" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__init__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8b420d1105b4c11d30c654a944d28a75"></a><!-- doxytag: member="seldon::string::assign" ref="a8b420d1105b4c11d30c654a944d28a75" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>assign</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa763c4be38c0bb0fc48247f7b3211a5c"></a><!-- doxytag: member="seldon::string::resize" ref="aa763c4be38c0bb0fc48247f7b3211a5c" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>resize</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afe38c2fb5efb03be2bff6ee6285b9086"></a><!-- doxytag: member="seldon::string::iterator" ref="afe38c2fb5efb03be2bff6ee6285b9086" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>iterator</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a32ddeda02726b7b5aa4f1737d936da12"></a><!-- doxytag: member="seldon::string::__iter__" ref="a32ddeda02726b7b5aa4f1737d936da12" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__iter__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac7e3b0a246c6d5fa2ad78528fb4f4d8d"></a><!-- doxytag: member="seldon::string::__nonzero__" ref="ac7e3b0a246c6d5fa2ad78528fb4f4d8d" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__nonzero__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9a8b1245aa9bc5aa2ed3efd9028f168d"></a><!-- doxytag: member="seldon::string::__bool__" ref="a9a8b1245aa9bc5aa2ed3efd9028f168d" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__bool__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0d052f14f9c0a0e6c99cf70b33de6221"></a><!-- doxytag: member="seldon::string::__len__" ref="a0d052f14f9c0a0e6c99cf70b33de6221" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__len__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa35ee04977ca426ddc172ae1df8bff2a"></a><!-- doxytag: member="seldon::string::__getslice__" ref="aa35ee04977ca426ddc172ae1df8bff2a" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__getslice__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0caa1e3e903a8e10767b8a041bca6160"></a><!-- doxytag: member="seldon::string::__setslice__" ref="a0caa1e3e903a8e10767b8a041bca6160" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__setslice__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a76e2107ec96bf10e8c13e2b13ddebcad"></a><!-- doxytag: member="seldon::string::__delslice__" ref="a76e2107ec96bf10e8c13e2b13ddebcad" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__delslice__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a63f7412754097f28029d62990e10b4a7"></a><!-- doxytag: member="seldon::string::__delitem__" ref="a63f7412754097f28029d62990e10b4a7" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__delitem__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab7ea7a28d89f036dcb84abdf67d36738"></a><!-- doxytag: member="seldon::string::__getitem__" ref="ab7ea7a28d89f036dcb84abdf67d36738" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__getitem__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a462c7a136cdd871e05457fbcedfc0615"></a><!-- doxytag: member="seldon::string::__setitem__" ref="a462c7a136cdd871e05457fbcedfc0615" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__setitem__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aeecc40053ff3c22bf8a9bd056682dbc2"></a><!-- doxytag: member="seldon::string::insert" ref="aeecc40053ff3c22bf8a9bd056682dbc2" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>insert</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aee2ee46aeeeedbe4f74fb0253e5c8753"></a><!-- doxytag: member="seldon::string::replace" ref="aee2ee46aeeeedbe4f74fb0253e5c8753" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>replace</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adcc6674987cf81deb400aa93370d8f01"></a><!-- doxytag: member="seldon::string::__iadd__" ref="adcc6674987cf81deb400aa93370d8f01" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__iadd__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0009b5ef3d796b22083267904a2c3bdc"></a><!-- doxytag: member="seldon::string::__add__" ref="a0009b5ef3d796b22083267904a2c3bdc" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__add__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af430517c57f1bf9cc5dffaed012830f4"></a><!-- doxytag: member="seldon::string::__radd__" ref="af430517c57f1bf9cc5dffaed012830f4" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__radd__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1624aaf5daaf8a6e8a762cbc443c882d"></a><!-- doxytag: member="seldon::string::__str__" ref="a1624aaf5daaf8a6e8a762cbc443c882d" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__str__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2c377d50750e24f75e4894b9ea82a42f"></a><!-- doxytag: member="seldon::string::__rlshift__" ref="a2c377d50750e24f75e4894b9ea82a42f" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__rlshift__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7281424f42248abe20ad56abcd858b18"></a><!-- doxytag: member="seldon::string::__eq__" ref="a7281424f42248abe20ad56abcd858b18" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__eq__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aaef227d044b6623409e50f83096c04ea"></a><!-- doxytag: member="seldon::string::__ne__" ref="aaef227d044b6623409e50f83096c04ea" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__ne__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a831083f980061e13d83f030ff6a99ff0"></a><!-- doxytag: member="seldon::string::__gt__" ref="a831083f980061e13d83f030ff6a99ff0" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__gt__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad2e93ebffe7f08bebd5892f96926bfe2"></a><!-- doxytag: member="seldon::string::__lt__" ref="ad2e93ebffe7f08bebd5892f96926bfe2" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__lt__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a09b468aa33927054a9c6a45d55ae4aee"></a><!-- doxytag: member="seldon::string::__ge__" ref="a09b468aa33927054a9c6a45d55ae4aee" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__ge__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a432f4cb927d9645a4572e5832f1435c1"></a><!-- doxytag: member="seldon::string::__le__" ref="a432f4cb927d9645a4572e5832f1435c1" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__le__</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-attribs"></a>
Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af39995dd2532e505b13194af21013ed1"></a><!-- doxytag: member="seldon::string::this" ref="af39995dd2532e505b13194af21013ed1" args="" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><b>this</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>

<p>Definition at line <a class="el" href="seldon_8py_source.php#l00200">200</a> of file <a class="el" href="seldon_8py_source.php">seldon.py</a>.</p>
<hr/>The documentation for this class was generated from the following file:<ul>
<li><a class="el" href="seldon_8py_source.php">seldon.py</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
