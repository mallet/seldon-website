<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/solver/SparseCholeskyFactorisation.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2010 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_SPARSE_CHOLESKY_FACTORISATION_HXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 
<a name="l00023"></a>00023 <span class="keyword">namespace </span>Seldon
<a name="l00024"></a>00024 {
<a name="l00025"></a>00025 
<a name="l00027"></a>00027   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00028"></a><a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php">00028</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php" title="Class grouping different Cholesky solvers.">SparseCholeskySolver</a>
<a name="l00029"></a>00029   {
<a name="l00030"></a>00030   <span class="keyword">protected</span> :
<a name="l00032"></a><a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a877304542a5dfbcb2a09a308477627b6">00032</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a877304542a5dfbcb2a09a308477627b6" title="Verbosity level.">print_level</a>;
<a name="l00034"></a><a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a8fb8259e3a995aac27ac0a100897c1eb">00034</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a8fb8259e3a995aac27ac0a100897c1eb" title="Ordering to use.">type_ordering</a>;
<a name="l00036"></a><a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#afbe06f619943edf715630578fca28f7a">00036</a>     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#afbe06f619943edf715630578fca28f7a" title="Permutation array.">permutation</a>;
<a name="l00038"></a><a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#ad2298348f3c85c3aaa38652dfe829719">00038</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#ad2298348f3c85c3aaa38652dfe829719" title="Solver to use.">type_solver</a>;
<a name="l00040"></a><a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#aae8e798c85e35d0225f5e0ec9f2d4c2f">00040</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#aae8e798c85e35d0225f5e0ec9f2d4c2f" title="Size of factorized linear system.">n</a>;
<a name="l00042"></a><a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a9e491c5c484a7e8b4d0cae095d4be34c">00042</a>     <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Symmetric, ArrayRowSymSparse&gt;</a> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a9e491c5c484a7e8b4d0cae095d4be34c" title="Cholesky factors.">mat_sym</a>;
<a name="l00044"></a><a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#af5e2e3beb8f4ab7713e4729b3e7f86ac">00044</a>     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#af5e2e3beb8f4ab7713e4729b3e7f86ac" title="Temporary vector.">xtmp</a>;
<a name="l00045"></a>00045 
<a name="l00046"></a>00046 <span class="preprocessor">#ifdef SELDON_WITH_CHOLMOD</span>
<a name="l00047"></a>00047 <span class="preprocessor"></span>    <a class="code" href="class_seldon_1_1_matrix_cholmod.php" title="Object containing Cholesky factorization.">MatrixCholmod</a> mat_chol;
<a name="l00048"></a>00048 <span class="preprocessor">#endif</span>
<a name="l00049"></a>00049 <span class="preprocessor"></span>
<a name="l00050"></a>00050   <span class="keyword">public</span> :
<a name="l00051"></a>00051     <span class="comment">// Available solvers.</span>
<a name="l00052"></a>00052     <span class="keyword">enum</span> {SELDON_SOLVER, CHOLMOD};
<a name="l00053"></a>00053 
<a name="l00054"></a>00054     <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a9f1966f5fe27659ea3d479df51c43c81" title="Default constructor.">SparseCholeskySolver</a>();
<a name="l00055"></a>00055 
<a name="l00056"></a>00056     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#af3ec93eff3ba46583297067febb44f16" title="Displays no messages.">HideMessages</a>();
<a name="l00057"></a>00057     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#ab8b96c5cc5065e333d3b79c32cdebe92" title="Displays only brief messages.">ShowMessages</a>();
<a name="l00058"></a>00058     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a24dc3be1fb9e8f479913b74f940384ed" title="Displays a lot of messages.">ShowFullHistory</a>();
<a name="l00059"></a>00059 
<a name="l00060"></a>00060     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a98bafc554b1ffc66030c99f8abf54f3d" title="Clears Cholesky factors.">Clear</a>();
<a name="l00061"></a>00061 
<a name="l00062"></a>00062     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#ad301f547c1eb78c7f1c8ed2f3dbdec6b" title="Returns the number of rows.">GetM</a>() <span class="keyword">const</span>;
<a name="l00063"></a>00063     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a833d2fdb33c43d7648e490da7cceee52" title="Returns the number of rows.">GetN</a>() <span class="keyword">const</span>;
<a name="l00064"></a>00064 
<a name="l00065"></a>00065     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#aeb8fc6c681810ace10586969a22c935a" title="Returns the type of ordering used.">GetTypeOrdering</a>() <span class="keyword">const</span>;
<a name="l00066"></a>00066     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#aa08d8f448a3408e05f75ae0268b90739" title="Modifies the ordering used.">SetOrdering</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp;);
<a name="l00067"></a>00067     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a12507e190d0320641997052c66039c93" title="Modifies the type of ordering used.">SelectOrdering</a>(<span class="keywordtype">int</span>);
<a name="l00068"></a>00068 
<a name="l00069"></a>00069     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#ae580e7293920e4bde686f3dbe4ebb379" title="Modifies the direct solver used.">SelectDirectSolver</a>(<span class="keywordtype">int</span>);
<a name="l00070"></a>00070     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a7351dbce7f0e1a5241f93c6f57457248" title="Returns the type of direct solver used.">GetDirectSolver</a>();
<a name="l00071"></a>00071 
<a name="l00072"></a>00072     <span class="keyword">template</span>&lt;<span class="keyword">class</span> MatrixSparse&gt;
<a name="l00073"></a>00073     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#ad33824372144f930799847ca4c8c93de" title="Performs Cholesky factorization.">Factorize</a>(MatrixSparse&amp; A, <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>);
<a name="l00074"></a>00074 
<a name="l00075"></a>00075     <span class="keyword">template</span>&lt;<span class="keyword">class</span> TransStatus, <span class="keyword">class</span> Vector1&gt;
<a name="l00076"></a>00076     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a34cf4c23c1a2fbd266d3f0466956e171" title="Solves L x = b or L^T x = b.">Solve</a>(<span class="keyword">const</span> TransStatus&amp; TransA, Vector1&amp; x);
<a name="l00077"></a>00077 
<a name="l00078"></a>00078     <span class="keyword">template</span>&lt;<span class="keyword">class</span> TransStatus, <span class="keyword">class</span> Vector1&gt;
<a name="l00079"></a>00079     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#abf3e9a17f8d27c777407ae73cb9b6a9b" title="Computes L x or L^T.">Mlt</a>(<span class="keyword">const</span> TransStatus&amp; TransA, Vector1&amp; x);
<a name="l00080"></a>00080 
<a name="l00081"></a>00081   };
<a name="l00082"></a>00082 
<a name="l00083"></a>00083 } <span class="comment">// namespace Seldon.</span>
<a name="l00084"></a>00084 
<a name="l00085"></a>00085 
<a name="l00086"></a>00086 <span class="preprocessor">#define SELDON_FILE_SPARSE_CHOLESKY_FACTORISATION_HXX</span>
<a name="l00087"></a>00087 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
