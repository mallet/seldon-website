<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php">Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;" --><!-- doxytag: inherits="Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;" -->
<p>Row-major symmetric sparse-matrix class.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_matrix___array_complex_sparse_8hxx_source.php">Matrix_ArrayComplexSparse.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.png" usemap="#Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;_map" alt=""/>
  <map id="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;_map" name="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;_map">
<area href="class_seldon_1_1_matrix___array_complex_sparse.php" alt="Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;" shape="rect" coords="0,0,525,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8346c29962eab7763115d6100352e1ec"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::value_type" ref="a8346c29962eab7763115d6100352e1ec" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3407ede749243e761df11f67af2d57a6"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::property" ref="a3407ede749243e761df11f67af2d57a6" args="" -->
typedef Prop&nbsp;</td><td class="memItemRight" valign="bottom"><b>property</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6cfa70c017df00cc1b5e1d476e73d011"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::storage" ref="a6cfa70c017df00cc1b5e1d476e73d011" args="" -->
typedef <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>&nbsp;</td><td class="memItemRight" valign="bottom"><b>storage</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1162ce772d7acfc6018cd0a5ef40078e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::allocator" ref="a1162ce772d7acfc6018cd0a5ef40078e" args="" -->
typedef Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>allocator</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5325ffba5ca6253c1df46e37283142d0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::pointer" ref="a5325ffba5ca6253c1df46e37283142d0" args="" -->
typedef Allocator::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a87436f000442ff05b2fc557a77dae13c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::const_pointer" ref="a87436f000442ff05b2fc557a77dae13c" args="" -->
typedef Allocator::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1ab28bb0ce6a3c1bba3b7680b0d2f6a2"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::reference" ref="a1ab28bb0ce6a3c1bba3b7680b0d2f6a2" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a963d0f91721a1d53fb2b4d5bc322abb8"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::const_reference" ref="a963d0f91721a1d53fb2b4d5bc322abb8" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2b4d88a19897b82ba5d8a37198cf4426"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::entry_type" ref="a2b4d88a19897b82ba5d8a37198cf4426" args="" -->
typedef complex&lt; T &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>entry_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a55a8cb7922d8fc8bbacd2b5548b03359"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::access_type" ref="a55a8cb7922d8fc8bbacd2b5548b03359" args="" -->
typedef complex&lt; T &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>access_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aaa7b45cfa45845f340f819e31f877f34"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::const_access_type" ref="aaa7b45cfa45845f340f819e31f877f34" args="" -->
typedef complex&lt; T &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_access_type</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a5436a5d9f8bf73dbceb229a0649facfe">Matrix</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#a5436a5d9f8bf73dbceb229a0649facfe"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a2a8c65edac40bcf4ba7ecb3b5db27502">Matrix</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Constructor.  <a href="#a2a8c65edac40bcf4ba7ecb3b5db27502"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">complex&lt; T &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a5fb8630efa21241d2517927eeb80de5e">operator()</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a5fb8630efa21241d2517927eeb80de5e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#abc507579e215d0fcd88b8d2bf8e710b9">ValReal</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns access to real part of element (i, j).  <a href="#abc507579e215d0fcd88b8d2bf8e710b9"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a2ccbdf02898e8b22ce9b5fe51aad89e1">ValReal</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns access to real part element (i, j).  <a href="#a2ccbdf02898e8b22ce9b5fe51aad89e1"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a8584e87cfb5ba75a746ec60a926f23a4">ValImag</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns access to imaginary part of element (i, j).  <a href="#a8584e87cfb5ba75a746ec60a926f23a4"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a9798b4ecbfea2a0629c8b484d9be931a">ValImag</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns access to imaginary part of element (i, j).  <a href="#a9798b4ecbfea2a0629c8b484d9be931a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a85eba6f5bb491fd35b28e5753f48eace">GetReal</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns access to real part element (i, j).  <a href="#a85eba6f5bb491fd35b28e5753f48eace"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a39ca8957207e6102cba15d2f0d7f47fb">GetReal</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns access to real part of element (i, j).  <a href="#a39ca8957207e6102cba15d2f0d7f47fb"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#affbf4d5e0fb00ec74943e47af3859d87">GetImag</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns access to imaginary part of element (i, j).  <a href="#affbf4d5e0fb00ec74943e47af3859d87"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#af7fa7341f09012e5f0ab244a40ab0ca2">GetImag</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns access to imaginary part element (i, j).  <a href="#af7fa7341f09012e5f0ab244a40ab0ca2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a49ac0164990312cc9485abe5de3a1d77">Set</a> (int i, int j, const complex&lt; T &gt; &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets element (i, j) of the matrix.  <a href="#a49ac0164990312cc9485abe5de3a1d77"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aff6987ec6d06c3a4c14ebc06b14ae355"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::ClearRealRow" ref="aff6987ec6d06c3a4c14ebc06b14ae355" args="(int i)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#aff6987ec6d06c3a4c14ebc06b14ae355">ClearRealRow</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears a row. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a47212adda8f7586925272cf2e4c620bb"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::ClearImagRow" ref="a47212adda8f7586925272cf2e4c620bb" args="(int i)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a47212adda8f7586925272cf2e4c620bb">ClearImagRow</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears a row. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a828bff3d9a061706b80cf3cc4a6725eb">ReallocateRealRow</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates row i.  <a href="#a828bff3d9a061706b80cf3cc4a6725eb"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#add6c25193de11ecc3f405c1ba713e0ef">ReallocateImagRow</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates row i.  <a href="#add6c25193de11ecc3f405c1ba713e0ef"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a9a7d49e83e0838f63c4e7210ccc42218">ResizeRealRow</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates row i.  <a href="#a9a7d49e83e0838f63c4e7210ccc42218"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a770cf431a7508fad913ef06bf487f9d6">ResizeImagRow</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates row i.  <a href="#a770cf431a7508fad913ef06bf487f9d6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#adf10d81ed34502289b1d928626921d91">SwapRealRow</a> (int i, int i_)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Swaps two rows.  <a href="#adf10d81ed34502289b1d928626921d91"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#abd914e374842533215ebeb12ae0fd5e6">SwapImagRow</a> (int i, int i_)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Swaps two rows.  <a href="#abd914e374842533215ebeb12ae0fd5e6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a5d858213b73625c365f2cf80c955b56c">ReplaceRealIndexRow</a> (int i, <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;new_index)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets column numbers of non-zero entries of a row.  <a href="#a5d858213b73625c365f2cf80c955b56c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a18e705d5da14b8ae374333f02a1801af">ReplaceImagIndexRow</a> (int i, <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;new_index)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets column numbers of non-zero entries of a row.  <a href="#a18e705d5da14b8ae374333f02a1801af"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#afc6d830c2b6290ab90af0733ca2991d9">GetRealRowSize</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of non-zero entries of a row.  <a href="#afc6d830c2b6290ab90af0733ca2991d9"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#af38c69e307e68125ec3304929b672e95">GetImagRowSize</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of non-zero entries of a row.  <a href="#af38c69e307e68125ec3304929b672e95"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4287416fdfc3c14ca814be6ebc29153c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::PrintRealRow" ref="a4287416fdfc3c14ca814be6ebc29153c" args="(int i) const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a4287416fdfc3c14ca814be6ebc29153c">PrintRealRow</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays non-zero values of a column. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6bf23b591a902626ffb78da822bc4c3e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::PrintImagRow" ref="a6bf23b591a902626ffb78da822bc4c3e" args="(int i) const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a6bf23b591a902626ffb78da822bc4c3e">PrintImagRow</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays non-zero values of a column. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a0dd4ca70074352d9df0370707dd05127">AssembleRealRow</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Assembles a column.  <a href="#a0dd4ca70074352d9df0370707dd05127"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a44db6b452950f6d03227f0b49e8aa427">AssembleImagRow</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Assembles a column.  <a href="#a44db6b452950f6d03227f0b49e8aa427"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a9cce706f016372a361f025cf92c3cb25">AddInteraction</a> (int i, int j, const complex&lt; T &gt; &amp;val)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds a coefficient in the matrix.  <a href="#a9cce706f016372a361f025cf92c3cb25"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Alloc1 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#adb9c19345b5e6f57b6d8f3dbec9bad93">AddInteractionRow</a> (int i, int nb, const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;col, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; complex&lt; T &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1 &gt; &amp;val)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds coefficients in a row.  <a href="#adb9c19345b5e6f57b6d8f3dbec9bad93"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Alloc1 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a4703da0eae726628352e0e68340c4ae4">AddInteractionColumn</a> (int i, int nb, const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;row, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; complex&lt; T &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1 &gt; &amp;val)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds coefficients in a column.  <a href="#a4703da0eae726628352e0e68340c4ae4"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0ee918c8c04a1a9434e157c15fdeb596"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::Clear" ref="a0ee918c8c04a1a9434e157c15fdeb596" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Clear</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad2348fb25206ef8516c7e835b7ce2287"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::Reallocate" ref="ad2348fb25206ef8516c7e835b7ce2287" args="(int i, int j)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Reallocate</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9166689334cc2f731220f76b730d692f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::Resize" ref="a9166689334cc2f731220f76b730d692f" args="(int i, int j)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Resize</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a35ea621d2e5351259ce5bca87febc821"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::GetM" ref="a35ea621d2e5351259ce5bca87febc821" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetM</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab3c6aad66c9dc6ad9addfb7de9a4ebdd"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::GetM" ref="ab3c6aad66c9dc6ad9addfb7de9a4ebdd" args="(const SeldonTranspose &amp;status) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetM</b> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a> &amp;status) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae413915ed95545b40df8b43737fa43c1"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::GetN" ref="ae413915ed95545b40df8b43737fa43c1" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetN</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a53e20518a8557211c89ca8ad0d607825"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::GetN" ref="a53e20518a8557211c89ca8ad0d607825" args="(const SeldonTranspose &amp;status) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetN</b> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a> &amp;status) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a11efc1bee6a95da705238628d7e3d958"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::GetRealNonZeros" ref="a11efc1bee6a95da705238628d7e3d958" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetRealNonZeros</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a33ebac042eda90796d7f674c250e85be"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::GetImagNonZeros" ref="a33ebac042eda90796d7f674c250e85be" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetImagNonZeros</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa40775992c738c7fd1e9543bfddb2694"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::GetRealDataSize" ref="aa40775992c738c7fd1e9543bfddb2694" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetRealDataSize</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad54321edab8e2633985e198f1a576340"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::GetImagDataSize" ref="ad54321edab8e2633985e198f1a576340" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetImagDataSize</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2e5c6c10af220eea29f9c1565b14a93f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::GetDataSize" ref="a2e5c6c10af220eea29f9c1565b14a93f" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataSize</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afae45969eda952b43748cf7756f0e6d6"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::GetRealInd" ref="afae45969eda952b43748cf7756f0e6d6" args="(int i) const" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetRealInd</b> (int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0a423d7e93a0ad9c19ed6de2b1052dca"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::GetImagInd" ref="a0a423d7e93a0ad9c19ed6de2b1052dca" args="(int i) const" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetImagInd</b> (int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aaa912365c7b8c191cf8b3fb2e2da4789"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::GetRealData" ref="aaa912365c7b8c191cf8b3fb2e2da4789" args="(int i) const" -->
T *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetRealData</b> (int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1d33377770e234c75aae39bed6f996ff"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::GetRealData" ref="a1d33377770e234c75aae39bed6f996ff" args="() const" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt; *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetRealData</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a578281c64cbe05a542a6b0e9642b326a"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::GetImagData" ref="a578281c64cbe05a542a6b0e9642b326a" args="(int i) const" -->
T *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetImagData</b> (int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac3940bc9993d64730333c83be0f839ce"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::GetImagData" ref="ac3940bc9993d64730333c83be0f839ce" args="() const" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt; *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetImagData</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7eb05d6902b13cfb7cc4f17093fdc17e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::Val" ref="a7eb05d6902b13cfb7cc4f17093fdc17e" args="(int i, int j)" -->
complex&lt; T &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>Val</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a994fbc16dd258d1609f7c7c582205e9f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::Val" ref="a994fbc16dd258d1609f7c7c582205e9f" args="(int i, int j) const" -->
const complex&lt; T &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>Val</b> (int i, int j) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad840efb36621d28aa9726088cfc29a29"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::Get" ref="ad840efb36621d28aa9726088cfc29a29" args="(int i, int j)" -->
complex&lt; T &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>Get</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a17a0b2e40258b30da1f0036c2a85c357"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::Get" ref="a17a0b2e40258b30da1f0036c2a85c357" args="(int i, int j) const" -->
const complex&lt; T &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>Get</b> (int i, int j) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a041c76033f939293aa871f2fe45ef22a"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::ValueReal" ref="a041c76033f939293aa871f2fe45ef22a" args="(int num_row, int i) const" -->
const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>ValueReal</b> (int num_row, int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a11fe8648ddd3e6e339eb81735d7e544e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::ValueReal" ref="a11fe8648ddd3e6e339eb81735d7e544e" args="(int num_row, int i)" -->
T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>ValueReal</b> (int num_row, int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2a3cd76154ac61a43afa42563805ce3b"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::IndexReal" ref="a2a3cd76154ac61a43afa42563805ce3b" args="(int num_row, int i) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>IndexReal</b> (int num_row, int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2fdb9482dad6378fb8ced1982b6860b1"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::IndexReal" ref="a2fdb9482dad6378fb8ced1982b6860b1" args="(int num_row, int i)" -->
int &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>IndexReal</b> (int num_row, int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa2d9a2e69a269bf9d15b62d520cb560d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::ValueImag" ref="aa2d9a2e69a269bf9d15b62d520cb560d" args="(int num_row, int i) const" -->
const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>ValueImag</b> (int num_row, int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab410f72a305938081fbc07e868eb161b"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::ValueImag" ref="ab410f72a305938081fbc07e868eb161b" args="(int num_row, int i)" -->
T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>ValueImag</b> (int num_row, int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aedba64fa9b01bce6bb75e9e5a14b26c5"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::IndexImag" ref="aedba64fa9b01bce6bb75e9e5a14b26c5" args="(int num_row, int i) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>IndexImag</b> (int num_row, int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a011b15c1909f712a693de10f2c0c2328"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::IndexImag" ref="a011b15c1909f712a693de10f2c0c2328" args="(int num_row, int i)" -->
int &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>IndexImag</b> (int num_row, int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aab6c31240cd6dcbd442f9a29658f51ea"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::SetRealData" ref="aab6c31240cd6dcbd442f9a29658f51ea" args="(int, int, Vector&lt; T, VectSparse, Allocator &gt; *)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetRealData</b> (int, int, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; *)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1ebb3dcbf317ec99e6463f0aafea10b9"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::SetRealData" ref="a1ebb3dcbf317ec99e6463f0aafea10b9" args="(int, int, T *, int *)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetRealData</b> (int, int, T *, int *)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8e3c2dfa0ea5aa57cd8607cca15562f2"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::SetImagData" ref="a8e3c2dfa0ea5aa57cd8607cca15562f2" args="(int, int, Vector&lt; T, VectSparse, Allocator &gt; *)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetImagData</b> (int, int, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; *)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a50622f29a8e112821c68e5a28236258f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::SetImagData" ref="a50622f29a8e112821c68e5a28236258f" args="(int, int, T *, int *)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetImagData</b> (int, int, T *, int *)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9f0922d865cdd4a627912583a9c73151"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::NullifyReal" ref="a9f0922d865cdd4a627912583a9c73151" args="(int i)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>NullifyReal</b> (int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aed1192a75fe2c7d19e545ff6054298e2"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::NullifyReal" ref="aed1192a75fe2c7d19e545ff6054298e2" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>NullifyReal</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad5e87ca37baabfb8cdc6f841f5125d7d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::NullifyImag" ref="ad5e87ca37baabfb8cdc6f841f5125d7d" args="(int i)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>NullifyImag</b> (int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adbdb03e02fd8617a23188d6bc3155ec0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::NullifyImag" ref="adbdb03e02fd8617a23188d6bc3155ec0" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>NullifyImag</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac24abb2e047eb8b91728023074b3fb13"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::Print" ref="ac24abb2e047eb8b91728023074b3fb13" args="() const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Print</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9b54a14b64f67242fe9367df4142bd7f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::Write" ref="a9b54a14b64f67242fe9367df4142bd7f" args="(string FileName) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Write</b> (string FileName) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0cc7af3e3d677e5cb6bd5f66d7965a6f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::Write" ref="a0cc7af3e3d677e5cb6bd5f66d7965a6f" args="(ostream &amp;FileStream) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Write</b> (ostream &amp;FileStream) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7095c684ecdb9ef6e9fe9b84462e22ce"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::WriteText" ref="a7095c684ecdb9ef6e9fe9b84462e22ce" args="(string FileName) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>WriteText</b> (string FileName) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a884e62c07aa29b59b259d232f7771c94"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::WriteText" ref="a884e62c07aa29b59b259d232f7771c94" args="(ostream &amp;FileStream) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>WriteText</b> (ostream &amp;FileStream) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a58c2b7013db7765a01181f928833e530"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::Read" ref="a58c2b7013db7765a01181f928833e530" args="(string FileName)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Read</b> (string FileName)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8743f281d37cc023cd3e628eba4644f0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::Read" ref="a8743f281d37cc023cd3e628eba4644f0" args="(istream &amp;FileStream)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Read</b> (istream &amp;FileStream)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab0d4c7f4d291599c0d9cb735dd7d9dc8"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::ReadText" ref="ab0d4c7f4d291599c0d9cb735dd7d9dc8" args="(string FileName)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>ReadText</b> (string FileName)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a20de6511128d3ac679c885abcd5538d0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::ReadText" ref="a20de6511128d3ac679c885abcd5538d0" args="(istream &amp;FileStream)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>ReadText</b> (istream &amp;FileStream)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a889d3baa1c4aa6a96b83ba8ca7b4cf4a"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::Assemble" ref="a889d3baa1c4aa6a96b83ba8ca7b4cf4a" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Assemble</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa9a81eead891c0ee85f2563dc8be8a99"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::RemoveSmallEntry" ref="aa9a81eead891c0ee85f2563dc8be8a99" args="(const T0 &amp;epsilon)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>RemoveSmallEntry</b> (const T0 &amp;epsilon)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a19b48691b03953dffd387ace5cfddd37"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::SetIdentity" ref="a19b48691b03953dffd387ace5cfddd37" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetIdentity</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa6f51cb3c7779c309578058fd34c5d25"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::Zero" ref="aa6f51cb3c7779c309578058fd34c5d25" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Zero</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1fce8c713bc87ca4e06df819ced39554"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::Fill" ref="a1fce8c713bc87ca4e06df819ced39554" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Fill</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a08a5f1de809bbb4565e90d954ca053f6"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::Fill" ref="a08a5f1de809bbb4565e90d954ca053f6" args="(const complex&lt; T0 &gt; &amp;x)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Fill</b> (const complex&lt; T0 &gt; &amp;x)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad1ff9883f030ca9eaada43616865acdb"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::FillRand" ref="ad1ff9883f030ca9eaada43616865acdb" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>FillRand</b> ()</td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2032848b78edab7ef6721eda9e2784de"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::m_" ref="a2032848b78edab7ef6721eda9e2784de" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de">m_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of rows. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acfa7a2c6c7f77dc8ebcee2dd411ebf98"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::n_" ref="acfa7a2c6c7f77dc8ebcee2dd411ebf98" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98">n_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of columns. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af1766a47a3d45c0b904b07870204e4a6"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::val_real_" ref="af1766a47a3d45c0b904b07870204e4a6" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6">val_real_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">real part rows or columns <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afa77c763d3b3a280dfef2a4410ed0bd8"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::val_imag_" ref="afa77c763d3b3a280dfef2a4410ed0bd8" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8">val_imag_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">imaginary part rows or columns <br/></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Prop, class Allocator&gt;<br/>
 class Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</h3>

<p>Row-major symmetric sparse-matrix class. </p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8hxx_source.php#l00314">314</a> of file <a class="el" href="_matrix___array_complex_sparse_8hxx_source.php">Matrix_ArrayComplexSparse.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="a5436a5d9f8bf73dbceb229a0649facfe"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::Matrix" ref="a5436a5d9f8bf73dbceb229a0649facfe" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>Builds an empty matrix. </p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02565">2565</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2a8c65edac40bcf4ba7ecb3b5db27502"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::Matrix" ref="a2a8c65edac40bcf4ba7ecb3b5db27502" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Constructor. </p>
<p>Builds a i by j matrix </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> values are not initialized. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02579">2579</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="a9cce706f016372a361f025cf92c3cb25"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::AddInteraction" ref="a9cce706f016372a361f025cf92c3cb25" args="(int i, int j, const complex&lt; T &gt; &amp;val)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::AddInteraction </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const complex&lt; T &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds a coefficient in the matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>coefficient to add. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l03111">3111</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a4703da0eae726628352e0e68340c4ae4"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::AddInteractionColumn" ref="a4703da0eae726628352e0e68340c4ae4" args="(int i, int nb, const IVect &amp;row, const Vector&lt; complex&lt; T &gt;, VectFull, Alloc1 &gt; &amp;val)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Alloc1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::AddInteractionColumn </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nb</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>row</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; complex&lt; T &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds coefficients in a column. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>nb</em>&nbsp;</td><td>number of coefficients to add. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>row</em>&nbsp;</td><td>row numbers of coefficients. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>values of coefficients. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l03172">3172</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="adb9c19345b5e6f57b6d8f3dbec9bad93"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::AddInteractionRow" ref="adb9c19345b5e6f57b6d8f3dbec9bad93" args="(int i, int nb, const IVect &amp;col, const Vector&lt; complex&lt; T &gt;, VectFull, Alloc1 &gt; &amp;val)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Alloc1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::AddInteractionRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nb</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>col</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; complex&lt; T &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds coefficients in a row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>nb</em>&nbsp;</td><td>number of coefficients to add. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>col</em>&nbsp;</td><td>column numbers of coefficients. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>values of coefficients. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l03133">3133</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a44db6b452950f6d03227f0b49e8aa427"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::AssembleImagRow" ref="a44db6b452950f6d03227f0b49e8aa427" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::AssembleImagRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Assembles a column. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>If you are using the methods AddInteraction, you don't need to call that method. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l03097">3097</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0dd4ca70074352d9df0370707dd05127"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::AssembleRealRow" ref="a0dd4ca70074352d9df0370707dd05127" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::AssembleRealRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Assembles a column. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>If you are using the methods AddInteraction, you don't need to call that method. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l03083">3083</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="affbf4d5e0fb00ec74943e47af3859d87"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::GetImag" ref="affbf4d5e0fb00ec74943e47af3859d87" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T &amp; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::GetImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns access to imaginary part of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>imaginary part of element (i, j) of the matrix. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a644eff8b81954e1f43c4f9321a866903">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02808">2808</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af7fa7341f09012e5f0ab244a40ab0ca2"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::GetImag" ref="af7fa7341f09012e5f0ab244a40ab0ca2" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const T &amp; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::GetImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns access to imaginary part element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>imaginary part of element (i, j) of the matrix. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a3233d9091b1fe40403f0e1779f260033">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02837">2837</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af38c69e307e68125ec3304929b672e95"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::GetImagRowSize" ref="af38c69e307e68125ec3304929b672e95" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::GetImagRowSize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of non-zero entries of a row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of non-zero entries of the row i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l03051">3051</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a85eba6f5bb491fd35b28e5753f48eace"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::GetReal" ref="a85eba6f5bb491fd35b28e5753f48eace" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T &amp; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::GetReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns access to real part element (i, j). </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>real part of element (i, j) of the matrix. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ac91be8b2868c034acbf161a8b3af9598">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02690">2690</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a39ca8957207e6102cba15d2f0d7f47fb"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::GetReal" ref="a39ca8957207e6102cba15d2f0d7f47fb" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const T &amp; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::GetReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns access to real part of element (i, j). </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>real part of element (i, j) of the matrix. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ae562065dff1e5df2ec2516bf7f4f56ae">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02720">2720</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="afc6d830c2b6290ab90af0733ca2991d9"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::GetRealRowSize" ref="afc6d830c2b6290ab90af0733ca2991d9" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::GetRealRowSize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of non-zero entries of a row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of non-zero entries of the row i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l03037">3037</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5fb8630efa21241d2517927eeb80de5e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::operator()" ref="a5fb8630efa21241d2517927eeb80de5e" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">complex&lt; T &gt; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a30be6306dbdd79b0a282ef99289b0c2b">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02599">2599</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="add6c25193de11ecc3f405c1ba713e0ef"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::ReallocateImagRow" ref="add6c25193de11ecc3f405c1ba713e0ef" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::ReallocateImagRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates row i. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>new number of non-zero entries in the row. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Data may be lost. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02944">2944</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a828bff3d9a061706b80cf3cc4a6725eb"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::ReallocateRealRow" ref="a828bff3d9a061706b80cf3cc4a6725eb" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::ReallocateRealRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates row i. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>new number of non-zero entries in the row. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Data may be lost. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02930">2930</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a18e705d5da14b8ae374333f02a1801af"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::ReplaceImagIndexRow" ref="a18e705d5da14b8ae374333f02a1801af" args="(int i, IVect &amp;new_index)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::ReplaceImagIndexRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>new_index</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets column numbers of non-zero entries of a row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>new_index</em>&nbsp;</td><td>new column numbers. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l03023">3023</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5d858213b73625c365f2cf80c955b56c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::ReplaceRealIndexRow" ref="a5d858213b73625c365f2cf80c955b56c" args="(int i, IVect &amp;new_index)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::ReplaceRealIndexRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>new_index</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets column numbers of non-zero entries of a row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>new_index</em>&nbsp;</td><td>new column numbers. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l03009">3009</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a770cf431a7508fad913ef06bf487f9d6"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::ResizeImagRow" ref="a770cf431a7508fad913ef06bf487f9d6" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::ResizeImagRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates row i. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>new number of non-zero entries in the row. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02970">2970</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9a7d49e83e0838f63c4e7210ccc42218"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::ResizeRealRow" ref="a9a7d49e83e0838f63c4e7210ccc42218" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::ResizeRealRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates row i. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>new number of non-zero entries in the row. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02957">2957</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a49ac0164990312cc9485abe5de3a1d77"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::Set" ref="a49ac0164990312cc9485abe5de3a1d77" args="(int i, int j, const complex&lt; T &gt; &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::Set </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const complex&lt; T &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets element (i, j) of the matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>A(i, j) = x </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2dfca2deb2a823fc64dc51b7c0728f49">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02865">2865</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="abd914e374842533215ebeb12ae0fd5e6"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::SwapImagRow" ref="abd914e374842533215ebeb12ae0fd5e6" args="(int i, int i_)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::SwapImagRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Swaps two rows. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>first row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>second row number. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02996">2996</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="adf10d81ed34502289b1d928626921d91"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::SwapRealRow" ref="adf10d81ed34502289b1d928626921d91" args="(int i, int i_)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::SwapRealRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Swaps two rows. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>first row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>second row number. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02983">2983</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a8584e87cfb5ba75a746ec60a926f23a4"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::ValImag" ref="a8584e87cfb5ba75a746ec60a926f23a4" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T &amp; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::ValImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns access to imaginary part of element (i, j). </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>imaginary part of element (i, j) of the matrix. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aee8de4c9149d47935bf84fc04727c7f2">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02749">2749</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9798b4ecbfea2a0629c8b484d9be931a"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::ValImag" ref="a9798b4ecbfea2a0629c8b484d9be931a" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const T &amp; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::ValImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns access to imaginary part of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>imaginary part of element (i, j) of the matrix. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0324d6e453ce4c18dbc84bdc48fb4d5f">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02779">2779</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="abc507579e215d0fcd88b8d2bf8e710b9"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::ValReal" ref="abc507579e215d0fcd88b8d2bf8e710b9" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T &amp; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::ValReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns access to real part of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>real part of element (i, j) of the matrix. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#abdacf01dc354d0449b16b1d9e111ec54">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02629">2629</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2ccbdf02898e8b22ce9b5fe51aad89e1"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;::ValReal" ref="a2ccbdf02898e8b22ce9b5fe51aad89e1" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const T &amp; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator &gt;::ValReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns access to real part element (i, j). </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>real part of element (i, j) of the matrix. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2fa5f2b8aeb3c848f5749d113f93dce4">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l02660">2660</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>matrix_sparse/<a class="el" href="_matrix___array_complex_sparse_8hxx_source.php">Matrix_ArrayComplexSparse.hxx</a></li>
<li>matrix_sparse/<a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
