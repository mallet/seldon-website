<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a>, including all inherited members.<table>
  <tr bgcolor="#f0f0f0"><td><b>access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>allocator</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>allocator_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a6f0db73087115429868eaf0a35cf213b">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#ac3fcc459520d0ec52a599a8def09c4fc">Copy</a>(const Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>data_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>entry_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a5e66e9e50e3ab69c85be605d69f287e3">Fill</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a1d5639ed65870562ab47684054d98e4a">Fill</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#ae77c0cd9d596c959b2be5e9b78750e65">FillRand</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#aa47392b447a0c718d29050d204d5d465">Get</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#ae5f7cb3057f4315ee8a12e57223cf7b5">Get</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a9d331415858c046a3a72699d2bbd8ceb">GetDataSize</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">GetM</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">GetN</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">GetSize</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>m_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php#a607e3089e1ccddf239b481a8ddc556e9">Matrix</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php#ab80e64a037043f8ab6d77403ad62d9f3">Matrix</a>(int i, int j=0)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a47988d7972247286332e853c13bbc00b">Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ad2ea11b0880dc32ba9472da9310c332b">Matrix_Base</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline, explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a127e0229931486cea3e6007988b446a0">Matrix_Base</a>(const Matrix_Base&lt; T, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#ad9698cd3163185b5794550f516224d65">Matrix_TriangPacked</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a785e3d146fce6e050f7fbba140a94406">Matrix_TriangPacked</a>(int i, int j=0)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a61b5e4d1b404eab627203abdfa6287f8">Matrix_TriangPacked</a>(const Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>n_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a9aa8d70640c7e035ca45aa406c8e6bd1">Nullify</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#aa72fab293552e87ee528d11b04d361bf">operator()</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php#a6d3dfe89ad65b9daf47875ac529f7aa2">operator*=</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php#a17e0f0ea723e14c87abfbbecb99abfc9">operator=</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php#a0cf78d4b42de124bb3fb274c09166a75">operator=</a>(const Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#ac8f8519e62ace1acd0e0c514d080471a">Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;::operator=</a>(const Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a086500c5914460971f1a7c4d48c5f5b4">operator[]</a>(int i)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#ade6cf6ee8f39e79ae4419b5a4851b32e">operator[]</a>(int i) const</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a931e21546d023c42baf9c2d6cfddb3ee">Print</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a79f239a5a5a6921e11811349ffacc5f7">Print</a>(int a, int b, int m, int n) const</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a8ab4032c1a9a9ed651c607e124858720">Print</a>(int l) const</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>property</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a96f1f584b5d04f9b6c92647c1dc2b353">Read</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a2d646eec576003a589b1abff16a9f56a">Read</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#ae204d32dd619e2241945cc3bfbebc018">ReadText</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a2dd37f8c98e4d010227e800a414708d9">ReadText</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#ab0a798e9e9eac81e73e3176e8a4f20ac">Reallocate</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php#aa5fb51377f06688fd10f00ef5abd12dd">Resize</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a600ae1e3aaa10e9e1d4732d69a027ae1">Set</a>(int i, int j, const T &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SetData</b>(int i, int j, pointer data) (defined in <a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a888b8dc8faeb5654fda9a0517cea0c7b">SetIdentity</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>storage</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a0d4ea94aa72456c633fd26385f28f7a2">Val</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a2106123c9da877618fae231860846f6f">Val</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>value_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#ae5e50dae49d4cc0a56532151206184c8">Write</a>(string FileName) const</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#aa060ab9a7f68cb565019133802cb648e">Write</a>(ostream &amp;FileStream) const</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#ad2d7a4bde7fea4f6dbbaabb4adb8c21c">WriteText</a>(string FileName) const</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#ae03952200cc98d597c9bc07e91500f70">WriteText</a>(ostream &amp;FileStream) const</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a9944e918b0b65d7fc51c5b03ac772635">Zero</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af34a03c0cc56f757a83cd56f97c74ed8">~Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a3e4295fa0a1e9a2f3eba8bdda1221565">~Matrix_TriangPacked</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></td><td></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
