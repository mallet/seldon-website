<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix_sparse/IOMatrixMarket.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_SPARSE_IOMATRIXMARKET_CXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;IOMatrixMarket.hxx&quot;</span>
<a name="l00024"></a>00024 <span class="preprocessor">#include &lt;iomanip&gt;</span>
<a name="l00025"></a>00025 
<a name="l00026"></a>00026 <span class="keyword">namespace </span>Seldon
<a name="l00027"></a>00027 {
<a name="l00028"></a>00028 
<a name="l00029"></a>00029 
<a name="l00031"></a>00031 
<a name="l00044"></a>00044   <span class="keyword">template</span> &lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00045"></a>00045   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a20103abe515f8c7d1474e5422915a6e6" title="Reads a sparse matrix in a file in Harwell-Boeing format.">ReadHarwellBoeing</a>(<span class="keywordtype">string</span> filename,
<a name="l00046"></a>00046                          Matrix&lt;float, Prop, ColSparse, Allocator&gt;&amp; A)
<a name="l00047"></a>00047   {
<a name="l00048"></a>00048     <a class="code" href="namespace_seldon.php#a20103abe515f8c7d1474e5422915a6e6" title="Reads a sparse matrix in a file in Harwell-Boeing format.">ReadHarwellBoeing</a>(filename, <span class="stringliteral">&quot;real&quot;</span>, <span class="stringliteral">&quot;general&quot;</span>, A);
<a name="l00049"></a>00049   }
<a name="l00050"></a>00050 
<a name="l00051"></a>00051 
<a name="l00053"></a>00053   <span class="keyword">template</span> &lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00054"></a>00054   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a20103abe515f8c7d1474e5422915a6e6" title="Reads a sparse matrix in a file in Harwell-Boeing format.">ReadHarwellBoeing</a>(<span class="keywordtype">string</span> filename,
<a name="l00055"></a>00055                          Matrix&lt;double, Prop, ColSparse, Allocator&gt;&amp; A)
<a name="l00056"></a>00056   {
<a name="l00057"></a>00057     <a class="code" href="namespace_seldon.php#a20103abe515f8c7d1474e5422915a6e6" title="Reads a sparse matrix in a file in Harwell-Boeing format.">ReadHarwellBoeing</a>(filename, <span class="stringliteral">&quot;real&quot;</span>, <span class="stringliteral">&quot;general&quot;</span>, A);
<a name="l00058"></a>00058   }
<a name="l00059"></a>00059 
<a name="l00060"></a>00060 
<a name="l00062"></a>00062 
<a name="l00080"></a>00080   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00081"></a>00081   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a20103abe515f8c7d1474e5422915a6e6" title="Reads a sparse matrix in a file in Harwell-Boeing format.">ReadHarwellBoeing</a>(<span class="keywordtype">string</span> filename,
<a name="l00082"></a>00082                          <span class="keywordtype">string</span> value_type, <span class="keywordtype">string</span> matrix_type,
<a name="l00083"></a>00083                          Matrix&lt;T, Prop, Storage, Allocator&gt; &amp; A)
<a name="l00084"></a>00084   {
<a name="l00085"></a>00085     <span class="keywordtype">int</span> i, j, k;
<a name="l00086"></a>00086     <span class="keywordtype">string</span> line, element;
<a name="l00087"></a>00087 
<a name="l00088"></a>00088     <span class="comment">// Dimensions of the output matrix.</span>
<a name="l00089"></a>00089     <span class="keywordtype">int</span> Nrow, Ncol;
<a name="l00090"></a>00090     <span class="keywordtype">int</span> Nnonzero;
<a name="l00091"></a>00091 
<a name="l00092"></a>00092     ifstream input_stream(filename.c_str());
<a name="l00093"></a>00093 
<a name="l00094"></a>00094 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00095"></a>00095 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00096"></a>00096     <span class="keywordflow">if</span> (!input_stream.good())
<a name="l00097"></a>00097       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;ReadHarwellBoeing(string filename, &quot;</span>
<a name="l00098"></a>00098                     <span class="stringliteral">&quot;Matrix&amp; A, string value_type, string matrix_type)&quot;</span>,
<a name="l00099"></a>00099                     <span class="stringliteral">&quot;Unable to read file \&quot;&quot;</span> + filename + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00100"></a>00100 <span class="preprocessor">#endif</span>
<a name="l00101"></a>00101 <span class="preprocessor"></span>
<a name="l00102"></a>00102     <span class="comment">/*** Parsing headers ***/</span>
<a name="l00103"></a>00103 
<a name="l00104"></a>00104     <span class="comment">// First line.</span>
<a name="l00105"></a>00105     <span class="keywordtype">string</span> title, name;
<a name="l00106"></a>00106     getline(input_stream, line);
<a name="l00107"></a>00107     title = line.substr(0, 72);
<a name="l00108"></a>00108     name = line.substr(72, 8);
<a name="l00109"></a>00109 
<a name="l00110"></a>00110     <span class="comment">// Second line.</span>
<a name="l00111"></a>00111     <span class="keywordtype">int</span> Nline_ptr, Nline_ind, Nline_val, Nline_rhs;
<a name="l00112"></a>00112     input_stream &gt;&gt; i &gt;&gt; Nline_ptr &gt;&gt; Nline_ind &gt;&gt; Nline_val &gt;&gt; Nline_rhs;
<a name="l00113"></a>00113 
<a name="l00114"></a>00114     <span class="comment">// Third line.</span>
<a name="l00115"></a>00115     <span class="keywordtype">string</span> type;
<a name="l00116"></a>00116     <span class="keywordtype">int</span> Nelemental;
<a name="l00117"></a>00117     input_stream &gt;&gt; type &gt;&gt; Nrow &gt;&gt; Ncol &gt;&gt; Nnonzero &gt;&gt; Nelemental;
<a name="l00118"></a>00118 
<a name="l00119"></a>00119     <span class="keywordflow">if</span> (type.size() != 3)
<a name="l00120"></a>00120       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;ReadHarwellBoeing(string filename, &quot;</span>
<a name="l00121"></a>00121                           <span class="stringliteral">&quot;Matrix&amp; A, string value_type, string matrix_type)&quot;</span>,
<a name="l00122"></a>00122                           <span class="stringliteral">&quot;File \&quot;&quot;</span> + filename + <span class="stringliteral">&quot;\&quot; contains a matrix of &quot;</span>
<a name="l00123"></a>00123                           <span class="stringliteral">&quot;type \&quot;&quot;</span> + type + <span class="stringliteral">&quot;\&quot;, which is not a valid type. &quot;</span>
<a name="l00124"></a>00124                           <span class="stringliteral">&quot;The type must contain exactly three characters.&quot;</span>);
<a name="l00125"></a>00125 
<a name="l00126"></a>00126     <span class="keywordflow">if</span> (type.substr(0, 1) != <span class="stringliteral">&quot;R&quot;</span> &amp;&amp; type.substr(0, 1) != <span class="stringliteral">&quot;C&quot;</span>
<a name="l00127"></a>00127         &amp;&amp; type.substr(0, 1) != <span class="stringliteral">&quot;P&quot;</span>)
<a name="l00128"></a>00128       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;ReadHarwellBoeing(string filename, &quot;</span>
<a name="l00129"></a>00129                           <span class="stringliteral">&quot;Matrix&amp; A, string value_type, string matrix_type)&quot;</span>,
<a name="l00130"></a>00130                           <span class="stringliteral">&quot;File \&quot;&quot;</span> + filename + <span class="stringliteral">&quot;\&quot; contains a matrix of &quot;</span>
<a name="l00131"></a>00131                           <span class="stringliteral">&quot;type \&quot;&quot;</span> + type + <span class="stringliteral">&quot;\&quot;, which is not a valid type. &quot;</span>
<a name="l00132"></a>00132                           <span class="stringliteral">&quot;The first character of that type must be &#39;R&#39;, &#39;C&#39; &quot;</span>
<a name="l00133"></a>00133                           <span class="stringliteral">&quot;(not supported anyway) or &#39;P&#39;.&quot;</span>);
<a name="l00134"></a>00134 
<a name="l00135"></a>00135     <span class="keywordflow">if</span> (type.substr(0, 1) == <span class="stringliteral">&quot;C&quot;</span>)
<a name="l00136"></a>00136       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;ReadHarwellBoeing(string filename, &quot;</span>
<a name="l00137"></a>00137                           <span class="stringliteral">&quot;Matrix&amp; A, string value_type, string matrix_type)&quot;</span>,
<a name="l00138"></a>00138                           <span class="stringliteral">&quot;File \&quot;&quot;</span> + filename + <span class="stringliteral">&quot;\&quot; contains a &quot;</span>
<a name="l00139"></a>00139                           <span class="stringliteral">&quot;complex-valued matrix, which is not supported.&quot;</span>);
<a name="l00140"></a>00140 
<a name="l00141"></a>00141     <span class="keywordflow">if</span> (type.substr(0, 1) == <span class="stringliteral">&quot;R&quot;</span> &amp;&amp; value_type != <span class="stringliteral">&quot;real&quot;</span>)
<a name="l00142"></a>00142       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;ReadHarwellBoeing(string filename, &quot;</span>
<a name="l00143"></a>00143                           <span class="stringliteral">&quot;Matrix&amp; A, string value_type, string matrix_type)&quot;</span>,
<a name="l00144"></a>00144                           <span class="stringliteral">&quot;File \&quot;&quot;</span> + filename + <span class="stringliteral">&quot;\&quot; contains a real-valued &quot;</span>
<a name="l00145"></a>00145                           <span class="stringliteral">&quot;matrix, while the input matrix &#39;A&#39; is not &quot;</span>
<a name="l00146"></a>00146                           <span class="stringliteral">&quot;declared as such.&quot;</span>);
<a name="l00147"></a>00147 
<a name="l00148"></a>00148     <span class="keywordflow">if</span> (type.substr(1, 1) == <span class="stringliteral">&quot;H&quot;</span>)
<a name="l00149"></a>00149       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;ReadHarwellBoeing(string filename, &quot;</span>
<a name="l00150"></a>00150                           <span class="stringliteral">&quot;Matrix&amp; A, string value_type, string matrix_type)&quot;</span>,
<a name="l00151"></a>00151                           <span class="stringliteral">&quot;File \&quot;&quot;</span> + filename + <span class="stringliteral">&quot;\&quot; contains a Hermitian &quot;</span>
<a name="l00152"></a>00152                           <span class="stringliteral">&quot;matrix, which is not supported.&quot;</span>);
<a name="l00153"></a>00153 
<a name="l00154"></a>00154     <span class="keywordflow">if</span> (type.substr(1, 1) == <span class="stringliteral">&quot;Z&quot;</span>)
<a name="l00155"></a>00155       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;ReadHarwellBoeing(string filename, &quot;</span>
<a name="l00156"></a>00156                           <span class="stringliteral">&quot;Matrix&amp; A, string value_type, string matrix_type)&quot;</span>,
<a name="l00157"></a>00157                           <span class="stringliteral">&quot;File \&quot;&quot;</span> + filename + <span class="stringliteral">&quot;\&quot; contains a skew &quot;</span>
<a name="l00158"></a>00158                           <span class="stringliteral">&quot;symmetric matrix, which is not supported.&quot;</span>);
<a name="l00159"></a>00159 
<a name="l00160"></a>00160     <span class="keywordflow">if</span> (type.substr(1, 1) == <span class="stringliteral">&quot;S&quot;</span>)
<a name="l00161"></a>00161       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;ReadHarwellBoeing(string filename, &quot;</span>
<a name="l00162"></a>00162                           <span class="stringliteral">&quot;Matrix&amp; A, string value_type, string matrix_type)&quot;</span>,
<a name="l00163"></a>00163                           <span class="stringliteral">&quot;File \&quot;&quot;</span> + filename + <span class="stringliteral">&quot;\&quot; contains a &quot;</span>
<a name="l00164"></a>00164                           <span class="stringliteral">&quot;symmetric matrix, which is not supported.&quot;</span>);
<a name="l00165"></a>00165 
<a name="l00166"></a>00166     <span class="keywordflow">if</span> (Nelemental != 0 || type.substr(2, 1) == <span class="stringliteral">&quot;E&quot;</span>)
<a name="l00167"></a>00167       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;ReadHarwellBoeing(string filename, &quot;</span>
<a name="l00168"></a>00168                           <span class="stringliteral">&quot;Matrix&amp; A, string value_type, string matrix_type)&quot;</span>,
<a name="l00169"></a>00169                           <span class="stringliteral">&quot;File \&quot;&quot;</span> + filename + <span class="stringliteral">&quot;\&quot; contains an elemental &quot;</span>
<a name="l00170"></a>00170                           <span class="stringliteral">&quot;matrix, which is not supported.&quot;</span>);
<a name="l00171"></a>00171 
<a name="l00172"></a>00172     getline(input_stream, line);
<a name="l00173"></a>00173 
<a name="l00174"></a>00174     <span class="comment">// Fourth line.</span>
<a name="l00175"></a>00175     getline(input_stream, line);
<a name="l00176"></a>00176     <span class="keywordtype">int</span> line_width_ptr, element_width_ptr, line_width_ind, element_width_ind;
<a name="l00177"></a>00177     element = line.substr(0, 16);
<a name="l00178"></a>00178     sscanf(element.c_str(), <span class="stringliteral">&quot;(%dI%d)&quot;</span>, &amp;line_width_ptr, &amp;element_width_ptr);
<a name="l00179"></a>00179     element = line.substr(16, 16);
<a name="l00180"></a>00180     sscanf(element.c_str(), <span class="stringliteral">&quot;(%dI%d)&quot;</span>, &amp;line_width_ind, &amp;element_width_ind);
<a name="l00181"></a>00181     <span class="keywordtype">int</span> line_width_val = 0;
<a name="l00182"></a>00182     <span class="keywordtype">int</span> element_width_val = 0;
<a name="l00183"></a>00183     <span class="keywordflow">if</span> (type.substr(0, 1) != <span class="stringliteral">&quot;P&quot;</span>)
<a name="l00184"></a>00184       {
<a name="l00185"></a>00185         element = line.substr(32, 16);
<a name="l00186"></a>00186 
<a name="l00187"></a>00187         <span class="comment">// Splits the format to retrieve the useful widths. This part is</span>
<a name="l00188"></a>00188         <span class="comment">// tricky because no parsing (in C++) of Fortran format was found.</span>
<a name="l00189"></a>00189         vector&lt;int&gt; vect;
<a name="l00190"></a>00190         <span class="keywordtype">string</span> delimiter = <span class="stringliteral">&quot; (ABEFGILOPZ*.)&quot;</span>;
<a name="l00191"></a>00191         <span class="keywordtype">string</span> tmp_str;
<a name="l00192"></a>00192         <span class="keywordtype">int</span> tmp_int;
<a name="l00193"></a>00193         string::size_type index_beg = element.find_first_not_of(delimiter);
<a name="l00194"></a>00194         string::size_type index_end;
<a name="l00195"></a>00195         <span class="keywordflow">while</span> (index_beg != string::npos)
<a name="l00196"></a>00196           {
<a name="l00197"></a>00197             index_end = element.find_first_of(delimiter, index_beg);
<a name="l00198"></a>00198             tmp_str = element.substr(index_beg,
<a name="l00199"></a>00199                                      index_end == string::npos ?
<a name="l00200"></a>00200                                      string::npos : (index_end-index_beg));
<a name="l00201"></a>00201             istringstream(tmp_str) &gt;&gt; tmp_int;
<a name="l00202"></a>00202             vect.push_back(tmp_int);
<a name="l00203"></a>00203             index_beg = element.find_first_not_of(delimiter, index_end);
<a name="l00204"></a>00204           }
<a name="l00205"></a>00205 
<a name="l00206"></a>00206         <span class="keywordflow">if</span> (vect.size() &lt; 3)
<a name="l00207"></a>00207           <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;ReadHarwellBoeing(string filename, Matrix&amp; A, &quot;</span>
<a name="l00208"></a>00208                               <span class="stringliteral">&quot;string value_type, string matrix_type)&quot;</span>,
<a name="l00209"></a>00209                               <span class="stringliteral">&quot;File \&quot;&quot;</span> + filename + <span class="stringliteral">&quot;\&quot; contains values &quot;</span>
<a name="l00210"></a>00210                               <span class="stringliteral">&quot;written in format \&quot;&quot;</span> + element + <span class="stringliteral">&quot;\&quot;, which &quot;</span>
<a name="l00211"></a>00211                               <span class="stringliteral">&quot;could not be parsed.&quot;</span>);
<a name="l00212"></a>00212 
<a name="l00213"></a>00213         line_width_val = vect[vect.size() - 3];
<a name="l00214"></a>00214         element_width_val = vect[vect.size() - 2];
<a name="l00215"></a>00215       }
<a name="l00216"></a>00216 
<a name="l00217"></a>00217     <span class="comment">// Fifth header line, if any: ignored. RHS are not read.</span>
<a name="l00218"></a>00218     <span class="keywordflow">if</span> (Nline_rhs != 0)
<a name="l00219"></a>00219       getline(input_stream, line);
<a name="l00220"></a>00220 
<a name="l00221"></a>00221     <span class="comment">/*** Allocations ***/</span>
<a name="l00222"></a>00222 
<a name="l00223"></a>00223     <span class="comment">// Content of output matrix A.</span>
<a name="l00224"></a>00224     <span class="keywordtype">int</span>* A_ptr;
<a name="l00225"></a>00225     <span class="keywordtype">int</span>* A_ind;
<a name="l00226"></a>00226     T* A_data;
<a name="l00227"></a>00227 
<a name="l00228"></a>00228 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00229"></a>00229 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00230"></a>00230       {
<a name="l00231"></a>00231 <span class="preprocessor">#endif</span>
<a name="l00232"></a>00232 <span class="preprocessor"></span>
<a name="l00233"></a>00233         A_ptr = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>(calloc(Ncol + 1, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)));
<a name="l00234"></a>00234 
<a name="l00235"></a>00235 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00236"></a>00236 <span class="preprocessor"></span>      }
<a name="l00237"></a>00237     <span class="keywordflow">catch</span> (...)
<a name="l00238"></a>00238       {
<a name="l00239"></a>00239         A_ptr = NULL;
<a name="l00240"></a>00240       }
<a name="l00241"></a>00241 
<a name="l00242"></a>00242     <span class="keywordflow">if</span> (A_ptr == NULL)
<a name="l00243"></a>00243       <span class="keywordflow">throw</span> NoMemory(<span class="stringliteral">&quot;ReadHarwellBoeing(string filename, &quot;</span>
<a name="l00244"></a>00244                      <span class="stringliteral">&quot;Matrix&amp; A, string value_type, string matrix_type)&quot;</span>,
<a name="l00245"></a>00245                      <span class="stringliteral">&quot;Unable to allocate memory for an array of &quot;</span>
<a name="l00246"></a>00246                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Ncol + 1) + <span class="stringliteral">&quot; integers.&quot;</span>);
<a name="l00247"></a>00247 <span class="preprocessor">#endif</span>
<a name="l00248"></a>00248 <span class="preprocessor"></span>
<a name="l00249"></a>00249 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00250"></a>00250 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00251"></a>00251       {
<a name="l00252"></a>00252 <span class="preprocessor">#endif</span>
<a name="l00253"></a>00253 <span class="preprocessor"></span>
<a name="l00254"></a>00254         <span class="comment">// Reallocates &#39;A_ind&#39; and &#39;A_data&#39; in order to append the</span>
<a name="l00255"></a>00255         <span class="comment">// elements of the i-th row of C.</span>
<a name="l00256"></a>00256         A_ind = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>(calloc(Nnonzero, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)));
<a name="l00257"></a>00257         A_data = <span class="keyword">reinterpret_cast&lt;</span>T*<span class="keyword">&gt;</span>
<a name="l00258"></a>00258           (A.GetAllocator().allocate(Nnonzero));
<a name="l00259"></a>00259 
<a name="l00260"></a>00260 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00261"></a>00261 <span class="preprocessor"></span>      }
<a name="l00262"></a>00262     <span class="keywordflow">catch</span> (...)
<a name="l00263"></a>00263       {
<a name="l00264"></a>00264         A_ind = NULL;
<a name="l00265"></a>00265         A_data = NULL;
<a name="l00266"></a>00266       }
<a name="l00267"></a>00267 
<a name="l00268"></a>00268     <span class="keywordflow">if</span> (A_ind == NULL || A_data == NULL)
<a name="l00269"></a>00269       <span class="keywordflow">throw</span> NoMemory(<span class="stringliteral">&quot;ReadHarwellBoeing(string filename, &quot;</span>
<a name="l00270"></a>00270                      <span class="stringliteral">&quot;Matrix&amp; A, string value_type, string matrix_type)&quot;</span>,
<a name="l00271"></a>00271                      <span class="stringliteral">&quot;Unable to allocate memory for an array of &quot;</span>
<a name="l00272"></a>00272                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nnonzero) + <span class="stringliteral">&quot; integers &quot;</span>
<a name="l00273"></a>00273                      <span class="stringliteral">&quot;and for an array of &quot;</span>
<a name="l00274"></a>00274                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(T) * Nnonzero) + <span class="stringliteral">&quot; bytes.&quot;</span>);
<a name="l00275"></a>00275 <span class="preprocessor">#endif</span>
<a name="l00276"></a>00276 <span class="preprocessor"></span>
<a name="l00277"></a>00277     <span class="comment">/*** Reads the structure ***/</span>
<a name="l00278"></a>00278 
<a name="l00279"></a>00279     <span class="keywordtype">int</span> index = 0;
<a name="l00280"></a>00280     <span class="keywordflow">for</span> (i = 0; i &lt; Nline_ptr; i++)
<a name="l00281"></a>00281       {
<a name="l00282"></a>00282         getline(input_stream, line);
<a name="l00283"></a>00283         k = 0;
<a name="l00284"></a>00284         <span class="keywordflow">for</span> (j = 0; j &lt; line_width_ptr; j++)
<a name="l00285"></a>00285           {
<a name="l00286"></a>00286             istringstream(line.substr(k, element_width_ptr)) &gt;&gt; A_ptr[index];
<a name="l00287"></a>00287             <span class="comment">// The indexes are 1-based, so this corrects it:</span>
<a name="l00288"></a>00288             A_ptr[index]--;
<a name="l00289"></a>00289             index++;
<a name="l00290"></a>00290             <span class="keywordflow">if</span> (index == Ncol + 1)
<a name="l00291"></a>00291               <span class="comment">// So as not to read more elements than actually available on</span>
<a name="l00292"></a>00292               <span class="comment">// the line.</span>
<a name="l00293"></a>00293               <span class="keywordflow">break</span>;
<a name="l00294"></a>00294             k += element_width_ptr;
<a name="l00295"></a>00295           }
<a name="l00296"></a>00296         <span class="keywordflow">if</span> (index == Ncol + 1)
<a name="l00297"></a>00297           <span class="keywordflow">break</span>;
<a name="l00298"></a>00298       }
<a name="l00299"></a>00299 
<a name="l00300"></a>00300     index = 0;
<a name="l00301"></a>00301     <span class="keywordflow">for</span> (i = 0; i &lt; Nline_ind; i++)
<a name="l00302"></a>00302       {
<a name="l00303"></a>00303         getline(input_stream, line);
<a name="l00304"></a>00304         k = 0;
<a name="l00305"></a>00305         <span class="keywordflow">for</span> (j = 0; j &lt; line_width_ind; j++)
<a name="l00306"></a>00306           {
<a name="l00307"></a>00307             istringstream(line.substr(k, element_width_ind)) &gt;&gt; A_ind[index];
<a name="l00308"></a>00308             <span class="comment">// The indexes are 1-based, so this corrects it:</span>
<a name="l00309"></a>00309             A_ind[index]--;
<a name="l00310"></a>00310             index++;
<a name="l00311"></a>00311             <span class="keywordflow">if</span> (index == Nnonzero)
<a name="l00312"></a>00312               <span class="comment">// So as not to read more elements than actually available on</span>
<a name="l00313"></a>00313               <span class="comment">// the line.</span>
<a name="l00314"></a>00314               <span class="keywordflow">break</span>;
<a name="l00315"></a>00315             k += element_width_ind;
<a name="l00316"></a>00316           }
<a name="l00317"></a>00317         <span class="keywordflow">if</span> (index == Nnonzero)
<a name="l00318"></a>00318           <span class="keywordflow">break</span>;
<a name="l00319"></a>00319       }
<a name="l00320"></a>00320 
<a name="l00321"></a>00321     <span class="comment">/*** Reads the values ***/</span>
<a name="l00322"></a>00322 
<a name="l00323"></a>00323     <span class="keywordflow">if</span> (type.substr(0, 1) == <span class="stringliteral">&quot;P&quot;</span>)
<a name="l00324"></a>00324       <span class="keywordflow">for</span> (i = 0; i &lt; Nnonzero; i++)
<a name="l00325"></a>00325         A_data[i] = T(1);
<a name="l00326"></a>00326     <span class="keywordflow">else</span>
<a name="l00327"></a>00327       {
<a name="l00328"></a>00328         index = 0;
<a name="l00329"></a>00329         <span class="keywordflow">for</span> (i = 0; i &lt; Nline_val; i++)
<a name="l00330"></a>00330           {
<a name="l00331"></a>00331             getline(input_stream, line);
<a name="l00332"></a>00332             k = 0;
<a name="l00333"></a>00333             <span class="keywordflow">for</span> (j = 0; j &lt; line_width_val; j++)
<a name="l00334"></a>00334               {
<a name="l00335"></a>00335                 istringstream(line.substr(k, element_width_val))
<a name="l00336"></a>00336                   &gt;&gt; A_data[index];
<a name="l00337"></a>00337                 index++;
<a name="l00338"></a>00338                 <span class="keywordflow">if</span> (index == Nnonzero)
<a name="l00339"></a>00339                   <span class="comment">// So as not to read more elements than actually available</span>
<a name="l00340"></a>00340                   <span class="comment">// on the line.</span>
<a name="l00341"></a>00341                   <span class="keywordflow">break</span>;
<a name="l00342"></a>00342                 k += element_width_val;
<a name="l00343"></a>00343               }
<a name="l00344"></a>00344             <span class="keywordflow">if</span> (index == Nnonzero)
<a name="l00345"></a>00345               <span class="keywordflow">break</span>;
<a name="l00346"></a>00346           }
<a name="l00347"></a>00347       }
<a name="l00348"></a>00348 
<a name="l00349"></a>00349 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00350"></a>00350 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is still in good shape.</span>
<a name="l00351"></a>00351     <span class="keywordflow">if</span> (!input_stream.good())
<a name="l00352"></a>00352       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;ReadHarwellBoeing(string filename, &quot;</span>
<a name="l00353"></a>00353                     <span class="stringliteral">&quot;Matrix&amp; A, string value_type, string matrix_type)&quot;</span>,
<a name="l00354"></a>00354                     <span class="stringliteral">&quot;Unable to read all values in file \&quot;&quot;</span>
<a name="l00355"></a>00355                     + filename + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00356"></a>00356 <span class="preprocessor">#endif</span>
<a name="l00357"></a>00357 <span class="preprocessor"></span>
<a name="l00358"></a>00358     input_stream.close();
<a name="l00359"></a>00359 
<a name="l00360"></a>00360     A.SetData(Nrow, Ncol, Nnonzero, A_data, A_ptr, A_ind);
<a name="l00361"></a>00361   }
<a name="l00362"></a>00362 
<a name="l00363"></a>00363 
<a name="l00364"></a>00364   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00365"></a>00365   <span class="keywordtype">void</span> PrintComplexValuesHarwell(<span class="keywordtype">int</span> nnz, <span class="keyword">const</span> Vector&lt;complex&lt;T&gt; &gt;&amp; Val,
<a name="l00366"></a>00366                                  ofstream&amp; file_out)
<a name="l00367"></a>00367   {
<a name="l00368"></a>00368     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; 2*nnz; i += 3)
<a name="l00369"></a>00369       {
<a name="l00370"></a>00370         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = i; j &lt; min(i+3, 2*nnz); j++)
<a name="l00371"></a>00371           {
<a name="l00372"></a>00372             <span class="keywordflow">if</span> (j%2 == 0)
<a name="l00373"></a>00373               file_out &lt;&lt; setw(23) &lt;&lt; real(Val(j/2));
<a name="l00374"></a>00374             <span class="keywordflow">else</span>
<a name="l00375"></a>00375               file_out &lt;&lt; setw(23) &lt;&lt; imag(Val(j/2));
<a name="l00376"></a>00376           }
<a name="l00377"></a>00377 
<a name="l00378"></a>00378         file_out &lt;&lt; <span class="charliteral">&#39;\n&#39;</span>;
<a name="l00379"></a>00379       }
<a name="l00380"></a>00380   }
<a name="l00381"></a>00381 
<a name="l00382"></a>00382 
<a name="l00383"></a>00383   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00384"></a>00384   <span class="keywordtype">void</span> PrintComplexValuesHarwell(<span class="keywordtype">int</span> nnz, <span class="keyword">const</span> Vector&lt;T&gt;&amp; Val,
<a name="l00385"></a>00385                                  ofstream&amp; file_out)
<a name="l00386"></a>00386   {
<a name="l00387"></a>00387     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; nnz; i += 3)
<a name="l00388"></a>00388       {
<a name="l00389"></a>00389         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = i; j &lt; min(i+3, nnz); j++)
<a name="l00390"></a>00390           file_out &lt;&lt; setw(23) &lt;&lt; Val(j);
<a name="l00391"></a>00391 
<a name="l00392"></a>00392         file_out &lt;&lt; <span class="charliteral">&#39;\n&#39;</span>;
<a name="l00393"></a>00393       }
<a name="l00394"></a>00394   }
<a name="l00395"></a>00395 
<a name="l00396"></a>00396 
<a name="l00397"></a>00397   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00398"></a>00398   <span class="keywordtype">void</span> WriteHarwellBoeing(<span class="keyword">const</span> Matrix&lt;T, Prop, Storage, Allocator&gt;&amp; A,
<a name="l00399"></a>00399                           <span class="keyword">const</span> <span class="keywordtype">string</span>&amp; file_name, <span class="keywordtype">bool</span> complex)
<a name="l00400"></a>00400   {
<a name="l00401"></a>00401     <span class="comment">// converting to CSR</span>
<a name="l00402"></a>00402     IVect Ptr, Ind;
<a name="l00403"></a>00403     Vector&lt;T&gt; Val;
<a name="l00404"></a>00404     General sym;
<a name="l00405"></a>00405     <a class="code" href="namespace_seldon.php#af590e7294696236e540099421ca518d7" title="Conversion from RowSparse to CSC format.">ConvertToCSC</a>(A, sym, Ptr, Ind, Val);
<a name="l00406"></a>00406 
<a name="l00407"></a>00407     <span class="comment">// number of columns and non-zero entries</span>
<a name="l00408"></a>00408     <span class="keywordtype">int</span> nnz = Val.GetM();
<a name="l00409"></a>00409     <span class="keywordtype">int</span> m = A.GetM();
<a name="l00410"></a>00410     <span class="keywordtype">int</span> n = A.GetN();
<a name="l00411"></a>00411 
<a name="l00412"></a>00412     <span class="comment">// First line</span>
<a name="l00413"></a>00413     ofstream file_out(file_name.data());
<a name="l00414"></a>00414 
<a name="l00415"></a>00415     <span class="keywordtype">string</span> title(<span class="stringliteral">&quot;Seldon&quot;</span>);
<a name="l00416"></a>00416     <span class="keywordtype">string</span> key(<span class="stringliteral">&quot;S0000008&quot;</span>);
<a name="l00417"></a>00417     file_out.setf(ios::left);
<a name="l00418"></a>00418     file_out &lt;&lt; setw(72) &lt;&lt; title;
<a name="l00419"></a>00419     file_out &lt;&lt; setw(8) &lt;&lt; key;
<a name="l00420"></a>00420     file_out &lt;&lt; endl;
<a name="l00421"></a>00421 
<a name="l00422"></a>00422     <span class="comment">// Compute column pointer format</span>
<a name="l00423"></a>00423     <span class="keywordtype">int</span> ptr_len = int(ceil( log10(0.1 + nnz + 1) )) + 1;
<a name="l00424"></a>00424     <span class="keywordtype">int</span> ptr_nperline = min(80 / ptr_len, n+1);
<a name="l00425"></a>00425     <span class="keywordtype">int</span> ptrcrd = n / ptr_nperline + 1;
<a name="l00426"></a>00426     <span class="keywordtype">string</span> ptrfmt = string(<span class="stringliteral">&quot;(&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(ptr_nperline)
<a name="l00427"></a>00427       + <span class="stringliteral">&quot;I&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(ptr_len) + <span class="stringliteral">&quot;)&quot;</span>;
<a name="l00428"></a>00428 
<a name="l00429"></a>00429     <span class="comment">// Compute row index format</span>
<a name="l00430"></a>00430     <span class="keywordtype">int</span> ind_len = int(ceil( log10(0.1 + n + 1) )) + 1;
<a name="l00431"></a>00431     <span class="keywordtype">int</span> ind_nperline = min(80 / ind_len, nnz);
<a name="l00432"></a>00432     <span class="keywordtype">int</span> indcrd = (nnz-1) / ind_nperline + 1;
<a name="l00433"></a>00433     <span class="keywordtype">string</span> indfmt = string(<span class="stringliteral">&quot;(&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(ind_nperline)
<a name="l00434"></a>00434       + <span class="charliteral">&#39;I&#39;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(ind_len) + <span class="charliteral">&#39;)&#39;</span>;
<a name="l00435"></a>00435 
<a name="l00436"></a>00436     <span class="comment">// compute value format</span>
<a name="l00437"></a>00437     <span class="keywordtype">string</span> valfmt(<span class="stringliteral">&quot;(3D23.15)&quot;</span>);
<a name="l00438"></a>00438     <span class="keywordtype">int</span> valcrd = (nnz-1) / 3 + 1;
<a name="l00439"></a>00439     <span class="keywordflow">if</span> (complex)
<a name="l00440"></a>00440       valcrd = (2*nnz-1) / 3 + 1;
<a name="l00441"></a>00441 
<a name="l00442"></a>00442     <span class="keywordtype">int</span> rhscrd = 0;
<a name="l00443"></a>00443     <span class="keywordtype">int</span> totcrd = ptrcrd + indcrd + valcrd + rhscrd;
<a name="l00444"></a>00444 
<a name="l00445"></a>00445     <span class="comment">// Second line</span>
<a name="l00446"></a>00446     file_out &lt;&lt; setw(14) &lt;&lt; totcrd;
<a name="l00447"></a>00447     file_out &lt;&lt; setw(14) &lt;&lt; ptrcrd;
<a name="l00448"></a>00448     file_out &lt;&lt; setw(14) &lt;&lt; indcrd;
<a name="l00449"></a>00449     file_out &lt;&lt; setw(14) &lt;&lt; valcrd;
<a name="l00450"></a>00450     file_out &lt;&lt; setw(14) &lt;&lt; rhscrd;
<a name="l00451"></a>00451     file_out &lt;&lt; endl;
<a name="l00452"></a>00452 
<a name="l00453"></a>00453     <span class="comment">// Third line</span>
<a name="l00454"></a>00454     <span class="keywordtype">int</span> neltvl = 0;
<a name="l00455"></a>00455     <span class="keywordtype">char</span> mxtype[4];
<a name="l00456"></a>00456     <span class="keywordflow">if</span> (complex)
<a name="l00457"></a>00457       mxtype[0] = <span class="charliteral">&#39;C&#39;</span>;
<a name="l00458"></a>00458     <span class="keywordflow">else</span>
<a name="l00459"></a>00459       mxtype[0] = <span class="charliteral">&#39;R&#39;</span>;
<a name="l00460"></a>00460 
<a name="l00461"></a>00461     <span class="keywordflow">if</span> (m == n)
<a name="l00462"></a>00462       mxtype[1] = <span class="charliteral">&#39;U&#39;</span>;
<a name="l00463"></a>00463     <span class="keywordflow">else</span>
<a name="l00464"></a>00464       mxtype[1] = <span class="charliteral">&#39;R&#39;</span>;
<a name="l00465"></a>00465 
<a name="l00466"></a>00466     mxtype[2] = <span class="charliteral">&#39;A&#39;</span>;
<a name="l00467"></a>00467     mxtype[3] = <span class="charliteral">&#39;\0&#39;</span>;
<a name="l00468"></a>00468 
<a name="l00469"></a>00469     file_out &lt;&lt; mxtype &lt;&lt; <span class="stringliteral">&quot;           &quot;</span>;
<a name="l00470"></a>00470     file_out &lt;&lt; setw(14) &lt;&lt; m;
<a name="l00471"></a>00471     file_out &lt;&lt; setw(14) &lt;&lt; n;
<a name="l00472"></a>00472     file_out &lt;&lt; setw(14) &lt;&lt; nnz;
<a name="l00473"></a>00473     file_out &lt;&lt; setw(14) &lt;&lt; neltvl;
<a name="l00474"></a>00474     file_out &lt;&lt; endl;
<a name="l00475"></a>00475 
<a name="l00476"></a>00476     <span class="comment">// Fourth line</span>
<a name="l00477"></a>00477     file_out &lt;&lt; setw(16) &lt;&lt; ptrfmt;
<a name="l00478"></a>00478     file_out &lt;&lt; setw(16) &lt;&lt; indfmt;
<a name="l00479"></a>00479     file_out &lt;&lt; setw(20) &lt;&lt; valfmt;
<a name="l00480"></a>00480     file_out &lt;&lt; setw(20) &lt;&lt; valfmt;
<a name="l00481"></a>00481     file_out &lt;&lt; endl;
<a name="l00482"></a>00482 
<a name="l00483"></a>00483     <span class="comment">// printing ptr values</span>
<a name="l00484"></a>00484     file_out.setf(ios::left);
<a name="l00485"></a>00485     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt;= n; i += ptr_nperline)
<a name="l00486"></a>00486       {
<a name="l00487"></a>00487         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = i; j &lt; min(i+ptr_nperline, n+1); j++)
<a name="l00488"></a>00488           file_out &lt;&lt; setw(ptr_len) &lt;&lt; Ptr(j)+1;
<a name="l00489"></a>00489 
<a name="l00490"></a>00490         file_out &lt;&lt; <span class="charliteral">&#39;\n&#39;</span>;
<a name="l00491"></a>00491       }
<a name="l00492"></a>00492 
<a name="l00493"></a>00493     <span class="comment">// printing ind values</span>
<a name="l00494"></a>00494     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; nnz; i += ind_nperline)
<a name="l00495"></a>00495       {
<a name="l00496"></a>00496         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = i; j &lt; min(i+ind_nperline, nnz); j++)
<a name="l00497"></a>00497           file_out &lt;&lt; setw(ind_len) &lt;&lt; Ind(j)+1;
<a name="l00498"></a>00498 
<a name="l00499"></a>00499         file_out &lt;&lt; <span class="charliteral">&#39;\n&#39;</span>;
<a name="l00500"></a>00500       }
<a name="l00501"></a>00501 
<a name="l00502"></a>00502     <span class="comment">// printing values</span>
<a name="l00503"></a>00503     file_out.precision(15);
<a name="l00504"></a>00504     file_out.setf(ios::scientific);
<a name="l00505"></a>00505     PrintComplexValuesHarwell(nnz, Val, file_out);
<a name="l00506"></a>00506 
<a name="l00507"></a>00507     file_out.close();
<a name="l00508"></a>00508   }
<a name="l00509"></a>00509 
<a name="l00510"></a>00510 
<a name="l00512"></a>00512   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00513"></a>00513   <span class="keywordtype">void</span> WriteHarwellBoeing(<span class="keyword">const</span> Matrix&lt;T, Prop, Storage, Allocator&gt;&amp; A,
<a name="l00514"></a>00514                           <span class="keyword">const</span> <span class="keywordtype">string</span>&amp; file_name)
<a name="l00515"></a>00515   {
<a name="l00516"></a>00516     WriteHarwellBoeing(A, file_name, <span class="keyword">false</span>);
<a name="l00517"></a>00517   }
<a name="l00518"></a>00518 
<a name="l00519"></a>00519 
<a name="l00521"></a>00521   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00522"></a>00522   <span class="keywordtype">void</span> WriteHarwellBoeing(<span class="keyword">const</span> Matrix&lt;complex&lt;T&gt;,
<a name="l00523"></a>00523                           Prop, Storage, Allocator&gt;&amp; A,
<a name="l00524"></a>00524                           <span class="keyword">const</span> <span class="keywordtype">string</span>&amp; file_name)
<a name="l00525"></a>00525   {
<a name="l00526"></a>00526     WriteHarwellBoeing(A, file_name, <span class="keyword">true</span>);
<a name="l00527"></a>00527   }
<a name="l00528"></a>00528 
<a name="l00529"></a>00529 }
<a name="l00530"></a>00530 
<a name="l00531"></a>00531 
<a name="l00532"></a>00532 <span class="preprocessor">#define SELDON_FILE_MATRIX_SPARSE_IOMATRIXMARKET_CXX</span>
<a name="l00533"></a>00533 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
