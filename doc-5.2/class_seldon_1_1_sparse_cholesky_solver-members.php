<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::SparseCholeskySolver&lt; T &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a>, including all inherited members.<table>
  <tr bgcolor="#f0f0f0"><td><b>CHOLMOD</b> enum value (defined in <a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#a98bafc554b1ffc66030c99f8abf54f3d">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#ad33824372144f930799847ca4c8c93de">Factorize</a>(MatrixSparse &amp;A, bool keep_matrix=false)</td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#a7351dbce7f0e1a5241f93c6f57457248">GetDirectSolver</a>()</td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#ad301f547c1eb78c7f1c8ed2f3dbdec6b">GetM</a>() const </td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#a833d2fdb33c43d7648e490da7cceee52">GetN</a>() const </td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#aeb8fc6c681810ace10586969a22c935a">GetTypeOrdering</a>() const </td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#af3ec93eff3ba46583297067febb44f16">HideMessages</a>()</td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#a9e491c5c484a7e8b4d0cae095d4be34c">mat_sym</a></td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#abf3e9a17f8d27c777407ae73cb9b6a9b">Mlt</a>(const TransStatus &amp;TransA, Vector1 &amp;x)</td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#aae8e798c85e35d0225f5e0ec9f2d4c2f">n</a></td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#afbe06f619943edf715630578fca28f7a">permutation</a></td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#a877304542a5dfbcb2a09a308477627b6">print_level</a></td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SELDON_SOLVER</b> enum value (defined in <a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#ae580e7293920e4bde686f3dbe4ebb379">SelectDirectSolver</a>(int)</td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#a12507e190d0320641997052c66039c93">SelectOrdering</a>(int)</td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#aa08d8f448a3408e05f75ae0268b90739">SetOrdering</a>(const IVect &amp;)</td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#a24dc3be1fb9e8f479913b74f940384ed">ShowFullHistory</a>()</td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#ab8b96c5cc5065e333d3b79c32cdebe92">ShowMessages</a>()</td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#a34cf4c23c1a2fbd266d3f0466956e171">Solve</a>(const TransStatus &amp;TransA, Vector1 &amp;x)</td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#a9f1966f5fe27659ea3d479df51c43c81">SparseCholeskySolver</a>()</td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#a8fb8259e3a995aac27ac0a100897c1eb">type_ordering</a></td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#ad2298348f3c85c3aaa38652dfe829719">type_solver</a></td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#af5e2e3beb8f4ab7713e4729b3e7f86ac">xtmp</a></td><td><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
