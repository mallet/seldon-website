<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix/Matrix_Symmetric.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_SYMMETRIC_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;Matrix_Symmetric.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="keyword">namespace </span>Seldon
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 
<a name="l00029"></a>00029   <span class="comment">/****************</span>
<a name="l00030"></a>00030 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00031"></a>00031 <span class="comment">   ****************/</span>
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00035"></a>00035 
<a name="l00038"></a>00038   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00039"></a>00039   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a82951e97ce120fda463beebe44d9c5e3" title="Default constructor.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::Matrix_Symmetric</a>():
<a name="l00040"></a>00040     Matrix_Base&lt;T, Allocator&gt;()
<a name="l00041"></a>00041   {
<a name="l00042"></a>00042     me_ = NULL;
<a name="l00043"></a>00043   }
<a name="l00044"></a>00044 
<a name="l00045"></a>00045 
<a name="l00047"></a>00047 
<a name="l00052"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#ab4c39b6e45f2f22dbf8dd50ad8509ad7">00052</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00053"></a>00053   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a82951e97ce120fda463beebe44d9c5e3" title="Default constructor.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00054"></a>00054 <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a82951e97ce120fda463beebe44d9c5e3" title="Default constructor.">  ::Matrix_Symmetric</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j): <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, i)
<a name="l00055"></a>00055   {
<a name="l00056"></a>00056 
<a name="l00057"></a>00057 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00058"></a>00058 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00059"></a>00059       {
<a name="l00060"></a>00060 <span class="preprocessor">#endif</span>
<a name="l00061"></a>00061 <span class="preprocessor"></span>
<a name="l00062"></a>00062         me_ = <span class="keyword">reinterpret_cast&lt;</span>pointer*<span class="keyword">&gt;</span>( calloc(i, <span class="keyword">sizeof</span>(pointer)) );
<a name="l00063"></a>00063 
<a name="l00064"></a>00064 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00065"></a>00065 <span class="preprocessor"></span>      }
<a name="l00066"></a>00066     <span class="keywordflow">catch</span> (...)
<a name="l00067"></a>00067       {
<a name="l00068"></a>00068         this-&gt;m_ = 0;
<a name="l00069"></a>00069         this-&gt;n_ = 0;
<a name="l00070"></a>00070         me_ = NULL;
<a name="l00071"></a>00071         this-&gt;data_ = NULL;
<a name="l00072"></a>00072       }
<a name="l00073"></a>00073     <span class="keywordflow">if</span> (me_ == NULL)
<a name="l00074"></a>00074       {
<a name="l00075"></a>00075         this-&gt;m_ = 0;
<a name="l00076"></a>00076         this-&gt;n_ = 0;
<a name="l00077"></a>00077         this-&gt;data_ = NULL;
<a name="l00078"></a>00078       }
<a name="l00079"></a>00079     <span class="keywordflow">if</span> (me_ == NULL &amp;&amp; i != 0)
<a name="l00080"></a>00080       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Symmetric::Matrix_Symmetric(int, int)&quot;</span>,
<a name="l00081"></a>00081                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate memory for a matrix of size &quot;</span>)
<a name="l00082"></a>00082                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(static_cast&lt;long int&gt;(i)
<a name="l00083"></a>00083                               * static_cast&lt;long int&gt;(i)
<a name="l00084"></a>00084                               * static_cast&lt;long int&gt;(<span class="keyword">sizeof</span>(T)))
<a name="l00085"></a>00085                      + <span class="stringliteral">&quot; bytes (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; x &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i)
<a name="l00086"></a>00086                      + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00087"></a>00087 <span class="preprocessor">#endif</span>
<a name="l00088"></a>00088 <span class="preprocessor"></span>
<a name="l00089"></a>00089 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00090"></a>00090 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00091"></a>00091       {
<a name="l00092"></a>00092 <span class="preprocessor">#endif</span>
<a name="l00093"></a>00093 <span class="preprocessor"></span>
<a name="l00094"></a>00094         this-&gt;data_ = this-&gt;allocator_.allocate(i * i, <span class="keyword">this</span>);
<a name="l00095"></a>00095 
<a name="l00096"></a>00096 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00097"></a>00097 <span class="preprocessor"></span>      }
<a name="l00098"></a>00098     <span class="keywordflow">catch</span> (...)
<a name="l00099"></a>00099       {
<a name="l00100"></a>00100         this-&gt;m_ = 0;
<a name="l00101"></a>00101         this-&gt;n_ = 0;
<a name="l00102"></a>00102         free(me_);
<a name="l00103"></a>00103         me_ = NULL;
<a name="l00104"></a>00104         this-&gt;data_ = NULL;
<a name="l00105"></a>00105       }
<a name="l00106"></a>00106     <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00107"></a>00107       {
<a name="l00108"></a>00108         this-&gt;m_ = 0;
<a name="l00109"></a>00109         this-&gt;n_ = 0;
<a name="l00110"></a>00110         free(me_);
<a name="l00111"></a>00111         me_ = NULL;
<a name="l00112"></a>00112       }
<a name="l00113"></a>00113     <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; i != 0)
<a name="l00114"></a>00114       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Symmetric::Matrix_Symmetric(int, int)&quot;</span>,
<a name="l00115"></a>00115                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate memory for a matrix of size &quot;</span>)
<a name="l00116"></a>00116                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(static_cast&lt;long int&gt;(i)
<a name="l00117"></a>00117                               * static_cast&lt;long int&gt;(i)
<a name="l00118"></a>00118                               * static_cast&lt;long int&gt;(<span class="keyword">sizeof</span>(T)))
<a name="l00119"></a>00119                      + <span class="stringliteral">&quot; bytes (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; x &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i)
<a name="l00120"></a>00120                      + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00121"></a>00121 <span class="preprocessor">#endif</span>
<a name="l00122"></a>00122 <span class="preprocessor"></span>
<a name="l00123"></a>00123     pointer ptr = this-&gt;data_;
<a name="l00124"></a>00124     <span class="keywordtype">int</span> lgth = i;
<a name="l00125"></a>00125     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; i; k++, ptr += lgth)
<a name="l00126"></a>00126       me_[k] = ptr;
<a name="l00127"></a>00127   }
<a name="l00128"></a>00128 
<a name="l00129"></a>00129 
<a name="l00131"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#a8d0b954996edc9ccf67c46eb8ed05ad2">00131</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00132"></a>00132   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a82951e97ce120fda463beebe44d9c5e3" title="Default constructor.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00133"></a>00133 <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a82951e97ce120fda463beebe44d9c5e3" title="Default constructor.">  ::Matrix_Symmetric</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php" title="Symmetric matrix stored in a full matrix.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00134"></a>00134     : <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;()
<a name="l00135"></a>00135   {
<a name="l00136"></a>00136     this-&gt;m_ = 0;
<a name="l00137"></a>00137     this-&gt;n_ = 0;
<a name="l00138"></a>00138     this-&gt;data_ = NULL;
<a name="l00139"></a>00139     this-&gt;me_ = NULL;
<a name="l00140"></a>00140 
<a name="l00141"></a>00141     this-&gt;Copy(A);
<a name="l00142"></a>00142   }
<a name="l00143"></a>00143 
<a name="l00144"></a>00144 
<a name="l00145"></a>00145   <span class="comment">/**************</span>
<a name="l00146"></a>00146 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00147"></a>00147 <span class="comment">   **************/</span>
<a name="l00148"></a>00148 
<a name="l00149"></a>00149 
<a name="l00151"></a>00151   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00152"></a>00152   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#ae3c7bcbdfae4e4240505bd3ad72df274" title="Destructor.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::~Matrix_Symmetric</a>()
<a name="l00153"></a>00153   {
<a name="l00154"></a>00154 
<a name="l00155"></a>00155 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00156"></a>00156 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00157"></a>00157       {
<a name="l00158"></a>00158 <span class="preprocessor">#endif</span>
<a name="l00159"></a>00159 <span class="preprocessor"></span>
<a name="l00160"></a>00160         <span class="keywordflow">if</span> (this-&gt;data_ != NULL)
<a name="l00161"></a>00161           {
<a name="l00162"></a>00162             this-&gt;allocator_.deallocate(this-&gt;data_, this-&gt;m_ * this-&gt;n_);
<a name="l00163"></a>00163             this-&gt;data_ = NULL;
<a name="l00164"></a>00164           }
<a name="l00165"></a>00165 
<a name="l00166"></a>00166 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00167"></a>00167 <span class="preprocessor"></span>      }
<a name="l00168"></a>00168     <span class="keywordflow">catch</span> (...)
<a name="l00169"></a>00169       {
<a name="l00170"></a>00170         this-&gt;data_ = NULL;
<a name="l00171"></a>00171       }
<a name="l00172"></a>00172 <span class="preprocessor">#endif</span>
<a name="l00173"></a>00173 <span class="preprocessor"></span>
<a name="l00174"></a>00174 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00175"></a>00175 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00176"></a>00176       {
<a name="l00177"></a>00177 <span class="preprocessor">#endif</span>
<a name="l00178"></a>00178 <span class="preprocessor"></span>
<a name="l00179"></a>00179         <span class="keywordflow">if</span> (me_ != NULL)
<a name="l00180"></a>00180           {
<a name="l00181"></a>00181             free(me_);
<a name="l00182"></a>00182             me_ = NULL;
<a name="l00183"></a>00183           }
<a name="l00184"></a>00184 
<a name="l00185"></a>00185 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00186"></a>00186 <span class="preprocessor"></span>      }
<a name="l00187"></a>00187     <span class="keywordflow">catch</span> (...)
<a name="l00188"></a>00188       {
<a name="l00189"></a>00189         this-&gt;m_ = 0;
<a name="l00190"></a>00190         this-&gt;n_ = 0;
<a name="l00191"></a>00191         me_ = NULL;
<a name="l00192"></a>00192       }
<a name="l00193"></a>00193 <span class="preprocessor">#endif</span>
<a name="l00194"></a>00194 <span class="preprocessor"></span>
<a name="l00195"></a>00195   }
<a name="l00196"></a>00196 
<a name="l00197"></a>00197 
<a name="l00199"></a>00199 
<a name="l00203"></a>00203   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00204"></a>00204   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a0ab7b853cb5e827503be5e65f2b39150" title="Clears the matrix.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::Clear</a>()
<a name="l00205"></a>00205   {
<a name="l00206"></a>00206     this-&gt;<a class="code" href="class_seldon_1_1_matrix___symmetric.php#ae3c7bcbdfae4e4240505bd3ad72df274" title="Destructor.">~Matrix_Symmetric</a>();
<a name="l00207"></a>00207     this-&gt;m_ = 0;
<a name="l00208"></a>00208     this-&gt;n_ = 0;
<a name="l00209"></a>00209   }
<a name="l00210"></a>00210 
<a name="l00211"></a>00211 
<a name="l00212"></a>00212   <span class="comment">/*******************</span>
<a name="l00213"></a>00213 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00214"></a>00214 <span class="comment">   *******************/</span>
<a name="l00215"></a>00215 
<a name="l00216"></a>00216 
<a name="l00218"></a>00218 
<a name="l00224"></a>00224   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00225"></a>00225   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a8876d406208337c491b2e9d2f56de084" title="Returns the number of elements stored in memory.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::GetDataSize</a>()<span class="keyword"> const</span>
<a name="l00226"></a>00226 <span class="keyword">  </span>{
<a name="l00227"></a>00227     <span class="keywordflow">return</span> this-&gt;m_ * this-&gt;n_;
<a name="l00228"></a>00228   }
<a name="l00229"></a>00229 
<a name="l00230"></a>00230 
<a name="l00231"></a>00231   <span class="comment">/*********************</span>
<a name="l00232"></a>00232 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00233"></a>00233 <span class="comment">   *********************/</span>
<a name="l00234"></a>00234 
<a name="l00235"></a>00235 
<a name="l00237"></a>00237 
<a name="l00243"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#a488c4c92d4c5f2e867a8f008c7ba6e5d">00243</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00244"></a>00244   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a488c4c92d4c5f2e867a8f008c7ba6e5d" title="Reallocates memory to resize the matrix.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00245"></a>00245 <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a488c4c92d4c5f2e867a8f008c7ba6e5d" title="Reallocates memory to resize the matrix.">  ::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00246"></a>00246   {
<a name="l00247"></a>00247 
<a name="l00248"></a>00248     <span class="keywordflow">if</span> (i != this-&gt;m_)
<a name="l00249"></a>00249       {
<a name="l00250"></a>00250         this-&gt;m_ = i;
<a name="l00251"></a>00251         this-&gt;n_ = i;
<a name="l00252"></a>00252 
<a name="l00253"></a>00253 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00254"></a>00254 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l00255"></a>00255           {
<a name="l00256"></a>00256 <span class="preprocessor">#endif</span>
<a name="l00257"></a>00257 <span class="preprocessor"></span>
<a name="l00258"></a>00258             me_ = <span class="keyword">reinterpret_cast&lt;</span>pointer*<span class="keyword">&gt;</span>( realloc(me_,
<a name="l00259"></a>00259                                                       i * <span class="keyword">sizeof</span>(pointer)) );
<a name="l00260"></a>00260 
<a name="l00261"></a>00261 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00262"></a>00262 <span class="preprocessor"></span>          }
<a name="l00263"></a>00263         <span class="keywordflow">catch</span> (...)
<a name="l00264"></a>00264           {
<a name="l00265"></a>00265             this-&gt;m_ = 0;
<a name="l00266"></a>00266             this-&gt;n_ = 0;
<a name="l00267"></a>00267             me_ = NULL;
<a name="l00268"></a>00268             this-&gt;data_ = NULL;
<a name="l00269"></a>00269           }
<a name="l00270"></a>00270         <span class="keywordflow">if</span> (me_ == NULL)
<a name="l00271"></a>00271           {
<a name="l00272"></a>00272             this-&gt;m_ = 0;
<a name="l00273"></a>00273             this-&gt;n_ = 0;
<a name="l00274"></a>00274             this-&gt;data_ = NULL;
<a name="l00275"></a>00275           }
<a name="l00276"></a>00276         <span class="keywordflow">if</span> (me_ == NULL &amp;&amp; i != 0)
<a name="l00277"></a>00277           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Symmetric::Reallocate(int, int)&quot;</span>,
<a name="l00278"></a>00278                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to reallocate memory&quot;</span>)
<a name="l00279"></a>00279                          + <span class="stringliteral">&quot; for a matrix of size &quot;</span>
<a name="l00280"></a>00280                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(static_cast&lt;long int&gt;(i)
<a name="l00281"></a>00281                                   * static_cast&lt;long int&gt;(i)
<a name="l00282"></a>00282                                   * static_cast&lt;long int&gt;(<span class="keyword">sizeof</span>(T)))
<a name="l00283"></a>00283                          + <span class="stringliteral">&quot; bytes (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; x &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i)
<a name="l00284"></a>00284                          + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00285"></a>00285 <span class="preprocessor">#endif</span>
<a name="l00286"></a>00286 <span class="preprocessor"></span>
<a name="l00287"></a>00287 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00288"></a>00288 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l00289"></a>00289           {
<a name="l00290"></a>00290 <span class="preprocessor">#endif</span>
<a name="l00291"></a>00291 <span class="preprocessor"></span>
<a name="l00292"></a>00292             this-&gt;data_ =
<a name="l00293"></a>00293               <span class="keyword">reinterpret_cast&lt;</span>pointer<span class="keyword">&gt;</span>(this-&gt;allocator_.reallocate(this-&gt;data_,
<a name="l00294"></a>00294                                                                     i * i,
<a name="l00295"></a>00295                                                                     <span class="keyword">this</span>) );
<a name="l00296"></a>00296 
<a name="l00297"></a>00297 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00298"></a>00298 <span class="preprocessor"></span>          }
<a name="l00299"></a>00299         <span class="keywordflow">catch</span> (...)
<a name="l00300"></a>00300           {
<a name="l00301"></a>00301             this-&gt;m_ = 0;
<a name="l00302"></a>00302             this-&gt;n_ = 0;
<a name="l00303"></a>00303             free(me_);
<a name="l00304"></a>00304             me_ = NULL;
<a name="l00305"></a>00305             this-&gt;data_ = NULL;
<a name="l00306"></a>00306           }
<a name="l00307"></a>00307         <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00308"></a>00308           {
<a name="l00309"></a>00309             this-&gt;m_ = 0;
<a name="l00310"></a>00310             this-&gt;n_ = 0;
<a name="l00311"></a>00311             free(me_);
<a name="l00312"></a>00312             me_ = NULL;
<a name="l00313"></a>00313           }
<a name="l00314"></a>00314         <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; i != 0)
<a name="l00315"></a>00315           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Symmetric::Reallocate(int, int)&quot;</span>,
<a name="l00316"></a>00316                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to reallocate memory&quot;</span>)
<a name="l00317"></a>00317                          + <span class="stringliteral">&quot; for a matrix of size &quot;</span>
<a name="l00318"></a>00318                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(static_cast&lt;long int&gt;(i)
<a name="l00319"></a>00319                                   * static_cast&lt;long int&gt;(i)
<a name="l00320"></a>00320                                   * static_cast&lt;long int&gt;(<span class="keyword">sizeof</span>(T)))
<a name="l00321"></a>00321                          + <span class="stringliteral">&quot; bytes (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; x &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i)
<a name="l00322"></a>00322                          + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00323"></a>00323 <span class="preprocessor">#endif</span>
<a name="l00324"></a>00324 <span class="preprocessor"></span>
<a name="l00325"></a>00325         pointer ptr = this-&gt;data_;
<a name="l00326"></a>00326         <span class="keywordtype">int</span> lgth = Storage::GetSecond(i, i);
<a name="l00327"></a>00327         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; Storage::GetFirst(i, i); k++, ptr += lgth)
<a name="l00328"></a>00328           me_[k] = ptr;
<a name="l00329"></a>00329       }
<a name="l00330"></a>00330   }
<a name="l00331"></a>00331 
<a name="l00332"></a>00332 
<a name="l00335"></a>00335 
<a name="l00349"></a>00349   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00350"></a>00350   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php" title="Symmetric matrix stored in a full matrix.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00351"></a>00351 <a class="code" href="class_seldon_1_1_matrix___symmetric.php" title="Symmetric matrix stored in a full matrix.">  ::SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00352"></a>00352             <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php" title="Symmetric matrix stored in a full matrix.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00353"></a>00353             ::pointer data)
<a name="l00354"></a>00354   {
<a name="l00355"></a>00355 
<a name="l00356"></a>00356     this-&gt;Clear();
<a name="l00357"></a>00357 
<a name="l00358"></a>00358     this-&gt;m_ = i;
<a name="l00359"></a>00359     this-&gt;n_ = i;
<a name="l00360"></a>00360 
<a name="l00361"></a>00361 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00362"></a>00362 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00363"></a>00363       {
<a name="l00364"></a>00364 <span class="preprocessor">#endif</span>
<a name="l00365"></a>00365 <span class="preprocessor"></span>
<a name="l00366"></a>00366         me_ = <span class="keyword">reinterpret_cast&lt;</span>pointer*<span class="keyword">&gt;</span>( calloc(i, <span class="keyword">sizeof</span>(pointer)) );
<a name="l00367"></a>00367 
<a name="l00368"></a>00368 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00369"></a>00369 <span class="preprocessor"></span>      }
<a name="l00370"></a>00370     <span class="keywordflow">catch</span> (...)
<a name="l00371"></a>00371       {
<a name="l00372"></a>00372         this-&gt;m_ = 0;
<a name="l00373"></a>00373         this-&gt;n_ = 0;
<a name="l00374"></a>00374         me_ = NULL;
<a name="l00375"></a>00375         this-&gt;data_ = NULL;
<a name="l00376"></a>00376         <span class="keywordflow">return</span>;
<a name="l00377"></a>00377       }
<a name="l00378"></a>00378     <span class="keywordflow">if</span> (me_ == NULL)
<a name="l00379"></a>00379       {
<a name="l00380"></a>00380         this-&gt;m_ = 0;
<a name="l00381"></a>00381         this-&gt;n_ = 0;
<a name="l00382"></a>00382         this-&gt;data_ = NULL;
<a name="l00383"></a>00383         <span class="keywordflow">return</span>;
<a name="l00384"></a>00384       }
<a name="l00385"></a>00385 <span class="preprocessor">#endif</span>
<a name="l00386"></a>00386 <span class="preprocessor"></span>
<a name="l00387"></a>00387     this-&gt;data_ = data;
<a name="l00388"></a>00388 
<a name="l00389"></a>00389     pointer ptr = this-&gt;data_;
<a name="l00390"></a>00390     <span class="keywordtype">int</span> lgth = i;
<a name="l00391"></a>00391     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; i; k++, ptr += lgth)
<a name="l00392"></a>00392       me_[k] = ptr;
<a name="l00393"></a>00393   }
<a name="l00394"></a>00394 
<a name="l00395"></a>00395 
<a name="l00397"></a>00397 
<a name="l00402"></a>00402   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00403"></a>00403   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#ae36123ec57e9423228e4815604ce2d5c" title="Clears the matrix without releasing memory.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::Nullify</a>()
<a name="l00404"></a>00404   {
<a name="l00405"></a>00405     this-&gt;m_ = 0;
<a name="l00406"></a>00406     this-&gt;n_ = 0;
<a name="l00407"></a>00407 
<a name="l00408"></a>00408 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00409"></a>00409 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00410"></a>00410       {
<a name="l00411"></a>00411 <span class="preprocessor">#endif</span>
<a name="l00412"></a>00412 <span class="preprocessor"></span>
<a name="l00413"></a>00413         <span class="keywordflow">if</span> (me_ != NULL)
<a name="l00414"></a>00414           {
<a name="l00415"></a>00415             free(me_);
<a name="l00416"></a>00416             me_ = NULL;
<a name="l00417"></a>00417           }
<a name="l00418"></a>00418 
<a name="l00419"></a>00419 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00420"></a>00420 <span class="preprocessor"></span>      }
<a name="l00421"></a>00421     <span class="keywordflow">catch</span> (...)
<a name="l00422"></a>00422       {
<a name="l00423"></a>00423         this-&gt;m_ = 0;
<a name="l00424"></a>00424         this-&gt;n_ = 0;
<a name="l00425"></a>00425         me_ = NULL;
<a name="l00426"></a>00426       }
<a name="l00427"></a>00427 <span class="preprocessor">#endif</span>
<a name="l00428"></a>00428 <span class="preprocessor"></span>
<a name="l00429"></a>00429     this-&gt;data_ = NULL;
<a name="l00430"></a>00430   }
<a name="l00431"></a>00431 
<a name="l00432"></a>00432 
<a name="l00434"></a>00434 
<a name="l00441"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#a2369e0b64027be77dd3ba9e3b77e4971">00441</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00442"></a>00442   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a2369e0b64027be77dd3ba9e3b77e4971" title="Reallocates memory to resize the matrix and keeps previous entries.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00443"></a>00443 <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a2369e0b64027be77dd3ba9e3b77e4971" title="Reallocates memory to resize the matrix and keeps previous entries.">  ::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00444"></a>00444   {
<a name="l00445"></a>00445 
<a name="l00446"></a>00446     <span class="comment">// Storing the old values of the matrix.</span>
<a name="l00447"></a>00447     <span class="keywordtype">int</span> iold = Storage::GetFirst(this-&gt;m_, this-&gt;n_);
<a name="l00448"></a>00448     <span class="keywordtype">int</span> jold = Storage::GetSecond(this-&gt;m_, this-&gt;n_);
<a name="l00449"></a>00449     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;value_type, VectFull, Allocator&gt;</a> xold(this-&gt;GetDataSize());
<a name="l00450"></a>00450     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; this-&gt;GetDataSize(); k++)
<a name="l00451"></a>00451       xold(k) = this-&gt;data_[k];
<a name="l00452"></a>00452 
<a name="l00453"></a>00453     <span class="comment">// Reallocation.</span>
<a name="l00454"></a>00454     <span class="keywordtype">int</span> inew = Storage::GetFirst(i, j);
<a name="l00455"></a>00455     <span class="keywordtype">int</span> jnew = Storage::GetSecond(i, j);
<a name="l00456"></a>00456     this-&gt;Reallocate(i, j);
<a name="l00457"></a>00457 
<a name="l00458"></a>00458     <span class="comment">// Filling the matrix with its old values.</span>
<a name="l00459"></a>00459     <span class="keywordtype">int</span> imin = min(iold, inew), jmin = min(jold, jnew);
<a name="l00460"></a>00460     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; imin; k++)
<a name="l00461"></a>00461       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> l = 0; l &lt; jmin; l++)
<a name="l00462"></a>00462         this-&gt;data_[k*jnew+l] = xold(l+jold*k);
<a name="l00463"></a>00463   }
<a name="l00464"></a>00464 
<a name="l00465"></a>00465 
<a name="l00466"></a>00466   <span class="comment">/**********************************</span>
<a name="l00467"></a>00467 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l00468"></a>00468 <span class="comment">   **********************************/</span>
<a name="l00469"></a>00469 
<a name="l00470"></a>00470 
<a name="l00472"></a>00472 
<a name="l00478"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#a8a5cd6bfbb855e65a5e585e9e62c72f1">00478</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00479"></a>00479   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php" title="Symmetric matrix stored in a full matrix.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00480"></a>00480   <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a8a5cd6bfbb855e65a5e585e9e62c72f1" title="Access operator.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00481"></a>00481   {
<a name="l00482"></a>00482 
<a name="l00483"></a>00483 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00484"></a>00484 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00485"></a>00485       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Symmetric::operator()&quot;</span>,
<a name="l00486"></a>00486                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00487"></a>00487                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00488"></a>00488     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00489"></a>00489       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Symmetric::operator()&quot;</span>,
<a name="l00490"></a>00490                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00491"></a>00491                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00492"></a>00492 <span class="preprocessor">#endif</span>
<a name="l00493"></a>00493 <span class="preprocessor"></span>
<a name="l00494"></a>00494     <span class="keywordflow">if</span> (i &gt; j)
<a name="l00495"></a>00495       <span class="keywordflow">return</span> me_[Storage::GetSecond(i, j)][Storage::GetFirst(i, j)];
<a name="l00496"></a>00496     <span class="keywordflow">else</span>
<a name="l00497"></a>00497       <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00498"></a>00498   }
<a name="l00499"></a>00499 
<a name="l00500"></a>00500 
<a name="l00502"></a>00502 
<a name="l00508"></a>00508   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00509"></a>00509   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php" title="Symmetric matrix stored in a full matrix.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00510"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#ab3bf006ebfa36eeafc3bd0b7a57f9d89">00510</a>   ::const_reference
<a name="l00511"></a>00511   <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a8a5cd6bfbb855e65a5e585e9e62c72f1" title="Access operator.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00512"></a>00512 <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a8a5cd6bfbb855e65a5e585e9e62c72f1" title="Access operator.">  ::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00513"></a>00513 <span class="keyword">  </span>{
<a name="l00514"></a>00514 
<a name="l00515"></a>00515 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00516"></a>00516 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00517"></a>00517       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Symmetric::operator() const&quot;</span>,
<a name="l00518"></a>00518                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00519"></a>00519                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00520"></a>00520     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00521"></a>00521       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Symmetric::operator() const&quot;</span>,
<a name="l00522"></a>00522                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00523"></a>00523                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00524"></a>00524 <span class="preprocessor">#endif</span>
<a name="l00525"></a>00525 <span class="preprocessor"></span>
<a name="l00526"></a>00526     <span class="keywordflow">if</span> (i &gt; j)
<a name="l00527"></a>00527       <span class="keywordflow">return</span> me_[Storage::GetSecond(i, j)][Storage::GetFirst(i, j)];
<a name="l00528"></a>00528     <span class="keywordflow">else</span>
<a name="l00529"></a>00529       <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00530"></a>00530   }
<a name="l00531"></a>00531 
<a name="l00532"></a>00532 
<a name="l00534"></a>00534 
<a name="l00540"></a>00540   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00541"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#a140aaa512f551fa698f84fc05c8df4c8">00541</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php" title="Symmetric matrix stored in a full matrix.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00542"></a>00542   ::const_reference
<a name="l00543"></a>00543   <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a140aaa512f551fa698f84fc05c8df4c8" title="Access operator.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00544"></a>00544 <span class="keyword">  </span>{
<a name="l00545"></a>00545 
<a name="l00546"></a>00546 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00547"></a>00547 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00548"></a>00548       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Symmetric::Val(int, int) const&quot;</span>,
<a name="l00549"></a>00549                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00550"></a>00550                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00551"></a>00551     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00552"></a>00552       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Symmetric::Val(int, int) const&quot;</span>,
<a name="l00553"></a>00553                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00554"></a>00554                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00555"></a>00555     <span class="keywordflow">if</span> (i &gt; j)
<a name="l00556"></a>00556       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Symmetric::Val(int, int)&quot;</span>,
<a name="l00557"></a>00557                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Attempted to access to element (&quot;</span>)
<a name="l00558"></a>00558                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00559"></a>00559                      + <span class="stringliteral">&quot;) but row index should not be strictly&quot;</span>
<a name="l00560"></a>00560                      + <span class="stringliteral">&quot; greater than column index.&quot;</span>);
<a name="l00561"></a>00561 <span class="preprocessor">#endif</span>
<a name="l00562"></a>00562 <span class="preprocessor"></span>
<a name="l00563"></a>00563     <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00564"></a>00564   }
<a name="l00565"></a>00565 
<a name="l00566"></a>00566 
<a name="l00568"></a>00568 
<a name="l00574"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#a1db21879907e925a20a58ad332a98446">00574</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00575"></a>00575   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php" title="Symmetric matrix stored in a full matrix.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00576"></a>00576   <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a140aaa512f551fa698f84fc05c8df4c8" title="Access operator.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00577"></a>00577   {
<a name="l00578"></a>00578 
<a name="l00579"></a>00579 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00580"></a>00580 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00581"></a>00581       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Symmetric::Val(int, int)&quot;</span>,
<a name="l00582"></a>00582                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00583"></a>00583                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00584"></a>00584     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00585"></a>00585       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Symmetric::Val(int, int)&quot;</span>,
<a name="l00586"></a>00586                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00587"></a>00587                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00588"></a>00588     <span class="keywordflow">if</span> (i &gt; j)
<a name="l00589"></a>00589       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Symmetric::Val(int, int)&quot;</span>,
<a name="l00590"></a>00590                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Attempted to access to element (&quot;</span>)
<a name="l00591"></a>00591                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00592"></a>00592                      + <span class="stringliteral">&quot;) but row index should not be strictly&quot;</span>
<a name="l00593"></a>00593                      + <span class="stringliteral">&quot; greater than column index.&quot;</span>);
<a name="l00594"></a>00594 <span class="preprocessor">#endif</span>
<a name="l00595"></a>00595 <span class="preprocessor"></span>
<a name="l00596"></a>00596     <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00597"></a>00597   }
<a name="l00598"></a>00598 
<a name="l00599"></a>00599 
<a name="l00601"></a>00601 
<a name="l00607"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#af5312b65658c2a50f9c94a784940c1e0">00607</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00608"></a>00608   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php" title="Symmetric matrix stored in a full matrix.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00609"></a>00609   <a class="code" href="class_seldon_1_1_matrix___symmetric.php#af5312b65658c2a50f9c94a784940c1e0" title="Returns the element (i, j).">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00610"></a>00610   {
<a name="l00611"></a>00611 
<a name="l00612"></a>00612 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00613"></a>00613 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00614"></a>00614       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Symmetric::Get(int, int)&quot;</span>,
<a name="l00615"></a>00615                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00616"></a>00616                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00617"></a>00617     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00618"></a>00618       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Symmetric::Get(int, int)&quot;</span>,
<a name="l00619"></a>00619                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00620"></a>00620                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00621"></a>00621 <span class="preprocessor">#endif</span>
<a name="l00622"></a>00622 <span class="preprocessor"></span>
<a name="l00623"></a>00623     <span class="keywordflow">if</span> (i &gt; j)
<a name="l00624"></a>00624       <span class="keywordflow">return</span> me_[Storage::GetSecond(i, j)][Storage::GetFirst(i, j)];
<a name="l00625"></a>00625     <span class="keywordflow">else</span>
<a name="l00626"></a>00626       <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00627"></a>00627   }
<a name="l00628"></a>00628 
<a name="l00629"></a>00629 
<a name="l00631"></a>00631 
<a name="l00637"></a>00637   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00638"></a>00638   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php" title="Symmetric matrix stored in a full matrix.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00639"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#a8b74b9c2fbd076cfb10e87a5c4812094">00639</a>   ::const_reference
<a name="l00640"></a>00640   <a class="code" href="class_seldon_1_1_matrix___symmetric.php#af5312b65658c2a50f9c94a784940c1e0" title="Returns the element (i, j).">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00641"></a>00641 <a class="code" href="class_seldon_1_1_matrix___symmetric.php#af5312b65658c2a50f9c94a784940c1e0" title="Returns the element (i, j).">  ::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00642"></a>00642 <span class="keyword">  </span>{
<a name="l00643"></a>00643 
<a name="l00644"></a>00644 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00645"></a>00645 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00646"></a>00646       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Symmetric::Get(int, int) const&quot;</span>,
<a name="l00647"></a>00647                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00648"></a>00648                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00649"></a>00649     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00650"></a>00650       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Symmetric::Get(int, int) const&quot;</span>,
<a name="l00651"></a>00651                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00652"></a>00652                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00653"></a>00653 <span class="preprocessor">#endif</span>
<a name="l00654"></a>00654 <span class="preprocessor"></span>
<a name="l00655"></a>00655     <span class="keywordflow">if</span> (i &gt; j)
<a name="l00656"></a>00656       <span class="keywordflow">return</span> me_[Storage::GetSecond(i, j)][Storage::GetFirst(i, j)];
<a name="l00657"></a>00657     <span class="keywordflow">else</span>
<a name="l00658"></a>00658       <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00659"></a>00659   }
<a name="l00660"></a>00660 
<a name="l00661"></a>00661 
<a name="l00663"></a>00663 
<a name="l00668"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#aead099f326217ddb05c0782e58b0b1a5">00668</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00669"></a>00669   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php" title="Symmetric matrix stored in a full matrix.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00670"></a>00670   <a class="code" href="class_seldon_1_1_matrix___symmetric.php#aead099f326217ddb05c0782e58b0b1a5" title="Access to elements of the data array.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::operator[] </a>(<span class="keywordtype">int</span> i)
<a name="l00671"></a>00671   {
<a name="l00672"></a>00672 
<a name="l00673"></a>00673 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00674"></a>00674 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___symmetric.php#a8876d406208337c491b2e9d2f56de084" title="Returns the number of elements stored in memory.">GetDataSize</a>())
<a name="l00675"></a>00675       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Matrix_Symmetric::operator[] (int)&quot;</span>,
<a name="l00676"></a>00676                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00677"></a>00677                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___symmetric.php#a8876d406208337c491b2e9d2f56de084" title="Returns the number of elements stored in memory.">GetDataSize</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00678"></a>00678                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00679"></a>00679 <span class="preprocessor">#endif</span>
<a name="l00680"></a>00680 <span class="preprocessor"></span>
<a name="l00681"></a>00681     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00682"></a>00682   }
<a name="l00683"></a>00683 
<a name="l00684"></a>00684 
<a name="l00686"></a>00686 
<a name="l00691"></a>00691   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00692"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#a674815a3c75a714b790ee7dec3a46b4b">00692</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php" title="Symmetric matrix stored in a full matrix.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00693"></a>00693   ::const_reference
<a name="l00694"></a>00694   <a class="code" href="class_seldon_1_1_matrix___symmetric.php#aead099f326217ddb05c0782e58b0b1a5" title="Access to elements of the data array.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::operator[] </a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00695"></a>00695 <span class="keyword">  </span>{
<a name="l00696"></a>00696 
<a name="l00697"></a>00697 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00698"></a>00698 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___symmetric.php#a8876d406208337c491b2e9d2f56de084" title="Returns the number of elements stored in memory.">GetDataSize</a>())
<a name="l00699"></a>00699       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Matrix_Symmetric::operator[] (int) const&quot;</span>,
<a name="l00700"></a>00700                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00701"></a>00701                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___symmetric.php#a8876d406208337c491b2e9d2f56de084" title="Returns the number of elements stored in memory.">GetDataSize</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00702"></a>00702                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00703"></a>00703 <span class="preprocessor">#endif</span>
<a name="l00704"></a>00704 <span class="preprocessor"></span>
<a name="l00705"></a>00705     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00706"></a>00706   }
<a name="l00707"></a>00707 
<a name="l00708"></a>00708 
<a name="l00710"></a>00710 
<a name="l00715"></a>00715   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00716"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#af554df9f69fd7674842120a4718f609b">00716</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php" title="Symmetric matrix stored in a full matrix.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00717"></a>00717   <a class="code" href="class_seldon_1_1_matrix___symmetric.php#af554df9f69fd7674842120a4718f609b" title="Duplicates a matrix (assignment operator).">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00718"></a>00718 <a class="code" href="class_seldon_1_1_matrix___symmetric.php#af554df9f69fd7674842120a4718f609b" title="Duplicates a matrix (assignment operator).">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php" title="Symmetric matrix stored in a full matrix.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00719"></a>00719   {
<a name="l00720"></a>00720     this-&gt;Copy(A);
<a name="l00721"></a>00721 
<a name="l00722"></a>00722     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00723"></a>00723   }
<a name="l00724"></a>00724 
<a name="l00725"></a>00725 
<a name="l00727"></a>00727 
<a name="l00732"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#a66c6a02ed5d50cbf204f7bd145d13880">00732</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00733"></a>00733   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a66c6a02ed5d50cbf204f7bd145d13880" title="Sets an element of the matrix.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00734"></a>00734 <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a66c6a02ed5d50cbf204f7bd145d13880" title="Sets an element of the matrix.">  ::Set</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; x)
<a name="l00735"></a>00735   {
<a name="l00736"></a>00736     this-&gt;Get(i, j) = x;
<a name="l00737"></a>00737   }
<a name="l00738"></a>00738 
<a name="l00739"></a>00739 
<a name="l00741"></a>00741 
<a name="l00746"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#a94f5e47677cc13311ab33879d2b05774">00746</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00747"></a>00747   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a94f5e47677cc13311ab33879d2b05774" title="Duplicates a matrix.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00748"></a>00748 <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a94f5e47677cc13311ab33879d2b05774" title="Duplicates a matrix.">  ::Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php" title="Symmetric matrix stored in a full matrix.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00749"></a>00749   {
<a name="l00750"></a>00750     this-&gt;Reallocate(A.<a class="code" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927" title="Returns the number of rows.">GetM</a>(), A.<a class="code" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984" title="Returns the number of columns.">GetN</a>());
<a name="l00751"></a>00751 
<a name="l00752"></a>00752     this-&gt;allocator_.memorycpy(this-&gt;data_, A.<a class="code" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a" title="Returns a pointer to the data array.">GetData</a>(), this-&gt;GetDataSize());
<a name="l00753"></a>00753   }
<a name="l00754"></a>00754 
<a name="l00755"></a>00755 
<a name="l00756"></a>00756   <span class="comment">/************************</span>
<a name="l00757"></a>00757 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l00758"></a>00758 <span class="comment">   ************************/</span>
<a name="l00759"></a>00759 
<a name="l00760"></a>00760 
<a name="l00762"></a>00762 
<a name="l00766"></a>00766   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00767"></a>00767   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a73066da717f1163947c11658fc7cba31" title="Sets all elements to zero.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::Zero</a>()
<a name="l00768"></a>00768   {
<a name="l00769"></a>00769     this-&gt;allocator_.memoryset(this-&gt;data_, <span class="keywordtype">char</span>(0),
<a name="l00770"></a>00770                                this-&gt;<a class="code" href="class_seldon_1_1_matrix___symmetric.php#a8876d406208337c491b2e9d2f56de084" title="Returns the number of elements stored in memory.">GetDataSize</a>() * <span class="keyword">sizeof</span>(value_type));
<a name="l00771"></a>00771   }
<a name="l00772"></a>00772 
<a name="l00773"></a>00773 
<a name="l00775"></a>00775   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00776"></a>00776   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a7cab27ba8e4ee1d46cb8d5eca3123d42" title="Sets the matrix to the identity.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::SetIdentity</a>()
<a name="l00777"></a>00777   {
<a name="l00778"></a>00778     this-&gt;<a class="code" href="class_seldon_1_1_matrix___symmetric.php#a9d34c8b60b6e577504a4264990194805" title="Fills the matrix with 0, 1, 2, ...">Fill</a>(T(0));
<a name="l00779"></a>00779 
<a name="l00780"></a>00780     T one(1);
<a name="l00781"></a>00781     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; min(this-&gt;m_, this-&gt;n_); i++)
<a name="l00782"></a>00782       this-&gt;<a class="code" href="class_seldon_1_1_matrix___symmetric.php#a140aaa512f551fa698f84fc05c8df4c8" title="Access operator.">Val</a>(i, i) = one;
<a name="l00783"></a>00783   }
<a name="l00784"></a>00784 
<a name="l00785"></a>00785 
<a name="l00787"></a>00787 
<a name="l00791"></a>00791   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00792"></a>00792   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a9d34c8b60b6e577504a4264990194805" title="Fills the matrix with 0, 1, 2, ...">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::Fill</a>()
<a name="l00793"></a>00793   {
<a name="l00794"></a>00794     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___symmetric.php#a8876d406208337c491b2e9d2f56de084" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00795"></a>00795       this-&gt;data_[i] = i;
<a name="l00796"></a>00796   }
<a name="l00797"></a>00797 
<a name="l00798"></a>00798 
<a name="l00800"></a>00800 
<a name="l00803"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#a966bcf7f7c204b9a8e2130ca93db0757">00803</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00804"></a>00804   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00805"></a>00805   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a9d34c8b60b6e577504a4264990194805" title="Fills the matrix with 0, 1, 2, ...">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::Fill</a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00806"></a>00806   {
<a name="l00807"></a>00807     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___symmetric.php#a8876d406208337c491b2e9d2f56de084" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00808"></a>00808       this-&gt;data_[i] = x;
<a name="l00809"></a>00809   }
<a name="l00810"></a>00810 
<a name="l00811"></a>00811 
<a name="l00813"></a>00813 
<a name="l00816"></a>00816   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00817"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#a13726b53c0a2db160c719e7d99b15274">00817</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00818"></a>00818   <a class="code" href="class_seldon_1_1_matrix___symmetric.php" title="Symmetric matrix stored in a full matrix.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00819"></a>00819   <a class="code" href="class_seldon_1_1_matrix___symmetric.php#af554df9f69fd7674842120a4718f609b" title="Duplicates a matrix (assignment operator).">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00820"></a>00820   {
<a name="l00821"></a>00821     this-&gt;<a class="code" href="class_seldon_1_1_matrix___symmetric.php#a9d34c8b60b6e577504a4264990194805" title="Fills the matrix with 0, 1, 2, ...">Fill</a>(x);
<a name="l00822"></a>00822 
<a name="l00823"></a>00823     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00824"></a>00824   }
<a name="l00825"></a>00825 
<a name="l00826"></a>00826 
<a name="l00828"></a>00828 
<a name="l00831"></a>00831   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00832"></a>00832   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a01a990a33e3e2b873b69587d8870d0c3" title="Fills a matrix randomly.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::FillRand</a>()
<a name="l00833"></a>00833   {
<a name="l00834"></a>00834     srand(time(NULL));
<a name="l00835"></a>00835     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___symmetric.php#a8876d406208337c491b2e9d2f56de084" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00836"></a>00836       this-&gt;data_[i] = rand();
<a name="l00837"></a>00837   }
<a name="l00838"></a>00838 
<a name="l00839"></a>00839 
<a name="l00841"></a>00841 
<a name="l00846"></a>00846   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00847"></a>00847   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a018e68da0aa00e2a3bb7d5b989b77837" title="Displays the matrix on the standard output.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00848"></a>00848 <span class="keyword">  </span>{
<a name="l00849"></a>00849     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00850"></a>00850       {
<a name="l00851"></a>00851         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;n_; j++)
<a name="l00852"></a>00852           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00853"></a>00853         cout &lt;&lt; endl;
<a name="l00854"></a>00854       }
<a name="l00855"></a>00855   }
<a name="l00856"></a>00856 
<a name="l00857"></a>00857 
<a name="l00859"></a>00859 
<a name="l00870"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#a424f412164caf3674f49d4c30dbb9606">00870</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00871"></a>00871   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a018e68da0aa00e2a3bb7d5b989b77837" title="Displays the matrix on the standard output.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00872"></a>00872 <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a018e68da0aa00e2a3bb7d5b989b77837" title="Displays the matrix on the standard output.">  ::Print</a>(<span class="keywordtype">int</span> a, <span class="keywordtype">int</span> b, <span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n)<span class="keyword"> const</span>
<a name="l00873"></a>00873 <span class="keyword">  </span>{
<a name="l00874"></a>00874     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = a; i &lt; min(this-&gt;m_, a + m); i++)
<a name="l00875"></a>00875       {
<a name="l00876"></a>00876         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = b; j &lt; min(this-&gt;n_, b + n); j++)
<a name="l00877"></a>00877           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00878"></a>00878         cout &lt;&lt; endl;
<a name="l00879"></a>00879       }
<a name="l00880"></a>00880   }
<a name="l00881"></a>00881 
<a name="l00882"></a>00882 
<a name="l00884"></a>00884 
<a name="l00892"></a>00892   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00893"></a>00893   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a018e68da0aa00e2a3bb7d5b989b77837" title="Displays the matrix on the standard output.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::Print</a>(<span class="keywordtype">int</span> l)<span class="keyword"> const</span>
<a name="l00894"></a>00894 <span class="keyword">  </span>{
<a name="l00895"></a>00895     <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a018e68da0aa00e2a3bb7d5b989b77837" title="Displays the matrix on the standard output.">Print</a>(0, 0, l, l);
<a name="l00896"></a>00896   }
<a name="l00897"></a>00897 
<a name="l00898"></a>00898 
<a name="l00899"></a>00899   <span class="comment">/**************************</span>
<a name="l00900"></a>00900 <span class="comment">   * INPUT/OUTPUT FUNCTIONS *</span>
<a name="l00901"></a>00901 <span class="comment">   **************************/</span>
<a name="l00902"></a>00902 
<a name="l00903"></a>00903 
<a name="l00905"></a>00905 
<a name="l00912"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#a0af8f548546cddebba11f07e7f55f4d3">00912</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00913"></a>00913   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a0af8f548546cddebba11f07e7f55f4d3" title="Writes the matrix in a file.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00914"></a>00914 <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a0af8f548546cddebba11f07e7f55f4d3" title="Writes the matrix in a file.">  ::Write</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00915"></a>00915 <span class="keyword">  </span>{
<a name="l00916"></a>00916     ofstream FileStream;
<a name="l00917"></a>00917     FileStream.open(FileName.c_str(), ofstream::binary);
<a name="l00918"></a>00918 
<a name="l00919"></a>00919 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00920"></a>00920 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00921"></a>00921     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00922"></a>00922       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Symmetric::Write(string FileName)&quot;</span>,
<a name="l00923"></a>00923                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00924"></a>00924 <span class="preprocessor">#endif</span>
<a name="l00925"></a>00925 <span class="preprocessor"></span>
<a name="l00926"></a>00926     this-&gt;Write(FileStream);
<a name="l00927"></a>00927 
<a name="l00928"></a>00928     FileStream.close();
<a name="l00929"></a>00929   }
<a name="l00930"></a>00930 
<a name="l00931"></a>00931 
<a name="l00933"></a>00933 
<a name="l00940"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#a89eb858308a323e175f01cd6f1b70e42">00940</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00941"></a>00941   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a0af8f548546cddebba11f07e7f55f4d3" title="Writes the matrix in a file.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00942"></a>00942 <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a0af8f548546cddebba11f07e7f55f4d3" title="Writes the matrix in a file.">  ::Write</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00943"></a>00943 <span class="keyword">  </span>{
<a name="l00944"></a>00944 
<a name="l00945"></a>00945 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00946"></a>00946 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00947"></a>00947     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00948"></a>00948       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Symmetric::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l00949"></a>00949                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00950"></a>00950 <span class="preprocessor">#endif</span>
<a name="l00951"></a>00951 <span class="preprocessor"></span>
<a name="l00952"></a>00952     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l00953"></a>00953                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00954"></a>00954     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;n_)),
<a name="l00955"></a>00955                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00956"></a>00956 
<a name="l00957"></a>00957     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l00958"></a>00958                      this-&gt;m_ * this-&gt;n_ * <span class="keyword">sizeof</span>(value_type));
<a name="l00959"></a>00959 
<a name="l00960"></a>00960 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00961"></a>00961 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00962"></a>00962     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00963"></a>00963       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Symmetric::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l00964"></a>00964                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Output operation failed.&quot;</span>)
<a name="l00965"></a>00965                     + string(<span class="stringliteral">&quot; The output file may have been removed&quot;</span>)
<a name="l00966"></a>00966                     + <span class="stringliteral">&quot; or there is no space left on device.&quot;</span>);
<a name="l00967"></a>00967 <span class="preprocessor">#endif</span>
<a name="l00968"></a>00968 <span class="preprocessor"></span>
<a name="l00969"></a>00969   }
<a name="l00970"></a>00970 
<a name="l00971"></a>00971 
<a name="l00973"></a>00973 
<a name="l00980"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#aaad47afa4f62b374a14e37d896a677a8">00980</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00981"></a>00981   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#aaad47afa4f62b374a14e37d896a677a8" title="Writes the matrix in a file.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00982"></a>00982 <a class="code" href="class_seldon_1_1_matrix___symmetric.php#aaad47afa4f62b374a14e37d896a677a8" title="Writes the matrix in a file.">  ::WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00983"></a>00983 <span class="keyword">  </span>{
<a name="l00984"></a>00984     ofstream FileStream;
<a name="l00985"></a>00985     FileStream.precision(cout.precision());
<a name="l00986"></a>00986     FileStream.flags(cout.flags());
<a name="l00987"></a>00987     FileStream.open(FileName.c_str());
<a name="l00988"></a>00988 
<a name="l00989"></a>00989 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00990"></a>00990 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00991"></a>00991     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00992"></a>00992       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Symmetric::WriteText(string FileName)&quot;</span>,
<a name="l00993"></a>00993                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00994"></a>00994 <span class="preprocessor">#endif</span>
<a name="l00995"></a>00995 <span class="preprocessor"></span>
<a name="l00996"></a>00996     this-&gt;WriteText(FileStream);
<a name="l00997"></a>00997 
<a name="l00998"></a>00998     FileStream.close();
<a name="l00999"></a>00999   }
<a name="l01000"></a>01000 
<a name="l01001"></a>01001 
<a name="l01003"></a>01003 
<a name="l01010"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#a3ca6951c93777f346d79e8bdd227ea0e">01010</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01011"></a>01011   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#aaad47afa4f62b374a14e37d896a677a8" title="Writes the matrix in a file.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01012"></a>01012 <a class="code" href="class_seldon_1_1_matrix___symmetric.php#aaad47afa4f62b374a14e37d896a677a8" title="Writes the matrix in a file.">  ::WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l01013"></a>01013 <span class="keyword">  </span>{
<a name="l01014"></a>01014 
<a name="l01015"></a>01015 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01016"></a>01016 <span class="preprocessor"></span>    <span class="comment">// Checks if the file is ready.</span>
<a name="l01017"></a>01017     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01018"></a>01018       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Symmetric::WriteText(ofstream&amp; FileStream)&quot;</span>,
<a name="l01019"></a>01019                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l01020"></a>01020 <span class="preprocessor">#endif</span>
<a name="l01021"></a>01021 <span class="preprocessor"></span>
<a name="l01022"></a>01022     <span class="keywordtype">int</span> i, j;
<a name="l01023"></a>01023     <span class="keywordflow">for</span> (i = 0; i &lt; this-&gt;GetM(); i++)
<a name="l01024"></a>01024       {
<a name="l01025"></a>01025         <span class="keywordflow">for</span> (j = 0; j &lt; this-&gt;GetN(); j++)
<a name="l01026"></a>01026           FileStream &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="charliteral">&#39;\t&#39;</span>;
<a name="l01027"></a>01027         FileStream &lt;&lt; endl;
<a name="l01028"></a>01028       }
<a name="l01029"></a>01029 
<a name="l01030"></a>01030 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01031"></a>01031 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l01032"></a>01032     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01033"></a>01033       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Symmetric::WriteText(ofstream&amp; FileStream)&quot;</span>,
<a name="l01034"></a>01034                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Output operation failed.&quot;</span>)
<a name="l01035"></a>01035                     + string(<span class="stringliteral">&quot; The output file may have been removed&quot;</span>)
<a name="l01036"></a>01036                     + <span class="stringliteral">&quot; or there is no space left on device.&quot;</span>);
<a name="l01037"></a>01037 <span class="preprocessor">#endif</span>
<a name="l01038"></a>01038 <span class="preprocessor"></span>
<a name="l01039"></a>01039   }
<a name="l01040"></a>01040 
<a name="l01041"></a>01041 
<a name="l01043"></a>01043 
<a name="l01050"></a>01050   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01051"></a>01051   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#af4e2d06a1044dd2c449384c580b347cd" title="Reads the matrix from a file.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::Read</a>(<span class="keywordtype">string</span> FileName)
<a name="l01052"></a>01052   {
<a name="l01053"></a>01053     ifstream FileStream;
<a name="l01054"></a>01054     FileStream.open(FileName.c_str(), ifstream::binary);
<a name="l01055"></a>01055 
<a name="l01056"></a>01056 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01057"></a>01057 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01058"></a>01058     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01059"></a>01059       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Symmetric::Read(string FileName)&quot;</span>,
<a name="l01060"></a>01060                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01061"></a>01061 <span class="preprocessor">#endif</span>
<a name="l01062"></a>01062 <span class="preprocessor"></span>
<a name="l01063"></a>01063     this-&gt;<a class="code" href="class_seldon_1_1_matrix___symmetric.php#af4e2d06a1044dd2c449384c580b347cd" title="Reads the matrix from a file.">Read</a>(FileStream);
<a name="l01064"></a>01064 
<a name="l01065"></a>01065     FileStream.close();
<a name="l01066"></a>01066   }
<a name="l01067"></a>01067 
<a name="l01068"></a>01068 
<a name="l01070"></a>01070 
<a name="l01077"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#a911a42c752de06caed9614fed95bb8b5">01077</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01078"></a>01078   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#af4e2d06a1044dd2c449384c580b347cd" title="Reads the matrix from a file.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01079"></a>01079 <a class="code" href="class_seldon_1_1_matrix___symmetric.php#af4e2d06a1044dd2c449384c580b347cd" title="Reads the matrix from a file.">  ::Read</a>(istream&amp; FileStream)
<a name="l01080"></a>01080   {
<a name="l01081"></a>01081 
<a name="l01082"></a>01082 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01083"></a>01083 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01084"></a>01084     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01085"></a>01085       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Symmetric::Read(ifstream&amp; FileStream)&quot;</span>,
<a name="l01086"></a>01086                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l01087"></a>01087 <span class="preprocessor">#endif</span>
<a name="l01088"></a>01088 <span class="preprocessor"></span>
<a name="l01089"></a>01089     <span class="keywordtype">int</span> new_m, new_n;
<a name="l01090"></a>01090     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;new_m), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01091"></a>01091     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;new_n), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01092"></a>01092     this-&gt;Reallocate(new_m, new_n);
<a name="l01093"></a>01093 
<a name="l01094"></a>01094     FileStream.read(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l01095"></a>01095                     new_m * new_n * <span class="keyword">sizeof</span>(value_type));
<a name="l01096"></a>01096 
<a name="l01097"></a>01097 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01098"></a>01098 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l01099"></a>01099     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01100"></a>01100       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Symmetric::Read(ifstream&amp; FileStream)&quot;</span>,
<a name="l01101"></a>01101                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Input operation failed.&quot;</span>)
<a name="l01102"></a>01102                     + string(<span class="stringliteral">&quot; The input file may have been removed&quot;</span>)
<a name="l01103"></a>01103                     + <span class="stringliteral">&quot; or may not contain enough data.&quot;</span>);
<a name="l01104"></a>01104 <span class="preprocessor">#endif</span>
<a name="l01105"></a>01105 <span class="preprocessor"></span>
<a name="l01106"></a>01106   }
<a name="l01107"></a>01107 
<a name="l01108"></a>01108 
<a name="l01110"></a>01110 
<a name="l01114"></a>01114   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01115"></a>01115   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a9ae2c1b1f8f80c87801b92dfc9a9a885" title="Reads the matrix from a file.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;::ReadText</a>(<span class="keywordtype">string</span> FileName)
<a name="l01116"></a>01116   {
<a name="l01117"></a>01117     ifstream FileStream;
<a name="l01118"></a>01118     FileStream.open(FileName.c_str());
<a name="l01119"></a>01119 
<a name="l01120"></a>01120 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01121"></a>01121 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01122"></a>01122     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01123"></a>01123       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(string FileName)&quot;</span>,
<a name="l01124"></a>01124                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01125"></a>01125 <span class="preprocessor">#endif</span>
<a name="l01126"></a>01126 <span class="preprocessor"></span>
<a name="l01127"></a>01127     this-&gt;<a class="code" href="class_seldon_1_1_matrix___symmetric.php#a9ae2c1b1f8f80c87801b92dfc9a9a885" title="Reads the matrix from a file.">ReadText</a>(FileStream);
<a name="l01128"></a>01128 
<a name="l01129"></a>01129     FileStream.close();
<a name="l01130"></a>01130   }
<a name="l01131"></a>01131 
<a name="l01132"></a>01132 
<a name="l01134"></a>01134 
<a name="l01138"></a><a class="code" href="class_seldon_1_1_matrix___symmetric.php#a72ba96f845054a5be5120c7a175c00c1">01138</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01139"></a>01139   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a9ae2c1b1f8f80c87801b92dfc9a9a885" title="Reads the matrix from a file.">Matrix_Symmetric&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01140"></a>01140 <a class="code" href="class_seldon_1_1_matrix___symmetric.php#a9ae2c1b1f8f80c87801b92dfc9a9a885" title="Reads the matrix from a file.">  ::ReadText</a>(istream&amp; FileStream)
<a name="l01141"></a>01141   {
<a name="l01142"></a>01142     <span class="comment">// clears previous matrix</span>
<a name="l01143"></a>01143     Clear();
<a name="l01144"></a>01144 
<a name="l01145"></a>01145 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01146"></a>01146 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01147"></a>01147     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01148"></a>01148       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(ifstream&amp; FileStream)&quot;</span>,
<a name="l01149"></a>01149                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l01150"></a>01150 <span class="preprocessor">#endif</span>
<a name="l01151"></a>01151 <span class="preprocessor"></span>
<a name="l01152"></a>01152     <span class="comment">// we read first line</span>
<a name="l01153"></a>01153     <span class="keywordtype">string</span> line;
<a name="l01154"></a>01154     getline(FileStream, line);
<a name="l01155"></a>01155 
<a name="l01156"></a>01156     <span class="keywordflow">if</span> (FileStream.fail())
<a name="l01157"></a>01157       {
<a name="l01158"></a>01158         <span class="comment">// empty file ?</span>
<a name="l01159"></a>01159         <span class="keywordflow">return</span>;
<a name="l01160"></a>01160       }
<a name="l01161"></a>01161 
<a name="l01162"></a>01162     <span class="comment">// converting first line into a vector</span>
<a name="l01163"></a>01163     istringstream line_stream(line);
<a name="l01164"></a>01164     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> first_row;
<a name="l01165"></a>01165     first_row.ReadText(line_stream);
<a name="l01166"></a>01166 
<a name="l01167"></a>01167     <span class="comment">// and now the other rows</span>
<a name="l01168"></a>01168     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> other_rows;
<a name="l01169"></a>01169     other_rows.ReadText(FileStream);
<a name="l01170"></a>01170 
<a name="l01171"></a>01171     <span class="comment">// number of rows and columns</span>
<a name="l01172"></a>01172     <span class="keywordtype">int</span> n = first_row.GetM();
<a name="l01173"></a>01173     <span class="keywordtype">int</span> m = 1 + other_rows.GetM()/n;
<a name="l01174"></a>01174 
<a name="l01175"></a>01175 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01176"></a>01176 <span class="preprocessor"></span>    <span class="comment">// Checking number of elements</span>
<a name="l01177"></a>01177     <span class="keywordflow">if</span> (other_rows.GetM() != (m-1)*n)
<a name="l01178"></a>01178       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(ifstream&amp; FileStream)&quot;</span>,
<a name="l01179"></a>01179                     <span class="stringliteral">&quot;The file should contain same number of columns.&quot;</span>);
<a name="l01180"></a>01180 <span class="preprocessor">#endif</span>
<a name="l01181"></a>01181 <span class="preprocessor"></span>
<a name="l01182"></a>01182     this-&gt;Reallocate(m,n);
<a name="l01183"></a>01183     <span class="comment">// filling matrix</span>
<a name="l01184"></a>01184     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l01185"></a>01185       this-&gt;Val(0, j) = first_row(j);
<a name="l01186"></a>01186 
<a name="l01187"></a>01187     <span class="keywordtype">int</span> nb = 0;
<a name="l01188"></a>01188     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 1; i &lt; m; i++)
<a name="l01189"></a>01189       {
<a name="l01190"></a>01190         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; i; j++)
<a name="l01191"></a>01191           nb++;
<a name="l01192"></a>01192 
<a name="l01193"></a>01193         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = i; j &lt; n; j++)
<a name="l01194"></a>01194           this-&gt;Val(i, j) = other_rows(nb++);
<a name="l01195"></a>01195       }
<a name="l01196"></a>01196   }
<a name="l01197"></a>01197 
<a name="l01198"></a>01198 
<a name="l01199"></a>01199 
<a name="l01201"></a>01201   <span class="comment">// MATRIX&lt;COLSYM&gt; //</span>
<a name="l01203"></a>01203 <span class="comment"></span>
<a name="l01204"></a>01204 
<a name="l01205"></a>01205   <span class="comment">/****************</span>
<a name="l01206"></a>01206 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01207"></a>01207 <span class="comment">   ****************/</span>
<a name="l01208"></a>01208 
<a name="l01209"></a>01209 
<a name="l01211"></a>01211 
<a name="l01214"></a>01214   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01215"></a>01215   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSym, Allocator&gt;::Matrix</a>():
<a name="l01216"></a>01216     <a class="code" href="class_seldon_1_1_matrix___symmetric.php" title="Symmetric matrix stored in a full matrix.">Matrix_Symmetric</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_sym.php">ColSym</a>, Allocator&gt;()
<a name="l01217"></a>01217   {
<a name="l01218"></a>01218   }
<a name="l01219"></a>01219 
<a name="l01220"></a>01220 
<a name="l01222"></a>01222 
<a name="l01226"></a>01226   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01227"></a>01227   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php#abd70ad2d9d8d9da0d9e5210447aed290" title="Default constructor.">Matrix&lt;T, Prop, ColSym, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01228"></a>01228     Matrix_Symmetric&lt;T, Prop, ColSym, Allocator&gt;(i, j)
<a name="l01229"></a>01229   {
<a name="l01230"></a>01230   }
<a name="l01231"></a>01231 
<a name="l01232"></a>01232 
<a name="l01233"></a>01233   <span class="comment">/*****************</span>
<a name="l01234"></a>01234 <span class="comment">   * OTHER METHODS *</span>
<a name="l01235"></a>01235 <span class="comment">   *****************/</span>
<a name="l01236"></a>01236 
<a name="l01237"></a>01237 
<a name="l01239"></a>01239 
<a name="l01242"></a>01242   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01243"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php#a02437899b3e29faaf714a28429b66de2">01243</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01244"></a>01244   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php" title="Column-major symmetric full-matrix class.">Matrix&lt;T, Prop, ColSym, Allocator&gt;</a>&amp;
<a name="l01245"></a>01245   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSym, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01246"></a>01246   {
<a name="l01247"></a>01247     this-&gt;Fill(x);
<a name="l01248"></a>01248 
<a name="l01249"></a>01249     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01250"></a>01250   }
<a name="l01251"></a>01251 
<a name="l01252"></a>01252 
<a name="l01254"></a>01254 
<a name="l01259"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php#a14d0e6fbd800e1d87cb7339f9835f5fe">01259</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01260"></a>01260   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php" title="Column-major symmetric full-matrix class.">Matrix&lt;T, Prop, ColSym, Allocator&gt;</a>&amp;
<a name="l01261"></a>01261   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSym, Allocator&gt;::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop,
<a name="l01262"></a>01262                                                        <a class="code" href="class_seldon_1_1_col_sym.php">ColSym</a>,
<a name="l01263"></a>01263                                                        Allocator&gt;&amp; A)
<a name="l01264"></a>01264   {
<a name="l01265"></a>01265     this-&gt;Copy(A);
<a name="l01266"></a>01266     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01267"></a>01267   }
<a name="l01268"></a>01268 
<a name="l01269"></a>01269 
<a name="l01271"></a>01271 
<a name="l01274"></a>01274   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01275"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php#aec3d6da476f91a8d3b6033e835a3ffe9">01275</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01276"></a>01276   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php" title="Column-major symmetric full-matrix class.">Matrix&lt;T, Prop, ColSym, Allocator&gt;</a>&amp;
<a name="l01277"></a>01277   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSym, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01278"></a>01278   {
<a name="l01279"></a>01279     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetDataSize();i++)
<a name="l01280"></a>01280       this-&gt;data_[i] *= x;
<a name="l01281"></a>01281 
<a name="l01282"></a>01282     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01283"></a>01283   }
<a name="l01284"></a>01284 
<a name="l01285"></a>01285 
<a name="l01287"></a>01287   <span class="comment">// MATRIX&lt;ROWSYM&gt; //</span>
<a name="l01289"></a>01289 <span class="comment"></span>
<a name="l01290"></a>01290 
<a name="l01291"></a>01291   <span class="comment">/****************</span>
<a name="l01292"></a>01292 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01293"></a>01293 <span class="comment">   ****************/</span>
<a name="l01294"></a>01294 
<a name="l01295"></a>01295 
<a name="l01297"></a>01297 
<a name="l01300"></a>01300   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01301"></a>01301   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSym, Allocator&gt;::Matrix</a>():
<a name="l01302"></a>01302     <a class="code" href="class_seldon_1_1_matrix___symmetric.php" title="Symmetric matrix stored in a full matrix.">Matrix_Symmetric</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_sym.php">RowSym</a>, Allocator&gt;()
<a name="l01303"></a>01303   {
<a name="l01304"></a>01304   }
<a name="l01305"></a>01305 
<a name="l01306"></a>01306 
<a name="l01308"></a>01308 
<a name="l01312"></a>01312   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01313"></a>01313   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_00_01_allocator_01_4.php#a25bc08edf0f71641b5f56d059ebed262" title="Default constructor.">Matrix&lt;T, Prop, RowSym, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01314"></a>01314     Matrix_Symmetric&lt;T, Prop, RowSym, Allocator&gt;(i, j)
<a name="l01315"></a>01315   {
<a name="l01316"></a>01316   }
<a name="l01317"></a>01317 
<a name="l01318"></a>01318 
<a name="l01319"></a>01319   <span class="comment">/*****************</span>
<a name="l01320"></a>01320 <span class="comment">   * OTHER METHODS *</span>
<a name="l01321"></a>01321 <span class="comment">   *****************/</span>
<a name="l01322"></a>01322 
<a name="l01323"></a>01323 
<a name="l01325"></a>01325 
<a name="l01328"></a>01328   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01329"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_00_01_allocator_01_4.php#a206f2265db8f733fbf11d574717d1e01">01329</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01330"></a>01330   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_00_01_allocator_01_4.php" title="Row-major symmetric full-matrix class.">Matrix&lt;T, Prop, RowSym, Allocator&gt;</a>&amp;
<a name="l01331"></a>01331   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSym, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01332"></a>01332   {
<a name="l01333"></a>01333     this-&gt;Fill(x);
<a name="l01334"></a>01334 
<a name="l01335"></a>01335     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01336"></a>01336   }
<a name="l01337"></a>01337 
<a name="l01339"></a>01339 
<a name="l01344"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_00_01_allocator_01_4.php#aa09de5e8c65dec6c8796245f540cb87b">01344</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01345"></a>01345   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_00_01_allocator_01_4.php" title="Row-major symmetric full-matrix class.">Matrix&lt;T, Prop, RowSym, Allocator&gt;</a>&amp;
<a name="l01346"></a>01346   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSym, Allocator&gt;::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop,
<a name="l01347"></a>01347                                                        <a class="code" href="class_seldon_1_1_row_sym.php">RowSym</a>,
<a name="l01348"></a>01348                                                        Allocator&gt;&amp; A)
<a name="l01349"></a>01349   {
<a name="l01350"></a>01350     this-&gt;Copy(A);
<a name="l01351"></a>01351     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01352"></a>01352   }
<a name="l01353"></a>01353 
<a name="l01354"></a>01354 
<a name="l01356"></a>01356 
<a name="l01359"></a>01359   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01360"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_00_01_allocator_01_4.php#a44d2b394ff94b327b2349ea2cc32e17c">01360</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01361"></a>01361   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_00_01_allocator_01_4.php" title="Row-major symmetric full-matrix class.">Matrix&lt;T, Prop, RowSym, Allocator&gt;</a>&amp;
<a name="l01362"></a>01362   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSym, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01363"></a>01363   {
<a name="l01364"></a>01364     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetDataSize();i++)
<a name="l01365"></a>01365       this-&gt;data_[i] *= x;
<a name="l01366"></a>01366 
<a name="l01367"></a>01367     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01368"></a>01368   }
<a name="l01369"></a>01369 
<a name="l01370"></a>01370 } <span class="comment">// namespace Seldon.</span>
<a name="l01371"></a>01371 
<a name="l01372"></a>01372 <span class="preprocessor">#define SELDON_FILE_MATRIX_SYMMETRIC_CXX</span>
<a name="l01373"></a>01373 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
