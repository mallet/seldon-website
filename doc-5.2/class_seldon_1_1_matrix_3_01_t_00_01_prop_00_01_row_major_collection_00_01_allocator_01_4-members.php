<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Matrix&lt; T, Prop, RowMajorCollection, Allocator &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_collection_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowMajorCollection, Allocator &gt;</a>, including all inherited members.<table>
  <tr bgcolor="#f0f0f0"><td><b>allocator</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_collection_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowMajorCollection, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_collection_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowMajorCollection, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>allocator_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#a508ae5c18005383193f2a289bf63d608">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>collection_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>collection_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_collection_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_collection_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_matrix_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_matrix_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#a0654c385392b3f0634a8721a797e249a">Copy</a>(const MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>data_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#ad1f04e7bbca49f4b4ac310f5a914085e">Deallocate</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#a1162607a4f1255a388598a6dc5eea1f8">GetDataSize</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">GetM</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#ab6cdf0d339a99d8ab7baa9e788c1d5b2">GetM</a>(int i) const</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">Seldon::Matrix_Base::GetM</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#ad18c52c1ed65330e6443e4519df390a4">GetMatrix</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#ad4fcca36d06203d8324a1fed15b12a65">GetMatrix</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#aaa428778f995ce6bbe08f1181fbb51e4">GetMmatrix</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">GetN</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#af2f613864c55870df9232689b81896c4">GetN</a>(int j) const</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">Seldon::Matrix_Base::GetN</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#a4a1165a73495d9eb4edfd49ba8e1f9b7">GetNmatrix</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">GetSize</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>m_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_collection_00_01_allocator_01_4.php#a5a2c326ccb8beb999bd7dd53b6f1ae5b">Matrix</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_collection_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowMajorCollection, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_collection_00_01_allocator_01_4.php#aee28bdf11cf37fdf9b2f7ed2b9b4cc51">Matrix</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_collection_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowMajorCollection, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#a66297d4139bb2b5b07979ef929358399">matrix_</a></td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a47988d7972247286332e853c13bbc00b">Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ad2ea11b0880dc32ba9472da9310c332b">Matrix_Base</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline, explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a127e0229931486cea3e6007988b446a0">Matrix_Base</a>(const Matrix_Base&lt; T, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>matrix_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>matrix_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>matrix_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#a4f5acfa96f23bdd2a9ad0f19e72a43c5">MatrixCollection</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#ad509be14489a556867ab433667c33a74">MatrixCollection</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#aac3566347d1654f0fd5748568ba11d60">MatrixCollection</a>(const MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#a234a716260de98d93fc40fc2a3fd44fe">Mlocal_</a></td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#aa16e80513c77e58a2c1b3974e360f220">Mlocal_sum_</a></td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#a094131b6edf39309cf51ec6aea9c271b">Mmatrix_</a></td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>n_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#afdcab0ec07a154e7ed23ee20b2f4f574">Nlocal_</a></td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#a2cd7c7804f76b6e8a6966e6597e05244">Nlocal_sum_</a></td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#abf9ee2f169c7992c2d540d07dd85b104">Nmatrix_</a></td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#a04f079fbfc7851ed7c1fa48a12936b9f">Nullify</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#a6b14667b3459659959f968847038524d">Nullify</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#a960417c35add0c178e31e8578bd5eb9b">nz_</a></td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#a774399178dd07c6e598e6051e8f89009">operator()</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#a9e3b633e9622c3a86561f8b08e9ff8e8">operator=</a>(const MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#af85cc972c66fe36630031f480dea28ca">Print</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#aaf59bd39550a1d4b460bc53e16362890">Print</a>(int m, int n) const</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>property</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_collection_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowMajorCollection, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_collection_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowMajorCollection, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#af54798b7169f468681f24203b6688aba">Read</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#ae7df567116f04beb60b90d5397ea16e8">Read</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#a2cafbea09c87f1798305211e30512994">Reallocate</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#a42828d0fb580934560080b2cf15eaa78">SetMatrix</a>(int m, int n, const Matrix&lt; T0, Prop0, Storage0, Allocator0 &gt; &amp;)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#a4960e099b8a36df7b772a6a9a7261cda">SetMatrix</a>(int m, int n, const Matrix&lt; T0, Prop0, RowSparse, Allocator0 &gt; &amp;)</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>storage</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_collection_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowMajorCollection, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_collection_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowMajorCollection, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>value_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_collection_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowMajorCollection, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_collection_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowMajorCollection, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#ab3016490b7df18d3017477352f6b60d6">Write</a>(string FileName, bool with_size) const</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#a66a1f1f41da42db350225a63f29000f5">Write</a>(ostream &amp;FileStream, bool with_size) const</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#a5c8fd96466c6409871d381feece50e44">WriteText</a>(string FileName) const</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#ad2d59d8ec2b0aca826f453d8ce646963">WriteText</a>(ostream &amp;FileStream) const</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af34a03c0cc56f757a83cd56f97c74ed8">~Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_collection.php#acd85176ecf48145fcb40621d4e7401e8">~MatrixCollection</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
