<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="tabs2">
    <ul class="tablist">
      <li><a href="functions.php"><span>All</span></a></li>
      <li class="current"><a href="functions_func.php"><span>Functions</span></a></li>
      <li><a href="functions_vars.php"><span>Variables</span></a></li>
      <li><a href="functions_type.php"><span>Typedefs</span></a></li>
    </ul>
  </div>
  <div class="tabs3">
    <ul class="tablist">
      <li><a href="functions_func.php#index_a"><span>a</span></a></li>
      <li><a href="functions_func_0x63.php#index_c"><span>c</span></a></li>
      <li><a href="functions_func_0x64.php#index_d"><span>d</span></a></li>
      <li><a href="functions_func_0x65.php#index_e"><span>e</span></a></li>
      <li><a href="functions_func_0x66.php#index_f"><span>f</span></a></li>
      <li><a href="functions_func_0x67.php#index_g"><span>g</span></a></li>
      <li><a href="functions_func_0x68.php#index_h"><span>h</span></a></li>
      <li><a href="functions_func_0x69.php#index_i"><span>i</span></a></li>
      <li><a href="functions_func_0x6c.php#index_l"><span>l</span></a></li>
      <li><a href="functions_func_0x6d.php#index_m"><span>m</span></a></li>
      <li><a href="functions_func_0x6e.php#index_n"><span>n</span></a></li>
      <li><a href="functions_func_0x6f.php#index_o"><span>o</span></a></li>
      <li><a href="functions_func_0x70.php#index_p"><span>p</span></a></li>
      <li class="current"><a href="functions_func_0x72.php#index_r"><span>r</span></a></li>
      <li><a href="functions_func_0x73.php#index_s"><span>s</span></a></li>
      <li><a href="functions_func_0x74.php#index_t"><span>t</span></a></li>
      <li><a href="functions_func_0x75.php#index_u"><span>u</span></a></li>
      <li><a href="functions_func_0x76.php#index_v"><span>v</span></a></li>
      <li><a href="functions_func_0x77.php#index_w"><span>w</span></a></li>
      <li><a href="functions_func_0x7a.php#index_z"><span>z</span></a></li>
      <li><a href="functions_func_0x7e.php#index_~"><span>~</span></a></li>
    </ul>
  </div>
<div class="contents">
&nbsp;

<h3><a class="anchor" id="index_r"></a>- r -</h3><ul>
<li>Read()
: <a class="el" href="class_seldon_1_1_array3_d.php#a943c1fb9b12c7e781bf4e39887636ab6">Seldon::Array3D&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a2c15862a4b7347bff455204fab3ea161">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a973fdab38a4f413ceec28d4c6b696d0c">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a2d646eec576003a589b1abff16a9f56a">Seldon::Matrix_TriangPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a58a6efaa3b49592962e9f3d50963ba62">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a882ab4d19bdff682493063a7a98e7631">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triangular.php#afd699e58126e4a75b734c72a7b07652a">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a4f3318c78df19c1a44d5f934c5707095">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a72272cc506d65af05eaf0d7d9e28ed0a">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___pointers.php#a45b2cbff13b8da2a247def9e87e910a6">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triangular.php#a1fad405d3c4a66ef2f1aab8ad0d2c054">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#afe242d0f1bdc515d83c5defd16ee4ece">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a8dd852fadb0f06aee6372585e6e36342">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_collection.php#af54798b7169f468681f24203b6688aba">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#ad2f97633a1f14c0ba7d8ae19d911458f">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#acd7cda2c966facc985412cdac9962a0f">Seldon::Vector&lt; T, PETScPar, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___hermitian.php#a4b2b0c1560503f1c81ac03230a118b47">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___pointers.php#a697168859e965ebfe3e0c722544217de">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7df567116f04beb60b90d5397ea16e8">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#abdab1e4df87abbed8c5a2ee34bddef3f">Seldon::Vector&lt; T, PETScPar, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#adc80b24a206e7ef91dd9b51aa33763d0">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_petsc_matrix.php#a2b5cb5cb3784d8754b5a2f3b73d256aa">Seldon::PetscMatrix&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a4335b7afce7863794f8ffbf76db91cbd">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a2ce4a8d2726b069e465d4156b1b531be">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___symmetric.php#af4e2d06a1044dd2c449384c580b347cd">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_petsc_matrix.php#a2c6835f0aaac0d2a61e834f32e5bc046">Seldon::PetscMatrix&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a634f8993354070451e53fd0c29bffa49">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector2.php#a3d4e468e1ec66a5e616f8eeee41c1897">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a58c2b7013db7765a01181f928833e530">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector2.php#a1f4923b25add7229c3bf87118875689a">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#ac28c5fe01b2ac97d21e560e9282444b5">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a11761a3a59b566c4e2d4ae2e020d1b24">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___hermitian.php#a6ac597e0a71778f33e9e1b7b98a26fea">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___symmetric.php#a911a42c752de06caed9614fed95bb8b5">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a8743f281d37cc023cd3e628eba4644f0">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#a96c8cfd79eaa900690893540509562f6">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a1457672aa96e240a6d32a5374f7aeffd">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a2e644c704eba4e3c531c403952189d86">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a0ae05e39f5b40baddab006c19cda76e3">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a29e41ce8ba3144c1a6982d55c035dfea">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#abeb6e31b268538d3c43fc8f795e994d4">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a0ba7bb9f087c978b3f528803136ffad7">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a19e44f4967fe55af83ef524f0f9be125">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_packed.php#ab52485427217c02d912ee5812dcbc4a9">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a1b8c684b65f487412ba7d437adfc2195">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sparse.php#ab3898fc51b20fa37a13cd69a2db3e5a1">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a96f1f584b5d04f9b6c92647c1dc2b353">Seldon::Matrix_TriangPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sparse.php#aa45e8f50f4227fbcfbc05730c1e65a90">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#aa70fdb81ecca409e7410b7bd91c71bc8">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>ReadText()
: <a class="el" href="class_seldon_1_1_matrix___triangular.php#a16abbe61e55e1a6a1f0a173f32457eed">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_petsc_matrix.php#a72c784f22fcba80ba6538f19bf29a9c1">Seldon::PetscMatrix&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ab0d4c7f4d291599c0d9cb735dd7d9dc8">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a22e85b66114b00ba89b1615bc1839b0c">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a2ae7d8f121dd314d8fab599225028432">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sparse.php#a6a2bad0b67f1410249057b24a75afcea">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ae5f0883621073e283ebd9ad047e81174">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#af55e946912a9936394784f9fd2968b74">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#acdbdc8ad44161af1fb63e6b8341257d9">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a0a9444914b4d3df7d5b99748ea98150e">Seldon::Vector&lt; T, PETScPar, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a3b9e7d2529315dfaa75a09d3c34da18a">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#af1a8f91e66d729c91fddb0a0c9e030b8">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___hermitian.php#a4457b2914ba7b363dcc0c339822231f9">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___herm_packed.php#aacf187c2102385c003476b6674d27fa1">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___pointers.php#a2ec5770914f507080dac5452b8ddaee5">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___symmetric.php#a9ae2c1b1f8f80c87801b92dfc9a9a885">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a64167ea39ea64fe44d1d1e1c838b4276">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triang_packed.php#ae204d32dd619e2241945cc3bfbebc018">Seldon::Matrix_TriangPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triangular.php#aa1064e91f58256717c26641aff9f8dbb">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>Reallocate()
: <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#abc4329f40632d4f9cfa083d314b45f4d">Seldon::Vector&lt; T, PETScPar, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#a67835c87beb5c2539ffe8af0d4a0b49c">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triangular.php#a472fa39bd3faee779cae713a5bd1aeb3">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#a21e22b8d942820eb4c60e3e34c2afcd6">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector2.php#aef69b526bcb070821a83f15ec44f0678">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
, <a class="el" href="class_seldon_1_1_array3_d.php#ae37a5c9ebc684062db0c1be3604641e8">Seldon::Array3D&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae600c754d08ba0af893d118cd3a38c60">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___hermitian.php#a87adcb1dbffb11e49fb698c6c5c9b7d7">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___herm_packed.php#aa2584f5a75b2ae0a2beb12fa45de3c11">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___pointers.php#a782143339736b347e5a1c7ecd4d0c793">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___symmetric.php#a488c4c92d4c5f2e867a8f008c7ba6e5d">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a22935d0bf71467744c3ee1ac505fe9e6">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triang_packed.php#ab0a798e9e9eac81e73e3176e8a4f20ac">Seldon::Matrix_TriangPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ae41ce69193bc5f40015b6bc0220db998">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_collection.php#a2cafbea09c87f1798305211e30512994">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_seq_dense_00_01_allocator_01_4.php#a33e12a6614cab92e3955cee408371b40">Seldon::Matrix&lt; T, Prop, PETScSeqDense, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#aaa6382ddfa0297d691e01634036ccdc3">Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad2348fb25206ef8516c7e835b7ce2287">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#afcd0d4612b325249c24407627b3dd568">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a72602182671e7f970959772510eed557">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sparse.php#a68315328fe25c14b9d8bef181c1c2f49">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af6ee0fe9e269657f40a8dccbe5e30b73">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a42b7c2729769fb7ffbb4be5f5214f582">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a1d1b36c91fe69fd44552e22c2808415f">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#abdf3b65f08de384fd0527e89a5478998">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#afcc8fef4210e7703493ad95acf389d19">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a6c1667ecf1265c0870ba2828b33a2d79">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector2.php#ab0e6ad584c49c4182153a4e523e177ea">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a8e14d527038311367db8ebfd79418995">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#aa704b2926c5f2671c31b7fa9f4f85d44">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
</li>
<li>ReallocateColumn()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a2f50f94ea485c1c81aecdedd76e09116">Seldon::Matrix&lt; T, Prop, ArrayColSymSparse, Allocator &gt;</a>
</li>
<li>ReallocateImagColumn()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a460d1b0cadc5cf4915ce024be57b1e58">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>
</li>
<li>ReallocateImagRow()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#aa798b00f5793c414777a5e3b421095a0">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#add6c25193de11ecc3f405c1ba713e0ef">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>
</li>
<li>ReallocateRealColumn()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a063f733a818719a8349d0de407a685a0">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>
</li>
<li>ReallocateRealRow()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#aefd387926aad7c3609a9f6bc062252e0">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a828bff3d9a061706b80cf3cc4a6725eb">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>
</li>
<li>ReallocateRow()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a141cfc9e4bacfcb106564df3154a104b">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a02d5086adf71d37c259c1a94a375bd57">Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</a>
</li>
<li>RefineSolution()
: <a class="el" href="class_seldon_1_1_matrix_pastix.php#a6f6a15165b8961d62505dc2ff4f261e0">Seldon::MatrixPastix&lt; T &gt;</a>
</li>
<li>RemoveSmallEntry()
: <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a6ff2b64975624f059a305a7661c24573">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a4b87eb77f9690ec605e9a1e7e1d155c5">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>ReplaceImagIndexColumn()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a54da32085a107892efa1828ebb9b43c7">Seldon::Matrix&lt; T, Prop, ArrayColComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a832f6f65f5c46c0cfaa66c0cb76e922e">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>
</li>
<li>ReplaceImagIndexRow()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a18e705d5da14b8ae374333f02a1801af">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a3e6d41ebaccab9d346d6fd291e28ff5d">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>
</li>
<li>ReplaceIndexColumn()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#aff85ed83508cf4e0bf75079ae2e73b4f">Seldon::Matrix&lt; T, Prop, ArrayColSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#aece56a1f1bdc20ffc86e29d92e8d0c41">Seldon::Matrix&lt; T, Prop, ArrayColSymSparse, Allocator &gt;</a>
</li>
<li>ReplaceIndexRow()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#af70fc4f3c8267b2a4f528e1fd3056020">Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a29c3376f6702a15782a46c240c7ce8bd">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a>
</li>
<li>ReplaceRealIndexColumn()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a618e575e978846f1ff95f9e8e3fe74c0">Seldon::Matrix&lt; T, Prop, ArrayColComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#addca588562b05576d4b37711bb4f6820">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>
</li>
<li>ReplaceRealIndexRow()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a5d858213b73625c365f2cf80c955b56c">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a350b6c00041f9e9db6bf111ac431bdf9">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>
</li>
<li>Resize()
: <a class="el" href="class_seldon_1_1_matrix___triangular.php#a30b10bb11499845df9d647c3e0ef8915">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php#aa5fb51377f06688fd10f00ef5abd12dd">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sparse.php#ad08188610a0d5051dad66e43ea2a017f">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a713d118c2c4a588ed3d043f45619e27b">Seldon::PETScVector&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_packed_00_01_allocator_01_4.php#ab1ba02f7e0a06777d9f1fa40b173b376">Seldon::Matrix&lt; T, Prop, ColUpTriangPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#aa53b17b254435486a52c058490abb0fb">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___hermitian.php#a6fe439e0f65eb7acd72730b4853e5e91">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___symmetric.php#a2369e0b64027be77dd3ba9e3b77e4971">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_petsc_matrix.php#a0cfea8ec472b6f1b44fb04eae709f8f8">Seldon::PetscMatrix&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_packed_00_01_allocator_01_4.php#aea77710be83e41d8e87e7d22bfab4265">Seldon::Matrix&lt; T, Prop, ColSymPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad2ef28f874e37f257af097c6ec306dfd">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a17e9f9857a46c900b33ab803ca584c2f">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___pointers.php#ae5cee7b46956ee19dbbb3308613378ec">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a99dda1b202ce562645f890897c593515">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a43f12ba2de0fb8d0ed79449b99e3c9a0">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sparse.php#a84a417a78587e38b7e9caeecb766aa84">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a2c6f9586424529c8b06b6110c5a01efa">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_packed_00_01_allocator_01_4.php#a9a9e04477cd9fb0bcded3aa2f5708fed">Seldon::Matrix&lt; T, Prop, RowUpTriangPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_00_01_allocator_01_4.php#aa3200624306161533e6be0e2263f8fdb">Seldon::Matrix&lt; T, Prop, RowSymPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a9166689334cc2f731220f76b730d692f">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a3637911015d5a0fafa4811ee2204bbc5">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_packed_00_01_allocator_01_4.php#ae2cbe0fdaa637692a6858648cadcb761">Seldon::Matrix&lt; T, Prop, ColLoTriangPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php#a46426f0740af4a12d2873b9b692f6278">Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a6537ad73d443b7b1e7008f626593811e">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a00809c7e5168b170c34e6e9c60d03674">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_packed_00_01_allocator_01_4.php#aa4e1fe59977fb95839539692ff8ed832">Seldon::Matrix&lt; T, Prop, RowHermPacked, Allocator &gt;</a>
</li>
<li>ResizeColumn()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a7350581df0edd83fe9d180db302afd56">Seldon::Matrix&lt; T, Prop, ArrayColSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#ad92aa8f008beddbec6d8a41711409655">Seldon::Matrix&lt; T, Prop, ArrayColSparse, Allocator &gt;</a>
</li>
<li>ResizeImagColumn()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#ac11baddc386e9569864bb21de1282eb0">Seldon::Matrix&lt; T, Prop, ArrayColComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a8f954de1b994af747e4e4e6c3bc2d8b9">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>
</li>
<li>ResizeImagRow()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#af89faca49416df1ad0b4262d440adec9">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a770cf431a7508fad913ef06bf487f9d6">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>
</li>
<li>ResizeRealColumn()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#ad7da5d2ec6445de44d3a5a81a0b92128">Seldon::Matrix&lt; T, Prop, ArrayColComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ab21dd3904c7fb5d4673106b83b5099d4">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>
</li>
<li>ResizeRealRow()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#ac668aac85ed864892bd3f4209ced4cf2">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a9a7d49e83e0838f63c4e7210ccc42218">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>
</li>
<li>ResizeRow()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#ad70cf7b6b6aadd9c1e51bc1944550d91">Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a6d7b0590ee4833ecc7a38c787cea92c3">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a>
</li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
