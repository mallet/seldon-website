<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="tabs2">
    <ul class="tablist">
      <li class="current"><a href="functions.php"><span>All</span></a></li>
      <li><a href="functions_func.php"><span>Functions</span></a></li>
      <li><a href="functions_vars.php"><span>Variables</span></a></li>
      <li><a href="functions_type.php"><span>Typedefs</span></a></li>
    </ul>
  </div>
  <div class="tabs3">
    <ul class="tablist">
      <li><a href="functions.php#index_a"><span>a</span></a></li>
      <li><a href="functions_0x63.php#index_c"><span>c</span></a></li>
      <li><a href="functions_0x64.php#index_d"><span>d</span></a></li>
      <li><a href="functions_0x65.php#index_e"><span>e</span></a></li>
      <li><a href="functions_0x66.php#index_f"><span>f</span></a></li>
      <li><a href="functions_0x67.php#index_g"><span>g</span></a></li>
      <li><a href="functions_0x68.php#index_h"><span>h</span></a></li>
      <li><a href="functions_0x69.php#index_i"><span>i</span></a></li>
      <li><a href="functions_0x6c.php#index_l"><span>l</span></a></li>
      <li><a href="functions_0x6d.php#index_m"><span>m</span></a></li>
      <li><a href="functions_0x6e.php#index_n"><span>n</span></a></li>
      <li><a href="functions_0x6f.php#index_o"><span>o</span></a></li>
      <li class="current"><a href="functions_0x70.php#index_p"><span>p</span></a></li>
      <li><a href="functions_0x72.php#index_r"><span>r</span></a></li>
      <li><a href="functions_0x73.php#index_s"><span>s</span></a></li>
      <li><a href="functions_0x74.php#index_t"><span>t</span></a></li>
      <li><a href="functions_0x75.php#index_u"><span>u</span></a></li>
      <li><a href="functions_0x76.php#index_v"><span>v</span></a></li>
      <li><a href="functions_0x77.php#index_w"><span>w</span></a></li>
      <li><a href="functions_0x78.php#index_x"><span>x</span></a></li>
      <li><a href="functions_0x7a.php#index_z"><span>z</span></a></li>
      <li><a href="functions_0x7e.php#index_~"><span>~</span></a></li>
    </ul>
  </div>
<div class="contents">
Here is a list of all documented class members with links to the class documentation for each member:

<h3><a class="anchor" id="index_p"></a>- p -</h3><ul>
<li>parameter_
: <a class="el" href="class_seldon_1_1_n_lopt_solver.php#aade135d662e35906b7cae2ecbd1d789c">Seldon::NLoptSolver</a>
</li>
<li>parameter_restart
: <a class="el" href="class_seldon_1_1_iteration.php#a5c4d36f6bff1c44d6e7b31bb3c70b603">Seldon::Iteration&lt; Titer &gt;</a>
</li>
<li>parameter_tolerance_
: <a class="el" href="class_seldon_1_1_n_lopt_solver.php#adefce6eb95498ffb35690ce4c8c7f637">Seldon::NLoptSolver</a>
</li>
<li>pastix_data
: <a class="el" href="class_seldon_1_1_matrix_pastix.php#a1d728cf85201e0e4b35fd66c985ca95a">Seldon::MatrixPastix&lt; T &gt;</a>
</li>
<li>PerformAnalysis()
: <a class="el" href="class_seldon_1_1_matrix_mumps.php#a9fd9e06d1da9a27e63cd2fb266f628c0">Seldon::MatrixMumps&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#a182a7c745c3d05a8fbe6733aa837087d">Seldon::MatrixUmfPack&lt; double &gt;</a>
</li>
<li>PerformFactorization()
: <a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#affb8a911794ad355a183469ed2e42480">Seldon::MatrixUmfPack&lt; double &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_mumps.php#ab59b1cd5d9c51196ea91dea968ae2c00">Seldon::MatrixMumps&lt; T &gt;</a>
</li>
<li>perm
: <a class="el" href="class_seldon_1_1_matrix_pastix.php#a0e40bdc9ac7c0ae1bd80e05d9942afee">Seldon::MatrixPastix&lt; T &gt;</a>
</li>
<li>permc_spec
: <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a7148d98444f507fdadd0127a2ba0b7ad">Seldon::MatrixSuperLU_Base&lt; T &gt;</a>
</li>
<li>permtol
: <a class="el" href="class_seldon_1_1_ilut_preconditioning.php#af02ed9cb6916f9cb848ad774b7686d94">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_sparse_seldon_solver.php#a5eec7564bd3006dfcb5aba246dff43f6">Seldon::SparseSeldonSolver&lt; T, Allocator &gt;</a>
</li>
<li>permut
: <a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a4e946689a26b4103f0551bd2e6cf96eb">Seldon::SparseDirectSolver&lt; T &gt;</a>
</li>
<li>permutation
: <a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#afbe06f619943edf715630578fca28f7a">Seldon::SparseCholeskySolver&lt; T &gt;</a>
</li>
<li>permutation_row
: <a class="el" href="class_seldon_1_1_sparse_seldon_solver.php#a98d9ac99260854c88001033c27003789">Seldon::SparseSeldonSolver&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_ilut_preconditioning.php#a81f338ca2bcf311a3fb70d4086512632">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a>
</li>
<li>petsc_matrix_
: <a class="el" href="class_seldon_1_1_petsc_matrix.php#a472c2776d1ddbe98b586a6260ea9dcfc">Seldon::PetscMatrix&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>petsc_matrix_deallocated_
: <a class="el" href="class_seldon_1_1_petsc_matrix.php#a06e38862e1dd29739d952dc62ee40c9d">Seldon::PetscMatrix&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>petsc_vector_
: <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a323f6bd490f539750d525646b15583f6">Seldon::PETScVector&lt; T, Allocator &gt;</a>
</li>
<li>petsc_vector_deallocated_
: <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a39bcb5ba22a031f8d6fbebea571a3c03">Seldon::PETScVector&lt; T, Allocator &gt;</a>
</li>
<li>PetscMatrix()
: <a class="el" href="class_seldon_1_1_petsc_matrix.php#aff9053f274de65aa8e0f69af8bd5491c">Seldon::PetscMatrix&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>PETScVector()
: <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a6ec09b49f9c964762ab9120380dc54d6">Seldon::PETScVector&lt; T, Allocator &gt;</a>
</li>
<li>pointer
: <a class="el" href="class_seldon_1_1_matrix_mumps.php#a7d695b24c99275e0911cb817757f2737">Seldon::MatrixMumps&lt; T &gt;</a>
</li>
<li>Preconditioner_Base()
: <a class="el" href="class_seldon_1_1_preconditioner___base.php#a2845a80350be45add812148062be20e9">Seldon::Preconditioner_Base</a>
</li>
<li>Print()
: <a class="el" href="class_seldon_1_1_matrix___hermitian.php#a433db64775f698010e5f13c57fa17c8c">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a7539ec920e545a23259b7fc5bcda7b25">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sparse.php#a8d6d48b6f0277f1df2d52b798975407e">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#abe86d6545d4aeca07257de2d8e7a85f2">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#adba43c4d26c5b47cd5868e26418f147c">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a8c5f87f1d06974d31d8ebe8dcc0fcb8e">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a5db0dd7d15f1a4ddf8f66bb68ab2cbd0">Seldon::Vector&lt; T, PETScPar, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___hermitian.php#a46a74bc40dd065078ea0d44fd1d8eb02">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a20b7ac2a821a2f54f4e325c07846b6cd">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#aff51ef2a12bca16732cd049b84502a87">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___hermitian.php#ac9828e7dfa45bf27fcc5a6c1afae8161">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#aa12c9192437db3a0d15566311646150d">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a3bf9cba0b13332169dd7bf67810387dd">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a61ff8d09ea73a05eff4fb21e16fb1118">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ac24abb2e047eb8b91728023074b3fb13">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a149556a4c7e8b8e9c952a7f3bc2f5942">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#ae9b1d53be394fd8ba4a8e58f9b3b0957">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a379079777210c4dfd9377f5fbc843c85">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector2.php#ab283856136c91c88d2f944eabed7bdc8">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#a9b3c87591141123ac43759d6b9ca9531">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_petsc_matrix.php#a4b5bb6ff820bb4b06605fc367ee9b12c">Seldon::PetscMatrix&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a51b162e562a758741735fa1a6875ec9e">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___pointers.php#a648346709f078652cb0a76f4001c901e">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___symmetric.php#a018e68da0aa00e2a3bb7d5b989b77837">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a6a31d1d0e30519f28a456fdd3e7a2a72">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a931e21546d023c42baf9c2d6cfddb3ee">Seldon::Matrix_TriangPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triangular.php#ad1b8f26eb7bfda6d3eee2a70707dea73">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_collection.php#af85cc972c66fe36630031f480dea28ca">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_petsc_matrix.php#adb57f345a20c0f85f5bb20b8341c49cb">Seldon::PetscMatrix&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_seq_dense_00_01_allocator_01_4.php#ae8e6138f19edf5892b10db60b5480e03">Seldon::Matrix&lt; T, Prop, PETScSeqDense, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php#aada3ed094eefa207ce84db9f69e6b3f3">Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_array3_d.php#aca40f1ee2914601787fceafab5d8c654">Seldon::Array3D&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#afa5beff801e73f6b95958d983a4e2c23">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a788108f4822afc37c6ef4054b37a03bc">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
</li>
<li>print_level
: <a class="el" href="class_seldon_1_1_matrix_pastix.php#a138482f32cb3046a3faf597aff405fb3">Seldon::MatrixPastix&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_iteration.php#a82adaef364a9a20a7525ab9299b6bfd5">Seldon::Iteration&lt; Titer &gt;</a>
, <a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php#a877304542a5dfbcb2a09a308477627b6">Seldon::SparseCholeskySolver&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_sparse_seldon_solver.php#a0f888b5b1b83a88c9aa649c2c5616376">Seldon::SparseSeldonSolver&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_ilut_preconditioning.php#a11647ecc317646a9560944199040df92">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a>
</li>
<li>PrintColumn()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#a2977eb745c35cea51499839b3903a6e8">Seldon::Matrix&lt; T, Prop, ArrayColSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a875318957a4ebbeabb1c01f24ea9e9cf">Seldon::Matrix&lt; T, Prop, ArrayColSymSparse, Allocator &gt;</a>
</li>
<li>PrintImagColumn()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#afa56428c069675df01f46fd68d39be47">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a1d651709170e5a488fb2f6a2cffa9ece">Seldon::Matrix&lt; T, Prop, ArrayColComplexSparse, Allocator &gt;</a>
</li>
<li>PrintImagRow()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#aebbf5322f0709878e15c379391a8aec8">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a6bf23b591a902626ffb78da822bc4c3e">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>
</li>
<li>PrintRealColumn()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ad080f5381fb2a720b7ecb72b245db852">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#ab9fef30299f95e6c195194f1f0ac3106">Seldon::Matrix&lt; T, Prop, ArrayColComplexSparse, Allocator &gt;</a>
</li>
<li>PrintRealRow()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a4287416fdfc3c14ca814be6ebc29153c">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#aeeca29af2514e61e003925875e7f7c33">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>
</li>
<li>PrintRow()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a10ca8f311170cfa8b11f6caa63d42635">Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#aedf0c511638d6bcc7f5b8e4cfddf6e93">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a>
</li>
<li>ptr_
: <a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a9a9a32db0c5d5edb1f6c7bc5e7560876">Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;</a>
</li>
<li>PushBack()
: <a class="el" href="class_seldon_1_1_vector2.php#a1d386f766b75c53d17b66bc843a4db54">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#aabfc0ec2c12884196ebe4ce2ba4638da">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector2.php#acbc05b0873524168d21b8f465a22670a">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#a96ad43bc29d35bf3a1b1bffc507d8d46">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a6a64b54653758dbbdcd650c8ffba4d52">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#aa0b88a0415692922c36c2b2f284efeca">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#a71fd6304ae029838b323c8092a2a109f">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector2.php#acd15ae896e68859a4d7f6a2aa29f09d2">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#a99bb83b050637e5e1d2aaedd540a33f4">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
</li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
