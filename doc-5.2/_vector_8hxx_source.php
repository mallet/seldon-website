<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>vector/Vector.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_VECTOR_HXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 <span class="preprocessor">#include &quot;../share/Common.hxx&quot;</span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;../share/Properties.hxx&quot;</span>
<a name="l00024"></a>00024 <span class="preprocessor">#include &quot;../share/Storage.hxx&quot;</span>
<a name="l00025"></a>00025 <span class="preprocessor">#include &quot;../share/Errors.hxx&quot;</span>
<a name="l00026"></a>00026 <span class="preprocessor">#include &quot;../share/Allocator.hxx&quot;</span>
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 <span class="keyword">namespace </span>Seldon
<a name="l00029"></a>00029 {
<a name="l00030"></a>00030 
<a name="l00031"></a>00031 
<a name="l00033"></a>00033 
<a name="l00037"></a>00037   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator = <span class="keywordtype">SEL</span>DON_DEFAULT_ALLOCATOR&lt;T&gt; &gt;
<a name="l00038"></a><a class="code" href="class_seldon_1_1_vector___base.php">00038</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_vector___base.php" title="Base structure for all vectors.">Vector_Base</a>
<a name="l00039"></a>00039   {
<a name="l00040"></a>00040     <span class="comment">// typdef declarations.</span>
<a name="l00041"></a>00041   <span class="keyword">public</span>:
<a name="l00042"></a>00042     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00043"></a>00043     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::pointer pointer;
<a name="l00044"></a>00044     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_pointer const_pointer;
<a name="l00045"></a>00045     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::reference reference;
<a name="l00046"></a>00046     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_reference const_reference;
<a name="l00047"></a>00047 
<a name="l00048"></a>00048     <span class="comment">// Static attributes.</span>
<a name="l00049"></a>00049   <span class="keyword">protected</span>:
<a name="l00050"></a>00050     <span class="keyword">static</span> Allocator vect_allocator_;
<a name="l00051"></a>00051 
<a name="l00052"></a>00052     <span class="comment">// Attributes.</span>
<a name="l00053"></a>00053   <span class="keyword">protected</span>:
<a name="l00054"></a>00054     <span class="comment">// Number of elements.</span>
<a name="l00055"></a>00055     <span class="keywordtype">int</span> m_;
<a name="l00056"></a>00056     <span class="comment">// Pointer to stored elements.</span>
<a name="l00057"></a>00057     pointer data_;
<a name="l00058"></a>00058 
<a name="l00059"></a>00059     <span class="comment">// Methods.</span>
<a name="l00060"></a>00060   <span class="keyword">public</span>:
<a name="l00061"></a>00061     <span class="comment">// Constructors.</span>
<a name="l00062"></a>00062     <a class="code" href="class_seldon_1_1_vector___base.php#ac8a6cee506f094504138943629b9dcbb" title="Default constructor.">Vector_Base</a>();
<a name="l00063"></a>00063     <span class="keyword">explicit</span> <a class="code" href="class_seldon_1_1_vector___base.php#ac8a6cee506f094504138943629b9dcbb" title="Default constructor.">Vector_Base</a>(<span class="keywordtype">int</span> i);
<a name="l00064"></a>00064     <a class="code" href="class_seldon_1_1_vector___base.php#ac8a6cee506f094504138943629b9dcbb" title="Default constructor.">Vector_Base</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector___base.php" title="Base structure for all vectors.">Vector_Base&lt;T, Allocator&gt;</a>&amp; A);
<a name="l00065"></a>00065 
<a name="l00066"></a>00066     <span class="comment">// Destructor.</span>
<a name="l00067"></a>00067     <a class="code" href="class_seldon_1_1_vector___base.php#a6da510796ebef867765ea3a037debb1b" title="Destructor.">~Vector_Base</a>();
<a name="l00068"></a>00068 
<a name="l00069"></a>00069     <span class="comment">// Basic methods.</span>
<a name="l00070"></a>00070     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae" title="Returns the number of elements.">GetM</a>() <span class="keyword">const</span>;
<a name="l00071"></a>00071     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector___base.php#a075daae3607a4767a77a3de60ab30ba7" title="Returns the number of elements.">GetLength</a>() <span class="keyword">const</span>;
<a name="l00072"></a>00072     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector___base.php#a839880a3113c1885ead70d79fade0294" title="Returns the number of elements stored.">GetSize</a>() <span class="keyword">const</span>;
<a name="l00073"></a>00073     pointer <a class="code" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a" title="Returns a pointer to data_ (stored data).">GetData</a>() <span class="keyword">const</span>;
<a name="l00074"></a>00074     const_pointer <a class="code" href="class_seldon_1_1_vector___base.php#a454aea78c82c4317dfe4160cc7c95afa" title="Returns a const pointer to data_ (stored data).">GetDataConst</a>() <span class="keyword">const</span>;
<a name="l00075"></a>00075     <span class="keywordtype">void</span>* <a class="code" href="class_seldon_1_1_vector___base.php#ad0f5184bd9eec6fca70c54e459ced78f" title="Returns a pointer of type &amp;quot;void*&amp;quot; to the data array (data_).">GetDataVoid</a>() <span class="keyword">const</span>;
<a name="l00076"></a>00076     <span class="keyword">const</span> <span class="keywordtype">void</span>* <a class="code" href="class_seldon_1_1_vector___base.php#a0aadc006977de037eb3ee550683e3f19" title="Returns a pointer of type &amp;quot;const void*&amp;quot; to the data array (data_).">GetDataConstVoid</a>() <span class="keyword">const</span>;
<a name="l00077"></a>00077 
<a name="l00078"></a>00078   };
<a name="l00079"></a>00079 
<a name="l00080"></a>00080 
<a name="l00081"></a>00081   <span class="comment">// Vector allocator.</span>
<a name="l00082"></a>00082   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00083"></a>00083   Allocator <a class="code" href="class_seldon_1_1_vector___base.php" title="Base structure for all vectors.">Vector_Base&lt;T, Allocator&gt;::vect_allocator_</a>;
<a name="l00084"></a>00084 
<a name="l00085"></a>00085 
<a name="l00087"></a>00087 
<a name="l00090"></a>00090   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00091"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">00091</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;T, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator&gt;: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_vector___base.php" title="Base structure for all vectors.">Vector_Base</a>&lt;T, Allocator&gt;
<a name="l00092"></a>00092   {
<a name="l00093"></a>00093     <span class="comment">// typedef declaration.</span>
<a name="l00094"></a>00094   <span class="keyword">public</span>:
<a name="l00095"></a>00095     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00096"></a>00096     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::pointer pointer;
<a name="l00097"></a>00097     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_pointer const_pointer;
<a name="l00098"></a>00098     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::reference reference;
<a name="l00099"></a>00099     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_reference const_reference;
<a name="l00100"></a>00100 
<a name="l00101"></a>00101     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a> <a class="code" href="class_seldon_1_1_vect_full.php">storage</a>;
<a name="l00102"></a>00102 
<a name="l00103"></a>00103     <span class="comment">// Attributes.</span>
<a name="l00104"></a>00104   <span class="keyword">private</span>:
<a name="l00105"></a>00105 
<a name="l00106"></a>00106     <span class="comment">// Methods.</span>
<a name="l00107"></a>00107   <span class="keyword">public</span>:
<a name="l00108"></a>00108     <span class="comment">// Constructor.</span>
<a name="l00109"></a>00109     <span class="keyword">explicit</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>();
<a name="l00110"></a>00110     <span class="keyword">explicit</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>(<span class="keywordtype">int</span> i);
<a name="l00111"></a>00111     <a class="code" href="class_seldon_1_1_vector.php">Vector</a>(<span class="keywordtype">int</span> i, pointer data);
<a name="l00112"></a>00112     <a class="code" href="class_seldon_1_1_vector.php">Vector</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a>&amp; A);
<a name="l00113"></a>00113 
<a name="l00114"></a>00114     <span class="comment">// Destructor.</span>
<a name="l00115"></a>00115     ~<a class="code" href="class_seldon_1_1_vector.php">Vector</a>();
<a name="l00116"></a>00116     <span class="keywordtype">void</span> Clear();
<a name="l00117"></a>00117 
<a name="l00118"></a>00118     <span class="comment">// Memory management.</span>
<a name="l00119"></a>00119     <span class="keywordtype">void</span> Reallocate(<span class="keywordtype">int</span> i);
<a name="l00120"></a>00120     <span class="keywordtype">void</span> Resize(<span class="keywordtype">int</span> i);
<a name="l00121"></a>00121     <span class="keywordtype">void</span> SetData(<span class="keywordtype">int</span> i, pointer data);
<a name="l00122"></a>00122     <span class="keyword">template</span> &lt;<span class="keyword">class</span> Allocator0&gt;
<a name="l00123"></a>00123     <span class="keywordtype">void</span> SetData(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator0&gt;</a>&amp; V);
<a name="l00124"></a>00124     <span class="keywordtype">void</span> Nullify();
<a name="l00125"></a>00125 
<a name="l00126"></a>00126     <span class="comment">// Element access and affectation.</span>
<a name="l00127"></a>00127     reference operator() (<span class="keywordtype">int</span> i);
<a name="l00128"></a>00128     reference Get(<span class="keywordtype">int</span> i);
<a name="l00129"></a>00129 <span class="preprocessor">#ifndef SWIG</span>
<a name="l00130"></a>00130 <span class="preprocessor"></span>    const_reference operator() (<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00131"></a>00131     const_reference Get(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00132"></a>00132     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a>&amp; operator= (<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;T, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>,
<a name="l00133"></a>00133                                                Allocator&gt;&amp; X);
<a name="l00134"></a>00134 <span class="preprocessor">#endif</span>
<a name="l00135"></a>00135 <span class="preprocessor"></span>    <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a>&amp; X);
<a name="l00136"></a>00136     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> Copy() <span class="keyword">const</span>;
<a name="l00137"></a>00137     <span class="keywordtype">void</span> Append(<span class="keyword">const</span> T&amp; x);
<a name="l00138"></a>00138     <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l00139"></a>00139     <span class="keywordtype">void</span> PushBack(<span class="keyword">const</span> T0&amp; x);
<a name="l00140"></a>00140     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator0&gt;
<a name="l00141"></a>00141     <span class="keywordtype">void</span> PushBack(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator0&gt;</a>&amp; X);
<a name="l00142"></a>00142 
<a name="l00143"></a>00143     <span class="comment">// Basic functions.</span>
<a name="l00144"></a>00144     <span class="keywordtype">int</span> GetDataSize();
<a name="l00145"></a>00145 
<a name="l00146"></a>00146     <span class="comment">// Convenient functions.</span>
<a name="l00147"></a>00147     <span class="keywordtype">void</span> Zero();
<a name="l00148"></a>00148     <span class="keywordtype">void</span> Fill();
<a name="l00149"></a>00149     <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00150"></a>00150     <span class="keywordtype">void</span> Fill(<span class="keyword">const</span> T0&amp; x);
<a name="l00151"></a>00151 <span class="preprocessor">#ifndef SWIG</span>
<a name="l00152"></a>00152 <span class="preprocessor"></span>    <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00153"></a>00153     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a>&amp; operator= (<span class="keyword">const</span> T0&amp; X);
<a name="l00154"></a>00154 <span class="preprocessor">#endif</span>
<a name="l00155"></a>00155 <span class="preprocessor"></span>    <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00156"></a>00156     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a>&amp; operator*= (<span class="keyword">const</span> T0&amp; X);
<a name="l00157"></a>00157     <span class="keywordtype">void</span> FillRand();
<a name="l00158"></a>00158     <span class="keywordtype">void</span> Print() <span class="keyword">const</span>;
<a name="l00159"></a>00159 
<a name="l00160"></a>00160     <span class="comment">// Norms.</span>
<a name="l00161"></a>00161     value_type GetNormInf() <span class="keyword">const</span>;
<a name="l00162"></a>00162     <span class="keywordtype">int</span> GetNormInfIndex() <span class="keyword">const</span>;
<a name="l00163"></a>00163 
<a name="l00164"></a>00164     <span class="comment">// Input/output functions.</span>
<a name="l00165"></a>00165     <span class="keywordtype">void</span> Write(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size = <span class="keyword">true</span>) <span class="keyword">const</span>;
<a name="l00166"></a>00166     <span class="keywordtype">void</span> Write(ostream&amp; FileStream, <span class="keywordtype">bool</span> with_size = <span class="keyword">true</span>) <span class="keyword">const</span>;
<a name="l00167"></a>00167     <span class="keywordtype">void</span> WriteText(<span class="keywordtype">string</span> FileName) <span class="keyword">const</span>;
<a name="l00168"></a>00168     <span class="keywordtype">void</span> WriteText(ostream&amp; FileStream) <span class="keyword">const</span>;
<a name="l00169"></a>00169 <span class="preprocessor">#ifdef SELDON_WITH_HDF5</span>
<a name="l00170"></a>00170 <span class="preprocessor"></span>    <span class="keywordtype">void</span> WriteHDF5(<span class="keywordtype">string</span> FileName, <span class="keywordtype">string</span> group_name,
<a name="l00171"></a>00171                    <span class="keywordtype">string</span> dataset_name) <span class="keyword">const</span>;
<a name="l00172"></a>00172 <span class="preprocessor">#endif</span>
<a name="l00173"></a>00173 <span class="preprocessor"></span>    <span class="keywordtype">void</span> Read(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size = <span class="keyword">true</span>);
<a name="l00174"></a>00174     <span class="keywordtype">void</span> Read(istream&amp; FileStream, <span class="keywordtype">bool</span> with_size = <span class="keyword">true</span>);
<a name="l00175"></a>00175     <span class="keywordtype">void</span> ReadText(<span class="keywordtype">string</span> FileName);
<a name="l00176"></a>00176     <span class="keywordtype">void</span> ReadText(istream&amp; FileStream);
<a name="l00177"></a>00177   };
<a name="l00178"></a>00178 
<a name="l00179"></a>00179 <span class="preprocessor">#ifndef SWIG</span>
<a name="l00180"></a>00180 <span class="preprocessor"></span>  <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00181"></a>00181   ostream&amp; <a class="code" href="namespace_seldon.php#a5bd4a6d6f3c83048663d57d2fc032107" title="operator&amp;lt;&amp;lt; overloaded for a 3D array.">operator &lt;&lt; </a>(ostream&amp; out,
<a name="l00182"></a>00182                         <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage, Allocator&gt;</a>&amp; V);
<a name="l00183"></a>00183 <span class="preprocessor">#endif</span>
<a name="l00184"></a>00184 <span class="preprocessor"></span>
<a name="l00185"></a>00185 
<a name="l00186"></a>00186 } <span class="comment">// namespace Seldon.</span>
<a name="l00187"></a>00187 
<a name="l00188"></a>00188 <span class="preprocessor">#define SELDON_FILE_VECTOR_HXX</span>
<a name="l00189"></a>00189 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
