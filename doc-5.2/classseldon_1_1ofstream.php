<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><b>seldon</b>      </li>
      <li><a class="el" href="classseldon_1_1ofstream.php">ofstream</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pub-attribs">Public Attributes</a> &#124;
<a href="#pub-static-attribs">Static Public Attributes</a>  </div>
  <div class="headertitle">
<h1>seldon::ofstream Class Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="seldon::ofstream" --><!-- doxytag: inherits="seldon::ostream" --><div class="dynheader">
Inheritance diagram for seldon::ofstream:</div>
<div class="dyncontent">
 <div class="center">
  <img src="classseldon_1_1ofstream.png" usemap="#seldon::ofstream_map" alt=""/>
  <map id="seldon::ofstream_map" name="seldon::ofstream_map">
<area href="classseldon_1_1ostream.php" alt="seldon::ostream" shape="rect" coords="0,168,105,192"/>
<area href="classseldon_1_1ios.php" alt="seldon::ios" shape="rect" coords="0,112,105,136"/>
<area href="classseldon_1_1ios__base.php" alt="seldon::ios_base" shape="rect" coords="0,56,105,80"/>
<area href="classseldon_1_1__object.php" alt="seldon::_object" shape="rect" coords="0,0,105,24"/>
</map>
</div>

<p><a href="classseldon_1_1ofstream-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a99c01cc32afde8b5c474d33f87d40642"></a><!-- doxytag: member="seldon::ofstream::__init__" ref="a99c01cc32afde8b5c474d33f87d40642" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__init__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a731c81e1e7c9902209996f4b02dcd674"></a><!-- doxytag: member="seldon::ofstream::is_open" ref="a731c81e1e7c9902209996f4b02dcd674" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>is_open</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1b1256f8533f6eccc88f4b2d3a871b04"></a><!-- doxytag: member="seldon::ofstream::close" ref="a1b1256f8533f6eccc88f4b2d3a871b04" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>close</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a21a78e39d5277faea40b5625d9c094d5"></a><!-- doxytag: member="seldon::ofstream::__init__" ref="a21a78e39d5277faea40b5625d9c094d5" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__init__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a10162dc5168f1cecf93898d9fdb2234d"></a><!-- doxytag: member="seldon::ofstream::__lshift__" ref="a10162dc5168f1cecf93898d9fdb2234d" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__lshift__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab8f25fa8926196201f32653aa905e3f4"></a><!-- doxytag: member="seldon::ofstream::put" ref="ab8f25fa8926196201f32653aa905e3f4" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>put</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a17fbcd61efe7107f231dbbbb9d83bdc1"></a><!-- doxytag: member="seldon::ofstream::write" ref="a17fbcd61efe7107f231dbbbb9d83bdc1" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>write</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab056e15bd0019a1769718a65941a7320"></a><!-- doxytag: member="seldon::ofstream::flush" ref="ab056e15bd0019a1769718a65941a7320" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>flush</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aea9f5a58e4c75c62522b214ab17db776"></a><!-- doxytag: member="seldon::ofstream::tellp" ref="aea9f5a58e4c75c62522b214ab17db776" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>tellp</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acad37933e22e6ccba0f34d816b5710ed"></a><!-- doxytag: member="seldon::ofstream::seekp" ref="acad37933e22e6ccba0f34d816b5710ed" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>seekp</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a678a94928eb876b50ad1287078172915"></a><!-- doxytag: member="seldon::ofstream::rdstate" ref="a678a94928eb876b50ad1287078172915" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>rdstate</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aec6d7d998c176e6298fb8b7cf46e736b"></a><!-- doxytag: member="seldon::ofstream::clear" ref="aec6d7d998c176e6298fb8b7cf46e736b" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>clear</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7c5dac98f5bb493c6f57da1ccbe6ff3f"></a><!-- doxytag: member="seldon::ofstream::setstate" ref="a7c5dac98f5bb493c6f57da1ccbe6ff3f" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>setstate</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3e63517a623b9429d69c5ed2eff2b3f8"></a><!-- doxytag: member="seldon::ofstream::good" ref="a3e63517a623b9429d69c5ed2eff2b3f8" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>good</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a92c326bb5bc190c93c13edc97f14f22a"></a><!-- doxytag: member="seldon::ofstream::eof" ref="a92c326bb5bc190c93c13edc97f14f22a" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>eof</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9f5c4aae249124e87ed9c5fe380ecdf6"></a><!-- doxytag: member="seldon::ofstream::fail" ref="a9f5c4aae249124e87ed9c5fe380ecdf6" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>fail</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2e362b04abfdc51772b3d8f231a8aaf7"></a><!-- doxytag: member="seldon::ofstream::bad" ref="a2e362b04abfdc51772b3d8f231a8aaf7" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>bad</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aff1e82ffa19bc755537af9d2cbf78d19"></a><!-- doxytag: member="seldon::ofstream::exceptions" ref="aff1e82ffa19bc755537af9d2cbf78d19" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>exceptions</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a15ba001738553b6cf4106b2ecc95b6c1"></a><!-- doxytag: member="seldon::ofstream::tie" ref="a15ba001738553b6cf4106b2ecc95b6c1" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>tie</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac249cb29c5966ee8c72cceba6b72e7f7"></a><!-- doxytag: member="seldon::ofstream::rdbuf" ref="ac249cb29c5966ee8c72cceba6b72e7f7" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>rdbuf</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a01e722f8d653f520345072974fbd58d7"></a><!-- doxytag: member="seldon::ofstream::copyfmt" ref="a01e722f8d653f520345072974fbd58d7" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>copyfmt</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acf8d3506ec9f1f50f12f0c4b32ae454e"></a><!-- doxytag: member="seldon::ofstream::fill" ref="acf8d3506ec9f1f50f12f0c4b32ae454e" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>fill</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a030d88f05d5b5f4df35bceebaab463cd"></a><!-- doxytag: member="seldon::ofstream::imbue" ref="a030d88f05d5b5f4df35bceebaab463cd" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>imbue</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a28b4f6e187362360e57b2f3661a9a14a"></a><!-- doxytag: member="seldon::ofstream::narrow" ref="a28b4f6e187362360e57b2f3661a9a14a" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>narrow</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0475ad29ab83371c1c8eeb8f8421e7f5"></a><!-- doxytag: member="seldon::ofstream::widen" ref="a0475ad29ab83371c1c8eeb8f8421e7f5" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>widen</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a79c824472f079470603496be66a4a228"></a><!-- doxytag: member="seldon::ofstream::register_callback" ref="a79c824472f079470603496be66a4a228" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>register_callback</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="abe2d97bc10dbebbef7f448f4ac3ae661"></a><!-- doxytag: member="seldon::ofstream::flags" ref="abe2d97bc10dbebbef7f448f4ac3ae661" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>flags</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afad76f0141c49ed816f2592374bedddd"></a><!-- doxytag: member="seldon::ofstream::setf" ref="afad76f0141c49ed816f2592374bedddd" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>setf</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4102412d5a3824e0f9b57389805d93a9"></a><!-- doxytag: member="seldon::ofstream::unsetf" ref="a4102412d5a3824e0f9b57389805d93a9" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>unsetf</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae029ba7a4fdc1aac0b86362887b9e022"></a><!-- doxytag: member="seldon::ofstream::precision" ref="ae029ba7a4fdc1aac0b86362887b9e022" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>precision</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae6eb750f835d233eed128bd75a22282a"></a><!-- doxytag: member="seldon::ofstream::width" ref="ae6eb750f835d233eed128bd75a22282a" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>width</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af38faa6bb107e35e373977312d9585e5"></a><!-- doxytag: member="seldon::ofstream::getloc" ref="af38faa6bb107e35e373977312d9585e5" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>getloc</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afa1e3d0c3d344006af1a10f5e6762022"></a><!-- doxytag: member="seldon::ofstream::iword" ref="afa1e3d0c3d344006af1a10f5e6762022" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>iword</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7cbef9a3b6a2c82c4451c9eaf59a5d3a"></a><!-- doxytag: member="seldon::ofstream::pword" ref="a7cbef9a3b6a2c82c4451c9eaf59a5d3a" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>pword</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-attribs"></a>
Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad41731bc8ce783ad69f092b20f317598"></a><!-- doxytag: member="seldon::ofstream::this" ref="ad41731bc8ce783ad69f092b20f317598" args="" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><b>this</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-static-attribs"></a>
Static Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a673fe40b5a7bc9162a174bdf75f50375"></a><!-- doxytag: member="seldon::ofstream::erase_event" ref="a673fe40b5a7bc9162a174bdf75f50375" args="" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><b>erase_event</b> = _seldon.ios_base_erase_event</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac14f2ad710a3c7defae6badafb5b434b"></a><!-- doxytag: member="seldon::ofstream::imbue_event" ref="ac14f2ad710a3c7defae6badafb5b434b" args="" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><b>imbue_event</b> = _seldon.ios_base_imbue_event</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2623bdfc3f6270db8f57cc3e82f946ff"></a><!-- doxytag: member="seldon::ofstream::copyfmt_event" ref="a2623bdfc3f6270db8f57cc3e82f946ff" args="" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><b>copyfmt_event</b> = _seldon.ios_base_copyfmt_event</td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>

<p>Definition at line <a class="el" href="seldon_8py_source.php#l00372">372</a> of file <a class="el" href="seldon_8py_source.php">seldon.py</a>.</p>
<hr/>The documentation for this class was generated from the following file:<ul>
<li><a class="el" href="seldon_8py_source.php">seldon.py</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
