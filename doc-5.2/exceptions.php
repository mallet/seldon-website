<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Exceptions </h1>  </div>
</div>
<div class="contents">
<h2>Options</h2>
<p>Exceptions are raised if an error is detected. Seldon is able to check for several mistakes:</p>
<ul>
<li>
<p class="startli">SELDON_CHECK_IO: checks input/output operations on disk.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli">SELDON_CHECK_MEMORY: checks memory allocations and deallocations.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli">SELDON_CHECK_DIMENSIONS: checks that the dimensions of involved structures are compatible. Notice that there are methods in which the compatibility is not checked however.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli">SELDON_CHECK_BOUNDS: checks that indices are not out of range.</p>
<p class="endli"></p>
</li>
</ul>
<p>To enable <code>SELDON_CHECK_MEMORY</code>, for example, put (before <code>include &lt;<a class="el" href="_seldon_8hxx_source.php">Seldon.hxx</a>&gt;</code>):</p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">#define SELDON_CHECK_MEMORY</pre></div>  </pre><p>Alternatively, there are debug levels:</p>
<ul>
<li>
<p class="startli">SELDON_DEBUG_LEVEL_0: nothing is checked.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli">SELDON_DEBUG_LEVEL_1: equivalent to SELDON_CHECK_IO plus SELDON_CHECK_MEMORY.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli">SELDON_DEBUG_LEVEL_2: equivalent to SELDON_DEBUG_LEVEL_1 plus SELDON_CHECK_DIMENSIONS.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli">SELDON_DEBUG_LEVEL_3: equivalent to SELDON_DEBUG_LEVEL_2 plus SELDON_CHECK_BOUNDS.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli">SELDON_DEBUG_LEVEL_4: equivalent to SELDON_DEBUG_LEVEL_3.</p>
<p class="endli"></p>
</li>
</ul>
<p>In practice, it is advocated to choose SELDON_DEBUG_LEVEL_4 in the development stage and SELDON_DEBUG_LEVEL_2 for the stable version. Indeed SELDON_DEBUG_LEVEL_4 slows down the program but checks many things and SELDON_DEBUG_LEVEL_2 should not slow down the program and ensures that it is reasonably safe.</p>
<p>Development stage:  <pre class="prettyprint">  <div class="fragment"><pre class="fragment">#define SELDON_DEBUG_LEVEL_4</pre></div>  </pre><p>Stable version:  <pre class="prettyprint">  <div class="fragment"><pre class="fragment">#define SELDON_DEBUG_LEVEL_2</pre></div>  </pre><h2>Exceptions raised</h2>
<p>The objects that may be launched by Seldon are of type: <code>WrongArgument</code>, <code>NoMemory</code>, <code>WrongDim</code>, <code>WrongIndex</code>, <code>WrongRow</code>, <code>WrongCol</code>, <code>IOError</code> and <code>LapackError</code>. They all derive from <code>Error</code>. They provide the method <code>What</code> that returns a string explaining the error, and the method <code>CoutWhat</code> that displays on screen this explanation. Therefore, to catch an exception raised by Seldon, put:</p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">catch(Seldon::Error&amp; Err)
{
Err.CoutWhat();
}</pre></div>  </pre><p>Two macros are defined to help:</p>
<p><code>TRY</code>:</p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">#define TRY try {</pre></div>  </pre><p><code>END</code>:</p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">#define END                                                     \
  }                                                             \
    catch(Seldon::Error&amp; Err)                                   \
      {                                                         \
        Err.CoutWhat();                                         \
        return 1;                                               \
      }                                                         \
    catch (std::exception&amp; Err)                                 \
      {                                                         \
        cout &lt;&lt; "C++ exception: " &lt;&lt; Err.what() &lt;&lt; endl;        \
        return 1;                                               \
      }                                                         \
    catch (std::string&amp; str)                                    \
      {                                                         \
        cout &lt;&lt; str &lt;&lt; endl;                                    \
        return 1;                                               \
      }                                                         \
    catch (const char* str)                                     \
      {                                                         \
        cout &lt;&lt; str &lt;&lt; endl;                                    \
        return 1;                                               \
      }                                                         \
    catch(...)                                                  \
      {                                                         \
        cout &lt;&lt; "Unknown exception..." &lt;&lt; endl;                 \
        return 1;                                               \
      }
</pre></div>  </pre><p>It is advocated that you enclose your code (in the <code>main</code> function) with <code>TRY;</code> and <code>END;</code>:</p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
int main(int argc, char** argv)
{
  TRY;

  // Here goes your code.

  END;

  return 0;
}
</pre></div>  </pre><h2>Exceptions and debugging</h2>
<p>Suppose your code contains an error and raises an exception. You probably want to identify the function that raised the exception. The error message should contain the name of the function. But you probably want to know the exact line where the error occurred and the sequence of calls. Then, you have two options, using a debugger.</p>
<p>One option is to place a breakpoint in <code>Error::Error(string function = "", string comment = "")</code> (see file <a class="el" href="_errors_8cxx_source.php">share/Errors.cxx</a>) because this constructor should be called before the exception is actually raised.</p>
<p>Another option, more convenient because no breakpoint is to be placed, is to define <code>SELDON_WITH_ABORT</code>. With that flag activated, if a Seldon exception is raised, the program will simply abort. The call stack is then at hand. See the example below, using gdb under Linux. The program <code> error.cpp</code> demonstrates the technique:</p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
#define SELDON_WITH_ABORT
#define SELDON_DEBUG_LEVEL_4 // which checks bounds.
#include "Seldon.hxx"
using namespace Seldon;

int main(int argc, char** argv)
{
  TRY;

  IVect V(3);
  cout &lt;&lt; "Bad access: " &lt;&lt; V(3) &lt;&lt; endl;

  END;

  return 0;
}
</pre></div>  </pre><p>The error and the calls sequence are easy to obtain:</p>
<div class="fragment"><pre class="fragment">
$ g++ -o error -g -I ~/src/seldon error.cpp
$ ./error                                        
ERROR!                                                     
Index out of range in Vector&lt;VectFull&gt;::operator().        
   Index should be in [0, 2], but is equal to 3.           
Aborted
$ gdb ./error
GNU gdb 6.8-debian
Copyright (C) 2008 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later &lt;http://gnu.org/licenses/gpl.html&gt;
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu"...
(gdb) run
Starting program: /tmp/error 
ERROR!
Index out of range in Vector&lt;VectFull&gt;::operator().
   Index should be in [0, 2], but is equal to 3.

Program received signal SIGABRT, Aborted.
0x00007f4b7e136fb5 in raise () from /lib/libc.so.6
(gdb) up
#1  0x00007f4b7e138bc3 in abort () from /lib/libc.so.6
(gdb) up
#2  0x000000000040308e in WrongIndex (this=0x1a83370, function=
        {static npos = 18446744073709551615, _M_dataplus = {&lt;std::allocator&lt;char&gt;&gt; = {&lt;__gnu_cxx::new_allocator&lt;char&gt;&gt; = {&lt;No data fields&gt;}, &lt;No data fields&gt;}, _M_p = 0x7fff86e3e0f0 "H?\001"}}, comment=
        {static npos = 18446744073709551615, _M_dataplus = {&lt;std::allocator&lt;char&gt;&gt; = {&lt;__gnu_cxx::new_allocator&lt;char&gt;&gt; = {&lt;No data fields&gt;}, &lt;No data fields&gt;}, _M_p = 0x7fff86e3e100 "??\001"}}) at /home/mallet/src/seldon/share/Errors.cxx:206
206	    abort();
(gdb) up
#3  0x0000000000404888 in Seldon::Vector&lt;int, Seldon::VectFull, Seldon::MallocAlloc&lt;int&gt; &gt;::operator() (this=0x7fff86e3e1c0, i=3)
    at /home/mallet/src/seldon/vector/Vector.cxx:440
440	      throw WrongIndex("Vector&lt;VectFull&gt;::operator()",
(gdb) up
#4  0x000000000040364b in main (argc=1, argv=0x7fff86e3e2d8) at error.cpp:11
11	  cout &lt;&lt; "Bad access: " &lt;&lt; V(3) &lt;&lt; endl;
</pre></div><h2>Notes</h2>
<p>Apart from exceptions, two useful macros are defined to ease the debugging activities:</p>
<p><code>ERR</code>:</p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">#define ERR(x) cout &lt;&lt; "Hermes - " #x &lt;&lt; endl</pre></div>  </pre><p><code>DISP</code>:</p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">DISP(x) cout &lt;&lt; #x ": " &lt;&lt; x &lt;&lt; endl</pre></div>  </pre><p>In a code:</p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
ERR(5);
ERR(5a);
int i = 7;
DISP(i + 1);
ERR(Last line);
</pre></div>  </pre><p>returns on screen:</p>
<pre class="fragment">Hermes - 5
Hermes - 5a
i + 1: 8
Hermes - Last line
</pre> </div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
