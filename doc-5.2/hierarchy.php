<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Class Hierarchy</h1>  </div>
</div>
<div class="contents">
This inheritance list is sorted roughly, but not completely, alphabetically:<ul>
<li><a class="el" href="classseldon_1_1__object.php">seldon::_object</a><ul>
<li><a class="el" href="classseldon_1_1_base_seldon_vector_double.php">seldon::BaseSeldonVectorDouble</a><ul>
<li><a class="el" href="classseldon_1_1_vector_double.php">seldon::VectorDouble</a><ul>
<li><a class="el" href="classseldon_1_1_vector_sparse_double.php">seldon::VectorSparseDouble</a></li>
</ul>
</li>
</ul>
</li>
<li><a class="el" href="classseldon_1_1_base_seldon_vector_int.php">seldon::BaseSeldonVectorInt</a><ul>
<li><a class="el" href="classseldon_1_1_vector_int.php">seldon::VectorInt</a></li>
</ul>
</li>
<li><a class="el" href="classseldon_1_1_col_major_collection.php">seldon::ColMajorCollection</a></li>
<li><a class="el" href="classseldon_1_1_col_sym_packed_collection.php">seldon::ColSymPackedCollection</a></li>
<li><a class="el" href="classseldon_1_1_col_up_triang_packed_collection.php">seldon::ColUpTriangPackedCollection</a></li>
<li><a class="el" href="classseldon_1_1_double_malloc.php">seldon::DoubleMalloc</a></li>
<li><a class="el" href="classseldon_1_1_error.php">seldon::Error</a><ul>
<li><a class="el" href="classseldon_1_1_i_o_error.php">seldon::IOError</a></li>
<li><a class="el" href="classseldon_1_1_lapack_error.php">seldon::LapackError</a></li>
<li><a class="el" href="classseldon_1_1_no_memory.php">seldon::NoMemory</a></li>
<li><a class="el" href="classseldon_1_1_undefined.php">seldon::Undefined</a></li>
<li><a class="el" href="classseldon_1_1_wrong_argument.php">seldon::WrongArgument</a></li>
<li><a class="el" href="classseldon_1_1_wrong_col.php">seldon::WrongCol</a></li>
<li><a class="el" href="classseldon_1_1_wrong_dim.php">seldon::WrongDim</a></li>
<li><a class="el" href="classseldon_1_1_wrong_index.php">seldon::WrongIndex</a></li>
<li><a class="el" href="classseldon_1_1_wrong_row.php">seldon::WrongRow</a></li>
</ul>
</li>
<li><a class="el" href="classseldon_1_1_float_double.php">seldon::FloatDouble</a></li>
<li><a class="el" href="classseldon_1_1_general.php">seldon::General</a></li>
<li><a class="el" href="classseldon_1_1_int_malloc.php">seldon::IntMalloc</a></li>
<li><a class="el" href="classseldon_1_1ios__base.php">seldon::ios_base</a><ul>
<li><a class="el" href="classseldon_1_1ios.php">seldon::ios</a><ul>
<li><a class="el" href="classseldon_1_1istream.php">seldon::istream</a><ul>
<li><a class="el" href="classseldon_1_1ifstream.php">seldon::ifstream</a></li>
<li><a class="el" href="classseldon_1_1iostream.php">seldon::iostream</a></li>
</ul>
</li>
<li><a class="el" href="classseldon_1_1ostream.php">seldon::ostream</a><ul>
<li><a class="el" href="classseldon_1_1iostream.php">seldon::iostream</a></li>
<li><a class="el" href="classseldon_1_1ofstream.php">seldon::ofstream</a></li>
</ul>
</li>
</ul>
</li>
</ul>
</li>
<li><a class="el" href="classseldon_1_1_lapack_info.php">seldon::LapackInfo</a></li>
<li><a class="el" href="classseldon_1_1_matrix_base_double.php">seldon::MatrixBaseDouble</a><ul>
<li><a class="el" href="classseldon_1_1_base_matrix_sparse_double.php">seldon::BaseMatrixSparseDouble</a><ul>
<li><a class="el" href="classseldon_1_1_matrix_sparse_double.php">seldon::MatrixSparseDouble</a></li>
</ul>
</li>
<li><a class="el" href="classseldon_1_1_matrix_pointers_double.php">seldon::MatrixPointersDouble</a><ul>
<li><a class="el" href="classseldon_1_1_matrix_double.php">seldon::MatrixDouble</a></li>
</ul>
</li>
</ul>
</li>
<li><a class="el" href="classseldon_1_1_matrix_base_int.php">seldon::MatrixBaseInt</a><ul>
<li><a class="el" href="classseldon_1_1_matrix_pointers_int.php">seldon::MatrixPointersInt</a><ul>
<li><a class="el" href="classseldon_1_1_matrix_int.php">seldon::MatrixInt</a></li>
</ul>
</li>
</ul>
</li>
<li><a class="el" href="classseldon_1_1_row_major.php">seldon::RowMajor</a></li>
<li><a class="el" href="classseldon_1_1_row_major_collection.php">seldon::RowMajorCollection</a></li>
<li><a class="el" href="classseldon_1_1_row_sparse.php">seldon::RowSparse</a></li>
<li><a class="el" href="classseldon_1_1_row_sym_packed_collection.php">seldon::RowSymPackedCollection</a></li>
<li><a class="el" href="classseldon_1_1_row_up_triang_packed_collection.php">seldon::RowUpTriangPackedCollection</a></li>
<li><a class="el" href="classseldon_1_1_str.php">seldon::Str</a></li>
<li><a class="el" href="classseldon_1_1string.php">seldon::string</a></li>
<li><a class="el" href="classseldon_1_1_swig_py_iterator.php">seldon::SwigPyIterator</a></li>
<li><a class="el" href="classseldon_1_1_symmetric.php">seldon::Symmetric</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_calloc_alloc.php">Seldon::CallocAlloc&lt; T &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_col_complex_sparse.php">Seldon::ColComplexSparse</a><ul>
<li><a class="el" href="class_seldon_1_1_array_col_complex_sparse.php">Seldon::ArrayColComplexSparse</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_col_herm.php">Seldon::ColHerm</a></li>
<li><a class="el" href="class_seldon_1_1_col_herm_packed.php">Seldon::ColHermPacked</a></li>
<li><a class="el" href="class_seldon_1_1_collection.php">Seldon::Collection</a></li>
<li><a class="el" href="class_seldon_1_1_col_lo_triang.php">Seldon::ColLoTriang</a></li>
<li><a class="el" href="class_seldon_1_1_col_lo_triang_packed.php">Seldon::ColLoTriangPacked</a></li>
<li><a class="el" href="class_seldon_1_1_col_major.php">Seldon::ColMajor</a></li>
<li><a class="el" href="class_seldon_1_1_col_major_collection.php">Seldon::ColMajorCollection</a></li>
<li><a class="el" href="class_seldon_1_1_col_sparse.php">Seldon::ColSparse</a><ul>
<li><a class="el" href="class_seldon_1_1_array_col_sparse.php">Seldon::ArrayColSparse</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_col_sym.php">Seldon::ColSym</a></li>
<li><a class="el" href="class_seldon_1_1_col_sym_complex_sparse.php">Seldon::ColSymComplexSparse</a><ul>
<li><a class="el" href="class_seldon_1_1_array_col_sym_complex_sparse.php">Seldon::ArrayColSymComplexSparse</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_col_sym_packed.php">Seldon::ColSymPacked</a></li>
<li><a class="el" href="class_seldon_1_1_col_sym_packed_collection.php">Seldon::ColSymPackedCollection</a></li>
<li><a class="el" href="class_seldon_1_1_col_sym_sparse.php">Seldon::ColSymSparse</a><ul>
<li><a class="el" href="class_seldon_1_1_array_col_sym_sparse.php">Seldon::ArrayColSymSparse</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_col_up_triang.php">Seldon::ColUpTriang</a></li>
<li><a class="el" href="class_seldon_1_1_col_up_triang_packed.php">Seldon::ColUpTriangPacked</a></li>
<li><a class="el" href="class_seldon_1_1_col_up_triang_packed_collection.php">Seldon::ColUpTriangPackedCollection</a></li>
<li><a class="el" href="class_seldon_1_1_dense_sparse_collection.php">Seldon::DenseSparseCollection</a></li>
<li><a class="el" href="class_seldon_1_1_error.php">Seldon::Error</a><ul>
<li><a class="el" href="class_seldon_1_1_i_o_error.php">Seldon::IOError</a></li>
<li><a class="el" href="class_seldon_1_1_lapack_error.php">Seldon::LapackError</a></li>
<li><a class="el" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a></li>
<li><a class="el" href="class_seldon_1_1_undefined.php">Seldon::Undefined</a></li>
<li><a class="el" href="class_seldon_1_1_wrong_argument.php">Seldon::WrongArgument</a></li>
<li><a class="el" href="class_seldon_1_1_wrong_col.php">Seldon::WrongCol</a></li>
<li><a class="el" href="class_seldon_1_1_wrong_dim.php">Seldon::WrongDim</a></li>
<li><a class="el" href="class_seldon_1_1_wrong_index.php">Seldon::WrongIndex</a></li>
<li><a class="el" href="class_seldon_1_1_wrong_row.php">Seldon::WrongRow</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_float_double.php">Seldon::FloatDouble</a></li>
<li><a class="el" href="class_seldon_1_1_general.php">Seldon::General</a></li>
<li><a class="el" href="class_seldon_1_1_ilut_preconditioning.php">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_lapack_info.php">Seldon::LapackInfo</a></li>
<li><a class="el" href="class_seldon_1_1_malloc_alloc.php">Seldon::MallocAlloc&lt; T &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_malloc_object.php">Seldon::MallocObject&lt; T &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix&lt; T, Prop, Storage, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix&lt; double_dense_m, General, RowMajorCollection, NewAlloc&lt; double_dense_m &gt; &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix&lt; double_sparse_m, General, RowMajorCollection, NewAlloc&lt; double_sparse_m &gt; &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix&lt; float_dense_m, General, RowMajorCollection, NewAlloc&lt; float_dense_m &gt; &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix&lt; float_sparse_m, General, RowMajorCollection, NewAlloc&lt; float_sparse_m &gt; &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix&lt; int, General, RowMajor, CallocAlloc&lt; int &gt; &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix&lt; M::value_type, M::property, SubStorage&lt; M &gt;, M::allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_sub_matrix.php">Seldon::SubMatrix&lt; M &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix&lt; matrix_type, Prop, ColMajor, NewAlloc&lt; T &gt; &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix&lt; matrix_type, Prop, ColSymPacked, NewAlloc&lt; T &gt; &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix&lt; matrix_type, Prop, RowMajor, NewAlloc&lt; T &gt; &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix&lt; matrix_type, Prop, RowSymPacked, NewAlloc&lt; T &gt; &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayColComplexSparse, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayColComplexSparse, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayColSparse, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayColSparse, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayColSymSparse, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayColSymSparse, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, ColComplexSparse, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColComplexSparse, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, RowComplexSparse, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowComplexSparse, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___hermitian.php">Seldon::Matrix_Hermitian&lt; T, Prop, ColHerm, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColHerm, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___hermitian.php">Seldon::Matrix_Hermitian&lt; T, Prop, RowHerm, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowHerm, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, RowHermPacked, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowHermPacked, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, ColMajor, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColMajor, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, RowMajor, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, ColSparse, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSparse, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, RowSparse, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSparse, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, ColSymComplexSparse, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSymComplexSparse, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, RowSym, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSym, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, ColSymPacked, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSymPacked, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, RowSymPacked, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymPacked, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, ColSymSparse, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSymSparse, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymSparse, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, ColLoTriangPacked, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColLoTriangPacked, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, ColUpTriangPacked, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColUpTriangPacked, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, RowUpTriangPacked, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowUpTriangPacked, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, ColLoTriang, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColLoTriang, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, ColUpTriang, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColUpTriang, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowLoTriang, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowLoTriang, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowUpTriang, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_collection_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColMajorCollection, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_packed_collection_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSymPackedCollection, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_collection_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowMajorCollection, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_collection_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_dense_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIDense, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_seq_dense_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScSeqDense, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix___hermitian.php">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix___triang_packed.php">Seldon::Matrix_TriangPacked&lt; T, Prop, Storage, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, Storage, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_sub_matrix___base.php">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_sub_storage_3_01_m_01_4_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, SubStorage&lt; M &gt;, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_sub_matrix.php">Seldon::SubMatrix&lt; M &gt;</a></li>
</ul>
</li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_3_01_float_double_00_01_general_00_01_dense_sparse_collection_00_01_allocator_3_01double_01_4_01_4.php">Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix_cholmod.php">Seldon::MatrixCholmod</a></li>
<li><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; T &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_super_l_u.php">Seldon::MatrixSuperLU&lt; T &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_super_l_u_3_01complex_3_01double_01_4_01_4.php">Seldon::MatrixSuperLU&lt; complex&lt; double &gt; &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; double &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_super_l_u_3_01double_01_4.php">Seldon::MatrixSuperLU&lt; double &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix_umf_pack___base.php">Seldon::MatrixUmfPack_Base&lt; T &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_umf_pack.php">Seldon::MatrixUmfPack&lt; T &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix_umf_pack___base.php">Seldon::MatrixUmfPack_Base&lt; complex&lt; double &gt; &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php">Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_matrix_umf_pack___base.php">Seldon::MatrixUmfPack_Base&lt; double &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php">Seldon::MatrixUmfPack&lt; double &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_na_n_alloc.php">Seldon::NaNAlloc&lt; T &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_new_alloc.php">Seldon::NewAlloc&lt; T &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_n_lopt_solver.php">Seldon::NLoptSolver</a></li>
<li><a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_a_i_j.php">Seldon::PETScMPIAIJ</a></li>
<li><a class="el" href="class_seldon_1_1_p_e_t_sc_m_p_i_dense.php">Seldon::PETScMPIDense</a></li>
<li><a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">Seldon::PETScPar</a></li>
<li><a class="el" href="class_seldon_1_1_p_e_t_sc_seq.php">Seldon::PETScSeq</a></li>
<li><a class="el" href="class_seldon_1_1_p_e_t_sc_seq_dense.php">Seldon::PETScSeqDense</a></li>
<li><a class="el" href="class_seldon_1_1_preconditioner___base.php">Seldon::Preconditioner_Base</a></li>
<li><a class="el" href="class_seldon_1_1_row_complex_sparse.php">Seldon::RowComplexSparse</a><ul>
<li><a class="el" href="class_seldon_1_1_array_row_complex_sparse.php">Seldon::ArrayRowComplexSparse</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_row_herm.php">Seldon::RowHerm</a></li>
<li><a class="el" href="class_seldon_1_1_row_herm_packed.php">Seldon::RowHermPacked</a></li>
<li><a class="el" href="class_seldon_1_1_row_lo_triang.php">Seldon::RowLoTriang</a></li>
<li><a class="el" href="class_seldon_1_1_row_lo_triang_packed.php">Seldon::RowLoTriangPacked</a></li>
<li><a class="el" href="class_seldon_1_1_row_major.php">Seldon::RowMajor</a></li>
<li><a class="el" href="class_seldon_1_1_row_major_collection.php">Seldon::RowMajorCollection</a></li>
<li><a class="el" href="class_seldon_1_1_row_sparse.php">Seldon::RowSparse</a><ul>
<li><a class="el" href="class_seldon_1_1_array_row_sparse.php">Seldon::ArrayRowSparse</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_row_sym.php">Seldon::RowSym</a></li>
<li><a class="el" href="class_seldon_1_1_row_sym_complex_sparse.php">Seldon::RowSymComplexSparse</a><ul>
<li><a class="el" href="class_seldon_1_1_array_row_sym_complex_sparse.php">Seldon::ArrayRowSymComplexSparse</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_row_sym_packed.php">Seldon::RowSymPacked</a></li>
<li><a class="el" href="class_seldon_1_1_row_sym_packed_collection.php">Seldon::RowSymPackedCollection</a></li>
<li><a class="el" href="class_seldon_1_1_row_sym_sparse.php">Seldon::RowSymSparse</a><ul>
<li><a class="el" href="class_seldon_1_1_array_row_sym_sparse.php">Seldon::ArrayRowSymSparse</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_row_up_triang.php">Seldon::RowUpTriang</a></li>
<li><a class="el" href="class_seldon_1_1_row_up_triang_packed.php">Seldon::RowUpTriangPacked</a></li>
<li><a class="el" href="class_seldon_1_1_row_up_triang_packed_collection.php">Seldon::RowUpTriangPackedCollection</a></li>
<li><a class="el" href="class_seldon_1_1_seldon_conjugate.php">Seldon::SeldonConjugate</a></li>
<li><a class="el" href="class_seldon_1_1_seldon_diag.php">Seldon::SeldonDiag</a><ul>
<li><a class="el" href="class_seldon_1_1class___seldon_non_unit.php">Seldon::class_SeldonNonUnit</a></li>
<li><a class="el" href="class_seldon_1_1class___seldon_unit.php">Seldon::class_SeldonUnit</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_seldon_norm.php">Seldon::SeldonNorm</a></li>
<li><a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a></li>
<li><a class="el" href="class_seldon_1_1_seldon_side.php">Seldon::SeldonSide</a><ul>
<li><a class="el" href="class_seldon_1_1class___seldon_left.php">Seldon::class_SeldonLeft</a></li>
<li><a class="el" href="class_seldon_1_1class___seldon_right.php">Seldon::class_SeldonRight</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a><ul>
<li><a class="el" href="class_seldon_1_1class___seldon_conj_trans.php">Seldon::class_SeldonConjTrans</a></li>
<li><a class="el" href="class_seldon_1_1class___seldon_no_trans.php">Seldon::class_SeldonNoTrans</a></li>
<li><a class="el" href="class_seldon_1_1class___seldon_trans.php">Seldon::class_SeldonTrans</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_seldon_uplo.php">Seldon::SeldonUplo</a></li>
<li><a class="el" href="class_seldon_1_1_sor_preconditioner.php">Seldon::SorPreconditioner&lt; T &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_sparse_cholesky_solver.php">Seldon::SparseCholeskySolver&lt; T &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_sparse_direct_solver.php">Seldon::SparseDirectSolver&lt; T &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_sparse_matrix_ordering.php">Seldon::SparseMatrixOrdering</a></li>
<li><a class="el" href="class_seldon_1_1_sparse_seldon_solver.php">Seldon::SparseSeldonSolver&lt; T, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_str.php">Seldon::Str</a></li>
<li><a class="el" href="class_seldon_1_1_sub_storage.php">Seldon::SubStorage&lt; M &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_symmetric.php">Seldon::Symmetric</a></li>
<li><a class="el" href="class_seldon_1_1_type_mumps.php">Seldon::TypeMumps&lt; T &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_type_mumps_3_01complex_3_01double_01_4_01_4.php">Seldon::TypeMumps&lt; complex&lt; double &gt; &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_type_mumps_3_01double_01_4.php">Seldon::TypeMumps&lt; double &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_vect_full.php">Seldon::VectFull</a></li>
<li><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector&lt; T, Storage, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector&lt; double &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector&lt; int &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector&lt; int, VectFull, CallocAlloc&lt; int &gt; &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector&lt; Vector&lt; T, VectSparse, Allocator &gt;, VectFull, NewAlloc&lt; Vector&lt; T, VectSparse, Allocator &gt; &gt; &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScPar, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php">Seldon::Vector&lt; T, Collection, Allocator &gt;</a></li>
<li><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></li>
</ul>
</li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator&lt; T &gt; &gt;</a><ul>
<li><a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a></li>
</ul>
</li>
<li><a class="el" href="class_seldon_1_1_vect_sparse.php">Seldon::VectSparse</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
