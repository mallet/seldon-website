<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix/MatrixCollection.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2010 INRIA</span>
<a name="l00002"></a>00002 <span class="comment">// Author(s): Marc Fragu</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_COLLECTION_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;MatrixCollection.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 
<a name="l00026"></a>00026 <span class="keyword">namespace </span>Seldon
<a name="l00027"></a>00027 {
<a name="l00028"></a>00028 
<a name="l00029"></a>00029 
<a name="l00031"></a>00031   <span class="comment">// MATRIXCOLLECTION //</span>
<a name="l00033"></a>00033 <span class="comment"></span>
<a name="l00034"></a>00034 
<a name="l00035"></a>00035   <span class="comment">/****************</span>
<a name="l00036"></a>00036 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00037"></a>00037 <span class="comment">   ****************/</span>
<a name="l00038"></a>00038 
<a name="l00039"></a>00039 
<a name="l00041"></a>00041 
<a name="l00044"></a>00044   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00045"></a>00045   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a4f5acfa96f23bdd2a9ad0f19e72a43c5" title="Default constructor.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;::MatrixCollection</a>():
<a name="l00046"></a>00046     Matrix_Base&lt;T, Allocator&gt;(), Mlocal_(), Mlocal_sum_(1),
<a name="l00047"></a>00047     Nlocal_(), Nlocal_sum_(1), matrix_()
<a name="l00048"></a>00048   {
<a name="l00049"></a>00049     nz_ = 0;
<a name="l00050"></a>00050     Mmatrix_ = 0;
<a name="l00051"></a>00051     Nmatrix_ = 0;
<a name="l00052"></a>00052     Mlocal_sum_.Fill(0);
<a name="l00053"></a>00053     Nlocal_sum_.Fill(0);
<a name="l00054"></a>00054   }
<a name="l00055"></a>00055 
<a name="l00056"></a>00056 
<a name="l00058"></a>00058 
<a name="l00062"></a>00062   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00063"></a>00063   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a4f5acfa96f23bdd2a9ad0f19e72a43c5" title="Default constructor.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00064"></a>00064 <a class="code" href="class_seldon_1_1_matrix_collection.php#a4f5acfa96f23bdd2a9ad0f19e72a43c5" title="Default constructor.">  ::MatrixCollection</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j): Matrix_Base&lt;T, Allocator&gt;(i, j),
<a name="l00065"></a>00065                                     Mlocal_(i), Mlocal_sum_(i + 1),
<a name="l00066"></a>00066                                     Nlocal_(j), Nlocal_sum_(j + 1),
<a name="l00067"></a>00067                                     matrix_(i, j)
<a name="l00068"></a>00068   {
<a name="l00069"></a>00069     nz_ = 0;
<a name="l00070"></a>00070     Mmatrix_ = i;
<a name="l00071"></a>00071     Nmatrix_ = j;
<a name="l00072"></a>00072     Mlocal_.Fill(0);
<a name="l00073"></a>00073     Nlocal_.Fill(0);
<a name="l00074"></a>00074     Mlocal_sum_.Fill(0);
<a name="l00075"></a>00075     Nlocal_sum_.Fill(0);
<a name="l00076"></a>00076   }
<a name="l00077"></a>00077 
<a name="l00078"></a>00078 
<a name="l00080"></a>00080   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00081"></a>00081   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a4f5acfa96f23bdd2a9ad0f19e72a43c5" title="Default constructor.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00082"></a>00082 <a class="code" href="class_seldon_1_1_matrix_collection.php#a4f5acfa96f23bdd2a9ad0f19e72a43c5" title="Default constructor.">  ::MatrixCollection</a>(<span class="keyword">const</span> MatrixCollection&lt;T, Prop, Storage, Allocator&gt;&amp; A)
<a name="l00083"></a>00083     : Matrix_Base&lt;T, Allocator&gt;()
<a name="l00084"></a>00084   {
<a name="l00085"></a>00085     this-&gt;Copy(A);
<a name="l00086"></a>00086   }
<a name="l00087"></a>00087 
<a name="l00088"></a>00088 
<a name="l00089"></a>00089   <span class="comment">/**************</span>
<a name="l00090"></a>00090 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00091"></a>00091 <span class="comment">   **************/</span>
<a name="l00092"></a>00092 
<a name="l00093"></a><a class="code" href="class_seldon_1_1_matrix_collection.php#acd85176ecf48145fcb40621d4e7401e8">00093</a> 
<a name="l00095"></a>00095   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00096"></a>00096   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#acd85176ecf48145fcb40621d4e7401e8" title="Destructor.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;::~MatrixCollection</a>()
<a name="l00097"></a>00097   {
<a name="l00098"></a>00098     <a class="code" href="class_seldon_1_1_matrix_collection.php#a508ae5c18005383193f2a289bf63d608" title="Clears the matrix.">Clear</a>();
<a name="l00099"></a>00099   }
<a name="l00100"></a>00100 
<a name="l00101"></a>00101 
<a name="l00103"></a>00103 
<a name="l00107"></a>00107   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00108"></a>00108   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a508ae5c18005383193f2a289bf63d608" title="Clears the matrix.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;::Clear</a>()
<a name="l00109"></a>00109   {
<a name="l00110"></a>00110     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_matrix_collection.php#a094131b6edf39309cf51ec6aea9c271b" title="Number of rows of matrices.">Mmatrix_</a>; i++)
<a name="l00111"></a>00111       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; <a class="code" href="class_seldon_1_1_matrix_collection.php#abf9ee2f169c7992c2d540d07dd85b104" title="Number of columns of matrices.">Nmatrix_</a>; j++)
<a name="l00112"></a>00112         <a class="code" href="class_seldon_1_1_matrix_collection.php#a66297d4139bb2b5b07979ef929358399" title="Pointers of the underlying matrices.">matrix_</a>(i, j).Nullify();
<a name="l00113"></a>00113 
<a name="l00114"></a>00114     <a class="code" href="class_seldon_1_1_matrix_collection.php#a66297d4139bb2b5b07979ef929358399" title="Pointers of the underlying matrices.">matrix_</a>.Clear();
<a name="l00115"></a>00115 
<a name="l00116"></a>00116     <a class="code" href="class_seldon_1_1_matrix_collection.php#a960417c35add0c178e31e8578bd5eb9b" title="Number of non-zero elements.">nz_</a> = 0;
<a name="l00117"></a>00117     Mmatrix_ = 0;
<a name="l00118"></a>00118     Nmatrix_ = 0;
<a name="l00119"></a>00119     <a class="code" href="class_seldon_1_1_matrix_collection.php#a234a716260de98d93fc40fc2a3fd44fe" title="Number of rows in the underlying matrices.">Mlocal_</a>.Clear();
<a name="l00120"></a>00120     <a class="code" href="class_seldon_1_1_matrix_collection.php#afdcab0ec07a154e7ed23ee20b2f4f574" title="Number of columns in the underlying matrices.">Nlocal_</a>.Clear();
<a name="l00121"></a>00121     <a class="code" href="class_seldon_1_1_matrix_collection.php#aa16e80513c77e58a2c1b3974e360f220" title="Cumulative number of rows in the underlying matrices.">Mlocal_sum_</a>.Clear();
<a name="l00122"></a>00122     <a class="code" href="class_seldon_1_1_matrix_collection.php#a2cd7c7804f76b6e8a6966e6597e05244" title="Cumulative number of columns in the underlying matrices.">Nlocal_sum_</a>.Clear();
<a name="l00123"></a>00123   }
<a name="l00124"></a>00124 
<a name="l00125"></a>00125 
<a name="l00127"></a>00127 
<a name="l00131"></a>00131   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00132"></a>00132   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a04f079fbfc7851ed7c1fa48a12936b9f" title="Clears the matrix.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;::Nullify</a>()
<a name="l00133"></a>00133   {
<a name="l00134"></a>00134     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Mmatrix_; i++)
<a name="l00135"></a>00135       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; Nmatrix_; j++)
<a name="l00136"></a>00136         <a class="code" href="class_seldon_1_1_matrix_collection.php#a66297d4139bb2b5b07979ef929358399" title="Pointers of the underlying matrices.">matrix_</a>(i, j).Nullify();
<a name="l00137"></a>00137 
<a name="l00138"></a>00138     <a class="code" href="class_seldon_1_1_matrix_collection.php#a960417c35add0c178e31e8578bd5eb9b" title="Number of non-zero elements.">nz_</a> = 0;
<a name="l00139"></a>00139     Mmatrix_ = 0;
<a name="l00140"></a>00140     Nmatrix_ = 0;
<a name="l00141"></a>00141     <a class="code" href="class_seldon_1_1_matrix_collection.php#a234a716260de98d93fc40fc2a3fd44fe" title="Number of rows in the underlying matrices.">Mlocal_</a>.Clear();
<a name="l00142"></a>00142     <a class="code" href="class_seldon_1_1_matrix_collection.php#afdcab0ec07a154e7ed23ee20b2f4f574" title="Number of columns in the underlying matrices.">Nlocal_</a>.Clear();
<a name="l00143"></a>00143     <a class="code" href="class_seldon_1_1_matrix_collection.php#aa16e80513c77e58a2c1b3974e360f220" title="Cumulative number of rows in the underlying matrices.">Mlocal_sum_</a>.Clear();
<a name="l00144"></a>00144     <a class="code" href="class_seldon_1_1_matrix_collection.php#a2cd7c7804f76b6e8a6966e6597e05244" title="Cumulative number of columns in the underlying matrices.">Nlocal_sum_</a>.Clear();
<a name="l00145"></a>00145   }
<a name="l00146"></a>00146 
<a name="l00147"></a>00147 
<a name="l00149"></a>00149 
<a name="l00153"></a>00153   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00154"></a>00154   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a04f079fbfc7851ed7c1fa48a12936b9f" title="Clears the matrix.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00155"></a>00155 <a class="code" href="class_seldon_1_1_matrix_collection.php#a04f079fbfc7851ed7c1fa48a12936b9f" title="Clears the matrix.">  ::Nullify</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00156"></a>00156   {
<a name="l00157"></a>00157     nz_ -= matrix_(i, j).GetDataSize();
<a name="l00158"></a>00158     matrix_(i, j).Nullify();
<a name="l00159"></a>00159   }
<a name="l00160"></a>00160 
<a name="l00161"></a><a class="code" href="class_seldon_1_1_matrix_collection.php#ad1f04e7bbca49f4b4ac310f5a914085e">00161</a> 
<a name="l00163"></a>00163   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00164"></a>00164   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#ad1f04e7bbca49f4b4ac310f5a914085e" title="Deallocates underlying the matrices.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;::Deallocate</a>()
<a name="l00165"></a>00165   {
<a name="l00166"></a>00166     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Mmatrix_; i++)
<a name="l00167"></a>00167       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; Nmatrix_; j++)
<a name="l00168"></a>00168         <a class="code" href="class_seldon_1_1_matrix_collection.php#ad18c52c1ed65330e6443e4519df390a4" title="Access to an underlying matrix.">GetMatrix</a>(i, j).Clear();
<a name="l00169"></a>00169     this-&gt;<a class="code" href="class_seldon_1_1_matrix_collection.php#acd85176ecf48145fcb40621d4e7401e8" title="Destructor.">~MatrixCollection</a>();
<a name="l00170"></a>00170   }
<a name="l00171"></a>00171 
<a name="l00172"></a>00172 
<a name="l00173"></a>00173   <span class="comment">/*******************</span>
<a name="l00174"></a>00174 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00175"></a>00175 <span class="comment">   *******************/</span>
<a name="l00176"></a>00176 
<a name="l00177"></a>00177 
<a name="l00179"></a>00179 
<a name="l00183"></a>00183   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00184"></a>00184   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7" title="Returns the number of rows.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;::GetM</a>()<span class="keyword"> const</span>
<a name="l00185"></a>00185 <span class="keyword">  </span>{
<a name="l00186"></a>00186     <span class="keywordflow">return</span> this-&gt;m_;
<a name="l00187"></a>00187   }
<a name="l00188"></a>00188 
<a name="l00189"></a>00189 
<a name="l00191"></a>00191 
<a name="l00195"></a>00195   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00196"></a>00196   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#aaa428778f995ce6bbe08f1181fbb51e4" title="Returns the number of rows.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;::GetMmatrix</a>()<span class="keyword"> const</span>
<a name="l00197"></a>00197 <span class="keyword">  </span>{
<a name="l00198"></a>00198     <span class="keywordflow">return</span> Mmatrix_;
<a name="l00199"></a>00199   }
<a name="l00200"></a>00200 
<a name="l00201"></a>00201 
<a name="l00203"></a>00203 
<a name="l00207"></a>00207   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00208"></a>00208   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7" title="Returns the number of rows.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;::GetM</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00209"></a>00209 <span class="keyword">  </span>{
<a name="l00210"></a>00210 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00211"></a>00211 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Mmatrix_)
<a name="l00212"></a>00212       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;MatrixCollection::GetM()&quot;</span>,
<a name="l00213"></a>00213                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00214"></a>00214                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Mmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00215"></a>00215                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00216"></a>00216 <span class="preprocessor">#endif</span>
<a name="l00217"></a>00217 <span class="preprocessor"></span>
<a name="l00218"></a>00218     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a234a716260de98d93fc40fc2a3fd44fe" title="Number of rows in the underlying matrices.">Mlocal_</a>(i);
<a name="l00219"></a>00219   }
<a name="l00220"></a>00220 
<a name="l00221"></a>00221 
<a name="l00223"></a>00223 
<a name="l00227"></a>00227   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00228"></a>00228   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d" title="Returns the number of columns.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;::GetN</a>()<span class="keyword"> const</span>
<a name="l00229"></a>00229 <span class="keyword">  </span>{
<a name="l00230"></a>00230     <span class="keywordflow">return</span> this-&gt;n_;
<a name="l00231"></a>00231   }
<a name="l00232"></a>00232 
<a name="l00233"></a>00233 
<a name="l00235"></a>00235 
<a name="l00239"></a>00239   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00240"></a>00240   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a4a1165a73495d9eb4edfd49ba8e1f9b7" title="Returns the number of columns.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;::GetNmatrix</a>()<span class="keyword"> const</span>
<a name="l00241"></a>00241 <span class="keyword">  </span>{
<a name="l00242"></a>00242     <span class="keywordflow">return</span> Nmatrix_;
<a name="l00243"></a>00243   }
<a name="l00244"></a>00244 
<a name="l00245"></a>00245 
<a name="l00247"></a>00247 
<a name="l00252"></a>00252   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00253"></a>00253   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d" title="Returns the number of columns.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;::GetN</a>(<span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00254"></a>00254 <span class="keyword">  </span>{
<a name="l00255"></a>00255 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00256"></a>00256 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= Nmatrix_)
<a name="l00257"></a>00257       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;MatrixCollection::GetN()&quot;</span>,
<a name="l00258"></a>00258                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00259"></a>00259                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00260"></a>00260                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00261"></a>00261 <span class="preprocessor">#endif</span>
<a name="l00262"></a>00262 <span class="preprocessor"></span>
<a name="l00263"></a>00263     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#afdcab0ec07a154e7ed23ee20b2f4f574" title="Number of columns in the underlying matrices.">Nlocal_</a>(j);
<a name="l00264"></a>00264   }
<a name="l00265"></a>00265 
<a name="l00266"></a>00266 
<a name="l00268"></a>00268 
<a name="l00271"></a>00271   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00272"></a>00272   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5" title="Returns the number of elements stored in memory.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;::GetSize</a>()<span class="keyword"> const</span>
<a name="l00273"></a>00273 <span class="keyword">  </span>{
<a name="l00274"></a>00274     <span class="keywordflow">return</span> this-&gt;m_ * this-&gt;n_;
<a name="l00275"></a>00275   }
<a name="l00276"></a>00276 
<a name="l00277"></a>00277 
<a name="l00279"></a>00279 
<a name="l00282"></a>00282   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00283"></a>00283   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a1162607a4f1255a388598a6dc5eea1f8" title="Returns the number of elements stored in memory.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;::GetDataSize</a>()<span class="keyword"> const</span>
<a name="l00284"></a>00284 <span class="keyword">  </span>{
<a name="l00285"></a>00285     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a960417c35add0c178e31e8578bd5eb9b" title="Number of non-zero elements.">nz_</a>;
<a name="l00286"></a>00286   }
<a name="l00287"></a>00287 
<a name="l00288"></a>00288 
<a name="l00289"></a>00289   <span class="comment">/*********************</span>
<a name="l00290"></a>00290 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00291"></a>00291 <span class="comment">   *********************/</span>
<a name="l00292"></a>00292 
<a name="l00293"></a>00293 
<a name="l00295"></a>00295 
<a name="l00301"></a>00301   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00302"></a>00302   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a2cafbea09c87f1798305211e30512994" title="Reallocates memory to resize the matrix collection.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00303"></a>00303 <a class="code" href="class_seldon_1_1_matrix_collection.php#a2cafbea09c87f1798305211e30512994" title="Reallocates memory to resize the matrix collection.">  ::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00304"></a>00304   {
<a name="l00305"></a>00305     nz_ = 0;
<a name="l00306"></a>00306     Mmatrix_ = i;
<a name="l00307"></a>00307     Nmatrix_ = j;
<a name="l00308"></a>00308     Mlocal_.Reallocate(i);
<a name="l00309"></a>00309     Nlocal_.Reallocate(j);
<a name="l00310"></a>00310     Mlocal_sum_.Reallocate(i + 1);
<a name="l00311"></a>00311     Nlocal_sum_.Reallocate(j + 1);
<a name="l00312"></a>00312     Mlocal_.Fill(0.);
<a name="l00313"></a>00313     Nlocal_.Fill(0.);
<a name="l00314"></a>00314     Mlocal_sum_.Fill(0.);
<a name="l00315"></a>00315     Nlocal_sum_.Fill(0.);
<a name="l00316"></a>00316     matrix_.Reallocate(i, j);
<a name="l00317"></a>00317   }
<a name="l00318"></a>00318 
<a name="l00319"></a>00319 
<a name="l00321"></a>00321 
<a name="l00326"></a><a class="code" href="class_seldon_1_1_matrix_collection.php#a42828d0fb580934560080b2cf15eaa78">00326</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00327"></a>00327   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Prop0, <span class="keyword">class</span> Storage0, <span class="keyword">class</span> Allocator0&gt;
<a name="l00328"></a>00328   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a42828d0fb580934560080b2cf15eaa78" title="Sets an underlying matrix in the matrix collection.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00329"></a>00329 <a class="code" href="class_seldon_1_1_matrix_collection.php#a42828d0fb580934560080b2cf15eaa78" title="Sets an underlying matrix in the matrix collection.">  ::SetMatrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;</a>&amp; A)
<a name="l00330"></a>00330   {
<a name="l00331"></a>00331 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00332"></a>00332 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Mmatrix_)
<a name="l00333"></a>00333       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;MatrixCollection::SetMatrix()&quot;</span>,
<a name="l00334"></a>00334                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Line index should be in [0, &quot;</span>)
<a name="l00335"></a>00335                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Mmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00336"></a>00336                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00337"></a>00337     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= Nmatrix_)
<a name="l00338"></a>00338       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;MatrixCollection::SetMatrix()&quot;</span>,
<a name="l00339"></a>00339                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Column index should be in [0, &quot;</span>)
<a name="l00340"></a>00340                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00341"></a>00341                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00342"></a>00342     <span class="keywordflow">if</span> ((Mlocal_(i) != 0) &amp;&amp; (Mlocal_(i) != A.GetM()))
<a name="l00343"></a>00343       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;MatrixCollection::SetMatrix()&quot;</span>,
<a name="l00344"></a>00344                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The matrix expected should have &quot;</span>)
<a name="l00345"></a>00345                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;Mlocal_(i)) + <span class="stringliteral">&quot; rows, but has &quot;</span>
<a name="l00346"></a>00346                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetM()) + <span class="stringliteral">&quot; rows.&quot;</span>);
<a name="l00347"></a>00347     <span class="keywordflow">if</span> ((Nlocal_(j) != 0) &amp;&amp; (Nlocal_(j) != A.GetN()))
<a name="l00348"></a>00348       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;MatrixCollection::SetMatrix()&quot;</span>,
<a name="l00349"></a>00349                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The matrix expected should have &quot;</span>)
<a name="l00350"></a>00350                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;Nlocal_(j)) + <span class="stringliteral">&quot; columns, but has &quot;</span>
<a name="l00351"></a>00351                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetN()) + <span class="stringliteral">&quot; columns.&quot;</span>);
<a name="l00352"></a>00352 <span class="preprocessor">#endif</span>
<a name="l00353"></a>00353 <span class="preprocessor"></span>
<a name="l00354"></a>00354     nz_ = A.GetDataSize() - matrix_(i, j).GetDataSize();
<a name="l00355"></a>00355 
<a name="l00356"></a>00356     <span class="keywordtype">int</span> Mdiff = A.GetM() - Mlocal_(i);
<a name="l00357"></a>00357     <span class="keywordtype">int</span> Ndiff = A.GetN() - Nlocal_(j);
<a name="l00358"></a>00358 
<a name="l00359"></a>00359     Mlocal_(i) = A.GetM();
<a name="l00360"></a>00360     Nlocal_(j) = A.GetN();
<a name="l00361"></a>00361 
<a name="l00362"></a>00362     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = i + 1; k &lt; Mmatrix_ + 1; k++)
<a name="l00363"></a>00363       Mlocal_sum_(k) += Mdiff;
<a name="l00364"></a>00364 
<a name="l00365"></a>00365     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = j + 1; k &lt; Nmatrix_ + 1; k++)
<a name="l00366"></a>00366       Nlocal_sum_(k) += Ndiff;
<a name="l00367"></a>00367 
<a name="l00368"></a>00368     this-&gt;m_ = Mlocal_sum_(Mmatrix_);
<a name="l00369"></a>00369     this-&gt;n_ = Nlocal_sum_(Nmatrix_);
<a name="l00370"></a>00370 
<a name="l00371"></a>00371     matrix_(i, j).SetData(A.GetM(), A.GetN(), A.GetData());
<a name="l00372"></a>00372   }
<a name="l00373"></a>00373 
<a name="l00374"></a>00374 
<a name="l00376"></a>00376 
<a name="l00381"></a><a class="code" href="class_seldon_1_1_matrix_collection.php#a4960e099b8a36df7b772a6a9a7261cda">00381</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00382"></a>00382   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Prop0, <span class="keyword">class</span> Allocator0&gt;
<a name="l00383"></a>00383   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a42828d0fb580934560080b2cf15eaa78" title="Sets an underlying matrix in the matrix collection.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00384"></a>00384 <a class="code" href="class_seldon_1_1_matrix_collection.php#a42828d0fb580934560080b2cf15eaa78" title="Sets an underlying matrix in the matrix collection.">  ::SetMatrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, RowSparse, Allocator0&gt;</a>&amp; A)
<a name="l00385"></a>00385   {
<a name="l00386"></a>00386 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00387"></a>00387 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Mmatrix_)
<a name="l00388"></a>00388       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;MatrixCollection::SetMatrix()&quot;</span>,
<a name="l00389"></a>00389                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00390"></a>00390                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Mmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00391"></a>00391                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00392"></a>00392     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= Nmatrix_)
<a name="l00393"></a>00393       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;MatrixCollection::SetMatrix()&quot;</span>,
<a name="l00394"></a>00394                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00395"></a>00395                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00396"></a>00396                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00397"></a>00397     <span class="keywordflow">if</span> ((Mlocal_(i) != 0) &amp;&amp; (Mlocal_(i) != A.GetM()))
<a name="l00398"></a>00398       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;MatrixCollection::SetMatrix()&quot;</span>,
<a name="l00399"></a>00399                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The matrix expected should have &quot;</span>)
<a name="l00400"></a>00400                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;Mlocal_(i)) + <span class="stringliteral">&quot; rows, but has &quot;</span>
<a name="l00401"></a>00401                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetM()) + <span class="stringliteral">&quot; rows.&quot;</span>);
<a name="l00402"></a>00402     <span class="keywordflow">if</span> ((Nlocal_(j) != 0) &amp;&amp; (Nlocal_(j) != A.GetN()))
<a name="l00403"></a>00403       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;MatrixCollection::SetMatrix()&quot;</span>,
<a name="l00404"></a>00404                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The matrix expected should have &quot;</span>)
<a name="l00405"></a>00405                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;Nlocal_(j)) + <span class="stringliteral">&quot; columns, but has &quot;</span>
<a name="l00406"></a>00406                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetN()) + <span class="stringliteral">&quot; columns.&quot;</span>);
<a name="l00407"></a>00407 <span class="preprocessor">#endif</span>
<a name="l00408"></a>00408 <span class="preprocessor"></span>
<a name="l00409"></a>00409     nz_ = A.GetDataSize() - matrix_(i, j).GetDataSize();
<a name="l00410"></a>00410 
<a name="l00411"></a>00411     <span class="keywordtype">int</span> Mdiff = A.GetM() - Mlocal_(i);
<a name="l00412"></a>00412     <span class="keywordtype">int</span> Ndiff = A.GetN() - Nlocal_(j);
<a name="l00413"></a>00413 
<a name="l00414"></a>00414     Mlocal_(i) = A.GetM();
<a name="l00415"></a>00415     Nlocal_(j) = A.GetN();
<a name="l00416"></a>00416 
<a name="l00417"></a>00417     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = i + 1; k &lt; Mmatrix_ + 1; k++)
<a name="l00418"></a>00418       Mlocal_sum_(k) += Mdiff;
<a name="l00419"></a>00419 
<a name="l00420"></a>00420     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = j + 1; k &lt; Nmatrix_ + 1; k++)
<a name="l00421"></a>00421       Nlocal_sum_(k) += Ndiff;
<a name="l00422"></a>00422 
<a name="l00423"></a>00423     this-&gt;m_ = Mlocal_sum_(Mmatrix_);
<a name="l00424"></a>00424     this-&gt;n_ = Nlocal_sum_(Nmatrix_);
<a name="l00425"></a>00425 
<a name="l00426"></a>00426     matrix_(i, j).SetData(A.GetM(), A.GetN(), A.GetNonZeros(), A.GetData(),
<a name="l00427"></a>00427                           A.GetPtr(), A.GetInd());
<a name="l00428"></a>00428   }
<a name="l00429"></a>00429 
<a name="l00430"></a>00430 
<a name="l00431"></a>00431   <span class="comment">/**********************************</span>
<a name="l00432"></a>00432 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l00433"></a>00433 <span class="comment">   **********************************/</span>
<a name="l00434"></a>00434 
<a name="l00435"></a>00435 
<a name="l00437"></a>00437 
<a name="l00443"></a><a class="code" href="class_seldon_1_1_matrix_collection.php#ad18c52c1ed65330e6443e4519df390a4">00443</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00444"></a>00444   <span class="keyword">inline</span>
<a name="l00445"></a>00445   <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix_collection.php" title="Matrix class made of a collection of matrices.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;::matrix_reference</a>
<a name="l00446"></a>00446   <a class="code" href="class_seldon_1_1_matrix_collection.php#ad18c52c1ed65330e6443e4519df390a4" title="Access to an underlying matrix.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;::GetMatrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00447"></a>00447   {
<a name="l00448"></a>00448 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00449"></a>00449 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Mmatrix_)
<a name="l00450"></a>00450       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;MatrixCollection::GetMatrix(int, int)&quot;</span>,
<a name="l00451"></a>00451                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Row index should be in [0, &quot;</span>)
<a name="l00452"></a>00452                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Mmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00453"></a>00453                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00454"></a>00454     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= Nmatrix_)
<a name="l00455"></a>00455       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;MatrixCollection::GetMatrix(int, int)&quot;</span>,
<a name="l00456"></a>00456                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Column index should be in [0, &quot;</span>)
<a name="l00457"></a>00457                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00458"></a>00458                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00459"></a>00459 <span class="preprocessor">#endif</span>
<a name="l00460"></a>00460 <span class="preprocessor"></span>
<a name="l00461"></a>00461     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a66297d4139bb2b5b07979ef929358399" title="Pointers of the underlying matrices.">matrix_</a>(i, j);
<a name="l00462"></a>00462   }
<a name="l00463"></a>00463 
<a name="l00464"></a>00464 
<a name="l00466"></a>00466 
<a name="l00472"></a><a class="code" href="class_seldon_1_1_matrix_collection.php#ad4fcca36d06203d8324a1fed15b12a65">00472</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00473"></a>00473   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix_collection.php" title="Matrix class made of a collection of matrices.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00474"></a>00474   ::const_matrix_reference
<a name="l00475"></a>00475   <a class="code" href="class_seldon_1_1_matrix_collection.php#ad18c52c1ed65330e6443e4519df390a4" title="Access to an underlying matrix.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;::GetMatrix</a>(<span class="keywordtype">int</span> i,
<a name="l00476"></a>00476                                                            <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00477"></a>00477 <span class="keyword">  </span>{
<a name="l00478"></a>00478 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00479"></a>00479 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Mmatrix_)
<a name="l00480"></a>00480       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;MatrixCollection::GetMatrix(int, int)&quot;</span>,
<a name="l00481"></a>00481                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Row index should be in [0, &quot;</span>)
<a name="l00482"></a>00482                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Mmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00483"></a>00483                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00484"></a>00484     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= Nmatrix_)
<a name="l00485"></a>00485       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;MatrixCollection::GetMatrix(int, int)&quot;</span>,
<a name="l00486"></a>00486                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Column index should be in [0, &quot;</span>)
<a name="l00487"></a>00487                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00488"></a>00488                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00489"></a>00489 <span class="preprocessor">#endif</span>
<a name="l00490"></a>00490 <span class="preprocessor"></span>
<a name="l00491"></a>00491     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a66297d4139bb2b5b07979ef929358399" title="Pointers of the underlying matrices.">matrix_</a>(i, j);
<a name="l00492"></a>00492   }
<a name="l00493"></a>00493 
<a name="l00494"></a>00494 
<a name="l00496"></a>00496 
<a name="l00502"></a><a class="code" href="class_seldon_1_1_matrix_collection.php#a774399178dd07c6e598e6051e8f89009">00502</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00503"></a>00503   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix_collection.php" title="Matrix class made of a collection of matrices.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00504"></a>00504   ::value_type
<a name="l00505"></a>00505   <a class="code" href="class_seldon_1_1_matrix_collection.php#a774399178dd07c6e598e6051e8f89009" title="Access operator.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i,
<a name="l00506"></a>00506                                                              <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00507"></a>00507 <span class="keyword">  </span>{
<a name="l00508"></a>00508 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00509"></a>00509 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix_collection.php#aa16e80513c77e58a2c1b3974e360f220" title="Cumulative number of rows in the underlying matrices.">Mlocal_sum_</a>(Mmatrix_))
<a name="l00510"></a>00510       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;MatrixCollection::operator(int, int)&quot;</span>,
<a name="l00511"></a>00511                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Row index should be in [0, &quot;</span>)
<a name="l00512"></a>00512                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix_collection.php#aa16e80513c77e58a2c1b3974e360f220" title="Cumulative number of rows in the underlying matrices.">Mlocal_sum_</a>(Mmatrix_) - 1)
<a name="l00513"></a>00513                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00514"></a>00514     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix_collection.php#a2cd7c7804f76b6e8a6966e6597e05244" title="Cumulative number of columns in the underlying matrices.">Nlocal_sum_</a>(Nmatrix_))
<a name="l00515"></a>00515       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;MatrixCollection::operator(int, int)&quot;</span>,
<a name="l00516"></a>00516                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Column index should be in [0, &quot;</span>)
<a name="l00517"></a>00517                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix_collection.php#a2cd7c7804f76b6e8a6966e6597e05244" title="Cumulative number of columns in the underlying matrices.">Nlocal_sum_</a>(Nmatrix_) - 1)
<a name="l00518"></a>00518                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00519"></a>00519 <span class="preprocessor">#endif</span>
<a name="l00520"></a>00520 <span class="preprocessor"></span>
<a name="l00521"></a>00521     <span class="keywordtype">int</span> i_global = 0;
<a name="l00522"></a>00522     <span class="keywordflow">while</span> (i &gt;= <a class="code" href="class_seldon_1_1_matrix_collection.php#aa16e80513c77e58a2c1b3974e360f220" title="Cumulative number of rows in the underlying matrices.">Mlocal_sum_</a>(i_global))
<a name="l00523"></a>00523       i_global++;
<a name="l00524"></a>00524     i_global--;
<a name="l00525"></a>00525 
<a name="l00526"></a>00526     <span class="keywordtype">int</span> j_global = 0;
<a name="l00527"></a>00527     <span class="keywordflow">while</span> (j &gt;= <a class="code" href="class_seldon_1_1_matrix_collection.php#a2cd7c7804f76b6e8a6966e6597e05244" title="Cumulative number of columns in the underlying matrices.">Nlocal_sum_</a>(j_global))
<a name="l00528"></a>00528       j_global++;
<a name="l00529"></a>00529     j_global--;
<a name="l00530"></a>00530 
<a name="l00531"></a>00531     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a66297d4139bb2b5b07979ef929358399" title="Pointers of the underlying matrices.">matrix_</a>(i_global, j_global)(i - <a class="code" href="class_seldon_1_1_matrix_collection.php#aa16e80513c77e58a2c1b3974e360f220" title="Cumulative number of rows in the underlying matrices.">Mlocal_sum_</a>(i_global),
<a name="l00532"></a>00532                                        j - <a class="code" href="class_seldon_1_1_matrix_collection.php#a2cd7c7804f76b6e8a6966e6597e05244" title="Cumulative number of columns in the underlying matrices.">Nlocal_sum_</a>(j_global));
<a name="l00533"></a>00533   }
<a name="l00534"></a>00534 
<a name="l00535"></a>00535 
<a name="l00537"></a>00537 
<a name="l00542"></a><a class="code" href="class_seldon_1_1_matrix_collection.php#a9e3b633e9622c3a86561f8b08e9ff8e8">00542</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00543"></a>00543   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_collection.php" title="Matrix class made of a collection of matrices.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00544"></a>00544   <a class="code" href="class_seldon_1_1_matrix_collection.php#a9e3b633e9622c3a86561f8b08e9ff8e8" title="Duplicates a matrix collection (assignment operator).">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00545"></a>00545 <a class="code" href="class_seldon_1_1_matrix_collection.php#a9e3b633e9622c3a86561f8b08e9ff8e8" title="Duplicates a matrix collection (assignment operator).">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_collection.php" title="Matrix class made of a collection of matrices.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00546"></a>00546   {
<a name="l00547"></a>00547     this-&gt;Copy(A);
<a name="l00548"></a>00548     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00549"></a>00549   }
<a name="l00550"></a>00550 
<a name="l00551"></a>00551 
<a name="l00553"></a>00553 
<a name="l00558"></a>00558   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00559"></a>00559   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a0654c385392b3f0634a8721a797e249a" title="Duplicates a matrix collection.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00560"></a>00560 <a class="code" href="class_seldon_1_1_matrix_collection.php#a0654c385392b3f0634a8721a797e249a" title="Duplicates a matrix collection.">  ::Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_collection.php" title="Matrix class made of a collection of matrices.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00561"></a>00561   {
<a name="l00562"></a>00562     Clear();
<a name="l00563"></a>00563 
<a name="l00564"></a>00564     this-&gt;nz_ = A.<a class="code" href="class_seldon_1_1_matrix_collection.php#a960417c35add0c178e31e8578bd5eb9b" title="Number of non-zero elements.">nz_</a>;
<a name="l00565"></a>00565     this-&gt;m_ = A.<a class="code" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7" title="Returns the number of rows.">GetM</a>();
<a name="l00566"></a>00566     this-&gt;n_ = A.<a class="code" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d" title="Returns the number of columns.">GetN</a>();
<a name="l00567"></a>00567     Mmatrix_ = A.<a class="code" href="class_seldon_1_1_matrix_collection.php#a094131b6edf39309cf51ec6aea9c271b" title="Number of rows of matrices.">Mmatrix_</a>;
<a name="l00568"></a>00568     Nmatrix_ = A.<a class="code" href="class_seldon_1_1_matrix_collection.php#abf9ee2f169c7992c2d540d07dd85b104" title="Number of columns of matrices.">Nmatrix_</a>;
<a name="l00569"></a>00569     this-&gt;Mlocal_ = A.<a class="code" href="class_seldon_1_1_matrix_collection.php#a234a716260de98d93fc40fc2a3fd44fe" title="Number of rows in the underlying matrices.">Mlocal_</a>;
<a name="l00570"></a>00570     this-&gt;Mlocal_sum_ = A.<a class="code" href="class_seldon_1_1_matrix_collection.php#aa16e80513c77e58a2c1b3974e360f220" title="Cumulative number of rows in the underlying matrices.">Mlocal_sum_</a>;
<a name="l00571"></a>00571     this-&gt;Nlocal_ = A.<a class="code" href="class_seldon_1_1_matrix_collection.php#afdcab0ec07a154e7ed23ee20b2f4f574" title="Number of columns in the underlying matrices.">Nlocal_</a>;
<a name="l00572"></a>00572     this-&gt;Nlocal_sum_ = A.<a class="code" href="class_seldon_1_1_matrix_collection.php#a2cd7c7804f76b6e8a6966e6597e05244" title="Cumulative number of columns in the underlying matrices.">Nlocal_sum_</a>;
<a name="l00573"></a>00573 
<a name="l00574"></a>00574     matrix_.Reallocate(Mmatrix_, Nmatrix_);
<a name="l00575"></a>00575 
<a name="l00576"></a>00576     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Mmatrix_; i++)
<a name="l00577"></a>00577       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; Nmatrix_; j++)
<a name="l00578"></a>00578         SetMatrix(i, j, A.<a class="code" href="class_seldon_1_1_matrix_collection.php#ad18c52c1ed65330e6443e4519df390a4" title="Access to an underlying matrix.">GetMatrix</a>(i, j));
<a name="l00579"></a>00579   }
<a name="l00580"></a>00580 
<a name="l00581"></a>00581 
<a name="l00582"></a>00582   <span class="comment">/************************</span>
<a name="l00583"></a>00583 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l00584"></a>00584 <span class="comment">   ************************/</span>
<a name="l00585"></a>00585 
<a name="l00586"></a>00586 
<a name="l00588"></a>00588 
<a name="l00591"></a>00591   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00592"></a>00592   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#af85cc972c66fe36630031f480dea28ca" title="Displays the matrix collection on the standard output.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00593"></a>00593 <span class="keyword">  </span>{
<a name="l00594"></a>00594     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Mmatrix_; i++)
<a name="l00595"></a>00595       {
<a name="l00596"></a>00596         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; Nmatrix_; j++)
<a name="l00597"></a>00597           cout &lt;&lt; <a class="code" href="class_seldon_1_1_matrix_collection.php#ad18c52c1ed65330e6443e4519df390a4" title="Access to an underlying matrix.">GetMatrix</a>(i, j) &lt;&lt; endl;
<a name="l00598"></a>00598         cout &lt;&lt; endl;
<a name="l00599"></a>00599       }
<a name="l00600"></a>00600   }
<a name="l00601"></a>00601 
<a name="l00602"></a>00602 
<a name="l00604"></a>00604 
<a name="l00608"></a>00608   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00609"></a>00609   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#af85cc972c66fe36630031f480dea28ca" title="Displays the matrix collection on the standard output.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00610"></a>00610 <a class="code" href="class_seldon_1_1_matrix_collection.php#af85cc972c66fe36630031f480dea28ca" title="Displays the matrix collection on the standard output.">  ::Print</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00611"></a>00611 <span class="keyword">  </span>{
<a name="l00612"></a>00612 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00613"></a>00613 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Mmatrix_)
<a name="l00614"></a>00614       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;MatrixCollection::Print(int, int)&quot;</span>,
<a name="l00615"></a>00615                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Row index should be in [0, &quot;</span>)
<a name="l00616"></a>00616                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Mmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00617"></a>00617                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00618"></a>00618     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= Nmatrix_)
<a name="l00619"></a>00619       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;MatrixCollection::Print(int, int)&quot;</span>,
<a name="l00620"></a>00620                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Column index should be in [0, &quot;</span>)
<a name="l00621"></a>00621                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00622"></a>00622                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00623"></a>00623 <span class="preprocessor">#endif</span>
<a name="l00624"></a>00624 <span class="preprocessor"></span>
<a name="l00625"></a>00625     cout &lt;&lt; matrix_(i, j) &lt;&lt; endl;
<a name="l00626"></a>00626   }
<a name="l00627"></a>00627 
<a name="l00628"></a>00628 
<a name="l00630"></a>00630 
<a name="l00638"></a>00638   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00639"></a>00639   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#ab3016490b7df18d3017477352f6b60d6" title="Writes the matrix collection in a file.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00640"></a>00640 <a class="code" href="class_seldon_1_1_matrix_collection.php#ab3016490b7df18d3017477352f6b60d6" title="Writes the matrix collection in a file.">  ::Write</a>(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size)<span class="keyword"> const</span>
<a name="l00641"></a>00641 <span class="keyword">  </span>{
<a name="l00642"></a>00642     ofstream FileStream;
<a name="l00643"></a>00643     FileStream.open(FileName.c_str(), ofstream::binary);
<a name="l00644"></a>00644 
<a name="l00645"></a>00645 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00646"></a>00646 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00647"></a>00647     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00648"></a>00648       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;MatrixCollection::Write(string FileName, bool)&quot;</span>,
<a name="l00649"></a>00649                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00650"></a>00650 <span class="preprocessor">#endif</span>
<a name="l00651"></a>00651 <span class="preprocessor"></span>
<a name="l00652"></a>00652     this-&gt;Write(FileStream, with_size);
<a name="l00653"></a>00653 
<a name="l00654"></a>00654     FileStream.close();
<a name="l00655"></a>00655   }
<a name="l00656"></a>00656 
<a name="l00657"></a>00657 
<a name="l00659"></a>00659 
<a name="l00667"></a>00667   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00668"></a>00668   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#ab3016490b7df18d3017477352f6b60d6" title="Writes the matrix collection in a file.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00669"></a>00669 <a class="code" href="class_seldon_1_1_matrix_collection.php#ab3016490b7df18d3017477352f6b60d6" title="Writes the matrix collection in a file.">  ::Write</a>(ostream&amp; FileStream, <span class="keywordtype">bool</span> with_size = <span class="keyword">true</span>)<span class="keyword"> const</span>
<a name="l00670"></a>00670 <span class="keyword">  </span>{
<a name="l00671"></a>00671 
<a name="l00672"></a>00672 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00673"></a>00673 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00674"></a>00674     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00675"></a>00675       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;MatrixCollection::Write(ostream&amp; FileStream, bool)&quot;</span>,
<a name="l00676"></a>00676                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00677"></a>00677 <span class="preprocessor">#endif</span>
<a name="l00678"></a>00678 <span class="preprocessor"></span>
<a name="l00679"></a>00679     <span class="keywordflow">if</span> (with_size)
<a name="l00680"></a>00680       {
<a name="l00681"></a>00681         FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;Mmatrix_)),
<a name="l00682"></a>00682                          <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00683"></a>00683         FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;Nmatrix_)),
<a name="l00684"></a>00684                          <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00685"></a>00685       }
<a name="l00686"></a>00686 
<a name="l00687"></a>00687     <span class="keywordtype">int</span> i, j;
<a name="l00688"></a>00688     <span class="keywordflow">for</span> (i = 0; i &lt; this-&gt;GetMmatrix(); i++)
<a name="l00689"></a>00689       {
<a name="l00690"></a>00690         <span class="keywordflow">for</span> (j = 0; j &lt; this-&gt;GetNmatrix(); j++)
<a name="l00691"></a>00691           GetMatrix(i, j).Write(FileStream, with_size);
<a name="l00692"></a>00692       }
<a name="l00693"></a>00693 
<a name="l00694"></a>00694 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00695"></a>00695 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00696"></a>00696     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00697"></a>00697       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;MatrixCollection::Write(ostream&amp; FileStream, bool)&quot;</span>,
<a name="l00698"></a>00698                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l00699"></a>00699 <span class="preprocessor">#endif</span>
<a name="l00700"></a>00700 <span class="preprocessor"></span>
<a name="l00701"></a>00701   }
<a name="l00702"></a>00702 
<a name="l00703"></a>00703 
<a name="l00705"></a>00705 
<a name="l00710"></a>00710   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00711"></a>00711   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a5c8fd96466c6409871d381feece50e44" title="Writes the matrix collection in a file.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00712"></a>00712 <a class="code" href="class_seldon_1_1_matrix_collection.php#a5c8fd96466c6409871d381feece50e44" title="Writes the matrix collection in a file.">  ::WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00713"></a>00713 <span class="keyword">  </span>{
<a name="l00714"></a>00714     ofstream FileStream;
<a name="l00715"></a>00715     FileStream.precision(cout.precision());
<a name="l00716"></a>00716     FileStream.flags(cout.flags());
<a name="l00717"></a>00717     FileStream.open(FileName.c_str());
<a name="l00718"></a>00718 
<a name="l00719"></a>00719 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00720"></a>00720 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00721"></a>00721     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00722"></a>00722       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;MatrixCollection::WriteText(string FileName)&quot;</span>,
<a name="l00723"></a>00723                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00724"></a>00724 <span class="preprocessor">#endif</span>
<a name="l00725"></a>00725 <span class="preprocessor"></span>
<a name="l00726"></a>00726     this-&gt;WriteText(FileStream);
<a name="l00727"></a>00727 
<a name="l00728"></a>00728     FileStream.close();
<a name="l00729"></a>00729   }
<a name="l00730"></a>00730 
<a name="l00731"></a>00731 
<a name="l00733"></a>00733 
<a name="l00739"></a>00739   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00740"></a>00740   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#a5c8fd96466c6409871d381feece50e44" title="Writes the matrix collection in a file.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00741"></a>00741 <a class="code" href="class_seldon_1_1_matrix_collection.php#a5c8fd96466c6409871d381feece50e44" title="Writes the matrix collection in a file.">  ::WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00742"></a>00742 <span class="keyword">  </span>{
<a name="l00743"></a>00743 
<a name="l00744"></a>00744 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00745"></a>00745 <span class="preprocessor"></span>    <span class="comment">// Checks if the file is ready.</span>
<a name="l00746"></a>00746     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00747"></a>00747       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;MatrixCollection::WriteText(ostream&amp; FileStream)&quot;</span>,
<a name="l00748"></a>00748                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00749"></a>00749 <span class="preprocessor">#endif</span>
<a name="l00750"></a>00750 <span class="preprocessor"></span>
<a name="l00751"></a>00751     <span class="keywordtype">int</span> i, j;
<a name="l00752"></a>00752     <span class="keywordflow">for</span> (i = 0; i &lt; this-&gt;GetMmatrix(); i++)
<a name="l00753"></a>00753       {
<a name="l00754"></a>00754         <span class="keywordflow">for</span> (j = 0; j &lt; this-&gt;GetNmatrix(); j++)
<a name="l00755"></a>00755           GetMatrix(i, j).WriteText(FileStream);
<a name="l00756"></a>00756         FileStream &lt;&lt; endl;
<a name="l00757"></a>00757       }
<a name="l00758"></a>00758 
<a name="l00759"></a>00759 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00760"></a>00760 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00761"></a>00761     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00762"></a>00762       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;MatrixCollection::WriteText(ostream&amp; FileStream)&quot;</span>,
<a name="l00763"></a>00763                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l00764"></a>00764 <span class="preprocessor">#endif</span>
<a name="l00765"></a>00765 <span class="preprocessor"></span>
<a name="l00766"></a>00766   }
<a name="l00767"></a>00767 
<a name="l00768"></a>00768 
<a name="l00770"></a>00770 
<a name="l00776"></a>00776   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00777"></a>00777   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#af54798b7169f468681f24203b6688aba" title="Reads the matrix collection from a file.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;::Read</a>(<span class="keywordtype">string</span> FileName)
<a name="l00778"></a>00778   {
<a name="l00779"></a>00779     ifstream FileStream;
<a name="l00780"></a>00780     FileStream.open(FileName.c_str(), ifstream::binary);
<a name="l00781"></a>00781 
<a name="l00782"></a>00782 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00783"></a>00783 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00784"></a>00784     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00785"></a>00785       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;MatrixCollection::Read(string FileName)&quot;</span>,
<a name="l00786"></a>00786                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00787"></a>00787 <span class="preprocessor">#endif</span>
<a name="l00788"></a>00788 <span class="preprocessor"></span>
<a name="l00789"></a>00789     this-&gt;<a class="code" href="class_seldon_1_1_matrix_collection.php#af54798b7169f468681f24203b6688aba" title="Reads the matrix collection from a file.">Read</a>(FileStream);
<a name="l00790"></a>00790 
<a name="l00791"></a>00791     FileStream.close();
<a name="l00792"></a>00792   }
<a name="l00793"></a>00793 
<a name="l00794"></a>00794 
<a name="l00796"></a>00796 
<a name="l00802"></a>00802   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00803"></a>00803   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_collection.php#af54798b7169f468681f24203b6688aba" title="Reads the matrix collection from a file.">MatrixCollection&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00804"></a>00804 <a class="code" href="class_seldon_1_1_matrix_collection.php#af54798b7169f468681f24203b6688aba" title="Reads the matrix collection from a file.">  ::Read</a>(istream&amp; FileStream)
<a name="l00805"></a>00805   {
<a name="l00806"></a>00806 
<a name="l00807"></a>00807 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00808"></a>00808 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00809"></a>00809     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00810"></a>00810       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;MatrixCollection::Read(istream&amp; FileStream)&quot;</span>,
<a name="l00811"></a>00811                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00812"></a>00812 <span class="preprocessor">#endif</span>
<a name="l00813"></a>00813 <span class="preprocessor"></span>
<a name="l00814"></a>00814     <span class="keywordtype">int</span> *new_m, *new_n;
<a name="l00815"></a>00815     new_m = <span class="keyword">new</span> int;
<a name="l00816"></a>00816     new_n = <span class="keyword">new</span> int;
<a name="l00817"></a>00817 
<a name="l00818"></a>00818     FileStream.read(reinterpret_cast&lt;char*&gt;(new_m), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00819"></a>00819     FileStream.read(reinterpret_cast&lt;char*&gt;(new_n), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00820"></a>00820 
<a name="l00821"></a>00821     this-&gt;Reallocate(*new_m, *new_n);
<a name="l00822"></a>00822 
<a name="l00823"></a>00823     T working_matrix;
<a name="l00824"></a>00824     <span class="keywordtype">int</span> i, j;
<a name="l00825"></a>00825     <span class="keywordflow">for</span> (i = 0; i &lt; *new_m; i++)
<a name="l00826"></a>00826       <span class="keywordflow">for</span> (j = 0; j &lt; *new_n; j++)
<a name="l00827"></a>00827         {
<a name="l00828"></a>00828           working_matrix.Read(FileStream);
<a name="l00829"></a>00829           SetMatrix(i, j, working_matrix);
<a name="l00830"></a>00830           working_matrix.Nullify();
<a name="l00831"></a>00831         }
<a name="l00832"></a>00832 
<a name="l00833"></a>00833 
<a name="l00834"></a>00834     <span class="keyword">delete</span> new_n;
<a name="l00835"></a>00835     <span class="keyword">delete</span> new_m;
<a name="l00836"></a>00836 
<a name="l00837"></a>00837 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00838"></a>00838 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l00839"></a>00839     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00840"></a>00840       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;MatrixCollection::Read(istream&amp; FileStream)&quot;</span>,
<a name="l00841"></a>00841                     <span class="stringliteral">&quot;Input operation failed.&quot;</span>);
<a name="l00842"></a>00842 <span class="preprocessor">#endif</span>
<a name="l00843"></a>00843 <span class="preprocessor"></span>
<a name="l00844"></a>00844   }
<a name="l00845"></a>00845 
<a name="l00846"></a>00846 
<a name="l00848"></a>00848   <span class="comment">// COLMAJORCOLLECTION //</span>
<a name="l00850"></a>00850 <span class="comment"></span>
<a name="l00851"></a>00851 
<a name="l00852"></a>00852   <span class="comment">/****************</span>
<a name="l00853"></a>00853 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00854"></a>00854 <span class="comment">   ****************/</span>
<a name="l00855"></a>00855 
<a name="l00856"></a>00856 
<a name="l00858"></a>00858 
<a name="l00861"></a>00861   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00862"></a>00862   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_collection_00_01_allocator_01_4.php#ac5858ad9d7f0e3a1a30dc73ca25be70a" title="Default constructor.">Matrix&lt;T, Prop, ColMajorCollection, Allocator&gt;::Matrix</a>():
<a name="l00863"></a>00863     MatrixCollection&lt;T, Prop, ColMajor, Allocator&gt;()
<a name="l00864"></a>00864   {
<a name="l00865"></a>00865   }
<a name="l00866"></a>00866 
<a name="l00867"></a>00867 
<a name="l00869"></a>00869 
<a name="l00873"></a>00873   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00874"></a>00874   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_collection_00_01_allocator_01_4.php#ac5858ad9d7f0e3a1a30dc73ca25be70a" title="Default constructor.">Matrix&lt;T, Prop, ColMajorCollection, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l00875"></a>00875     MatrixCollection&lt;T, Prop, ColMajor, Allocator&gt;(i, j)
<a name="l00876"></a>00876   {
<a name="l00877"></a>00877   }
<a name="l00878"></a>00878 
<a name="l00879"></a>00879 
<a name="l00881"></a>00881   <span class="comment">// ROWMAJORCOLLECTION //</span>
<a name="l00883"></a>00883 <span class="comment"></span>
<a name="l00884"></a>00884 
<a name="l00885"></a>00885   <span class="comment">/****************</span>
<a name="l00886"></a>00886 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00887"></a>00887 <span class="comment">   ****************/</span>
<a name="l00888"></a>00888 
<a name="l00889"></a>00889 
<a name="l00891"></a>00891 
<a name="l00894"></a>00894   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00895"></a>00895   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_collection_00_01_allocator_01_4.php#a5a2c326ccb8beb999bd7dd53b6f1ae5b" title="Default constructor.">Matrix&lt;T, Prop, RowMajorCollection, Allocator&gt;::Matrix</a>():
<a name="l00896"></a>00896     MatrixCollection&lt;T, Prop, RowMajor, Allocator&gt;()
<a name="l00897"></a>00897   {
<a name="l00898"></a>00898   }
<a name="l00899"></a>00899 
<a name="l00900"></a>00900 
<a name="l00902"></a>00902 
<a name="l00906"></a>00906   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00907"></a>00907   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_collection_00_01_allocator_01_4.php#a5a2c326ccb8beb999bd7dd53b6f1ae5b" title="Default constructor.">Matrix&lt;T, Prop, RowMajorCollection, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l00908"></a>00908     MatrixCollection&lt;T, Prop, RowMajor, Allocator&gt;(i, j)
<a name="l00909"></a>00909   {
<a name="l00910"></a>00910   }
<a name="l00911"></a>00911 
<a name="l00912"></a>00912 
<a name="l00914"></a>00914   <span class="comment">// COLSYMPACKEDCOLLECTION //</span>
<a name="l00916"></a>00916 <span class="comment"></span>
<a name="l00917"></a>00917 
<a name="l00918"></a>00918   <span class="comment">/****************</span>
<a name="l00919"></a>00919 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00920"></a>00920 <span class="comment">   ****************/</span>
<a name="l00921"></a>00921 
<a name="l00922"></a>00922 
<a name="l00924"></a>00924 
<a name="l00927"></a>00927   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00928"></a>00928   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_packed_collection_00_01_allocator_01_4.php#a16f9fae88d8080b207a38199d90c76d6" title="Default constructor.">Matrix&lt;T, Prop, ColSymPackedCollection, Allocator&gt;::Matrix</a>():
<a name="l00929"></a>00929     MatrixCollection&lt;T, Prop, ColSymPacked, Allocator&gt;()
<a name="l00930"></a>00930   {
<a name="l00931"></a>00931   }
<a name="l00932"></a>00932 
<a name="l00933"></a>00933 
<a name="l00935"></a>00935 
<a name="l00939"></a>00939   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00940"></a>00940   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_packed_collection_00_01_allocator_01_4.php#a16f9fae88d8080b207a38199d90c76d6" title="Default constructor.">Matrix&lt;T, Prop, ColSymPackedCollection, Allocator&gt;</a>
<a name="l00941"></a>00941 <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_packed_collection_00_01_allocator_01_4.php#a16f9fae88d8080b207a38199d90c76d6" title="Default constructor.">  ::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l00942"></a>00942     MatrixCollection&lt;T, Prop, ColSymPacked, Allocator&gt;(i, j)
<a name="l00943"></a>00943   {
<a name="l00944"></a>00944   }
<a name="l00945"></a>00945 
<a name="l00946"></a>00946 
<a name="l00948"></a>00948   <span class="comment">// ROWSYMPACKEDCOLLECTION //</span>
<a name="l00950"></a>00950 <span class="comment"></span>
<a name="l00951"></a>00951 
<a name="l00952"></a>00952   <span class="comment">/****************</span>
<a name="l00953"></a>00953 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00954"></a>00954 <span class="comment">   ****************/</span>
<a name="l00955"></a>00955 
<a name="l00956"></a>00956 
<a name="l00958"></a>00958 
<a name="l00961"></a>00961   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00962"></a>00962   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_collection_00_01_allocator_01_4.php#aafc716f71526b10a5221833661109bf8" title="Default constructor.">Matrix&lt;T, Prop, RowSymPackedCollection, Allocator&gt;::Matrix</a>():
<a name="l00963"></a>00963     MatrixCollection&lt;T, Prop, RowSymPacked, Allocator&gt;()
<a name="l00964"></a>00964   {
<a name="l00965"></a>00965   }
<a name="l00966"></a>00966 
<a name="l00967"></a>00967 
<a name="l00969"></a>00969 
<a name="l00973"></a>00973   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00974"></a>00974   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_collection_00_01_allocator_01_4.php#aafc716f71526b10a5221833661109bf8" title="Default constructor.">Matrix&lt;T, Prop, RowSymPackedCollection, Allocator&gt;</a>
<a name="l00975"></a>00975 <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_collection_00_01_allocator_01_4.php#aafc716f71526b10a5221833661109bf8" title="Default constructor.">  ::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l00976"></a>00976     MatrixCollection&lt;T, Prop, RowSymPacked, Allocator&gt;(i, j)
<a name="l00977"></a>00977   {
<a name="l00978"></a>00978   }
<a name="l00979"></a>00979 
<a name="l00980"></a>00980 
<a name="l00981"></a>00981 } <span class="comment">// namespace Seldon.</span>
<a name="l00982"></a>00982 
<a name="l00983"></a>00983 
<a name="l00984"></a>00984 <span class="preprocessor">#define SELDON_FILE_MATRIX_COLLECTION_CXX</span>
<a name="l00985"></a>00985 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
