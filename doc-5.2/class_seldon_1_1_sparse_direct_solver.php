<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_sparse_direct_solver.php">SparseDirectSolver</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::SparseDirectSolver&lt; T &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::SparseDirectSolver" -->
<p>Class grouping different direct solvers.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_sparse_solver_8hxx_source.php">SparseSolver.hxx</a>&gt;</code></p>

<p><a href="class_seldon_1_1_sparse_direct_solver-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">enum &nbsp;</td><td class="memItemRight" valign="bottom">{ <br/>
&nbsp;&nbsp;<b>SELDON_SOLVER</b>, 
<b>UMFPACK</b>, 
<b>SUPERLU</b>, 
<b>MUMPS</b>, 
<br/>
&nbsp;&nbsp;<b>PASTIX</b>, 
<b>ILUT</b>
<br/>
 }</td></tr>
<tr><td class="memItemLeft" align="right" valign="top">enum &nbsp;</td><td class="memItemRight" valign="bottom">{ <br/>
&nbsp;&nbsp;<b>FACTO_OK</b>, 
<b>STRUCTURALLY_SINGULAR_MATRIX</b>, 
<b>NUMERICALLY_SINGULAR_MATRIX</b>, 
<b>OUT_OF_MEMORY</b>, 
<br/>
&nbsp;&nbsp;<b>INVALID_ARGUMENT</b>, 
<b>INCORRECT_NUMBER_OF_ROWS</b>, 
<b>MATRIX_INDICES_INCORRECT</b>, 
<b>INVALID_PERMUTATION</b>, 
<br/>
&nbsp;&nbsp;<b>ORDERING_FAILED</b>, 
<b>INTERNAL_ERROR</b>
<br/>
 }</td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aef26a5c7fcbe65069bca7967c73f7ab5"></a><!-- doxytag: member="Seldon::SparseDirectSolver::SparseDirectSolver" ref="aef26a5c7fcbe65069bca7967c73f7ab5" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#aef26a5c7fcbe65069bca7967c73f7ab5">SparseDirectSolver</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4dc8a27fc02a2bc978629f6630f319cd"></a><!-- doxytag: member="Seldon::SparseDirectSolver::HideMessages" ref="a4dc8a27fc02a2bc978629f6630f319cd" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a4dc8a27fc02a2bc978629f6630f319cd">HideMessages</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Hiding all messages. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad4b15cbf1fc0faec2354e0beaa4678eb"></a><!-- doxytag: member="Seldon::SparseDirectSolver::ShowMessages" ref="ad4b15cbf1fc0faec2354e0beaa4678eb" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#ad4b15cbf1fc0faec2354e0beaa4678eb">ShowMessages</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displaying basic messages. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a70a07b6d4ada2aa3b63e71edb71675a6"></a><!-- doxytag: member="Seldon::SparseDirectSolver::ShowFullHistory" ref="a70a07b6d4ada2aa3b63e71edb71675a6" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a70a07b6d4ada2aa3b63e71edb71675a6">ShowFullHistory</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displaying all the messages. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1da1c40d9433a58bcef7dec63695a9f9"></a><!-- doxytag: member="Seldon::SparseDirectSolver::Clear" ref="a1da1c40d9433a58bcef7dec63695a9f9" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a1da1c40d9433a58bcef7dec63695a9f9">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clearing factorization. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a72ddb935a03d6e507f4f0175d748c54d"></a><!-- doxytag: member="Seldon::SparseDirectSolver::GetM" ref="a72ddb935a03d6e507f4f0175d748c54d" args="() const " -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a72ddb935a03d6e507f4f0175d748c54d">GetM</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows of the factorized matrix. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa00bcb4a22c9ce619e88b4e1247023fc"></a><!-- doxytag: member="Seldon::SparseDirectSolver::GetN" ref="aa00bcb4a22c9ce619e88b4e1247023fc" args="() const " -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#aa00bcb4a22c9ce619e88b4e1247023fc">GetN</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows of the factorized matrix. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab706f04923d61bd5316dd65d5c5a4286"></a><!-- doxytag: member="Seldon::SparseDirectSolver::GetTypeOrdering" ref="ab706f04923d61bd5316dd65d5c5a4286" args="() const " -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#ab706f04923d61bd5316dd65d5c5a4286">GetTypeOrdering</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the ordering algorithm to use. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a42cd77074d9e29b54946cc2e99826f71"></a><!-- doxytag: member="Seldon::SparseDirectSolver::SetPermutation" ref="a42cd77074d9e29b54946cc2e99826f71" args="(const IVect &amp;)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a42cd77074d9e29b54946cc2e99826f71">SetPermutation</a> (const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets directly the new ordering (by giving a permutation vector). <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adc649792b3da7574a6340563a9bc7378"></a><!-- doxytag: member="Seldon::SparseDirectSolver::SelectOrdering" ref="adc649792b3da7574a6340563a9bc7378" args="(int)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#adc649792b3da7574a6340563a9bc7378">SelectOrdering</a> (int)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Modifies the ordering algorithm to use. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad1d61abb463cc28f50e0acce15a41ce7"></a><!-- doxytag: member="Seldon::SparseDirectSolver::SetNumberThreadPerNode" ref="ad1d61abb463cc28f50e0acce15a41ce7" args="(int m)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#ad1d61abb463cc28f50e0acce15a41ce7">SetNumberThreadPerNode</a> (int m)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Modifies the number of threads per node (for Pastix only). <br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a2283e241ce581a0571761db3ff7b1498"></a><!-- doxytag: member="Seldon::SparseDirectSolver::ComputeOrdering" ref="a2283e241ce581a0571761db3ff7b1498" args="(MatrixSparse &amp;A)" -->
template&lt;class MatrixSparse &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a2283e241ce581a0571761db3ff7b1498">ComputeOrdering</a> (MatrixSparse &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Computation of the permutation vector in order to reduce fill-in. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aee5b1ccbb25a3dfb21a6ea3ed00b037e"></a><!-- doxytag: member="Seldon::SparseDirectSolver::SelectDirectSolver" ref="aee5b1ccbb25a3dfb21a6ea3ed00b037e" args="(int)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#aee5b1ccbb25a3dfb21a6ea3ed00b037e">SelectDirectSolver</a> (int)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Modifies the direct solver to use. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a706d99d20b79c014b676cd91074860ee"></a><!-- doxytag: member="Seldon::SparseDirectSolver::SetNonSymmetricIlut" ref="a706d99d20b79c014b676cd91074860ee" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a706d99d20b79c014b676cd91074860ee">SetNonSymmetricIlut</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Enforces the use of unsymmetric algorithm for ilut solver. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a260c50fc0c60a95d6ee6636ff948c3a8"></a><!-- doxytag: member="Seldon::SparseDirectSolver::GetDirectSolver" ref="a260c50fc0c60a95d6ee6636ff948c3a8" args="()" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a260c50fc0c60a95d6ee6636ff948c3a8">GetDirectSolver</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the direct solver to use. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a58aa5d9084aa9dd43ea66bf84c4d920a"></a><!-- doxytag: member="Seldon::SparseDirectSolver::GetThresholdMatrix" ref="a58aa5d9084aa9dd43ea66bf84c4d920a" args="() const " -->
double&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a58aa5d9084aa9dd43ea66bf84c4d920a">GetThresholdMatrix</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns threshold used for ilut (if this solver is selected). <br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class MatrixSparse &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#afba79fbe3b2e117a3235e9801db9e9f4">Factorize</a> (MatrixSparse &amp;A, bool keep_matrix=false)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Factorization of matrix A.  <a href="#afba79fbe3b2e117a3235e9801db9e9f4"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="abee36e87c1e23539fc099e871a7bf6d3"></a><!-- doxytag: member="Seldon::SparseDirectSolver::GetInfoFactorization" ref="abee36e87c1e23539fc099e871a7bf6d3" args="(int &amp;ierr) const " -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#abee36e87c1e23539fc099e871a7bf6d3">GetInfoFactorization</a> (int &amp;ierr) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns error code of the direct solver (for Mumps only). <br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Vector1 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a63f7c474a05a6034c08ab44af925f55b">Solve</a> (Vector1 &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">x_solution is overwritten by solution of A x = b.  <a href="#a63f7c474a05a6034c08ab44af925f55b"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a278d2d4b4dd170ae821b7057385c2807"></a><!-- doxytag: member="Seldon::SparseDirectSolver::Solve" ref="a278d2d4b4dd170ae821b7057385c2807" args="(const TransStatus &amp;TransA, Vector1 &amp;x)" -->
template&lt;class TransStatus , class Vector1 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a278d2d4b4dd170ae821b7057385c2807">Solve</a> (const TransStatus &amp;TransA, Vector1 &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">x_solution is overwritten with solution of A x = b or A^T x = b. <br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae9819b726220aa9599132e149a6416e6"></a><!-- doxytag: member="Seldon::SparseDirectSolver::type_ordering" ref="ae9819b726220aa9599132e149a6416e6" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#ae9819b726220aa9599132e149a6416e6">type_ordering</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Ordering to use. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8be09e641dce4b15ccf37be8f9b3baa6"></a><!-- doxytag: member="Seldon::SparseDirectSolver::type_solver" ref="a8be09e641dce4b15ccf37be8f9b3baa6" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6">type_solver</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Solver to use. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2b4fcbacb7f6fb5276fe33b36501e99c"></a><!-- doxytag: member="Seldon::SparseDirectSolver::number_threads_per_node" ref="a2b4fcbacb7f6fb5276fe33b36501e99c" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a2b4fcbacb7f6fb5276fe33b36501e99c">number_threads_per_node</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of threads (for Pastix). <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4e946689a26b4103f0551bd2e6cf96eb"></a><!-- doxytag: member="Seldon::SparseDirectSolver::permut" ref="a4e946689a26b4103f0551bd2e6cf96eb" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">IVect</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a4e946689a26b4103f0551bd2e6cf96eb">permut</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Ordering (if supplied by the user). <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af37f390e9852c82c8eb3659abbdff7a4"></a><!-- doxytag: member="Seldon::SparseDirectSolver::n" ref="af37f390e9852c82c8eb3659abbdff7a4" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#af37f390e9852c82c8eb3659abbdff7a4">n</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Size of factorized linear system. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a215ef20ce0827e0974cade309a9aaced"></a><!-- doxytag: member="Seldon::SparseDirectSolver::threshold_matrix" ref="a215ef20ce0827e0974cade309a9aaced" args="" -->
double&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a215ef20ce0827e0974cade309a9aaced">threshold_matrix</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Threshold for ilut solver. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a67010f0cc1e81d66504f3987385fb5f7"></a><!-- doxytag: member="Seldon::SparseDirectSolver::enforce_unsym_ilut" ref="a67010f0cc1e81d66504f3987385fb5f7" args="" -->
bool&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a67010f0cc1e81d66504f3987385fb5f7">enforce_unsym_ilut</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Use of non-symmetric ilut? <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a18d4ec1c11f680b7a364757063bde92d"></a><!-- doxytag: member="Seldon::SparseDirectSolver::mat_seldon" ref="a18d4ec1c11f680b7a364757063bde92d" args="" -->
<a class="el" href="class_seldon_1_1_sparse_seldon_solver.php">SparseSeldonSolver</a>&lt; T &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sparse_direct_solver.php#a18d4ec1c11f680b7a364757063bde92d">mat_seldon</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default solver. <br/></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T&gt;<br/>
 class Seldon::SparseDirectSolver&lt; T &gt;</h3>

<p>Class grouping different direct solvers. </p>

<p>Definition at line <a class="el" href="_sparse_solver_8hxx_source.php#l00084">84</a> of file <a class="el" href="_sparse_solver_8hxx_source.php">SparseSolver.hxx</a>.</p>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="afba79fbe3b2e117a3235e9801db9e9f4"></a><!-- doxytag: member="Seldon::SparseDirectSolver::Factorize" ref="afba79fbe3b2e117a3235e9801db9e9f4" args="(MatrixSparse &amp;A, bool keep_matrix=false)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T &gt; </div>
<div class="memtemplate">
template&lt;class MatrixSparse &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_sparse_direct_solver.php">Seldon::SparseDirectSolver</a>&lt; T &gt;::Factorize </td>
          <td>(</td>
          <td class="paramtype">MatrixSparse &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>keep_matrix</em> = <code>false</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Factorization of matrix A. </p>
<p>LU factorization is stored in the current object. You can ask to clear the matrix given on input (to spare memory). </p>

<p>Definition at line <a class="el" href="_sparse_solver_8cxx_source.php#l01358">1358</a> of file <a class="el" href="_sparse_solver_8cxx_source.php">SparseSolver.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a63f7c474a05a6034c08ab44af925f55b"></a><!-- doxytag: member="Seldon::SparseDirectSolver::Solve" ref="a63f7c474a05a6034c08ab44af925f55b" args="(Vector1 &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T &gt; </div>
<div class="memtemplate">
template&lt;class Vector1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_sparse_direct_solver.php">Seldon::SparseDirectSolver</a>&lt; T &gt;::Solve </td>
          <td>(</td>
          <td class="paramtype">Vector1 &amp;&nbsp;</td>
          <td class="paramname"> <em>x_solution</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>x_solution is overwritten by solution of A x = b. </p>
<p>We assume that Factorize has been called previously. </p>

<p>Definition at line <a class="el" href="_sparse_solver_8cxx_source.php#l01523">1523</a> of file <a class="el" href="_sparse_solver_8cxx_source.php">SparseSolver.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>computation/solver/<a class="el" href="_sparse_solver_8hxx_source.php">SparseSolver.hxx</a></li>
<li>computation/solver/<a class="el" href="_sparse_solver_8cxx_source.php">SparseSolver.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
