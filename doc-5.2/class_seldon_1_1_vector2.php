<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_vector2.php">Vector2</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Vector2" -->
<p>Vector of vectors.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_vector2_8hxx_source.php">Vector2.hxx</a>&gt;</code></p>

<p><a href="class_seldon_1_1_vector2-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a89682055b32aaeb263a55f54353c07b9"></a><!-- doxytag: member="Seldon::Vector2::value_type" ref="a89682055b32aaeb263a55f54353c07b9" args="" -->
typedef T&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae99ac2d0a082339a1e9f08f26ba370ec"></a><!-- doxytag: member="Seldon::Vector2::pointer" ref="ae99ac2d0a082339a1e9f08f26ba370ec" args="" -->
typedef T *&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a29e17b2b227aaeb4ba4bbb3ab3196024"></a><!-- doxytag: member="Seldon::Vector2::const_pointer" ref="a29e17b2b227aaeb4ba4bbb3ab3196024" args="" -->
typedef const T *&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5f0b6d6575fa0d569a90d54fa171972f"></a><!-- doxytag: member="Seldon::Vector2::reference" ref="a5f0b6d6575fa0d569a90d54fa171972f" args="" -->
typedef T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a62c22da9220bfd37532e48df8f65acf1"></a><!-- doxytag: member="Seldon::Vector2::const_reference" ref="a62c22da9220bfd37532e48df8f65acf1" args="" -->
typedef const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#ac0902a15a512d012c9825d69345c0cc2">Vector2</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#ac0902a15a512d012c9825d69345c0cc2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a75217df57cc3aa01f13a20668ba410fb">Vector2</a> (int length)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Constructor.  <a href="#a75217df57cc3aa01f13a20668ba410fb"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a00822908f0850cae2262f884297d579e">Vector2</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt; &amp;length)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Constructor.  <a href="#a00822908f0850cae2262f884297d579e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a07190b6e16f512ee68feddf636c2698d"></a><!-- doxytag: member="Seldon::Vector2::~Vector2" ref="a07190b6e16f512ee68feddf636c2698d" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a07190b6e16f512ee68feddf636c2698d">~Vector2</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Destructor. The vector of vectors and the inner vectors are deallocated. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">bool&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a4dadbc6f7e013e21ceacc75efcb42c58">IsEmpty</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Checks whether no elements are contained in the inner vectors.  <a href="#a4dadbc6f7e013e21ceacc75efcb42c58"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8">GetLength</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the size along dimension 1.  <a href="#a5817c617540d8147b15a6296a3fa49b8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a1eb80b01b78bb858b72a3daace5b7fa2">GetSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the size along dimension 1.  <a href="#a1eb80b01b78bb858b72a3daace5b7fa2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a77b5138b5a35df2dbbd8088782b161a1">GetLength</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the size of the inner vector #<em>i</em>.  <a href="#a77b5138b5a35df2dbbd8088782b161a1"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a1d5851ecabcb996725793f8aeb9ac7fa">GetSize</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the size of the inner vector #<em>i</em>.  <a href="#a1d5851ecabcb996725793f8aeb9ac7fa"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#aff7d041efb4e03152ecb03966ae92948">GetNelement</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the total number of elements in the inner vectors.  <a href="#aff7d041efb4e03152ecb03966ae92948"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a482333565b8f18465e2b10a7a5b8f2d9">GetNelement</a> (int beg, int end) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the total number of elements in a range of inner vectors.  <a href="#a482333565b8f18465e2b10a7a5b8f2d9"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a82efefffc4e2e7ebaf120e080a79fb95">GetShape</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the shape.  <a href="#a82efefffc4e2e7ebaf120e080a79fb95"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a2800ef7139ca54d9386bab9a94910ebe">GetShape</a> (<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt; &amp;shape) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the shape.  <a href="#a2800ef7139ca54d9386bab9a94910ebe"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#ab0e6ad584c49c4182153a4e523e177ea">Reallocate</a> (int M)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates the vector of vector.  <a href="#ab0e6ad584c49c4182153a4e523e177ea"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#aef69b526bcb070821a83f15ec44f0678">Reallocate</a> (int i, int N)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates the inner vector #<em>i</em>.  <a href="#aef69b526bcb070821a83f15ec44f0678"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#ad7b662c330a05a55bd927f9b9ae8fc24">Reallocate</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt; &amp;length)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates the whole structure.  <a href="#ad7b662c330a05a55bd927f9b9ae8fc24"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a2cf9e05eacb905603b0f0cfc8e7cdc5a">Select</a> (int beg, int end)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Selects a range of inner vectors.  <a href="#a2cf9e05eacb905603b0f0cfc8e7cdc5a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#aa6cc7e42c8a0b53bd337e1e083753c8d">Flatten</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns all values in a vector.  <a href="#aa6cc7e42c8a0b53bd337e1e083753c8d"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Td , class Allocatord &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a3be94439c78312344107d23fbb290e0e">Flatten</a> (<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; Td, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocatord &gt; &amp;data) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns all values in a vector.  <a href="#a3be94439c78312344107d23fbb290e0e"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Td , class Allocatord &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a98d6e0c5dda92cbd0d70b1bfa16bd2ee">Flatten</a> (int beg, int end, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; Td, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocatord &gt; &amp;data) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns in a vector all values from a range of inner vectors.  <a href="#a98d6e0c5dda92cbd0d70b1bfa16bd2ee"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a1d386f766b75c53d17b66bc843a4db54">PushBack</a> (int i, const T &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Appends an element at the end of the inner vector #<em>i</em>.  <a href="#a1d386f766b75c53d17b66bc843a4db54"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#acbc05b0873524168d21b8f465a22670a">PushBack</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt; &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Appends an inner vector at the end of the vector.  <a href="#acbc05b0873524168d21b8f465a22670a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a424df1ac3a13eb1120c1afd07c6b0271">PushBack</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator1 &gt; &amp;V)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Appends a vector of vectors.  <a href="#a424df1ac3a13eb1120c1afd07c6b0271"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#acd15ae896e68859a4d7f6a2aa29f09d2">PushBack</a> (const <a class="el" href="class_seldon_1_1_vector2.php">Vector2</a>&lt; T, Allocator0, Allocator1 &gt; &amp;V)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Appends a vector of vectors.  <a href="#acd15ae896e68859a4d7f6a2aa29f09d2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8c34a780a5b3f8b83bf36a14a4e7f21a"></a><!-- doxytag: member="Seldon::Vector2::Clear" ref="a8c34a780a5b3f8b83bf36a14a4e7f21a" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a8c34a780a5b3f8b83bf36a14a4e7f21a">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the vector. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#ad95c6d7a9a6dd52aae5aef1ffc608b71">Clear</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears a given vector.  <a href="#ad95c6d7a9a6dd52aae5aef1ffc608b71"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a2794e1d6c95f016265e70453f13c88d3">Fill</a> (const T &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the vector with a given value.  <a href="#a2794e1d6c95f016265e70453f13c88d3"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
Allocator0 &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
Allocator1 &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a6955ada9427842710beeaa19ae65e299">GetVector</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the vector of vectors.  <a href="#a6955ada9427842710beeaa19ae65e299"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt;<br class="typebreak"/>
, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator1 &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a3f2341c313e4d593476afe4f94257330">GetVector</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the vector of vectors.  <a href="#a3f2341c313e4d593476afe4f94257330"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a9db5289640f107404e5a4d536c991511">GetVector</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a given inner vector.  <a href="#a9db5289640f107404e5a4d536c991511"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
Allocator0 &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a223c85f251be71548ad21b3f9844be7d">GetVector</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a given inner vector.  <a href="#a223c85f251be71548ad21b3f9844be7d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a7074df5f337f30702d6b6d59f4855a30">Copy</a> (const <a class="el" href="class_seldon_1_1_vector2.php">Vector2</a>&lt; T, Allocator0, Allocator1 &gt; &amp;V)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copies a <a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> instance.  <a href="#a7074df5f337f30702d6b6d59f4855a30"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector2.php">Vector2</a>&lt; T, Allocator0, <br class="typebreak"/>
Allocator1 &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a3450c8c52259796e94781a3fb607b2ab">Copy</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copies a <a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> instance.  <a href="#a3450c8c52259796e94781a3fb607b2ab"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
Allocator0 &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#ae163011cc1d13382280abd54c1f4af93">operator()</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a given inner vector.  <a href="#ae163011cc1d13382280abd54c1f4af93"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#ad8b3dd46b3b18c63fbf91ff266151ce4">operator()</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a given inner vector.  <a href="#ad8b3dd46b3b18c63fbf91ff266151ce4"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a90309917cc1fe070d182ed32bf97a0ed">operator()</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns an element of a given inner vector.  <a href="#a90309917cc1fe070d182ed32bf97a0ed"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a4fe142f0dce38f333d19854401394d86">operator()</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns an element of a given inner vector.  <a href="#a4fe142f0dce38f333d19854401394d86"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class V2 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">bool&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#ab3a611bb7d4dd2bfc0f7c6540665db38">HasSameShape</a> (const V2 &amp;V) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Checks whether another <a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> instance has the same shape.  <a href="#ab3a611bb7d4dd2bfc0f7c6540665db38"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab283856136c91c88d2f944eabed7bdc8"></a><!-- doxytag: member="Seldon::Vector2::Print" ref="ab283856136c91c88d2f944eabed7bdc8" args="() const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#ab283856136c91c88d2f944eabed7bdc8">Print</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays the vector. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a9d56f5030b0d407bbb1746998eaea337">Write</a> (string file_name, bool with_size=true) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the instance in a binary file.  <a href="#a9d56f5030b0d407bbb1746998eaea337"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a6e1eba2b39790d6aaa2403ecb477b175">Write</a> (ostream &amp;file_stream, bool with_size=true) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the instance in a stream in a binary format.  <a href="#a6e1eba2b39790d6aaa2403ecb477b175"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a3d4e468e1ec66a5e616f8eeee41c1897">Read</a> (string file_name, bool with_size=true)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the <a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> from a file.  <a href="#a3d4e468e1ec66a5e616f8eeee41c1897"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector2.php#a1f4923b25add7229c3bf87118875689a">Read</a> (istream &amp;file_stream, bool with_size=true)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the <a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> from a file.  <a href="#a1f4923b25add7229c3bf87118875689a"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad429ed53277ccbfd2fcf21195eb7c9ac"></a><!-- doxytag: member="Seldon::Vector2::data_" ref="ad429ed53277ccbfd2fcf21195eb7c9ac" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
Allocator0 &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
Allocator1 &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Allocator0 = SELDON_VECTOR2_DEFAULT_ALLOCATOR_0&lt;T&gt;, class Allocator1 = SELDON_VECTOR2_DEFAULT_ALLOCATOR_1&lt;              Vector&lt;T, VectFull, Allocator0&gt; &gt;&gt;<br/>
 class Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</h3>

<p>Vector of vectors. </p>
<p><a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> is a structure that acts like a vector of vectors. The inner vectors can be of any dimension, so that this structure is more flexible than a matrix. </p>
<dl><dt><b>Template Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>T</em>&nbsp;</td><td>numerical type of the inner vectors. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>Allocator0</em>&nbsp;</td><td>allocator for the inner vectors. The default allocator is SELDON_DEFAULT_ALLOCATOR. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>Allocator1</em>&nbsp;</td><td>allocator for the vector of vectors. It is recommended to choose <a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a> or, for more efficient in reallocations, <a class="el" href="class_seldon_1_1_malloc_object.php">MallocObject</a> (default allocator here): these allocators can manage an array of inner vectors. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector2_8hxx_source.php#l00060">60</a> of file <a class="el" href="_vector2_8hxx_source.php">Vector2.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="ac0902a15a512d012c9825d69345c0cc2"></a><!-- doxytag: member="Seldon::Vector2::Vector2" ref="ac0902a15a512d012c9825d69345c0cc2" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::<a class="el" href="class_seldon_1_1_vector2.php">Vector2</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>Nothing is allocated. </p>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00044">44</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a75217df57cc3aa01f13a20668ba410fb"></a><!-- doxytag: member="Seldon::Vector2::Vector2" ref="a75217df57cc3aa01f13a20668ba410fb" args="(int length)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::<a class="el" href="class_seldon_1_1_vector2.php">Vector2</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>length</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Constructor. </p>
<p>The vector of vectors is allocated with <em>length</em> empty vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>length</em>&nbsp;</td><td>the length of the vector of vectors. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00054">54</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a00822908f0850cae2262f884297d579e"></a><!-- doxytag: member="Seldon::Vector2::Vector2" ref="a00822908f0850cae2262f884297d579e" args="(const Vector&lt; int &gt; &amp;length)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::<a class="el" href="class_seldon_1_1_vector2.php">Vector2</a> </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>length</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Constructor. </p>
<p>The vector of vectors and the inner vectors are allocated. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>length</em>&nbsp;</td><td>the lengths of the inner vectors. The vector of vectors will obviously have as many elements as <em>length</em> has. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00066">66</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="ad95c6d7a9a6dd52aae5aef1ffc608b71"></a><!-- doxytag: member="Seldon::Vector2::Clear" ref="ad95c6d7a9a6dd52aae5aef1ffc608b71" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::Clear </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears a given vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the vector to be cleared. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00423">423</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7074df5f337f30702d6b6d59f4855a30"></a><!-- doxytag: member="Seldon::Vector2::Copy" ref="a7074df5f337f30702d6b6d59f4855a30" args="(const Vector2&lt; T, Allocator0, Allocator1 &gt; &amp;V)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Allocator0, class Allocator1&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::Copy </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector2.php">Vector2</a>&lt; T, Allocator0, Allocator1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>V</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Copies a <a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> instance. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>V</em>&nbsp;</td><td><a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> instance to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>The current instance and <em>V</em> do not share memory on exit: <em>V</em> is duplicated in memory. </dd></dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00499">499</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3450c8c52259796e94781a3fb607b2ab"></a><!-- doxytag: member="Seldon::Vector2::Copy" ref="a3450c8c52259796e94781a3fb607b2ab" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Allocator0, class Allocator1&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector2.php">Vector2</a>&lt; T, Allocator0, Allocator1 &gt; <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::Copy </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Copies a <a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> instance. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A copy of the current <a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> instance. </dd></dl>
<dl class="note"><dt><b>Note:</b></dt><dd>The current instance and the copy do not share memory on exit. </dd></dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00515">515</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2794e1d6c95f016265e70453f13c88d3"></a><!-- doxytag: member="Seldon::Vector2::Fill" ref="a2794e1d6c95f016265e70453f13c88d3" args="(const T &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::Fill </td>
          <td>(</td>
          <td class="paramtype">const T &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the vector with a given value. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>x</em>&nbsp;</td><td>value to fill the vector with. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00434">434</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa6cc7e42c8a0b53bd337e1e083753c8d"></a><!-- doxytag: member="Seldon::Vector2::Flatten" ref="aa6cc7e42c8a0b53bd337e1e083753c8d" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt; <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::Flatten </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns all values in a vector. </p>
<p>The output vector contains all inner vectors concatenated in the same order as they appear in the current <a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> instance. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>All values from the current <a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> instance. </dd></dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00296">296</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a98d6e0c5dda92cbd0d70b1bfa16bd2ee"></a><!-- doxytag: member="Seldon::Vector2::Flatten" ref="a98d6e0c5dda92cbd0d70b1bfa16bd2ee" args="(int beg, int end, Vector&lt; Td, VectFull, Allocatord &gt; &amp;data) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
<div class="memtemplate">
template&lt;class Td , class Allocatord &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::Flatten </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>beg</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>end</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; Td, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocatord &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>data</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns in a vector all values from a range of inner vectors. </p>
<p>The output vector <em>data</em> contains all inner vectors, in the index range [<em>beg</em>, <em>end</em>[, concatenated in the same order as they appear in the current <a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> instance. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>beg</em>&nbsp;</td><td>inclusive lower-bound for the indexes. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>end</em>&nbsp;</td><td>exclusive upper-bound for the indexes. </td></tr>
    <tr><td valign="top"><tt>[out]</tt>&nbsp;</td><td valign="top"><em>data</em>&nbsp;</td><td>the values contained in the inner vectors [<em>beg</em>, <em>end</em>[. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00337">337</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3be94439c78312344107d23fbb290e0e"></a><!-- doxytag: member="Seldon::Vector2::Flatten" ref="a3be94439c78312344107d23fbb290e0e" args="(Vector&lt; Td, VectFull, Allocatord &gt; &amp;data) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
<div class="memtemplate">
template&lt;class Td , class Allocatord &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::Flatten </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; Td, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocatord &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>data</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns all values in a vector. </p>
<p>The output vector <em>data</em> contains all inner vectors concatenated in the same order as they appear in the current <a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> instance. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[out]</tt>&nbsp;</td><td valign="top"><em>data</em>&nbsp;</td><td>all values from the current <a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> instance. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00315">315</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a77b5138b5a35df2dbbd8088782b161a1"></a><!-- doxytag: member="Seldon::Vector2::GetLength" ref="a77b5138b5a35df2dbbd8088782b161a1" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::GetLength </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the size of the inner vector #<em>i</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The size of the inner vector #<em>i</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00147">147</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5817c617540d8147b15a6296a3fa49b8"></a><!-- doxytag: member="Seldon::Vector2::GetLength" ref="a5817c617540d8147b15a6296a3fa49b8" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::GetLength </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the size along dimension 1. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The size along dimension 1. </dd></dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00123">123</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a482333565b8f18465e2b10a7a5b8f2d9"></a><!-- doxytag: member="Seldon::Vector2::GetNelement" ref="a482333565b8f18465e2b10a7a5b8f2d9" args="(int beg, int end) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::GetNelement </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>beg</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>end</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the total number of elements in a range of inner vectors. </p>
<p>Returns the total number of elements in the range [<em>beg</em>, <em>end</em>[ of inner vectors. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>beg</em>&nbsp;</td><td>inclusive lower-bound for the indexes. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>end</em>&nbsp;</td><td>exclusive upper-bound for the indexes. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The sum of the lengths of the inner vectors with index <em>beg</em> to <em>end-1</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00176">176</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aff7d041efb4e03152ecb03966ae92948"></a><!-- doxytag: member="Seldon::Vector2::GetNelement" ref="aff7d041efb4e03152ecb03966ae92948" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::GetNelement </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the total number of elements in the inner vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The sum of the lengths of the inner vectors. </dd></dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00158">158</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a82efefffc4e2e7ebaf120e080a79fb95"></a><!-- doxytag: member="Seldon::Vector2::GetShape" ref="a82efefffc4e2e7ebaf120e080a79fb95" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt; <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::GetShape </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the shape. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A vector with the lengths of the inner vectors. </dd></dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00201">201</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2800ef7139ca54d9386bab9a94910ebe"></a><!-- doxytag: member="Seldon::Vector2::GetShape" ref="a2800ef7139ca54d9386bab9a94910ebe" args="(Vector&lt; int &gt; &amp;shape) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::GetShape </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>shape</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the shape. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[out]</tt>&nbsp;</td><td valign="top"><em>shape</em>&nbsp;</td><td>the lengths of the inner vectors. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00215">215</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1d5851ecabcb996725793f8aeb9ac7fa"></a><!-- doxytag: member="Seldon::Vector2::GetSize" ref="a1d5851ecabcb996725793f8aeb9ac7fa" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::GetSize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the size of the inner vector #<em>i</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The size of the inner vector #<em>i</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00135">135</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1eb80b01b78bb858b72a3daace5b7fa2"></a><!-- doxytag: member="Seldon::Vector2::GetSize" ref="a1eb80b01b78bb858b72a3daace5b7fa2" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::GetSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the size along dimension 1. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The size along dimension 1. </dd></dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00112">112</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a223c85f251be71548ad21b3f9844be7d"></a><!-- doxytag: member="Seldon::Vector2::GetVector" ref="a223c85f251be71548ad21b3f9844be7d" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt; &amp; <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::GetVector </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a given inner vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The inner vector #<em>i</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00485">485</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6955ada9427842710beeaa19ae65e299"></a><!-- doxytag: member="Seldon::Vector2::GetVector" ref="a6955ada9427842710beeaa19ae65e299" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator1 &gt; &amp; <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::GetVector </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the vector of vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The vector of vectors. </dd></dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00447">447</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3f2341c313e4d593476afe4f94257330"></a><!-- doxytag: member="Seldon::Vector2::GetVector" ref="a3f2341c313e4d593476afe4f94257330" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator1 &gt; <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::GetVector </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the vector of vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The vector of vectors. </dd></dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00459">459</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9db5289640f107404e5a4d536c991511"></a><!-- doxytag: member="Seldon::Vector2::GetVector" ref="a9db5289640f107404e5a4d536c991511" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt; &amp; <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::GetVector </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a given inner vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The inner vector #<em>i</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00472">472</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab3a611bb7d4dd2bfc0f7c6540665db38"></a><!-- doxytag: member="Seldon::Vector2::HasSameShape" ref="ab3a611bb7d4dd2bfc0f7c6540665db38" args="(const V2 &amp;V) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
<div class="memtemplate">
template&lt;class V2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">bool <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::HasSameShape </td>
          <td>(</td>
          <td class="paramtype">const V2 &amp;&nbsp;</td>
          <td class="paramname"> <em>V</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Checks whether another <a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> instance has the same shape. </p>
<p>Checks whether another <a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> instance has the same shape as the current instance. The shapes are the same if both instances have the same number of inner vectors, and if the inner vectors have the same lengths. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>V</em>&nbsp;</td><td><a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> instance whose shape is compared to that of the current instance. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>True if the current instance as the same shape as <em>V</em>, false otherwise. </dd></dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00598">598</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a4dadbc6f7e013e21ceacc75efcb42c58"></a><!-- doxytag: member="Seldon::Vector2::IsEmpty" ref="a4dadbc6f7e013e21ceacc75efcb42c58" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">bool <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::IsEmpty </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Checks whether no elements are contained in the inner vectors. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>True is no inner vector contains an element, false otherwise. </dd></dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00098">98</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a90309917cc1fe070d182ed32bf97a0ed"></a><!-- doxytag: member="Seldon::Vector2::operator()" ref="a90309917cc1fe070d182ed32bf97a0ed" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector2.php">Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::const_reference <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns an element of a given inner vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>index of the element in the inner vector #<em>i</em>. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The element #<em>j</em> of the inner vector #<em>i</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00562">562</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae163011cc1d13382280abd54c1f4af93"></a><!-- doxytag: member="Seldon::Vector2::operator()" ref="ae163011cc1d13382280abd54c1f4af93" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt; &amp; <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a given inner vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The inner vector #<em>i</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00535">535</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a4fe142f0dce38f333d19854401394d86"></a><!-- doxytag: member="Seldon::Vector2::operator()" ref="a4fe142f0dce38f333d19854401394d86" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector2.php">Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::reference <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns an element of a given inner vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>index of the element in the inner vector #<em>i</em>. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The element #<em>j</em> of the inner vector #<em>i</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00576">576</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad8b3dd46b3b18c63fbf91ff266151ce4"></a><!-- doxytag: member="Seldon::Vector2::operator()" ref="ad8b3dd46b3b18c63fbf91ff266151ce4" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt; &amp; <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a given inner vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The inner vector #<em>i</em>. </dd></dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00548">548</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a424df1ac3a13eb1120c1afd07c6b0271"></a><!-- doxytag: member="Seldon::Vector2::PushBack" ref="a424df1ac3a13eb1120c1afd07c6b0271" args="(const Vector&lt; Vector&lt; T, VectFull, Allocator0 &gt;, VectFull, Allocator1 &gt; &amp;V)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Allocator0, class Allocator1&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::PushBack </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>V</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Appends a vector of vectors. </p>
<p>The inner vectors of <em>V</em> are appended to the current instance, in the same order as they appear in <em>V</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>V</em>&nbsp;</td><td>vector of vectors to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00389">389</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1d386f766b75c53d17b66bc843a4db54"></a><!-- doxytag: member="Seldon::Vector2::PushBack" ref="a1d386f766b75c53d17b66bc843a4db54" args="(int i, const T &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::PushBack </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const T &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Appends an element at the end of the inner vector #<em>i</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector to which <em>x</em> should be appended. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>x</em>&nbsp;</td><td>element to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00364">364</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="acbc05b0873524168d21b8f465a22670a"></a><!-- doxytag: member="Seldon::Vector2::PushBack" ref="acbc05b0873524168d21b8f465a22670a" args="(const Vector&lt; T, VectFull, Allocator0 &gt; &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Allocator0, class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::PushBack </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Appends an inner vector at the end of the vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>X</em>&nbsp;</td><td>vector to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00376">376</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="acd15ae896e68859a4d7f6a2aa29f09d2"></a><!-- doxytag: member="Seldon::Vector2::PushBack" ref="acd15ae896e68859a4d7f6a2aa29f09d2" args="(const Vector2&lt; T, Allocator0, Allocator1 &gt; &amp;V)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Allocator0, class Allocator1&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::PushBack </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector2.php">Vector2</a>&lt; T, Allocator0, Allocator1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>V</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Appends a vector of vectors. </p>
<p>The inner vectors of <em>V</em> are appended to the current instance, in the same order as they appear in <em>V</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>V</em>&nbsp;</td><td>vector of vectors to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00404">404</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1f4923b25add7229c3bf87118875689a"></a><!-- doxytag: member="Seldon::Vector2::Read" ref="a1f4923b25add7229c3bf87118875689a" args="(istream &amp;file_stream, bool with_size=true)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>stream</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the <a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> from a file. </p>
<p>Sets the current <a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> instance according to a binary stream that stores the total number of inner vectors, and, for each inner vector, the length of the vector (integer) and its elements. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in,out]</tt>&nbsp;</td><td valign="top"><em>stream</em>&nbsp;</td><td>input stream. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the total number of inner vectors and the lengths of the vectors are not available in the stream. In this case, the shape of the current instance is unchanged and the values of the elements are directly read in the file. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00740">740</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3d4e468e1ec66a5e616f8eeee41c1897"></a><!-- doxytag: member="Seldon::Vector2::Read" ref="a3d4e468e1ec66a5e616f8eeee41c1897" args="(string file_name, bool with_size=true)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>file_name</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the <a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> from a file. </p>
<p>Sets the current <a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Vector2</a> instance according to a binary file that stores the total number of inner vectors, and, for each inner vector, the length of the vector (integer) and its elements. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>file_name</em>&nbsp;</td><td>file name. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the total number of inner vectors and the lengths of the vectors are not available in the file. In this case, the shape of the current instance is unchanged and the values of the elements are directly read in the file. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00709">709</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad7b662c330a05a55bd927f9b9ae8fc24"></a><!-- doxytag: member="Seldon::Vector2::Reallocate" ref="ad7b662c330a05a55bd927f9b9ae8fc24" args="(const Vector&lt; int &gt; &amp;length)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::Reallocate </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>length</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates the whole structure. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>length</em>&nbsp;</td><td>the new lengths of the inner vectors. The vector of vectors will obviously have as many elements as <em>length</em> has. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00253">253</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab0e6ad584c49c4182153a4e523e177ea"></a><!-- doxytag: member="Seldon::Vector2::Reallocate" ref="ab0e6ad584c49c4182153a4e523e177ea" args="(int M)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::Reallocate </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>M</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates the vector of vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>M</em>&nbsp;</td><td>the new size of the vector of vectors. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00228">228</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aef69b526bcb070821a83f15ec44f0678"></a><!-- doxytag: member="Seldon::Vector2::Reallocate" ref="aef69b526bcb070821a83f15ec44f0678" args="(int i, int N)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::Reallocate </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>N</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates the inner vector #<em>i</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the inner vector to be reallocated. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>N</em>&nbsp;</td><td>the new size of the inner vector #<em>i</em>. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00240">240</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2cf9e05eacb905603b0f0cfc8e7cdc5a"></a><!-- doxytag: member="Seldon::Vector2::Select" ref="a2cf9e05eacb905603b0f0cfc8e7cdc5a" args="(int beg, int end)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::Select </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>beg</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>end</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Selects a range of inner vectors. </p>
<p>Only the inner vectors with index in [<em>beg</em>, <em>end</em>[ are kept. The other vectors are destroyed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>beg</em>&nbsp;</td><td>inclusive lower-bound for the indexes. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>end</em>&nbsp;</td><td>exclusive upper-bound for the indexes. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00269">269</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9d56f5030b0d407bbb1746998eaea337"></a><!-- doxytag: member="Seldon::Vector2::Write" ref="a9d56f5030b0d407bbb1746998eaea337" args="(string file_name, bool with_size=true) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>file_name</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the instance in a binary file. </p>
<p>The number of inner vectors (integer) is written first. Then for each vector, the length of the vector (integer) and all elements of the vector are written. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>file_name</em>&nbsp;</td><td>file name. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the number of vectors and the lengths of the inner vectors are not saved. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00637">637</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6e1eba2b39790d6aaa2403ecb477b175"></a><!-- doxytag: member="Seldon::Vector2::Write" ref="a6e1eba2b39790d6aaa2403ecb477b175" args="(ostream &amp;file_stream, bool with_size=true) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator0 , class Allocator1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2</a>&lt; T, Allocator0, Allocator1 &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>stream</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the instance in a stream in a binary format. </p>
<p>The number of inner vectors (integer) is written first. Then for each vector, the length of the vector (integer) and all elements of the vector are written. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in,out]</tt>&nbsp;</td><td valign="top"><em>stream</em>&nbsp;</td><td>output stream. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the number of vectors and the lengths of the inner vectors are not saved. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector2_8cxx_source.php#l00666">666</a> of file <a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>vector/<a class="el" href="_vector2_8hxx_source.php">Vector2.hxx</a></li>
<li>vector/<a class="el" href="_vector2_8cxx_source.php">Vector2.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
