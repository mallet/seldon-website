<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix/Matrix_HermPacked.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_HERMPACKED_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;Matrix_HermPacked.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="keyword">namespace </span>Seldon
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 
<a name="l00029"></a>00029   <span class="comment">/****************</span>
<a name="l00030"></a>00030 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00031"></a>00031 <span class="comment">   ****************/</span>
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00035"></a>00035 
<a name="l00038"></a>00038   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00039"></a>00039   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ad6584a1ca7199865d2a4d5dc6af246fe" title="Default constructor.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Matrix_HermPacked</a>():
<a name="l00040"></a>00040     Matrix_Base&lt;T, Allocator&gt;()
<a name="l00041"></a>00041   {
<a name="l00042"></a>00042   }
<a name="l00043"></a>00043 
<a name="l00044"></a>00044 
<a name="l00046"></a>00046 
<a name="l00051"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ad5435d242af5fabbac0f1941972045fe">00051</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00052"></a>00052   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ad6584a1ca7199865d2a4d5dc6af246fe" title="Default constructor.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00053"></a>00053 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ad6584a1ca7199865d2a4d5dc6af246fe" title="Default constructor.">  ::Matrix_HermPacked</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l00054"></a>00054     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, i)
<a name="l00055"></a>00055   {
<a name="l00056"></a>00056 
<a name="l00057"></a>00057 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00058"></a>00058 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00059"></a>00059       {
<a name="l00060"></a>00060 <span class="preprocessor">#endif</span>
<a name="l00061"></a>00061 <span class="preprocessor"></span>
<a name="l00062"></a>00062         this-&gt;data_ = this-&gt;allocator_.allocate((i * (i + 1)) / 2, <span class="keyword">this</span>);
<a name="l00063"></a>00063 
<a name="l00064"></a>00064 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00065"></a>00065 <span class="preprocessor"></span>      }
<a name="l00066"></a>00066     <span class="keywordflow">catch</span> (...)
<a name="l00067"></a>00067       {
<a name="l00068"></a>00068         this-&gt;m_ = 0;
<a name="l00069"></a>00069         this-&gt;n_ = 0;
<a name="l00070"></a>00070         this-&gt;data_ = NULL;
<a name="l00071"></a>00071         <span class="keywordflow">return</span>;
<a name="l00072"></a>00072       }
<a name="l00073"></a>00073     <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00074"></a>00074       {
<a name="l00075"></a>00075         this-&gt;m_ = 0;
<a name="l00076"></a>00076         this-&gt;n_ = 0;
<a name="l00077"></a>00077         <span class="keywordflow">return</span>;
<a name="l00078"></a>00078       }
<a name="l00079"></a>00079 <span class="preprocessor">#endif</span>
<a name="l00080"></a>00080 <span class="preprocessor"></span>
<a name="l00081"></a>00081   }
<a name="l00082"></a>00082 
<a name="l00083"></a>00083 
<a name="l00085"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a0c7b7a12f90ac04b5c8d2c76089ebee1">00085</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00086"></a>00086   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ad6584a1ca7199865d2a4d5dc6af246fe" title="Default constructor.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00087"></a>00087 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ad6584a1ca7199865d2a4d5dc6af246fe" title="Default constructor.">  ::Matrix_HermPacked</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked</a>&lt;T, Prop,
<a name="l00088"></a>00088                       Storage, Allocator&gt;&amp; A):
<a name="l00089"></a>00089     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;()
<a name="l00090"></a>00090   {
<a name="l00091"></a>00091     this-&gt;m_ = 0;
<a name="l00092"></a>00092     this-&gt;n_ = 0;
<a name="l00093"></a>00093     this-&gt;data_ = NULL;
<a name="l00094"></a>00094 
<a name="l00095"></a>00095     this-&gt;Copy(A);
<a name="l00096"></a>00096   }
<a name="l00097"></a>00097 
<a name="l00098"></a>00098 
<a name="l00099"></a>00099   <span class="comment">/**************</span>
<a name="l00100"></a>00100 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00101"></a>00101 <span class="comment">   **************/</span>
<a name="l00102"></a>00102 
<a name="l00103"></a>00103 
<a name="l00105"></a>00105   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00106"></a>00106   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a2d9134dbc8606170ccaaf6aed472df26" title="Destructor.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::~Matrix_HermPacked</a>()
<a name="l00107"></a>00107   {
<a name="l00108"></a>00108 
<a name="l00109"></a>00109 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00110"></a>00110 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00111"></a>00111       {
<a name="l00112"></a>00112 <span class="preprocessor">#endif</span>
<a name="l00113"></a>00113 <span class="preprocessor"></span>
<a name="l00114"></a>00114         <span class="keywordflow">if</span> (this-&gt;data_ != NULL)
<a name="l00115"></a>00115           {
<a name="l00116"></a>00116             this-&gt;allocator_.deallocate(this-&gt;data_,
<a name="l00117"></a>00117                                         (this-&gt;m_ * (this-&gt;m_ + 1)) / 2);
<a name="l00118"></a>00118             this-&gt;data_ = NULL;
<a name="l00119"></a>00119           }
<a name="l00120"></a>00120 
<a name="l00121"></a>00121 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00122"></a>00122 <span class="preprocessor"></span>      }
<a name="l00123"></a>00123     <span class="keywordflow">catch</span> (...)
<a name="l00124"></a>00124       {
<a name="l00125"></a>00125         this-&gt;m_ = 0;
<a name="l00126"></a>00126         this-&gt;n_ = 0;
<a name="l00127"></a>00127         this-&gt;data_ = NULL;
<a name="l00128"></a>00128       }
<a name="l00129"></a>00129 <span class="preprocessor">#endif</span>
<a name="l00130"></a>00130 <span class="preprocessor"></span>
<a name="l00131"></a>00131   }
<a name="l00132"></a>00132 
<a name="l00133"></a>00133 
<a name="l00135"></a>00135 
<a name="l00139"></a>00139   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00140"></a>00140   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a98226901bde3cf19ae74571c38280792" title="Clears the matrix.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Clear</a>()
<a name="l00141"></a>00141   {
<a name="l00142"></a>00142     this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a2d9134dbc8606170ccaaf6aed472df26" title="Destructor.">~Matrix_HermPacked</a>();
<a name="l00143"></a>00143     this-&gt;m_ = 0;
<a name="l00144"></a>00144     this-&gt;n_ = 0;
<a name="l00145"></a>00145   }
<a name="l00146"></a>00146 
<a name="l00147"></a>00147 
<a name="l00148"></a>00148   <span class="comment">/*******************</span>
<a name="l00149"></a>00149 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00150"></a>00150 <span class="comment">   *******************/</span>
<a name="l00151"></a>00151 
<a name="l00152"></a>00152 
<a name="l00154"></a>00154 
<a name="l00157"></a>00157   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00158"></a>00158   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a8bd2747b6c90c4114cd2f54620d1f230" title="Returns the number of elements stored in memory.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::GetDataSize</a>()<span class="keyword"> const</span>
<a name="l00159"></a>00159 <span class="keyword">  </span>{
<a name="l00160"></a>00160     <span class="keywordflow">return</span> (this-&gt;m_ * (this-&gt;m_ + 1)) / 2;
<a name="l00161"></a>00161   }
<a name="l00162"></a>00162 
<a name="l00163"></a>00163 
<a name="l00164"></a>00164   <span class="comment">/*********************</span>
<a name="l00165"></a>00165 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00166"></a>00166 <span class="comment">   *********************/</span>
<a name="l00167"></a>00167 
<a name="l00168"></a>00168 
<a name="l00170"></a>00170 
<a name="l00176"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#aa2584f5a75b2ae0a2beb12fa45de3c11">00176</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00177"></a>00177   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#aa2584f5a75b2ae0a2beb12fa45de3c11" title="Reallocates memory to resize the matrix.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00178"></a>00178 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#aa2584f5a75b2ae0a2beb12fa45de3c11" title="Reallocates memory to resize the matrix.">  ::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00179"></a>00179   {
<a name="l00180"></a>00180 
<a name="l00181"></a>00181     <span class="keywordflow">if</span> (i != this-&gt;m_)
<a name="l00182"></a>00182       {
<a name="l00183"></a>00183         this-&gt;m_ = i;
<a name="l00184"></a>00184         this-&gt;n_ = i;
<a name="l00185"></a>00185 
<a name="l00186"></a>00186 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00187"></a>00187 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l00188"></a>00188           {
<a name="l00189"></a>00189 <span class="preprocessor">#endif</span>
<a name="l00190"></a>00190 <span class="preprocessor"></span>
<a name="l00191"></a>00191             this-&gt;data_ =
<a name="l00192"></a>00192               <span class="keyword">reinterpret_cast&lt;</span>pointer<span class="keyword">&gt;</span>(this-&gt;allocator_.reallocate(this-&gt;data_,
<a name="l00193"></a>00193                                                                     (i*(i + 1)) / 2,
<a name="l00194"></a>00194                                                                     <span class="keyword">this</span>));
<a name="l00195"></a>00195 
<a name="l00196"></a>00196 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00197"></a>00197 <span class="preprocessor"></span>          }
<a name="l00198"></a>00198         <span class="keywordflow">catch</span> (...)
<a name="l00199"></a>00199           {
<a name="l00200"></a>00200             this-&gt;m_ = 0;
<a name="l00201"></a>00201             this-&gt;n_ = 0;
<a name="l00202"></a>00202             this-&gt;data_ = NULL;
<a name="l00203"></a>00203             <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Reallocate(int, int)&quot;</span>,
<a name="l00204"></a>00204                            <span class="stringliteral">&quot;Unable to reallocate memory for data_.&quot;</span>);
<a name="l00205"></a>00205           }
<a name="l00206"></a>00206         <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00207"></a>00207           {
<a name="l00208"></a>00208             this-&gt;m_ = 0;
<a name="l00209"></a>00209             this-&gt;n_ = 0;
<a name="l00210"></a>00210             <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Reallocate(int, int)&quot;</span>,
<a name="l00211"></a>00211                            <span class="stringliteral">&quot;Unable to reallocate memory for data_.&quot;</span>);
<a name="l00212"></a>00212           }
<a name="l00213"></a>00213 <span class="preprocessor">#endif</span>
<a name="l00214"></a>00214 <span class="preprocessor"></span>
<a name="l00215"></a>00215       }
<a name="l00216"></a>00216   }
<a name="l00217"></a>00217 
<a name="l00218"></a>00218 
<a name="l00221"></a>00221 
<a name="l00235"></a>00235   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00236"></a>00236   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00237"></a>00237 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">  ::SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00238"></a>00238             <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00239"></a>00239             ::pointer data)
<a name="l00240"></a>00240   {
<a name="l00241"></a>00241     this-&gt;Clear();
<a name="l00242"></a>00242 
<a name="l00243"></a>00243     this-&gt;m_ = i;
<a name="l00244"></a>00244     this-&gt;n_ = i;
<a name="l00245"></a>00245 
<a name="l00246"></a>00246     this-&gt;data_ = data;
<a name="l00247"></a>00247   }
<a name="l00248"></a>00248 
<a name="l00249"></a>00249 
<a name="l00251"></a>00251 
<a name="l00255"></a>00255   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00256"></a>00256   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a4837520f92011b63c5115a19c8c49ad3" title="Clears the matrix without releasing memory.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Nullify</a>()
<a name="l00257"></a>00257   {
<a name="l00258"></a>00258     this-&gt;data_ = NULL;
<a name="l00259"></a>00259     this-&gt;m_ = 0;
<a name="l00260"></a>00260     this-&gt;n_ = 0;
<a name="l00261"></a>00261   }
<a name="l00262"></a>00262 
<a name="l00263"></a>00263 
<a name="l00264"></a>00264   <span class="comment">/**********************************</span>
<a name="l00265"></a>00265 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l00266"></a>00266 <span class="comment">   **********************************/</span>
<a name="l00267"></a>00267 
<a name="l00268"></a>00268 
<a name="l00270"></a>00270 
<a name="l00276"></a>00276   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00277"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a165b523d2483eaf716a43f2a89363d73">00277</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::value_type</a>
<a name="l00278"></a>00278   <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a165b523d2483eaf716a43f2a89363d73" title="Access operator.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00279"></a>00279 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a165b523d2483eaf716a43f2a89363d73" title="Access operator.">  ::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00280"></a>00280 <span class="keyword">  </span>{
<a name="l00281"></a>00281 
<a name="l00282"></a>00282 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00283"></a>00283 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00284"></a>00284       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_HermPacked::operator()&quot;</span>,
<a name="l00285"></a>00285                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00286"></a>00286                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00287"></a>00287     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00288"></a>00288       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_HermPacked::operator()&quot;</span>,
<a name="l00289"></a>00289                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00290"></a>00290                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00291"></a>00291 <span class="preprocessor">#endif</span>
<a name="l00292"></a>00292 <span class="preprocessor"></span>
<a name="l00293"></a>00293     <span class="keywordflow">if</span> (i &gt; j)
<a name="l00294"></a>00294       <span class="keywordflow">return</span> conj(this-&gt;data_[Storage::GetFirst(j * this-&gt;m_
<a name="l00295"></a>00295                                                 - (j*(j+1)) / 2 + i,
<a name="l00296"></a>00296                                                 (i*(i+1)) / 2 + j)]);
<a name="l00297"></a>00297     <span class="keywordflow">else</span>
<a name="l00298"></a>00298       <span class="keywordflow">return</span> this-&gt;data_[Storage::GetFirst(i * this-&gt;n_ - (i*(i+1)) / 2 + j,
<a name="l00299"></a>00299                                            (j*(j+1)) / 2 + i)];
<a name="l00300"></a>00300   }
<a name="l00301"></a>00301 
<a name="l00302"></a>00302 
<a name="l00304"></a>00304 
<a name="l00311"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ae7f67ca8d3876a5fbdbce8ac3c914f36">00311</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00312"></a>00312   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00313"></a>00313   <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ae7f67ca8d3876a5fbdbce8ac3c914f36" title="Direct access method.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00314"></a>00314   {
<a name="l00315"></a>00315 
<a name="l00316"></a>00316 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00317"></a>00317 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00318"></a>00318       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Val(int, int)&quot;</span>,
<a name="l00319"></a>00319                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00320"></a>00320                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00321"></a>00321     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00322"></a>00322       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Val(int, int)&quot;</span>,
<a name="l00323"></a>00323                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00324"></a>00324                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00325"></a>00325     <span class="keywordflow">if</span> (i &gt; j)
<a name="l00326"></a>00326       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Val(int, int)&quot;</span>,
<a name="l00327"></a>00327                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Attempted to access to element (&quot;</span>)
<a name="l00328"></a>00328                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00329"></a>00329                      + <span class="stringliteral">&quot;) but row index should not be strictly&quot;</span>
<a name="l00330"></a>00330                      + <span class="stringliteral">&quot; greater than column index.&quot;</span>);
<a name="l00331"></a>00331 <span class="preprocessor">#endif</span>
<a name="l00332"></a>00332 <span class="preprocessor"></span>
<a name="l00333"></a>00333     <span class="keywordflow">return</span> this-&gt;data_[Storage::GetFirst(i * this-&gt;n_ - (i*(i+1)) / 2 + j,
<a name="l00334"></a>00334                                          (j*(j+1)) / 2 + i)];
<a name="l00335"></a>00335   }
<a name="l00336"></a>00336 
<a name="l00337"></a>00337 
<a name="l00339"></a>00339 
<a name="l00346"></a>00346   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00347"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a7eda051ec34190ad13528f89d233acdb">00347</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00348"></a>00348   ::const_reference
<a name="l00349"></a>00349   <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ae7f67ca8d3876a5fbdbce8ac3c914f36" title="Direct access method.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00350"></a>00350 <span class="keyword">  </span>{
<a name="l00351"></a>00351 
<a name="l00352"></a>00352 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00353"></a>00353 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00354"></a>00354       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Val(int, int) const&quot;</span>,
<a name="l00355"></a>00355                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00356"></a>00356                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00357"></a>00357     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00358"></a>00358       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Val(int, int) cont&quot;</span>,
<a name="l00359"></a>00359                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00360"></a>00360                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00361"></a>00361     <span class="keywordflow">if</span> (i &gt; j)
<a name="l00362"></a>00362       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Val(int, int) const&quot;</span>,
<a name="l00363"></a>00363                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Attempted to access to element (&quot;</span>)
<a name="l00364"></a>00364                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00365"></a>00365                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;) but row index should not be strictly&quot;</span>)
<a name="l00366"></a>00366                      + <span class="stringliteral">&quot; greater than column index.&quot;</span>);
<a name="l00367"></a>00367 <span class="preprocessor">#endif</span>
<a name="l00368"></a>00368 <span class="preprocessor"></span>
<a name="l00369"></a>00369     <span class="keywordflow">return</span> this-&gt;data_[Storage::GetFirst(i * this-&gt;n_ - (i*(i+1)) / 2 + j,
<a name="l00370"></a>00370                                          (j*(j+1)) / 2 + i)];
<a name="l00371"></a>00371   }
<a name="l00372"></a>00372 
<a name="l00373"></a>00373 
<a name="l00375"></a>00375 
<a name="l00380"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#aea830c8b894aabb480c752eaaa67170a">00380</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00381"></a>00381   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00382"></a>00382   <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#aea830c8b894aabb480c752eaaa67170a" title="Returns access to an element (i, j).">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00383"></a>00383   {
<a name="l00384"></a>00384     <span class="keywordflow">return</span> this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ae7f67ca8d3876a5fbdbce8ac3c914f36" title="Direct access method.">Val</a>(i, j);
<a name="l00385"></a>00385   }
<a name="l00386"></a>00386 
<a name="l00387"></a>00387 
<a name="l00389"></a>00389 
<a name="l00394"></a>00394   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00395"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ab543dcda0c24ea3ab98dcbde52db84da">00395</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00396"></a>00396   ::const_reference
<a name="l00397"></a>00397   <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#aea830c8b894aabb480c752eaaa67170a" title="Returns access to an element (i, j).">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00398"></a>00398 <span class="keyword">  </span>{
<a name="l00399"></a>00399     <span class="keywordflow">return</span> this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ae7f67ca8d3876a5fbdbce8ac3c914f36" title="Direct access method.">Val</a>(i, j);
<a name="l00400"></a>00400   }
<a name="l00401"></a>00401 
<a name="l00402"></a>00402 
<a name="l00404"></a>00404 
<a name="l00409"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a804fa785b8d17fd2d56d01226bce844a">00409</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00410"></a>00410   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00411"></a>00411   <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a804fa785b8d17fd2d56d01226bce844a" title="Access to elements of the data array.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::operator[] </a>(<span class="keywordtype">int</span> i)
<a name="l00412"></a>00412   {
<a name="l00413"></a>00413 
<a name="l00414"></a>00414 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00415"></a>00415 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a8bd2747b6c90c4114cd2f54620d1f230" title="Returns the number of elements stored in memory.">GetDataSize</a>())
<a name="l00416"></a>00416       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Matrix_HermPacked::operator[] (int)&quot;</span>,
<a name="l00417"></a>00417                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00418"></a>00418                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a8bd2747b6c90c4114cd2f54620d1f230" title="Returns the number of elements stored in memory.">GetDataSize</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00419"></a>00419                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00420"></a>00420 <span class="preprocessor">#endif</span>
<a name="l00421"></a>00421 <span class="preprocessor"></span>
<a name="l00422"></a>00422     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00423"></a>00423   }
<a name="l00424"></a>00424 
<a name="l00425"></a>00425 
<a name="l00427"></a>00427 
<a name="l00432"></a>00432   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00433"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a77a7f1c5d26dc807a7fe5c7cebabd94b">00433</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00434"></a>00434   ::const_reference
<a name="l00435"></a>00435   <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a804fa785b8d17fd2d56d01226bce844a" title="Access to elements of the data array.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::operator[] </a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00436"></a>00436 <span class="keyword">  </span>{
<a name="l00437"></a>00437 
<a name="l00438"></a>00438 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00439"></a>00439 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a8bd2747b6c90c4114cd2f54620d1f230" title="Returns the number of elements stored in memory.">GetDataSize</a>())
<a name="l00440"></a>00440       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Matrix_HermPacked::operator[] (int) const&quot;</span>,
<a name="l00441"></a>00441                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00442"></a>00442                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a8bd2747b6c90c4114cd2f54620d1f230" title="Returns the number of elements stored in memory.">GetDataSize</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00443"></a>00443                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00444"></a>00444 <span class="preprocessor">#endif</span>
<a name="l00445"></a>00445 <span class="preprocessor"></span>
<a name="l00446"></a>00446     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00447"></a>00447   }
<a name="l00448"></a>00448 
<a name="l00449"></a>00449 
<a name="l00451"></a>00451 
<a name="l00456"></a>00456   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00457"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ad57f72a50f4f8bdb421c9f02251bde85">00457</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00458"></a>00458   <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ad57f72a50f4f8bdb421c9f02251bde85" title="Duplicates a matrix (assignment operator).">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00459"></a>00459 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ad57f72a50f4f8bdb421c9f02251bde85" title="Duplicates a matrix (assignment operator).">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00460"></a>00460   {
<a name="l00461"></a>00461     this-&gt;Copy(A);
<a name="l00462"></a>00462 
<a name="l00463"></a>00463     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00464"></a>00464   }
<a name="l00465"></a>00465 
<a name="l00466"></a>00466 
<a name="l00468"></a>00468 
<a name="l00473"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a7f2f874c147e1ee426c29439bc6b6708">00473</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00474"></a>00474   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a7f2f874c147e1ee426c29439bc6b6708" title="Sets an element of the matrix.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00475"></a>00475 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a7f2f874c147e1ee426c29439bc6b6708" title="Sets an element of the matrix.">  ::Set</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; x)
<a name="l00476"></a>00476   {
<a name="l00477"></a>00477     <span class="keywordflow">if</span> (i &gt; j)
<a name="l00478"></a>00478       this-&gt;Val(j, i) = conj(x);
<a name="l00479"></a>00479     <span class="keywordflow">else</span>
<a name="l00480"></a>00480       this-&gt;Val(i, j) = x;
<a name="l00481"></a>00481   }
<a name="l00482"></a>00482 
<a name="l00483"></a>00483 
<a name="l00485"></a>00485 
<a name="l00490"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a43f929eaa7b183e23c49d9ad31e5699e">00490</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00491"></a>00491   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a43f929eaa7b183e23c49d9ad31e5699e" title="Duplicates a matrix.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00492"></a>00492 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a43f929eaa7b183e23c49d9ad31e5699e" title="Duplicates a matrix.">  ::Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00493"></a>00493   {
<a name="l00494"></a>00494     this-&gt;Reallocate(A.<a class="code" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927" title="Returns the number of rows.">GetM</a>(), A.<a class="code" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984" title="Returns the number of columns.">GetN</a>());
<a name="l00495"></a>00495 
<a name="l00496"></a>00496     this-&gt;allocator_.memorycpy(this-&gt;data_, A.<a class="code" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a" title="Returns a pointer to the data array.">GetData</a>(), this-&gt;GetDataSize());
<a name="l00497"></a>00497   }
<a name="l00498"></a>00498 
<a name="l00499"></a>00499 
<a name="l00500"></a>00500   <span class="comment">/************************</span>
<a name="l00501"></a>00501 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l00502"></a>00502 <span class="comment">   ************************/</span>
<a name="l00503"></a>00503 
<a name="l00504"></a>00504 
<a name="l00506"></a>00506 
<a name="l00510"></a>00510   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00511"></a>00511   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a6222342fc8d2e7fccf530c892e259782" title="Sets all elements to zero.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Zero</a>()
<a name="l00512"></a>00512   {
<a name="l00513"></a>00513     this-&gt;allocator_.memoryset(this-&gt;data_, <span class="keywordtype">char</span>(0),
<a name="l00514"></a>00514                                this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a8bd2747b6c90c4114cd2f54620d1f230" title="Returns the number of elements stored in memory.">GetDataSize</a>() * <span class="keyword">sizeof</span>(value_type));
<a name="l00515"></a>00515   }
<a name="l00516"></a>00516 
<a name="l00517"></a>00517 
<a name="l00519"></a>00519   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00520"></a>00520   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a5e62a678cb672277c536d1ad9aad96f1" title="Sets the matrix to the identity.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::SetIdentity</a>()
<a name="l00521"></a>00521   {
<a name="l00522"></a>00522     this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#acb848b52cbdb82504a1e79fd5e75a87a" title="Fills the matrix with 0, 1, 2, ...">Fill</a>(T(0));
<a name="l00523"></a>00523 
<a name="l00524"></a>00524     T one;
<a name="l00525"></a>00525     SetComplexOne(one);
<a name="l00526"></a>00526     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; min(this-&gt;m_, this-&gt;n_); i++)
<a name="l00527"></a>00527       this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ae7f67ca8d3876a5fbdbce8ac3c914f36" title="Direct access method.">Val</a>(i,i) = one;
<a name="l00528"></a>00528   }
<a name="l00529"></a>00529 
<a name="l00530"></a>00530 
<a name="l00532"></a>00532 
<a name="l00536"></a>00536   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00537"></a>00537   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#acb848b52cbdb82504a1e79fd5e75a87a" title="Fills the matrix with 0, 1, 2, ...">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Fill</a>()
<a name="l00538"></a>00538   {
<a name="l00539"></a>00539     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a8bd2747b6c90c4114cd2f54620d1f230" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00540"></a>00540       this-&gt;data_[i] = i;
<a name="l00541"></a>00541   }
<a name="l00542"></a>00542 
<a name="l00543"></a>00543 
<a name="l00545"></a>00545 
<a name="l00550"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a498b54d93b35a42bfb524cc697bb8d5b">00550</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00551"></a>00551   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00552"></a>00552   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#acb848b52cbdb82504a1e79fd5e75a87a" title="Fills the matrix with 0, 1, 2, ...">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Fill</a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00553"></a>00553   {
<a name="l00554"></a>00554     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a8bd2747b6c90c4114cd2f54620d1f230" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00555"></a>00555       this-&gt;data_[i] = x;
<a name="l00556"></a>00556   }
<a name="l00557"></a>00557 
<a name="l00558"></a>00558 
<a name="l00560"></a>00560 
<a name="l00565"></a>00565   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00566"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a892540112f14801e1b48f3cf18b112ab">00566</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00567"></a>00567   <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00568"></a>00568   <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ad57f72a50f4f8bdb421c9f02251bde85" title="Duplicates a matrix (assignment operator).">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00569"></a>00569   {
<a name="l00570"></a>00570     this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#acb848b52cbdb82504a1e79fd5e75a87a" title="Fills the matrix with 0, 1, 2, ...">Fill</a>(x);
<a name="l00571"></a>00571 
<a name="l00572"></a>00572     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00573"></a>00573   }
<a name="l00574"></a>00574 
<a name="l00575"></a>00575 
<a name="l00577"></a>00577 
<a name="l00580"></a>00580   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00581"></a>00581   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a24f99cb9628f8259310d685a55ecbb86" title="Fills the matrix randomly.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::FillRand</a>()
<a name="l00582"></a>00582   {
<a name="l00583"></a>00583     srand(time(NULL));
<a name="l00584"></a>00584     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a8bd2747b6c90c4114cd2f54620d1f230" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00585"></a>00585       this-&gt;data_[i] = rand();
<a name="l00586"></a>00586   }
<a name="l00587"></a>00587 
<a name="l00588"></a>00588 
<a name="l00590"></a>00590 
<a name="l00595"></a>00595   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00596"></a>00596   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a149556a4c7e8b8e9c952a7f3bc2f5942" title="Displays the matrix on the standard output.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00597"></a>00597 <span class="keyword">  </span>{
<a name="l00598"></a>00598     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00599"></a>00599       {
<a name="l00600"></a>00600         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;n_; j++)
<a name="l00601"></a>00601           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00602"></a>00602         cout &lt;&lt; endl;
<a name="l00603"></a>00603       }
<a name="l00604"></a>00604   }
<a name="l00605"></a>00605 
<a name="l00606"></a>00606 
<a name="l00608"></a>00608 
<a name="l00619"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a379079777210c4dfd9377f5fbc843c85">00619</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00620"></a>00620   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a149556a4c7e8b8e9c952a7f3bc2f5942" title="Displays the matrix on the standard output.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00621"></a>00621 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a149556a4c7e8b8e9c952a7f3bc2f5942" title="Displays the matrix on the standard output.">  ::Print</a>(<span class="keywordtype">int</span> a, <span class="keywordtype">int</span> b, <span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n)<span class="keyword"> const</span>
<a name="l00622"></a>00622 <span class="keyword">  </span>{
<a name="l00623"></a>00623     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = a; i &lt; min(this-&gt;m_, a+m); i++)
<a name="l00624"></a>00624       {
<a name="l00625"></a>00625         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = b; j &lt; min(this-&gt;n_, b+n); j++)
<a name="l00626"></a>00626           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00627"></a>00627         cout &lt;&lt; endl;
<a name="l00628"></a>00628       }
<a name="l00629"></a>00629   }
<a name="l00630"></a>00630 
<a name="l00631"></a>00631 
<a name="l00633"></a>00633 
<a name="l00641"></a>00641   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00642"></a>00642   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a149556a4c7e8b8e9c952a7f3bc2f5942" title="Displays the matrix on the standard output.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Print</a>(<span class="keywordtype">int</span> l)<span class="keyword"> const</span>
<a name="l00643"></a>00643 <span class="keyword">  </span>{
<a name="l00644"></a>00644     <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a149556a4c7e8b8e9c952a7f3bc2f5942" title="Displays the matrix on the standard output.">Print</a>(0, 0, l, l);
<a name="l00645"></a>00645   }
<a name="l00646"></a>00646 
<a name="l00647"></a>00647 
<a name="l00648"></a>00648   <span class="comment">/**************************</span>
<a name="l00649"></a>00649 <span class="comment">   * INPUT/OUTPUT FUNCTIONS *</span>
<a name="l00650"></a>00650 <span class="comment">   **************************/</span>
<a name="l00651"></a>00651 
<a name="l00652"></a>00652 
<a name="l00654"></a>00654 
<a name="l00661"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a3a8a9450a83c1e5aaa301a8f33cc3eb1">00661</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00662"></a>00662   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a3a8a9450a83c1e5aaa301a8f33cc3eb1" title="Writes the matrix in a file.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00663"></a>00663 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a3a8a9450a83c1e5aaa301a8f33cc3eb1" title="Writes the matrix in a file.">  ::Write</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00664"></a>00664 <span class="keyword">  </span>{
<a name="l00665"></a>00665 
<a name="l00666"></a>00666     ofstream FileStream;
<a name="l00667"></a>00667     FileStream.open(FileName.c_str(), ofstream::binary);
<a name="l00668"></a>00668 
<a name="l00669"></a>00669 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00670"></a>00670 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00671"></a>00671     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00672"></a>00672       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Write(string FileName)&quot;</span>,
<a name="l00673"></a>00673                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00674"></a>00674 <span class="preprocessor">#endif</span>
<a name="l00675"></a>00675 <span class="preprocessor"></span>
<a name="l00676"></a>00676     this-&gt;Write(FileStream);
<a name="l00677"></a>00677 
<a name="l00678"></a>00678     FileStream.close();
<a name="l00679"></a>00679 
<a name="l00680"></a>00680   }
<a name="l00681"></a>00681 
<a name="l00682"></a>00682 
<a name="l00684"></a>00684 
<a name="l00691"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a57a7e0eb6fa3a34edfe3334ff460faec">00691</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00692"></a>00692   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a3a8a9450a83c1e5aaa301a8f33cc3eb1" title="Writes the matrix in a file.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00693"></a>00693 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a3a8a9450a83c1e5aaa301a8f33cc3eb1" title="Writes the matrix in a file.">  ::Write</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00694"></a>00694 <span class="keyword">  </span>{
<a name="l00695"></a>00695 
<a name="l00696"></a>00696 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00697"></a>00697 <span class="preprocessor"></span>    <span class="comment">// Checks if the file is ready.</span>
<a name="l00698"></a>00698     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00699"></a>00699       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l00700"></a>00700                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00701"></a>00701 <span class="preprocessor">#endif</span>
<a name="l00702"></a>00702 <span class="preprocessor"></span>
<a name="l00703"></a>00703     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l00704"></a>00704                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00705"></a>00705     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;n_)),
<a name="l00706"></a>00706                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00707"></a>00707 
<a name="l00708"></a>00708     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l00709"></a>00709                      this-&gt;GetDataSize() * <span class="keyword">sizeof</span>(value_type));
<a name="l00710"></a>00710 
<a name="l00711"></a>00711 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00712"></a>00712 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00713"></a>00713     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00714"></a>00714       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l00715"></a>00715                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Output operation failed.&quot;</span>)
<a name="l00716"></a>00716                     + string(<span class="stringliteral">&quot; The output file may have been removed&quot;</span>)
<a name="l00717"></a>00717                     + <span class="stringliteral">&quot; or there is no space left on device.&quot;</span>);
<a name="l00718"></a>00718 <span class="preprocessor">#endif</span>
<a name="l00719"></a>00719 <span class="preprocessor"></span>
<a name="l00720"></a>00720   }
<a name="l00721"></a>00721 
<a name="l00722"></a>00722 
<a name="l00724"></a>00724 
<a name="l00731"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a33aa63ce5334152d99d56de03a103f45">00731</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00732"></a>00732   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a33aa63ce5334152d99d56de03a103f45" title="Writes the matrix in a file.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00733"></a>00733 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a33aa63ce5334152d99d56de03a103f45" title="Writes the matrix in a file.">  ::WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00734"></a>00734 <span class="keyword">  </span>{
<a name="l00735"></a>00735     ofstream FileStream;
<a name="l00736"></a>00736     FileStream.precision(cout.precision());
<a name="l00737"></a>00737     FileStream.flags(cout.flags());
<a name="l00738"></a>00738     FileStream.open(FileName.c_str());
<a name="l00739"></a>00739 
<a name="l00740"></a>00740 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00741"></a>00741 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00742"></a>00742     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00743"></a>00743       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_HermPacked::WriteText(string FileName)&quot;</span>,
<a name="l00744"></a>00744                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00745"></a>00745 <span class="preprocessor">#endif</span>
<a name="l00746"></a>00746 <span class="preprocessor"></span>
<a name="l00747"></a>00747     this-&gt;WriteText(FileStream);
<a name="l00748"></a>00748 
<a name="l00749"></a>00749     FileStream.close();
<a name="l00750"></a>00750   }
<a name="l00751"></a>00751 
<a name="l00752"></a>00752 
<a name="l00754"></a>00754 
<a name="l00761"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a15b7c7126f1e630b644a29d0c1d38ecd">00761</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00762"></a>00762   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a33aa63ce5334152d99d56de03a103f45" title="Writes the matrix in a file.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00763"></a>00763 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a33aa63ce5334152d99d56de03a103f45" title="Writes the matrix in a file.">  ::WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00764"></a>00764 <span class="keyword">  </span>{
<a name="l00765"></a>00765 
<a name="l00766"></a>00766 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00767"></a>00767 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00768"></a>00768     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00769"></a>00769       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_HermPacked::WriteText(ofstream&amp; FileStream)&quot;</span>,
<a name="l00770"></a>00770                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00771"></a>00771 <span class="preprocessor">#endif</span>
<a name="l00772"></a>00772 <span class="preprocessor"></span>
<a name="l00773"></a>00773     <span class="keywordtype">int</span> i, j;
<a name="l00774"></a>00774     <span class="keywordflow">for</span> (i = 0; i &lt; this-&gt;GetM(); i++)
<a name="l00775"></a>00775       {
<a name="l00776"></a>00776         <span class="keywordflow">for</span> (j = 0; j &lt; this-&gt;GetN(); j++)
<a name="l00777"></a>00777           FileStream &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="charliteral">&#39;\t&#39;</span>;
<a name="l00778"></a>00778         FileStream &lt;&lt; endl;
<a name="l00779"></a>00779       }
<a name="l00780"></a>00780 
<a name="l00781"></a>00781 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00782"></a>00782 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00783"></a>00783     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00784"></a>00784       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_HermPacked::WriteText(ofstream&amp; FileStream)&quot;</span>,
<a name="l00785"></a>00785                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Output operation failed.&quot;</span>)
<a name="l00786"></a>00786                     + string(<span class="stringliteral">&quot; The output file may have been removed&quot;</span>)
<a name="l00787"></a>00787                     + <span class="stringliteral">&quot; or there is no space left on device.&quot;</span>);
<a name="l00788"></a>00788 <span class="preprocessor">#endif</span>
<a name="l00789"></a>00789 <span class="preprocessor"></span>
<a name="l00790"></a>00790   }
<a name="l00791"></a>00791 
<a name="l00792"></a>00792 
<a name="l00794"></a>00794 
<a name="l00801"></a>00801   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00802"></a>00802   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a19e44f4967fe55af83ef524f0f9be125" title="Reads the matrix from a file.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Read</a>(<span class="keywordtype">string</span> FileName)
<a name="l00803"></a>00803   {
<a name="l00804"></a>00804     ifstream FileStream;
<a name="l00805"></a>00805     FileStream.open(FileName.c_str(), ifstream::binary);
<a name="l00806"></a>00806 
<a name="l00807"></a>00807 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00808"></a>00808 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00809"></a>00809     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00810"></a>00810       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Read(string FileName)&quot;</span>,
<a name="l00811"></a>00811                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00812"></a>00812 <span class="preprocessor">#endif</span>
<a name="l00813"></a>00813 <span class="preprocessor"></span>
<a name="l00814"></a>00814     this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a19e44f4967fe55af83ef524f0f9be125" title="Reads the matrix from a file.">Read</a>(FileStream);
<a name="l00815"></a>00815 
<a name="l00816"></a>00816     FileStream.close();
<a name="l00817"></a>00817   }
<a name="l00818"></a>00818 
<a name="l00819"></a>00819 
<a name="l00821"></a>00821 
<a name="l00828"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a973fdab38a4f413ceec28d4c6b696d0c">00828</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00829"></a>00829   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a19e44f4967fe55af83ef524f0f9be125" title="Reads the matrix from a file.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00830"></a>00830 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a19e44f4967fe55af83ef524f0f9be125" title="Reads the matrix from a file.">  ::Read</a>(istream&amp; FileStream)
<a name="l00831"></a>00831   {
<a name="l00832"></a>00832 
<a name="l00833"></a>00833 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00834"></a>00834 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00835"></a>00835     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00836"></a>00836       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Read(ifstream&amp; FileStream)&quot;</span>,
<a name="l00837"></a>00837                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00838"></a>00838 <span class="preprocessor">#endif</span>
<a name="l00839"></a>00839 <span class="preprocessor"></span>
<a name="l00840"></a>00840     <span class="keywordtype">int</span> new_m, new_n;
<a name="l00841"></a>00841     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;new_m), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00842"></a>00842     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;new_n), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00843"></a>00843     this-&gt;Reallocate(new_m, new_n);
<a name="l00844"></a>00844 
<a name="l00845"></a>00845     FileStream.read(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l00846"></a>00846                     this-&gt;GetDataSize() * <span class="keyword">sizeof</span>(value_type));
<a name="l00847"></a>00847 
<a name="l00848"></a>00848 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00849"></a>00849 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l00850"></a>00850     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00851"></a>00851       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Read(ifstream&amp; FileStream)&quot;</span>,
<a name="l00852"></a>00852                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Input operation failed.&quot;</span>)
<a name="l00853"></a>00853                     + string(<span class="stringliteral">&quot; The input file may have been removed&quot;</span>)
<a name="l00854"></a>00854                     + <span class="stringliteral">&quot; or may not contain enough data.&quot;</span>);
<a name="l00855"></a>00855 <span class="preprocessor">#endif</span>
<a name="l00856"></a>00856 <span class="preprocessor"></span>
<a name="l00857"></a>00857   }
<a name="l00858"></a>00858 
<a name="l00859"></a>00859 
<a name="l00861"></a>00861 
<a name="l00865"></a>00865   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00866"></a>00866   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#aacf187c2102385c003476b6674d27fa1" title="Reads the matrix from a file.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::ReadText</a>(<span class="keywordtype">string</span> FileName)
<a name="l00867"></a>00867   {
<a name="l00868"></a>00868     ifstream FileStream;
<a name="l00869"></a>00869     FileStream.open(FileName.c_str());
<a name="l00870"></a>00870 
<a name="l00871"></a>00871 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00872"></a>00872 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00873"></a>00873     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00874"></a>00874       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(string FileName)&quot;</span>,
<a name="l00875"></a>00875                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00876"></a>00876 <span class="preprocessor">#endif</span>
<a name="l00877"></a>00877 <span class="preprocessor"></span>
<a name="l00878"></a>00878     this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#aacf187c2102385c003476b6674d27fa1" title="Reads the matrix from a file.">ReadText</a>(FileStream);
<a name="l00879"></a>00879 
<a name="l00880"></a>00880     FileStream.close();
<a name="l00881"></a>00881   }
<a name="l00882"></a>00882 
<a name="l00883"></a>00883 
<a name="l00885"></a>00885 
<a name="l00889"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a2c77fe462fbafce6965c88e55ebec79d">00889</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00890"></a>00890   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#aacf187c2102385c003476b6674d27fa1" title="Reads the matrix from a file.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00891"></a>00891 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#aacf187c2102385c003476b6674d27fa1" title="Reads the matrix from a file.">  ::ReadText</a>(istream&amp; FileStream)
<a name="l00892"></a>00892   {
<a name="l00893"></a>00893     <span class="comment">// clears previous matrix</span>
<a name="l00894"></a>00894     Clear();
<a name="l00895"></a>00895 
<a name="l00896"></a>00896 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00897"></a>00897 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00898"></a>00898     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00899"></a>00899       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(ifstream&amp; FileStream)&quot;</span>,
<a name="l00900"></a>00900                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00901"></a>00901 <span class="preprocessor">#endif</span>
<a name="l00902"></a>00902 <span class="preprocessor"></span>
<a name="l00903"></a>00903     <span class="comment">// we read first line</span>
<a name="l00904"></a>00904     <span class="keywordtype">string</span> line;
<a name="l00905"></a>00905     getline(FileStream, line);
<a name="l00906"></a>00906 
<a name="l00907"></a>00907     <span class="keywordflow">if</span> (FileStream.fail())
<a name="l00908"></a>00908       {
<a name="l00909"></a>00909         <span class="comment">// empty file ?</span>
<a name="l00910"></a>00910         <span class="keywordflow">return</span>;
<a name="l00911"></a>00911       }
<a name="l00912"></a>00912 
<a name="l00913"></a>00913     <span class="comment">// converting first line into a vector</span>
<a name="l00914"></a>00914     istringstream line_stream(line);
<a name="l00915"></a>00915     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> first_row;
<a name="l00916"></a>00916     first_row.ReadText(line_stream);
<a name="l00917"></a>00917 
<a name="l00918"></a>00918     <span class="comment">// and now the other rows</span>
<a name="l00919"></a>00919     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> other_rows;
<a name="l00920"></a>00920     other_rows.ReadText(FileStream);
<a name="l00921"></a>00921 
<a name="l00922"></a>00922     <span class="comment">// number of rows and columns</span>
<a name="l00923"></a>00923     <span class="keywordtype">int</span> n = first_row.GetM();
<a name="l00924"></a>00924     <span class="keywordtype">int</span> m = 1 + other_rows.GetM()/n;
<a name="l00925"></a>00925 
<a name="l00926"></a>00926 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00927"></a>00927 <span class="preprocessor"></span>    <span class="comment">// Checking number of elements</span>
<a name="l00928"></a>00928     <span class="keywordflow">if</span> (other_rows.GetM() != (m-1)*n)
<a name="l00929"></a>00929       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(ifstream&amp; FileStream)&quot;</span>,
<a name="l00930"></a>00930                     <span class="stringliteral">&quot;The file should contain same number of columns.&quot;</span>);
<a name="l00931"></a>00931 <span class="preprocessor">#endif</span>
<a name="l00932"></a>00932 <span class="preprocessor"></span>
<a name="l00933"></a>00933     this-&gt;Reallocate(m,n);
<a name="l00934"></a>00934     <span class="comment">// filling matrix</span>
<a name="l00935"></a>00935     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l00936"></a>00936       this-&gt;Val(0, j) = first_row(j);
<a name="l00937"></a>00937 
<a name="l00938"></a>00938     <span class="keywordtype">int</span> nb = 0;
<a name="l00939"></a>00939     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 1; i &lt; m; i++)
<a name="l00940"></a>00940       {
<a name="l00941"></a>00941         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; i; j++)
<a name="l00942"></a>00942           nb++;
<a name="l00943"></a>00943 
<a name="l00944"></a>00944         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = i; j &lt; n; j++)
<a name="l00945"></a>00945           this-&gt;Val(i, j) = other_rows(nb++);
<a name="l00946"></a>00946       }
<a name="l00947"></a>00947   }
<a name="l00948"></a>00948 
<a name="l00949"></a>00949 
<a name="l00950"></a>00950 
<a name="l00952"></a>00952   <span class="comment">// MATRIX&lt;COLHERMPACKED&gt; //</span>
<a name="l00954"></a>00954 <span class="comment"></span>
<a name="l00955"></a>00955 
<a name="l00956"></a>00956   <span class="comment">/****************</span>
<a name="l00957"></a>00957 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00958"></a>00958 <span class="comment">   ****************/</span>
<a name="l00959"></a>00959 
<a name="l00960"></a>00960 
<a name="l00962"></a>00962 
<a name="l00965"></a>00965   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00966"></a>00966   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColHermPacked, Allocator&gt;::Matrix</a>():
<a name="l00967"></a>00967     <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_herm_packed.php">ColHermPacked</a>, Allocator&gt;()
<a name="l00968"></a>00968   {
<a name="l00969"></a>00969   }
<a name="l00970"></a>00970 
<a name="l00971"></a>00971 
<a name="l00973"></a>00973 
<a name="l00978"></a>00978   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00979"></a>00979   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php#a45762f0c3a34c78f8d6e99b74f269e1f" title="Default constructor.">Matrix&lt;T, Prop, ColHermPacked, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l00980"></a>00980     Matrix_HermPacked&lt;T, Prop, ColHermPacked, Allocator&gt;(i, j)
<a name="l00981"></a>00981   {
<a name="l00982"></a>00982   }
<a name="l00983"></a>00983 
<a name="l00984"></a>00984 
<a name="l00985"></a>00985   <span class="comment">/*******************</span>
<a name="l00986"></a>00986 <span class="comment">   * OTHER FUNCTIONS *</span>
<a name="l00987"></a>00987 <span class="comment">   *******************/</span>
<a name="l00988"></a>00988 
<a name="l00989"></a>00989 
<a name="l00991"></a>00991 
<a name="l00994"></a>00994   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00995"></a>00995   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00996"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php#ad8f1511c8abd5cf290c32c1ec43dad01">00996</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php" title="Column-major hermitian packed matrix class.">Matrix&lt;T, Prop, ColHermPacked, Allocator&gt;</a>&amp;
<a name="l00997"></a>00997   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColHermPacked, Allocator&gt;</a>
<a name="l00998"></a>00998 <a class="code" href="class_seldon_1_1_matrix.php">  ::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00999"></a>00999   {
<a name="l01000"></a>01000     this-&gt;Fill(x);
<a name="l01001"></a>01001 
<a name="l01002"></a>01002     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01003"></a>01003   }
<a name="l01004"></a>01004 
<a name="l01005"></a>01005 
<a name="l01007"></a>01007 
<a name="l01012"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php#a57d35e0f5978a4b9868fb98cc7fa247e">01012</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01013"></a>01013   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php" title="Column-major hermitian packed matrix class.">Matrix&lt;T, Prop, ColHermPacked, Allocator&gt;</a>&amp;
<a name="l01014"></a>01014   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColHermPacked, Allocator&gt;::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop,
<a name="l01015"></a>01015                                                        <a class="code" href="class_seldon_1_1_col_herm_packed.php">ColHermPacked</a>,
<a name="l01016"></a>01016                                                        Allocator&gt;&amp; A)
<a name="l01017"></a>01017   {
<a name="l01018"></a>01018     this-&gt;Copy(A);
<a name="l01019"></a>01019     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01020"></a>01020   }
<a name="l01021"></a>01021 
<a name="l01022"></a>01022 
<a name="l01024"></a>01024 
<a name="l01027"></a>01027   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01028"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php#a53e8c9f819dad5bb1d40a9b6edd24005">01028</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01029"></a>01029   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php" title="Column-major hermitian packed matrix class.">Matrix&lt;T, Prop, ColHermPacked, Allocator&gt;</a>&amp;
<a name="l01030"></a>01030   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColHermPacked, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01031"></a>01031   {
<a name="l01032"></a>01032     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetDataSize();i++)
<a name="l01033"></a>01033       this-&gt;data_[i] *= x;
<a name="l01034"></a>01034 
<a name="l01035"></a>01035     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01036"></a>01036   }
<a name="l01037"></a>01037 
<a name="l01038"></a>01038 
<a name="l01040"></a>01040 
<a name="l01047"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php#a46426f0740af4a12d2873b9b692f6278">01047</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01048"></a>01048   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColHermPacked, Allocator&gt;</a>
<a name="l01049"></a>01049 <a class="code" href="class_seldon_1_1_matrix.php">  ::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01050"></a>01050   {
<a name="l01051"></a>01051 
<a name="l01052"></a>01052     <span class="comment">// Storing the old values of the matrix.</span>
<a name="l01053"></a>01053     <span class="keywordtype">int</span> nold = this-&gt;GetDataSize();
<a name="l01054"></a>01054     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> xold(nold);
<a name="l01055"></a>01055     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; nold; k++)
<a name="l01056"></a>01056       xold(k) = this-&gt;data_[k];
<a name="l01057"></a>01057 
<a name="l01058"></a>01058     <span class="comment">// Reallocation.</span>
<a name="l01059"></a>01059     this-&gt;Reallocate(i, j);
<a name="l01060"></a>01060 
<a name="l01061"></a>01061     <span class="comment">// Filling the matrix with its old values.</span>
<a name="l01062"></a>01062     <span class="keywordtype">int</span> nmin = min(nold, this-&gt;GetDataSize());
<a name="l01063"></a>01063     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; nmin; k++)
<a name="l01064"></a>01064       this-&gt;data_[k] = xold(k);
<a name="l01065"></a>01065   }
<a name="l01066"></a>01066 
<a name="l01067"></a>01067 
<a name="l01069"></a>01069   <span class="comment">// MATRIX&lt;ROWHERMPACKED&gt; //</span>
<a name="l01071"></a>01071 <span class="comment"></span>
<a name="l01072"></a>01072 
<a name="l01073"></a>01073   <span class="comment">/****************</span>
<a name="l01074"></a>01074 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01075"></a>01075 <span class="comment">   ****************/</span>
<a name="l01076"></a>01076 
<a name="l01077"></a>01077 
<a name="l01079"></a>01079 
<a name="l01082"></a>01082   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01083"></a>01083   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowHermPacked, Allocator&gt;::Matrix</a>():
<a name="l01084"></a>01084     <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_herm_packed.php">RowHermPacked</a>, Allocator&gt;()
<a name="l01085"></a>01085   {
<a name="l01086"></a>01086   }
<a name="l01087"></a>01087 
<a name="l01088"></a>01088 
<a name="l01090"></a>01090 
<a name="l01095"></a>01095   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01096"></a>01096   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_packed_00_01_allocator_01_4.php#ab2ca86f35ad38650084f05f6b15ca476" title="Default constructor.">Matrix&lt;T, Prop, RowHermPacked, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01097"></a>01097     Matrix_HermPacked&lt;T, Prop, RowHermPacked, Allocator&gt;(i, j)
<a name="l01098"></a>01098   {
<a name="l01099"></a>01099   }
<a name="l01100"></a>01100 
<a name="l01101"></a>01101 
<a name="l01102"></a>01102   <span class="comment">/*******************</span>
<a name="l01103"></a>01103 <span class="comment">   * OTHER FUNCTIONS *</span>
<a name="l01104"></a>01104 <span class="comment">   *******************/</span>
<a name="l01105"></a>01105 
<a name="l01106"></a>01106 
<a name="l01108"></a>01108 
<a name="l01111"></a>01111   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01112"></a>01112   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01113"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_packed_00_01_allocator_01_4.php#a4edcaa25fa32c27fa993e46fcc3472a0">01113</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_packed_00_01_allocator_01_4.php" title="Row-major hermitian packed matrix class.">Matrix&lt;T, Prop, RowHermPacked, Allocator&gt;</a>&amp;
<a name="l01114"></a>01114   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowHermPacked, Allocator&gt;</a>
<a name="l01115"></a>01115 <a class="code" href="class_seldon_1_1_matrix.php">  ::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01116"></a>01116   {
<a name="l01117"></a>01117     this-&gt;Fill(x);
<a name="l01118"></a>01118 
<a name="l01119"></a>01119     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01120"></a>01120   }
<a name="l01121"></a>01121 
<a name="l01122"></a>01122 
<a name="l01124"></a>01124 
<a name="l01129"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_packed_00_01_allocator_01_4.php#ae5ab919b068ca05a5d551b8573c025b9">01129</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01130"></a>01130   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_packed_00_01_allocator_01_4.php" title="Row-major hermitian packed matrix class.">Matrix&lt;T, Prop, RowHermPacked, Allocator&gt;</a>&amp;
<a name="l01131"></a>01131   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowHermPacked, Allocator&gt;::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop,
<a name="l01132"></a>01132                                                        <a class="code" href="class_seldon_1_1_row_herm_packed.php">RowHermPacked</a>,
<a name="l01133"></a>01133                                                        Allocator&gt;&amp; A)
<a name="l01134"></a>01134   {
<a name="l01135"></a>01135     this-&gt;Copy(A);
<a name="l01136"></a>01136     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01137"></a>01137   }
<a name="l01138"></a>01138 
<a name="l01139"></a>01139 
<a name="l01141"></a>01141 
<a name="l01144"></a>01144   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01145"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_packed_00_01_allocator_01_4.php#a9079c6a12adc5c1b0ef054bc03bdf5b6">01145</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01146"></a>01146   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_packed_00_01_allocator_01_4.php" title="Row-major hermitian packed matrix class.">Matrix&lt;T, Prop, RowHermPacked, Allocator&gt;</a>&amp;
<a name="l01147"></a>01147   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowHermPacked, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01148"></a>01148   {
<a name="l01149"></a>01149     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetDataSize();i++)
<a name="l01150"></a>01150       this-&gt;data_[i] *= x;
<a name="l01151"></a>01151 
<a name="l01152"></a>01152     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01153"></a>01153   }
<a name="l01154"></a>01154 
<a name="l01155"></a>01155 
<a name="l01157"></a>01157 
<a name="l01164"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_packed_00_01_allocator_01_4.php#aa4e1fe59977fb95839539692ff8ed832">01164</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01165"></a>01165   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowHermPacked, Allocator&gt;</a>
<a name="l01166"></a>01166 <a class="code" href="class_seldon_1_1_matrix.php">  ::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01167"></a>01167   {
<a name="l01168"></a>01168     <span class="comment">// Storing the old values of the matrix.</span>
<a name="l01169"></a>01169     <span class="keywordtype">int</span> nold = this-&gt;GetDataSize(), iold = this-&gt;m_;
<a name="l01170"></a>01170     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> xold(nold);
<a name="l01171"></a>01171     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; nold; k++)
<a name="l01172"></a>01172       xold(k) = this-&gt;data_[k];
<a name="l01173"></a>01173 
<a name="l01174"></a>01174     <span class="comment">// Reallocation.</span>
<a name="l01175"></a>01175     this-&gt;Reallocate(i, j);
<a name="l01176"></a>01176 
<a name="l01177"></a>01177     <span class="comment">// Filling the matrix with its old values.</span>
<a name="l01178"></a>01178     <span class="keywordtype">int</span> imin = min(iold, i);
<a name="l01179"></a>01179     nold = 0;
<a name="l01180"></a>01180     <span class="keywordtype">int</span> n = 0;
<a name="l01181"></a>01181     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; imin; k++)
<a name="l01182"></a>01182       {
<a name="l01183"></a>01183         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> l = k; l &lt; imin; l++)
<a name="l01184"></a>01184           this-&gt;data_[n+l-k] = xold(nold+l-k);
<a name="l01185"></a>01185 
<a name="l01186"></a>01186         n += i - k;
<a name="l01187"></a>01187         nold += iold - k;
<a name="l01188"></a>01188       }
<a name="l01189"></a>01189   }
<a name="l01190"></a>01190 
<a name="l01191"></a>01191 
<a name="l01192"></a>01192 } <span class="comment">// namespace Seldon.</span>
<a name="l01193"></a>01193 
<a name="l01194"></a>01194 <span class="preprocessor">#define SELDON_FILE_MATRIX_HERMPACKED_CXX</span>
<a name="l01195"></a>01195 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
