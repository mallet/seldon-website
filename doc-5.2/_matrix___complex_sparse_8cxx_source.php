<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix_sparse/Matrix_ComplexSparse.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_COMPLEXSPARSE_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;Matrix_ComplexSparse.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="keyword">namespace </span>Seldon
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 
<a name="l00029"></a>00029   <span class="comment">/****************</span>
<a name="l00030"></a>00030 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00031"></a>00031 <span class="comment">   ****************/</span>
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00035"></a>00035 
<a name="l00038"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9">00038</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00039"></a>00039   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00040"></a>00040 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">  ::Matrix_ComplexSparse</a>(): <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;()
<a name="l00041"></a>00041   {
<a name="l00042"></a>00042     real_nz_ = 0;
<a name="l00043"></a>00043     imag_nz_ = 0;
<a name="l00044"></a>00044     real_ptr_ = NULL;
<a name="l00045"></a>00045     imag_ptr_ = NULL;
<a name="l00046"></a>00046     real_ind_ = NULL;
<a name="l00047"></a>00047     imag_ind_ = NULL;
<a name="l00048"></a>00048     real_data_ = NULL;
<a name="l00049"></a>00049     imag_data_ = NULL;
<a name="l00050"></a>00050   }
<a name="l00051"></a>00051 
<a name="l00052"></a>00052 
<a name="l00054"></a>00054 
<a name="l00059"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a0105ca0b54784973e1272931f9c9e420">00059</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00060"></a>00060   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00061"></a>00061 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">  ::Matrix_ComplexSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j): <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;()
<a name="l00062"></a>00062   {
<a name="l00063"></a>00063     real_nz_ = 0;
<a name="l00064"></a>00064     imag_nz_ = 0;
<a name="l00065"></a>00065     real_ptr_ = NULL;
<a name="l00066"></a>00066     imag_ptr_ = NULL;
<a name="l00067"></a>00067     real_ind_ = NULL;
<a name="l00068"></a>00068     imag_ind_ = NULL;
<a name="l00069"></a>00069     real_data_ = NULL;
<a name="l00070"></a>00070     imag_data_ = NULL;
<a name="l00071"></a>00071 
<a name="l00072"></a>00072     Reallocate(i, j);
<a name="l00073"></a>00073   }
<a name="l00074"></a>00074 
<a name="l00075"></a>00075 
<a name="l00077"></a>00077 
<a name="l00087"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a728b0dd58af72224fc92a6ab299ad274">00087</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00088"></a>00088   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00089"></a>00089 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">  Matrix_ComplexSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> real_nz, <span class="keywordtype">int</span> imag_nz):
<a name="l00090"></a>00090     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;()
<a name="l00091"></a>00091   {
<a name="l00092"></a>00092     real_nz_ = 0;
<a name="l00093"></a>00093     imag_nz_ = 0;
<a name="l00094"></a>00094     real_ptr_ = NULL;
<a name="l00095"></a>00095     imag_ptr_ = NULL;
<a name="l00096"></a>00096     real_ind_ = NULL;
<a name="l00097"></a>00097     imag_ind_ = NULL;
<a name="l00098"></a>00098     real_data_ = NULL;
<a name="l00099"></a>00099     imag_data_ = NULL;
<a name="l00100"></a>00100 
<a name="l00101"></a>00101     <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a72602182671e7f970959772510eed557" title="Initialization of an empty matrix i x j.">Reallocate</a>(i, j, real_nz, imag_nz);
<a name="l00102"></a>00102   }
<a name="l00103"></a>00103 
<a name="l00104"></a>00104 
<a name="l00106"></a>00106 
<a name="l00124"></a>00124   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00125"></a>00125   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00126"></a>00126             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00127"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a079f7006a23ebdc5e63c2337f88f9bfc">00127</a>             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00128"></a>00128   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00129"></a>00129 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">  Matrix_ComplexSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00130"></a>00130                        <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; real_values,
<a name="l00131"></a>00131                        <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; real_ptr,
<a name="l00132"></a>00132                        <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; real_ind,
<a name="l00133"></a>00133                        <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; imag_values,
<a name="l00134"></a>00134                        <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; imag_ptr,
<a name="l00135"></a>00135                        <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; imag_ind):
<a name="l00136"></a>00136     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, j)
<a name="l00137"></a>00137   {
<a name="l00138"></a>00138 
<a name="l00139"></a>00139     real_nz_ = real_values.GetLength();
<a name="l00140"></a>00140     imag_nz_ = imag_values.GetLength();
<a name="l00141"></a>00141 
<a name="l00142"></a>00142 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00143"></a>00143 <span class="preprocessor"></span>    <span class="comment">// Checks whether vector sizes are acceptable.</span>
<a name="l00144"></a>00144 
<a name="l00145"></a>00145     <span class="keywordflow">if</span> (real_ind.GetLength() != real_nz_)
<a name="l00146"></a>00146       {
<a name="l00147"></a>00147         this-&gt;m_ = 0;
<a name="l00148"></a>00148         this-&gt;n_ = 0;
<a name="l00149"></a>00149         real_nz_ = 0;
<a name="l00150"></a>00150         imag_nz_ = 0;
<a name="l00151"></a>00151         real_ptr_ = NULL;
<a name="l00152"></a>00152         imag_ptr_ = NULL;
<a name="l00153"></a>00153         real_ind_ = NULL;
<a name="l00154"></a>00154         imag_ind_ = NULL;
<a name="l00155"></a>00155         this-&gt;real_data_ = NULL;
<a name="l00156"></a>00156         this-&gt;imag_data_ = NULL;
<a name="l00157"></a>00157         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00158"></a>00158                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, &quot;</span>)
<a name="l00159"></a>00159                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00160"></a>00160                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00161"></a>00161                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz_)
<a name="l00162"></a>00162                        + <span class="stringliteral">&quot; values (real part) but &quot;</span>
<a name="l00163"></a>00163                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_ind.GetLength())
<a name="l00164"></a>00164                        + <span class="stringliteral">&quot; row or column indices.&quot;</span>);
<a name="l00165"></a>00165       }
<a name="l00166"></a>00166 
<a name="l00167"></a>00167     <span class="keywordflow">if</span> (imag_ind.GetLength() != imag_nz_)
<a name="l00168"></a>00168       {
<a name="l00169"></a>00169         this-&gt;m_ = 0;
<a name="l00170"></a>00170         this-&gt;n_ = 0;
<a name="l00171"></a>00171         real_nz_ = 0;
<a name="l00172"></a>00172         imag_nz_ = 0;
<a name="l00173"></a>00173         real_ptr_ = NULL;
<a name="l00174"></a>00174         imag_ptr_ = NULL;
<a name="l00175"></a>00175         real_ind_ = NULL;
<a name="l00176"></a>00176         imag_ind_ = NULL;
<a name="l00177"></a>00177         this-&gt;real_data_ = NULL;
<a name="l00178"></a>00178         this-&gt;imag_data_ = NULL;
<a name="l00179"></a>00179         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00180"></a>00180                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, &quot;</span>)
<a name="l00181"></a>00181                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00182"></a>00182                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00183"></a>00183                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz_)
<a name="l00184"></a>00184                        + <span class="stringliteral">&quot; values (imaginary part) but &quot;</span>
<a name="l00185"></a>00185                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_ind.GetLength())
<a name="l00186"></a>00186                        + <span class="stringliteral">&quot; row or column indices.&quot;</span>);
<a name="l00187"></a>00187       }
<a name="l00188"></a>00188 
<a name="l00189"></a>00189     <span class="keywordflow">if</span> (real_ptr.GetLength()-1 != Storage::GetFirst(i, j))
<a name="l00190"></a>00190       {
<a name="l00191"></a>00191         this-&gt;m_ = 0;
<a name="l00192"></a>00192         this-&gt;n_ = 0;
<a name="l00193"></a>00193         real_nz_ = 0;
<a name="l00194"></a>00194         imag_nz_ = 0;
<a name="l00195"></a>00195         real_ptr_ = NULL;
<a name="l00196"></a>00196         imag_ptr_ = NULL;
<a name="l00197"></a>00197         real_ind_ = NULL;
<a name="l00198"></a>00198         imag_ind_ = NULL;
<a name="l00199"></a>00199         this-&gt;real_data_ = NULL;
<a name="l00200"></a>00200         this-&gt;imag_data_ = NULL;
<a name="l00201"></a>00201         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00202"></a>00202                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, &quot;</span>)
<a name="l00203"></a>00203                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00204"></a>00204                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00205"></a>00205                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The vector of start indices (real part)&quot;</span>)
<a name="l00206"></a>00206                        + <span class="stringliteral">&quot; contains &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_ptr.GetLength()-1)
<a name="l00207"></a>00207                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (plus the&quot;</span>)
<a name="l00208"></a>00208                        + <span class="stringliteral">&quot; number of non-zero entries) but there are &quot;</span>
<a name="l00209"></a>00209                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j))
<a name="l00210"></a>00210                        + <span class="stringliteral">&quot; rows or columns (&quot;</span>
<a name="l00211"></a>00211                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix).&quot;</span>);
<a name="l00212"></a>00212       }
<a name="l00213"></a>00213 
<a name="l00214"></a>00214     <span class="keywordflow">if</span> (imag_ptr.GetLength()-1 != Storage::GetFirst(i, j))
<a name="l00215"></a>00215       {
<a name="l00216"></a>00216         this-&gt;m_ = 0;
<a name="l00217"></a>00217         this-&gt;n_ = 0;
<a name="l00218"></a>00218         real_nz_ = 0;
<a name="l00219"></a>00219         imag_nz_ = 0;
<a name="l00220"></a>00220         real_ptr_ = NULL;
<a name="l00221"></a>00221         imag_ptr_ = NULL;
<a name="l00222"></a>00222         real_ind_ = NULL;
<a name="l00223"></a>00223         imag_ind_ = NULL;
<a name="l00224"></a>00224         this-&gt;real_data_ = NULL;
<a name="l00225"></a>00225         this-&gt;imag_data_ = NULL;
<a name="l00226"></a>00226         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00227"></a>00227                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, &quot;</span>)
<a name="l00228"></a>00228                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00229"></a>00229                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00230"></a>00230                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The vector of start indices (imaginary part)&quot;</span>)
<a name="l00231"></a>00231                        + <span class="stringliteral">&quot; contains &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_ptr.GetLength()-1)
<a name="l00232"></a>00232                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (plus the&quot;</span>)
<a name="l00233"></a>00233                        + <span class="stringliteral">&quot; number of non-zero entries) but there are &quot;</span>
<a name="l00234"></a>00234                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j))
<a name="l00235"></a>00235                        + <span class="stringliteral">&quot; rows or columns (&quot;</span>
<a name="l00236"></a>00236                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix).&quot;</span>);
<a name="l00237"></a>00237       }
<a name="l00238"></a>00238 
<a name="l00239"></a>00239     <span class="keywordflow">if</span> ((real_nz_ &gt; 0
<a name="l00240"></a>00240          &amp;&amp; (j == 0
<a name="l00241"></a>00241              || static_cast&lt;long int&gt;(real_nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l00242"></a>00242              &gt;= static_cast&lt;long int&gt;(i)))
<a name="l00243"></a>00243         ||
<a name="l00244"></a>00244         (imag_nz_ &gt; 0
<a name="l00245"></a>00245          &amp;&amp; (j == 0
<a name="l00246"></a>00246              || <span class="keyword">static_cast&lt;</span><span class="keywordtype">long</span> <span class="keywordtype">int</span><span class="keyword">&gt;</span>(imag_nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l00247"></a>00247              &gt;= <span class="keyword">static_cast&lt;</span><span class="keywordtype">long</span> <span class="keywordtype">int</span><span class="keyword">&gt;</span>(i))))
<a name="l00248"></a>00248       {
<a name="l00249"></a>00249         this-&gt;m_ = 0;
<a name="l00250"></a>00250         this-&gt;n_ = 0;
<a name="l00251"></a>00251         real_nz_ = 0;
<a name="l00252"></a>00252         imag_nz_ = 0;
<a name="l00253"></a>00253         real_ptr_ = NULL;
<a name="l00254"></a>00254         imag_ptr_ = NULL;
<a name="l00255"></a>00255         real_ind_ = NULL;
<a name="l00256"></a>00256         imag_ind_ = NULL;
<a name="l00257"></a>00257         this-&gt;real_data_ = NULL;
<a name="l00258"></a>00258         this-&gt;imag_data_ = NULL;
<a name="l00259"></a>00259         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00260"></a>00260                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, &quot;</span>)
<a name="l00261"></a>00261                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00262"></a>00262                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00263"></a>00263                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>)
<a name="l00264"></a>00264                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_values.GetLength())
<a name="l00265"></a>00265                        + <span class="stringliteral">&quot; values for the real part and &quot;</span>
<a name="l00266"></a>00266                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_values.GetLength()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; values&quot;</span>)
<a name="l00267"></a>00267                        + string(<span class="stringliteral">&quot; for the imaginary part) than elements&quot;</span>)
<a name="l00268"></a>00268                        + <span class="stringliteral">&quot; in the matrix (&quot;</span>
<a name="l00269"></a>00269                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00270"></a>00270       }
<a name="l00271"></a>00271 <span class="preprocessor">#endif</span>
<a name="l00272"></a>00272 <span class="preprocessor"></span>
<a name="l00273"></a>00273     this-&gt;real_ptr_ = real_ptr.GetData();
<a name="l00274"></a>00274     this-&gt;imag_ptr_ = imag_ptr.GetData();
<a name="l00275"></a>00275     this-&gt;real_ind_ = real_ind.GetData();
<a name="l00276"></a>00276     this-&gt;imag_ind_ = imag_ind.GetData();
<a name="l00277"></a>00277     this-&gt;real_data_ = real_values.GetData();
<a name="l00278"></a>00278     this-&gt;imag_data_ = imag_values.GetData();
<a name="l00279"></a>00279 
<a name="l00280"></a>00280     real_ptr.Nullify();
<a name="l00281"></a>00281     imag_ptr.Nullify();
<a name="l00282"></a>00282     real_ind.Nullify();
<a name="l00283"></a>00283     imag_ind.Nullify();
<a name="l00284"></a>00284     real_values.Nullify();
<a name="l00285"></a>00285     imag_values.Nullify();
<a name="l00286"></a>00286   }
<a name="l00287"></a>00287 
<a name="l00288"></a>00288 
<a name="l00290"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3df0b27785c83bf1b8809cdd263c48c5">00290</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00291"></a>00291   <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00292"></a>00292 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">  ::Matrix_ComplexSparse</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse</a>&lt;T, Prop,
<a name="l00293"></a>00293                          Storage, Allocator&gt;&amp; A)
<a name="l00294"></a>00294   {
<a name="l00295"></a>00295     this-&gt;m_ = 0;
<a name="l00296"></a>00296     this-&gt;n_ = 0;
<a name="l00297"></a>00297     real_nz_ = 0;
<a name="l00298"></a>00298     imag_nz_ = 0;
<a name="l00299"></a>00299     real_ptr_ = NULL;
<a name="l00300"></a>00300     imag_ptr_ = NULL;
<a name="l00301"></a>00301     real_ind_ = NULL;
<a name="l00302"></a>00302     imag_ind_ = NULL;
<a name="l00303"></a>00303     real_data_ = NULL;
<a name="l00304"></a>00304     imag_data_ = NULL;
<a name="l00305"></a>00305 
<a name="l00306"></a>00306     this-&gt;Copy(A);
<a name="l00307"></a>00307   }
<a name="l00308"></a>00308 
<a name="l00309"></a>00309 
<a name="l00310"></a>00310   <span class="comment">/**************</span>
<a name="l00311"></a>00311 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00312"></a>00312 <span class="comment">   **************/</span>
<a name="l00313"></a>00313 
<a name="l00314"></a>00314 
<a name="l00316"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3b785e50c55d9c0e00180bc7d3332d75">00316</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00317"></a>00317   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3b785e50c55d9c0e00180bc7d3332d75" title="Destructor.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00318"></a>00318 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3b785e50c55d9c0e00180bc7d3332d75" title="Destructor.">  ::~Matrix_ComplexSparse</a>()
<a name="l00319"></a>00319   {
<a name="l00320"></a>00320     this-&gt;m_ = 0;
<a name="l00321"></a>00321     this-&gt;n_ = 0;
<a name="l00322"></a>00322 
<a name="l00323"></a>00323 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00324"></a>00324 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00325"></a>00325       {
<a name="l00326"></a>00326 <span class="preprocessor">#endif</span>
<a name="l00327"></a>00327 <span class="preprocessor"></span>
<a name="l00328"></a>00328         <span class="keywordflow">if</span> (real_ptr_ != NULL)
<a name="l00329"></a>00329           {
<a name="l00330"></a>00330             free(real_ptr_);
<a name="l00331"></a>00331             real_ptr_ = NULL;
<a name="l00332"></a>00332           }
<a name="l00333"></a>00333 
<a name="l00334"></a>00334 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00335"></a>00335 <span class="preprocessor"></span>      }
<a name="l00336"></a>00336     <span class="keywordflow">catch</span> (...)
<a name="l00337"></a>00337       {
<a name="l00338"></a>00338         real_ptr_ = NULL;
<a name="l00339"></a>00339       }
<a name="l00340"></a>00340 <span class="preprocessor">#endif</span>
<a name="l00341"></a>00341 <span class="preprocessor"></span>
<a name="l00342"></a>00342 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00343"></a>00343 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00344"></a>00344       {
<a name="l00345"></a>00345 <span class="preprocessor">#endif</span>
<a name="l00346"></a>00346 <span class="preprocessor"></span>
<a name="l00347"></a>00347         <span class="keywordflow">if</span> (imag_ptr_ != NULL)
<a name="l00348"></a>00348           {
<a name="l00349"></a>00349             free(imag_ptr_);
<a name="l00350"></a>00350             imag_ptr_ = NULL;
<a name="l00351"></a>00351           }
<a name="l00352"></a>00352 
<a name="l00353"></a>00353 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00354"></a>00354 <span class="preprocessor"></span>      }
<a name="l00355"></a>00355     <span class="keywordflow">catch</span> (...)
<a name="l00356"></a>00356       {
<a name="l00357"></a>00357         imag_ptr_ = NULL;
<a name="l00358"></a>00358       }
<a name="l00359"></a>00359 <span class="preprocessor">#endif</span>
<a name="l00360"></a>00360 <span class="preprocessor"></span>
<a name="l00361"></a>00361 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00362"></a>00362 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00363"></a>00363       {
<a name="l00364"></a>00364 <span class="preprocessor">#endif</span>
<a name="l00365"></a>00365 <span class="preprocessor"></span>
<a name="l00366"></a>00366         <span class="keywordflow">if</span> (real_ind_ != NULL)
<a name="l00367"></a>00367           {
<a name="l00368"></a>00368             free(real_ind_);
<a name="l00369"></a>00369             real_ind_ = NULL;
<a name="l00370"></a>00370           }
<a name="l00371"></a>00371 
<a name="l00372"></a>00372 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00373"></a>00373 <span class="preprocessor"></span>      }
<a name="l00374"></a>00374     <span class="keywordflow">catch</span> (...)
<a name="l00375"></a>00375       {
<a name="l00376"></a>00376         real_ind_ = NULL;
<a name="l00377"></a>00377       }
<a name="l00378"></a>00378 <span class="preprocessor">#endif</span>
<a name="l00379"></a>00379 <span class="preprocessor"></span>
<a name="l00380"></a>00380 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00381"></a>00381 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00382"></a>00382       {
<a name="l00383"></a>00383 <span class="preprocessor">#endif</span>
<a name="l00384"></a>00384 <span class="preprocessor"></span>
<a name="l00385"></a>00385         <span class="keywordflow">if</span> (imag_ind_ != NULL)
<a name="l00386"></a>00386           {
<a name="l00387"></a>00387             free(imag_ind_);
<a name="l00388"></a>00388             imag_ind_ = NULL;
<a name="l00389"></a>00389           }
<a name="l00390"></a>00390 
<a name="l00391"></a>00391 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00392"></a>00392 <span class="preprocessor"></span>      }
<a name="l00393"></a>00393     <span class="keywordflow">catch</span> (...)
<a name="l00394"></a>00394       {
<a name="l00395"></a>00395         imag_ind_ = NULL;
<a name="l00396"></a>00396       }
<a name="l00397"></a>00397 <span class="preprocessor">#endif</span>
<a name="l00398"></a>00398 <span class="preprocessor"></span>
<a name="l00399"></a>00399 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00400"></a>00400 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00401"></a>00401       {
<a name="l00402"></a>00402 <span class="preprocessor">#endif</span>
<a name="l00403"></a>00403 <span class="preprocessor"></span>
<a name="l00404"></a>00404         <span class="keywordflow">if</span> (this-&gt;real_data_ != NULL)
<a name="l00405"></a>00405           {
<a name="l00406"></a>00406             this-&gt;allocator_.deallocate(this-&gt;real_data_, real_nz_);
<a name="l00407"></a>00407             this-&gt;real_data_ = NULL;
<a name="l00408"></a>00408           }
<a name="l00409"></a>00409 
<a name="l00410"></a>00410 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00411"></a>00411 <span class="preprocessor"></span>      }
<a name="l00412"></a>00412     <span class="keywordflow">catch</span> (...)
<a name="l00413"></a>00413       {
<a name="l00414"></a>00414         this-&gt;real_nz_ = 0;
<a name="l00415"></a>00415         this-&gt;real_data_ = NULL;
<a name="l00416"></a>00416       }
<a name="l00417"></a>00417 <span class="preprocessor">#endif</span>
<a name="l00418"></a>00418 <span class="preprocessor"></span>
<a name="l00419"></a>00419 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00420"></a>00420 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00421"></a>00421       {
<a name="l00422"></a>00422 <span class="preprocessor">#endif</span>
<a name="l00423"></a>00423 <span class="preprocessor"></span>
<a name="l00424"></a>00424         <span class="keywordflow">if</span> (this-&gt;imag_data_ != NULL)
<a name="l00425"></a>00425           {
<a name="l00426"></a>00426             this-&gt;allocator_.deallocate(this-&gt;imag_data_, imag_nz_);
<a name="l00427"></a>00427             this-&gt;imag_data_ = NULL;
<a name="l00428"></a>00428           }
<a name="l00429"></a>00429 
<a name="l00430"></a>00430 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00431"></a>00431 <span class="preprocessor"></span>      }
<a name="l00432"></a>00432     <span class="keywordflow">catch</span> (...)
<a name="l00433"></a>00433       {
<a name="l00434"></a>00434         this-&gt;imag_nz_ = 0;
<a name="l00435"></a>00435         this-&gt;imag_data_ = NULL;
<a name="l00436"></a>00436       }
<a name="l00437"></a>00437 <span class="preprocessor">#endif</span>
<a name="l00438"></a>00438 <span class="preprocessor"></span>
<a name="l00439"></a>00439     this-&gt;real_nz_ = 0;
<a name="l00440"></a>00440     this-&gt;imag_nz_ = 0;
<a name="l00441"></a>00441   }
<a name="l00442"></a>00442 
<a name="l00443"></a>00443 
<a name="l00445"></a>00445 
<a name="l00448"></a>00448   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00449"></a>00449   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a8940cdf1faedc44051919b9774132fa9" title="Clears the matrix.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Clear</a>()
<a name="l00450"></a>00450   {
<a name="l00451"></a>00451     this-&gt;<a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3b785e50c55d9c0e00180bc7d3332d75" title="Destructor.">~Matrix_ComplexSparse</a>();
<a name="l00452"></a>00452   }
<a name="l00453"></a>00453 
<a name="l00454"></a>00454 
<a name="l00455"></a>00455   <span class="comment">/*********************</span>
<a name="l00456"></a>00456 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00457"></a>00457 <span class="comment">   *********************/</span>
<a name="l00458"></a>00458 
<a name="l00459"></a>00459 
<a name="l00461"></a>00461 
<a name="l00478"></a>00478   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00479"></a>00479   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00480"></a>00480             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00481"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3729058de11ead808a09e98a10702d85">00481</a>             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00482"></a>00482   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3729058de11ead808a09e98a10702d85" title="Redefines the matrix.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00483"></a>00483 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3729058de11ead808a09e98a10702d85" title="Redefines the matrix.">  SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00484"></a>00484           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; real_values,
<a name="l00485"></a>00485           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; real_ptr,
<a name="l00486"></a>00486           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; real_ind,
<a name="l00487"></a>00487           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; imag_values,
<a name="l00488"></a>00488           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; imag_ptr,
<a name="l00489"></a>00489           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; imag_ind)
<a name="l00490"></a>00490   {
<a name="l00491"></a>00491     this-&gt;<a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a8940cdf1faedc44051919b9774132fa9" title="Clears the matrix.">Clear</a>();
<a name="l00492"></a>00492     this-&gt;m_ = i;
<a name="l00493"></a>00493     this-&gt;n_ = j;
<a name="l00494"></a>00494     real_nz_ = real_values.GetLength();
<a name="l00495"></a>00495     imag_nz_ = imag_values.GetLength();
<a name="l00496"></a>00496 
<a name="l00497"></a>00497 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00498"></a>00498 <span class="preprocessor"></span>    <span class="comment">// Checks whether vector sizes are acceptable.</span>
<a name="l00499"></a>00499 
<a name="l00500"></a>00500     <span class="keywordflow">if</span> (real_ind.GetLength() != real_nz_)
<a name="l00501"></a>00501       {
<a name="l00502"></a>00502         this-&gt;m_ = 0;
<a name="l00503"></a>00503         this-&gt;n_ = 0;
<a name="l00504"></a>00504         real_nz_ = 0;
<a name="l00505"></a>00505         imag_nz_ = 0;
<a name="l00506"></a>00506         real_ptr_ = NULL;
<a name="l00507"></a>00507         imag_ptr_ = NULL;
<a name="l00508"></a>00508         real_ind_ = NULL;
<a name="l00509"></a>00509         imag_ind_ = NULL;
<a name="l00510"></a>00510         this-&gt;real_data_ = NULL;
<a name="l00511"></a>00511         this-&gt;imag_data_ = NULL;
<a name="l00512"></a>00512         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::SetData(int, int, &quot;</span>)
<a name="l00513"></a>00513                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00514"></a>00514                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00515"></a>00515                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz_)
<a name="l00516"></a>00516                        + <span class="stringliteral">&quot; values (real part) but &quot;</span>
<a name="l00517"></a>00517                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_ind.GetLength())
<a name="l00518"></a>00518                        + <span class="stringliteral">&quot; row or column indices.&quot;</span>);
<a name="l00519"></a>00519       }
<a name="l00520"></a>00520 
<a name="l00521"></a>00521     <span class="keywordflow">if</span> (imag_ind.GetLength() != imag_nz_)
<a name="l00522"></a>00522       {
<a name="l00523"></a>00523         this-&gt;m_ = 0;
<a name="l00524"></a>00524         this-&gt;n_ = 0;
<a name="l00525"></a>00525         real_nz_ = 0;
<a name="l00526"></a>00526         imag_nz_ = 0;
<a name="l00527"></a>00527         real_ptr_ = NULL;
<a name="l00528"></a>00528         imag_ptr_ = NULL;
<a name="l00529"></a>00529         real_ind_ = NULL;
<a name="l00530"></a>00530         imag_ind_ = NULL;
<a name="l00531"></a>00531         this-&gt;real_data_ = NULL;
<a name="l00532"></a>00532         this-&gt;imag_data_ = NULL;
<a name="l00533"></a>00533         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::SetData(int, int, &quot;</span>)
<a name="l00534"></a>00534                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00535"></a>00535                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00536"></a>00536                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz_)
<a name="l00537"></a>00537                        + <span class="stringliteral">&quot; values (imaginary part) but &quot;</span>
<a name="l00538"></a>00538                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_ind.GetLength())
<a name="l00539"></a>00539                        + <span class="stringliteral">&quot; row or column indices.&quot;</span>);
<a name="l00540"></a>00540       }
<a name="l00541"></a>00541 
<a name="l00542"></a>00542     <span class="keywordflow">if</span> (real_ptr.GetLength()-1 != Storage::GetFirst(i, j))
<a name="l00543"></a>00543       {
<a name="l00544"></a>00544         this-&gt;m_ = 0;
<a name="l00545"></a>00545         this-&gt;n_ = 0;
<a name="l00546"></a>00546         real_nz_ = 0;
<a name="l00547"></a>00547         imag_nz_ = 0;
<a name="l00548"></a>00548         real_ptr_ = NULL;
<a name="l00549"></a>00549         imag_ptr_ = NULL;
<a name="l00550"></a>00550         real_ind_ = NULL;
<a name="l00551"></a>00551         imag_ind_ = NULL;
<a name="l00552"></a>00552         this-&gt;real_data_ = NULL;
<a name="l00553"></a>00553         this-&gt;imag_data_ = NULL;
<a name="l00554"></a>00554         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::SetData(int, int, &quot;</span>)
<a name="l00555"></a>00555                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00556"></a>00556                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00557"></a>00557                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The vector of start indices (real part)&quot;</span>)
<a name="l00558"></a>00558                        + <span class="stringliteral">&quot; contains &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_ptr.GetLength()-1)
<a name="l00559"></a>00559                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (plus the&quot;</span>)
<a name="l00560"></a>00560                        + <span class="stringliteral">&quot; number of non-zero entries) but there are &quot;</span>
<a name="l00561"></a>00561                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j))
<a name="l00562"></a>00562                        + <span class="stringliteral">&quot; rows or columns (&quot;</span>
<a name="l00563"></a>00563                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix).&quot;</span>);
<a name="l00564"></a>00564       }
<a name="l00565"></a>00565 
<a name="l00566"></a>00566     <span class="keywordflow">if</span> (imag_ptr.GetLength()-1 != Storage::GetFirst(i, j))
<a name="l00567"></a>00567       {
<a name="l00568"></a>00568         this-&gt;m_ = 0;
<a name="l00569"></a>00569         this-&gt;n_ = 0;
<a name="l00570"></a>00570         real_nz_ = 0;
<a name="l00571"></a>00571         imag_nz_ = 0;
<a name="l00572"></a>00572         real_ptr_ = NULL;
<a name="l00573"></a>00573         imag_ptr_ = NULL;
<a name="l00574"></a>00574         real_ind_ = NULL;
<a name="l00575"></a>00575         imag_ind_ = NULL;
<a name="l00576"></a>00576         this-&gt;real_data_ = NULL;
<a name="l00577"></a>00577         this-&gt;imag_data_ = NULL;
<a name="l00578"></a>00578         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::SetData(int, int, &quot;</span>)
<a name="l00579"></a>00579                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00580"></a>00580                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00581"></a>00581                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The vector of start indices (imaginary part)&quot;</span>)
<a name="l00582"></a>00582                        + <span class="stringliteral">&quot; contains &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_ptr.GetLength()-1)
<a name="l00583"></a>00583                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (plus the&quot;</span>)
<a name="l00584"></a>00584                        + <span class="stringliteral">&quot; number of non-zero entries) but there are &quot;</span>
<a name="l00585"></a>00585                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j))
<a name="l00586"></a>00586                        + <span class="stringliteral">&quot; rows or columns (&quot;</span>
<a name="l00587"></a>00587                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix).&quot;</span>);
<a name="l00588"></a>00588       }
<a name="l00589"></a>00589 
<a name="l00590"></a>00590     <span class="keywordflow">if</span> ((real_nz_ &gt; 0
<a name="l00591"></a>00591          &amp;&amp; (j == 0
<a name="l00592"></a>00592              || static_cast&lt;long int&gt;(real_nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l00593"></a>00593              &gt;= static_cast&lt;long int&gt;(i)))
<a name="l00594"></a>00594         ||
<a name="l00595"></a>00595         (imag_nz_ &gt; 0
<a name="l00596"></a>00596          &amp;&amp; (j == 0
<a name="l00597"></a>00597              || <span class="keyword">static_cast&lt;</span><span class="keywordtype">long</span> <span class="keywordtype">int</span><span class="keyword">&gt;</span>(imag_nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l00598"></a>00598              &gt;= <span class="keyword">static_cast&lt;</span><span class="keywordtype">long</span> <span class="keywordtype">int</span><span class="keyword">&gt;</span>(i))))
<a name="l00599"></a>00599       {
<a name="l00600"></a>00600         this-&gt;m_ = 0;
<a name="l00601"></a>00601         this-&gt;n_ = 0;
<a name="l00602"></a>00602         real_nz_ = 0;
<a name="l00603"></a>00603         imag_nz_ = 0;
<a name="l00604"></a>00604         real_ptr_ = NULL;
<a name="l00605"></a>00605         imag_ptr_ = NULL;
<a name="l00606"></a>00606         real_ind_ = NULL;
<a name="l00607"></a>00607         imag_ind_ = NULL;
<a name="l00608"></a>00608         this-&gt;real_data_ = NULL;
<a name="l00609"></a>00609         this-&gt;imag_data_ = NULL;
<a name="l00610"></a>00610         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::SetData(int, int, &quot;</span>)
<a name="l00611"></a>00611                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00612"></a>00612                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00613"></a>00613                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>)
<a name="l00614"></a>00614                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_values.GetLength())
<a name="l00615"></a>00615                        + <span class="stringliteral">&quot; values for the real part and &quot;</span>
<a name="l00616"></a>00616                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_values.GetLength()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; values&quot;</span>)
<a name="l00617"></a>00617                        + string(<span class="stringliteral">&quot; for the imaginary part) than elements&quot;</span>)
<a name="l00618"></a>00618                        + <span class="stringliteral">&quot; in the matrix (&quot;</span>
<a name="l00619"></a>00619                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00620"></a>00620       }
<a name="l00621"></a>00621 <span class="preprocessor">#endif</span>
<a name="l00622"></a>00622 <span class="preprocessor"></span>
<a name="l00623"></a>00623     this-&gt;real_ptr_ = real_ptr.GetData();
<a name="l00624"></a>00624     this-&gt;imag_ptr_ = imag_ptr.GetData();
<a name="l00625"></a>00625     this-&gt;real_ind_ = real_ind.GetData();
<a name="l00626"></a>00626     this-&gt;imag_ind_ = imag_ind.GetData();
<a name="l00627"></a>00627     this-&gt;real_data_ = real_values.GetData();
<a name="l00628"></a>00628     this-&gt;imag_data_ = imag_values.GetData();
<a name="l00629"></a>00629 
<a name="l00630"></a>00630     real_ptr.Nullify();
<a name="l00631"></a>00631     imag_ptr.Nullify();
<a name="l00632"></a>00632     real_ind.Nullify();
<a name="l00633"></a>00633     imag_ind.Nullify();
<a name="l00634"></a>00634     real_values.Nullify();
<a name="l00635"></a>00635     imag_values.Nullify();
<a name="l00636"></a>00636   }
<a name="l00637"></a>00637 
<a name="l00638"></a>00638 
<a name="l00640"></a>00640 
<a name="l00661"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a50bafb538cfd1b8e6bb18bba62451dff">00661</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00662"></a>00662   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3729058de11ead808a09e98a10702d85" title="Redefines the matrix.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00663"></a>00663 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3729058de11ead808a09e98a10702d85" title="Redefines the matrix.">  SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> real_nz,
<a name="l00664"></a>00664           <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00665"></a>00665           ::pointer real_values,
<a name="l00666"></a>00666           <span class="keywordtype">int</span>* real_ptr, <span class="keywordtype">int</span>* real_ind, <span class="keywordtype">int</span> imag_nz,
<a name="l00667"></a>00667           <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00668"></a>00668           ::pointer imag_values,
<a name="l00669"></a>00669           <span class="keywordtype">int</span>* imag_ptr, <span class="keywordtype">int</span>* imag_ind)
<a name="l00670"></a>00670   {
<a name="l00671"></a>00671     this-&gt;<a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a8940cdf1faedc44051919b9774132fa9" title="Clears the matrix.">Clear</a>();
<a name="l00672"></a>00672 
<a name="l00673"></a>00673     this-&gt;m_ = i;
<a name="l00674"></a>00674     this-&gt;n_ = j;
<a name="l00675"></a>00675 
<a name="l00676"></a>00676     this-&gt;real_nz_ = real_nz;
<a name="l00677"></a>00677     this-&gt;imag_nz_ = imag_nz;
<a name="l00678"></a>00678 
<a name="l00679"></a>00679     real_data_ = real_values;
<a name="l00680"></a>00680     imag_data_ = imag_values;
<a name="l00681"></a>00681     real_ind_ = real_ind;
<a name="l00682"></a>00682     imag_ind_ = imag_ind;
<a name="l00683"></a>00683     real_ptr_ = real_ptr;
<a name="l00684"></a>00684     imag_ptr_ = imag_ptr;
<a name="l00685"></a>00685   }
<a name="l00686"></a>00686 
<a name="l00687"></a>00687 
<a name="l00689"></a>00689 
<a name="l00693"></a>00693   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00694"></a>00694   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a10672a9a570060c9bd8f17eb54dc46fa" title="Clears the matrix without releasing memory.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Nullify</a>()
<a name="l00695"></a>00695   {
<a name="l00696"></a>00696     this-&gt;data_ = NULL;
<a name="l00697"></a>00697     this-&gt;m_ = 0;
<a name="l00698"></a>00698     this-&gt;n_ = 0;
<a name="l00699"></a>00699     real_nz_ = 0;
<a name="l00700"></a>00700     real_ptr_ = NULL;
<a name="l00701"></a>00701     real_ind_ = NULL;
<a name="l00702"></a>00702     imag_nz_ = 0;
<a name="l00703"></a>00703     imag_ptr_ = NULL;
<a name="l00704"></a>00704     imag_ind_ = NULL;
<a name="l00705"></a>00705     real_data_ = NULL;
<a name="l00706"></a>00706     imag_data_ = NULL;
<a name="l00707"></a>00707   }
<a name="l00708"></a>00708 
<a name="l00709"></a>00709 
<a name="l00711"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a72602182671e7f970959772510eed557">00711</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00712"></a>00712   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a72602182671e7f970959772510eed557" title="Initialization of an empty matrix i x j.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00713"></a>00713 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a72602182671e7f970959772510eed557" title="Initialization of an empty matrix i x j.">  ::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00714"></a>00714   {
<a name="l00715"></a>00715     <span class="comment">// previous entries are removed</span>
<a name="l00716"></a>00716     Clear();
<a name="l00717"></a>00717 
<a name="l00718"></a>00718     this-&gt;m_ = i;
<a name="l00719"></a>00719     this-&gt;n_ = j;
<a name="l00720"></a>00720 
<a name="l00721"></a>00721     <span class="comment">// we try to allocate real_ptr_ and imag_ptr_</span>
<a name="l00722"></a>00722 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00723"></a>00723 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00724"></a>00724       {
<a name="l00725"></a>00725 <span class="preprocessor">#endif</span>
<a name="l00726"></a>00726 <span class="preprocessor"></span>
<a name="l00727"></a>00727         real_ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(Storage::GetFirst(i, j)+1,
<a name="l00728"></a>00728                                                    <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00729"></a>00729 
<a name="l00730"></a>00730         imag_ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(Storage::GetFirst(i, j)+1,
<a name="l00731"></a>00731                                                    <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00732"></a>00732 
<a name="l00733"></a>00733 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00734"></a>00734 <span class="preprocessor"></span>      }
<a name="l00735"></a>00735     <span class="keywordflow">catch</span> (...)
<a name="l00736"></a>00736       {
<a name="l00737"></a>00737         this-&gt;m_ = 0;
<a name="l00738"></a>00738         this-&gt;n_ = 0;
<a name="l00739"></a>00739         real_nz_ = 0;
<a name="l00740"></a>00740         real_ptr_ = NULL;
<a name="l00741"></a>00741         real_ind_ = NULL;
<a name="l00742"></a>00742         imag_nz_ = 0;
<a name="l00743"></a>00743         imag_ptr_ = NULL;
<a name="l00744"></a>00744         imag_ind_ = NULL;
<a name="l00745"></a>00745         real_data_ = NULL;
<a name="l00746"></a>00746         imag_data_ = NULL;
<a name="l00747"></a>00747       }
<a name="l00748"></a>00748     <span class="keywordflow">if</span> ((real_ptr_ == NULL) || (imag_ptr_ == NULL))
<a name="l00749"></a>00749       {
<a name="l00750"></a>00750         this-&gt;m_ = 0;
<a name="l00751"></a>00751         this-&gt;n_ = 0;
<a name="l00752"></a>00752         real_nz_ = 0;
<a name="l00753"></a>00753         real_ptr_ = NULL;
<a name="l00754"></a>00754         real_ind_ = NULL;
<a name="l00755"></a>00755         imag_nz_ = 0;
<a name="l00756"></a>00756         imag_ptr_ = NULL;
<a name="l00757"></a>00757         imag_ind_ = NULL;
<a name="l00758"></a>00758         real_data_ = NULL;
<a name="l00759"></a>00759         imag_data_ = NULL;
<a name="l00760"></a>00760       }
<a name="l00761"></a>00761     <span class="keywordflow">if</span> (((real_ptr_ == NULL) || (imag_ptr_ == NULL)) &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00762"></a>00762       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::Reallocate(int, int)&quot;</span>,
<a name="l00763"></a>00763                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00764"></a>00764                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (Storage::GetFirst(i, j)+1) )
<a name="l00765"></a>00765                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j)+1)
<a name="l00766"></a>00766                      + <span class="stringliteral">&quot; row or column start indices, for a &quot;</span>
<a name="l00767"></a>00767                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00768"></a>00768 <span class="preprocessor">#endif</span>
<a name="l00769"></a>00769 <span class="preprocessor"></span>
<a name="l00770"></a>00770     <span class="comment">// then filing real_ptr_ with 0</span>
<a name="l00771"></a>00771     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt;= Storage::GetFirst(i, j); k++)
<a name="l00772"></a>00772       {
<a name="l00773"></a>00773         real_ptr_[k] = 0;
<a name="l00774"></a>00774         imag_ptr_[k] = 0;
<a name="l00775"></a>00775       }
<a name="l00776"></a>00776   }
<a name="l00777"></a>00777 
<a name="l00778"></a>00778 
<a name="l00780"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a8e14d527038311367db8ebfd79418995">00780</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00781"></a>00781   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a72602182671e7f970959772510eed557" title="Initialization of an empty matrix i x j.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00782"></a>00782 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a72602182671e7f970959772510eed557" title="Initialization of an empty matrix i x j.">  ::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> real_nz, <span class="keywordtype">int</span> imag_nz)
<a name="l00783"></a>00783   {
<a name="l00784"></a>00784     <span class="comment">// previous entries are removed</span>
<a name="l00785"></a>00785     Clear();
<a name="l00786"></a>00786 
<a name="l00787"></a>00787     this-&gt;m_ = i;
<a name="l00788"></a>00788     this-&gt;n_ = j;
<a name="l00789"></a>00789     this-&gt;real_nz_ = real_nz;
<a name="l00790"></a>00790     this-&gt;imag_nz_ = imag_nz;
<a name="l00791"></a>00791 
<a name="l00792"></a>00792 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00793"></a>00793 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (real_nz_ &lt; 0 || imag_nz_ &lt; 0)
<a name="l00794"></a>00794       {
<a name="l00795"></a>00795         this-&gt;m_ = 0;
<a name="l00796"></a>00796         this-&gt;n_ = 0;
<a name="l00797"></a>00797         real_nz_ = 0;
<a name="l00798"></a>00798         imag_nz_ = 0;
<a name="l00799"></a>00799         real_ptr_ = NULL;
<a name="l00800"></a>00800         imag_ptr_ = NULL;
<a name="l00801"></a>00801         real_ind_ = NULL;
<a name="l00802"></a>00802         imag_ind_ = NULL;
<a name="l00803"></a>00803         this-&gt;real_data_ = NULL;
<a name="l00804"></a>00804         this-&gt;imag_data_ = NULL;
<a name="l00805"></a>00805         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00806"></a>00806                        + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l00807"></a>00807                        <span class="stringliteral">&quot;Invalid number of non-zero elements: &quot;</span>
<a name="l00808"></a>00808                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz) + <span class="stringliteral">&quot; in the real part and &quot;</span>
<a name="l00809"></a>00809                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz) + <span class="stringliteral">&quot; in the imaginary part.&quot;</span>);
<a name="l00810"></a>00810       }
<a name="l00811"></a>00811     <span class="keywordflow">if</span> ((real_nz_ &gt; 0
<a name="l00812"></a>00812          &amp;&amp; (j == 0
<a name="l00813"></a>00813              || static_cast&lt;long int&gt;(real_nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l00814"></a>00814              &gt;= static_cast&lt;long int&gt;(i)))
<a name="l00815"></a>00815         ||
<a name="l00816"></a>00816         (imag_nz_ &gt; 0
<a name="l00817"></a>00817          &amp;&amp; (j == 0
<a name="l00818"></a>00818              || static_cast&lt;long int&gt;(imag_nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l00819"></a>00819              &gt;= static_cast&lt;long int&gt;(i))))
<a name="l00820"></a>00820       {
<a name="l00821"></a>00821         this-&gt;m_ = 0;
<a name="l00822"></a>00822         this-&gt;n_ = 0;
<a name="l00823"></a>00823         real_nz_ = 0;
<a name="l00824"></a>00824         imag_nz_ = 0;
<a name="l00825"></a>00825         real_ptr_ = NULL;
<a name="l00826"></a>00826         imag_ptr_ = NULL;
<a name="l00827"></a>00827         real_ind_ = NULL;
<a name="l00828"></a>00828         imag_ind_ = NULL;
<a name="l00829"></a>00829         this-&gt;real_data_ = NULL;
<a name="l00830"></a>00830         this-&gt;imag_data_ = NULL;
<a name="l00831"></a>00831         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00832"></a>00832                        + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l00833"></a>00833                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz)
<a name="l00834"></a>00834                        + <span class="stringliteral">&quot; values for the real part and &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz)
<a name="l00835"></a>00835                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; values for the imaginary part) than&quot;</span>)
<a name="l00836"></a>00836                        + <span class="stringliteral">&quot; elements in the matrix (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span>
<a name="l00837"></a>00837                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00838"></a>00838       }
<a name="l00839"></a>00839 <span class="preprocessor">#endif</span>
<a name="l00840"></a>00840 <span class="preprocessor"></span>
<a name="l00841"></a>00841 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00842"></a>00842 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00843"></a>00843       {
<a name="l00844"></a>00844 <span class="preprocessor">#endif</span>
<a name="l00845"></a>00845 <span class="preprocessor"></span>
<a name="l00846"></a>00846         real_ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(Storage::GetFirst(i, j)+1,
<a name="l00847"></a>00847                                                    <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00848"></a>00848 
<a name="l00849"></a>00849 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00850"></a>00850 <span class="preprocessor"></span>      }
<a name="l00851"></a>00851     <span class="keywordflow">catch</span> (...)
<a name="l00852"></a>00852       {
<a name="l00853"></a>00853         this-&gt;m_ = 0;
<a name="l00854"></a>00854         this-&gt;n_ = 0;
<a name="l00855"></a>00855         real_nz_ = 0;
<a name="l00856"></a>00856         imag_nz_ = 0;
<a name="l00857"></a>00857         real_ptr_ = NULL;
<a name="l00858"></a>00858         imag_ptr_ = NULL;
<a name="l00859"></a>00859         real_ind_ = NULL;
<a name="l00860"></a>00860         imag_ind_ = NULL;
<a name="l00861"></a>00861         this-&gt;real_data_ = NULL;
<a name="l00862"></a>00862         this-&gt;imag_data_ = NULL;
<a name="l00863"></a>00863       }
<a name="l00864"></a>00864     <span class="keywordflow">if</span> (real_ptr_ == NULL)
<a name="l00865"></a>00865       {
<a name="l00866"></a>00866         this-&gt;m_ = 0;
<a name="l00867"></a>00867         this-&gt;n_ = 0;
<a name="l00868"></a>00868         real_nz_ = 0;
<a name="l00869"></a>00869         imag_nz_ = 0;
<a name="l00870"></a>00870         imag_ptr_ = 0;
<a name="l00871"></a>00871         real_ind_ = NULL;
<a name="l00872"></a>00872         imag_ind_ = NULL;
<a name="l00873"></a>00873         this-&gt;real_data_ = NULL;
<a name="l00874"></a>00874         this-&gt;imag_data_ = NULL;
<a name="l00875"></a>00875       }
<a name="l00876"></a>00876     <span class="keywordflow">if</span> (real_ptr_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00877"></a>00877       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00878"></a>00878                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l00879"></a>00879                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00880"></a>00880                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (Storage::GetFirst(i, j)+1))
<a name="l00881"></a>00881                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j)+1)
<a name="l00882"></a>00882                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (for the real&quot;</span>)
<a name="l00883"></a>00883                      + <span class="stringliteral">&quot; part), for a &quot;</span>
<a name="l00884"></a>00884                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00885"></a>00885 <span class="preprocessor">#endif</span>
<a name="l00886"></a>00886 <span class="preprocessor"></span>
<a name="l00887"></a>00887 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00888"></a>00888 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00889"></a>00889       {
<a name="l00890"></a>00890 <span class="preprocessor">#endif</span>
<a name="l00891"></a>00891 <span class="preprocessor"></span>
<a name="l00892"></a>00892         imag_ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(Storage::GetFirst(i, j)+1,
<a name="l00893"></a>00893                                                    <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00894"></a>00894 
<a name="l00895"></a>00895 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00896"></a>00896 <span class="preprocessor"></span>      }
<a name="l00897"></a>00897     <span class="keywordflow">catch</span> (...)
<a name="l00898"></a>00898       {
<a name="l00899"></a>00899         this-&gt;m_ = 0;
<a name="l00900"></a>00900         this-&gt;n_ = 0;
<a name="l00901"></a>00901         real_nz_ = 0;
<a name="l00902"></a>00902         imag_nz_ = 0;
<a name="l00903"></a>00903         free(real_ptr_);
<a name="l00904"></a>00904         real_ptr_ = NULL;
<a name="l00905"></a>00905         imag_ptr_ = NULL;
<a name="l00906"></a>00906         real_ind_ = NULL;
<a name="l00907"></a>00907         imag_ind_ = NULL;
<a name="l00908"></a>00908         this-&gt;real_data_ = NULL;
<a name="l00909"></a>00909         this-&gt;imag_data_ = NULL;
<a name="l00910"></a>00910       }
<a name="l00911"></a>00911     <span class="keywordflow">if</span> (imag_ptr_ == NULL)
<a name="l00912"></a>00912       {
<a name="l00913"></a>00913         this-&gt;m_ = 0;
<a name="l00914"></a>00914         this-&gt;n_ = 0;
<a name="l00915"></a>00915         real_nz_ = 0;
<a name="l00916"></a>00916         imag_nz_ = 0;
<a name="l00917"></a>00917         free(real_ptr_);
<a name="l00918"></a>00918         real_ptr_ = 0;
<a name="l00919"></a>00919         real_ind_ = NULL;
<a name="l00920"></a>00920         imag_ind_ = NULL;
<a name="l00921"></a>00921         this-&gt;real_data_ = NULL;
<a name="l00922"></a>00922         this-&gt;imag_data_ = NULL;
<a name="l00923"></a>00923       }
<a name="l00924"></a>00924     <span class="keywordflow">if</span> (imag_ptr_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00925"></a>00925       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00926"></a>00926                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l00927"></a>00927                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00928"></a>00928                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (Storage::GetFirst(i, j)+1))
<a name="l00929"></a>00929                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j)+1)
<a name="l00930"></a>00930                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (for the&quot;</span>)
<a name="l00931"></a>00931                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; imaginary part), for a &quot;</span>)
<a name="l00932"></a>00932                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00933"></a>00933 <span class="preprocessor">#endif</span>
<a name="l00934"></a>00934 <span class="preprocessor"></span>
<a name="l00935"></a>00935 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00936"></a>00936 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00937"></a>00937       {
<a name="l00938"></a>00938 <span class="preprocessor">#endif</span>
<a name="l00939"></a>00939 <span class="preprocessor"></span>
<a name="l00940"></a>00940         real_ind_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(real_nz_, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00941"></a>00941 
<a name="l00942"></a>00942 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00943"></a>00943 <span class="preprocessor"></span>      }
<a name="l00944"></a>00944     <span class="keywordflow">catch</span> (...)
<a name="l00945"></a>00945       {
<a name="l00946"></a>00946         this-&gt;m_ = 0;
<a name="l00947"></a>00947         this-&gt;n_ = 0;
<a name="l00948"></a>00948         real_nz_ = 0;
<a name="l00949"></a>00949         imag_nz_ = 0;
<a name="l00950"></a>00950         free(real_ptr_);
<a name="l00951"></a>00951         free(imag_ptr_);
<a name="l00952"></a>00952         real_ptr_ = NULL;
<a name="l00953"></a>00953         imag_ptr_ = NULL;
<a name="l00954"></a>00954         real_ind_ = NULL;
<a name="l00955"></a>00955         imag_ind_ = NULL;
<a name="l00956"></a>00956         this-&gt;real_data_ = NULL;
<a name="l00957"></a>00957         this-&gt;imag_data_ = NULL;
<a name="l00958"></a>00958       }
<a name="l00959"></a>00959     <span class="keywordflow">if</span> (real_ind_ == NULL)
<a name="l00960"></a>00960       {
<a name="l00961"></a>00961         this-&gt;m_ = 0;
<a name="l00962"></a>00962         this-&gt;n_ = 0;
<a name="l00963"></a>00963         real_nz_ = 0;
<a name="l00964"></a>00964         imag_nz_ = 0;
<a name="l00965"></a>00965         free(real_ptr_);
<a name="l00966"></a>00966         free(imag_ptr_);
<a name="l00967"></a>00967         real_ptr_ = NULL;
<a name="l00968"></a>00968         imag_ptr_ = NULL;
<a name="l00969"></a>00969         this-&gt;real_data_ = NULL;
<a name="l00970"></a>00970         this-&gt;imag_data_ = NULL;
<a name="l00971"></a>00971       }
<a name="l00972"></a>00972     <span class="keywordflow">if</span> (real_ind_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00973"></a>00973       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00974"></a>00974                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l00975"></a>00975                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00976"></a>00976                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * real_nz)
<a name="l00977"></a>00977                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz)
<a name="l00978"></a>00978                      + <span class="stringliteral">&quot; row or column indices (real part), for a &quot;</span>
<a name="l00979"></a>00979                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00980"></a>00980 <span class="preprocessor">#endif</span>
<a name="l00981"></a>00981 <span class="preprocessor"></span>
<a name="l00982"></a>00982 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00983"></a>00983 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00984"></a>00984       {
<a name="l00985"></a>00985 <span class="preprocessor">#endif</span>
<a name="l00986"></a>00986 <span class="preprocessor"></span>
<a name="l00987"></a>00987         imag_ind_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(imag_nz_, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00988"></a>00988 
<a name="l00989"></a>00989 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00990"></a>00990 <span class="preprocessor"></span>      }
<a name="l00991"></a>00991     <span class="keywordflow">catch</span> (...)
<a name="l00992"></a>00992       {
<a name="l00993"></a>00993         this-&gt;m_ = 0;
<a name="l00994"></a>00994         this-&gt;n_ = 0;
<a name="l00995"></a>00995         real_nz_ = 0;
<a name="l00996"></a>00996         imag_nz_ = 0;
<a name="l00997"></a>00997         free(real_ptr_);
<a name="l00998"></a>00998         free(imag_ptr_);
<a name="l00999"></a>00999         real_ptr_ = NULL;
<a name="l01000"></a>01000         imag_ptr_ = NULL;
<a name="l01001"></a>01001         free(imag_ind_);
<a name="l01002"></a>01002         real_ind_ = NULL;
<a name="l01003"></a>01003         imag_ind_ = NULL;
<a name="l01004"></a>01004         this-&gt;real_data_ = NULL;
<a name="l01005"></a>01005         this-&gt;imag_data_ = NULL;
<a name="l01006"></a>01006       }
<a name="l01007"></a>01007     <span class="keywordflow">if</span> (real_ind_ == NULL)
<a name="l01008"></a>01008       {
<a name="l01009"></a>01009         this-&gt;m_ = 0;
<a name="l01010"></a>01010         this-&gt;n_ = 0;
<a name="l01011"></a>01011         real_nz_ = 0;
<a name="l01012"></a>01012         imag_nz_ = 0;
<a name="l01013"></a>01013         free(real_ptr_);
<a name="l01014"></a>01014         free(imag_ptr_);
<a name="l01015"></a>01015         real_ptr_ = NULL;
<a name="l01016"></a>01016         imag_ptr_ = NULL;
<a name="l01017"></a>01017         free(imag_ind_);
<a name="l01018"></a>01018         imag_ind_ = NULL;
<a name="l01019"></a>01019         this-&gt;real_data_ = NULL;
<a name="l01020"></a>01020         this-&gt;imag_data_ = NULL;
<a name="l01021"></a>01021       }
<a name="l01022"></a>01022     <span class="keywordflow">if</span> (imag_ind_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l01023"></a>01023       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01024"></a>01024                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01025"></a>01025                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01026"></a>01026                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * imag_nz)
<a name="l01027"></a>01027                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz)
<a name="l01028"></a>01028                      + <span class="stringliteral">&quot; row or column indices (imaginary part), for a &quot;</span>
<a name="l01029"></a>01029                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01030"></a>01030 <span class="preprocessor">#endif</span>
<a name="l01031"></a>01031 <span class="preprocessor"></span>
<a name="l01032"></a>01032 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01033"></a>01033 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01034"></a>01034       {
<a name="l01035"></a>01035 <span class="preprocessor">#endif</span>
<a name="l01036"></a>01036 <span class="preprocessor"></span>
<a name="l01037"></a>01037         this-&gt;real_data_ = this-&gt;allocator_.allocate(real_nz_, <span class="keyword">this</span>);
<a name="l01038"></a>01038 
<a name="l01039"></a>01039 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01040"></a>01040 <span class="preprocessor"></span>      }
<a name="l01041"></a>01041     <span class="keywordflow">catch</span> (...)
<a name="l01042"></a>01042       {
<a name="l01043"></a>01043         this-&gt;m_ = 0;
<a name="l01044"></a>01044         this-&gt;n_ = 0;
<a name="l01045"></a>01045         free(real_ptr_);
<a name="l01046"></a>01046         free(imag_ptr_);
<a name="l01047"></a>01047         real_ptr_ = NULL;
<a name="l01048"></a>01048         imag_ptr_ = NULL;
<a name="l01049"></a>01049         free(real_ind_);
<a name="l01050"></a>01050         free(imag_ind_);
<a name="l01051"></a>01051         real_ind_ = NULL;
<a name="l01052"></a>01052         imag_ind_ = NULL;
<a name="l01053"></a>01053         this-&gt;real_data_ = NULL;
<a name="l01054"></a>01054         this-&gt;imag_data_ = NULL;
<a name="l01055"></a>01055       }
<a name="l01056"></a>01056     <span class="keywordflow">if</span> (real_data_ == NULL)
<a name="l01057"></a>01057       {
<a name="l01058"></a>01058         this-&gt;m_ = 0;
<a name="l01059"></a>01059         this-&gt;n_ = 0;
<a name="l01060"></a>01060         free(real_ptr_);
<a name="l01061"></a>01061         free(imag_ptr_);
<a name="l01062"></a>01062         real_ptr_ = NULL;
<a name="l01063"></a>01063         imag_ptr_ = NULL;
<a name="l01064"></a>01064         free(real_ind_);
<a name="l01065"></a>01065         free(imag_ind_);
<a name="l01066"></a>01066         real_ind_ = NULL;
<a name="l01067"></a>01067         imag_ind_ = NULL;
<a name="l01068"></a>01068         imag_data_ = NULL;
<a name="l01069"></a>01069       }
<a name="l01070"></a>01070     <span class="keywordflow">if</span> (real_data_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l01071"></a>01071       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01072"></a>01072                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01073"></a>01073                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01074"></a>01074                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * real_nz)
<a name="l01075"></a>01075                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz)
<a name="l01076"></a>01076                      + <span class="stringliteral">&quot; values (real part), for a &quot;</span>
<a name="l01077"></a>01077                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01078"></a>01078 <span class="preprocessor">#endif</span>
<a name="l01079"></a>01079 <span class="preprocessor"></span>
<a name="l01080"></a>01080 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01081"></a>01081 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01082"></a>01082       {
<a name="l01083"></a>01083 <span class="preprocessor">#endif</span>
<a name="l01084"></a>01084 <span class="preprocessor"></span>
<a name="l01085"></a>01085         this-&gt;imag_data_ = this-&gt;allocator_.allocate(imag_nz_, <span class="keyword">this</span>);
<a name="l01086"></a>01086 
<a name="l01087"></a>01087 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01088"></a>01088 <span class="preprocessor"></span>      }
<a name="l01089"></a>01089     <span class="keywordflow">catch</span> (...)
<a name="l01090"></a>01090       {
<a name="l01091"></a>01091         this-&gt;m_ = 0;
<a name="l01092"></a>01092         this-&gt;n_ = 0;
<a name="l01093"></a>01093         free(real_ptr_);
<a name="l01094"></a>01094         free(imag_ptr_);
<a name="l01095"></a>01095         real_ptr_ = NULL;
<a name="l01096"></a>01096         imag_ptr_ = NULL;
<a name="l01097"></a>01097         free(real_ind_);
<a name="l01098"></a>01098         free(imag_ind_);
<a name="l01099"></a>01099         real_ind_ = NULL;
<a name="l01100"></a>01100         imag_ind_ = NULL;
<a name="l01101"></a>01101         this-&gt;allocator_.deallocate(this-&gt;real_data_, real_nz_);
<a name="l01102"></a>01102         this-&gt;real_data_ = NULL;
<a name="l01103"></a>01103         this-&gt;imag_data_ = NULL;
<a name="l01104"></a>01104       }
<a name="l01105"></a>01105     <span class="keywordflow">if</span> (real_data_ == NULL)
<a name="l01106"></a>01106       {
<a name="l01107"></a>01107         this-&gt;m_ = 0;
<a name="l01108"></a>01108         this-&gt;n_ = 0;
<a name="l01109"></a>01109         free(real_ptr_);
<a name="l01110"></a>01110         free(imag_ptr_);
<a name="l01111"></a>01111         real_ptr_ = NULL;
<a name="l01112"></a>01112         imag_ptr_ = NULL;
<a name="l01113"></a>01113         free(real_ind_);
<a name="l01114"></a>01114         free(imag_ind_);
<a name="l01115"></a>01115         real_ind_ = NULL;
<a name="l01116"></a>01116         imag_ind_ = NULL;
<a name="l01117"></a>01117         this-&gt;allocator_.deallocate(this-&gt;real_data_, real_nz_);
<a name="l01118"></a>01118         real_data_ = NULL;
<a name="l01119"></a>01119       }
<a name="l01120"></a>01120     <span class="keywordflow">if</span> (imag_data_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l01121"></a>01121       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01122"></a>01122                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01123"></a>01123                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01124"></a>01124                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * imag_nz)
<a name="l01125"></a>01125                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz)
<a name="l01126"></a>01126                      + <span class="stringliteral">&quot; values (imaginary part), for a &quot;</span>
<a name="l01127"></a>01127                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01128"></a>01128 <span class="preprocessor">#endif</span>
<a name="l01129"></a>01129 <span class="preprocessor"></span>  }
<a name="l01130"></a>01130 
<a name="l01131"></a>01131 
<a name="l01133"></a>01133 
<a name="l01137"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#aa53b17b254435486a52c058490abb0fb">01137</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01138"></a>01138   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#aa53b17b254435486a52c058490abb0fb" title="Changing the number of rows and columns.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01139"></a>01139 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#aa53b17b254435486a52c058490abb0fb" title="Changing the number of rows and columns.">  ::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01140"></a>01140   {
<a name="l01141"></a>01141     <span class="keywordflow">if</span> (Storage::GetFirst(i, j) &lt; Storage::GetFirst(this-&gt;m_, this-&gt;n_))
<a name="l01142"></a>01142       Resize(i, j, real_ptr_[Storage::GetFirst(i, j)],
<a name="l01143"></a>01143              imag_ptr_[Storage::GetFirst(i, j)]);
<a name="l01144"></a>01144     <span class="keywordflow">else</span>
<a name="l01145"></a>01145       Resize(i, j, real_nz_, imag_nz_);
<a name="l01146"></a>01146   }
<a name="l01147"></a>01147 
<a name="l01148"></a>01148 
<a name="l01150"></a>01150 
<a name="l01155"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a17e9f9857a46c900b33ab803ca584c2f">01155</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01156"></a>01156   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#aa53b17b254435486a52c058490abb0fb" title="Changing the number of rows and columns.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01157"></a>01157 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#aa53b17b254435486a52c058490abb0fb" title="Changing the number of rows and columns.">  ::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> real_nz, <span class="keywordtype">int</span> imag_nz)
<a name="l01158"></a>01158   {
<a name="l01159"></a>01159 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l01160"></a>01160 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (real_nz &lt; 0 || imag_nz &lt; 0)
<a name="l01161"></a>01161       {
<a name="l01162"></a>01162         this-&gt;m_ = 0;
<a name="l01163"></a>01163         this-&gt;n_ = 0;
<a name="l01164"></a>01164         real_nz_ = 0;
<a name="l01165"></a>01165         imag_nz_ = 0;
<a name="l01166"></a>01166         real_ptr_ = NULL;
<a name="l01167"></a>01167         imag_ptr_ = NULL;
<a name="l01168"></a>01168         real_ind_ = NULL;
<a name="l01169"></a>01169         imag_ind_ = NULL;
<a name="l01170"></a>01170         this-&gt;real_data_ = NULL;
<a name="l01171"></a>01171         this-&gt;imag_data_ = NULL;
<a name="l01172"></a>01172         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01173"></a>01173                        + <span class="stringliteral">&quot;Resize(int, int, int, int)&quot;</span>,
<a name="l01174"></a>01174                        <span class="stringliteral">&quot;Invalid number of non-zero elements: &quot;</span>
<a name="l01175"></a>01175                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz) + <span class="stringliteral">&quot; in the real part and &quot;</span>
<a name="l01176"></a>01176                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz) + <span class="stringliteral">&quot; in the imaginary part.&quot;</span>);
<a name="l01177"></a>01177       }
<a name="l01178"></a>01178     <span class="keywordflow">if</span> ((real_nz &gt; 0
<a name="l01179"></a>01179          &amp;&amp; (j == 0
<a name="l01180"></a>01180              || static_cast&lt;long int&gt;(real_nz-1) / static_cast&lt;long int&gt;(j)
<a name="l01181"></a>01181              &gt;= static_cast&lt;long int&gt;(i)))
<a name="l01182"></a>01182         ||
<a name="l01183"></a>01183         (imag_nz &gt; 0
<a name="l01184"></a>01184          &amp;&amp; (j == 0
<a name="l01185"></a>01185              || static_cast&lt;long int&gt;(imag_nz-1) / static_cast&lt;long int&gt;(j)
<a name="l01186"></a>01186              &gt;= static_cast&lt;long int&gt;(i))))
<a name="l01187"></a>01187       {
<a name="l01188"></a>01188         this-&gt;m_ = 0;
<a name="l01189"></a>01189         this-&gt;n_ = 0;
<a name="l01190"></a>01190         real_nz_ = 0;
<a name="l01191"></a>01191         imag_nz_ = 0;
<a name="l01192"></a>01192         real_ptr_ = NULL;
<a name="l01193"></a>01193         imag_ptr_ = NULL;
<a name="l01194"></a>01194         real_ind_ = NULL;
<a name="l01195"></a>01195         imag_ind_ = NULL;
<a name="l01196"></a>01196         this-&gt;real_data_ = NULL;
<a name="l01197"></a>01197         this-&gt;imag_data_ = NULL;
<a name="l01198"></a>01198         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01199"></a>01199                        + <span class="stringliteral">&quot;Resize(int, int, int, int)&quot;</span>,
<a name="l01200"></a>01200                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz)
<a name="l01201"></a>01201                        + <span class="stringliteral">&quot; values for the real part and &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz)
<a name="l01202"></a>01202                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; values for the imaginary part) than&quot;</span>)
<a name="l01203"></a>01203                        + <span class="stringliteral">&quot; elements in the matrix (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span>
<a name="l01204"></a>01204                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l01205"></a>01205       }
<a name="l01206"></a>01206 <span class="preprocessor">#endif</span>
<a name="l01207"></a>01207 <span class="preprocessor"></span>
<a name="l01208"></a>01208     <span class="keywordflow">if</span> (Storage::GetFirst(this-&gt;m_, this-&gt;n_) != Storage::GetFirst(i, j))
<a name="l01209"></a>01209       {
<a name="l01210"></a>01210 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01211"></a>01211 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l01212"></a>01212           {
<a name="l01213"></a>01213 <span class="preprocessor">#endif</span>
<a name="l01214"></a>01214 <span class="preprocessor"></span>
<a name="l01215"></a>01215             real_ptr_
<a name="l01216"></a>01216               = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( realloc(real_ptr_,
<a name="l01217"></a>01217                                                 (Storage::GetFirst(i, j)+1)
<a name="l01218"></a>01218                                                 *<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l01219"></a>01219 
<a name="l01220"></a>01220 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01221"></a>01221 <span class="preprocessor"></span>          }
<a name="l01222"></a>01222         <span class="keywordflow">catch</span> (...)
<a name="l01223"></a>01223           {
<a name="l01224"></a>01224             this-&gt;m_ = 0;
<a name="l01225"></a>01225             this-&gt;n_ = 0;
<a name="l01226"></a>01226             real_nz_ = 0;
<a name="l01227"></a>01227             imag_nz_ = 0;
<a name="l01228"></a>01228             real_ptr_ = NULL;
<a name="l01229"></a>01229             imag_ptr_ = NULL;
<a name="l01230"></a>01230             real_ind_ = NULL;
<a name="l01231"></a>01231             imag_ind_ = NULL;
<a name="l01232"></a>01232             this-&gt;real_data_ = NULL;
<a name="l01233"></a>01233             this-&gt;imag_data_ = NULL;
<a name="l01234"></a>01234           }
<a name="l01235"></a>01235         <span class="keywordflow">if</span> (real_ptr_ == NULL)
<a name="l01236"></a>01236           {
<a name="l01237"></a>01237             this-&gt;m_ = 0;
<a name="l01238"></a>01238             this-&gt;n_ = 0;
<a name="l01239"></a>01239             real_nz_ = 0;
<a name="l01240"></a>01240             imag_nz_ = 0;
<a name="l01241"></a>01241             imag_ptr_ = 0;
<a name="l01242"></a>01242             real_ind_ = NULL;
<a name="l01243"></a>01243             imag_ind_ = NULL;
<a name="l01244"></a>01244             this-&gt;real_data_ = NULL;
<a name="l01245"></a>01245             this-&gt;imag_data_ = NULL;
<a name="l01246"></a>01246           }
<a name="l01247"></a>01247         <span class="keywordflow">if</span> (real_ptr_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l01248"></a>01248           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01249"></a>01249                      + <span class="stringliteral">&quot;Resize(int, int, int, int)&quot;</span>,
<a name="l01250"></a>01250                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01251"></a>01251                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (Storage::GetFirst(i, j)+1))
<a name="l01252"></a>01252                          + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j)+1)
<a name="l01253"></a>01253                          + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (for the real&quot;</span>)
<a name="l01254"></a>01254                          + <span class="stringliteral">&quot; part), for a &quot;</span>
<a name="l01255"></a>01255                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01256"></a>01256 <span class="preprocessor">#endif</span>
<a name="l01257"></a>01257 <span class="preprocessor"></span>
<a name="l01258"></a>01258 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01259"></a>01259 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l01260"></a>01260           {
<a name="l01261"></a>01261 <span class="preprocessor">#endif</span>
<a name="l01262"></a>01262 <span class="preprocessor"></span>
<a name="l01263"></a>01263             imag_ptr_
<a name="l01264"></a>01264               = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( realloc(imag_ptr_,
<a name="l01265"></a>01265                                                 (Storage::GetFirst(i, j)+1)
<a name="l01266"></a>01266                                                 *<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l01267"></a>01267 
<a name="l01268"></a>01268 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01269"></a>01269 <span class="preprocessor"></span>          }
<a name="l01270"></a>01270         <span class="keywordflow">catch</span> (...)
<a name="l01271"></a>01271           {
<a name="l01272"></a>01272             this-&gt;m_ = 0;
<a name="l01273"></a>01273             this-&gt;n_ = 0;
<a name="l01274"></a>01274             real_nz_ = 0;
<a name="l01275"></a>01275             imag_nz_ = 0;
<a name="l01276"></a>01276             free(real_ptr_);
<a name="l01277"></a>01277             real_ptr_ = NULL;
<a name="l01278"></a>01278             imag_ptr_ = NULL;
<a name="l01279"></a>01279             real_ind_ = NULL;
<a name="l01280"></a>01280             imag_ind_ = NULL;
<a name="l01281"></a>01281             this-&gt;real_data_ = NULL;
<a name="l01282"></a>01282             this-&gt;imag_data_ = NULL;
<a name="l01283"></a>01283           }
<a name="l01284"></a>01284         <span class="keywordflow">if</span> (imag_ptr_ == NULL)
<a name="l01285"></a>01285           {
<a name="l01286"></a>01286             this-&gt;m_ = 0;
<a name="l01287"></a>01287             this-&gt;n_ = 0;
<a name="l01288"></a>01288             real_nz_ = 0;
<a name="l01289"></a>01289             imag_nz_ = 0;
<a name="l01290"></a>01290             free(real_ptr_);
<a name="l01291"></a>01291             real_ptr_ = 0;
<a name="l01292"></a>01292             real_ind_ = NULL;
<a name="l01293"></a>01293             imag_ind_ = NULL;
<a name="l01294"></a>01294             this-&gt;real_data_ = NULL;
<a name="l01295"></a>01295             this-&gt;imag_data_ = NULL;
<a name="l01296"></a>01296           }
<a name="l01297"></a>01297         <span class="keywordflow">if</span> (imag_ptr_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l01298"></a>01298           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01299"></a>01299                          + <span class="stringliteral">&quot;Resize(int, int, int, int)&quot;</span>,
<a name="l01300"></a>01300                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01301"></a>01301                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (Storage::GetFirst(i, j)+1))
<a name="l01302"></a>01302                          + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j)+1)
<a name="l01303"></a>01303                          + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (for the&quot;</span>)
<a name="l01304"></a>01304                          + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; imaginary part), for a &quot;</span>)
<a name="l01305"></a>01305                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01306"></a>01306 <span class="preprocessor">#endif</span>
<a name="l01307"></a>01307 <span class="preprocessor"></span>      }
<a name="l01308"></a>01308 
<a name="l01309"></a>01309     <span class="keywordflow">if</span> (real_nz != real_nz_)
<a name="l01310"></a>01310       {
<a name="l01311"></a>01311 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01312"></a>01312 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l01313"></a>01313           {
<a name="l01314"></a>01314 <span class="preprocessor">#endif</span>
<a name="l01315"></a>01315 <span class="preprocessor"></span>
<a name="l01316"></a>01316             real_ind_
<a name="l01317"></a>01317               = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( realloc(real_ind_,
<a name="l01318"></a>01318                                                 real_nz*<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l01319"></a>01319 
<a name="l01320"></a>01320 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01321"></a>01321 <span class="preprocessor"></span>          }
<a name="l01322"></a>01322         <span class="keywordflow">catch</span> (...)
<a name="l01323"></a>01323           {
<a name="l01324"></a>01324             this-&gt;m_ = 0;
<a name="l01325"></a>01325             this-&gt;n_ = 0;
<a name="l01326"></a>01326             real_nz_ = 0;
<a name="l01327"></a>01327             imag_nz_ = 0;
<a name="l01328"></a>01328             free(real_ptr_);
<a name="l01329"></a>01329             free(imag_ptr_);
<a name="l01330"></a>01330             real_ptr_ = NULL;
<a name="l01331"></a>01331             imag_ptr_ = NULL;
<a name="l01332"></a>01332             real_ind_ = NULL;
<a name="l01333"></a>01333             imag_ind_ = NULL;
<a name="l01334"></a>01334             this-&gt;real_data_ = NULL;
<a name="l01335"></a>01335             this-&gt;imag_data_ = NULL;
<a name="l01336"></a>01336           }
<a name="l01337"></a>01337         <span class="keywordflow">if</span> (real_ind_ == NULL)
<a name="l01338"></a>01338           {
<a name="l01339"></a>01339             this-&gt;m_ = 0;
<a name="l01340"></a>01340             this-&gt;n_ = 0;
<a name="l01341"></a>01341             real_nz_ = 0;
<a name="l01342"></a>01342             imag_nz_ = 0;
<a name="l01343"></a>01343             free(real_ptr_);
<a name="l01344"></a>01344             free(imag_ptr_);
<a name="l01345"></a>01345             real_ptr_ = NULL;
<a name="l01346"></a>01346             imag_ptr_ = NULL;
<a name="l01347"></a>01347             this-&gt;real_data_ = NULL;
<a name="l01348"></a>01348             this-&gt;imag_data_ = NULL;
<a name="l01349"></a>01349           }
<a name="l01350"></a>01350         <span class="keywordflow">if</span> (real_ind_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l01351"></a>01351           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01352"></a>01352                          + <span class="stringliteral">&quot;Resize(int, int, int, int)&quot;</span>,
<a name="l01353"></a>01353                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01354"></a>01354                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * real_nz)
<a name="l01355"></a>01355                          + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz)
<a name="l01356"></a>01356                          + <span class="stringliteral">&quot; row or column indices (real part), for a &quot;</span>
<a name="l01357"></a>01357                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01358"></a>01358 <span class="preprocessor">#endif</span>
<a name="l01359"></a>01359 <span class="preprocessor"></span>      }
<a name="l01360"></a>01360 
<a name="l01361"></a>01361     <span class="keywordflow">if</span> (imag_nz != imag_nz_)
<a name="l01362"></a>01362       {
<a name="l01363"></a>01363 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01364"></a>01364 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l01365"></a>01365           {
<a name="l01366"></a>01366 <span class="preprocessor">#endif</span>
<a name="l01367"></a>01367 <span class="preprocessor"></span>
<a name="l01368"></a>01368             imag_ind_
<a name="l01369"></a>01369               = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( realloc(imag_ind_,
<a name="l01370"></a>01370                                                 imag_nz*<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l01371"></a>01371 
<a name="l01372"></a>01372 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01373"></a>01373 <span class="preprocessor"></span>          }
<a name="l01374"></a>01374         <span class="keywordflow">catch</span> (...)
<a name="l01375"></a>01375           {
<a name="l01376"></a>01376             this-&gt;m_ = 0;
<a name="l01377"></a>01377             this-&gt;n_ = 0;
<a name="l01378"></a>01378             real_nz_ = 0;
<a name="l01379"></a>01379             imag_nz_ = 0;
<a name="l01380"></a>01380             free(real_ptr_);
<a name="l01381"></a>01381             free(imag_ptr_);
<a name="l01382"></a>01382             real_ptr_ = NULL;
<a name="l01383"></a>01383             imag_ptr_ = NULL;
<a name="l01384"></a>01384             free(imag_ind_);
<a name="l01385"></a>01385             real_ind_ = NULL;
<a name="l01386"></a>01386             imag_ind_ = NULL;
<a name="l01387"></a>01387             this-&gt;real_data_ = NULL;
<a name="l01388"></a>01388             this-&gt;imag_data_ = NULL;
<a name="l01389"></a>01389           }
<a name="l01390"></a>01390         <span class="keywordflow">if</span> (real_ind_ == NULL)
<a name="l01391"></a>01391           {
<a name="l01392"></a>01392             this-&gt;m_ = 0;
<a name="l01393"></a>01393             this-&gt;n_ = 0;
<a name="l01394"></a>01394             real_nz_ = 0;
<a name="l01395"></a>01395             imag_nz_ = 0;
<a name="l01396"></a>01396             free(real_ptr_);
<a name="l01397"></a>01397             free(imag_ptr_);
<a name="l01398"></a>01398             real_ptr_ = NULL;
<a name="l01399"></a>01399             imag_ptr_ = NULL;
<a name="l01400"></a>01400             free(imag_ind_);
<a name="l01401"></a>01401             imag_ind_ = NULL;
<a name="l01402"></a>01402             this-&gt;real_data_ = NULL;
<a name="l01403"></a>01403             this-&gt;imag_data_ = NULL;
<a name="l01404"></a>01404           }
<a name="l01405"></a>01405         <span class="keywordflow">if</span> (imag_ind_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l01406"></a>01406           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01407"></a>01407                          + <span class="stringliteral">&quot;Resize(int, int, int, int)&quot;</span>,
<a name="l01408"></a>01408                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01409"></a>01409                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * imag_nz)
<a name="l01410"></a>01410                          + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz)
<a name="l01411"></a>01411                          + <span class="stringliteral">&quot; row or column indices (imaginary part), for a &quot;</span>
<a name="l01412"></a>01412                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01413"></a>01413 <span class="preprocessor">#endif</span>
<a name="l01414"></a>01414 <span class="preprocessor"></span>      }
<a name="l01415"></a>01415 
<a name="l01416"></a>01416     <span class="keywordflow">if</span> (real_nz != real_nz_)
<a name="l01417"></a>01417       {
<a name="l01418"></a>01418         <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> val;
<a name="l01419"></a>01419         val.SetData(real_nz_, real_data_);
<a name="l01420"></a>01420         val.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a99dda1b202ce562645f890897c593515" title="Changes the length of the vector, and keeps previous values.">Resize</a>(real_nz);
<a name="l01421"></a>01421 
<a name="l01422"></a>01422         real_data_ = val.<a class="code" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a" title="Returns a pointer to data_ (stored data).">GetData</a>();
<a name="l01423"></a>01423         val.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a151dc47113d07ab24775733d0c46dc90" title="Clears the vector without releasing memory.">Nullify</a>();
<a name="l01424"></a>01424       }
<a name="l01425"></a>01425 
<a name="l01426"></a>01426     <span class="keywordflow">if</span> (imag_nz != imag_nz_)
<a name="l01427"></a>01427       {
<a name="l01428"></a>01428         <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> val;
<a name="l01429"></a>01429         val.SetData(imag_nz_, imag_data_);
<a name="l01430"></a>01430         val.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a99dda1b202ce562645f890897c593515" title="Changes the length of the vector, and keeps previous values.">Resize</a>(imag_nz);
<a name="l01431"></a>01431 
<a name="l01432"></a>01432         imag_data_ = val.<a class="code" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a" title="Returns a pointer to data_ (stored data).">GetData</a>();
<a name="l01433"></a>01433         val.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a151dc47113d07ab24775733d0c46dc90" title="Clears the vector without releasing memory.">Nullify</a>();
<a name="l01434"></a>01434       }
<a name="l01435"></a>01435 
<a name="l01436"></a>01436     <span class="comment">// then filing last values of ptr_ with nz_</span>
<a name="l01437"></a>01437     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = Storage::GetFirst(this-&gt;m_, this-&gt;n_);
<a name="l01438"></a>01438          k &lt;= Storage::GetFirst(i, j); k++)
<a name="l01439"></a>01439       {
<a name="l01440"></a>01440         real_ptr_[k] = real_nz_;
<a name="l01441"></a>01441         imag_ptr_[k] = imag_nz_;
<a name="l01442"></a>01442       }
<a name="l01443"></a>01443 
<a name="l01444"></a>01444     this-&gt;m_ = i;
<a name="l01445"></a>01445     this-&gt;n_ = j;
<a name="l01446"></a>01446     real_nz_ = real_nz;
<a name="l01447"></a>01447     imag_nz_ = imag_nz;
<a name="l01448"></a>01448   }
<a name="l01449"></a>01449 
<a name="l01450"></a>01450 
<a name="l01452"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae583202802b373763ce3780fe2382f4f">01452</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01453"></a>01453   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae583202802b373763ce3780fe2382f4f" title="Copies a matrix.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l01454"></a>01454 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae583202802b373763ce3780fe2382f4f" title="Copies a matrix.">  Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l01455"></a>01455   {
<a name="l01456"></a>01456     this-&gt;<a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a8940cdf1faedc44051919b9774132fa9" title="Clears the matrix.">Clear</a>();
<a name="l01457"></a>01457     <span class="keywordtype">int</span> i = A.m_;
<a name="l01458"></a>01458     <span class="keywordtype">int</span> j = A.n_;
<a name="l01459"></a>01459     real_nz_ = A.real_nz_;
<a name="l01460"></a>01460     imag_nz_ = A.imag_nz_;
<a name="l01461"></a>01461     this-&gt;m_ = i;
<a name="l01462"></a>01462     this-&gt;n_ = j;
<a name="l01463"></a>01463     <span class="keywordflow">if</span> ((i == 0)||(j == 0))
<a name="l01464"></a>01464       {
<a name="l01465"></a>01465         this-&gt;m_ = 0;
<a name="l01466"></a>01466         this-&gt;n_ = 0;
<a name="l01467"></a>01467         this-&gt;real_nz_ = 0;
<a name="l01468"></a>01468         this-&gt;imag_nz_ = 0;
<a name="l01469"></a>01469         <span class="keywordflow">return</span>;
<a name="l01470"></a>01470       }
<a name="l01471"></a>01471 
<a name="l01472"></a>01472 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l01473"></a>01473 <span class="preprocessor"></span>    <span class="keywordflow">if</span> ((real_nz_ &gt; 0
<a name="l01474"></a>01474          &amp;&amp; (j == 0
<a name="l01475"></a>01475              || static_cast&lt;long int&gt;(real_nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l01476"></a>01476              &gt;= static_cast&lt;long int&gt;(i)))
<a name="l01477"></a>01477         ||
<a name="l01478"></a>01478         (imag_nz_ &gt; 0
<a name="l01479"></a>01479          &amp;&amp; (j == 0
<a name="l01480"></a>01480              || static_cast&lt;long int&gt;(imag_nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l01481"></a>01481              &gt;= static_cast&lt;long int&gt;(i))))
<a name="l01482"></a>01482       {
<a name="l01483"></a>01483         this-&gt;m_ = 0;
<a name="l01484"></a>01484         this-&gt;n_ = 0;
<a name="l01485"></a>01485         real_nz_ = 0;
<a name="l01486"></a>01486         imag_nz_ = 0;
<a name="l01487"></a>01487         real_ptr_ = NULL;
<a name="l01488"></a>01488         imag_ptr_ = NULL;
<a name="l01489"></a>01489         real_ind_ = NULL;
<a name="l01490"></a>01490         imag_ind_ = NULL;
<a name="l01491"></a>01491         this-&gt;real_data_ = NULL;
<a name="l01492"></a>01492         this-&gt;imag_data_ = NULL;
<a name="l01493"></a>01493         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01494"></a>01494                        + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01495"></a>01495                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz_)
<a name="l01496"></a>01496                        + <span class="stringliteral">&quot; values for the real part and &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz_)
<a name="l01497"></a>01497                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; values for the imaginary part) than&quot;</span>)
<a name="l01498"></a>01498                        + <span class="stringliteral">&quot; elements in the matrix (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span>
<a name="l01499"></a>01499                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l01500"></a>01500       }
<a name="l01501"></a>01501 <span class="preprocessor">#endif</span>
<a name="l01502"></a>01502 <span class="preprocessor"></span>
<a name="l01503"></a>01503 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01504"></a>01504 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01505"></a>01505       {
<a name="l01506"></a>01506 <span class="preprocessor">#endif</span>
<a name="l01507"></a>01507 <span class="preprocessor"></span>
<a name="l01508"></a>01508         real_ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(Storage::GetFirst(i, j)+1,
<a name="l01509"></a>01509                                                    <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l01510"></a>01510         memcpy(this-&gt;real_ptr_, A.real_ptr_,
<a name="l01511"></a>01511                (Storage::GetFirst(i, j) + 1) * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01512"></a>01512 
<a name="l01513"></a>01513 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01514"></a>01514 <span class="preprocessor"></span>      }
<a name="l01515"></a>01515     <span class="keywordflow">catch</span> (...)
<a name="l01516"></a>01516       {
<a name="l01517"></a>01517         this-&gt;m_ = 0;
<a name="l01518"></a>01518         this-&gt;n_ = 0;
<a name="l01519"></a>01519         real_nz_ = 0;
<a name="l01520"></a>01520         imag_nz_ = 0;
<a name="l01521"></a>01521         real_ptr_ = NULL;
<a name="l01522"></a>01522         imag_ptr_ = NULL;
<a name="l01523"></a>01523         real_ind_ = NULL;
<a name="l01524"></a>01524         imag_ind_ = NULL;
<a name="l01525"></a>01525         this-&gt;real_data_ = NULL;
<a name="l01526"></a>01526         this-&gt;imag_data_ = NULL;
<a name="l01527"></a>01527       }
<a name="l01528"></a>01528     <span class="keywordflow">if</span> (real_ptr_ == NULL)
<a name="l01529"></a>01529       {
<a name="l01530"></a>01530         this-&gt;m_ = 0;
<a name="l01531"></a>01531         this-&gt;n_ = 0;
<a name="l01532"></a>01532         real_nz_ = 0;
<a name="l01533"></a>01533         imag_nz_ = 0;
<a name="l01534"></a>01534         imag_ptr_ = 0;
<a name="l01535"></a>01535         real_ind_ = NULL;
<a name="l01536"></a>01536         imag_ind_ = NULL;
<a name="l01537"></a>01537         this-&gt;real_data_ = NULL;
<a name="l01538"></a>01538         this-&gt;imag_data_ = NULL;
<a name="l01539"></a>01539       }
<a name="l01540"></a>01540     <span class="keywordflow">if</span> (real_ptr_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l01541"></a>01541       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01542"></a>01542                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01543"></a>01543                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01544"></a>01544                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (Storage::GetFirst(i, j)+1))
<a name="l01545"></a>01545                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j)+1)
<a name="l01546"></a>01546                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (for the real&quot;</span>)
<a name="l01547"></a>01547                      + <span class="stringliteral">&quot; part), for a &quot;</span>
<a name="l01548"></a>01548                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01549"></a>01549 <span class="preprocessor">#endif</span>
<a name="l01550"></a>01550 <span class="preprocessor"></span>
<a name="l01551"></a>01551 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01552"></a>01552 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01553"></a>01553       {
<a name="l01554"></a>01554 <span class="preprocessor">#endif</span>
<a name="l01555"></a>01555 <span class="preprocessor"></span>
<a name="l01556"></a>01556         imag_ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(Storage::GetFirst(i, j)+1,
<a name="l01557"></a>01557                                                    <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l01558"></a>01558         memcpy(this-&gt;imag_ptr_, A.imag_ptr_,
<a name="l01559"></a>01559                (Storage::GetFirst(i, j) + 1) * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01560"></a>01560 
<a name="l01561"></a>01561 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01562"></a>01562 <span class="preprocessor"></span>      }
<a name="l01563"></a>01563     <span class="keywordflow">catch</span> (...)
<a name="l01564"></a>01564       {
<a name="l01565"></a>01565         this-&gt;m_ = 0;
<a name="l01566"></a>01566         this-&gt;n_ = 0;
<a name="l01567"></a>01567         real_nz_ = 0;
<a name="l01568"></a>01568         imag_nz_ = 0;
<a name="l01569"></a>01569         free(real_ptr_);
<a name="l01570"></a>01570         real_ptr_ = NULL;
<a name="l01571"></a>01571         imag_ptr_ = NULL;
<a name="l01572"></a>01572         real_ind_ = NULL;
<a name="l01573"></a>01573         imag_ind_ = NULL;
<a name="l01574"></a>01574         this-&gt;real_data_ = NULL;
<a name="l01575"></a>01575         this-&gt;imag_data_ = NULL;
<a name="l01576"></a>01576       }
<a name="l01577"></a>01577     <span class="keywordflow">if</span> (imag_ptr_ == NULL)
<a name="l01578"></a>01578       {
<a name="l01579"></a>01579         this-&gt;m_ = 0;
<a name="l01580"></a>01580         this-&gt;n_ = 0;
<a name="l01581"></a>01581         real_nz_ = 0;
<a name="l01582"></a>01582         imag_nz_ = 0;
<a name="l01583"></a>01583         free(real_ptr_);
<a name="l01584"></a>01584         real_ptr_ = 0;
<a name="l01585"></a>01585         real_ind_ = NULL;
<a name="l01586"></a>01586         imag_ind_ = NULL;
<a name="l01587"></a>01587         this-&gt;real_data_ = NULL;
<a name="l01588"></a>01588         this-&gt;imag_data_ = NULL;
<a name="l01589"></a>01589       }
<a name="l01590"></a>01590     <span class="keywordflow">if</span> (imag_ptr_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l01591"></a>01591       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01592"></a>01592                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01593"></a>01593                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01594"></a>01594                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (Storage::GetFirst(i, j)+1))
<a name="l01595"></a>01595                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j)+1)
<a name="l01596"></a>01596                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (for the&quot;</span>)
<a name="l01597"></a>01597                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; imaginary part), for a &quot;</span>)
<a name="l01598"></a>01598                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01599"></a>01599 <span class="preprocessor">#endif</span>
<a name="l01600"></a>01600 <span class="preprocessor"></span>
<a name="l01601"></a>01601 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01602"></a>01602 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01603"></a>01603       {
<a name="l01604"></a>01604 <span class="preprocessor">#endif</span>
<a name="l01605"></a>01605 <span class="preprocessor"></span>
<a name="l01606"></a>01606         real_ind_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(real_nz_, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l01607"></a>01607         memcpy(this-&gt;real_ind_, A.real_ind_, real_nz_ * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01608"></a>01608 
<a name="l01609"></a>01609 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01610"></a>01610 <span class="preprocessor"></span>      }
<a name="l01611"></a>01611     <span class="keywordflow">catch</span> (...)
<a name="l01612"></a>01612       {
<a name="l01613"></a>01613         this-&gt;m_ = 0;
<a name="l01614"></a>01614         this-&gt;n_ = 0;
<a name="l01615"></a>01615         real_nz_ = 0;
<a name="l01616"></a>01616         imag_nz_ = 0;
<a name="l01617"></a>01617         free(real_ptr_);
<a name="l01618"></a>01618         free(imag_ptr_);
<a name="l01619"></a>01619         real_ptr_ = NULL;
<a name="l01620"></a>01620         imag_ptr_ = NULL;
<a name="l01621"></a>01621         real_ind_ = NULL;
<a name="l01622"></a>01622         imag_ind_ = NULL;
<a name="l01623"></a>01623         this-&gt;real_data_ = NULL;
<a name="l01624"></a>01624         this-&gt;imag_data_ = NULL;
<a name="l01625"></a>01625       }
<a name="l01626"></a>01626     <span class="keywordflow">if</span> (real_ind_ == NULL)
<a name="l01627"></a>01627       {
<a name="l01628"></a>01628         this-&gt;m_ = 0;
<a name="l01629"></a>01629         this-&gt;n_ = 0;
<a name="l01630"></a>01630         real_nz_ = 0;
<a name="l01631"></a>01631         imag_nz_ = 0;
<a name="l01632"></a>01632         free(real_ptr_);
<a name="l01633"></a>01633         free(imag_ptr_);
<a name="l01634"></a>01634         real_ptr_ = NULL;
<a name="l01635"></a>01635         imag_ptr_ = NULL;
<a name="l01636"></a>01636         this-&gt;real_data_ = NULL;
<a name="l01637"></a>01637         this-&gt;imag_data_ = NULL;
<a name="l01638"></a>01638       }
<a name="l01639"></a>01639     <span class="keywordflow">if</span> (real_ind_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l01640"></a>01640       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01641"></a>01641                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01642"></a>01642                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01643"></a>01643                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * real_nz_)
<a name="l01644"></a>01644                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz_)
<a name="l01645"></a>01645                      + <span class="stringliteral">&quot; row or column indices (real part), for a &quot;</span>
<a name="l01646"></a>01646                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01647"></a>01647 <span class="preprocessor">#endif</span>
<a name="l01648"></a>01648 <span class="preprocessor"></span>
<a name="l01649"></a>01649 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01650"></a>01650 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01651"></a>01651       {
<a name="l01652"></a>01652 <span class="preprocessor">#endif</span>
<a name="l01653"></a>01653 <span class="preprocessor"></span>
<a name="l01654"></a>01654         imag_ind_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(imag_nz_, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l01655"></a>01655         memcpy(this-&gt;imag_ind_, A.imag_ind_, imag_nz_ * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01656"></a>01656 
<a name="l01657"></a>01657 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01658"></a>01658 <span class="preprocessor"></span>      }
<a name="l01659"></a>01659     <span class="keywordflow">catch</span> (...)
<a name="l01660"></a>01660       {
<a name="l01661"></a>01661         this-&gt;m_ = 0;
<a name="l01662"></a>01662         this-&gt;n_ = 0;
<a name="l01663"></a>01663         real_nz_ = 0;
<a name="l01664"></a>01664         imag_nz_ = 0;
<a name="l01665"></a>01665         free(real_ptr_);
<a name="l01666"></a>01666         free(imag_ptr_);
<a name="l01667"></a>01667         real_ptr_ = NULL;
<a name="l01668"></a>01668         imag_ptr_ = NULL;
<a name="l01669"></a>01669         free(imag_ind_);
<a name="l01670"></a>01670         real_ind_ = NULL;
<a name="l01671"></a>01671         imag_ind_ = NULL;
<a name="l01672"></a>01672         this-&gt;real_data_ = NULL;
<a name="l01673"></a>01673         this-&gt;imag_data_ = NULL;
<a name="l01674"></a>01674       }
<a name="l01675"></a>01675     <span class="keywordflow">if</span> (real_ind_ == NULL)
<a name="l01676"></a>01676       {
<a name="l01677"></a>01677         this-&gt;m_ = 0;
<a name="l01678"></a>01678         this-&gt;n_ = 0;
<a name="l01679"></a>01679         real_nz_ = 0;
<a name="l01680"></a>01680         imag_nz_ = 0;
<a name="l01681"></a>01681         free(real_ptr_);
<a name="l01682"></a>01682         free(imag_ptr_);
<a name="l01683"></a>01683         real_ptr_ = NULL;
<a name="l01684"></a>01684         imag_ptr_ = NULL;
<a name="l01685"></a>01685         free(imag_ind_);
<a name="l01686"></a>01686         imag_ind_ = NULL;
<a name="l01687"></a>01687         this-&gt;real_data_ = NULL;
<a name="l01688"></a>01688         this-&gt;imag_data_ = NULL;
<a name="l01689"></a>01689       }
<a name="l01690"></a>01690     <span class="keywordflow">if</span> (imag_ind_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l01691"></a>01691       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01692"></a>01692                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01693"></a>01693                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01694"></a>01694                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * imag_nz_)
<a name="l01695"></a>01695                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz_)
<a name="l01696"></a>01696                      + <span class="stringliteral">&quot; row or column indices (imaginary part), for a &quot;</span>
<a name="l01697"></a>01697                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01698"></a>01698 <span class="preprocessor">#endif</span>
<a name="l01699"></a>01699 <span class="preprocessor"></span>
<a name="l01700"></a>01700 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01701"></a>01701 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01702"></a>01702       {
<a name="l01703"></a>01703 <span class="preprocessor">#endif</span>
<a name="l01704"></a>01704 <span class="preprocessor"></span>
<a name="l01705"></a>01705         this-&gt;real_data_ = this-&gt;allocator_.allocate(real_nz_, <span class="keyword">this</span>);
<a name="l01706"></a>01706         this-&gt;allocator_.memorycpy(this-&gt;real_data_, A.real_data_, real_nz_);
<a name="l01707"></a>01707 
<a name="l01708"></a>01708 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01709"></a>01709 <span class="preprocessor"></span>      }
<a name="l01710"></a>01710     <span class="keywordflow">catch</span> (...)
<a name="l01711"></a>01711       {
<a name="l01712"></a>01712         this-&gt;m_ = 0;
<a name="l01713"></a>01713         this-&gt;n_ = 0;
<a name="l01714"></a>01714         free(real_ptr_);
<a name="l01715"></a>01715         free(imag_ptr_);
<a name="l01716"></a>01716         real_ptr_ = NULL;
<a name="l01717"></a>01717         imag_ptr_ = NULL;
<a name="l01718"></a>01718         free(real_ind_);
<a name="l01719"></a>01719         free(imag_ind_);
<a name="l01720"></a>01720         real_ind_ = NULL;
<a name="l01721"></a>01721         imag_ind_ = NULL;
<a name="l01722"></a>01722         this-&gt;real_data_ = NULL;
<a name="l01723"></a>01723         this-&gt;imag_data_ = NULL;
<a name="l01724"></a>01724       }
<a name="l01725"></a>01725     <span class="keywordflow">if</span> (real_data_ == NULL)
<a name="l01726"></a>01726       {
<a name="l01727"></a>01727         this-&gt;m_ = 0;
<a name="l01728"></a>01728         this-&gt;n_ = 0;
<a name="l01729"></a>01729         free(real_ptr_);
<a name="l01730"></a>01730         free(imag_ptr_);
<a name="l01731"></a>01731         real_ptr_ = NULL;
<a name="l01732"></a>01732         imag_ptr_ = NULL;
<a name="l01733"></a>01733         free(real_ind_);
<a name="l01734"></a>01734         free(imag_ind_);
<a name="l01735"></a>01735         real_ind_ = NULL;
<a name="l01736"></a>01736         imag_ind_ = NULL;
<a name="l01737"></a>01737         imag_data_ = NULL;
<a name="l01738"></a>01738       }
<a name="l01739"></a>01739     <span class="keywordflow">if</span> (real_data_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l01740"></a>01740       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01741"></a>01741                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01742"></a>01742                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01743"></a>01743                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * real_nz_)
<a name="l01744"></a>01744                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz_)
<a name="l01745"></a>01745                      + <span class="stringliteral">&quot; values (real part), for a &quot;</span>
<a name="l01746"></a>01746                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01747"></a>01747 <span class="preprocessor">#endif</span>
<a name="l01748"></a>01748 <span class="preprocessor"></span>
<a name="l01749"></a>01749 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01750"></a>01750 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01751"></a>01751       {
<a name="l01752"></a>01752 <span class="preprocessor">#endif</span>
<a name="l01753"></a>01753 <span class="preprocessor"></span>
<a name="l01754"></a>01754         this-&gt;imag_data_ = this-&gt;allocator_.allocate(imag_nz_, <span class="keyword">this</span>);
<a name="l01755"></a>01755         this-&gt;allocator_.memorycpy(this-&gt;imag_data_, A.imag_data_, imag_nz_);
<a name="l01756"></a>01756 
<a name="l01757"></a>01757 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01758"></a>01758 <span class="preprocessor"></span>      }
<a name="l01759"></a>01759     <span class="keywordflow">catch</span> (...)
<a name="l01760"></a>01760       {
<a name="l01761"></a>01761         this-&gt;m_ = 0;
<a name="l01762"></a>01762         this-&gt;n_ = 0;
<a name="l01763"></a>01763         free(real_ptr_);
<a name="l01764"></a>01764         free(imag_ptr_);
<a name="l01765"></a>01765         real_ptr_ = NULL;
<a name="l01766"></a>01766         imag_ptr_ = NULL;
<a name="l01767"></a>01767         free(real_ind_);
<a name="l01768"></a>01768         free(imag_ind_);
<a name="l01769"></a>01769         real_ind_ = NULL;
<a name="l01770"></a>01770         imag_ind_ = NULL;
<a name="l01771"></a>01771         this-&gt;allocator_.deallocate(this-&gt;real_data_, real_nz_);
<a name="l01772"></a>01772         this-&gt;real_data_ = NULL;
<a name="l01773"></a>01773         this-&gt;imag_data_ = NULL;
<a name="l01774"></a>01774       }
<a name="l01775"></a>01775     <span class="keywordflow">if</span> (real_data_ == NULL)
<a name="l01776"></a>01776       {
<a name="l01777"></a>01777         this-&gt;m_ = 0;
<a name="l01778"></a>01778         this-&gt;n_ = 0;
<a name="l01779"></a>01779         free(real_ptr_);
<a name="l01780"></a>01780         free(imag_ptr_);
<a name="l01781"></a>01781         real_ptr_ = NULL;
<a name="l01782"></a>01782         imag_ptr_ = NULL;
<a name="l01783"></a>01783         free(real_ind_);
<a name="l01784"></a>01784         free(imag_ind_);
<a name="l01785"></a>01785         real_ind_ = NULL;
<a name="l01786"></a>01786         imag_ind_ = NULL;
<a name="l01787"></a>01787         this-&gt;allocator_.deallocate(this-&gt;real_data_, real_nz_);
<a name="l01788"></a>01788         real_data_ = NULL;
<a name="l01789"></a>01789       }
<a name="l01790"></a>01790     <span class="keywordflow">if</span> (imag_data_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l01791"></a>01791       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01792"></a>01792                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01793"></a>01793                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01794"></a>01794                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * imag_nz_)
<a name="l01795"></a>01795                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz_)
<a name="l01796"></a>01796                      + <span class="stringliteral">&quot; values (imaginary part), for a &quot;</span>
<a name="l01797"></a>01797                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01798"></a>01798 <span class="preprocessor">#endif</span>
<a name="l01799"></a>01799 <span class="preprocessor"></span>
<a name="l01800"></a>01800   }
<a name="l01801"></a>01801 
<a name="l01802"></a>01802 
<a name="l01803"></a>01803   <span class="comment">/*******************</span>
<a name="l01804"></a>01804 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l01805"></a>01805 <span class="comment">   *******************/</span>
<a name="l01806"></a>01806 
<a name="l01807"></a>01807 
<a name="l01809"></a>01809 
<a name="l01815"></a>01815   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01816"></a>01816   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a543d8834abfbd7888c6d2516612bde7b" title="Returns the number of elements stored in memory.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetDataSize</a>()<span class="keyword"> const</span>
<a name="l01817"></a>01817 <span class="keyword">  </span>{
<a name="l01818"></a>01818     <span class="keywordflow">return</span> real_nz_ + imag_nz_;
<a name="l01819"></a>01819   }
<a name="l01820"></a>01820 
<a name="l01821"></a>01821 
<a name="l01823"></a>01823 
<a name="l01827"></a>01827   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01828"></a>01828   <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#afa783bff3e823f8ad2480a50d4d28929" title="Returns (row or column) start indices for the real part.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetRealPtr</a>()<span class="keyword"> const</span>
<a name="l01829"></a>01829 <span class="keyword">  </span>{
<a name="l01830"></a>01830     <span class="keywordflow">return</span> real_ptr_;
<a name="l01831"></a>01831   }
<a name="l01832"></a>01832 
<a name="l01833"></a>01833 
<a name="l01835"></a>01835 
<a name="l01839"></a>01839   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01840"></a>01840   <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a6766b6db59aa35b4fa7458a12e531e9b" title="Returns (row or column) start indices for the imaginary part.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetImagPtr</a>()<span class="keyword"> const</span>
<a name="l01841"></a>01841 <span class="keyword">  </span>{
<a name="l01842"></a>01842     <span class="keywordflow">return</span> imag_ptr_;
<a name="l01843"></a>01843   }
<a name="l01844"></a>01844 
<a name="l01845"></a>01845 
<a name="l01847"></a>01847 
<a name="l01854"></a>01854   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01855"></a>01855   <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a4eb0b0535cb51c9c0886da147f6e0cb0" title="Returns (row or column) indices of non-zero entries for the real part.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetRealInd</a>()<span class="keyword"> const</span>
<a name="l01856"></a>01856 <span class="keyword">  </span>{
<a name="l01857"></a>01857     <span class="keywordflow">return</span> real_ind_;
<a name="l01858"></a>01858   }
<a name="l01859"></a>01859 
<a name="l01860"></a>01860 
<a name="l01863"></a>01863 
<a name="l01870"></a>01870   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01871"></a>01871   <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a08f3d91609557b1650066765343909db" title="Returns (row or column) indices of non-zero entries //! for the imaginary part.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetImagInd</a>()<span class="keyword"> const</span>
<a name="l01872"></a>01872 <span class="keyword">  </span>{
<a name="l01873"></a>01873     <span class="keywordflow">return</span> imag_ind_;
<a name="l01874"></a>01874   }
<a name="l01875"></a>01875 
<a name="l01876"></a>01876 
<a name="l01878"></a>01878 
<a name="l01881"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3be4bf190b7c5957d81ddf7e7a7b7882">01881</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01882"></a>01882   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3be4bf190b7c5957d81ddf7e7a7b7882" title="Returns the length of the array of start indices for the real part.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01883"></a>01883 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3be4bf190b7c5957d81ddf7e7a7b7882" title="Returns the length of the array of start indices for the real part.">  ::GetRealPtrSize</a>()<span class="keyword"> const</span>
<a name="l01884"></a>01884 <span class="keyword">  </span>{
<a name="l01885"></a>01885     <span class="keywordflow">return</span> (Storage::GetFirst(this-&gt;m_, this-&gt;n_) + 1);
<a name="l01886"></a>01886   }
<a name="l01887"></a>01887 
<a name="l01888"></a>01888 
<a name="l01890"></a>01890 
<a name="l01893"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae97357d4afa167ca95d28e8897e44ef2">01893</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01894"></a>01894   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae97357d4afa167ca95d28e8897e44ef2" title="Returns the length of the array of start indices for the imaginary part.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01895"></a>01895 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae97357d4afa167ca95d28e8897e44ef2" title="Returns the length of the array of start indices for the imaginary part.">  ::GetImagPtrSize</a>()<span class="keyword"> const</span>
<a name="l01896"></a>01896 <span class="keyword">  </span>{
<a name="l01897"></a>01897     <span class="keywordflow">return</span> (Storage::GetFirst(this-&gt;m_, this-&gt;n_) + 1);
<a name="l01898"></a>01898   }
<a name="l01899"></a>01899 
<a name="l01900"></a>01900 
<a name="l01903"></a>01903 
<a name="l01912"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae573e32219c095276106254a84e7f684">01912</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01913"></a>01913   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae573e32219c095276106254a84e7f684" title="Returns the length of the array of (column or row) indices //! for the real part...">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01914"></a>01914 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae573e32219c095276106254a84e7f684" title="Returns the length of the array of (column or row) indices //! for the real part...">  ::GetRealIndSize</a>()<span class="keyword"> const</span>
<a name="l01915"></a>01915 <span class="keyword">  </span>{
<a name="l01916"></a>01916     <span class="keywordflow">return</span> real_nz_;
<a name="l01917"></a>01917   }
<a name="l01918"></a>01918 
<a name="l01919"></a>01919 
<a name="l01922"></a>01922 
<a name="l01931"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad61069ad96f1bfd7b57f6a4e9fe85764">01931</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01932"></a>01932   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad61069ad96f1bfd7b57f6a4e9fe85764" title="Returns the length of the array of (column or row) indices //! for the imaginary...">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01933"></a>01933 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad61069ad96f1bfd7b57f6a4e9fe85764" title="Returns the length of the array of (column or row) indices //! for the imaginary...">  ::GetImagIndSize</a>()<span class="keyword"> const</span>
<a name="l01934"></a>01934 <span class="keyword">  </span>{
<a name="l01935"></a>01935     <span class="keywordflow">return</span> imag_nz_;
<a name="l01936"></a>01936   }
<a name="l01937"></a>01937 
<a name="l01938"></a>01938 
<a name="l01941"></a>01941 
<a name="l01950"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a00680b1689e564a1ed51dd34c4dcd0c9">01950</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01951"></a>01951   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a00680b1689e564a1ed51dd34c4dcd0c9" title="Returns the length of the array of (column or row) indices //! for the real part...">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01952"></a>01952 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a00680b1689e564a1ed51dd34c4dcd0c9" title="Returns the length of the array of (column or row) indices //! for the real part...">  ::GetRealDataSize</a>()<span class="keyword"> const</span>
<a name="l01953"></a>01953 <span class="keyword">  </span>{
<a name="l01954"></a>01954     <span class="keywordflow">return</span> real_nz_;
<a name="l01955"></a>01955   }
<a name="l01956"></a>01956 
<a name="l01957"></a>01957 
<a name="l01960"></a>01960 
<a name="l01969"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a2aa0e0d1430941ccfecb8e2947801570">01969</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01970"></a>01970   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a2aa0e0d1430941ccfecb8e2947801570" title="Returns the length of the array of (column or row) indices //! for the imaginary...">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01971"></a>01971 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a2aa0e0d1430941ccfecb8e2947801570" title="Returns the length of the array of (column or row) indices //! for the imaginary...">  ::GetImagDataSize</a>()<span class="keyword"> const</span>
<a name="l01972"></a>01972 <span class="keyword">  </span>{
<a name="l01973"></a>01973     <span class="keywordflow">return</span> imag_nz_;
<a name="l01974"></a>01974   }
<a name="l01975"></a>01975 
<a name="l01976"></a>01976 
<a name="l01978"></a>01978 
<a name="l01981"></a>01981   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01982"></a>01982   T* <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a74325c4f424c0043682b5cb3ad3df501" title="Returns the array of values of the real part.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetRealData</a>()<span class="keyword"> const</span>
<a name="l01983"></a>01983 <span class="keyword">  </span>{
<a name="l01984"></a>01984     <span class="keywordflow">return</span> real_data_;
<a name="l01985"></a>01985   }
<a name="l01986"></a>01986 
<a name="l01987"></a>01987 
<a name="l01989"></a>01989 
<a name="l01992"></a>01992   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01993"></a>01993   T* <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a5637e2dd95b0e62a7f42c0f3fd1383b1" title="Returns the array of values of the imaginary part.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetImagData</a>()<span class="keyword"> const</span>
<a name="l01994"></a>01994 <span class="keyword">  </span>{
<a name="l01995"></a>01995     <span class="keywordflow">return</span> imag_data_;
<a name="l01996"></a>01996   }
<a name="l01997"></a>01997 
<a name="l01998"></a>01998 
<a name="l01999"></a>01999   <span class="comment">/**********************************</span>
<a name="l02000"></a>02000 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l02001"></a>02001 <span class="comment">   **********************************/</span>
<a name="l02002"></a>02002 
<a name="l02003"></a>02003 
<a name="l02005"></a>02005 
<a name="l02011"></a>02011   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02012"></a>02012   <span class="keyword">inline</span> complex&lt;typename Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;
<a name="l02013"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad9e481e5ea6de45867d8952dd9676bea">02013</a>                  ::value_type&gt;
<a name="l02014"></a>02014   <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad9e481e5ea6de45867d8952dd9676bea" title="Access operator.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l02015"></a>02015 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad9e481e5ea6de45867d8952dd9676bea" title="Access operator.">  ::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l02016"></a>02016 <span class="keyword">  </span>{
<a name="l02017"></a>02017 
<a name="l02018"></a>02018 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02019"></a>02019 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02020"></a>02020       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::operator()&quot;</span>,
<a name="l02021"></a>02021                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l02022"></a>02022                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02023"></a>02023     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02024"></a>02024       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::operator()&quot;</span>,
<a name="l02025"></a>02025                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l02026"></a>02026                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02027"></a>02027 <span class="preprocessor">#endif</span>
<a name="l02028"></a>02028 <span class="preprocessor"></span>
<a name="l02029"></a>02029     <span class="keywordtype">int</span> real_k, imag_k, l;
<a name="l02030"></a>02030     <span class="keywordtype">int</span> real_a, real_b;
<a name="l02031"></a>02031     <span class="keywordtype">int</span> imag_a, imag_b;
<a name="l02032"></a>02032 
<a name="l02033"></a>02033     real_a = real_ptr_[Storage::GetFirst(i, j)];
<a name="l02034"></a>02034     real_b = real_ptr_[Storage::GetFirst(i, j) + 1];
<a name="l02035"></a>02035 
<a name="l02036"></a>02036     imag_a = imag_ptr_[Storage::GetFirst(i, j)];
<a name="l02037"></a>02037     imag_b = imag_ptr_[Storage::GetFirst(i, j) + 1];
<a name="l02038"></a>02038 
<a name="l02039"></a>02039     <span class="keywordflow">if</span> (real_a != real_b)
<a name="l02040"></a>02040       {
<a name="l02041"></a>02041         l = Storage::GetSecond(i, j);
<a name="l02042"></a>02042         <span class="keywordflow">for</span> (real_k = real_a;
<a name="l02043"></a>02043              (real_k &lt;real_b-1) &amp;&amp; (real_ind_[real_k] &lt; l);
<a name="l02044"></a>02044              real_k++);
<a name="l02045"></a>02045         <span class="keywordflow">if</span> (imag_a != imag_b)
<a name="l02046"></a>02046           {
<a name="l02047"></a>02047             <span class="keywordflow">for</span> (imag_k = imag_a;
<a name="l02048"></a>02048                  (imag_k &lt; imag_b-1) &amp;&amp; (imag_ind_[imag_k] &lt; l);
<a name="l02049"></a>02049                  imag_k++);
<a name="l02050"></a>02050             <span class="keywordflow">if</span> (real_ind_[real_k] == l)
<a name="l02051"></a>02051               {
<a name="l02052"></a>02052                 <span class="keywordflow">if</span> (imag_ind_[imag_k] == l)
<a name="l02053"></a>02053                   <span class="keywordflow">return</span> complex&lt;T&gt;(real_data_[real_k], imag_data_[imag_k]);
<a name="l02054"></a>02054                 <span class="keywordflow">else</span>
<a name="l02055"></a>02055                   <span class="keywordflow">return</span> complex&lt;T&gt;(real_data_[real_k], T(0));
<a name="l02056"></a>02056               }
<a name="l02057"></a>02057             <span class="keywordflow">else</span>
<a name="l02058"></a>02058               <span class="keywordflow">if</span> (imag_ind_[imag_k] == l)
<a name="l02059"></a>02059                 <span class="keywordflow">return</span> complex&lt;T&gt;(T(0), imag_data_[imag_k]);
<a name="l02060"></a>02060               <span class="keywordflow">else</span>
<a name="l02061"></a>02061                 <span class="keywordflow">return</span> complex&lt;T&gt;(T(0), T(0));
<a name="l02062"></a>02062           }
<a name="l02063"></a>02063         <span class="keywordflow">else</span>
<a name="l02064"></a>02064           {
<a name="l02065"></a>02065             <span class="keywordflow">if</span> (real_ind_[real_k] == l)
<a name="l02066"></a>02066               <span class="keywordflow">return</span> complex&lt;T&gt;(real_data_[real_k], T(0));
<a name="l02067"></a>02067             <span class="keywordflow">else</span>
<a name="l02068"></a>02068               <span class="keywordflow">return</span> complex&lt;T&gt;(T(0), T(0));
<a name="l02069"></a>02069           }
<a name="l02070"></a>02070       }
<a name="l02071"></a>02071     <span class="keywordflow">else</span>
<a name="l02072"></a>02072       {
<a name="l02073"></a>02073         <span class="keywordflow">if</span> (imag_a != imag_b)
<a name="l02074"></a>02074           {
<a name="l02075"></a>02075             l = Storage::GetSecond(i, j);
<a name="l02076"></a>02076             <span class="keywordflow">for</span> (imag_k = imag_a;
<a name="l02077"></a>02077                  (imag_k &lt; imag_b-1) &amp;&amp; (imag_ind_[imag_k] &lt; l);
<a name="l02078"></a>02078                  imag_k++);
<a name="l02079"></a>02079             <span class="keywordflow">if</span> (imag_ind_[imag_k] == l)
<a name="l02080"></a>02080               <span class="keywordflow">return</span> complex&lt;T&gt;(T(0), imag_data_[imag_k]);
<a name="l02081"></a>02081             <span class="keywordflow">else</span>
<a name="l02082"></a>02082               <span class="keywordflow">return</span> complex&lt;T&gt;(T(0), T(0));
<a name="l02083"></a>02083           }
<a name="l02084"></a>02084         <span class="keywordflow">else</span>
<a name="l02085"></a>02085           <span class="keywordflow">return</span> complex&lt;T&gt;(T(0), T(0));
<a name="l02086"></a>02086       }
<a name="l02087"></a>02087 
<a name="l02088"></a>02088   }
<a name="l02089"></a>02089 
<a name="l02090"></a>02090 
<a name="l02092"></a>02092 
<a name="l02100"></a>02100   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02101"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a928e2aa74c9ad336e77ab76593b3c8da">02101</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l02102"></a>02102   ::value_type&amp;
<a name="l02103"></a>02103   <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a928e2aa74c9ad336e77ab76593b3c8da" title="Access method.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::ValReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l02104"></a>02104   {
<a name="l02105"></a>02105 
<a name="l02106"></a>02106 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02107"></a>02107 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02108"></a>02108       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::ValReal(int, int)&quot;</span>,
<a name="l02109"></a>02109                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l02110"></a>02110                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02111"></a>02111     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02112"></a>02112       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::ValReal(int, int)&quot;</span>,
<a name="l02113"></a>02113                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l02114"></a>02114                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02115"></a>02115 <span class="preprocessor">#endif</span>
<a name="l02116"></a>02116 <span class="preprocessor"></span>
<a name="l02117"></a>02117     <span class="keywordtype">int</span> k, l;
<a name="l02118"></a>02118     <span class="keywordtype">int</span> a, b;
<a name="l02119"></a>02119 
<a name="l02120"></a>02120     a = real_ptr_[Storage::GetFirst(i, j)];
<a name="l02121"></a>02121     b = real_ptr_[Storage::GetFirst(i, j) + 1];
<a name="l02122"></a>02122 
<a name="l02123"></a>02123     <span class="keywordflow">if</span> (a == b)
<a name="l02124"></a>02124       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::ValReal(int, int)&quot;</span>,
<a name="l02125"></a>02125                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l02126"></a>02126                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l02127"></a>02127                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l02128"></a>02128 
<a name="l02129"></a>02129     l = Storage::GetSecond(i, j);
<a name="l02130"></a>02130 
<a name="l02131"></a>02131     <span class="keywordflow">for</span> (k = a; (k &lt; b-1) &amp;&amp; (real_ind_[k] &lt; l); k++);
<a name="l02132"></a>02132 
<a name="l02133"></a>02133     <span class="keywordflow">if</span> (real_ind_[k] == l)
<a name="l02134"></a>02134       <span class="keywordflow">return</span> this-&gt;real_data_[k];
<a name="l02135"></a>02135     <span class="keywordflow">else</span>
<a name="l02136"></a>02136       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::ValReal(int, int)&quot;</span>,
<a name="l02137"></a>02137                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l02138"></a>02138                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l02139"></a>02139                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l02140"></a>02140   }
<a name="l02141"></a>02141 
<a name="l02142"></a>02142 
<a name="l02144"></a>02144 
<a name="l02152"></a>02152   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02153"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3a5cde7a78f4fea30cf13090eba0892f">02153</a>   <span class="keyword">inline</span> <span class="keyword">const</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l02154"></a>02154   ::value_type&amp;
<a name="l02155"></a>02155   <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a928e2aa74c9ad336e77ab76593b3c8da" title="Access method.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::ValReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l02156"></a>02156 <span class="keyword">  </span>{
<a name="l02157"></a>02157 
<a name="l02158"></a>02158 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02159"></a>02159 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02160"></a>02160       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::ValReal(int, int)&quot;</span>,
<a name="l02161"></a>02161                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l02162"></a>02162                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02163"></a>02163     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02164"></a>02164       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::ValReal(int, int)&quot;</span>,
<a name="l02165"></a>02165                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l02166"></a>02166                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02167"></a>02167 <span class="preprocessor">#endif</span>
<a name="l02168"></a>02168 <span class="preprocessor"></span>
<a name="l02169"></a>02169     <span class="keywordtype">int</span> k, l;
<a name="l02170"></a>02170     <span class="keywordtype">int</span> a, b;
<a name="l02171"></a>02171 
<a name="l02172"></a>02172     a = real_ptr_[Storage::GetFirst(i, j)];
<a name="l02173"></a>02173     b = real_ptr_[Storage::GetFirst(i, j) + 1];
<a name="l02174"></a>02174 
<a name="l02175"></a>02175     <span class="keywordflow">if</span> (a == b)
<a name="l02176"></a>02176       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::ValReal(int, int)&quot;</span>,
<a name="l02177"></a>02177                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l02178"></a>02178                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l02179"></a>02179                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l02180"></a>02180 
<a name="l02181"></a>02181     l = Storage::GetSecond(i, j);
<a name="l02182"></a>02182 
<a name="l02183"></a>02183     <span class="keywordflow">for</span> (k = a; (k &lt; b-1) &amp;&amp; (real_ind_[k] &lt; l); k++);
<a name="l02184"></a>02184 
<a name="l02185"></a>02185     <span class="keywordflow">if</span> (real_ind_[k] == l)
<a name="l02186"></a>02186       <span class="keywordflow">return</span> this-&gt;real_data_[k];
<a name="l02187"></a>02187     <span class="keywordflow">else</span>
<a name="l02188"></a>02188       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::ValReal(int, int)&quot;</span>,
<a name="l02189"></a>02189                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l02190"></a>02190                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l02191"></a>02191                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l02192"></a>02192   }
<a name="l02193"></a>02193 
<a name="l02194"></a>02194 
<a name="l02196"></a>02196 
<a name="l02204"></a>02204   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02205"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a9d49fb0a1544b3eda52ee5e002d0822d">02205</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l02206"></a>02206   ::value_type&amp;
<a name="l02207"></a>02207   <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a9d49fb0a1544b3eda52ee5e002d0822d" title="Access method.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::ValImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l02208"></a>02208   {
<a name="l02209"></a>02209 
<a name="l02210"></a>02210 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02211"></a>02211 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02212"></a>02212       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::ValImag(int, int)&quot;</span>,
<a name="l02213"></a>02213                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l02214"></a>02214                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02215"></a>02215     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02216"></a>02216       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::ValImag(int, int)&quot;</span>,
<a name="l02217"></a>02217                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l02218"></a>02218                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02219"></a>02219 <span class="preprocessor">#endif</span>
<a name="l02220"></a>02220 <span class="preprocessor"></span>
<a name="l02221"></a>02221     <span class="keywordtype">int</span> k, l;
<a name="l02222"></a>02222     <span class="keywordtype">int</span> a, b;
<a name="l02223"></a>02223 
<a name="l02224"></a>02224     a = imag_ptr_[Storage::GetFirst(i, j)];
<a name="l02225"></a>02225     b = imag_ptr_[Storage::GetFirst(i, j) + 1];
<a name="l02226"></a>02226 
<a name="l02227"></a>02227     <span class="keywordflow">if</span> (a == b)
<a name="l02228"></a>02228       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::ValImag(int, int)&quot;</span>,
<a name="l02229"></a>02229                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l02230"></a>02230                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l02231"></a>02231                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l02232"></a>02232 
<a name="l02233"></a>02233     l = Storage::GetSecond(i, j);
<a name="l02234"></a>02234 
<a name="l02235"></a>02235     <span class="keywordflow">for</span> (k = a; (k &lt; b-1) &amp;&amp; (imag_ind_[k] &lt; l); k++);
<a name="l02236"></a>02236 
<a name="l02237"></a>02237     <span class="keywordflow">if</span> (imag_ind_[k] == l)
<a name="l02238"></a>02238       <span class="keywordflow">return</span> this-&gt;imag_data_[k];
<a name="l02239"></a>02239     <span class="keywordflow">else</span>
<a name="l02240"></a>02240       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::ValImag(int, int)&quot;</span>,
<a name="l02241"></a>02241                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l02242"></a>02242                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l02243"></a>02243                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l02244"></a>02244   }
<a name="l02245"></a>02245 
<a name="l02246"></a>02246 
<a name="l02248"></a>02248 
<a name="l02256"></a>02256   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02257"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a7b8f83d193758434bcb1cfe959518f91">02257</a>   <span class="keyword">inline</span> <span class="keyword">const</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l02258"></a>02258   ::value_type&amp;
<a name="l02259"></a>02259   <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a9d49fb0a1544b3eda52ee5e002d0822d" title="Access method.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::ValImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l02260"></a>02260 <span class="keyword">  </span>{
<a name="l02261"></a>02261 
<a name="l02262"></a>02262 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02263"></a>02263 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02264"></a>02264       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::ValImag(int, int)&quot;</span>,
<a name="l02265"></a>02265                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l02266"></a>02266                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02267"></a>02267     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02268"></a>02268       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::ValImag(int, int)&quot;</span>,
<a name="l02269"></a>02269                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l02270"></a>02270                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02271"></a>02271 <span class="preprocessor">#endif</span>
<a name="l02272"></a>02272 <span class="preprocessor"></span>
<a name="l02273"></a>02273     <span class="keywordtype">int</span> k, l;
<a name="l02274"></a>02274     <span class="keywordtype">int</span> a, b;
<a name="l02275"></a>02275 
<a name="l02276"></a>02276     a = imag_ptr_[Storage::GetFirst(i, j)];
<a name="l02277"></a>02277     b = imag_ptr_[Storage::GetFirst(i, j) + 1];
<a name="l02278"></a>02278 
<a name="l02279"></a>02279     <span class="keywordflow">if</span> (a == b)
<a name="l02280"></a>02280       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::ValImag(int, int)&quot;</span>,
<a name="l02281"></a>02281                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l02282"></a>02282                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l02283"></a>02283                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l02284"></a>02284 
<a name="l02285"></a>02285     l = Storage::GetSecond(i, j);
<a name="l02286"></a>02286 
<a name="l02287"></a>02287     <span class="keywordflow">for</span> (k = a; (k &lt; b-1) &amp;&amp; (imag_ind_[k] &lt; l); k++);
<a name="l02288"></a>02288 
<a name="l02289"></a>02289     <span class="keywordflow">if</span> (imag_ind_[k] == l)
<a name="l02290"></a>02290       <span class="keywordflow">return</span> this-&gt;imag_data_[k];
<a name="l02291"></a>02291     <span class="keywordflow">else</span>
<a name="l02292"></a>02292       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::ValImag(int, int)&quot;</span>,
<a name="l02293"></a>02293                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l02294"></a>02294                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l02295"></a>02295                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l02296"></a>02296   }
<a name="l02297"></a>02297 
<a name="l02298"></a>02298 
<a name="l02300"></a>02300 
<a name="l02307"></a>02307   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02308"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a703b6043d3ad7c59cc36e9c5fd0a7d77">02308</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l02309"></a>02309   ::value_type&amp;
<a name="l02310"></a>02310   <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a703b6043d3ad7c59cc36e9c5fd0a7d77" title="Access method.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l02311"></a>02311   {
<a name="l02312"></a>02312 
<a name="l02313"></a>02313 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02314"></a>02314 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02315"></a>02315       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::GetReal(int, int)&quot;</span>,
<a name="l02316"></a>02316                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l02317"></a>02317                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02318"></a>02318     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02319"></a>02319       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::GetReal(int, int)&quot;</span>,
<a name="l02320"></a>02320                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l02321"></a>02321                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02322"></a>02322 <span class="preprocessor">#endif</span>
<a name="l02323"></a>02323 <span class="preprocessor"></span>
<a name="l02324"></a>02324     <span class="keywordtype">int</span> k, l;
<a name="l02325"></a>02325     <span class="keywordtype">int</span> a, b;
<a name="l02326"></a>02326 
<a name="l02327"></a>02327     a = real_ptr_[Storage::GetFirst(i, j)];
<a name="l02328"></a>02328     b = real_ptr_[Storage::GetFirst(i, j) + 1];
<a name="l02329"></a>02329 
<a name="l02330"></a>02330     <span class="keywordflow">if</span> (a &lt; b)
<a name="l02331"></a>02331       {
<a name="l02332"></a>02332         l = Storage::GetSecond(i, j);
<a name="l02333"></a>02333 
<a name="l02334"></a>02334         <span class="keywordflow">for</span> (k = a; (k &lt; b) &amp;&amp; (real_ind_[k] &lt; l); k++);
<a name="l02335"></a>02335 
<a name="l02336"></a>02336         <span class="keywordflow">if</span> ( (k &lt; b) &amp;&amp; (real_ind_[k] == l))
<a name="l02337"></a>02337           <span class="keywordflow">return</span> this-&gt;real_data_[k];
<a name="l02338"></a>02338       }
<a name="l02339"></a>02339     <span class="keywordflow">else</span>
<a name="l02340"></a>02340       k = a;
<a name="l02341"></a>02341 
<a name="l02342"></a>02342     <span class="comment">// adding a non-zero entry</span>
<a name="l02343"></a>02343     <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#aa53b17b254435486a52c058490abb0fb" title="Changing the number of rows and columns.">Resize</a>(this-&gt;m_, this-&gt;n_, real_nz_+1, imag_nz_);
<a name="l02344"></a>02344 
<a name="l02345"></a>02345     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> m = Storage::GetFirst(i, j)+1;
<a name="l02346"></a>02346          m &lt;= Storage::GetFirst(this-&gt;m_, this-&gt;n_); m++)
<a name="l02347"></a>02347       real_ptr_[m]++;
<a name="l02348"></a>02348 
<a name="l02349"></a>02349     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> m = real_nz_-1; m &gt;= k+1; m--)
<a name="l02350"></a>02350       {
<a name="l02351"></a>02351         real_ind_[m] = real_ind_[m-1];
<a name="l02352"></a>02352         this-&gt;real_data_[m] = this-&gt;real_data_[m-1];
<a name="l02353"></a>02353       }
<a name="l02354"></a>02354 
<a name="l02355"></a>02355     real_ind_[k] = Storage::GetSecond(i, j);
<a name="l02356"></a>02356 
<a name="l02357"></a>02357     <span class="comment">// value of new non-zero entry is set to 0</span>
<a name="l02358"></a>02358     SetComplexZero(this-&gt;real_data_[k]);
<a name="l02359"></a>02359 
<a name="l02360"></a>02360     <span class="keywordflow">return</span> this-&gt;real_data_[k];
<a name="l02361"></a>02361   }
<a name="l02362"></a>02362 
<a name="l02363"></a>02363 
<a name="l02365"></a>02365 
<a name="l02372"></a>02372   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02373"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae8796f2d633623f1179a19a38d7f68e1">02373</a>   <span class="keyword">inline</span> <span class="keyword">const</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l02374"></a>02374   ::value_type&amp;
<a name="l02375"></a>02375   <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a703b6043d3ad7c59cc36e9c5fd0a7d77" title="Access method.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l02376"></a>02376 <span class="keyword">  </span>{
<a name="l02377"></a>02377     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a928e2aa74c9ad336e77ab76593b3c8da" title="Access method.">ValReal</a>(i, j);
<a name="l02378"></a>02378   }
<a name="l02379"></a>02379 
<a name="l02380"></a>02380 
<a name="l02382"></a>02382 
<a name="l02389"></a>02389   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02390"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a54303525013c8dc63f7064c4720b9348">02390</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l02391"></a>02391   ::value_type&amp;
<a name="l02392"></a>02392   <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a54303525013c8dc63f7064c4720b9348" title="Access method.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l02393"></a>02393   {
<a name="l02394"></a>02394 
<a name="l02395"></a>02395 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l02396"></a>02396 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l02397"></a>02397       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::GetImag(int, int)&quot;</span>,
<a name="l02398"></a>02398                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l02399"></a>02399                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02400"></a>02400     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l02401"></a>02401       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::GetImag(int, int)&quot;</span>,
<a name="l02402"></a>02402                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l02403"></a>02403                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l02404"></a>02404 <span class="preprocessor">#endif</span>
<a name="l02405"></a>02405 <span class="preprocessor"></span>
<a name="l02406"></a>02406     <span class="keywordtype">int</span> k, l;
<a name="l02407"></a>02407     <span class="keywordtype">int</span> a, b;
<a name="l02408"></a>02408 
<a name="l02409"></a>02409     a = imag_ptr_[Storage::GetFirst(i, j)];
<a name="l02410"></a>02410     b = imag_ptr_[Storage::GetFirst(i, j) + 1];
<a name="l02411"></a>02411 
<a name="l02412"></a>02412     <span class="keywordflow">if</span> (a &lt; b)
<a name="l02413"></a>02413       {
<a name="l02414"></a>02414         l = Storage::GetSecond(i, j);
<a name="l02415"></a>02415 
<a name="l02416"></a>02416         <span class="keywordflow">for</span> (k = a; (k &lt; b) &amp;&amp; (imag_ind_[k] &lt; l); k++);
<a name="l02417"></a>02417 
<a name="l02418"></a>02418         <span class="keywordflow">if</span> ( (k &lt; b) &amp;&amp; (imag_ind_[k] == l))
<a name="l02419"></a>02419           <span class="keywordflow">return</span> this-&gt;imag_data_[k];
<a name="l02420"></a>02420       }
<a name="l02421"></a>02421     <span class="keywordflow">else</span>
<a name="l02422"></a>02422       k = a;
<a name="l02423"></a>02423 
<a name="l02424"></a>02424     <span class="comment">// adding a non-zero entry</span>
<a name="l02425"></a>02425     <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#aa53b17b254435486a52c058490abb0fb" title="Changing the number of rows and columns.">Resize</a>(this-&gt;m_, this-&gt;n_, real_nz_, imag_nz_+1);
<a name="l02426"></a>02426 
<a name="l02427"></a>02427     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> m = Storage::GetFirst(i, j)+1;
<a name="l02428"></a>02428          m &lt;= Storage::GetFirst(this-&gt;m_, this-&gt;n_); m++)
<a name="l02429"></a>02429       imag_ptr_[m]++;
<a name="l02430"></a>02430 
<a name="l02431"></a>02431     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> m = imag_nz_-1; m &gt;= k+1; m--)
<a name="l02432"></a>02432       {
<a name="l02433"></a>02433         imag_ind_[m] = imag_ind_[m-1];
<a name="l02434"></a>02434         this-&gt;imag_data_[m] = this-&gt;imag_data_[m-1];
<a name="l02435"></a>02435       }
<a name="l02436"></a>02436 
<a name="l02437"></a>02437     imag_ind_[k] = Storage::GetSecond(i, j);
<a name="l02438"></a>02438 
<a name="l02439"></a>02439     <span class="comment">// value of new non-zero entry is set to 0</span>
<a name="l02440"></a>02440     SetComplexZero(this-&gt;imag_data_[k]);
<a name="l02441"></a>02441 
<a name="l02442"></a>02442     <span class="keywordflow">return</span> this-&gt;imag_data_[k];
<a name="l02443"></a>02443   }
<a name="l02444"></a>02444 
<a name="l02445"></a>02445 
<a name="l02447"></a>02447 
<a name="l02454"></a>02454   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02455"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a9fec8163944b4af720f89473c7675a34">02455</a>   <span class="keyword">inline</span> <span class="keyword">const</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l02456"></a>02456   ::value_type&amp;
<a name="l02457"></a>02457   <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a54303525013c8dc63f7064c4720b9348" title="Access method.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l02458"></a>02458 <span class="keyword">  </span>{
<a name="l02459"></a>02459     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a9d49fb0a1544b3eda52ee5e002d0822d" title="Access method.">ValImag</a>(i, j);
<a name="l02460"></a>02460   }
<a name="l02461"></a>02461 
<a name="l02462"></a>02462 
<a name="l02464"></a>02464 
<a name="l02471"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ade592203fcc968e928f0083cd5e21644">02471</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02472"></a>02472   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ade592203fcc968e928f0083cd5e21644" title="Add a value to a non-zero entry.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l02473"></a>02473 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ade592203fcc968e928f0083cd5e21644" title="Add a value to a non-zero entry.">  ::AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; val)
<a name="l02474"></a>02474   {
<a name="l02475"></a>02475     <span class="keywordflow">if</span> (real(val) != T(0))
<a name="l02476"></a>02476       GetReal(i, j) += real(val);
<a name="l02477"></a>02477 
<a name="l02478"></a>02478     <span class="keywordflow">if</span> (imag(val) != T(0))
<a name="l02479"></a>02479       GetImag(i, j) += imag(val);
<a name="l02480"></a>02480   }
<a name="l02481"></a>02481 
<a name="l02482"></a>02482 
<a name="l02484"></a>02484 
<a name="l02489"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad297118a9c4023d96a871e6b95d78aa6">02489</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02490"></a>02490   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad297118a9c4023d96a871e6b95d78aa6" title="Sets an element (i, j) to a value.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l02491"></a>02491 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad297118a9c4023d96a871e6b95d78aa6" title="Sets an element (i, j) to a value.">  ::Set</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; val)
<a name="l02492"></a>02492   {
<a name="l02493"></a>02493     GetReal(i, j) = real(val);
<a name="l02494"></a>02494     GetImag(i, j) = imag(val);
<a name="l02495"></a>02495   }
<a name="l02496"></a>02496 
<a name="l02497"></a>02497 
<a name="l02499"></a>02499 
<a name="l02504"></a>02504   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02505"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3e54cf340623d3c2c03f4926d9846383">02505</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l02506"></a>02506   <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3e54cf340623d3c2c03f4926d9846383" title="Duplicates a matrix (assignment operator).">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l02507"></a>02507 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3e54cf340623d3c2c03f4926d9846383" title="Duplicates a matrix (assignment operator).">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l02508"></a>02508   {
<a name="l02509"></a>02509     this-&gt;Copy(A);
<a name="l02510"></a>02510 
<a name="l02511"></a>02511     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l02512"></a>02512   }
<a name="l02513"></a>02513 
<a name="l02514"></a>02514 
<a name="l02515"></a>02515   <span class="comment">/************************</span>
<a name="l02516"></a>02516 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l02517"></a>02517 <span class="comment">   ************************/</span>
<a name="l02518"></a>02518 
<a name="l02519"></a>02519 
<a name="l02521"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#acdb7d1d285d2601b1ef970aecc861178">02521</a> 
<a name="l02522"></a>02522   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02523"></a>02523   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#acdb7d1d285d2601b1ef970aecc861178" title="Resets all non-zero entries to 0-value.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Zero</a>()
<a name="l02524"></a>02524   {
<a name="l02525"></a>02525     this-&gt;allocator_.memoryset(this-&gt;real_data_, <span class="keywordtype">char</span>(0),
<a name="l02526"></a>02526                                this-&gt;real_nz_ * <span class="keyword">sizeof</span>(value_type));
<a name="l02527"></a>02527 
<a name="l02528"></a>02528     this-&gt;allocator_.memoryset(this-&gt;imag_data_, <span class="keywordtype">char</span>(0),
<a name="l02529"></a>02529                                this-&gt;imag_nz_ * <span class="keyword">sizeof</span>(value_type));
<a name="l02530"></a>02530   }
<a name="l02531"></a>02531 
<a name="l02532"></a>02532 
<a name="l02534"></a>02534 
<a name="l02537"></a>02537   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02538"></a>02538   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a7ca605152313a38a14014b92fb1947a5" title="Sets the matrix to identity.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::SetIdentity</a>()
<a name="l02539"></a>02539   {
<a name="l02540"></a>02540     <span class="keywordtype">int</span> m = this-&gt;m_;
<a name="l02541"></a>02541     <span class="keywordtype">int</span> n = this-&gt;n_;
<a name="l02542"></a>02542     <span class="keywordtype">int</span> nz = min(m, n);
<a name="l02543"></a>02543 
<a name="l02544"></a>02544     <span class="keywordflow">if</span> (nz == 0)
<a name="l02545"></a>02545       <span class="keywordflow">return</span>;
<a name="l02546"></a>02546 
<a name="l02547"></a>02547     <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a8940cdf1faedc44051919b9774132fa9" title="Clears the matrix.">Clear</a>();
<a name="l02548"></a>02548 
<a name="l02549"></a>02549     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> real_values(nz), imag_values;
<a name="l02550"></a>02550     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt;
<a name="l02551"></a>02551       real_ptr(Storage::GetFirst(m, n) + 1);
<a name="l02552"></a>02552     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; real_ind(nz);
<a name="l02553"></a>02553     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; imag_ptr(real_ptr);
<a name="l02554"></a>02554     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; imag_ind;
<a name="l02555"></a>02555 
<a name="l02556"></a>02556     real_values.Fill(T(1));
<a name="l02557"></a>02557     real_ind.Fill();
<a name="l02558"></a>02558     imag_ind.Zero();
<a name="l02559"></a>02559     <span class="keywordtype">int</span> i;
<a name="l02560"></a>02560     <span class="keywordflow">for</span> (i = 0; i &lt; nz + 1; i++)
<a name="l02561"></a>02561       real_ptr(i) = i;
<a name="l02562"></a>02562     <span class="keywordflow">for</span> (i = nz + 1; i &lt; real_ptr.GetLength(); i++)
<a name="l02563"></a>02563       real_ptr(i) = nz;
<a name="l02564"></a>02564 
<a name="l02565"></a>02565     <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3729058de11ead808a09e98a10702d85" title="Redefines the matrix.">SetData</a>(m, n, real_values, real_ptr, real_ind,
<a name="l02566"></a>02566             imag_values, imag_ptr, imag_ind);
<a name="l02567"></a>02567   }
<a name="l02568"></a>02568 
<a name="l02569"></a>02569 
<a name="l02571"></a>02571 
<a name="l02574"></a>02574   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02575"></a>02575   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a4f936ebae0d6d37d4ef7e71022ad0490" title="Fills the non-zero entries with 0, 1, 2, ...">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Fill</a>()
<a name="l02576"></a>02576   {
<a name="l02577"></a>02577     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;real_nz_; i++)
<a name="l02578"></a>02578       this-&gt;real_data_[i] = i;
<a name="l02579"></a>02579 
<a name="l02580"></a>02580     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;imag_nz_; i++)
<a name="l02581"></a>02581       this-&gt;imag_data_[i] = T(0);
<a name="l02582"></a>02582   }
<a name="l02583"></a>02583 
<a name="l02584"></a>02584 
<a name="l02586"></a>02586 
<a name="l02589"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a1b17bf0f1b4be60ae64a51564737f834">02589</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02590"></a>02590   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a4f936ebae0d6d37d4ef7e71022ad0490" title="Fills the non-zero entries with 0, 1, 2, ...">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l02591"></a>02591 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a4f936ebae0d6d37d4ef7e71022ad0490" title="Fills the non-zero entries with 0, 1, 2, ...">  ::Fill</a>(<span class="keyword">const</span> complex&lt;T&gt;&amp; x)
<a name="l02592"></a>02592   {
<a name="l02593"></a>02593     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;real_nz_; i++)
<a name="l02594"></a>02594       this-&gt;real_data_[i] = real(x);
<a name="l02595"></a>02595 
<a name="l02596"></a>02596     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;imag_nz_; i++)
<a name="l02597"></a>02597       this-&gt;imag_data_[i] = imag(x);
<a name="l02598"></a>02598   }
<a name="l02599"></a>02599 
<a name="l02600"></a>02600 
<a name="l02602"></a>02602 
<a name="l02605"></a>02605   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02606"></a>02606   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a99688ae138cf3093f657a1673869d2ba" title="Fills the non-zero entries randomly.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::FillRand</a>()
<a name="l02607"></a>02607   {
<a name="l02608"></a>02608     srand(time(NULL));
<a name="l02609"></a>02609     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;real_nz_; i++)
<a name="l02610"></a>02610       this-&gt;real_data_[i] = rand();
<a name="l02611"></a>02611 
<a name="l02612"></a>02612     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;imag_nz_; i++)
<a name="l02613"></a>02613       this-&gt;imag_data_[i] = rand();
<a name="l02614"></a>02614   }
<a name="l02615"></a>02615 
<a name="l02616"></a>02616 
<a name="l02618"></a>02618 
<a name="l02623"></a>02623   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02624"></a>02624   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3bf9cba0b13332169dd7bf67810387dd" title="Displays the matrix on the standard output.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l02625"></a>02625 <span class="keyword">  </span>{
<a name="l02626"></a>02626     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l02627"></a>02627       {
<a name="l02628"></a>02628         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;n_; j++)
<a name="l02629"></a>02629           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l02630"></a>02630         cout &lt;&lt; endl;
<a name="l02631"></a>02631       }
<a name="l02632"></a>02632   }
<a name="l02633"></a>02633 
<a name="l02634"></a>02634 
<a name="l02636"></a>02636 
<a name="l02640"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#afb6419e497671ea7dd09027df0d43bc7">02640</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02641"></a>02641   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#afb6419e497671ea7dd09027df0d43bc7" title="Writes the matrix in a file.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l02642"></a>02642 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#afb6419e497671ea7dd09027df0d43bc7" title="Writes the matrix in a file.">  ::Write</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l02643"></a>02643 <span class="keyword">  </span>{
<a name="l02644"></a>02644     ofstream FileStream;
<a name="l02645"></a>02645     FileStream.open(FileName.c_str());
<a name="l02646"></a>02646 
<a name="l02647"></a>02647 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l02648"></a>02648 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l02649"></a>02649     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l02650"></a>02650       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::Write(string FileName)&quot;</span>,
<a name="l02651"></a>02651                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l02652"></a>02652 <span class="preprocessor">#endif</span>
<a name="l02653"></a>02653 <span class="preprocessor"></span>
<a name="l02654"></a>02654     this-&gt;Write(FileStream);
<a name="l02655"></a>02655 
<a name="l02656"></a>02656     FileStream.close();
<a name="l02657"></a>02657   }
<a name="l02658"></a>02658 
<a name="l02659"></a>02659 
<a name="l02661"></a>02661 
<a name="l02665"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a6bda3b0c8892f7cb59b59ab9ec043980">02665</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02666"></a>02666   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#afb6419e497671ea7dd09027df0d43bc7" title="Writes the matrix in a file.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l02667"></a>02667 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#afb6419e497671ea7dd09027df0d43bc7" title="Writes the matrix in a file.">  ::Write</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l02668"></a>02668 <span class="keyword">  </span>{
<a name="l02669"></a>02669 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l02670"></a>02670 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l02671"></a>02671     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l02672"></a>02672       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l02673"></a>02673                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l02674"></a>02674 <span class="preprocessor">#endif</span>
<a name="l02675"></a>02675 <span class="preprocessor"></span>
<a name="l02676"></a>02676     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l02677"></a>02677                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l02678"></a>02678     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;n_)),
<a name="l02679"></a>02679                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l02680"></a>02680     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;real_nz_)),
<a name="l02681"></a>02681                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l02682"></a>02682     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;imag_nz_)),
<a name="l02683"></a>02683                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l02684"></a>02684 
<a name="l02685"></a>02685     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;real_ptr_),
<a name="l02686"></a>02686                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)*(Storage::GetFirst(this-&gt;m_, this-&gt;n_)+1));
<a name="l02687"></a>02687     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;real_ind_),
<a name="l02688"></a>02688                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)*this-&gt;real_nz_);
<a name="l02689"></a>02689     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;real_data_),
<a name="l02690"></a>02690                      <span class="keyword">sizeof</span>(T)*this-&gt;real_nz_);
<a name="l02691"></a>02691 
<a name="l02692"></a>02692     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;imag_ptr_),
<a name="l02693"></a>02693                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)*(Storage::GetFirst(this-&gt;m_, this-&gt;n_)+1));
<a name="l02694"></a>02694     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;imag_ind_),
<a name="l02695"></a>02695                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)*this-&gt;imag_nz_);
<a name="l02696"></a>02696     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;imag_data_),
<a name="l02697"></a>02697                      <span class="keyword">sizeof</span>(T)*this-&gt;imag_nz_);
<a name="l02698"></a>02698   }
<a name="l02699"></a>02699 
<a name="l02700"></a>02700 
<a name="l02702"></a>02702 
<a name="l02708"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ab2a9ab616ba78d7a0c63f23f58a3e70f">02708</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02709"></a>02709   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ab2a9ab616ba78d7a0c63f23f58a3e70f" title="Writes the matrix in a file.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l02710"></a>02710 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ab2a9ab616ba78d7a0c63f23f58a3e70f" title="Writes the matrix in a file.">  WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l02711"></a>02711 <span class="keyword">  </span>{
<a name="l02712"></a>02712     ofstream FileStream; FileStream.precision(14);
<a name="l02713"></a>02713     FileStream.open(FileName.c_str());
<a name="l02714"></a>02714 
<a name="l02715"></a>02715 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l02716"></a>02716 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l02717"></a>02717     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l02718"></a>02718       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::Write(string FileName)&quot;</span>,
<a name="l02719"></a>02719                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l02720"></a>02720 <span class="preprocessor">#endif</span>
<a name="l02721"></a>02721 <span class="preprocessor"></span>
<a name="l02722"></a>02722     this-&gt;<a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ab2a9ab616ba78d7a0c63f23f58a3e70f" title="Writes the matrix in a file.">WriteText</a>(FileStream);
<a name="l02723"></a>02723 
<a name="l02724"></a>02724     FileStream.close();
<a name="l02725"></a>02725   }
<a name="l02726"></a>02726 
<a name="l02727"></a>02727 
<a name="l02729"></a>02729 
<a name="l02735"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ac2f20995f37def4157cf2b363b908f86">02735</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02736"></a>02736   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ab2a9ab616ba78d7a0c63f23f58a3e70f" title="Writes the matrix in a file.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l02737"></a>02737 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ab2a9ab616ba78d7a0c63f23f58a3e70f" title="Writes the matrix in a file.">  WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l02738"></a>02738 <span class="keyword">  </span>{
<a name="l02739"></a>02739 
<a name="l02740"></a>02740 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l02741"></a>02741 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l02742"></a>02742     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l02743"></a>02743       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l02744"></a>02744                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l02745"></a>02745 <span class="preprocessor">#endif</span>
<a name="l02746"></a>02746 <span class="preprocessor"></span>
<a name="l02747"></a>02747     <span class="comment">// conversion in coordinate format (1-index convention)</span>
<a name="l02748"></a>02748     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> IndRow, IndCol; <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;complex&lt;T&gt;</a> &gt; Value;
<a name="l02749"></a>02749     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; leaf_class =
<a name="l02750"></a>02750       <span class="keyword">static_cast&lt;</span><span class="keyword">const </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; <span class="keyword">&gt;</span>(*this);
<a name="l02751"></a>02751 
<a name="l02752"></a>02752     <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(leaf_class, IndRow, IndCol,
<a name="l02753"></a>02753                                  Value, 1);
<a name="l02754"></a>02754 
<a name="l02755"></a>02755     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; IndRow.GetM(); i++)
<a name="l02756"></a>02756       FileStream &lt;&lt; IndRow(i) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; IndCol(i) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; Value(i) &lt;&lt; <span class="charliteral">&#39;\n&#39;</span>;
<a name="l02757"></a>02757 
<a name="l02758"></a>02758   }
<a name="l02759"></a>02759 
<a name="l02760"></a>02760 
<a name="l02762"></a>02762 
<a name="l02766"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a0ba7bb9f087c978b3f528803136ffad7">02766</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02767"></a>02767   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a0ba7bb9f087c978b3f528803136ffad7" title="Reads the matrix from a file.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l02768"></a>02768 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a0ba7bb9f087c978b3f528803136ffad7" title="Reads the matrix from a file.">  Read</a>(<span class="keywordtype">string</span> FileName)
<a name="l02769"></a>02769   {
<a name="l02770"></a>02770     ifstream FileStream;
<a name="l02771"></a>02771     FileStream.open(FileName.c_str());
<a name="l02772"></a>02772 
<a name="l02773"></a>02773 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l02774"></a>02774 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l02775"></a>02775     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l02776"></a>02776       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::Read(string FileName)&quot;</span>,
<a name="l02777"></a>02777                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l02778"></a>02778 <span class="preprocessor">#endif</span>
<a name="l02779"></a>02779 <span class="preprocessor"></span>
<a name="l02780"></a>02780     this-&gt;<a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a0ba7bb9f087c978b3f528803136ffad7" title="Reads the matrix from a file.">Read</a>(FileStream);
<a name="l02781"></a>02781 
<a name="l02782"></a>02782     FileStream.close();
<a name="l02783"></a>02783   }
<a name="l02784"></a>02784 
<a name="l02785"></a>02785 
<a name="l02787"></a>02787 
<a name="l02791"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a1b8c684b65f487412ba7d437adfc2195">02791</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02792"></a>02792   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a0ba7bb9f087c978b3f528803136ffad7" title="Reads the matrix from a file.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l02793"></a>02793 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a0ba7bb9f087c978b3f528803136ffad7" title="Reads the matrix from a file.">  ::Read</a>(istream&amp; FileStream)
<a name="l02794"></a>02794   {
<a name="l02795"></a>02795 
<a name="l02796"></a>02796 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l02797"></a>02797 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l02798"></a>02798     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l02799"></a>02799       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::Read(istream&amp; FileStream)&quot;</span>,
<a name="l02800"></a>02800                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l02801"></a>02801 <span class="preprocessor">#endif</span>
<a name="l02802"></a>02802 <span class="preprocessor"></span>
<a name="l02803"></a>02803     <span class="keywordtype">int</span> m, n, real_nz, imag_nz;
<a name="l02804"></a>02804     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;m), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l02805"></a>02805     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;n), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l02806"></a>02806     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;real_nz), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l02807"></a>02807     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;imag_nz), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l02808"></a>02808 
<a name="l02809"></a>02809     Reallocate(m, n, real_nz, imag_nz);
<a name="l02810"></a>02810 
<a name="l02811"></a>02811     FileStream.read(reinterpret_cast&lt;char*&gt;(real_ptr_),
<a name="l02812"></a>02812                     <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)*(Storage::GetFirst(m, n)+1));
<a name="l02813"></a>02813     FileStream.read(reinterpret_cast&lt;char*&gt;(real_ind_), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)*real_nz);
<a name="l02814"></a>02814     FileStream.read(reinterpret_cast&lt;char*&gt;(this-&gt;real_data_), <span class="keyword">sizeof</span>(T)*real_nz);
<a name="l02815"></a>02815 
<a name="l02816"></a>02816     FileStream.read(reinterpret_cast&lt;char*&gt;(imag_ptr_),
<a name="l02817"></a>02817                     <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)*(Storage::GetFirst(m, n)+1));
<a name="l02818"></a>02818     FileStream.read(reinterpret_cast&lt;char*&gt;(imag_ind_), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)*imag_nz);
<a name="l02819"></a>02819     FileStream.read(reinterpret_cast&lt;char*&gt;(this-&gt;imag_data_), <span class="keyword">sizeof</span>(T)*imag_nz);
<a name="l02820"></a>02820 
<a name="l02821"></a>02821 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l02822"></a>02822 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l02823"></a>02823     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l02824"></a>02824       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::Read(istream&amp; FileStream)&quot;</span>,
<a name="l02825"></a>02825                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Input operation failed.&quot;</span>)
<a name="l02826"></a>02826                     + string(<span class="stringliteral">&quot; The input file may have been removed&quot;</span>)
<a name="l02827"></a>02827                     + <span class="stringliteral">&quot; or may not contain enough data.&quot;</span>);
<a name="l02828"></a>02828 <span class="preprocessor">#endif</span>
<a name="l02829"></a>02829 <span class="preprocessor"></span>
<a name="l02830"></a>02830   }
<a name="l02831"></a>02831 
<a name="l02832"></a>02832 
<a name="l02834"></a>02834 
<a name="l02838"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a2ae7d8f121dd314d8fab599225028432">02838</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02839"></a>02839   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a2ae7d8f121dd314d8fab599225028432" title="Reads the matrix from a file.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l02840"></a>02840 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a2ae7d8f121dd314d8fab599225028432" title="Reads the matrix from a file.">  ::ReadText</a>(<span class="keywordtype">string</span> FileName)
<a name="l02841"></a>02841   {
<a name="l02842"></a>02842     ifstream FileStream;
<a name="l02843"></a>02843     FileStream.open(FileName.c_str());
<a name="l02844"></a>02844 
<a name="l02845"></a>02845 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l02846"></a>02846 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l02847"></a>02847     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l02848"></a>02848       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::ReadText(string FileName)&quot;</span>,
<a name="l02849"></a>02849                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l02850"></a>02850 <span class="preprocessor">#endif</span>
<a name="l02851"></a>02851 <span class="preprocessor"></span>
<a name="l02852"></a>02852     this-&gt;ReadText(FileStream);
<a name="l02853"></a>02853 
<a name="l02854"></a>02854     FileStream.close();
<a name="l02855"></a>02855   }
<a name="l02856"></a>02856 
<a name="l02857"></a>02857 
<a name="l02859"></a>02859 
<a name="l02863"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#aa33b08968aee2a8a9fca8e823db4274c">02863</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02864"></a>02864   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a2ae7d8f121dd314d8fab599225028432" title="Reads the matrix from a file.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l02865"></a>02865 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a2ae7d8f121dd314d8fab599225028432" title="Reads the matrix from a file.">  ::ReadText</a>(istream&amp; FileStream)
<a name="l02866"></a>02866   {
<a name="l02867"></a>02867     <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; leaf_class =
<a name="l02868"></a>02868       <span class="keyword">static_cast&lt;</span><a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; <span class="keyword">&gt;</span>(*this);
<a name="l02869"></a>02869 
<a name="l02870"></a>02870     complex&lt;T&gt; zero; <span class="keywordtype">int</span> index = 1;
<a name="l02871"></a>02871     <a class="code" href="namespace_seldon.php#af8428b13721ad15b95a74ecf07d23668" title="Reading of matrix in coordinate format.">ReadCoordinateMatrix</a>(leaf_class, FileStream, zero, index);
<a name="l02872"></a>02872   }
<a name="l02873"></a>02873 
<a name="l02874"></a>02874 
<a name="l02876"></a>02876   <span class="comment">// MATRIX&lt;COLCOMPLEXSPARSE&gt; //</span>
<a name="l02878"></a>02878 <span class="comment"></span>
<a name="l02879"></a>02879 
<a name="l02880"></a>02880   <span class="comment">/****************</span>
<a name="l02881"></a>02881 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l02882"></a>02882 <span class="comment">   ****************/</span>
<a name="l02883"></a>02883 
<a name="l02885"></a>02885 
<a name="l02888"></a>02888   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02889"></a>02889   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColComplexSparse, Allocator&gt;::Matrix</a>():
<a name="l02890"></a>02890     <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_complex_sparse.php">ColComplexSparse</a>, Allocator&gt;()
<a name="l02891"></a>02891   {
<a name="l02892"></a>02892   }
<a name="l02893"></a>02893 
<a name="l02894"></a>02894 
<a name="l02896"></a>02896 
<a name="l02900"></a>02900   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02901"></a>02901   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_complex_sparse_00_01_allocator_01_4.php#a3480b30876e7433f0ce962194bbf7a93" title="Default constructor.">Matrix&lt;T, Prop, ColComplexSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l02902"></a>02902     Matrix_ComplexSparse&lt;T, Prop, ColComplexSparse, Allocator&gt;(i, j, 0, 0)
<a name="l02903"></a>02903   {
<a name="l02904"></a>02904   }
<a name="l02905"></a>02905 
<a name="l02906"></a>02906 
<a name="l02908"></a>02908 
<a name="l02916"></a>02916   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02917"></a>02917   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_complex_sparse_00_01_allocator_01_4.php#a3480b30876e7433f0ce962194bbf7a93" title="Default constructor.">Matrix&lt;T, Prop, ColComplexSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l02918"></a>02918                                                        <span class="keywordtype">int</span> real_nz,
<a name="l02919"></a>02919                                                        <span class="keywordtype">int</span> imag_nz):
<a name="l02920"></a>02920     Matrix_ComplexSparse&lt;T, Prop, ColComplexSparse, Allocator&gt;(i, j,
<a name="l02921"></a>02921                                                                real_nz,
<a name="l02922"></a>02922                                                                imag_nz)
<a name="l02923"></a>02923   {
<a name="l02924"></a>02924   }
<a name="l02925"></a>02925 
<a name="l02926"></a>02926 
<a name="l02928"></a>02928 
<a name="l02946"></a>02946   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02947"></a>02947   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l02948"></a>02948             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l02949"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_complex_sparse_00_01_allocator_01_4.php#a35df5579e443bcd49dcdc381869ae507">02949</a>             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l02950"></a>02950   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColComplexSparse, Allocator&gt;::</a>
<a name="l02951"></a>02951 <a class="code" href="class_seldon_1_1_matrix.php">  Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l02952"></a>02952          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; real_values,
<a name="l02953"></a>02953          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; real_ptr,
<a name="l02954"></a>02954          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; real_ind,
<a name="l02955"></a>02955          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; imag_values,
<a name="l02956"></a>02956          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; imag_ptr,
<a name="l02957"></a>02957          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; imag_ind):
<a name="l02958"></a>02958     <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_complex_sparse.php">ColComplexSparse</a>, Allocator&gt;(i, j,
<a name="l02959"></a>02959                                                                real_values,
<a name="l02960"></a>02960                                                                real_ptr,
<a name="l02961"></a>02961                                                                real_ind,
<a name="l02962"></a>02962                                                                imag_values,
<a name="l02963"></a>02963                                                                imag_ptr,
<a name="l02964"></a>02964                                                                imag_ind)
<a name="l02965"></a>02965   {
<a name="l02966"></a>02966   }
<a name="l02967"></a>02967 
<a name="l02968"></a>02968 
<a name="l02969"></a>02969 
<a name="l02971"></a>02971   <span class="comment">// MATRIX&lt;ROWCOMPLEXSPARSE&gt; //</span>
<a name="l02973"></a>02973 <span class="comment"></span>
<a name="l02974"></a>02974 
<a name="l02975"></a>02975   <span class="comment">/****************</span>
<a name="l02976"></a>02976 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l02977"></a>02977 <span class="comment">   ****************/</span>
<a name="l02978"></a>02978 
<a name="l02980"></a>02980 
<a name="l02983"></a>02983   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02984"></a>02984   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowComplexSparse, Allocator&gt;::Matrix</a>():
<a name="l02985"></a>02985     <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_complex_sparse.php">RowComplexSparse</a>, Allocator&gt;()
<a name="l02986"></a>02986   {
<a name="l02987"></a>02987   }
<a name="l02988"></a>02988 
<a name="l02989"></a>02989 
<a name="l02991"></a>02991 
<a name="l02995"></a>02995   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02996"></a>02996   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_complex_sparse_00_01_allocator_01_4.php#ac7efc201b54c0a4eeea3753a1d9c5412" title="Default constructor.">Matrix&lt;T, Prop, RowComplexSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l02997"></a>02997     Matrix_ComplexSparse&lt;T, Prop, RowComplexSparse, Allocator&gt;(i, j, 0, 0)
<a name="l02998"></a>02998   {
<a name="l02999"></a>02999   }
<a name="l03000"></a>03000 
<a name="l03001"></a>03001 
<a name="l03010"></a>03010   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l03011"></a>03011   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_complex_sparse_00_01_allocator_01_4.php#ac7efc201b54c0a4eeea3753a1d9c5412" title="Default constructor.">Matrix&lt;T, Prop, RowComplexSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l03012"></a>03012                                                        <span class="keywordtype">int</span> real_nz,
<a name="l03013"></a>03013                                                        <span class="keywordtype">int</span> imag_nz):
<a name="l03014"></a>03014     Matrix_ComplexSparse&lt;T, Prop, RowComplexSparse, Allocator&gt;(i, j,
<a name="l03015"></a>03015                                                                real_nz,
<a name="l03016"></a>03016                                                                imag_nz)
<a name="l03017"></a>03017   {
<a name="l03018"></a>03018   }
<a name="l03019"></a>03019 
<a name="l03020"></a>03020 
<a name="l03022"></a>03022 
<a name="l03040"></a>03040   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l03041"></a>03041   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l03042"></a>03042             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l03043"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_complex_sparse_00_01_allocator_01_4.php#a4b28e5078f55b5d146712d0e046a0124">03043</a>             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l03044"></a>03044   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowComplexSparse, Allocator&gt;::</a>
<a name="l03045"></a>03045 <a class="code" href="class_seldon_1_1_matrix.php">  Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l03046"></a>03046          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; real_values,
<a name="l03047"></a>03047          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; real_ptr,
<a name="l03048"></a>03048          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; real_ind,
<a name="l03049"></a>03049          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; imag_values,
<a name="l03050"></a>03050          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; imag_ptr,
<a name="l03051"></a>03051          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; imag_ind):
<a name="l03052"></a>03052     <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_complex_sparse.php">RowComplexSparse</a>, Allocator&gt;(i, j,
<a name="l03053"></a>03053                                                                real_values,
<a name="l03054"></a>03054                                                                real_ptr,
<a name="l03055"></a>03055                                                                real_ind,
<a name="l03056"></a>03056                                                                imag_values,
<a name="l03057"></a>03057                                                                imag_ptr,
<a name="l03058"></a>03058                                                                imag_ind)
<a name="l03059"></a>03059   {
<a name="l03060"></a>03060   }
<a name="l03061"></a>03061 
<a name="l03062"></a>03062 
<a name="l03063"></a>03063 } <span class="comment">// namespace Seldon.</span>
<a name="l03064"></a>03064 
<a name="l03065"></a>03065 <span class="preprocessor">#define SELDON_FILE_MATRIX_COMPLEXSPARSE_CXX</span>
<a name="l03066"></a>03066 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
