<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_no_memory.php">NoMemory</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::NoMemory Class Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::NoMemory" --><!-- doxytag: inherits="Seldon::Error" --><div class="dynheader">
Inheritance diagram for Seldon::NoMemory:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_no_memory.png" usemap="#Seldon::NoMemory_map" alt=""/>
  <map id="Seldon::NoMemory_map" name="Seldon::NoMemory_map">
<area href="class_seldon_1_1_error.php" alt="Seldon::Error" shape="rect" coords="0,0,119,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_no_memory-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_no_memory.php#a3eef35951fabdffb7bab6ad483c09567">NoMemory</a> (string function=&quot;&quot;, string comment=&quot;&quot;)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Main constructor.  <a href="#a3eef35951fabdffb7bab6ad483c09567"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">virtual string&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_error.php#a6bc7b1e7b21242994e4a7b0cf9f49ade">What</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Delivers information about the error.  <a href="#a6bc7b1e7b21242994e4a7b0cf9f49ade"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_error.php#a19ea259b22ab7970a19beae646267ba3">CoutWhat</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Delivers information about the error.  <a href="#a19ea259b22ab7970a19beae646267ba3"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5442a8a1dffc72f0e04dd9f746e1a3f0"></a><!-- doxytag: member="Seldon::NoMemory::description_" ref="a5442a8a1dffc72f0e04dd9f746e1a3f0" args="" -->
string&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_error.php#a5442a8a1dffc72f0e04dd9f746e1a3f0">description_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Message describing the exception type. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a45bc7e61221de461fb4b92964b7b9058"></a><!-- doxytag: member="Seldon::NoMemory::function_" ref="a45bc7e61221de461fb4b92964b7b9058" args="" -->
string&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_error.php#a45bc7e61221de461fb4b92964b7b9058">function_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Name of the function in which the error occurred. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a00e98cbd016cc12b92ee48e59a492275"></a><!-- doxytag: member="Seldon::NoMemory::comment_" ref="a00e98cbd016cc12b92ee48e59a492275" args="" -->
string&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_error.php#a00e98cbd016cc12b92ee48e59a492275">comment_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">A comment about the error. <br/></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>

<p>Definition at line <a class="el" href="_errors_8hxx_source.php#l00088">88</a> of file <a class="el" href="_errors_8hxx_source.php">Errors.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="a3eef35951fabdffb7bab6ad483c09567"></a><!-- doxytag: member="Seldon::NoMemory::NoMemory" ref="a3eef35951fabdffb7bab6ad483c09567" args="(string function=&quot;&quot;, string comment=&quot;&quot;)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">Seldon::NoMemory::NoMemory </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>function</em> = <code>&quot;&quot;</code>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>comment</em> = <code>&quot;&quot;</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Main constructor. </p>
<p><a class="el" href="class_seldon_1_1_error.php">Error</a> associated with both a function and a comment. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>function</em>&nbsp;</td><td>function with which the error is associated. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>comment</em>&nbsp;</td><td>comment associated with the error. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_errors_8cxx_source.php#l00199">199</a> of file <a class="el" href="_errors_8cxx_source.php">Errors.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="a19ea259b22ab7970a19beae646267ba3"></a><!-- doxytag: member="Seldon::NoMemory::CoutWhat" ref="a19ea259b22ab7970a19beae646267ba3" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void Seldon::Error::CoutWhat </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Delivers information about the error. </p>
<p>Displays available information, i.e. the error description, the function and/or a comment. </p>

<p>Definition at line <a class="el" href="_errors_8cxx_source.php#l00106">106</a> of file <a class="el" href="_errors_8cxx_source.php">Errors.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6bc7b1e7b21242994e4a7b0cf9f49ade"></a><!-- doxytag: member="Seldon::NoMemory::What" ref="a6bc7b1e7b21242994e4a7b0cf9f49ade" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">string Seldon::Error::What </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [virtual, inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Delivers information about the error. </p>
<p>Displays available information, i.e. the error description, the function and/or a comment. </p>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_undefined.php#af999e2526746396d8336d2a89e3668cf">Seldon::Undefined</a>, <a class="el" href="class_seldon_1_1_wrong_argument.php#a93330801ab53bb5cf0a30373351e2c1d">Seldon::WrongArgument</a>, and <a class="el" href="class_seldon_1_1_lapack_error.php#a1b20521655f5ca3c44e86f4c7776505c">Seldon::LapackError</a>.</p>

<p>Definition at line <a class="el" href="_errors_8cxx_source.php#l00090">90</a> of file <a class="el" href="_errors_8cxx_source.php">Errors.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>share/<a class="el" href="_errors_8hxx_source.php">Errors.hxx</a></li>
<li>share/<a class="el" href="_errors_8cxx_source.php">Errors.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
