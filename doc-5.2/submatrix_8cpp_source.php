<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>doc/example/submatrix.cpp</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="preprocessor">#define SELDON_DEBUG_LEVEL_4</span>
<a name="l00002"></a>00002 <span class="preprocessor"></span><span class="preprocessor">#include &quot;Seldon.hxx&quot;</span>
<a name="l00003"></a>00003 <span class="keyword">using namespace </span>Seldon;
<a name="l00004"></a>00004 
<a name="l00005"></a>00005 <span class="keywordtype">int</span> main()
<a name="l00006"></a>00006 {
<a name="l00007"></a>00007 
<a name="l00008"></a>00008   TRY;
<a name="l00009"></a>00009 
<a name="l00010"></a>00010   <span class="comment">/*** Full matrix ***/</span>
<a name="l00011"></a>00011 
<a name="l00012"></a>00012   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double&gt;</a> A(4, 6);
<a name="l00013"></a>00013   A.Fill();
<a name="l00014"></a>00014   cout &lt;&lt; <span class="stringliteral">&quot;Complete matrix:&quot;</span> &lt;&lt; endl;
<a name="l00015"></a>00015   A.Print();
<a name="l00016"></a>00016 
<a name="l00017"></a>00017   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> row_list(2);
<a name="l00018"></a>00018   row_list(0) = 1;
<a name="l00019"></a>00019   row_list(1) = 2;
<a name="l00020"></a>00020   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> column_list(3);
<a name="l00021"></a>00021   column_list(0) = 0;
<a name="l00022"></a>00022   column_list(1) = 1;
<a name="l00023"></a>00023   column_list(2) = 5;
<a name="l00024"></a>00024   <a class="code" href="class_seldon_1_1_sub_matrix.php" title="Sub-matrix class.">SubMatrix&lt;Matrix&lt;double&gt;</a> &gt; SubA(A, row_list, column_list);
<a name="l00025"></a>00025 
<a name="l00026"></a>00026   cout &lt;&lt; <span class="stringliteral">&quot;Sub-matrix:&quot;</span> &lt;&lt; endl;
<a name="l00027"></a>00027   SubA.Print();
<a name="l00028"></a>00028 
<a name="l00029"></a>00029   <span class="comment">// Basic operations are supported, but they are slow (Blas/Lapack will not</span>
<a name="l00030"></a>00030   <span class="comment">// be called).</span>
<a name="l00031"></a>00031   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double&gt;</a> X(3), Y(2);
<a name="l00032"></a>00032   X.Fill();
<a name="l00033"></a>00033   cout &lt;&lt; <span class="stringliteral">&quot;Multiplied by X = [&quot;</span> &lt;&lt; X &lt;&lt; <span class="stringliteral">&quot;]:&quot;</span> &lt;&lt; endl;
<a name="l00034"></a>00034   <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(SubA, X, Y);
<a name="l00035"></a>00035   Y.Print();
<a name="l00036"></a>00036 
<a name="l00037"></a>00037   <span class="comment">/*** Symmetric matrix ***/</span>
<a name="l00038"></a>00038 
<a name="l00039"></a>00039   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, General, ColSymPacked&gt;</a> B(4);
<a name="l00040"></a>00040   B.Fill();
<a name="l00041"></a>00041   cout &lt;&lt; <span class="stringliteral">&quot;\nComplete matrix:&quot;</span> &lt;&lt; endl;
<a name="l00042"></a>00042   B.Print();
<a name="l00043"></a>00043 
<a name="l00044"></a>00044   row_list(0) = 1;
<a name="l00045"></a>00045   row_list(1) = 3;
<a name="l00046"></a>00046   column_list(0) = 0;
<a name="l00047"></a>00047   column_list(1) = 2;
<a name="l00048"></a>00048   column_list(2) = 3;
<a name="l00049"></a>00049   <a class="code" href="class_seldon_1_1_sub_matrix.php" title="Sub-matrix class.">SubMatrix&lt;Matrix&lt;double, General, ColSymPacked&gt;</a> &gt;
<a name="l00050"></a>00050     SubB(B, row_list, column_list);
<a name="l00051"></a>00051 
<a name="l00052"></a>00052   cout &lt;&lt; <span class="stringliteral">&quot;Sub-matrix (no more symmetric):&quot;</span> &lt;&lt; endl;
<a name="l00053"></a>00053   SubB.Print();
<a name="l00054"></a>00054 
<a name="l00055"></a>00055   <span class="comment">// Assignments in the sub-matrix.</span>
<a name="l00056"></a>00056   <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; 2; i++)
<a name="l00057"></a>00057     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; 3; j++)
<a name="l00058"></a>00058       SubB(i, j) = -1.;
<a name="l00059"></a>00059 
<a name="l00060"></a>00060   <span class="comment">// &#39;B&#39; will remain symmetric.</span>
<a name="l00061"></a>00061   cout &lt;&lt; <span class="stringliteral">&quot;Complete matrix after the sub-matrix is filled with -1:&quot;</span> &lt;&lt; endl;
<a name="l00062"></a>00062   B.Print();
<a name="l00063"></a>00063 
<a name="l00064"></a>00064   END;
<a name="l00065"></a>00065 
<a name="l00066"></a>00066   <span class="keywordflow">return</span> 0;
<a name="l00067"></a>00067 
<a name="l00068"></a>00068 }
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
