<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>vector/PetscVector.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2011-2012, INRIA</span>
<a name="l00002"></a>00002 <span class="comment">// Author(s): Marc Fragu</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_VECTOR_PETSCVECTOR_HXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;../share/Common.hxx&quot;</span>
<a name="l00024"></a>00024 <span class="preprocessor">#include &quot;../share/Properties.hxx&quot;</span>
<a name="l00025"></a>00025 <span class="preprocessor">#include &quot;../share/Storage.hxx&quot;</span>
<a name="l00026"></a>00026 <span class="preprocessor">#include &quot;../share/Errors.hxx&quot;</span>
<a name="l00027"></a>00027 <span class="preprocessor">#include &quot;../share/Allocator.hxx&quot;</span>
<a name="l00028"></a>00028 
<a name="l00029"></a>00029 <span class="preprocessor">#include &quot;petscvec.h&quot;</span>
<a name="l00030"></a>00030 
<a name="l00031"></a>00031 
<a name="l00032"></a>00032 <span class="keyword">namespace </span>Seldon
<a name="l00033"></a>00033 {
<a name="l00034"></a>00034 
<a name="l00035"></a>00035 
<a name="l00037"></a>00037 
<a name="l00040"></a>00040   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00041"></a><a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php">00041</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php">PETScVector</a>: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_vector___base.php" title="Base structure for all vectors.">Vector_Base</a>&lt;T, Allocator&gt;
<a name="l00042"></a>00042   {
<a name="l00043"></a>00043   <span class="keyword">public</span>:
<a name="l00044"></a>00044     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00045"></a>00045     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::pointer pointer;
<a name="l00046"></a>00046     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_pointer const_pointer;
<a name="l00047"></a>00047     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::reference reference;
<a name="l00048"></a>00048     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_reference const_reference;
<a name="l00049"></a>00049 
<a name="l00051"></a><a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a323f6bd490f539750d525646b15583f6">00051</a>     Vec <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a323f6bd490f539750d525646b15583f6" title="Encapsulated PETSc vector.">petsc_vector_</a>;
<a name="l00053"></a><a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a70bd7cbade506f2690f742931f6d46c2">00053</a>     MPI_Comm <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a70bd7cbade506f2690f742931f6d46c2" title="The MPI communicator to use.">mpi_communicator_</a>;
<a name="l00055"></a><a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a39bcb5ba22a031f8d6fbebea571a3c03">00055</a>     <span class="keywordtype">bool</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a39bcb5ba22a031f8d6fbebea571a3c03" title="Boolean to indicate if the inner PETSc vector is destroyed or not.">petsc_vector_deallocated_</a>;
<a name="l00056"></a>00056 
<a name="l00057"></a>00057     <span class="comment">// Methods.</span>
<a name="l00058"></a>00058   <span class="keyword">public</span>:
<a name="l00059"></a>00059     <span class="comment">// Constructor.</span>
<a name="l00060"></a>00060     <span class="keyword">explicit</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a6ec09b49f9c964762ab9120380dc54d6" title="Default constructor.">PETScVector</a>();
<a name="l00061"></a>00061     <span class="keyword">explicit</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a6ec09b49f9c964762ab9120380dc54d6" title="Default constructor.">PETScVector</a>(<span class="keywordtype">int</span> i, MPI_Comm mpi_communicator = MPI_COMM_WORLD);
<a name="l00062"></a>00062     <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a6ec09b49f9c964762ab9120380dc54d6" title="Default constructor.">PETScVector</a>(Vec&amp; petsc_vector);
<a name="l00063"></a>00063     <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a6ec09b49f9c964762ab9120380dc54d6" title="Default constructor.">PETScVector</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php">PETScVector&lt;T, Allocator&gt;</a>&amp; A);
<a name="l00064"></a>00064 
<a name="l00065"></a>00065     <span class="comment">// Destructor.</span>
<a name="l00066"></a>00066     <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#ae416e60119dccc2e4f8d2ee949290ccc" title="Destructor.">~PETScVector</a>();
<a name="l00067"></a>00067 
<a name="l00068"></a>00068     Vec&amp; <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#ac288674b5e0cbab4289b783279c20347" title="Returns a reference on the inner petsc vector.">GetPetscVector</a>();
<a name="l00069"></a>00069     <span class="keyword">const</span> Vec&amp; <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#ac288674b5e0cbab4289b783279c20347" title="Returns a reference on the inner petsc vector.">GetPetscVector</a>() <span class="keyword">const</span>;
<a name="l00070"></a>00070     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#ac6b7117d6550912d70c1b929be557f27" title="Sets the MPI communicator.">SetCommunicator</a>(MPI_Comm mpi_communicator);
<a name="l00071"></a>00071 
<a name="l00072"></a>00072     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a87b2f7caa1f91402c0943f4b0fd7a4ac" title="Clears the vector.">Clear</a>();
<a name="l00073"></a>00073 
<a name="l00074"></a>00074     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a713d118c2c4a588ed3d043f45619e27b" title="Changes the length of the vector, and keeps previous values.">Resize</a>(<span class="keywordtype">int</span> i);
<a name="l00075"></a>00075     <span class="keywordtype">void</span> SetData(<span class="keywordtype">int</span> i, pointer data);
<a name="l00076"></a>00076     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a73f72da9e6a83f0b54aa4e6f08339159" title="Clears the vector without releasing memory.">Nullify</a>();
<a name="l00077"></a>00077 
<a name="l00078"></a>00078     <span class="comment">// Element access.</span>
<a name="l00079"></a>00079     value_type <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a415d9ec2b7070127f200a40abfa09df8" title="Access operator.">operator() </a>(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00080"></a>00080     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#aa9c4710028825f7e8d4c4b38e28d77bc" title="Inserts or adds values into certain locations of a vector.">SetBuffer</a>(<span class="keywordtype">int</span> i, T value, InsertMode insert_mode = INSERT_VALUES);
<a name="l00081"></a>00081     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a7ece84387ef984021d4606b093c2497d" title="Assembles the PETSc vector.">Flush</a>();
<a name="l00082"></a>00082     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a0a0a3a91dc5491fd88f6803b208db9ac" title="Returns the range of indices owned by this processor.">GetProcessorRange</a>(<span class="keywordtype">int</span>&amp; i, <span class="keywordtype">int</span>&amp; j) <span class="keyword">const</span>;
<a name="l00083"></a>00083     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a0b19d99fda0bdd859486db905f3e758f" title="Duplicates a vector.">Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php">PETScVector&lt;T, Allocator&gt;</a>&amp; X);
<a name="l00084"></a>00084     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a0b19d99fda0bdd859486db905f3e758f" title="Duplicates a vector.">Copy</a>(<span class="keyword">const</span> Vec&amp; petsc_vector);
<a name="l00085"></a>00085     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a922a0d69853d84d718dc9102fa8a7f4f" title="Appends an element to the vector.">Append</a>(<span class="keyword">const</span> T&amp; x);
<a name="l00086"></a>00086 
<a name="l00087"></a>00087     <span class="comment">// Basic functions.</span>
<a name="l00088"></a>00088     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a4b1dfd7f5db9600d9e65826c4462d37e" title="Returns the number of elements stored.">GetDataSize</a>() <span class="keyword">const</span>;
<a name="l00089"></a>00089     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a0dfb99ee603ed3f578132425f5afed32" title="Returns the number of elements stored.">GetLocalM</a>() <span class="keyword">const</span>;
<a name="l00090"></a>00090 
<a name="l00091"></a>00091     <span class="comment">// Convenient functions.</span>
<a name="l00092"></a>00092     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a0b318ae92a2a224db5f8a80332b8d25f" title="Sets all elements to zero.">Zero</a>();
<a name="l00093"></a>00093     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#ab1fc92c50dd23964a3ce240c1d5c953a" title="Fills the vector with 0, 1, 2, ...">Fill</a>();
<a name="l00094"></a>00094     <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00095"></a>00095     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#ab1fc92c50dd23964a3ce240c1d5c953a" title="Fills the vector with 0, 1, 2, ...">Fill</a>(<span class="keyword">const</span> T0&amp; x);
<a name="l00096"></a>00096     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#aeea94a272f075d781c2a71d21072ccf4" title="Fills the vector randomly.">FillRand</a>();
<a name="l00097"></a>00097 
<a name="l00098"></a>00098     <span class="comment">// Norms.</span>
<a name="l00099"></a>00099     value_type <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a1216ed94d76cd4cc9a57203781deb864" title="Returns the infinite norm.">GetNormInf</a>() <span class="keyword">const</span>;
<a name="l00100"></a>00100     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a7ecdd5c6469cdaa4e263f6ff71fbbf00" title="Returns the index of the highest absolute value.">GetNormInfIndex</a>() <span class="keyword">const</span>;
<a name="l00101"></a>00101   };
<a name="l00102"></a>00102 
<a name="l00103"></a>00103 
<a name="l00104"></a>00104   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00105"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">00105</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;T, <a class="code" href="class_seldon_1_1_p_e_t_sc_seq.php">PETScSeq</a>, Allocator&gt;: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php">PETScVector</a>&lt;T, Allocator&gt;
<a name="l00106"></a>00106   {
<a name="l00107"></a>00107   <span class="keyword">public</span>:
<a name="l00108"></a>00108     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00109"></a>00109     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::pointer pointer;
<a name="l00110"></a>00110     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_pointer const_pointer;
<a name="l00111"></a>00111     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::reference reference;
<a name="l00112"></a>00112     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_reference const_reference;
<a name="l00113"></a>00113 
<a name="l00114"></a>00114     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_seq.php">PETScSeq</a> <a class="code" href="class_seldon_1_1_p_e_t_sc_seq.php">storage</a>;
<a name="l00115"></a>00115 
<a name="l00116"></a>00116     <span class="comment">// Methods.</span>
<a name="l00117"></a>00117   <span class="keyword">public</span>:
<a name="l00118"></a>00118     <span class="comment">// Constructor.</span>
<a name="l00119"></a>00119     <span class="keyword">explicit</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>();
<a name="l00120"></a>00120     <span class="keyword">explicit</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>(<span class="keywordtype">int</span> i, MPI_Comm mpi_communicator = MPI_COMM_WORLD);
<a name="l00121"></a>00121     <a class="code" href="class_seldon_1_1_vector.php">Vector</a>(Vec&amp; petsc_vector);
<a name="l00122"></a>00122     <a class="code" href="class_seldon_1_1_vector.php">Vector</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Vector&lt;T, PETScSeq, Allocator&gt;</a>&amp; A);
<a name="l00123"></a>00123     <span class="comment">// Destructor.</span>
<a name="l00124"></a>00124     ~<a class="code" href="class_seldon_1_1_vector.php">Vector</a>();
<a name="l00125"></a>00125 
<a name="l00126"></a>00126     <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Vector&lt;T, PETScSeq, Allocator&gt;</a>&amp; X);
<a name="l00127"></a>00127     <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Vec&amp; petsc_vector);
<a name="l00128"></a>00128     <span class="comment">// Memory management.</span>
<a name="l00129"></a>00129     <span class="keywordtype">void</span> Reallocate(<span class="keywordtype">int</span> i);
<a name="l00130"></a>00130 
<a name="l00131"></a>00131 <span class="preprocessor">#ifndef SWIG</span>
<a name="l00132"></a>00132 <span class="preprocessor"></span>    <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Vector&lt;T, PETScSeq, Allocator&gt;</a>&amp; operator= (<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;T,
<a name="l00133"></a>00133                                                <a class="code" href="class_seldon_1_1_p_e_t_sc_seq.php">PETScSeq</a>, Allocator&gt;&amp; X);
<a name="l00134"></a>00134     <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00135"></a>00135     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Vector&lt;T, PETScSeq, Allocator&gt;</a>&amp; operator= (<span class="keyword">const</span> T0&amp; X);
<a name="l00136"></a>00136 <span class="preprocessor">#endif</span>
<a name="l00137"></a>00137 <span class="preprocessor"></span>    <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00138"></a>00138     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Vector&lt;T, PETScSeq, Allocator&gt;</a>&amp; operator*= (<span class="keyword">const</span> T0&amp; X);
<a name="l00139"></a>00139 
<a name="l00140"></a>00140     <span class="keywordtype">void</span> Print() <span class="keyword">const</span>;
<a name="l00141"></a>00141 
<a name="l00142"></a>00142     <span class="comment">// Input/output functions.</span>
<a name="l00143"></a>00143     <span class="keywordtype">void</span> Write(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size = <span class="keyword">true</span>) <span class="keyword">const</span>;
<a name="l00144"></a>00144     <span class="keywordtype">void</span> Write(ostream&amp; FileStream, <span class="keywordtype">bool</span> with_size = <span class="keyword">true</span>) <span class="keyword">const</span>;
<a name="l00145"></a>00145     <span class="keywordtype">void</span> WriteText(<span class="keywordtype">string</span> FileName) <span class="keyword">const</span>;
<a name="l00146"></a>00146     <span class="keywordtype">void</span> WriteText(ostream&amp; FileStream) <span class="keyword">const</span>;
<a name="l00147"></a>00147     <span class="keywordtype">void</span> Read(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size = <span class="keyword">true</span>);
<a name="l00148"></a>00148     <span class="keywordtype">void</span> Read(istream&amp; FileStream, <span class="keywordtype">bool</span> with_size = <span class="keyword">true</span>);
<a name="l00149"></a>00149     <span class="keywordtype">void</span> ReadText(<span class="keywordtype">string</span> FileName);
<a name="l00150"></a>00150     <span class="keywordtype">void</span> ReadText(istream&amp; FileStream);
<a name="l00151"></a>00151   };
<a name="l00152"></a>00152 
<a name="l00153"></a>00153 
<a name="l00154"></a>00154 <span class="preprocessor">#ifndef SWIG</span>
<a name="l00155"></a>00155 <span class="preprocessor"></span>  <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00156"></a>00156   ostream&amp; <a class="code" href="namespace_seldon.php#a5bd4a6d6f3c83048663d57d2fc032107" title="operator&amp;lt;&amp;lt; overloaded for a 3D array.">operator &lt;&lt; </a>(ostream&amp; out,
<a name="l00157"></a>00157                         <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Vector&lt;T, PETScSeq, Allocator&gt;</a>&amp; V);
<a name="l00158"></a>00158 <span class="preprocessor">#endif</span>
<a name="l00159"></a>00159 <span class="preprocessor"></span>
<a name="l00160"></a>00160 
<a name="l00161"></a>00161   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00162"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php">00162</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;T, <a class="code" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator&gt;: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php">PETScVector</a>&lt;T, Allocator&gt;
<a name="l00163"></a>00163   {
<a name="l00164"></a>00164   <span class="keyword">public</span>:
<a name="l00165"></a>00165     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00166"></a>00166     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::pointer pointer;
<a name="l00167"></a>00167     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_pointer const_pointer;
<a name="l00168"></a>00168     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::reference reference;
<a name="l00169"></a>00169     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_reference const_reference;
<a name="l00170"></a>00170 
<a name="l00171"></a>00171     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a> <a class="code" href="class_seldon_1_1_p_e_t_sc_par.php">storage</a>;
<a name="l00172"></a>00172 
<a name="l00173"></a>00173     <span class="comment">// Methods.</span>
<a name="l00174"></a>00174   <span class="keyword">public</span>:
<a name="l00175"></a>00175     <span class="comment">// Constructor.</span>
<a name="l00176"></a>00176     <span class="keyword">explicit</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>();
<a name="l00177"></a>00177     <span class="keyword">explicit</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>(<span class="keywordtype">int</span> i, MPI_Comm mpi_communicator = MPI_COMM_WORLD);
<a name="l00178"></a>00178     <span class="keyword">explicit</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> Nlocal, MPI_Comm mpi_communicator);
<a name="l00179"></a>00179     <a class="code" href="class_seldon_1_1_vector.php">Vector</a>(Vec&amp; petsc_vector);
<a name="l00180"></a>00180     <a class="code" href="class_seldon_1_1_vector.php">Vector</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php">Vector&lt;T, PETScPar, Allocator&gt;</a>&amp; A);
<a name="l00181"></a>00181     <span class="comment">// Destructor.</span>
<a name="l00182"></a>00182     ~<a class="code" href="class_seldon_1_1_vector.php">Vector</a>();
<a name="l00183"></a>00183 
<a name="l00184"></a>00184     <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php">Vector&lt;T, PETScPar, Allocator&gt;</a>&amp; X);
<a name="l00185"></a>00185     <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Vec&amp; petsc_vector);
<a name="l00186"></a>00186     <span class="comment">// Memory management.</span>
<a name="l00187"></a>00187     <span class="keywordtype">void</span> Reallocate(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> local_size = PETSC_DECIDE);
<a name="l00188"></a>00188 
<a name="l00189"></a>00189 <span class="preprocessor">#ifndef SWIG</span>
<a name="l00190"></a>00190 <span class="preprocessor"></span>    <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php">Vector&lt;T, PETScPar, Allocator&gt;</a>&amp; operator= (<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;T,
<a name="l00191"></a>00191                                                <a class="code" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator&gt;&amp; X);
<a name="l00192"></a>00192     <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00193"></a>00193     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php">Vector&lt;T, PETScPar, Allocator&gt;</a>&amp; operator= (<span class="keyword">const</span> T0&amp; X);
<a name="l00194"></a>00194 <span class="preprocessor">#endif</span>
<a name="l00195"></a>00195 <span class="preprocessor"></span>    <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00196"></a>00196     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php">Vector&lt;T, PETScPar, Allocator&gt;</a>&amp; operator*= (<span class="keyword">const</span> T0&amp; X);
<a name="l00197"></a>00197 
<a name="l00198"></a>00198     <span class="keywordtype">void</span> Print() <span class="keyword">const</span>;
<a name="l00199"></a>00199 
<a name="l00200"></a>00200     <span class="comment">// Input/output functions.</span>
<a name="l00201"></a>00201     <span class="keywordtype">void</span> Write(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size = <span class="keyword">true</span>) <span class="keyword">const</span>;
<a name="l00202"></a>00202     <span class="keywordtype">void</span> Write(ostream&amp; FileStream, <span class="keywordtype">bool</span> with_size = <span class="keyword">true</span>) <span class="keyword">const</span>;
<a name="l00203"></a>00203     <span class="keywordtype">void</span> WriteText(<span class="keywordtype">string</span> FileName) <span class="keyword">const</span>;
<a name="l00204"></a>00204     <span class="keywordtype">void</span> WriteText(ostream&amp; FileStream) <span class="keyword">const</span>;
<a name="l00205"></a>00205     <span class="keywordtype">void</span> Read(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size = <span class="keyword">true</span>);
<a name="l00206"></a>00206     <span class="keywordtype">void</span> Read(istream&amp; FileStream, <span class="keywordtype">bool</span> with_size = <span class="keyword">true</span>);
<a name="l00207"></a>00207     <span class="keywordtype">void</span> ReadText(<span class="keywordtype">string</span> FileName);
<a name="l00208"></a>00208     <span class="keywordtype">void</span> ReadText(istream&amp; FileStream);
<a name="l00209"></a>00209   };
<a name="l00210"></a>00210 
<a name="l00211"></a>00211 
<a name="l00212"></a>00212 <span class="preprocessor">#ifndef SWIG</span>
<a name="l00213"></a>00213 <span class="preprocessor"></span>  <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00214"></a>00214   ostream&amp; <a class="code" href="namespace_seldon.php#a5bd4a6d6f3c83048663d57d2fc032107" title="operator&amp;lt;&amp;lt; overloaded for a 3D array.">operator &lt;&lt; </a>(ostream&amp; out,
<a name="l00215"></a>00215                         <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php">Vector&lt;T, PETScPar, Allocator&gt;</a>&amp; V);
<a name="l00216"></a>00216 <span class="preprocessor">#endif</span>
<a name="l00217"></a>00217 <span class="preprocessor"></span>
<a name="l00218"></a>00218 
<a name="l00219"></a>00219 
<a name="l00220"></a>00220 } <span class="comment">// namespace Seldon.</span>
<a name="l00221"></a>00221 
<a name="l00222"></a>00222 <span class="preprocessor">#define SELDON_FILE_VECTOR_PETSCVECTOR_HXX</span>
<a name="l00223"></a>00223 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
