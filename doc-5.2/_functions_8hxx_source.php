<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix/Functions.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet, Marc Fragu</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_FUNCTIONS_HXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 <span class="keyword">namespace </span>Seldon
<a name="l00023"></a>00023 {
<a name="l00024"></a>00024 
<a name="l00025"></a>00025   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> T1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00026"></a>00026   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad8df46dfc8619f16f3a44ac8a42a777a" title="Extracts a row from a sparse matrix.">GetRow</a>(<span class="keyword">const</span> Matrix&lt;T0, General, RowSparse, Allocator0&gt;&amp; M,
<a name="l00027"></a>00027               <span class="keywordtype">int</span> i, Vector&lt;T1, Vect_Sparse, Allocator1&gt;&amp; X);
<a name="l00028"></a>00028 
<a name="l00029"></a>00029   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00030"></a>00030             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00031"></a>00031   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad8df46dfc8619f16f3a44ac8a42a777a" title="Extracts a row from a sparse matrix.">GetRow</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l00032"></a>00032               <span class="keywordtype">int</span> i, Vector&lt;T1, Storage1, Allocator1&gt;&amp; X);
<a name="l00033"></a>00033 
<a name="l00034"></a>00034   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> T1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00035"></a>00035   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#acb182413f85715422b2289a98d2e9379" title="Extracts a column from a matrix.">GetCol</a>(<span class="keyword">const</span> Matrix&lt;T0, General, RowSparse, Allocator0&gt;&amp; M,
<a name="l00036"></a>00036               <span class="keywordtype">int</span> j, Vector&lt;T1, Vect_Sparse, Allocator1&gt;&amp; X);
<a name="l00037"></a>00037 
<a name="l00038"></a>00038   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00039"></a>00039   <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00040"></a>00040   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#acb182413f85715422b2289a98d2e9379" title="Extracts a column from a matrix.">GetCol</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, PETScSeqDense, Allocator0&gt;&amp; M,
<a name="l00041"></a>00041               <span class="keywordtype">int</span> j, Vector&lt;T1, PETScSeq, Allocator1&gt;&amp; X);
<a name="l00042"></a>00042 
<a name="l00043"></a>00043   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00044"></a>00044   <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00045"></a>00045   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#acb182413f85715422b2289a98d2e9379" title="Extracts a column from a matrix.">GetCol</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, PETScMPIDense, Allocator0&gt;&amp; M,
<a name="l00046"></a>00046               <span class="keywordtype">int</span> j, Vector&lt;T1, PETScPar, Allocator1&gt;&amp; X);
<a name="l00047"></a>00047 
<a name="l00048"></a>00048   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00049"></a>00049             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00050"></a>00050   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#acb182413f85715422b2289a98d2e9379" title="Extracts a column from a matrix.">GetCol</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l00051"></a>00051               <span class="keywordtype">int</span> j, Vector&lt;T1, Storage1, Allocator1&gt;&amp; X);
<a name="l00052"></a>00052 
<a name="l00053"></a>00053   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00054"></a>00054             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00055"></a>00055   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#acb182413f85715422b2289a98d2e9379" title="Extracts a column from a matrix.">GetCol</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M_in,
<a name="l00056"></a>00056               <span class="keywordtype">int</span> begin, <span class="keywordtype">int</span> end,
<a name="l00057"></a>00057               Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;&amp; M_out);
<a name="l00058"></a>00058 
<a name="l00059"></a>00059 
<a name="l00060"></a>00060   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00061"></a>00061             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00062"></a>00062   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ae7c5e5e111b10a46344f75f1e8429a88" title="Sets a row of a matrix.">SetRow</a>(<span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00063"></a>00063               <span class="keywordtype">int</span> i, Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M);
<a name="l00064"></a>00064 
<a name="l00065"></a>00065   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00066"></a>00066   <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00067"></a>00067   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ae7c5e5e111b10a46344f75f1e8429a88" title="Sets a row of a matrix.">SetRow</a>(<span class="keyword">const</span> Vector&lt;T1, PETScSeq, Allocator1&gt;&amp; X,
<a name="l00068"></a>00068               <span class="keywordtype">int</span> i, Matrix&lt;T0, Prop0, PETScSeqDense, Allocator0&gt;&amp; M);
<a name="l00069"></a>00069 
<a name="l00070"></a>00070   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00071"></a>00071   <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00072"></a>00072   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ae7c5e5e111b10a46344f75f1e8429a88" title="Sets a row of a matrix.">SetRow</a>(<span class="keyword">const</span> Vector&lt;T1, PETScPar, Allocator1&gt;&amp; X,
<a name="l00073"></a>00073               <span class="keywordtype">int</span> i, Matrix&lt;T0, Prop0, PETScMPIDense, Allocator0&gt;&amp; M);
<a name="l00074"></a>00074 
<a name="l00075"></a>00075   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> T1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00076"></a>00076   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ae7c5e5e111b10a46344f75f1e8429a88" title="Sets a row of a matrix.">SetRow</a>(<span class="keyword">const</span> Vector&lt;T1, Vect_Sparse, Allocator1&gt;&amp; X,
<a name="l00077"></a>00077               <span class="keywordtype">int</span> i, Matrix&lt;T0, General, RowSparse, Allocator0&gt;&amp; M);
<a name="l00078"></a>00078 
<a name="l00079"></a>00079   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00080"></a>00080             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00081"></a>00081   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a47489289334cd178af06072aac3e091b" title="Sets a column of a matrix.">SetCol</a>(<span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00082"></a>00082               <span class="keywordtype">int</span> j, Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M);
<a name="l00083"></a>00083 
<a name="l00084"></a>00084   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00085"></a>00085   <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00086"></a>00086   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a47489289334cd178af06072aac3e091b" title="Sets a column of a matrix.">SetCol</a>(<span class="keyword">const</span> Vector&lt;T1, PETScSeq, Allocator1&gt;&amp; X,
<a name="l00087"></a>00087               <span class="keywordtype">int</span> j, Matrix&lt;T0, Prop0, PETScSeqDense, Allocator0&gt;&amp; M);
<a name="l00088"></a>00088 
<a name="l00089"></a>00089   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00090"></a>00090   <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00091"></a>00091   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a47489289334cd178af06072aac3e091b" title="Sets a column of a matrix.">SetCol</a>(<span class="keyword">const</span> Vector&lt;T1, PETScPar, Allocator1&gt;&amp; X,
<a name="l00092"></a>00092               <span class="keywordtype">int</span> j, Matrix&lt;T0, Prop0, PETScMPIDense, Allocator0&gt;&amp; M);
<a name="l00093"></a>00093 
<a name="l00094"></a>00094   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Allocator0,
<a name="l00095"></a>00095             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00096"></a>00096   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a47489289334cd178af06072aac3e091b" title="Sets a column of a matrix.">SetCol</a>(<span class="keyword">const</span> Vector&lt;T1, VectFull, Allocator1&gt;&amp; X,
<a name="l00097"></a>00097               <span class="keywordtype">int</span> j, Matrix&lt;T0, General, RowSparse, Allocator0&gt;&amp; M);
<a name="l00098"></a>00098 
<a name="l00099"></a>00099   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> T1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00100"></a>00100   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a47489289334cd178af06072aac3e091b" title="Sets a column of a matrix.">SetCol</a>(<span class="keyword">const</span> Vector&lt;T1, VectSparse, Allocator1&gt;&amp; X,
<a name="l00101"></a>00101               <span class="keywordtype">int</span> j, Matrix&lt;T0, General, RowSparse, Allocator0&gt;&amp; M);
<a name="l00102"></a>00102 
<a name="l00103"></a>00103   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00104"></a>00104   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a48069e460ec162c2f6fef6b0ee2e13ec" title="Permutation of a general matrix stored by rows.">ApplyPermutation</a>(Matrix&lt;T, Prop, RowMajor, Allocator&gt;&amp; A,
<a name="l00105"></a>00105                         <span class="keyword">const</span> Vector&lt;int&gt;&amp; row_perm,
<a name="l00106"></a>00106                         <span class="keyword">const</span> Vector&lt;int&gt;&amp; col_perm,
<a name="l00107"></a>00107                         <span class="keywordtype">int</span> starting_index = 0);
<a name="l00108"></a>00108 
<a name="l00109"></a>00109   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00110"></a>00110   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a48069e460ec162c2f6fef6b0ee2e13ec" title="Permutation of a general matrix stored by rows.">ApplyPermutation</a>(Matrix&lt;T, Prop, ColMajor, Allocator&gt;&amp; A,
<a name="l00111"></a>00111                         <span class="keyword">const</span> Vector&lt;int&gt;&amp; row_perm,
<a name="l00112"></a>00112                         <span class="keyword">const</span> Vector&lt;int&gt;&amp; col_perm,
<a name="l00113"></a>00113                         <span class="keywordtype">int</span> starting_index = 0);
<a name="l00114"></a>00114 
<a name="l00115"></a>00115   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00116"></a>00116   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a48edc9483ed12de114cfde40b9bed96e" title="Inverse permutation of a general matrix stored by rows.">ApplyInversePermutation</a>(Matrix&lt;T, Prop, RowMajor, Allocator&gt;&amp; A,
<a name="l00117"></a>00117                                <span class="keyword">const</span> Vector&lt;int&gt;&amp; row_perm,
<a name="l00118"></a>00118                                <span class="keyword">const</span> Vector&lt;int&gt;&amp; col_perm,
<a name="l00119"></a>00119                                <span class="keywordtype">int</span> starting_index = 0);
<a name="l00120"></a>00120 
<a name="l00121"></a>00121   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00122"></a>00122   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a48edc9483ed12de114cfde40b9bed96e" title="Inverse permutation of a general matrix stored by rows.">ApplyInversePermutation</a>(Matrix&lt;T, Prop, ColMajor, Allocator&gt;&amp; A,
<a name="l00123"></a>00123                                <span class="keyword">const</span> Vector&lt;int&gt;&amp; row_perm,
<a name="l00124"></a>00124                                <span class="keyword">const</span> Vector&lt;int&gt;&amp; col_perm,
<a name="l00125"></a>00125                                <span class="keywordtype">int</span> starting_index = 0);
<a name="l00126"></a>00126 
<a name="l00127"></a>00127 } <span class="comment">// namespace Seldon.</span>
<a name="l00128"></a>00128 
<a name="l00129"></a>00129 <span class="preprocessor">#define SELDON_FILE_FUNCTIONS_HXX</span>
<a name="l00130"></a>00130 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
