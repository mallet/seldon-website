<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/interfaces/direct/Pastix.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2010 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 <span class="preprocessor">#ifndef SELDON_FILE_PASTIX_HXX</span>
<a name="l00020"></a>00020 <span class="preprocessor"></span>
<a name="l00021"></a>00021 <span class="comment">// including Pastix headers</span>
<a name="l00022"></a>00022 <span class="keyword">extern</span> <span class="stringliteral">&quot;C&quot;</span>
<a name="l00023"></a>00023 {
<a name="l00024"></a>00024 <span class="preprocessor">#define _COMPLEX_H</span>
<a name="l00025"></a>00025 <span class="preprocessor"></span>
<a name="l00026"></a>00026 <span class="preprocessor">#include &quot;pastix.h&quot;</span>
<a name="l00027"></a>00027 }
<a name="l00028"></a>00028 
<a name="l00029"></a>00029 <span class="keyword">namespace </span>Seldon
<a name="l00030"></a>00030 {
<a name="l00031"></a>00031 
<a name="l00032"></a>00032   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00033"></a><a class="code" href="class_seldon_1_1_matrix_pastix.php">00033</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix_pastix.php">MatrixPastix</a>
<a name="l00034"></a>00034   {
<a name="l00035"></a>00035   <span class="keyword">protected</span> :
<a name="l00037"></a><a class="code" href="class_seldon_1_1_matrix_pastix.php#a1d728cf85201e0e4b35fd66c985ca95a">00037</a>     pastix_data_t* <a class="code" href="class_seldon_1_1_matrix_pastix.php#a1d728cf85201e0e4b35fd66c985ca95a" title="pastix structure">pastix_data</a>;
<a name="l00039"></a><a class="code" href="class_seldon_1_1_matrix_pastix.php#a3e0e4d255058e66c82419232b28fe178">00039</a>     pastix_int_t <a class="code" href="class_seldon_1_1_matrix_pastix.php#a3e0e4d255058e66c82419232b28fe178" title="options (integers)">iparm</a>[64];
<a name="l00041"></a><a class="code" href="class_seldon_1_1_matrix_pastix.php#ab0e31b30663fb336f638fc847663c1eb">00041</a>     <span class="keywordtype">double</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#ab0e31b30663fb336f638fc847663c1eb" title="options (floats)">dparm</a>[64];
<a name="l00043"></a><a class="code" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e">00043</a>     pastix_int_t <a class="code" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e" title="number of columns">n</a>;
<a name="l00045"></a><a class="code" href="class_seldon_1_1_matrix_pastix.php#a0e40bdc9ac7c0ae1bd80e05d9942afee">00045</a>     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;pastix_int_t&gt;</a> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a0e40bdc9ac7c0ae1bd80e05d9942afee" title="permutation arrays">perm</a>, invp;
<a name="l00047"></a><a class="code" href="class_seldon_1_1_matrix_pastix.php#aeca666d886a9ab8e8e8e907963a6ed64">00047</a>     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;pastix_int_t&gt;</a> <a class="code" href="class_seldon_1_1_matrix_pastix.php#aeca666d886a9ab8e8e8e907963a6ed64" title="local to global">col_num</a>;
<a name="l00049"></a><a class="code" href="class_seldon_1_1_matrix_pastix.php#ab0637a34811226bdedf63b3e71c7cef6">00049</a>     <span class="keywordtype">bool</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#ab0637a34811226bdedf63b3e71c7cef6" title="if true, resolution on several nodes">distributed</a>;
<a name="l00051"></a><a class="code" href="class_seldon_1_1_matrix_pastix.php#a138482f32cb3046a3faf597aff405fb3">00051</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a138482f32cb3046a3faf597aff405fb3" title="level of display">print_level</a>;
<a name="l00053"></a><a class="code" href="class_seldon_1_1_matrix_pastix.php#a8c22604c5e88bb75086bde3f344cd4d5">00053</a>     <span class="keywordtype">bool</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a8c22604c5e88bb75086bde3f344cd4d5" title="if true, solution is refined">refine_solution</a>;
<a name="l00054"></a>00054 
<a name="l00055"></a>00055   <span class="keyword">public</span> :
<a name="l00056"></a>00056 
<a name="l00057"></a>00057     <a class="code" href="class_seldon_1_1_matrix_pastix.php#a862e60553d3532ab99a3645f219d782d" title="Default constructor.">MatrixPastix</a>();
<a name="l00058"></a>00058     <a class="code" href="class_seldon_1_1_matrix_pastix.php#a06e7866292ac5b30289cf566d56b0caf" title="destructor">~MatrixPastix</a>();
<a name="l00059"></a>00059 
<a name="l00060"></a>00060     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a0840578aaa2639d33d5c47adea893cf8" title="Clearing factorization.">Clear</a>();
<a name="l00061"></a>00061 
<a name="l00062"></a>00062     <span class="keywordtype">void</span> CallPastix(<span class="keyword">const</span> MPI_Comm&amp;, pastix_int_t* colptr, pastix_int_t* row,
<a name="l00063"></a>00063                     T* val, T* b, pastix_int_t nrhs);
<a name="l00064"></a>00064 
<a name="l00065"></a>00065     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a29ac5cb9e5315ae68d6fc5d1e97eef71" title="no message will be displayed">HideMessages</a>();
<a name="l00066"></a>00066     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a0ca315759b1dd24114ff47958c37cb96" title="Low level of display.">ShowMessages</a>();
<a name="l00067"></a>00067     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#adc33ae8c7f93e468abd3ade499bea393" title="Displaying all messages.">ShowFullHistory</a>();
<a name="l00068"></a>00068 
<a name="l00069"></a>00069     <span class="keywordtype">void</span> SelectOrdering(<span class="keywordtype">int</span> type);
<a name="l00070"></a>00070     <span class="keywordtype">void</span> SetPermutation(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; permut);
<a name="l00071"></a>00071 
<a name="l00072"></a>00072     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a6f6a15165b8961d62505dc2ff4f261e0" title="You can require that solution is refined after LU resolution.">RefineSolution</a>();
<a name="l00073"></a>00073     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#af8a85f37469fa702b6ea1ebd2a2b4b0e" title="You can require that solution is not refined (faster).">DoNotRefineSolution</a>();
<a name="l00074"></a>00074 
<a name="l00075"></a>00075     <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator, <span class="keyword">class</span> T<span class="keywordtype">int</span>&gt;
<a name="l00076"></a>00076     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a69e64cad0fe1938748e49c0d0057bd66" title="Returning ordering found by Scotch.">FindOrdering</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop, Storage, Allocator&gt;</a> &amp; mat,
<a name="l00077"></a>00077                       <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Tint&gt;</a>&amp; numbers, <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>);
<a name="l00078"></a>00078 
<a name="l00079"></a>00079     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00080"></a>00080     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a638bb937143f0aacd9f241d4c89febba" title="Factorization of unsymmetric matrix.">FactorizeMatrix</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, General, Storage, Allocator&gt;</a> &amp; mat,
<a name="l00081"></a>00081                          <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>);
<a name="l00082"></a>00082 
<a name="l00083"></a>00083     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00084"></a>00084     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a638bb937143f0aacd9f241d4c89febba" title="Factorization of unsymmetric matrix.">FactorizeMatrix</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Symmetric, Storage, Allocator&gt;</a> &amp; mat,
<a name="l00085"></a>00085                          <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>);
<a name="l00086"></a>00086 
<a name="l00087"></a>00087     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2&gt;
<a name="l00088"></a>00088     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a74f16146dfc8aa871028cfa6086a184a" title="solving A x = b (A is already factorized)">Solve</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator2&gt;</a>&amp; x);
<a name="l00089"></a>00089 
<a name="l00090"></a>00090     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Transpose_status&gt;
<a name="l00091"></a>00091     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a74f16146dfc8aa871028cfa6086a184a" title="solving A x = b (A is already factorized)">Solve</a>(<span class="keyword">const</span> Transpose_status&amp; TransA,
<a name="l00092"></a>00092                <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator2&gt;</a>&amp; x);
<a name="l00093"></a>00093 
<a name="l00094"></a>00094     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#ac3ebb0a32a3aa30164f54dbaa75bff4a" title="Modifies the number of threads per node.">SetNumberThreadPerNode</a>(<span class="keywordtype">int</span> num_thread);
<a name="l00095"></a>00095 
<a name="l00096"></a>00096     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Alloc1, <span class="keyword">class</span> Alloc2, <span class="keyword">class</span> Alloc3, <span class="keyword">class</span> T<span class="keywordtype">int</span>&gt;
<a name="l00097"></a>00097     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a64fb6fb73d7407926c1fbd93ed3182da" title="Distributed factorization (on several nodes).">FactorizeDistributedMatrix</a>(MPI::Comm&amp; comm_facto,
<a name="l00098"></a>00098                                     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;pastix_int_t, VectFull, Alloc1&gt;</a>&amp;,
<a name="l00099"></a>00099                                     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;pastix_int_t, VectFull, Alloc2&gt;</a>&amp;,
<a name="l00100"></a>00100                                     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc3&gt;</a>&amp;,
<a name="l00101"></a>00101                                     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Tint&gt;</a>&amp; glob_number,
<a name="l00102"></a>00102                                     <span class="keywordtype">bool</span> sym, <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>);
<a name="l00103"></a>00103 
<a name="l00104"></a>00104     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2, <span class="keyword">class</span> T<span class="keywordtype">int</span>&gt;
<a name="l00105"></a>00105     <span class="keywordtype">void</span> SolveDistributed(MPI::Comm&amp; comm_facto,
<a name="l00106"></a>00106                           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Vect_Full, Allocator2&gt;</a>&amp; x,
<a name="l00107"></a>00107                           <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Tint&gt;</a>&amp; glob_num);
<a name="l00108"></a>00108 
<a name="l00109"></a>00109     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Transpose_status, <span class="keyword">class</span> T<span class="keywordtype">int</span>&gt;
<a name="l00110"></a>00110     <span class="keywordtype">void</span> SolveDistributed(MPI::Comm&amp; comm_facto,
<a name="l00111"></a>00111                           <span class="keyword">const</span> Transpose_status&amp; TransA,
<a name="l00112"></a>00112                           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Vect_Full, Allocator2&gt;</a>&amp; x,
<a name="l00113"></a>00113                           <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Tint&gt;</a>&amp; glob_num);
<a name="l00114"></a>00114 
<a name="l00115"></a>00115   };
<a name="l00116"></a>00116 
<a name="l00117"></a>00117 }
<a name="l00118"></a>00118 
<a name="l00119"></a>00119 <span class="preprocessor">#define SELDON_FILE_PASTIX_HXX</span>
<a name="l00120"></a>00120 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
<a name="l00121"></a>00121 <span class="preprocessor"></span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
