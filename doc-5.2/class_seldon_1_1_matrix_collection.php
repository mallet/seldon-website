<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix_collection.php">MatrixCollection</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a> &#124;
<a href="#pro-static-attribs">Static Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::MatrixCollection" --><!-- doxytag: inherits="Seldon::Matrix_Base" -->
<p><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> class made of a collection of matrices.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_matrix_collection_8hxx_source.php">MatrixCollection.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_matrix_collection.png" usemap="#Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;_map" alt=""/>
  <map id="Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;_map" name="Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;_map">
<area href="class_seldon_1_1_matrix___base.php" alt="Seldon::Matrix_Base&lt; T, Allocator &gt;" shape="rect" coords="0,0,328,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_matrix_collection-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa88a18e034c43ecf58af89c12caf40ee"></a><!-- doxytag: member="Seldon::MatrixCollection::value_type" ref="aa88a18e034c43ecf58af89c12caf40ee" args="" -->
typedef T::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac10cd04b73114f5da0d646fa86c06db9"></a><!-- doxytag: member="Seldon::MatrixCollection::pointer" ref="ac10cd04b73114f5da0d646fa86c06db9" args="" -->
typedef T::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2e95eec3bc45f7a986dab8ac4469e553"></a><!-- doxytag: member="Seldon::MatrixCollection::const_pointer" ref="a2e95eec3bc45f7a986dab8ac4469e553" args="" -->
typedef T::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae1bc5d1b104a0d53406ca57817e1700e"></a><!-- doxytag: member="Seldon::MatrixCollection::reference" ref="ae1bc5d1b104a0d53406ca57817e1700e" args="" -->
typedef T::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aeb8eb42d6af02fe07be33ba18f864b54"></a><!-- doxytag: member="Seldon::MatrixCollection::const_reference" ref="aeb8eb42d6af02fe07be33ba18f864b54" args="" -->
typedef T::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a05bdff9b076483174950ec393abe4edb"></a><!-- doxytag: member="Seldon::MatrixCollection::matrix_type" ref="a05bdff9b076483174950ec393abe4edb" args="" -->
typedef T&nbsp;</td><td class="memItemRight" valign="bottom"><b>matrix_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a99f0d5fa0a85e9e3a0c2a399707e25ac"></a><!-- doxytag: member="Seldon::MatrixCollection::matrix_pointer" ref="a99f0d5fa0a85e9e3a0c2a399707e25ac" args="" -->
typedef matrix_type *&nbsp;</td><td class="memItemRight" valign="bottom"><b>matrix_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9b8813cb57ad9d0b57f44bd7d50f1dc2"></a><!-- doxytag: member="Seldon::MatrixCollection::const_matrix_pointer" ref="a9b8813cb57ad9d0b57f44bd7d50f1dc2" args="" -->
typedef const matrix_type *&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_matrix_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a08f2deac4107cd2143f6b226f2f636cc"></a><!-- doxytag: member="Seldon::MatrixCollection::matrix_reference" ref="a08f2deac4107cd2143f6b226f2f636cc" args="" -->
typedef matrix_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>matrix_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0d8aa06b182fdb00cd02662b6c796c9d"></a><!-- doxytag: member="Seldon::MatrixCollection::const_matrix_reference" ref="a0d8aa06b182fdb00cd02662b6c796c9d" args="" -->
typedef const matrix_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_matrix_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa7b493846b0081099137acb2d51cd27d"></a><!-- doxytag: member="Seldon::MatrixCollection::collection_type" ref="aa7b493846b0081099137acb2d51cd27d" args="" -->
typedef <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; matrix_type, <br class="typebreak"/>
Prop, Storage, <a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a>&lt; T &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>collection_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a663cb4cb4f207cc949b6ba754ec0ba21"></a><!-- doxytag: member="Seldon::MatrixCollection::const_collection_type" ref="a663cb4cb4f207cc949b6ba754ec0ba21" args="" -->
typedef const <a class="el" href="class_seldon_1_1_matrix.php">collection_type</a>&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_collection_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9bf98196aa77adf6ffc568b68b243637"></a><!-- doxytag: member="Seldon::MatrixCollection::collection_reference" ref="a9bf98196aa77adf6ffc568b68b243637" args="" -->
typedef <a class="el" href="class_seldon_1_1_matrix.php">collection_type</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>collection_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac8ea06c664aa78eafedf4ecc9d7a806c"></a><!-- doxytag: member="Seldon::MatrixCollection::const_collection_reference" ref="ac8ea06c664aa78eafedf4ecc9d7a806c" args="" -->
typedef const <a class="el" href="class_seldon_1_1_matrix.php">collection_type</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_collection_reference</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a4f5acfa96f23bdd2a9ad0f19e72a43c5">MatrixCollection</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#a4f5acfa96f23bdd2a9ad0f19e72a43c5"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#ad509be14489a556867ab433667c33a74">MatrixCollection</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Main constructor.  <a href="#ad509be14489a556867ab433667c33a74"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aac3566347d1654f0fd5748568ba11d60"></a><!-- doxytag: member="Seldon::MatrixCollection::MatrixCollection" ref="aac3566347d1654f0fd5748568ba11d60" args="(const MatrixCollection&lt; T, Prop, Storage, Allocator &gt; &amp;A)" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#aac3566347d1654f0fd5748568ba11d60">MatrixCollection</a> (const <a class="el" href="class_seldon_1_1_matrix_collection.php">MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copy constructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acd85176ecf48145fcb40621d4e7401e8"></a><!-- doxytag: member="Seldon::MatrixCollection::~MatrixCollection" ref="acd85176ecf48145fcb40621d4e7401e8" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#acd85176ecf48145fcb40621d4e7401e8">~MatrixCollection</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Destructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a508ae5c18005383193f2a289bf63d608">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the matrix.  <a href="#a508ae5c18005383193f2a289bf63d608"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a04f079fbfc7851ed7c1fa48a12936b9f">Nullify</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the matrix.  <a href="#a04f079fbfc7851ed7c1fa48a12936b9f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a6b14667b3459659959f968847038524d">Nullify</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears a given underlying matrix.  <a href="#a6b14667b3459659959f968847038524d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad1f04e7bbca49f4b4ac310f5a914085e"></a><!-- doxytag: member="Seldon::MatrixCollection::Deallocate" ref="ad1f04e7bbca49f4b4ac310f5a914085e" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#ad1f04e7bbca49f4b4ac310f5a914085e">Deallocate</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Deallocates underlying the matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">GetM</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows.  <a href="#ae7cfeea59b5f9181e6148e084a6efcf7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#aaa428778f995ce6bbe08f1181fbb51e4">GetMmatrix</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows.  <a href="#aaa428778f995ce6bbe08f1181fbb51e4"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#ab6cdf0d339a99d8ab7baa9e788c1d5b2">GetM</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows in an underlying matrix.  <a href="#ab6cdf0d339a99d8ab7baa9e788c1d5b2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">GetN</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns.  <a href="#a498904993d7b65f5c522c21ca991475d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a4a1165a73495d9eb4edfd49ba8e1f9b7">GetNmatrix</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns.  <a href="#a4a1165a73495d9eb4edfd49ba8e1f9b7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#af2f613864c55870df9232689b81896c4">GetN</a> (int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns in an underlying matrix.  <a href="#af2f613864c55870df9232689b81896c4"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">GetSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored in memory.  <a href="#a897f68c33d448f7961680307ae4d75c5"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a1162607a4f1255a388598a6dc5eea1f8">GetDataSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored in memory.  <a href="#a1162607a4f1255a388598a6dc5eea1f8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a2cafbea09c87f1798305211e30512994">Reallocate</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates memory to resize the matrix collection.  <a href="#a2cafbea09c87f1798305211e30512994"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 , class Prop0 , class Storage0 , class Allocator0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a42828d0fb580934560080b2cf15eaa78">SetMatrix</a> (int m, int n, const <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T0, Prop0, Storage0, Allocator0 &gt; &amp;)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets an underlying matrix in the matrix collection.  <a href="#a42828d0fb580934560080b2cf15eaa78"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 , class Prop0 , class Allocator0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a4960e099b8a36df7b772a6a9a7261cda">SetMatrix</a> (int m, int n, const <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T0, Prop0, <a class="el" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator0 &gt; &amp;)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets an underlying matrix in the matrix collection.  <a href="#a4960e099b8a36df7b772a6a9a7261cda"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">matrix_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#ad18c52c1ed65330e6443e4519df390a4">GetMatrix</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access to an underlying matrix.  <a href="#ad18c52c1ed65330e6443e4519df390a4"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_matrix_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#ad4fcca36d06203d8324a1fed15b12a65">GetMatrix</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access to an underlying matrix.  <a href="#ad4fcca36d06203d8324a1fed15b12a65"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">value_type&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a774399178dd07c6e598e6051e8f89009">operator()</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a774399178dd07c6e598e6051e8f89009"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix_collection.php">MatrixCollection</a>&lt; T, Prop, <br class="typebreak"/>
Storage, Allocator &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a9e3b633e9622c3a86561f8b08e9ff8e8">operator=</a> (const <a class="el" href="class_seldon_1_1_matrix_collection.php">MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a matrix collection (assignment operator).  <a href="#a9e3b633e9622c3a86561f8b08e9ff8e8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a0654c385392b3f0634a8721a797e249a">Copy</a> (const <a class="el" href="class_seldon_1_1_matrix_collection.php">MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a matrix collection.  <a href="#a0654c385392b3f0634a8721a797e249a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#af85cc972c66fe36630031f480dea28ca">Print</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays the matrix collection on the standard output.  <a href="#af85cc972c66fe36630031f480dea28ca"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#aaf59bd39550a1d4b460bc53e16362890">Print</a> (int m, int n) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays an underlying matrix on the standard output.  <a href="#aaf59bd39550a1d4b460bc53e16362890"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#ab3016490b7df18d3017477352f6b60d6">Write</a> (string FileName, bool with_size) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix collection in a file.  <a href="#ab3016490b7df18d3017477352f6b60d6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a66a1f1f41da42db350225a63f29000f5">Write</a> (ostream &amp;FileStream, bool with_size) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix collection to an output stream.  <a href="#a66a1f1f41da42db350225a63f29000f5"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a5c8fd96466c6409871d381feece50e44">WriteText</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix collection in a file.  <a href="#a5c8fd96466c6409871d381feece50e44"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#ad2d59d8ec2b0aca826f453d8ce646963">WriteText</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix collection to an output stream.  <a href="#ad2d59d8ec2b0aca826f453d8ce646963"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#af54798b7169f468681f24203b6688aba">Read</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix collection from a file.  <a href="#af54798b7169f468681f24203b6688aba"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#ae7df567116f04beb60b90d5397ea16e8">Read</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix collection from an input stream.  <a href="#ae7df567116f04beb60b90d5397ea16e8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows of the matrix possibly transposed.  <a href="#ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns of the matrix possibly transposed.  <a href="#a7d27412bc9384a5ce0c0db1ee310d31c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer to the data array.  <a href="#a453a269dfe7fadba249064363d5ab92a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a const pointer to the data array.  <a href="#a0f2796430deb08e565df8ced656097bf"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "void*" to the data array.  <a href="#a505cdfee34741c463e0dc1943337bedc"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "const void*" to the data array.  <a href="#a5979450cd8801810229f4a24c36e6639"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">Allocator &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the allocator of the matrix.  <a href="#af88748a55208349367d6860ad76fd691"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a960417c35add0c178e31e8578bd5eb9b"></a><!-- doxytag: member="Seldon::MatrixCollection::nz_" ref="a960417c35add0c178e31e8578bd5eb9b" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a960417c35add0c178e31e8578bd5eb9b">nz_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of non-zero elements. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a094131b6edf39309cf51ec6aea9c271b"></a><!-- doxytag: member="Seldon::MatrixCollection::Mmatrix_" ref="a094131b6edf39309cf51ec6aea9c271b" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a094131b6edf39309cf51ec6aea9c271b">Mmatrix_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of rows of matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="abf9ee2f169c7992c2d540d07dd85b104"></a><!-- doxytag: member="Seldon::MatrixCollection::Nmatrix_" ref="abf9ee2f169c7992c2d540d07dd85b104" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#abf9ee2f169c7992c2d540d07dd85b104">Nmatrix_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of columns of matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a234a716260de98d93fc40fc2a3fd44fe"></a><!-- doxytag: member="Seldon::MatrixCollection::Mlocal_" ref="a234a716260de98d93fc40fc2a3fd44fe" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_calloc_alloc.php">CallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a234a716260de98d93fc40fc2a3fd44fe">Mlocal_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of rows in the underlying matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa16e80513c77e58a2c1b3974e360f220"></a><!-- doxytag: member="Seldon::MatrixCollection::Mlocal_sum_" ref="aa16e80513c77e58a2c1b3974e360f220" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_calloc_alloc.php">CallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#aa16e80513c77e58a2c1b3974e360f220">Mlocal_sum_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Cumulative number of rows in the underlying matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afdcab0ec07a154e7ed23ee20b2f4f574"></a><!-- doxytag: member="Seldon::MatrixCollection::Nlocal_" ref="afdcab0ec07a154e7ed23ee20b2f4f574" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_calloc_alloc.php">CallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#afdcab0ec07a154e7ed23ee20b2f4f574">Nlocal_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of columns in the underlying matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2cd7c7804f76b6e8a6966e6597e05244"></a><!-- doxytag: member="Seldon::MatrixCollection::Nlocal_sum_" ref="a2cd7c7804f76b6e8a6966e6597e05244" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_calloc_alloc.php">CallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a2cd7c7804f76b6e8a6966e6597e05244">Nlocal_sum_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Cumulative number of columns in the underlying matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a66297d4139bb2b5b07979ef929358399"></a><!-- doxytag: member="Seldon::MatrixCollection::matrix_" ref="a66297d4139bb2b5b07979ef929358399" args="" -->
<a class="el" href="class_seldon_1_1_matrix.php">collection_type</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a66297d4139bb2b5b07979ef929358399">matrix_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Pointers of the underlying matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a96b39ff494d4b7d3e30f320a1c7b4aba"></a><!-- doxytag: member="Seldon::MatrixCollection::m_" ref="a96b39ff494d4b7d3e30f320a1c7b4aba" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>m_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a11cb5d502050e5f14482ff0c9cef5a76"></a><!-- doxytag: member="Seldon::MatrixCollection::n_" ref="a11cb5d502050e5f14482ff0c9cef5a76" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>n_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acb12a8b74699fae7ae3b2b67376795a1"></a><!-- doxytag: member="Seldon::MatrixCollection::data_" ref="acb12a8b74699fae7ae3b2b67376795a1" args="" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-static-attribs"></a>
Static Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a132fa01ce120f352d434fd920e5ad9b4"></a><!-- doxytag: member="Seldon::MatrixCollection::allocator_" ref="a132fa01ce120f352d434fd920e5ad9b4" args="" -->
static Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>allocator_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Prop, class Storage, class Allocator = NewAlloc&lt;T&gt;&gt;<br/>
 class Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</h3>

<p><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> class made of a collection of matrices. </p>

<p>Definition at line <a class="el" href="_matrix_collection_8hxx_source.php#l00037">37</a> of file <a class="el" href="_matrix_collection_8hxx_source.php">MatrixCollection.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="a4f5acfa96f23bdd2a9ad0f19e72a43c5"></a><!-- doxytag: member="Seldon::MatrixCollection::MatrixCollection" ref="a4f5acfa96f23bdd2a9ad0f19e72a43c5" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix_collection.php">MatrixCollection</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>On exit, the matrix is an empty 0x0 matrix. </p>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00042">42</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad509be14489a556867ab433667c33a74"></a><!-- doxytag: member="Seldon::MatrixCollection::MatrixCollection" ref="ad509be14489a556867ab433667c33a74" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix_collection.php">MatrixCollection</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Main constructor. </p>
<p>Builds a i x j collection matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows of matrices. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns of matrices. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00061">61</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="a508ae5c18005383193f2a289bf63d608"></a><!-- doxytag: member="Seldon::MatrixCollection::Clear" ref="a508ae5c18005383193f2a289bf63d608" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::Clear </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the matrix. </p>
<p>Destructs the matrix. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>On exit, the matrix is an empty 0x0 matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00105">105</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0654c385392b3f0634a8721a797e249a"></a><!-- doxytag: member="Seldon::MatrixCollection::Copy" ref="a0654c385392b3f0634a8721a797e249a" args="(const MatrixCollection&lt; T, Prop, Storage, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop, class Storage, class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::Copy </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_matrix_collection.php">MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a matrix collection. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>A</em>&nbsp;</td><td>matrix collection to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: <em>A</em> is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00557">557</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af88748a55208349367d6860ad76fd691"></a><!-- doxytag: member="Seldon::MatrixCollection::GetAllocator" ref="af88748a55208349367d6860ad76fd691" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">Allocator &amp; <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetAllocator </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the allocator of the matrix. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The allocator. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00258">258</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a453a269dfe7fadba249064363d5ab92a"></a><!-- doxytag: member="Seldon::MatrixCollection::GetData" ref="a453a269dfe7fadba249064363d5ab92a" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer to the data array. </p>
<p>Returns a pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00208">208</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0f2796430deb08e565df8ced656097bf"></a><!-- doxytag: member="Seldon::MatrixCollection::GetDataConst" ref="a0f2796430deb08e565df8ced656097bf" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::const_pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConst </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a const pointer to the data array. </p>
<p>Returns a const pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00221">221</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5979450cd8801810229f4a24c36e6639"></a><!-- doxytag: member="Seldon::MatrixCollection::GetDataConstVoid" ref="a5979450cd8801810229f4a24c36e6639" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConstVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "const void*" to the data array. </p>
<p>Returns a pointer of type "const void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00247">247</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1162607a4f1255a388598a6dc5eea1f8"></a><!-- doxytag: member="Seldon::MatrixCollection::GetDataSize" ref="a1162607a4f1255a388598a6dc5eea1f8" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::GetDataSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored in memory. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements stored in memory. </dd></dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00280">280</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a505cdfee34741c463e0dc1943337bedc"></a><!-- doxytag: member="Seldon::MatrixCollection::GetDataVoid" ref="a505cdfee34741c463e0dc1943337bedc" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "void*" to the data array. </p>
<p>Returns a pointer of type "void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00234">234</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae7cfeea59b5f9181e6148e084a6efcf7"></a><!-- doxytag: member="Seldon::MatrixCollection::GetM" ref="ae7cfeea59b5f9181e6148e084a6efcf7" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the total number of rows. It is the sum of the number of rows in the underlying matrices. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00181">181</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab6cdf0d339a99d8ab7baa9e788c1d5b2"></a><!-- doxytag: member="Seldon::MatrixCollection::GetM" ref="ab6cdf0d339a99d8ab7baa9e788c1d5b2" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows in an underlying matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index of the underlying matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows in the underlying matrices with row index <em>i</em>. </dd></dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00205">205</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><!-- doxytag: member="Seldon::MatrixCollection::GetM" ref="ab6f6c5bba065ca82dad7c3abdbc8ea79" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#aecf3e8c636dbb83335ea588afe2558a8">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00130">130</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad4fcca36d06203d8324a1fed15b12a65"></a><!-- doxytag: member="Seldon::MatrixCollection::GetMatrix" ref="ad4fcca36d06203d8324a1fed15b12a65" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix_collection.php">MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::const_matrix_reference <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::GetMatrix </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access to an underlying matrix. </p>
<p>Returns the underlying matrix (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The matrix collection (i, j). </dd></dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00472">472</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad18c52c1ed65330e6443e4519df390a4"></a><!-- doxytag: member="Seldon::MatrixCollection::GetMatrix" ref="ad18c52c1ed65330e6443e4519df390a4" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix_collection.php">MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::matrix_reference <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::GetMatrix </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access to an underlying matrix. </p>
<p>Returns the underlying matrix (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The matrix collection (i, j). </dd></dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00443">443</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aaa428778f995ce6bbe08f1181fbb51e4"></a><!-- doxytag: member="Seldon::MatrixCollection::GetMmatrix" ref="aaa428778f995ce6bbe08f1181fbb51e4" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::GetMmatrix </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the total number of rows. It is the sum of the number of rows in the underlying matrices. </dd></dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00193">193</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a498904993d7b65f5c522c21ca991475d"></a><!-- doxytag: member="Seldon::MatrixCollection::GetN" ref="a498904993d7b65f5c522c21ca991475d" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the total number of columns. It is the sum of the number of columns in the underlying matrices. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00225">225</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af2f613864c55870df9232689b81896c4"></a><!-- doxytag: member="Seldon::MatrixCollection::GetN" ref="af2f613864c55870df9232689b81896c4" args="(int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns in an underlying matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index of the underlying matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns in the underlying matrices with column index <em>j</em>. </dd></dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00250">250</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7d27412bc9384a5ce0c0db1ee310d31c"></a><!-- doxytag: member="Seldon::MatrixCollection::GetN" ref="a7d27412bc9384a5ce0c0db1ee310d31c" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a867066f7bf531bf7ad15bdcd034dc3c0">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00145">145</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a4a1165a73495d9eb4edfd49ba8e1f9b7"></a><!-- doxytag: member="Seldon::MatrixCollection::GetNmatrix" ref="a4a1165a73495d9eb4edfd49ba8e1f9b7" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::GetNmatrix </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the total number of columns. It is the sum of the number of columns in the underlying matrices. </dd></dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00237">237</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a897f68c33d448f7961680307ae4d75c5"></a><!-- doxytag: member="Seldon::MatrixCollection::GetSize" ref="a897f68c33d448f7961680307ae4d75c5" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::GetSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored in memory. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements stored in memory. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00269">269</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a04f079fbfc7851ed7c1fa48a12936b9f"></a><!-- doxytag: member="Seldon::MatrixCollection::Nullify" ref="a04f079fbfc7851ed7c1fa48a12936b9f" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::Nullify </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the matrix. </p>
<p>Destructs the matrix. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>On exit, the matrix is an empty 0x0 matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00129">129</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6b14667b3459659959f968847038524d"></a><!-- doxytag: member="Seldon::MatrixCollection::Nullify" ref="a6b14667b3459659959f968847038524d" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::Nullify </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears a given underlying matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row of the underlying matrix to be nullified. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column of the underlying matrix to be nullified. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00152">152</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a774399178dd07c6e598e6051e8f89009"></a><!-- doxytag: member="Seldon::MatrixCollection::operator()" ref="a774399178dd07c6e598e6051e8f89009" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix_collection.php">MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::value_type <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00502">502</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9e3b633e9622c3a86561f8b08e9ff8e8"></a><!-- doxytag: member="Seldon::MatrixCollection::operator=" ref="a9e3b633e9622c3a86561f8b08e9ff8e8" args="(const MatrixCollection&lt; T, Prop, Storage, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop, class Storage, class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix_collection.php">MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_matrix_collection.php">MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a matrix collection (assignment operator). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>A</em>&nbsp;</td><td>matrix collection to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: <em>A</em> is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00542">542</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af85cc972c66fe36630031f480dea28ca"></a><!-- doxytag: member="Seldon::MatrixCollection::Print" ref="af85cc972c66fe36630031f480dea28ca" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::Print </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Displays the matrix collection on the standard output. </p>
<p>Displays elements on the standard output, in text format. </p>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00589">589</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aaf59bd39550a1d4b460bc53e16362890"></a><!-- doxytag: member="Seldon::MatrixCollection::Print" ref="aaf59bd39550a1d4b460bc53e16362890" args="(int m, int n) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::Print </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Displays an underlying matrix on the standard output. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>m</em>&nbsp;</td><td>row index of the underlying matrix. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>n</em>&nbsp;</td><td>column index of the underlying matrix. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00607">607</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae7df567116f04beb60b90d5397ea16e8"></a><!-- doxytag: member="Seldon::MatrixCollection::Read" ref="ae7df567116f04beb60b90d5397ea16e8" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix collection from an input stream. </p>
<p>Reads a matrix collection stored in binary format from a stream. The number of rows of matrices (integer) and the number of columns of matrices (integer) are read, and the underlying matrices are then read in the same order as it should be in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in,out]</tt>&nbsp;</td><td valign="top"><em>FileStream</em>&nbsp;</td><td>input stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00801">801</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af54798b7169f468681f24203b6688aba"></a><!-- doxytag: member="Seldon::MatrixCollection::Read" ref="af54798b7169f468681f24203b6688aba" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix collection from a file. </p>
<p>Reads a matrix collection stored in binary format in a file. The number of rows of matrices (integer) and the number of columns of matrices (integer) are read, and the underlying matrices are then read in the same order as it should be in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>FileName</em>&nbsp;</td><td>input file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00774">774</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2cafbea09c87f1798305211e30512994"></a><!-- doxytag: member="Seldon::MatrixCollection::Reallocate" ref="a2cafbea09c87f1798305211e30512994" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::Reallocate </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates memory to resize the matrix collection. </p>
<p>On exit, the matrix is a matrix collection with <em>i</em> x <em>j</em> underlying matrices. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows of matrices. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns of matrices. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Depending on your allocator, data may be lost. </dd></dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00300">300</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a4960e099b8a36df7b772a6a9a7261cda"></a><!-- doxytag: member="Seldon::MatrixCollection::SetMatrix" ref="a4960e099b8a36df7b772a6a9a7261cda" args="(int m, int n, const Matrix&lt; T0, Prop0, RowSparse, Allocator0 &gt; &amp;)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 , class Prop0 , class Allocator0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::SetMatrix </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T0, Prop0, <a class="el" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets an underlying matrix in the matrix collection. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row of the underlying matrix to be set. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column of the underlying matrix to be set. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>matrix</em>&nbsp;</td><td>new value for the underlying matrix. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00381">381</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a42828d0fb580934560080b2cf15eaa78"></a><!-- doxytag: member="Seldon::MatrixCollection::SetMatrix" ref="a42828d0fb580934560080b2cf15eaa78" args="(int m, int n, const Matrix&lt; T0, Prop0, Storage0, Allocator0 &gt; &amp;)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 , class Prop0 , class Storage0 , class Allocator0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::SetMatrix </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T0, Prop0, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets an underlying matrix in the matrix collection. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row of the underlying matrix to be set. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column of the underlying matrix to be set. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>matrix</em>&nbsp;</td><td>new value for the underlying matrix. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00326">326</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a66a1f1f41da42db350225a63f29000f5"></a><!-- doxytag: member="Seldon::MatrixCollection::Write" ref="a66a1f1f41da42db350225a63f29000f5" args="(ostream &amp;FileStream, bool with_size) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix collection to an output stream. </p>
<p>Writes the matrix collection to an output stream in binary format. The number of rows of matrices (integer) and the number of columns of matrices (integer) are written, and the underlying matrices are then written in the same order as in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in,out]</tt>&nbsp;</td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output stream. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the dimensions of the matrix are not saved. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00666">666</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab3016490b7df18d3017477352f6b60d6"></a><!-- doxytag: member="Seldon::MatrixCollection::Write" ref="ab3016490b7df18d3017477352f6b60d6" args="(string FileName, bool with_size) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix collection in a file. </p>
<p>Stores the matrix collection in a file in binary format. The number of rows of matrices (integer) and the number of columns of matrices (integer) are written, and the underlying matrices are then written in the same order as in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the dimensions of the matrix are not saved. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00637">637</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad2d59d8ec2b0aca826f453d8ce646963"></a><!-- doxytag: member="Seldon::MatrixCollection::WriteText" ref="ad2d59d8ec2b0aca826f453d8ce646963" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix collection to an output stream. </p>
<p>Stores the matrix to an output stream in text format. Only the underlying matrices are written, without the dimensions. Each row is written on a single line and elements of a row are delimited by tabulations. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in,out]</tt>&nbsp;</td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00738">738</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5c8fd96466c6409871d381feece50e44"></a><!-- doxytag: member="Seldon::MatrixCollection::WriteText" ref="a5c8fd96466c6409871d381feece50e44" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, Storage, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix collection in a file. </p>
<p>Stores the matrix in a file in text format. Only the underlying matrices are written, without the dimensions. Each row is written on a single line and elements of a row are delimited by tabulations. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00709">709</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>matrix/<a class="el" href="_matrix_collection_8hxx_source.php">MatrixCollection.hxx</a></li>
<li>matrix/<a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
