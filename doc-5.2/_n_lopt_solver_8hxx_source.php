<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/optimization/NLoptSolver.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2011 INRIA</span>
<a name="l00002"></a>00002 <span class="comment">// Author(s): Marc Fragu</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_COMPUTATION_OPTIMIZATION_NLOPTSOLVER_HXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span><span class="preprocessor">#define SELDON_COMPUTATION_OPTIMIZATION_NLOPTSOLVER_HXX</span>
<a name="l00023"></a>00023 <span class="preprocessor"></span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="preprocessor">#include &quot;NLopt.hxx&quot;</span>
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 <span class="keyword">namespace </span>Seldon
<a name="l00029"></a>00029 {
<a name="l00030"></a>00030 
<a name="l00031"></a>00031 
<a name="l00033"></a>00033   <span class="comment">// NLOPTSOLVER //</span>
<a name="l00035"></a>00035 <span class="comment"></span>
<a name="l00036"></a>00036 
<a name="l00038"></a><a class="code" href="class_seldon_1_1_n_lopt_solver.php">00038</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_n_lopt_solver.php" title="NLopt optimization.">NLoptSolver</a>
<a name="l00039"></a>00039   {
<a name="l00040"></a>00040 
<a name="l00041"></a>00041   <span class="keyword">protected</span>:
<a name="l00042"></a>00042 
<a name="l00043"></a>00043     <span class="keyword">typedef</span> double (*cost_ptr)(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double&gt;</a>&amp;,
<a name="l00044"></a>00044                                <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double&gt;</a>&amp;, <span class="keywordtype">void</span>*);
<a name="l00045"></a>00045 
<a name="l00047"></a><a class="code" href="class_seldon_1_1_n_lopt_solver.php#a0236ea3add112a572ed2326a12306ded">00047</a>     <a class="code" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a0236ea3add112a572ed2326a12306ded" title="NLopt optimization solver.">opt_</a>;
<a name="l00049"></a><a class="code" href="class_seldon_1_1_n_lopt_solver.php#aad0beaa06728577f5f7c39c2989f81ed">00049</a>     nlopt::algorithm <a class="code" href="class_seldon_1_1_n_lopt_solver.php#aad0beaa06728577f5f7c39c2989f81ed" title="Optimization algorithm.">algorithm_</a>;
<a name="l00051"></a><a class="code" href="class_seldon_1_1_n_lopt_solver.php#adefce6eb95498ffb35690ce4c8c7f637">00051</a>     <span class="keywordtype">double</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#adefce6eb95498ffb35690ce4c8c7f637" title="Relative tolerance on the optimization parameters.">parameter_tolerance_</a>;
<a name="l00053"></a><a class="code" href="class_seldon_1_1_n_lopt_solver.php#afd6a5c3ebbb73a71fbd412a641eca860">00053</a>     <span class="keywordtype">double</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#afd6a5c3ebbb73a71fbd412a641eca860" title="Relative tolerance on the cost function.">cost_function_tolerance_</a>;
<a name="l00056"></a><a class="code" href="class_seldon_1_1_n_lopt_solver.php#ac3340df79837a4cdde5560939ed01989">00056</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#ac3340df79837a4cdde5560939ed01989" title="Maximum number of function evaluations. It is ignored if it is non-positive.">Niteration_max_</a>;
<a name="l00060"></a><a class="code" href="class_seldon_1_1_n_lopt_solver.php#aade135d662e35906b7cae2ecbd1d789c">00060</a>     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double&gt;</a> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#aade135d662e35906b7cae2ecbd1d789c" title="The vector that stores parameters values. Before optimization, stores the initial...">parameter_</a>;
<a name="l00064"></a><a class="code" href="class_seldon_1_1_n_lopt_solver.php#a3d4a7ac50a792cdd899ff11a0cd5f3f3">00064</a>     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double&gt;</a> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a3d4a7ac50a792cdd899ff11a0cd5f3f3" title="The vector that stores gradient values. Before optimization, unspecified; after optimization...">gradient_</a>;
<a name="l00066"></a><a class="code" href="class_seldon_1_1_n_lopt_solver.php#afb1ea25dcf6a9da4b54f97d5ccf95cc8">00066</a>     <span class="keywordtype">double</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#afb1ea25dcf6a9da4b54f97d5ccf95cc8" title="The value of cost function for given parameter values.">cost_</a>;
<a name="l00067"></a>00067 
<a name="l00068"></a>00068   <span class="keyword">public</span>:
<a name="l00069"></a>00069     <span class="comment">// Constructor and destructor.</span>
<a name="l00070"></a>00070     <a class="code" href="class_seldon_1_1_n_lopt_solver.php#ad775e71c990c2d8f08feaf0399a1a916" title="Default constructor.">NLoptSolver</a>();
<a name="l00071"></a>00071     <a class="code" href="class_seldon_1_1_n_lopt_solver.php#adcf818c843283a20eef4d17a974fd6cc" title="Destructor.">~NLoptSolver</a>();
<a name="l00072"></a>00072 
<a name="l00073"></a>00073     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#ad80720692c1df8718044c24ecbc98372" title="Initializations.">Initialize</a>(<span class="keywordtype">int</span> Nparameter, <span class="keywordtype">string</span> algorithm,
<a name="l00074"></a>00074                     <span class="keywordtype">double</span> parameter_tolerance = 1.e-6,
<a name="l00075"></a>00075                     <span class="keywordtype">double</span> cost_function_tolerance = 1.e-6,
<a name="l00076"></a>00076                     <span class="keywordtype">int</span> Niteration_max = -1);
<a name="l00077"></a>00077     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a6f92bbc59849ce8489da5210dfd6d994" title="Sets lower bounds on the parameters.">SetLowerBound</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double&gt;</a>&amp;);
<a name="l00078"></a>00078     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a46fee6721c1a4172bae0ce10870d4442" title="Sets upper bounds on the parameters.">SetUpperBound</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double&gt;</a>&amp;);
<a name="l00079"></a>00079     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#aa74cb3aca730ca6444fff8bd600f3be7" title="Sets the relative tolerance on the parameters.">SetParameterTolerance</a>(<span class="keywordtype">double</span>);
<a name="l00080"></a>00080     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a44ed740163ffdc82a1eda9a47001e58a" title="Sets the relative tolerance on the cost function.">SetCostFunctionTolerance</a>(<span class="keywordtype">double</span>);
<a name="l00081"></a>00081     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#ae8d18863be9cbbf79bc14e5fdc905a39" title="Sets the maximum number of cost function evaluations.">SetNiterationMax</a>(<span class="keywordtype">int</span>);
<a name="l00082"></a>00082     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a4e7c4cb0c59c99b234c89b23ffd56927" title="Gets the relative tolerance on the parameters.">GetParameterTolerance</a>(<span class="keywordtype">double</span>&amp;) <span class="keyword">const</span>;
<a name="l00083"></a>00083     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#ab585d1ac63039aa34eb41ba3a1548817" title="Gets the relative tolerance on the cost function.">GetCostFunctionTolerance</a>(<span class="keywordtype">double</span>&amp;) <span class="keyword">const</span>;
<a name="l00084"></a>00084     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#ac584f3bed8e3326faa50fff3c7004b62" title="Gets the maximum number of cost function evaluations.">GetNiterationMax</a>(<span class="keywordtype">int</span>&amp;) <span class="keyword">const</span>;
<a name="l00085"></a>00085     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#adb63bff470ea8bbc8df7cc78207a597f" title="Sets the parameters.">SetParameter</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double&gt;</a>&amp; parameter);
<a name="l00086"></a>00086     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a2de56e3f3de295800e412039ca5b0e69" title="Gets the parameters.">GetParameter</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double&gt;</a>&amp; parameter) <span class="keyword">const</span>;
<a name="l00087"></a>00087     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a15954e351f26ae09799c07e0009ce864" title="Optimization.">Optimize</a>(cost_ptr cost, <span class="keywordtype">void</span>* argument);
<a name="l00088"></a>00088     <span class="keywordtype">double</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#aaf4264800dbfb5f24af404ac6019e0b0" title="Returns the value of the cost function.">GetCost</a>() <span class="keyword">const</span>;
<a name="l00089"></a>00089 
<a name="l00090"></a>00090   };
<a name="l00091"></a>00091 
<a name="l00092"></a>00092 
<a name="l00093"></a>00093 } <span class="comment">// namespace Seldon.</span>
<a name="l00094"></a>00094 
<a name="l00095"></a>00095 
<a name="l00096"></a>00096 <span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
