<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a> &#124;
<a href="#pro-static-attribs">Static Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Matrix_SymComplexSparse" --><!-- doxytag: inherits="Seldon::Matrix_Base" -->
<p><a class="el" href="class_seldon_1_1_symmetric.php">Symmetric</a> complex sparse-matrix class.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_matrix___sym_complex_sparse_8hxx_source.php">Matrix_SymComplexSparse.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_matrix___sym_complex_sparse.png" usemap="#Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;_map" alt=""/>
  <map id="Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;_map" name="Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;_map">
<area href="class_seldon_1_1_matrix___base.php" alt="Seldon::Matrix_Base&lt; T, Allocator &gt;" shape="rect" coords="0,0,391,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_matrix___sym_complex_sparse-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a60a98aa026820ad21349412e79bb7805"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::value_type" ref="a60a98aa026820ad21349412e79bb7805" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a91d05e834526448fb4cf9dfa85aca66c"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::pointer" ref="a91d05e834526448fb4cf9dfa85aca66c" args="" -->
typedef Allocator::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aefe5cc70a26876e3a3dd113b23dab90e"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::const_pointer" ref="aefe5cc70a26876e3a3dd113b23dab90e" args="" -->
typedef Allocator::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a43c3660803a61b5c9bd38f3133d427ab"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::reference" ref="a43c3660803a61b5c9bd38f3133d427ab" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a27c5eceb528f581eb2e93216140ffa77"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::const_reference" ref="a27c5eceb528f581eb2e93216140ffa77" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a00d2964761ccb7bf70b99dbc737c0df8"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::entry_type" ref="a00d2964761ccb7bf70b99dbc737c0df8" args="" -->
typedef complex&lt; value_type &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>entry_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a97e7be8ea1b5ce6202d851394e5d419e"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::access_type" ref="a97e7be8ea1b5ce6202d851394e5d419e" args="" -->
typedef complex&lt; value_type &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>access_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af20b241a9a3ab6be0f8e7012d6ec8a1c"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::const_access_type" ref="af20b241a9a3ab6be0f8e7012d6ec8a1c" args="" -->
typedef complex&lt; value_type &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_access_type</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af4fd673569b7398d84f2d4f277de50a8">Matrix_SymComplexSparse</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#af4fd673569b7398d84f2d4f277de50a8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ab055efcee796094368d74c476dd28221">Matrix_SymComplexSparse</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Constructor.  <a href="#ab055efcee796094368d74c476dd28221"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a102b1c069e24ea2b907cefc593243ac4">Matrix_SymComplexSparse</a> (int i, int j, int real_nz, int imag_nz)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Constructor.  <a href="#a102b1c069e24ea2b907cefc593243ac4"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Storage0 , class Allocator0 , class Storage1 , class Allocator1 , class Storage2 , class Allocator2 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a063b7c64394f13e35d04d87310cf552e">Matrix_SymComplexSparse</a> (int i, int j, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;real_values, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;real_ptr, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;real_ind, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;imag_values, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;imag_ptr, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;imag_ind)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Constructor.  <a href="#a063b7c64394f13e35d04d87310cf552e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad373107c8496819b617d13f8d8336c73"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::Matrix_SymComplexSparse" ref="ad373107c8496819b617d13f8d8336c73" args="(const Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt; &amp;A)" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad373107c8496819b617d13f8d8336c73">Matrix_SymComplexSparse</a> (const <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copy constructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afcf6a5d81a59ad003dfa7f905e0b5aaa"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::~Matrix_SymComplexSparse" ref="afcf6a5d81a59ad003dfa7f905e0b5aaa" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#afcf6a5d81a59ad003dfa7f905e0b5aaa">~Matrix_SymComplexSparse</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Destructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a77439fdc40537903681e7cd912c5f732">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the matrix.  <a href="#a77439fdc40537903681e7cd912c5f732"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Storage0 , class Allocator0 , class Storage1 , class Allocator1 , class Storage2 , class Allocator2 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a3f202fe96b9289912e1cb6acfe24f18b">SetData</a> (int i, int j, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;real_values, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;real_ptr, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;real_ind, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;imag_values, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;imag_ptr, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;imag_ind)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Redefines the matrix.  <a href="#a3f202fe96b9289912e1cb6acfe24f18b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad94e329d8c48132a374a6cdf72ad2bab"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::SetData" ref="ad94e329d8c48132a374a6cdf72ad2bab" args="(int i, int j, int real_nz, pointer real_values, int *real_ptr, int *real_ind, int imag_nz, pointer imag_values, int *imag_ptr, int *imag_ind)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetData</b> (int i, int j, int real_nz, pointer real_values, int *real_ptr, int *real_ind, int imag_nz, pointer imag_values, int *imag_ptr, int *imag_ind)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af9adb37bee7561a855fef31642b85bfd">Nullify</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the matrix without releasing memory.  <a href="#af9adb37bee7561a855fef31642b85bfd"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af6ee0fe9e269657f40a8dccbe5e30b73"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::Reallocate" ref="af6ee0fe9e269657f40a8dccbe5e30b73" args="(int i, int j)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af6ee0fe9e269657f40a8dccbe5e30b73">Reallocate</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Initialization of an empty matrix i x j. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae41ce69193bc5f40015b6bc0220db998"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::Reallocate" ref="ae41ce69193bc5f40015b6bc0220db998" args="(int i, int j, int real_nz, int imag_nz)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ae41ce69193bc5f40015b6bc0220db998">Reallocate</a> (int i, int j, int real_nz, int imag_nz)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Initialization of a matrix i x j with n non-zero entries. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a6537ad73d443b7b1e7008f626593811e">Resize</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates memory to resize the matrix and keeps previous entries.  <a href="#a6537ad73d443b7b1e7008f626593811e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad2ef28f874e37f257af097c6ec306dfd">Resize</a> (int i, int j, int real_nz, int imag_nz)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates memory to resize the matrix and keeps previous entries.  <a href="#ad2ef28f874e37f257af097c6ec306dfd"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5e1062c93e237401d6bf6b1797861c62"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::Copy" ref="a5e1062c93e237401d6bf6b1797861c62" args="(const Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt; &amp;A)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a5e1062c93e237401d6bf6b1797861c62">Copy</a> (const <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copies a matrix. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#afda1dbd968e2e4b3047c014ab5931846">GetDataSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored in memory.  <a href="#afda1dbd968e2e4b3047c014ab5931846"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a005a3b7389aecf38482cc16e4984787c">GetRealPtr</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns (row or column) start indices for the real part.  <a href="#a005a3b7389aecf38482cc16e4984787c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad1bb43e80676e9ee420ea692ba06d0ac">GetImagPtr</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns (row or column) start indices for the imaginary part.  <a href="#ad1bb43e80676e9ee420ea692ba06d0ac"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a72f50ab4230ffe4af23c8bbf2203ad0b">GetRealInd</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns (row or column) indices of non-zero entries for the real part.  <a href="#a72f50ab4230ffe4af23c8bbf2203ad0b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a2bb61950179c75be2150cfae734bb05b">GetImagInd</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns (row or column) indices of non-zero entries for / the imaginary part.  <a href="#a2bb61950179c75be2150cfae734bb05b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a913aadd75d1ed2e076ea4b014e5fb32d">GetRealPtrSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the length of the array of start indices for the real part.  <a href="#a913aadd75d1ed2e076ea4b014e5fb32d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#acde6ac68023f92f32eceb4f0a422580f">GetImagPtrSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the length of the array of start indices for the imaginary part.  <a href="#acde6ac68023f92f32eceb4f0a422580f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#acdb80353a65a45dfc11b886e7c75a807">GetRealIndSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the length of the array of (column or row) indices for //! the real part.  <a href="#acdb80353a65a45dfc11b886e7c75a807"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af1c1a606649e03827a3710751238a5bc">GetImagIndSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the length of the array of (column or row) indices //! for the imaginary part.  <a href="#af1c1a606649e03827a3710751238a5bc"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad1304eb0b4e7197e603e95b119f24796">GetRealDataSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the length of the array of (column or row) indices for //! the real part.  <a href="#ad1304eb0b4e7197e603e95b119f24796"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#acd8debd721f3560c317babe090aa09e6">GetImagDataSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the length of the array of (column or row) indices //! for the imaginary part.  <a href="#acd8debd721f3560c317babe090aa09e6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a1a98f711de4b7daf42ea42c8493ca08d">GetRealData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the array of values of the real part.  <a href="#a1a98f711de4b7daf42ea42c8493ca08d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a411656148afd39425bd6df81a013d180">GetImagData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the array of values of the imaginary part.  <a href="#a411656148afd39425bd6df81a013d180"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">complex&lt; value_type &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad441174c77a9c101560e77794aae6369">operator()</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#ad441174c77a9c101560e77794aae6369"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">value_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a0e58d558d63fc51d62db121f9732cf1c">ValReal</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#a0e58d558d63fc51d62db121f9732cf1c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const value_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#abec4edb809f2be635bff90b508f9e9f6">ValReal</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#abec4edb809f2be635bff90b508f9e9f6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">value_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a4cf220882aeebe97ab0cd8afb1eb52c7">ValImag</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#a4cf220882aeebe97ab0cd8afb1eb52c7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const value_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a6243d988622a10dec95e8733710ef179">ValImag</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#a6243d988622a10dec95e8733710ef179"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">value_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a1f8cbc14fed12f61ebc5a2bc7085c619">GetReal</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#a1f8cbc14fed12f61ebc5a2bc7085c619"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const value_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a82d4964b6d1049d2a3aa8e861fe7c9c3">GetReal</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#a82d4964b6d1049d2a3aa8e861fe7c9c3"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">value_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a2be1493a466aee2b75403551ddf42539">GetImag</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#a2be1493a466aee2b75403551ddf42539"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const value_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a14c8495abb3d6ef2841ceeb76a5f1149">GetImag</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#a14c8495abb3d6ef2841ceeb76a5f1149"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ab4c3a8968e2c79248fc7d1add27f3433">Set</a> (int i, int j, const complex&lt; T &gt; &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets an element (i, j) to a value.  <a href="#ab4c3a8968e2c79248fc7d1add27f3433"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#aaf8bdd8dd7d3f089b38a6db2c18c4154">AddInteraction</a> (int i, int j, const complex&lt; T &gt; &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Add a value to a non-zero entry.  <a href="#aaf8bdd8dd7d3f089b38a6db2c18c4154"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a>&lt; T, <br class="typebreak"/>
Prop, Storage, Allocator &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#aba07f35ba600bf49197e1b1802f95c67">operator=</a> (const <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a matrix (assignment operator).  <a href="#aba07f35ba600bf49197e1b1802f95c67"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a55f35dad8db11e522dde99f236472d9d">Zero</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Resets all non-zero entries to 0-value.  <a href="#a55f35dad8db11e522dde99f236472d9d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a2116e2afc727a7c2ecf920fe41811c97">SetIdentity</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the matrix to identity.  <a href="#a2116e2afc727a7c2ecf920fe41811c97"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ab53e3d0367145d47e83de9480ac29f54">Fill</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the non-zero entries with 0, 1, 2, ...  <a href="#ab53e3d0367145d47e83de9480ac29f54"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a7a7182bc0ef4972f4e124e37836bd06c">Fill</a> (const complex&lt; T &gt; &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the non-zero entries with a given value.  <a href="#a7a7182bc0ef4972f4e124e37836bd06c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#afda343754417cd33781e8bd3f69f07d4">FillRand</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the non-zero entries randomly.  <a href="#afda343754417cd33781e8bd3f69f07d4"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#abe86d6545d4aeca07257de2d8e7a85f2">Print</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays the matrix on the standard output.  <a href="#abe86d6545d4aeca07257de2d8e7a85f2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a8c8179698d7eccc69911fcbb21262d9b">Write</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix in a file.  <a href="#a8c8179698d7eccc69911fcbb21262d9b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a01e5bc999ba6caed5db79815ebfe549e">Write</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix to an output stream.  <a href="#a01e5bc999ba6caed5db79815ebfe549e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a1c9015515a2e218c875d739bd9f410e1">WriteText</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix in a file.  <a href="#a1c9015515a2e218c875d739bd9f410e1"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a6dc1206f68ef04a7bb6ac40b2e9f7e59">WriteText</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix to an output stream.  <a href="#a6dc1206f68ef04a7bb6ac40b2e9f7e59"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#aa70fdb81ecca409e7410b7bd91c71bc8">Read</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from a file.  <a href="#aa70fdb81ecca409e7410b7bd91c71bc8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a58a6efaa3b49592962e9f3d50963ba62">Read</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from an input stream.  <a href="#a58a6efaa3b49592962e9f3d50963ba62"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ae5f0883621073e283ebd9ad047e81174">ReadText</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from a file.  <a href="#ae5f0883621073e283ebd9ad047e81174"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a2be8c4d3b6d3f794ee5c6b7cba504571">ReadText</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from an input stream.  <a href="#a2be8c4d3b6d3f794ee5c6b7cba504571"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T , class Prop , class Storage , class Allocator &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a288b0c3b764e1db7eff81cb5e56e7c63">SetData</a> (int i, int j, int real_nz, typename <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::pointer real_values, int *real_ptr, int *real_ind, int imag_nz, typename <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::pointer imag_values, int *imag_ptr, int *imag_ind)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Redefines the matrix.  <a href="#a288b0c3b764e1db7eff81cb5e56e7c63"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">GetM</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows.  <a href="#a65e9c3f0db9c7c8c3fa10ead777dd927"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows of the matrix possibly transposed.  <a href="#ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">GetN</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns.  <a href="#a6b134070d1b890ba6b7eb72fee170984"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns of the matrix possibly transposed.  <a href="#a7d27412bc9384a5ce0c0db1ee310d31c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">GetSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements in the matrix.  <a href="#a0fbc3f7030583174eaa69429f655a1b0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer to the data array.  <a href="#a453a269dfe7fadba249064363d5ab92a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a const pointer to the data array.  <a href="#a0f2796430deb08e565df8ced656097bf"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "void*" to the data array.  <a href="#a505cdfee34741c463e0dc1943337bedc"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "const void*" to the data array.  <a href="#a5979450cd8801810229f4a24c36e6639"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">Allocator &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the allocator of the matrix.  <a href="#af88748a55208349367d6860ad76fd691"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aabc20cbf97a5f06fb7542e32ca86a452"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::real_nz_" ref="aabc20cbf97a5f06fb7542e32ca86a452" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>real_nz_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a32157995226016273870a0db60e9fd36"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::imag_nz_" ref="a32157995226016273870a0db60e9fd36" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>imag_nz_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad1f2ace9d9fdc6e21cac9b7a057f0782"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::real_ptr_" ref="ad1f2ace9d9fdc6e21cac9b7a057f0782" args="" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>real_ptr_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a34a2783f923953f461ccab073054f60f"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::imag_ptr_" ref="a34a2783f923953f461ccab073054f60f" args="" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>imag_ptr_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="abf49b27e4a3d9c669d6705e29bfc6352"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::real_ind_" ref="abf49b27e4a3d9c669d6705e29bfc6352" args="" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>real_ind_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa0e30f0ff17c81bb638d1bba242992db"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::imag_ind_" ref="aa0e30f0ff17c81bb638d1bba242992db" args="" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>imag_ind_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9bf6ae45b8d302795e88e12c7f98fc2a"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::real_data_" ref="a9bf6ae45b8d302795e88e12c7f98fc2a" args="" -->
T *&nbsp;</td><td class="memItemRight" valign="bottom"><b>real_data_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a87a36b09736188fc4a13061b2c890c4e"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::imag_data_" ref="a87a36b09736188fc4a13061b2c890c4e" args="" -->
T *&nbsp;</td><td class="memItemRight" valign="bottom"><b>imag_data_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a96b39ff494d4b7d3e30f320a1c7b4aba"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::m_" ref="a96b39ff494d4b7d3e30f320a1c7b4aba" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>m_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a11cb5d502050e5f14482ff0c9cef5a76"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::n_" ref="a11cb5d502050e5f14482ff0c9cef5a76" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>n_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acb12a8b74699fae7ae3b2b67376795a1"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::data_" ref="acb12a8b74699fae7ae3b2b67376795a1" args="" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-static-attribs"></a>
Static Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a132fa01ce120f352d434fd920e5ad9b4"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::allocator_" ref="a132fa01ce120f352d434fd920e5ad9b4" args="" -->
static Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>allocator_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Prop, class Storage, class Allocator = SELDON_DEFAULT_ALLOCATOR&lt;T&gt;&gt;<br/>
 class Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</h3>

<p><a class="el" href="class_seldon_1_1_symmetric.php">Symmetric</a> complex sparse-matrix class. </p>
<p><a class="el" href="class_seldon_1_1_symmetric.php">Symmetric</a> sparse matrices are defined by: (1) the number of rows and columns; (2) the number of non-zero entries; (3) an array 'ptr_' of start indices (i.e. indices of the first element of each row or column, depending on the storage); (4) an array 'ind_' of column or row indices of each non-zero entry; (5) values of non-zero entries.</p>
<dl class="user"><dt><b></b></dt><dd>Complex sparse matrices are defined in the same way except that real and imaginary parts are splitted. It is as if two matrices were stored. There are therefore 6 arrays: 'real_ptr_', 'real_ind_', 'real_data_', 'imag_ptr_', 'imag_ind_' and 'imag_data_'.</dd></dl>
<dl class="user"><dt><b></b></dt><dd>Finally, since the matrix is symmetric, only its upper part is stored. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8hxx_source.php#l00049">49</a> of file <a class="el" href="_matrix___sym_complex_sparse_8hxx_source.php">Matrix_SymComplexSparse.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="af4fd673569b7398d84f2d4f277de50a8"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::Matrix_SymComplexSparse" ref="af4fd673569b7398d84f2d4f277de50a8" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>Builds an empty 0x0 matrix. </p>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l00038">38</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab055efcee796094368d74c476dd28221"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::Matrix_SymComplexSparse" ref="ab055efcee796094368d74c476dd28221" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Constructor. </p>
<p>Builds a i by j sparse matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>'j' is assumed to be equal to 'i' so that 'j' is discarded. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l00060">60</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a102b1c069e24ea2b907cefc593243ac4"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::Matrix_SymComplexSparse" ref="a102b1c069e24ea2b907cefc593243ac4" args="(int i, int j, int real_nz, int imag_nz)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>real_nz</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>imag_nz</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Constructor. </p>
<p>Builds a sparse matrix of size i by j , with real_nz non-zero (stored) elements in the real part of the matrix and imag_nz non-zero elements in the imaginary part of the matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_nz</em>&nbsp;</td><td>number of non-zero elements that are stored for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_nz</em>&nbsp;</td><td>number of non-zero elements that are stored for the imaginary part. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> values are not initialized. Indices of non-zero entries are not initialized either. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l00090">90</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a063b7c64394f13e35d04d87310cf552e"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::Matrix_SymComplexSparse" ref="a063b7c64394f13e35d04d87310cf552e" args="(int i, int j, Vector&lt; T, Storage0, Allocator0 &gt; &amp;real_values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;real_ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;real_ind, Vector&lt; T, Storage0, Allocator0 &gt; &amp;imag_values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;imag_ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;imag_ind)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Storage0 , class Allocator0 , class Storage1 , class Allocator1 , class Storage2 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>real_values</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>real_ptr</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>real_ind</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>imag_values</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>imag_ptr</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>imag_ind</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Constructor. </p>
<p>Builds a i by j sparse matrix with non-zero values and indices provided by 'real_values' (values of the real part), 'real_ptr' (pointers for the real part), 'real_ind' (indices for the real part), 'imag_values' (values of the imaginary part), 'imag_ptr' (pointers for the imaginary part) and 'imag_ind' (indices for the imaginary part). Input vectors are released and are empty on exit. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_values</em>&nbsp;</td><td>values of non-zero entries for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_ptr</em>&nbsp;</td><td>row or column start indices for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_ind</em>&nbsp;</td><td>row or column indices for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_values</em>&nbsp;</td><td>values of non-zero entries for the imaginary part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_ptr</em>&nbsp;</td><td>row or column start indices for the imaginary part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_ind</em>&nbsp;</td><td>row or column indices for the imaginary part. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Input vectors 'real_values', 'real_ptr' and 'real_ind', 'imag_values', 'imag_ptr' and 'imag_ind' are empty on exit. Moreover 'j' is assumed to be equal to 'i' so that 'j' is discarded. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l00131">131</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="aaf8bdd8dd7d3f089b38a6db2c18c4154"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::AddInteraction" ref="aaf8bdd8dd7d3f089b38a6db2c18c4154" args="(int i, int j, const complex&lt; T &gt; &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::AddInteraction </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const complex&lt; T &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Add a value to a non-zero entry. </p>
<p>This function adds <em>val</em> to the element (<em>i</em>, <em>j</em>), provided that this element is already a non-zero entry. Otherwise a non-zero entry is inserted equal to <em>val</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>value to be added to the element (<em>i</em>, <em>j</em>). </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02482">2482</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a77439fdc40537903681e7cd912c5f732"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::Clear" ref="a77439fdc40537903681e7cd912c5f732" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Clear </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the matrix. </p>
<p>This methods is equivalent to the destructor. On exit, the matrix is empty (0x0). </p>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l00444">444</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab53e3d0367145d47e83de9480ac29f54"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::Fill" ref="ab53e3d0367145d47e83de9480ac29f54" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Fill </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the non-zero entries with 0, 1, 2, ... </p>
<p>On exit, the non-zero entries are 0, 1, 2, 3, ... The order of the numbers depends on the storage. </p>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02581">2581</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7a7182bc0ef4972f4e124e37836bd06c"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::Fill" ref="a7a7182bc0ef4972f4e124e37836bd06c" args="(const complex&lt; T &gt; &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Fill </td>
          <td>(</td>
          <td class="paramtype">const complex&lt; T &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the non-zero entries with a given value. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>the value to set the non-zero entries to. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02597">2597</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="afda343754417cd33781e8bd3f69f07d4"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::FillRand" ref="afda343754417cd33781e8bd3f69f07d4" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::FillRand </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the non-zero entries randomly. </p>
<dl class="note"><dt><b>Note:</b></dt><dd>The random generator is very basic. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02612">2612</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af88748a55208349367d6860ad76fd691"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetAllocator" ref="af88748a55208349367d6860ad76fd691" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">Allocator &amp; <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetAllocator </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the allocator of the matrix. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The allocator. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00258">258</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a453a269dfe7fadba249064363d5ab92a"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetData" ref="a453a269dfe7fadba249064363d5ab92a" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer to the data array. </p>
<p>Returns a pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00208">208</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0f2796430deb08e565df8ced656097bf"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetDataConst" ref="a0f2796430deb08e565df8ced656097bf" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::const_pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConst </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a const pointer to the data array. </p>
<p>Returns a const pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00221">221</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5979450cd8801810229f4a24c36e6639"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetDataConstVoid" ref="a5979450cd8801810229f4a24c36e6639" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConstVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "const void*" to the data array. </p>
<p>Returns a pointer of type "const void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00247">247</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="afda1dbd968e2e4b3047c014ab5931846"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetDataSize" ref="afda1dbd968e2e4b3047c014ab5931846" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetDataSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored in memory. </p>
<p>Returns the number of elements stored in memory, i.e. the cumulated number of non-zero entries of both the real and the imaginary part. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements stored in memory. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l01762">1762</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a505cdfee34741c463e0dc1943337bedc"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetDataVoid" ref="a505cdfee34741c463e0dc1943337bedc" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "void*" to the data array. </p>
<p>Returns a pointer of type "void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00234">234</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2be1493a466aee2b75403551ddf42539"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetImag" ref="a2be1493a466aee2b75403551ddf42539" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &amp; <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the imaginary part of element (<em>i</em>, <em>j</em>) if it can be returned as a reference. If the non-zero entry does not exit, it is created </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02393">2393</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a14c8495abb3d6ef2841ceeb76a5f1149"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetImag" ref="a14c8495abb3d6ef2841ceeb76a5f1149" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &amp; <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the imaginary part of element (<em>i</em>, <em>j</em>) if it can be returned as a reference. If the non-zero entry does not exit, it is created </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02466">2466</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a411656148afd39425bd6df81a013d180"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetImagData" ref="a411656148afd39425bd6df81a013d180" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T * <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImagData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the array of values of the imaginary part. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The array 'imag_data_' of values of the imaginary part.. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l01947">1947</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="acd8debd721f3560c317babe090aa09e6"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetImagDataSize" ref="acd8debd721f3560c317babe090aa09e6" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImagDataSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the length of the array of (column or row) indices //! for the imaginary part. </p>
<p>Returns the length of the array ('ind_') of (row or column) indices of non-zero entries (that are stored) for the imaginary part. This array defines non-zero entries indices if coupled with (column or row) start indices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the array of (column or row) indices for the imaginary part. </dd></dl>
<dl class="note"><dt><b>Note:</b></dt><dd>The length of the array of (column or row) indices is the number of non-zero entries that are stored. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l01925">1925</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2bb61950179c75be2150cfae734bb05b"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetImagInd" ref="a2bb61950179c75be2150cfae734bb05b" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int * <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImagInd </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns (row or column) indices of non-zero entries for / the imaginary part. </p>
<p>Returns the array ('ind_') of (row or column) indices of non-zero entries for the imaginary part. This array defines non-zero entries indices if coupled with (column or row) start indices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The array of (row or column) indices of non-zero entries for the imaginary part. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l01821">1821</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af1c1a606649e03827a3710751238a5bc"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetImagIndSize" ref="af1c1a606649e03827a3710751238a5bc" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImagIndSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the length of the array of (column or row) indices //! for the imaginary part. </p>
<p>Returns the length of the array ('ind_') of (row or column) indices of non-zero entries (that are stored) for the imaginary part. This array defines non-zero entries indices if coupled with (column or row) start indices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the array of (column or row) indices for the imaginary part. </dd></dl>
<dl class="note"><dt><b>Note:</b></dt><dd>The length of the array of (column or row) indices is the number of non-zero entries that are stored. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l01885">1885</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad1bb43e80676e9ee420ea692ba06d0ac"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetImagPtr" ref="ad1bb43e80676e9ee420ea692ba06d0ac" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int * <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImagPtr </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns (row or column) start indices for the imaginary part. </p>
<p>Returns the array ('ptr_') of start indices for the imaginary part. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The array of start indices for the imaginary part. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l01788">1788</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="acde6ac68023f92f32eceb4f0a422580f"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetImagPtrSize" ref="acde6ac68023f92f32eceb4f0a422580f" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImagPtrSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the length of the array of start indices for the imaginary part. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the array of start indices for the imaginary part. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l01845">1845</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetM" ref="ab6f6c5bba065ca82dad7c3abdbc8ea79" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#aecf3e8c636dbb83335ea588afe2558a8">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00130">130</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a65e9c3f0db9c7c8c3fa10ead777dd927"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetM" ref="a65e9c3f0db9c7c8c3fa10ead777dd927" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a8c781e99310aae01786eac0e8e5aa50c">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00107">107</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7d27412bc9384a5ce0c0db1ee310d31c"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetN" ref="a7d27412bc9384a5ce0c0db1ee310d31c" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a867066f7bf531bf7ad15bdcd034dc3c0">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00145">145</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6b134070d1b890ba6b7eb72fee170984"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetN" ref="a6b134070d1b890ba6b7eb72fee170984" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a40580a92202044416e379c1304ff0220">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00118">118</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1f8cbc14fed12f61ebc5a2bc7085c619"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetReal" ref="a1f8cbc14fed12f61ebc5a2bc7085c619" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &amp; <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the real part of element (<em>i</em>, <em>j</em>) if it can be returned as a reference. If the non-zero entry does not exit, it is created </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02303">2303</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a82d4964b6d1049d2a3aa8e861fe7c9c3"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetReal" ref="a82d4964b6d1049d2a3aa8e861fe7c9c3" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &amp; <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the real part of element (<em>i</em>, <em>j</em>) if it can be returned as a reference. If the non-zero entry does not exit, it is created </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02376">2376</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1a98f711de4b7daf42ea42c8493ca08d"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetRealData" ref="a1a98f711de4b7daf42ea42c8493ca08d" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T * <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetRealData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the array of values of the real part. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The array 'real_data_' of values of the real part.. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l01936">1936</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad1304eb0b4e7197e603e95b119f24796"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetRealDataSize" ref="ad1304eb0b4e7197e603e95b119f24796" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetRealDataSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the length of the array of (column or row) indices for //! the real part. </p>
<p>Returns the length of the array ('ind_') of (row or column) indices of non-zero entries (that are stored) for the real part. This array defines non-zero entries indices if coupled with (column or row) start indices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the array of (column or row) indices for the real part. </dd></dl>
<dl class="note"><dt><b>Note:</b></dt><dd>The length of the array of (column or row) indices is the number of non-zero entries that are stored. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l01905">1905</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a72f50ab4230ffe4af23c8bbf2203ad0b"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetRealInd" ref="a72f50ab4230ffe4af23c8bbf2203ad0b" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int * <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetRealInd </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns (row or column) indices of non-zero entries for the real part. </p>
<p>Returns the array ('ind_') of (row or column) indices of non-zero entries for the real part. This array defines non-zero entries indices if coupled with (column or row) start indices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The array of (row or column) indices of non-zero entries for the real part. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l01804">1804</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="acdb80353a65a45dfc11b886e7c75a807"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetRealIndSize" ref="acdb80353a65a45dfc11b886e7c75a807" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetRealIndSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the length of the array of (column or row) indices for //! the real part. </p>
<p>Returns the length of the array ('ind_') of (row or column) indices of non-zero entries (that are stored) for the real part. This array defines non-zero entries indices if coupled with (column or row) start indices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the array of (column or row) indices for the real part. </dd></dl>
<dl class="note"><dt><b>Note:</b></dt><dd>The length of the array of (column or row) indices is the number of non-zero entries that are stored. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l01865">1865</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a005a3b7389aecf38482cc16e4984787c"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetRealPtr" ref="a005a3b7389aecf38482cc16e4984787c" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int * <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetRealPtr </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns (row or column) start indices for the real part. </p>
<p>Returns the array ('ptr_') of start indices for the real part. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The array of start indices for the real part. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l01775">1775</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a913aadd75d1ed2e076ea4b014e5fb32d"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetRealPtrSize" ref="a913aadd75d1ed2e076ea4b014e5fb32d" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetRealPtrSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the length of the array of start indices for the real part. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the array of start indices for the real part. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l01833">1833</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0fbc3f7030583174eaa69429f655a1b0"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::GetSize" ref="a0fbc3f7030583174eaa69429f655a1b0" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements in the matrix. </p>
<p>Returns the number of elements in the matrix, i.e. the number of rows multiplied by the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements in the matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00195">195</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af9adb37bee7561a855fef31642b85bfd"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::Nullify" ref="af9adb37bee7561a855fef31642b85bfd" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Nullify </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the matrix without releasing memory. </p>
<p>On exit, the matrix is empty and the memory has not been released. It is useful for low level manipulations on a <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> instance. </p>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l00684">684</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad441174c77a9c101560e77794aae6369"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::operator()" ref="ad441174c77a9c101560e77794aae6369" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">complex&lt; typename <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &gt; <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l01969">1969</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aba07f35ba600bf49197e1b1802f95c67"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::operator=" ref="aba07f35ba600bf49197e1b1802f95c67" args="(const Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop, class Storage, class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a matrix (assignment operator). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>A</em>&nbsp;</td><td>matrix to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'A' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02519">2519</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="abe86d6545d4aeca07257de2d8e7a85f2"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::Print" ref="abe86d6545d4aeca07257de2d8e7a85f2" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Print </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Displays the matrix on the standard output. </p>
<p>Displays elements on the standard output, in text format. Each row is displayed on a single line and elements of a row are delimited by tabulations. </p>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02630">2630</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa70fdb81ecca409e7410b7bd91c71bc8"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::Read" ref="aa70fdb81ecca409e7410b7bd91c71bc8" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from a file. </p>
<p>Reads a matrix stored in binary format in a file. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>input file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02774">2774</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a58a6efaa3b49592962e9f3d50963ba62"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::Read" ref="a58a6efaa3b49592962e9f3d50963ba62" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from an input stream. </p>
<p>Reads a matrix in binary format from an input stream. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>input stream </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02799">2799</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2be8c4d3b6d3f794ee5c6b7cba504571"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::ReadText" ref="a2be8c4d3b6d3f794ee5c6b7cba504571" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from an input stream. </p>
<p>Reads a matrix from a stream in text format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>input stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02871">2871</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae5f0883621073e283ebd9ad047e81174"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::ReadText" ref="ae5f0883621073e283ebd9ad047e81174" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from a file. </p>
<p>Reads the matrix from a file in text format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>input file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02846">2846</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6537ad73d443b7b1e7008f626593811e"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::Resize" ref="a6537ad73d443b7b1e7008f626593811e" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Resize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates memory to resize the matrix and keeps previous entries. </p>
<p>On exit, the matrix is a i x j matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>new number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>new number of columns. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l01099">1099</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad2ef28f874e37f257af097c6ec306dfd"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::Resize" ref="ad2ef28f874e37f257af097c6ec306dfd" args="(int i, int j, int real_nz, int imag_nz)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Resize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>real_nz</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>imag_nz</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates memory to resize the matrix and keeps previous entries. </p>
<p>On exit, the matrix is a i x j matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>new number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>new number of columns. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_nz</em>&nbsp;</td><td>number of non-zero elements in the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_nz</em>&nbsp;</td><td>number of non-zero elements in the imaginary part. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l01118">1118</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab4c3a8968e2c79248fc7d1add27f3433"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::Set" ref="ab4c3a8968e2c79248fc7d1add27f3433" args="(int i, int j, const complex&lt; T &gt; &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Set </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const complex&lt; T &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets an element (i, j) to a value. </p>
<p>This function sets <em>val</em> to the element (<em>i</em>, <em>j</em>) </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>A(i, j) = val </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02503">2503</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a288b0c3b764e1db7eff81cb5e56e7c63"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::SetData" ref="a288b0c3b764e1db7eff81cb5e56e7c63" args="(int i, int j, int real_nz, typename Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;::pointer real_values, int *real_ptr, int *real_ind, int imag_nz, typename Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;::pointer imag_values, int *imag_ptr, int *imag_ind)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop, class Storage, class Allocator = SELDON_DEFAULT_ALLOCATOR&lt;T&gt;&gt; </div>
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::SetData </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>real_nz</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">typename <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::pointer&nbsp;</td>
          <td class="paramname"> <em>real_values</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int *&nbsp;</td>
          <td class="paramname"> <em>real_ptr</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int *&nbsp;</td>
          <td class="paramname"> <em>real_ind</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>imag_nz</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">typename <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::pointer&nbsp;</td>
          <td class="paramname"> <em>imag_values</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int *&nbsp;</td>
          <td class="paramname"> <em>imag_ptr</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int *&nbsp;</td>
          <td class="paramname"> <em>imag_ind</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Redefines the matrix. </p>
<p>It clears the matrix and sets it to a new matrix defined by arrays 'real_values' (values of the real part), 'real_ptr' (pointers for the real part), 'real_ind' (indices for the real part), 'imag_values' (values of the imaginary part), 'imag_ptr' (pointers for the imaginary part) and 'imag_ind' (indices for the imaginary part). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_nz</em>&nbsp;</td><td>number of non-zero entries (real part). </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_values</em>&nbsp;</td><td>values of non-zero entries for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_ptr</em>&nbsp;</td><td>row or column start indices for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_ind</em>&nbsp;</td><td>row or column indices for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_nz</em>&nbsp;</td><td>number of non-zero entries (imaginary part). </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_values</em>&nbsp;</td><td>values of non-zero entries for the imaginary part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_ptr</em>&nbsp;</td><td>row or column start indices for the imaginary part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_ind</em>&nbsp;</td><td>row or column indices for the imaginary part. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>On exit, arrays 'real_values', 'real_ptr', 'real_ind', 'imag_values', 'imag_ptr' and 'imag_ind' are managed by the matrix. For example, it means that the destructor will release those arrays; therefore, the user mustn't release those arrays. Moreover 'j' is assumed to be equal to 'i' so that 'j' is discarded. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l00653">653</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3f202fe96b9289912e1cb6acfe24f18b"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::SetData" ref="a3f202fe96b9289912e1cb6acfe24f18b" args="(int i, int j, Vector&lt; T, Storage0, Allocator0 &gt; &amp;real_values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;real_ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;real_ind, Vector&lt; T, Storage0, Allocator0 &gt; &amp;imag_values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;imag_ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;imag_ind)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Storage0 , class Allocator0 , class Storage1 , class Allocator1 , class Storage2 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::SetData </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>real_values</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>real_ptr</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>real_ind</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>imag_values</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>imag_ptr</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>imag_ind</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Redefines the matrix. </p>
<p>It clears the matrix and sets it to a new matrix defined by 'real_values' (values of the real part), 'real_ptr' (pointers for the real part), 'real_ind' (indices for the real part), 'imag_values' (values of the imaginary part), 'imag_ptr' (pointers for the imaginary part) and 'imag_ind' (indices for the imaginary part). Input vectors are released and are empty on exit. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_values</em>&nbsp;</td><td>values of non-zero entries for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_ptr</em>&nbsp;</td><td>row or column start indices for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_ind</em>&nbsp;</td><td>row or column indices for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_values</em>&nbsp;</td><td>values of non-zero entries for the imaginary part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_ptr</em>&nbsp;</td><td>row or column start indices for the imaginary part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_ind</em>&nbsp;</td><td>row or column indices for the imaginary part. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Input vectors 'real_values', 'real_ptr' and 'real_ind', 'imag_values', 'imag_ptr' and 'imag_ind' are empty on exit. Moreover 'j' is assumed to be equal to 'i' so that 'j' is discarded. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l00479">479</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2116e2afc727a7c2ecf920fe41811c97"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::SetIdentity" ref="a2116e2afc727a7c2ecf920fe41811c97" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::SetIdentity </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the matrix to identity. </p>
<p>This method fills the diagonal of the matrix with ones. </p>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02549">2549</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a4cf220882aeebe97ab0cd8afb1eb52c7"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::ValImag" ref="a4cf220882aeebe97ab0cd8afb1eb52c7" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &amp; <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ValImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the imaginary part of element (<em>i</em>, <em>j</em>) if it can be returned as a reference. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. </dd></dl>
<dl><dt><b>Exceptions:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em><a class="el" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a></em>&nbsp;</td><td>No reference can be returned because the element is a zero entry (not stored in the matrix). </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02184">2184</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6243d988622a10dec95e8733710ef179"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::ValImag" ref="a6243d988622a10dec95e8733710ef179" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &amp; <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ValImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the imaginary part of element (<em>i</em>, <em>j</em>) if it can be returned as a reference. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. </dd></dl>
<dl><dt><b>Exceptions:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em><a class="el" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a></em>&nbsp;</td><td>No reference can be returned because the element is a zero entry (not stored in the matrix). </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02244">2244</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="abec4edb809f2be635bff90b508f9e9f6"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::ValReal" ref="abec4edb809f2be635bff90b508f9e9f6" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &amp; <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ValReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the real value of element (<em>i</em>, <em>j</em>) if it can be returned as a reference. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. </dd></dl>
<dl><dt><b>Exceptions:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em><a class="el" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a></em>&nbsp;</td><td>No reference can be returned because the element is a zero entry (not stored in the matrix). </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02124">2124</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0e58d558d63fc51d62db121f9732cf1c"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::ValReal" ref="a0e58d558d63fc51d62db121f9732cf1c" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &amp; <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ValReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the real value of element (<em>i</em>, <em>j</em>) if it can be returned as a reference. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. </dd></dl>
<dl><dt><b>Exceptions:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em><a class="el" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a></em>&nbsp;</td><td>No reference can be returned because the element is a zero entry (not stored in the matrix). </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02064">2064</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a8c8179698d7eccc69911fcbb21262d9b"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::Write" ref="a8c8179698d7eccc69911fcbb21262d9b" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix in a file. </p>
<p>Stores the matrix in a file in binary format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02648">2648</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a01e5bc999ba6caed5db79815ebfe549e"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::Write" ref="a01e5bc999ba6caed5db79815ebfe549e" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix to an output stream. </p>
<p>Stores the matrix in an output stream in binary format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02673">2673</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6dc1206f68ef04a7bb6ac40b2e9f7e59"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::WriteText" ref="a6dc1206f68ef04a7bb6ac40b2e9f7e59" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix to an output stream. </p>
<p>Stores the matrix in a file in ascii format. The entries are written in coordinate format (row column value) 1-index convention is used </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02743">2743</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1c9015515a2e218c875d739bd9f410e1"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::WriteText" ref="a1c9015515a2e218c875d739bd9f410e1" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix in a file. </p>
<p>Stores the matrix in a file in ascii format. The entries are written in coordinate format (row column value) 1-index convention is used </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02716">2716</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a55f35dad8db11e522dde99f236472d9d"></a><!-- doxytag: member="Seldon::Matrix_SymComplexSparse::Zero" ref="a55f35dad8db11e522dde99f236472d9d" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Zero </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Resets all non-zero entries to 0-value. </p>
<p>The sparsity pattern remains unchanged. </p>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l02535">2535</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>matrix_sparse/<a class="el" href="_matrix___sym_complex_sparse_8hxx_source.php">Matrix_SymComplexSparse.hxx</a></li>
<li>matrix_sparse/<a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
