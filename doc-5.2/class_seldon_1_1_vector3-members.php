<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>, including all inherited members.<table>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a0e178a2f8fad92f0929f4ea1b1dd1604">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a173d2e7734fb3098785523c63488a577">Clear</a>(int i)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a3a1c6051634fc7ec05f011779b855f5e">Clear</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>data_</b> (defined in <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a0d6388a07fda225152eef87cdab2ab5f">Fill</a>(const T &amp;x)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a1f70de4b1edeb1a64078ad3113014a8e">Flatten</a>(Vector&lt; Td, VectFull, Allocatord &gt; &amp;data) const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a84d542b134e8f98e944932b154c1d48d">Flatten</a>(int beg, int end, Vector&lt; Td, VectFull, Allocatord &gt; &amp;data) const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a84310d341a922124b1b6c9f5a541e998">Flatten</a>(int beg0, int end0, int beg1, int end1, Vector&lt; Td, VectFull, Allocatord &gt; &amp;data) const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a4a694febe8dba0a1203fd628cd1e3167">GetLength</a>() const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a89eac88f9d58edbf81884b2f8645219c">GetLength</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a51d4b3778ff8220b1c80377852826d7b">GetLength</a>(int i, int j) const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a497379d28c87524052ee202702db9c91">GetNelement</a>() const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#ab83e9c4434cdc9aeda454890ae557662">GetNelement</a>(int beg, int end) const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a0d7115fd1e3802ad9417a97f0a1fcad3">GetNelement</a>(int beg0, int end0, int beg1, int end1) const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a5772120ddd32015c71c56ac2e44413e0">GetShape</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a2520f6f6ecf4d5853eaefe38904a4fc7">GetShape</a>(int i, Vector&lt; int &gt; &amp;shape) const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a66946f1764cc14abec8a51d4e310f6b5">GetSize</a>() const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a6b29a36baf352da31f2a97ec972fd761">GetSize</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a81b5f3374df5a7f46ff50d60840eb81b">GetSize</a>(int i, int j) const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17">GetVector</a>()</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#acc38f6e8f4660dc1c98999b08a467454">GetVector</a>() const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a356bbfa79f98b46d264aaf80def94eb0">GetVector</a>(int i)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a0d684b2dc3e14d471e6af816d3d11e36">GetVector</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a1770eeb944cb8eb3d8600a9e5fdba6b5">GetVector</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#ad5fd1f0b6dac7bd14faf617633a3a364">GetVector</a>(int i, int j) const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#aaacba77cabdd3d80b2809778ee76206b">operator()</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a3cc0d7b9283c1069cee2770860c39133">operator()</a>(int i)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a1298a6ec16e6a70ce7de2e456a0c56aa">operator()</a>(int i, int j) const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#ac1ba1e37b274b6a1522a5db89b1e5c7a">operator()</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#afc9bb303c83fd61af66cb9f2b7ea9842">operator()</a>(int i, int j, int k) const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#ad67a55c6828c310590dae6b671ff2d07">operator()</a>(int i, int j, int k)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a9b3c87591141123ac43759d6b9ca9531">Print</a>() const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd">PushBack</a>(int i, int j, const T &amp;x)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a71fd6304ae029838b323c8092a2a109f">PushBack</a>(int i, const Vector&lt; T, Vect_Full, Allocator0 &gt; &amp;X)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a99bb83b050637e5e1d2aaedd540a33f4">PushBack</a>(const Vector&lt; Vector&lt; T, Vect_Full, Allocator0 &gt;, Vect_Full, Allocator1 &gt; &amp;X)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#aabfc0ec2c12884196ebe4ce2ba4638da">PushBack</a>(const Vector&lt; Vector&lt; Vector&lt; T, Vect_Full, Allocator0 &gt;, Vect_Full, Allocator1 &gt;, Vect_Full, Allocator2 &gt; &amp;X)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a96ad43bc29d35bf3a1b1bffc507d8d46">PushBack</a>(const Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt; &amp;X)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#ac28c5fe01b2ac97d21e560e9282444b5">Read</a>(string file_name, bool with_size=true)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a96c8cfd79eaa900690893540509562f6">Read</a>(istream &amp;file_stream, bool with_size=true)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a21e22b8d942820eb4c60e3e34c2afcd6">Reallocate</a>(int N)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a5d5b76dbcaa0a445f089f2bdf341aba5">Reallocate</a>(int i, int N)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a67835c87beb5c2539ffe8af0d4a0b49c">Reallocate</a>(int i, int j, int N)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>value_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a3425eec2b0400057adeda33a005a188d">Vector3</a>()</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a8384e50e394357e4b9a6c0699f39f241">Vector3</a>(int)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#ae42d9b0d42feb5459c7efea52c2f47ac">Vector3</a>(Vector&lt; int &gt; &amp;length)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a9fff1c8ce72eb5dc5c002b6f1c0e7b86">Vector3</a>(Vector&lt; Vector&lt; int &gt;, Vect_Full, Allocator &gt; &amp;length)</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#ad0372ca6e3d3dbee249dddd5eb4c4363">Write</a>(string file_name, bool with_size=true) const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#a05d48fe1fd82579d1138e9677852ccca">Write</a>(ostream &amp;file_stream, bool with_size=true) const </td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector3.php#ae85c3b686a4100a5c05dc7d82a4b6b80">~Vector3</a>()</td><td><a class="el" href="class_seldon_1_1_vector3.php">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a></td><td></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
