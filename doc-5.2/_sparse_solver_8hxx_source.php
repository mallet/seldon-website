<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/solver/SparseSolver.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2011 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2010 Vivien Mallet</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_COMPUTATION_SPARSESOLVER_HXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span><span class="preprocessor">#define SELDON_FILE_COMPUTATION_SPARSESOLVER_HXX</span>
<a name="l00023"></a>00023 <span class="preprocessor"></span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="preprocessor">#include &quot;Ordering.hxx&quot;</span>
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 <span class="keyword">namespace </span>Seldon
<a name="l00029"></a>00029 {
<a name="l00030"></a>00030 
<a name="l00032"></a>00032   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator = <span class="keywordtype">SEL</span>DON_DEFAULT_ALLOCATOR&lt;T&gt; &gt;
<a name="l00033"></a><a class="code" href="class_seldon_1_1_sparse_seldon_solver.php">00033</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_sparse_seldon_solver.php" title="Default solver in Seldon.">SparseSeldonSolver</a>
<a name="l00034"></a>00034   {
<a name="l00035"></a>00035   <span class="keyword">protected</span> :
<a name="l00037"></a><a class="code" href="class_seldon_1_1_sparse_seldon_solver.php#a0f888b5b1b83a88c9aa649c2c5616376">00037</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_seldon_solver.php#a0f888b5b1b83a88c9aa649c2c5616376" title="Verbosity level.">print_level</a>;
<a name="l00039"></a><a class="code" href="class_seldon_1_1_sparse_seldon_solver.php#a5eec7564bd3006dfcb5aba246dff43f6">00039</a>     <span class="keywordtype">double</span> <a class="code" href="class_seldon_1_1_sparse_seldon_solver.php#a5eec7564bd3006dfcb5aba246dff43f6" title="Threshold for pivoting.">permtol</a>;
<a name="l00041"></a><a class="code" href="class_seldon_1_1_sparse_seldon_solver.php#aef351e01ca8c7a91d17dcb25a1db0ad2">00041</a>     <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Symmetric, ArrayRowSymSparse, Allocator&gt;</a> <a class="code" href="class_seldon_1_1_sparse_seldon_solver.php#aef351e01ca8c7a91d17dcb25a1db0ad2" title="Symmetric matrix.">mat_sym</a>;
<a name="l00043"></a><a class="code" href="class_seldon_1_1_sparse_seldon_solver.php#ac431fb87cdbab5b904cab7af439aec42">00043</a>     <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, General, ArrayRowSparse, Allocator&gt;</a> <a class="code" href="class_seldon_1_1_sparse_seldon_solver.php#ac431fb87cdbab5b904cab7af439aec42" title="Unsymmetric matrix.">mat_unsym</a>;
<a name="l00045"></a><a class="code" href="class_seldon_1_1_sparse_seldon_solver.php#a98d9ac99260854c88001033c27003789">00045</a>     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> <a class="code" href="class_seldon_1_1_sparse_seldon_solver.php#a98d9ac99260854c88001033c27003789" title="Permutation arrays.">permutation_row</a>, permutation_col;
<a name="l00047"></a><a class="code" href="class_seldon_1_1_sparse_seldon_solver.php#af90804792d5142a687a6f950ddad325b">00047</a>     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> <a class="code" href="class_seldon_1_1_sparse_seldon_solver.php#af90804792d5142a687a6f950ddad325b" title="Temporary vector.">xtmp</a>;
<a name="l00049"></a><a class="code" href="class_seldon_1_1_sparse_seldon_solver.php#abe4e7e238d8d5e74265a70f46e23c88b">00049</a>     <span class="keywordtype">bool</span> <a class="code" href="class_seldon_1_1_sparse_seldon_solver.php#abe4e7e238d8d5e74265a70f46e23c88b" title="Is the factorization contained in &amp;quot;mat_sym&amp;quot;?">symmetric_matrix</a>;
<a name="l00050"></a>00050 
<a name="l00051"></a>00051   <span class="keyword">public</span> :
<a name="l00052"></a>00052 
<a name="l00053"></a>00053     <a class="code" href="class_seldon_1_1_sparse_seldon_solver.php" title="Default solver in Seldon.">SparseSeldonSolver</a>();
<a name="l00054"></a>00054 
<a name="l00055"></a>00055     <span class="keywordtype">void</span> Clear();
<a name="l00056"></a>00056 
<a name="l00057"></a>00057     <span class="keywordtype">void</span> HideMessages();
<a name="l00058"></a>00058     <span class="keywordtype">void</span> ShowMessages();
<a name="l00059"></a>00059 
<a name="l00060"></a>00060     <span class="keywordtype">double</span> GetPivotThreshold() <span class="keyword">const</span>;
<a name="l00061"></a>00061     <span class="keywordtype">void</span> SetPivotThreshold(<span class="keyword">const</span> <span class="keywordtype">double</span>&amp;);
<a name="l00062"></a>00062 
<a name="l00063"></a>00063     <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Storage0, <span class="keyword">class</span> Allocator0&gt;
<a name="l00064"></a>00064     <span class="keywordtype">void</span> FactorizeMatrix(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; perm,
<a name="l00065"></a>00065                          <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, General, Storage0, Allocator0&gt;</a>&amp; mat,
<a name="l00066"></a>00066                          <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>);
<a name="l00067"></a>00067 
<a name="l00068"></a>00068     <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Storage0, <span class="keyword">class</span> Allocator0&gt;
<a name="l00069"></a>00069     <span class="keywordtype">void</span> FactorizeMatrix(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; perm,
<a name="l00070"></a>00070                          <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Symmetric, Storage0, Allocator0&gt;</a>&amp; mat,
<a name="l00071"></a>00071                          <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>);
<a name="l00072"></a>00072 
<a name="l00073"></a>00073     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Vector1&gt;
<a name="l00074"></a>00074     <span class="keywordtype">void</span> Solve(Vector1&amp; z);
<a name="l00075"></a>00075 
<a name="l00076"></a>00076     <span class="keyword">template</span>&lt;<span class="keyword">class</span> TransStatus, <span class="keyword">class</span> Vector1&gt;
<a name="l00077"></a>00077     <span class="keywordtype">void</span> Solve(<span class="keyword">const</span> TransStatus&amp; TransA, Vector1&amp; z);
<a name="l00078"></a>00078 
<a name="l00079"></a>00079   };
<a name="l00080"></a>00080 
<a name="l00081"></a>00081 
<a name="l00083"></a>00083   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00084"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php">00084</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_sparse_direct_solver.php" title="Class grouping different direct solvers.">SparseDirectSolver</a>
<a name="l00085"></a>00085   {
<a name="l00086"></a>00086   <span class="keyword">protected</span> :
<a name="l00088"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ae9819b726220aa9599132e149a6416e6">00088</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ae9819b726220aa9599132e149a6416e6" title="Ordering to use.">type_ordering</a>;
<a name="l00090"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6">00090</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a>;
<a name="l00092"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a2b4fcbacb7f6fb5276fe33b36501e99c">00092</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a2b4fcbacb7f6fb5276fe33b36501e99c" title="Number of threads (for Pastix).">number_threads_per_node</a>;
<a name="l00094"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a4e946689a26b4103f0551bd2e6cf96eb">00094</a>     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a4e946689a26b4103f0551bd2e6cf96eb" title="Ordering (if supplied by the user).">permut</a>;
<a name="l00096"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#af37f390e9852c82c8eb3659abbdff7a4">00096</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#af37f390e9852c82c8eb3659abbdff7a4" title="Size of factorized linear system.">n</a>;
<a name="l00097"></a>00097 
<a name="l00098"></a>00098 <span class="preprocessor">#ifdef SELDON_WITH_UMFPACK</span>
<a name="l00099"></a>00099 <span class="preprocessor"></span>
<a name="l00100"></a>00100     <a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack&lt;T&gt;</a> mat_umf;
<a name="l00101"></a>00101 <span class="preprocessor">#endif</span>
<a name="l00102"></a>00102 <span class="preprocessor"></span><span class="preprocessor">#ifdef SELDON_WITH_SUPERLU</span>
<a name="l00103"></a>00103 <span class="preprocessor"></span>
<a name="l00104"></a>00104     <a class="code" href="class_seldon_1_1_matrix_super_l_u.php" title="empty matrix">MatrixSuperLU&lt;T&gt;</a> mat_superlu;
<a name="l00105"></a>00105 <span class="preprocessor">#endif</span>
<a name="l00106"></a>00106 <span class="preprocessor"></span><span class="preprocessor">#ifdef SELDON_WITH_MUMPS</span>
<a name="l00107"></a>00107 <span class="preprocessor"></span>
<a name="l00108"></a>00108     <a class="code" href="class_seldon_1_1_matrix_mumps.php" title="object used to solve linear system by calling mumps subroutines">MatrixMumps&lt;T&gt;</a> mat_mumps;
<a name="l00109"></a>00109 <span class="preprocessor">#endif</span>
<a name="l00110"></a>00110 <span class="preprocessor"></span><span class="preprocessor">#ifdef SELDON_WITH_PASTIX</span>
<a name="l00111"></a>00111 <span class="preprocessor"></span>
<a name="l00112"></a>00112     <a class="code" href="class_seldon_1_1_matrix_pastix.php">MatrixPastix&lt;T&gt;</a> mat_pastix;
<a name="l00113"></a>00113 <span class="preprocessor">#endif</span>
<a name="l00114"></a>00114 <span class="preprocessor"></span>
<a name="l00115"></a>00115 <span class="preprocessor">#ifdef SELDON_WITH_PRECONDITIONING</span>
<a name="l00116"></a>00116 <span class="preprocessor"></span>
<a name="l00117"></a>00117     <a class="code" href="class_seldon_1_1_ilut_preconditioning.php">IlutPreconditioning&lt;double, T, NewAlloc&lt;T&gt;</a> &gt; mat_ilut;
<a name="l00118"></a>00118 <span class="preprocessor">#endif</span>
<a name="l00119"></a>00119 <span class="preprocessor"></span>
<a name="l00121"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a215ef20ce0827e0974cade309a9aaced">00121</a>     <span class="keywordtype">double</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a215ef20ce0827e0974cade309a9aaced" title="Threshold for ilut solver.">threshold_matrix</a>;
<a name="l00123"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a67010f0cc1e81d66504f3987385fb5f7">00123</a>     <span class="keywordtype">bool</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a67010f0cc1e81d66504f3987385fb5f7" title="Use of non-symmetric ilut?">enforce_unsym_ilut</a>;
<a name="l00124"></a>00124 
<a name="l00126"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a18d4ec1c11f680b7a364757063bde92d">00126</a>     <a class="code" href="class_seldon_1_1_sparse_seldon_solver.php">SparseSeldonSolver&lt;T&gt;</a> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a18d4ec1c11f680b7a364757063bde92d" title="Default solver.">mat_seldon</a>;
<a name="l00127"></a>00127 
<a name="l00128"></a>00128   <span class="keyword">public</span> :
<a name="l00129"></a>00129     <span class="comment">// Available solvers.</span>
<a name="l00130"></a>00130     <span class="keyword">enum</span> {SELDON_SOLVER, UMFPACK, SUPERLU, MUMPS, PASTIX, ILUT};
<a name="l00131"></a>00131     <span class="comment">// Error codes.</span>
<a name="l00132"></a>00132     <span class="keyword">enum</span> {FACTO_OK, STRUCTURALLY_SINGULAR_MATRIX,
<a name="l00133"></a>00133           NUMERICALLY_SINGULAR_MATRIX, OUT_OF_MEMORY, INVALID_ARGUMENT,
<a name="l00134"></a>00134           INCORRECT_NUMBER_OF_ROWS, MATRIX_INDICES_INCORRECT,
<a name="l00135"></a>00135           INVALID_PERMUTATION, ORDERING_FAILED, INTERNAL_ERROR};
<a name="l00136"></a>00136 
<a name="l00137"></a>00137     <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#aef26a5c7fcbe65069bca7967c73f7ab5" title="Default constructor.">SparseDirectSolver</a>();
<a name="l00138"></a>00138 
<a name="l00139"></a>00139     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a4dc8a27fc02a2bc978629f6630f319cd" title="Hiding all messages.">HideMessages</a>();
<a name="l00140"></a>00140     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ad4b15cbf1fc0faec2354e0beaa4678eb" title="Displaying basic messages.">ShowMessages</a>();
<a name="l00141"></a>00141     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a70a07b6d4ada2aa3b63e71edb71675a6" title="Displaying all the messages.">ShowFullHistory</a>();
<a name="l00142"></a>00142 
<a name="l00143"></a>00143     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a1da1c40d9433a58bcef7dec63695a9f9" title="Clearing factorization.">Clear</a>();
<a name="l00144"></a>00144 
<a name="l00145"></a>00145     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a72ddb935a03d6e507f4f0175d748c54d" title="Returns the number of rows of the factorized matrix.">GetM</a>() <span class="keyword">const</span>;
<a name="l00146"></a>00146     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#aa00bcb4a22c9ce619e88b4e1247023fc" title="Returns the number of rows of the factorized matrix.">GetN</a>() <span class="keyword">const</span>;
<a name="l00147"></a>00147 
<a name="l00148"></a>00148     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ab706f04923d61bd5316dd65d5c5a4286" title="Returns the ordering algorithm to use.">GetTypeOrdering</a>() <span class="keyword">const</span>;
<a name="l00149"></a>00149     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a42cd77074d9e29b54946cc2e99826f71" title="Sets directly the new ordering (by giving a permutation vector).">SetPermutation</a>(<span class="keyword">const</span> IVect&amp;);
<a name="l00150"></a>00150     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#adc649792b3da7574a6340563a9bc7378" title="Modifies the ordering algorithm to use.">SelectOrdering</a>(<span class="keywordtype">int</span>);
<a name="l00151"></a>00151 
<a name="l00152"></a>00152     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ad1d61abb463cc28f50e0acce15a41ce7" title="Modifies the number of threads per node (for Pastix only).">SetNumberThreadPerNode</a>(<span class="keywordtype">int</span> m);
<a name="l00153"></a>00153 
<a name="l00154"></a>00154     <span class="keyword">template</span>&lt;<span class="keyword">class</span> MatrixSparse&gt;
<a name="l00155"></a>00155     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a2283e241ce581a0571761db3ff7b1498" title="Computation of the permutation vector in order to reduce fill-in.">ComputeOrdering</a>(MatrixSparse&amp; A);
<a name="l00156"></a>00156 
<a name="l00157"></a>00157     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#aee5b1ccbb25a3dfb21a6ea3ed00b037e" title="Modifies the direct solver to use.">SelectDirectSolver</a>(<span class="keywordtype">int</span>);
<a name="l00158"></a>00158     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a706d99d20b79c014b676cd91074860ee" title="Enforces the use of unsymmetric algorithm for ilut solver.">SetNonSymmetricIlut</a>();
<a name="l00159"></a>00159 
<a name="l00160"></a>00160     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a260c50fc0c60a95d6ee6636ff948c3a8" title="Returns the direct solver to use.">GetDirectSolver</a>();
<a name="l00161"></a>00161 
<a name="l00162"></a>00162     <span class="keywordtype">double</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a58aa5d9084aa9dd43ea66bf84c4d920a" title="Returns threshold used for ilut (if this solver is selected).">GetThresholdMatrix</a>() <span class="keyword">const</span>;
<a name="l00163"></a>00163 
<a name="l00164"></a>00164     <span class="keyword">template</span>&lt;<span class="keyword">class</span> MatrixSparse&gt;
<a name="l00165"></a>00165     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#afba79fbe3b2e117a3235e9801db9e9f4" title="Factorization of matrix A.">Factorize</a>(MatrixSparse&amp; A, <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>);
<a name="l00166"></a>00166 
<a name="l00167"></a>00167     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#abee36e87c1e23539fc099e871a7bf6d3" title="Returns error code of the direct solver (for Mumps only).">GetInfoFactorization</a>(<span class="keywordtype">int</span>&amp; ierr) <span class="keyword">const</span>;
<a name="l00168"></a>00168 
<a name="l00169"></a>00169     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Vector1&gt;
<a name="l00170"></a>00170     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a63f7c474a05a6034c08ab44af925f55b" title="x_solution is overwritten by solution of A x = b.">Solve</a>(Vector1&amp; x);
<a name="l00171"></a>00171 
<a name="l00172"></a>00172     <span class="keyword">template</span>&lt;<span class="keyword">class</span> TransStatus, <span class="keyword">class</span> Vector1&gt;
<a name="l00173"></a>00173     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a63f7c474a05a6034c08ab44af925f55b" title="x_solution is overwritten by solution of A x = b.">Solve</a>(<span class="keyword">const</span> TransStatus&amp; TransA, Vector1&amp; x);
<a name="l00174"></a>00174 
<a name="l00175"></a>00175 <span class="preprocessor">#ifdef SELDON_WITH_MPI</span>
<a name="l00176"></a>00176 <span class="preprocessor"></span>    <span class="keyword">template</span>&lt;<span class="keyword">class</span> T<span class="keywordtype">int</span>&gt;
<a name="l00177"></a>00177     <span class="keywordtype">void</span> FactorizeDistributed(MPI::Comm&amp; comm_facto,
<a name="l00178"></a>00178                               Vector&lt;Tint&gt;&amp; Ptr, Vector&lt;Tint&gt;&amp; IndRow,
<a name="l00179"></a>00179                               Vector&lt;T&gt;&amp; Val, <span class="keyword">const</span> IVect&amp; glob_num,
<a name="l00180"></a>00180                               <span class="keywordtype">bool</span> sym, <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>);
<a name="l00181"></a>00181 
<a name="l00182"></a>00182     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Vector1&gt;
<a name="l00183"></a>00183     <span class="keywordtype">void</span> SolveDistributed(MPI::Comm&amp; comm_facto, Vector1&amp; x_solution,
<a name="l00184"></a>00184                           <span class="keyword">const</span> IVect&amp; glob_number);
<a name="l00185"></a>00185 
<a name="l00186"></a>00186     <span class="keyword">template</span>&lt;<span class="keyword">class</span> TransStatus, <span class="keyword">class</span> Vector1&gt;
<a name="l00187"></a>00187     <span class="keywordtype">void</span> SolveDistributed(MPI::Comm&amp; comm_facto,
<a name="l00188"></a>00188                           <span class="keyword">const</span> TransStatus&amp; TransA, Vector1&amp; x_solution,
<a name="l00189"></a>00189                           <span class="keyword">const</span> IVect&amp; glob_number);
<a name="l00190"></a>00190 <span class="preprocessor">#endif</span>
<a name="l00191"></a>00191 <span class="preprocessor"></span>
<a name="l00192"></a>00192 
<a name="l00193"></a>00193   };
<a name="l00194"></a>00194 
<a name="l00195"></a>00195 
<a name="l00196"></a>00196   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00197"></a>00197             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00198"></a>00198   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a696a4c34a72d618cf68fde030966f48e" title="Solves a sparse linear system using LU factorization.">SparseSolve</a>(Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l00199"></a>00199                    Vector&lt;T1, Storage1, Allocator1&gt;&amp; Y);
<a name="l00200"></a>00200 
<a name="l00201"></a>00201 
<a name="l00202"></a>00202   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop0, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00203"></a>00203   <span class="keywordtype">void</span> Solve(Matrix&lt;T, Prop0, ColSparse, Allocator0&gt;&amp; M,
<a name="l00204"></a>00204              Vector&lt;T, VectFull, Allocator1&gt;&amp; Y);
<a name="l00205"></a>00205 
<a name="l00206"></a>00206 
<a name="l00207"></a>00207   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop0, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00208"></a>00208   <span class="keywordtype">void</span> Solve(Matrix&lt;T, Prop0, RowSparse, Allocator0&gt;&amp; M,
<a name="l00209"></a>00209              Vector&lt;T, VectFull, Allocator1&gt;&amp; Y);
<a name="l00210"></a>00210 
<a name="l00211"></a>00211 
<a name="l00212"></a>00212 }  <span class="comment">// namespace Seldon.</span>
<a name="l00213"></a>00213 
<a name="l00214"></a>00214 
<a name="l00215"></a>00215 <span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
