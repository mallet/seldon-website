<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Functions for Matrices </h1>  </div>
</div>
<div class="contents">
<p>In that page, we detail functions that are not related to <a href="functions_lapack.php">Lapack</a> </p>
<table  class="category-table">
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#transpose">Transpose </a> </td><td class="category-table-td">replaces a matrix by its transpose   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#transpose">TransposeConj </a> </td><td class="category-table-td">replaces a matrix by its conjugate transpose   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#setrow">SetRow </a> </td><td class="category-table-td">modifies a row of the matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getrow">GetRow </a> </td><td class="category-table-td">extracts a row from the matrix  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#setcol">SetCol </a> </td><td class="category-table-td">modifies a column of the matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getcol">GetCol </a> </td><td class="category-table-td">extracts a column from the matrix   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#maxabs">MaxAbs </a> </td><td class="category-table-td">returns highest absolute value of A   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#norm1">Norm1 </a> </td><td class="category-table-td">returns 1-norm of A   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#norminf">NormInf </a> </td><td class="category-table-td">returns infinity-norm of A   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#permutematrix">ApplyInversePermutation </a> </td><td class="category-table-td">permutes row and column numbers of a matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#scalematrix">ScaleMatrix </a> </td><td class="category-table-td">multiplies rows and columns of a matrix by coefficients   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#scaleleftmatrix">ScaleLeftMatrix </a> </td><td class="category-table-td">multiplies rows of a matrix by coefficients  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#Copy">Copy </a> </td><td class="category-table-td">copies a sparse matrix into another one (conversion of format if needed)   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#ConvertMatrix_to_Coordinates">ConvertMatrix_to_Coordinates </a> </td><td class="category-table-td">conversion of a sparse matrix into coordinates format   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#ConvertMatrix_from_Coordinates">ConvertMatrix_from_Coordinates </a> </td><td class="category-table-td">conversion of a matrix given as a triplet (i, j, val) to a sparse matrix   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#ConvertToCSC">ConvertToCSC </a> </td><td class="category-table-td">converts a sparse matrix to CSC (Compressed Sparse Column) format   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#ConvertToCSR">ConvertToCSR </a> </td><td class="category-table-td">converts a sparse matrix to CSR (Compressed Sparse Row) format   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#ConvertToSparse">ConvertToSparse </a> </td><td class="category-table-td">converts dense matrices to sparse matrices by specifying a threshold.   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="iterative.php#sor">SOR</a> </td><td class="category-table-td">applies successive over-relaxations to matrix  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="iterative.php#solvers">Cg, Gmres, BiCgSTAB, etc</a> </td><td class="category-table-td">solves iteratively a linear system  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#IsComplexMatrix">IsComplexMatrix </a> </td><td class="category-table-td">returns true if the matrix is complex   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#IsSparseMatrix">IsSparseMatrix </a> </td><td class="category-table-td">returns true if the matrix is sparse   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#IsSymmetricMatrix">IsSymmetricMatrix </a> </td><td class="category-table-td">returns true if the matrix is symmetric   </td></tr>
</table>
<div class="separator"><a class="anchor" id="transpose"></a></div><h3>Transpose, TransposeConj</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void Transpose(Matrix&amp;);
  void TransposeConj(Matrix&amp;);
</pre><p><code>Transpose</code> overwrites a matrix by its transpose, while <code>TransposeConj</code> overwrites a matrix by its conjugate transpose. These functions are only available for dense matrices.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(5, 5);
A.Fill();

Transpose(A);

Matrix&lt;complex&lt;double&gt; &gt; B(5, 5);
// you fill B as you want, then overwrites it by conj(transpose(B))
TransposeConj(B);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___matrix_8cxx_source.php">Functions_Matrix.cxx</a></p>
<div class="separator"><a class="anchor" id="setrow"></a></div><h3>SetRow</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void SetRow(const Vector&amp;, int, Matrix&amp;);
</pre><p>This function modifies a row in the provided matrix. This function is available only for dense matrices.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(5, 5);
A.Fill();

// now you construct a row
Vector&lt;double&gt; row(5);
row.FillRand();

// and you put it in A
int irow = 1;
SetRow(row, irow, A);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions_8cxx_source.php">Functions.cxx</a></p>
<div class="separator"><a class="anchor" id="getrow"></a></div><h3>GetRow</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void GetRow(const Matrix&amp;, int, Vector&amp;);
</pre><p>This function extracts a row from the provided matrix. This function is available only for dense matrices.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(5, 5);
A.Fill();

// now you extract the first row
Vector&lt;double&gt; row;
GetRow(A, 0, row);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions_8cxx_source.php">Functions.cxx</a></p>
<div class="separator"><a class="anchor" id="setcol"></a></div><h3>SetCol</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void SetCol(const Vector&amp;, int, Matrix&amp;);
</pre><p>This function modifies a column in the provided matrix. This function is available only for dense matrices.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(5, 5);
A.Fill();

// now you construct a column
Vector&lt;double&gt; col(5);
col.FillRand();

// and you put it in A
int icol = 1;
SetCol(col, icol, A);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions_8cxx_source.php">Functions.cxx</a></p>
<div class="separator"><a class="anchor" id="getcol"></a></div><h3>GetCol</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void GetCol(const Matrix&amp;, int, Vector&amp;);
</pre><p>This function extracts a column from the provided matrix. This function is available only for dense matrices.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(5, 5);
A.Fill();

// now you extract the first column
Vector&lt;double&gt; col;
GetCol(A, 0, col);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions_8cxx_source.php">Functions.cxx</a></p>
<div class="separator"><a class="anchor" id="maxabs"></a></div><h3>MaxAbs</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  T MaxAbs(const Matrix&lt;T&gt;&amp;);
</pre><p>This function returns the highest absolute value of a matrix.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;complex&lt;double&gt; &gt; A(5, 5);
A.Fill();

double module_max = MaxAbs(A);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___matrix_8cxx_source.php">Functions_Matrix.cxx</a></p>
<div class="separator"><a class="anchor" id="norm1"></a></div><h3>Norm1</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  T Norm1(const Matrix&lt;T&gt;&amp;);
</pre><p>This function returns the 1-norm of a matrix.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;complex&lt;double&gt; &gt; A(5, 5);
A.Fill();

double anorm_one = Norm1(A);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___matrix_8cxx_source.php">Functions_Matrix.cxx</a></p>
<div class="separator"><a class="anchor" id="norminf"></a></div><h3>NormInf</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  T NormInf(const Matrix&lt;T&gt;&amp;);
</pre><p>This function returns the infinite norm of a matrix.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;complex&lt;double&gt; &gt; A(5, 5);
A.Fill();

double anorm_inf = NormInf(A);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___matrix_8cxx_source.php">Functions_Matrix.cxx</a></p>
<div class="separator"><a class="anchor" id="permutematrix"></a></div><h3>ApplyInversePermutation</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void ApplyInversePermutation(Matrix&amp;, const Vector&lt;int&gt;&amp;, const Vector&lt;int&gt;&amp;);
</pre><p>This function permutes a given matrix with the provided new row numbers and column numbers. <code>ApplyInversePermutation(A, row, col)</code> does the same operation as <code>A(row, col) = A</code> in Matlab. This function is only available for RowMajor, ColMajor, ArrayRowSparse, ArrayRowSymSparse, ArrayRowComplexSparse and ArrayRowSymComplexSparse. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// you fill A as you wish
Matrix&lt;double, Symmetric, ArrayRowSymSparse&gt; A(5, 5);
// then new row and column numbers
IVect row(5);
// for symmetric matrix, row and column permutation must be the same
// if you want to keep symmetry
ApplyInversePermutation(A, row, row);

// for unsymmetric matrices, you can specify different permutations
IVect col(5);
Matrix&lt;double, General, ArrayRowSparse&gt; B(5, 5);
ApplyInversePermutation(B, row, col);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_permutation___scaling_matrix_8cxx_source.php">Permutation_ScalingMatrix.cxx</a></p>
<div class="separator"><a class="anchor" id="scalematrix"></a></div><h3>ScaleMatrix</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void ScaleMatrix(Matrix&amp;, const Vector&amp;, const Vector&amp;);
</pre><p>This function multiplies each row and column with a coefficient, i.e. <code>A</code> is replaced by <code>L*A*R</code> where <code>L</code> and <code>R</code> are diagonal matrices and you provide the diagonal when you call <code>ScaleMatrix</code>. This function is only available for ArrayRowSparse, ArrayRowSymSparse, ArrayRowComplexSparse and ArrayRowSymComplexSparse. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// you fill A as you wish
Matrix&lt;double, Symmetric, ArrayRowSymSparse&gt; A(5, 5);
// then scaling vectors
Vector&lt;double&gt; scale(5);
// for symmetric matrix, row and column scaling must be the same
// if you want to keep symmetry
ScaleMatrix(A, scale, scale);

// for unsymmetric matrices, you can specify different scalings
IVect scale_right(5);
Matrix&lt;double, General, ArrayRowSparse&gt; B(5, 5);
ApplyInversePermutation(B, scale, scale_right);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_permutation___scaling_matrix_8cxx_source.php">Permutation_ScalingMatrix.cxx</a></p>
<div class="separator"><a class="anchor" id="scaleleftmatrix"></a></div><h3>ScaleLeftMatrix</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void ScaleLeftMatrix(Matrix&amp;, const Vector&amp;);
</pre><p>This function multiplies each row with a coefficient, i.e. <code>A</code> is replaced by <code>L*A</code> where <code>L</code> is diagonal and you provide the diagonal when you call <code>ScaleLeftMatrix</code>. This function is only available for ArrayRowSparse, and ArrayRowComplexSparse. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// you fill A as you wish
Matrix&lt;double, General, ArrayRowSparse&gt; A(5, 5);
// then scaling vector
Vector&lt;double&gt; scale(5);
ScaleLeftMatrix(A, scale);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_permutation___scaling_matrix_8cxx_source.php">Permutation_ScalingMatrix.cxx</a></p>
<div class="separator"><a class="anchor" id="Copy"></a></div><h3>Copy</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void Copy(const Matrix&amp;, Matrix2&amp;);
</pre><p>This function copies a sparse matrix into another one. If the types of the matrices differ, a conversion is performed. However, not all the conversion have been implemented, so you may have a compilation error when copying some matrices. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// you fill A as you wish
Matrix&lt;double, General, ArrayRowSparse&gt; A(5, 5);

// then you can copy it to another form
Matrix&lt;double, General, RowSparse&gt; B;

// B does not need to be allocated
Copy(A, B);

// For other types that don't compile
// you can use ConvertMatrix_to_Coordinates :
Matrix&lt;double, Symmetric, ColSymSparse&gt; C;
// conversion to triplet form (i, j, value)
IVect IndRow, IndCol;
Vector&lt;double&gt; Val;
ConvertMatrix_to_Coordinates(A, IndRow, IndCol, Val, 0, true);
// then C is filled
ConvertMatrix_from_Coordinates(IndRow, IndCol, Val, C, 0);

</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_matrix___conversions_8cxx_source.php">Matrix_Conversions.cxx</a></p>
<div class="separator"><a class="anchor" id="ConvertMatrix_to_Coordinates"></a></div><h3>ConvertMatrix_to_Coordinates</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void ConvertMatrix_to_Coordinates(const Matrix&amp; A, Vector&amp; IndRow, Vector&amp; IndCol, Vector&amp; Val, int index, bool sym);
</pre><p>This function converts a sparse matrix to the triplet form (i, j, val) (coordinate format). The row and column numbers will start at index, therefore you can switch between 1-based indices and 0-based indices. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// you fill A as you wish
Matrix&lt;double, General, ArrayRowSparse&gt; A(5, 5);

// conversion to triplet form (i, j, value)
IVect IndRow, IndCol;
Vector&lt;double&gt; Val;
ConvertMatrix_to_Coordinates(A, IndRow, IndCol, Val, 0, true);
// number of non-zero entries :
int nnz = IndRow.GetM();

for (int i = 0; i &lt; nnz; i++)
  {
    cout &lt;&lt; "Row index : " &lt;&lt; IndRow(i) &lt;&lt; endl;
    cout &lt;&lt; "Column index : " &lt;&lt; IndCol(i) &lt;&lt; endl;
    cout &lt;&lt; "value : " &lt;&lt; Val(i) &lt;&lt; endl;
  }
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_matrix___conversions_8cxx_source.php">Matrix_Conversions.cxx</a></p>
<div class="separator"><a class="anchor" id="ConvertMatrix_from_Coordinates"></a></div><h3>ConvertMatrix_from_Coordinates</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void ConvertMatrix_from_Coordinates(Vector&amp; IndRow, Vector&amp; IndCol, Vector&amp; Val, Matrix&amp; A, int index);
</pre><p>This function converts a triplet form (i, j, val) (coordinate format) to a sparse matrix. The row and column numbers are assumed to start at index, therefore you can switch between 1-based indices and 0-based indices. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// creating a sparse matrix
// A = | 1    0   0   2|
//     | -1   2   3   0|
//     |  0  1.2  0  2.5|
int nnz = 7;
IVect IndRow(nnz), IndCol(nnz);
Vector&lt;double&gt; Val(nnz);
IndRow(0) = 0; IndCol(0) = 0; Val(0) = 1.0;
IndRow(1) = 0; IndCol(1) = 3; Val(1) = 2.0;
IndRow(2) = 1; IndCol(2) = 0; Val(2) = -1.0;
IndRow(3) = 1; IndCol(3) = 1; Val(3) = 2.0;
IndRow(4) = 1; IndCol(4) = 2; Val(4) = 3.0;
IndRow(5) = 2; IndCol(5) = 1; Val(5) = 1.2;
IndRow(6) = 2; IndCol(6) = 3; Val(6) = 2.5;

// conversion to a Seldon structure
Matrix&lt;double, General, RowSparse&gt;
ConvertMatrix_from_Coordinates(IndRow, IndCol, Val, A, 0);

</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_matrix___conversions_8cxx_source.php">Matrix_Conversions.cxx</a></p>
<div class="separator"><a class="anchor" id="ConvertToCSC"></a></div><h3>ConvertToCSC</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void ConvertToCSC(const Matrix&amp; A, Property&amp; sym, Vector&amp; Ptr, Vector&amp; Ind, Vector&amp; Val, bool sym_pat);
</pre><p>This function converts a matrix to Compressed Sparse Column (CSC) format. Val stores the values of non-zero entries, Ind the row indexes of the non-zero entries, and Ptr the locations in Val of non-zero entries starting a column. This is the storage represented by ColSparse in <a class="el" href="namespace_seldon.php" title="Seldon namespace.">Seldon</a>. If Property is Symmetric, only upper part of the matrix will be converted (ColSymSparse storage). If sym_pat is true, the sparsity pattern is symmetrized, that is to say that if a(i, j) exists, then a(j, i) also exists. Default value of sym_pat is false. This feature is used for exemple in the interface of Pastix solver, since this solver requires that the sparsity pattern is symmetric (values may be not symmetric). </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">

// you fill A as you wish
Matrix&lt;double, General, ArrayRowSparse&gt; A(5, 5);

// then you can retrieve Ptr, Ind, Val arrays of CSC format
General prop;
ConvertToCSC(A, prop, Ptr, Ind, Val);

</pre></div>  </pre><div class="separator"><a class="anchor" id="ConvertToCSR"></a></div><h3>ConvertToCSR</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void ConvertToCSR(const Matrix&amp; A, Property&amp; sym, Vector&amp; Ptr, Vector&amp; Ind, Vector&amp; Val);
</pre><p>This function converts a matrix to Compressed Sparse Row (CSR) format. Val stores the values of non-zero entries, Ind the column indexes of the non-zero entries, and Ptr the locations in Val of non-zero entries starting a row. This is the storage represented by RowSparse in <a class="el" href="namespace_seldon.php" title="Seldon namespace.">Seldon</a>. If Property is Symmetric, only upper part of the matrix will be converted (RowSymSparse storage). </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">

// you fill A as you wish
Matrix&lt;double, General, ArrayColSparse&gt; A(5, 5);

// then you can retrieve Ptr, Ind, Val arrays of CSR format
General prop;
ConvertToCSR(A, prop, Ptr, Ind, Val);

</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_matrix___conversions_8cxx_source.php">Matrix_Conversions.cxx</a></p>
<div class="separator"><a class="anchor" id="ConvertToSparse"></a></div><h3>ConvertToSparse</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void ConvertToSparse(const Matrix&amp; A, Matrix&amp; B, const T&amp; threshold);
</pre><p>This function converts a dense matrix to a sparse matrix. All values whose modulus is below or equal to threshold are skipped. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">

// you fill A as you wish
Matrix&lt;double, General, RowMajor&gt; A(5, 5);

// then you can convert it to a sparse matrix
Matrix&lt;double, General, RowSparse&gt; B;
ConvertToSparse(A, B, 1e-12);

// and retrieve the number of non-zero entries
int nnz = B.GetDataSize();

</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_matrix___conversions_8cxx_source.php">Matrix_Conversions.cxx</a></p>
<div class="separator"><a class="anchor" id="IsComplexMatrix"></a></div><h3>IsComplexMatrix</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void IsComplexMatrix(const Matrix&amp;)
</pre><p>This function returns true if the matrix is complex. It does not check if all the values are real, but merely returns true if the value type is complex (e.g. T = complex&lt;double&gt;). </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">

// complex matrix
Matrix&lt;complex&lt;double&gt;, General, RowMajor&gt; A;

// IsComplexMatrix should return true
if (IsComplexMatrix(A))
  { 
    cout &lt;&lt; "A is complex" &lt;&lt; endl;
  }

</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___matrix_8cxx_source.php">Functions_Matrix.cxx</a></p>
<div class="separator"><a class="anchor" id="IsSymmetricMatrix"></a></div><h3>IsSymmetricMatrix</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void IsSymmetricMatrix(const Matrix&amp;)
</pre><p>This function returns true if the matrix is symmetric. It does not check if a(i,j) = a(j,i) for all i and j, but merely returns true if the property of the matrix is set to symmetric. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">

// complex matrix
Matrix&lt;complex&lt;double&gt;, Symmetric, RowSymPacked&gt; A;

// IsSymmetricMatrix should return true
if (IsSymmetricMatrix(A))
  { 
    cout &lt;&lt; "A is symmetric" &lt;&lt; endl;
  }

</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___matrix_8cxx_source.php">Functions_Matrix.cxx</a></p>
<div class="separator"><a class="anchor" id="IsSparseMatrix"></a></div><h3>IsSparseMatrix</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void IsSparseMatrix(const Matrix&amp;)
</pre><p>This function returns true if the matrix is sparse. It does not check if all the values are different from 0, but merely returns true if the storage associated with the matrix is a sparse storage. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">

// complex matrix
Matrix&lt;complex&lt;double&gt;, General, ArrayRowSparse&gt; A;

// IsSparseMatrix should return true
if (IsSparseMatrix(A))
  { 
    cout &lt;&lt; "A is sparse" &lt;&lt; endl;
  }

</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___matrix_8cxx_source.php">Functions_Matrix.cxx</a> </p>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
