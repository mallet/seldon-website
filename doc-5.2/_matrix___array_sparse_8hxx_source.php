<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix_sparse/Matrix_ArraySparse.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="comment">// To be included by Seldon.hxx</span>
<a name="l00022"></a>00022 
<a name="l00023"></a>00023 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_ARRAY_SPARSE_HXX</span>
<a name="l00024"></a>00024 <span class="preprocessor"></span>
<a name="l00025"></a>00025 <span class="keyword">namespace </span>Seldon
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 
<a name="l00029"></a>00029 
<a name="l00036"></a>00036   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Storage,
<a name="l00037"></a>00037             <span class="keyword">class </span>Allocator = SELDON_DEFAULT_ALLOCATOR&lt;T&gt; &gt;
<a name="l00038"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php">00038</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix___array_sparse.php" title="Sparse Array-matrix class.">Matrix_ArraySparse</a>
<a name="l00039"></a>00039   {
<a name="l00040"></a>00040     <span class="comment">// typedef declaration.</span>
<a name="l00041"></a>00041   <span class="keyword">public</span>:
<a name="l00042"></a>00042     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00043"></a>00043     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::pointer pointer;
<a name="l00044"></a>00044     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_pointer const_pointer;
<a name="l00045"></a>00045     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::reference reference;
<a name="l00046"></a>00046     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_reference const_reference;
<a name="l00047"></a>00047     <span class="keyword">typedef</span> T entry_type;
<a name="l00048"></a>00048     <span class="keyword">typedef</span> T&amp; access_type;
<a name="l00049"></a>00049     <span class="keyword">typedef</span> T const_access_type;
<a name="l00050"></a>00050 
<a name="l00051"></a>00051     <span class="comment">// Attributes.</span>
<a name="l00052"></a>00052   <span class="keyword">protected</span>:
<a name="l00054"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db">00054</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>;
<a name="l00056"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4">00056</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>;
<a name="l00058"></a>00058     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;T, VectSparse, Allocator&gt;</a>, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>,
<a name="l00059"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6">00059</a>            <a class="code" href="class_seldon_1_1_new_alloc.php">NewAlloc&lt;Vector&lt;T, VectSparse, Allocator&gt;</a> &gt; &gt; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>;
<a name="l00060"></a>00060 
<a name="l00061"></a>00061   <span class="keyword">public</span>:
<a name="l00062"></a>00062     <span class="comment">// Constructors.</span>
<a name="l00063"></a>00063     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a6d460ce1bd8388adce95c8ad99b24f1d" title="Default constructor.">Matrix_ArraySparse</a>();
<a name="l00064"></a>00064     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a6d460ce1bd8388adce95c8ad99b24f1d" title="Default constructor.">Matrix_ArraySparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00065"></a>00065 
<a name="l00066"></a>00066     <span class="comment">// Destructor.</span>
<a name="l00067"></a>00067     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#adf298ef8550ee9681a384a539802736f" title="Destructor.">~Matrix_ArraySparse</a>();
<a name="l00068"></a>00068     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a61227beea7612dd22bf1f46546bb5cde" title="Clears the matrix.">Clear</a>();
<a name="l00069"></a>00069 
<a name="l00070"></a>00070     <span class="comment">// Memory management.</span>
<a name="l00071"></a>00071     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#afcd0d4612b325249c24407627b3dd568" title="Reallocates memory to resize the matrix.">Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00072"></a>00072     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a2c6f9586424529c8b06b6110c5a01efa" title="Reallocates additional memory to resize the matrix.">Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00073"></a>00073 
<a name="l00074"></a>00074     <span class="comment">// Basic methods.</span>
<a name="l00075"></a>00075     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>() <span class="keyword">const</span>;
<a name="l00076"></a>00076     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3ae65d56c548233051a15054643f6753" title="Returns the number of columns.">GetN</a>() <span class="keyword">const</span>;
<a name="l00077"></a>00077     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; status) <span class="keyword">const</span>;
<a name="l00078"></a>00078     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3ae65d56c548233051a15054643f6753" title="Returns the number of columns.">GetN</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; status) <span class="keyword">const</span>;
<a name="l00079"></a>00079     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ac58573fd27b4f0345ae196b1a6c69d0d" title="Returns the number of non-zero entries.">GetNonZeros</a>() <span class="keyword">const</span>;
<a name="l00080"></a>00080     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a01787b806fc996c2ff3c1dcf9b2d54de" title="Returns the number of elements stored in memory.">GetDataSize</a>() <span class="keyword">const</span>;
<a name="l00081"></a>00081     <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ac3d6a23829eb932f41de04c13de22f6b" title="Returns (row or column) indices of non-zero entries in row.">GetIndex</a>(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00082"></a>00082     T* <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a550784008495b254998193d94f75bff9" title="Returns values of non-zero entries.">GetData</a>(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00083"></a>00083 
<a name="l00084"></a>00084     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php" title="Sparse vector class.">Vector&lt;T, VectSparse, Allocator&gt;</a>* <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a550784008495b254998193d94f75bff9" title="Returns values of non-zero entries.">GetData</a>() <span class="keyword">const</span>;
<a name="l00085"></a>00085 
<a name="l00086"></a>00086     <span class="comment">// Element acess and affectation.</span>
<a name="l00087"></a>00087     T <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a41e1ef18aa87fce8062d36be5e634dfd" title="Access operator.">operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00088"></a>00088     T&amp; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a93c303a4312317ac5f90c125ef3c5a97" title="Access operator.">Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00089"></a>00089     <span class="keyword">const</span> T&amp; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a93c303a4312317ac5f90c125ef3c5a97" title="Access operator.">Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00090"></a>00090     T&amp; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ad114c8c6e002a7a0def70f2413a32659" title="Access operator.">Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00091"></a>00091     <span class="keyword">const</span> T&amp; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ad114c8c6e002a7a0def70f2413a32659" title="Access operator.">Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00092"></a>00092     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a7efa3a40752d1506b63504aaf65775b2" title="Sets an element of the matrix.">Set</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; x);
<a name="l00093"></a>00093 
<a name="l00094"></a>00094     <span class="keyword">const</span> T&amp; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">Value</a>(<span class="keywordtype">int</span> num_row, <span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00095"></a>00095     T&amp; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">Value</a>(<span class="keywordtype">int</span> num_row, <span class="keywordtype">int</span> i);
<a name="l00096"></a>00096     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3534fb2c57f2640c9b8cd5a59e9da0e2" title="Returns column/row number of j-th non-zero value of row/column i.">Index</a>(<span class="keywordtype">int</span> num_row, <span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00097"></a>00097     <span class="keywordtype">int</span>&amp; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3534fb2c57f2640c9b8cd5a59e9da0e2" title="Returns column/row number of j-th non-zero value of row/column i.">Index</a>(<span class="keywordtype">int</span> num_row, <span class="keywordtype">int</span> i);
<a name="l00098"></a>00098 
<a name="l00099"></a>00099     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a9fb98c93d5bee7a4be62e1a3e1bb5a46" title="Redefines the matrix.">SetData</a>(<span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php" title="Sparse vector class.">Vector&lt;T, VectSparse, Allocator&gt;</a>*);
<a name="l00100"></a>00100     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a9fb98c93d5bee7a4be62e1a3e1bb5a46" title="Redefines the matrix.">SetData</a>(<span class="keywordtype">int</span>, <span class="keywordtype">int</span>, T*, <span class="keywordtype">int</span>*);
<a name="l00101"></a>00101     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0cceba81597165b0e23ec0c20daed462" title="Clears the matrix without releasing memory.">Nullify</a>(<span class="keywordtype">int</span> i);
<a name="l00102"></a>00102     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0cceba81597165b0e23ec0c20daed462" title="Clears the matrix without releasing memory.">Nullify</a>();
<a name="l00103"></a>00103 
<a name="l00104"></a>00104     <span class="comment">// Convenient functions.</span>
<a name="l00105"></a>00105     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a61ff8d09ea73a05eff4fb21e16fb1118" title="Displays the matrix on the standard output.">Print</a>() <span class="keyword">const</span>;
<a name="l00106"></a>00106     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ac469920c36d5a57516a3e53c0c4e3126" title="Assembles the matrix.">Assemble</a>();
<a name="l00107"></a>00107     <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l00108"></a>00108     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a4b87eb77f9690ec605e9a1e7e1d155c5" title="Removes small coefficients from entries.">RemoveSmallEntry</a>(<span class="keyword">const</span> T0&amp; epsilon);
<a name="l00109"></a>00109 
<a name="l00110"></a>00110     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#af92baadfc393d70b6e3f3ecaa25d16c7" title="Matrix is initialized to the identity matrix.">SetIdentity</a>();
<a name="l00111"></a>00111     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a06fc483908e9219787dfc9331b07d828" title="Non-zero entries are set to 0 (but not removed).">Zero</a>();
<a name="l00112"></a>00112     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a806a12c725733434b08b4276de7fbcee" title="Non-zero entries are filled with values 0, 1, 2, 3 ...">Fill</a>();
<a name="l00113"></a>00113     <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00114"></a>00114     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a806a12c725733434b08b4276de7fbcee" title="Non-zero entries are filled with values 0, 1, 2, 3 ...">Fill</a>(<span class="keyword">const</span> T0&amp; x);
<a name="l00115"></a>00115     <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00116"></a>00116     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php" title="Sparse Array-matrix class.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a28d0b06c82e4bb29f36c872f71fe7b48" title="Non-zero entries are set to a given value x.">operator= </a>(<span class="keyword">const</span> T0&amp; x);
<a name="l00117"></a>00117     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ae7161fac73344a98f937abbf957fdbb3" title="Non-zero entries take a random value.">FillRand</a>();
<a name="l00118"></a>00118 
<a name="l00119"></a>00119     <span class="comment">// Input/output functions.</span>
<a name="l00120"></a>00120     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a014e4dca632c4955660ad849db2cb363" title="Writes the matrix in a file.">Write</a>(<span class="keywordtype">string</span> FileName) <span class="keyword">const</span>;
<a name="l00121"></a>00121     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a014e4dca632c4955660ad849db2cb363" title="Writes the matrix in a file.">Write</a>(ostream&amp; FileStream) <span class="keyword">const</span>;
<a name="l00122"></a>00122     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#adf0d3ec9d5ffeb1b3d46f523051d1412" title="Writes the matrix in a file.">WriteText</a>(<span class="keywordtype">string</span> FileName) <span class="keyword">const</span>;
<a name="l00123"></a>00123     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#adf0d3ec9d5ffeb1b3d46f523051d1412" title="Writes the matrix in a file.">WriteText</a>(ostream&amp; FileStream) <span class="keyword">const</span>;
<a name="l00124"></a>00124     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a2e644c704eba4e3c531c403952189d86" title="Reads the matrix from a file.">Read</a>(<span class="keywordtype">string</span> FileName);
<a name="l00125"></a>00125     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a2e644c704eba4e3c531c403952189d86" title="Reads the matrix from a file.">Read</a>(istream&amp; FileStream);
<a name="l00126"></a>00126     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a22e85b66114b00ba89b1615bc1839b0c" title="Reads the matrix from a file.">ReadText</a>(<span class="keywordtype">string</span> FileName);
<a name="l00127"></a>00127     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a22e85b66114b00ba89b1615bc1839b0c" title="Reads the matrix from a file.">ReadText</a>(istream&amp; FileStream);
<a name="l00128"></a>00128 
<a name="l00129"></a>00129   };
<a name="l00130"></a>00130 
<a name="l00131"></a>00131 
<a name="l00133"></a>00133   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00134"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php">00134</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_col_sparse.php">ArrayColSparse</a>, Allocator&gt; :
<a name="l00135"></a>00135     <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php" title="Sparse Array-matrix class.">Matrix_ArraySparse</a>&lt;T, Prop, ArrayColSparse, Allocator&gt;
<a name="l00136"></a>00136   {
<a name="l00137"></a>00137     <span class="comment">// typedef declaration.</span>
<a name="l00138"></a>00138   <span class="keyword">public</span>:
<a name="l00139"></a>00139     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00140"></a>00140     <span class="keyword">typedef</span> Prop property;
<a name="l00141"></a>00141     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_array_col_sparse.php">ArrayColSparse</a> <a class="code" href="class_seldon_1_1_array_col_sparse.php">storage</a>;
<a name="l00142"></a>00142     <span class="keyword">typedef</span> Allocator allocator;
<a name="l00143"></a>00143 
<a name="l00144"></a>00144   <span class="keyword">public</span>:
<a name="l00145"></a>00145     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>();
<a name="l00146"></a>00146     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00147"></a>00147 
<a name="l00148"></a>00148     <span class="comment">// Memory management.</span>
<a name="l00149"></a>00149     <span class="keywordtype">void</span> ClearColumn(<span class="keywordtype">int</span> i);
<a name="l00150"></a>00150     <span class="keywordtype">void</span> ReallocateColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00151"></a>00151     <span class="keywordtype">void</span> ResizeColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00152"></a>00152     <span class="keywordtype">void</span> SwapColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> i_);
<a name="l00153"></a>00153     <span class="keywordtype">void</span> ReplaceIndexColumn(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index);
<a name="l00154"></a>00154 
<a name="l00155"></a>00155     <span class="keywordtype">int</span> GetColumnSize(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00156"></a>00156     <span class="keywordtype">void</span> PrintColumn(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00157"></a>00157     <span class="keywordtype">void</span> AssembleColumn(<span class="keywordtype">int</span> i);
<a name="l00158"></a>00158 
<a name="l00159"></a>00159     <span class="keywordtype">void</span> AddInteraction(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; val);
<a name="l00160"></a>00160 
<a name="l00161"></a>00161     <span class="keywordtype">void</span> AddInteractionRow(<span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>*, T*);
<a name="l00162"></a>00162     <span class="keywordtype">void</span> AddInteractionColumn(<span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>*, T*);
<a name="l00163"></a>00163 
<a name="l00164"></a>00164     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l00165"></a>00165     <span class="keywordtype">void</span> AddInteractionRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l00166"></a>00166                            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val);
<a name="l00167"></a>00167     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l00168"></a>00168     <span class="keywordtype">void</span> AddInteractionColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l00169"></a>00169                               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val);
<a name="l00170"></a>00170   };
<a name="l00171"></a>00171 
<a name="l00172"></a>00172 
<a name="l00174"></a>00174   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00175"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">00175</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_row_sparse.php">ArrayRowSparse</a>, Allocator&gt; :
<a name="l00176"></a>00176     <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php" title="Sparse Array-matrix class.">Matrix_ArraySparse</a>&lt;T, Prop, ArrayRowSparse, Allocator&gt;
<a name="l00177"></a>00177   {
<a name="l00178"></a>00178     <span class="comment">// typedef declaration.</span>
<a name="l00179"></a>00179   <span class="keyword">public</span>:
<a name="l00180"></a>00180     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00181"></a>00181     <span class="keyword">typedef</span> Prop property;
<a name="l00182"></a>00182     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_array_row_sparse.php">ArrayRowSparse</a> <a class="code" href="class_seldon_1_1_array_row_sparse.php">storage</a>;
<a name="l00183"></a>00183     <span class="keyword">typedef</span> Allocator allocator;
<a name="l00184"></a>00184 
<a name="l00185"></a>00185   <span class="keyword">public</span>:
<a name="l00186"></a>00186     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>();
<a name="l00187"></a>00187     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00188"></a>00188 
<a name="l00189"></a>00189     <span class="comment">// Memory management.</span>
<a name="l00190"></a>00190     <span class="keywordtype">void</span> ClearRow(<span class="keywordtype">int</span> i);
<a name="l00191"></a>00191     <span class="keywordtype">void</span> ReallocateRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00192"></a>00192     <span class="keywordtype">void</span> ResizeRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00193"></a>00193     <span class="keywordtype">void</span> SwapRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> i_);
<a name="l00194"></a>00194     <span class="keywordtype">void</span> ReplaceIndexRow(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index);
<a name="l00195"></a>00195 
<a name="l00196"></a>00196     <span class="keywordtype">int</span> GetRowSize(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00197"></a>00197     <span class="keywordtype">void</span> PrintRow(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00198"></a>00198     <span class="keywordtype">void</span> AssembleRow(<span class="keywordtype">int</span> i);
<a name="l00199"></a>00199 
<a name="l00200"></a>00200     <span class="keywordtype">void</span> AddInteraction(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; val);
<a name="l00201"></a>00201 
<a name="l00202"></a>00202     <span class="keywordtype">void</span> AddInteractionRow(<span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>*, T*);
<a name="l00203"></a>00203     <span class="keywordtype">void</span> AddInteractionColumn(<span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>*, T*);
<a name="l00204"></a>00204 
<a name="l00205"></a>00205     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l00206"></a>00206     <span class="keywordtype">void</span> AddInteractionRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l00207"></a>00207                            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val);
<a name="l00208"></a>00208     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l00209"></a>00209     <span class="keywordtype">void</span> AddInteractionColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l00210"></a>00210                               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val);
<a name="l00211"></a>00211   };
<a name="l00212"></a>00212 
<a name="l00214"></a>00214   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00215"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php">00215</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_col_sym_sparse.php">ArrayColSymSparse</a>, Allocator&gt;:
<a name="l00216"></a>00216     <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php" title="Sparse Array-matrix class.">Matrix_ArraySparse</a>&lt;T, Prop, ArrayColSymSparse, Allocator&gt;
<a name="l00217"></a>00217   {
<a name="l00218"></a>00218     <span class="comment">// typedef declaration.</span>
<a name="l00219"></a>00219   <span class="keyword">public</span>:
<a name="l00220"></a>00220     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00221"></a>00221     <span class="keyword">typedef</span> Prop property;
<a name="l00222"></a>00222     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_array_col_sym_sparse.php">ArrayColSymSparse</a> <a class="code" href="class_seldon_1_1_array_col_sym_sparse.php">storage</a>;
<a name="l00223"></a>00223     <span class="keyword">typedef</span> Allocator allocator;
<a name="l00224"></a>00224 
<a name="l00225"></a>00225   <span class="keyword">public</span>:
<a name="l00226"></a>00226     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>();
<a name="l00227"></a>00227     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00228"></a>00228 
<a name="l00229"></a>00229     T operator() (<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00230"></a>00230     T&amp; Get(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00231"></a>00231     <span class="keyword">const</span> T&amp; Get(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00232"></a>00232     T&amp; Val(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00233"></a>00233     <span class="keyword">const</span> T&amp; Val(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00234"></a>00234     <span class="keywordtype">void</span> Set(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; x);
<a name="l00235"></a>00235 
<a name="l00236"></a>00236     <span class="comment">// Memory management.</span>
<a name="l00237"></a>00237     <span class="keywordtype">void</span> ClearColumn(<span class="keywordtype">int</span> i);
<a name="l00238"></a>00238     <span class="keywordtype">void</span> ReallocateColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00239"></a>00239     <span class="keywordtype">void</span> ResizeColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00240"></a>00240     <span class="keywordtype">void</span> SwapColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> i_);
<a name="l00241"></a>00241     <span class="keywordtype">void</span> ReplaceIndexColumn(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index);
<a name="l00242"></a>00242 
<a name="l00243"></a>00243     <span class="keywordtype">int</span> GetColumnSize(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00244"></a>00244     <span class="keywordtype">void</span> PrintColumn(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00245"></a>00245     <span class="keywordtype">void</span> AssembleColumn(<span class="keywordtype">int</span> i);
<a name="l00246"></a>00246 
<a name="l00247"></a>00247     <span class="keywordtype">void</span> AddInteraction(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; val);
<a name="l00248"></a>00248 
<a name="l00249"></a>00249     <span class="keywordtype">void</span> AddInteractionRow(<span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>*, T*);
<a name="l00250"></a>00250     <span class="keywordtype">void</span> AddInteractionColumn(<span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>*, T*);
<a name="l00251"></a>00251 
<a name="l00252"></a>00252     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l00253"></a>00253     <span class="keywordtype">void</span> AddInteractionRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l00254"></a>00254                            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val);
<a name="l00255"></a>00255     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l00256"></a>00256     <span class="keywordtype">void</span> AddInteractionColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l00257"></a>00257                               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val);
<a name="l00258"></a>00258   };
<a name="l00259"></a>00259 
<a name="l00260"></a>00260 
<a name="l00262"></a>00262   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00263"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php">00263</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_row_sym_sparse.php">ArrayRowSymSparse</a>, Allocator&gt;:
<a name="l00264"></a>00264     <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php" title="Sparse Array-matrix class.">Matrix_ArraySparse</a>&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;
<a name="l00265"></a>00265   {
<a name="l00266"></a>00266     <span class="comment">// typedef declaration.</span>
<a name="l00267"></a>00267   <span class="keyword">public</span>:
<a name="l00268"></a>00268     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00269"></a>00269     <span class="keyword">typedef</span> Prop property;
<a name="l00270"></a>00270     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_array_row_sym_sparse.php">ArrayRowSymSparse</a> <a class="code" href="class_seldon_1_1_array_row_sym_sparse.php">storage</a>;
<a name="l00271"></a>00271     <span class="keyword">typedef</span> Allocator allocator;
<a name="l00272"></a>00272 
<a name="l00273"></a>00273   <span class="keyword">public</span>:
<a name="l00274"></a>00274     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>();
<a name="l00275"></a>00275     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00276"></a>00276 
<a name="l00277"></a>00277     T operator() (<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00278"></a>00278     T&amp; Get(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00279"></a>00279     <span class="keyword">const</span> T&amp; Get(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00280"></a>00280     T&amp; Val(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00281"></a>00281     <span class="keyword">const</span> T&amp; Val(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00282"></a>00282     <span class="keywordtype">void</span> Set(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; x);
<a name="l00283"></a>00283 
<a name="l00284"></a>00284     <span class="comment">// Memory management.</span>
<a name="l00285"></a>00285     <span class="keywordtype">void</span> ClearRow(<span class="keywordtype">int</span> i);
<a name="l00286"></a>00286     <span class="keywordtype">void</span> ReallocateRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00287"></a>00287     <span class="keywordtype">void</span> ResizeRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00288"></a>00288     <span class="keywordtype">void</span> SwapRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> i_);
<a name="l00289"></a>00289     <span class="keywordtype">void</span> ReplaceIndexRow(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index);
<a name="l00290"></a>00290 
<a name="l00291"></a>00291     <span class="keywordtype">int</span> GetRowSize(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00292"></a>00292     <span class="keywordtype">void</span> PrintRow(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00293"></a>00293     <span class="keywordtype">void</span> AssembleRow(<span class="keywordtype">int</span> i);
<a name="l00294"></a>00294 
<a name="l00295"></a>00295     <span class="keywordtype">void</span> AddInteraction(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; val);
<a name="l00296"></a>00296 
<a name="l00297"></a>00297     <span class="keywordtype">void</span> AddInteractionRow(<span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>*, T*);
<a name="l00298"></a>00298     <span class="keywordtype">void</span> AddInteractionColumn(<span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>*, T*);
<a name="l00299"></a>00299 
<a name="l00300"></a>00300     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l00301"></a>00301     <span class="keywordtype">void</span> AddInteractionRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l00302"></a>00302                            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val);
<a name="l00303"></a>00303     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l00304"></a>00304     <span class="keywordtype">void</span> AddInteractionColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l00305"></a>00305                               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val);
<a name="l00306"></a>00306   };
<a name="l00307"></a>00307 
<a name="l00308"></a>00308 } <span class="comment">// namespace Seldon</span>
<a name="l00309"></a>00309 
<a name="l00310"></a>00310 <span class="preprocessor">#define SELDON_FILE_MATRIX_ARRAY_SPARSE_HXX</span>
<a name="l00311"></a>00311 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
