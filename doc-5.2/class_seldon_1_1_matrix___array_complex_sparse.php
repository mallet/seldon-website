<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Matrix_ArrayComplexSparse</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Matrix_ArrayComplexSparse" -->
<p>Sparse Array-matrix class.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_matrix___array_complex_sparse_8hxx_source.php">Matrix_ArrayComplexSparse.hxx</a>&gt;</code></p>

<p><a href="class_seldon_1_1_matrix___array_complex_sparse-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a75cdf2b702bd1ce38c1946cd91018dd9"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::value_type" ref="a75cdf2b702bd1ce38c1946cd91018dd9" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5325ffba5ca6253c1df46e37283142d0"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::pointer" ref="a5325ffba5ca6253c1df46e37283142d0" args="" -->
typedef Allocator::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a87436f000442ff05b2fc557a77dae13c"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::const_pointer" ref="a87436f000442ff05b2fc557a77dae13c" args="" -->
typedef Allocator::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1ab28bb0ce6a3c1bba3b7680b0d2f6a2"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::reference" ref="a1ab28bb0ce6a3c1bba3b7680b0d2f6a2" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a963d0f91721a1d53fb2b4d5bc322abb8"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::const_reference" ref="a963d0f91721a1d53fb2b4d5bc322abb8" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2b4d88a19897b82ba5d8a37198cf4426"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::entry_type" ref="a2b4d88a19897b82ba5d8a37198cf4426" args="" -->
typedef complex&lt; T &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>entry_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a55a8cb7922d8fc8bbacd2b5548b03359"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::access_type" ref="a55a8cb7922d8fc8bbacd2b5548b03359" args="" -->
typedef complex&lt; T &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>access_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aaa7b45cfa45845f340f819e31f877f34"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::const_access_type" ref="aaa7b45cfa45845f340f819e31f877f34" args="" -->
typedef complex&lt; T &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_access_type</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a415bb8c2cb2379647933ddc5d9dbcc32">Matrix_ArrayComplexSparse</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#a415bb8c2cb2379647933ddc5d9dbcc32"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a410f415f4976918fc7678bc770611043">Matrix_ArrayComplexSparse</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Constructor.  <a href="#a410f415f4976918fc7678bc770611043"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acec6f5d4aa6488c9389a2065944568b0"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::~Matrix_ArrayComplexSparse" ref="acec6f5d4aa6488c9389a2065944568b0" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#acec6f5d4aa6488c9389a2065944568b0">~Matrix_ArrayComplexSparse</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Destructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0ee918c8c04a1a9434e157c15fdeb596">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the matrix.  <a href="#a0ee918c8c04a1a9434e157c15fdeb596"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad2348fb25206ef8516c7e835b7ce2287">Reallocate</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates memory to resize the matrix.  <a href="#ad2348fb25206ef8516c7e835b7ce2287"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a9166689334cc2f731220f76b730d692f">Resize</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates additional memory to resize the matrix.  <a href="#a9166689334cc2f731220f76b730d692f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821">GetM</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows.  <a href="#a35ea621d2e5351259ce5bca87febc821"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ae413915ed95545b40df8b43737fa43c1">GetN</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns.  <a href="#ae413915ed95545b40df8b43737fa43c1"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ab3c6aad66c9dc6ad9addfb7de9a4ebdd">GetM</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows of the matrix possibly transposed.  <a href="#ab3c6aad66c9dc6ad9addfb7de9a4ebdd"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a53e20518a8557211c89ca8ad0d607825">GetN</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns of the matrix possibly transposed.  <a href="#a53e20518a8557211c89ca8ad0d607825"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a11efc1bee6a95da705238628d7e3d958">GetRealNonZeros</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of non-zero elements (real part).  <a href="#a11efc1bee6a95da705238628d7e3d958"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a33ebac042eda90796d7f674c250e85be">GetImagNonZeros</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of non-zero elements (imaginary part).  <a href="#a33ebac042eda90796d7f674c250e85be"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa40775992c738c7fd1e9543bfddb2694">GetRealDataSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored in memory (real part).  <a href="#aa40775992c738c7fd1e9543bfddb2694"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad54321edab8e2633985e198f1a576340">GetImagDataSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored in memory (imaginary part).  <a href="#ad54321edab8e2633985e198f1a576340"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2e5c6c10af220eea29f9c1565b14a93f">GetDataSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored in memory (real+imaginary part).  <a href="#a2e5c6c10af220eea29f9c1565b14a93f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#afae45969eda952b43748cf7756f0e6d6">GetRealInd</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns column indices of non-zero entries in row (real part).  <a href="#afae45969eda952b43748cf7756f0e6d6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0a423d7e93a0ad9c19ed6de2b1052dca">GetImagInd</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns column indices of non-zero entries in row (imaginary part).  <a href="#a0a423d7e93a0ad9c19ed6de2b1052dca"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aaa912365c7b8c191cf8b3fb2e2da4789">GetRealData</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns values of non-zero entries of a row (real part).  <a href="#aaa912365c7b8c191cf8b3fb2e2da4789"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a578281c64cbe05a542a6b0e9642b326a">GetImagData</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns values of non-zero entries of a row (imaginary part).  <a href="#a578281c64cbe05a542a6b0e9642b326a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1d33377770e234c75aae39bed6f996ff"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::GetRealData" ref="a1d33377770e234c75aae39bed6f996ff" args="() const " -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt; *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetRealData</b> () const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac3940bc9993d64730333c83be0f839ce"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::GetImagData" ref="ac3940bc9993d64730333c83be0f839ce" args="() const " -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt; *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetImagData</b> () const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top">complex&lt; T &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a30be6306dbdd79b0a282ef99289b0c2b">operator()</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a30be6306dbdd79b0a282ef99289b0c2b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">complex&lt; T &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7eb05d6902b13cfb7cc4f17093fdc17e">Val</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Unavailable access method.  <a href="#a7eb05d6902b13cfb7cc4f17093fdc17e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const complex&lt; T &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a994fbc16dd258d1609f7c7c582205e9f">Val</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Unavailable access method.  <a href="#a994fbc16dd258d1609f7c7c582205e9f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">complex&lt; T &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad840efb36621d28aa9726088cfc29a29">Get</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Unavailable access method.  <a href="#ad840efb36621d28aa9726088cfc29a29"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const complex&lt; T &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a17a0b2e40258b30da1f0036c2a85c357">Get</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Unavailable access method.  <a href="#a17a0b2e40258b30da1f0036c2a85c357"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#abdacf01dc354d0449b16b1d9e111ec54">ValReal</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns acces to real part of A(i, j).  <a href="#abdacf01dc354d0449b16b1d9e111ec54"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2fa5f2b8aeb3c848f5749d113f93dce4">ValReal</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns acces to real part of A(i, j).  <a href="#a2fa5f2b8aeb3c848f5749d113f93dce4"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aee8de4c9149d47935bf84fc04727c7f2">ValImag</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns acces to imaginary part of A(i, j).  <a href="#aee8de4c9149d47935bf84fc04727c7f2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0324d6e453ce4c18dbc84bdc48fb4d5f">ValImag</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns acces to imaginary part of A(i, j).  <a href="#a0324d6e453ce4c18dbc84bdc48fb4d5f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ac91be8b2868c034acbf161a8b3af9598">GetReal</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns acces to real part of A(i, j).  <a href="#ac91be8b2868c034acbf161a8b3af9598"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ae562065dff1e5df2ec2516bf7f4f56ae">GetReal</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns acces to real part of A(i, j).  <a href="#ae562065dff1e5df2ec2516bf7f4f56ae"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a644eff8b81954e1f43c4f9321a866903">GetImag</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns acces to imaginary part of A(i, j).  <a href="#a644eff8b81954e1f43c4f9321a866903"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a3233d9091b1fe40403f0e1779f260033">GetImag</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns acces to imaginary part of A(i, j).  <a href="#a3233d9091b1fe40403f0e1779f260033"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2dfca2deb2a823fc64dc51b7c0728f49">Set</a> (int i, int j, const complex&lt; T &gt; &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets element (i, j) of matrix.  <a href="#a2dfca2deb2a823fc64dc51b7c0728f49"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a041c76033f939293aa871f2fe45ef22a">ValueReal</a> (int num_row, int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns j-th non-zero value of row i (real part).  <a href="#a041c76033f939293aa871f2fe45ef22a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a11fe8648ddd3e6e339eb81735d7e544e">ValueReal</a> (int num_row, int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns j-th non-zero value of row i (real part).  <a href="#a11fe8648ddd3e6e339eb81735d7e544e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2a3cd76154ac61a43afa42563805ce3b">IndexReal</a> (int num_row, int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns column number of j-th non-zero value of row i (real part).  <a href="#a2a3cd76154ac61a43afa42563805ce3b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2fdb9482dad6378fb8ced1982b6860b1">IndexReal</a> (int num_row, int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns column number of j-th non-zero value of row i (real part).  <a href="#a2fdb9482dad6378fb8ced1982b6860b1"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa2d9a2e69a269bf9d15b62d520cb560d">ValueImag</a> (int num_row, int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns j-th non-zero value of row i (imaginary part).  <a href="#aa2d9a2e69a269bf9d15b62d520cb560d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ab410f72a305938081fbc07e868eb161b">ValueImag</a> (int num_row, int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns j-th non-zero value of row i (imaginary part).  <a href="#ab410f72a305938081fbc07e868eb161b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aedba64fa9b01bce6bb75e9e5a14b26c5">IndexImag</a> (int num_row, int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns column number of j-th non-zero value of row i (imaginary part).  <a href="#aedba64fa9b01bce6bb75e9e5a14b26c5"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a011b15c1909f712a693de10f2c0c2328">IndexImag</a> (int num_row, int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns column number of j-th non-zero value of row i (imaginary part).  <a href="#a011b15c1909f712a693de10f2c0c2328"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aab6c31240cd6dcbd442f9a29658f51ea">SetRealData</a> (int, int, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; *)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Redefines the real part of the matrix.  <a href="#aab6c31240cd6dcbd442f9a29658f51ea"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a8e3c2dfa0ea5aa57cd8607cca15562f2">SetImagData</a> (int, int, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; *)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Redefines the imaginary part of the matrix.  <a href="#a8e3c2dfa0ea5aa57cd8607cca15562f2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a1ebb3dcbf317ec99e6463f0aafea10b9">SetRealData</a> (int, int, T *, int *)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Redefines a row/column of the matrix.  <a href="#a1ebb3dcbf317ec99e6463f0aafea10b9"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a50622f29a8e112821c68e5a28236258f">SetImagData</a> (int, int, T *, int *)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Redefines a row/column of the matrix.  <a href="#a50622f29a8e112821c68e5a28236258f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a9f0922d865cdd4a627912583a9c73151">NullifyReal</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears a row without releasing memory.  <a href="#a9f0922d865cdd4a627912583a9c73151"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad5e87ca37baabfb8cdc6f841f5125d7d">NullifyImag</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears a row without releasing memory.  <a href="#ad5e87ca37baabfb8cdc6f841f5125d7d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aed1192a75fe2c7d19e545ff6054298e2">NullifyReal</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the matrix without releasing memory.  <a href="#aed1192a75fe2c7d19e545ff6054298e2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#adbdb03e02fd8617a23188d6bc3155ec0">NullifyImag</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the matrix without releasing memory.  <a href="#adbdb03e02fd8617a23188d6bc3155ec0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ac24abb2e047eb8b91728023074b3fb13">Print</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays the matrix on the standard output.  <a href="#ac24abb2e047eb8b91728023074b3fb13"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a9b54a14b64f67242fe9367df4142bd7f">Write</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix in a file.  <a href="#a9b54a14b64f67242fe9367df4142bd7f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0cc7af3e3d677e5cb6bd5f66d7965a6f">Write</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix to an output stream.  <a href="#a0cc7af3e3d677e5cb6bd5f66d7965a6f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7095c684ecdb9ef6e9fe9b84462e22ce">WriteText</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix in a file.  <a href="#a7095c684ecdb9ef6e9fe9b84462e22ce"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a884e62c07aa29b59b259d232f7771c94">WriteText</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix to an output stream.  <a href="#a884e62c07aa29b59b259d232f7771c94"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a58c2b7013db7765a01181f928833e530">Read</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from a file.  <a href="#a58c2b7013db7765a01181f928833e530"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a8743f281d37cc023cd3e628eba4644f0">Read</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from an input stream.  <a href="#a8743f281d37cc023cd3e628eba4644f0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ab0d4c7f4d291599c0d9cb735dd7d9dc8">ReadText</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from a file.  <a href="#ab0d4c7f4d291599c0d9cb735dd7d9dc8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a20de6511128d3ac679c885abcd5538d0">ReadText</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from an input stream.  <a href="#a20de6511128d3ac679c885abcd5538d0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a889d3baa1c4aa6a96b83ba8ca7b4cf4a">Assemble</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Assembles the matrix.  <a href="#a889d3baa1c4aa6a96b83ba8ca7b4cf4a"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="aa9a81eead891c0ee85f2563dc8be8a99"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::RemoveSmallEntry" ref="aa9a81eead891c0ee85f2563dc8be8a99" args="(const T0 &amp;epsilon)" -->
template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><b>RemoveSmallEntry</b> (const T0 &amp;epsilon)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a19b48691b03953dffd387ace5cfddd37"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::SetIdentity" ref="a19b48691b03953dffd387ace5cfddd37" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a19b48691b03953dffd387ace5cfddd37">SetIdentity</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> is initialized to the identity matrix. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa6f51cb3c7779c309578058fd34c5d25"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::Zero" ref="aa6f51cb3c7779c309578058fd34c5d25" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa6f51cb3c7779c309578058fd34c5d25">Zero</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Non-zero entries are set to 0 (but not removed). <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1fce8c713bc87ca4e06df819ced39554"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::Fill" ref="a1fce8c713bc87ca4e06df819ced39554" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a1fce8c713bc87ca4e06df819ced39554">Fill</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Non-zero entries are filled with values 0, 1, 2, 3 ... <br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a08a5f1de809bbb4565e90d954ca053f6">Fill</a> (const complex&lt; T0 &gt; &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Non-zero entries are set to a given value x.  <a href="#a08a5f1de809bbb4565e90d954ca053f6"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a95e0c06209ddfeea67740ecb9c0c1451"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::operator=" ref="a95e0c06209ddfeea67740ecb9c0c1451" args="(const complex&lt; T0 &gt; &amp;x)" -->
template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Matrix_ArrayComplexSparse</a>&lt; T, <br class="typebreak"/>
Prop, Storage, Allocator &gt; &amp;&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a95e0c06209ddfeea67740ecb9c0c1451">operator=</a> (const complex&lt; T0 &gt; &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Non-zero entries are set to a given value x. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad1ff9883f030ca9eaada43616865acdb"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::FillRand" ref="ad1ff9883f030ca9eaada43616865acdb" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad1ff9883f030ca9eaada43616865acdb">FillRand</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Non-zero entries take a random value. <br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2032848b78edab7ef6721eda9e2784de"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::m_" ref="a2032848b78edab7ef6721eda9e2784de" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de">m_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of rows. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acfa7a2c6c7f77dc8ebcee2dd411ebf98"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::n_" ref="acfa7a2c6c7f77dc8ebcee2dd411ebf98" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98">n_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of columns. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af1766a47a3d45c0b904b07870204e4a6"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::val_real_" ref="af1766a47a3d45c0b904b07870204e4a6" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6">val_real_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">real part rows or columns <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afa77c763d3b3a280dfef2a4410ed0bd8"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::val_imag_" ref="afa77c763d3b3a280dfef2a4410ed0bd8" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8">val_imag_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">imaginary part rows or columns <br/></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Prop, class Storage, class Allocator = SELDON_DEFAULT_ALLOCATOR&lt;T&gt;&gt;<br/>
 class Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</h3>

<p>Sparse Array-matrix class. </p>
<p>Sparse matrices are defined by: (1) the number of rows and columns; (2) the number of non-zero entries; (3) an array of vectors ind ind(i) is a vector, which contains indices of columns of the row i; (4) an array of vectors val : val(i) is a vector, which contains values of the row i </p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8hxx_source.php#l00037">37</a> of file <a class="el" href="_matrix___array_complex_sparse_8hxx_source.php">Matrix_ArrayComplexSparse.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="a415bb8c2cb2379647933ddc5d9dbcc32"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::Matrix_ArrayComplexSparse" ref="a415bb8c2cb2379647933ddc5d9dbcc32" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Matrix_ArrayComplexSparse</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>Builds an empty matrix. </p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00038">38</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a410f415f4976918fc7678bc770611043"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::Matrix_ArrayComplexSparse" ref="a410f415f4976918fc7678bc770611043" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Matrix_ArrayComplexSparse</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Constructor. </p>
<p>Builds a i by j sparse matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00054">54</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="a889d3baa1c4aa6a96b83ba8ca7b4cf4a"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::Assemble" ref="a889d3baa1c4aa6a96b83ba8ca7b4cf4a" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Assemble </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Assembles the matrix. </p>
<p>All the row numbers are sorted. If same row numbers exist, values are added. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>If you are using the methods AddInteraction/AddInteractions, you don't need to call that method. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01234">1234</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0ee918c8c04a1a9434e157c15fdeb596"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::Clear" ref="a0ee918c8c04a1a9434e157c15fdeb596" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Clear </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the matrix. </p>
<p>This methods is equivalent to the destructor. On exit, the matrix is empty (0 by 0). </p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00081">81</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a08a5f1de809bbb4565e90d954ca053f6"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::Fill" ref="a08a5f1de809bbb4565e90d954ca053f6" args="(const complex&lt; T0 &gt; &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allo &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allo &gt;::Fill </td>
          <td>(</td>
          <td class="paramtype">const complex&lt; T0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Non-zero entries are set to a given value x. </p>
<p>real non-zero entries are set to real(x) whereas imaginary non-zero entries are set to imag(x) </p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01294">1294</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad840efb36621d28aa9726088cfc29a29"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::Get" ref="ad840efb36621d28aa9726088cfc29a29" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">complex&lt; T &gt; &amp; <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Get </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Unavailable access method. </p>
<p>This method is declared for consistency with other classes, but it is not defined because no reference can possibly be returned. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Raises an exception. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00428">428</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a17a0b2e40258b30da1f0036c2a85c357"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::Get" ref="a17a0b2e40258b30da1f0036c2a85c357" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const complex&lt; T &gt; &amp; <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Get </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Unavailable access method. </p>
<p>This method is declared for consistency with other classes, but it is not defined because no reference can possibly be returned. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Raises an exception. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00444">444</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2e5c6c10af220eea29f9c1565b14a93f"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::GetDataSize" ref="a2e5c6c10af220eea29f9c1565b14a93f" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetDataSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored in memory (real+imaginary part). </p>
<p>Returns the number of elements stored in memory, i.e. the number of non-zero entries. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements stored in memory. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00289">289</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a644eff8b81954e1f43c4f9321a866903"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::GetImag" ref="a644eff8b81954e1f43c4f9321a866903" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T &amp; <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns acces to imaginary part of A(i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>imaginary part of A(i, j) </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a913307297605f734e98ba7c7e870ae39">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#affbf4d5e0fb00ec74943e47af3859d87">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00549">549</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3233d9091b1fe40403f0e1779f260033"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::GetImag" ref="a3233d9091b1fe40403f0e1779f260033" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const T &amp; <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns acces to imaginary part of A(i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>imaginary part of A(i, j) </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#af4a356b6428d68d9890b96494ef89cc0">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#af7fa7341f09012e5f0ab244a40ab0ca2">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00564">564</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a578281c64cbe05a542a6b0e9642b326a"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::GetImagData" ref="a578281c64cbe05a542a6b0e9642b326a" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T * <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImagData </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns values of non-zero entries of a row (imaginary part). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The array of values of non-zero entries of row i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00342">342</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad54321edab8e2633985e198f1a576340"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::GetImagDataSize" ref="ad54321edab8e2633985e198f1a576340" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImagDataSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored in memory (imaginary part). </p>
<p>Returns the number of elements stored in memory, i.e. the number of non-zero entries. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements stored in memory. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00275">275</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0a423d7e93a0ad9c19ed6de2b1052dca"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::GetImagInd" ref="a0a423d7e93a0ad9c19ed6de2b1052dca" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int * <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImagInd </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns column indices of non-zero entries in row (imaginary part). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>the array of column indices of non-zero entries of row i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00330">330</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a33ebac042eda90796d7f674c250e85be"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::GetImagNonZeros" ref="a33ebac042eda90796d7f674c250e85be" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetImagNonZeros </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of non-zero elements (imaginary part). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of non-zero elements for imaginary part of matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00243">243</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a35ea621d2e5351259ce5bca87febc821"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::GetM" ref="a35ea621d2e5351259ce5bca87febc821" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the number of rows. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00171">171</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab3c6aad66c9dc6ad9addfb7de9a4ebdd"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::GetM" ref="ab3c6aad66c9dc6ad9addfb7de9a4ebdd" args="(const SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows of the possibly-transposed matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00196">196</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae413915ed95545b40df8b43737fa43c1"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::GetN" ref="ae413915ed95545b40df8b43737fa43c1" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the number of columns. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00183">183</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a53e20518a8557211c89ca8ad0d607825"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::GetN" ref="a53e20518a8557211c89ca8ad0d607825" args="(const SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns of the possibly-transposed matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00212">212</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac91be8b2868c034acbf161a8b3af9598"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::GetReal" ref="ac91be8b2868c034acbf161a8b3af9598" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T &amp; <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns acces to real part of A(i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>real part of A(i, j) </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a9a8f3daf0ad036985c22c4772b1477aa">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a85eba6f5bb491fd35b28e5753f48eace">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00519">519</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae562065dff1e5df2ec2516bf7f4f56ae"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::GetReal" ref="ae562065dff1e5df2ec2516bf7f4f56ae" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const T &amp; <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns acces to real part of A(i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>real part of A(i, j) </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a20616f82c194e42a610e3e48e63d5a40">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a39ca8957207e6102cba15d2f0d7f47fb">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00534">534</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aaa912365c7b8c191cf8b3fb2e2da4789"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::GetRealData" ref="aaa912365c7b8c191cf8b3fb2e2da4789" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T * <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetRealData </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns values of non-zero entries of a row (real part). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The array of values of non-zero entries of row i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00315">315</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa40775992c738c7fd1e9543bfddb2694"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::GetRealDataSize" ref="aa40775992c738c7fd1e9543bfddb2694" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetRealDataSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored in memory (real part). </p>
<p>Returns the number of elements stored in memory, i.e. the number of non-zero entries. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements stored in memory. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00261">261</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="afae45969eda952b43748cf7756f0e6d6"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::GetRealInd" ref="afae45969eda952b43748cf7756f0e6d6" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int * <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetRealInd </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns column indices of non-zero entries in row (real part). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The array of column indices of non-zero entries of row i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00303">303</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a11efc1bee6a95da705238628d7e3d958"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::GetRealNonZeros" ref="a11efc1bee6a95da705238628d7e3d958" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetRealNonZeros </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of non-zero elements (real part). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of non-zero elements for real part of matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00227">227</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aedba64fa9b01bce6bb75e9e5a14b26c5"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::IndexImag" ref="aedba64fa9b01bce6bb75e9e5a14b26c5" args="(int num_row, int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::IndexImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns column number of j-th non-zero value of row i (imaginary part). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>local number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Column number of j-th non-zero entry of row i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00770">770</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a011b15c1909f712a693de10f2c0c2328"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::IndexImag" ref="a011b15c1909f712a693de10f2c0c2328" args="(int num_row, int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int &amp; <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::IndexImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns column number of j-th non-zero value of row i (imaginary part). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>local number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>column number of j-th non-zero entry of row i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00797">797</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2a3cd76154ac61a43afa42563805ce3b"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::IndexReal" ref="a2a3cd76154ac61a43afa42563805ce3b" args="(int num_row, int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::IndexReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns column number of j-th non-zero value of row i (real part). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>local number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Column number of j-th non-zero entry of row i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00662">662</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2fdb9482dad6378fb8ced1982b6860b1"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::IndexReal" ref="a2fdb9482dad6378fb8ced1982b6860b1" args="(int num_row, int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int &amp; <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::IndexReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns column number of j-th non-zero value of row i (real part). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>local number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>column number of j-th non-zero entry of row i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00689">689</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad5e87ca37baabfb8cdc6f841f5125d7d"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::NullifyImag" ref="ad5e87ca37baabfb8cdc6f841f5125d7d" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::NullifyImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears a row without releasing memory. </p>
<p>On exit, the row is empty and the memory has not been released. It is useful for low level manipulations on a <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> instance. </p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00865">865</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="adbdb03e02fd8617a23188d6bc3155ec0"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::NullifyImag" ref="adbdb03e02fd8617a23188d6bc3155ec0" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::NullifyImag </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the matrix without releasing memory. </p>
<p>On exit, the matrix is empty and the memory has not been released. It is useful for low level manipulations on a <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> instance. </p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00923">923</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9f0922d865cdd4a627912583a9c73151"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::NullifyReal" ref="a9f0922d865cdd4a627912583a9c73151" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::NullifyReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears a row without releasing memory. </p>
<p>On exit, the row is empty and the memory has not been released. It is useful for low level manipulations on a <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> instance. </p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00853">853</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aed1192a75fe2c7d19e545ff6054298e2"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::NullifyReal" ref="aed1192a75fe2c7d19e545ff6054298e2" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::NullifyReal </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the matrix without releasing memory. </p>
<p>On exit, the matrix is empty and the memory has not been released. It is useful for low level manipulations on a <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> instance. </p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00909">909</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a30be6306dbdd79b0a282ef99289b0c2b"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::operator()" ref="a30be6306dbdd79b0a282ef99289b0c2b" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">complex&lt; T &gt; <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ace196e6c26320571bf77a782833d7232">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a5fb8630efa21241d2517927eeb80de5e">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00364">364</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac24abb2e047eb8b91728023074b3fb13"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::Print" ref="ac24abb2e047eb8b91728023074b3fb13" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Print </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Displays the matrix on the standard output. </p>
<p>Displays elements on the standard output, in text format. Each row is displayed on a single line and elements of a row are delimited by tabulations. </p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00943">943</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a8743f281d37cc023cd3e628eba4644f0"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::Read" ref="a8743f281d37cc023cd3e628eba4644f0" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from an input stream. </p>
<p>Reads a matrix in binary format from an input stream. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>input stream </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01148">1148</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a58c2b7013db7765a01181f928833e530"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::Read" ref="a58c2b7013db7765a01181f928833e530" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from a file. </p>
<p>Reads a matrix stored in binary format in a file. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>input file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01123">1123</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab0d4c7f4d291599c0d9cb735dd7d9dc8"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::ReadText" ref="ab0d4c7f4d291599c0d9cb735dd7d9dc8" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from a file. </p>
<p>Reads the matrix from a file in text format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>input file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01191">1191</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a20de6511128d3ac679c885abcd5538d0"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::ReadText" ref="a20de6511128d3ac679c885abcd5538d0" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from an input stream. </p>
<p>Reads a matrix from a stream in text format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>input stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01216">1216</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad2348fb25206ef8516c7e835b7ce2287"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::Reallocate" ref="ad2348fb25206ef8516c7e835b7ce2287" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Reallocate </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates memory to resize the matrix. </p>
<p>On exit, the matrix is a i x j matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Data is lost. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00104">104</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9166689334cc2f731220f76b730d692f"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::Resize" ref="a9166689334cc2f731220f76b730d692f" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Resize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates additional memory to resize the matrix. </p>
<p>On exit, the matrix is a i x j matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Data is kept. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00127">127</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2dfca2deb2a823fc64dc51b7c0728f49"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::Set" ref="a2dfca2deb2a823fc64dc51b7c0728f49" args="(int i, int j, const complex&lt; T &gt; &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Set </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const complex&lt; T &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets element (i, j) of matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>x</em>&nbsp;</td><td>A(i, j) = x </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a6f67461ccf260c8bb9e9fd1fc40811e7">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a49ac0164990312cc9485abe5de3a1d77">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00579">579</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a8e3c2dfa0ea5aa57cd8607cca15562f2"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::SetImagData" ref="a8e3c2dfa0ea5aa57cd8607cca15562f2" args="(int, int, Vector&lt; T, VectSparse, Allocator &gt; *)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::SetImagData </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>m</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>n</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; *&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Redefines the imaginary part of the matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>m</em>&nbsp;</td><td>new number of rows. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>n</em>&nbsp;</td><td>new number of columns. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>array of sparse rows/columns. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00895">895</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a50622f29a8e112821c68e5a28236258f"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::SetImagData" ref="a50622f29a8e112821c68e5a28236258f" args="(int, int, T *, int *)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::SetImagData </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>n</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">T *&nbsp;</td>
          <td class="paramname"> <em>val</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int *&nbsp;</td>
          <td class="paramname"> <em>ind</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Redefines a row/column of the matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row/col number </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>n</em>&nbsp;</td><td>number of non-zero entries in the row </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>values </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>ind</em>&nbsp;</td><td>column numbers </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00841">841</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aab6c31240cd6dcbd442f9a29658f51ea"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::SetRealData" ref="aab6c31240cd6dcbd442f9a29658f51ea" args="(int, int, Vector&lt; T, VectSparse, Allocator &gt; *)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::SetRealData </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>m</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>n</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; *&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Redefines the real part of the matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>m</em>&nbsp;</td><td>new number of rows. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>n</em>&nbsp;</td><td>new number of columns. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>array of sparse rows/columns. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00879">879</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1ebb3dcbf317ec99e6463f0aafea10b9"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::SetRealData" ref="a1ebb3dcbf317ec99e6463f0aafea10b9" args="(int, int, T *, int *)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::SetRealData </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>n</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">T *&nbsp;</td>
          <td class="paramname"> <em>val</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int *&nbsp;</td>
          <td class="paramname"> <em>ind</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Redefines a row/column of the matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row/col number </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>n</em>&nbsp;</td><td>number of non-zero entries in the row </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>values </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>ind</em>&nbsp;</td><td>column numbers </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00826">826</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7eb05d6902b13cfb7cc4f17093fdc17e"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::Val" ref="a7eb05d6902b13cfb7cc4f17093fdc17e" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">complex&lt; T &gt; &amp; <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Val </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Unavailable access method. </p>
<p>This method is declared for consistency with other classes, but it is not defined because no reference can possibly be returned. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Raises an exception. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00396">396</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a994fbc16dd258d1609f7c7c582205e9f"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::Val" ref="a994fbc16dd258d1609f7c7c582205e9f" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const complex&lt; T &gt; &amp; <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Val </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Unavailable access method. </p>
<p>This method is declared for consistency with other classes, but it is not defined because no reference can possibly be returned. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Raises an exception. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00412">412</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0324d6e453ce4c18dbc84bdc48fb4d5f"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::ValImag" ref="a0324d6e453ce4c18dbc84bdc48fb4d5f" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const T &amp; <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ValImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns acces to imaginary part of A(i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>imaginary part of A(i, j) </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a113a4f9a03767b54cb38c34af0d03ff7">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a9798b4ecbfea2a0629c8b484d9be931a">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00504">504</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aee8de4c9149d47935bf84fc04727c7f2"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::ValImag" ref="aee8de4c9149d47935bf84fc04727c7f2" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T &amp; <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ValImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns acces to imaginary part of A(i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>imaginary part of A(i, j) </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a1b6113bbca815576f929be337af47483">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a8584e87cfb5ba75a746ec60a926f23a4">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00489">489</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2fa5f2b8aeb3c848f5749d113f93dce4"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::ValReal" ref="a2fa5f2b8aeb3c848f5749d113f93dce4" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const T &amp; <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ValReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns acces to real part of A(i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>real part of A(i, j) </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a69b5391fd7e911f3f3204bf627e1ed24">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a2ccbdf02898e8b22ce9b5fe51aad89e1">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00474">474</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="abdacf01dc354d0449b16b1d9e111ec54"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::ValReal" ref="abdacf01dc354d0449b16b1d9e111ec54" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T &amp; <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ValReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns acces to real part of A(i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>real part of A(i, j) </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a9edb81b7ae21d2118c6cc4d98058d731">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#abc507579e215d0fcd88b8d2bf8e710b9">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00459">459</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa2d9a2e69a269bf9d15b62d520cb560d"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::ValueImag" ref="aa2d9a2e69a269bf9d15b62d520cb560d" args="(int num_row, int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const T &amp; <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ValueImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns j-th non-zero value of row i (imaginary part). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>local number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>j-th non-zero entry of row i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00716">716</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab410f72a305938081fbc07e868eb161b"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::ValueImag" ref="ab410f72a305938081fbc07e868eb161b" args="(int num_row, int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T &amp; <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ValueImag </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns j-th non-zero value of row i (imaginary part). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>local number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>j-th non-zero entry of row i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00743">743</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a041c76033f939293aa871f2fe45ef22a"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::ValueReal" ref="a041c76033f939293aa871f2fe45ef22a" args="(int num_row, int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const T &amp; <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ValueReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns j-th non-zero value of row i (real part). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>local number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>j-th non-zero entry of row i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00608">608</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a11fe8648ddd3e6e339eb81735d7e544e"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::ValueReal" ref="a11fe8648ddd3e6e339eb81735d7e544e" args="(int num_row, int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T &amp; <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::ValueReal </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns j-th non-zero value of row i (real part). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>local number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>j-th non-zero entry of row i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00636">636</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0cc7af3e3d677e5cb6bd5f66d7965a6f"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::Write" ref="a0cc7af3e3d677e5cb6bd5f66d7965a6f" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix to an output stream. </p>
<p>Stores the matrix in a file in binary format. The number of rows (integer) and the number of columns (integer) are written and matrix elements are then written in the same order as in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01008">1008</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9b54a14b64f67242fe9367df4142bd7f"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::Write" ref="a9b54a14b64f67242fe9367df4142bd7f" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix in a file. </p>
<p>Stores the matrix in a file in binary format. The number of rows (integer) and the number of columns (integer) are written and matrix elements are then written in the same order as in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l00980">980</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a884e62c07aa29b59b259d232f7771c94"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::WriteText" ref="a884e62c07aa29b59b259d232f7771c94" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix to an output stream. </p>
<p>Stores the matrix in a file in ascii format. The entries are written in coordinate format (row column value). 1-index convention is used. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01064">1064</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7095c684ecdb9ef6e9fe9b84462e22ce"></a><!-- doxytag: member="Seldon::Matrix_ArrayComplexSparse::WriteText" ref="a7095c684ecdb9ef6e9fe9b84462e22ce" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse</a>&lt; T, Prop, Storage, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix in a file. </p>
<p>Stores the matrix in a file in ascii format. The entries are written in coordinate format (row column value). 1-index convention is used. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01039">1039</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>matrix_sparse/<a class="el" href="_matrix___array_complex_sparse_8hxx_source.php">Matrix_ArrayComplexSparse.hxx</a></li>
<li>matrix_sparse/<a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
