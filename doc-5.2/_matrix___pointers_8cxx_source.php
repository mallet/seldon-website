<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix/Matrix_Pointers.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_POINTERS_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;Matrix_Pointers.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="keyword">namespace </span>Seldon
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 
<a name="l00029"></a>00029   <span class="comment">/****************</span>
<a name="l00030"></a>00030 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00031"></a>00031 <span class="comment">   ****************/</span>
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00035"></a>00035 
<a name="l00038"></a>00038   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00039"></a>00039   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a5f99e5a5a2f9a8f8e7873f829d82ed33" title="Default constructor.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Matrix_Pointers</a>():
<a name="l00040"></a>00040     Matrix_Base&lt;T, Allocator&gt;()
<a name="l00041"></a>00041   {
<a name="l00042"></a>00042     me_ = NULL;
<a name="l00043"></a>00043   }
<a name="l00044"></a>00044 
<a name="l00045"></a>00045 
<a name="l00047"></a>00047 
<a name="l00051"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a70433b5aff5b052203136dd43783ac7d">00051</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00052"></a>00052   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a5f99e5a5a2f9a8f8e7873f829d82ed33" title="Default constructor.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00053"></a>00053 <a class="code" href="class_seldon_1_1_matrix___pointers.php#a5f99e5a5a2f9a8f8e7873f829d82ed33" title="Default constructor.">  ::Matrix_Pointers</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j): <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, j)
<a name="l00054"></a>00054   {
<a name="l00055"></a>00055 
<a name="l00056"></a>00056 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00057"></a>00057 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00058"></a>00058       {
<a name="l00059"></a>00059 <span class="preprocessor">#endif</span>
<a name="l00060"></a>00060 <span class="preprocessor"></span>
<a name="l00061"></a>00061         me_ = <span class="keyword">reinterpret_cast&lt;</span>pointer*<span class="keyword">&gt;</span>( calloc(Storage::GetFirst(i, j),
<a name="l00062"></a>00062                                                  <span class="keyword">sizeof</span>(pointer)) );
<a name="l00063"></a>00063 
<a name="l00064"></a>00064 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00065"></a>00065 <span class="preprocessor"></span>      }
<a name="l00066"></a>00066     <span class="keywordflow">catch</span> (...)
<a name="l00067"></a>00067       {
<a name="l00068"></a>00068         this-&gt;m_ = 0;
<a name="l00069"></a>00069         this-&gt;n_ = 0;
<a name="l00070"></a>00070         me_ = NULL;
<a name="l00071"></a>00071         this-&gt;data_ = NULL;
<a name="l00072"></a>00072       }
<a name="l00073"></a>00073     <span class="keywordflow">if</span> (me_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00074"></a>00074       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Pointers::Matrix_Pointers(int, int)&quot;</span>,
<a name="l00075"></a>00075                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate memory for a matrix of size &quot;</span>)
<a name="l00076"></a>00076                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(static_cast&lt;long int&gt;(i)
<a name="l00077"></a>00077                               * static_cast&lt;long int&gt;(j)
<a name="l00078"></a>00078                               * static_cast&lt;long int&gt;(<span class="keyword">sizeof</span>(T)))
<a name="l00079"></a>00079                      + <span class="stringliteral">&quot; bytes (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; x &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00080"></a>00080                      + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00081"></a>00081 <span class="preprocessor">#endif</span>
<a name="l00082"></a>00082 <span class="preprocessor"></span>
<a name="l00083"></a>00083 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00084"></a>00084 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00085"></a>00085       {
<a name="l00086"></a>00086 <span class="preprocessor">#endif</span>
<a name="l00087"></a>00087 <span class="preprocessor"></span>
<a name="l00088"></a>00088         this-&gt;data_ = this-&gt;allocator_.allocate(i * j, <span class="keyword">this</span>);
<a name="l00089"></a>00089 
<a name="l00090"></a>00090 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00091"></a>00091 <span class="preprocessor"></span>      }
<a name="l00092"></a>00092     <span class="keywordflow">catch</span> (...)
<a name="l00093"></a>00093       {
<a name="l00094"></a>00094         this-&gt;m_ = 0;
<a name="l00095"></a>00095         this-&gt;n_ = 0;
<a name="l00096"></a>00096         free(me_);
<a name="l00097"></a>00097         me_ = NULL;
<a name="l00098"></a>00098         this-&gt;data_ = NULL;
<a name="l00099"></a>00099       }
<a name="l00100"></a>00100     <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00101"></a>00101       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Pointers::Matrix_Pointers(int, int)&quot;</span>,
<a name="l00102"></a>00102                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate memory for a matrix of size &quot;</span>)
<a name="l00103"></a>00103                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(static_cast&lt;long int&gt;(i)
<a name="l00104"></a>00104                               * static_cast&lt;long int&gt;(j)
<a name="l00105"></a>00105                               * static_cast&lt;long int&gt;(<span class="keyword">sizeof</span>(T)))
<a name="l00106"></a>00106                      + <span class="stringliteral">&quot; bytes (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; x &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00107"></a>00107                      + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00108"></a>00108 <span class="preprocessor">#endif</span>
<a name="l00109"></a>00109 <span class="preprocessor"></span>
<a name="l00110"></a>00110     pointer ptr = this-&gt;data_;
<a name="l00111"></a>00111     <span class="keywordtype">int</span> lgth = Storage::GetSecond(i, j);
<a name="l00112"></a>00112     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; Storage::GetFirst(i, j); k++, ptr += lgth)
<a name="l00113"></a>00113       me_[k] = ptr;
<a name="l00114"></a>00114 
<a name="l00115"></a>00115   }
<a name="l00116"></a>00116 
<a name="l00117"></a>00117 
<a name="l00119"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#ad8f9783bed7ab3a8a3c781b8bab3ce62">00119</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00120"></a>00120   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a5f99e5a5a2f9a8f8e7873f829d82ed33" title="Default constructor.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00121"></a>00121 <a class="code" href="class_seldon_1_1_matrix___pointers.php#a5f99e5a5a2f9a8f8e7873f829d82ed33" title="Default constructor.">  ::Matrix_Pointers</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A):
<a name="l00122"></a>00122     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(A)
<a name="l00123"></a>00123   {
<a name="l00124"></a>00124     this-&gt;m_ = 0;
<a name="l00125"></a>00125     this-&gt;n_ = 0;
<a name="l00126"></a>00126     this-&gt;data_ = NULL;
<a name="l00127"></a>00127     this-&gt;me_ = NULL;
<a name="l00128"></a>00128 
<a name="l00129"></a>00129     this-&gt;Copy(A);
<a name="l00130"></a>00130   }
<a name="l00131"></a>00131 
<a name="l00132"></a>00132 
<a name="l00133"></a>00133   <span class="comment">/**************</span>
<a name="l00134"></a>00134 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00135"></a>00135 <span class="comment">   **************/</span>
<a name="l00136"></a>00136 
<a name="l00137"></a>00137 
<a name="l00139"></a>00139   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00140"></a>00140   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#adc705f0d17fc4d5676e39b1b8f2b08ca" title="Destructor.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::~Matrix_Pointers</a>()
<a name="l00141"></a>00141   {
<a name="l00142"></a>00142 
<a name="l00143"></a>00143 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00144"></a>00144 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00145"></a>00145       {
<a name="l00146"></a>00146 <span class="preprocessor">#endif</span>
<a name="l00147"></a>00147 <span class="preprocessor"></span>
<a name="l00148"></a>00148         <span class="keywordflow">if</span> (this-&gt;data_ != NULL)
<a name="l00149"></a>00149           {
<a name="l00150"></a>00150             this-&gt;allocator_.deallocate(this-&gt;data_, this-&gt;m_ * this-&gt;n_);
<a name="l00151"></a>00151             this-&gt;data_ = NULL;
<a name="l00152"></a>00152           }
<a name="l00153"></a>00153 
<a name="l00154"></a>00154 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00155"></a>00155 <span class="preprocessor"></span>      }
<a name="l00156"></a>00156     <span class="keywordflow">catch</span> (...)
<a name="l00157"></a>00157       {
<a name="l00158"></a>00158         this-&gt;data_ = NULL;
<a name="l00159"></a>00159       }
<a name="l00160"></a>00160 <span class="preprocessor">#endif</span>
<a name="l00161"></a>00161 <span class="preprocessor"></span>
<a name="l00162"></a>00162 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00163"></a>00163 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00164"></a>00164       {
<a name="l00165"></a>00165 <span class="preprocessor">#endif</span>
<a name="l00166"></a>00166 <span class="preprocessor"></span>
<a name="l00167"></a>00167         <span class="keywordflow">if</span> (me_ != NULL)
<a name="l00168"></a>00168           {
<a name="l00169"></a>00169             free(me_);
<a name="l00170"></a>00170             me_ = NULL;
<a name="l00171"></a>00171           }
<a name="l00172"></a>00172 
<a name="l00173"></a>00173 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00174"></a>00174 <span class="preprocessor"></span>      }
<a name="l00175"></a>00175     <span class="keywordflow">catch</span> (...)
<a name="l00176"></a>00176       {
<a name="l00177"></a>00177         this-&gt;m_ = 0;
<a name="l00178"></a>00178         this-&gt;n_ = 0;
<a name="l00179"></a>00179         me_ = NULL;
<a name="l00180"></a>00180       }
<a name="l00181"></a>00181 <span class="preprocessor">#endif</span>
<a name="l00182"></a>00182 <span class="preprocessor"></span>
<a name="l00183"></a>00183   }
<a name="l00184"></a>00184 
<a name="l00185"></a>00185 
<a name="l00187"></a>00187 
<a name="l00191"></a>00191   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00192"></a>00192   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a361729faf34aaedf14ad0b3f044e093c" title="Clears the matrix.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Clear</a>()
<a name="l00193"></a>00193   {
<a name="l00194"></a>00194     this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#adc705f0d17fc4d5676e39b1b8f2b08ca" title="Destructor.">~Matrix_Pointers</a>();
<a name="l00195"></a>00195     this-&gt;m_ = 0;
<a name="l00196"></a>00196     this-&gt;n_ = 0;
<a name="l00197"></a>00197   }
<a name="l00198"></a>00198 
<a name="l00199"></a>00199 
<a name="l00200"></a>00200   <span class="comment">/*******************</span>
<a name="l00201"></a>00201 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00202"></a>00202 <span class="comment">   *******************/</span>
<a name="l00203"></a>00203 
<a name="l00204"></a>00204 
<a name="l00206"></a>00206 
<a name="l00212"></a>00212   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00213"></a>00213   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a36ac8e7e9bee92ae266201dfb60bf32e" title="Returns the number of elements stored in memory.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::GetDataSize</a>()<span class="keyword"> const</span>
<a name="l00214"></a>00214 <span class="keyword">  </span>{
<a name="l00215"></a>00215     <span class="keywordflow">return</span> this-&gt;m_ * this-&gt;n_;
<a name="l00216"></a>00216   }
<a name="l00217"></a>00217 
<a name="l00218"></a>00218 
<a name="l00220"></a>00220 
<a name="l00225"></a>00225   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00226"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#adf9f21c9fda6a0761549aee70f555f43">00226</a>   <span class="keyword">typename</span>
<a name="l00227"></a>00227   <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::pointer</a>*
<a name="l00228"></a>00228   <a class="code" href="class_seldon_1_1_matrix___pointers.php#adf9f21c9fda6a0761549aee70f555f43" title="Returns the pointer &amp;#39;me_&amp;#39;.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::GetMe</a>()<span class="keyword"> const</span>
<a name="l00229"></a>00229 <span class="keyword">  </span>{
<a name="l00230"></a>00230     <span class="keywordflow">return</span> me_;
<a name="l00231"></a>00231   }
<a name="l00232"></a>00232 
<a name="l00233"></a>00233 
<a name="l00234"></a>00234   <span class="comment">/*********************</span>
<a name="l00235"></a>00235 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00236"></a>00236 <span class="comment">   *********************/</span>
<a name="l00237"></a>00237 
<a name="l00238"></a>00238 
<a name="l00240"></a>00240 
<a name="l00246"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a782143339736b347e5a1c7ecd4d0c793">00246</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00247"></a>00247   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a782143339736b347e5a1c7ecd4d0c793" title="Reallocates memory to resize the matrix.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00248"></a>00248 <a class="code" href="class_seldon_1_1_matrix___pointers.php#a782143339736b347e5a1c7ecd4d0c793" title="Reallocates memory to resize the matrix.">  ::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00249"></a>00249   {
<a name="l00250"></a>00250 
<a name="l00251"></a>00251     <span class="keywordflow">if</span> (i != this-&gt;m_ || j != this-&gt;n_)
<a name="l00252"></a>00252       {
<a name="l00253"></a>00253         this-&gt;m_ = i;
<a name="l00254"></a>00254         this-&gt;n_ = j;
<a name="l00255"></a>00255 
<a name="l00256"></a>00256 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00257"></a>00257 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l00258"></a>00258           {
<a name="l00259"></a>00259 <span class="preprocessor">#endif</span>
<a name="l00260"></a>00260 <span class="preprocessor"></span>
<a name="l00261"></a>00261             me_ = <span class="keyword">reinterpret_cast&lt;</span>pointer*<span class="keyword">&gt;</span>( realloc(me_,
<a name="l00262"></a>00262                                                       Storage::GetFirst(i, j)
<a name="l00263"></a>00263                                                       * <span class="keyword">sizeof</span>(pointer)) );
<a name="l00264"></a>00264 
<a name="l00265"></a>00265 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00266"></a>00266 <span class="preprocessor"></span>          }
<a name="l00267"></a>00267         <span class="keywordflow">catch</span> (...)
<a name="l00268"></a>00268           {
<a name="l00269"></a>00269             this-&gt;m_ = 0;
<a name="l00270"></a>00270             this-&gt;n_ = 0;
<a name="l00271"></a>00271             me_ = NULL;
<a name="l00272"></a>00272             this-&gt;data_ = NULL;
<a name="l00273"></a>00273           }
<a name="l00274"></a>00274         <span class="keywordflow">if</span> (me_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00275"></a>00275           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Pointers::Reallocate(int, int)&quot;</span>,
<a name="l00276"></a>00276                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to reallocate memory for&quot;</span>)
<a name="l00277"></a>00277                          + <span class="stringliteral">&quot; a matrix of size &quot;</span>
<a name="l00278"></a>00278                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(static_cast&lt;long int&gt;(i)
<a name="l00279"></a>00279                                   * static_cast&lt;long int&gt;(j)
<a name="l00280"></a>00280                                   * static_cast&lt;long int&gt;(<span class="keyword">sizeof</span>(T)))
<a name="l00281"></a>00281                          + <span class="stringliteral">&quot; bytes (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; x &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00282"></a>00282                          + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00283"></a>00283 <span class="preprocessor">#endif</span>
<a name="l00284"></a>00284 <span class="preprocessor"></span>
<a name="l00285"></a>00285 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00286"></a>00286 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l00287"></a>00287           {
<a name="l00288"></a>00288 <span class="preprocessor">#endif</span>
<a name="l00289"></a>00289 <span class="preprocessor"></span>
<a name="l00290"></a>00290             this-&gt;data_ =
<a name="l00291"></a>00291               <span class="keyword">reinterpret_cast&lt;</span>pointer<span class="keyword">&gt;</span>(this-&gt;allocator_
<a name="l00292"></a>00292                                         .reallocate(this-&gt;data_, i * j,
<a name="l00293"></a>00293                                                     <span class="keyword">this</span>));
<a name="l00294"></a>00294 
<a name="l00295"></a>00295 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00296"></a>00296 <span class="preprocessor"></span>          }
<a name="l00297"></a>00297         <span class="keywordflow">catch</span> (...)
<a name="l00298"></a>00298           {
<a name="l00299"></a>00299             this-&gt;m_ = 0;
<a name="l00300"></a>00300             this-&gt;n_ = 0;
<a name="l00301"></a>00301             free(me_);
<a name="l00302"></a>00302             me_ = NULL;
<a name="l00303"></a>00303             this-&gt;data_ = NULL;
<a name="l00304"></a>00304           }
<a name="l00305"></a>00305         <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00306"></a>00306           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Pointers::Reallocate(int, int)&quot;</span>,
<a name="l00307"></a>00307                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to reallocate memory&quot;</span>)
<a name="l00308"></a>00308                          + <span class="stringliteral">&quot; for a matrix of size &quot;</span>
<a name="l00309"></a>00309                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(static_cast&lt;long int&gt;(i)
<a name="l00310"></a>00310                                   * static_cast&lt;long int&gt;(j)
<a name="l00311"></a>00311                                   * static_cast&lt;long int&gt;(<span class="keyword">sizeof</span>(T)))
<a name="l00312"></a>00312                          + <span class="stringliteral">&quot; bytes (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; x &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00313"></a>00313                          + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00314"></a>00314 <span class="preprocessor">#endif</span>
<a name="l00315"></a>00315 <span class="preprocessor"></span>
<a name="l00316"></a>00316         pointer ptr = this-&gt;data_;
<a name="l00317"></a>00317         <span class="keywordtype">int</span> lgth = Storage::GetSecond(i, j);
<a name="l00318"></a>00318         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; Storage::GetFirst(i, j); k++, ptr += lgth)
<a name="l00319"></a>00319           me_[k] = ptr;
<a name="l00320"></a>00320       }
<a name="l00321"></a>00321   }
<a name="l00322"></a>00322 
<a name="l00323"></a>00323 
<a name="l00326"></a>00326 
<a name="l00340"></a>00340   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00341"></a>00341   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00342"></a>00342 <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">  ::SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00343"></a>00343             <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00344"></a>00344             ::pointer data)
<a name="l00345"></a>00345   {
<a name="l00346"></a>00346     this-&gt;Clear();
<a name="l00347"></a>00347 
<a name="l00348"></a>00348     this-&gt;m_ = i;
<a name="l00349"></a>00349     this-&gt;n_ = j;
<a name="l00350"></a>00350 
<a name="l00351"></a>00351 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00352"></a>00352 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00353"></a>00353       {
<a name="l00354"></a>00354 <span class="preprocessor">#endif</span>
<a name="l00355"></a>00355 <span class="preprocessor"></span>
<a name="l00356"></a>00356         me_ = <span class="keyword">reinterpret_cast&lt;</span>pointer*<span class="keyword">&gt;</span>( calloc(Storage::GetFirst(i, j),
<a name="l00357"></a>00357                                                  <span class="keyword">sizeof</span>(pointer)) );
<a name="l00358"></a>00358 
<a name="l00359"></a>00359 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00360"></a>00360 <span class="preprocessor"></span>      }
<a name="l00361"></a>00361     <span class="keywordflow">catch</span> (...)
<a name="l00362"></a>00362       {
<a name="l00363"></a>00363         this-&gt;m_ = 0;
<a name="l00364"></a>00364         this-&gt;n_ = 0;
<a name="l00365"></a>00365         me_ = NULL;
<a name="l00366"></a>00366         this-&gt;data_ = NULL;
<a name="l00367"></a>00367         <span class="keywordflow">return</span>;
<a name="l00368"></a>00368       }
<a name="l00369"></a>00369     <span class="keywordflow">if</span> (me_ == NULL)
<a name="l00370"></a>00370       {
<a name="l00371"></a>00371         this-&gt;m_ = 0;
<a name="l00372"></a>00372         this-&gt;n_ = 0;
<a name="l00373"></a>00373         this-&gt;data_ = NULL;
<a name="l00374"></a>00374         <span class="keywordflow">return</span>;
<a name="l00375"></a>00375       }
<a name="l00376"></a>00376 <span class="preprocessor">#endif</span>
<a name="l00377"></a>00377 <span class="preprocessor"></span>
<a name="l00378"></a>00378     this-&gt;data_ = data;
<a name="l00379"></a>00379 
<a name="l00380"></a>00380     pointer ptr = this-&gt;data_;
<a name="l00381"></a>00381     <span class="keywordtype">int</span> lgth = Storage::GetSecond(i, j);
<a name="l00382"></a>00382     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; Storage::GetFirst(i, j); k++, ptr += lgth)
<a name="l00383"></a>00383       me_[k] = ptr;
<a name="l00384"></a>00384   }
<a name="l00385"></a>00385 
<a name="l00386"></a>00386 
<a name="l00388"></a>00388 
<a name="l00393"></a>00393   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00394"></a>00394   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a2898e791b8d8e9b282bc36a59da5a0ec" title="Clears the matrix without releasing memory.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Nullify</a>()
<a name="l00395"></a>00395   {
<a name="l00396"></a>00396     this-&gt;m_ = 0;
<a name="l00397"></a>00397     this-&gt;n_ = 0;
<a name="l00398"></a>00398 
<a name="l00399"></a>00399 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00400"></a>00400 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00401"></a>00401       {
<a name="l00402"></a>00402 <span class="preprocessor">#endif</span>
<a name="l00403"></a>00403 <span class="preprocessor"></span>
<a name="l00404"></a>00404         <span class="keywordflow">if</span> (me_ != NULL)
<a name="l00405"></a>00405           {
<a name="l00406"></a>00406             free(me_);
<a name="l00407"></a>00407             me_ = NULL;
<a name="l00408"></a>00408           }
<a name="l00409"></a>00409 
<a name="l00410"></a>00410 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00411"></a>00411 <span class="preprocessor"></span>      }
<a name="l00412"></a>00412     <span class="keywordflow">catch</span> (...)
<a name="l00413"></a>00413       {
<a name="l00414"></a>00414         this-&gt;m_ = 0;
<a name="l00415"></a>00415         this-&gt;n_ = 0;
<a name="l00416"></a>00416         me_ = NULL;
<a name="l00417"></a>00417       }
<a name="l00418"></a>00418 <span class="preprocessor">#endif</span>
<a name="l00419"></a>00419 <span class="preprocessor"></span>
<a name="l00420"></a>00420     this-&gt;data_ = NULL;
<a name="l00421"></a>00421   }
<a name="l00422"></a>00422 
<a name="l00423"></a>00423 
<a name="l00425"></a>00425 
<a name="l00432"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#ae5cee7b46956ee19dbbb3308613378ec">00432</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00433"></a>00433   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#ae5cee7b46956ee19dbbb3308613378ec" title="Reallocates memory to resize the matrix and keeps previous entries.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00434"></a>00434 <a class="code" href="class_seldon_1_1_matrix___pointers.php#ae5cee7b46956ee19dbbb3308613378ec" title="Reallocates memory to resize the matrix and keeps previous entries.">  ::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00435"></a>00435   {
<a name="l00436"></a>00436 
<a name="l00437"></a>00437     <span class="keywordflow">if</span> (i == this-&gt;m_ &amp;&amp; j == this-&gt;n_)
<a name="l00438"></a>00438       <span class="keywordflow">return</span>;
<a name="l00439"></a>00439 
<a name="l00440"></a>00440     <span class="comment">// Storing the old values of the matrix.</span>
<a name="l00441"></a>00441     <span class="keywordtype">int</span> iold = Storage::GetFirst(this-&gt;m_, this-&gt;n_);
<a name="l00442"></a>00442     <span class="keywordtype">int</span> jold = Storage::GetSecond(this-&gt;m_, this-&gt;n_);
<a name="l00443"></a>00443     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;value_type, VectFull, Allocator&gt;</a> xold(this-&gt;GetDataSize());
<a name="l00444"></a>00444     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; this-&gt;GetDataSize(); k++)
<a name="l00445"></a>00445       xold(k) = this-&gt;data_[k];
<a name="l00446"></a>00446 
<a name="l00447"></a>00447     <span class="comment">// Reallocation.</span>
<a name="l00448"></a>00448     <span class="keywordtype">int</span> inew = Storage::GetFirst(i, j);
<a name="l00449"></a>00449     <span class="keywordtype">int</span> jnew = Storage::GetSecond(i, j);
<a name="l00450"></a>00450     this-&gt;Reallocate(i,j);
<a name="l00451"></a>00451 
<a name="l00452"></a>00452     <span class="comment">// Filling the matrix with its old values.</span>
<a name="l00453"></a>00453     <span class="keywordtype">int</span> imin = min(iold, inew), jmin = min(jold, jnew);
<a name="l00454"></a>00454     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; imin; k++)
<a name="l00455"></a>00455       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> l = 0; l &lt; jmin; l++)
<a name="l00456"></a>00456         this-&gt;data_[k*jnew+l] = xold(l+jold*k);
<a name="l00457"></a>00457   }
<a name="l00458"></a>00458 
<a name="l00459"></a>00459 
<a name="l00460"></a>00460   <span class="comment">/**********************************</span>
<a name="l00461"></a>00461 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l00462"></a>00462 <span class="comment">   **********************************/</span>
<a name="l00463"></a>00463 
<a name="l00464"></a>00464 
<a name="l00466"></a>00466 
<a name="l00472"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a859b5a76ca8ff32246c610145151ebac">00472</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00473"></a>00473   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00474"></a>00474   <a class="code" href="class_seldon_1_1_matrix___pointers.php#a859b5a76ca8ff32246c610145151ebac" title="Access operator.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00475"></a>00475   {
<a name="l00476"></a>00476 
<a name="l00477"></a>00477 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00478"></a>00478 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00479"></a>00479       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Pointers::operator()&quot;</span>,
<a name="l00480"></a>00480                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00481"></a>00481                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00482"></a>00482                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00483"></a>00483     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00484"></a>00484       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Pointers::operator()&quot;</span>,
<a name="l00485"></a>00485                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00486"></a>00486                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00487"></a>00487                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00488"></a>00488 <span class="preprocessor">#endif</span>
<a name="l00489"></a>00489 <span class="preprocessor"></span>
<a name="l00490"></a>00490     <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00491"></a>00491   }
<a name="l00492"></a>00492 
<a name="l00493"></a>00493 
<a name="l00495"></a>00495 
<a name="l00501"></a>00501   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00502"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a1152a779c807df6022715b8635be7bf7">00502</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00503"></a>00503   ::const_reference <a class="code" href="class_seldon_1_1_matrix___pointers.php#a859b5a76ca8ff32246c610145151ebac" title="Access operator.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00504"></a>00504 <a class="code" href="class_seldon_1_1_matrix___pointers.php#a859b5a76ca8ff32246c610145151ebac" title="Access operator.">  ::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00505"></a>00505 <span class="keyword">  </span>{
<a name="l00506"></a>00506 
<a name="l00507"></a>00507 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00508"></a>00508 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00509"></a>00509       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Pointers::operator()&quot;</span>,
<a name="l00510"></a>00510                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00511"></a>00511                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00512"></a>00512                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00513"></a>00513     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00514"></a>00514       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Pointers::operator()&quot;</span>,
<a name="l00515"></a>00515                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00516"></a>00516                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00517"></a>00517                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00518"></a>00518 <span class="preprocessor">#endif</span>
<a name="l00519"></a>00519 <span class="preprocessor"></span>
<a name="l00520"></a>00520     <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00521"></a>00521   }
<a name="l00522"></a>00522 
<a name="l00523"></a>00523 
<a name="l00525"></a>00525 
<a name="l00531"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#afca09e0690510a4a40fe022e62677b6e">00531</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00532"></a>00532   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00533"></a>00533   <a class="code" href="class_seldon_1_1_matrix___pointers.php#afca09e0690510a4a40fe022e62677b6e" title="Access operator.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00534"></a>00534   {
<a name="l00535"></a>00535 
<a name="l00536"></a>00536 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00537"></a>00537 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00538"></a>00538       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Pointers::Val(int, int)&quot;</span>,
<a name="l00539"></a>00539                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00540"></a>00540                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00541"></a>00541     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00542"></a>00542       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Pointers::Val(int, int)&quot;</span>,
<a name="l00543"></a>00543                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00544"></a>00544                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00545"></a>00545 <span class="preprocessor">#endif</span>
<a name="l00546"></a>00546 <span class="preprocessor"></span>
<a name="l00547"></a>00547     <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00548"></a>00548   }
<a name="l00549"></a>00549 
<a name="l00550"></a>00550 
<a name="l00552"></a>00552 
<a name="l00558"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#ab39472ce33509eee84f5f9b078d38a96">00558</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00559"></a>00559   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00560"></a>00560   <a class="code" href="class_seldon_1_1_matrix___pointers.php#ab39472ce33509eee84f5f9b078d38a96" title="Access operator.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00561"></a>00561   {
<a name="l00562"></a>00562     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#afca09e0690510a4a40fe022e62677b6e" title="Access operator.">Val</a>(i, j);
<a name="l00563"></a>00563   }
<a name="l00564"></a>00564 
<a name="l00565"></a>00565 
<a name="l00567"></a>00567 
<a name="l00573"></a>00573   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00574"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a3d83977c9b496a6c1745a8e41f79af5c">00574</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00575"></a>00575   ::const_reference
<a name="l00576"></a>00576   <a class="code" href="class_seldon_1_1_matrix___pointers.php#afca09e0690510a4a40fe022e62677b6e" title="Access operator.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00577"></a>00577 <span class="keyword">  </span>{
<a name="l00578"></a>00578 
<a name="l00579"></a>00579 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00580"></a>00580 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00581"></a>00581       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Pointers::Val(int, int) const&quot;</span>,
<a name="l00582"></a>00582                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00583"></a>00583                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00584"></a>00584     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00585"></a>00585       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Pointers::Val(int, int) const&quot;</span>,
<a name="l00586"></a>00586                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00587"></a>00587                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00588"></a>00588 <span class="preprocessor">#endif</span>
<a name="l00589"></a>00589 <span class="preprocessor"></span>
<a name="l00590"></a>00590     <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00591"></a>00591   }
<a name="l00592"></a>00592 
<a name="l00593"></a>00593 
<a name="l00595"></a>00595 
<a name="l00601"></a>00601   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00602"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a17902c9305fb18c9a40749340945f830">00602</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00603"></a>00603   ::const_reference
<a name="l00604"></a>00604   <a class="code" href="class_seldon_1_1_matrix___pointers.php#ab39472ce33509eee84f5f9b078d38a96" title="Access operator.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00605"></a>00605 <span class="keyword">  </span>{
<a name="l00606"></a>00606     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#afca09e0690510a4a40fe022e62677b6e" title="Access operator.">Val</a>(i, j);
<a name="l00607"></a>00607   }
<a name="l00608"></a>00608 
<a name="l00609"></a>00609 
<a name="l00611"></a>00611 
<a name="l00616"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#ac3be1736d3b8f78244483b879b72b255">00616</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00617"></a>00617   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00618"></a>00618   <a class="code" href="class_seldon_1_1_matrix___pointers.php#ac3be1736d3b8f78244483b879b72b255" title="Access to elements of the data array.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::operator[] </a>(<span class="keywordtype">int</span> i)
<a name="l00619"></a>00619   {
<a name="l00620"></a>00620 
<a name="l00621"></a>00621 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00622"></a>00622 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#a36ac8e7e9bee92ae266201dfb60bf32e" title="Returns the number of elements stored in memory.">GetDataSize</a>())
<a name="l00623"></a>00623       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Matrix_Pointers::operator[] (int)&quot;</span>,
<a name="l00624"></a>00624                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00625"></a>00625                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#a36ac8e7e9bee92ae266201dfb60bf32e" title="Returns the number of elements stored in memory.">GetDataSize</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00626"></a>00626                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00627"></a>00627 <span class="preprocessor">#endif</span>
<a name="l00628"></a>00628 <span class="preprocessor"></span>
<a name="l00629"></a>00629     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00630"></a>00630   }
<a name="l00631"></a>00631 
<a name="l00632"></a>00632 
<a name="l00634"></a>00634 
<a name="l00639"></a>00639   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00640"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#af98cb1a88c5cd8675ce73ccff9f37db9">00640</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00641"></a>00641   ::const_reference
<a name="l00642"></a>00642   <a class="code" href="class_seldon_1_1_matrix___pointers.php#ac3be1736d3b8f78244483b879b72b255" title="Access to elements of the data array.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::operator[] </a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00643"></a>00643 <span class="keyword">  </span>{
<a name="l00644"></a>00644 
<a name="l00645"></a>00645 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00646"></a>00646 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#a36ac8e7e9bee92ae266201dfb60bf32e" title="Returns the number of elements stored in memory.">GetDataSize</a>())
<a name="l00647"></a>00647       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Matrix_Pointers::operator[] (int) const&quot;</span>,
<a name="l00648"></a>00648                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00649"></a>00649                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#a36ac8e7e9bee92ae266201dfb60bf32e" title="Returns the number of elements stored in memory.">GetDataSize</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00650"></a>00650                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00651"></a>00651 <span class="preprocessor">#endif</span>
<a name="l00652"></a>00652 <span class="preprocessor"></span>
<a name="l00653"></a>00653     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00654"></a>00654   }
<a name="l00655"></a>00655 
<a name="l00656"></a>00656 
<a name="l00658"></a>00658 
<a name="l00663"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a2f4238595987c707ac784f437b5e04e3">00663</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00664"></a>00664   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a2f4238595987c707ac784f437b5e04e3" title="Sets an element of the matrix.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00665"></a>00665 <a class="code" href="class_seldon_1_1_matrix___pointers.php#a2f4238595987c707ac784f437b5e04e3" title="Sets an element of the matrix.">  ::Set</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; val)
<a name="l00666"></a>00666   {
<a name="l00667"></a>00667     this-&gt;Val(i, j) = val;
<a name="l00668"></a>00668   }
<a name="l00669"></a>00669 
<a name="l00670"></a>00670 
<a name="l00672"></a>00672 
<a name="l00677"></a>00677   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00678"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#ab52557ba17dd1288794a2e6d472ebdc3">00678</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00679"></a>00679   <a class="code" href="class_seldon_1_1_matrix___pointers.php#ab52557ba17dd1288794a2e6d472ebdc3" title="Duplicates a matrix (assignment operator).">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00680"></a>00680 <a class="code" href="class_seldon_1_1_matrix___pointers.php#ab52557ba17dd1288794a2e6d472ebdc3" title="Duplicates a matrix (assignment operator).">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00681"></a>00681   {
<a name="l00682"></a>00682     this-&gt;Copy(A);
<a name="l00683"></a>00683 
<a name="l00684"></a>00684     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00685"></a>00685   }
<a name="l00686"></a>00686 
<a name="l00687"></a>00687 
<a name="l00689"></a>00689 
<a name="l00694"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a043929510eae4908750348b4862e190c">00694</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00695"></a>00695   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a043929510eae4908750348b4862e190c" title="Duplicates a matrix.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00696"></a>00696 <a class="code" href="class_seldon_1_1_matrix___pointers.php#a043929510eae4908750348b4862e190c" title="Duplicates a matrix.">  ::Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00697"></a>00697   {
<a name="l00698"></a>00698     this-&gt;Reallocate(A.<a class="code" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927" title="Returns the number of rows.">GetM</a>(), A.<a class="code" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984" title="Returns the number of columns.">GetN</a>());
<a name="l00699"></a>00699 
<a name="l00700"></a>00700     this-&gt;allocator_.memorycpy(this-&gt;data_, A.<a class="code" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a" title="Returns a pointer to the data array.">GetData</a>(), this-&gt;GetDataSize());
<a name="l00701"></a>00701   }
<a name="l00702"></a>00702 
<a name="l00703"></a>00703 
<a name="l00704"></a>00704   <span class="comment">/************************</span>
<a name="l00705"></a>00705 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l00706"></a>00706 <span class="comment">   ************************/</span>
<a name="l00707"></a>00707 
<a name="l00708"></a>00708 
<a name="l00710"></a>00710 
<a name="l00713"></a>00713   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00714"></a>00714   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#af93c5e73fc737a2f6e4f0b15164dd46d" title="Returns the leading dimension.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::GetLD</a>()<span class="keyword"> const</span>
<a name="l00715"></a>00715 <span class="keyword">  </span>{
<a name="l00716"></a>00716     <span class="keywordflow">return</span> Storage::GetSecond(this-&gt;m_, this-&gt;n_);
<a name="l00717"></a>00717   }
<a name="l00718"></a>00718 
<a name="l00719"></a>00719 
<a name="l00721"></a>00721 
<a name="l00725"></a>00725   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00726"></a>00726   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#add248e51552e9cec9a6214a8c1328ed0" title="Sets all elements to zero.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Zero</a>()
<a name="l00727"></a>00727   {
<a name="l00728"></a>00728     this-&gt;allocator_.memoryset(this-&gt;data_, <span class="keywordtype">char</span>(0),
<a name="l00729"></a>00729                                this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#a36ac8e7e9bee92ae266201dfb60bf32e" title="Returns the number of elements stored in memory.">GetDataSize</a>() * <span class="keyword">sizeof</span>(value_type));
<a name="l00730"></a>00730   }
<a name="l00731"></a>00731 
<a name="l00732"></a>00732 
<a name="l00734"></a>00734   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00735"></a>00735   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a35fc7bad6fe6cf252737916dfafae0ff" title="Sets the current matrix to the identity.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::SetIdentity</a>()
<a name="l00736"></a>00736   {
<a name="l00737"></a>00737     <a class="code" href="class_seldon_1_1_matrix___pointers.php#a2d3aeda0fa734518dd700f8568aaccde" title="Fills the matrix the matrix with 0, 1, 2, ...">Fill</a>(T(0));
<a name="l00738"></a>00738 
<a name="l00739"></a>00739     T one(1);
<a name="l00740"></a>00740     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; min(this-&gt;m_, this-&gt;n_); i++)
<a name="l00741"></a>00741       (*<span class="keyword">this</span>)(i,i) = one;
<a name="l00742"></a>00742   }
<a name="l00743"></a>00743 
<a name="l00744"></a>00744 
<a name="l00746"></a>00746 
<a name="l00750"></a>00750   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00751"></a>00751   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a2d3aeda0fa734518dd700f8568aaccde" title="Fills the matrix the matrix with 0, 1, 2, ...">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Fill</a>()
<a name="l00752"></a>00752   {
<a name="l00753"></a>00753     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#a36ac8e7e9bee92ae266201dfb60bf32e" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00754"></a>00754       this-&gt;data_[i] = i;
<a name="l00755"></a>00755   }
<a name="l00756"></a>00756 
<a name="l00757"></a>00757 
<a name="l00759"></a>00759 
<a name="l00762"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#aff7bbc1161aeb1ffcf7894e4b21a270f">00762</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00763"></a>00763   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00764"></a>00764   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a2d3aeda0fa734518dd700f8568aaccde" title="Fills the matrix the matrix with 0, 1, 2, ...">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Fill</a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00765"></a>00765   {
<a name="l00766"></a>00766     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#a36ac8e7e9bee92ae266201dfb60bf32e" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00767"></a>00767       this-&gt;data_[i] = x;
<a name="l00768"></a>00768   }
<a name="l00769"></a>00769 
<a name="l00770"></a>00770 
<a name="l00772"></a>00772 
<a name="l00775"></a>00775   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00776"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#ac18163af8e9002a336f71ad38fd69230">00776</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00777"></a>00777   <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00778"></a>00778   <a class="code" href="class_seldon_1_1_matrix___pointers.php#ab52557ba17dd1288794a2e6d472ebdc3" title="Duplicates a matrix (assignment operator).">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00779"></a>00779   {
<a name="l00780"></a>00780     this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#a2d3aeda0fa734518dd700f8568aaccde" title="Fills the matrix the matrix with 0, 1, 2, ...">Fill</a>(x);
<a name="l00781"></a>00781 
<a name="l00782"></a>00782     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00783"></a>00783   }
<a name="l00784"></a>00784 
<a name="l00785"></a>00785 
<a name="l00787"></a>00787 
<a name="l00790"></a>00790   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00791"></a>00791   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a656390ed0505de78a077a6533586428e" title="Fills the matrix randomly.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::FillRand</a>()
<a name="l00792"></a>00792   {
<a name="l00793"></a>00793     srand(time(NULL));
<a name="l00794"></a>00794     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#a36ac8e7e9bee92ae266201dfb60bf32e" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00795"></a>00795       this-&gt;data_[i] = rand();
<a name="l00796"></a>00796   }
<a name="l00797"></a>00797 
<a name="l00798"></a>00798 
<a name="l00800"></a>00800 
<a name="l00805"></a>00805   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00806"></a>00806   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a648346709f078652cb0a76f4001c901e" title="Displays the matrix on the standard output.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00807"></a>00807 <span class="keyword">  </span>{
<a name="l00808"></a>00808     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00809"></a>00809       {
<a name="l00810"></a>00810         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;n_; j++)
<a name="l00811"></a>00811           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00812"></a>00812         cout &lt;&lt; endl;
<a name="l00813"></a>00813       }
<a name="l00814"></a>00814   }
<a name="l00815"></a>00815 
<a name="l00816"></a>00816 
<a name="l00818"></a>00818 
<a name="l00829"></a>00829   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00830"></a>00830   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a648346709f078652cb0a76f4001c901e" title="Displays the matrix on the standard output.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Print</a>(<span class="keywordtype">int</span> a, <span class="keywordtype">int</span> b,
<a name="l00831"></a>00831                                                            <span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n)<span class="keyword"> const</span>
<a name="l00832"></a>00832 <span class="keyword">  </span>{
<a name="l00833"></a>00833     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = a; i &lt; min(this-&gt;m_, a+m); i++)
<a name="l00834"></a>00834       {
<a name="l00835"></a>00835         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = b; j &lt; min(this-&gt;n_, b+n); j++)
<a name="l00836"></a>00836           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00837"></a>00837         cout &lt;&lt; endl;
<a name="l00838"></a>00838       }
<a name="l00839"></a>00839   }
<a name="l00840"></a>00840 
<a name="l00841"></a>00841 
<a name="l00843"></a>00843 
<a name="l00851"></a>00851   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00852"></a>00852   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a648346709f078652cb0a76f4001c901e" title="Displays the matrix on the standard output.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Print</a>(<span class="keywordtype">int</span> l)<span class="keyword"> const</span>
<a name="l00853"></a>00853 <span class="keyword">  </span>{
<a name="l00854"></a>00854     <a class="code" href="class_seldon_1_1_matrix___pointers.php#a648346709f078652cb0a76f4001c901e" title="Displays the matrix on the standard output.">Print</a>(0, 0, l, l);
<a name="l00855"></a>00855   }
<a name="l00856"></a>00856 
<a name="l00857"></a>00857 
<a name="l00858"></a>00858   <span class="comment">/**************************</span>
<a name="l00859"></a>00859 <span class="comment">   * INPUT/OUTPUT FUNCTIONS *</span>
<a name="l00860"></a>00860 <span class="comment">   **************************/</span>
<a name="l00861"></a>00861 
<a name="l00862"></a>00862 <span class="preprocessor">#ifdef SELDON_WITH_HDF5</span>
<a name="l00863"></a>00863 <span class="preprocessor"></span>
<a name="l00864"></a>00864 
<a name="l00871"></a>00871  <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00872"></a>00872   <span class="keywordtype">void</span> Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;
<a name="l00873"></a>00873  ::WriteHDF5(<span class="keywordtype">string</span> FileName, <span class="keywordtype">string</span> group_name, <span class="keywordtype">string</span> dataset_name)<span class="keyword"> const</span>
<a name="l00874"></a>00874 <span class="keyword">  </span>{
<a name="l00875"></a>00875     <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Matrix_Pointers::WriteHDF5(string FileName)&quot;</span>,
<a name="l00876"></a>00876                   <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to write matrix in \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00877"></a>00877   }
<a name="l00878"></a>00878 <span class="preprocessor">#endif</span>
<a name="l00879"></a>00879 <span class="preprocessor"></span>
<a name="l00880"></a>00880 
<a name="l00882"></a>00882 
<a name="l00891"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a05252b9ce76816127365574721a97e44">00891</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00892"></a>00892   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a05252b9ce76816127365574721a97e44" title="Writes the matrix in a file.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00893"></a>00893 <a class="code" href="class_seldon_1_1_matrix___pointers.php#a05252b9ce76816127365574721a97e44" title="Writes the matrix in a file.">  ::Write</a>(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size)<span class="keyword"> const</span>
<a name="l00894"></a>00894 <span class="keyword">  </span>{
<a name="l00895"></a>00895     ofstream FileStream;
<a name="l00896"></a>00896     FileStream.open(FileName.c_str(), ofstream::binary);
<a name="l00897"></a>00897 
<a name="l00898"></a>00898 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00899"></a>00899 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00900"></a>00900     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00901"></a>00901       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::Write(string FileName)&quot;</span>,
<a name="l00902"></a>00902                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00903"></a>00903 <span class="preprocessor">#endif</span>
<a name="l00904"></a>00904 <span class="preprocessor"></span>
<a name="l00905"></a>00905     this-&gt;Write(FileStream, with_size);
<a name="l00906"></a>00906 
<a name="l00907"></a>00907     FileStream.close();
<a name="l00908"></a>00908   }
<a name="l00909"></a>00909 
<a name="l00910"></a>00910 
<a name="l00912"></a>00912 
<a name="l00921"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a150457071742a4d3a4bfd503a0e10c29">00921</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00922"></a>00922   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a05252b9ce76816127365574721a97e44" title="Writes the matrix in a file.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00923"></a>00923 <a class="code" href="class_seldon_1_1_matrix___pointers.php#a05252b9ce76816127365574721a97e44" title="Writes the matrix in a file.">  ::Write</a>(ostream&amp; FileStream, <span class="keywordtype">bool</span> with_size)<span class="keyword"> const</span>
<a name="l00924"></a>00924 <span class="keyword">  </span>{
<a name="l00925"></a>00925 
<a name="l00926"></a>00926 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00927"></a>00927 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00928"></a>00928     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00929"></a>00929       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::Write(ostream&amp; FileStream)&quot;</span>,
<a name="l00930"></a>00930                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00931"></a>00931 <span class="preprocessor">#endif</span>
<a name="l00932"></a>00932 <span class="preprocessor"></span>
<a name="l00933"></a>00933     <span class="keywordflow">if</span> (with_size)
<a name="l00934"></a>00934       {
<a name="l00935"></a>00935         FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l00936"></a>00936                          <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00937"></a>00937         FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;n_)),
<a name="l00938"></a>00938                          <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00939"></a>00939       }
<a name="l00940"></a>00940 
<a name="l00941"></a>00941     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l00942"></a>00942                      this-&gt;m_ * this-&gt;n_ * <span class="keyword">sizeof</span>(value_type));
<a name="l00943"></a>00943 
<a name="l00944"></a>00944 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00945"></a>00945 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00946"></a>00946     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00947"></a>00947       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::Write(ostream&amp; FileStream)&quot;</span>,
<a name="l00948"></a>00948                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l00949"></a>00949 <span class="preprocessor">#endif</span>
<a name="l00950"></a>00950 <span class="preprocessor"></span>
<a name="l00951"></a>00951   }
<a name="l00952"></a>00952 
<a name="l00953"></a>00953 
<a name="l00955"></a>00955 
<a name="l00962"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#aa1b41e8fe89480269acdc9b8cd36c21e">00962</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00963"></a>00963   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#aa1b41e8fe89480269acdc9b8cd36c21e" title="Writes the matrix in a file.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00964"></a>00964 <a class="code" href="class_seldon_1_1_matrix___pointers.php#aa1b41e8fe89480269acdc9b8cd36c21e" title="Writes the matrix in a file.">  ::WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00965"></a>00965 <span class="keyword">  </span>{
<a name="l00966"></a>00966     ofstream FileStream;
<a name="l00967"></a>00967     FileStream.precision(cout.precision());
<a name="l00968"></a>00968     FileStream.flags(cout.flags());
<a name="l00969"></a>00969     FileStream.open(FileName.c_str());
<a name="l00970"></a>00970 
<a name="l00971"></a>00971 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00972"></a>00972 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00973"></a>00973     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00974"></a>00974       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::WriteText(string FileName)&quot;</span>,
<a name="l00975"></a>00975                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00976"></a>00976 <span class="preprocessor">#endif</span>
<a name="l00977"></a>00977 <span class="preprocessor"></span>
<a name="l00978"></a>00978     this-&gt;WriteText(FileStream);
<a name="l00979"></a>00979 
<a name="l00980"></a>00980     FileStream.close();
<a name="l00981"></a>00981   }
<a name="l00982"></a>00982 
<a name="l00983"></a>00983 
<a name="l00985"></a>00985 
<a name="l00992"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#af1aa0dd65f43e9ab7fc47dddc38d2cd6">00992</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00993"></a>00993   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#aa1b41e8fe89480269acdc9b8cd36c21e" title="Writes the matrix in a file.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00994"></a>00994 <a class="code" href="class_seldon_1_1_matrix___pointers.php#aa1b41e8fe89480269acdc9b8cd36c21e" title="Writes the matrix in a file.">  ::WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00995"></a>00995 <span class="keyword">  </span>{
<a name="l00996"></a>00996 
<a name="l00997"></a>00997 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00998"></a>00998 <span class="preprocessor"></span>    <span class="comment">// Checks if the file is ready.</span>
<a name="l00999"></a>00999     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01000"></a>01000       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::WriteText(ostream&amp; FileStream)&quot;</span>,
<a name="l01001"></a>01001                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l01002"></a>01002 <span class="preprocessor">#endif</span>
<a name="l01003"></a>01003 <span class="preprocessor"></span>
<a name="l01004"></a>01004     <span class="keywordtype">int</span> i, j;
<a name="l01005"></a>01005     <span class="keywordflow">for</span> (i = 0; i &lt; this-&gt;GetM(); i++)
<a name="l01006"></a>01006       {
<a name="l01007"></a>01007         <span class="keywordflow">for</span> (j = 0; j &lt; this-&gt;GetN(); j++)
<a name="l01008"></a>01008           FileStream &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="charliteral">&#39;\t&#39;</span>;
<a name="l01009"></a>01009         FileStream &lt;&lt; endl;
<a name="l01010"></a>01010       }
<a name="l01011"></a>01011 
<a name="l01012"></a>01012 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01013"></a>01013 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l01014"></a>01014     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01015"></a>01015       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::WriteText(ostream&amp; FileStream)&quot;</span>,
<a name="l01016"></a>01016                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l01017"></a>01017 <span class="preprocessor">#endif</span>
<a name="l01018"></a>01018 <span class="preprocessor"></span>
<a name="l01019"></a>01019   }
<a name="l01020"></a>01020 
<a name="l01021"></a>01021 
<a name="l01023"></a>01023 
<a name="l01033"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a45b2cbff13b8da2a247def9e87e910a6">01033</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01034"></a>01034   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a45b2cbff13b8da2a247def9e87e910a6" title="Reads the matrix from a file.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01035"></a>01035 <a class="code" href="class_seldon_1_1_matrix___pointers.php#a45b2cbff13b8da2a247def9e87e910a6" title="Reads the matrix from a file.">  ::Read</a>(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size)
<a name="l01036"></a>01036   {
<a name="l01037"></a>01037     ifstream FileStream;
<a name="l01038"></a>01038     FileStream.open(FileName.c_str(), ifstream::binary);
<a name="l01039"></a>01039 
<a name="l01040"></a>01040 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01041"></a>01041 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01042"></a>01042     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01043"></a>01043       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::Read(string FileName)&quot;</span>,
<a name="l01044"></a>01044                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01045"></a>01045 <span class="preprocessor">#endif</span>
<a name="l01046"></a>01046 <span class="preprocessor"></span>
<a name="l01047"></a>01047     this-&gt;Read(FileStream, with_size);
<a name="l01048"></a>01048 
<a name="l01049"></a>01049     FileStream.close();
<a name="l01050"></a>01050   }
<a name="l01051"></a>01051 
<a name="l01052"></a>01052 
<a name="l01054"></a>01054 
<a name="l01064"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a697168859e965ebfe3e0c722544217de">01064</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01065"></a>01065   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a45b2cbff13b8da2a247def9e87e910a6" title="Reads the matrix from a file.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01066"></a>01066 <a class="code" href="class_seldon_1_1_matrix___pointers.php#a45b2cbff13b8da2a247def9e87e910a6" title="Reads the matrix from a file.">  ::Read</a>(istream&amp; FileStream, <span class="keywordtype">bool</span> with_size)
<a name="l01067"></a>01067   {
<a name="l01068"></a>01068 
<a name="l01069"></a>01069 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01070"></a>01070 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01071"></a>01071     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01072"></a>01072       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::Read(istream&amp; FileStream)&quot;</span>,
<a name="l01073"></a>01073                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l01074"></a>01074 <span class="preprocessor">#endif</span>
<a name="l01075"></a>01075 <span class="preprocessor"></span>
<a name="l01076"></a>01076     <span class="keywordflow">if</span> (with_size)
<a name="l01077"></a>01077       {
<a name="l01078"></a>01078         <span class="keywordtype">int</span> new_m, new_n;
<a name="l01079"></a>01079         FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;new_m), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01080"></a>01080         FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;new_n), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01081"></a>01081         this-&gt;Reallocate(new_m, new_n);
<a name="l01082"></a>01082       }
<a name="l01083"></a>01083 
<a name="l01084"></a>01084     FileStream.read(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l01085"></a>01085                     this-&gt;GetM() * this-&gt;GetN() * <span class="keyword">sizeof</span>(value_type));
<a name="l01086"></a>01086 
<a name="l01087"></a>01087 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01088"></a>01088 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l01089"></a>01089     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01090"></a>01090       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::Read(istream&amp; FileStream)&quot;</span>,
<a name="l01091"></a>01091                     <span class="stringliteral">&quot;Input operation failed.&quot;</span>);
<a name="l01092"></a>01092 <span class="preprocessor">#endif</span>
<a name="l01093"></a>01093 <span class="preprocessor"></span>
<a name="l01094"></a>01094   }
<a name="l01095"></a>01095 
<a name="l01096"></a>01096 
<a name="l01098"></a>01098 
<a name="l01102"></a>01102   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01103"></a>01103   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a2ec5770914f507080dac5452b8ddaee5" title="Reads the matrix from a file.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::ReadText</a>(<span class="keywordtype">string</span> FileName)
<a name="l01104"></a>01104   {
<a name="l01105"></a>01105     ifstream FileStream;
<a name="l01106"></a>01106     FileStream.open(FileName.c_str());
<a name="l01107"></a>01107 
<a name="l01108"></a>01108 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01109"></a>01109 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01110"></a>01110     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01111"></a>01111       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(string FileName)&quot;</span>,
<a name="l01112"></a>01112                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01113"></a>01113 <span class="preprocessor">#endif</span>
<a name="l01114"></a>01114 <span class="preprocessor"></span>
<a name="l01115"></a>01115     this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#a2ec5770914f507080dac5452b8ddaee5" title="Reads the matrix from a file.">ReadText</a>(FileStream);
<a name="l01116"></a>01116 
<a name="l01117"></a>01117     FileStream.close();
<a name="l01118"></a>01118   }
<a name="l01119"></a>01119 
<a name="l01120"></a>01120 
<a name="l01122"></a>01122 
<a name="l01126"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a96dc6a908187241ae7fea78ed2f12e93">01126</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01127"></a>01127   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a2ec5770914f507080dac5452b8ddaee5" title="Reads the matrix from a file.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01128"></a>01128 <a class="code" href="class_seldon_1_1_matrix___pointers.php#a2ec5770914f507080dac5452b8ddaee5" title="Reads the matrix from a file.">  ::ReadText</a>(istream&amp; FileStream)
<a name="l01129"></a>01129   {
<a name="l01130"></a>01130     <span class="comment">// Clears the matrix.</span>
<a name="l01131"></a>01131     Clear();
<a name="l01132"></a>01132 
<a name="l01133"></a>01133 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01134"></a>01134 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01135"></a>01135     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01136"></a>01136       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(istream&amp; FileStream)&quot;</span>,
<a name="l01137"></a>01137                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l01138"></a>01138 <span class="preprocessor">#endif</span>
<a name="l01139"></a>01139 <span class="preprocessor"></span>
<a name="l01140"></a>01140     <span class="comment">// Reads the first line.</span>
<a name="l01141"></a>01141     <span class="keywordtype">string</span> line;
<a name="l01142"></a>01142     getline(FileStream, line);
<a name="l01143"></a>01143     <span class="keywordflow">if</span> (FileStream.fail())
<a name="l01144"></a>01144       <span class="comment">// Is the file empty?</span>
<a name="l01145"></a>01145       <span class="keywordflow">return</span>;
<a name="l01146"></a>01146 
<a name="l01147"></a>01147     <span class="comment">// Converts the first line into a vector.</span>
<a name="l01148"></a>01148     istringstream line_stream(line);
<a name="l01149"></a>01149     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> first_row;
<a name="l01150"></a>01150     first_row.ReadText(line_stream);
<a name="l01151"></a>01151 
<a name="l01152"></a>01152     <span class="comment">// Now reads all other rows, and puts them in a single vector.</span>
<a name="l01153"></a>01153     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> other_row;
<a name="l01154"></a>01154     other_row.ReadText(FileStream);
<a name="l01155"></a>01155 
<a name="l01156"></a>01156     <span class="comment">// Number of rows and columns.</span>
<a name="l01157"></a>01157     <span class="keywordtype">int</span> n = first_row.GetM();
<a name="l01158"></a>01158     <span class="keywordtype">int</span> m = 1 + other_row.GetM() / n;
<a name="l01159"></a>01159 
<a name="l01160"></a>01160 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01161"></a>01161 <span class="preprocessor"></span>    <span class="comment">// Checks that enough elements were read.</span>
<a name="l01162"></a>01162     <span class="keywordflow">if</span> (other_row.GetM() != (m - 1) * n)
<a name="l01163"></a>01163       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(istream&amp; FileStream)&quot;</span>,
<a name="l01164"></a>01164                     <span class="stringliteral">&quot;Not all rows have the same number of columns.&quot;</span>);
<a name="l01165"></a>01165 <span class="preprocessor">#endif</span>
<a name="l01166"></a>01166 <span class="preprocessor"></span>
<a name="l01167"></a>01167     this-&gt;Reallocate(m, n);
<a name="l01168"></a>01168     <span class="comment">// Fills the matrix.</span>
<a name="l01169"></a>01169     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l01170"></a>01170       this-&gt;Val(0, j) = first_row(j);
<a name="l01171"></a>01171 
<a name="l01172"></a>01172     <span class="keywordtype">int</span> k = 0;
<a name="l01173"></a>01173     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 1; i &lt; m; i++)
<a name="l01174"></a>01174       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l01175"></a>01175         this-&gt;Val(i, j) = other_row(k++);
<a name="l01176"></a>01176   }
<a name="l01177"></a>01177 
<a name="l01178"></a>01178 
<a name="l01180"></a>01180   <span class="comment">// MATRIX&lt;COLMAJOR&gt; //</span>
<a name="l01182"></a>01182 <span class="comment"></span>
<a name="l01183"></a>01183 
<a name="l01184"></a>01184   <span class="comment">/****************</span>
<a name="l01185"></a>01185 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01186"></a>01186 <span class="comment">   ****************/</span>
<a name="l01187"></a>01187 
<a name="l01188"></a>01188 
<a name="l01190"></a>01190 
<a name="l01193"></a>01193   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01194"></a>01194   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColMajor, Allocator&gt;::Matrix</a>():
<a name="l01195"></a>01195     <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_major.php">ColMajor</a>, Allocator&gt;()
<a name="l01196"></a>01196   {
<a name="l01197"></a>01197   }
<a name="l01198"></a>01198 
<a name="l01199"></a>01199 
<a name="l01201"></a>01201 
<a name="l01205"></a>01205   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01206"></a>01206   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php#aa20ef1ed118ce455350561b4e7446bbb" title="Default constructor.">Matrix&lt;T, Prop, ColMajor, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01207"></a>01207     Matrix_Pointers&lt;T, Prop, ColMajor, Allocator&gt;(i, j)
<a name="l01208"></a>01208   {
<a name="l01209"></a>01209   }
<a name="l01210"></a>01210 
<a name="l01211"></a>01211 
<a name="l01213"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php#a20c877edd580054313c623edfdaf8837">01213</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01214"></a>01214   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColMajor, Allocator&gt;</a>
<a name="l01215"></a>01215 <a class="code" href="class_seldon_1_1_matrix.php">  ::Matrix</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php" title="Column-major full-matrix class.">Matrix&lt;T, Prop, ColMajor, Allocator&gt;</a>&amp; A):
<a name="l01216"></a>01216     <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_major.php">ColMajor</a>, Allocator&gt;(A)
<a name="l01217"></a>01217   {
<a name="l01218"></a>01218   }
<a name="l01219"></a>01219 
<a name="l01220"></a>01220 
<a name="l01221"></a>01221   <span class="comment">/*****************</span>
<a name="l01222"></a>01222 <span class="comment">   * OTHER METHODS *</span>
<a name="l01223"></a>01223 <span class="comment">   *****************/</span>
<a name="l01224"></a>01224 
<a name="l01225"></a>01225 
<a name="l01227"></a>01227 
<a name="l01230"></a>01230   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01231"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php#a5f16442782b3155c49889aee1a6df7e1">01231</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01232"></a>01232   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php" title="Column-major full-matrix class.">Matrix&lt;T, Prop, ColMajor, Allocator&gt;</a>&amp;
<a name="l01233"></a>01233   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColMajor, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01234"></a>01234   {
<a name="l01235"></a>01235     this-&gt;Fill(x);
<a name="l01236"></a>01236 
<a name="l01237"></a>01237     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01238"></a>01238   }
<a name="l01239"></a>01239 
<a name="l01240"></a>01240 
<a name="l01242"></a>01242 
<a name="l01247"></a>01247   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01248"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php#a71b5c6a90688dd19e45d2b6f59369f14">01248</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php" title="Column-major full-matrix class.">Matrix&lt;T, Prop, ColMajor, Allocator&gt;</a>&amp;
<a name="l01249"></a>01249   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColMajor, Allocator&gt;</a>
<a name="l01250"></a>01250 <a class="code" href="class_seldon_1_1_matrix.php">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php" title="Column-major full-matrix class.">Matrix&lt;T, Prop, ColMajor, Allocator&gt;</a>&amp; A)
<a name="l01251"></a>01251   {
<a name="l01252"></a>01252     this-&gt;Copy(A);
<a name="l01253"></a>01253 
<a name="l01254"></a>01254     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01255"></a>01255   }
<a name="l01256"></a>01256 
<a name="l01257"></a>01257 
<a name="l01259"></a>01259 
<a name="l01262"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php#ac3a6b15a4c8e247460385cb9b924e9e9">01262</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l01263"></a>01263   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php" title="Column-major full-matrix class.">Matrix&lt;T, Prop, ColMajor, Allocator&gt;</a>&amp;
<a name="l01264"></a>01264   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColMajor, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; alpha)
<a name="l01265"></a>01265   {
<a name="l01266"></a>01266     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_ * this-&gt;n_; i++)
<a name="l01267"></a>01267       this-&gt;data_[i] *= alpha;
<a name="l01268"></a>01268 
<a name="l01269"></a>01269     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01270"></a>01270   }
<a name="l01271"></a>01271 
<a name="l01272"></a>01272 
<a name="l01274"></a>01274   <span class="comment">// MATRIX&lt;ROWMAJOR&gt; //</span>
<a name="l01276"></a>01276 <span class="comment"></span>
<a name="l01277"></a>01277 
<a name="l01278"></a>01278   <span class="comment">/****************</span>
<a name="l01279"></a>01279 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01280"></a>01280 <span class="comment">   ****************/</span>
<a name="l01281"></a>01281 
<a name="l01282"></a>01282 
<a name="l01284"></a>01284 
<a name="l01287"></a>01287   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01288"></a>01288   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowMajor, Allocator&gt;::Matrix</a>():
<a name="l01289"></a>01289     <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_major.php">RowMajor</a>, Allocator&gt;()
<a name="l01290"></a>01290   {
<a name="l01291"></a>01291   }
<a name="l01292"></a>01292 
<a name="l01293"></a>01293 
<a name="l01295"></a>01295 
<a name="l01299"></a>01299   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01300"></a>01300   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#ad6540bf8d5f0d58e12915162e1d6b852" title="Default constructor.">Matrix&lt;T, Prop, RowMajor, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01301"></a>01301     Matrix_Pointers&lt;T, Prop, RowMajor, Allocator&gt;(i, j)
<a name="l01302"></a>01302   {
<a name="l01303"></a>01303   }
<a name="l01304"></a>01304 
<a name="l01305"></a>01305 
<a name="l01307"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#a4951a370ce9457e8625cc5257a349f96">01307</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01308"></a>01308   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowMajor, Allocator&gt;</a>
<a name="l01309"></a>01309 <a class="code" href="class_seldon_1_1_matrix.php">  ::Matrix</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php" title="Row-major full-matrix class.">Matrix&lt;T, Prop, RowMajor, Allocator&gt;</a>&amp; A):
<a name="l01310"></a>01310     <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_major.php">RowMajor</a>, Allocator&gt;(A)
<a name="l01311"></a>01311   {
<a name="l01312"></a>01312   }
<a name="l01313"></a>01313 
<a name="l01314"></a>01314 
<a name="l01315"></a>01315   <span class="comment">/*****************</span>
<a name="l01316"></a>01316 <span class="comment">   * OTHER METHODS *</span>
<a name="l01317"></a>01317 <span class="comment">   *****************/</span>
<a name="l01318"></a>01318 
<a name="l01319"></a>01319 
<a name="l01321"></a>01321 
<a name="l01324"></a>01324   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01325"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#a3727350f7283b6e8e08ccb2f6733362b">01325</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01326"></a>01326   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php" title="Row-major full-matrix class.">Matrix&lt;T, Prop, RowMajor, Allocator&gt;</a>&amp;
<a name="l01327"></a>01327   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowMajor, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01328"></a>01328   {
<a name="l01329"></a>01329     this-&gt;Fill(x);
<a name="l01330"></a>01330 
<a name="l01331"></a>01331     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01332"></a>01332   }
<a name="l01333"></a>01333 
<a name="l01334"></a>01334 
<a name="l01336"></a>01336 
<a name="l01341"></a>01341   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01342"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#a11d3b562dc6263cad3299de5cbff269a">01342</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php" title="Row-major full-matrix class.">Matrix&lt;T, Prop, RowMajor, Allocator&gt;</a>&amp;
<a name="l01343"></a>01343   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowMajor, Allocator&gt;</a>
<a name="l01344"></a>01344 <a class="code" href="class_seldon_1_1_matrix.php">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php" title="Row-major full-matrix class.">Matrix&lt;T, Prop, RowMajor, Allocator&gt;</a>&amp; A)
<a name="l01345"></a>01345   {
<a name="l01346"></a>01346     this-&gt;Copy(A);
<a name="l01347"></a>01347 
<a name="l01348"></a>01348     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01349"></a>01349   }
<a name="l01350"></a>01350 
<a name="l01351"></a>01351 
<a name="l01353"></a>01353 
<a name="l01356"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#a1f8c93e3abfe109f258ab5b31e6382e4">01356</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l01357"></a>01357   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php" title="Row-major full-matrix class.">Matrix&lt;T, Prop, RowMajor, Allocator&gt;</a>&amp;
<a name="l01358"></a>01358   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowMajor, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; alpha)
<a name="l01359"></a>01359   {
<a name="l01360"></a>01360     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_*this-&gt;n_; i++)
<a name="l01361"></a>01361       this-&gt;data_[i] *= alpha;
<a name="l01362"></a>01362 
<a name="l01363"></a>01363     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01364"></a>01364   }
<a name="l01365"></a>01365 
<a name="l01366"></a>01366 
<a name="l01367"></a>01367 } <span class="comment">// namespace Seldon.</span>
<a name="l01368"></a>01368 
<a name="l01369"></a>01369 <span class="preprocessor">#define SELDON_FILE_MATRIX_POINTERS_CXX</span>
<a name="l01370"></a>01370 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
