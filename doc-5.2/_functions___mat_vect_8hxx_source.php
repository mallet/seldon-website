<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/basic_functions/Functions_MatVect.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2011 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_FUNCTIONS_MATVECT_HXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span><span class="preprocessor">#define SELDON_FILE_FUNCTIONS_MATVECT_HXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 
<a name="l00024"></a>00024 <span class="comment">/*</span>
<a name="l00025"></a>00025 <span class="comment">  Functions defined in this file:</span>
<a name="l00026"></a>00026 <span class="comment"></span>
<a name="l00027"></a>00027 <span class="comment">  M X -&gt; Y</span>
<a name="l00028"></a>00028 <span class="comment">  Mlt(M, X, Y)</span>
<a name="l00029"></a>00029 <span class="comment"></span>
<a name="l00030"></a>00030 <span class="comment">  alpha M X -&gt; Y</span>
<a name="l00031"></a>00031 <span class="comment">  Mlt(alpha, M, X, Y)</span>
<a name="l00032"></a>00032 <span class="comment"></span>
<a name="l00033"></a>00033 <span class="comment">  M X -&gt; Y</span>
<a name="l00034"></a>00034 <span class="comment">  M^T X -&gt; Y</span>
<a name="l00035"></a>00035 <span class="comment">  Mlt(Trans, M, X, Y)</span>
<a name="l00036"></a>00036 <span class="comment"></span>
<a name="l00037"></a>00037 <span class="comment">  alpha M X + beta Y -&gt; Y</span>
<a name="l00038"></a>00038 <span class="comment">  MltAdd(alpha, M, X, beta, Y)</span>
<a name="l00039"></a>00039 <span class="comment"></span>
<a name="l00040"></a>00040 <span class="comment">  Gauss(M, X)</span>
<a name="l00041"></a>00041 <span class="comment"></span>
<a name="l00042"></a>00042 <span class="comment">  GaussSeidel(M, X, Y, iter)</span>
<a name="l00043"></a>00043 <span class="comment"></span>
<a name="l00044"></a>00044 <span class="comment">  SOR(M, X, Y, omega, iter)</span>
<a name="l00045"></a>00045 <span class="comment"></span>
<a name="l00046"></a>00046 <span class="comment">  SolveLU(M, Y)</span>
<a name="l00047"></a>00047 <span class="comment"></span>
<a name="l00048"></a>00048 <span class="comment">  Solve(M, Y)</span>
<a name="l00049"></a>00049 <span class="comment">*/</span>
<a name="l00050"></a>00050 
<a name="l00051"></a>00051 <span class="keyword">namespace </span>Seldon
<a name="l00052"></a>00052 {
<a name="l00053"></a>00053 
<a name="l00054"></a>00054 
<a name="l00056"></a>00056   <span class="comment">// MLT //</span>
<a name="l00057"></a>00057 
<a name="l00058"></a>00058 
<a name="l00059"></a>00059   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00060"></a>00060             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00061"></a>00061             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00062"></a>00062   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l00063"></a>00063            <span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00064"></a>00064            Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y);
<a name="l00065"></a>00065 
<a name="l00066"></a>00066   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00067"></a>00067             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00068"></a>00068             <span class="keyword">class </span>T3, <span class="keyword">class </span>Storage3, <span class="keyword">class </span>Allocator3&gt;
<a name="l00069"></a>00069   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(<span class="keyword">const</span> T3&amp; alpha,
<a name="l00070"></a>00070            <span class="keyword">const</span> Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;&amp; M,
<a name="l00071"></a>00071            <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00072"></a>00072            Vector&lt;T3, Storage3, Allocator3&gt;&amp; Y);
<a name="l00073"></a>00073 
<a name="l00074"></a>00074   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00075"></a>00075             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00076"></a>00076             <span class="keyword">class </span>T3, <span class="keyword">class </span>Storage3, <span class="keyword">class </span>Allocator3&gt;
<a name="l00077"></a>00077   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(<span class="keyword">const</span> SeldonTranspose&amp; Trans,
<a name="l00078"></a>00078            <span class="keyword">const</span> Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;&amp; M,
<a name="l00079"></a>00079            <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00080"></a>00080            Vector&lt;T3, Storage3, Allocator3&gt;&amp; Y);
<a name="l00081"></a>00081 
<a name="l00082"></a>00082 
<a name="l00083"></a>00083   <span class="comment">// MLT //</span>
<a name="l00085"></a>00085 <span class="comment"></span>
<a name="l00086"></a>00086 
<a name="l00088"></a>00088   <span class="comment">// MLTADD //</span>
<a name="l00089"></a>00089 
<a name="l00090"></a>00090 
<a name="l00091"></a>00091   <span class="comment">/*** PETSc matrices ***/</span>
<a name="l00092"></a>00092 
<a name="l00093"></a>00093   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00094"></a>00094             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00095"></a>00095             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2,
<a name="l00096"></a>00096             <span class="keyword">class </span>T3,
<a name="l00097"></a>00097             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00098"></a>00098   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00099"></a>00099               <span class="keyword">const</span> Matrix&lt;T1, Prop1, PETScMPIAIJ, Allocator1&gt;&amp; M,
<a name="l00100"></a>00100               <span class="keyword">const</span> Vector&lt;T2, PETScSeq, Allocator2&gt;&amp; X,
<a name="l00101"></a>00101               <span class="keyword">const</span> T3 beta, Vector&lt;T4, PETScSeq, Allocator4&gt;&amp; Y);
<a name="l00102"></a>00102 
<a name="l00103"></a>00103   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00104"></a>00104             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00105"></a>00105             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2,
<a name="l00106"></a>00106             <span class="keyword">class </span>T3,
<a name="l00107"></a>00107             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00108"></a>00108   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00109"></a>00109               <span class="keyword">const</span> Matrix&lt;T1, Prop1, PETScMPIAIJ, Allocator1&gt;&amp; M,
<a name="l00110"></a>00110               <span class="keyword">const</span> Vector&lt;T2, PETScPar, Allocator2&gt;&amp; X,
<a name="l00111"></a>00111               <span class="keyword">const</span> T3 beta, Vector&lt;T4, PETScPar, Allocator4&gt;&amp; Y);
<a name="l00112"></a>00112 
<a name="l00113"></a>00113   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00114"></a>00114             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00115"></a>00115             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2,
<a name="l00116"></a>00116             <span class="keyword">class </span>T3,
<a name="l00117"></a>00117             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00118"></a>00118   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00119"></a>00119               <span class="keyword">const</span> Matrix&lt;T1, Prop1, PETScMPIAIJ, Allocator1&gt;&amp; M,
<a name="l00120"></a>00120               <span class="keyword">const</span> Vector&lt;T2, VectFull, Allocator2&gt;&amp; X,
<a name="l00121"></a>00121               <span class="keyword">const</span> T3 beta, Vector&lt;T4, PETScSeq, Allocator4&gt;&amp; Y);
<a name="l00122"></a>00122 
<a name="l00123"></a>00123   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00124"></a>00124             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00125"></a>00125             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2,
<a name="l00126"></a>00126             <span class="keyword">class </span>T3,
<a name="l00127"></a>00127             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00128"></a>00128   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00129"></a>00129               <span class="keyword">const</span> Matrix&lt;T1, Prop1, PETScMPIAIJ, Allocator1&gt;&amp; M,
<a name="l00130"></a>00130               <span class="keyword">const</span> Vector&lt;T2, VectFull, Allocator2&gt;&amp; X,
<a name="l00131"></a>00131               <span class="keyword">const</span> T3 beta, Vector&lt;T4, PETScPar, Allocator4&gt;&amp; Y);
<a name="l00132"></a>00132 
<a name="l00133"></a>00133   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00134"></a>00134             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00135"></a>00135             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2,
<a name="l00136"></a>00136             <span class="keyword">class </span>T3,
<a name="l00137"></a>00137             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00138"></a>00138   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00139"></a>00139               <span class="keyword">const</span> Matrix&lt;T1, Prop1, PETScMPIDense, Allocator1&gt;&amp; M,
<a name="l00140"></a>00140               <span class="keyword">const</span> Vector&lt;T2, VectFull, Allocator2&gt;&amp; X,
<a name="l00141"></a>00141               <span class="keyword">const</span> T3 beta, Vector&lt;T4, PETScPar, Allocator4&gt;&amp; Y);
<a name="l00142"></a>00142 
<a name="l00143"></a>00143   <span class="comment">/*** Sparse matrices ***/</span>
<a name="l00144"></a>00144 
<a name="l00145"></a>00145   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00146"></a>00146             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00147"></a>00147             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00148"></a>00148             <span class="keyword">class </span>T3,
<a name="l00149"></a>00149             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00150"></a>00150   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00151"></a>00151               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;&amp; M,
<a name="l00152"></a>00152               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00153"></a>00153               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y);
<a name="l00154"></a>00154 
<a name="l00155"></a>00155   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00156"></a>00156             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00157"></a>00157             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00158"></a>00158             <span class="keyword">class </span>T3,
<a name="l00159"></a>00159             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00160"></a>00160   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00161"></a>00161               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;&amp; M,
<a name="l00162"></a>00162               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00163"></a>00163               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Collection, Allocator4&gt;&amp; Y);
<a name="l00164"></a>00164 
<a name="l00165"></a>00165   <span class="comment">/*** Complex sparse matrices ***/</span>
<a name="l00166"></a>00166 
<a name="l00167"></a>00167   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00168"></a>00168             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00169"></a>00169             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00170"></a>00170             <span class="keyword">class </span>T3,
<a name="l00171"></a>00171             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00172"></a>00172   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00173"></a>00173               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;&amp; M,
<a name="l00174"></a>00174               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00175"></a>00175               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y);
<a name="l00176"></a>00176 
<a name="l00177"></a>00177   <span class="comment">/*** Symmetric sparse matrices ***/</span>
<a name="l00178"></a>00178 
<a name="l00179"></a>00179   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00180"></a>00180             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00181"></a>00181             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00182"></a>00182             <span class="keyword">class </span>T3,
<a name="l00183"></a>00183             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00184"></a>00184   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00185"></a>00185               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymSparse, Allocator1&gt;&amp; M,
<a name="l00186"></a>00186               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00187"></a>00187               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y);
<a name="l00188"></a>00188 
<a name="l00189"></a>00189   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00190"></a>00190             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00191"></a>00191             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2,
<a name="l00192"></a>00192             <span class="keyword">class </span>T3,
<a name="l00193"></a>00193             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00194"></a>00194   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00195"></a>00195               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymSparse, Allocator1&gt;&amp; M,
<a name="l00196"></a>00196               <span class="keyword">const</span> Vector&lt;T2, Collection, Allocator2&gt;&amp; X,
<a name="l00197"></a>00197               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Collection, Allocator4&gt;&amp; Y);
<a name="l00198"></a>00198 
<a name="l00199"></a>00199   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00200"></a>00200             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00201"></a>00201             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00202"></a>00202             <span class="keyword">class </span>T3,
<a name="l00203"></a>00203             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00204"></a>00204   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00205"></a>00205               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymSparse, Allocator1&gt;&amp; M,
<a name="l00206"></a>00206               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00207"></a>00207               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Collection, Allocator4&gt;&amp; Y);
<a name="l00208"></a>00208 
<a name="l00209"></a>00209   <span class="comment">/*** Symmetric complex sparse matrices ***/</span>
<a name="l00210"></a>00210 
<a name="l00211"></a>00211   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00212"></a>00212             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00213"></a>00213             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00214"></a>00214             <span class="keyword">class </span>T3,
<a name="l00215"></a>00215             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00216"></a>00216   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00217"></a>00217               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymComplexSparse, Allocator1&gt;&amp; M,
<a name="l00218"></a>00218               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00219"></a>00219               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y);
<a name="l00220"></a>00220 
<a name="l00221"></a>00221   <span class="comment">/*** Sparse matrices, *Trans ***/</span>
<a name="l00222"></a>00222 
<a name="l00223"></a>00223   <span class="comment">// NoTrans.</span>
<a name="l00224"></a>00224   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00225"></a>00225             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00226"></a>00226             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00227"></a>00227             <span class="keyword">class </span>T3,
<a name="l00228"></a>00228             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00229"></a>00229   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00230"></a>00230               <span class="keyword">const</span> class_SeldonNoTrans&amp; Trans,
<a name="l00231"></a>00231               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;&amp; M,
<a name="l00232"></a>00232               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00233"></a>00233               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y);
<a name="l00234"></a>00234 
<a name="l00235"></a>00235   <span class="comment">// Trans.</span>
<a name="l00236"></a>00236   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00237"></a>00237             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00238"></a>00238             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00239"></a>00239             <span class="keyword">class </span>T3,
<a name="l00240"></a>00240             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00241"></a>00241   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00242"></a>00242               <span class="keyword">const</span> class_SeldonTrans&amp; Trans,
<a name="l00243"></a>00243               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;&amp; M,
<a name="l00244"></a>00244               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00245"></a>00245               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y);
<a name="l00246"></a>00246 
<a name="l00247"></a>00247   <span class="comment">// ConjTrans.</span>
<a name="l00248"></a>00248   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00249"></a>00249             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00250"></a>00250             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00251"></a>00251             <span class="keyword">class </span>T3,
<a name="l00252"></a>00252             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00253"></a>00253   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00254"></a>00254               <span class="keyword">const</span> class_SeldonConjTrans&amp; Trans,
<a name="l00255"></a>00255               <span class="keyword">const</span> Matrix&lt;complex&lt;T1&gt;, Prop1, RowSparse, Allocator1&gt;&amp; M,
<a name="l00256"></a>00256               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00257"></a>00257               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y);
<a name="l00258"></a>00258 
<a name="l00259"></a>00259   <span class="comment">/*** Complex sparse matrices, *Trans ***/</span>
<a name="l00260"></a>00260 
<a name="l00261"></a>00261   <span class="comment">// NoTrans.</span>
<a name="l00262"></a>00262   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00263"></a>00263             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00264"></a>00264             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00265"></a>00265             <span class="keyword">class </span>T3,
<a name="l00266"></a>00266             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00267"></a>00267   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00268"></a>00268               <span class="keyword">const</span> class_SeldonNoTrans&amp; Trans,
<a name="l00269"></a>00269               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;&amp; M,
<a name="l00270"></a>00270               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00271"></a>00271               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y);
<a name="l00272"></a>00272 
<a name="l00273"></a>00273   <span class="comment">// Trans.</span>
<a name="l00274"></a>00274   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00275"></a>00275             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00276"></a>00276             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00277"></a>00277             <span class="keyword">class </span>T3,
<a name="l00278"></a>00278             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00279"></a>00279   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00280"></a>00280               <span class="keyword">const</span> class_SeldonTrans&amp; Trans,
<a name="l00281"></a>00281               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;&amp; M,
<a name="l00282"></a>00282               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00283"></a>00283               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y);
<a name="l00284"></a>00284 
<a name="l00285"></a>00285   <span class="comment">// ConjTrans.</span>
<a name="l00286"></a>00286   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00287"></a>00287             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00288"></a>00288             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00289"></a>00289             <span class="keyword">class </span>T3,
<a name="l00290"></a>00290             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00291"></a>00291   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00292"></a>00292               <span class="keyword">const</span> class_SeldonConjTrans&amp; Trans,
<a name="l00293"></a>00293               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;&amp; M,
<a name="l00294"></a>00294               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00295"></a>00295               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y);
<a name="l00296"></a>00296 
<a name="l00297"></a>00297   <span class="comment">/*** Symmetric sparse matrices, *Trans ***/</span>
<a name="l00298"></a>00298 
<a name="l00299"></a>00299   <span class="comment">// NoTrans.</span>
<a name="l00300"></a>00300   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00301"></a>00301             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00302"></a>00302             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00303"></a>00303             <span class="keyword">class </span>T3,
<a name="l00304"></a>00304             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00305"></a>00305   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00306"></a>00306               <span class="keyword">const</span> class_SeldonNoTrans&amp; Trans,
<a name="l00307"></a>00307               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymSparse, Allocator1&gt;&amp; M,
<a name="l00308"></a>00308               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00309"></a>00309               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y);
<a name="l00310"></a>00310 
<a name="l00311"></a>00311   <span class="comment">// Trans.</span>
<a name="l00312"></a>00312   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00313"></a>00313             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00314"></a>00314             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00315"></a>00315             <span class="keyword">class </span>T3,
<a name="l00316"></a>00316             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00317"></a>00317   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00318"></a>00318               <span class="keyword">const</span> class_SeldonTrans&amp; Trans,
<a name="l00319"></a>00319               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymSparse, Allocator1&gt;&amp; M,
<a name="l00320"></a>00320               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00321"></a>00321               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y);
<a name="l00322"></a>00322 
<a name="l00323"></a>00323   <span class="comment">// ConjTrans.</span>
<a name="l00324"></a>00324   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00325"></a>00325             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00326"></a>00326             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00327"></a>00327             <span class="keyword">class </span>T3,
<a name="l00328"></a>00328             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00329"></a>00329   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00330"></a>00330               <span class="keyword">const</span> class_SeldonConjTrans&amp; Trans,
<a name="l00331"></a>00331               <span class="keyword">const</span> Matrix&lt;complex&lt;T1&gt;, Prop1, RowSymSparse, Allocator1&gt;&amp; M,
<a name="l00332"></a>00332               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00333"></a>00333               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y);
<a name="l00334"></a>00334 
<a name="l00335"></a>00335   <span class="comment">/*** Symmetric complex sparse matrices, *Trans ***/</span>
<a name="l00336"></a>00336 
<a name="l00337"></a>00337   <span class="comment">// NoTrans.</span>
<a name="l00338"></a>00338   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00339"></a>00339             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00340"></a>00340             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00341"></a>00341             <span class="keyword">class </span>T3,
<a name="l00342"></a>00342             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00343"></a>00343   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00344"></a>00344               <span class="keyword">const</span> class_SeldonNoTrans&amp; Trans,
<a name="l00345"></a>00345               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymComplexSparse, Allocator1&gt;&amp; M,
<a name="l00346"></a>00346               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00347"></a>00347               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y);
<a name="l00348"></a>00348 
<a name="l00349"></a>00349   <span class="comment">// Trans.</span>
<a name="l00350"></a>00350   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00351"></a>00351             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00352"></a>00352             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00353"></a>00353             <span class="keyword">class </span>T3,
<a name="l00354"></a>00354             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00355"></a>00355   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00356"></a>00356               <span class="keyword">const</span> class_SeldonTrans&amp; Trans,
<a name="l00357"></a>00357               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymComplexSparse, Allocator1&gt;&amp; M,
<a name="l00358"></a>00358               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00359"></a>00359               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y);
<a name="l00360"></a>00360 
<a name="l00361"></a>00361   <span class="comment">// ConjTrans.</span>
<a name="l00362"></a>00362   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00363"></a>00363             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00364"></a>00364             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00365"></a>00365             <span class="keyword">class </span>T3,
<a name="l00366"></a>00366             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00367"></a>00367   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00368"></a>00368               <span class="keyword">const</span> class_SeldonConjTrans&amp; Trans,
<a name="l00369"></a>00369               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymComplexSparse, Allocator1&gt;&amp; M,
<a name="l00370"></a>00370               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00371"></a>00371               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y);
<a name="l00372"></a>00372 
<a name="l00373"></a>00373 
<a name="l00374"></a>00374   <span class="comment">// MLTADD //</span>
<a name="l00376"></a>00376 <span class="comment"></span>
<a name="l00377"></a>00377 
<a name="l00379"></a>00379   <span class="comment">// MLTADD //</span>
<a name="l00380"></a>00380 
<a name="l00381"></a>00381 
<a name="l00382"></a>00382   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00383"></a>00383             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00384"></a>00384             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00385"></a>00385             <span class="keyword">class </span>T3,
<a name="l00386"></a>00386             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00387"></a>00387   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00388"></a>00388               <span class="keyword">const</span> Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;&amp; M,
<a name="l00389"></a>00389               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00390"></a>00390               <span class="keyword">const</span> T3 beta,
<a name="l00391"></a>00391               Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y);
<a name="l00392"></a>00392 
<a name="l00393"></a>00393   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00394"></a>00394             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00395"></a>00395             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00396"></a>00396             <span class="keyword">class </span>T3,
<a name="l00397"></a>00397             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00398"></a>00398   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00399"></a>00399               <span class="keyword">const</span> Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;&amp; M,
<a name="l00400"></a>00400               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00401"></a>00401               <span class="keyword">const</span> T3 beta,
<a name="l00402"></a>00402               Vector&lt;T4, Collection, Allocator4&gt;&amp; Y);
<a name="l00403"></a>00403 
<a name="l00404"></a>00404   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00405"></a>00405             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00406"></a>00406             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2,
<a name="l00407"></a>00407             <span class="keyword">class </span>T3,
<a name="l00408"></a>00408             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00409"></a>00409   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00410"></a>00410               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowMajorCollection, Allocator1&gt;&amp; M,
<a name="l00411"></a>00411               <span class="keyword">const</span> Vector&lt;T2, Collection, Allocator2&gt;&amp; X,
<a name="l00412"></a>00412               <span class="keyword">const</span> T3 beta,
<a name="l00413"></a>00413               Vector&lt;T4, Collection, Allocator4&gt;&amp; Y);
<a name="l00414"></a>00414 
<a name="l00415"></a>00415   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00416"></a>00416             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00417"></a>00417             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2,
<a name="l00418"></a>00418             <span class="keyword">class </span>T3,
<a name="l00419"></a>00419             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00420"></a>00420   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00421"></a>00421               <span class="keyword">const</span> Matrix&lt;T1, Prop1, ColMajorCollection, Allocator1&gt;&amp; M,
<a name="l00422"></a>00422               <span class="keyword">const</span> Vector&lt;T2, Collection, Allocator2&gt;&amp; X,
<a name="l00423"></a>00423               <span class="keyword">const</span> T3 beta,
<a name="l00424"></a>00424               Vector&lt;T4, Collection, Allocator4&gt;&amp; Y);
<a name="l00425"></a>00425 
<a name="l00426"></a>00426   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00427"></a>00427             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00428"></a>00428             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00429"></a>00429             <span class="keyword">class </span>T3,
<a name="l00430"></a>00430             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00431"></a>00431   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00432"></a>00432               <span class="keyword">const</span> SeldonTranspose&amp; Trans,
<a name="l00433"></a>00433               <span class="keyword">const</span> Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;&amp; M,
<a name="l00434"></a>00434               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00435"></a>00435               <span class="keyword">const</span> T3 beta,
<a name="l00436"></a>00436               Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y);
<a name="l00437"></a>00437 
<a name="l00438"></a>00438 
<a name="l00439"></a>00439   <span class="comment">// MLTADD //</span>
<a name="l00441"></a>00441 <span class="comment"></span>
<a name="l00442"></a>00442 
<a name="l00444"></a>00444   <span class="comment">// GAUSS //</span>
<a name="l00445"></a>00445 
<a name="l00446"></a>00446 
<a name="l00447"></a>00447   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00448"></a>00448             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00449"></a>00449   <span class="keyword">inline</span> <span class="keywordtype">void</span> Gauss(Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l00450"></a>00450                     Vector&lt;T1, Storage1, Allocator1&gt;&amp; X);
<a name="l00451"></a>00451 
<a name="l00452"></a>00452   <span class="comment">// GAUSS //</span>
<a name="l00454"></a>00454 <span class="comment"></span>
<a name="l00455"></a>00455 
<a name="l00457"></a>00457   <span class="comment">// GAUSS-SEIDEL //</span>
<a name="l00458"></a>00458 
<a name="l00459"></a>00459 
<a name="l00460"></a>00460   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00461"></a>00461             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00462"></a>00462             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00463"></a>00463   <span class="keyword">inline</span> <span class="keywordtype">void</span> GaussSeidel(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l00464"></a>00464                           <span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00465"></a>00465                           Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y,
<a name="l00466"></a>00466                           <span class="keywordtype">int</span> iter);
<a name="l00467"></a>00467 
<a name="l00468"></a>00468 
<a name="l00469"></a>00469   <span class="comment">// GAUSS-SEIDEL //</span>
<a name="l00471"></a>00471 <span class="comment"></span>
<a name="l00472"></a>00472 
<a name="l00473"></a>00473 
<a name="l00475"></a>00475   <span class="comment">// S.O.R. METHOD //</span>
<a name="l00476"></a>00476 
<a name="l00477"></a>00477 
<a name="l00478"></a>00478   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00479"></a>00479             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00480"></a>00480             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00481"></a>00481             <span class="keyword">class </span>T3&gt;
<a name="l00482"></a>00482   <span class="keyword">inline</span> <span class="keywordtype">void</span> SOR(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l00483"></a>00483                   <span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00484"></a>00484                   Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y,
<a name="l00485"></a>00485                   T3 omega,
<a name="l00486"></a>00486                   <span class="keywordtype">int</span> iter);
<a name="l00487"></a>00487 
<a name="l00488"></a>00488 
<a name="l00489"></a>00489   <span class="comment">// S.O.R. METHOD //</span>
<a name="l00491"></a>00491 <span class="comment"></span>
<a name="l00492"></a>00492 
<a name="l00493"></a>00493 
<a name="l00495"></a>00495   <span class="comment">// SOLVELU //</span>
<a name="l00496"></a>00496 
<a name="l00497"></a>00497 
<a name="l00498"></a>00498   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00499"></a>00499             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00500"></a>00500   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l00501"></a>00501                Vector&lt;T1, Storage1, Allocator1&gt;&amp; Y);
<a name="l00502"></a>00502 
<a name="l00503"></a>00503   <span class="comment">// SOLVELU //</span>
<a name="l00505"></a>00505 <span class="comment"></span>
<a name="l00506"></a>00506 
<a name="l00508"></a>00508   <span class="comment">// SOLVE //</span>
<a name="l00509"></a>00509 
<a name="l00510"></a>00510 
<a name="l00511"></a>00511   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00512"></a>00512             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00513"></a>00513   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ac173624f2f8a8e737e4f9c7b29ad95af" title="Solves a linear system using LU factorization.">GetAndSolveLU</a>(Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l00514"></a>00514                      Vector&lt;T1, Storage1, Allocator1&gt;&amp; Y);
<a name="l00515"></a>00515 
<a name="l00516"></a>00516   <span class="comment">// SOLVE //</span>
<a name="l00518"></a>00518 <span class="comment"></span>
<a name="l00519"></a>00519 
<a name="l00521"></a>00521   <span class="comment">// CHECKDIM //</span>
<a name="l00522"></a>00522 
<a name="l00523"></a>00523 
<a name="l00524"></a>00524   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00525"></a>00525             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00526"></a>00526             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00527"></a>00527   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l00528"></a>00528                 <span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00529"></a>00529                 <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y,
<a name="l00530"></a>00530                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>);
<a name="l00531"></a>00531 
<a name="l00532"></a>00532   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00533"></a>00533             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00534"></a>00534             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00535"></a>00535   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, RowMajorCollection, Allocator0&gt;&amp; M,
<a name="l00536"></a>00536                 <span class="keyword">const</span> Vector&lt;T1, Collection, Allocator1&gt;&amp; X,
<a name="l00537"></a>00537                 <span class="keyword">const</span> Vector&lt;T2, Collection, Allocator2&gt;&amp; Y,
<a name="l00538"></a>00538                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>);
<a name="l00539"></a>00539 
<a name="l00540"></a>00540   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00541"></a>00541             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00542"></a>00542             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00543"></a>00543   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, ColMajorCollection, Allocator0&gt;&amp; M,
<a name="l00544"></a>00544                 <span class="keyword">const</span> Vector&lt;T1, Collection, Allocator1&gt;&amp; X,
<a name="l00545"></a>00545                 <span class="keyword">const</span> Vector&lt;T2, Collection, Allocator2&gt;&amp; Y,
<a name="l00546"></a>00546                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>);
<a name="l00547"></a>00547 
<a name="l00548"></a>00548   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00549"></a>00549             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00550"></a>00550             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00551"></a>00551   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l00552"></a>00552                 <span class="keyword">const</span> Vector&lt;T1, Collection, Allocator1&gt;&amp; X,
<a name="l00553"></a>00553                 <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y,
<a name="l00554"></a>00554                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>);
<a name="l00555"></a>00555 
<a name="l00556"></a>00556   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00557"></a>00557             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00558"></a>00558             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00559"></a>00559   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> SeldonTranspose&amp; trans,
<a name="l00560"></a>00560                 <span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l00561"></a>00561                 <span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00562"></a>00562                 <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y,
<a name="l00563"></a>00563                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> op = <span class="stringliteral">&quot;M X + Y -&gt; Y&quot;</span>);
<a name="l00564"></a>00564 
<a name="l00565"></a>00565   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00566"></a>00566             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00567"></a>00567   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l00568"></a>00568                 <span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00569"></a>00569                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> op = <span class="stringliteral">&quot;M X&quot;</span>);
<a name="l00570"></a>00570 
<a name="l00571"></a>00571 
<a name="l00572"></a>00572   <span class="comment">// CHECKDIM //</span>
<a name="l00574"></a>00574 <span class="comment"></span>
<a name="l00575"></a>00575 
<a name="l00576"></a>00576 }  <span class="comment">// namespace Seldon.</span>
<a name="l00577"></a>00577 
<a name="l00578"></a>00578 
<a name="l00579"></a>00579 <span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
