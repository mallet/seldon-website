<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_n_lopt_solver.php">NLoptSolver</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-types">Protected Types</a> &#124;
<a href="#pro-attribs">Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::NLoptSolver Class Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::NLoptSolver" -->
<p>NLopt optimization.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_n_lopt_solver_8hxx_source.php">NLoptSolver.hxx</a>&gt;</code></p>

<p><a href="class_seldon_1_1_n_lopt_solver-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad775e71c990c2d8f08feaf0399a1a916"></a><!-- doxytag: member="Seldon::NLoptSolver::NLoptSolver" ref="ad775e71c990c2d8f08feaf0399a1a916" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#ad775e71c990c2d8f08feaf0399a1a916">NLoptSolver</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adcf818c843283a20eef4d17a974fd6cc"></a><!-- doxytag: member="Seldon::NLoptSolver::~NLoptSolver" ref="adcf818c843283a20eef4d17a974fd6cc" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#adcf818c843283a20eef4d17a974fd6cc">~NLoptSolver</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Destructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#ad80720692c1df8718044c24ecbc98372">Initialize</a> (int Nparameter, string algorithm, double parameter_tolerance=1.e-6, double cost_function_tolerance=1.e-6, int Niteration_max=-1)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Initializations.  <a href="#ad80720692c1df8718044c24ecbc98372"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#a6f92bbc59849ce8489da5210dfd6d994">SetLowerBound</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double &gt; &amp;)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets lower bounds on the parameters.  <a href="#a6f92bbc59849ce8489da5210dfd6d994"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#a46fee6721c1a4172bae0ce10870d4442">SetUpperBound</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double &gt; &amp;)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets upper bounds on the parameters.  <a href="#a46fee6721c1a4172bae0ce10870d4442"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#aa74cb3aca730ca6444fff8bd600f3be7">SetParameterTolerance</a> (double)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the relative tolerance on the parameters.  <a href="#aa74cb3aca730ca6444fff8bd600f3be7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#a44ed740163ffdc82a1eda9a47001e58a">SetCostFunctionTolerance</a> (double)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the relative tolerance on the cost function.  <a href="#a44ed740163ffdc82a1eda9a47001e58a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#ae8d18863be9cbbf79bc14e5fdc905a39">SetNiterationMax</a> (int)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the maximum number of cost function evaluations.  <a href="#ae8d18863be9cbbf79bc14e5fdc905a39"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#a4e7c4cb0c59c99b234c89b23ffd56927">GetParameterTolerance</a> (double &amp;) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Gets the relative tolerance on the parameters.  <a href="#a4e7c4cb0c59c99b234c89b23ffd56927"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#ab585d1ac63039aa34eb41ba3a1548817">GetCostFunctionTolerance</a> (double &amp;) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Gets the relative tolerance on the cost function.  <a href="#ab585d1ac63039aa34eb41ba3a1548817"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#ac584f3bed8e3326faa50fff3c7004b62">GetNiterationMax</a> (int &amp;) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Gets the maximum number of cost function evaluations.  <a href="#ac584f3bed8e3326faa50fff3c7004b62"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#adb63bff470ea8bbc8df7cc78207a597f">SetParameter</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double &gt; &amp;parameter)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the parameters.  <a href="#adb63bff470ea8bbc8df7cc78207a597f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#a2de56e3f3de295800e412039ca5b0e69">GetParameter</a> (<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double &gt; &amp;parameter) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Gets the parameters.  <a href="#a2de56e3f3de295800e412039ca5b0e69"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#a15954e351f26ae09799c07e0009ce864">Optimize</a> (cost_ptr cost, void *argument)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Optimization.  <a href="#a15954e351f26ae09799c07e0009ce864"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">double&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#aaf4264800dbfb5f24af404ac6019e0b0">GetCost</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the value of the cost function.  <a href="#aaf4264800dbfb5f24af404ac6019e0b0"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-types"></a>
Protected Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a65dc4316cdebb4b8dcc16fef0bd7cca9"></a><!-- doxytag: member="Seldon::NLoptSolver::cost_ptr" ref="a65dc4316cdebb4b8dcc16fef0bd7cca9" args=")(const Vector&lt; double &gt; &amp;, Vector&lt; double &gt; &amp;, void *)" -->
typedef double(*&nbsp;</td><td class="memItemRight" valign="bottom"><b>cost_ptr</b> )(const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double &gt; &amp;, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double &gt; &amp;, void *)</td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0236ea3add112a572ed2326a12306ded"></a><!-- doxytag: member="Seldon::NLoptSolver::opt_" ref="a0236ea3add112a572ed2326a12306ded" args="" -->
<a class="el" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#a0236ea3add112a572ed2326a12306ded">opt_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">NLopt optimization solver. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aad0beaa06728577f5f7c39c2989f81ed"></a><!-- doxytag: member="Seldon::NLoptSolver::algorithm_" ref="aad0beaa06728577f5f7c39c2989f81ed" args="" -->
nlopt::algorithm&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#aad0beaa06728577f5f7c39c2989f81ed">algorithm_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Optimization algorithm. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adefce6eb95498ffb35690ce4c8c7f637"></a><!-- doxytag: member="Seldon::NLoptSolver::parameter_tolerance_" ref="adefce6eb95498ffb35690ce4c8c7f637" args="" -->
double&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#adefce6eb95498ffb35690ce4c8c7f637">parameter_tolerance_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Relative tolerance on the optimization parameters. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afd6a5c3ebbb73a71fbd412a641eca860"></a><!-- doxytag: member="Seldon::NLoptSolver::cost_function_tolerance_" ref="afd6a5c3ebbb73a71fbd412a641eca860" args="" -->
double&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#afd6a5c3ebbb73a71fbd412a641eca860">cost_function_tolerance_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Relative tolerance on the cost function. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac3340df79837a4cdde5560939ed01989"></a><!-- doxytag: member="Seldon::NLoptSolver::Niteration_max_" ref="ac3340df79837a4cdde5560939ed01989" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#ac3340df79837a4cdde5560939ed01989">Niteration_max_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Maximum number of function evaluations. It is ignored if it is non-positive. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aade135d662e35906b7cae2ecbd1d789c"></a><!-- doxytag: member="Seldon::NLoptSolver::parameter_" ref="aade135d662e35906b7cae2ecbd1d789c" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#aade135d662e35906b7cae2ecbd1d789c">parameter_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">The vector that stores parameters values. Before optimization, stores the initial parameter vector; after optimization, it returns the optimized parameters. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3d4a7ac50a792cdd899ff11a0cd5f3f3"></a><!-- doxytag: member="Seldon::NLoptSolver::gradient_" ref="a3d4a7ac50a792cdd899ff11a0cd5f3f3" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#a3d4a7ac50a792cdd899ff11a0cd5f3f3">gradient_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">The vector that stores gradient values. Before optimization, unspecified; after optimization, it returns the gradient vector for optimized parameters. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afb1ea25dcf6a9da4b54f97d5ccf95cc8"></a><!-- doxytag: member="Seldon::NLoptSolver::cost_" ref="afb1ea25dcf6a9da4b54f97d5ccf95cc8" args="" -->
double&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_n_lopt_solver.php#afb1ea25dcf6a9da4b54f97d5ccf95cc8">cost_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">The value of cost function for given parameter values. <br/></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<p>NLopt optimization. </p>

<p>Definition at line <a class="el" href="_n_lopt_solver_8hxx_source.php#l00038">38</a> of file <a class="el" href="_n_lopt_solver_8hxx_source.php">NLoptSolver.hxx</a>.</p>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="aaf4264800dbfb5f24af404ac6019e0b0"></a><!-- doxytag: member="Seldon::NLoptSolver::GetCost" ref="aaf4264800dbfb5f24af404ac6019e0b0" args="() const " -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">double Seldon::NLoptSolver::GetCost </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the value of the cost function. </p>
<p>This method should be called after the optimization, and it returns the value of the cost function associated with the optimized parameters. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the value of the cost function. </dd></dl>

<p>Definition at line <a class="el" href="_n_lopt_solver_8cxx_source.php#l00314">314</a> of file <a class="el" href="_n_lopt_solver_8cxx_source.php">NLoptSolver.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab585d1ac63039aa34eb41ba3a1548817"></a><!-- doxytag: member="Seldon::NLoptSolver::GetCostFunctionTolerance" ref="ab585d1ac63039aa34eb41ba3a1548817" args="(double &amp;) const " -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void Seldon::NLoptSolver::GetCostFunctionTolerance </td>
          <td>(</td>
          <td class="paramtype">double &amp;&nbsp;</td>
          <td class="paramname"> <em>tolerance</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Gets the relative tolerance on the cost function. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[out]</tt>&nbsp;</td><td valign="top"><em>tolerance</em>&nbsp;</td><td>relative tolerance on the cost function . When the variation of the cost function, after one step of the algorithm, has changed by less than <em>tolerance</em> multiplied by the value of the cost function, the optimization is stopped. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_n_lopt_solver_8cxx_source.php#l00238">238</a> of file <a class="el" href="_n_lopt_solver_8cxx_source.php">NLoptSolver.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac584f3bed8e3326faa50fff3c7004b62"></a><!-- doxytag: member="Seldon::NLoptSolver::GetNiterationMax" ref="ac584f3bed8e3326faa50fff3c7004b62" args="(int &amp;) const " -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void Seldon::NLoptSolver::GetNiterationMax </td>
          <td>(</td>
          <td class="paramtype">int &amp;&nbsp;</td>
          <td class="paramname"> <em>Niteration_max</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Gets the maximum number of cost function evaluations. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[out]</tt>&nbsp;</td><td valign="top"><em>Niteration_max</em>&nbsp;</td><td>maximum number of cost function evaluations. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_n_lopt_solver_8cxx_source.php#l00248">248</a> of file <a class="el" href="_n_lopt_solver_8cxx_source.php">NLoptSolver.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2de56e3f3de295800e412039ca5b0e69"></a><!-- doxytag: member="Seldon::NLoptSolver::GetParameter" ref="a2de56e3f3de295800e412039ca5b0e69" args="(Vector&lt; double &gt; &amp;parameter) const " -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void Seldon::NLoptSolver::GetParameter </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>parameter</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Gets the parameters. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[out]</tt>&nbsp;</td><td valign="top"><em>parameter</em>&nbsp;</td><td>the parameters vector. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_n_lopt_solver_8cxx_source.php#l00269">269</a> of file <a class="el" href="_n_lopt_solver_8cxx_source.php">NLoptSolver.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a4e7c4cb0c59c99b234c89b23ffd56927"></a><!-- doxytag: member="Seldon::NLoptSolver::GetParameterTolerance" ref="a4e7c4cb0c59c99b234c89b23ffd56927" args="(double &amp;) const " -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void Seldon::NLoptSolver::GetParameterTolerance </td>
          <td>(</td>
          <td class="paramtype">double &amp;&nbsp;</td>
          <td class="paramname"> <em>tolerance</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Gets the relative tolerance on the parameters. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[out]</tt>&nbsp;</td><td valign="top"><em>tolerance</em>&nbsp;</td><td>relative tolerance on the parameters . When the variation of every parameter, after one step of the algorithm, has changed by less than <em>tolerance</em> multiplied by the value of the parameter, the optimization is stopped. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_n_lopt_solver_8cxx_source.php#l00225">225</a> of file <a class="el" href="_n_lopt_solver_8cxx_source.php">NLoptSolver.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad80720692c1df8718044c24ecbc98372"></a><!-- doxytag: member="Seldon::NLoptSolver::Initialize" ref="ad80720692c1df8718044c24ecbc98372" args="(int Nparameter, string algorithm, double parameter_tolerance=1.e&#45;6, double cost_function_tolerance=1.e&#45;6, int Niteration_max=&#45;1)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void Seldon::NLoptSolver::Initialize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>Nparameter</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>algorithm</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">double&nbsp;</td>
          <td class="paramname"> <em>parameter_tolerance</em> = <code>1.e-6</code>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">double&nbsp;</td>
          <td class="paramname"> <em>cost_function_tolerance</em> = <code>1.e-6</code>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>Niteration_max</em> = <code>-1</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Initializations. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>Nparameter</em>&nbsp;</td><td>total number of parameters to be optimized. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>algorithm</em>&nbsp;</td><td>name of the optimization algorithm, one of: GN_DIRECT, GN_DIRECT_L, GN_DIRECT_L_RAND, GN_DIRECT_NOSCAL, GN_DIRECT_L_NOSCAL, GN_DIRECT_L_RAND_NOSCAL, GN_ORIG_DIRECT, GN_ORIG_DIRECT_L, GD_STOGO, GD_STOGO_RAND, LD_LBFGS_NOCEDAL, LD_LBFGS, LN_PRAXIS, LD_VAR1, LD_VAR2, LD_TNEWTON, LD_TNEWTON_RESTART, LD_TNEWTON_PRECOND, LD_TNEWTON_PRECOND_RESTART, GN_CRS2_LM, GN_MLSL, GD_MLSL, GN_MLSL_LDS, GD_MLSL_LDS, LD_MMA, LN_COBYLA, LN_NEWUOA, LN_NEWUOA_BOUND, LN_NELDERMEAD, LN_SBPLX, LN_AUGLAG, LD_AUGLAG, LN_AUGLAG_EQ, LD_AUGLAG_EQ, LN_BOBYQA, GN_ISRES, AUGLAG, AUGLAG_EQ, G_MLSL, G_MLSL_LDS, LD_SLSQP, NUM_ALGORITHMS. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>parameter_tolerance</em>&nbsp;</td><td>relative tolerance on the parameters. When the variation of the parameters, after one step of the algorithm, has changed by less than <em>parameter_tolerance</em> multiplied by the value of the parameters, the optimization is stopped. If you do not want to use a particular tolerance termination, you can just set that tolerance to zero and it will be ignored. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>cost_function_tolerance</em>&nbsp;</td><td>relative tolerance on the cost function. When the variation of the cost function, after one step of the algorithm, has changed by less than <em>cost_function_tolerance</em> multiplied by the value of the cost function, the optimization is stopped. If you do not want to use a particular tolerance termination, you can just set that tolerance to zero and it will be ignored. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>Niteration_max</em>&nbsp;</td><td>maximum number of cost function evaluations. It is ignored if it is non-positive. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_n_lopt_solver_8cxx_source.php#l00069">69</a> of file <a class="el" href="_n_lopt_solver_8cxx_source.php">NLoptSolver.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a15954e351f26ae09799c07e0009ce864"></a><!-- doxytag: member="Seldon::NLoptSolver::Optimize" ref="a15954e351f26ae09799c07e0009ce864" args="(cost_ptr cost, void *argument)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void Seldon::NLoptSolver::Optimize </td>
          <td>(</td>
          <td class="paramtype">cost_ptr&nbsp;</td>
          <td class="paramname"> <em>cost</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">void *&nbsp;</td>
          <td class="paramname"> <em>argument</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Optimization. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>cost</em>&nbsp;</td><td>pointer to the cost function. This function takes as first argument a 'const <a class="el" href="class_seldon_1_1_vector.php">Vector&lt;double&gt;</a>&amp;' of parameters. The second argument of the function is a 'Vector&lt;double&gt;&amp;' which must be, on exit, the gradient of the cost function. NLopt will allocate it before the call. Note that, in case a derivative-free algorithm is used, this gradient vector is empty, and the cost function is not supposed to compute it (it is thus recommended to test the length of the vector in the cost function). The third argument of the function is <em>argument</em>, provided as 'void *'. The cost function returns the cost value in 'double'. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>argument</em>&nbsp;</td><td>third argument of the cost function. This argument is passed to the cost function after the parameters vector and the gradient. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_n_lopt_solver_8cxx_source.php#l00290">290</a> of file <a class="el" href="_n_lopt_solver_8cxx_source.php">NLoptSolver.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a44ed740163ffdc82a1eda9a47001e58a"></a><!-- doxytag: member="Seldon::NLoptSolver::SetCostFunctionTolerance" ref="a44ed740163ffdc82a1eda9a47001e58a" args="(double)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void Seldon::NLoptSolver::SetCostFunctionTolerance </td>
          <td>(</td>
          <td class="paramtype">double&nbsp;</td>
          <td class="paramname"> <em>tolerance</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the relative tolerance on the cost function. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>tolerance</em>&nbsp;</td><td>relative tolerance on the cost function. When the variation of the cost function, after one step of the algorithm, has changed by less than <em>tolerance</em> multiplied by the value of the cost function, the optimization is stopped. If you do not want to use a particular tolerance termination, you can just set that tolerance to zero and it will be ignored. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_n_lopt_solver_8cxx_source.php#l00201">201</a> of file <a class="el" href="_n_lopt_solver_8cxx_source.php">NLoptSolver.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6f92bbc59849ce8489da5210dfd6d994"></a><!-- doxytag: member="Seldon::NLoptSolver::SetLowerBound" ref="a6f92bbc59849ce8489da5210dfd6d994" args="(const Vector&lt; double &gt; &amp;)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void Seldon::NLoptSolver::SetLowerBound </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>lower_bound</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets lower bounds on the parameters. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>lower_bound</em>&nbsp;</td><td>the lower bound vector. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_n_lopt_solver_8cxx_source.php#l00159">159</a> of file <a class="el" href="_n_lopt_solver_8cxx_source.php">NLoptSolver.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae8d18863be9cbbf79bc14e5fdc905a39"></a><!-- doxytag: member="Seldon::NLoptSolver::SetNiterationMax" ref="ae8d18863be9cbbf79bc14e5fdc905a39" args="(int)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void Seldon::NLoptSolver::SetNiterationMax </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>Niteration_max</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the maximum number of cost function evaluations. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>Niteration_max</em>&nbsp;</td><td>maximum number of cost function evaluations. It is ignored if it is non-positive. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_n_lopt_solver_8cxx_source.php#l00212">212</a> of file <a class="el" href="_n_lopt_solver_8cxx_source.php">NLoptSolver.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="adb63bff470ea8bbc8df7cc78207a597f"></a><!-- doxytag: member="Seldon::NLoptSolver::SetParameter" ref="adb63bff470ea8bbc8df7cc78207a597f" args="(const Vector&lt; double &gt; &amp;parameter)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void Seldon::NLoptSolver::SetParameter </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>parameter</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the parameters. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>parameter</em>&nbsp;</td><td>the parameters vector. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_n_lopt_solver_8cxx_source.php#l00258">258</a> of file <a class="el" href="_n_lopt_solver_8cxx_source.php">NLoptSolver.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa74cb3aca730ca6444fff8bd600f3be7"></a><!-- doxytag: member="Seldon::NLoptSolver::SetParameterTolerance" ref="aa74cb3aca730ca6444fff8bd600f3be7" args="(double)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void Seldon::NLoptSolver::SetParameterTolerance </td>
          <td>(</td>
          <td class="paramtype">double&nbsp;</td>
          <td class="paramname"> <em>tolerance</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the relative tolerance on the parameters. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>tolerance</em>&nbsp;</td><td>relative tolerance on the parameters. When the variation of every parameter, after one step of the algorithm, has changed by less than <em>tolerance</em> multiplied by the value of the parameter, the optimization is stopped. If you do not want to use a particular tolerance termination, you can just set that tolerance to zero and it will be ignored. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_n_lopt_solver_8cxx_source.php#l00186">186</a> of file <a class="el" href="_n_lopt_solver_8cxx_source.php">NLoptSolver.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a46fee6721c1a4172bae0ce10870d4442"></a><!-- doxytag: member="Seldon::NLoptSolver::SetUpperBound" ref="a46fee6721c1a4172bae0ce10870d4442" args="(const Vector&lt; double &gt; &amp;)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void Seldon::NLoptSolver::SetUpperBound </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>upper_bound</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets upper bounds on the parameters. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>upper_bound</em>&nbsp;</td><td>the lower bound vector. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_n_lopt_solver_8cxx_source.php#l00170">170</a> of file <a class="el" href="_n_lopt_solver_8cxx_source.php">NLoptSolver.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>computation/optimization/<a class="el" href="_n_lopt_solver_8hxx_source.php">NLoptSolver.hxx</a></li>
<li>computation/optimization/<a class="el" href="_n_lopt_solver_8cxx_source.php">NLoptSolver.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
