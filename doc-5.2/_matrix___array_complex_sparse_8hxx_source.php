<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix_sparse/Matrix_ArrayComplexSparse.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="comment">// To be included by Seldon.hxx</span>
<a name="l00021"></a>00021 
<a name="l00022"></a>00022 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_ARRAY_COMPLEX_SPARSE_HXX</span>
<a name="l00023"></a>00023 <span class="preprocessor"></span>
<a name="l00024"></a>00024 <span class="keyword">namespace </span>Seldon
<a name="l00025"></a>00025 {
<a name="l00026"></a>00026 
<a name="l00028"></a>00028 
<a name="l00035"></a>00035   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Storage,
<a name="l00036"></a>00036             <span class="keyword">class </span>Allocator = SELDON_DEFAULT_ALLOCATOR&lt;T&gt; &gt;
<a name="l00037"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php">00037</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse</a>
<a name="l00038"></a>00038   {
<a name="l00039"></a>00039     <span class="comment">// typedef declaration.</span>
<a name="l00040"></a>00040   <span class="keyword">public</span>:
<a name="l00041"></a>00041     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00042"></a>00042     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::pointer pointer;
<a name="l00043"></a>00043     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_pointer const_pointer;
<a name="l00044"></a>00044     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::reference reference;
<a name="l00045"></a>00045     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_reference const_reference;
<a name="l00046"></a>00046     <span class="keyword">typedef</span> complex&lt;T&gt; entry_type;
<a name="l00047"></a>00047     <span class="keyword">typedef</span> complex&lt;T&gt; access_type;
<a name="l00048"></a>00048     <span class="keyword">typedef</span> complex&lt;T&gt; const_access_type;
<a name="l00049"></a>00049 
<a name="l00050"></a>00050     <span class="comment">// Attributes.</span>
<a name="l00051"></a>00051   <span class="keyword">protected</span>:
<a name="l00053"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de">00053</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>;
<a name="l00055"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98">00055</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a>;
<a name="l00057"></a>00057     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;T, VectSparse, Allocator&gt;</a>, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>,
<a name="l00058"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6">00058</a>            <a class="code" href="class_seldon_1_1_new_alloc.php">NewAlloc&lt;Vector&lt;T, VectSparse, Allocator&gt;</a> &gt; &gt; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>;
<a name="l00060"></a>00060     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;T, VectSparse, Allocator&gt;</a>, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>,
<a name="l00061"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8">00061</a>            <a class="code" href="class_seldon_1_1_new_alloc.php">NewAlloc&lt;Vector&lt;T, VectSparse, Allocator&gt;</a> &gt; &gt; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>;
<a name="l00062"></a>00062 
<a name="l00063"></a>00063     <span class="comment">// Methods.</span>
<a name="l00064"></a>00064   <span class="keyword">public</span>:
<a name="l00065"></a>00065     <span class="comment">// Constructors.</span>
<a name="l00066"></a>00066     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a415bb8c2cb2379647933ddc5d9dbcc32" title="Default constructor.">Matrix_ArrayComplexSparse</a>();
<a name="l00067"></a>00067     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a415bb8c2cb2379647933ddc5d9dbcc32" title="Default constructor.">Matrix_ArrayComplexSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00068"></a>00068 
<a name="l00069"></a>00069     <span class="comment">// Destructor.</span>
<a name="l00070"></a>00070     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acec6f5d4aa6488c9389a2065944568b0" title="Destructor.">~Matrix_ArrayComplexSparse</a>();
<a name="l00071"></a>00071     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0ee918c8c04a1a9434e157c15fdeb596" title="Clears the matrix.">Clear</a>();
<a name="l00072"></a>00072 
<a name="l00073"></a>00073     <span class="comment">// Memory management.</span>
<a name="l00074"></a>00074     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad2348fb25206ef8516c7e835b7ce2287" title="Reallocates memory to resize the matrix.">Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00075"></a>00075     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a9166689334cc2f731220f76b730d692f" title="Reallocates additional memory to resize the matrix.">Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00076"></a>00076 
<a name="l00077"></a>00077     <span class="comment">// Basic methods.</span>
<a name="l00078"></a>00078     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>() <span class="keyword">const</span>;
<a name="l00079"></a>00079     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ae413915ed95545b40df8b43737fa43c1" title="Returns the number of columns.">GetN</a>() <span class="keyword">const</span>;
<a name="l00080"></a>00080     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; status) <span class="keyword">const</span>;
<a name="l00081"></a>00081     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ae413915ed95545b40df8b43737fa43c1" title="Returns the number of columns.">GetN</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; status) <span class="keyword">const</span>;
<a name="l00082"></a>00082     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a11efc1bee6a95da705238628d7e3d958" title="Returns the number of non-zero elements (real part).">GetRealNonZeros</a>() <span class="keyword">const</span>;
<a name="l00083"></a>00083     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a33ebac042eda90796d7f674c250e85be" title="Returns the number of non-zero elements (imaginary part).">GetImagNonZeros</a>() <span class="keyword">const</span>;
<a name="l00084"></a>00084     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa40775992c738c7fd1e9543bfddb2694" title="Returns the number of elements stored in memory (real part).">GetRealDataSize</a>() <span class="keyword">const</span>;
<a name="l00085"></a>00085     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad54321edab8e2633985e198f1a576340" title="Returns the number of elements stored in memory (imaginary part).">GetImagDataSize</a>() <span class="keyword">const</span>;
<a name="l00086"></a>00086     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2e5c6c10af220eea29f9c1565b14a93f" title="Returns the number of elements stored in memory (real+imaginary part).">GetDataSize</a>() <span class="keyword">const</span>;
<a name="l00087"></a>00087     <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afae45969eda952b43748cf7756f0e6d6" title="Returns column indices of non-zero entries in row (real part).">GetRealInd</a>(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00088"></a>00088     <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0a423d7e93a0ad9c19ed6de2b1052dca" title="Returns column indices of non-zero entries in row (imaginary part).">GetImagInd</a>(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00089"></a>00089     T* GetRealData(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00090"></a>00090     T* GetImagData(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00091"></a>00091     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php" title="Sparse vector class.">Vector&lt;T, VectSparse, Allocator&gt;</a>* GetRealData() <span class="keyword">const</span>;
<a name="l00092"></a>00092     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php" title="Sparse vector class.">Vector&lt;T, VectSparse, Allocator&gt;</a>* GetImagData() <span class="keyword">const</span>;
<a name="l00093"></a>00093 
<a name="l00094"></a>00094     <span class="comment">// Element acess and affectation.</span>
<a name="l00095"></a>00095     complex&lt;T&gt; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a30be6306dbdd79b0a282ef99289b0c2b" title="Access operator.">operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00096"></a>00096     complex&lt;T&gt;&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7eb05d6902b13cfb7cc4f17093fdc17e" title="Unavailable access method.">Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00097"></a>00097     <span class="keyword">const</span> complex&lt;T&gt;&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7eb05d6902b13cfb7cc4f17093fdc17e" title="Unavailable access method.">Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00098"></a>00098     complex&lt;T&gt;&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad840efb36621d28aa9726088cfc29a29" title="Unavailable access method.">Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00099"></a>00099     <span class="keyword">const</span> complex&lt;T&gt;&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad840efb36621d28aa9726088cfc29a29" title="Unavailable access method.">Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00100"></a>00100 
<a name="l00101"></a>00101     T&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#abdacf01dc354d0449b16b1d9e111ec54" title="Returns acces to real part of A(i, j).">ValReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00102"></a>00102     <span class="keyword">const</span> T&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#abdacf01dc354d0449b16b1d9e111ec54" title="Returns acces to real part of A(i, j).">ValReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00103"></a>00103     T&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aee8de4c9149d47935bf84fc04727c7f2" title="Returns acces to imaginary part of A(i, j).">ValImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00104"></a>00104     <span class="keyword">const</span> T&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aee8de4c9149d47935bf84fc04727c7f2" title="Returns acces to imaginary part of A(i, j).">ValImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00105"></a>00105     T&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ac91be8b2868c034acbf161a8b3af9598" title="Returns acces to real part of A(i, j).">GetReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00106"></a>00106     <span class="keyword">const</span> T&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ac91be8b2868c034acbf161a8b3af9598" title="Returns acces to real part of A(i, j).">GetReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00107"></a>00107     T&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a644eff8b81954e1f43c4f9321a866903" title="Returns acces to imaginary part of A(i, j).">GetImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00108"></a>00108     <span class="keyword">const</span> T&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a644eff8b81954e1f43c4f9321a866903" title="Returns acces to imaginary part of A(i, j).">GetImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00109"></a>00109 
<a name="l00110"></a>00110     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2dfca2deb2a823fc64dc51b7c0728f49" title="Sets element (i, j) of matrix.">Set</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; x);
<a name="l00111"></a>00111 
<a name="l00112"></a>00112     <span class="keyword">const</span> T&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a041c76033f939293aa871f2fe45ef22a" title="Returns j-th non-zero value of row i (real part).">ValueReal</a>(<span class="keywordtype">int</span> num_row,<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00113"></a>00113     T&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a041c76033f939293aa871f2fe45ef22a" title="Returns j-th non-zero value of row i (real part).">ValueReal</a>(<span class="keywordtype">int</span> num_row,<span class="keywordtype">int</span> i);
<a name="l00114"></a>00114     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2a3cd76154ac61a43afa42563805ce3b" title="Returns column number of j-th non-zero value of row i (real part).">IndexReal</a>(<span class="keywordtype">int</span> num_row,<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00115"></a>00115     <span class="keywordtype">int</span>&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2a3cd76154ac61a43afa42563805ce3b" title="Returns column number of j-th non-zero value of row i (real part).">IndexReal</a>(<span class="keywordtype">int</span> num_row,<span class="keywordtype">int</span> i);
<a name="l00116"></a>00116     <span class="keyword">const</span> T&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa2d9a2e69a269bf9d15b62d520cb560d" title="Returns j-th non-zero value of row i (imaginary part).">ValueImag</a>(<span class="keywordtype">int</span> num_row,<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00117"></a>00117     T&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa2d9a2e69a269bf9d15b62d520cb560d" title="Returns j-th non-zero value of row i (imaginary part).">ValueImag</a>(<span class="keywordtype">int</span> num_row,<span class="keywordtype">int</span> i);
<a name="l00118"></a>00118     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aedba64fa9b01bce6bb75e9e5a14b26c5" title="Returns column number of j-th non-zero value of row i (imaginary part).">IndexImag</a>(<span class="keywordtype">int</span> num_row,<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00119"></a>00119     <span class="keywordtype">int</span>&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aedba64fa9b01bce6bb75e9e5a14b26c5" title="Returns column number of j-th non-zero value of row i (imaginary part).">IndexImag</a>(<span class="keywordtype">int</span> num_row,<span class="keywordtype">int</span> i);
<a name="l00120"></a>00120 
<a name="l00121"></a>00121     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aab6c31240cd6dcbd442f9a29658f51ea" title="Redefines the real part of the matrix.">SetRealData</a>(<span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php" title="Sparse vector class.">Vector&lt;T, VectSparse, Allocator&gt;</a>*);
<a name="l00122"></a>00122     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a8e3c2dfa0ea5aa57cd8607cca15562f2" title="Redefines the imaginary part of the matrix.">SetImagData</a>(<span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php" title="Sparse vector class.">Vector&lt;T, VectSparse, Allocator&gt;</a>*);
<a name="l00123"></a>00123     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aab6c31240cd6dcbd442f9a29658f51ea" title="Redefines the real part of the matrix.">SetRealData</a>(<span class="keywordtype">int</span>, <span class="keywordtype">int</span>, T*, <span class="keywordtype">int</span>*);
<a name="l00124"></a>00124     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a8e3c2dfa0ea5aa57cd8607cca15562f2" title="Redefines the imaginary part of the matrix.">SetImagData</a>(<span class="keywordtype">int</span>, <span class="keywordtype">int</span>, T*, <span class="keywordtype">int</span>*);
<a name="l00125"></a>00125     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aed1192a75fe2c7d19e545ff6054298e2" title="Clears the matrix without releasing memory.">NullifyReal</a>(<span class="keywordtype">int</span> i);
<a name="l00126"></a>00126     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#adbdb03e02fd8617a23188d6bc3155ec0" title="Clears the matrix without releasing memory.">NullifyImag</a>(<span class="keywordtype">int</span> i);
<a name="l00127"></a>00127     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aed1192a75fe2c7d19e545ff6054298e2" title="Clears the matrix without releasing memory.">NullifyReal</a>();
<a name="l00128"></a>00128     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#adbdb03e02fd8617a23188d6bc3155ec0" title="Clears the matrix without releasing memory.">NullifyImag</a>();
<a name="l00129"></a>00129 
<a name="l00130"></a>00130     <span class="comment">// Convenient functions.</span>
<a name="l00131"></a>00131     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ac24abb2e047eb8b91728023074b3fb13" title="Displays the matrix on the standard output.">Print</a>() <span class="keyword">const</span>;
<a name="l00132"></a>00132     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a9b54a14b64f67242fe9367df4142bd7f" title="Writes the matrix in a file.">Write</a>(<span class="keywordtype">string</span> FileName) <span class="keyword">const</span>;
<a name="l00133"></a>00133     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a9b54a14b64f67242fe9367df4142bd7f" title="Writes the matrix in a file.">Write</a>(ostream&amp; FileStream) <span class="keyword">const</span>;
<a name="l00134"></a>00134     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7095c684ecdb9ef6e9fe9b84462e22ce" title="Writes the matrix in a file.">WriteText</a>(<span class="keywordtype">string</span> FileName) <span class="keyword">const</span>;
<a name="l00135"></a>00135     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7095c684ecdb9ef6e9fe9b84462e22ce" title="Writes the matrix in a file.">WriteText</a>(ostream&amp; FileStream) <span class="keyword">const</span>;
<a name="l00136"></a>00136     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a58c2b7013db7765a01181f928833e530" title="Reads the matrix from a file.">Read</a>(<span class="keywordtype">string</span> FileName);
<a name="l00137"></a>00137     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a58c2b7013db7765a01181f928833e530" title="Reads the matrix from a file.">Read</a>(istream&amp; FileStream);
<a name="l00138"></a>00138     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ab0d4c7f4d291599c0d9cb735dd7d9dc8" title="Reads the matrix from a file.">ReadText</a>(<span class="keywordtype">string</span> FileName);
<a name="l00139"></a>00139     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ab0d4c7f4d291599c0d9cb735dd7d9dc8" title="Reads the matrix from a file.">ReadText</a>(istream&amp; FileStream);
<a name="l00140"></a>00140 
<a name="l00141"></a>00141     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a889d3baa1c4aa6a96b83ba8ca7b4cf4a" title="Assembles the matrix.">Assemble</a>();
<a name="l00142"></a>00142     <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l00143"></a>00143     <span class="keywordtype">void</span> RemoveSmallEntry(<span class="keyword">const</span> T0&amp; epsilon);
<a name="l00144"></a>00144 
<a name="l00145"></a>00145     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a19b48691b03953dffd387ace5cfddd37" title="Matrix is initialized to the identity matrix.">SetIdentity</a>();
<a name="l00146"></a>00146     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa6f51cb3c7779c309578058fd34c5d25" title="Non-zero entries are set to 0 (but not removed).">Zero</a>();
<a name="l00147"></a>00147     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a1fce8c713bc87ca4e06df819ced39554" title="Non-zero entries are filled with values 0, 1, 2, 3 ...">Fill</a>();
<a name="l00148"></a>00148     <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00149"></a>00149     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a1fce8c713bc87ca4e06df819ced39554" title="Non-zero entries are filled with values 0, 1, 2, 3 ...">Fill</a>(<span class="keyword">const</span> complex&lt;T0&gt;&amp; x);
<a name="l00150"></a>00150     <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00151"></a>00151     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; <span class="keyword">operator</span>=
<a name="l00152"></a>00152     (<span class="keyword">const</span> complex&lt;T0&gt;&amp; x);
<a name="l00153"></a>00153     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad1ff9883f030ca9eaada43616865acdb" title="Non-zero entries take a random value.">FillRand</a>();
<a name="l00154"></a>00154 
<a name="l00155"></a>00155   };
<a name="l00156"></a>00156 
<a name="l00157"></a>00157 
<a name="l00159"></a>00159   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00160"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php">00160</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_col_complex_sparse.php">ArrayColComplexSparse</a>, Allocator&gt; :
<a name="l00161"></a>00161     <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse</a>&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;
<a name="l00162"></a>00162   {
<a name="l00163"></a>00163     <span class="comment">// typedef declaration.</span>
<a name="l00164"></a>00164   <span class="keyword">public</span>:
<a name="l00165"></a>00165     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00166"></a>00166     <span class="keyword">typedef</span> Prop property;
<a name="l00167"></a>00167     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_array_col_complex_sparse.php">ArrayColComplexSparse</a> <a class="code" href="class_seldon_1_1_array_col_complex_sparse.php">storage</a>;
<a name="l00168"></a>00168     <span class="keyword">typedef</span> Allocator allocator;
<a name="l00169"></a>00169 
<a name="l00170"></a>00170   <span class="keyword">public</span>:
<a name="l00171"></a>00171     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>();
<a name="l00172"></a>00172     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00173"></a>00173 
<a name="l00174"></a>00174     <span class="comment">// Memory management.</span>
<a name="l00175"></a>00175     <span class="keywordtype">void</span> ClearRealColumn(<span class="keywordtype">int</span> i);
<a name="l00176"></a>00176     <span class="keywordtype">void</span> ClearImagColumn(<span class="keywordtype">int</span> i);
<a name="l00177"></a>00177     <span class="keywordtype">void</span> ReallocateRealColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00178"></a>00178     <span class="keywordtype">void</span> ReallocateImagColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00179"></a>00179     <span class="keywordtype">void</span> ResizeRealColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00180"></a>00180     <span class="keywordtype">void</span> ResizeImagColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00181"></a>00181     <span class="keywordtype">void</span> SwapRealColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> i_);
<a name="l00182"></a>00182     <span class="keywordtype">void</span> SwapImagColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> i_);
<a name="l00183"></a>00183     <span class="keywordtype">void</span> ReplaceRealIndexColumn(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index);
<a name="l00184"></a>00184     <span class="keywordtype">void</span> ReplaceImagIndexColumn(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index);
<a name="l00185"></a>00185 
<a name="l00186"></a>00186     <span class="keywordtype">int</span> GetRealColumnSize(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00187"></a>00187     <span class="keywordtype">int</span> GetImagColumnSize(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00188"></a>00188     <span class="keywordtype">void</span> PrintRealColumn(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00189"></a>00189     <span class="keywordtype">void</span> PrintImagColumn(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00190"></a>00190     <span class="keywordtype">void</span> AssembleRealColumn(<span class="keywordtype">int</span> i);
<a name="l00191"></a>00191     <span class="keywordtype">void</span> AssembleImagColumn(<span class="keywordtype">int</span> i);
<a name="l00192"></a>00192 
<a name="l00193"></a>00193     <span class="keywordtype">void</span> AddInteraction(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; val);
<a name="l00194"></a>00194 
<a name="l00195"></a>00195     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l00196"></a>00196     <span class="keywordtype">void</span> AddInteractionRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l00197"></a>00197                            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1&gt;&amp; val);
<a name="l00198"></a>00198     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l00199"></a>00199     <span class="keywordtype">void</span> AddInteractionColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l00200"></a>00200                               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>,
<a name="l00201"></a>00201                               Alloc1&gt;&amp; val);
<a name="l00202"></a>00202   };
<a name="l00203"></a>00203 
<a name="l00204"></a>00204 
<a name="l00206"></a>00206   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00207"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">00207</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>, Allocator&gt; :
<a name="l00208"></a>00208     <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse</a>&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;
<a name="l00209"></a>00209   {
<a name="l00210"></a>00210     <span class="comment">// typedef declaration.</span>
<a name="l00211"></a>00211   <span class="keyword">public</span>:
<a name="l00212"></a>00212     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00213"></a>00213     <span class="keyword">typedef</span> Prop property;
<a name="l00214"></a>00214     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a> <a class="code" href="class_seldon_1_1_array_row_complex_sparse.php">storage</a>;
<a name="l00215"></a>00215     <span class="keyword">typedef</span> Allocator allocator;
<a name="l00216"></a>00216 
<a name="l00217"></a>00217   <span class="keyword">public</span>:
<a name="l00218"></a>00218     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>();
<a name="l00219"></a>00219     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00220"></a>00220 
<a name="l00221"></a>00221     <span class="comment">// Memory management.</span>
<a name="l00222"></a>00222     <span class="keywordtype">void</span> ClearRealRow(<span class="keywordtype">int</span> i);
<a name="l00223"></a>00223     <span class="keywordtype">void</span> ClearImagRow(<span class="keywordtype">int</span> i);
<a name="l00224"></a>00224     <span class="keywordtype">void</span> ReallocateRealRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00225"></a>00225     <span class="keywordtype">void</span> ReallocateImagRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00226"></a>00226     <span class="keywordtype">void</span> ResizeRealRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00227"></a>00227     <span class="keywordtype">void</span> ResizeImagRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00228"></a>00228     <span class="keywordtype">void</span> SwapRealRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> i_);
<a name="l00229"></a>00229     <span class="keywordtype">void</span> SwapImagRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> i_);
<a name="l00230"></a>00230     <span class="keywordtype">void</span> ReplaceRealIndexRow(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index);
<a name="l00231"></a>00231     <span class="keywordtype">void</span> ReplaceImagIndexRow(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index);
<a name="l00232"></a>00232 
<a name="l00233"></a>00233     <span class="keywordtype">int</span> GetRealRowSize(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00234"></a>00234     <span class="keywordtype">int</span> GetImagRowSize(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00235"></a>00235     <span class="keywordtype">void</span> PrintRealRow(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00236"></a>00236     <span class="keywordtype">void</span> PrintImagRow(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00237"></a>00237     <span class="keywordtype">void</span> AssembleRealRow(<span class="keywordtype">int</span> i);
<a name="l00238"></a>00238     <span class="keywordtype">void</span> AssembleImagRow(<span class="keywordtype">int</span> i);
<a name="l00239"></a>00239 
<a name="l00240"></a>00240     <span class="keywordtype">void</span> AddInteraction(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; val);
<a name="l00241"></a>00241 
<a name="l00242"></a>00242     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l00243"></a>00243     <span class="keywordtype">void</span> AddInteractionRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l00244"></a>00244                            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1&gt;&amp; val);
<a name="l00245"></a>00245     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l00246"></a>00246     <span class="keywordtype">void</span> AddInteractionColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l00247"></a>00247                               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>,
<a name="l00248"></a>00248                               Alloc1&gt;&amp; val);
<a name="l00249"></a>00249   };
<a name="l00250"></a>00250 
<a name="l00251"></a>00251 
<a name="l00253"></a>00253   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00254"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php">00254</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator&gt;:
<a name="l00255"></a>00255     <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse</a>&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;
<a name="l00256"></a>00256   {
<a name="l00257"></a>00257     <span class="comment">// typedef declaration.</span>
<a name="l00258"></a>00258   <span class="keyword">public</span>:
<a name="l00259"></a>00259     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00260"></a>00260     <span class="keyword">typedef</span> Prop property;
<a name="l00261"></a>00261     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a> <a class="code" href="class_seldon_1_1_array_col_sym_complex_sparse.php">storage</a>;
<a name="l00262"></a>00262     <span class="keyword">typedef</span> Allocator allocator;
<a name="l00263"></a>00263 
<a name="l00264"></a>00264   <span class="keyword">public</span>:
<a name="l00265"></a>00265     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>();
<a name="l00266"></a>00266     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00267"></a>00267 
<a name="l00268"></a>00268     complex&lt;T&gt; operator() (<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00269"></a>00269 
<a name="l00270"></a>00270     T&amp; ValReal(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00271"></a>00271     <span class="keyword">const</span> T&amp; ValReal(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00272"></a>00272     T&amp; ValImag(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00273"></a>00273     <span class="keyword">const</span> T&amp; ValImag(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00274"></a>00274     T&amp; GetReal(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00275"></a>00275     <span class="keyword">const</span> T&amp; GetReal(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00276"></a>00276     T&amp; GetImag(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00277"></a>00277     <span class="keyword">const</span> T&amp; GetImag(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00278"></a>00278 
<a name="l00279"></a>00279     <span class="keywordtype">void</span> Set(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; x);
<a name="l00280"></a>00280 
<a name="l00281"></a>00281     <span class="comment">// Memory management.</span>
<a name="l00282"></a>00282     <span class="keywordtype">void</span> ClearRealColumn(<span class="keywordtype">int</span> i);
<a name="l00283"></a>00283     <span class="keywordtype">void</span> ClearImagColumn(<span class="keywordtype">int</span> i);
<a name="l00284"></a>00284     <span class="keywordtype">void</span> ReallocateRealColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00285"></a>00285     <span class="keywordtype">void</span> ReallocateImagColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00286"></a>00286     <span class="keywordtype">void</span> ResizeRealColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00287"></a>00287     <span class="keywordtype">void</span> ResizeImagColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00288"></a>00288     <span class="keywordtype">void</span> SwapRealColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> i_);
<a name="l00289"></a>00289     <span class="keywordtype">void</span> SwapImagColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> i_);
<a name="l00290"></a>00290     <span class="keywordtype">void</span> ReplaceRealIndexColumn(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index);
<a name="l00291"></a>00291     <span class="keywordtype">void</span> ReplaceImagIndexColumn(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index);
<a name="l00292"></a>00292 
<a name="l00293"></a>00293     <span class="keywordtype">int</span> GetRealColumnSize(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00294"></a>00294     <span class="keywordtype">int</span> GetImagColumnSize(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00295"></a>00295     <span class="keywordtype">void</span> PrintRealColumn(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00296"></a>00296     <span class="keywordtype">void</span> PrintImagColumn(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00297"></a>00297     <span class="keywordtype">void</span> AssembleRealColumn(<span class="keywordtype">int</span> i);
<a name="l00298"></a>00298     <span class="keywordtype">void</span> AssembleImagColumn(<span class="keywordtype">int</span> i);
<a name="l00299"></a>00299 
<a name="l00300"></a>00300     <span class="keywordtype">void</span> AddInteraction(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; val);
<a name="l00301"></a>00301 
<a name="l00302"></a>00302     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l00303"></a>00303     <span class="keywordtype">void</span> AddInteractionRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l00304"></a>00304                            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1&gt;&amp; val);
<a name="l00305"></a>00305     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l00306"></a>00306     <span class="keywordtype">void</span> AddInteractionColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l00307"></a>00307                               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>,
<a name="l00308"></a>00308                               Alloc1&gt;&amp; val);
<a name="l00309"></a>00309   };
<a name="l00310"></a>00310 
<a name="l00311"></a>00311 
<a name="l00313"></a>00313   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00314"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php">00314</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator&gt;:
<a name="l00315"></a>00315     <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse</a>&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;
<a name="l00316"></a>00316   {
<a name="l00317"></a>00317     <span class="comment">// typedef declaration.</span>
<a name="l00318"></a>00318   <span class="keyword">public</span>:
<a name="l00319"></a>00319     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00320"></a>00320     <span class="keyword">typedef</span> Prop property;
<a name="l00321"></a>00321     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a> <a class="code" href="class_seldon_1_1_array_row_sym_complex_sparse.php">storage</a>;
<a name="l00322"></a>00322     <span class="keyword">typedef</span> Allocator allocator;
<a name="l00323"></a>00323 
<a name="l00324"></a>00324   <span class="keyword">public</span>:
<a name="l00325"></a>00325     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>();
<a name="l00326"></a>00326     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00327"></a>00327 
<a name="l00328"></a>00328     complex&lt;T&gt; operator() (<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00329"></a>00329 
<a name="l00330"></a>00330     T&amp; ValReal(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00331"></a>00331     <span class="keyword">const</span> T&amp; ValReal(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00332"></a>00332     T&amp; ValImag(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00333"></a>00333     <span class="keyword">const</span> T&amp; ValImag(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00334"></a>00334     T&amp; GetReal(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00335"></a>00335     <span class="keyword">const</span> T&amp; GetReal(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00336"></a>00336     T&amp; GetImag(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00337"></a>00337     <span class="keyword">const</span> T&amp; GetImag(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00338"></a>00338 
<a name="l00339"></a>00339     <span class="keywordtype">void</span> Set(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; x);
<a name="l00340"></a>00340 
<a name="l00341"></a>00341     <span class="comment">// Memory management.</span>
<a name="l00342"></a>00342     <span class="keywordtype">void</span> ClearRealRow(<span class="keywordtype">int</span> i);
<a name="l00343"></a>00343     <span class="keywordtype">void</span> ClearImagRow(<span class="keywordtype">int</span> i);
<a name="l00344"></a>00344     <span class="keywordtype">void</span> ReallocateRealRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00345"></a>00345     <span class="keywordtype">void</span> ReallocateImagRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00346"></a>00346     <span class="keywordtype">void</span> ResizeRealRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00347"></a>00347     <span class="keywordtype">void</span> ResizeImagRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00348"></a>00348     <span class="keywordtype">void</span> SwapRealRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> i_);
<a name="l00349"></a>00349     <span class="keywordtype">void</span> SwapImagRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> i_);
<a name="l00350"></a>00350     <span class="keywordtype">void</span> ReplaceRealIndexRow(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index);
<a name="l00351"></a>00351     <span class="keywordtype">void</span> ReplaceImagIndexRow(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index);
<a name="l00352"></a>00352 
<a name="l00353"></a>00353     <span class="keywordtype">int</span> GetRealRowSize(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00354"></a>00354     <span class="keywordtype">int</span> GetImagRowSize(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00355"></a>00355     <span class="keywordtype">void</span> PrintRealRow(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00356"></a>00356     <span class="keywordtype">void</span> PrintImagRow(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00357"></a>00357     <span class="keywordtype">void</span> AssembleRealRow(<span class="keywordtype">int</span> i);
<a name="l00358"></a>00358     <span class="keywordtype">void</span> AssembleImagRow(<span class="keywordtype">int</span> i);
<a name="l00359"></a>00359 
<a name="l00360"></a>00360     <span class="keywordtype">void</span> AddInteraction(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; val);
<a name="l00361"></a>00361 
<a name="l00362"></a>00362     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l00363"></a>00363     <span class="keywordtype">void</span> AddInteractionRow(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l00364"></a>00364                            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1&gt;&amp; val);
<a name="l00365"></a>00365     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l00366"></a>00366     <span class="keywordtype">void</span> AddInteractionColumn(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l00367"></a>00367                               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>,
<a name="l00368"></a>00368                               Alloc1&gt;&amp; val);
<a name="l00369"></a>00369   };
<a name="l00370"></a>00370 
<a name="l00371"></a>00371 
<a name="l00372"></a>00372 } <span class="comment">// namespace Seldon</span>
<a name="l00373"></a>00373 
<a name="l00374"></a>00374 <span class="preprocessor">#define SELDON_FILE_MATRIX_ARRAY_COMPLEX_SPARSE_HXX</span>
<a name="l00375"></a>00375 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
