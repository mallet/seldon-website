<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Vector&lt; T, PETScSeq, Allocator &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a>, including all inherited members.<table>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a922a0d69853d84d718dc9102fa8a7f4f">Append</a>(const T &amp;x)</td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a87b2f7caa1f91402c0943f4b0fd7a4ac">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#ad158e7fe8ecc8df073b85ead2b0c5584">Copy</a>(const Vector&lt; T, PETScSeq, Allocator &gt; &amp;X)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a75a3eafd113c8c4f88227dc0cc1e787f">Copy</a>(const Vec &amp;petsc_vector)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a0b19d99fda0bdd859486db905f3e758f">Seldon::PETScVector::Copy</a>(const PETScVector&lt; T, Allocator &gt; &amp;X)</td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>data_</b> (defined in <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#ab1fc92c50dd23964a3ce240c1d5c953a">Fill</a>()</td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a9a354dbfc573d2eec31be3707d1d6aae">Fill</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#aeea94a272f075d781c2a71d21072ccf4">FillRand</a>()</td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a7ece84387ef984021d4606b093c2497d">Flush</a>()</td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a">GetData</a>() const </td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a454aea78c82c4317dfe4160cc7c95afa">GetDataConst</a>() const </td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a0aadc006977de037eb3ee550683e3f19">GetDataConstVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a4b1dfd7f5db9600d9e65826c4462d37e">GetDataSize</a>() const </td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#ad0f5184bd9eec6fca70c54e459ced78f">GetDataVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a075daae3607a4767a77a3de60ab30ba7">GetLength</a>() const </td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a0dfb99ee603ed3f578132425f5afed32">GetLocalM</a>() const </td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae">GetM</a>() const </td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a1216ed94d76cd4cc9a57203781deb864">GetNormInf</a>() const </td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a7ecdd5c6469cdaa4e263f6ff71fbbf00">GetNormInfIndex</a>() const </td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#ac288674b5e0cbab4289b783279c20347">GetPetscVector</a>()</td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#af186015f5c0c35d348f8f7a57cdbaa64">GetPetscVector</a>() const </td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a0a0a3a91dc5491fd88f6803b208db9ac">GetProcessorRange</a>(int &amp;i, int &amp;j) const </td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a839880a3113c1885ead70d79fade0294">GetSize</a>() const </td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>m_</b> (defined in <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a70bd7cbade506f2690f742931f6d46c2">mpi_communicator_</a></td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a73f72da9e6a83f0b54aa4e6f08339159">Nullify</a>()</td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a415d9ec2b7070127f200a40abfa09df8">operator()</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#aed8a3631e999991258cc5cc386b4d266">operator*=</a>(const T0 &amp;X)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a30a826bc72f41ad614457044c541022e">operator=</a>(const Vector&lt; T, PETScSeq, Allocator &gt; &amp;X)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#acb9780fe5a7e5771e9a8effb7efb8ca1">operator=</a>(const T0 &amp;X)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a323f6bd490f539750d525646b15583f6">petsc_vector_</a></td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a39bcb5ba22a031f8d6fbebea571a3c03">petsc_vector_deallocated_</a></td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a6ec09b49f9c964762ab9120380dc54d6">PETScVector</a>()</td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td><code> [explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a3e1634d2da86c544edc3a05c525b9bd7">PETScVector</a>(int i, MPI_Comm mpi_communicator=MPI_COMM_WORLD)</td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td><code> [explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#ae865dd96bb9824937d37a8227b03e2b9">PETScVector</a>(Vec &amp;petsc_vector)</td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a4946812e1545f73a5304e38627339c34">PETScVector</a>(const PETScVector&lt; T, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#aa12c9192437db3a0d15566311646150d">Print</a>() const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a8dd852fadb0f06aee6372585e6e36342">Read</a>(string FileName, bool with_size=true)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#ad2f97633a1f14c0ba7d8ae19d911458f">Read</a>(istream &amp;FileStream, bool with_size=true)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#acdbdc8ad44161af1fb63e6b8341257d9">ReadText</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#ab1b2d9c09ca0354b8564ec87b6f9f1c2">ReadText</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a1d1b36c91fe69fd44552e22c2808415f">Reallocate</a>(int i)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a713d118c2c4a588ed3d043f45619e27b">Resize</a>(int i)</td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#aa9c4710028825f7e8d4c4b38e28d77bc">SetBuffer</a>(int i, T value, InsertMode insert_mode=INSERT_VALUES)</td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#ac6b7117d6550912d70c1b929be557f27">SetCommunicator</a>(MPI_Comm mpi_communicator)</td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SetData</b>(int i, pointer data) (defined in <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>storage</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>value_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>vect_allocator_</b> (defined in <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td><code> [protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a1cf9876e5d323efa8918954a01679394">Vector</a>()</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td><code> [explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a690f575c3d09274138868ce4ab2e5970">Vector</a>(int i, MPI_Comm mpi_communicator=MPI_COMM_WORLD)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td><code> [explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a48375793ed0a05a7c62a872568386539">Vector</a>(Vec &amp;petsc_vector)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a1f201ae576bd2dd972fc238206a3a724">Vector</a>(const Vector&lt; T, PETScSeq, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#ac8a6cee506f094504138943629b9dcbb">Vector_Base</a>()</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#aa5d9ca198d4175b9baf9a0543f7376be">Vector_Base</a>(int i)</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td><code> [inline, explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a27796125b93f3a771a5c92e8196c9229">Vector_Base</a>(const Vector_Base&lt; T, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#adb0698f98268c84a3a762b350931501f">Write</a>(string FileName, bool with_size=true) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a0ed3850719a72ef0e1407942f5d59d84">Write</a>(ostream &amp;FileStream, bool with_size=true) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a10f0723ad305b895314eae22d663a13c">WriteText</a>(string FileName) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a9f0b2cdc8c6305b51b578d5ce3dd9b87">WriteText</a>(ostream &amp;FileStream) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a0b318ae92a2a224db5f8a80332b8d25f">Zero</a>()</td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#ae416e60119dccc2e4f8d2ee949290ccc">~PETScVector</a>()</td><td><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a8305af2f1806473a28c7fb71d8189cd2">~Vector</a>()</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a6da510796ebef867765ea3a037debb1b">~Vector_Base</a>()</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
