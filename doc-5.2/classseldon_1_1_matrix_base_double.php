<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><b>seldon</b>      </li>
      <li><a class="el" href="classseldon_1_1_matrix_base_double.php">MatrixBaseDouble</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pub-attribs">Public Attributes</a>  </div>
  <div class="headertitle">
<h1>seldon::MatrixBaseDouble Class Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="seldon::MatrixBaseDouble" --><!-- doxytag: inherits="seldon::_object" --><div class="dynheader">
Inheritance diagram for seldon::MatrixBaseDouble:</div>
<div class="dyncontent">
 <div class="center">
  <img src="classseldon_1_1_matrix_base_double.png" usemap="#seldon::MatrixBaseDouble_map" alt=""/>
  <map id="seldon::MatrixBaseDouble_map" name="seldon::MatrixBaseDouble_map">
<area href="classseldon_1_1__object.php" alt="seldon::_object" shape="rect" coords="104,0,302,24"/>
<area href="classseldon_1_1_base_matrix_sparse_double.php" alt="seldon::BaseMatrixSparseDouble" shape="rect" coords="0,112,198,136"/>
<area href="classseldon_1_1_matrix_pointers_double.php" alt="seldon::MatrixPointersDouble" shape="rect" coords="208,112,406,136"/>
<area href="classseldon_1_1_matrix_sparse_double.php" alt="seldon::MatrixSparseDouble" shape="rect" coords="0,168,198,192"/>
<area href="classseldon_1_1_matrix_double.php" alt="seldon::MatrixDouble" shape="rect" coords="208,168,406,192"/>
</map>
</div>

<p><a href="classseldon_1_1_matrix_base_double-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adf30d0f674a9738b9c129190c65fb007"></a><!-- doxytag: member="seldon::MatrixBaseDouble::__init__" ref="adf30d0f674a9738b9c129190c65fb007" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__init__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a10557b0ca5e128907518343d38f9d430"></a><!-- doxytag: member="seldon::MatrixBaseDouble::GetM" ref="a10557b0ca5e128907518343d38f9d430" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetM</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a556688675412c7c5d063cf313ad4997f"></a><!-- doxytag: member="seldon::MatrixBaseDouble::GetN" ref="a556688675412c7c5d063cf313ad4997f" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetN</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a569343e9fe5001da21c874c4ce1a11f8"></a><!-- doxytag: member="seldon::MatrixBaseDouble::GetSize" ref="a569343e9fe5001da21c874c4ce1a11f8" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetSize</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac25edf930e9247880287294c0e9d4b54"></a><!-- doxytag: member="seldon::MatrixBaseDouble::GetData" ref="ac25edf930e9247880287294c0e9d4b54" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetData</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a475019abd6e2a17c673efa0fb9bc2643"></a><!-- doxytag: member="seldon::MatrixBaseDouble::GetDataConst" ref="a475019abd6e2a17c673efa0fb9bc2643" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataConst</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a100846ac7e1a2610cf1167a654328de4"></a><!-- doxytag: member="seldon::MatrixBaseDouble::GetDataVoid" ref="a100846ac7e1a2610cf1167a654328de4" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataVoid</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6a8f262d641f7d43ffebcf4980941b78"></a><!-- doxytag: member="seldon::MatrixBaseDouble::GetDataConstVoid" ref="a6a8f262d641f7d43ffebcf4980941b78" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataConstVoid</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9d1c2383fbdcdb9afabe800635103663"></a><!-- doxytag: member="seldon::MatrixBaseDouble::GetAllocator" ref="a9d1c2383fbdcdb9afabe800635103663" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetAllocator</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-attribs"></a>
Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2ce11ac1904e65a88d8ed8ac326a8b1a"></a><!-- doxytag: member="seldon::MatrixBaseDouble::this" ref="a2ce11ac1904e65a88d8ed8ac326a8b1a" args="" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><b>this</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>

<p>Definition at line <a class="el" href="seldon_8py_source.php#l01031">1031</a> of file <a class="el" href="seldon_8py_source.php">seldon.py</a>.</p>
<hr/>The documentation for this class was generated from the following file:<ul>
<li><a class="el" href="seldon_8py_source.php">seldon.py</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
