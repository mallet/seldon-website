<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/optimization/NLoptSolver.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2010 INRIA</span>
<a name="l00002"></a>00002 <span class="comment">// Author(s): Marc Fragu</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_COMPUTATION_OPTIMIZATION_NLOPTSOLVER_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span><span class="preprocessor">#define SELDON_COMPUTATION_OPTIMIZATION_NLOPTSOLVER_CXX</span>
<a name="l00023"></a>00023 <span class="preprocessor"></span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="preprocessor">#include &quot;NLoptSolver.hxx&quot;</span>
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 <span class="keyword">namespace </span>Seldon
<a name="l00029"></a>00029 {
<a name="l00030"></a><a class="code" href="class_seldon_1_1_n_lopt_solver.php#ad775e71c990c2d8f08feaf0399a1a916">00030</a> 
<a name="l00031"></a>00031 
<a name="l00033"></a>00033   <a class="code" href="class_seldon_1_1_n_lopt_solver.php#ad775e71c990c2d8f08feaf0399a1a916" title="Default constructor.">NLoptSolver::NLoptSolver</a>()
<a name="l00034"></a>00034   {
<a name="l00035"></a>00035   }
<a name="l00036"></a><a class="code" href="class_seldon_1_1_n_lopt_solver.php#adcf818c843283a20eef4d17a974fd6cc">00036</a> 
<a name="l00037"></a>00037 
<a name="l00039"></a>00039   <a class="code" href="class_seldon_1_1_n_lopt_solver.php#adcf818c843283a20eef4d17a974fd6cc" title="Destructor.">NLoptSolver::~NLoptSolver</a>()
<a name="l00040"></a>00040   {
<a name="l00041"></a>00041   }
<a name="l00042"></a>00042 
<a name="l00043"></a>00043 
<a name="l00045"></a>00045 
<a name="l00072"></a>00072   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#ad80720692c1df8718044c24ecbc98372" title="Initializations.">NLoptSolver::Initialize</a>(<span class="keywordtype">int</span> Nparameter, <span class="keywordtype">string</span> algorithm,
<a name="l00073"></a>00073                                <span class="keywordtype">double</span> parameter_tolerance,
<a name="l00074"></a>00074                                <span class="keywordtype">double</span> cost_function_tolerance,
<a name="l00075"></a>00075                                <span class="keywordtype">int</span> Niteration_max)
<a name="l00076"></a>00076   {
<a name="l00077"></a>00077     map&lt;string, nlopt::algorithm&gt; algorithm_map;
<a name="l00078"></a>00078     map&lt;string, nlopt::algorithm&gt;::iterator it;
<a name="l00079"></a>00079     algorithm_map[<span class="stringliteral">&quot;GN_DIRECT&quot;</span>] = nlopt::GN_DIRECT;
<a name="l00080"></a>00080     algorithm_map[<span class="stringliteral">&quot;GN_DIRECT_L&quot;</span>] = nlopt::GN_DIRECT_L;
<a name="l00081"></a>00081     algorithm_map[<span class="stringliteral">&quot;GN_DIRECT_L_RAND&quot;</span>] = nlopt::GN_DIRECT_L_RAND;
<a name="l00082"></a>00082     algorithm_map[<span class="stringliteral">&quot;GN_DIRECT_NOSCAL&quot;</span>] = nlopt::GN_DIRECT_NOSCAL;
<a name="l00083"></a>00083     algorithm_map[<span class="stringliteral">&quot;GN_DIRECT_L_NOSCAL&quot;</span>] = nlopt::GN_DIRECT_L_NOSCAL;
<a name="l00084"></a>00084     algorithm_map[<span class="stringliteral">&quot;GN_DIRECT_L_RAND_NOSCAL&quot;</span>]
<a name="l00085"></a>00085       = nlopt::GN_DIRECT_L_RAND_NOSCAL;
<a name="l00086"></a>00086     algorithm_map[<span class="stringliteral">&quot;GN_ORIG_DIRECT&quot;</span>] = nlopt::GN_ORIG_DIRECT;
<a name="l00087"></a>00087     algorithm_map[<span class="stringliteral">&quot;GN_ORIG_DIRECT_L&quot;</span>] = nlopt::GN_ORIG_DIRECT_L;
<a name="l00088"></a>00088     algorithm_map[<span class="stringliteral">&quot;GD_STOGO&quot;</span>] = nlopt::GD_STOGO;
<a name="l00089"></a>00089     algorithm_map[<span class="stringliteral">&quot;GD_STOGO_RAND&quot;</span>] = nlopt::GD_STOGO_RAND;
<a name="l00090"></a>00090     algorithm_map[<span class="stringliteral">&quot;LD_LBFGS_NOCEDAL&quot;</span>] = nlopt::LD_LBFGS_NOCEDAL;
<a name="l00091"></a>00091     algorithm_map[<span class="stringliteral">&quot;LD_LBFGS&quot;</span>] = nlopt::LD_LBFGS;
<a name="l00092"></a>00092     algorithm_map[<span class="stringliteral">&quot;LN_PRAXIS&quot;</span>] = nlopt::LN_PRAXIS;
<a name="l00093"></a>00093     algorithm_map[<span class="stringliteral">&quot;LD_VAR1&quot;</span>] = nlopt::LD_VAR1;
<a name="l00094"></a>00094     algorithm_map[<span class="stringliteral">&quot;LD_VAR2&quot;</span>] = nlopt::LD_VAR2;
<a name="l00095"></a>00095     algorithm_map[<span class="stringliteral">&quot;LD_TNEWTON&quot;</span>] = nlopt::LD_TNEWTON;
<a name="l00096"></a>00096     algorithm_map[<span class="stringliteral">&quot;LD_TNEWTON_RESTART&quot;</span>] = nlopt::LD_TNEWTON_RESTART;
<a name="l00097"></a>00097     algorithm_map[<span class="stringliteral">&quot;LD_TNEWTON_PRECOND&quot;</span>] = nlopt::LD_TNEWTON_PRECOND;
<a name="l00098"></a>00098     algorithm_map[<span class="stringliteral">&quot;LD_TNEWTON_PRECOND_RESTART&quot;</span>]
<a name="l00099"></a>00099       = nlopt::LD_TNEWTON_PRECOND_RESTART;
<a name="l00100"></a>00100     algorithm_map[<span class="stringliteral">&quot;GN_CRS2_LM&quot;</span>] = nlopt::GN_CRS2_LM;
<a name="l00101"></a>00101     algorithm_map[<span class="stringliteral">&quot;GN_MLSL&quot;</span>] = nlopt::GN_MLSL;
<a name="l00102"></a>00102     algorithm_map[<span class="stringliteral">&quot;GD_MLSL&quot;</span>] = nlopt::GD_MLSL;
<a name="l00103"></a>00103     algorithm_map[<span class="stringliteral">&quot;GN_MLSL_LDS&quot;</span>] = nlopt::GN_MLSL_LDS;
<a name="l00104"></a>00104     algorithm_map[<span class="stringliteral">&quot;GD_MLSL_LDS&quot;</span>] = nlopt::GD_MLSL_LDS;
<a name="l00105"></a>00105     algorithm_map[<span class="stringliteral">&quot;LD_MMA&quot;</span>] = nlopt::LD_MMA;
<a name="l00106"></a>00106     algorithm_map[<span class="stringliteral">&quot;LN_COBYLA&quot;</span>] = nlopt::LN_COBYLA;
<a name="l00107"></a>00107     algorithm_map[<span class="stringliteral">&quot;LN_NEWUOA&quot;</span>] = nlopt::LN_NEWUOA;
<a name="l00108"></a>00108     algorithm_map[<span class="stringliteral">&quot;LN_NEWUOA_BOUND&quot;</span>] = nlopt::LN_NEWUOA_BOUND;
<a name="l00109"></a>00109     algorithm_map[<span class="stringliteral">&quot;LN_NELDERMEAD&quot;</span>] = nlopt::LN_NELDERMEAD;
<a name="l00110"></a>00110     algorithm_map[<span class="stringliteral">&quot;LN_SBPLX&quot;</span>] = nlopt::LN_SBPLX;
<a name="l00111"></a>00111     algorithm_map[<span class="stringliteral">&quot;LN_AUGLAG&quot;</span>] = nlopt::LN_AUGLAG;
<a name="l00112"></a>00112     algorithm_map[<span class="stringliteral">&quot;LD_AUGLAG&quot;</span>] = nlopt::LD_AUGLAG;
<a name="l00113"></a>00113     algorithm_map[<span class="stringliteral">&quot;LN_AUGLAG_EQ&quot;</span>] = nlopt::LN_AUGLAG_EQ;
<a name="l00114"></a>00114     algorithm_map[<span class="stringliteral">&quot;LD_AUGLAG_EQ&quot;</span>] = nlopt::LD_AUGLAG_EQ;
<a name="l00115"></a>00115     algorithm_map[<span class="stringliteral">&quot;LN_BOBYQA&quot;</span>] = nlopt::LN_BOBYQA;
<a name="l00116"></a>00116     algorithm_map[<span class="stringliteral">&quot;GN_ISRES&quot;</span>] = nlopt::GN_ISRES;
<a name="l00117"></a>00117     algorithm_map[<span class="stringliteral">&quot;AUGLAG&quot;</span>] = nlopt::AUGLAG;
<a name="l00118"></a>00118     algorithm_map[<span class="stringliteral">&quot;AUGLAG_EQ&quot;</span>] = nlopt::AUGLAG_EQ;
<a name="l00119"></a>00119     algorithm_map[<span class="stringliteral">&quot;G_MLSL&quot;</span>] = nlopt::G_MLSL;
<a name="l00120"></a>00120     algorithm_map[<span class="stringliteral">&quot;G_MLSL_LDS&quot;</span>] = nlopt::G_MLSL_LDS;
<a name="l00121"></a>00121     algorithm_map[<span class="stringliteral">&quot;LD_SLSQP&quot;</span>] = nlopt::LD_SLSQP;
<a name="l00122"></a>00122 
<a name="l00123"></a>00123     it = algorithm_map.find(algorithm);
<a name="l00124"></a>00124     <span class="keywordflow">if</span> (it == algorithm_map.end())
<a name="l00125"></a>00125       <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;void NLoptSolver::Initialize(int, string, double)&quot;</span>,
<a name="l00126"></a>00126                     <span class="stringliteral">&quot;Unknown algorithm. Implemented algorithms are:&quot;</span>
<a name="l00127"></a>00127                     <span class="stringliteral">&quot; GN_DIRECT, &quot;</span>
<a name="l00128"></a>00128                     <span class="stringliteral">&quot;GN_DIRECT_L, GN_DIRECT_L_RAND, &quot;</span>
<a name="l00129"></a>00129                     <span class="stringliteral">&quot;GN_DIRECT_NOSCAL, &quot;</span>
<a name="l00130"></a>00130                     <span class="stringliteral">&quot;GN_DIRECT_L_NOSCAL, GN_DIRECT_L_RAND_NOSCAL,&quot;</span>
<a name="l00131"></a>00131                     <span class="stringliteral">&quot; GN_ORIG_DIRECT, &quot;</span>
<a name="l00132"></a>00132                     <span class="stringliteral">&quot;GN_ORIG_DIRECT_L, GD_STOGO, GD_STOGO_RAND, &quot;</span>
<a name="l00133"></a>00133                     <span class="stringliteral">&quot;LD_LBFGS_NOCEDAL, LD_LBFGS, LN_PRAXIS, &quot;</span>
<a name="l00134"></a>00134                     <span class="stringliteral">&quot;LD_VAR1, LD_VAR2, LD_TNEWTON, &quot;</span>
<a name="l00135"></a>00135                     <span class="stringliteral">&quot;LD_TNEWTON_RESTART, LD_TNEWTON_PRECOND, &quot;</span>
<a name="l00136"></a>00136                     <span class="stringliteral">&quot;LD_TNEWTON_PRECOND_RESTART, GN_CRS2_LM, &quot;</span>
<a name="l00137"></a>00137                     <span class="stringliteral">&quot;GN_MLSL,&quot;</span>
<a name="l00138"></a>00138                     <span class="stringliteral">&quot;GD_MLSL, GN_MLSL_LDS, GD_MLSL_LDS, &quot;</span>
<a name="l00139"></a>00139                     <span class="stringliteral">&quot;LD_MMA, LN_COBYLA, LN_NEWUOA, &quot;</span>
<a name="l00140"></a>00140                     <span class="stringliteral">&quot;LN_NEWUOA_BOUND, LN_NELDERMEAD, &quot;</span>
<a name="l00141"></a>00141                     <span class="stringliteral">&quot;LN_SBPLX, LN_AUGLAG, LD_AUGLAG, &quot;</span>
<a name="l00142"></a>00142                     <span class="stringliteral">&quot;LN_AUGLAG_EQ, LD_AUGLAG_EQ, LN_BOBYQA, &quot;</span>
<a name="l00143"></a>00143                     <span class="stringliteral">&quot;GN_ISRES, AUGLAG, AUGLAG_EQ,&quot;</span>
<a name="l00144"></a>00144                     <span class="stringliteral">&quot;G_MLSL, G_MLSL_LDS, LD_SLSQP, NUM_ALGORITHMS&quot;</span>);
<a name="l00145"></a>00145     <span class="keywordflow">else</span>
<a name="l00146"></a>00146       <a class="code" href="class_seldon_1_1_n_lopt_solver.php#aad0beaa06728577f5f7c39c2989f81ed" title="Optimization algorithm.">algorithm_</a> = it-&gt;second;
<a name="l00147"></a>00147 
<a name="l00148"></a>00148     <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a0236ea3add112a572ed2326a12306ded" title="NLopt optimization solver.">opt_</a> = <a class="code" href="classnlopt_1_1_seldon_opt.php">nlopt::SeldonOpt</a>(<a class="code" href="class_seldon_1_1_n_lopt_solver.php#aad0beaa06728577f5f7c39c2989f81ed" title="Optimization algorithm.">algorithm_</a>, Nparameter);
<a name="l00149"></a>00149 
<a name="l00150"></a>00150     <a class="code" href="class_seldon_1_1_n_lopt_solver.php#adefce6eb95498ffb35690ce4c8c7f637" title="Relative tolerance on the optimization parameters.">parameter_tolerance_</a> = parameter_tolerance;
<a name="l00151"></a>00151 
<a name="l00152"></a>00152     <a class="code" href="class_seldon_1_1_n_lopt_solver.php#afd6a5c3ebbb73a71fbd412a641eca860" title="Relative tolerance on the cost function.">cost_function_tolerance_</a> = cost_function_tolerance;
<a name="l00153"></a>00153 
<a name="l00154"></a>00154     <a class="code" href="class_seldon_1_1_n_lopt_solver.php#ac3340df79837a4cdde5560939ed01989" title="Maximum number of function evaluations. It is ignored if it is non-positive.">Niteration_max_</a> = Niteration_max;
<a name="l00155"></a>00155   }
<a name="l00156"></a>00156 
<a name="l00157"></a>00157 
<a name="l00159"></a><a class="code" href="class_seldon_1_1_n_lopt_solver.php#a6f92bbc59849ce8489da5210dfd6d994">00159</a> 
<a name="l00162"></a>00162   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a6f92bbc59849ce8489da5210dfd6d994" title="Sets lower bounds on the parameters.">NLoptSolver::SetLowerBound</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double&gt;</a>&amp; lower_bound)
<a name="l00163"></a>00163   {
<a name="l00164"></a>00164     <span class="keywordflow">if</span> (lower_bound.GetSize() != 0)
<a name="l00165"></a>00165       <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a0236ea3add112a572ed2326a12306ded" title="NLopt optimization solver.">opt_</a>.set_lower_bounds(lower_bound);
<a name="l00166"></a>00166   }
<a name="l00167"></a>00167 
<a name="l00168"></a>00168 
<a name="l00170"></a><a class="code" href="class_seldon_1_1_n_lopt_solver.php#a46fee6721c1a4172bae0ce10870d4442">00170</a> 
<a name="l00173"></a>00173   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a46fee6721c1a4172bae0ce10870d4442" title="Sets upper bounds on the parameters.">NLoptSolver::SetUpperBound</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double&gt;</a>&amp; upper_bound)
<a name="l00174"></a>00174   {
<a name="l00175"></a>00175     <span class="keywordflow">if</span> (upper_bound.GetSize() != 0)
<a name="l00176"></a>00176       <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a0236ea3add112a572ed2326a12306ded" title="NLopt optimization solver.">opt_</a>.set_upper_bounds(upper_bound);
<a name="l00177"></a>00177   }
<a name="l00178"></a>00178 
<a name="l00179"></a>00179 
<a name="l00181"></a>00181 
<a name="l00189"></a>00189   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#aa74cb3aca730ca6444fff8bd600f3be7" title="Sets the relative tolerance on the parameters.">NLoptSolver::SetParameterTolerance</a>(<span class="keywordtype">double</span> tolerance)
<a name="l00190"></a>00190   {
<a name="l00191"></a>00191     <a class="code" href="class_seldon_1_1_n_lopt_solver.php#adefce6eb95498ffb35690ce4c8c7f637" title="Relative tolerance on the optimization parameters.">parameter_tolerance_</a> = tolerance;
<a name="l00192"></a>00192   }
<a name="l00193"></a>00193 
<a name="l00194"></a>00194 
<a name="l00196"></a>00196 
<a name="l00204"></a>00204   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a44ed740163ffdc82a1eda9a47001e58a" title="Sets the relative tolerance on the cost function.">NLoptSolver::SetCostFunctionTolerance</a>(<span class="keywordtype">double</span> tolerance)
<a name="l00205"></a>00205   {
<a name="l00206"></a>00206     <a class="code" href="class_seldon_1_1_n_lopt_solver.php#afd6a5c3ebbb73a71fbd412a641eca860" title="Relative tolerance on the cost function.">cost_function_tolerance_</a> = tolerance;
<a name="l00207"></a>00207   }
<a name="l00208"></a>00208 
<a name="l00209"></a>00209 
<a name="l00211"></a>00211 
<a name="l00215"></a>00215   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#ae8d18863be9cbbf79bc14e5fdc905a39" title="Sets the maximum number of cost function evaluations.">NLoptSolver::SetNiterationMax</a>(<span class="keywordtype">int</span> Niteration_max)
<a name="l00216"></a>00216   {
<a name="l00217"></a>00217     <a class="code" href="class_seldon_1_1_n_lopt_solver.php#ac3340df79837a4cdde5560939ed01989" title="Maximum number of function evaluations. It is ignored if it is non-positive.">Niteration_max_</a> = Niteration_max;
<a name="l00218"></a>00218   }
<a name="l00219"></a>00219 
<a name="l00220"></a>00220 
<a name="l00222"></a>00222 
<a name="l00228"></a>00228   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a4e7c4cb0c59c99b234c89b23ffd56927" title="Gets the relative tolerance on the parameters.">NLoptSolver::GetParameterTolerance</a>(<span class="keywordtype">double</span>&amp; tolerance)<span class="keyword"> const</span>
<a name="l00229"></a>00229 <span class="keyword">  </span>{
<a name="l00230"></a>00230     tolerance = <a class="code" href="class_seldon_1_1_n_lopt_solver.php#adefce6eb95498ffb35690ce4c8c7f637" title="Relative tolerance on the optimization parameters.">parameter_tolerance_</a>;
<a name="l00231"></a>00231   }
<a name="l00232"></a>00232 
<a name="l00233"></a>00233 
<a name="l00235"></a>00235 
<a name="l00241"></a>00241   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#ab585d1ac63039aa34eb41ba3a1548817" title="Gets the relative tolerance on the cost function.">NLoptSolver::GetCostFunctionTolerance</a>(<span class="keywordtype">double</span>&amp; tolerance)<span class="keyword"> const</span>
<a name="l00242"></a>00242 <span class="keyword">  </span>{
<a name="l00243"></a>00243     tolerance = <a class="code" href="class_seldon_1_1_n_lopt_solver.php#afd6a5c3ebbb73a71fbd412a641eca860" title="Relative tolerance on the cost function.">cost_function_tolerance_</a>;
<a name="l00244"></a>00244   }
<a name="l00245"></a>00245 
<a name="l00246"></a>00246 
<a name="l00248"></a><a class="code" href="class_seldon_1_1_n_lopt_solver.php#ac584f3bed8e3326faa50fff3c7004b62">00248</a> 
<a name="l00251"></a>00251   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#ac584f3bed8e3326faa50fff3c7004b62" title="Gets the maximum number of cost function evaluations.">NLoptSolver::GetNiterationMax</a>(<span class="keywordtype">int</span>&amp; Niteration_max)<span class="keyword"> const</span>
<a name="l00252"></a>00252 <span class="keyword">  </span>{
<a name="l00253"></a>00253     Niteration_max = <a class="code" href="class_seldon_1_1_n_lopt_solver.php#ac3340df79837a4cdde5560939ed01989" title="Maximum number of function evaluations. It is ignored if it is non-positive.">Niteration_max_</a>;
<a name="l00254"></a>00254   }
<a name="l00255"></a>00255 
<a name="l00256"></a>00256 
<a name="l00258"></a><a class="code" href="class_seldon_1_1_n_lopt_solver.php#adb63bff470ea8bbc8df7cc78207a597f">00258</a> 
<a name="l00261"></a>00261   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#adb63bff470ea8bbc8df7cc78207a597f" title="Sets the parameters.">NLoptSolver::SetParameter</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double&gt;</a>&amp; parameter)
<a name="l00262"></a>00262   {
<a name="l00263"></a>00263     <a class="code" href="class_seldon_1_1_n_lopt_solver.php#aade135d662e35906b7cae2ecbd1d789c" title="The vector that stores parameters values. Before optimization, stores the initial...">parameter_</a>.Reallocate(parameter.GetM());
<a name="l00264"></a>00264     Copy(parameter, <a class="code" href="class_seldon_1_1_n_lopt_solver.php#aade135d662e35906b7cae2ecbd1d789c" title="The vector that stores parameters values. Before optimization, stores the initial...">parameter_</a>);
<a name="l00265"></a>00265   }
<a name="l00266"></a>00266 
<a name="l00267"></a>00267 
<a name="l00269"></a><a class="code" href="class_seldon_1_1_n_lopt_solver.php#a2de56e3f3de295800e412039ca5b0e69">00269</a> 
<a name="l00272"></a>00272   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a2de56e3f3de295800e412039ca5b0e69" title="Gets the parameters.">NLoptSolver::GetParameter</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double&gt;</a>&amp; parameter)<span class="keyword"> const</span>
<a name="l00273"></a>00273 <span class="keyword">  </span>{
<a name="l00274"></a>00274     parameter.Reallocate(<a class="code" href="class_seldon_1_1_n_lopt_solver.php#aade135d662e35906b7cae2ecbd1d789c" title="The vector that stores parameters values. Before optimization, stores the initial...">parameter_</a>.GetM());
<a name="l00275"></a>00275     Copy(<a class="code" href="class_seldon_1_1_n_lopt_solver.php#aade135d662e35906b7cae2ecbd1d789c" title="The vector that stores parameters values. Before optimization, stores the initial...">parameter_</a>, parameter);
<a name="l00276"></a>00276   }
<a name="l00277"></a>00277 
<a name="l00278"></a>00278 
<a name="l00280"></a>00280 
<a name="l00293"></a>00293   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a15954e351f26ae09799c07e0009ce864" title="Optimization.">NLoptSolver::Optimize</a>(cost_ptr cost, <span class="keywordtype">void</span>* argument)
<a name="l00294"></a>00294   {
<a name="l00295"></a>00295     <span class="keywordtype">int</span> Nparameter;
<a name="l00296"></a>00296     <span class="keywordflow">if</span> (0 == (Nparameter = <a class="code" href="class_seldon_1_1_n_lopt_solver.php#aade135d662e35906b7cae2ecbd1d789c" title="The vector that stores parameters values. Before optimization, stores the initial...">parameter_</a>.GetM()))
<a name="l00297"></a>00297       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;NLoptSolver::Optimize()&quot;</span>,
<a name="l00298"></a>00298                           <span class="stringliteral">&quot;The vector of parameters to be optimized&quot;</span>
<a name="l00299"></a>00299                           <span class="stringliteral">&quot; is empty.&quot;</span>);
<a name="l00300"></a>00300 
<a name="l00301"></a>00301     <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a0236ea3add112a572ed2326a12306ded" title="NLopt optimization solver.">opt_</a>.set_min_objective(cost, argument);
<a name="l00302"></a>00302     <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a0236ea3add112a572ed2326a12306ded" title="NLopt optimization solver.">opt_</a>.set_xtol_rel(<a class="code" href="class_seldon_1_1_n_lopt_solver.php#adefce6eb95498ffb35690ce4c8c7f637" title="Relative tolerance on the optimization parameters.">parameter_tolerance_</a>);
<a name="l00303"></a>00303     <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a0236ea3add112a572ed2326a12306ded" title="NLopt optimization solver.">opt_</a>.set_ftol_rel(<a class="code" href="class_seldon_1_1_n_lopt_solver.php#afd6a5c3ebbb73a71fbd412a641eca860" title="Relative tolerance on the cost function.">cost_function_tolerance_</a>);
<a name="l00304"></a>00304     <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a0236ea3add112a572ed2326a12306ded" title="NLopt optimization solver.">opt_</a>.set_maxeval(<a class="code" href="class_seldon_1_1_n_lopt_solver.php#ac3340df79837a4cdde5560939ed01989" title="Maximum number of function evaluations. It is ignored if it is non-positive.">Niteration_max_</a>);
<a name="l00305"></a>00305     nlopt::result result = <a class="code" href="class_seldon_1_1_n_lopt_solver.php#a0236ea3add112a572ed2326a12306ded" title="NLopt optimization solver.">opt_</a>.optimize(<a class="code" href="class_seldon_1_1_n_lopt_solver.php#aade135d662e35906b7cae2ecbd1d789c" title="The vector that stores parameters values. Before optimization, stores the initial...">parameter_</a>, <a class="code" href="class_seldon_1_1_n_lopt_solver.php#afb1ea25dcf6a9da4b54f97d5ccf95cc8" title="The value of cost function for given parameter values.">cost_</a>);
<a name="l00306"></a>00306 
<a name="l00307"></a>00307     <span class="keywordflow">if</span> (result &lt; 0)
<a name="l00308"></a>00308       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_error.php">Error</a>(<span class="stringliteral">&quot;NLoptSolver::Optimize()&quot;</span>, <span class="stringliteral">&quot;Nlopt failed.&quot;</span>);
<a name="l00309"></a>00309   }
<a name="l00310"></a>00310 
<a name="l00311"></a>00311 
<a name="l00313"></a>00313 
<a name="l00317"></a>00317   <span class="keywordtype">double</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#aaf4264800dbfb5f24af404ac6019e0b0" title="Returns the value of the cost function.">NLoptSolver::GetCost</a>()<span class="keyword"> const</span>
<a name="l00318"></a>00318 <span class="keyword">  </span>{
<a name="l00319"></a>00319     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_n_lopt_solver.php#afb1ea25dcf6a9da4b54f97d5ccf95cc8" title="The value of cost function for given parameter values.">cost_</a>;
<a name="l00320"></a>00320   }
<a name="l00321"></a>00321 
<a name="l00322"></a>00322 
<a name="l00323"></a>00323 } <span class="comment">// namespace Seldon.</span>
<a name="l00324"></a>00324 
<a name="l00325"></a>00325 
<a name="l00326"></a>00326 <span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
