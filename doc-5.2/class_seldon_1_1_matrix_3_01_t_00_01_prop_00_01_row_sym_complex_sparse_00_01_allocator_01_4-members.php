<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>, including all inherited members.<table>
  <tr bgcolor="#f0f0f0"><td><b>access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#aaf8bdd8dd7d3f089b38a6db2c18c4154">AddInteraction</a>(int i, int j, const complex&lt; T &gt; &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>allocator</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>allocator_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a77439fdc40537903681e7cd912c5f732">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a5e1062c93e237401d6bf6b1797861c62">Copy</a>(const Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>data_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>entry_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ab53e3d0367145d47e83de9480ac29f54">Fill</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a7a7182bc0ef4972f4e124e37836bd06c">Fill</a>(const complex&lt; T &gt; &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#afda343754417cd33781e8bd3f69f07d4">FillRand</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#afda1dbd968e2e4b3047c014ab5931846">GetDataSize</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a2be1493a466aee2b75403551ddf42539">GetImag</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a14c8495abb3d6ef2841ceeb76a5f1149">GetImag</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a411656148afd39425bd6df81a013d180">GetImagData</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#acd8debd721f3560c317babe090aa09e6">GetImagDataSize</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a2bb61950179c75be2150cfae734bb05b">GetImagInd</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af1c1a606649e03827a3710751238a5bc">GetImagIndSize</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad1bb43e80676e9ee420ea692ba06d0ac">GetImagPtr</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#acde6ac68023f92f32eceb4f0a422580f">GetImagPtrSize</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">GetM</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">GetN</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a1f8cbc14fed12f61ebc5a2bc7085c619">GetReal</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a82d4964b6d1049d2a3aa8e861fe7c9c3">GetReal</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a1a98f711de4b7daf42ea42c8493ca08d">GetRealData</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad1304eb0b4e7197e603e95b119f24796">GetRealDataSize</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a72f50ab4230ffe4af23c8bbf2203ad0b">GetRealInd</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#acdb80353a65a45dfc11b886e7c75a807">GetRealIndSize</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a005a3b7389aecf38482cc16e4984787c">GetRealPtr</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a913aadd75d1ed2e076ea4b014e5fb32d">GetRealPtrSize</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">GetSize</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>imag_data_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>imag_ind_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>imag_nz_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>imag_ptr_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>m_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php#aac12efd56ba2dcb96cbb2ed94cbf440c">Matrix</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php#a47f8fde52ccac1a7747c5b66c4c0b46e">Matrix</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php#a10616155b7471e78fdf59f4f2a760304">Matrix</a>(int i, int j, int real_nz, int imag_nz)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php#a0df1be9842a16925408e0ed5761218f3">Matrix</a>(int i, int j, Vector&lt; T, Storage0, Allocator0 &gt; &amp;values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;ind, Vector&lt; T, Storage0, Allocator0 &gt; &amp;imag_values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;imag_ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;imag_ind)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a47988d7972247286332e853c13bbc00b">Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ad2ea11b0880dc32ba9472da9310c332b">Matrix_Base</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline, explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a127e0229931486cea3e6007988b446a0">Matrix_Base</a>(const Matrix_Base&lt; T, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af4fd673569b7398d84f2d4f277de50a8">Matrix_SymComplexSparse</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ab055efcee796094368d74c476dd28221">Matrix_SymComplexSparse</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a102b1c069e24ea2b907cefc593243ac4">Matrix_SymComplexSparse</a>(int i, int j, int real_nz, int imag_nz)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a063b7c64394f13e35d04d87310cf552e">Matrix_SymComplexSparse</a>(int i, int j, Vector&lt; T, Storage0, Allocator0 &gt; &amp;real_values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;real_ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;real_ind, Vector&lt; T, Storage0, Allocator0 &gt; &amp;imag_values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;imag_ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;imag_ind)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad373107c8496819b617d13f8d8336c73">Matrix_SymComplexSparse</a>(const Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>n_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af9adb37bee7561a855fef31642b85bfd">Nullify</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad441174c77a9c101560e77794aae6369">operator()</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#aba07f35ba600bf49197e1b1802f95c67">operator=</a>(const Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#abe86d6545d4aeca07257de2d8e7a85f2">Print</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>property</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#aa70fdb81ecca409e7410b7bd91c71bc8">Read</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a58a6efaa3b49592962e9f3d50963ba62">Read</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ae5f0883621073e283ebd9ad047e81174">ReadText</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a2be8c4d3b6d3f794ee5c6b7cba504571">ReadText</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>real_data_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>real_ind_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>real_nz_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>real_ptr_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af6ee0fe9e269657f40a8dccbe5e30b73">Reallocate</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ae41ce69193bc5f40015b6bc0220db998">Reallocate</a>(int i, int j, int real_nz, int imag_nz)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a6537ad73d443b7b1e7008f626593811e">Resize</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad2ef28f874e37f257af097c6ec306dfd">Resize</a>(int i, int j, int real_nz, int imag_nz)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ab4c3a8968e2c79248fc7d1add27f3433">Set</a>(int i, int j, const complex&lt; T &gt; &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a3f202fe96b9289912e1cb6acfe24f18b">SetData</a>(int i, int j, Vector&lt; T, Storage0, Allocator0 &gt; &amp;real_values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;real_ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;real_ind, Vector&lt; T, Storage0, Allocator0 &gt; &amp;imag_values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;imag_ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;imag_ind)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SetData</b>(int i, int j, int real_nz, pointer real_values, int *real_ptr, int *real_ind, int imag_nz, pointer imag_values, int *imag_ptr, int *imag_ind) (defined in <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a2116e2afc727a7c2ecf920fe41811c97">SetIdentity</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>storage</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a4cf220882aeebe97ab0cd8afb1eb52c7">ValImag</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a6243d988622a10dec95e8733710ef179">ValImag</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a0e58d558d63fc51d62db121f9732cf1c">ValReal</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#abec4edb809f2be635bff90b508f9e9f6">ValReal</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>value_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a8c8179698d7eccc69911fcbb21262d9b">Write</a>(string FileName) const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a01e5bc999ba6caed5db79815ebfe549e">Write</a>(ostream &amp;FileStream) const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a1c9015515a2e218c875d739bd9f410e1">WriteText</a>(string FileName) const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a6dc1206f68ef04a7bb6ac40b2e9f7e59">WriteText</a>(ostream &amp;FileStream) const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a55f35dad8db11e522dde99f236472d9d">Zero</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af34a03c0cc56f757a83cd56f97c74ed8">~Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#afcf6a5d81a59ad003dfa7f905e0b5aaa">~Matrix_SymComplexSparse</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a></td><td></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
