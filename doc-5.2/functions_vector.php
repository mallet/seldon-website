<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Functions for Vectors </h1>  </div>
</div>
<div class="contents">
<p>In that page, we detail functions that are not related to <a href="functions_blas.php">Blas</a> </p>
<table  class="category-table">
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#quicksort">QuickSort </a> </td><td class="category-table-td">sorts a vector with quick sort algorithm  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#mergesort">MergeSort </a> </td><td class="category-table-td">sorts a vector with merge sort algorithm  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#sort">Sort </a> </td><td class="category-table-td">sorts a vector  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#assemble">Assemble </a> </td><td class="category-table-td">assembles a vector  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#removeduplicate">RemoveDuplicate </a> </td><td class="category-table-td">sorts and removes duplicate elements of a vector  </td></tr>
</table>
<div class="separator"><a class="anchor" id="quicksort"></a></div><h3>QuickSort</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void QuickSort(int, int, Vector&amp;);
  void QuickSort(int, int, Vector&amp;, Vector&amp;);
  void QuickSort(int, int, Vector&amp;, Vector&amp;, Vector&amp;);
</pre><p>This function sorts a vector with quick sort algorithm, and affects the permutation to other vectors.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;int&gt; X(4), Y(4), Z(4);
X(0) = 3;
X(1) = 0;
X(2) = 2;
X(3) = 1;
Y = X;
 
// if you want to sort all elements of X
// the start index is 0, the final index is X.GetM()-1 
QuickSort(0, Y.GetM()-1, Y);

// if you want to retrieve the permutation vector
// sorting operations will affect vector Y
Y.Fill();
QuickSort(0, X.GetM()-1, X, Y);

// you can ask that a third vector is affected like the second vector
QuickSort(0, X.GetM()-1, X, Y, Z);

</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___arrays_8cxx_source.php">Functions_Arrays.cxx</a></p>
<div class="separator"><a class="anchor" id="mergesort"></a></div><h3>MergeSort</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void MergeSort(int, int, Vector&amp;);
  void MergeSort(int, int, Vector&amp;, Vector&amp;);
  void MergeSort(int, int, Vector&amp;, Vector&amp;, Vector&amp;);
</pre><p>This function sorts a vector with merge sort algorithm, and affects the permutation to other vectors.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;int&gt; X(4), Y(4), Z(4);
X(0) = 3;
X(1) = 0;
X(2) = 2;
X(3) = 1;
Y = X;

// if you want to sort all elements of X
// the start index is 0, the final index is X.GetM()-1 
MergeSort(0, Y.GetM()-1, Y);

// if you want to retrieve the permutation vector
// sorting operations will affect vector Y
Y.Fill();
MergeSort(0, X.GetM()-1, X, Y);

// you can ask that a third vector is affected like the second vector
MergeSort(0, X.GetM()-1, X, Y, Z);

</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___arrays_8cxx_source.php">Functions_Arrays.cxx</a></p>
<div class="separator"><a class="anchor" id="sort"></a></div><h3>Sort</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void Sort(int, int, Vector&amp;);
  void Sort(int, int, Vector&amp;, Vector&amp;);
  void Sort(int, int, Vector&amp;, Vector&amp;, Vector&amp;);</pre><pre class="syntax-box">  void Sort(int, Vector&amp;);
  void Sort(int, Vector&amp;, Vector&amp;);
  void Sort(int, Vector&amp;, Vector&amp;, Vector&amp;);</pre><pre class="syntax-box">  void Sort(Vector&amp;);
  void Sort(Vector&amp;, Vector&amp;);
  void Sort(Vector&amp;, Vector&amp;, Vector&amp;);
</pre><p>This function sorts a vector and affects the permutation to other vectors.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;int&gt; X(4), Y(4), Z(4);
X(0) = 3;
X(1) = 0;
X(2) = 2;
X(3) = 1;
Y = X;

Sort(Y);

// if you want to retrieve the permutation vector
// sorting operations will affect vector Y
Y.Fill();
Sort(X, Y);
// you should get X = [0 1 2 3], Y = [1 3 2 0]

// you can ask that a third vector is affected like the second vector
Sort(X, Y, Z);

// you can ask to sort only the first n elements of vector
int n = 3;
Sort(n, X);

// or ask to sort the elements between two indices of the vector
int start_index = 1, end_index = 3;
Sort(start_index, end_index, X);

</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___arrays_8cxx_source.php">Functions_Arrays.cxx</a></p>
<div class="separator"><a class="anchor" id="assemble"></a></div><h3>Assemble</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void Assemble(int&amp;, Vector&amp;);
  void Assemble(int&amp;, Vector&amp;, Vector&amp;);
</pre><p>This function sorts the first vector and removes duplicate elements. If a second vector is present in the arguments, the elements of the second vector are added when there are duplicate elements in the first vector. The vectors are not resized, and the first argument is an integer that will contain the new number of elements.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;int&gt; num(4);
Vector&lt;double&gt; values(4);
// we initialize with duplicate numbers
num(0) = 2;
num(1) = 1;
num(2) = 3;
num(3) = 1;
values(0) = 0.2;
values(1) = 0.7;
values(2) = -1.2;
values(3) = 0.8;

// you provide the number of elements to assemble
int nb = num.GetM();
Assemble(nb, num, values);

// in this example, there is one duplicate element -&gt; 1
// as a result, after the call to Assemble, you should have
// nb = 3,  num = [1 2 3],  values = [1.5 0.2 -1.2]

</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___arrays_8cxx_source.php">Functions_Arrays.cxx</a></p>
<div class="separator"><a class="anchor" id="removeduplicate"></a></div><h3>RemoveDuplicate</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void RemoveDuplicate(int&amp;, Vector&amp;);
  void RemoveDuplicate(int&amp;, Vector&amp;, Vector&amp;);
  void RemoveDuplicate(Vector&amp;);
  void RemoveDuplicate(Vector&amp;, Vector&amp;);
</pre><p>This function sorts the first vector and removes duplicate elements. If an integer is given as first argument, the vectors are not resized, and the first argument will contain the new number of elements. The second vector is affected by the operations made on the first vector.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;int&gt; num(4), permutation(4);
// we initialize with duplicate numbers
num(0) = 2;
num(1) = 1;
num(2) = 3;
num(3) = 1;
permutation.Fill();

// you remove duplicate elements, and find the permutation vector
// vectors are resized
RemoveDuplicate(num, permutation);


// you can also provide the number of elements to treat
// and vectors will not be resized, but you get the new number of elements
int nb_elt = num.GetM();
RemoveDuplicate(nb_elt, num, permutation);

</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___arrays_8cxx_source.php">Functions_Arrays.cxx</a> </p>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
