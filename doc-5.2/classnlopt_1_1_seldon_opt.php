<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><b>nlopt</b>      </li>
      <li><a class="el" href="classnlopt_1_1_seldon_opt.php">SeldonOpt</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#nested-classes">Classes</a> &#124;
<a href="#pub-methods">Public Member Functions</a>  </div>
  <div class="headertitle">
<h1>nlopt::SeldonOpt Class Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="nlopt::SeldonOpt" -->
<p><a href="classnlopt_1_1_seldon_opt-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="nested-classes"></a>
Classes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">struct &nbsp;</td><td class="memItemRight" valign="bottom"><b>myfunc_data</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a671f8e06f25fa513da59ffd72e92ece1"></a><!-- doxytag: member="nlopt::SeldonOpt::SeldonOpt" ref="a671f8e06f25fa513da59ffd72e92ece1" args="(algorithm a, unsigned n)" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><b>SeldonOpt</b> (algorithm a, unsigned n)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7a5e8090155e0ae553aaf6a62837fc4d"></a><!-- doxytag: member="nlopt::SeldonOpt::SeldonOpt" ref="a7a5e8090155e0ae553aaf6a62837fc4d" args="(const SeldonOpt &amp;f)" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><b>SeldonOpt</b> (const <a class="el" href="classnlopt_1_1_seldon_opt.php">SeldonOpt</a> &amp;f)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab4a28932005ee9670a1a7c90303b76cd"></a><!-- doxytag: member="nlopt::SeldonOpt::operator=" ref="ab4a28932005ee9670a1a7c90303b76cd" args="(SeldonOpt const &amp;f)" -->
<a class="el" href="classnlopt_1_1_seldon_opt.php">SeldonOpt</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>operator=</b> (<a class="el" href="classnlopt_1_1_seldon_opt.php">SeldonOpt</a> const &amp;f)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a10114520eeb71b98960df122d14246c1"></a><!-- doxytag: member="nlopt::SeldonOpt::optimize" ref="a10114520eeb71b98960df122d14246c1" args="(Seldon::Vector&lt; double &gt; &amp;x, double &amp;opt_f)" -->
result&nbsp;</td><td class="memItemRight" valign="bottom"><b>optimize</b> (<a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; double &gt; &amp;x, double &amp;opt_f)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2cc85931b398858510651a0eea7ba951"></a><!-- doxytag: member="nlopt::SeldonOpt::optimize" ref="a2cc85931b398858510651a0eea7ba951" args="(const Seldon::Vector&lt; double &gt; &amp;x0)" -->
<a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; double &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>optimize</b> (const <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; double &gt; &amp;x0)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac888f710079fc2a8762f8acfc3cdf5b3"></a><!-- doxytag: member="nlopt::SeldonOpt::last_optimize_result" ref="ac888f710079fc2a8762f8acfc3cdf5b3" args="() const " -->
result&nbsp;</td><td class="memItemRight" valign="bottom"><b>last_optimize_result</b> () const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a482ade20923c428fea5746bf5326030d"></a><!-- doxytag: member="nlopt::SeldonOpt::last_optimum_value" ref="a482ade20923c428fea5746bf5326030d" args="() const " -->
double&nbsp;</td><td class="memItemRight" valign="bottom"><b>last_optimum_value</b> () const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2e4cf5b08c0806dec60fae1e32bca29b"></a><!-- doxytag: member="nlopt::SeldonOpt::get_algorithm" ref="a2e4cf5b08c0806dec60fae1e32bca29b" args="() const " -->
algorithm&nbsp;</td><td class="memItemRight" valign="bottom"><b>get_algorithm</b> () const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac745abf85c16c4477b0b762764e9679a"></a><!-- doxytag: member="nlopt::SeldonOpt::get_algorithm_name" ref="ac745abf85c16c4477b0b762764e9679a" args="() const " -->
const char *&nbsp;</td><td class="memItemRight" valign="bottom"><b>get_algorithm_name</b> () const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a582d9ac9420a968aa2b885aa34255091"></a><!-- doxytag: member="nlopt::SeldonOpt::get_dimension" ref="a582d9ac9420a968aa2b885aa34255091" args="() const " -->
unsigned&nbsp;</td><td class="memItemRight" valign="bottom"><b>get_dimension</b> () const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a33418cc2dab23d962076474f7d387ee9"></a><!-- doxytag: member="nlopt::SeldonOpt::set_min_objective" ref="a33418cc2dab23d962076474f7d387ee9" args="(func f, void *f_data)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>set_min_objective</b> (func f, void *f_data)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad6e9ee049ac285f05e9a6e72f436a544"></a><!-- doxytag: member="nlopt::SeldonOpt::set_min_objective" ref="ad6e9ee049ac285f05e9a6e72f436a544" args="(svfunc vf, void *f_data)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>set_min_objective</b> (svfunc vf, void *f_data)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9818bed4c9065c5d7b983eb4b5ffb0d8"></a><!-- doxytag: member="nlopt::SeldonOpt::set_max_objective" ref="a9818bed4c9065c5d7b983eb4b5ffb0d8" args="(func f, void *f_data)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>set_max_objective</b> (func f, void *f_data)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="abe628d828dd8fa1d3e61bb26974e49f6"></a><!-- doxytag: member="nlopt::SeldonOpt::set_max_objective" ref="abe628d828dd8fa1d3e61bb26974e49f6" args="(svfunc vf, void *f_data)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>set_max_objective</b> (svfunc vf, void *f_data)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8c28b8418513568c2f09632d1f098c53"></a><!-- doxytag: member="nlopt::SeldonOpt::set_min_objective" ref="a8c28b8418513568c2f09632d1f098c53" args="(func f, void *f_data, nlopt_munge md, nlopt_munge mc)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>set_min_objective</b> (func f, void *f_data, nlopt_munge md, nlopt_munge mc)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7097fd3a6cbec20cf3a8f80199e029e4"></a><!-- doxytag: member="nlopt::SeldonOpt::set_max_objective" ref="a7097fd3a6cbec20cf3a8f80199e029e4" args="(func f, void *f_data, nlopt_munge md, nlopt_munge mc)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>set_max_objective</b> (func f, void *f_data, nlopt_munge md, nlopt_munge mc)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2a6bd526e0fa1cfacb5b1dcf51659810"></a><!-- doxytag: member="nlopt::SeldonOpt::remove_inequality_constraints" ref="a2a6bd526e0fa1cfacb5b1dcf51659810" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>remove_inequality_constraints</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a177c3df2343c13b4fa37397aaec43fd5"></a><!-- doxytag: member="nlopt::SeldonOpt::add_inequality_constraint" ref="a177c3df2343c13b4fa37397aaec43fd5" args="(func f, void *f_data, double tol=0)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>add_inequality_constraint</b> (func f, void *f_data, double tol=0)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acdb4b79e9d2d549a2ffa9f40790cd977"></a><!-- doxytag: member="nlopt::SeldonOpt::add_inequality_constraint" ref="acdb4b79e9d2d549a2ffa9f40790cd977" args="(svfunc vf, void *f_data, double tol=0)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>add_inequality_constraint</b> (svfunc vf, void *f_data, double tol=0)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1094e1ba63f04585708810d4d620a556"></a><!-- doxytag: member="nlopt::SeldonOpt::add_inequality_mconstraint" ref="a1094e1ba63f04585708810d4d620a556" args="(mfunc mf, void *f_data, const Seldon::Vector&lt; double &gt; &amp;tol)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>add_inequality_mconstraint</b> (mfunc mf, void *f_data, const <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; double &gt; &amp;tol)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab105dd41399469db54fdaf7a76bb3111"></a><!-- doxytag: member="nlopt::SeldonOpt::remove_equality_constraints" ref="ab105dd41399469db54fdaf7a76bb3111" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>remove_equality_constraints</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8277d65b72394d2d03f077ade465e551"></a><!-- doxytag: member="nlopt::SeldonOpt::add_equality_constraint" ref="a8277d65b72394d2d03f077ade465e551" args="(func f, void *f_data, double tol=0)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>add_equality_constraint</b> (func f, void *f_data, double tol=0)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad378f24435c4a1cfc13b1da0542c8838"></a><!-- doxytag: member="nlopt::SeldonOpt::add_equality_constraint" ref="ad378f24435c4a1cfc13b1da0542c8838" args="(svfunc vf, void *f_data, double tol=0)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>add_equality_constraint</b> (svfunc vf, void *f_data, double tol=0)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9710a324d60b9cfec33ec41cb46564a2"></a><!-- doxytag: member="nlopt::SeldonOpt::add_equality_mconstraint" ref="a9710a324d60b9cfec33ec41cb46564a2" args="(mfunc mf, void *f_data, const Seldon::Vector&lt; double &gt; &amp;tol)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>add_equality_mconstraint</b> (mfunc mf, void *f_data, const <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; double &gt; &amp;tol)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1976c48f8d28d9777208cd9f572dfc50"></a><!-- doxytag: member="nlopt::SeldonOpt::add_inequality_constraint" ref="a1976c48f8d28d9777208cd9f572dfc50" args="(func f, void *f_data, nlopt_munge md, nlopt_munge mc, double tol=0)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>add_inequality_constraint</b> (func f, void *f_data, nlopt_munge md, nlopt_munge mc, double tol=0)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0f3482c24a6684cd8be79ce189034bb1"></a><!-- doxytag: member="nlopt::SeldonOpt::add_equality_constraint" ref="a0f3482c24a6684cd8be79ce189034bb1" args="(func f, void *f_data, nlopt_munge md, nlopt_munge mc, double tol=0)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>add_equality_constraint</b> (func f, void *f_data, nlopt_munge md, nlopt_munge mc, double tol=0)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac72f5ea08e1a6c02c73c9fd8d91c0623"></a><!-- doxytag: member="nlopt::SeldonOpt::add_inequality_mconstraint" ref="ac72f5ea08e1a6c02c73c9fd8d91c0623" args="(mfunc mf, void *f_data, nlopt_munge md, nlopt_munge mc, const Seldon::Vector&lt; double &gt; &amp;tol)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>add_inequality_mconstraint</b> (mfunc mf, void *f_data, nlopt_munge md, nlopt_munge mc, const <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; double &gt; &amp;tol)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a938fb082a9c678f88ec59a4cc0609ff5"></a><!-- doxytag: member="nlopt::SeldonOpt::add_equality_mconstraint" ref="a938fb082a9c678f88ec59a4cc0609ff5" args="(mfunc mf, void *f_data, nlopt_munge md, nlopt_munge mc, const Seldon::Vector&lt; double &gt; &amp;tol)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>add_equality_mconstraint</b> (mfunc mf, void *f_data, nlopt_munge md, nlopt_munge mc, const <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; double &gt; &amp;tol)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afa12c466882072bdd2ad46f10173c08f"></a><!-- doxytag: member="nlopt::SeldonOpt::force_stop" ref="afa12c466882072bdd2ad46f10173c08f" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>force_stop</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae5f2177d70bb3ee94f41b10e5adb15ba"></a><!-- doxytag: member="nlopt::SeldonOpt::set_local_optimizer" ref="ae5f2177d70bb3ee94f41b10e5adb15ba" args="(const SeldonOpt &amp;lo)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>set_local_optimizer</b> (const <a class="el" href="classnlopt_1_1_seldon_opt.php">SeldonOpt</a> &amp;lo)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5e7714a042be1270612921f2b904a8f5"></a><!-- doxytag: member="nlopt::SeldonOpt::set_default_initial_step" ref="a5e7714a042be1270612921f2b904a8f5" args="(const Seldon::Vector&lt; double &gt; &amp;x)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>set_default_initial_step</b> (const <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; double &gt; &amp;x)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afcdb2e7d4fbdc3e6286e7c3702c0a87c"></a><!-- doxytag: member="nlopt::SeldonOpt::get_initial_step" ref="afcdb2e7d4fbdc3e6286e7c3702c0a87c" args="(const Seldon::Vector&lt; double &gt; &amp;x, Seldon::Vector&lt; double &gt; &amp;dx) const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>get_initial_step</b> (const <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; double &gt; &amp;x, <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; double &gt; &amp;dx) const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aca2801ca33d80aed54e50ca9e8206331"></a><!-- doxytag: member="nlopt::SeldonOpt::get_initial_step_" ref="aca2801ca33d80aed54e50ca9e8206331" args="(const Seldon::Vector&lt; double &gt; &amp;x) const " -->
<a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; double &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>get_initial_step_</b> (const <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; double &gt; &amp;x) const </td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>

<p>Definition at line <a class="el" href="_n_lopt_8hxx_source.php#l00037">37</a> of file <a class="el" href="_n_lopt_8hxx_source.php">NLopt.hxx</a>.</p>
<hr/>The documentation for this class was generated from the following file:<ul>
<li>computation/optimization/<a class="el" href="_n_lopt_8hxx_source.php">NLopt.hxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
