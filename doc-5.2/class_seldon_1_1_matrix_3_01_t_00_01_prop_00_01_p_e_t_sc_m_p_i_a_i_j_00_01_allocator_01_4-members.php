<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a>, including all inherited members.<table>
  <tr bgcolor="#f0f0f0"><td><b>access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>allocator_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a5344decb2cb632f0db5b2bb1204afcbe">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#ab542e6e5403f75d41b6d24d42400e16a">Copy</a>(const Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#a6c592a94514945ad3c1ea77dc2ef6f76">Copy</a>(const Matrix&lt; T0, General, RowSparse, Allocator0 &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a4b04405490e02061ddb68a4e33037106">PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;::Copy</a>(const Mat &amp;A)</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>data_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>entry_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a3a3b27f67d957f4a7cce1708f8e1c8b8">Fill</a>()</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a4b35bb1419e705d671180eb2afa31e02">Fill</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a7a6c8b901b09261c340d88500d7066e0">FillRand</a>()</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a2f608b718871d65dc96806c4f5867a60">Flush</a>() const</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a290f3593cb516534a86c0b9f5c1646c8">GetCommunicator</a>() const</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#ae4910201f37353501e68c3d9fa531e7a">GetLocalM</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#a695d0eadf5e3bec6a94a0155d3627fbc">GetLocalN</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">GetM</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">GetN</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a0fa3793829d97bf29211e30ab72384bf">GetPetscMatrix</a>()</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a23c632e3a5c2e5710ac4429ca2fdc726">GetPetscMatrix</a>() const</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#afb8978d1bd4774d18b4815ad454b62e9">GetProcessorRowRange</a>(int &amp;i, int &amp;j) const</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">GetSize</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>m_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#ae1fb6818eb7e3f325d6f6f2d915217af">Matrix</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#a49b4738a0e09268a1c5e59e6a89dcdcf">Matrix</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#aeee56404fba8515411ed5d3146d5cb1b">Matrix</a>(Mat &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#a6f0c7eea96be5966068e649ee5f7e278">Matrix</a>(const Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a47988d7972247286332e853c13bbc00b">Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ad2ea11b0880dc32ba9472da9310c332b">Matrix_Base</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline, explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a127e0229931486cea3e6007988b446a0">Matrix_Base</a>(const Matrix_Base&lt; T, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a71d0b850860f9c228b6efc8ea83a2898">mpi_communicator_</a></td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>n_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#aec50a5001f0de562e2e20b1c9e166e29">Nullify</a>()</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#aac091fbb0b9613110e38a4f94da20ba3">operator()</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#a2c38b335b8c01ae59fe91fd1f73800a0">operator()</a>(int i, int j) const </td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#a58810530bac6b6b662fdb4b9509ab431">operator=</a>(const Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#ae4f22eb628e0e160dcf3789673748861">operator[]</a>(int i)</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a5ade50e9ecdaaa8854d22f16b800a265">operator[]</a>(int i) const</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a472c2776d1ddbe98b586a6260ea9dcfc">petsc_matrix_</a></td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a06e38862e1dd29739d952dc62ee40c9d">petsc_matrix_deallocated_</a></td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#aff9053f274de65aa8e0f69af8bd5491c">PetscMatrix</a>()</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a8d5aa5c5c25da20dbf2e5726cde2bc39">PetscMatrix</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#ae13a2135e02d5b65204eb0911534e1a7">PetscMatrix</a>(Mat &amp;A)</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#ae9b1d53be394fd8ba4a8e58f9b3b0957">Print</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a4b5bb6ff820bb4b06605fc367ee9b12c">PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;::Print</a>(int a, int b, int m, int n) const</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#adb57f345a20c0f85f5bb20b8341c49cb">PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;::Print</a>(int l) const</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a2b5cb5cb3784d8754b5a2f3b73d256aa">Read</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a2c6835f0aaac0d2a61e834f32e5bc046">Read</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a72c784f22fcba80ba6538f19bf29a9c1">ReadText</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#ad06f7c6ed48a9b52800a67b77d4d2fb4">ReadText</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php#abdf3b65f08de384fd0527e89a5478998">Reallocate</a>(int i, int j, int local_i=PETSC_DECIDE, int local_j=PETSC_DECIDE)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a0cfea8ec472b6f1b44fb04eae709f8f8">Resize</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>Set</b>(int, int, T) (defined in <a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#ad3cc1f14b9ba6ac1b459ce448d6adfd9">SetBuffer</a>(int, int, T, InsertMode)</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#ab0fa102ad451a796f30de474ee7c036e">SetCommunicator</a>(MPI_Comm mpi_communicator)</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#af9571d45be248575d07b866c490c1ba2">SetData</a>(int i, int j, pointer data)</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a177616dc93ea00fc5a570cbc1cedf421">SetIdentity</a>()</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a0353d3a236f3f6f3a688b5e422a5e568">Val</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#ad780213ae6138a9a9532c02806985625">Val</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>value_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_p_e_t_sc_m_p_i_a_i_j_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, PETScMPIAIJ, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a87f057e43012523ccd9f2087f688aa11">Write</a>(string FileName, bool with_size) const</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a33e2b2d752293700486568ef8257fac5">Write</a>(ostream &amp;FileStream, bool with_size) const</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a88ebd49e399e37027904bce13e7f63c4">WriteText</a>(string FileName) const</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#ab4b190fca03d36f2b5a3de31680be4c0">WriteText</a>(ostream &amp;FileStream) const</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a452a9f334888ccca27951ce53e4b2cf9">Zero</a>()</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af34a03c0cc56f757a83cd56f97c74ed8">~Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_petsc_matrix.php#a791babbea4d834e8559e316e7605dbbc">~PetscMatrix</a>()</td><td><a class="el" href="class_seldon_1_1_petsc_matrix.php">Seldon::PetscMatrix&lt; T, Prop, RowMajor, Allocator &gt;</a></td><td></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
