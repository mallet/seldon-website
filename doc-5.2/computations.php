<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Computations </h1>  </div>
</div>
<div class="contents">
<p>Seldon is fully interfaced with <b>Blas</b> (level 1, 2 and 3), except for Blas functions involving banded matrices (since this format is not available for the moment) and rank operations. If Blas is not available to the user, a few alternative functions (same functions written in C++) may be used.</p>
<h2>Blas</h2>
<p>The interface is implemented in the files <code>Seldon-[version]/computation/interfaces/Blas_*</code>) if you have a doubt about the syntax. In the following list, C++ functions are detailed, and the equivalent BLAS subroutines mentionned in bold. Duplicate names are avoided, e.g. only <b> daxpy </b> is mentionned even if other subroutines <b>saxpy</b>, <b>caxpy</b> and <b> zaxpy </b> are interfaced as well.  </p>
<ul>
<li>
<p class="startli"><code>GenRot(a, b, c, s)</code> constructs givens plane rotation (<b>drotg</b>) </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>ApplyRot(X, Y, c, s)</code> applies givens plane rotation (<b>drot</b>)</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>GenModifRot(a, b, c, s, param)</code> constructs modified givens plane rotation (<b>drotmg</b>) </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>ApplyModifRot(X, Y, param)</code> applies modified givens plane rotation (<b>drotm</b>) </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>Swap(X, Y)</code> exchanges two vectors (<b>dswap</b>).</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>Copy(X, Y)</code> copies vector X into vector Y (<b>dcopy</b>).</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>Mlt(alpha, X)</code> multiplies vector X by alpha (<b>dscal</b>).</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>Add(alpha, X, Y)</code> adds vector X to Y : alpha.X + Y -&gt; Y (<b>daxpy</b>).</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>DotProd(X, Y)</code> returns scalar product between two vectors (<b>ddot, zdotu</b>).</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>DotProdConj(X, Y)</code> returns scalar product between two vectors, first vector being conjugated (<b>zdotc</b>).</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>Norm1(X)</code> computes 1-norm of vector X (<b>dasum</b>).</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>Norm2(X)</code> computes 2-norm of vector X (<b>dnrm2</b>).</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>GetMaxAbsIndex(X)</code> returns index where maximum of X is reached (<b>idamax</b>).</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>Mlt(A, X)</code> computes product of A by X for triangular matrices : A*X -&gt; X (<b>dtrmv</b>, <b>dtpmv</b>).</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code> MltAdd(alpha, A, X, beta, Y) </code> computes matrix-vector product alpha*A*X + beta*Y -&gt; Y. This function is overloaded for all types of matrices (<b>dgemv</b>, <b>zhemv</b>, <b>zhpmv</b>, <b>dsymv</b>, <b>dspmv</b>). </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code> Rank1Update(alpha, X, Y, M) </code> Performs rank 1 operation : alpha*X.Y' + M -&gt; M (<b>dger</b>, <b>zgeru</b>) </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code> Rank1Update(alpha, X, conj, Y, M) </code> Performs rank 1 operation : alpha*X.Y' + M -&gt; M by taking the conjugate of Y (<b>zgerc</b>) </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code> Rank1Update(alpha, X, M) </code> Performs rank 1 operation : alpha*X.X' + M -&gt; M for symmetric/hermitian matrices (<b>dspr</b>, <b> zhpr</b>) </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code> Rank1Update(alpha, X, M) </code> Performs rank 2 operation : alpha*X.Y' + Y.X' + M -&gt; M for symmetric/hermitian matrices (<b>dspr2</b>, <b>zhpr2</b>) </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code> Solve(A, X) </code> Solves triangular linear system A -&gt; X (<b>dtrsv</b>, <b>dtpsv</b>) </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code> MltAdd(alpha, A, B, beta, C) </code> computes matrix-matrix product alpha*A*B + beta*C -&gt; C. This function is overloaded for all types of matrices (<b>dgemm</b>, <b>dsymm</b>, <b> zhemm</b>). </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code> Mlt(alpha, A, B) </code> computes matrix-matrix product alpha*A*B -&gt; B with A triangular matrix (<b>dtrmm</b>, <b>dsymm</b>, <b> zhemm</b>). </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code> Solve(alpha, A, B) </code> Solves triangular linear system alpha*A -&gt; B with B general matrix (<b>dtrsm</b>, <b> dtpsv</b>, <b> ztpsv </b>) </p>
<p class="endli"></p>
</li>
</ul>
<p>Remember that, before the inclusion of Seldon (<code>include "Seldon.hxx"</code>), you must define <code>SELDON_WITH_BLAS</code>. For backward compatibility, <code>SELDON_WITH_CBLAS</code> is also accepted, but not recommended. Below is an example: </p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">

#define SELDON_WITH_BLAS
#include "Seldon.hxx"

using namespace Seldon;

// complex vectors
int n = 10;
ZVect X(n), Y(n); 
X. Fill(); Y.Fill();

// scalar product X'.Y (conjugate of X)
complex&lt;double&gt; alpha = DotProdConj(X, Y);
// norm of the vector
double norm_vec = Norm2(X);
// alpha*X + Y -&gt; Y
Add(alpha, X, Y);

// matrices must have the good dimension before multiplication
int n = 4, k = 5;
Matrix&lt;double&gt; A(m,n), B(n,k), C(m,k);
MltAdd(1.0, A, B, 2.0, C);

// you can multiply with transpose
A.Reallocate(n,m); A.Fill();
MltAdd(1.0, SeldonTrans, A, SeldonNoTrans, B, 2.0, C);

 </pre></div>  </pre><p>A comprehensive test of Blas interface is done in file <code>test/program/blas_test.cpp</code>.</p>
<h2>C++ functions</h2>
<p>C++ functions (that do not call Blas) are available in <code>Seldon-[version]/computation/basic_functions/*</code>. The syntax is the same as for the functions in the interface to Blas. The functions <code>Add</code>, <code> DotProd </code>, <code> DotProdConj </code>, <code> Mlt </code> and <code> MltAdd </code> are written in C++ for any type of matrix and vector, and those functions can be used without using the Blas interface.</p>
<h2>Lapack</h2>
<p>The interface is implemented in the files <code>Seldon-[version]/computation/interfaces/Lapack_*</code>) if you have a doubt about the syntax. The following C++ names have been chosen (in bold, name of blas subroutines)  </p>
<ul>
<li>
<p class="startli"><code>GetLU(A, pivot)</code> factorization of matrix A with pivot. (<b>dgetrf</b>, <b> dsytrf</b>, <b> dsptrf</b>, <b>zhetrf</b>, <b> zhptrf</b>) </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>SolveLU(A, pivot, X)</code> solves linear system, assuming that GetLU has been called before. (<b> dgetrs </b>, <b>dsytrs</b>, <b>dsptrs</b>, <b>zhetrs</b>, <b>zhptrs</b>, <b>dtrtrs</b>, <b> dtptrs </b>) </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code> ReciprocalConditionNumber(A, pivot, norm, anorm) </code> returns the inverse of condition number of matrix A, assuming that GetLU has been called before (<b>dgecon</b>, <b>dsycon</b>, <b>dspcon</b>, <b> zhecon </b>, <b> zhpcon </b>, <b> dtrcon </b>, <b> dtpcon </b>) </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>RefineSolutionLU(A, Alu, pivot, x, b, ferr, berr)</code> improves the solution computed by SolveLU and provides error bounds and backward error estimates. (<b>dgerfs</b>, <b> dsyrfs</b>, <b> dsprfs</b>, <b>zherfs</b>, <b> zhprfs</b>) </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>GetInverse(A)</code> computes the inverse of A. This method first calls GetLU in order to factorize the matrix before forming the inverse. (<b>dgetri</b>, <b> dsytri</b>, <b> dsptri</b>, <b>zhetri</b>, <b> zhptri</b>, <b> dtrtri </b>, <b> dtptri </b>) </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>GetScalingFactors(A, row_scale, col_scale, row_cond_number, col_cond_number, amax)</code> computes row and column scalings intended to equilibrate a matrix and reduce its condition number. (<b>dgeequ</b>) </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>GetAndSolveLU(A, pivot, X)</code> factorization of matrix A, and resolution of linear system A X = b. </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>GetQR(A, tau)</code> QR factorization of matrix A. (<b>dgeqrf</b>) </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>GetLQ(A, tau)</code> LQ factorization of matrix A. (<b>dgelqf</b>) </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>GetQ_FromQR(A, tau)</code> explicits matrix Q issued from QR factorization of matrix A. It is assumed that GetQR has been called before. (<b>dorgqr</b>) </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>MltQ_FromQR(A, tau, x)</code> multiplies by matrix Q issued from QR factorization of matrix A. It is assumed that GetQR has been called before. (<b>dormqr</b>) </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>SolveQR(A, tau, x)</code> solves least-squares problem by using QR factorization of matrix A. It is assumed that GetQR has been called before. </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>SolveLQ(A, tau, x)</code> solves least-squares problem by using LQ factorization of matrix A. It is assumed that GetLQ has been called before. </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>GetEigenvalues(A, lambda_real, lambda_imag)</code> computes eigenvalues (real part and imaginary part) of unsymmetric real matrix. (<b>dgeev</b> ) </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>GetEigenvalues(A, lambda)</code> computes eigenvalues of matrix. (<b>zgeev</b>, <b>dsyev</b>, <b>zheev</b>, <b>dspev</b>, <b>zhpev</b>) </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>GetEigenvaluesEigenvectors(A, lambda_real, lambda_imag, W)</code> computes eigenvalues (real part and imaginary part) and eigenvectors of unsymmetric real matrix. </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>GetEigenvaluesEigenvectors(A, lambda, W)</code> computes eigenvalues and eigenvectors of matrix. </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>GetEigenvalues(A, B, lambda)</code> solves generalized eigenvalue problem. (<b>zggev</b>, <b>dsygv</b>, <b>zhegv</b>, <b>dspgv</b>, <b>zhpgv</b>) </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>GetEigenvaluesEigenvectors(A, B, lambda, W)</code> solves generalized eigenvalue problem. </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>GetSVD(A, lambda, U, V)</code> computes singular value decomposition of matrix A. (<b>dgesvd</b>) </p>
<p class="endli"></p>
</li>
</ul>
<p>A comprehensive test of Lapack interface is done in file <code>test/program/lapack_test.cpp</code>. </p>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
