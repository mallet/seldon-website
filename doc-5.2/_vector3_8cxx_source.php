<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>vector/Vector3.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2010-2012, INRIA</span>
<a name="l00002"></a>00002 <span class="comment">// Author(s): Marc Fragu, Vivien Mallet</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_VECTOR_VECTOR_3_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 
<a name="l00024"></a>00024 <span class="preprocessor">#include &quot;Vector3.hxx&quot;</span>
<a name="l00025"></a>00025 
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 <span class="keyword">namespace </span>Seldon
<a name="l00028"></a>00028 {
<a name="l00029"></a>00029 
<a name="l00030"></a>00030 
<a name="l00032"></a>00032   <span class="comment">// VECTOR3 //</span>
<a name="l00034"></a>00034 <span class="comment"></span>
<a name="l00035"></a>00035 
<a name="l00036"></a>00036   <span class="comment">/***************</span>
<a name="l00037"></a>00037 <span class="comment">   * CONSTRUCTOR *</span>
<a name="l00038"></a>00038 <span class="comment">   ***************/</span>
<a name="l00039"></a>00039 
<a name="l00040"></a>00040 
<a name="l00042"></a>00042 
<a name="l00045"></a>00045   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00046"></a>00046   <a class="code" href="class_seldon_1_1_vector3.php#a3425eec2b0400057adeda33a005a188d" title="Default constructor.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::Vector3</a>()
<a name="l00047"></a>00047   {
<a name="l00048"></a>00048   }
<a name="l00049"></a>00049 
<a name="l00050"></a>00050 
<a name="l00052"></a>00052 
<a name="l00056"></a>00056   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00057"></a>00057   <a class="code" href="class_seldon_1_1_vector3.php#a3425eec2b0400057adeda33a005a188d" title="Default constructor.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::Vector3</a>(<span class="keywordtype">int</span> length)
<a name="l00058"></a>00058   {
<a name="l00059"></a>00059     data_.Reallocate(length);
<a name="l00060"></a>00060   }
<a name="l00061"></a>00061 
<a name="l00062"></a>00062 
<a name="l00064"></a>00064 
<a name="l00070"></a>00070   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00071"></a>00071   <a class="code" href="class_seldon_1_1_vector3.php#a3425eec2b0400057adeda33a005a188d" title="Default constructor.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00072"></a>00072 <a class="code" href="class_seldon_1_1_vector3.php#a3425eec2b0400057adeda33a005a188d" title="Default constructor.">  ::Vector3</a>(Vector&lt;int&gt;&amp; length)
<a name="l00073"></a>00073   {
<a name="l00074"></a>00074     data_.Clear();
<a name="l00075"></a>00075     <span class="keywordtype">int</span> n, m = length.GetSize();
<a name="l00076"></a>00076     data_.Reallocate(m);
<a name="l00077"></a>00077     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l00078"></a>00078       {
<a name="l00079"></a>00079         n = length(i);
<a name="l00080"></a>00080         data_(i).Reallocate(n);
<a name="l00081"></a>00081       }
<a name="l00082"></a>00082   }
<a name="l00083"></a>00083 
<a name="l00084"></a>00084 
<a name="l00086"></a>00086 
<a name="l00092"></a><a class="code" href="class_seldon_1_1_vector3.php#a9fff1c8ce72eb5dc5c002b6f1c0e7b86">00092</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00093"></a>00093   <span class="keyword">template</span> &lt;<span class="keyword">class</span> Allocator&gt;
<a name="l00094"></a>00094   <a class="code" href="class_seldon_1_1_vector3.php#a3425eec2b0400057adeda33a005a188d" title="Default constructor.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00095"></a>00095 <a class="code" href="class_seldon_1_1_vector3.php#a3425eec2b0400057adeda33a005a188d" title="Default constructor.">  ::Vector3</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a>, Vect_Full, Allocator&gt;&amp; length)
<a name="l00096"></a>00096   {
<a name="l00097"></a>00097     data_.Clear();
<a name="l00098"></a>00098     <span class="keywordtype">int</span> n, m = length.GetSize();
<a name="l00099"></a>00099     data_.Reallocate(m);
<a name="l00100"></a>00100     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l00101"></a>00101       {
<a name="l00102"></a>00102         n = length(i).GetSize();
<a name="l00103"></a>00103         data_(i).Reallocate(n);
<a name="l00104"></a>00104         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l00105"></a>00105           data_(i)(j).Reallocate(length(i)(j));
<a name="l00106"></a>00106       }
<a name="l00107"></a>00107   }
<a name="l00108"></a>00108 
<a name="l00109"></a>00109 
<a name="l00110"></a>00110   <span class="comment">/**************</span>
<a name="l00111"></a>00111 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00112"></a>00112 <span class="comment">   **************/</span>
<a name="l00113"></a>00113 
<a name="l00114"></a>00114 
<a name="l00116"></a>00116 
<a name="l00119"></a>00119   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00120"></a>00120   <a class="code" href="class_seldon_1_1_vector3.php#ae85c3b686a4100a5c05dc7d82a4b6b80" title="Destructor.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::~Vector3</a>()
<a name="l00121"></a>00121   {
<a name="l00122"></a>00122   }
<a name="l00123"></a>00123 
<a name="l00124"></a>00124 
<a name="l00125"></a>00125   <span class="comment">/**********************</span>
<a name="l00126"></a>00126 <span class="comment">   * VECTORS MANAGEMENT *</span>
<a name="l00127"></a>00127 <span class="comment">   **********************/</span>
<a name="l00128"></a>00128 
<a name="l00129"></a>00129 
<a name="l00131"></a>00131 
<a name="l00134"></a>00134   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00135"></a>00135   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a4a694febe8dba0a1203fd628cd1e3167" title="Returns size along dimension 1.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetLength</a>()<span class="keyword"> const</span>
<a name="l00136"></a>00136 <span class="keyword">  </span>{
<a name="l00137"></a>00137     <span class="keywordflow">return</span> data_.GetLength();
<a name="l00138"></a>00138   }
<a name="l00139"></a>00139 
<a name="l00140"></a>00140 
<a name="l00142"></a>00142 
<a name="l00145"></a>00145   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00146"></a>00146   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a66946f1764cc14abec8a51d4e310f6b5" title="Returns size along dimension 1.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetSize</a>()<span class="keyword"> const</span>
<a name="l00147"></a>00147 <span class="keyword">  </span>{
<a name="l00148"></a>00148     <span class="keywordflow">return</span> data_.GetSize();
<a name="l00149"></a>00149   }
<a name="l00150"></a>00150 
<a name="l00151"></a>00151 
<a name="l00153"></a>00153 
<a name="l00157"></a>00157   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00158"></a>00158   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a4a694febe8dba0a1203fd628cd1e3167" title="Returns size along dimension 1.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetLength</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00159"></a>00159 <span class="keyword">  </span>{
<a name="l00160"></a>00160     <span class="keywordflow">return</span> data_(i).GetLength();
<a name="l00161"></a>00161   }
<a name="l00162"></a>00162 
<a name="l00163"></a>00163 
<a name="l00165"></a>00165 
<a name="l00169"></a>00169   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00170"></a>00170   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a66946f1764cc14abec8a51d4e310f6b5" title="Returns size along dimension 1.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00171"></a>00171 <span class="keyword">  </span>{
<a name="l00172"></a>00172     <span class="keywordflow">return</span> data_(i).GetSize();
<a name="l00173"></a>00173   }
<a name="l00174"></a>00174 
<a name="l00175"></a>00175 
<a name="l00177"></a>00177 
<a name="l00182"></a>00182   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00183"></a>00183   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a4a694febe8dba0a1203fd628cd1e3167" title="Returns size along dimension 1.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00184"></a>00184 <a class="code" href="class_seldon_1_1_vector3.php#a4a694febe8dba0a1203fd628cd1e3167" title="Returns size along dimension 1.">  ::GetLength</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00185"></a>00185 <span class="keyword">  </span>{
<a name="l00186"></a>00186     <span class="keywordflow">return</span> data_(i)(j).GetLength();
<a name="l00187"></a>00187   }
<a name="l00188"></a>00188 
<a name="l00189"></a>00189 
<a name="l00191"></a>00191 
<a name="l00196"></a>00196   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00197"></a>00197   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a66946f1764cc14abec8a51d4e310f6b5" title="Returns size along dimension 1.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00198"></a>00198 <a class="code" href="class_seldon_1_1_vector3.php#a66946f1764cc14abec8a51d4e310f6b5" title="Returns size along dimension 1.">  ::GetSize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00199"></a>00199 <span class="keyword">  </span>{
<a name="l00200"></a>00200     <span class="keywordflow">return</span> data_(i)(j).GetSize();
<a name="l00201"></a>00201   }
<a name="l00202"></a>00202 
<a name="l00203"></a>00203 
<a name="l00205"></a>00205 
<a name="l00208"></a>00208   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00209"></a>00209   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a497379d28c87524052ee202702db9c91" title="Returns the total number of elements in the inner vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetNelement</a>()<span class="keyword"> const</span>
<a name="l00210"></a>00210 <span class="keyword">  </span>{
<a name="l00211"></a>00211     <span class="keywordtype">int</span> total = 0;
<a name="l00212"></a>00212     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_vector3.php#a4a694febe8dba0a1203fd628cd1e3167" title="Returns size along dimension 1.">GetLength</a>(); i++)
<a name="l00213"></a>00213       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; <a class="code" href="class_seldon_1_1_vector3.php#a4a694febe8dba0a1203fd628cd1e3167" title="Returns size along dimension 1.">GetLength</a>(i); j++ )
<a name="l00214"></a>00214         total += <a class="code" href="class_seldon_1_1_vector3.php#a4a694febe8dba0a1203fd628cd1e3167" title="Returns size along dimension 1.">GetLength</a>(i, j);
<a name="l00215"></a>00215     <span class="keywordflow">return</span> total;
<a name="l00216"></a>00216   }
<a name="l00217"></a>00217 
<a name="l00218"></a>00218 
<a name="l00220"></a>00220 
<a name="l00227"></a>00227   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00228"></a>00228   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a497379d28c87524052ee202702db9c91" title="Returns the total number of elements in the inner vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00229"></a>00229 <a class="code" href="class_seldon_1_1_vector3.php#a497379d28c87524052ee202702db9c91" title="Returns the total number of elements in the inner vectors.">  ::GetNelement</a>(<span class="keywordtype">int</span> beg, <span class="keywordtype">int</span> end)<span class="keyword"> const</span>
<a name="l00230"></a>00230 <span class="keyword">  </span>{
<a name="l00231"></a>00231     <span class="keywordflow">if</span> (beg &gt; end)
<a name="l00232"></a>00232       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Vector3::GetNelement(int beg, int end)&quot;</span>,
<a name="l00233"></a>00233                           <span class="stringliteral">&quot;The lower bound of the range of inner vectors &quot;</span>
<a name="l00234"></a>00234                           <span class="stringliteral">&quot;of vectors, [&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end)
<a name="l00235"></a>00235                           + <span class="stringliteral">&quot;[, is strictly greater than its upper bound.&quot;</span>);
<a name="l00236"></a>00236     <span class="keywordflow">if</span> (beg &lt; 0 || end &gt; GetLength())
<a name="l00237"></a>00237       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Vector3::GetNelement(int beg, int end)&quot;</span>,
<a name="l00238"></a>00238                           <span class="stringliteral">&quot;The inner-vector of vectors indexes should be in &quot;</span>
<a name="l00239"></a>00239                           <span class="stringliteral">&quot;[0,&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(GetLength()) + <span class="stringliteral">&quot;] but [&quot;</span>
<a name="l00240"></a>00240                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg)
<a name="l00241"></a>00241                           + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end) + <span class="stringliteral">&quot;[ was provided.&quot;</span>);
<a name="l00242"></a>00242 
<a name="l00243"></a>00243     <span class="keywordtype">int</span> total = 0;
<a name="l00244"></a>00244     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = beg; i &lt; end; i++)
<a name="l00245"></a>00245       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; GetLength(i); j++ )
<a name="l00246"></a>00246         total += GetLength(i, j);
<a name="l00247"></a>00247     <span class="keywordflow">return</span> total;
<a name="l00248"></a>00248   }
<a name="l00249"></a>00249 
<a name="l00250"></a>00250 
<a name="l00252"></a>00252 
<a name="l00266"></a>00266   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00267"></a>00267   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a497379d28c87524052ee202702db9c91" title="Returns the total number of elements in the inner vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00268"></a>00268 <a class="code" href="class_seldon_1_1_vector3.php#a497379d28c87524052ee202702db9c91" title="Returns the total number of elements in the inner vectors.">  ::GetNelement</a>(<span class="keywordtype">int</span> beg0, <span class="keywordtype">int</span> end0, <span class="keywordtype">int</span> beg1, <span class="keywordtype">int</span> end1)<span class="keyword"> const</span>
<a name="l00269"></a>00269 <span class="keyword">  </span>{
<a name="l00270"></a>00270     <span class="keywordflow">if</span> (beg0 &gt; end0)
<a name="l00271"></a>00271       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Vector3:::GetNelement(int beg0, int end0, &quot;</span>
<a name="l00272"></a>00272                           <span class="stringliteral">&quot;int beg1, int end1)&quot;</span>,
<a name="l00273"></a>00273                           <span class="stringliteral">&quot;The lower bound of the range of inner vectors &quot;</span>
<a name="l00274"></a>00274                           <span class="stringliteral">&quot;of vectors, [&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg0) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end0)
<a name="l00275"></a>00275                           + <span class="stringliteral">&quot;[, is strictly greater than its upper bound.&quot;</span>);
<a name="l00276"></a>00276     <span class="keywordflow">if</span> (beg1 &gt; end1)
<a name="l00277"></a>00277       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Vector3:::GetNelement(int beg0, int end0, &quot;</span>
<a name="l00278"></a>00278                           <span class="stringliteral">&quot;int beg1, int end1)&quot;</span>,
<a name="l00279"></a>00279                           <span class="stringliteral">&quot;The lower bound of the range of &quot;</span>
<a name="l00280"></a>00280                           <span class="stringliteral">&quot;of vectors, [&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg1) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end1)
<a name="l00281"></a>00281                           + <span class="stringliteral">&quot;[, is strictly greater than its upper bound.&quot;</span>);
<a name="l00282"></a>00282     <span class="keywordflow">if</span> (beg0 &lt; 0 || end0 &gt; GetLength())
<a name="l00283"></a>00283       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Vector3:::GetNelement(int beg0, int end0, &quot;</span>
<a name="l00284"></a>00284                           <span class="stringliteral">&quot;int beg1, int end1)&quot;</span>,
<a name="l00285"></a>00285                           <span class="stringliteral">&quot;The inner-vector of vectors indexes should be in &quot;</span>
<a name="l00286"></a>00286                           <span class="stringliteral">&quot;[0,&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(GetLength()) + <span class="stringliteral">&quot;] but [&quot;</span>
<a name="l00287"></a>00287                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg0)
<a name="l00288"></a>00288                           + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end0) + <span class="stringliteral">&quot;[ was provided.&quot;</span>);
<a name="l00289"></a>00289 
<a name="l00290"></a>00290     <span class="keywordtype">int</span> total = 0;
<a name="l00291"></a>00291     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = beg0; i &lt; end0; i++)
<a name="l00292"></a>00292       {
<a name="l00293"></a>00293         <span class="keywordflow">if</span> (beg1 &lt; 0 || end1 &gt; GetLength(i))
<a name="l00294"></a>00294           <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Vector3:::GetNelement(int beg0, int end0, &quot;</span>
<a name="l00295"></a>00295                               <span class="stringliteral">&quot;int beg1, int end1)&quot;</span>,
<a name="l00296"></a>00296                               <span class="stringliteral">&quot;For the inner vector of vectors &quot;</span>
<a name="l00297"></a>00297                               + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, the vectors indexes should be &quot;</span>
<a name="l00298"></a>00298                               <span class="stringliteral">&quot;in [0,&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(GetLength(i)) + <span class="stringliteral">&quot;] but [&quot;</span>
<a name="l00299"></a>00299                               + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg1)
<a name="l00300"></a>00300                               + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end1) + <span class="stringliteral">&quot;[ was provided.&quot;</span>);
<a name="l00301"></a>00301         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = beg1; j &lt; end1; j++)
<a name="l00302"></a>00302           total += GetLength(i, j);
<a name="l00303"></a>00303       }
<a name="l00304"></a>00304     <span class="keywordflow">return</span> total;
<a name="l00305"></a>00305   }
<a name="l00306"></a>00306 
<a name="l00307"></a>00307 
<a name="l00309"></a>00309 
<a name="l00314"></a>00314   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00315"></a>00315   Vector&lt;int&gt; <a class="code" href="class_seldon_1_1_vector3.php#a5772120ddd32015c71c56ac2e44413e0" title="Returns the shape of a vector of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00316"></a>00316 <a class="code" href="class_seldon_1_1_vector3.php#a5772120ddd32015c71c56ac2e44413e0" title="Returns the shape of a vector of vectors.">  ::GetShape</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00317"></a>00317 <span class="keyword">  </span>{
<a name="l00318"></a>00318     Vector&lt;int&gt; shape(GetLength(i));
<a name="l00319"></a>00319     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; GetLength(i); j++)
<a name="l00320"></a>00320       shape(j) = GetLength(i, j);
<a name="l00321"></a>00321     <span class="keywordflow">return</span> shape;
<a name="l00322"></a>00322   }
<a name="l00323"></a>00323 
<a name="l00324"></a>00324 
<a name="l00326"></a>00326 
<a name="l00331"></a>00331   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00332"></a>00332   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a5772120ddd32015c71c56ac2e44413e0" title="Returns the shape of a vector of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00333"></a>00333 <a class="code" href="class_seldon_1_1_vector3.php#a5772120ddd32015c71c56ac2e44413e0" title="Returns the shape of a vector of vectors.">  ::GetShape</a>(<span class="keywordtype">int</span> i, Vector&lt;int&gt;&amp; shape)<span class="keyword"> const</span>
<a name="l00334"></a>00334 <span class="keyword">  </span>{
<a name="l00335"></a>00335     shape.Reallocate(GetLength(i));
<a name="l00336"></a>00336     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; GetLength(i); j++)
<a name="l00337"></a>00337       shape(j) = GetLength(i, j);
<a name="l00338"></a>00338   }
<a name="l00339"></a>00339 
<a name="l00340"></a>00340 
<a name="l00342"></a>00342 
<a name="l00345"></a>00345   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00346"></a>00346   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a21e22b8d942820eb4c60e3e34c2afcd6" title="Reallocates the vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::Reallocate</a>(<span class="keywordtype">int</span> N)
<a name="l00347"></a>00347   {
<a name="l00348"></a>00348     data_.Reallocate(N);
<a name="l00349"></a>00349   }
<a name="l00350"></a>00350 
<a name="l00351"></a>00351 
<a name="l00353"></a>00353 
<a name="l00357"></a>00357   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00358"></a>00358   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a21e22b8d942820eb4c60e3e34c2afcd6" title="Reallocates the vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00359"></a>00359 <a class="code" href="class_seldon_1_1_vector3.php#a21e22b8d942820eb4c60e3e34c2afcd6" title="Reallocates the vector of vectors of vectors.">  ::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> N)
<a name="l00360"></a>00360   {
<a name="l00361"></a>00361     data_(i).Reallocate(N);
<a name="l00362"></a>00362   }
<a name="l00363"></a>00363 
<a name="l00364"></a>00364 
<a name="l00366"></a>00366 
<a name="l00371"></a>00371   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00372"></a>00372   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a21e22b8d942820eb4c60e3e34c2afcd6" title="Reallocates the vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00373"></a>00373 <a class="code" href="class_seldon_1_1_vector3.php#a21e22b8d942820eb4c60e3e34c2afcd6" title="Reallocates the vector of vectors of vectors.">  ::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> N)
<a name="l00374"></a>00374   {
<a name="l00375"></a>00375     data_(i)(j).Reallocate(N);
<a name="l00376"></a>00376   }
<a name="l00377"></a>00377 
<a name="l00378"></a>00378 
<a name="l00380"></a>00380 
<a name="l00384"></a><a class="code" href="class_seldon_1_1_vector3.php#a1f70de4b1edeb1a64078ad3113014a8e">00384</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00385"></a>00385   <span class="keyword">template</span> &lt;<span class="keyword">class</span> Td, <span class="keyword">class</span> Allocatord&gt;
<a name="l00386"></a>00386   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a1f70de4b1edeb1a64078ad3113014a8e" title="Returns all values in a vector.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00387"></a>00387 <a class="code" href="class_seldon_1_1_vector3.php#a1f70de4b1edeb1a64078ad3113014a8e" title="Returns all values in a vector.">  ::Flatten</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Td, VectFull, Allocatord&gt;</a>&amp; data)<span class="keyword"> const</span>
<a name="l00388"></a>00388 <span class="keyword">  </span>{
<a name="l00389"></a>00389     data.Reallocate(GetNelement());
<a name="l00390"></a>00390     <span class="keywordtype">int</span> i, j, k, n(0);
<a name="l00391"></a>00391     <span class="keywordflow">for</span> (i = 0; i &lt; GetLength(); i++)
<a name="l00392"></a>00392       <span class="keywordflow">for</span> (j = 0; j &lt; GetLength(i); j++)
<a name="l00393"></a>00393         <span class="keywordflow">for</span> (k = 0; k &lt; GetLength(i, j); k++)
<a name="l00394"></a>00394           data(n++) = data_(i)(j)(k);
<a name="l00395"></a>00395   }
<a name="l00396"></a>00396 
<a name="l00397"></a>00397 
<a name="l00399"></a>00399 
<a name="l00407"></a><a class="code" href="class_seldon_1_1_vector3.php#a84d542b134e8f98e944932b154c1d48d">00407</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00408"></a>00408   <span class="keyword">template</span> &lt;<span class="keyword">class</span> Td, <span class="keyword">class</span> Allocatord&gt;
<a name="l00409"></a>00409   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a1f70de4b1edeb1a64078ad3113014a8e" title="Returns all values in a vector.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00410"></a>00410 <a class="code" href="class_seldon_1_1_vector3.php#a1f70de4b1edeb1a64078ad3113014a8e" title="Returns all values in a vector.">  ::Flatten</a>(<span class="keywordtype">int</span> beg, <span class="keywordtype">int</span> end, <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Td, VectFull, Allocatord&gt;</a>&amp; data)<span class="keyword"> const</span>
<a name="l00411"></a>00411 <span class="keyword">  </span>{
<a name="l00412"></a>00412     <span class="keywordflow">if</span> (beg &gt; end)
<a name="l00413"></a>00413       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Vector3:::Flatten(int beg, int end, Vector&amp; data)&quot;</span>,
<a name="l00414"></a>00414                           <span class="stringliteral">&quot;The lower bound of the range of inner vectors &quot;</span>
<a name="l00415"></a>00415                           <span class="stringliteral">&quot;of vectors, [&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end)
<a name="l00416"></a>00416                           + <span class="stringliteral">&quot;[, is strictly greater than its upper bound.&quot;</span>);
<a name="l00417"></a>00417     <span class="keywordflow">if</span> (beg &lt; 0 || end &gt; GetLength())
<a name="l00418"></a>00418       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Vector3:::Flatten(int beg, int end, Vector&amp; data)&quot;</span>,
<a name="l00419"></a>00419                           <span class="stringliteral">&quot;The inner-vector of vectors indexes should be in &quot;</span>
<a name="l00420"></a>00420                           <span class="stringliteral">&quot;[0,&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(GetLength()) + <span class="stringliteral">&quot;] but [&quot;</span>
<a name="l00421"></a>00421                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg)
<a name="l00422"></a>00422                           + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end) + <span class="stringliteral">&quot;[ was provided.&quot;</span>);
<a name="l00423"></a>00423 
<a name="l00424"></a>00424     data.Reallocate(GetNelement(beg, end));
<a name="l00425"></a>00425     <span class="keywordtype">int</span> i, j, k, n(0);
<a name="l00426"></a>00426     <span class="keywordflow">for</span> (i = beg; i &lt; end; i++)
<a name="l00427"></a>00427       <span class="keywordflow">for</span> (j = 0; j &lt; GetLength(i); j++)
<a name="l00428"></a>00428         <span class="keywordflow">for</span> (k = 0; k &lt; GetLength(i, j); k++)
<a name="l00429"></a>00429           data(n++) = data_(i)(j)(k);
<a name="l00430"></a>00430   }
<a name="l00431"></a>00431 
<a name="l00432"></a>00432 
<a name="l00450"></a><a class="code" href="class_seldon_1_1_vector3.php#a84310d341a922124b1b6c9f5a541e998">00450</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00451"></a>00451   <span class="keyword">template</span> &lt;<span class="keyword">class</span> Td, <span class="keyword">class</span> Allocatord&gt;
<a name="l00452"></a>00452   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a1f70de4b1edeb1a64078ad3113014a8e" title="Returns all values in a vector.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00453"></a>00453 <a class="code" href="class_seldon_1_1_vector3.php#a1f70de4b1edeb1a64078ad3113014a8e" title="Returns all values in a vector.">  ::Flatten</a>(<span class="keywordtype">int</span> beg0, <span class="keywordtype">int</span> end0, <span class="keywordtype">int</span> beg1, <span class="keywordtype">int</span> end1,
<a name="l00454"></a>00454             <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Td, VectFull, Allocatord&gt;</a>&amp; data)<span class="keyword"> const</span>
<a name="l00455"></a>00455 <span class="keyword">  </span>{
<a name="l00456"></a>00456     <span class="keywordflow">if</span> (beg0 &gt; end0)
<a name="l00457"></a>00457       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Vector3:::Flatten(int beg0, int end0, int beg1, &quot;</span>
<a name="l00458"></a>00458                           <span class="stringliteral">&quot;int end1, Vector&amp; data)&quot;</span>,
<a name="l00459"></a>00459                           <span class="stringliteral">&quot;The lower bound of the range of inner vectors &quot;</span>
<a name="l00460"></a>00460                           <span class="stringliteral">&quot;of vectors, [&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg0) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end0)
<a name="l00461"></a>00461                           + <span class="stringliteral">&quot;[, is strictly greater than its upper bound.&quot;</span>);
<a name="l00462"></a>00462     <span class="keywordflow">if</span> (beg1 &gt; end1)
<a name="l00463"></a>00463       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Vector3:::Flatten(int beg0, int end0, int beg1, &quot;</span>
<a name="l00464"></a>00464                           <span class="stringliteral">&quot;int end1, Vector&amp; data)&quot;</span>,
<a name="l00465"></a>00465                           <span class="stringliteral">&quot;The lower bound of the range of &quot;</span>
<a name="l00466"></a>00466                           <span class="stringliteral">&quot;of vectors, [&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg1) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end1)
<a name="l00467"></a>00467                           + <span class="stringliteral">&quot;[, is strictly greater than its upper bound.&quot;</span>);
<a name="l00468"></a>00468     <span class="keywordflow">if</span> (beg0 &lt; 0 || end0 &gt; GetLength())
<a name="l00469"></a>00469       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Vector3:::Flatten(int beg0, int end0, int beg1, &quot;</span>
<a name="l00470"></a>00470                           <span class="stringliteral">&quot;int end1, Vector&amp; data)&quot;</span>,
<a name="l00471"></a>00471                           <span class="stringliteral">&quot;The inner-vector of vectors indexes should be in &quot;</span>
<a name="l00472"></a>00472                           <span class="stringliteral">&quot;[0,&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(GetLength()) + <span class="stringliteral">&quot;] but [&quot;</span>
<a name="l00473"></a>00473                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg0)
<a name="l00474"></a>00474                           + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end0) + <span class="stringliteral">&quot;[ was provided.&quot;</span>);
<a name="l00475"></a>00475 
<a name="l00476"></a>00476     data.Reallocate(GetNelement(beg0, end0, beg1, end1));
<a name="l00477"></a>00477     <span class="keywordtype">int</span> i, j, k, n(0);
<a name="l00478"></a>00478     <span class="keywordflow">for</span> (i = beg0; i &lt; end0; i++)
<a name="l00479"></a>00479       {
<a name="l00480"></a>00480         <span class="keywordflow">if</span> (beg1 &lt; 0 || end1 &gt; GetLength(i))
<a name="l00481"></a>00481           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Vector3:::Flatten(int beg0, int end0, int beg1, &quot;</span>
<a name="l00482"></a>00482                               <span class="stringliteral">&quot;int end1, Vector&amp; data)&quot;</span>,
<a name="l00483"></a>00483                               <span class="stringliteral">&quot;For the inner vector of vectors &quot;</span>
<a name="l00484"></a>00484                               + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, the vectors indexes should be &quot;</span>
<a name="l00485"></a>00485                               <span class="stringliteral">&quot;in [0,&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(GetLength(i)) + <span class="stringliteral">&quot;] but [&quot;</span>
<a name="l00486"></a>00486                               + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg1)
<a name="l00487"></a>00487                               + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end1) + <span class="stringliteral">&quot;[ was provided.&quot;</span>);
<a name="l00488"></a>00488         <span class="keywordflow">for</span> (j = beg1; j &lt; end1; j++)
<a name="l00489"></a>00489           <span class="keywordflow">for</span> (k = 0; k &lt; GetLength(i, j); k++)
<a name="l00490"></a>00490             data(n++) = data_(i)(j)(k);
<a name="l00491"></a>00491       }
<a name="l00492"></a>00492   }
<a name="l00493"></a>00493 
<a name="l00494"></a>00494 
<a name="l00496"></a>00496 
<a name="l00501"></a>00501   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00502"></a>00502   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3</a>&lt;T, Allocator0,
<a name="l00503"></a>00503                Allocator1, Allocator2&gt;::PushBack(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; x)
<a name="l00504"></a>00504   {
<a name="l00505"></a>00505     data_(i)(j).PushBack(x);
<a name="l00506"></a>00506   }
<a name="l00507"></a>00507 
<a name="l00508"></a>00508 
<a name="l00510"></a>00510 
<a name="l00514"></a>00514   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00515"></a>00515   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd" title="Appends an element at the end of the inner vector #i #j.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::</a>
<a name="l00516"></a>00516 <a class="code" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd" title="Appends an element at the end of the inner vector #i #j.">  PushBack</a>(<span class="keywordtype">int</span> i, <span class="keyword">const</span> Vector&lt;T, Vect_Full, Allocator0&gt;&amp; X)
<a name="l00517"></a>00517   {
<a name="l00518"></a>00518     data_(i).PushBack(X);
<a name="l00519"></a>00519   }
<a name="l00520"></a>00520 
<a name="l00521"></a>00521 
<a name="l00523"></a>00523 
<a name="l00526"></a>00526   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00527"></a>00527   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd" title="Appends an element at the end of the inner vector #i #j.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00528"></a>00528 <a class="code" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd" title="Appends an element at the end of the inner vector #i #j.">  ::PushBack</a>(<span class="keyword">const</span> Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;,
<a name="l00529"></a>00529              Vect_Full, Allocator1&gt;&amp; X)
<a name="l00530"></a>00530   {
<a name="l00531"></a>00531     data_.PushBack(X);
<a name="l00532"></a>00532   }
<a name="l00533"></a>00533 
<a name="l00534"></a>00534 
<a name="l00536"></a>00536 
<a name="l00540"></a>00540   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00541"></a>00541   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd" title="Appends an element at the end of the inner vector #i #j.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00542"></a>00542 <a class="code" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd" title="Appends an element at the end of the inner vector #i #j.">  ::PushBack</a>(<span class="keyword">const</span> Vector&lt;Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;,
<a name="l00543"></a>00543              Vect_Full, Allocator1&gt;, Vect_Full, Allocator2&gt;&amp; X)
<a name="l00544"></a>00544   {
<a name="l00545"></a>00545     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetLength(); i++)
<a name="l00546"></a>00546       data_.PushBack(X(i));
<a name="l00547"></a>00547   }
<a name="l00548"></a>00548 
<a name="l00549"></a>00549 
<a name="l00551"></a>00551 
<a name="l00555"></a>00555   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00556"></a>00556   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd" title="Appends an element at the end of the inner vector #i #j.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00557"></a>00557 <a class="code" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd" title="Appends an element at the end of the inner vector #i #j.">  ::PushBack</a>(<span class="keyword">const</span> Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;&amp; X)
<a name="l00558"></a>00558   {
<a name="l00559"></a>00559     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetLength(); i++)
<a name="l00560"></a>00560       data_.PushBack(X.GetVector());
<a name="l00561"></a>00561   }
<a name="l00562"></a>00562 
<a name="l00563"></a><a class="code" href="class_seldon_1_1_vector3.php#a0e178a2f8fad92f0929f4ea1b1dd1604">00563</a> 
<a name="l00565"></a>00565   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00566"></a>00566   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a0e178a2f8fad92f0929f4ea1b1dd1604" title="Clears the vector.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::Clear</a>()
<a name="l00567"></a>00567   {
<a name="l00568"></a>00568     data_.Clear();
<a name="l00569"></a>00569   }
<a name="l00570"></a>00570 
<a name="l00571"></a>00571 
<a name="l00573"></a>00573 
<a name="l00576"></a>00576   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00577"></a>00577   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3</a>&lt;T, Allocator0,
<a name="l00578"></a>00578                Allocator1, Allocator2&gt;::Clear(<span class="keywordtype">int</span> i)
<a name="l00579"></a>00579   {
<a name="l00580"></a>00580     data_(i).Clear();
<a name="l00581"></a>00581   }
<a name="l00582"></a>00582 
<a name="l00583"></a>00583 
<a name="l00585"></a>00585 
<a name="l00589"></a>00589   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00590"></a>00590   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a0e178a2f8fad92f0929f4ea1b1dd1604" title="Clears the vector.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::Clear</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00591"></a>00591   {
<a name="l00592"></a>00592     data_(i)(j).<a class="code" href="class_seldon_1_1_vector3.php#a0e178a2f8fad92f0929f4ea1b1dd1604" title="Clears the vector.">Clear</a>();
<a name="l00593"></a>00593   }
<a name="l00594"></a>00594 
<a name="l00595"></a>00595 
<a name="l00597"></a>00597 
<a name="l00600"></a>00600   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00601"></a>00601   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a0d6388a07fda225152eef87cdab2ab5f" title="Fills the vector with a given value.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::Fill</a>(<span class="keyword">const</span> T&amp; x)
<a name="l00602"></a>00602   {
<a name="l00603"></a>00603     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; data_.GetSize(); i++)
<a name="l00604"></a>00604       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; data_(i).GetSize(); j++)
<a name="l00605"></a>00605         data_(i)(j).<a class="code" href="class_seldon_1_1_vector3.php#a0d6388a07fda225152eef87cdab2ab5f" title="Fills the vector with a given value.">Fill</a>(x);
<a name="l00606"></a>00606   }
<a name="l00607"></a>00607 
<a name="l00608"></a>00608 
<a name="l00610"></a>00610 
<a name="l00613"></a><a class="code" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17">00613</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00614"></a>00614   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;</a>, Vect_Full, Allocator1&gt;,
<a name="l00615"></a>00615          Vect_Full, Allocator2&gt;&amp;
<a name="l00616"></a>00616   <a class="code" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17" title="Returns the vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetVector</a>()
<a name="l00617"></a>00617   {
<a name="l00618"></a>00618     <span class="keywordflow">return</span> data_;
<a name="l00619"></a>00619   }
<a name="l00620"></a>00620 
<a name="l00621"></a>00621 
<a name="l00623"></a>00623 
<a name="l00626"></a><a class="code" href="class_seldon_1_1_vector3.php#acc38f6e8f4660dc1c98999b08a467454">00626</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00627"></a>00627   <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;</a>,
<a name="l00628"></a>00628                       Vect_Full, Allocator1&gt;, Vect_Full, Allocator2&gt;&amp;
<a name="l00629"></a>00629   <a class="code" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17" title="Returns the vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetVector</a>()<span class="keyword"> const</span>
<a name="l00630"></a>00630 <span class="keyword">  </span>{
<a name="l00631"></a>00631     <span class="keywordflow">return</span> data_;
<a name="l00632"></a>00632   }
<a name="l00633"></a>00633 
<a name="l00634"></a>00634 
<a name="l00636"></a>00636 
<a name="l00640"></a>00640   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00641"></a>00641   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;</a>, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator1&gt;&amp;
<a name="l00642"></a>00642   <a class="code" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17" title="Returns the vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetVector</a>(<span class="keywordtype">int</span> i)
<a name="l00643"></a>00643   {
<a name="l00644"></a>00644     <span class="keywordflow">return</span> data_(i);
<a name="l00645"></a>00645   }
<a name="l00646"></a>00646 
<a name="l00647"></a>00647 
<a name="l00649"></a>00649 
<a name="l00653"></a>00653   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00654"></a>00654   <span class="keyword">const</span> Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;, VectFull, Allocator1&gt;&amp;
<a name="l00655"></a>00655   <a class="code" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17" title="Returns the vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetVector</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00656"></a>00656 <span class="keyword">  </span>{
<a name="l00657"></a>00657     <span class="keywordflow">return</span> data_(i);
<a name="l00658"></a>00658   }
<a name="l00659"></a>00659 
<a name="l00660"></a>00660 
<a name="l00662"></a>00662 
<a name="l00667"></a>00667   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00668"></a>00668   Vector&lt;T, Vect_Full, Allocator0&gt;&amp;
<a name="l00669"></a>00669   <a class="code" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17" title="Returns the vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetVector</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00670"></a>00670   {
<a name="l00671"></a>00671     <span class="keywordflow">return</span> data_(i)(j);
<a name="l00672"></a>00672   }
<a name="l00673"></a>00673 
<a name="l00674"></a>00674 
<a name="l00676"></a>00676 
<a name="l00681"></a>00681   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00682"></a>00682   <span class="keyword">const</span> Vector&lt;T, Vect_Full, Allocator0&gt;&amp;
<a name="l00683"></a>00683   <a class="code" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17" title="Returns the vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetVector</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"></span>
<a name="l00684"></a>00684 <span class="keyword">    const</span>
<a name="l00685"></a>00685 <span class="keyword">  </span>{
<a name="l00686"></a>00686     <span class="keywordflow">return</span> data_(i)(j);
<a name="l00687"></a>00687   }
<a name="l00688"></a>00688 
<a name="l00689"></a>00689 
<a name="l00690"></a>00690   <span class="comment">/*********************************</span>
<a name="l00691"></a>00691 <span class="comment">   * ELEMENT ACCESS AND ASSIGNMENT *</span>
<a name="l00692"></a>00692 <span class="comment">   *********************************/</span>
<a name="l00693"></a>00693 
<a name="l00694"></a>00694 
<a name="l00696"></a>00696 
<a name="l00700"></a><a class="code" href="class_seldon_1_1_vector3.php#aaacba77cabdd3d80b2809778ee76206b">00700</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00701"></a>00701   <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;</a>, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator1&gt;&amp;
<a name="l00702"></a>00702   <a class="code" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3</a>&lt;T, Allocator0,
<a name="l00703"></a>00703           Allocator1, Allocator2&gt;::operator() (<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00704"></a>00704 <span class="keyword">  </span>{
<a name="l00705"></a>00705     <span class="keywordflow">return</span> data_(i);
<a name="l00706"></a>00706   }
<a name="l00707"></a>00707 
<a name="l00708"></a>00708 
<a name="l00710"></a>00710 
<a name="l00714"></a>00714   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00715"></a>00715   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;</a>, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator1&gt;&amp;
<a name="l00716"></a>00716   <a class="code" href="class_seldon_1_1_vector3.php#aaacba77cabdd3d80b2809778ee76206b" title="Returns a given inner vector of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::operator() </a>(<span class="keywordtype">int</span> i)
<a name="l00717"></a>00717   {
<a name="l00718"></a>00718     <span class="keywordflow">return</span> data_(i);
<a name="l00719"></a>00719   }
<a name="l00720"></a>00720 
<a name="l00721"></a>00721 
<a name="l00723"></a>00723 
<a name="l00728"></a><a class="code" href="class_seldon_1_1_vector3.php#a1298a6ec16e6a70ce7de2e456a0c56aa">00728</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00729"></a>00729   <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Vect_Full, Allocator0&gt;</a>&amp;
<a name="l00730"></a>00730   <a class="code" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3</a>&lt;T, Allocator0,
<a name="l00731"></a>00731           Allocator1, Allocator2&gt;::operator() (<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00732"></a>00732 <span class="keyword">  </span>{
<a name="l00733"></a>00733     <span class="keywordflow">return</span> data_(i)(j);
<a name="l00734"></a>00734   }
<a name="l00735"></a>00735 
<a name="l00736"></a>00736 
<a name="l00738"></a>00738 
<a name="l00743"></a>00743   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00744"></a>00744   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Vect_Full, Allocator0&gt;</a>&amp;
<a name="l00745"></a>00745   <a class="code" href="class_seldon_1_1_vector3.php#aaacba77cabdd3d80b2809778ee76206b" title="Returns a given inner vector of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00746"></a>00746   {
<a name="l00747"></a>00747     <span class="keywordflow">return</span> data_(i)(j);
<a name="l00748"></a>00748   }
<a name="l00749"></a>00749 
<a name="l00750"></a>00750 
<a name="l00752"></a>00752 
<a name="l00758"></a><a class="code" href="class_seldon_1_1_vector3.php#afc9bb303c83fd61af66cb9f2b7ea9842">00758</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00759"></a>00759   <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::const_reference</a>
<a name="l00760"></a>00760   <a class="code" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3</a>&lt;T, Allocator0,
<a name="l00761"></a>00761           Allocator1, Allocator2&gt;::operator() (<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> k)<span class="keyword"> const</span>
<a name="l00762"></a>00762 <span class="keyword">  </span>{
<a name="l00763"></a>00763     <span class="keywordflow">return</span> data_(i)(j)(k);
<a name="l00764"></a>00764   }
<a name="l00765"></a>00765 
<a name="l00766"></a>00766 
<a name="l00768"></a>00768 
<a name="l00774"></a><a class="code" href="class_seldon_1_1_vector3.php#ad67a55c6828c310590dae6b671ff2d07">00774</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00775"></a>00775   <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::reference</a>
<a name="l00776"></a>00776   <a class="code" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::operator</a>()
<a name="l00777"></a>00777     (<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> k)
<a name="l00778"></a>00778   {
<a name="l00779"></a>00779     <span class="keywordflow">return</span> data_(i)(j)(k);
<a name="l00780"></a>00780   }
<a name="l00781"></a>00781 
<a name="l00782"></a>00782 
<a name="l00783"></a>00783   <span class="comment">/**********************</span>
<a name="l00784"></a>00784 <span class="comment">   * CONVENIENT METHODS *</span>
<a name="l00785"></a>00785 <span class="comment">   *********************/</span>
<a name="l00786"></a>00786 
<a name="l00787"></a><a class="code" href="class_seldon_1_1_vector3.php#a9b3c87591141123ac43759d6b9ca9531">00787</a> 
<a name="l00789"></a>00789   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00790"></a>00790   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a9b3c87591141123ac43759d6b9ca9531" title="Displays the vector.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00791"></a>00791 <span class="keyword">  </span>{
<a name="l00792"></a>00792     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; data_.GetSize(); i++)
<a name="l00793"></a>00793       <span class="keywordflow">for</span>(<span class="keywordtype">int</span> j = 0; j &lt; data_(i).GetSize(); j++)
<a name="l00794"></a>00794         {
<a name="l00795"></a>00795           cout &lt;&lt; <span class="stringliteral">&quot;Vector &quot;</span> &lt;&lt; i &lt;&lt; <span class="stringliteral">&quot;, &quot;</span> &lt;&lt; j &lt;&lt; <span class="stringliteral">&quot;: &quot;</span>;
<a name="l00796"></a>00796           data_(i)(j).<a class="code" href="class_seldon_1_1_vector3.php#a9b3c87591141123ac43759d6b9ca9531" title="Displays the vector.">Print</a>();
<a name="l00797"></a>00797         }
<a name="l00798"></a>00798   }
<a name="l00799"></a>00799 
<a name="l00800"></a>00800 
<a name="l00801"></a>00801   <span class="comment">/**************************</span>
<a name="l00802"></a>00802 <span class="comment">   * INPUT/OUTPUT FUNCTIONS *</span>
<a name="l00803"></a>00803 <span class="comment">   **************************/</span>
<a name="l00804"></a>00804 
<a name="l00805"></a>00805 
<a name="l00807"></a>00807 
<a name="l00812"></a>00812   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00813"></a>00813   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#ad0372ca6e3d3dbee249dddd5eb4c4363" title="Writes the instance in a binary file.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00814"></a>00814 <a class="code" href="class_seldon_1_1_vector3.php#ad0372ca6e3d3dbee249dddd5eb4c4363" title="Writes the instance in a binary file.">  ::Write</a>(<span class="keywordtype">string</span> file_name, <span class="keywordtype">bool</span> with_size)<span class="keyword"> const</span>
<a name="l00815"></a>00815 <span class="keyword">  </span>{
<a name="l00816"></a>00816     ofstream file_stream;
<a name="l00817"></a>00817     file_stream.open(file_name.c_str());
<a name="l00818"></a>00818 
<a name="l00819"></a>00819 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00820"></a>00820 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00821"></a>00821     <span class="keywordflow">if</span> (!file_stream.is_open())
<a name="l00822"></a>00822       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector3::Write(string file_name, bool with_size)&quot;</span>,
<a name="l00823"></a>00823                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + file_name + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00824"></a>00824 <span class="preprocessor">#endif</span>
<a name="l00825"></a>00825 <span class="preprocessor"></span>
<a name="l00826"></a>00826     this-&gt;Write(file_stream, with_size);
<a name="l00827"></a>00827 
<a name="l00828"></a>00828     file_stream.close();
<a name="l00829"></a>00829   }
<a name="l00830"></a>00830 
<a name="l00831"></a>00831 
<a name="l00833"></a>00833 
<a name="l00838"></a>00838   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00839"></a>00839   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#ad0372ca6e3d3dbee249dddd5eb4c4363" title="Writes the instance in a binary file.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00840"></a>00840 <a class="code" href="class_seldon_1_1_vector3.php#ad0372ca6e3d3dbee249dddd5eb4c4363" title="Writes the instance in a binary file.">  ::Write</a>(ostream&amp; stream, <span class="keywordtype">bool</span> with_size)<span class="keyword"> const</span>
<a name="l00841"></a>00841 <span class="keyword">  </span>{
<a name="l00842"></a>00842 
<a name="l00843"></a>00843 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00844"></a>00844 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00845"></a>00845     <span class="keywordflow">if</span> (!stream.good())
<a name="l00846"></a>00846       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector3::Write(ostream&amp; stream, bool with_size)&quot;</span>,
<a name="l00847"></a>00847                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00848"></a>00848 <span class="preprocessor">#endif</span>
<a name="l00849"></a>00849 <span class="preprocessor"></span>
<a name="l00850"></a>00850     <span class="keywordflow">if</span> (with_size)
<a name="l00851"></a>00851       {
<a name="l00852"></a>00852         <span class="keywordtype">int</span> m = GetLength();
<a name="l00853"></a>00853         stream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;m)),
<a name="l00854"></a>00854                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00855"></a>00855       }
<a name="l00856"></a>00856 
<a name="l00857"></a>00857     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; GetLength(); i++)
<a name="l00858"></a>00858       {
<a name="l00859"></a>00859         <span class="keywordflow">if</span> (with_size)
<a name="l00860"></a>00860           {
<a name="l00861"></a>00861             <span class="keywordtype">int</span> m = GetLength(i);
<a name="l00862"></a>00862             stream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;m)),
<a name="l00863"></a>00863                          <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00864"></a>00864           }
<a name="l00865"></a>00865         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; GetLength(i); j++)
<a name="l00866"></a>00866           data_(i)(j).Write(stream, with_size);
<a name="l00867"></a>00867       }
<a name="l00868"></a>00868 
<a name="l00869"></a>00869 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00870"></a>00870 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00871"></a>00871     <span class="keywordflow">if</span> (!stream.good())
<a name="l00872"></a>00872       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector3::Write(ostream&amp; stream, bool with_size)&quot;</span>,
<a name="l00873"></a>00873                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l00874"></a>00874 <span class="preprocessor">#endif</span>
<a name="l00875"></a>00875 <span class="preprocessor"></span>
<a name="l00876"></a>00876   }
<a name="l00877"></a>00877 
<a name="l00878"></a>00878 
<a name="l00880"></a>00880 
<a name="l00887"></a>00887   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00888"></a>00888   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#ac28c5fe01b2ac97d21e560e9282444b5" title="Reads the Vector3 from a file.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00889"></a>00889 <a class="code" href="class_seldon_1_1_vector3.php#ac28c5fe01b2ac97d21e560e9282444b5" title="Reads the Vector3 from a file.">  ::Read</a>(<span class="keywordtype">string</span> file_name, <span class="keywordtype">bool</span> with_size)
<a name="l00890"></a>00890   {
<a name="l00891"></a>00891     ifstream file_stream;
<a name="l00892"></a>00892     file_stream.open(file_name.c_str());
<a name="l00893"></a>00893 
<a name="l00894"></a>00894 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00895"></a>00895 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00896"></a>00896     <span class="keywordflow">if</span> (!file_stream.is_open())
<a name="l00897"></a>00897       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector3::Read(string file_name, bool with_size)&quot;</span>,
<a name="l00898"></a>00898                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + file_name + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00899"></a>00899 <span class="preprocessor">#endif</span>
<a name="l00900"></a>00900 <span class="preprocessor"></span>
<a name="l00901"></a>00901     this-&gt;Read(file_stream, with_size);
<a name="l00902"></a>00902 
<a name="l00903"></a>00903     file_stream.close();
<a name="l00904"></a>00904   }
<a name="l00905"></a>00905 
<a name="l00906"></a>00906 
<a name="l00908"></a>00908 
<a name="l00915"></a>00915   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00916"></a>00916   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#ac28c5fe01b2ac97d21e560e9282444b5" title="Reads the Vector3 from a file.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00917"></a>00917 <a class="code" href="class_seldon_1_1_vector3.php#ac28c5fe01b2ac97d21e560e9282444b5" title="Reads the Vector3 from a file.">  ::Read</a>(istream&amp; stream, <span class="keywordtype">bool</span> with_size)
<a name="l00918"></a>00918   {
<a name="l00919"></a>00919 
<a name="l00920"></a>00920 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00921"></a>00921 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00922"></a>00922     <span class="keywordflow">if</span> (!stream.good())
<a name="l00923"></a>00923       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector3::Read(istream&amp; stream, bool with_size)&quot;</span>,
<a name="l00924"></a>00924                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00925"></a>00925 <span class="preprocessor">#endif</span>
<a name="l00926"></a>00926 <span class="preprocessor"></span>
<a name="l00927"></a>00927     <span class="keywordflow">if</span> (with_size)
<a name="l00928"></a>00928       {
<a name="l00929"></a>00929         <span class="keywordtype">int</span> new_size;
<a name="l00930"></a>00930         stream.read(reinterpret_cast&lt;char*&gt;(&amp;new_size), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00931"></a>00931         this-&gt;Reallocate(new_size);
<a name="l00932"></a>00932       }
<a name="l00933"></a>00933 
<a name="l00934"></a>00934     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; GetLength(); i++)
<a name="l00935"></a>00935       {
<a name="l00936"></a>00936         <span class="keywordflow">if</span> (with_size)
<a name="l00937"></a>00937           {
<a name="l00938"></a>00938             <span class="keywordtype">int</span> new_size;
<a name="l00939"></a>00939             stream.read(reinterpret_cast&lt;char*&gt;(&amp;new_size), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00940"></a>00940             this-&gt;Reallocate(i, new_size);
<a name="l00941"></a>00941           }
<a name="l00942"></a>00942         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; GetLength(i); j++)
<a name="l00943"></a>00943           data_(i)(j).Read(stream, with_size);
<a name="l00944"></a>00944       }
<a name="l00945"></a>00945 
<a name="l00946"></a>00946 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00947"></a>00947 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l00948"></a>00948     <span class="keywordflow">if</span> (!stream.good())
<a name="l00949"></a>00949       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector3::Read(istream&amp; stream, bool with_size)&quot;</span>,
<a name="l00950"></a>00950                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l00951"></a>00951 <span class="preprocessor">#endif</span>
<a name="l00952"></a>00952 <span class="preprocessor"></span>
<a name="l00953"></a>00953   }
<a name="l00954"></a>00954 
<a name="l00955"></a>00955 
<a name="l00956"></a>00956 } <span class="comment">// namespace Seldon.</span>
<a name="l00957"></a>00957 
<a name="l00958"></a>00958 
<a name="l00959"></a>00959 <span class="preprocessor">#define SELDON_FILE_VECTOR_VECTOR_3_CXX</span>
<a name="l00960"></a>00960 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
