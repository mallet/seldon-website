<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">MatrixSuperLU_Base</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::MatrixSuperLU_Base&lt; T &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::MatrixSuperLU_Base" -->
<p>class interfacing SuperLU functions  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_super_l_u_8hxx_source.php">SuperLU.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::MatrixSuperLU_Base&lt; T &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_matrix_super_l_u___base.png" usemap="#Seldon::MatrixSuperLU_Base&lt; T &gt;_map" alt=""/>
  <map id="Seldon::MatrixSuperLU_Base&lt; T &gt;_map" name="Seldon::MatrixSuperLU_Base&lt; T &gt;_map">
<area href="class_seldon_1_1_matrix_super_l_u.php" alt="Seldon::MatrixSuperLU&lt; T &gt;" shape="rect" coords="0,56,207,80"/>
</map>
</div>

<p><a href="class_seldon_1_1_matrix_super_l_u___base-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a526c7f0e29c86b81fc4705b437e167f9"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::MatrixSuperLU_Base" ref="a526c7f0e29c86b81fc4705b437e167f9" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a526c7f0e29c86b81fc4705b437e167f9">MatrixSuperLU_Base</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">default constructor <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8f0a2adf458ec4f187f3936fea612106"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::~MatrixSuperLU_Base" ref="a8f0a2adf458ec4f187f3936fea612106" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a8f0a2adf458ec4f187f3936fea612106">~MatrixSuperLU_Base</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">destructor <br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Prop , class Allocator &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a6492cdcde57b8524db187be6f4602ef5">GetLU</a> (<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; double, Prop, <a class="el" href="class_seldon_1_1_col_sparse.php">ColSparse</a>, Allocator &gt; &amp;Lmat, <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; double, Prop, <a class="el" href="class_seldon_1_1_col_sparse.php">ColSparse</a>, Allocator &gt; &amp;Umat, bool permuted=true)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the LU factorization.  <a href="#a6492cdcde57b8524db187be6f4602ef5"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Prop , class Allocator &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a7713db1a177053fdee0cd121411ae986">GetLU</a> (<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; double, Prop, <a class="el" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator &gt; &amp;Lmat, <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; double, Prop, <a class="el" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator &gt; &amp;Umat, bool permuted=true)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the LU factorization.  <a href="#a7713db1a177053fdee0cd121411ae986"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a27aca5955b9d58ab561f652f153139ee">GetRowPermutation</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the permutation of rows.  <a href="#a27aca5955b9d58ab561f652f153139ee"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#acf6c8120e481e56c99e77bea79b30f84">GetColPermutation</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the permutation of columns.  <a href="#acf6c8120e481e56c99e77bea79b30f84"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a963e0bcc739b0930fda1b929ab178f4c"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::SelectOrdering" ref="a963e0bcc739b0930fda1b929ab178f4c" args="(colperm_t type)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SelectOrdering</b> (colperm_t type)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1f4b2081a071dd460addbd6e76121911"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::SetPermutation" ref="a1f4b2081a071dd460addbd6e76121911" args="(const IVect &amp;)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetPermutation</b> (const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acac4d12b1d9e464f630b7d56b2c61a23"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::Clear" ref="acac4d12b1d9e464f630b7d56b2c61a23" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#acac4d12b1d9e464f630b7d56b2c61a23">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">same effect as a call to the destructor <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a733cd4e38035ae701435f9f298442e4d"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::HideMessages" ref="a733cd4e38035ae701435f9f298442e4d" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a733cd4e38035ae701435f9f298442e4d">HideMessages</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">no message from SuperLU <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af5322d1293240d5d3ab6aeb93183acc7"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::ShowMessages" ref="af5322d1293240d5d3ab6aeb93183acc7" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#af5322d1293240d5d3ab6aeb93183acc7">ShowMessages</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">allows messages from SuperLU <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1f4cc492e099fa1bc8a608bac33be675"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::GetInfoFactorization" ref="a1f4cc492e099fa1bc8a608bac33be675" args="() const " -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetInfoFactorization</b> () const </td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aee03c768f0dde6badc86532125c9c232"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::L" ref="aee03c768f0dde6badc86532125c9c232" args="" -->
SuperMatrix&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#aee03c768f0dde6badc86532125c9c232">L</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">objects of SuperLU <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6df92cbf9ee7241547d6ae7a35db4d27"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::U" ref="a6df92cbf9ee7241547d6ae7a35db4d27" args="" -->
SuperMatrix&nbsp;</td><td class="memItemRight" valign="bottom"><b>U</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af4c5370a21280071538dc643dc97ab32"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::B" ref="af4c5370a21280071538dc643dc97ab32" args="" -->
SuperMatrix&nbsp;</td><td class="memItemRight" valign="bottom"><b>B</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3639abafe150759f173dc6d6a654c534"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::Lstore" ref="a3639abafe150759f173dc6d6a654c534" args="" -->
SCformat *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a3639abafe150759f173dc6d6a654c534">Lstore</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">object of SuperLU <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3c0030723c06e7bffc470c94b344c00e"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::Ustore" ref="a3c0030723c06e7bffc470c94b344c00e" args="" -->
NCformat *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a3c0030723c06e7bffc470c94b344c00e">Ustore</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">object of SuperLU <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a51a947858f30367b812df38f97ec3aa4"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::stat" ref="a51a947858f30367b812df38f97ec3aa4" args="" -->
SuperLUStat_t&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a51a947858f30367b812df38f97ec3aa4">stat</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">statistics <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3de025ec4fdc83b5c793d69becc051c1"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::options" ref="a3de025ec4fdc83b5c793d69becc051c1" args="" -->
superlu_options_t&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a3de025ec4fdc83b5c793d69becc051c1">options</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">options //! permutation array <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acf6f26139e9e0ec850dba1aba8fa749e"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::perm_r" ref="acf6f26139e9e0ec850dba1aba8fa749e" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>perm_r</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1dcdbdbf293c502b64485ddfdb711c2e"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::perm_c" ref="a1dcdbdbf293c502b64485ddfdb711c2e" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>perm_c</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7148d98444f507fdadd0127a2ba0b7ad"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::permc_spec" ref="a7148d98444f507fdadd0127a2ba0b7ad" args="" -->
colperm_t&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a7148d98444f507fdadd0127a2ba0b7ad">permc_spec</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">ordering scheme <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3a2831b9dac4fa42c7815015627f5f38"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::n" ref="a3a2831b9dac4fa42c7815015627f5f38" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a3a2831b9dac4fa42c7815015627f5f38">n</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">number of rows <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5b067c2c8ef0a0fcbfec31d399fd2bc3"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::display_info" ref="a5b067c2c8ef0a0fcbfec31d399fd2bc3" args="" -->
bool&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a5b067c2c8ef0a0fcbfec31d399fd2bc3">display_info</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">display information about factorization ? //! <a class="el" href="class_seldon_1_1_error.php">Error</a> code returned by SuperLU. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0b6e4ede47b67d446e540ed1376d10db"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::info_facto" ref="a0b6e4ede47b67d446e540ed1376d10db" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>info_facto</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T&gt;<br/>
 class Seldon::MatrixSuperLU_Base&lt; T &gt;</h3>

<p>class interfacing SuperLU functions </p>

<p>Definition at line <a class="el" href="_super_l_u_8hxx_source.php#l00033">33</a> of file <a class="el" href="_super_l_u_8hxx_source.php">SuperLU.hxx</a>.</p>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="acf6c8120e481e56c99e77bea79b30f84"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::GetColPermutation" ref="acf6c8120e481e56c99e77bea79b30f84" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt; &amp; <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base</a>&lt; T &gt;::GetColPermutation </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the permutation of columns. </p>
<p>In order to retain the sparsity as much as possible, SuperLU permutes rows and columns before the factorization. This method returns the column permutation that was employed in the factorization. This method is obviously to be called after the factorization has been performed. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The permutation of the columns. </dd></dl>

<p>Definition at line <a class="el" href="_super_l_u_8cxx_source.php#l00230">230</a> of file <a class="el" href="_super_l_u_8cxx_source.php">SuperLU.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7713db1a177053fdee0cd121411ae986"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::GetLU" ref="a7713db1a177053fdee0cd121411ae986" args="(Matrix&lt; double, Prop, RowSparse, Allocator &gt; &amp;Lmat, Matrix&lt; double, Prop, RowSparse, Allocator &gt; &amp;Umat, bool permuted=true)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T &gt; </div>
<div class="memtemplate">
template&lt;class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base</a>&lt; T &gt;::GetLU </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; double, Prop, <a class="el" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>Lmat</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; double, Prop, <a class="el" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>Umat</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>permuted</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the LU factorization. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[out]</tt>&nbsp;</td><td valign="top"><em>Lmat</em>&nbsp;</td><td>matrix L in the LU factorization. </td></tr>
    <tr><td valign="top"><tt>[out]</tt>&nbsp;</td><td valign="top"><em>Umat</em>&nbsp;</td><td>matrix U in the LU factorization. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>permuted</em>&nbsp;</td><td>should the permuted matrices be provided? SuperLU permutes the rows and columns of the factorized matrix. If <em>permuted</em> is set to true, L and U are returned as SuperLU computed them, hence with permuted rows and columns. If <em>permuted</em> is set to false, the matrices L and U are "unpermuted" so that L times U is equal to the initial matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>This method will first retrieve the L and U matrices in 'ColSparse' format and then convert them into 'RowSparse'. </dd></dl>

<p>Definition at line <a class="el" href="_super_l_u_8cxx_source.php#l00190">190</a> of file <a class="el" href="_super_l_u_8cxx_source.php">SuperLU.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6492cdcde57b8524db187be6f4602ef5"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::GetLU" ref="a6492cdcde57b8524db187be6f4602ef5" args="(Matrix&lt; double, Prop, ColSparse, Allocator &gt; &amp;Lmat, Matrix&lt; double, Prop, ColSparse, Allocator &gt; &amp;Umat, bool permuted=true)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T &gt; </div>
<div class="memtemplate">
template&lt;class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base</a>&lt; T &gt;::GetLU </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; double, Prop, <a class="el" href="class_seldon_1_1_col_sparse.php">ColSparse</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>Lmat</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; double, Prop, <a class="el" href="class_seldon_1_1_col_sparse.php">ColSparse</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>Umat</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>permuted</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the LU factorization. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[out]</tt>&nbsp;</td><td valign="top"><em>Lmat</em>&nbsp;</td><td>matrix L in the LU factorization. </td></tr>
    <tr><td valign="top"><tt>[out]</tt>&nbsp;</td><td valign="top"><em>Umat</em>&nbsp;</td><td>matrix U in the LU factorization. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>permuted</em>&nbsp;</td><td>should the permuted matrices be provided? SuperLU permutes the rows and columns of the factorized matrix. If <em>permuted</em> is set to true, L and U are returned as SuperLU computed them, hence with permuted rows and columns. If <em>permuted</em> is set to false, the matrices L and U are "unpermuted" so that L times U is equal to the initial matrix. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_super_l_u_8cxx_source.php#l00127">127</a> of file <a class="el" href="_super_l_u_8cxx_source.php">SuperLU.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a27aca5955b9d58ab561f652f153139ee"></a><!-- doxytag: member="Seldon::MatrixSuperLU_Base::GetRowPermutation" ref="a27aca5955b9d58ab561f652f153139ee" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt; &amp; <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base</a>&lt; T &gt;::GetRowPermutation </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the permutation of rows. </p>
<p>In order to retain the sparsity as much as possible, SuperLU permutes rows and columns before the factorization. This method returns the row permutation that was employed in the factorization. This method is obviously to be called after the factorization has been performed. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The permutation of the rows. </dd></dl>

<p>Definition at line <a class="el" href="_super_l_u_8cxx_source.php#l00216">216</a> of file <a class="el" href="_super_l_u_8cxx_source.php">SuperLU.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>computation/interfaces/direct/<a class="el" href="_super_l_u_8hxx_source.php">SuperLU.hxx</a></li>
<li>computation/interfaces/direct/<a class="el" href="_super_l_u_8cxx_source.php">SuperLU.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
