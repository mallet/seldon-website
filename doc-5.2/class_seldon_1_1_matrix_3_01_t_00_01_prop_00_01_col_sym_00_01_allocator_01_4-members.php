<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt;</a>, including all inherited members.<table>
  <tr bgcolor="#f0f0f0"><td><b>access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>allocator</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>allocator_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a0ab7b853cb5e827503be5e65f2b39150">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a94f5e47677cc13311ab33879d2b05774">Copy</a>(const Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>data_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>entry_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a9d34c8b60b6e577504a4264990194805">Fill</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a966bcf7f7c204b9a8e2130ca93db0757">Fill</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a01a990a33e3e2b873b69587d8870d0c3">FillRand</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#af5312b65658c2a50f9c94a784940c1e0">Get</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a8b74b9c2fbd076cfb10e87a5c4812094">Get</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a8876d406208337c491b2e9d2f56de084">GetDataSize</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">GetM</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">GetN</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">GetSize</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>m_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php#abd70ad2d9d8d9da0d9e5210447aed290">Matrix</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php#aa3eb2f4294b73154b78971e46213e527">Matrix</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a47988d7972247286332e853c13bbc00b">Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ad2ea11b0880dc32ba9472da9310c332b">Matrix_Base</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline, explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a127e0229931486cea3e6007988b446a0">Matrix_Base</a>(const Matrix_Base&lt; T, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a82951e97ce120fda463beebe44d9c5e3">Matrix_Symmetric</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#ab4c39b6e45f2f22dbf8dd50ad8509ad7">Matrix_Symmetric</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a8d0b954996edc9ccf67c46eb8ed05ad2">Matrix_Symmetric</a>(const Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>me_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>n_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#ae36123ec57e9423228e4815604ce2d5c">Nullify</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a8a5cd6bfbb855e65a5e585e9e62c72f1">operator()</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#ab3bf006ebfa36eeafc3bd0b7a57f9d89">operator()</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php#aec3d6da476f91a8d3b6033e835a3ffe9">operator*=</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php#a02437899b3e29faaf714a28429b66de2">operator=</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php#a14d0e6fbd800e1d87cb7339f9835f5fe">operator=</a>(const Matrix&lt; T, Prop, ColSym, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#af554df9f69fd7674842120a4718f609b">Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;::operator=</a>(const Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#aead099f326217ddb05c0782e58b0b1a5">operator[]</a>(int i)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a674815a3c75a714b790ee7dec3a46b4b">operator[]</a>(int i) const</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a018e68da0aa00e2a3bb7d5b989b77837">Print</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a424f412164caf3674f49d4c30dbb9606">Print</a>(int a, int b, int m, int n) const</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#ac03b20710ef1613894a73f8d10b6154c">Print</a>(int l) const</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>property</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#af4e2d06a1044dd2c449384c580b347cd">Read</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a911a42c752de06caed9614fed95bb8b5">Read</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a9ae2c1b1f8f80c87801b92dfc9a9a885">ReadText</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a72ba96f845054a5be5120c7a175c00c1">ReadText</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a488c4c92d4c5f2e867a8f008c7ba6e5d">Reallocate</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a2369e0b64027be77dd3ba9e3b77e4971">Resize</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a66c6a02ed5d50cbf204f7bd145d13880">Set</a>(int i, int j, const T &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SetData</b>(int i, int j, pointer data) (defined in <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a7cab27ba8e4ee1d46cb8d5eca3123d42">SetIdentity</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>storage</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a140aaa512f551fa698f84fc05c8df4c8">Val</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a1db21879907e925a20a58ad332a98446">Val</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>value_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a0af8f548546cddebba11f07e7f55f4d3">Write</a>(string FileName) const</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a89eb858308a323e175f01cd6f1b70e42">Write</a>(ostream &amp;FileStream) const</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#aaad47afa4f62b374a14e37d896a677a8">WriteText</a>(string FileName) const</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a3ca6951c93777f346d79e8bdd227ea0e">WriteText</a>(ostream &amp;FileStream) const</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a73066da717f1163947c11658fc7cba31">Zero</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af34a03c0cc56f757a83cd56f97c74ed8">~Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php#ae3c7bcbdfae4e4240505bd3ad72df274">~Matrix_Symmetric</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric&lt; T, Prop, ColSym, Allocator &gt;</a></td><td></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
