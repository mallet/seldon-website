<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/solver/SparseSolver.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2011 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2010-2011 Vivien Mallet</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_COMPUTATION_SPARSESOLVER_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span><span class="preprocessor">#define SELDON_FILE_COMPUTATION_SPARSESOLVER_CXX</span>
<a name="l00023"></a>00023 <span class="preprocessor"></span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="preprocessor">#include &quot;Ordering.cxx&quot;</span>
<a name="l00026"></a>00026 <span class="preprocessor">#include &quot;SparseSolver.hxx&quot;</span>
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 
<a name="l00029"></a>00029 <span class="preprocessor">#ifdef SELDON_WITH_UMFPACK</span>
<a name="l00030"></a>00030 <span class="preprocessor"></span><span class="preprocessor">#include &quot;camd.h&quot;</span>
<a name="l00031"></a>00031 <span class="preprocessor">#include &quot;colamd.h&quot;</span>
<a name="l00032"></a>00032 <span class="preprocessor">#endif</span>
<a name="l00033"></a>00033 <span class="preprocessor"></span>
<a name="l00034"></a>00034 <span class="keyword">namespace </span>Seldon
<a name="l00035"></a>00035 {
<a name="l00036"></a>00036 
<a name="l00037"></a>00037 
<a name="l00038"></a>00038   <span class="comment">/*************************</span>
<a name="l00039"></a>00039 <span class="comment">   * Default Seldon solver *</span>
<a name="l00040"></a>00040 <span class="comment">   *************************/</span>
<a name="l00041"></a>00041 
<a name="l00042"></a>00042 
<a name="l00043"></a>00043   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00044"></a>00044   SparseSeldonSolver&lt;T, Allocator&gt;::SparseSeldonSolver()
<a name="l00045"></a>00045   {
<a name="l00046"></a>00046     <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a877304542a5dfbcb2a09a308477627b6" title="Verbosity level.">print_level</a> = -1;
<a name="l00047"></a>00047     symmetric_matrix = <span class="keyword">false</span>;
<a name="l00048"></a>00048     permtol = 0.1;
<a name="l00049"></a>00049   }
<a name="l00050"></a>00050 
<a name="l00051"></a>00051 
<a name="l00052"></a>00052   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00053"></a>00053   <span class="keywordtype">void</span> SparseSeldonSolver&lt;T, Allocator&gt;::Clear()
<a name="l00054"></a>00054   {
<a name="l00055"></a>00055     <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a9e491c5c484a7e8b4d0cae095d4be34c" title="Cholesky factors.">mat_sym</a>.Clear();
<a name="l00056"></a>00056     mat_unsym.Clear();
<a name="l00057"></a>00057   }
<a name="l00058"></a>00058 
<a name="l00059"></a>00059 
<a name="l00060"></a>00060   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00061"></a>00061   <span class="keywordtype">void</span> SparseSeldonSolver&lt;T, Allocator&gt;::HideMessages()
<a name="l00062"></a>00062   {
<a name="l00063"></a>00063     <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a877304542a5dfbcb2a09a308477627b6" title="Verbosity level.">print_level</a> = -1;
<a name="l00064"></a>00064   }
<a name="l00065"></a>00065 
<a name="l00066"></a>00066 
<a name="l00067"></a>00067   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00068"></a>00068   <span class="keywordtype">void</span> SparseSeldonSolver&lt;T, Allocator&gt;::ShowMessages()
<a name="l00069"></a>00069   {
<a name="l00070"></a>00070     <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a877304542a5dfbcb2a09a308477627b6" title="Verbosity level.">print_level</a> = 1;
<a name="l00071"></a>00071   }
<a name="l00072"></a>00072 
<a name="l00073"></a>00073 
<a name="l00074"></a>00074   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00075"></a>00075   <span class="keywordtype">double</span> SparseSeldonSolver&lt;T, Allocator&gt;::GetPivotThreshold()<span class="keyword"> const</span>
<a name="l00076"></a>00076 <span class="keyword">  </span>{
<a name="l00077"></a>00077     <span class="keywordflow">return</span> permtol;
<a name="l00078"></a>00078   }
<a name="l00079"></a>00079 
<a name="l00080"></a>00080 
<a name="l00081"></a>00081   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00082"></a>00082   <span class="keywordtype">void</span> SparseSeldonSolver&lt;T, Allocator&gt;::SetPivotThreshold(<span class="keyword">const</span> <span class="keywordtype">double</span>&amp; a)
<a name="l00083"></a>00083   {
<a name="l00084"></a>00084     permtol = a;
<a name="l00085"></a>00085   }
<a name="l00086"></a>00086 
<a name="l00087"></a>00087 
<a name="l00088"></a>00088   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00089"></a>00089   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Storage0, <span class="keyword">class</span> Allocator0&gt;
<a name="l00090"></a>00090   <span class="keywordtype">void</span> SparseSeldonSolver&lt;T, Allocator&gt;::
<a name="l00091"></a>00091   FactorizeMatrix(<span class="keyword">const</span> IVect&amp; perm,
<a name="l00092"></a>00092                   Matrix&lt;T0, General, Storage0, Allocator0&gt;&amp; mat,
<a name="l00093"></a>00093                   <span class="keywordtype">bool</span> keep_matrix)
<a name="l00094"></a>00094   {
<a name="l00095"></a>00095     IVect inv_permutation;
<a name="l00096"></a>00096 
<a name="l00097"></a>00097     <span class="comment">// We convert matrix to unsymmetric format.</span>
<a name="l00098"></a>00098     Copy(mat, mat_unsym);
<a name="l00099"></a>00099 
<a name="l00100"></a>00100     <span class="comment">// Old matrix is erased if needed.</span>
<a name="l00101"></a>00101     <span class="keywordflow">if</span> (!keep_matrix)
<a name="l00102"></a>00102       mat.Clear();
<a name="l00103"></a>00103 
<a name="l00104"></a>00104     <span class="comment">// We keep permutation array in memory, and check it.</span>
<a name="l00105"></a>00105     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#aae8e798c85e35d0225f5e0ec9f2d4c2f" title="Size of factorized linear system.">n</a> = mat_unsym.GetM();
<a name="l00106"></a>00106     <span class="keywordflow">if</span> (perm.GetM() != n)
<a name="l00107"></a>00107       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;FactorizeMatrix(IVect&amp;, Matrix&amp;, bool)&quot;</span>,
<a name="l00108"></a>00108                           <span class="stringliteral">&quot;Numbering array is of size &quot;</span>
<a name="l00109"></a>00109                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(perm.GetM())
<a name="l00110"></a>00110                           + <span class="stringliteral">&quot; while the matrix is of size &quot;</span>
<a name="l00111"></a>00111                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(mat.GetM()) + <span class="stringliteral">&quot; x &quot;</span>
<a name="l00112"></a>00112                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(mat.GetN()) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00113"></a>00113 
<a name="l00114"></a>00114     permutation_row.Reallocate(n);
<a name="l00115"></a>00115     permutation_col.Reallocate(n);
<a name="l00116"></a>00116     inv_permutation.Reallocate(n);
<a name="l00117"></a>00117     inv_permutation.Fill(-1);
<a name="l00118"></a>00118     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; n; i++)
<a name="l00119"></a>00119       {
<a name="l00120"></a>00120         permutation_row(i) = i;
<a name="l00121"></a>00121         permutation_col(i) = i;
<a name="l00122"></a>00122         inv_permutation(perm(i)) = i;
<a name="l00123"></a>00123       }
<a name="l00124"></a>00124 
<a name="l00125"></a>00125     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; n; i++)
<a name="l00126"></a>00126       <span class="keywordflow">if</span> (inv_permutation(i) == -1)
<a name="l00127"></a>00127         <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;FactorizeMatrix(IVect&amp;, Matrix&amp;, bool)&quot;</span>,
<a name="l00128"></a>00128                             <span class="stringliteral">&quot;The numbering array is invalid.&quot;</span>);
<a name="l00129"></a>00129 
<a name="l00130"></a>00130     IVect iperm = inv_permutation;
<a name="l00131"></a>00131 
<a name="l00132"></a>00132     <span class="comment">// Rows of matrix are permuted.</span>
<a name="l00133"></a>00133     <a class="code" href="namespace_seldon.php#a48edc9483ed12de114cfde40b9bed96e" title="Inverse permutation of a general matrix stored by rows.">ApplyInversePermutation</a>(mat_unsym, perm, perm);
<a name="l00134"></a>00134 
<a name="l00135"></a>00135     <span class="comment">// Temporary vector used for solving.</span>
<a name="l00136"></a>00136     <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#af5e2e3beb8f4ab7713e4729b3e7f86ac" title="Temporary vector.">xtmp</a>.Reallocate(n);
<a name="l00137"></a>00137 
<a name="l00138"></a>00138     <span class="comment">// Factorization is performed.</span>
<a name="l00139"></a>00139     <span class="comment">// Columns are permuted during the factorization.</span>
<a name="l00140"></a>00140     symmetric_matrix = <span class="keyword">false</span>;
<a name="l00141"></a>00141     inv_permutation.Fill();
<a name="l00142"></a>00142     <a class="code" href="namespace_seldon.php#a76d3f187a877c6c58245b0716e1adc00" title="Returns the LU factorization of a matrix.">GetLU</a>(mat_unsym, permutation_col, inv_permutation, permtol, <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a877304542a5dfbcb2a09a308477627b6" title="Verbosity level.">print_level</a>);
<a name="l00143"></a>00143 
<a name="l00144"></a>00144     <span class="comment">// Combining permutations.</span>
<a name="l00145"></a>00145     IVect itmp = permutation_col;
<a name="l00146"></a>00146     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; n; i++)
<a name="l00147"></a>00147       permutation_col(i) = iperm(itmp(i));
<a name="l00148"></a>00148 
<a name="l00149"></a>00149     permutation_row = perm;
<a name="l00150"></a>00150 
<a name="l00151"></a>00151   }
<a name="l00152"></a>00152 
<a name="l00153"></a>00153 
<a name="l00154"></a>00154   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00155"></a>00155   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Storage0, <span class="keyword">class</span> Allocator0&gt;
<a name="l00156"></a>00156   <span class="keywordtype">void</span> SparseSeldonSolver&lt;T, Allocator&gt;::
<a name="l00157"></a>00157   FactorizeMatrix(<span class="keyword">const</span> IVect&amp; perm,
<a name="l00158"></a>00158                   Matrix&lt;T0, Symmetric, Storage0, Allocator0&gt;&amp; mat,
<a name="l00159"></a>00159                   <span class="keywordtype">bool</span> keep_matrix)
<a name="l00160"></a>00160   {
<a name="l00161"></a>00161     IVect inv_permutation;
<a name="l00162"></a>00162 
<a name="l00163"></a>00163     <span class="comment">// We convert matrix to symmetric format.</span>
<a name="l00164"></a>00164     Copy(mat, <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a9e491c5c484a7e8b4d0cae095d4be34c" title="Cholesky factors.">mat_sym</a>);
<a name="l00165"></a>00165 
<a name="l00166"></a>00166     <span class="comment">// Old matrix is erased if needed.</span>
<a name="l00167"></a>00167     <span class="keywordflow">if</span> (!keep_matrix)
<a name="l00168"></a>00168       mat.Clear();
<a name="l00169"></a>00169 
<a name="l00170"></a>00170     <span class="comment">// We keep permutation array in memory, and check it.</span>
<a name="l00171"></a>00171     <span class="keywordtype">int</span> n = <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a9e491c5c484a7e8b4d0cae095d4be34c" title="Cholesky factors.">mat_sym</a>.GetM();
<a name="l00172"></a>00172     <span class="keywordflow">if</span> (perm.GetM() != n)
<a name="l00173"></a>00173       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;FactorizeMatrix(IVect&amp;, Matrix&amp;, bool)&quot;</span>,
<a name="l00174"></a>00174                           <span class="stringliteral">&quot;Numbering array is of size &quot;</span>
<a name="l00175"></a>00175                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(perm.GetM())
<a name="l00176"></a>00176                           + <span class="stringliteral">&quot; while the matrix is of size &quot;</span>
<a name="l00177"></a>00177                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(mat.GetM()) + <span class="stringliteral">&quot; x &quot;</span>
<a name="l00178"></a>00178                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(mat.GetN()) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00179"></a>00179 
<a name="l00180"></a>00180     permutation_row.Reallocate(n);
<a name="l00181"></a>00181     inv_permutation.Reallocate(n);
<a name="l00182"></a>00182     inv_permutation.Fill(-1);
<a name="l00183"></a>00183     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; n; i++)
<a name="l00184"></a>00184       {
<a name="l00185"></a>00185         permutation_row(i) = perm(i);
<a name="l00186"></a>00186         inv_permutation(perm(i)) = i;
<a name="l00187"></a>00187       }
<a name="l00188"></a>00188 
<a name="l00189"></a>00189     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; n; i++)
<a name="l00190"></a>00190       <span class="keywordflow">if</span> (inv_permutation(i) == -1)
<a name="l00191"></a>00191         <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;FactorizeMatrix(IVect&amp;, Matrix&amp;, bool)&quot;</span>,
<a name="l00192"></a>00192                             <span class="stringliteral">&quot;The numbering array is invalid.&quot;</span>);
<a name="l00193"></a>00193 
<a name="l00194"></a>00194     <span class="comment">// Matrix is permuted.</span>
<a name="l00195"></a>00195     <a class="code" href="namespace_seldon.php#a48edc9483ed12de114cfde40b9bed96e" title="Inverse permutation of a general matrix stored by rows.">ApplyInversePermutation</a>(<a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a9e491c5c484a7e8b4d0cae095d4be34c" title="Cholesky factors.">mat_sym</a>, perm, perm);
<a name="l00196"></a>00196 
<a name="l00197"></a>00197     <span class="comment">// Temporary vector used for solving.</span>
<a name="l00198"></a>00198     <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#af5e2e3beb8f4ab7713e4729b3e7f86ac" title="Temporary vector.">xtmp</a>.Reallocate(n);
<a name="l00199"></a>00199 
<a name="l00200"></a>00200     <span class="comment">// Factorization is performed.</span>
<a name="l00201"></a>00201     symmetric_matrix = <span class="keyword">true</span>;
<a name="l00202"></a>00202     <a class="code" href="namespace_seldon.php#a76d3f187a877c6c58245b0716e1adc00" title="Returns the LU factorization of a matrix.">GetLU</a>(<a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a9e491c5c484a7e8b4d0cae095d4be34c" title="Cholesky factors.">mat_sym</a>, <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a877304542a5dfbcb2a09a308477627b6" title="Verbosity level.">print_level</a>);
<a name="l00203"></a>00203   }
<a name="l00204"></a>00204 
<a name="l00205"></a>00205 
<a name="l00206"></a>00206   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> Vector1&gt;
<a name="l00207"></a>00207   <span class="keywordtype">void</span> SparseSeldonSolver&lt;T, Allocator&gt;::Solve(Vector1&amp; z)
<a name="l00208"></a>00208   {
<a name="l00209"></a>00209     <span class="keywordflow">if</span> (symmetric_matrix)
<a name="l00210"></a>00210       {
<a name="l00211"></a>00211         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; z.GetM(); i++)
<a name="l00212"></a>00212           <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#af5e2e3beb8f4ab7713e4729b3e7f86ac" title="Temporary vector.">xtmp</a>(permutation_row(i)) = z(i);
<a name="l00213"></a>00213 
<a name="l00214"></a>00214         <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(<a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#a9e491c5c484a7e8b4d0cae095d4be34c" title="Cholesky factors.">mat_sym</a>, <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#af5e2e3beb8f4ab7713e4729b3e7f86ac" title="Temporary vector.">xtmp</a>);
<a name="l00215"></a>00215 
<a name="l00216"></a>00216         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; z.GetM(); i++)
<a name="l00217"></a>00217           z(i) = <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#af5e2e3beb8f4ab7713e4729b3e7f86ac" title="Temporary vector.">xtmp</a>(permutation_row(i));
<a name="l00218"></a>00218       }
<a name="l00219"></a>00219     <span class="keywordflow">else</span>
<a name="l00220"></a>00220       {
<a name="l00221"></a>00221         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; z.GetM(); i++)
<a name="l00222"></a>00222           <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#af5e2e3beb8f4ab7713e4729b3e7f86ac" title="Temporary vector.">xtmp</a>(permutation_row(i)) = z(i);
<a name="l00223"></a>00223 
<a name="l00224"></a>00224         <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(mat_unsym, <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#af5e2e3beb8f4ab7713e4729b3e7f86ac" title="Temporary vector.">xtmp</a>);
<a name="l00225"></a>00225 
<a name="l00226"></a>00226         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; z.GetM(); i++)
<a name="l00227"></a>00227           z(permutation_col(i)) = <a class="code" href="class_seldon_1_1_sparse_cholesky_solver.php#af5e2e3beb8f4ab7713e4729b3e7f86ac" title="Temporary vector.">xtmp</a>(i);
<a name="l00228"></a>00228       }
<a name="l00229"></a>00229   }
<a name="l00230"></a>00230 
<a name="l00231"></a>00231 
<a name="l00232"></a>00232   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00233"></a>00233   <span class="keyword">template</span>&lt;<span class="keyword">class</span> TransStatus, <span class="keyword">class</span> Vector1&gt;
<a name="l00234"></a>00234   <span class="keywordtype">void</span> SparseSeldonSolver&lt;T, Allocator&gt;
<a name="l00235"></a>00235   ::Solve(<span class="keyword">const</span> TransStatus&amp; TransA, Vector1&amp; z)
<a name="l00236"></a>00236   {
<a name="l00237"></a>00237     <span class="keywordflow">if</span> (symmetric_matrix)
<a name="l00238"></a>00238       Solve(z);
<a name="l00239"></a>00239     <span class="keywordflow">else</span>
<a name="l00240"></a>00240       <span class="keywordflow">if</span> (TransA.Trans())
<a name="l00241"></a>00241         {
<a name="l00242"></a>00242           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; z.GetM(); i++)
<a name="l00243"></a>00243             xtmp(i) = z(permutation_col(i));
<a name="l00244"></a>00244 
<a name="l00245"></a>00245           <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(SeldonTrans, mat_unsym, xtmp);
<a name="l00246"></a>00246 
<a name="l00247"></a>00247           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; z.GetM(); i++)
<a name="l00248"></a>00248             z(i) = xtmp(permutation_row(i));
<a name="l00249"></a>00249         }
<a name="l00250"></a>00250       <span class="keywordflow">else</span>
<a name="l00251"></a>00251         Solve(z);
<a name="l00252"></a>00252   }
<a name="l00253"></a>00253 
<a name="l00254"></a><a class="code" href="namespace_seldon.php#adf37927849c543b2c3089490b0536580">00254</a> 
<a name="l00255"></a>00255   <span class="comment">/************************************************</span>
<a name="l00256"></a>00256 <span class="comment">   * GetLU and SolveLU used by SeldonSparseSolver *</span>
<a name="l00257"></a>00257 <span class="comment">   ************************************************/</span>
<a name="l00258"></a>00258 
<a name="l00259"></a>00259 
<a name="l00261"></a>00261   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00262"></a>00262   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a76d3f187a877c6c58245b0716e1adc00" title="Returns the LU factorization of a matrix.">GetLU</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, General, ArrayRowSparse, Allocator&gt;</a>&amp; A,
<a name="l00263"></a>00263              <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; iperm, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; rperm,
<a name="l00264"></a>00264              <span class="keywordtype">double</span> permtol, <span class="keywordtype">int</span> print_level)
<a name="l00265"></a>00265   {
<a name="l00266"></a>00266     <span class="keywordtype">int</span> n = A.GetN();
<a name="l00267"></a>00267 
<a name="l00268"></a>00268     T fact, s, t;
<a name="l00269"></a>00269     <span class="keywordtype">double</span> tnorm, zero = 0.0;
<a name="l00270"></a>00270     <span class="keywordtype">int</span> length_lower, length_upper, jpos, jrow, i_row, j_col;
<a name="l00271"></a>00271     <span class="keywordtype">int</span> i, j, k, length, size_row, index_lu;
<a name="l00272"></a>00272 
<a name="l00273"></a>00273     T czero, cone;
<a name="l00274"></a>00274     SetComplexZero(czero);
<a name="l00275"></a>00275     SetComplexOne(cone);
<a name="l00276"></a>00276     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> Row_Val(n);
<a name="l00277"></a>00277     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> Index(n), Row_Ind(n), Index_Diag(n);
<a name="l00278"></a>00278     Row_Val.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a722046c309ee8e50abd68a5a5a77aa60" title="Fills the vector with 0, 1, 2, ...">Fill</a>(czero);
<a name="l00279"></a>00279     Row_Ind.Fill(-1);
<a name="l00280"></a>00280     Index_Diag.Fill(-1);
<a name="l00281"></a>00281 
<a name="l00282"></a>00282     Index.Fill(-1);
<a name="l00283"></a>00283     <span class="comment">// main loop</span>
<a name="l00284"></a>00284     <span class="keywordtype">int</span> new_percent = 0, old_percent = 0;
<a name="l00285"></a>00285     <span class="keywordflow">for</span> (i_row = 0; i_row &lt; n; i_row++)
<a name="l00286"></a>00286       {
<a name="l00287"></a>00287         <span class="comment">// Progress bar if print level is high enough.</span>
<a name="l00288"></a>00288         <span class="keywordflow">if</span> (print_level &gt; 0)
<a name="l00289"></a>00289           {
<a name="l00290"></a>00290             new_percent = int(<span class="keywordtype">double</span>(i_row) / <span class="keywordtype">double</span>(n-1) * 78.);
<a name="l00291"></a>00291             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> percent = old_percent; percent &lt; new_percent; percent++)
<a name="l00292"></a>00292               {
<a name="l00293"></a>00293                 cout &lt;&lt; <span class="stringliteral">&quot;#&quot;</span>;
<a name="l00294"></a>00294                 cout.flush();
<a name="l00295"></a>00295               }
<a name="l00296"></a>00296             old_percent = new_percent;
<a name="l00297"></a>00297           }
<a name="l00298"></a>00298 
<a name="l00299"></a>00299         size_row = A.GetRowSize(i_row);
<a name="l00300"></a>00300         tnorm = zero;
<a name="l00301"></a>00301 
<a name="l00302"></a>00302         <span class="comment">// tnorm is the sum of absolute value of coefficients of row i_row.</span>
<a name="l00303"></a>00303         <span class="keywordflow">for</span> (k = 0 ; k &lt; size_row; k++)
<a name="l00304"></a>00304           <span class="keywordflow">if</span> (A.Value(i_row, k) != czero)
<a name="l00305"></a>00305             tnorm += abs(A.Value(i_row, k));
<a name="l00306"></a>00306 
<a name="l00307"></a>00307         <span class="keywordflow">if</span> (tnorm == zero)
<a name="l00308"></a>00308           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;GetLU(Matrix&lt;ArrayRowSparse&gt;&amp;, IVect&amp;, &quot;</span>
<a name="l00309"></a>00309                               <span class="stringliteral">&quot;IVect&amp;, double, int)&quot;</span>,
<a name="l00310"></a>00310                               <span class="stringliteral">&quot;The matrix is structurally singular. &quot;</span>
<a name="l00311"></a>00311                               <span class="stringliteral">&quot;The norm of row &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i_row)
<a name="l00312"></a>00312                               + <span class="stringliteral">&quot; is equal to 0.&quot;</span>);
<a name="l00313"></a>00313 
<a name="l00314"></a>00314         <span class="comment">// Unpack L-part and U-part of row of A.</span>
<a name="l00315"></a>00315         length_upper = 1;
<a name="l00316"></a>00316         length_lower = 0;
<a name="l00317"></a>00317         Row_Ind(i_row) = i_row;
<a name="l00318"></a>00318         Row_Val(i_row) = czero;
<a name="l00319"></a>00319         Index(i_row) = i_row;
<a name="l00320"></a>00320 
<a name="l00321"></a>00321         <span class="keywordflow">for</span> (j = 0; j &lt; size_row; j++)
<a name="l00322"></a>00322           {
<a name="l00323"></a>00323             k = rperm(A.Index(i_row, j));
<a name="l00324"></a>00324 
<a name="l00325"></a>00325             t = A.Value(i_row,j);
<a name="l00326"></a>00326             <span class="keywordflow">if</span> (k &lt; i_row)
<a name="l00327"></a>00327               {
<a name="l00328"></a>00328                 Row_Ind(length_lower) = k;
<a name="l00329"></a>00329                 Row_Val(length_lower) = t;
<a name="l00330"></a>00330                 Index(k) = length_lower;
<a name="l00331"></a>00331                 ++length_lower;
<a name="l00332"></a>00332               }
<a name="l00333"></a>00333             <span class="keywordflow">else</span> <span class="keywordflow">if</span> (k == i_row)
<a name="l00334"></a>00334               {
<a name="l00335"></a>00335                 Row_Val(i_row) = t;
<a name="l00336"></a>00336               }
<a name="l00337"></a>00337             <span class="keywordflow">else</span>
<a name="l00338"></a>00338               {
<a name="l00339"></a>00339                 jpos = i_row + length_upper;
<a name="l00340"></a>00340                 Row_Ind(jpos) = k;
<a name="l00341"></a>00341                 Row_Val(jpos) = t;
<a name="l00342"></a>00342                 Index(k) = jpos;
<a name="l00343"></a>00343                 length_upper++;
<a name="l00344"></a>00344               }
<a name="l00345"></a>00345           }
<a name="l00346"></a>00346 
<a name="l00347"></a>00347         j_col = 0;
<a name="l00348"></a>00348         length = 0;
<a name="l00349"></a>00349 
<a name="l00350"></a>00350         <span class="comment">// Eliminates previous rows.</span>
<a name="l00351"></a>00351         <span class="keywordflow">while</span> (j_col &lt; length_lower)
<a name="l00352"></a>00352           {
<a name="l00353"></a>00353             <span class="comment">// In order to do the elimination in the correct order, we must</span>
<a name="l00354"></a>00354             <span class="comment">// select the smallest column index.</span>
<a name="l00355"></a>00355             jrow = Row_Ind(j_col);
<a name="l00356"></a>00356             k = j_col;
<a name="l00357"></a>00357 
<a name="l00358"></a>00358             <span class="comment">// Determine smallest column index.</span>
<a name="l00359"></a>00359             <span class="keywordflow">for</span> (j = j_col + 1; j &lt; length_lower; j++)
<a name="l00360"></a>00360               <span class="keywordflow">if</span> (Row_Ind(j) &lt; jrow)
<a name="l00361"></a>00361                 {
<a name="l00362"></a>00362                   jrow = Row_Ind(j);
<a name="l00363"></a>00363                   k = j;
<a name="l00364"></a>00364                 }
<a name="l00365"></a>00365 
<a name="l00366"></a>00366             <span class="keywordflow">if</span> (k != j_col)
<a name="l00367"></a>00367               {
<a name="l00368"></a>00368                 <span class="comment">// Exchanging column coefficients.</span>
<a name="l00369"></a>00369                 j = Row_Ind(j_col);
<a name="l00370"></a>00370                 Row_Ind(j_col) = Row_Ind(k);
<a name="l00371"></a>00371                 Row_Ind(k) = j;
<a name="l00372"></a>00372 
<a name="l00373"></a>00373                 Index(jrow) = j_col;
<a name="l00374"></a>00374                 Index(j) = k;
<a name="l00375"></a>00375 
<a name="l00376"></a>00376                 s = Row_Val(j_col);
<a name="l00377"></a>00377                 Row_Val(j_col) = Row_Val(k);
<a name="l00378"></a>00378                 Row_Val(k) = s;
<a name="l00379"></a>00379               }
<a name="l00380"></a>00380 
<a name="l00381"></a>00381             <span class="comment">// Zero out element in row.</span>
<a name="l00382"></a>00382             Index(jrow) = -1;
<a name="l00383"></a>00383 
<a name="l00384"></a>00384             <span class="comment">// Gets the multiplier for row to be eliminated (jrow).</span>
<a name="l00385"></a>00385             <span class="comment">// first_index_upper points now on the diagonal coefficient.</span>
<a name="l00386"></a>00386             fact = Row_Val(j_col) * A.Value(jrow, Index_Diag(jrow));
<a name="l00387"></a>00387 
<a name="l00388"></a>00388             <span class="comment">// Combines current row and row jrow.</span>
<a name="l00389"></a>00389             <span class="keywordflow">for</span> (k = (Index_Diag(jrow)+1); k &lt; A.GetRowSize(jrow); k++)
<a name="l00390"></a>00390               {
<a name="l00391"></a>00391                 s = fact * A.Value(jrow,k);
<a name="l00392"></a>00392                 j = rperm(A.Index(jrow,k));
<a name="l00393"></a>00393 
<a name="l00394"></a>00394                 jpos = Index(j);
<a name="l00395"></a>00395 
<a name="l00396"></a>00396                 <span class="keywordflow">if</span> (j &gt;= i_row)
<a name="l00397"></a>00397                   <span class="comment">// Dealing with upper part.</span>
<a name="l00398"></a>00398                   <span class="keywordflow">if</span> (jpos == -1)
<a name="l00399"></a>00399                     {
<a name="l00400"></a>00400                       <span class="comment">// This is a fill-in element.</span>
<a name="l00401"></a>00401                       i = i_row + length_upper;
<a name="l00402"></a>00402                       Row_Ind(i) = j;
<a name="l00403"></a>00403                       Index(j) = i;
<a name="l00404"></a>00404                       Row_Val(i) = -s;
<a name="l00405"></a>00405                       ++length_upper;
<a name="l00406"></a>00406                     }
<a name="l00407"></a>00407                   <span class="keywordflow">else</span>
<a name="l00408"></a>00408                     <span class="comment">// This is not a fill-in element.</span>
<a name="l00409"></a>00409                     Row_Val(jpos) -= s;
<a name="l00410"></a>00410                 <span class="keywordflow">else</span>
<a name="l00411"></a>00411                   <span class="comment">// Dealing  with lower part.</span>
<a name="l00412"></a>00412                   <span class="keywordflow">if</span> (jpos == -1)
<a name="l00413"></a>00413                     {
<a name="l00414"></a>00414                       <span class="comment">// this is a fill-in element</span>
<a name="l00415"></a>00415                       Row_Ind(length_lower) = j;
<a name="l00416"></a>00416                       Index(j) = length_lower;
<a name="l00417"></a>00417                       Row_Val(length_lower) = -s;
<a name="l00418"></a>00418                       ++length_lower;
<a name="l00419"></a>00419                     }
<a name="l00420"></a>00420                   <span class="keywordflow">else</span>
<a name="l00421"></a>00421                     <span class="comment">// This is not a fill-in element.</span>
<a name="l00422"></a>00422                     Row_Val(jpos) -= s;
<a name="l00423"></a>00423               }
<a name="l00424"></a>00424 
<a name="l00425"></a>00425             <span class="comment">// Stores this pivot element from left to right -- no danger</span>
<a name="l00426"></a>00426             <span class="comment">// of overlap with the working elements in L (pivots).</span>
<a name="l00427"></a>00427             Row_Val(length) = fact;
<a name="l00428"></a>00428             Row_Ind(length) = jrow;
<a name="l00429"></a>00429             ++length;
<a name="l00430"></a>00430             j_col++;
<a name="l00431"></a>00431           }
<a name="l00432"></a>00432 
<a name="l00433"></a>00433         <span class="comment">// Resets double-pointer to zero (U-part).</span>
<a name="l00434"></a>00434         <span class="keywordflow">for</span> (k = 0; k &lt; length_upper; k++)
<a name="l00435"></a>00435           Index(Row_Ind(i_row + k)) = -1;
<a name="l00436"></a>00436 
<a name="l00437"></a>00437         size_row = length;
<a name="l00438"></a>00438         A.ReallocateRow(i_row,size_row);
<a name="l00439"></a>00439 
<a name="l00440"></a>00440         <span class="comment">// store L-part</span>
<a name="l00441"></a>00441         index_lu = 0;
<a name="l00442"></a>00442         <span class="keywordflow">for</span> (k = 0 ; k &lt; length ; k++)
<a name="l00443"></a>00443           {
<a name="l00444"></a>00444             A.Value(i_row,index_lu) = Row_Val(k);
<a name="l00445"></a>00445             A.Index(i_row,index_lu) = iperm(Row_Ind(k));
<a name="l00446"></a>00446             ++index_lu;
<a name="l00447"></a>00447           }
<a name="l00448"></a>00448 
<a name="l00449"></a>00449         <span class="comment">// Saves pointer to beginning of row i_row of U.</span>
<a name="l00450"></a>00450         Index_Diag(i_row) = index_lu;
<a name="l00451"></a>00451 
<a name="l00452"></a>00452         <span class="comment">// Updates. U-matrix -- first apply dropping strategy.</span>
<a name="l00453"></a>00453         length = 0;
<a name="l00454"></a>00454         <span class="keywordflow">for</span> (k = 1; k &lt;= (length_upper-1); k++)
<a name="l00455"></a>00455           {
<a name="l00456"></a>00456             ++length;
<a name="l00457"></a>00457             Row_Val(i_row+length) = Row_Val(i_row+k);
<a name="l00458"></a>00458             Row_Ind(i_row+length) = Row_Ind(i_row+k);
<a name="l00459"></a>00459           }
<a name="l00460"></a>00460 
<a name="l00461"></a>00461         length++;
<a name="l00462"></a>00462 
<a name="l00463"></a>00463         <span class="comment">// Determines next pivot.</span>
<a name="l00464"></a>00464         <span class="keywordtype">int</span> imax = i_row;
<a name="l00465"></a>00465         <span class="keywordtype">double</span> xmax = abs(Row_Val(imax));
<a name="l00466"></a>00466         <span class="keywordtype">double</span> xmax0 = xmax;
<a name="l00467"></a>00467         <span class="keywordflow">for</span> (k = i_row + 1; k &lt;= i_row + length - 1; k++)
<a name="l00468"></a>00468           {
<a name="l00469"></a>00469             tnorm = abs(Row_Val(k));
<a name="l00470"></a>00470             <span class="keywordflow">if</span> (tnorm &gt; xmax &amp;&amp; tnorm * permtol &gt; xmax0)
<a name="l00471"></a>00471               {
<a name="l00472"></a>00472                 imax = k;
<a name="l00473"></a>00473                 xmax = tnorm;
<a name="l00474"></a>00474               }
<a name="l00475"></a>00475           }
<a name="l00476"></a>00476 
<a name="l00477"></a>00477         <span class="comment">// Exchanges Row_Val.</span>
<a name="l00478"></a>00478         s = Row_Val(i_row);
<a name="l00479"></a>00479         Row_Val(i_row) = Row_Val(imax);
<a name="l00480"></a>00480         Row_Val(imax) = s;
<a name="l00481"></a>00481 
<a name="l00482"></a>00482         <span class="comment">// Updates iperm and reverses iperm.</span>
<a name="l00483"></a>00483         j = Row_Ind(imax);
<a name="l00484"></a>00484         i = iperm(i_row);
<a name="l00485"></a>00485         iperm(i_row) = iperm(j);
<a name="l00486"></a>00486         iperm(j) = i;
<a name="l00487"></a>00487 
<a name="l00488"></a>00488         <span class="comment">// Reverses iperm.</span>
<a name="l00489"></a>00489         rperm(iperm(i_row)) = i_row;
<a name="l00490"></a>00490         rperm(iperm(j)) = j;
<a name="l00491"></a>00491 
<a name="l00492"></a>00492         <span class="comment">// Copies U-part in original coordinates.</span>
<a name="l00493"></a>00493         <span class="keywordtype">int</span> index_diag = index_lu;
<a name="l00494"></a>00494         A.ResizeRow(i_row, size_row+length);
<a name="l00495"></a>00495 
<a name="l00496"></a>00496         <span class="keywordflow">for</span> (k = i_row ; k &lt;= i_row + length - 1; k++)
<a name="l00497"></a>00497           {
<a name="l00498"></a>00498             A.Index(i_row,index_lu) = iperm(Row_Ind(k));
<a name="l00499"></a>00499             A.Value(i_row,index_lu) = Row_Val(k);
<a name="l00500"></a>00500             ++index_lu;
<a name="l00501"></a>00501           }
<a name="l00502"></a>00502 
<a name="l00503"></a>00503         <span class="comment">// Stores inverse of diagonal element of u.</span>
<a name="l00504"></a>00504         A.Value(i_row, index_diag) = cone / Row_Val(i_row);
<a name="l00505"></a>00505 
<a name="l00506"></a>00506       } <span class="comment">// end main loop</span>
<a name="l00507"></a>00507 
<a name="l00508"></a>00508     <span class="keywordflow">if</span> (print_level &gt; 0)
<a name="l00509"></a>00509       cout &lt;&lt; endl;
<a name="l00510"></a>00510 
<a name="l00511"></a>00511     <span class="keywordflow">if</span> (print_level &gt; 0)
<a name="l00512"></a>00512       cout &lt;&lt; <span class="stringliteral">&quot;The matrix takes &quot;</span> &lt;&lt;
<a name="l00513"></a>00513         int((A.GetDataSize() * (<span class="keyword">sizeof</span>(T) + 4)) / (1024. * 1024.))
<a name="l00514"></a>00514            &lt;&lt; <span class="stringliteral">&quot; MB&quot;</span> &lt;&lt; endl;
<a name="l00515"></a>00515 
<a name="l00516"></a>00516     <span class="keywordflow">for</span> (i = 0; i &lt; n; i++ )
<a name="l00517"></a>00517       <span class="keywordflow">for</span> (j = 0; j &lt; A.GetRowSize(i); j++)
<a name="l00518"></a>00518         A.Index(i,j) = rperm(A.Index(i,j));
<a name="l00519"></a>00519   }
<a name="l00520"></a>00520 
<a name="l00521"></a>00521 
<a name="l00522"></a>00522   <span class="keyword">template</span>&lt;<span class="keyword">class </span>cplx,
<a name="l00523"></a>00523            <span class="keyword">class </span>Allocator, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00524"></a>00524   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(<span class="keyword">const</span> Matrix&lt;cplx, General, ArrayRowSparse, Allocator&gt;&amp; A,
<a name="l00525"></a>00525                Vector&lt;cplx, Storage2, Allocator2&gt;&amp; x)
<a name="l00526"></a>00526   {
<a name="l00527"></a>00527     <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(SeldonNoTrans, A, x);
<a name="l00528"></a>00528   }
<a name="l00529"></a><a class="code" href="namespace_seldon.php#afe58124d84be9c80083f521f7088a252">00529</a> 
<a name="l00530"></a>00530 
<a name="l00532"></a>00532 
<a name="l00535"></a>00535   <span class="keyword">template</span>&lt;<span class="keyword">class </span>cplx, <span class="keyword">class </span>TransStatus,
<a name="l00536"></a>00536            <span class="keyword">class </span>Allocator, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00537"></a>00537   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(<span class="keyword">const</span> TransStatus&amp; transA,
<a name="l00538"></a>00538                <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;cplx, General, ArrayRowSparse, Allocator&gt;</a>&amp; A,
<a name="l00539"></a>00539                <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;cplx, Storage2, Allocator2&gt;</a>&amp; x)
<a name="l00540"></a>00540   {
<a name="l00541"></a>00541     <span class="keywordtype">int</span> i, k, n, k_;
<a name="l00542"></a>00542     cplx inv_diag;
<a name="l00543"></a>00543     n = A.GetM();
<a name="l00544"></a>00544 
<a name="l00545"></a>00545     <span class="keywordflow">if</span> (transA.Trans())
<a name="l00546"></a>00546       {
<a name="l00547"></a>00547         <span class="comment">// Forward solve (with U^T).</span>
<a name="l00548"></a>00548         <span class="keywordflow">for</span> (i = 0 ; i &lt; n ; i++)
<a name="l00549"></a>00549           {
<a name="l00550"></a>00550             k_ = 0; k = A.Index(i, k_);
<a name="l00551"></a>00551             <span class="keywordflow">while</span> ( k &lt; i)
<a name="l00552"></a>00552               {
<a name="l00553"></a>00553                 k_++;
<a name="l00554"></a>00554                 k = A.Index(i, k_);
<a name="l00555"></a>00555               }
<a name="l00556"></a>00556 
<a name="l00557"></a>00557             x(i) *= A.Value(i, k_);
<a name="l00558"></a>00558             <span class="keywordflow">for</span> (k = k_ + 1; k &lt; A.GetRowSize(i); k++)
<a name="l00559"></a>00559                 x(A.Index(i, k)) -= A.Value(i, k) * x(i);
<a name="l00560"></a>00560           }
<a name="l00561"></a>00561 
<a name="l00562"></a>00562         <span class="comment">// Backward solve (with L^T).</span>
<a name="l00563"></a>00563         <span class="keywordflow">for</span> (i = n-1; i &gt;= 0; i--)
<a name="l00564"></a>00564           {
<a name="l00565"></a>00565             k_ = 0; k = A.Index(i, k_);
<a name="l00566"></a>00566             <span class="keywordflow">while</span> (k &lt; i)
<a name="l00567"></a>00567               {
<a name="l00568"></a>00568                 x(k) -= A.Value(i, k_) * x(i);
<a name="l00569"></a>00569                 k_++;
<a name="l00570"></a>00570                 k = A.Index(i, k_);
<a name="l00571"></a>00571               }
<a name="l00572"></a>00572           }
<a name="l00573"></a>00573       }
<a name="l00574"></a>00574     <span class="keywordflow">else</span>
<a name="l00575"></a>00575       {
<a name="l00576"></a>00576         <span class="comment">// Forward solve.</span>
<a name="l00577"></a>00577         <span class="keywordflow">for</span> (i = 0; i &lt; n; i++)
<a name="l00578"></a>00578           {
<a name="l00579"></a>00579             k_ = 0;
<a name="l00580"></a>00580             k = A.Index(i, k_);
<a name="l00581"></a>00581             <span class="keywordflow">while</span> (k &lt; i)
<a name="l00582"></a>00582               {
<a name="l00583"></a>00583                 x(i) -= A.Value(i, k_) * x(k);
<a name="l00584"></a>00584                 k_++;
<a name="l00585"></a>00585                 k = A.Index(i, k_);
<a name="l00586"></a>00586               }
<a name="l00587"></a>00587           }
<a name="l00588"></a>00588 
<a name="l00589"></a>00589         <span class="comment">// Backward solve.</span>
<a name="l00590"></a>00590         <span class="keywordflow">for</span> (i = n-1; i &gt;= 0; i--)
<a name="l00591"></a>00591           {
<a name="l00592"></a>00592             k_ = 0;
<a name="l00593"></a>00593             k = A.Index(i, k_);
<a name="l00594"></a>00594             <span class="keywordflow">while</span> (k &lt; i)
<a name="l00595"></a>00595               {
<a name="l00596"></a>00596                 k_++;
<a name="l00597"></a>00597                 k = A.Index(i, k_);
<a name="l00598"></a>00598               }
<a name="l00599"></a>00599 
<a name="l00600"></a>00600             inv_diag = A.Value(i, k_);
<a name="l00601"></a>00601             <span class="keywordflow">for</span> (k = k_ + 1; k &lt; A.GetRowSize(i); k++)
<a name="l00602"></a>00602               x(i) -= A.Value(i, k) * x(A.Index(i, k));
<a name="l00603"></a>00603 
<a name="l00604"></a><a class="code" href="namespace_seldon.php#ab168df3da03c8bc96cea8540a02077b6">00604</a>             x(i) *= inv_diag;
<a name="l00605"></a>00605           }
<a name="l00606"></a>00606       }
<a name="l00607"></a>00607   }
<a name="l00608"></a>00608 
<a name="l00609"></a>00609 
<a name="l00611"></a>00611   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00612"></a>00612   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a76d3f187a877c6c58245b0716e1adc00" title="Returns the LU factorization of a matrix.">GetLU</a>(Matrix&lt;T, Symmetric, ArrayRowSymSparse, Allocator&gt;&amp; A,
<a name="l00613"></a>00613              <span class="keywordtype">int</span> print_level)
<a name="l00614"></a>00614   {
<a name="l00615"></a>00615     <span class="keywordtype">int</span> size_row;
<a name="l00616"></a>00616     <span class="keywordtype">int</span> n = A.GetN();
<a name="l00617"></a>00617     <span class="keywordtype">double</span> zero = 0.0;
<a name="l00618"></a>00618 
<a name="l00619"></a>00619     T fact, s, t;
<a name="l00620"></a>00620     <span class="keywordtype">double</span> tnorm;
<a name="l00621"></a>00621     <span class="keywordtype">int</span> length_lower, length_upper, jpos, jrow;
<a name="l00622"></a>00622     <span class="keywordtype">int</span> i_row, j_col, index_lu, length;
<a name="l00623"></a>00623     <span class="keywordtype">int</span> i, j, k;
<a name="l00624"></a>00624 
<a name="l00625"></a>00625     Vector&lt;T, VectFull, Allocator&gt; Row_Val(n);
<a name="l00626"></a>00626     IVect Index(n), Row_Ind(n);
<a name="l00627"></a>00627     Row_Val.Zero();
<a name="l00628"></a>00628     Row_Ind.Fill(-1);
<a name="l00629"></a>00629 
<a name="l00630"></a>00630     Index.Fill(-1);
<a name="l00631"></a>00631 
<a name="l00632"></a>00632     <span class="comment">// We convert A into an unsymmetric matrix.</span>
<a name="l00633"></a>00633     Matrix&lt;T, General, ArrayRowSparse, Allocator&gt; B;
<a name="l00634"></a>00634     Seldon::Copy(A, B);
<a name="l00635"></a>00635 
<a name="l00636"></a>00636     A.Clear();
<a name="l00637"></a>00637     A.Reallocate(n, n);
<a name="l00638"></a>00638 
<a name="l00639"></a>00639     <span class="comment">// Main loop.</span>
<a name="l00640"></a>00640     <span class="keywordtype">int</span> new_percent = 0, old_percent = 0;
<a name="l00641"></a>00641     <span class="keywordflow">for</span> (i_row = 0; i_row &lt; n; i_row++)
<a name="l00642"></a>00642       {
<a name="l00643"></a>00643         <span class="comment">// Progress bar if print level is high enough.</span>
<a name="l00644"></a>00644         <span class="keywordflow">if</span> (print_level &gt; 0)
<a name="l00645"></a>00645           {
<a name="l00646"></a>00646             new_percent = int(<span class="keywordtype">double</span>(i_row) / <span class="keywordtype">double</span>(n-1) * 78.);
<a name="l00647"></a>00647             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> percent = old_percent; percent &lt; new_percent; percent++)
<a name="l00648"></a>00648               {
<a name="l00649"></a>00649                 cout &lt;&lt; <span class="stringliteral">&quot;#&quot;</span>;
<a name="l00650"></a>00650                 cout.flush();
<a name="l00651"></a>00651               }
<a name="l00652"></a>00652             old_percent = new_percent;
<a name="l00653"></a>00653           }
<a name="l00654"></a>00654 
<a name="l00655"></a>00655         <span class="comment">// 1-norm of the row of initial matrix.</span>
<a name="l00656"></a>00656         size_row = B.GetRowSize(i_row);
<a name="l00657"></a>00657         tnorm = zero;
<a name="l00658"></a>00658         <span class="keywordflow">for</span> (k = 0 ; k &lt; size_row; k++)
<a name="l00659"></a>00659           tnorm += abs(B.Value(i_row, k));
<a name="l00660"></a>00660 
<a name="l00661"></a>00661         <span class="keywordflow">if</span> (tnorm == zero)
<a name="l00662"></a>00662           <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;GetLU(Matrix&lt;ArrayRowSymSparse&gt;&amp;, int)&quot;</span>,
<a name="l00663"></a>00663                               <span class="stringliteral">&quot;The matrix is structurally singular. &quot;</span>
<a name="l00664"></a>00664                               <span class="stringliteral">&quot;The norm of row &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i_row)
<a name="l00665"></a>00665                               + <span class="stringliteral">&quot; is equal to 0.&quot;</span>);
<a name="l00666"></a>00666 
<a name="l00667"></a>00667         <span class="comment">// Separating lower part from upper part for this row.</span>
<a name="l00668"></a>00668         length_upper = 1;
<a name="l00669"></a>00669         length_lower = 0;
<a name="l00670"></a>00670         Row_Ind(i_row) = i_row;
<a name="l00671"></a>00671         Row_Val(i_row) = 0.0;
<a name="l00672"></a>00672         Index(i_row) = i_row;
<a name="l00673"></a>00673 
<a name="l00674"></a>00674         <span class="keywordflow">for</span> (j = 0; j &lt; size_row; j++)
<a name="l00675"></a>00675           {
<a name="l00676"></a>00676             k = B.Index(i_row,j);
<a name="l00677"></a>00677             t = B.Value(i_row,j);
<a name="l00678"></a>00678             <span class="keywordflow">if</span> (k &lt; i_row)
<a name="l00679"></a>00679               {
<a name="l00680"></a>00680                 Row_Ind(length_lower) = k;
<a name="l00681"></a>00681                 Row_Val(length_lower) = t;
<a name="l00682"></a>00682                 Index(k) = length_lower;
<a name="l00683"></a>00683                 ++length_lower;
<a name="l00684"></a>00684               }
<a name="l00685"></a>00685             <span class="keywordflow">else</span> <span class="keywordflow">if</span> (k == i_row)
<a name="l00686"></a>00686               {
<a name="l00687"></a>00687                 Row_Val(i_row) = t;
<a name="l00688"></a>00688               }
<a name="l00689"></a>00689             <span class="keywordflow">else</span>
<a name="l00690"></a>00690               {
<a name="l00691"></a>00691                 jpos = i_row + length_upper;
<a name="l00692"></a>00692                 Row_Ind(jpos) = k;
<a name="l00693"></a>00693                 Row_Val(jpos) = t;
<a name="l00694"></a>00694                 Index(k) = jpos;
<a name="l00695"></a>00695                 length_upper++;
<a name="l00696"></a>00696               }
<a name="l00697"></a>00697           }
<a name="l00698"></a>00698 
<a name="l00699"></a>00699         <span class="comment">// This row of B is cleared.</span>
<a name="l00700"></a>00700         B.ClearRow(i_row);
<a name="l00701"></a>00701 
<a name="l00702"></a>00702         j_col = 0;
<a name="l00703"></a>00703         length = 0;
<a name="l00704"></a>00704 
<a name="l00705"></a>00705         <span class="comment">// We eliminate previous rows.</span>
<a name="l00706"></a>00706         <span class="keywordflow">while</span> (j_col &lt;length_lower)
<a name="l00707"></a>00707           {
<a name="l00708"></a>00708             <span class="comment">// In order to do the elimination in the correct order, we must</span>
<a name="l00709"></a>00709             <span class="comment">// select the smallest column index.</span>
<a name="l00710"></a>00710             jrow = Row_Ind(j_col);
<a name="l00711"></a>00711             k = j_col;
<a name="l00712"></a>00712 
<a name="l00713"></a>00713             <span class="comment">// We determine smallest column index.</span>
<a name="l00714"></a>00714             <span class="keywordflow">for</span> (j = (j_col+1) ; j &lt; length_lower; j++)
<a name="l00715"></a>00715               {
<a name="l00716"></a>00716                 <span class="keywordflow">if</span> (Row_Ind(j) &lt; jrow)
<a name="l00717"></a>00717                   {
<a name="l00718"></a>00718                     jrow = Row_Ind(j);
<a name="l00719"></a>00719                     k = j;
<a name="l00720"></a>00720                   }
<a name="l00721"></a>00721               }
<a name="l00722"></a>00722 
<a name="l00723"></a>00723             <span class="comment">// If needed, we exchange positions of this element in</span>
<a name="l00724"></a>00724             <span class="comment">// Row_Ind/Row_Val so that it appears first.</span>
<a name="l00725"></a>00725             <span class="keywordflow">if</span> (k != j_col)
<a name="l00726"></a>00726               {
<a name="l00727"></a>00727 
<a name="l00728"></a>00728                 j = Row_Ind(j_col);
<a name="l00729"></a>00729                 Row_Ind(j_col) = Row_Ind(k);
<a name="l00730"></a>00730                 Row_Ind(k) = j;
<a name="l00731"></a>00731 
<a name="l00732"></a>00732                 Index(jrow) = j_col;
<a name="l00733"></a>00733                 Index(j) = k;
<a name="l00734"></a>00734 
<a name="l00735"></a>00735                 s = Row_Val(j_col);
<a name="l00736"></a>00736                 Row_Val(j_col) = Row_Val(k);
<a name="l00737"></a>00737                 Row_Val(k) = s;
<a name="l00738"></a>00738               }
<a name="l00739"></a>00739 
<a name="l00740"></a>00740             <span class="comment">// Zero out element in row by setting Index to -1.</span>
<a name="l00741"></a>00741             Index(jrow) = -1;
<a name="l00742"></a>00742 
<a name="l00743"></a>00743             <span class="comment">// Gets the multiplier for row to be eliminated.</span>
<a name="l00744"></a>00744             fact = Row_Val(j_col) * A.Value(jrow, 0);
<a name="l00745"></a>00745 
<a name="l00746"></a>00746             <span class="comment">// Combines current row and row jrow.</span>
<a name="l00747"></a>00747             <span class="keywordflow">for</span> (k = 1; k &lt; A.GetRowSize(jrow); k++)
<a name="l00748"></a>00748               {
<a name="l00749"></a>00749                 s = fact * A.Value(jrow, k);
<a name="l00750"></a>00750                 j = A.Index(jrow, k);
<a name="l00751"></a>00751 
<a name="l00752"></a>00752                 jpos = Index(j);
<a name="l00753"></a>00753                 <span class="keywordflow">if</span> (j &gt;= i_row)
<a name="l00754"></a>00754                   {
<a name="l00755"></a>00755 
<a name="l00756"></a>00756                     <span class="comment">// Dealing with upper part.</span>
<a name="l00757"></a>00757                     <span class="keywordflow">if</span> (jpos == -1)
<a name="l00758"></a>00758                       {
<a name="l00759"></a>00759 
<a name="l00760"></a>00760                         <span class="comment">// This is a fill-in element.</span>
<a name="l00761"></a>00761                         i = i_row + length_upper;
<a name="l00762"></a>00762                         Row_Ind(i) = j;
<a name="l00763"></a>00763                         Index(j) = i;
<a name="l00764"></a>00764                         Row_Val(i) = -s;
<a name="l00765"></a>00765                         ++length_upper;
<a name="l00766"></a>00766                       }
<a name="l00767"></a>00767                     <span class="keywordflow">else</span>
<a name="l00768"></a>00768                       {
<a name="l00769"></a>00769                         <span class="comment">// This is not a fill-in element.</span>
<a name="l00770"></a>00770                         Row_Val(jpos) -= s;
<a name="l00771"></a>00771                       }
<a name="l00772"></a>00772                   }
<a name="l00773"></a>00773                 <span class="keywordflow">else</span>
<a name="l00774"></a>00774                   {
<a name="l00775"></a>00775                     <span class="comment">// Dealing  with lower part.</span>
<a name="l00776"></a>00776                     <span class="keywordflow">if</span> (jpos == -1)
<a name="l00777"></a>00777                       {
<a name="l00778"></a>00778                         <span class="comment">// This is a fill-in element.</span>
<a name="l00779"></a>00779                         Row_Ind(length_lower) = j;
<a name="l00780"></a>00780                         Index(j) = length_lower;
<a name="l00781"></a>00781                         Row_Val(length_lower) = -s;
<a name="l00782"></a>00782                         ++length_lower;
<a name="l00783"></a>00783                       }
<a name="l00784"></a>00784                     <span class="keywordflow">else</span>
<a name="l00785"></a>00785                       {
<a name="l00786"></a>00786                         <span class="comment">// This is not a fill-in element.</span>
<a name="l00787"></a>00787                         Row_Val(jpos) -= s;
<a name="l00788"></a>00788                       }
<a name="l00789"></a>00789                   }
<a name="l00790"></a>00790               }
<a name="l00791"></a>00791 
<a name="l00792"></a>00792             <span class="comment">// We store this pivot element (from left to right -- no</span>
<a name="l00793"></a>00793             <span class="comment">// danger of overlap with the working elements in L (pivots).</span>
<a name="l00794"></a>00794             Row_Val(length) = fact;
<a name="l00795"></a>00795             Row_Ind(length) = jrow;
<a name="l00796"></a>00796             ++length;
<a name="l00797"></a>00797             j_col++;
<a name="l00798"></a>00798           }
<a name="l00799"></a>00799 
<a name="l00800"></a>00800         <span class="comment">// Resets double-pointer to zero (U-part).</span>
<a name="l00801"></a>00801         <span class="keywordflow">for</span> (k = 0; k &lt; length_upper; k++)
<a name="l00802"></a>00802           Index(Row_Ind(i_row + k)) = -1;
<a name="l00803"></a>00803 
<a name="l00804"></a>00804         <span class="comment">// Updating U-matrix</span>
<a name="l00805"></a>00805         length = 0;
<a name="l00806"></a>00806         <span class="keywordflow">for</span> (k = 1; k &lt;= length_upper - 1; k++)
<a name="l00807"></a>00807           {
<a name="l00808"></a>00808             ++length;
<a name="l00809"></a>00809             Row_Val(i_row + length) = Row_Val(i_row + k);
<a name="l00810"></a>00810             Row_Ind(i_row + length) = Row_Ind(i_row + k);
<a name="l00811"></a>00811           }
<a name="l00812"></a>00812 
<a name="l00813"></a>00813         length++;
<a name="l00814"></a>00814 
<a name="l00815"></a>00815         <span class="comment">// Copies U-part in matrix A.</span>
<a name="l00816"></a>00816         A.ReallocateRow(i_row, length);
<a name="l00817"></a>00817         index_lu = 1;
<a name="l00818"></a>00818         <span class="keywordflow">for</span> (k = i_row + 1 ; k &lt;= i_row + length - 1 ; k++)
<a name="l00819"></a>00819           {
<a name="l00820"></a>00820             A.Index(i_row, index_lu) = Row_Ind(k);
<a name="l00821"></a>00821             A.Value(i_row, index_lu) = Row_Val(k);
<a name="l00822"></a>00822             ++index_lu;
<a name="l00823"></a>00823           }
<a name="l00824"></a>00824 
<a name="l00825"></a>00825         <span class="comment">// Stores the inverse of the diagonal element of u.</span>
<a name="l00826"></a>00826         A.Value(i_row,0) = 1.0 / Row_Val(i_row);
<a name="l00827"></a>00827 
<a name="l00828"></a>00828       } <span class="comment">// end main loop.</span>
<a name="l00829"></a>00829 
<a name="l00830"></a>00830     <span class="keywordflow">if</span> (print_level &gt; 0)
<a name="l00831"></a>00831       cout &lt;&lt; endl;
<a name="l00832"></a>00832 
<a name="l00833"></a>00833     <span class="comment">// for each row of A, we divide by diagonal value</span>
<a name="l00834"></a>00834     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; n; i++)
<a name="l00835"></a>00835       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 1; j &lt; A.GetRowSize(i); j++)
<a name="l00836"></a>00836         A.Value(i, j) *= A.Value(i, 0);
<a name="l00837"></a>00837 
<a name="l00838"></a>00838     <span class="keywordflow">if</span> (print_level &gt; 0)
<a name="l00839"></a>00839       cout &lt;&lt; <span class="stringliteral">&quot;The matrix takes &quot;</span> &lt;&lt;
<a name="l00840"></a>00840         int((A.GetDataSize() * (<span class="keyword">sizeof</span>(T) + 4)) / (1024. * 1024.))
<a name="l00841"></a>00841            &lt;&lt; <span class="stringliteral">&quot; MB&quot;</span> &lt;&lt; endl;
<a name="l00842"></a>00842 
<a name="l00843"></a>00843   }
<a name="l00844"></a><a class="code" href="namespace_seldon.php#a2c4b7db2f9998750f069c9564e7d6de9">00844</a> 
<a name="l00845"></a>00845 
<a name="l00847"></a>00847 
<a name="l00850"></a>00850   <span class="keyword">template</span>&lt;<span class="keyword">class </span>real, <span class="keyword">class </span>cplx, <span class="keyword">class </span>Allocator,
<a name="l00851"></a>00851            <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00852"></a>00852   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;real, Symmetric, ArrayRowSymSparse, Allocator&gt;</a>&amp; A,
<a name="l00853"></a>00853                <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;cplx, Storage2, Allocator2&gt;</a>&amp; x)
<a name="l00854"></a>00854   {
<a name="l00855"></a>00855     <span class="keywordtype">int</span> n = A.GetM(), j_row;
<a name="l00856"></a>00856     cplx tmp;
<a name="l00857"></a>00857 
<a name="l00858"></a>00858     <span class="comment">// We solve first L y = b.</span>
<a name="l00859"></a>00859     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i_col = 0; i_col &lt; n ; i_col++)
<a name="l00860"></a>00860       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 1; k &lt; A.GetRowSize(i_col) ; k++)
<a name="l00861"></a>00861         {
<a name="l00862"></a>00862           j_row = A.Index(i_col, k);
<a name="l00863"></a>00863           x(j_row) -= A.Value(i_col, k)*x(i_col);
<a name="l00864"></a>00864         }
<a name="l00865"></a>00865 
<a name="l00866"></a>00866     <span class="comment">// Inverting by diagonal D.</span>
<a name="l00867"></a>00867     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i_col = 0; i_col &lt; n ; i_col++)
<a name="l00868"></a>00868       x(i_col) *= A.Value(i_col, 0);
<a name="l00869"></a>00869 
<a name="l00870"></a>00870     <span class="comment">// Then we solve L^t x = y.</span>
<a name="l00871"></a>00871     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i_col = n-1; i_col &gt;=0; i_col--)
<a name="l00872"></a>00872       {
<a name="l00873"></a>00873         tmp = x(i_col);
<a name="l00874"></a>00874         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 1; k &lt; A.GetRowSize(i_col); k++)
<a name="l00875"></a>00875           {
<a name="l00876"></a>00876             j_row = A.Index(i_col,k);
<a name="l00877"></a>00877             tmp -= A.Value(i_col,k) * x(j_row);
<a name="l00878"></a>00878           }
<a name="l00879"></a>00879         x(i_col) = tmp;
<a name="l00880"></a>00880       }
<a name="l00881"></a>00881   }
<a name="l00882"></a>00882 
<a name="l00883"></a>00883 
<a name="l00884"></a>00884   <span class="comment">/********************************************</span>
<a name="l00885"></a>00885 <span class="comment">   * GetLU and SolveLU for SeldonSparseSolver *</span>
<a name="l00886"></a>00886 <span class="comment">   ********************************************/</span>
<a name="l00887"></a>00887 
<a name="l00888"></a>00888 
<a name="l00889"></a>00889   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator, <span class="keyword">class</span> Alloc2&gt;
<a name="l00890"></a>00890   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a76d3f187a877c6c58245b0716e1adc00" title="Returns the LU factorization of a matrix.">GetLU</a>(Matrix&lt;T, Symmetric, Storage, Allocator&gt;&amp; A,
<a name="l00891"></a>00891              SparseSeldonSolver&lt;T, Alloc2&gt;&amp; mat_lu,
<a name="l00892"></a>00892              <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>)
<a name="l00893"></a>00893   {
<a name="l00894"></a>00894     <span class="comment">// identity ordering</span>
<a name="l00895"></a>00895     IVect perm(A.GetM());
<a name="l00896"></a>00896     perm.Fill();
<a name="l00897"></a>00897 
<a name="l00898"></a>00898     <span class="comment">// factorization</span>
<a name="l00899"></a>00899     mat_lu.FactorizeMatrix(perm, A, keep_matrix);
<a name="l00900"></a>00900   }
<a name="l00901"></a>00901 
<a name="l00902"></a>00902 
<a name="l00903"></a>00903   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator, <span class="keyword">class</span> Alloc2&gt;
<a name="l00904"></a>00904   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a76d3f187a877c6c58245b0716e1adc00" title="Returns the LU factorization of a matrix.">GetLU</a>(Matrix&lt;T, General, Storage, Allocator&gt;&amp; A,
<a name="l00905"></a>00905              SparseSeldonSolver&lt;T, Alloc2&gt;&amp; mat_lu,
<a name="l00906"></a>00906              <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>)
<a name="l00907"></a>00907   {
<a name="l00908"></a>00908     <span class="comment">// identity ordering</span>
<a name="l00909"></a>00909     IVect perm(A.GetM());
<a name="l00910"></a>00910     perm.Fill();
<a name="l00911"></a>00911 
<a name="l00912"></a>00912     <span class="comment">// factorization</span>
<a name="l00913"></a>00913     mat_lu.FactorizeMatrix(perm, A, keep_matrix);
<a name="l00914"></a>00914   }
<a name="l00915"></a>00915 
<a name="l00916"></a>00916 
<a name="l00917"></a>00917   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Alloc2, <span class="keyword">class</span> Allocator&gt;
<a name="l00918"></a>00918   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(SparseSeldonSolver&lt;T, Alloc2&gt;&amp; mat_lu,
<a name="l00919"></a>00919                Vector&lt;T, VectFull, Allocator&gt;&amp; x)
<a name="l00920"></a>00920   {
<a name="l00921"></a>00921     mat_lu.Solve(x);
<a name="l00922"></a>00922   }
<a name="l00923"></a>00923 
<a name="l00924"></a>00924 
<a name="l00925"></a>00925   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Alloc2, <span class="keyword">class</span> Allocator, <span class="keyword">class</span> Transpose_status&gt;
<a name="l00926"></a>00926   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(<span class="keyword">const</span> Transpose_status&amp; TransA,
<a name="l00927"></a>00927                SparseSeldonSolver&lt;T, Alloc2&gt;&amp; mat_lu,
<a name="l00928"></a>00928                Vector&lt;T, VectFull, Allocator&gt;&amp; x)
<a name="l00929"></a>00929   {
<a name="l00930"></a>00930     mat_lu.Solve(TransA, x);
<a name="l00931"></a>00931   }
<a name="l00932"></a>00932 
<a name="l00933"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#aef26a5c7fcbe65069bca7967c73f7ab5">00933</a> 
<a name="l00934"></a>00934   <span class="comment">/*************************</span>
<a name="l00935"></a>00935 <span class="comment">   * Choice of best solver *</span>
<a name="l00936"></a>00936 <span class="comment">   *************************/</span>
<a name="l00937"></a>00937 
<a name="l00938"></a>00938 
<a name="l00940"></a>00940   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00941"></a>00941   <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#aef26a5c7fcbe65069bca7967c73f7ab5" title="Default constructor.">SparseDirectSolver&lt;T&gt;::SparseDirectSolver</a>()
<a name="l00942"></a>00942   {
<a name="l00943"></a>00943     n = 0;
<a name="l00944"></a>00944     <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ae9819b726220aa9599132e149a6416e6" title="Ordering to use.">type_ordering</a> = SparseMatrixOrdering::AUTO;
<a name="l00945"></a>00945 
<a name="l00946"></a>00946     <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> = SELDON_SOLVER;
<a name="l00947"></a>00947 
<a name="l00948"></a>00948     <span class="comment">// We try to use an other available solver.</span>
<a name="l00949"></a>00949     <span class="comment">// The order of preference is Pastix, Mumps, UmfPack and SuperLU.</span>
<a name="l00950"></a>00950 <span class="preprocessor">#ifdef SELDON_WITH_SUPERLU</span>
<a name="l00951"></a>00951 <span class="preprocessor"></span>    <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> = SUPERLU;
<a name="l00952"></a>00952 <span class="preprocessor">#endif</span>
<a name="l00953"></a>00953 <span class="preprocessor"></span><span class="preprocessor">#ifdef SELDON_WITH_UMFPACK</span>
<a name="l00954"></a>00954 <span class="preprocessor"></span>    <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> = UMFPACK;
<a name="l00955"></a>00955 <span class="preprocessor">#endif</span>
<a name="l00956"></a>00956 <span class="preprocessor"></span><span class="preprocessor">#ifdef SELDON_WITH_MUMPS</span>
<a name="l00957"></a>00957 <span class="preprocessor"></span>    <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> = MUMPS;
<a name="l00958"></a>00958 <span class="preprocessor">#endif</span>
<a name="l00959"></a>00959 <span class="preprocessor"></span><span class="preprocessor">#ifdef SELDON_WITH_PASTIX</span>
<a name="l00960"></a>00960 <span class="preprocessor"></span>    <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> = PASTIX;
<a name="l00961"></a>00961 <span class="preprocessor">#endif</span>
<a name="l00962"></a>00962 <span class="preprocessor"></span>
<a name="l00963"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a4dc8a27fc02a2bc978629f6630f319cd">00963</a>     <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a2b4fcbacb7f6fb5276fe33b36501e99c" title="Number of threads (for Pastix).">number_threads_per_node</a> = 1;
<a name="l00964"></a>00964     <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a215ef20ce0827e0974cade309a9aaced" title="Threshold for ilut solver.">threshold_matrix</a> = 0;
<a name="l00965"></a>00965     <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a67010f0cc1e81d66504f3987385fb5f7" title="Use of non-symmetric ilut?">enforce_unsym_ilut</a> = <span class="keyword">false</span>;
<a name="l00966"></a>00966   }
<a name="l00967"></a>00967 
<a name="l00968"></a>00968 
<a name="l00970"></a>00970   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00971"></a>00971   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a4dc8a27fc02a2bc978629f6630f319cd" title="Hiding all messages.">SparseDirectSolver&lt;T&gt;::HideMessages</a>()
<a name="l00972"></a>00972   {
<a name="l00973"></a>00973     <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a18d4ec1c11f680b7a364757063bde92d" title="Default solver.">mat_seldon</a>.HideMessages();
<a name="l00974"></a>00974 
<a name="l00975"></a>00975 <span class="preprocessor">#ifdef SELDON_WITH_MUMPS</span>
<a name="l00976"></a>00976 <span class="preprocessor"></span>    mat_mumps.HideMessages();
<a name="l00977"></a>00977 <span class="preprocessor">#endif</span>
<a name="l00978"></a>00978 <span class="preprocessor"></span>
<a name="l00979"></a>00979 <span class="preprocessor">#ifdef SELDON_WITH_SUPERLU</span>
<a name="l00980"></a>00980 <span class="preprocessor"></span>    mat_superlu.HideMessages();
<a name="l00981"></a>00981 <span class="preprocessor">#endif</span>
<a name="l00982"></a>00982 <span class="preprocessor"></span>
<a name="l00983"></a>00983 <span class="preprocessor">#ifdef SELDON_WITH_UMFPACK</span>
<a name="l00984"></a>00984 <span class="preprocessor"></span>    mat_umf.HideMessages();
<a name="l00985"></a>00985 <span class="preprocessor">#endif</span>
<a name="l00986"></a>00986 <span class="preprocessor"></span>
<a name="l00987"></a>00987 <span class="preprocessor">#ifdef SELDON_WITH_PASTIX</span>
<a name="l00988"></a>00988 <span class="preprocessor"></span>    mat_pastix.HideMessages();
<a name="l00989"></a>00989 <span class="preprocessor">#endif</span>
<a name="l00990"></a>00990 <span class="preprocessor"></span>
<a name="l00991"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ad4b15cbf1fc0faec2354e0beaa4678eb">00991</a> <span class="preprocessor">#ifdef SELDON_WITH_PRECONDITIONING</span>
<a name="l00992"></a>00992 <span class="preprocessor"></span>    mat_ilut.SetPrintLevel(0);
<a name="l00993"></a>00993 <span class="preprocessor">#endif</span>
<a name="l00994"></a>00994 <span class="preprocessor"></span>  }
<a name="l00995"></a>00995 
<a name="l00996"></a>00996 
<a name="l00998"></a>00998   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00999"></a>00999   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ad4b15cbf1fc0faec2354e0beaa4678eb" title="Displaying basic messages.">SparseDirectSolver&lt;T&gt;::ShowMessages</a>()
<a name="l01000"></a>01000   {
<a name="l01001"></a>01001     <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a18d4ec1c11f680b7a364757063bde92d" title="Default solver.">mat_seldon</a>.ShowMessages();
<a name="l01002"></a>01002 
<a name="l01003"></a>01003 <span class="preprocessor">#ifdef SELDON_WITH_MUMPS</span>
<a name="l01004"></a>01004 <span class="preprocessor"></span>    mat_mumps.ShowMessages();
<a name="l01005"></a>01005 <span class="preprocessor">#endif</span>
<a name="l01006"></a>01006 <span class="preprocessor"></span>
<a name="l01007"></a>01007 <span class="preprocessor">#ifdef SELDON_WITH_SUPERLU</span>
<a name="l01008"></a>01008 <span class="preprocessor"></span>    mat_superlu.ShowMessages();
<a name="l01009"></a>01009 <span class="preprocessor">#endif</span>
<a name="l01010"></a>01010 <span class="preprocessor"></span>
<a name="l01011"></a>01011 <span class="preprocessor">#ifdef SELDON_WITH_UMFPACK</span>
<a name="l01012"></a>01012 <span class="preprocessor"></span>    mat_umf.ShowMessages();
<a name="l01013"></a>01013 <span class="preprocessor">#endif</span>
<a name="l01014"></a>01014 <span class="preprocessor"></span>
<a name="l01015"></a>01015 <span class="preprocessor">#ifdef SELDON_WITH_PASTIX</span>
<a name="l01016"></a>01016 <span class="preprocessor"></span>    mat_pastix.ShowMessages();
<a name="l01017"></a>01017 <span class="preprocessor">#endif</span>
<a name="l01018"></a>01018 <span class="preprocessor"></span>
<a name="l01019"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a70a07b6d4ada2aa3b63e71edb71675a6">01019</a> <span class="preprocessor">#ifdef SELDON_WITH_PRECONDITIONING</span>
<a name="l01020"></a>01020 <span class="preprocessor"></span>    mat_ilut.SetPrintLevel(1);
<a name="l01021"></a>01021 <span class="preprocessor">#endif</span>
<a name="l01022"></a>01022 <span class="preprocessor"></span>  }
<a name="l01023"></a>01023 
<a name="l01024"></a>01024 
<a name="l01026"></a>01026   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l01027"></a>01027   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a70a07b6d4ada2aa3b63e71edb71675a6" title="Displaying all the messages.">SparseDirectSolver&lt;T&gt;::ShowFullHistory</a>()
<a name="l01028"></a>01028   {
<a name="l01029"></a>01029     <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a18d4ec1c11f680b7a364757063bde92d" title="Default solver.">mat_seldon</a>.ShowMessages();
<a name="l01030"></a>01030 
<a name="l01031"></a>01031 <span class="preprocessor">#ifdef SELDON_WITH_MUMPS</span>
<a name="l01032"></a>01032 <span class="preprocessor"></span>    mat_mumps.ShowMessages();
<a name="l01033"></a>01033 <span class="preprocessor">#endif</span>
<a name="l01034"></a>01034 <span class="preprocessor"></span>
<a name="l01035"></a>01035 <span class="preprocessor">#ifdef SELDON_WITH_SUPERLU</span>
<a name="l01036"></a>01036 <span class="preprocessor"></span>    mat_superlu.ShowMessages();
<a name="l01037"></a>01037 <span class="preprocessor">#endif</span>
<a name="l01038"></a>01038 <span class="preprocessor"></span>
<a name="l01039"></a>01039 <span class="preprocessor">#ifdef SELDON_WITH_UMFPACK</span>
<a name="l01040"></a>01040 <span class="preprocessor"></span>    mat_umf.ShowMessages();
<a name="l01041"></a>01041 <span class="preprocessor">#endif</span>
<a name="l01042"></a>01042 <span class="preprocessor"></span>
<a name="l01043"></a>01043 <span class="preprocessor">#ifdef SELDON_WITH_PASTIX</span>
<a name="l01044"></a>01044 <span class="preprocessor"></span>    mat_pastix.ShowFullHistory();
<a name="l01045"></a>01045 <span class="preprocessor">#endif</span>
<a name="l01046"></a>01046 <span class="preprocessor"></span>
<a name="l01047"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a1da1c40d9433a58bcef7dec63695a9f9">01047</a> <span class="preprocessor">#ifdef SELDON_WITH_PRECONDITIONING</span>
<a name="l01048"></a>01048 <span class="preprocessor"></span>    mat_ilut.SetPrintLevel(1);
<a name="l01049"></a>01049 <span class="preprocessor">#endif</span>
<a name="l01050"></a>01050 <span class="preprocessor"></span>  }
<a name="l01051"></a>01051 
<a name="l01052"></a>01052 
<a name="l01054"></a>01054   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l01055"></a>01055   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a1da1c40d9433a58bcef7dec63695a9f9" title="Clearing factorization.">SparseDirectSolver&lt;T&gt;::Clear</a>()
<a name="l01056"></a>01056   {
<a name="l01057"></a>01057     <span class="keywordflow">if</span> (n &gt; 0)
<a name="l01058"></a>01058       {
<a name="l01059"></a>01059         n = 0;
<a name="l01060"></a>01060         <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a18d4ec1c11f680b7a364757063bde92d" title="Default solver.">mat_seldon</a>.Clear();
<a name="l01061"></a>01061 
<a name="l01062"></a>01062 <span class="preprocessor">#ifdef SELDON_WITH_MUMPS</span>
<a name="l01063"></a>01063 <span class="preprocessor"></span>        mat_mumps.Clear();
<a name="l01064"></a>01064 <span class="preprocessor">#endif</span>
<a name="l01065"></a>01065 <span class="preprocessor"></span>
<a name="l01066"></a>01066 <span class="preprocessor">#ifdef SELDON_WITH_SUPERLU</span>
<a name="l01067"></a>01067 <span class="preprocessor"></span>        mat_superlu.Clear();
<a name="l01068"></a>01068 <span class="preprocessor">#endif</span>
<a name="l01069"></a>01069 <span class="preprocessor"></span>
<a name="l01070"></a>01070 <span class="preprocessor">#ifdef SELDON_WITH_UMFPACK</span>
<a name="l01071"></a>01071 <span class="preprocessor"></span>        mat_umf.Clear();
<a name="l01072"></a>01072 <span class="preprocessor">#endif</span>
<a name="l01073"></a>01073 <span class="preprocessor"></span>
<a name="l01074"></a>01074 <span class="preprocessor">#ifdef SELDON_WITH_PASTIX</span>
<a name="l01075"></a>01075 <span class="preprocessor"></span>        mat_pastix.Clear();
<a name="l01076"></a>01076 <span class="preprocessor">#endif</span>
<a name="l01077"></a>01077 <span class="preprocessor"></span>
<a name="l01078"></a>01078 <span class="preprocessor">#ifdef SELDON_WITH_PRECONDITIONING</span>
<a name="l01079"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a72ddb935a03d6e507f4f0175d748c54d">01079</a> <span class="preprocessor"></span>        mat_ilut.Clear();
<a name="l01080"></a>01080 <span class="preprocessor">#endif</span>
<a name="l01081"></a>01081 <span class="preprocessor"></span>      }
<a name="l01082"></a>01082   }
<a name="l01083"></a>01083 
<a name="l01084"></a>01084 
<a name="l01086"></a>01086   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l01087"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#aa00bcb4a22c9ce619e88b4e1247023fc">01087</a>   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a72ddb935a03d6e507f4f0175d748c54d" title="Returns the number of rows of the factorized matrix.">SparseDirectSolver&lt;T&gt;::GetM</a>()<span class="keyword"> const</span>
<a name="l01088"></a>01088 <span class="keyword">  </span>{
<a name="l01089"></a>01089     <span class="keywordflow">return</span> n;
<a name="l01090"></a>01090   }
<a name="l01091"></a>01091 
<a name="l01092"></a>01092 
<a name="l01094"></a>01094   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l01095"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ab706f04923d61bd5316dd65d5c5a4286">01095</a>   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#aa00bcb4a22c9ce619e88b4e1247023fc" title="Returns the number of rows of the factorized matrix.">SparseDirectSolver&lt;T&gt;::GetN</a>()<span class="keyword"> const</span>
<a name="l01096"></a>01096 <span class="keyword">  </span>{
<a name="l01097"></a>01097     <span class="keywordflow">return</span> n;
<a name="l01098"></a>01098   }
<a name="l01099"></a>01099 
<a name="l01100"></a>01100 
<a name="l01102"></a>01102   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l01103"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a42cd77074d9e29b54946cc2e99826f71">01103</a>   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ab706f04923d61bd5316dd65d5c5a4286" title="Returns the ordering algorithm to use.">SparseDirectSolver&lt;T&gt;::GetTypeOrdering</a>()<span class="keyword"> const</span>
<a name="l01104"></a>01104 <span class="keyword">  </span>{
<a name="l01105"></a>01105     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ae9819b726220aa9599132e149a6416e6" title="Ordering to use.">type_ordering</a>;
<a name="l01106"></a>01106   }
<a name="l01107"></a>01107 
<a name="l01108"></a>01108 
<a name="l01110"></a>01110   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l01111"></a>01111   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a42cd77074d9e29b54946cc2e99826f71" title="Sets directly the new ordering (by giving a permutation vector).">SparseDirectSolver&lt;T&gt;::SetPermutation</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; num)
<a name="l01112"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#adc649792b3da7574a6340563a9bc7378">01112</a>   {
<a name="l01113"></a>01113     <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ae9819b726220aa9599132e149a6416e6" title="Ordering to use.">type_ordering</a> = SparseMatrixOrdering::USER;
<a name="l01114"></a>01114     <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a4e946689a26b4103f0551bd2e6cf96eb" title="Ordering (if supplied by the user).">permut</a> = num;
<a name="l01115"></a>01115   }
<a name="l01116"></a>01116 
<a name="l01117"></a>01117 
<a name="l01119"></a>01119   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l01120"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ad1d61abb463cc28f50e0acce15a41ce7">01120</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#adc649792b3da7574a6340563a9bc7378" title="Modifies the ordering algorithm to use.">SparseDirectSolver&lt;T&gt;::SelectOrdering</a>(<span class="keywordtype">int</span> type)
<a name="l01121"></a>01121   {
<a name="l01122"></a>01122     <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ae9819b726220aa9599132e149a6416e6" title="Ordering to use.">type_ordering</a> = type;
<a name="l01123"></a>01123   }
<a name="l01124"></a>01124 
<a name="l01125"></a>01125 
<a name="l01127"></a>01127   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l01128"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#aee5b1ccbb25a3dfb21a6ea3ed00b037e">01128</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ad1d61abb463cc28f50e0acce15a41ce7" title="Modifies the number of threads per node (for Pastix only).">SparseDirectSolver&lt;T&gt;::SetNumberThreadPerNode</a>(<span class="keywordtype">int</span> p)
<a name="l01129"></a>01129   {
<a name="l01130"></a>01130     <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a2b4fcbacb7f6fb5276fe33b36501e99c" title="Number of threads (for Pastix).">number_threads_per_node</a> = p;
<a name="l01131"></a>01131   }
<a name="l01132"></a>01132 
<a name="l01133"></a>01133 
<a name="l01135"></a>01135   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l01136"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a706d99d20b79c014b676cd91074860ee">01136</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#aee5b1ccbb25a3dfb21a6ea3ed00b037e" title="Modifies the direct solver to use.">SparseDirectSolver&lt;T&gt;::SelectDirectSolver</a>(<span class="keywordtype">int</span> type)
<a name="l01137"></a>01137   {
<a name="l01138"></a>01138     <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> = type;
<a name="l01139"></a>01139   }
<a name="l01140"></a>01140 
<a name="l01141"></a>01141 
<a name="l01143"></a>01143   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l01144"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a260c50fc0c60a95d6ee6636ff948c3a8">01144</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a706d99d20b79c014b676cd91074860ee" title="Enforces the use of unsymmetric algorithm for ilut solver.">SparseDirectSolver&lt;T&gt;::SetNonSymmetricIlut</a>()
<a name="l01145"></a>01145   {
<a name="l01146"></a>01146     <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a67010f0cc1e81d66504f3987385fb5f7" title="Use of non-symmetric ilut?">enforce_unsym_ilut</a> = <span class="keyword">true</span>;
<a name="l01147"></a>01147   }
<a name="l01148"></a>01148 
<a name="l01149"></a>01149 
<a name="l01151"></a>01151   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l01152"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a58aa5d9084aa9dd43ea66bf84c4d920a">01152</a>   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a260c50fc0c60a95d6ee6636ff948c3a8" title="Returns the direct solver to use.">SparseDirectSolver&lt;T&gt;::GetDirectSolver</a>()
<a name="l01153"></a>01153   {
<a name="l01154"></a>01154     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a>;
<a name="l01155"></a>01155   }
<a name="l01156"></a>01156 
<a name="l01157"></a>01157 
<a name="l01159"></a>01159   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l01160"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a2283e241ce581a0571761db3ff7b1498">01160</a>   <span class="keywordtype">double</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a58aa5d9084aa9dd43ea66bf84c4d920a" title="Returns threshold used for ilut (if this solver is selected).">SparseDirectSolver&lt;T&gt;::GetThresholdMatrix</a>()<span class="keyword"> const</span>
<a name="l01161"></a>01161 <span class="keyword">  </span>{
<a name="l01162"></a>01162     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a215ef20ce0827e0974cade309a9aaced" title="Threshold for ilut solver.">threshold_matrix</a>;
<a name="l01163"></a>01163   }
<a name="l01164"></a>01164 
<a name="l01165"></a>01165 
<a name="l01167"></a>01167   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> MatrixSparse&gt;
<a name="l01168"></a>01168   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a2283e241ce581a0571761db3ff7b1498" title="Computation of the permutation vector in order to reduce fill-in.">SparseDirectSolver&lt;T&gt;::ComputeOrdering</a>(MatrixSparse&amp; A)
<a name="l01169"></a>01169   {
<a name="l01170"></a>01170     <span class="keywordtype">bool</span> user_ordering = <span class="keyword">false</span>;
<a name="l01171"></a>01171     <span class="comment">// We set the ordering for each direct solver interfaced.</span>
<a name="l01172"></a>01172     <span class="keywordflow">switch</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ae9819b726220aa9599132e149a6416e6" title="Ordering to use.">type_ordering</a>)
<a name="l01173"></a>01173       {
<a name="l01174"></a>01174       <span class="keywordflow">case</span> SparseMatrixOrdering::AUTO :
<a name="l01175"></a>01175         {
<a name="l01176"></a>01176           <span class="comment">// We choose the default strategy proposed by the direct solver that</span>
<a name="l01177"></a>01177           <span class="comment">// will be called.</span>
<a name="l01178"></a>01178           <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == MUMPS)
<a name="l01179"></a>01179             {
<a name="l01180"></a>01180 <span class="preprocessor">#ifdef SELDON_WITH_MUMPS</span>
<a name="l01181"></a>01181 <span class="preprocessor"></span>              mat_mumps.SelectOrdering(7);
<a name="l01182"></a>01182 <span class="preprocessor">#endif</span>
<a name="l01183"></a>01183 <span class="preprocessor"></span>            }
<a name="l01184"></a>01184           <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == PASTIX)
<a name="l01185"></a>01185             {
<a name="l01186"></a>01186 <span class="preprocessor">#ifdef SELDON_WITH_PASTIX</span>
<a name="l01187"></a>01187 <span class="preprocessor"></span>              mat_pastix.SelectOrdering(API_ORDER_SCOTCH);
<a name="l01188"></a>01188 <span class="preprocessor">#endif</span>
<a name="l01189"></a>01189 <span class="preprocessor"></span>            }
<a name="l01190"></a>01190           <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == UMFPACK)
<a name="l01191"></a>01191             {
<a name="l01192"></a>01192 <span class="preprocessor">#ifdef SELDON_WITH_UMFPACK</span>
<a name="l01193"></a>01193 <span class="preprocessor"></span>              mat_umf.SelectOrdering(UMFPACK_ORDERING_AMD);
<a name="l01194"></a>01194 <span class="preprocessor">#endif</span>
<a name="l01195"></a>01195 <span class="preprocessor"></span>            }
<a name="l01196"></a>01196           <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == SUPERLU)
<a name="l01197"></a>01197             {
<a name="l01198"></a>01198 <span class="preprocessor">#ifdef SELDON_WITH_SUPERLU</span>
<a name="l01199"></a>01199 <span class="preprocessor"></span>              mat_superlu.SelectOrdering(COLAMD);
<a name="l01200"></a>01200 <span class="preprocessor">#endif</span>
<a name="l01201"></a>01201 <span class="preprocessor"></span>            }
<a name="l01202"></a>01202           <span class="keywordflow">else</span>
<a name="l01203"></a>01203             {
<a name="l01204"></a>01204 
<a name="l01205"></a>01205               <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ae9819b726220aa9599132e149a6416e6" title="Ordering to use.">type_ordering</a> = SparseMatrixOrdering::IDENTITY;
<a name="l01206"></a>01206 
<a name="l01207"></a>01207 <span class="preprocessor">#ifdef SELDON_WITH_UMFPACK</span>
<a name="l01208"></a>01208 <span class="preprocessor"></span>              <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ae9819b726220aa9599132e149a6416e6" title="Ordering to use.">type_ordering</a> = SparseMatrixOrdering::AMD;
<a name="l01209"></a>01209 <span class="preprocessor">#endif</span>
<a name="l01210"></a>01210 <span class="preprocessor"></span>
<a name="l01211"></a>01211 <span class="preprocessor">#ifdef SELDON_WITH_MUMPS</span>
<a name="l01212"></a>01212 <span class="preprocessor"></span>              <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ae9819b726220aa9599132e149a6416e6" title="Ordering to use.">type_ordering</a> = SparseMatrixOrdering::METIS;
<a name="l01213"></a>01213 <span class="preprocessor">#endif</span>
<a name="l01214"></a>01214 <span class="preprocessor"></span>
<a name="l01215"></a>01215 <span class="preprocessor">#ifdef SELDON_WITH_PASTIX</span>
<a name="l01216"></a>01216 <span class="preprocessor"></span>              <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ae9819b726220aa9599132e149a6416e6" title="Ordering to use.">type_ordering</a> = SparseMatrixOrdering::SCOTCH;
<a name="l01217"></a>01217 <span class="preprocessor">#endif</span>
<a name="l01218"></a>01218 <span class="preprocessor"></span>
<a name="l01219"></a>01219               user_ordering = <span class="keyword">true</span>;
<a name="l01220"></a>01220             }
<a name="l01221"></a>01221         }
<a name="l01222"></a>01222         <span class="keywordflow">break</span>;
<a name="l01223"></a>01223       <span class="keywordflow">case</span> SparseMatrixOrdering::IDENTITY :
<a name="l01224"></a>01224       <span class="keywordflow">case</span> SparseMatrixOrdering::REVERSE_CUTHILL_MCKEE :
<a name="l01225"></a>01225       <span class="keywordflow">case</span> SparseMatrixOrdering::USER :
<a name="l01226"></a>01226         {
<a name="l01227"></a>01227           user_ordering = <span class="keyword">true</span>;
<a name="l01228"></a>01228         }
<a name="l01229"></a>01229         <span class="keywordflow">break</span>;
<a name="l01230"></a>01230       <span class="keywordflow">case</span> SparseMatrixOrdering::PORD :
<a name="l01231"></a>01231       <span class="keywordflow">case</span> SparseMatrixOrdering::QAMD :
<a name="l01232"></a>01232         {
<a name="l01233"></a>01233           <span class="comment">// Mumps ordering.</span>
<a name="l01234"></a>01234           <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == MUMPS)
<a name="l01235"></a>01235             {
<a name="l01236"></a>01236 <span class="preprocessor">#ifdef SELDON_WITH_MUMPS</span>
<a name="l01237"></a>01237 <span class="preprocessor"></span>              <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ae9819b726220aa9599132e149a6416e6" title="Ordering to use.">type_ordering</a> == SparseMatrixOrdering::PORD)
<a name="l01238"></a>01238                 mat_mumps.SelectOrdering(4);
<a name="l01239"></a>01239               <span class="keywordflow">else</span>
<a name="l01240"></a>01240                 mat_mumps.SelectOrdering(6);
<a name="l01241"></a>01241 <span class="preprocessor">#endif</span>
<a name="l01242"></a>01242 <span class="preprocessor"></span>            }
<a name="l01243"></a>01243           <span class="keywordflow">else</span>
<a name="l01244"></a>01244             {
<a name="l01245"></a>01245               user_ordering = <span class="keyword">true</span>;
<a name="l01246"></a>01246             }
<a name="l01247"></a>01247         }
<a name="l01248"></a>01248         <span class="keywordflow">break</span>;
<a name="l01249"></a>01249       <span class="keywordflow">case</span> SparseMatrixOrdering::SCOTCH :
<a name="l01250"></a>01250         {
<a name="l01251"></a>01251           <span class="comment">// Available for Mumps and Pastix.</span>
<a name="l01252"></a>01252           <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == MUMPS)
<a name="l01253"></a>01253             {
<a name="l01254"></a>01254 <span class="preprocessor">#ifdef SELDON_WITH_MUMPS</span>
<a name="l01255"></a>01255 <span class="preprocessor"></span>              mat_mumps.SelectOrdering(3);
<a name="l01256"></a>01256 <span class="preprocessor">#endif</span>
<a name="l01257"></a>01257 <span class="preprocessor"></span>            }
<a name="l01258"></a>01258           <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == PASTIX)
<a name="l01259"></a>01259             {
<a name="l01260"></a>01260 <span class="preprocessor">#ifdef SELDON_WITH_SCOTCH</span>
<a name="l01261"></a>01261 <span class="preprocessor"></span>              mat_pastix.SelectOrdering(API_ORDER_SCOTCH);
<a name="l01262"></a>01262 <span class="preprocessor">#endif</span>
<a name="l01263"></a>01263 <span class="preprocessor"></span>            }
<a name="l01264"></a>01264           <span class="keywordflow">else</span>
<a name="l01265"></a>01265             {
<a name="l01266"></a>01266               user_ordering = <span class="keyword">true</span>;
<a name="l01267"></a>01267             }
<a name="l01268"></a>01268         }
<a name="l01269"></a>01269         <span class="keywordflow">break</span>;
<a name="l01270"></a>01270       <span class="keywordflow">case</span> SparseMatrixOrdering::METIS :
<a name="l01271"></a>01271         {
<a name="l01272"></a>01272           <span class="comment">// Available for Mumps and Pastix.</span>
<a name="l01273"></a>01273           <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == MUMPS)
<a name="l01274"></a>01274             {
<a name="l01275"></a>01275 <span class="preprocessor">#ifdef SELDON_WITH_MUMPS</span>
<a name="l01276"></a>01276 <span class="preprocessor"></span>              mat_mumps.SelectOrdering(5);
<a name="l01277"></a>01277 <span class="preprocessor">#endif</span>
<a name="l01278"></a>01278 <span class="preprocessor"></span>            }
<a name="l01279"></a>01279           <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == PASTIX)
<a name="l01280"></a>01280             {
<a name="l01281"></a>01281 <span class="preprocessor">#ifdef SELDON_WITH_SCOTCH</span>
<a name="l01282"></a>01282 <span class="preprocessor"></span>              mat_pastix.SelectOrdering(API_ORDER_METIS);
<a name="l01283"></a>01283 <span class="preprocessor">#endif</span>
<a name="l01284"></a>01284 <span class="preprocessor"></span>            }
<a name="l01285"></a>01285           <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == UMFPACK)
<a name="l01286"></a>01286             {
<a name="l01287"></a>01287 <span class="preprocessor">#ifdef SELDON_WITH_SCOTCH</span>
<a name="l01288"></a>01288 <span class="preprocessor"></span>              mat_umf.SelectOrdering(UMFPACK_ORDERING_METIS);
<a name="l01289"></a>01289 <span class="preprocessor">#endif</span>
<a name="l01290"></a>01290 <span class="preprocessor"></span>            }
<a name="l01291"></a>01291 
<a name="l01292"></a>01292           <span class="keywordflow">else</span>
<a name="l01293"></a>01293             {
<a name="l01294"></a>01294               user_ordering = <span class="keyword">true</span>;
<a name="l01295"></a>01295             }
<a name="l01296"></a>01296         }
<a name="l01297"></a>01297         <span class="keywordflow">break</span>;
<a name="l01298"></a>01298       <span class="keywordflow">case</span> SparseMatrixOrdering::AMD :
<a name="l01299"></a>01299       <span class="keywordflow">case</span> SparseMatrixOrdering::COLAMD :
<a name="l01300"></a>01300         {
<a name="l01301"></a>01301           <span class="comment">// Default ordering for UmfPack.</span>
<a name="l01302"></a>01302           <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == UMFPACK)
<a name="l01303"></a>01303             {
<a name="l01304"></a>01304 <span class="preprocessor">#ifdef SELDON_WITH_UMFPACK</span>
<a name="l01305"></a>01305 <span class="preprocessor"></span>              mat_umf.SelectOrdering(UMFPACK_ORDERING_AMD);
<a name="l01306"></a>01306 <span class="preprocessor">#endif</span>
<a name="l01307"></a>01307 <span class="preprocessor"></span>            }
<a name="l01308"></a>01308           <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == SUPERLU)
<a name="l01309"></a>01309             {
<a name="l01310"></a>01310 <span class="preprocessor">#ifdef SELDON_WITH_SUPERLU</span>
<a name="l01311"></a>01311 <span class="preprocessor"></span>              mat_superlu.SelectOrdering(COLAMD);
<a name="l01312"></a>01312 <span class="preprocessor">#endif</span>
<a name="l01313"></a>01313 <span class="preprocessor"></span>            }
<a name="l01314"></a>01314           <span class="keywordflow">else</span>
<a name="l01315"></a>01315             {
<a name="l01316"></a>01316               user_ordering = <span class="keyword">true</span>;
<a name="l01317"></a>01317             }
<a name="l01318"></a>01318         }
<a name="l01319"></a>01319         <span class="keywordflow">break</span>;
<a name="l01320"></a>01320       }
<a name="l01321"></a>01321 
<a name="l01322"></a>01322     <span class="keywordflow">if</span> (user_ordering)
<a name="l01323"></a>01323       {
<a name="l01324"></a>01324         <span class="comment">// Case where the ordering is not natively available in the direct</span>
<a name="l01325"></a>01325         <span class="comment">// solver computing separetely the ordering.</span>
<a name="l01326"></a>01326         <a class="code" href="namespace_seldon.php#a5fcb66be9f67a3a926c77bacfaa8b33c" title="Constructs an ordering for the factorization of a sparse matrix.">FindSparseOrdering</a>(A, <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a4e946689a26b4103f0551bd2e6cf96eb" title="Ordering (if supplied by the user).">permut</a>, <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#ae9819b726220aa9599132e149a6416e6" title="Ordering to use.">type_ordering</a>);
<a name="l01327"></a>01327 
<a name="l01328"></a>01328         <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == MUMPS)
<a name="l01329"></a>01329           {
<a name="l01330"></a>01330 <span class="preprocessor">#ifdef SELDON_WITH_MUMPS</span>
<a name="l01331"></a>01331 <span class="preprocessor"></span>            mat_mumps.SetPermutation(<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a4e946689a26b4103f0551bd2e6cf96eb" title="Ordering (if supplied by the user).">permut</a>);
<a name="l01332"></a>01332 <span class="preprocessor">#endif</span>
<a name="l01333"></a>01333 <span class="preprocessor"></span>          }
<a name="l01334"></a>01334         <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == PASTIX)
<a name="l01335"></a>01335           {
<a name="l01336"></a>01336 <span class="preprocessor">#ifdef SELDON_WITH_PASTIX</span>
<a name="l01337"></a>01337 <span class="preprocessor"></span>            mat_pastix.SetPermutation(<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a4e946689a26b4103f0551bd2e6cf96eb" title="Ordering (if supplied by the user).">permut</a>);
<a name="l01338"></a>01338 <span class="preprocessor">#endif</span>
<a name="l01339"></a>01339 <span class="preprocessor"></span>          }
<a name="l01340"></a>01340         <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == UMFPACK)
<a name="l01341"></a>01341           {
<a name="l01342"></a>01342 <span class="preprocessor">#ifdef SELDON_WITH_UMFPACK</span>
<a name="l01343"></a>01343 <span class="preprocessor"></span>            mat_umf.SetPermutation(<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a4e946689a26b4103f0551bd2e6cf96eb" title="Ordering (if supplied by the user).">permut</a>);
<a name="l01344"></a>01344 <span class="preprocessor">#endif</span>
<a name="l01345"></a>01345 <span class="preprocessor"></span>          }
<a name="l01346"></a>01346         <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == SUPERLU)
<a name="l01347"></a>01347           {
<a name="l01348"></a>01348 <span class="preprocessor">#ifdef SELDON_WITH_SUPERLU</span>
<a name="l01349"></a>01349 <span class="preprocessor"></span>            mat_superlu.SetPermutation(<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a4e946689a26b4103f0551bd2e6cf96eb" title="Ordering (if supplied by the user).">permut</a>);
<a name="l01350"></a>01350 <span class="preprocessor">#endif</span>
<a name="l01351"></a>01351 <span class="preprocessor"></span>          }
<a name="l01352"></a>01352         <span class="keywordflow">else</span>
<a name="l01353"></a>01353           {
<a name="l01354"></a>01354           }
<a name="l01355"></a>01355       }
<a name="l01356"></a>01356 
<a name="l01357"></a>01357   }
<a name="l01358"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#afba79fbe3b2e117a3235e9801db9e9f4">01358</a> 
<a name="l01359"></a>01359 
<a name="l01361"></a>01361 
<a name="l01365"></a>01365   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> MatrixSparse&gt;
<a name="l01366"></a>01366   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#afba79fbe3b2e117a3235e9801db9e9f4" title="Factorization of matrix A.">SparseDirectSolver&lt;T&gt;::Factorize</a>(MatrixSparse&amp; A, <span class="keywordtype">bool</span> keep_matrix)
<a name="l01367"></a>01367   {
<a name="l01368"></a>01368     <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a2283e241ce581a0571761db3ff7b1498" title="Computation of the permutation vector in order to reduce fill-in.">ComputeOrdering</a>(A);
<a name="l01369"></a>01369 
<a name="l01370"></a>01370     n = A.GetM();
<a name="l01371"></a>01371     <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == UMFPACK)
<a name="l01372"></a>01372       {
<a name="l01373"></a>01373 <span class="preprocessor">#ifdef SELDON_WITH_UMFPACK</span>
<a name="l01374"></a>01374 <span class="preprocessor"></span>        mat_umf.FactorizeMatrix(A, keep_matrix);
<a name="l01375"></a>01375 <span class="preprocessor">#else</span>
<a name="l01376"></a>01376 <span class="preprocessor"></span>        <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;SparseDirectSolver::Factorize(MatrixSparse&amp;, bool)&quot;</span>,
<a name="l01377"></a>01377                         <span class="stringliteral">&quot;Seldon was not compiled with UmfPack support.&quot;</span>);
<a name="l01378"></a>01378 <span class="preprocessor">#endif</span>
<a name="l01379"></a>01379 <span class="preprocessor"></span>      }
<a name="l01380"></a>01380     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == SUPERLU)
<a name="l01381"></a>01381       {
<a name="l01382"></a>01382 <span class="preprocessor">#ifdef SELDON_WITH_SUPERLU</span>
<a name="l01383"></a>01383 <span class="preprocessor"></span>        mat_superlu.FactorizeMatrix(A, keep_matrix);
<a name="l01384"></a>01384 <span class="preprocessor">#else</span>
<a name="l01385"></a>01385 <span class="preprocessor"></span>        <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;SparseDirectSolver::Factorize(MatrixSparse&amp;, bool)&quot;</span>,
<a name="l01386"></a>01386                         <span class="stringliteral">&quot;Seldon was not compiled with SuperLU support.&quot;</span>);
<a name="l01387"></a>01387 <span class="preprocessor">#endif</span>
<a name="l01388"></a>01388 <span class="preprocessor"></span>      }
<a name="l01389"></a>01389     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == MUMPS)
<a name="l01390"></a>01390       {
<a name="l01391"></a>01391 <span class="preprocessor">#ifdef SELDON_WITH_MUMPS</span>
<a name="l01392"></a>01392 <span class="preprocessor"></span>        mat_mumps.FactorizeMatrix(A, keep_matrix);
<a name="l01393"></a>01393 <span class="preprocessor">#else</span>
<a name="l01394"></a>01394 <span class="preprocessor"></span>        <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;SparseDirectSolver::Factorize(MatrixSparse&amp;, bool)&quot;</span>,
<a name="l01395"></a>01395                         <span class="stringliteral">&quot;Seldon was not compiled with Mumps support.&quot;</span>);
<a name="l01396"></a>01396 <span class="preprocessor">#endif</span>
<a name="l01397"></a>01397 <span class="preprocessor"></span>      }
<a name="l01398"></a>01398     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == PASTIX)
<a name="l01399"></a>01399       {
<a name="l01400"></a>01400 <span class="preprocessor">#ifdef SELDON_WITH_PASTIX</span>
<a name="l01401"></a>01401 <span class="preprocessor"></span>        mat_pastix.SetNumberThreadPerNode(<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a2b4fcbacb7f6fb5276fe33b36501e99c" title="Number of threads (for Pastix).">number_threads_per_node</a>);
<a name="l01402"></a>01402         mat_pastix.FactorizeMatrix(A, keep_matrix);
<a name="l01403"></a>01403 <span class="preprocessor">#else</span>
<a name="l01404"></a>01404 <span class="preprocessor"></span>        <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;SparseDirectSolver::Factorize(MatrixSparse&amp;, bool)&quot;</span>,
<a name="l01405"></a>01405                         <span class="stringliteral">&quot;Seldon was not compiled with Pastix support.&quot;</span>);
<a name="l01406"></a>01406 <span class="preprocessor">#endif</span>
<a name="l01407"></a>01407 <span class="preprocessor"></span>      }
<a name="l01408"></a>01408     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == ILUT)
<a name="l01409"></a>01409       {
<a name="l01410"></a>01410 <span class="preprocessor">#ifdef SELDON_WITH_PRECONDITIONING</span>
<a name="l01411"></a>01411 <span class="preprocessor"></span>        <span class="comment">// Setting some parameters.</span>
<a name="l01412"></a>01412         <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a67010f0cc1e81d66504f3987385fb5f7" title="Use of non-symmetric ilut?">enforce_unsym_ilut</a> || !<a class="code" href="namespace_seldon.php#aa92697611346050d5157af5e936def3d" title="returns true if the matrix is symmetric">IsSymmetricMatrix</a>(A))
<a name="l01413"></a>01413           mat_ilut.SetUnsymmetricAlgorithm();
<a name="l01414"></a>01414         <span class="keywordflow">else</span>
<a name="l01415"></a>01415           mat_ilut.SetSymmetricAlgorithm();
<a name="l01416"></a>01416 
<a name="l01417"></a>01417         <span class="comment">// Then performing factorization.</span>
<a name="l01418"></a>01418         mat_ilut.FactorizeMatrix(<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a4e946689a26b4103f0551bd2e6cf96eb" title="Ordering (if supplied by the user).">permut</a>, A, keep_matrix);
<a name="l01419"></a>01419 <span class="preprocessor">#else</span>
<a name="l01420"></a>01420 <span class="preprocessor"></span>        <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;SparseDirectSolver::Factorize(MatrixSparse&amp;, bool)&quot;</span>,
<a name="l01421"></a>01421                         <span class="stringliteral">&quot;Seldon was not compiled with the preconditioners.&quot;</span>);
<a name="l01422"></a>01422 <span class="preprocessor">#endif</span>
<a name="l01423"></a>01423 <span class="preprocessor"></span>      }
<a name="l01424"></a>01424     <span class="keywordflow">else</span>
<a name="l01425"></a>01425       {
<a name="l01426"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#abee36e87c1e23539fc099e871a7bf6d3">01426</a>         <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a18d4ec1c11f680b7a364757063bde92d" title="Default solver.">mat_seldon</a>.FactorizeMatrix(<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a4e946689a26b4103f0551bd2e6cf96eb" title="Ordering (if supplied by the user).">permut</a>, A);
<a name="l01427"></a>01427       }
<a name="l01428"></a>01428 
<a name="l01429"></a>01429   }
<a name="l01430"></a>01430 
<a name="l01431"></a>01431 
<a name="l01433"></a>01433   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T&gt;
<a name="l01434"></a>01434   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#abee36e87c1e23539fc099e871a7bf6d3" title="Returns error code of the direct solver (for Mumps only).">SparseDirectSolver&lt;T&gt;::GetInfoFactorization</a>(<span class="keywordtype">int</span>&amp; ierr)<span class="keyword"> const</span>
<a name="l01435"></a>01435 <span class="keyword">  </span>{
<a name="l01436"></a>01436     <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == UMFPACK)
<a name="l01437"></a>01437       {
<a name="l01438"></a>01438 <span class="preprocessor">#ifdef SELDON_WITH_UMFPACK</span>
<a name="l01439"></a>01439 <span class="preprocessor"></span>        ierr = mat_umf.GetInfoFactorization();
<a name="l01440"></a>01440         <span class="keywordflow">switch</span> (ierr)
<a name="l01441"></a>01441           {
<a name="l01442"></a>01442           <span class="keywordflow">case</span> UMFPACK_OK :
<a name="l01443"></a>01443             <span class="keywordflow">return</span> FACTO_OK;
<a name="l01444"></a>01444           <span class="keywordflow">case</span> UMFPACK_WARNING_singular_matrix :
<a name="l01445"></a>01445             <span class="keywordflow">return</span> NUMERICALLY_SINGULAR_MATRIX;
<a name="l01446"></a>01446           <span class="keywordflow">case</span> UMFPACK_ERROR_out_of_memory :
<a name="l01447"></a>01447             <span class="keywordflow">return</span> OUT_OF_MEMORY;
<a name="l01448"></a>01448           <span class="keywordflow">case</span> UMFPACK_ERROR_invalid_Numeric_object :
<a name="l01449"></a>01449           <span class="keywordflow">case</span> UMFPACK_ERROR_invalid_Symbolic_object :
<a name="l01450"></a>01450           <span class="keywordflow">case</span> UMFPACK_ERROR_argument_missing :
<a name="l01451"></a>01451           <span class="keywordflow">case</span> UMFPACK_ERROR_different_pattern :
<a name="l01452"></a>01452           <span class="keywordflow">case</span> UMFPACK_ERROR_invalid_system :
<a name="l01453"></a>01453             <span class="keywordflow">return</span> INVALID_ARGUMENT;
<a name="l01454"></a>01454           <span class="keywordflow">case</span> UMFPACK_ERROR_n_nonpositive :
<a name="l01455"></a>01455             <span class="keywordflow">return</span> INCORRECT_NUMBER_OF_ROWS;
<a name="l01456"></a>01456           <span class="keywordflow">case</span> UMFPACK_ERROR_invalid_matrix :
<a name="l01457"></a>01457             <span class="keywordflow">return</span> MATRIX_INDICES_INCORRECT;
<a name="l01458"></a>01458           <span class="keywordflow">case</span> UMFPACK_ERROR_invalid_permutation :
<a name="l01459"></a>01459             <span class="keywordflow">return</span> INVALID_PERMUTATION;
<a name="l01460"></a>01460           <span class="keywordflow">case</span> UMFPACK_ERROR_ordering_failed :
<a name="l01461"></a>01461             <span class="keywordflow">return</span> ORDERING_FAILED;
<a name="l01462"></a>01462           <span class="keywordflow">default</span> :
<a name="l01463"></a>01463             <span class="keywordflow">return</span> INTERNAL_ERROR;
<a name="l01464"></a>01464           }
<a name="l01465"></a>01465 <span class="preprocessor">#endif</span>
<a name="l01466"></a>01466 <span class="preprocessor"></span>      }
<a name="l01467"></a>01467     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == SUPERLU)
<a name="l01468"></a>01468       {
<a name="l01469"></a>01469 <span class="preprocessor">#ifdef SELDON_WITH_SUPERLU</span>
<a name="l01470"></a>01470 <span class="preprocessor"></span>        ierr = mat_superlu.GetInfoFactorization();
<a name="l01471"></a>01471         <span class="keywordflow">if</span> (ierr &gt; 0)
<a name="l01472"></a>01472           <span class="keywordflow">return</span> INTERNAL_ERROR;
<a name="l01473"></a>01473 <span class="preprocessor">#endif</span>
<a name="l01474"></a>01474 <span class="preprocessor"></span>      }
<a name="l01475"></a>01475     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == MUMPS)
<a name="l01476"></a>01476       {
<a name="l01477"></a>01477 <span class="preprocessor">#ifdef SELDON_WITH_MUMPS</span>
<a name="l01478"></a>01478 <span class="preprocessor"></span>        ierr = mat_mumps.GetInfoFactorization();
<a name="l01479"></a>01479         <span class="keywordflow">switch</span> (ierr)
<a name="l01480"></a>01480           {
<a name="l01481"></a>01481           <span class="keywordflow">case</span> -2 :
<a name="l01482"></a>01482             <span class="comment">// nz out of range</span>
<a name="l01483"></a>01483             <span class="keywordflow">return</span> MATRIX_INDICES_INCORRECT;
<a name="l01484"></a>01484           <span class="keywordflow">case</span> -3 :
<a name="l01485"></a>01485             <span class="comment">// invalid job number</span>
<a name="l01486"></a>01486             <span class="keywordflow">return</span> INVALID_ARGUMENT;
<a name="l01487"></a>01487           <span class="keywordflow">case</span> -4 :
<a name="l01488"></a>01488             <span class="comment">// invalid permutation</span>
<a name="l01489"></a>01489             <span class="keywordflow">return</span> INVALID_PERMUTATION;
<a name="l01490"></a>01490           <span class="keywordflow">case</span> -5 :
<a name="l01491"></a>01491             <span class="comment">// problem of real workspace allocation</span>
<a name="l01492"></a>01492             <span class="keywordflow">return</span> OUT_OF_MEMORY;
<a name="l01493"></a>01493           <span class="keywordflow">case</span> -6 :
<a name="l01494"></a>01494             <span class="comment">// structurally singular matrix</span>
<a name="l01495"></a>01495             <span class="keywordflow">return</span> STRUCTURALLY_SINGULAR_MATRIX;
<a name="l01496"></a>01496           <span class="keywordflow">case</span> -7 :
<a name="l01497"></a>01497             <span class="comment">// problem of integer workspace allocation</span>
<a name="l01498"></a>01498             <span class="keywordflow">return</span> OUT_OF_MEMORY;
<a name="l01499"></a>01499           <span class="keywordflow">case</span> -10 :
<a name="l01500"></a>01500             <span class="comment">// numerically singular matrix</span>
<a name="l01501"></a>01501             <span class="keywordflow">return</span> NUMERICALLY_SINGULAR_MATRIX;
<a name="l01502"></a>01502           <span class="keywordflow">case</span> -13 :
<a name="l01503"></a>01503             <span class="comment">// allocate failed</span>
<a name="l01504"></a>01504             <span class="keywordflow">return</span> OUT_OF_MEMORY;
<a name="l01505"></a>01505           <span class="keywordflow">case</span> -16 :
<a name="l01506"></a>01506             <span class="comment">// N out of range</span>
<a name="l01507"></a>01507             <span class="keywordflow">return</span> INCORRECT_NUMBER_OF_ROWS;
<a name="l01508"></a>01508           <span class="keywordflow">case</span> -22 :
<a name="l01509"></a>01509             <span class="comment">// invalid pointer</span>
<a name="l01510"></a>01510             <span class="keywordflow">return</span> INVALID_ARGUMENT;
<a name="l01511"></a>01511           <span class="keywordflow">case</span> 1 :
<a name="l01512"></a>01512             <span class="comment">// index out of range</span>
<a name="l01513"></a>01513             <span class="keywordflow">return</span> MATRIX_INDICES_INCORRECT;
<a name="l01514"></a>01514           <span class="keywordflow">case</span> 0 :
<a name="l01515"></a>01515             <span class="keywordflow">return</span> FACTO_OK;
<a name="l01516"></a>01516           <span class="keywordflow">default</span> :
<a name="l01517"></a>01517             <span class="keywordflow">return</span> INTERNAL_ERROR;
<a name="l01518"></a>01518           }
<a name="l01519"></a>01519 <span class="preprocessor">#endif</span>
<a name="l01520"></a>01520 <span class="preprocessor"></span>      }
<a name="l01521"></a>01521 
<a name="l01522"></a>01522     <span class="keywordflow">return</span> FACTO_OK;
<a name="l01523"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a63f7c474a05a6034c08ab44af925f55b">01523</a>   }
<a name="l01524"></a>01524 
<a name="l01525"></a>01525 
<a name="l01527"></a>01527 
<a name="l01530"></a>01530   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> Vector1&gt;
<a name="l01531"></a>01531   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a63f7c474a05a6034c08ab44af925f55b" title="x_solution is overwritten by solution of A x = b.">SparseDirectSolver&lt;T&gt;::Solve</a>(Vector1&amp; x_solution)
<a name="l01532"></a>01532   {
<a name="l01533"></a>01533     <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == UMFPACK)
<a name="l01534"></a>01534       {
<a name="l01535"></a>01535 <span class="preprocessor">#ifdef SELDON_WITH_UMFPACK</span>
<a name="l01536"></a>01536 <span class="preprocessor"></span>        <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">Seldon::SolveLU</a>(mat_umf, x_solution);
<a name="l01537"></a>01537 <span class="preprocessor">#else</span>
<a name="l01538"></a>01538 <span class="preprocessor"></span>        <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;SparseDirectSolver::Solve(Vector&amp;)&quot;</span>,
<a name="l01539"></a>01539                         <span class="stringliteral">&quot;Seldon was not compiled with UmfPack support.&quot;</span>);
<a name="l01540"></a>01540 <span class="preprocessor">#endif</span>
<a name="l01541"></a>01541 <span class="preprocessor"></span>      }
<a name="l01542"></a>01542     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == SUPERLU)
<a name="l01543"></a>01543       {
<a name="l01544"></a>01544 <span class="preprocessor">#ifdef SELDON_WITH_SUPERLU</span>
<a name="l01545"></a>01545 <span class="preprocessor"></span>        <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">Seldon::SolveLU</a>(mat_superlu, x_solution);
<a name="l01546"></a>01546 <span class="preprocessor">#else</span>
<a name="l01547"></a>01547 <span class="preprocessor"></span>        <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;SparseDirectSolver::Solve(Vector&amp;)&quot;</span>,
<a name="l01548"></a>01548                         <span class="stringliteral">&quot;Seldon was not compiled with SuperLU support.&quot;</span>);
<a name="l01549"></a>01549 <span class="preprocessor">#endif</span>
<a name="l01550"></a>01550 <span class="preprocessor"></span>      }
<a name="l01551"></a>01551     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == MUMPS)
<a name="l01552"></a>01552       {
<a name="l01553"></a>01553 <span class="preprocessor">#ifdef SELDON_WITH_MUMPS</span>
<a name="l01554"></a>01554 <span class="preprocessor"></span>        <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">Seldon::SolveLU</a>(mat_mumps, x_solution);
<a name="l01555"></a>01555 <span class="preprocessor">#else</span>
<a name="l01556"></a>01556 <span class="preprocessor"></span>        <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;SparseDirectSolver::Solve(Vector&amp;)&quot;</span>,
<a name="l01557"></a>01557                         <span class="stringliteral">&quot;Seldon was not compiled with Mumps support.&quot;</span>);
<a name="l01558"></a>01558 <span class="preprocessor">#endif</span>
<a name="l01559"></a>01559 <span class="preprocessor"></span>      }
<a name="l01560"></a>01560     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == PASTIX)
<a name="l01561"></a>01561       {
<a name="l01562"></a>01562 <span class="preprocessor">#ifdef SELDON_WITH_PASTIX</span>
<a name="l01563"></a>01563 <span class="preprocessor"></span>        <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">Seldon::SolveLU</a>(mat_pastix, x_solution);
<a name="l01564"></a>01564 <span class="preprocessor">#else</span>
<a name="l01565"></a>01565 <span class="preprocessor"></span>        <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;SparseDirectSolver::Solve(Vector&amp;)&quot;</span>,
<a name="l01566"></a>01566                         <span class="stringliteral">&quot;Seldon was not compiled with Pastix support.&quot;</span>);
<a name="l01567"></a>01567 <span class="preprocessor">#endif</span>
<a name="l01568"></a>01568 <span class="preprocessor"></span>      }
<a name="l01569"></a>01569     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == ILUT)
<a name="l01570"></a>01570       {
<a name="l01571"></a>01571 <span class="preprocessor">#ifdef SELDON_WITH_PRECONDITIONING</span>
<a name="l01572"></a>01572 <span class="preprocessor"></span>        mat_ilut.Solve(x_solution);
<a name="l01573"></a>01573 <span class="preprocessor">#else</span>
<a name="l01574"></a>01574 <span class="preprocessor"></span>        <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;SparseDirectSolver::Solve(Vector&amp;)&quot;</span>,
<a name="l01575"></a>01575                         <span class="stringliteral">&quot;Seldon was not compiled with the preconditioners.&quot;</span>);
<a name="l01576"></a>01576 <span class="preprocessor">#endif</span>
<a name="l01577"></a>01577 <span class="preprocessor"></span>      }
<a name="l01578"></a>01578     <span class="keywordflow">else</span>
<a name="l01579"></a>01579       {
<a name="l01580"></a><a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a278d2d4b4dd170ae821b7057385c2807">01580</a>         <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">Seldon::SolveLU</a>(<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a18d4ec1c11f680b7a364757063bde92d" title="Default solver.">mat_seldon</a>, x_solution);
<a name="l01581"></a>01581       }
<a name="l01582"></a>01582   }
<a name="l01583"></a>01583 
<a name="l01584"></a>01584 
<a name="l01586"></a>01586   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> TransStatus, <span class="keyword">class</span> Vector1&gt;
<a name="l01587"></a>01587   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a63f7c474a05a6034c08ab44af925f55b" title="x_solution is overwritten by solution of A x = b.">SparseDirectSolver&lt;T&gt;</a>
<a name="l01588"></a>01588 <a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a63f7c474a05a6034c08ab44af925f55b" title="x_solution is overwritten by solution of A x = b.">  ::Solve</a>(<span class="keyword">const</span> TransStatus&amp; TransA, Vector1&amp; x_solution)
<a name="l01589"></a>01589   {
<a name="l01590"></a>01590     <span class="keywordflow">if</span> (type_solver == UMFPACK)
<a name="l01591"></a>01591       {
<a name="l01592"></a>01592 <span class="preprocessor">#ifdef SELDON_WITH_UMFPACK</span>
<a name="l01593"></a>01593 <span class="preprocessor"></span>        <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">Seldon::SolveLU</a>(TransA, mat_umf, x_solution);
<a name="l01594"></a>01594 <span class="preprocessor">#else</span>
<a name="l01595"></a>01595 <span class="preprocessor"></span>        <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;SparseDirectSolver::Solve(TransStatus, Vector&amp;)&quot;</span>,
<a name="l01596"></a>01596                         <span class="stringliteral">&quot;Seldon was not compiled with UmpfPack support.&quot;</span>);
<a name="l01597"></a>01597 <span class="preprocessor">#endif</span>
<a name="l01598"></a>01598 <span class="preprocessor"></span>      }
<a name="l01599"></a>01599     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (type_solver == SUPERLU)
<a name="l01600"></a>01600       {
<a name="l01601"></a>01601 <span class="preprocessor">#ifdef SELDON_WITH_SUPERLU</span>
<a name="l01602"></a>01602 <span class="preprocessor"></span>        <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">Seldon::SolveLU</a>(TransA, mat_superlu, x_solution);
<a name="l01603"></a>01603 <span class="preprocessor">#else</span>
<a name="l01604"></a>01604 <span class="preprocessor"></span>        <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;SparseDirectSolver::Solve(TransStatus, Vector&amp;)&quot;</span>,
<a name="l01605"></a>01605                         <span class="stringliteral">&quot;Seldon was not compiled with SuperLU support.&quot;</span>);
<a name="l01606"></a>01606 <span class="preprocessor">#endif</span>
<a name="l01607"></a>01607 <span class="preprocessor"></span>      }
<a name="l01608"></a>01608     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (type_solver == MUMPS)
<a name="l01609"></a>01609       {
<a name="l01610"></a>01610 <span class="preprocessor">#ifdef SELDON_WITH_MUMPS</span>
<a name="l01611"></a>01611 <span class="preprocessor"></span>        <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">Seldon::SolveLU</a>(TransA, mat_mumps, x_solution);
<a name="l01612"></a>01612 <span class="preprocessor">#else</span>
<a name="l01613"></a>01613 <span class="preprocessor"></span>        <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;SparseDirectSolver::Solve(TransStatus, Vector&amp;)&quot;</span>,
<a name="l01614"></a>01614                         <span class="stringliteral">&quot;Seldon was not compiled with Mumps support.&quot;</span>);
<a name="l01615"></a>01615 <span class="preprocessor">#endif</span>
<a name="l01616"></a>01616 <span class="preprocessor"></span>      }
<a name="l01617"></a>01617     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (type_solver == PASTIX)
<a name="l01618"></a>01618       {
<a name="l01619"></a>01619 <span class="preprocessor">#ifdef SELDON_WITH_PASTIX</span>
<a name="l01620"></a>01620 <span class="preprocessor"></span>        <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">Seldon::SolveLU</a>(TransA, mat_pastix, x_solution);
<a name="l01621"></a>01621 <span class="preprocessor">#else</span>
<a name="l01622"></a>01622 <span class="preprocessor"></span>        <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;SparseDirectSolver::Solve(TransStatus, Vector&amp;)&quot;</span>,
<a name="l01623"></a>01623                         <span class="stringliteral">&quot;Seldon was not compiled with Pastix support.&quot;</span>);
<a name="l01624"></a>01624 <span class="preprocessor">#endif</span>
<a name="l01625"></a>01625 <span class="preprocessor"></span>      }
<a name="l01626"></a>01626     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (type_solver == ILUT)
<a name="l01627"></a>01627       {
<a name="l01628"></a>01628 <span class="preprocessor">#ifdef SELDON_WITH_PRECONDITIONING</span>
<a name="l01629"></a>01629 <span class="preprocessor"></span>        mat_ilut.Solve(TransA, x_solution);
<a name="l01630"></a>01630 <span class="preprocessor">#else</span>
<a name="l01631"></a>01631 <span class="preprocessor"></span>        <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;SparseDirectSolver::Solve(TransStatus, Vector&amp;)&quot;</span>,
<a name="l01632"></a>01632                         <span class="stringliteral">&quot;Seldon was not compiled with the preconditioners.&quot;</span>);
<a name="l01633"></a>01633 <span class="preprocessor">#endif</span>
<a name="l01634"></a>01634 <span class="preprocessor"></span>      }
<a name="l01635"></a>01635     <span class="keywordflow">else</span>
<a name="l01636"></a>01636       {
<a name="l01637"></a>01637         <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">Seldon::SolveLU</a>(TransA, mat_seldon, x_solution);
<a name="l01638"></a>01638       }
<a name="l01639"></a>01639   }
<a name="l01640"></a>01640 
<a name="l01641"></a>01641 
<a name="l01642"></a>01642 <span class="preprocessor">#ifdef SELDON_WITH_MPI</span>
<a name="l01643"></a>01643 <span class="preprocessor"></span>
<a name="l01644"></a>01644 
<a name="l01657"></a>01657   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> T<span class="keywordtype">int</span>&gt;
<a name="l01658"></a>01658   <span class="keywordtype">void</span> SparseDirectSolver&lt;T&gt;::
<a name="l01659"></a>01659   FactorizeDistributed(MPI::Comm&amp; comm_facto,
<a name="l01660"></a>01660                        Vector&lt;Tint&gt;&amp; Ptr, Vector&lt;Tint&gt;&amp; Row, Vector&lt;T&gt;&amp; Val,
<a name="l01661"></a>01661                        <span class="keyword">const</span> IVect&amp; glob_num, <span class="keywordtype">bool</span> sym, <span class="keywordtype">bool</span> keep_matrix)
<a name="l01662"></a>01662   {
<a name="l01663"></a>01663     n = Ptr.GetM()-1;
<a name="l01664"></a>01664     <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == MUMPS)
<a name="l01665"></a>01665       {
<a name="l01666"></a>01666 <span class="preprocessor">#ifdef SELDON_WITH_MUMPS</span>
<a name="l01667"></a>01667 <span class="preprocessor"></span>        mat_mumps.FactorizeDistributedMatrix(comm_facto, Ptr, Row, Val,
<a name="l01668"></a>01668                                              glob_num, sym, keep_matrix);
<a name="l01669"></a>01669 <span class="preprocessor">#else</span>
<a name="l01670"></a>01670 <span class="preprocessor"></span>        <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;SparseDirectSolver::FactorizeDistributed(MPI::Comm&amp;,&quot;</span>
<a name="l01671"></a>01671                         <span class="stringliteral">&quot; IVect&amp;, IVect&amp;, Vector&lt;T&gt;&amp;, IVect&amp;, bool, bool)&quot;</span>,
<a name="l01672"></a>01672                         <span class="stringliteral">&quot;Seldon was not compiled with Mumps support.&quot;</span>);
<a name="l01673"></a>01673 <span class="preprocessor">#endif</span>
<a name="l01674"></a>01674 <span class="preprocessor"></span>      }
<a name="l01675"></a>01675     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == PASTIX)
<a name="l01676"></a>01676       {
<a name="l01677"></a>01677 <span class="preprocessor">#ifdef SELDON_WITH_PASTIX</span>
<a name="l01678"></a>01678 <span class="preprocessor"></span>        mat_pastix.FactorizeDistributedMatrix(comm_facto, Ptr, Row, Val,
<a name="l01679"></a>01679                                               glob_num, sym, keep_matrix);
<a name="l01680"></a>01680 <span class="preprocessor">#else</span>
<a name="l01681"></a>01681 <span class="preprocessor"></span>        <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;SparseDirectSolver::FactorizeDistributed(MPI::Comm&amp;,&quot;</span>
<a name="l01682"></a>01682                         <span class="stringliteral">&quot; IVect&amp;, IVect&amp;, Vector&lt;T&gt;&amp;, IVect&amp;, bool, bool)&quot;</span>,
<a name="l01683"></a>01683                         <span class="stringliteral">&quot;Seldon was not compiled with Pastix support.&quot;</span>);
<a name="l01684"></a>01684 <span class="preprocessor">#endif</span>
<a name="l01685"></a>01685 <span class="preprocessor"></span>      }
<a name="l01686"></a>01686     <span class="keywordflow">else</span>
<a name="l01687"></a>01687       {
<a name="l01688"></a>01688         <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;SparseDirectSolver::FactorizeDistributed(MPI::Comm&amp;,&quot;</span>
<a name="l01689"></a>01689                         <span class="stringliteral">&quot; IVect&amp;, IVect&amp;, Vector&lt;T&gt;&amp;, IVect&amp;, bool, bool)&quot;</span>,
<a name="l01690"></a>01690                         <span class="stringliteral">&quot;The method is defined for Mumps and Pastix only.&quot;</span>);
<a name="l01691"></a>01691       }
<a name="l01692"></a>01692   }
<a name="l01693"></a>01693 
<a name="l01694"></a>01694 
<a name="l01696"></a>01696 
<a name="l01699"></a>01699   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> Vector1&gt;
<a name="l01700"></a>01700   <span class="keywordtype">void</span> SparseDirectSolver&lt;T&gt;::
<a name="l01701"></a>01701   SolveDistributed(MPI::Comm&amp; comm_facto, Vector1&amp; x_solution,
<a name="l01702"></a>01702                    <span class="keyword">const</span> IVect&amp; glob_number)
<a name="l01703"></a>01703   {
<a name="l01704"></a>01704     <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == MUMPS)
<a name="l01705"></a>01705       {
<a name="l01706"></a>01706 <span class="preprocessor">#ifdef SELDON_WITH_MUMPS</span>
<a name="l01707"></a>01707 <span class="preprocessor"></span>        mat_mumps.SolveDistributed(comm_facto, x_solution, glob_number);
<a name="l01708"></a>01708 <span class="preprocessor">#else</span>
<a name="l01709"></a>01709 <span class="preprocessor"></span>        <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;SparseDirectSolver::SolveDistributed(MPI::Comm&amp;,&quot;</span>
<a name="l01710"></a>01710                         <span class="stringliteral">&quot; Vector&amp;, IVect&amp;)&quot;</span>,
<a name="l01711"></a>01711                         <span class="stringliteral">&quot;Seldon was not compiled with Mumps support.&quot;</span>);
<a name="l01712"></a>01712 <span class="preprocessor">#endif</span>
<a name="l01713"></a>01713 <span class="preprocessor"></span>      }
<a name="l01714"></a>01714     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == PASTIX)
<a name="l01715"></a>01715       {
<a name="l01716"></a>01716 <span class="preprocessor">#ifdef SELDON_WITH_PASTIX</span>
<a name="l01717"></a>01717 <span class="preprocessor"></span>        mat_pastix.SolveDistributed(comm_facto, x_solution, glob_number);
<a name="l01718"></a>01718 <span class="preprocessor">#else</span>
<a name="l01719"></a>01719 <span class="preprocessor"></span>        <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;SparseDirectSolver::SolveDistributed(MPI::Comm&amp;,&quot;</span>
<a name="l01720"></a>01720                         <span class="stringliteral">&quot; Vector&amp;, IVect&amp;)&quot;</span>,
<a name="l01721"></a>01721                         <span class="stringliteral">&quot;Seldon was not compiled with Pastix support.&quot;</span>);
<a name="l01722"></a>01722 <span class="preprocessor">#endif</span>
<a name="l01723"></a>01723 <span class="preprocessor"></span>      }
<a name="l01724"></a>01724     <span class="keywordflow">else</span>
<a name="l01725"></a>01725       {
<a name="l01726"></a>01726         <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;SparseDirectSolver::SolveDistributed(MPI::Comm&amp;,&quot;</span>
<a name="l01727"></a>01727                         <span class="stringliteral">&quot; Vector&amp;, IVect&amp;)&quot;</span>,
<a name="l01728"></a>01728                         <span class="stringliteral">&quot;The method is defined for Mumps and Pastix only.&quot;</span>);
<a name="l01729"></a>01729       }
<a name="l01730"></a>01730 
<a name="l01731"></a>01731   }
<a name="l01732"></a>01732 
<a name="l01733"></a>01733 
<a name="l01739"></a>01739   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> TransStatus, <span class="keyword">class</span> Vector1&gt;
<a name="l01740"></a>01740   <span class="keywordtype">void</span> SparseDirectSolver&lt;T&gt;::
<a name="l01741"></a>01741   SolveDistributed(MPI::Comm&amp; comm_facto, <span class="keyword">const</span> TransStatus&amp; TransA,
<a name="l01742"></a>01742                    Vector1&amp; x_solution, <span class="keyword">const</span> IVect&amp; glob_number)
<a name="l01743"></a>01743   {
<a name="l01744"></a>01744     <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == MUMPS)
<a name="l01745"></a>01745       {
<a name="l01746"></a>01746 <span class="preprocessor">#ifdef SELDON_WITH_MUMPS</span>
<a name="l01747"></a>01747 <span class="preprocessor"></span>        mat_mumps.SolveDistributed(comm_facto, TransA, x_solution,
<a name="l01748"></a>01748                                    glob_number);
<a name="l01749"></a>01749 <span class="preprocessor">#else</span>
<a name="l01750"></a>01750 <span class="preprocessor"></span>        <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;SparseDirectSolver::SolveDistributed(TransStatus, &quot;</span>
<a name="l01751"></a>01751                         <span class="stringliteral">&quot;MPI::Comm&amp;, Vector&amp;, IVect&amp;)&quot;</span>,
<a name="l01752"></a>01752                         <span class="stringliteral">&quot;Seldon was not compiled with Mumps support.&quot;</span>);
<a name="l01753"></a>01753 <span class="preprocessor">#endif</span>
<a name="l01754"></a>01754 <span class="preprocessor"></span>      }
<a name="l01755"></a>01755     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_sparse_direct_solver.php#a8be09e641dce4b15ccf37be8f9b3baa6" title="Solver to use.">type_solver</a> == PASTIX)
<a name="l01756"></a>01756       {
<a name="l01757"></a>01757 <span class="preprocessor">#ifdef SELDON_WITH_PASTIX</span>
<a name="l01758"></a>01758 <span class="preprocessor"></span>        mat_pastix.SolveDistributed(comm_facto, TransA, x_solution, glob_number);
<a name="l01759"></a>01759 <span class="preprocessor">#else</span>
<a name="l01760"></a>01760 <span class="preprocessor"></span>        <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;SparseDirectSolver::SolveDistributed(TransStatus, &quot;</span>
<a name="l01761"></a>01761                         <span class="stringliteral">&quot;MPI::Comm&amp;, Vector&amp;, IVect&amp;)&quot;</span>,
<a name="l01762"></a>01762                         <span class="stringliteral">&quot;Seldon was not compiled with Pastix support.&quot;</span>);
<a name="l01763"></a>01763 <span class="preprocessor">#endif</span>
<a name="l01764"></a>01764 <span class="preprocessor"></span>      }
<a name="l01765"></a>01765     <span class="keywordflow">else</span>
<a name="l01766"></a>01766       {
<a name="l01767"></a>01767         <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;SparseDirectSolver::SolveDistributed(TransStatus, &quot;</span>
<a name="l01768"></a>01768                         <span class="stringliteral">&quot;MPI::Comm&amp;, Vector&amp;, IVect&amp;)&quot;</span>,
<a name="l01769"></a>01769                         <span class="stringliteral">&quot;The method is defined for Mumps and Pastix only.&quot;</span>);
<a name="l01770"></a>01770       }
<a name="l01771"></a>01771   }
<a name="l01772"></a>01772 <span class="preprocessor">#endif</span>
<a name="l01773"></a>01773 <span class="preprocessor"></span>
<a name="l01774"></a>01774 
<a name="l01775"></a>01775   <span class="comment">/*************************</span>
<a name="l01776"></a>01776 <span class="comment">   * Solve and SparseSolve *</span>
<a name="l01777"></a>01777 <span class="comment">   *************************/</span>
<a name="l01778"></a>01778 
<a name="l01779"></a>01779 
<a name="l01781"></a>01781 
<a name="l01788"></a>01788   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01789"></a>01789             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l01790"></a>01790   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a696a4c34a72d618cf68fde030966f48e" title="Solves a sparse linear system using LU factorization.">SparseSolve</a>(Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l01791"></a>01791                    Vector&lt;T1, Storage1, Allocator1&gt;&amp; Y)
<a name="l01792"></a>01792   {
<a name="l01793"></a>01793 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l01794"></a>01794 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, Y, <span class="stringliteral">&quot;SparseSolve(M, Y)&quot;</span>);
<a name="l01795"></a>01795 <span class="preprocessor">#endif</span>
<a name="l01796"></a>01796 <span class="preprocessor"></span>
<a name="l01797"></a>01797     SparseDirectSolver&lt;T0&gt; matrix_lu;
<a name="l01798"></a>01798 
<a name="l01799"></a>01799     matrix_lu.Factorize(M);
<a name="l01800"></a>01800     matrix_lu.Solve(Y);
<a name="l01801"></a>01801   }
<a name="l01802"></a>01802 
<a name="l01803"></a>01803 
<a name="l01805"></a>01805   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop0, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l01806"></a>01806   <span class="keywordtype">void</span> Solve(Matrix&lt;T, Prop0, ColSparse, Allocator0&gt;&amp; M,
<a name="l01807"></a>01807              Vector&lt;T, VectFull, Allocator1&gt;&amp; Y)
<a name="l01808"></a>01808   {
<a name="l01809"></a>01809     <a class="code" href="namespace_seldon.php#a696a4c34a72d618cf68fde030966f48e" title="Solves a sparse linear system using LU factorization.">SparseSolve</a>(M, Y);
<a name="l01810"></a>01810   }
<a name="l01811"></a>01811 
<a name="l01812"></a>01812 
<a name="l01814"></a>01814   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop0, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l01815"></a>01815   <span class="keywordtype">void</span> Solve(Matrix&lt;T, Prop0, RowSparse, Allocator0&gt;&amp; M,
<a name="l01816"></a>01816              Vector&lt;T, VectFull, Allocator1&gt;&amp; Y)
<a name="l01817"></a>01817   {
<a name="l01818"></a>01818     <a class="code" href="namespace_seldon.php#a696a4c34a72d618cf68fde030966f48e" title="Solves a sparse linear system using LU factorization.">SparseSolve</a>(M, Y);
<a name="l01819"></a>01819   }
<a name="l01820"></a>01820 
<a name="l01821"></a>01821 
<a name="l01822"></a>01822 }  <span class="comment">// namespace Seldon.</span>
<a name="l01823"></a>01823 
<a name="l01824"></a>01824 
<a name="l01825"></a>01825 <span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
