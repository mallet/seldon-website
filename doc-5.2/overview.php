<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Overview </h1>  </div>
</div>
<div class="contents">
<h2>Basic ideas</h2>
<p>Seldon provides matrix and vector structures (for numerical computations) which are not part of the C++ standard library. Those structures are a lot more convenient than basic arrays (like <code>float* vect = new float[5]</code> or <code>double mat[5][4]</code>). They can be resized, displayed, copied, automatically destroyed, etc. The use of Seldon is therefore easy. </p>
<h2>Example 1 - basic example</h2>
 <div style="text-align:right;"><a href="src/doc/example/basic_example.cpp">doc/example/basic_example.cpp</a></div>  <div class="fragment"><pre class="fragment"><span class="preprocessor">#define SELDON_DEBUG_LEVEL_3</span>
<span class="preprocessor"></span>
<span class="preprocessor">#include &quot;Seldon.hxx&quot;</span>
<span class="keyword">using namespace </span>Seldon;

<span class="keywordtype">int</span> main()
{

  TRY;

  Matrix&lt;double&gt; A(3, 3);
  Vector&lt;double&gt; U, V;

  A.SetIdentity();
  A(0, 1) = -1.0;

  U.Reallocate(A.GetN());
  U.Fill();

  U.Print();

  V.Reallocate(A.GetM());
  <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(2.0, A, U, V);

  cout &lt;&lt; V &lt;&lt; endl;

  END;

  <span class="keywordflow">return</span> 0;

}
</pre></div><p>The example above does the following: </p>
<ul>
<li>
<p class="startli">A "debug level" is defined. It defines the amount of checks that Seldon will perform. A high debugging level will force Seldon to check a lot of things (e.g. indices validity), while a low debugging level will lead to less checks but to a faster code.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli">Seldon is included. Its classes and functions are in the namespace <a class="el" href="namespace_seldon.php" title="Seldon namespace.">Seldon</a>.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli">In the main function, the whole is included in a <code>try</code> block (macros <code>TRY</code> and <code>END</code>). This is not required, but it may catch Seldon exceptions.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli">A matrix of size 3 by 3 if declared.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli">Two vectors are defined. Their sizes are unknown.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli">The matrix <code>A</code> is set to the identity. Then its element at (0, 1) (first row, second column) is set to -1.0.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>U</code> is reallocated so that its length is the number of columns of <code>A</code>. It is simply filled with 1, 2 and 3. Then it is displayed thanks to the method <code>Print</code>.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli">The next operation is simply: 2.0 x A x U -&gt; V. Notice that V is reallocated to have the right size; otherwise an exception would have been raised. If Blas was used, <code>Mlt</code> would call it.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli">The result <code>V</code> is displayed.</p>
<p class="endli"></p>
</li>
</ul>
<p>The output is: </p>
<div class="fragment"><pre class="fragment">
1       2       3
-2      4       6
</pre></div><h2>Example 2 - exceptions</h2>
 <div style="text-align:right;"><a href="src/doc/example/basic_example_exception.cpp">doc/example/basic_example_exception.cpp</a></div>  <div class="fragment"><pre class="fragment"><span class="preprocessor">#define SELDON_DEBUG_LEVEL_4</span>
<span class="preprocessor"></span>
<span class="preprocessor">#include &quot;Seldon.hxx&quot;</span>
<span class="keyword">using namespace </span>Seldon;

<span class="keywordtype">int</span> main()
{

  TRY;

  Matrix&lt;double&gt; A(3, 3);

  A.Zero();
  A(0, 3) = 2.0;

  END;

  cout &lt;&lt; <span class="stringliteral">&quot;The program should not reach this point...&quot;</span> &lt;&lt; endl;

  <span class="keywordflow">return</span> 0;

}
</pre></div><p>The example above does the following:</p>
<ul>
<li>
<p class="startli">A high debugging level is defined. For example, the validity of indices is checked at every access.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli">Within a try block, a matrix <code>A</code> of size 3 by 3 is defined.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>A</code> is set to 0.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli">One then tries to set to 2.0 the element in the first row and the fourth column. Obviously the column index is out of range. An exception is raised.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli">The exception is caught, and the lines following the try block (i.e. after <code>END</code>) are executed.</p>
<p class="endli"></p>
</li>
</ul>
<p>The output is: </p>
<div class="fragment"><pre class="fragment">
ERROR!
Column index out of range in Matrix_Pointers::operator().
   Index should be in [0, 2], but is equal to 3.
</pre></div> </div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
