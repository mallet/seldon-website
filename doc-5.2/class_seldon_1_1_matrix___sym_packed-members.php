<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>, including all inherited members.<table>
  <tr bgcolor="#f0f0f0"><td><b>access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>allocator_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a230f90f867e62f789bdfb7ea2310dc40">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#afcaec454efa976f5e61e9c6188df5ae7">Copy</a>(const Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>data_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>entry_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a8812e90aba4832686109be1f0094362c">Fill</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a0c98bdfc02b66c4c5d94d572bb46c9d3">Fill</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#aa5657b619208fb20d1091ffb34a6a354">FillRand</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a50fb81fc0ddaa05bee8e9934a1beca8f">Get</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#af85e9c2911cfc58102741472792c8675">Get</a>(int i, int j) const </td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a3fed51d6dd6d46d8e611ce98f9d8f724">GetDataSize</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">GetM</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">GetN</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">GetSize</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>m_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a47988d7972247286332e853c13bbc00b">Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ad2ea11b0880dc32ba9472da9310c332b">Matrix_Base</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline, explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a127e0229931486cea3e6007988b446a0">Matrix_Base</a>(const Matrix_Base&lt; T, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a03e8a4aea2ab3a95be4f8e12023217ae">Matrix_SymPacked</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a9e3f8d4a030e998880e185f265b0c912">Matrix_SymPacked</a>(int i, int j=0)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#ac06fa9e352440052fc5e4c8315688dee">Matrix_SymPacked</a>(const Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>n_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a17adea2b2fb65d8d26e851b558e284d7">Nullify</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a8ea7c92005432ab73b7b8fbbe37e4868">operator()</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a2323bcd2dcd381a1b02e5e6c308bb4f0">operator()</a>(int i, int j) const </td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a9a70002e76a6930a24aa0b724c6976a1">operator=</a>(const Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a63be5be398e6a3be1f028f6c6966766c">operator=</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a57a7db421db0df658c32ed661d935089">operator[]</a>(int i)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a7628441645b1367f95b15d1f77a208af">operator[]</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a6a31d1d0e30519f28a456fdd3e7a2a72">Print</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a99cb75b68a8f4542df790f105bdf438e">Print</a>(int a, int b, int m, int n) const </td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a65f75370393e9843e06e84dde1ab6ca1">Print</a>(int l) const </td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a29e41ce8ba3144c1a6982d55c035dfea">Read</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#ab52485427217c02d912ee5812dcbc4a9">Read</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a64167ea39ea64fe44d1d1e1c838b4276">ReadText</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#aab6dd6ad39b5cede189404b75b008c94">ReadText</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a22935d0bf71467744c3ee1ac505fe9e6">Reallocate</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a0db0736203c7e574e0fcdd931f889c41">Set</a>(int i, int j, const T &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SetData</b>(int i, int j, pointer data) (defined in <a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a86973f10c1197e1a62c2df95049f5c69">SetIdentity</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a8bcf6acc85a1b7d64735db18587896d9">Val</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#ae7199787c1e2ee658a1590e7e1dd45a8">Val</a>(int i, int j) const </td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>value_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a49cb3c589cb0ff56b187dec20f52d1af">Write</a>(string FileName) const </td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a286cd3b79c5ece685fc82afa068bf4a1">Write</a>(ostream &amp;FileStream) const </td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#ac0894fe788885ee8cc4bb73b862d9976">WriteText</a>(string FileName) const </td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#afe97f8a573b6b59e4d790b7da35a2e11">WriteText</a>(ostream &amp;FileStream) const </td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a2774131a1c9e73091962ed97259d312d">Zero</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af34a03c0cc56f757a83cd56f97c74ed8">~Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a5b43b96986b649ecab6baa564f58068f">~Matrix_SymPacked</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sym_packed.php">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
