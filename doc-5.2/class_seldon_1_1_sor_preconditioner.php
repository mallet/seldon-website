<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_sor_preconditioner.php">SorPreconditioner</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::SorPreconditioner&lt; T &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::SorPreconditioner" -->
<p><a href="class_seldon_1_1_sor_preconditioner-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad1755eda6403d0cc33f8cc30ffc038ae"></a><!-- doxytag: member="Seldon::SorPreconditioner::SorPreconditioner" ref="ad1755eda6403d0cc33f8cc30ffc038ae" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sor_preconditioner.php#ad1755eda6403d0cc33f8cc30ffc038ae">SorPreconditioner</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ade52c2a59dbdd186fad1963a9496be90"></a><!-- doxytag: member="Seldon::SorPreconditioner::InitSymmetricPreconditioning" ref="ade52c2a59dbdd186fad1963a9496be90" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>InitSymmetricPreconditioning</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5efefce3170822d17a8e910fd3ea50e9"></a><!-- doxytag: member="Seldon::SorPreconditioner::InitUnSymmetricPreconditioning" ref="a5efefce3170822d17a8e910fd3ea50e9" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>InitUnSymmetricPreconditioning</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4da28437b6722f41fcb3c021a1b4264e"></a><!-- doxytag: member="Seldon::SorPreconditioner::SetParameterRelaxation" ref="a4da28437b6722f41fcb3c021a1b4264e" args="(const T &amp;param)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetParameterRelaxation</b> (const T &amp;param)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae14c67c17fcde05e08a10bdc5900f7a9"></a><!-- doxytag: member="Seldon::SorPreconditioner::SetNumberIterations" ref="ae14c67c17fcde05e08a10bdc5900f7a9" args="(int nb_iterations)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetNumberIterations</b> (int nb_iterations)</td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="aba3a4a6c4b451382b4a850bde82a71a9"></a><!-- doxytag: member="Seldon::SorPreconditioner::Solve" ref="aba3a4a6c4b451382b4a850bde82a71a9" args="(const Matrix &amp;A, const Vector &amp;r, Vector &amp;z, bool init_guess_null=true)" -->
template&lt;class Vector , class Matrix &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sor_preconditioner.php#aba3a4a6c4b451382b4a850bde82a71a9">Solve</a> (const <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> &amp;A, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a> &amp;r, <a class="el" href="class_seldon_1_1_vector.php">Vector</a> &amp;z, bool init_guess_null=true)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Solves M z = r. <br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="aa39ee94b64ba1d3d2da2d6fa70a3df05"></a><!-- doxytag: member="Seldon::SorPreconditioner::TransSolve" ref="aa39ee94b64ba1d3d2da2d6fa70a3df05" args="(const Matrix &amp;A, const Vector &amp;r, Vector &amp;z, bool init_guess_null=true)" -->
template&lt;class Vector , class Matrix &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sor_preconditioner.php#aa39ee94b64ba1d3d2da2d6fa70a3df05">TransSolve</a> (const <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> &amp;A, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a> &amp;r, <a class="el" href="class_seldon_1_1_vector.php">Vector</a> &amp;z, bool init_guess_null=true)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Solves M^t z = r. <br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4e58db67067834701f1080f81fb2442d"></a><!-- doxytag: member="Seldon::SorPreconditioner::symmetric_precond" ref="a4e58db67067834701f1080f81fb2442d" args="" -->
bool&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sor_preconditioner.php#a4e58db67067834701f1080f81fb2442d">symmetric_precond</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">true for <a class="el" href="class_seldon_1_1_symmetric.php">Symmetric</a> relaxation <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3b3bd355ce1b08110da7483a0b4944fe"></a><!-- doxytag: member="Seldon::SorPreconditioner::nb_iter" ref="a3b3bd355ce1b08110da7483a0b4944fe" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sor_preconditioner.php#a3b3bd355ce1b08110da7483a0b4944fe">nb_iter</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">number of iterations <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a38fb6b7c7b21677ee0042e1c3b5f4d6f"></a><!-- doxytag: member="Seldon::SorPreconditioner::omega" ref="a38fb6b7c7b21677ee0042e1c3b5f4d6f" args="" -->
T&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_sor_preconditioner.php#a38fb6b7c7b21677ee0042e1c3b5f4d6f">omega</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">relaxation parameter <br/></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T&gt;<br/>
 class Seldon::SorPreconditioner&lt; T &gt;</h3>


<p>Definition at line <a class="el" href="_precond___ssor_8cxx_source.php#l00025">25</a> of file <a class="el" href="_precond___ssor_8cxx_source.php">Precond_Ssor.cxx</a>.</p>
<hr/>The documentation for this class was generated from the following file:<ul>
<li>computation/solver/preconditioner/<a class="el" href="_precond___ssor_8cxx_source.php">Precond_Ssor.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
