<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix/Matrix_Base.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_BASE_CXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 <span class="preprocessor">#include &quot;Matrix_Base.hxx&quot;</span>
<a name="l00023"></a>00023 
<a name="l00024"></a>00024 <span class="keyword">namespace </span>Seldon
<a name="l00025"></a>00025 {
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 
<a name="l00028"></a>00028   <span class="comment">/****************</span>
<a name="l00029"></a>00029 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00030"></a>00030 <span class="comment">   ****************/</span>
<a name="l00031"></a>00031 
<a name="l00032"></a>00032 
<a name="l00034"></a>00034 
<a name="l00037"></a>00037   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00038"></a>00038   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___base.php#a47988d7972247286332e853c13bbc00b" title="Default constructor.">Matrix_Base&lt;T, Allocator&gt;::Matrix_Base</a>()
<a name="l00039"></a>00039   {
<a name="l00040"></a>00040     m_ = 0;
<a name="l00041"></a>00041     n_ = 0;
<a name="l00042"></a>00042     data_ = NULL;
<a name="l00043"></a>00043   }
<a name="l00044"></a>00044 
<a name="l00045"></a>00045 
<a name="l00047"></a>00047 
<a name="l00052"></a>00052   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00053"></a>00053   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___base.php#a47988d7972247286332e853c13bbc00b" title="Default constructor.">Matrix_Base&lt;T, Allocator&gt;::Matrix_Base</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00054"></a>00054   {
<a name="l00055"></a>00055 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00056"></a>00056 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || j &lt; 0)
<a name="l00057"></a>00057       <span class="keywordflow">throw</span> WrongDim(<span class="stringliteral">&quot;Matrix_Base::Matrix_Base(int, int)&quot;</span>,
<a name="l00058"></a>00058                      <span class="stringliteral">&quot;Unable to define a matrix with size &quot;</span>
<a name="l00059"></a>00059                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00060"></a>00060 <span class="preprocessor">#endif</span>
<a name="l00061"></a>00061 <span class="preprocessor"></span>
<a name="l00062"></a>00062     m_ = i;
<a name="l00063"></a>00063     n_ = j;
<a name="l00064"></a>00064     data_ = NULL;
<a name="l00065"></a>00065   }
<a name="l00066"></a>00066 
<a name="l00067"></a>00067 
<a name="l00069"></a>00069 
<a name="l00073"></a><a class="code" href="class_seldon_1_1_matrix___base.php#a127e0229931486cea3e6007988b446a0">00073</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00074"></a>00074   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___base.php#a47988d7972247286332e853c13bbc00b" title="Default constructor.">Matrix_Base&lt;T, Allocator&gt;::</a>
<a name="l00075"></a>00075 <a class="code" href="class_seldon_1_1_matrix___base.php#a47988d7972247286332e853c13bbc00b" title="Default constructor.">  Matrix_Base</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base&lt;T, Allocator&gt;</a>&amp; A)
<a name="l00076"></a>00076   {
<a name="l00077"></a>00077     m_ = A.<a class="code" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927" title="Returns the number of rows.">GetM</a>();
<a name="l00078"></a>00078     n_ = A.<a class="code" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984" title="Returns the number of columns.">GetN</a>();
<a name="l00079"></a>00079     data_ = NULL;
<a name="l00080"></a>00080   }
<a name="l00081"></a>00081 
<a name="l00082"></a>00082 
<a name="l00083"></a>00083   <span class="comment">/**************</span>
<a name="l00084"></a>00084 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00085"></a>00085 <span class="comment">   **************/</span>
<a name="l00086"></a>00086 
<a name="l00087"></a>00087 
<a name="l00089"></a>00089 
<a name="l00092"></a>00092   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00093"></a>00093   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___base.php#af34a03c0cc56f757a83cd56f97c74ed8" title="Destructor.">Matrix_Base&lt;T, Allocator&gt;::~Matrix_Base</a>()
<a name="l00094"></a>00094   {
<a name="l00095"></a>00095 
<a name="l00096"></a>00096   }
<a name="l00097"></a>00097 
<a name="l00098"></a>00098 
<a name="l00099"></a>00099   <span class="comment">/*******************</span>
<a name="l00100"></a>00100 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00101"></a>00101 <span class="comment">   *******************/</span>
<a name="l00102"></a>00102 
<a name="l00103"></a>00103 
<a name="l00105"></a>00105 
<a name="l00108"></a>00108   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00109"></a>00109   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927" title="Returns the number of rows.">Matrix_Base&lt;T, Allocator&gt;::GetM</a>()<span class="keyword"> const</span>
<a name="l00110"></a>00110 <span class="keyword">  </span>{
<a name="l00111"></a>00111     <span class="keywordflow">return</span> m_;
<a name="l00112"></a>00112   }
<a name="l00113"></a>00113 
<a name="l00114"></a>00114 
<a name="l00116"></a>00116 
<a name="l00119"></a>00119   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00120"></a>00120   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984" title="Returns the number of columns.">Matrix_Base&lt;T, Allocator&gt;::GetN</a>()<span class="keyword"> const</span>
<a name="l00121"></a>00121 <span class="keyword">  </span>{
<a name="l00122"></a>00122     <span class="keywordflow">return</span> n_;
<a name="l00123"></a>00123   }
<a name="l00124"></a>00124 
<a name="l00125"></a>00125 
<a name="l00127"></a>00127 
<a name="l00131"></a>00131   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00132"></a>00132   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927" title="Returns the number of rows.">Matrix_Base&lt;T, Allocator&gt;::GetM</a>(<span class="keyword">const</span> SeldonTranspose&amp; status)<span class="keyword"> const</span>
<a name="l00133"></a>00133 <span class="keyword">  </span>{
<a name="l00134"></a>00134     <span class="keywordflow">if</span> (status.NoTrans())
<a name="l00135"></a>00135       <span class="keywordflow">return</span> m_;
<a name="l00136"></a>00136     <span class="keywordflow">else</span>
<a name="l00137"></a>00137       <span class="keywordflow">return</span> n_;
<a name="l00138"></a>00138   }
<a name="l00139"></a>00139 
<a name="l00140"></a>00140 
<a name="l00142"></a>00142 
<a name="l00146"></a>00146   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00147"></a>00147   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984" title="Returns the number of columns.">Matrix_Base&lt;T, Allocator&gt;::GetN</a>(<span class="keyword">const</span> SeldonTranspose&amp; status)<span class="keyword"> const</span>
<a name="l00148"></a>00148 <span class="keyword">  </span>{
<a name="l00149"></a>00149     <span class="keywordflow">if</span> (status.NoTrans())
<a name="l00150"></a>00150       <span class="keywordflow">return</span> n_;
<a name="l00151"></a>00151     <span class="keywordflow">else</span>
<a name="l00152"></a>00152       <span class="keywordflow">return</span> m_;
<a name="l00153"></a>00153   }
<a name="l00154"></a>00154 
<a name="l00155"></a>00155 
<a name="l00156"></a>00156 <span class="preprocessor">#ifdef SELDON_WITH_BLAS</span>
<a name="l00157"></a>00157 <span class="preprocessor"></span>
<a name="l00158"></a>00158 
<a name="l00162"></a>00162   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00163"></a>00163   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927" title="Returns the number of rows.">Matrix_Base&lt;T, Allocator&gt;::GetM</a>(<span class="keyword">const</span> CBLAS_TRANSPOSE&amp; status)<span class="keyword"> const</span>
<a name="l00164"></a>00164 <span class="keyword">  </span>{
<a name="l00165"></a>00165     <span class="keywordflow">if</span> (status == CblasNoTrans)
<a name="l00166"></a>00166       <span class="keywordflow">return</span> m_;
<a name="l00167"></a>00167     <span class="keywordflow">else</span>
<a name="l00168"></a>00168       <span class="keywordflow">return</span> n_;
<a name="l00169"></a>00169   }
<a name="l00170"></a>00170 <span class="preprocessor">#endif</span>
<a name="l00171"></a>00171 <span class="preprocessor"></span>
<a name="l00172"></a>00172 
<a name="l00173"></a>00173 <span class="preprocessor">#ifdef SELDON_WITH_BLAS</span>
<a name="l00174"></a>00174 <span class="preprocessor"></span>
<a name="l00175"></a>00175 
<a name="l00179"></a>00179   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00180"></a>00180   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984" title="Returns the number of columns.">Matrix_Base&lt;T, Allocator&gt;::GetN</a>(<span class="keyword">const</span> CBLAS_TRANSPOSE&amp; status)<span class="keyword"> const</span>
<a name="l00181"></a>00181 <span class="keyword">  </span>{
<a name="l00182"></a>00182     <span class="keywordflow">if</span> (status == CblasNoTrans)
<a name="l00183"></a>00183       <span class="keywordflow">return</span> n_;
<a name="l00184"></a>00184     <span class="keywordflow">else</span>
<a name="l00185"></a>00185       <span class="keywordflow">return</span> m_;
<a name="l00186"></a>00186   }
<a name="l00187"></a>00187 <span class="preprocessor">#endif</span>
<a name="l00188"></a>00188 <span class="preprocessor"></span>
<a name="l00189"></a>00189 
<a name="l00191"></a>00191 
<a name="l00196"></a>00196   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00197"></a>00197   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0" title="Returns the number of elements in the matrix.">Matrix_Base&lt;T, Allocator&gt;::GetSize</a>()<span class="keyword"> const</span>
<a name="l00198"></a>00198 <span class="keyword">  </span>{
<a name="l00199"></a>00199     <span class="keywordflow">return</span> m_ * n_;
<a name="l00200"></a>00200   }
<a name="l00201"></a>00201 
<a name="l00202"></a>00202 
<a name="l00204"></a>00204 
<a name="l00208"></a><a class="code" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">00208</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00209"></a>00209   <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base&lt;T, Allocator&gt;::pointer</a>
<a name="l00210"></a>00210   <a class="code" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a" title="Returns a pointer to the data array.">Matrix_Base&lt;T, Allocator&gt;::GetData</a>()<span class="keyword"> const</span>
<a name="l00211"></a>00211 <span class="keyword">  </span>{
<a name="l00212"></a>00212     <span class="keywordflow">return</span> data_;
<a name="l00213"></a>00213   }
<a name="l00214"></a>00214 
<a name="l00215"></a>00215 
<a name="l00217"></a>00217 
<a name="l00221"></a><a class="code" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">00221</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00222"></a>00222   <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base&lt;T, Allocator&gt;::const_pointer</a>
<a name="l00223"></a>00223   <a class="code" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf" title="Returns a const pointer to the data array.">Matrix_Base&lt;T, Allocator&gt;::GetDataConst</a>()<span class="keyword"> const</span>
<a name="l00224"></a>00224 <span class="keyword">  </span>{
<a name="l00225"></a>00225     <span class="keywordflow">return</span> <span class="keyword">reinterpret_cast&lt;</span>typename <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T,
<a name="l00226"></a>00226       Allocator<span class="keyword">&gt;</span>::const_pointer&gt;(data_);
<a name="l00227"></a>00227   }
<a name="l00228"></a>00228 
<a name="l00229"></a>00229 
<a name="l00231"></a>00231 
<a name="l00235"></a>00235   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00236"></a>00236   <span class="keywordtype">void</span>* <a class="code" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc" title="Returns a pointer of type &amp;quot;void*&amp;quot; to the data array.">Matrix_Base&lt;T, Allocator&gt;::GetDataVoid</a>()<span class="keyword"> const</span>
<a name="l00237"></a>00237 <span class="keyword">  </span>{
<a name="l00238"></a>00238     <span class="keywordflow">return</span> <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">void</span>*<span class="keyword">&gt;</span>(data_);
<a name="l00239"></a>00239   }
<a name="l00240"></a>00240 
<a name="l00241"></a>00241 
<a name="l00243"></a>00243 
<a name="l00248"></a>00248   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00249"></a>00249   <span class="keyword">const</span> <span class="keywordtype">void</span>* <a class="code" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639" title="Returns a pointer of type &amp;quot;const void*&amp;quot; to the data array.">Matrix_Base&lt;T, Allocator&gt;::GetDataConstVoid</a>()<span class="keyword"> const</span>
<a name="l00250"></a>00250 <span class="keyword">  </span>{
<a name="l00251"></a>00251     <span class="keywordflow">return</span> <span class="keyword">reinterpret_cast&lt;</span><span class="keyword">const </span><span class="keywordtype">void</span>*<span class="keyword">&gt;</span>(data_);
<a name="l00252"></a>00252   }
<a name="l00253"></a>00253 
<a name="l00254"></a>00254 
<a name="l00256"></a>00256 
<a name="l00259"></a>00259   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00260"></a>00260   Allocator&amp; <a class="code" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691" title="Returns the allocator of the matrix.">Matrix_Base&lt;T, Allocator&gt;::GetAllocator</a>()
<a name="l00261"></a>00261   {
<a name="l00262"></a>00262     <span class="keywordflow">return</span> allocator_;
<a name="l00263"></a>00263   }
<a name="l00264"></a>00264 
<a name="l00265"></a>00265 
<a name="l00266"></a>00266   <span class="comment">// operator&lt;&lt; overloaded for matrices.</span>
<a name="l00272"></a>00272 <span class="comment"></span>  <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00273"></a>00273   ostream&amp; <a class="code" href="namespace_seldon.php#a5bd4a6d6f3c83048663d57d2fc032107" title="operator&amp;lt;&amp;lt; overloaded for a 3D array.">operator &lt;&lt; </a>(ostream&amp; out,
<a name="l00274"></a>00274                         <span class="keyword">const</span> Matrix&lt;T, Prop, Storage, Allocator&gt;&amp; A)
<a name="l00275"></a>00275   {
<a name="l00276"></a>00276 
<a name="l00277"></a>00277     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetM(); i++)
<a name="l00278"></a>00278       {
<a name="l00279"></a>00279         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetN(); j++)
<a name="l00280"></a>00280           out &lt;&lt; A(i, j) &lt;&lt; <span class="charliteral">&#39;\t&#39;</span>;
<a name="l00281"></a>00281         out &lt;&lt; endl;
<a name="l00282"></a>00282       }
<a name="l00283"></a>00283 
<a name="l00284"></a>00284     <span class="keywordflow">return</span> out;
<a name="l00285"></a>00285 
<a name="l00286"></a>00286   }
<a name="l00287"></a>00287 
<a name="l00288"></a>00288 
<a name="l00289"></a>00289 } <span class="comment">// namespace Seldon.</span>
<a name="l00290"></a>00290 
<a name="l00291"></a>00291 <span class="preprocessor">#define SELDON_FILE_MATRIX_BASE_CXX</span>
<a name="l00292"></a>00292 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
