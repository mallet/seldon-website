<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php">Vector&lt; T, PETScPar, Allocator &gt;</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pub-attribs">Public Attributes</a> &#124;
<a href="#pro-attribs">Protected Attributes</a> &#124;
<a href="#pro-static-attribs">Static Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Vector&lt; T, PETScPar, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Vector&lt; T, PETScPar, Allocator &gt;" --><!-- doxytag: inherits="Seldon::PETScVector" --><div class="dynheader">
Inheritance diagram for Seldon::Vector&lt; T, PETScPar, Allocator &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.png" usemap="#Seldon::Vector&lt; T, PETScPar, Allocator &gt;_map" alt=""/>
  <map id="Seldon::Vector&lt; T, PETScPar, Allocator &gt;_map" name="Seldon::Vector&lt; T, PETScPar, Allocator &gt;_map">
<area href="class_seldon_1_1_p_e_t_sc_vector.php" alt="Seldon::PETScVector&lt; T, Allocator &gt;" shape="rect" coords="0,56,253,80"/>
<area href="class_seldon_1_1_vector___base.php" alt="Seldon::Vector_Base&lt; T, Allocator &gt;" shape="rect" coords="0,0,253,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad84792c92c27f6ae6f8009b779b51d20"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::value_type" ref="ad84792c92c27f6ae6f8009b779b51d20" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a635c424b06d6b5559b493f8e2e35c88b"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::pointer" ref="a635c424b06d6b5559b493f8e2e35c88b" args="" -->
typedef Allocator::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aee484302370352dbf6134adf7d79c3f2"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::const_pointer" ref="aee484302370352dbf6134adf7d79c3f2" args="" -->
typedef Allocator::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9e38d252005abcc9bd4967473638d58b"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::reference" ref="a9e38d252005abcc9bd4967473638d58b" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a643d558a5f9d3925804fdec9327be425"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::const_reference" ref="a643d558a5f9d3925804fdec9327be425" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af443acb1a7d1f68e60978a7d21d0f608"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::storage" ref="af443acb1a7d1f68e60978a7d21d0f608" args="" -->
typedef <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>&nbsp;</td><td class="memItemRight" valign="bottom"><b>storage</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#af2ab08543ecdfb8a09d01e4c024607ff">Vector</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#af2ab08543ecdfb8a09d01e4c024607ff"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a58773b566b67c3bc9830a56f5a4e0499">Vector</a> (int i, MPI_Comm mpi_communicator=MPI_COMM_WORLD)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Main constructor.  <a href="#a58773b566b67c3bc9830a56f5a4e0499"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a93033ed7da4e225e1cc183fb963a1315">Vector</a> (int i, int Nlocal, MPI_Comm mpi_communicator)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Main constructor.  <a href="#a93033ed7da4e225e1cc183fb963a1315"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a1c6444722ded7b4443326eeaa32ebe1c">Vector</a> (Vec &amp;petsc_vector)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copy constructor.  <a href="#a1c6444722ded7b4443326eeaa32ebe1c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#aa74b1bffa16b72281d8fe517cd883d48">Vector</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copy constructor.  <a href="#aa74b1bffa16b72281d8fe517cd883d48"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aea88a28edc2030920f93f83ac1f4952f"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::~Vector" ref="aea88a28edc2030920f93f83ac1f4952f" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#aea88a28edc2030920f93f83ac1f4952f">~Vector</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Destructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a9ee7458d06d687a0078b368d1788738a">Copy</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt; &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a vector.  <a href="#a9ee7458d06d687a0078b368d1788738a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#ad806a5711dd4273454143fc6f52d0405">Copy</a> (const Vec &amp;petsc_vector)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a vector.  <a href="#ad806a5711dd4273454143fc6f52d0405"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#abc4329f40632d4f9cfa083d314b45f4d">Reallocate</a> (int i, int local_size=PETSC_DECIDE)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight"><a class="el" href="class_seldon_1_1_vector.php">Vector</a> reallocation.  <a href="#abc4329f40632d4f9cfa083d314b45f4d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a2251829a7141e9b02e2fef218ad32675">operator=</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt; &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a vector (assignment operator).  <a href="#a2251829a7141e9b02e2fef218ad32675"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt; &amp;&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#ad8ce057df39b8cdee03515c7aeca3964">operator=</a> (const T0 &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the vector with a given value.  <a href="#ad8ce057df39b8cdee03515c7aeca3964"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt; &amp;&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#ac48afbd3ef8035d87ec1f1be1de7ed5e">operator*=</a> (const T0 &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Multiplies a vector by a scalar.  <a href="#ac48afbd3ef8035d87ec1f1be1de7ed5e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5db0dd7d15f1a4ddf8f66bb68ab2cbd0"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Print" ref="a5db0dd7d15f1a4ddf8f66bb68ab2cbd0" args="() const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a5db0dd7d15f1a4ddf8f66bb68ab2cbd0">Print</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays the vector. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#af1e6f26c236b3a57a51c03f820349168">Write</a> (string FileName, bool with_size=true) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the vector in a file.  <a href="#af1e6f26c236b3a57a51c03f820349168"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#ad4578b7b28d548187117674e6d8a3322">Write</a> (ostream &amp;FileStream, bool with_size=true) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the vector in a file stream.  <a href="#ad4578b7b28d548187117674e6d8a3322"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a617424e1aa6f5e3e3f5f87189b6c66aa">WriteText</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the vector in a file.  <a href="#a617424e1aa6f5e3e3f5f87189b6c66aa"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#ad32ff4be223e4b43ca9e7043d38bd65e">WriteText</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the vector in a file stream.  <a href="#ad32ff4be223e4b43ca9e7043d38bd65e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#acd7cda2c966facc985412cdac9962a0f">Read</a> (string FileName, bool with_size=true)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the vector from a file.  <a href="#acd7cda2c966facc985412cdac9962a0f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#abdab1e4df87abbed8c5a2ee34bddef3f">Read</a> (istream &amp;FileStream, bool with_size=true)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the vector from a file stream.  <a href="#abdab1e4df87abbed8c5a2ee34bddef3f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a0a9444914b4d3df7d5b99748ea98150e">ReadText</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the vector from a file.  <a href="#a0a9444914b4d3df7d5b99748ea98150e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a1a8c07b809e78392217c67feb612ddeb">ReadText</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the vector from a file stream.  <a href="#a1a8c07b809e78392217c67feb612ddeb"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">Vec &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#ac288674b5e0cbab4289b783279c20347">GetPetscVector</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a reference on the inner petsc vector.  <a href="#ac288674b5e0cbab4289b783279c20347"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const Vec &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#af186015f5c0c35d348f8f7a57cdbaa64">GetPetscVector</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a const reference on the inner petsc vector.  <a href="#af186015f5c0c35d348f8f7a57cdbaa64"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#ac6b7117d6550912d70c1b929be557f27">SetCommunicator</a> (MPI_Comm mpi_communicator)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the MPI communicator.  <a href="#ac6b7117d6550912d70c1b929be557f27"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a87b2f7caa1f91402c0943f4b0fd7a4ac">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the vector.  <a href="#a87b2f7caa1f91402c0943f4b0fd7a4ac"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a713d118c2c4a588ed3d043f45619e27b">Resize</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Changes the length of the vector, and keeps previous values.  <a href="#a713d118c2c4a588ed3d043f45619e27b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8d99cadc3f697fdc5f5c85cea949b6c6"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::SetData" ref="a8d99cadc3f697fdc5f5c85cea949b6c6" args="(int i, pointer data)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetData</b> (int i, pointer data)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a73f72da9e6a83f0b54aa4e6f08339159">Nullify</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the vector without releasing memory.  <a href="#a73f72da9e6a83f0b54aa4e6f08339159"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">value_type&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a415d9ec2b7070127f200a40abfa09df8">operator()</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a415d9ec2b7070127f200a40abfa09df8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#aa9c4710028825f7e8d4c4b38e28d77bc">SetBuffer</a> (int i, T value, InsertMode insert_mode=INSERT_VALUES)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Inserts or adds values into certain locations of a vector.  <a href="#aa9c4710028825f7e8d4c4b38e28d77bc"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7ece84387ef984021d4606b093c2497d"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Flush" ref="a7ece84387ef984021d4606b093c2497d" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a7ece84387ef984021d4606b093c2497d">Flush</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Assembles the PETSc vector. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a0a0a3a91dc5491fd88f6803b208db9ac">GetProcessorRange</a> (int &amp;i, int &amp;j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the range of indices owned by this processor.  <a href="#a0a0a3a91dc5491fd88f6803b208db9ac"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a0b19d99fda0bdd859486db905f3e758f">Copy</a> (const <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">PETScVector</a>&lt; T, Allocator &gt; &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a vector.  <a href="#a0b19d99fda0bdd859486db905f3e758f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a922a0d69853d84d718dc9102fa8a7f4f">Append</a> (const T &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Appends an element to the vector.  <a href="#a922a0d69853d84d718dc9102fa8a7f4f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a4b1dfd7f5db9600d9e65826c4462d37e">GetDataSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored.  <a href="#a4b1dfd7f5db9600d9e65826c4462d37e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a0dfb99ee603ed3f578132425f5afed32">GetLocalM</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored.  <a href="#a0dfb99ee603ed3f578132425f5afed32"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a0b318ae92a2a224db5f8a80332b8d25f">Zero</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets all elements to zero.  <a href="#a0b318ae92a2a224db5f8a80332b8d25f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab1fc92c50dd23964a3ce240c1d5c953a"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Fill" ref="ab1fc92c50dd23964a3ce240c1d5c953a" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#ab1fc92c50dd23964a3ce240c1d5c953a">Fill</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the vector with 0, 1, 2, ... <br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a9a354dbfc573d2eec31be3707d1d6aae">Fill</a> (const T0 &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the vector with a given value.  <a href="#a9a354dbfc573d2eec31be3707d1d6aae"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#aeea94a272f075d781c2a71d21072ccf4">FillRand</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the vector randomly.  <a href="#aeea94a272f075d781c2a71d21072ccf4"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">value_type&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a1216ed94d76cd4cc9a57203781deb864">GetNormInf</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the infinite norm.  <a href="#a1216ed94d76cd4cc9a57203781deb864"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a7ecdd5c6469cdaa4e263f6ff71fbbf00">GetNormInfIndex</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the index of the highest absolute value.  <a href="#a7ecdd5c6469cdaa4e263f6ff71fbbf00"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae">GetM</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements.  <a href="#a51f50515f0c6d0663465c2cc7de71bae"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a075daae3607a4767a77a3de60ab30ba7">GetLength</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements.  <a href="#a075daae3607a4767a77a3de60ab30ba7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a839880a3113c1885ead70d79fade0294">GetSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored.  <a href="#a839880a3113c1885ead70d79fade0294"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a">GetData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer to data_ (stored data).  <a href="#a4128ad4898e42211d22a7531c9f5f80a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a454aea78c82c4317dfe4160cc7c95afa">GetDataConst</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a const pointer to data_ (stored data).  <a href="#a454aea78c82c4317dfe4160cc7c95afa"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#ad0f5184bd9eec6fca70c54e459ced78f">GetDataVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "void*" to the data array (data_).  <a href="#ad0f5184bd9eec6fca70c54e459ced78f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a0aadc006977de037eb3ee550683e3f19">GetDataConstVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "const void*" to the data array (data_).  <a href="#a0aadc006977de037eb3ee550683e3f19"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pub-attribs"></a>
Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a323f6bd490f539750d525646b15583f6"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::petsc_vector_" ref="a323f6bd490f539750d525646b15583f6" args="" -->
Vec&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a323f6bd490f539750d525646b15583f6">petsc_vector_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Encapsulated PETSc vector. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a70bd7cbade506f2690f742931f6d46c2"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::mpi_communicator_" ref="a70bd7cbade506f2690f742931f6d46c2" args="" -->
MPI_Comm&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a70bd7cbade506f2690f742931f6d46c2">mpi_communicator_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">The MPI communicator to use. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a39bcb5ba22a031f8d6fbebea571a3c03"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::petsc_vector_deallocated_" ref="a39bcb5ba22a031f8d6fbebea571a3c03" args="" -->
bool&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#a39bcb5ba22a031f8d6fbebea571a3c03">petsc_vector_deallocated_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Boolean to indicate if the inner PETSc vector is destroyed or not. <br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac58514f2c122d7538dfc4db4ce0d4da9"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::m_" ref="ac58514f2c122d7538dfc4db4ce0d4da9" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>m_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1a6ae822c00f9ee2a798d54a36602430"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::data_" ref="a1a6ae822c00f9ee2a798d54a36602430" args="" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-static-attribs"></a>
Static Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6c958ad29a5f7eabd1f085e50046ab85"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::vect_allocator_" ref="a6c958ad29a5f7eabd1f085e50046ab85" args="" -->
static Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>vect_allocator_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Allocator&gt;<br/>
 class Seldon::Vector&lt; T, PETScPar, Allocator &gt;</h3>


<p>Definition at line <a class="el" href="_petsc_vector_8hxx_source.php#l00162">162</a> of file <a class="el" href="_petsc_vector_8hxx_source.php">PetscVector.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="af2ab08543ecdfb8a09d01e4c024607ff"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Vector" ref="af2ab08543ecdfb8a09d01e4c024607ff" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_vector.php">Vector</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [explicit]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>On exit, the vector is empty. </p>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00835">835</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a58773b566b67c3bc9830a56f5a4e0499"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Vector" ref="a58773b566b67c3bc9830a56f5a4e0499" args="(int i, MPI_Comm mpi_communicator=MPI_COMM_WORLD)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_vector.php">Vector</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">MPI_Comm&nbsp;</td>
          <td class="paramname"> <em>mpi_communicator</em> = <code>MPI_COMM_WORLD</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [explicit]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Main constructor. </p>
<p>Builds a vector of a given size. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>length of the vector. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>mpi_communicator</em>&nbsp;</td><td>MPI communicator to use. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00852">852</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a93033ed7da4e225e1cc183fb963a1315"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Vector" ref="a93033ed7da4e225e1cc183fb963a1315" args="(int i, int Nlocal, MPI_Comm mpi_communicator)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_vector.php">Vector</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>Nlocal</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">MPI_Comm&nbsp;</td>
          <td class="paramname"> <em>mpi_communicator</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [explicit]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Main constructor. </p>
<p>Builds a vector of a given size. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>length of the vector. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>Nlocal</em>&nbsp;</td><td>size of local vector. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>mpi_communicator</em>&nbsp;</td><td>MPI communicator to use. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00873">873</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1c6444722ded7b4443326eeaa32ebe1c"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Vector" ref="a1c6444722ded7b4443326eeaa32ebe1c" args="(Vec &amp;petsc_vector)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_vector.php">Vector</a> </td>
          <td>(</td>
          <td class="paramtype">Vec &amp;&nbsp;</td>
          <td class="paramname"> <em>petsc_vector</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Copy constructor. </p>
<p>Builds a copy of a vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>petsc_vector</em>&nbsp;</td><td>vector to be copied. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00894">894</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa74b1bffa16b72281d8fe517cd883d48"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Vector" ref="aa74b1bffa16b72281d8fe517cd883d48" args="(const Vector&lt; T, PETScPar, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_vector.php">Vector</a> </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>V</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Copy constructor. </p>
<p>Builds a copy of a vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>V</em>&nbsp;</td><td>vector to be copied. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00905">905</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="a922a0d69853d84d718dc9102fa8a7f4f"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Append" ref="a922a0d69853d84d718dc9102fa8a7f4f" args="(const T &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector</a>&lt; T, Allocator &gt;::Append </td>
          <td>(</td>
          <td class="paramtype">const T &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline, inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Appends an element to the vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>element to be appended. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>This method will only work if the allocator preserves the elements while reallocating. </dd></dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00332">332</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a87b2f7caa1f91402c0943f4b0fd7a4ac"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Clear" ref="a87b2f7caa1f91402c0943f4b0fd7a4ac" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector</a>&lt; T, Allocator &gt;::Clear </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline, inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the vector. </p>
<p>Destructs the vector. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>On exit, the vector is an empty vector. </dd></dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00142">142</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9ee7458d06d687a0078b368d1788738a"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Copy" ref="a9ee7458d06d687a0078b368d1788738a" args="(const Vector&lt; T, PETScPar, Allocator &gt; &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt;::Copy </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>X</em>&nbsp;</td><td>vector to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'X' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00931">931</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad806a5711dd4273454143fc6f52d0405"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Copy" ref="ad806a5711dd4273454143fc6f52d0405" args="(const Vec &amp;petsc_vector)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt;::Copy </td>
          <td>(</td>
          <td class="paramtype">const Vec &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>X</em>&nbsp;</td><td>vector to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'X' is therefore independent from the current instance after the copy. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#aaf9f0e22b4b7f5928f2a60b66a1b1534">Seldon::PETScVector&lt; T, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00946">946</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0b19d99fda0bdd859486db905f3e758f"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Copy" ref="a0b19d99fda0bdd859486db905f3e758f" args="(const PETScVector&lt; T, Allocator &gt; &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector</a>&lt; T, Allocator &gt;::Copy </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">PETScVector</a>&lt; T, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline, inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>X</em>&nbsp;</td><td>vector to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'X' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00293">293</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9a354dbfc573d2eec31be3707d1d6aae"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Fill" ref="a9a354dbfc573d2eec31be3707d1d6aae" args="(const T0 &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector</a>&lt; T, Allocator &gt;::Fill </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the vector with a given value. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>value to fill the vector with. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00408">408</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aeea94a272f075d781c2a71d21072ccf4"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::FillRand" ref="aeea94a272f075d781c2a71d21072ccf4" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector</a>&lt; T, Allocator &gt;::FillRand </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the vector randomly. </p>
<dl class="note"><dt><b>Note:</b></dt><dd>The random generator is very basic. </dd></dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00422">422</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a4128ad4898e42211d22a7531c9f5f80a"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::GetData" ref="a4128ad4898e42211d22a7531c9f5f80a" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector___base.php">Vector_Base</a>&lt; T, Allocator &gt;::pointer <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer to data_ (stored data). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer to the data_, i.e. the data array. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00156">156</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a454aea78c82c4317dfe4160cc7c95afa"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::GetDataConst" ref="a454aea78c82c4317dfe4160cc7c95afa" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector___base.php">Vector_Base</a>&lt; T, Allocator &gt;::const_pointer <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetDataConst </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a const pointer to data_ (stored data). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer to the data_, i.e. the data array. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00168">168</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0aadc006977de037eb3ee550683e3f19"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::GetDataConstVoid" ref="a0aadc006977de037eb3ee550683e3f19" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const void * <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetDataConstVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "const void*" to the data array (data_). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer of type "const void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00191">191</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a4b1dfd7f5db9600d9e65826c4462d37e"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::GetDataSize" ref="a4b1dfd7f5db9600d9e65826c4462d37e" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector</a>&lt; T, Allocator &gt;::GetDataSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements stored in memory. </dd></dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00348">348</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad0f5184bd9eec6fca70c54e459ced78f"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::GetDataVoid" ref="ad0f5184bd9eec6fca70c54e459ced78f" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void * <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetDataVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "void*" to the data array (data_). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00180">180</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a075daae3607a4767a77a3de60ab30ba7"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::GetLength" ref="a075daae3607a4767a77a3de60ab30ba7" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetLength </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the vector. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ae501c34e51f6559568846b94e24c249c">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>, and <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a2a2b8d5ad39c79bdcdcd75fa20ae5ae8">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00133">133</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0dfb99ee603ed3f578132425f5afed32"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::GetLocalM" ref="a0dfb99ee603ed3f578132425f5afed32" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector</a>&lt; T, Allocator &gt;::GetLocalM </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements stored in memory. </dd></dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00359">359</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a51f50515f0c6d0663465c2cc7de71bae"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::GetM" ref="a51f50515f0c6d0663465c2cc7de71bae" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the vector. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a6f40d71a69e016ae3e1d9db92b6538a4">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>, and <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a9b0a8be2fb500d4f6c2edb4c1a5247ef">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00122">122</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1216ed94d76cd4cc9a57203781deb864"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::GetNormInf" ref="a1216ed94d76cd4cc9a57203781deb864" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">PETScVector</a>&lt; T, Allocator &gt;::value_type <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector</a>&lt; T, Allocator &gt;::GetNormInf </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the infinite norm. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The infinite norm. </dd></dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00447">447</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7ecdd5c6469cdaa4e263f6ff71fbbf00"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::GetNormInfIndex" ref="a7ecdd5c6469cdaa4e263f6ff71fbbf00" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector</a>&lt; T, Allocator &gt;::GetNormInfIndex </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the index of the highest absolute value. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The index of the element that has the highest absolute value. </dd></dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00462">462</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af186015f5c0c35d348f8f7a57cdbaa64"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::GetPetscVector" ref="af186015f5c0c35d348f8f7a57cdbaa64" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const Vec &amp; <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector</a>&lt; T, Allocator &gt;::GetPetscVector </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a const reference on the inner petsc vector. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>a const reference on the inner petsc vector. </dd></dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00114">114</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac288674b5e0cbab4289b783279c20347"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::GetPetscVector" ref="ac288674b5e0cbab4289b783279c20347" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">Vec &amp; <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector</a>&lt; T, Allocator &gt;::GetPetscVector </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a reference on the inner petsc vector. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>a reference on the inner petsc vector. </dd></dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00103">103</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0a0a3a91dc5491fd88f6803b208db9ac"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::GetProcessorRange" ref="a0a0a3a91dc5491fd88f6803b208db9ac" args="(int &amp;i, int &amp;j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector</a>&lt; T, Allocator &gt;::GetProcessorRange </td>
          <td>(</td>
          <td class="paramtype">int &amp;&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int &amp;&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline, inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the range of indices owned by this processor. </p>
<p>The vectors are laid out with the first <img class="formulaInl" alt="$n_1$" src="form_30.png"/> elements on the first processor, next <img class="formulaInl" alt="$n_2$" src="form_31.png"/> elements on the second, etc. If the current processor is <img class="formulaInl" alt="$k$" src="form_32.png"/>, this method returns <img class="formulaInl" alt="$n_k$" src="form_33.png"/> in <em>i</em> and <img class="formulaInl" alt="$n_{k+1}$" src="form_34.png"/> in <em>j</em>. If <em>i</em> is set to PETSC_NULL on entry, it is not modified by this function. Same is true for <em>j</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in,out]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>the index of the first local element. </td></tr>
    <tr><td valign="top"><tt>[in,out]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>the index of the last local element, plus 1. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00277">277</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a839880a3113c1885ead70d79fade0294"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::GetSize" ref="a839880a3113c1885ead70d79fade0294" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the vector stored. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00144">144</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a73f72da9e6a83f0b54aa4e6f08339159"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Nullify" ref="a73f72da9e6a83f0b54aa4e6f08339159" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector</a>&lt; T, Allocator &gt;::Nullify </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the vector without releasing memory. </p>
<p>On exit, the vector is empty and the memory has not been released. It is useful for low level manipulations on a <a class="el" href="class_seldon_1_1_vector.php">Vector</a> instance. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Memory is not released. </dd></dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00195">195</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a415d9ec2b7070127f200a40abfa09df8"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::operator()" ref="a415d9ec2b7070127f200a40abfa09df8" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">PETScVector</a>&lt; T, Allocator &gt;::value_type <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector</a>&lt; T, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline, inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The value of the vector at 'i'. </dd></dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00213">213</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac48afbd3ef8035d87ec1f1be1de7ed5e"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::operator*=" ref="ac48afbd3ef8035d87ec1f1be1de7ed5e" args="(const T0 &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt;::operator*= </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>alpha</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Multiplies a vector by a scalar. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>alpha</em>&nbsp;</td><td>scalar. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00988">988</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2251829a7141e9b02e2fef218ad32675"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::operator=" ref="a2251829a7141e9b02e2fef218ad32675" args="(const Vector&lt; T, PETScPar, Allocator &gt; &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a vector (assignment operator). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>X</em>&nbsp;</td><td>vector to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'X' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00960">960</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad8ce057df39b8cdee03515c7aeca3964"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::operator=" ref="ad8ce057df39b8cdee03515c7aeca3964" args="(const T0 &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the vector with a given value. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>value to fill the vector with. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00974">974</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="acd7cda2c966facc985412cdac9962a0f"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Read" ref="acd7cda2c966facc985412cdac9962a0f" args="(string FileName, bool with_size=true)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the vector from a file. </p>
<p>Sets the vector according to a binary file that stores the length of the vector (integer) and all elements. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>file name. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the length of the vector is not available in the file. In this case, the current size N of the vector is unchanged, and N elements are read in the file. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l01139">1139</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="abdab1e4df87abbed8c5a2ee34bddef3f"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Read" ref="abdab1e4df87abbed8c5a2ee34bddef3f" args="(istream &amp;FileStream, bool with_size=true)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the vector from a file stream. </p>
<p>Sets the vector according to a binary file stream that stores the length of the vector (integer) and all elements. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>file stream. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the length of the vector is not available in the stream. In this case, the current size N of the vector is unchanged, and N elements are read in the stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l01165">1165</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1a8c07b809e78392217c67feb612ddeb"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::ReadText" ref="a1a8c07b809e78392217c67feb612ddeb" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the vector from a file stream. </p>
<p>Sets all elements of the vector according to a text format. The length is not stored. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>file stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l01193">1193</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0a9444914b4d3df7d5b99748ea98150e"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::ReadText" ref="a0a9444914b4d3df7d5b99748ea98150e" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the vector from a file. </p>
<p>Sets all elements of the vector according to a text format. The length is not stored. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l01179">1179</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="abc4329f40632d4f9cfa083d314b45f4d"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Reallocate" ref="abc4329f40632d4f9cfa083d314b45f4d" args="(int i, int local_size=PETSC_DECIDE)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt;::Reallocate </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>local_size</em> = <code>PETSC_DECIDE</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p><a class="el" href="class_seldon_1_1_vector.php">Vector</a> reallocation. </p>
<p>The vector is resized. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>new length of the vector. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Depending on your allocator, initial elements of the vector may be lost. </dd></dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l01015">1015</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a713d118c2c4a588ed3d043f45619e27b"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Resize" ref="a713d118c2c4a588ed3d043f45619e27b" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector</a>&lt; T, Allocator &gt;::Resize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>n</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline, inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Changes the length of the vector, and keeps previous values. </p>
<p>Reallocates the vector to size i. Previous values are kept. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>n</em>&nbsp;</td><td>new length of the vector. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00159">159</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa9c4710028825f7e8d4c4b38e28d77bc"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::SetBuffer" ref="aa9c4710028825f7e8d4c4b38e28d77bc" args="(int i, T value, InsertMode insert_mode=INSERT_VALUES)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector</a>&lt; T, Allocator &gt;::SetBuffer </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">T&nbsp;</td>
          <td class="paramname"> <em>value</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">InsertMode&nbsp;</td>
          <td class="paramname"> <em>insert_mode</em> = <code>INSERT_VALUES</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline, inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Inserts or adds values into certain locations of a vector. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>These values may be cached, so 'Flush' must be called after all calls to <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php#aa9c4710028825f7e8d4c4b38e28d77bc" title="Inserts or adds values into certain locations of a vector.">SetBuffer()</a> have been completed. </dd></dl>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index where to insert the value. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>value</em>&nbsp;</td><td>the value to insert. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>insert_mode</em>&nbsp;</td><td>either INSERT_VALUES or ADD_VALUES, where ADD_VALUES adds the value to the entry, and INSERT_VALUES replaces existing entry with new value. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00242">242</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac6b7117d6550912d70c1b929be557f27"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::SetCommunicator" ref="ac6b7117d6550912d70c1b929be557f27" args="(MPI_Comm mpi_communicator)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector</a>&lt; T, Allocator &gt;::SetCommunicator </td>
          <td>(</td>
          <td class="paramtype">MPI_Comm&nbsp;</td>
          <td class="paramname"> <em>mpi_communicator</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the MPI communicator. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>mpi_communicator</em>&nbsp;</td><td>the mpi communicator to be set. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00125">125</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad4578b7b28d548187117674e6d8a3322"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Write" ref="ad4578b7b28d548187117674e6d8a3322" args="(ostream &amp;FileStream, bool with_size=true) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the vector in a file stream. </p>
<p>The length of the vector (integer) and all elements of the vector are stored in binary format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>file stream. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the length of the vector is not saved. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l01059">1059</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af1e6f26c236b3a57a51c03f820349168"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Write" ref="af1e6f26c236b3a57a51c03f820349168" args="(string FileName, bool with_size=true) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the vector in a file. </p>
<p>The length of the vector (integer) and all elements of the vector are stored in binary format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>file name. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the length of the vector is not saved. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l01043">1043</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a617424e1aa6f5e3e3f5f87189b6c66aa"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::WriteText" ref="a617424e1aa6f5e3e3f5f87189b6c66aa" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the vector in a file. </p>
<p>All elements of the vector are stored in text format. The length is not stored. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l01084">1084</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad32ff4be223e4b43ca9e7043d38bd65e"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::WriteText" ref="ad32ff4be223e4b43ca9e7043d38bd65e" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_p_e_t_sc_par.php">PETScPar</a>, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the vector in a file stream. </p>
<p>All elements of the vector are stored in text format. The length is not stored. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>file stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l01110">1110</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0b318ae92a2a224db5f8a80332b8d25f"></a><!-- doxytag: member="Seldon::Vector&lt; T, PETScPar, Allocator &gt;::Zero" ref="a0b318ae92a2a224db5f8a80332b8d25f" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_p_e_t_sc_vector.php">Seldon::PETScVector</a>&lt; T, Allocator &gt;::Zero </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets all elements to zero. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>It fills the memory with zeros. If the vector stores complex structures, use 'Fill' instead. </dd></dl>

<p>Definition at line <a class="el" href="_petsc_vector_8cxx_source.php#l00378">378</a> of file <a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>vector/<a class="el" href="_petsc_vector_8hxx_source.php">PetscVector.hxx</a></li>
<li>vector/<a class="el" href="_petsc_vector_8cxx_source.php">PetscVector.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
