<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/solver/iterative/Iterative.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_ITERATIVE_HXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 <span class="keyword">namespace </span>Seldon
<a name="l00023"></a>00023 {
<a name="l00025"></a><a class="code" href="class_seldon_1_1_preconditioner___base.php">00025</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_preconditioner___base.php" title="Base class for preconditioners.">Preconditioner_Base</a>
<a name="l00026"></a>00026   {
<a name="l00027"></a>00027   <span class="keyword">public</span> :
<a name="l00028"></a>00028 
<a name="l00029"></a>00029     <a class="code" href="class_seldon_1_1_preconditioner___base.php#a2845a80350be45add812148062be20e9" title="Default constructor.">Preconditioner_Base</a>();
<a name="l00030"></a>00030 
<a name="l00031"></a>00031     <span class="comment">// solving M z = r</span>
<a name="l00032"></a>00032     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Matrix1, <span class="keyword">class</span> Vector1&gt;
<a name="l00033"></a>00033     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_preconditioner___base.php#ae0cef5d8a4c53cc5a5025ccdc8712fad" title="Solves M z = r.">Solve</a>(<span class="keyword">const</span> Matrix1&amp; A, <span class="keyword">const</span> Vector1 &amp; r, Vector1 &amp; z);
<a name="l00034"></a>00034 
<a name="l00035"></a>00035     <span class="comment">// solving M^t z = r</span>
<a name="l00036"></a>00036     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Matrix1, <span class="keyword">class</span> Vector1&gt;
<a name="l00037"></a>00037     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_preconditioner___base.php#ac7ddd68847e7c895ac63866742138852" title="Solves M^t z = r.">TransSolve</a>(<span class="keyword">const</span> Matrix1&amp; A, <span class="keyword">const</span> Vector1&amp; r, Vector1 &amp; z);
<a name="l00038"></a>00038 
<a name="l00039"></a>00039   };
<a name="l00040"></a>00040 
<a name="l00041"></a>00041 
<a name="l00043"></a>00043 
<a name="l00047"></a>00047   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Titer&gt;
<a name="l00048"></a><a class="code" href="class_seldon_1_1_iteration.php">00048</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_iteration.php" title="Class containing parameters for an iterative resolution.">Iteration</a>
<a name="l00049"></a>00049   {
<a name="l00050"></a>00050   <span class="keyword">protected</span> :
<a name="l00051"></a><a class="code" href="class_seldon_1_1_iteration.php#acf9550c0ec96e51fc5d395446342c4e8">00051</a>     Titer <a class="code" href="class_seldon_1_1_iteration.php#acf9550c0ec96e51fc5d395446342c4e8" title="stopping criterion">tolerance</a>; 
<a name="l00052"></a><a class="code" href="class_seldon_1_1_iteration.php#adf29aa77b9dd1ecdc57c132712b02811">00052</a>     Titer <a class="code" href="class_seldon_1_1_iteration.php#adf29aa77b9dd1ecdc57c132712b02811" title="inverse of norm of first residual">facteur_reste</a>; 
<a name="l00053"></a><a class="code" href="class_seldon_1_1_iteration.php#a75359d82e80a14fa10907b203fc5a2ff">00053</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_iteration.php#a75359d82e80a14fa10907b203fc5a2ff" title="maximum number of iterations">max_iter</a>; 
<a name="l00054"></a><a class="code" href="class_seldon_1_1_iteration.php#a4ff83b0b358c1574c6c0ababbbe04745">00054</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_iteration.php#a4ff83b0b358c1574c6c0ababbbe04745" title="number of iterations">nb_iter</a>; 
<a name="l00055"></a><a class="code" href="class_seldon_1_1_iteration.php#a493984d8abaec3ac0d4f73c29d2a0e60">00055</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_iteration.php#a493984d8abaec3ac0d4f73c29d2a0e60" title="error code returned by iterative solver">error_code</a>; 
<a name="l00056"></a><a class="code" href="class_seldon_1_1_iteration.php#afea45227a5d730ac1464b3c33dabfc9e">00056</a>     <span class="keywordtype">bool</span> <a class="code" href="class_seldon_1_1_iteration.php#afea45227a5d730ac1464b3c33dabfc9e" title="true if the iterative solver has converged //! print level">fail_convergence</a>; 
<a name="l00057"></a>00057 
<a name="l00058"></a>00058 
<a name="l00063"></a><a class="code" href="class_seldon_1_1_iteration.php#a82adaef364a9a20a7525ab9299b6bfd5">00063</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_iteration.php#a82adaef364a9a20a7525ab9299b6bfd5">print_level</a>;
<a name="l00064"></a><a class="code" href="class_seldon_1_1_iteration.php#a363306d1242c5a2b78737ace82ea465b">00064</a>     <span class="keywordtype">bool</span> <a class="code" href="class_seldon_1_1_iteration.php#a363306d1242c5a2b78737ace82ea465b" title="true if initial guess is null">init_guess_null</a>; 
<a name="l00065"></a><a class="code" href="class_seldon_1_1_iteration.php#aced354c0bf1e3868ae5b77283982b8fa">00065</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_iteration.php#aced354c0bf1e3868ae5b77283982b8fa" title="iterative solver used">type_solver</a>; 
<a name="l00066"></a><a class="code" href="class_seldon_1_1_iteration.php#a5c4d36f6bff1c44d6e7b31bb3c70b603">00066</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_iteration.php#a5c4d36f6bff1c44d6e7b31bb3c70b603" title="restart parameter (for Gmres and Gcr)">parameter_restart</a>; 
<a name="l00067"></a><a class="code" href="class_seldon_1_1_iteration.php#a19f60dd0946c1a5a212b0e28b5be55a9">00067</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_iteration.php#a19f60dd0946c1a5a212b0e28b5be55a9" title="preconditioner used">type_preconditioning</a>; 
<a name="l00068"></a>00068 
<a name="l00069"></a>00069   <span class="keyword">public</span> :
<a name="l00070"></a>00070 
<a name="l00071"></a>00071     <a class="code" href="class_seldon_1_1_iteration.php#a9c42765a055d9dd271b2b8a52a0baa57" title="Default constructor.">Iteration</a>();
<a name="l00072"></a>00072     <a class="code" href="class_seldon_1_1_iteration.php#a9c42765a055d9dd271b2b8a52a0baa57" title="Default constructor.">Iteration</a>(<span class="keywordtype">int</span> max_iteration, <span class="keyword">const</span> Titer&amp; tol);
<a name="l00073"></a>00073     <a class="code" href="class_seldon_1_1_iteration.php#a9c42765a055d9dd271b2b8a52a0baa57" title="Default constructor.">Iteration</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_iteration.php" title="Class containing parameters for an iterative resolution.">Iteration&lt;Titer&gt;</a>&amp; outer);
<a name="l00074"></a>00074 
<a name="l00075"></a>00075     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_iteration.php#ad57613fa7b74a7a7a4e758262957ca0f" title="Returns the type of solver.">GetTypeSolver</a>() <span class="keyword">const</span>;
<a name="l00076"></a>00076     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_iteration.php#ae383df46b41aff26fe97418cab8a070f" title="Returns the restart parameter.">GetRestart</a>() <span class="keyword">const</span>;
<a name="l00077"></a>00077     Titer <a class="code" href="class_seldon_1_1_iteration.php#a617c6d99afeb2b5a2d18144f367dba88" title="Returns used coefficient to compute relative residual.">GetFactor</a>() <span class="keyword">const</span>;
<a name="l00078"></a>00078     Titer <a class="code" href="class_seldon_1_1_iteration.php#af31105a0169bb625d24a4ba76183b3a5" title="Returns stopping criterion.">GetTolerance</a>() <span class="keyword">const</span>;
<a name="l00079"></a>00079     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_iteration.php#ab426008d4bfc40842d86c8c6042a6594" title="Returns the number of iterations.">GetNumberIteration</a>() <span class="keyword">const</span>;
<a name="l00080"></a>00080 
<a name="l00081"></a>00081     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_iteration.php#a7906239fb42957439f236258d4423465" title="Changes the type of solver and preconditioning.">SetSolver</a>(<span class="keywordtype">int</span> type_resolution, <span class="keywordtype">int</span> param_restart, <span class="keywordtype">int</span> type_prec);
<a name="l00082"></a>00082     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_iteration.php#a3a5874b5b76075698ceeb99944261311" title="Changes the restart parameter.">SetRestart</a>(<span class="keywordtype">int</span> m);
<a name="l00083"></a>00083     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_iteration.php#a1393846a1b9090c4bb3516b28e70561b" title="Changes the stopping criterion.">SetTolerance</a>(Titer stopping_criterion);
<a name="l00084"></a>00084     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_iteration.php#afed55f067231a1defcf50acd0755aef1" title="Changes the maximum number of iterations.">SetMaxNumberIteration</a>(<span class="keywordtype">int</span> max_iteration);
<a name="l00085"></a>00085     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_iteration.php#a24c69ffb62504d708354f8874a5f0f00" title="Changes the number of iterations.">SetNumberIteration</a>(<span class="keywordtype">int</span> nb);
<a name="l00086"></a>00086 
<a name="l00087"></a>00087     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_iteration.php#a18f2bdb38c22716d640422ead8998e1f" title="Sets to a normal display (residual each 100 iterations).">ShowMessages</a>();
<a name="l00088"></a>00088     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_iteration.php#a69000baf75577546e3d78e8bcfe1576a" title="Sets to a complete display (residual each iteration).">ShowFullHistory</a>();
<a name="l00089"></a>00089     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_iteration.php#a305d9d618a978830c3d2e36953c001c3" title="Doesn&amp;#39;t display any information.">HideMessages</a>();
<a name="l00090"></a>00090 
<a name="l00091"></a>00091     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Vector1&gt;
<a name="l00092"></a>00092     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_iteration.php#a047b05d60ecc8b6f8f0789255472bfe3" title="Initialization with the right hand side.">Init</a>(<span class="keyword">const</span> Vector1&amp; r);
<a name="l00093"></a>00093     <span class="keywordtype">bool</span> <a class="code" href="class_seldon_1_1_iteration.php#adaeada1a17ad527435684f5f8ad33f33" title="Returns true if it is the first iteration.">First</a>() <span class="keyword">const</span>;
<a name="l00094"></a>00094 
<a name="l00095"></a>00095     <span class="keywordtype">bool</span> <a class="code" href="class_seldon_1_1_iteration.php#aa02c78783891fabcb4f7517f60f4912e" title="Returns true if the initial guess is null.">IsInitGuess_Null</a>() <span class="keyword">const</span>;
<a name="l00096"></a>00096     <span class="keywordtype">void</span> SetInitGuess(<span class="keywordtype">bool</span> type) { <a class="code" href="class_seldon_1_1_iteration.php#a363306d1242c5a2b78737ace82ea465b" title="true if initial guess is null">init_guess_null</a> = type; }
<a name="l00097"></a>00097 
<a name="l00098"></a>00098     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Vector1&gt;
<a name="l00099"></a>00099     <span class="keywordtype">bool</span> <a class="code" href="class_seldon_1_1_iteration.php#a07c3b0af0412cfc7fe6fd3ed5476c4e0" title="Returns true if the iterative solver has reached its end.">Finished</a>(<span class="keyword">const</span> Vector1&amp; r) <span class="keyword">const</span>;
<a name="l00100"></a>00100     <span class="keywordtype">bool</span> <a class="code" href="class_seldon_1_1_iteration.php#a07c3b0af0412cfc7fe6fd3ed5476c4e0" title="Returns true if the iterative solver has reached its end.">Finished</a>(<span class="keyword">const</span> Titer&amp; r) <span class="keyword">const</span>;
<a name="l00101"></a>00101 
<a name="l00102"></a>00102     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_iteration.php#a32dd5ec2e2b6216c50fbc1d0e27607a7" title="Informs of a failure in the iterative solver.">Fail</a>(<span class="keywordtype">int</span> i, <span class="keyword">const</span> <span class="keywordtype">string</span>&amp; s);
<a name="l00103"></a>00103 
<a name="l00104"></a>00104     <a class="code" href="class_seldon_1_1_iteration.php" title="Class containing parameters for an iterative resolution.">Iteration</a>&amp; <a class="code" href="class_seldon_1_1_iteration.php#a105cecfc4c5d1c1c27278eaa5807e32d" title="Increment the number of iterations.">operator ++ </a>(<span class="keywordtype">void</span>);
<a name="l00105"></a>00105 
<a name="l00106"></a>00106     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_iteration.php#aa84736520dcca50ab5b8c72449ef8ed4" title="Returns the error code (if an error occured).">ErrorCode</a>() <span class="keyword">const</span>;
<a name="l00107"></a>00107 
<a name="l00108"></a>00108   };
<a name="l00109"></a>00109 
<a name="l00110"></a>00110 } <span class="comment">// end namespace</span>
<a name="l00111"></a>00111 
<a name="l00112"></a>00112 <span class="preprocessor">#define SELDON_FILE_ITERATIVE_HXX</span>
<a name="l00113"></a>00113 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
