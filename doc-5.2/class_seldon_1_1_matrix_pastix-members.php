<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::MatrixPastix&lt; T &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a>, including all inherited members.<table>
  <tr bgcolor="#f0f0f0"><td><b>CallPastix</b>(const MPI_Comm &amp;, pastix_int_t *colptr, pastix_int_t *row, T *val, T *b, pastix_int_t nrhs) (defined in <a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>CallPastix</b>(const MPI_Comm &amp;comm, pastix_int_t *colptr, pastix_int_t *row, double *val, double *b, pastix_int_t nrhs) (defined in <a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>CallPastix</b>(const MPI_Comm &amp;comm, pastix_int_t *colptr, pastix_int_t *row, complex&lt; double &gt; *val, complex&lt; double &gt; *b, pastix_int_t nrhs) (defined in <a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#a0840578aaa2639d33d5c47adea893cf8">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#aeca666d886a9ab8e8e8e907963a6ed64">col_num</a></td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#ab0637a34811226bdedf63b3e71c7cef6">distributed</a></td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#af8a85f37469fa702b6ea1ebd2a2b4b0e">DoNotRefineSolution</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#ab0e31b30663fb336f638fc847663c1eb">dparm</a></td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#a64fb6fb73d7407926c1fbd93ed3182da">FactorizeDistributedMatrix</a>(MPI::Comm &amp;comm_facto, Vector&lt; pastix_int_t, VectFull, Alloc1 &gt; &amp;, Vector&lt; pastix_int_t, VectFull, Alloc2 &gt; &amp;, Vector&lt; T, VectFull, Alloc3 &gt; &amp;, const Vector&lt; Tint &gt; &amp;glob_number, bool sym, bool keep_matrix=false)</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#a638bb937143f0aacd9f241d4c89febba">FactorizeMatrix</a>(Matrix&lt; T, General, Storage, Allocator &gt; &amp;mat, bool keep_matrix=false)</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#a328852f857f9e71c3065c4f9e4b37b06">FactorizeMatrix</a>(Matrix&lt; T, Symmetric, Storage, Allocator &gt; &amp;mat, bool keep_matrix=false)</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#a69e64cad0fe1938748e49c0d0057bd66">FindOrdering</a>(Matrix&lt; T0, Prop, Storage, Allocator &gt; &amp;mat, Vector&lt; Tint &gt; &amp;numbers, bool keep_matrix=false)</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#a29ac5cb9e5315ae68d6fc5d1e97eef71">HideMessages</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>invp</b> (defined in <a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#a3e0e4d255058e66c82419232b28fe178">iparm</a></td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#a862e60553d3532ab99a3645f219d782d">MatrixPastix</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e">n</a></td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#a1d728cf85201e0e4b35fd66c985ca95a">pastix_data</a></td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#a0e40bdc9ac7c0ae1bd80e05d9942afee">perm</a></td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#a138482f32cb3046a3faf597aff405fb3">print_level</a></td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#a8c22604c5e88bb75086bde3f344cd4d5">refine_solution</a></td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#a6f6a15165b8961d62505dc2ff4f261e0">RefineSolution</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SelectOrdering</b>(int type) (defined in <a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#ac3ebb0a32a3aa30164f54dbaa75bff4a">SetNumberThreadPerNode</a>(int num_thread)</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SetPermutation</b>(const IVect &amp;permut) (defined in <a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#adc33ae8c7f93e468abd3ade499bea393">ShowFullHistory</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#a0ca315759b1dd24114ff47958c37cb96">ShowMessages</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#a74f16146dfc8aa871028cfa6086a184a">Solve</a>(Vector&lt; T, VectFull, Allocator2 &gt; &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#a9f48e18656ea0d118f8e4d60bed06718">Solve</a>(const Transpose_status &amp;TransA, Vector&lt; T, VectFull, Allocator2 &gt; &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SolveDistributed</b>(MPI::Comm &amp;comm_facto, Vector&lt; T, Vect_Full, Allocator2 &gt; &amp;x, const Vector&lt; Tint &gt; &amp;glob_num) (defined in <a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SolveDistributed</b>(MPI::Comm &amp;comm_facto, const Transpose_status &amp;TransA, Vector&lt; T, Vect_Full, Allocator2 &gt; &amp;x, const Vector&lt; Tint &gt; &amp;glob_num) (defined in <a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_pastix.php#a06e7866292ac5b30289cf566d56b0caf">~MatrixPastix</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_pastix.php">Seldon::MatrixPastix&lt; T &gt;</a></td><td></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
