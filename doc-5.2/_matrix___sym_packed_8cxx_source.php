<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix/Matrix_SymPacked.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_SYMPACKED_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;Matrix_SymPacked.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="keyword">namespace </span>Seldon
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 
<a name="l00029"></a>00029   <span class="comment">/****************</span>
<a name="l00030"></a>00030 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00031"></a>00031 <span class="comment">   ****************/</span>
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00035"></a>00035 
<a name="l00038"></a>00038   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00039"></a>00039   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a03e8a4aea2ab3a95be4f8e12023217ae" title="Default constructor.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::Matrix_SymPacked</a>():
<a name="l00040"></a>00040     Matrix_Base&lt;T, Allocator&gt;()
<a name="l00041"></a>00041   {
<a name="l00042"></a>00042   }
<a name="l00043"></a>00043 
<a name="l00044"></a>00044 
<a name="l00046"></a>00046 
<a name="l00051"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a9e3f8d4a030e998880e185f265b0c912">00051</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00052"></a>00052   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a03e8a4aea2ab3a95be4f8e12023217ae" title="Default constructor.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00053"></a>00053 <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a03e8a4aea2ab3a95be4f8e12023217ae" title="Default constructor.">  ::Matrix_SymPacked</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j): <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, i)
<a name="l00054"></a>00054   {
<a name="l00055"></a>00055 
<a name="l00056"></a>00056 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00057"></a>00057 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00058"></a>00058       {
<a name="l00059"></a>00059 <span class="preprocessor">#endif</span>
<a name="l00060"></a>00060 <span class="preprocessor"></span>
<a name="l00061"></a>00061         this-&gt;data_ = this-&gt;allocator_.allocate((i * (i + 1)) / 2, <span class="keyword">this</span>);
<a name="l00062"></a>00062 
<a name="l00063"></a>00063 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00064"></a>00064 <span class="preprocessor"></span>      }
<a name="l00065"></a>00065     <span class="keywordflow">catch</span> (...)
<a name="l00066"></a>00066       {
<a name="l00067"></a>00067         this-&gt;m_ = 0;
<a name="l00068"></a>00068         this-&gt;n_ = 0;
<a name="l00069"></a>00069         this-&gt;data_ = NULL;
<a name="l00070"></a>00070         <span class="keywordflow">return</span>;
<a name="l00071"></a>00071       }
<a name="l00072"></a>00072     <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00073"></a>00073       {
<a name="l00074"></a>00074         this-&gt;m_ = 0;
<a name="l00075"></a>00075         this-&gt;n_ = 0;
<a name="l00076"></a>00076         <span class="keywordflow">return</span>;
<a name="l00077"></a>00077       }
<a name="l00078"></a>00078 <span class="preprocessor">#endif</span>
<a name="l00079"></a>00079 <span class="preprocessor"></span>
<a name="l00080"></a>00080   }
<a name="l00081"></a>00081 
<a name="l00082"></a>00082 
<a name="l00084"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#ac06fa9e352440052fc5e4c8315688dee">00084</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00085"></a>00085   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a03e8a4aea2ab3a95be4f8e12023217ae" title="Default constructor.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00086"></a>00086 <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a03e8a4aea2ab3a95be4f8e12023217ae" title="Default constructor.">  ::Matrix_SymPacked</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php" title="Symmetric packed matrix class.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00087"></a>00087     : <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;()
<a name="l00088"></a>00088   {
<a name="l00089"></a>00089     this-&gt;m_ = 0;
<a name="l00090"></a>00090     this-&gt;n_ = 0;
<a name="l00091"></a>00091     this-&gt;data_ = NULL;
<a name="l00092"></a>00092 
<a name="l00093"></a>00093     this-&gt;Copy(A);
<a name="l00094"></a>00094   }
<a name="l00095"></a>00095 
<a name="l00096"></a>00096 
<a name="l00097"></a>00097   <span class="comment">/**************</span>
<a name="l00098"></a>00098 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00099"></a>00099 <span class="comment">   **************/</span>
<a name="l00100"></a>00100 
<a name="l00101"></a>00101 
<a name="l00103"></a>00103   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00104"></a>00104   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a5b43b96986b649ecab6baa564f58068f" title="Destructor.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::~Matrix_SymPacked</a>()
<a name="l00105"></a>00105   {
<a name="l00106"></a>00106 
<a name="l00107"></a>00107 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00108"></a>00108 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00109"></a>00109       {
<a name="l00110"></a>00110 <span class="preprocessor">#endif</span>
<a name="l00111"></a>00111 <span class="preprocessor"></span>
<a name="l00112"></a>00112         <span class="keywordflow">if</span> (this-&gt;data_ != NULL)
<a name="l00113"></a>00113           {
<a name="l00114"></a>00114             this-&gt;allocator_.deallocate(this-&gt;data_,
<a name="l00115"></a>00115                                         (this-&gt;m_ * (this-&gt;m_ + 1)) / 2);
<a name="l00116"></a>00116             this-&gt;data_ = NULL;
<a name="l00117"></a>00117           }
<a name="l00118"></a>00118 
<a name="l00119"></a>00119 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00120"></a>00120 <span class="preprocessor"></span>      }
<a name="l00121"></a>00121     <span class="keywordflow">catch</span> (...)
<a name="l00122"></a>00122       {
<a name="l00123"></a>00123         this-&gt;m_ = 0;
<a name="l00124"></a>00124         this-&gt;n_ = 0;
<a name="l00125"></a>00125         this-&gt;data_ = NULL;
<a name="l00126"></a>00126       }
<a name="l00127"></a>00127 <span class="preprocessor">#endif</span>
<a name="l00128"></a>00128 <span class="preprocessor"></span>
<a name="l00129"></a>00129   }
<a name="l00130"></a>00130 
<a name="l00131"></a>00131 
<a name="l00133"></a>00133 
<a name="l00137"></a>00137   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00138"></a>00138   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a230f90f867e62f789bdfb7ea2310dc40" title="Clears the matrix.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::Clear</a>()
<a name="l00139"></a>00139   {
<a name="l00140"></a>00140     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a5b43b96986b649ecab6baa564f58068f" title="Destructor.">~Matrix_SymPacked</a>();
<a name="l00141"></a>00141     this-&gt;m_ = 0;
<a name="l00142"></a>00142     this-&gt;n_ = 0;
<a name="l00143"></a>00143   }
<a name="l00144"></a>00144 
<a name="l00145"></a>00145 
<a name="l00146"></a>00146   <span class="comment">/*******************</span>
<a name="l00147"></a>00147 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00148"></a>00148 <span class="comment">   *******************/</span>
<a name="l00149"></a>00149 
<a name="l00150"></a>00150 
<a name="l00152"></a>00152 
<a name="l00155"></a>00155   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00156"></a>00156   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a3fed51d6dd6d46d8e611ce98f9d8f724" title="Returns the number of elements stored in memory.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::GetDataSize</a>()<span class="keyword"> const</span>
<a name="l00157"></a>00157 <span class="keyword">  </span>{
<a name="l00158"></a>00158     <span class="keywordflow">return</span> (this-&gt;m_ * (this-&gt;m_ + 1)) / 2;
<a name="l00159"></a>00159   }
<a name="l00160"></a>00160 
<a name="l00161"></a>00161 
<a name="l00162"></a>00162   <span class="comment">/*********************</span>
<a name="l00163"></a>00163 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00164"></a>00164 <span class="comment">   *********************/</span>
<a name="l00165"></a>00165 
<a name="l00166"></a>00166 
<a name="l00168"></a>00168 
<a name="l00174"></a>00174   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00175"></a>00175   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a22935d0bf71467744c3ee1ac505fe9e6" title="Reallocates memory to resize the matrix.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::Reallocate</a>(<span class="keywordtype">int</span> i,
<a name="l00176"></a>00176                                                                         <span class="keywordtype">int</span> j)
<a name="l00177"></a>00177   {
<a name="l00178"></a>00178 
<a name="l00179"></a>00179     <span class="keywordflow">if</span> (i != this-&gt;m_)
<a name="l00180"></a>00180       {
<a name="l00181"></a>00181         this-&gt;m_ = i;
<a name="l00182"></a>00182         this-&gt;n_ = i;
<a name="l00183"></a>00183 
<a name="l00184"></a>00184 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00185"></a>00185 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l00186"></a>00186           {
<a name="l00187"></a>00187 <span class="preprocessor">#endif</span>
<a name="l00188"></a>00188 <span class="preprocessor"></span>
<a name="l00189"></a>00189             this-&gt;data_ =
<a name="l00190"></a>00190               <span class="keyword">reinterpret_cast&lt;</span>pointer<span class="keyword">&gt;</span>(this-&gt;allocator_
<a name="l00191"></a>00191                                         .reallocate(this-&gt;data_,
<a name="l00192"></a>00192                                                     (i * (i + 1)) / 2,
<a name="l00193"></a>00193                                                     <span class="keyword">this</span>));
<a name="l00194"></a>00194 
<a name="l00195"></a>00195 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00196"></a>00196 <span class="preprocessor"></span>          }
<a name="l00197"></a>00197         <span class="keywordflow">catch</span> (...)
<a name="l00198"></a>00198           {
<a name="l00199"></a>00199             this-&gt;m_ = 0;
<a name="l00200"></a>00200             this-&gt;n_ = 0;
<a name="l00201"></a>00201             this-&gt;data_ = NULL;
<a name="l00202"></a>00202             <span class="keywordflow">throw</span> NoMemory(<span class="stringliteral">&quot;Matrix_SymPacked::Reallocate(int, int)&quot;</span>,
<a name="l00203"></a>00203                            <span class="stringliteral">&quot;Unable to reallocate memory for data_.&quot;</span>);
<a name="l00204"></a>00204           }
<a name="l00205"></a>00205         <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00206"></a>00206           {
<a name="l00207"></a>00207             this-&gt;m_ = 0;
<a name="l00208"></a>00208             this-&gt;n_ = 0;
<a name="l00209"></a>00209             <span class="keywordflow">throw</span> NoMemory(<span class="stringliteral">&quot;Matrix_SymPacked::Reallocate(int, int)&quot;</span>,
<a name="l00210"></a>00210                            <span class="stringliteral">&quot;Unable to reallocate memory for data_.&quot;</span>);
<a name="l00211"></a>00211           }
<a name="l00212"></a>00212 <span class="preprocessor">#endif</span>
<a name="l00213"></a>00213 <span class="preprocessor"></span>
<a name="l00214"></a>00214       }
<a name="l00215"></a>00215   }
<a name="l00216"></a>00216 
<a name="l00217"></a>00217 
<a name="l00220"></a>00220 
<a name="l00234"></a>00234   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00235"></a>00235   <span class="keyword">inline</span> <span class="keywordtype">void</span> Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;
<a name="l00236"></a>00236   ::SetData(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00237"></a>00237             <span class="keyword">typename</span> Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;
<a name="l00238"></a>00238             ::pointer data)
<a name="l00239"></a>00239   {
<a name="l00240"></a>00240     this-&gt;Clear();
<a name="l00241"></a>00241 
<a name="l00242"></a>00242     this-&gt;m_ = i;
<a name="l00243"></a>00243     this-&gt;n_ = i;
<a name="l00244"></a>00244 
<a name="l00245"></a>00245     this-&gt;data_ = data;
<a name="l00246"></a>00246   }
<a name="l00247"></a>00247 
<a name="l00248"></a>00248 
<a name="l00250"></a>00250 
<a name="l00254"></a>00254   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00255"></a>00255   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a17adea2b2fb65d8d26e851b558e284d7" title="Clears the matrix without releasing memory.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::Nullify</a>()
<a name="l00256"></a>00256   {
<a name="l00257"></a>00257     this-&gt;data_ = NULL;
<a name="l00258"></a>00258     this-&gt;m_ = 0;
<a name="l00259"></a>00259     this-&gt;n_ = 0;
<a name="l00260"></a>00260   }
<a name="l00261"></a>00261 
<a name="l00262"></a>00262 
<a name="l00263"></a>00263   <span class="comment">/**********************************</span>
<a name="l00264"></a>00264 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l00265"></a>00265 <span class="comment">   **********************************/</span>
<a name="l00266"></a>00266 
<a name="l00267"></a>00267 
<a name="l00269"></a>00269 
<a name="l00275"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a8ea7c92005432ab73b7b8fbbe37e4868">00275</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00276"></a>00276   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php" title="Symmetric packed matrix class.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00277"></a>00277   <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a8ea7c92005432ab73b7b8fbbe37e4868" title="Access operator.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00278"></a>00278   {
<a name="l00279"></a>00279 
<a name="l00280"></a>00280 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00281"></a>00281 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00282"></a>00282       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_SymPacked::operator()&quot;</span>,
<a name="l00283"></a>00283                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00284"></a>00284                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00285"></a>00285     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00286"></a>00286       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_SymPacked::operator()&quot;</span>,
<a name="l00287"></a>00287                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00288"></a>00288                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00289"></a>00289 <span class="preprocessor">#endif</span>
<a name="l00290"></a>00290 <span class="preprocessor"></span>
<a name="l00291"></a>00291     <span class="keywordflow">return</span> this-&gt;data_[j &gt; i
<a name="l00292"></a>00292                        ? Storage::GetFirst(i * this-&gt;n_
<a name="l00293"></a>00293                                            - (i * (i + 1)) / 2 + j,
<a name="l00294"></a>00294                                            (j*(j+1)) / 2 + i)
<a name="l00295"></a>00295                        : Storage::GetFirst(j * this-&gt;m_
<a name="l00296"></a>00296                                            - (j * (j + 1)) / 2 + i,
<a name="l00297"></a>00297                                            (i * (i + 1)) / 2 + j)];
<a name="l00298"></a>00298   }
<a name="l00299"></a>00299 
<a name="l00300"></a>00300 
<a name="l00302"></a>00302 
<a name="l00308"></a>00308   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00309"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a2323bcd2dcd381a1b02e5e6c308bb4f0">00309</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php" title="Symmetric packed matrix class.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00310"></a>00310   ::const_reference
<a name="l00311"></a>00311   <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a8ea7c92005432ab73b7b8fbbe37e4868" title="Access operator.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i,
<a name="l00312"></a>00312                                                              <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00313"></a>00313 <span class="keyword">  </span>{
<a name="l00314"></a>00314 
<a name="l00315"></a>00315 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00316"></a>00316 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00317"></a>00317       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_SymPacked::operator()&quot;</span>,
<a name="l00318"></a>00318                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00319"></a>00319                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00320"></a>00320     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00321"></a>00321       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_SymPacked::operator()&quot;</span>,
<a name="l00322"></a>00322                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00323"></a>00323                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00324"></a>00324 <span class="preprocessor">#endif</span>
<a name="l00325"></a>00325 <span class="preprocessor"></span>
<a name="l00326"></a>00326     <span class="keywordflow">return</span> this-&gt;data_[j &gt; i
<a name="l00327"></a>00327                        ? Storage::GetFirst(i * this-&gt;n_
<a name="l00328"></a>00328                                            - (i * (i + 1)) / 2 + j,
<a name="l00329"></a>00329                                            (j * (j + 1)) / 2 + i)
<a name="l00330"></a>00330                        : Storage::GetFirst(j * this-&gt;m_
<a name="l00331"></a>00331                                            - (j * (j + 1)) / 2 + i,
<a name="l00332"></a>00332                                            (i * (i + 1)) / 2 + j)];
<a name="l00333"></a>00333   }
<a name="l00334"></a>00334 
<a name="l00335"></a>00335 
<a name="l00337"></a>00337 
<a name="l00344"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a8bcf6acc85a1b7d64735db18587896d9">00344</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00345"></a>00345   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php" title="Symmetric packed matrix class.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00346"></a>00346   <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a8bcf6acc85a1b7d64735db18587896d9" title="Direct access method.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00347"></a>00347   {
<a name="l00348"></a>00348 
<a name="l00349"></a>00349 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00350"></a>00350 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00351"></a>00351       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_SymPacked::Val(int, int)&quot;</span>,
<a name="l00352"></a>00352                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00353"></a>00353                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00354"></a>00354     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00355"></a>00355       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_SymPacked::Val(int, int)&quot;</span>,
<a name="l00356"></a>00356                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00357"></a>00357                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00358"></a>00358 <span class="preprocessor">#endif</span>
<a name="l00359"></a>00359 <span class="preprocessor"></span>
<a name="l00360"></a>00360     <span class="keywordflow">return</span> this-&gt;data_[j &gt; i
<a name="l00361"></a>00361                        ? Storage::GetFirst(i * this-&gt;n_
<a name="l00362"></a>00362                                            - (i * (i + 1)) / 2 + j,
<a name="l00363"></a>00363                                            (j * (j + 1)) / 2 + i)
<a name="l00364"></a>00364                        : Storage::GetFirst(j * this-&gt;m_
<a name="l00365"></a>00365                                            - (j * (j + 1)) / 2 + i,
<a name="l00366"></a>00366                                            (i * (i + 1)) / 2 + j)];
<a name="l00367"></a>00367   }
<a name="l00368"></a>00368 
<a name="l00369"></a>00369 
<a name="l00371"></a>00371 
<a name="l00378"></a>00378   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00379"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#ae7199787c1e2ee658a1590e7e1dd45a8">00379</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php" title="Symmetric packed matrix class.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00380"></a>00380   ::const_reference
<a name="l00381"></a>00381   <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a8bcf6acc85a1b7d64735db18587896d9" title="Direct access method.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00382"></a>00382 <span class="keyword">  </span>{
<a name="l00383"></a>00383 
<a name="l00384"></a>00384 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00385"></a>00385 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00386"></a>00386       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_SymPacked::Val(int, int) const&quot;</span>,
<a name="l00387"></a>00387                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00388"></a>00388                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00389"></a>00389     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00390"></a>00390       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_SymPacked::Val(int, int) const&quot;</span>,
<a name="l00391"></a>00391                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00392"></a>00392                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00393"></a>00393 <span class="preprocessor">#endif</span>
<a name="l00394"></a>00394 <span class="preprocessor"></span>
<a name="l00395"></a>00395     <span class="keywordflow">return</span> this-&gt;data_[j &gt; i
<a name="l00396"></a>00396                        ? Storage::GetFirst(i * this-&gt;n_
<a name="l00397"></a>00397                                            - (i * (i + 1)) / 2 + j,
<a name="l00398"></a>00398                                            (j * (j + 1)) / 2 + i)
<a name="l00399"></a>00399                        : Storage::GetFirst(j * this-&gt;m_
<a name="l00400"></a>00400                                            - (j * (j + 1)) / 2 + i,
<a name="l00401"></a>00401                                            (i * (i + 1)) / 2 + j)];
<a name="l00402"></a>00402   }
<a name="l00403"></a>00403 
<a name="l00404"></a>00404 
<a name="l00406"></a>00406 
<a name="l00412"></a>00412   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00413"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#af85e9c2911cfc58102741472792c8675">00413</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php" title="Symmetric packed matrix class.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00414"></a>00414   ::const_reference
<a name="l00415"></a>00415   <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a50fb81fc0ddaa05bee8e9934a1beca8f" title="Returns the element (i, j).">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00416"></a>00416 <span class="keyword">  </span>{
<a name="l00417"></a>00417     <span class="keywordflow">return</span> this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a8bcf6acc85a1b7d64735db18587896d9" title="Direct access method.">Val</a>(i, j);
<a name="l00418"></a>00418   }
<a name="l00419"></a>00419 
<a name="l00420"></a>00420 
<a name="l00422"></a>00422 
<a name="l00428"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a50fb81fc0ddaa05bee8e9934a1beca8f">00428</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00429"></a>00429   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php" title="Symmetric packed matrix class.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00430"></a>00430   ::reference <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a50fb81fc0ddaa05bee8e9934a1beca8f" title="Returns the element (i, j).">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00431"></a>00431   {
<a name="l00432"></a>00432     <span class="keywordflow">return</span> this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a8bcf6acc85a1b7d64735db18587896d9" title="Direct access method.">Val</a>(i, j);
<a name="l00433"></a>00433   }
<a name="l00434"></a>00434 
<a name="l00435"></a>00435 
<a name="l00437"></a>00437 
<a name="l00442"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a57a7db421db0df658c32ed661d935089">00442</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00443"></a>00443   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php" title="Symmetric packed matrix class.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00444"></a>00444   <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a57a7db421db0df658c32ed661d935089" title="Access to elements of the data array.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::operator[] </a>(<span class="keywordtype">int</span> i)
<a name="l00445"></a>00445   {
<a name="l00446"></a>00446 
<a name="l00447"></a>00447 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00448"></a>00448 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a3fed51d6dd6d46d8e611ce98f9d8f724" title="Returns the number of elements stored in memory.">GetDataSize</a>())
<a name="l00449"></a>00449       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Matrix_SymPacked::operator[] (int)&quot;</span>,
<a name="l00450"></a>00450                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00451"></a>00451                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a3fed51d6dd6d46d8e611ce98f9d8f724" title="Returns the number of elements stored in memory.">GetDataSize</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00452"></a>00452                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00453"></a>00453 <span class="preprocessor">#endif</span>
<a name="l00454"></a>00454 <span class="preprocessor"></span>
<a name="l00455"></a>00455     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00456"></a>00456   }
<a name="l00457"></a>00457 
<a name="l00458"></a>00458 
<a name="l00460"></a>00460 
<a name="l00465"></a>00465   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00466"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a7628441645b1367f95b15d1f77a208af">00466</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php" title="Symmetric packed matrix class.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00467"></a>00467   ::const_reference
<a name="l00468"></a>00468   <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a57a7db421db0df658c32ed661d935089" title="Access to elements of the data array.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::operator[] </a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00469"></a>00469 <span class="keyword">  </span>{
<a name="l00470"></a>00470 
<a name="l00471"></a>00471 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00472"></a>00472 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a3fed51d6dd6d46d8e611ce98f9d8f724" title="Returns the number of elements stored in memory.">GetDataSize</a>())
<a name="l00473"></a>00473       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Matrix_SymPacked::operator[] (int) const&quot;</span>,
<a name="l00474"></a>00474                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00475"></a>00475                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a3fed51d6dd6d46d8e611ce98f9d8f724" title="Returns the number of elements stored in memory.">GetDataSize</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00476"></a>00476                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00477"></a>00477 <span class="preprocessor">#endif</span>
<a name="l00478"></a>00478 <span class="preprocessor"></span>
<a name="l00479"></a>00479     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00480"></a>00480   }
<a name="l00481"></a>00481 
<a name="l00482"></a>00482 
<a name="l00484"></a>00484 
<a name="l00489"></a>00489   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00490"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a9a70002e76a6930a24aa0b724c6976a1">00490</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php" title="Symmetric packed matrix class.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00491"></a>00491   <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a9a70002e76a6930a24aa0b724c6976a1" title="Duplicates a matrix (assignment operator).">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00492"></a>00492 <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a9a70002e76a6930a24aa0b724c6976a1" title="Duplicates a matrix (assignment operator).">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php" title="Symmetric packed matrix class.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00493"></a>00493   {
<a name="l00494"></a>00494     this-&gt;Copy(A);
<a name="l00495"></a>00495 
<a name="l00496"></a>00496     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00497"></a>00497   }
<a name="l00498"></a>00498 
<a name="l00499"></a>00499 
<a name="l00501"></a>00501 
<a name="l00506"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a0db0736203c7e574e0fcdd931f889c41">00506</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00507"></a>00507   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a0db0736203c7e574e0fcdd931f889c41" title="Sets an element of the matrix.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00508"></a>00508 <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a0db0736203c7e574e0fcdd931f889c41" title="Sets an element of the matrix.">  ::Set</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; x)
<a name="l00509"></a>00509   {
<a name="l00510"></a>00510     this-&gt;Val(i, j) = x;
<a name="l00511"></a>00511   }
<a name="l00512"></a>00512 
<a name="l00513"></a>00513 
<a name="l00515"></a>00515 
<a name="l00520"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#afcaec454efa976f5e61e9c6188df5ae7">00520</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00521"></a>00521   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#afcaec454efa976f5e61e9c6188df5ae7" title="Duplicates a matrix.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00522"></a>00522 <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#afcaec454efa976f5e61e9c6188df5ae7" title="Duplicates a matrix.">  ::Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php" title="Symmetric packed matrix class.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00523"></a>00523   {
<a name="l00524"></a>00524     this-&gt;Reallocate(A.<a class="code" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927" title="Returns the number of rows.">GetM</a>(), A.<a class="code" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984" title="Returns the number of columns.">GetN</a>());
<a name="l00525"></a>00525 
<a name="l00526"></a>00526     this-&gt;allocator_.memorycpy(this-&gt;data_, A.<a class="code" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a" title="Returns a pointer to the data array.">GetData</a>(), this-&gt;GetDataSize());
<a name="l00527"></a>00527   }
<a name="l00528"></a>00528 
<a name="l00529"></a>00529 
<a name="l00530"></a>00530   <span class="comment">/************************</span>
<a name="l00531"></a>00531 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l00532"></a>00532 <span class="comment">   ************************/</span>
<a name="l00533"></a>00533 
<a name="l00534"></a>00534 
<a name="l00536"></a>00536 
<a name="l00540"></a>00540   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00541"></a>00541   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a2774131a1c9e73091962ed97259d312d" title="Sets all elements to zero.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::Zero</a>()
<a name="l00542"></a>00542   {
<a name="l00543"></a>00543     this-&gt;allocator_.memoryset(this-&gt;data_, <span class="keywordtype">char</span>(0),
<a name="l00544"></a>00544                                this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a3fed51d6dd6d46d8e611ce98f9d8f724" title="Returns the number of elements stored in memory.">GetDataSize</a>() * <span class="keyword">sizeof</span>(value_type));
<a name="l00545"></a>00545   }
<a name="l00546"></a>00546 
<a name="l00547"></a>00547 
<a name="l00549"></a>00549   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00550"></a>00550   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a86973f10c1197e1a62c2df95049f5c69" title="Sets the matrix to the identity.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::SetIdentity</a>()
<a name="l00551"></a>00551   {
<a name="l00552"></a>00552     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a8812e90aba4832686109be1f0094362c" title="Fills the matrix with 0, 1, 2, ...">Fill</a>(T(0));
<a name="l00553"></a>00553 
<a name="l00554"></a>00554     T one(1);
<a name="l00555"></a>00555     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; min(this-&gt;m_, this-&gt;n_); i++)
<a name="l00556"></a>00556       (*<span class="keyword">this</span>)(i, i) = one;
<a name="l00557"></a>00557   }
<a name="l00558"></a>00558 
<a name="l00559"></a>00559 
<a name="l00561"></a>00561 
<a name="l00565"></a>00565   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00566"></a>00566   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a8812e90aba4832686109be1f0094362c" title="Fills the matrix with 0, 1, 2, ...">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::Fill</a>()
<a name="l00567"></a>00567   {
<a name="l00568"></a>00568     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a3fed51d6dd6d46d8e611ce98f9d8f724" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00569"></a>00569       this-&gt;data_[i] = i;
<a name="l00570"></a>00570   }
<a name="l00571"></a>00571 
<a name="l00572"></a>00572 
<a name="l00574"></a>00574 
<a name="l00577"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a0c98bdfc02b66c4c5d94d572bb46c9d3">00577</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00578"></a>00578   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00579"></a>00579   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a8812e90aba4832686109be1f0094362c" title="Fills the matrix with 0, 1, 2, ...">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::Fill</a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00580"></a>00580   {
<a name="l00581"></a>00581     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a3fed51d6dd6d46d8e611ce98f9d8f724" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00582"></a>00582       this-&gt;data_[i] = x;
<a name="l00583"></a>00583   }
<a name="l00584"></a>00584 
<a name="l00585"></a>00585 
<a name="l00587"></a>00587 
<a name="l00590"></a>00590   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00591"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a63be5be398e6a3be1f028f6c6966766c">00591</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00592"></a>00592   <a class="code" href="class_seldon_1_1_matrix___sym_packed.php" title="Symmetric packed matrix class.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00593"></a>00593   <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a9a70002e76a6930a24aa0b724c6976a1" title="Duplicates a matrix (assignment operator).">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00594"></a>00594   {
<a name="l00595"></a>00595     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a8812e90aba4832686109be1f0094362c" title="Fills the matrix with 0, 1, 2, ...">Fill</a>(x);
<a name="l00596"></a>00596 
<a name="l00597"></a>00597     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00598"></a>00598   }
<a name="l00599"></a>00599 
<a name="l00600"></a>00600 
<a name="l00602"></a>00602 
<a name="l00605"></a>00605   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00606"></a>00606   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#aa5657b619208fb20d1091ffb34a6a354" title="Fills the matrix randomly.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::FillRand</a>()
<a name="l00607"></a>00607   {
<a name="l00608"></a>00608     srand(time(NULL));
<a name="l00609"></a>00609     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a3fed51d6dd6d46d8e611ce98f9d8f724" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00610"></a>00610       this-&gt;data_[i] = rand();
<a name="l00611"></a>00611   }
<a name="l00612"></a>00612 
<a name="l00613"></a>00613 
<a name="l00615"></a>00615 
<a name="l00620"></a>00620   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00621"></a>00621   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a6a31d1d0e30519f28a456fdd3e7a2a72" title="Displays the matrix on the standard output.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00622"></a>00622 <span class="keyword">  </span>{
<a name="l00623"></a>00623     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00624"></a>00624       {
<a name="l00625"></a>00625         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;n_; j++)
<a name="l00626"></a>00626           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00627"></a>00627         cout &lt;&lt; endl;
<a name="l00628"></a>00628       }
<a name="l00629"></a>00629   }
<a name="l00630"></a>00630 
<a name="l00631"></a>00631 
<a name="l00633"></a>00633 
<a name="l00644"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a99cb75b68a8f4542df790f105bdf438e">00644</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00645"></a>00645   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a6a31d1d0e30519f28a456fdd3e7a2a72" title="Displays the matrix on the standard output.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00646"></a>00646 <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a6a31d1d0e30519f28a456fdd3e7a2a72" title="Displays the matrix on the standard output.">  ::Print</a>(<span class="keywordtype">int</span> a, <span class="keywordtype">int</span> b, <span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n)<span class="keyword"> const</span>
<a name="l00647"></a>00647 <span class="keyword">  </span>{
<a name="l00648"></a>00648     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = a; i &lt; min(this-&gt;m_, a + m); i++)
<a name="l00649"></a>00649       {
<a name="l00650"></a>00650         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = b; j &lt; min(this-&gt;n_, b + n); j++)
<a name="l00651"></a>00651           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00652"></a>00652         cout &lt;&lt; endl;
<a name="l00653"></a>00653       }
<a name="l00654"></a>00654   }
<a name="l00655"></a>00655 
<a name="l00656"></a>00656 
<a name="l00658"></a>00658 
<a name="l00666"></a>00666   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00667"></a>00667   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a6a31d1d0e30519f28a456fdd3e7a2a72" title="Displays the matrix on the standard output.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::Print</a>(<span class="keywordtype">int</span> l)<span class="keyword"> const</span>
<a name="l00668"></a>00668 <span class="keyword">  </span>{
<a name="l00669"></a>00669     <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a6a31d1d0e30519f28a456fdd3e7a2a72" title="Displays the matrix on the standard output.">Print</a>(0, 0, l, l);
<a name="l00670"></a>00670   }
<a name="l00671"></a>00671 
<a name="l00672"></a>00672 
<a name="l00673"></a>00673   <span class="comment">/**************************</span>
<a name="l00674"></a>00674 <span class="comment">   * INPUT/OUTPUT FUNCTIONS *</span>
<a name="l00675"></a>00675 <span class="comment">   **************************/</span>
<a name="l00676"></a>00676 
<a name="l00677"></a>00677 
<a name="l00679"></a>00679 
<a name="l00686"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a49cb3c589cb0ff56b187dec20f52d1af">00686</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00687"></a>00687   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a49cb3c589cb0ff56b187dec20f52d1af" title="Writes the matrix in a file.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00688"></a>00688 <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a49cb3c589cb0ff56b187dec20f52d1af" title="Writes the matrix in a file.">  ::Write</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00689"></a>00689 <span class="keyword">  </span>{
<a name="l00690"></a>00690     ofstream FileStream;
<a name="l00691"></a>00691     FileStream.open(FileName.c_str(), ofstream::binary);
<a name="l00692"></a>00692 
<a name="l00693"></a>00693 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00694"></a>00694 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00695"></a>00695     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00696"></a>00696       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_SymPacked::Write(string FileName)&quot;</span>,
<a name="l00697"></a>00697                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00698"></a>00698 <span class="preprocessor">#endif</span>
<a name="l00699"></a>00699 <span class="preprocessor"></span>
<a name="l00700"></a>00700     this-&gt;Write(FileStream);
<a name="l00701"></a>00701 
<a name="l00702"></a>00702     FileStream.close();
<a name="l00703"></a>00703   }
<a name="l00704"></a>00704 
<a name="l00705"></a>00705 
<a name="l00707"></a>00707 
<a name="l00714"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a286cd3b79c5ece685fc82afa068bf4a1">00714</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00715"></a>00715   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a49cb3c589cb0ff56b187dec20f52d1af" title="Writes the matrix in a file.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00716"></a>00716 <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a49cb3c589cb0ff56b187dec20f52d1af" title="Writes the matrix in a file.">  ::Write</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00717"></a>00717 <span class="keyword">  </span>{
<a name="l00718"></a>00718 
<a name="l00719"></a>00719 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00720"></a>00720 <span class="preprocessor"></span>    <span class="comment">// Checks if the file is ready.</span>
<a name="l00721"></a>00721     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00722"></a>00722       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_SymPacked::Write(ostream&amp; FileStream)&quot;</span>,
<a name="l00723"></a>00723                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00724"></a>00724 <span class="preprocessor">#endif</span>
<a name="l00725"></a>00725 <span class="preprocessor"></span>
<a name="l00726"></a>00726     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l00727"></a>00727                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00728"></a>00728     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;n_)),
<a name="l00729"></a>00729                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00730"></a>00730 
<a name="l00731"></a>00731     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l00732"></a>00732                      this-&gt;GetDataSize() * <span class="keyword">sizeof</span>(value_type));
<a name="l00733"></a>00733 
<a name="l00734"></a>00734 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00735"></a>00735 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00736"></a>00736     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00737"></a>00737       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_SymPacked::Write(ostream&amp; FileStream)&quot;</span>,
<a name="l00738"></a>00738                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l00739"></a>00739 <span class="preprocessor">#endif</span>
<a name="l00740"></a>00740 <span class="preprocessor"></span>
<a name="l00741"></a>00741   }
<a name="l00742"></a>00742 
<a name="l00743"></a>00743 
<a name="l00745"></a>00745 
<a name="l00752"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#ac0894fe788885ee8cc4bb73b862d9976">00752</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00753"></a>00753   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#ac0894fe788885ee8cc4bb73b862d9976" title="Writes the matrix in a file.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00754"></a>00754 <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#ac0894fe788885ee8cc4bb73b862d9976" title="Writes the matrix in a file.">  ::WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00755"></a>00755 <span class="keyword">  </span>{
<a name="l00756"></a>00756     ofstream FileStream;
<a name="l00757"></a>00757     FileStream.precision(cout.precision());
<a name="l00758"></a>00758     FileStream.flags(cout.flags());
<a name="l00759"></a>00759     FileStream.open(FileName.c_str());
<a name="l00760"></a>00760 
<a name="l00761"></a>00761 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00762"></a>00762 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00763"></a>00763     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00764"></a>00764       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_SymPacked::WriteText(string FileName)&quot;</span>,
<a name="l00765"></a>00765                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00766"></a>00766 <span class="preprocessor">#endif</span>
<a name="l00767"></a>00767 <span class="preprocessor"></span>
<a name="l00768"></a>00768     this-&gt;WriteText(FileStream);
<a name="l00769"></a>00769 
<a name="l00770"></a>00770     FileStream.close();
<a name="l00771"></a>00771   }
<a name="l00772"></a>00772 
<a name="l00773"></a>00773 
<a name="l00775"></a>00775 
<a name="l00782"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#afe97f8a573b6b59e4d790b7da35a2e11">00782</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00783"></a>00783   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#ac0894fe788885ee8cc4bb73b862d9976" title="Writes the matrix in a file.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00784"></a>00784 <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#ac0894fe788885ee8cc4bb73b862d9976" title="Writes the matrix in a file.">  ::WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00785"></a>00785 <span class="keyword">  </span>{
<a name="l00786"></a>00786 
<a name="l00787"></a>00787 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00788"></a>00788 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00789"></a>00789     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00790"></a>00790       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_SymPacked::WriteText(ostream&amp; FileStream)&quot;</span>,
<a name="l00791"></a>00791                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00792"></a>00792 <span class="preprocessor">#endif</span>
<a name="l00793"></a>00793 <span class="preprocessor"></span>
<a name="l00794"></a>00794     <span class="keywordtype">int</span> i, j;
<a name="l00795"></a>00795     <span class="keywordflow">for</span> (i = 0; i &lt; this-&gt;GetM(); i++)
<a name="l00796"></a>00796       {
<a name="l00797"></a>00797         <span class="keywordflow">for</span> (j = 0; j &lt; this-&gt;GetN(); j++)
<a name="l00798"></a>00798           FileStream &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="charliteral">&#39;\t&#39;</span>;
<a name="l00799"></a>00799         FileStream &lt;&lt; endl;
<a name="l00800"></a>00800       }
<a name="l00801"></a>00801 
<a name="l00802"></a>00802 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00803"></a>00803 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00804"></a>00804     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00805"></a>00805       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_SymPacked::WriteText(ostream&amp; FileStream)&quot;</span>,
<a name="l00806"></a>00806                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l00807"></a>00807 <span class="preprocessor">#endif</span>
<a name="l00808"></a>00808 <span class="preprocessor"></span>
<a name="l00809"></a>00809   }
<a name="l00810"></a>00810 
<a name="l00811"></a>00811 
<a name="l00813"></a>00813 
<a name="l00820"></a>00820   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00821"></a>00821   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a29e41ce8ba3144c1a6982d55c035dfea" title="Reads the matrix from a file.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;::Read</a>(<span class="keywordtype">string</span> FileName)
<a name="l00822"></a>00822   {
<a name="l00823"></a>00823     ifstream FileStream;
<a name="l00824"></a>00824     FileStream.open(FileName.c_str(), ifstream::binary);
<a name="l00825"></a>00825 
<a name="l00826"></a>00826 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00827"></a>00827 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00828"></a>00828     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00829"></a>00829       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_SymPacked::Read(string FileName)&quot;</span>,
<a name="l00830"></a>00830                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00831"></a>00831 <span class="preprocessor">#endif</span>
<a name="l00832"></a>00832 <span class="preprocessor"></span>
<a name="l00833"></a>00833     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a29e41ce8ba3144c1a6982d55c035dfea" title="Reads the matrix from a file.">Read</a>(FileStream);
<a name="l00834"></a>00834 
<a name="l00835"></a>00835     FileStream.close();
<a name="l00836"></a>00836   }
<a name="l00837"></a>00837 
<a name="l00838"></a>00838 
<a name="l00840"></a>00840 
<a name="l00847"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#ab52485427217c02d912ee5812dcbc4a9">00847</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00848"></a>00848   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a29e41ce8ba3144c1a6982d55c035dfea" title="Reads the matrix from a file.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00849"></a>00849 <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a29e41ce8ba3144c1a6982d55c035dfea" title="Reads the matrix from a file.">  ::Read</a>(istream&amp; FileStream)
<a name="l00850"></a>00850   {
<a name="l00851"></a>00851 
<a name="l00852"></a>00852 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00853"></a>00853 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00854"></a>00854     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00855"></a>00855       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_SymPacked::Read(istream&amp; FileStream)&quot;</span>,
<a name="l00856"></a>00856                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00857"></a>00857 <span class="preprocessor">#endif</span>
<a name="l00858"></a>00858 <span class="preprocessor"></span>
<a name="l00859"></a>00859     <span class="keywordtype">int</span> new_m, new_n;
<a name="l00860"></a>00860     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;new_m), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00861"></a>00861     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;new_n), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00862"></a>00862     this-&gt;Reallocate(new_m, new_n);
<a name="l00863"></a>00863 
<a name="l00864"></a>00864     FileStream.read(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l00865"></a>00865                     this-&gt;GetDataSize() * <span class="keyword">sizeof</span>(value_type));
<a name="l00866"></a>00866 
<a name="l00867"></a>00867 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00868"></a>00868 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l00869"></a>00869     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00870"></a>00870       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_SymPacked::Read(istream&amp; FileStream)&quot;</span>,
<a name="l00871"></a>00871                     <span class="stringliteral">&quot;Input operation failed.&quot;</span>);
<a name="l00872"></a>00872 <span class="preprocessor">#endif</span>
<a name="l00873"></a>00873 <span class="preprocessor"></span>
<a name="l00874"></a>00874   }
<a name="l00875"></a>00875 
<a name="l00876"></a>00876 
<a name="l00878"></a>00878 
<a name="l00882"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a64167ea39ea64fe44d1d1e1c838b4276">00882</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00883"></a>00883   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a64167ea39ea64fe44d1d1e1c838b4276" title="Reads the matrix from a file.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00884"></a>00884 <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a64167ea39ea64fe44d1d1e1c838b4276" title="Reads the matrix from a file.">  ::ReadText</a>(<span class="keywordtype">string</span> FileName)
<a name="l00885"></a>00885   {
<a name="l00886"></a>00886     ifstream FileStream;
<a name="l00887"></a>00887     FileStream.open(FileName.c_str());
<a name="l00888"></a>00888 
<a name="l00889"></a>00889 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00890"></a>00890 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00891"></a>00891     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00892"></a>00892       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(string FileName)&quot;</span>,
<a name="l00893"></a>00893                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00894"></a>00894 <span class="preprocessor">#endif</span>
<a name="l00895"></a>00895 <span class="preprocessor"></span>
<a name="l00896"></a>00896     this-&gt;ReadText(FileStream);
<a name="l00897"></a>00897 
<a name="l00898"></a>00898     FileStream.close();
<a name="l00899"></a>00899   }
<a name="l00900"></a>00900 
<a name="l00901"></a>00901 
<a name="l00903"></a>00903 
<a name="l00907"></a><a class="code" href="class_seldon_1_1_matrix___sym_packed.php#aab6dd6ad39b5cede189404b75b008c94">00907</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00908"></a>00908   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a64167ea39ea64fe44d1d1e1c838b4276" title="Reads the matrix from a file.">Matrix_SymPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00909"></a>00909 <a class="code" href="class_seldon_1_1_matrix___sym_packed.php#a64167ea39ea64fe44d1d1e1c838b4276" title="Reads the matrix from a file.">  ::ReadText</a>(istream&amp; FileStream)
<a name="l00910"></a>00910   {
<a name="l00911"></a>00911     <span class="comment">// Clears the matrix.</span>
<a name="l00912"></a>00912     Clear();
<a name="l00913"></a>00913 
<a name="l00914"></a>00914 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00915"></a>00915 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00916"></a>00916     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00917"></a>00917       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_SymPacked::ReadText(istream&amp; FileStream)&quot;</span>,
<a name="l00918"></a>00918                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00919"></a>00919 <span class="preprocessor">#endif</span>
<a name="l00920"></a>00920 <span class="preprocessor"></span>
<a name="l00921"></a>00921     <span class="comment">// Reads the first line.</span>
<a name="l00922"></a>00922     <span class="keywordtype">string</span> line;
<a name="l00923"></a>00923     getline(FileStream, line);
<a name="l00924"></a>00924     <span class="keywordflow">if</span> (FileStream.fail())
<a name="l00925"></a>00925       <span class="comment">// Is the file empty?</span>
<a name="l00926"></a>00926       <span class="keywordflow">return</span>;
<a name="l00927"></a>00927 
<a name="l00928"></a>00928     <span class="comment">// Converts the first line into a vector.</span>
<a name="l00929"></a>00929     istringstream line_stream(line);
<a name="l00930"></a>00930     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> first_row;
<a name="l00931"></a>00931     first_row.ReadText(line_stream);
<a name="l00932"></a>00932 
<a name="l00933"></a>00933     <span class="comment">// Now reads all other rows, and puts them in a single vector.</span>
<a name="l00934"></a>00934     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> other_row;
<a name="l00935"></a>00935     other_row.ReadText(FileStream);
<a name="l00936"></a>00936 
<a name="l00937"></a>00937     <span class="comment">// Number of rows and columns.</span>
<a name="l00938"></a>00938     <span class="keywordtype">int</span> n = first_row.GetM();
<a name="l00939"></a>00939     <span class="keywordtype">int</span> m = 1 + other_row.GetM() / n;
<a name="l00940"></a>00940 
<a name="l00941"></a>00941 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00942"></a>00942 <span class="preprocessor"></span>    <span class="comment">// Checks that enough elements were read.</span>
<a name="l00943"></a>00943     <span class="keywordflow">if</span> (other_row.GetM() != (m - 1) * n)
<a name="l00944"></a>00944       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_SymPacked::ReadText(istream&amp; FileStream)&quot;</span>,
<a name="l00945"></a>00945                     <span class="stringliteral">&quot;Not all rows have the same number of columns.&quot;</span>);
<a name="l00946"></a>00946 <span class="preprocessor">#endif</span>
<a name="l00947"></a>00947 <span class="preprocessor"></span>
<a name="l00948"></a>00948     this-&gt;Reallocate(m,n);
<a name="l00949"></a>00949 
<a name="l00950"></a>00950     <span class="comment">// Fills the matrix.</span>
<a name="l00951"></a>00951     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l00952"></a>00952       this-&gt;Val(0, j) = first_row(j);
<a name="l00953"></a>00953     <span class="keywordtype">int</span> k = 0;
<a name="l00954"></a>00954     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 1; i &lt; m; i++)
<a name="l00955"></a>00955       {
<a name="l00956"></a>00956         k += i;
<a name="l00957"></a>00957         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = i; j &lt; n; j++)
<a name="l00958"></a>00958           this-&gt;Val(i, j) = other_row(k++);
<a name="l00959"></a>00959       }
<a name="l00960"></a>00960   }
<a name="l00961"></a>00961 
<a name="l00962"></a>00962 
<a name="l00964"></a>00964   <span class="comment">// MATRIX&lt;COLSYMPACKED&gt; //</span>
<a name="l00966"></a>00966 <span class="comment"></span>
<a name="l00967"></a>00967 
<a name="l00968"></a>00968   <span class="comment">/****************</span>
<a name="l00969"></a>00969 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00970"></a>00970 <span class="comment">   ****************/</span>
<a name="l00971"></a>00971 
<a name="l00972"></a>00972 
<a name="l00974"></a>00974 
<a name="l00977"></a>00977   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00978"></a>00978   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSymPacked, Allocator&gt;::Matrix</a>():
<a name="l00979"></a>00979     <a class="code" href="class_seldon_1_1_matrix___sym_packed.php" title="Symmetric packed matrix class.">Matrix_SymPacked</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_sym_packed.php">ColSymPacked</a>, Allocator&gt;()
<a name="l00980"></a>00980   {
<a name="l00981"></a>00981   }
<a name="l00982"></a>00982 
<a name="l00983"></a>00983 
<a name="l00985"></a>00985 
<a name="l00990"></a>00990   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00991"></a>00991   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_packed_00_01_allocator_01_4.php#a751c1a6931312ab459584f2f73008a24" title="Default constructor.">Matrix&lt;T, Prop, ColSymPacked, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l00992"></a>00992     Matrix_SymPacked&lt;T, Prop, ColSymPacked, Allocator&gt;(i, i)
<a name="l00993"></a>00993   {
<a name="l00994"></a>00994   }
<a name="l00995"></a>00995 
<a name="l00996"></a>00996 
<a name="l00997"></a>00997   <span class="comment">/*****************</span>
<a name="l00998"></a>00998 <span class="comment">   * OTHER METHODS *</span>
<a name="l00999"></a>00999 <span class="comment">   *****************/</span>
<a name="l01000"></a>01000 
<a name="l01001"></a>01001 
<a name="l01003"></a>01003 
<a name="l01006"></a>01006   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01007"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_packed_00_01_allocator_01_4.php#a2be59cf2b48ebed2b1972cdf7791b209">01007</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01008"></a>01008   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_packed_00_01_allocator_01_4.php" title="Column-major symmetric packed matrix class.">Matrix&lt;T, Prop, ColSymPacked, Allocator&gt;</a>&amp;
<a name="l01009"></a>01009   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSymPacked, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01010"></a>01010   {
<a name="l01011"></a>01011     this-&gt;Fill(x);
<a name="l01012"></a>01012 
<a name="l01013"></a>01013     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01014"></a>01014   }
<a name="l01015"></a>01015 
<a name="l01017"></a>01017 
<a name="l01022"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_packed_00_01_allocator_01_4.php#a45876c70363a586163bcd7900fb972d0">01022</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01023"></a>01023   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_packed_00_01_allocator_01_4.php" title="Column-major symmetric packed matrix class.">Matrix&lt;T, Prop, ColSymPacked, Allocator&gt;</a>&amp;
<a name="l01024"></a>01024   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSymPacked, Allocator&gt;::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop,
<a name="l01025"></a>01025                                                        <a class="code" href="class_seldon_1_1_col_sym_packed.php">ColSymPacked</a>,
<a name="l01026"></a>01026                                                        Allocator&gt;&amp; A)
<a name="l01027"></a>01027   {
<a name="l01028"></a>01028     this-&gt;Copy(A);
<a name="l01029"></a>01029     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01030"></a>01030   }
<a name="l01031"></a>01031 
<a name="l01032"></a>01032 
<a name="l01034"></a>01034 
<a name="l01037"></a>01037   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01038"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_packed_00_01_allocator_01_4.php#aa5f0317f8db1d83626497941bf83e9e1">01038</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01039"></a>01039   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_packed_00_01_allocator_01_4.php" title="Column-major symmetric packed matrix class.">Matrix&lt;T, Prop, ColSymPacked, Allocator&gt;</a>&amp;
<a name="l01040"></a>01040   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSymPacked, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01041"></a>01041   {
<a name="l01042"></a>01042     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetDataSize();i++)
<a name="l01043"></a>01043       this-&gt;data_[i] *= x;
<a name="l01044"></a>01044 
<a name="l01045"></a>01045     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01046"></a>01046   }
<a name="l01047"></a>01047 
<a name="l01048"></a>01048 
<a name="l01050"></a>01050 
<a name="l01057"></a>01057   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01058"></a>01058   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSymPacked, Allocator&gt;::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01059"></a>01059   {
<a name="l01060"></a>01060 
<a name="l01061"></a>01061     <span class="comment">// Storing the old values of the matrix.</span>
<a name="l01062"></a>01062     <span class="keywordtype">int</span> nold = this-&gt;GetDataSize();
<a name="l01063"></a>01063     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> xold(nold);
<a name="l01064"></a>01064     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; nold; k++)
<a name="l01065"></a>01065       xold(k) = this-&gt;data_[k];
<a name="l01066"></a>01066 
<a name="l01067"></a>01067     <span class="comment">// Reallocation.</span>
<a name="l01068"></a>01068     this-&gt;Reallocate(i,j);
<a name="l01069"></a>01069 
<a name="l01070"></a>01070     <span class="comment">// Filling the matrix with its old values.</span>
<a name="l01071"></a>01071     <span class="keywordtype">int</span> nmin = min(nold, this-&gt;GetDataSize());
<a name="l01072"></a>01072     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; nmin; k++)
<a name="l01073"></a>01073       this-&gt;data_[k] = xold(k);
<a name="l01074"></a>01074   }
<a name="l01075"></a>01075 
<a name="l01076"></a>01076 
<a name="l01078"></a>01078   <span class="comment">// MATRIX&lt;ROWSYMPACKED&gt; //</span>
<a name="l01080"></a>01080 <span class="comment"></span>
<a name="l01081"></a>01081 
<a name="l01082"></a>01082   <span class="comment">/****************</span>
<a name="l01083"></a>01083 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01084"></a>01084 <span class="comment">   ****************/</span>
<a name="l01085"></a>01085 
<a name="l01086"></a>01086 
<a name="l01088"></a>01088 
<a name="l01091"></a>01091   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01092"></a>01092   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_00_01_allocator_01_4.php#a77ff941e5bcf4841e7763800d327c470" title="Default constructor.">Matrix&lt;T, Prop, RowSymPacked, Allocator&gt;::Matrix</a>():
<a name="l01093"></a>01093     Matrix_SymPacked&lt;T, Prop, RowSymPacked, Allocator&gt;()
<a name="l01094"></a>01094   {
<a name="l01095"></a>01095   }
<a name="l01096"></a>01096 
<a name="l01097"></a>01097 
<a name="l01099"></a>01099 
<a name="l01104"></a>01104   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01105"></a>01105   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_00_01_allocator_01_4.php#a77ff941e5bcf4841e7763800d327c470" title="Default constructor.">Matrix&lt;T, Prop, RowSymPacked, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01106"></a>01106     Matrix_SymPacked&lt;T, Prop, RowSymPacked, Allocator&gt;(i, i)
<a name="l01107"></a>01107   {
<a name="l01108"></a>01108   }
<a name="l01109"></a>01109 
<a name="l01110"></a>01110 
<a name="l01111"></a>01111   <span class="comment">/*****************</span>
<a name="l01112"></a>01112 <span class="comment">   * OTHER METHODS *</span>
<a name="l01113"></a>01113 <span class="comment">   *****************/</span>
<a name="l01114"></a>01114 
<a name="l01115"></a>01115 
<a name="l01117"></a>01117 
<a name="l01120"></a>01120   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01121"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_00_01_allocator_01_4.php#a847d2334cad1a45a4d2555eb5f15e95c">01121</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01122"></a>01122   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_00_01_allocator_01_4.php" title="Row-major symmetric packed matrix class.">Matrix&lt;T, Prop, RowSymPacked, Allocator&gt;</a>&amp;
<a name="l01123"></a>01123   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSymPacked, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01124"></a>01124   {
<a name="l01125"></a>01125     this-&gt;Fill(x);
<a name="l01126"></a>01126     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01127"></a>01127   }
<a name="l01128"></a>01128 
<a name="l01129"></a>01129 
<a name="l01131"></a>01131 
<a name="l01136"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_00_01_allocator_01_4.php#ae79c98b9562e2df6da01cde16409f337">01136</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01137"></a>01137   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_00_01_allocator_01_4.php" title="Row-major symmetric packed matrix class.">Matrix&lt;T, Prop, RowSymPacked, Allocator&gt;</a>&amp;
<a name="l01138"></a>01138   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSymPacked, Allocator&gt;::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop,
<a name="l01139"></a>01139                                                        <a class="code" href="class_seldon_1_1_row_sym_packed.php">RowSymPacked</a>,
<a name="l01140"></a>01140                                                        Allocator&gt;&amp; A)
<a name="l01141"></a>01141   {
<a name="l01142"></a>01142     this-&gt;Copy(A);
<a name="l01143"></a>01143     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01144"></a>01144   }
<a name="l01145"></a>01145 
<a name="l01146"></a>01146 
<a name="l01148"></a>01148 
<a name="l01151"></a>01151   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01152"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_00_01_allocator_01_4.php#a59af6d333e0929fb0d556d42c7d8b791">01152</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01153"></a>01153   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_00_01_allocator_01_4.php" title="Row-major symmetric packed matrix class.">Matrix&lt;T, Prop, RowSymPacked, Allocator&gt;</a>&amp;
<a name="l01154"></a>01154   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSymPacked, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01155"></a>01155   {
<a name="l01156"></a>01156     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetDataSize();i++)
<a name="l01157"></a>01157       this-&gt;data_[i] *= x;
<a name="l01158"></a>01158 
<a name="l01159"></a>01159     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01160"></a>01160   }
<a name="l01161"></a>01161 
<a name="l01162"></a>01162 
<a name="l01164"></a>01164 
<a name="l01171"></a>01171   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01172"></a>01172   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSymPacked, Allocator&gt;::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01173"></a>01173   {
<a name="l01174"></a>01174 
<a name="l01175"></a>01175     <span class="comment">// Storing the old values of the matrix.</span>
<a name="l01176"></a>01176     <span class="keywordtype">int</span> nold = this-&gt;GetDataSize(), iold = this-&gt;m_;
<a name="l01177"></a>01177     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> xold(nold);
<a name="l01178"></a>01178     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; nold; k++)
<a name="l01179"></a>01179       xold(k) = this-&gt;data_[k];
<a name="l01180"></a>01180 
<a name="l01181"></a>01181     <span class="comment">// Reallocation.</span>
<a name="l01182"></a>01182     this-&gt;Reallocate(i,j);
<a name="l01183"></a>01183 
<a name="l01184"></a>01184     <span class="comment">// Filling the matrix with its old values.</span>
<a name="l01185"></a>01185     <span class="keywordtype">int</span> imin = min(iold, i);
<a name="l01186"></a>01186     nold = 0;
<a name="l01187"></a>01187     <span class="keywordtype">int</span> n = 0;
<a name="l01188"></a>01188     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; imin; k++)
<a name="l01189"></a>01189       {
<a name="l01190"></a>01190         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> l = k; l &lt; imin; l++)
<a name="l01191"></a>01191           this-&gt;data_[n+l-k] = xold(nold+l-k);
<a name="l01192"></a>01192 
<a name="l01193"></a>01193         n += i - k;
<a name="l01194"></a>01194         nold += iold - k;
<a name="l01195"></a>01195       }
<a name="l01196"></a>01196   }
<a name="l01197"></a>01197 
<a name="l01198"></a>01198 
<a name="l01199"></a>01199 } <span class="comment">// namespace Seldon.</span>
<a name="l01200"></a>01200 
<a name="l01201"></a>01201 <span class="preprocessor">#define SELDON_FILE_MATRIX_SYMPACKED_CXX</span>
<a name="l01202"></a>01202 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
