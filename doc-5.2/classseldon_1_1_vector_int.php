<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><b>seldon</b>      </li>
      <li><a class="el" href="classseldon_1_1_vector_int.php">VectorInt</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pub-attribs">Public Attributes</a>  </div>
  <div class="headertitle">
<h1>seldon::VectorInt Class Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="seldon::VectorInt" --><!-- doxytag: inherits="seldon::BaseSeldonVectorInt" --><div class="dynheader">
Inheritance diagram for seldon::VectorInt:</div>
<div class="dyncontent">
 <div class="center">
  <img src="classseldon_1_1_vector_int.png" usemap="#seldon::VectorInt_map" alt=""/>
  <map id="seldon::VectorInt_map" name="seldon::VectorInt_map">
<area href="classseldon_1_1_base_seldon_vector_int.php" alt="seldon::BaseSeldonVectorInt" shape="rect" coords="0,56,173,80"/>
<area href="classseldon_1_1__object.php" alt="seldon::_object" shape="rect" coords="0,0,173,24"/>
</map>
</div>

<p><a href="classseldon_1_1_vector_int-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0f21025425fb434f5bae8724f63caf6c"></a><!-- doxytag: member="seldon::VectorInt::__init__" ref="a0f21025425fb434f5bae8724f63caf6c" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__init__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="abbf9639520542686d4fb89f00daf1f06"></a><!-- doxytag: member="seldon::VectorInt::Clear" ref="abbf9639520542686d4fb89f00daf1f06" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Clear</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad6b261d4facbd1b92100c1c0a343ebfe"></a><!-- doxytag: member="seldon::VectorInt::Reallocate" ref="ad6b261d4facbd1b92100c1c0a343ebfe" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Reallocate</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a21f03e32e4e50c1b0e118e0ee58c2a0a"></a><!-- doxytag: member="seldon::VectorInt::Resize" ref="a21f03e32e4e50c1b0e118e0ee58c2a0a" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Resize</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a60bb9d3d3c659b28e1b42cf86130cb25"></a><!-- doxytag: member="seldon::VectorInt::SetData" ref="a60bb9d3d3c659b28e1b42cf86130cb25" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetData</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a727077d4dfc871916aae37873abce7fc"></a><!-- doxytag: member="seldon::VectorInt::Nullify" ref="a727077d4dfc871916aae37873abce7fc" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Nullify</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac13439eb52c7a5aeca462f474da832a5"></a><!-- doxytag: member="seldon::VectorInt::__call__" ref="ac13439eb52c7a5aeca462f474da832a5" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__call__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="add44feff59ef44464093605400193241"></a><!-- doxytag: member="seldon::VectorInt::Get" ref="add44feff59ef44464093605400193241" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Get</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3e11c1299c0172a16496c4cfb21f0eb8"></a><!-- doxytag: member="seldon::VectorInt::Copy" ref="a3e11c1299c0172a16496c4cfb21f0eb8" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Copy</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a19b010b0867cd09c3e9f682a8138abfb"></a><!-- doxytag: member="seldon::VectorInt::Append" ref="a19b010b0867cd09c3e9f682a8138abfb" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Append</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a130c189b260650069f2d98f3c7e80b51"></a><!-- doxytag: member="seldon::VectorInt::GetDataSize" ref="a130c189b260650069f2d98f3c7e80b51" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataSize</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab18e2276f780ac592b8aeba2a0968a4c"></a><!-- doxytag: member="seldon::VectorInt::Zero" ref="ab18e2276f780ac592b8aeba2a0968a4c" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Zero</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9634753fa208839e93db3e4b0b50a3ad"></a><!-- doxytag: member="seldon::VectorInt::Fill" ref="a9634753fa208839e93db3e4b0b50a3ad" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Fill</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6ab0f77b013cb9b0cd16647e7fce57f0"></a><!-- doxytag: member="seldon::VectorInt::FillRand" ref="a6ab0f77b013cb9b0cd16647e7fce57f0" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>FillRand</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa9be807c9335a19977b5f3185ea6d274"></a><!-- doxytag: member="seldon::VectorInt::Print" ref="aa9be807c9335a19977b5f3185ea6d274" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Print</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0e47abf793a58b903d88191211854d68"></a><!-- doxytag: member="seldon::VectorInt::GetNormInf" ref="a0e47abf793a58b903d88191211854d68" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetNormInf</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aca636d3a0a3b41d2a0fae6fc6acfe494"></a><!-- doxytag: member="seldon::VectorInt::GetNormInfIndex" ref="aca636d3a0a3b41d2a0fae6fc6acfe494" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetNormInfIndex</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a109d9ad5b15a13870dd4fd26b17cb850"></a><!-- doxytag: member="seldon::VectorInt::Write" ref="a109d9ad5b15a13870dd4fd26b17cb850" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Write</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a86cd4a2e92e8eb1150ee15a710bfa005"></a><!-- doxytag: member="seldon::VectorInt::WriteText" ref="a86cd4a2e92e8eb1150ee15a710bfa005" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>WriteText</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3f001e4ea9e03b2150e0ad3cbf80afcf"></a><!-- doxytag: member="seldon::VectorInt::Read" ref="a3f001e4ea9e03b2150e0ad3cbf80afcf" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Read</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab6f23f813c8110b57c3a4df46350e3cb"></a><!-- doxytag: member="seldon::VectorInt::ReadText" ref="ab6f23f813c8110b57c3a4df46350e3cb" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>ReadText</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a49a5c553bbda73a3f70dd2ebb5ca748c"></a><!-- doxytag: member="seldon::VectorInt::__getitem__" ref="a49a5c553bbda73a3f70dd2ebb5ca748c" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__getitem__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a96480cf474d9fe34f9177c567f2c426b"></a><!-- doxytag: member="seldon::VectorInt::__setitem__" ref="a96480cf474d9fe34f9177c567f2c426b" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__setitem__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a81412c5d6c4bcac49cba2c3e2886c692"></a><!-- doxytag: member="seldon::VectorInt::__len__" ref="a81412c5d6c4bcac49cba2c3e2886c692" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__len__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adf44f8fccf99ad440c63573380ef09d4"></a><!-- doxytag: member="seldon::VectorInt::GetM" ref="adf44f8fccf99ad440c63573380ef09d4" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetM</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad9c3892a5aa25660bf88de52effb1c9f"></a><!-- doxytag: member="seldon::VectorInt::GetLength" ref="ad9c3892a5aa25660bf88de52effb1c9f" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetLength</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac132c3f1cee0899c85bceb1e3720ca24"></a><!-- doxytag: member="seldon::VectorInt::GetSize" ref="ac132c3f1cee0899c85bceb1e3720ca24" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetSize</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a811211f682b04263f1dea292e1572215"></a><!-- doxytag: member="seldon::VectorInt::GetData" ref="a811211f682b04263f1dea292e1572215" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetData</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aaedc3b489d234825d515d3ddb2bc37eb"></a><!-- doxytag: member="seldon::VectorInt::GetDataConst" ref="aaedc3b489d234825d515d3ddb2bc37eb" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataConst</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a09e369b5382e4823fbf98a03fd9eed62"></a><!-- doxytag: member="seldon::VectorInt::GetDataVoid" ref="a09e369b5382e4823fbf98a03fd9eed62" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataVoid</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1db8044c604f15f0471b9901495ecf2d"></a><!-- doxytag: member="seldon::VectorInt::GetDataConstVoid" ref="a1db8044c604f15f0471b9901495ecf2d" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataConstVoid</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-attribs"></a>
Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5dba37ce2201c0caa5181cb09981212d"></a><!-- doxytag: member="seldon::VectorInt::this" ref="a5dba37ce2201c0caa5181cb09981212d" args="" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><b>this</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>

<p>Definition at line <a class="el" href="seldon_8py_source.php#l00827">827</a> of file <a class="el" href="seldon_8py_source.php">seldon.py</a>.</p>
<hr/>The documentation for this class was generated from the following file:<ul>
<li><a class="el" href="seldon_8py_source.php">seldon.py</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
