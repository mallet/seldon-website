<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>share/Errors.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_ERRORS_CXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 <span class="preprocessor">#include &quot;Errors.hxx&quot;</span>
<a name="l00023"></a>00023 
<a name="l00024"></a>00024 <span class="keyword">namespace </span>Seldon
<a name="l00025"></a>00025 {
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 
<a name="l00029"></a>00029   <span class="comment">// ERROR //</span>
<a name="l00031"></a>00031 <span class="comment"></span>
<a name="l00032"></a>00032 
<a name="l00033"></a>00033   <span class="comment">/****************</span>
<a name="l00034"></a>00034 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00035"></a>00035 <span class="comment">   ****************/</span>
<a name="l00036"></a>00036 
<a name="l00037"></a>00037 
<a name="l00039"></a>00039 
<a name="l00043"></a><a class="code" href="class_seldon_1_1_error.php#a0d623beb431c2fde7d00e6afad009d18">00043</a>   <a class="code" href="class_seldon_1_1_error.php#a0d623beb431c2fde7d00e6afad009d18" title="Main constructor.">Error::Error</a>(<span class="keywordtype">string</span> function, <span class="keywordtype">string</span> comment):
<a name="l00044"></a>00044     description_(<span class="stringliteral">&quot;ERROR!\nAn undefined error occurred&quot;</span>),
<a name="l00045"></a>00045     function_(function), comment_(comment)
<a name="l00046"></a>00046   {
<a name="l00047"></a>00047 <span class="preprocessor">#ifdef SELDON_WITH_ABORT</span>
<a name="l00048"></a>00048 <span class="preprocessor"></span>    this-&gt;<a class="code" href="class_seldon_1_1_error.php#a19ea259b22ab7970a19beae646267ba3" title="Delivers information about the error.">CoutWhat</a>();
<a name="l00049"></a>00049     abort();
<a name="l00050"></a>00050 <span class="preprocessor">#endif</span>
<a name="l00051"></a>00051 <span class="preprocessor"></span>  }
<a name="l00052"></a>00052 
<a name="l00053"></a>00053 
<a name="l00055"></a>00055 
<a name="l00060"></a><a class="code" href="class_seldon_1_1_error.php#a5eb11d5adc4a05a711d8190bda262073">00060</a>   <a class="code" href="class_seldon_1_1_error.php#a0d623beb431c2fde7d00e6afad009d18" title="Main constructor.">Error::Error</a>(<span class="keywordtype">string</span> description, <span class="keywordtype">string</span> function, <span class="keywordtype">string</span> comment):
<a name="l00061"></a>00061     description_(<span class="stringliteral">&quot;ERROR!\n&quot;</span> + description),
<a name="l00062"></a>00062     function_(function), comment_(comment)
<a name="l00063"></a>00063   {
<a name="l00064"></a>00064   }
<a name="l00065"></a>00065 
<a name="l00066"></a>00066 
<a name="l00067"></a>00067   <span class="comment">/**************</span>
<a name="l00068"></a>00068 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00069"></a>00069 <span class="comment">   **************/</span>
<a name="l00070"></a>00070 
<a name="l00071"></a>00071 
<a name="l00073"></a>00073 
<a name="l00076"></a><a class="code" href="class_seldon_1_1_error.php#a82007af97b6bc0fd14de04f165e07497">00076</a>   <a class="code" href="class_seldon_1_1_error.php#a82007af97b6bc0fd14de04f165e07497" title="Destructor.">Error::~Error</a>()
<a name="l00077"></a>00077   {
<a name="l00078"></a>00078   }
<a name="l00079"></a>00079 
<a name="l00080"></a>00080 
<a name="l00081"></a>00081   <span class="comment">/***********</span>
<a name="l00082"></a>00082 <span class="comment">   * METHODS *</span>
<a name="l00083"></a>00083 <span class="comment">   ***********/</span>
<a name="l00084"></a>00084 
<a name="l00085"></a>00085 
<a name="l00087"></a>00087 
<a name="l00090"></a><a class="code" href="class_seldon_1_1_error.php#a6bc7b1e7b21242994e4a7b0cf9f49ade">00090</a>   <span class="keywordtype">string</span> <a class="code" href="class_seldon_1_1_error.php#a6bc7b1e7b21242994e4a7b0cf9f49ade" title="Delivers information about the error.">Error::What</a>()
<a name="l00091"></a>00091   {
<a name="l00092"></a>00092     <span class="keywordtype">string</span> message(<a class="code" href="class_seldon_1_1_error.php#a5442a8a1dffc72f0e04dd9f746e1a3f0" title="Message describing the exception type.">description_</a>);
<a name="l00093"></a>00093     <span class="keywordflow">if</span> (!<a class="code" href="class_seldon_1_1_error.php#a45bc7e61221de461fb4b92964b7b9058" title="Name of the function in which the error occurred.">function_</a>.empty())
<a name="l00094"></a>00094       message += <span class="stringliteral">&quot; in &quot;</span> + <a class="code" href="class_seldon_1_1_error.php#a45bc7e61221de461fb4b92964b7b9058" title="Name of the function in which the error occurred.">function_</a>;
<a name="l00095"></a>00095     message += <span class="stringliteral">&quot;.\n&quot;</span>;
<a name="l00096"></a>00096     <span class="keywordflow">if</span> (!<a class="code" href="class_seldon_1_1_error.php#a00e98cbd016cc12b92ee48e59a492275" title="A comment about the error.">comment_</a>.empty())
<a name="l00097"></a>00097       message += <span class="stringliteral">&quot;   &quot;</span> + <a class="code" href="class_seldon_1_1_error.php#a00e98cbd016cc12b92ee48e59a492275" title="A comment about the error.">comment_</a>;
<a name="l00098"></a>00098     <span class="keywordflow">return</span> message;
<a name="l00099"></a>00099   }
<a name="l00100"></a>00100 
<a name="l00101"></a>00101 
<a name="l00103"></a>00103 
<a name="l00106"></a><a class="code" href="class_seldon_1_1_error.php#a19ea259b22ab7970a19beae646267ba3">00106</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_error.php#a19ea259b22ab7970a19beae646267ba3" title="Delivers information about the error.">Error::CoutWhat</a>()
<a name="l00107"></a>00107   {
<a name="l00108"></a>00108     cout &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_error.php#a6bc7b1e7b21242994e4a7b0cf9f49ade" title="Delivers information about the error.">What</a>() &lt;&lt; endl;
<a name="l00109"></a>00109   }
<a name="l00110"></a>00110 
<a name="l00111"></a>00111 
<a name="l00112"></a>00112 
<a name="l00114"></a>00114   <span class="comment">// UNDEFINED //</span>
<a name="l00116"></a>00116 <span class="comment"></span>
<a name="l00117"></a>00117 
<a name="l00119"></a>00119 
<a name="l00123"></a><a class="code" href="class_seldon_1_1_undefined.php#a6c5596fed1db27afb82930f1c9581306">00123</a>   <a class="code" href="class_seldon_1_1_undefined.php#a6c5596fed1db27afb82930f1c9581306" title="Main constructor.">Undefined::Undefined</a>(<span class="keywordtype">string</span> function, <span class="keywordtype">string</span> comment):
<a name="l00124"></a>00124   <a class="code" href="class_seldon_1_1_error.php">Error</a>(<span class="stringliteral">&quot;&quot;</span>, function, comment)
<a name="l00125"></a>00125   {
<a name="l00126"></a>00126 <span class="preprocessor">#ifdef SELDON_WITH_ABORT</span>
<a name="l00127"></a>00127 <span class="preprocessor"></span>    this-&gt;<a class="code" href="class_seldon_1_1_error.php#a19ea259b22ab7970a19beae646267ba3" title="Delivers information about the error.">CoutWhat</a>();
<a name="l00128"></a>00128     abort();
<a name="l00129"></a>00129 <span class="preprocessor">#endif</span>
<a name="l00130"></a>00130 <span class="preprocessor"></span>  }
<a name="l00131"></a>00131 
<a name="l00132"></a>00132 
<a name="l00134"></a>00134 
<a name="l00137"></a><a class="code" href="class_seldon_1_1_undefined.php#af999e2526746396d8336d2a89e3668cf">00137</a>   <span class="keywordtype">string</span> <a class="code" href="class_seldon_1_1_undefined.php#af999e2526746396d8336d2a89e3668cf" title="Delivers information about the error.">Undefined::What</a>()
<a name="l00138"></a>00138   {
<a name="l00139"></a>00139     <span class="keywordtype">string</span> message;
<a name="l00140"></a>00140     <span class="keywordflow">if</span> (!this-&gt;<a class="code" href="class_seldon_1_1_error.php#a45bc7e61221de461fb4b92964b7b9058" title="Name of the function in which the error occurred.">function_</a>.empty())
<a name="l00141"></a>00141       message = this-&gt;<a class="code" href="class_seldon_1_1_error.php#a45bc7e61221de461fb4b92964b7b9058" title="Name of the function in which the error occurred.">function_</a>;
<a name="l00142"></a>00142     <span class="keywordflow">else</span>
<a name="l00143"></a>00143       message = <span class="stringliteral">&quot;A function or a method&quot;</span>;
<a name="l00144"></a>00144     message += <span class="stringliteral">&quot; is undefined.\nEither its implementation is missing,&quot;</span>
<a name="l00145"></a>00145       + string(<span class="stringliteral">&quot; or it does not make sense or it is impossible &quot;</span>)
<a name="l00146"></a>00146       + <span class="stringliteral">&quot;to implement it.\n&quot;</span>;
<a name="l00147"></a>00147     <span class="keywordflow">if</span> (!this-&gt;<a class="code" href="class_seldon_1_1_error.php#a00e98cbd016cc12b92ee48e59a492275" title="A comment about the error.">comment_</a>.empty())
<a name="l00148"></a>00148       message += <span class="stringliteral">&quot;   &quot;</span> + this-&gt;<a class="code" href="class_seldon_1_1_error.php#a00e98cbd016cc12b92ee48e59a492275" title="A comment about the error.">comment_</a>;
<a name="l00149"></a>00149     <span class="keywordflow">return</span> message;
<a name="l00150"></a>00150   }
<a name="l00151"></a>00151 
<a name="l00152"></a>00152 
<a name="l00154"></a>00154   <span class="comment">// WRONGARGUMENT //</span>
<a name="l00156"></a>00156 <span class="comment"></span>
<a name="l00157"></a>00157 
<a name="l00159"></a>00159 
<a name="l00163"></a><a class="code" href="class_seldon_1_1_wrong_argument.php#a31d2277bdae395083fa6f2b7cde9a175">00163</a>   <a class="code" href="class_seldon_1_1_wrong_argument.php#a31d2277bdae395083fa6f2b7cde9a175" title="Main constructor.">WrongArgument::WrongArgument</a>(<span class="keywordtype">string</span> function, <span class="keywordtype">string</span> comment):
<a name="l00164"></a>00164   <a class="code" href="class_seldon_1_1_error.php">Error</a>(<span class="stringliteral">&quot;Wrong argument given to &quot;</span>, function, comment)
<a name="l00165"></a>00165   {
<a name="l00166"></a>00166 <span class="preprocessor">#ifdef SELDON_WITH_ABORT</span>
<a name="l00167"></a>00167 <span class="preprocessor"></span>    this-&gt;<a class="code" href="class_seldon_1_1_error.php#a19ea259b22ab7970a19beae646267ba3" title="Delivers information about the error.">CoutWhat</a>();
<a name="l00168"></a>00168     abort();
<a name="l00169"></a>00169 <span class="preprocessor">#endif</span>
<a name="l00170"></a>00170 <span class="preprocessor"></span>  }
<a name="l00171"></a>00171 
<a name="l00172"></a>00172 
<a name="l00174"></a>00174 
<a name="l00177"></a><a class="code" href="class_seldon_1_1_wrong_argument.php#a93330801ab53bb5cf0a30373351e2c1d">00177</a>   <span class="keywordtype">string</span> <a class="code" href="class_seldon_1_1_wrong_argument.php#a93330801ab53bb5cf0a30373351e2c1d" title="Delivers information about the error.">WrongArgument::What</a>()
<a name="l00178"></a>00178   {
<a name="l00179"></a>00179     <span class="keywordtype">string</span> message(this-&gt;<a class="code" href="class_seldon_1_1_error.php#a5442a8a1dffc72f0e04dd9f746e1a3f0" title="Message describing the exception type.">description_</a>);
<a name="l00180"></a>00180     <span class="keywordflow">if</span> (!this-&gt;<a class="code" href="class_seldon_1_1_error.php#a45bc7e61221de461fb4b92964b7b9058" title="Name of the function in which the error occurred.">function_</a>.empty())
<a name="l00181"></a>00181       message += this-&gt;<a class="code" href="class_seldon_1_1_error.php#a45bc7e61221de461fb4b92964b7b9058" title="Name of the function in which the error occurred.">function_</a>;
<a name="l00182"></a>00182     message += <span class="stringliteral">&quot;.\n&quot;</span>;
<a name="l00183"></a>00183     <span class="keywordflow">if</span> (!this-&gt;<a class="code" href="class_seldon_1_1_error.php#a00e98cbd016cc12b92ee48e59a492275" title="A comment about the error.">comment_</a>.empty())
<a name="l00184"></a>00184       message += <span class="stringliteral">&quot;   &quot;</span> + this-&gt;<a class="code" href="class_seldon_1_1_error.php#a00e98cbd016cc12b92ee48e59a492275" title="A comment about the error.">comment_</a>;
<a name="l00185"></a>00185     <span class="keywordflow">return</span> message;
<a name="l00186"></a>00186   }
<a name="l00187"></a>00187 
<a name="l00188"></a>00188 
<a name="l00190"></a>00190   <span class="comment">// NOMEMORY //</span>
<a name="l00192"></a>00192 <span class="comment"></span>
<a name="l00193"></a>00193 
<a name="l00195"></a>00195 
<a name="l00199"></a><a class="code" href="class_seldon_1_1_no_memory.php#a3eef35951fabdffb7bab6ad483c09567">00199</a>   <a class="code" href="class_seldon_1_1_no_memory.php#a3eef35951fabdffb7bab6ad483c09567" title="Main constructor.">NoMemory::NoMemory</a>(<span class="keywordtype">string</span> function, <span class="keywordtype">string</span> comment):
<a name="l00200"></a>00200     <a class="code" href="class_seldon_1_1_error.php">Error</a>(<span class="stringliteral">&quot;Out of memory&quot;</span>, function, comment)
<a name="l00201"></a>00201   {
<a name="l00202"></a>00202 <span class="preprocessor">#ifdef SELDON_WITH_ABORT</span>
<a name="l00203"></a>00203 <span class="preprocessor"></span>    this-&gt;<a class="code" href="class_seldon_1_1_error.php#a19ea259b22ab7970a19beae646267ba3" title="Delivers information about the error.">CoutWhat</a>();
<a name="l00204"></a>00204     abort();
<a name="l00205"></a>00205 <span class="preprocessor">#endif</span>
<a name="l00206"></a>00206 <span class="preprocessor"></span>  }
<a name="l00207"></a>00207 
<a name="l00208"></a>00208 
<a name="l00209"></a>00209 
<a name="l00211"></a>00211   <span class="comment">// WRONGDIM //</span>
<a name="l00213"></a>00213 <span class="comment"></span>
<a name="l00214"></a>00214 
<a name="l00216"></a>00216 
<a name="l00220"></a><a class="code" href="class_seldon_1_1_wrong_dim.php#a9dac67659398f6d281bda91566fa03c5">00220</a>   <a class="code" href="class_seldon_1_1_wrong_dim.php#a9dac67659398f6d281bda91566fa03c5" title="Main constructor.">WrongDim::WrongDim</a>(<span class="keywordtype">string</span> function, <span class="keywordtype">string</span> comment):
<a name="l00221"></a>00221     <a class="code" href="class_seldon_1_1_error.php">Error</a>(<span class="stringliteral">&quot;Wrong dimensions involved&quot;</span>, function, comment)
<a name="l00222"></a>00222   {
<a name="l00223"></a>00223 <span class="preprocessor">#ifdef SELDON_WITH_ABORT</span>
<a name="l00224"></a>00224 <span class="preprocessor"></span>    this-&gt;<a class="code" href="class_seldon_1_1_error.php#a19ea259b22ab7970a19beae646267ba3" title="Delivers information about the error.">CoutWhat</a>();
<a name="l00225"></a>00225     abort();
<a name="l00226"></a>00226 <span class="preprocessor">#endif</span>
<a name="l00227"></a>00227 <span class="preprocessor"></span>  }
<a name="l00228"></a>00228 
<a name="l00229"></a>00229 
<a name="l00230"></a>00230 
<a name="l00232"></a>00232   <span class="comment">// WRONGINDEX //</span>
<a name="l00234"></a>00234 <span class="comment"></span>
<a name="l00235"></a>00235 
<a name="l00237"></a>00237 
<a name="l00241"></a><a class="code" href="class_seldon_1_1_wrong_index.php#a5c35bf51d3d5293c0dee815a3fdfcd89">00241</a>   <a class="code" href="class_seldon_1_1_wrong_index.php#a5c35bf51d3d5293c0dee815a3fdfcd89" title="Main constructor.">WrongIndex::WrongIndex</a>(<span class="keywordtype">string</span> function, <span class="keywordtype">string</span> comment):
<a name="l00242"></a>00242     <a class="code" href="class_seldon_1_1_error.php">Error</a>(<span class="stringliteral">&quot;Index out of range&quot;</span>, function, comment)
<a name="l00243"></a>00243   {
<a name="l00244"></a>00244 <span class="preprocessor">#ifdef SELDON_WITH_ABORT</span>
<a name="l00245"></a>00245 <span class="preprocessor"></span>    this-&gt;<a class="code" href="class_seldon_1_1_error.php#a19ea259b22ab7970a19beae646267ba3" title="Delivers information about the error.">CoutWhat</a>();
<a name="l00246"></a>00246     abort();
<a name="l00247"></a>00247 <span class="preprocessor">#endif</span>
<a name="l00248"></a>00248 <span class="preprocessor"></span>  }
<a name="l00249"></a>00249 
<a name="l00250"></a>00250 
<a name="l00251"></a>00251 
<a name="l00253"></a>00253   <span class="comment">// WRONGROW //</span>
<a name="l00255"></a>00255 <span class="comment"></span>
<a name="l00256"></a>00256 
<a name="l00258"></a>00258 
<a name="l00262"></a><a class="code" href="class_seldon_1_1_wrong_row.php#a2d9b5f9cfcfd976fc63b2f68bf6f1c69">00262</a>   <a class="code" href="class_seldon_1_1_wrong_row.php#a2d9b5f9cfcfd976fc63b2f68bf6f1c69" title="Main constructor.">WrongRow::WrongRow</a>(<span class="keywordtype">string</span> function, <span class="keywordtype">string</span> comment):
<a name="l00263"></a>00263     <a class="code" href="class_seldon_1_1_error.php">Error</a>(<span class="stringliteral">&quot;Row index out of range&quot;</span>, function, comment)
<a name="l00264"></a>00264   {
<a name="l00265"></a>00265 <span class="preprocessor">#ifdef SELDON_WITH_ABORT</span>
<a name="l00266"></a>00266 <span class="preprocessor"></span>    this-&gt;<a class="code" href="class_seldon_1_1_error.php#a19ea259b22ab7970a19beae646267ba3" title="Delivers information about the error.">CoutWhat</a>();
<a name="l00267"></a>00267     abort();
<a name="l00268"></a>00268 <span class="preprocessor">#endif</span>
<a name="l00269"></a>00269 <span class="preprocessor"></span>  }
<a name="l00270"></a>00270 
<a name="l00271"></a>00271 
<a name="l00272"></a>00272 
<a name="l00274"></a>00274   <span class="comment">// WRONGCOL //</span>
<a name="l00276"></a>00276 <span class="comment"></span>
<a name="l00277"></a>00277 
<a name="l00279"></a>00279 
<a name="l00283"></a><a class="code" href="class_seldon_1_1_wrong_col.php#a7fdcf78ed0cb369e7880c35fbd5ed42a">00283</a>   <a class="code" href="class_seldon_1_1_wrong_col.php#a7fdcf78ed0cb369e7880c35fbd5ed42a" title="Main constructor.">WrongCol::WrongCol</a>(<span class="keywordtype">string</span> function, <span class="keywordtype">string</span> comment):
<a name="l00284"></a>00284     <a class="code" href="class_seldon_1_1_error.php">Error</a>(<span class="stringliteral">&quot;Column index out of range&quot;</span>, function, comment)
<a name="l00285"></a>00285   {
<a name="l00286"></a>00286 <span class="preprocessor">#ifdef SELDON_WITH_ABORT</span>
<a name="l00287"></a>00287 <span class="preprocessor"></span>    this-&gt;<a class="code" href="class_seldon_1_1_error.php#a19ea259b22ab7970a19beae646267ba3" title="Delivers information about the error.">CoutWhat</a>();
<a name="l00288"></a>00288     abort();
<a name="l00289"></a>00289 <span class="preprocessor">#endif</span>
<a name="l00290"></a>00290 <span class="preprocessor"></span>  }
<a name="l00291"></a>00291 
<a name="l00292"></a>00292 
<a name="l00293"></a>00293 
<a name="l00295"></a>00295   <span class="comment">// IOERROR //</span>
<a name="l00297"></a>00297 <span class="comment"></span>
<a name="l00298"></a>00298 
<a name="l00300"></a>00300 
<a name="l00304"></a><a class="code" href="class_seldon_1_1_i_o_error.php#a404b6377bffa2188464bbe317fa2056e">00304</a>   <a class="code" href="class_seldon_1_1_i_o_error.php#a404b6377bffa2188464bbe317fa2056e" title="Main constructor.">IOError::IOError</a>(<span class="keywordtype">string</span> function, <span class="keywordtype">string</span> comment):
<a name="l00305"></a>00305     <a class="code" href="class_seldon_1_1_error.php">Error</a>(<span class="stringliteral">&quot;Error while performing an I/O operation&quot;</span>, function, comment)
<a name="l00306"></a>00306   {
<a name="l00307"></a>00307 <span class="preprocessor">#ifdef SELDON_WITH_ABORT</span>
<a name="l00308"></a>00308 <span class="preprocessor"></span>    this-&gt;<a class="code" href="class_seldon_1_1_error.php#a19ea259b22ab7970a19beae646267ba3" title="Delivers information about the error.">CoutWhat</a>();
<a name="l00309"></a>00309     abort();
<a name="l00310"></a>00310 <span class="preprocessor">#endif</span>
<a name="l00311"></a>00311 <span class="preprocessor"></span>  }
<a name="l00312"></a>00312 
<a name="l00313"></a>00313 
<a name="l00314"></a>00314 
<a name="l00316"></a>00316   <span class="comment">// LAPACKERROR //</span>
<a name="l00318"></a>00318 <span class="comment"></span>
<a name="l00319"></a>00319 
<a name="l00321"></a>00321 
<a name="l00326"></a><a class="code" href="class_seldon_1_1_lapack_error.php#acff0ca57524081c2a504e3ea4bd2d217">00326</a>   <a class="code" href="class_seldon_1_1_lapack_error.php#acff0ca57524081c2a504e3ea4bd2d217" title="Main constructor.">LapackError::LapackError</a>(<span class="keywordtype">int</span> info, <span class="keywordtype">string</span> function, <span class="keywordtype">string</span> comment):
<a name="l00327"></a>00327   <a class="code" href="class_seldon_1_1_error.php">Error</a>(<span class="stringliteral">&quot;Error returned by Lapack&quot;</span>, function, comment), info_(info)
<a name="l00328"></a>00328   {
<a name="l00329"></a>00329 <span class="preprocessor">#ifdef SELDON_WITH_ABORT</span>
<a name="l00330"></a>00330 <span class="preprocessor"></span>    this-&gt;<a class="code" href="class_seldon_1_1_error.php#a19ea259b22ab7970a19beae646267ba3" title="Delivers information about the error.">CoutWhat</a>();
<a name="l00331"></a>00331     abort();
<a name="l00332"></a>00332 <span class="preprocessor">#endif</span>
<a name="l00333"></a>00333 <span class="preprocessor"></span>  }
<a name="l00334"></a>00334 
<a name="l00335"></a>00335 
<a name="l00337"></a>00337 
<a name="l00340"></a><a class="code" href="class_seldon_1_1_lapack_error.php#a1b20521655f5ca3c44e86f4c7776505c">00340</a>   <span class="keywordtype">string</span> <a class="code" href="class_seldon_1_1_lapack_error.php#a1b20521655f5ca3c44e86f4c7776505c" title="Delivers information about the error.">LapackError::What</a>()
<a name="l00341"></a>00341   {
<a name="l00342"></a>00342     <span class="keywordtype">string</span> message(this-&gt;<a class="code" href="class_seldon_1_1_error.php#a5442a8a1dffc72f0e04dd9f746e1a3f0" title="Message describing the exception type.">description_</a>);
<a name="l00343"></a>00343     <span class="keywordflow">if</span> (!this-&gt;<a class="code" href="class_seldon_1_1_error.php#a45bc7e61221de461fb4b92964b7b9058" title="Name of the function in which the error occurred.">function_</a>.empty())
<a name="l00344"></a>00344       message += <span class="stringliteral">&quot; in &quot;</span> + this-&gt;<a class="code" href="class_seldon_1_1_error.php#a45bc7e61221de461fb4b92964b7b9058" title="Name of the function in which the error occurred.">function_</a>;
<a name="l00345"></a>00345     message += <span class="stringliteral">&quot;.\n&quot;</span>;
<a name="l00346"></a>00346     <span class="keywordflow">if</span> (!this-&gt;<a class="code" href="class_seldon_1_1_error.php#a00e98cbd016cc12b92ee48e59a492275" title="A comment about the error.">comment_</a>.empty())
<a name="l00347"></a>00347       message += <span class="stringliteral">&quot;   &quot;</span> + this-&gt;<a class="code" href="class_seldon_1_1_error.php#a00e98cbd016cc12b92ee48e59a492275" title="A comment about the error.">comment_</a>;
<a name="l00348"></a>00348     message += <span class="stringliteral">&quot;   Diagnostic integer (\&quot;info\&quot;): &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(info_)
<a name="l00349"></a>00349       + <span class="stringliteral">&quot;.\n&quot;</span>;
<a name="l00350"></a>00350     <span class="keywordflow">return</span> message;
<a name="l00351"></a>00351   }
<a name="l00352"></a>00352 
<a name="l00353"></a>00353 
<a name="l00355"></a>00355   <span class="comment">// LAPACKINFO //</span>
<a name="l00357"></a>00357 <span class="comment"></span>
<a name="l00358"></a>00358 
<a name="l00359"></a>00359   LapackInfo::LapackInfo(<span class="keywordtype">int</span> info): info_(info)
<a name="l00360"></a>00360   {
<a name="l00361"></a>00361   }
<a name="l00362"></a>00362 
<a name="l00363"></a>00363 
<a name="l00364"></a>00364   LapackInfo::operator int ()
<a name="l00365"></a>00365   {
<a name="l00366"></a>00366     <span class="keywordflow">return</span> info_;
<a name="l00367"></a>00367   }
<a name="l00368"></a>00368 
<a name="l00369"></a>00369 
<a name="l00370"></a>00370   <span class="keywordtype">int</span> LapackInfo::GetInfo()
<a name="l00371"></a>00371   {
<a name="l00372"></a>00372     <span class="keywordflow">return</span> info_;
<a name="l00373"></a>00373   }
<a name="l00374"></a>00374 
<a name="l00375"></a>00375 
<a name="l00376"></a>00376   <span class="keywordtype">int</span>&amp; LapackInfo::GetInfoRef()
<a name="l00377"></a>00377   {
<a name="l00378"></a>00378     <span class="keywordflow">return</span> info_;
<a name="l00379"></a>00379   }
<a name="l00380"></a>00380 
<a name="l00381"></a>00381 
<a name="l00382"></a>00382 <span class="preprocessor">#ifndef SELDON_WITH_COMPILED_LIBRARY</span>
<a name="l00383"></a>00383 <span class="preprocessor"></span>  LapackInfo lapack_info(0);
<a name="l00384"></a>00384 <span class="preprocessor">#endif</span>
<a name="l00385"></a>00385 <span class="preprocessor"></span>
<a name="l00386"></a>00386 
<a name="l00387"></a>00387 } <span class="comment">// namespace Seldon.</span>
<a name="l00388"></a>00388 
<a name="l00389"></a>00389 <span class="preprocessor">#define SELDON_FILE_ERRORS_CXX</span>
<a name="l00390"></a>00390 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
