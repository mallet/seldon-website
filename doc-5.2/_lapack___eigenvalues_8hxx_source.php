<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/interfaces/Lapack_Eigenvalues.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2012 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_LAPACK_EIGENVALUES_HXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 <span class="comment">/*</span>
<a name="l00023"></a>00023 <span class="comment">  Functions included in this file:</span>
<a name="l00024"></a>00024 <span class="comment"></span>
<a name="l00025"></a>00025 <span class="comment">  xGEEV   (GetEigenvalues, GetEigenvaluesEigenvectors)</span>
<a name="l00026"></a>00026 <span class="comment">  xSYEV   (GetEigenvalues, GetEigenvaluesEigenvectors)</span>
<a name="l00027"></a>00027 <span class="comment">  xHEEV   (GetEigenvalues, GetEigenvaluesEigenvectors)</span>
<a name="l00028"></a>00028 <span class="comment">  xSPEV   (GetEigenvalues, GetEigenvaluesEigenvectors)</span>
<a name="l00029"></a>00029 <span class="comment">  xHPEV   (GetEigenvalues, GetEigenvaluesEigenvectors)</span>
<a name="l00030"></a>00030 <span class="comment">  xSYGV   (GetEigenvalues, GetEigenvaluesEigenvectors)</span>
<a name="l00031"></a>00031 <span class="comment">  xGGEV   (GetEigenvalues, GetEigenvaluesEigenvectors)</span>
<a name="l00032"></a>00032 <span class="comment">  xHEGV   (GetEigenvalues, GetEigenvaluesEigenvectors)</span>
<a name="l00033"></a>00033 <span class="comment">  xSPGV   (GetEigenvalues, GetEigenvaluesEigenvectors)</span>
<a name="l00034"></a>00034 <span class="comment">  xHPGV   (GetEigenvalues, GetEigenvaluesEigenvectors)</span>
<a name="l00035"></a>00035 <span class="comment">  xGESVD  (GetSVD)</span>
<a name="l00036"></a>00036 <span class="comment">  xGEQRF  (GetHessenberg)</span>
<a name="l00037"></a>00037 <span class="comment">  ZGEQRF + ZUNGQR + ZUNMQR + ZGGHRD   (GetHessenberg)</span>
<a name="l00038"></a>00038 <span class="comment">  ZGEQRF + ZUNGQR + ZUNMQR + ZGGHRD + ZHGEQZ   (GetQZ)</span>
<a name="l00039"></a>00039 <span class="comment">  (SolveSylvester)</span>
<a name="l00040"></a>00040 <span class="comment">*/</span>
<a name="l00041"></a>00041 
<a name="l00042"></a>00042 <span class="keyword">namespace </span>Seldon
<a name="l00043"></a>00043 {
<a name="l00044"></a>00044 
<a name="l00045"></a>00045 
<a name="l00047"></a>00047   <span class="comment">// STANDARD EIGENVALUE PROBLEM //</span>
<a name="l00048"></a>00048 
<a name="l00049"></a>00049 
<a name="l00050"></a>00050   <span class="comment">/* RowMajor */</span>
<a name="l00051"></a>00051 
<a name="l00052"></a>00052 
<a name="l00053"></a>00053   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00054"></a>00054   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;float, Prop, RowMajor, Allocator1&gt;&amp; A,
<a name="l00055"></a>00055                       Vector&lt;float, VectFull, Allocator2&gt;&amp; wr,
<a name="l00056"></a>00056                       Vector&lt;float, VectFull, Allocator3&gt;&amp; wi,
<a name="l00057"></a>00057                       LapackInfo&amp; info = lapack_info);
<a name="l00058"></a>00058 
<a name="l00059"></a>00059 
<a name="l00060"></a>00060   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00061"></a>00061            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00062"></a>00062   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;float, Prop, RowMajor, Allocator1&gt;&amp; A,
<a name="l00063"></a>00063                                   Vector&lt;float, VectFull, Allocator2&gt;&amp; wr,
<a name="l00064"></a>00064                                   Vector&lt;float, VectFull, Allocator3&gt;&amp; wi,
<a name="l00065"></a>00065                                   Matrix&lt;float, General, RowMajor, Allocator4&gt;&amp; zr,
<a name="l00066"></a>00066                                   LapackInfo&amp; info = lapack_info);
<a name="l00067"></a>00067 
<a name="l00068"></a>00068 
<a name="l00069"></a>00069   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00070"></a>00070   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;float&gt;, Prop, RowMajor, Allocator1&gt;&amp; A,
<a name="l00071"></a>00071                       Vector&lt;complex&lt;float&gt;, VectFull, Allocator2&gt;&amp; w,
<a name="l00072"></a>00072                       LapackInfo&amp; info = lapack_info);
<a name="l00073"></a>00073 
<a name="l00074"></a>00074 
<a name="l00075"></a>00075   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00076"></a>00076   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;float&gt;,
<a name="l00077"></a>00077                                   Prop, RowMajor, Allocator1&gt;&amp; A,
<a name="l00078"></a>00078                                   Vector&lt;complex&lt;float&gt;, VectFull, Allocator2&gt;&amp; w,
<a name="l00079"></a>00079                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00080"></a>00080                                   General, RowMajor, Allocator3&gt;&amp; z,
<a name="l00081"></a>00081                                   LapackInfo&amp; info = lapack_info);
<a name="l00082"></a>00082 
<a name="l00083"></a>00083 
<a name="l00084"></a>00084   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00085"></a>00085   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;double, Prop, RowMajor, Allocator1&gt;&amp; A,
<a name="l00086"></a>00086                       Vector&lt;double, VectFull, Allocator2&gt;&amp; wr,
<a name="l00087"></a>00087                       Vector&lt;double, VectFull, Allocator3&gt;&amp; wi,
<a name="l00088"></a>00088                       LapackInfo&amp; info = lapack_info);
<a name="l00089"></a>00089 
<a name="l00090"></a>00090 
<a name="l00091"></a>00091   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00092"></a>00092            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00093"></a>00093   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;double, Prop, RowMajor, Allocator1&gt;&amp; A,
<a name="l00094"></a>00094                                   Vector&lt;double, VectFull, Allocator2&gt;&amp; wr,
<a name="l00095"></a>00095                                   Vector&lt;double, VectFull, Allocator3&gt;&amp; wi,
<a name="l00096"></a>00096                                   Matrix&lt;double, General, RowMajor, Allocator4&gt;&amp; zr,
<a name="l00097"></a>00097                                   LapackInfo&amp; info = lapack_info);
<a name="l00098"></a>00098 
<a name="l00099"></a>00099 
<a name="l00100"></a>00100   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00101"></a>00101   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;double&gt;, Prop, RowMajor, Allocator1&gt;&amp; A,
<a name="l00102"></a>00102                       Vector&lt;complex&lt;double&gt;, VectFull, Allocator2&gt;&amp; w,
<a name="l00103"></a>00103                       LapackInfo&amp; info = lapack_info);
<a name="l00104"></a>00104 
<a name="l00105"></a>00105 
<a name="l00106"></a>00106   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00107"></a>00107   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;double&gt;,
<a name="l00108"></a>00108                                   Prop, RowMajor, Allocator1&gt;&amp; A,
<a name="l00109"></a>00109                                   Vector&lt;complex&lt;double&gt;,
<a name="l00110"></a>00110                                   VectFull, Allocator2&gt;&amp; w,
<a name="l00111"></a>00111                                   Matrix&lt;complex&lt;double&gt;,
<a name="l00112"></a>00112                                   General, RowMajor, Allocator3&gt;&amp; z,
<a name="l00113"></a>00113                                   LapackInfo&amp; info = lapack_info);
<a name="l00114"></a>00114 
<a name="l00115"></a>00115 
<a name="l00116"></a>00116   <span class="comment">/* ColMajor */</span>
<a name="l00117"></a>00117 
<a name="l00118"></a>00118 
<a name="l00119"></a>00119   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00120"></a>00120   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;float, Prop, ColMajor, Allocator1&gt;&amp; A,
<a name="l00121"></a>00121                       Vector&lt;float, VectFull, Allocator2&gt;&amp; wr,
<a name="l00122"></a>00122                       Vector&lt;float, VectFull, Allocator3&gt;&amp; wi,
<a name="l00123"></a>00123                       LapackInfo&amp; info = lapack_info);
<a name="l00124"></a>00124 
<a name="l00125"></a>00125   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00126"></a>00126            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00127"></a>00127   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;float, Prop, ColMajor, Allocator1&gt;&amp; A,
<a name="l00128"></a>00128                                   Vector&lt;float, VectFull, Allocator2&gt;&amp; wr,
<a name="l00129"></a>00129                                   Vector&lt;float, VectFull, Allocator3&gt;&amp; wi,
<a name="l00130"></a>00130                                   Matrix&lt;float, General, ColMajor, Allocator4&gt;&amp;zr,
<a name="l00131"></a>00131                                   LapackInfo&amp; info = lapack_info);
<a name="l00132"></a>00132 
<a name="l00133"></a>00133 
<a name="l00134"></a>00134   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00135"></a>00135   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;float&gt;, Prop, ColMajor, Allocator1&gt;&amp; A,
<a name="l00136"></a>00136                       Vector&lt;complex&lt;float&gt;, VectFull, Allocator2&gt;&amp; w,
<a name="l00137"></a>00137                       LapackInfo&amp; info = lapack_info);
<a name="l00138"></a>00138 
<a name="l00139"></a>00139 
<a name="l00140"></a>00140   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00141"></a>00141   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;float&gt;,
<a name="l00142"></a>00142                                   Prop, ColMajor, Allocator1&gt;&amp; A,
<a name="l00143"></a>00143                                   Vector&lt;complex&lt;float&gt;,
<a name="l00144"></a>00144                                   VectFull, Allocator2&gt;&amp; w,
<a name="l00145"></a>00145                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00146"></a>00146                                   General, ColMajor, Allocator3&gt;&amp; z,
<a name="l00147"></a>00147                                   LapackInfo&amp; info = lapack_info);
<a name="l00148"></a>00148 
<a name="l00149"></a>00149   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00150"></a>00150   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;double, Prop, ColMajor, Allocator1&gt;&amp; A,
<a name="l00151"></a>00151                       Vector&lt;double, VectFull, Allocator2&gt;&amp; wr,
<a name="l00152"></a>00152                       Vector&lt;double, VectFull, Allocator3&gt;&amp; wi,
<a name="l00153"></a>00153                       LapackInfo&amp; info = lapack_info);
<a name="l00154"></a>00154 
<a name="l00155"></a>00155 
<a name="l00156"></a>00156   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00157"></a>00157            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00158"></a>00158   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;double, Prop, ColMajor, Allocator1&gt;&amp; A,
<a name="l00159"></a>00159                                   Vector&lt;double, VectFull, Allocator2&gt;&amp; wr,
<a name="l00160"></a>00160                                   Vector&lt;double, VectFull, Allocator3&gt;&amp; wi,
<a name="l00161"></a>00161                                   Matrix&lt;double, General, ColMajor, Allocator4&gt;&amp;zr,
<a name="l00162"></a>00162                                   LapackInfo&amp; info = lapack_info);
<a name="l00163"></a>00163 
<a name="l00164"></a>00164 
<a name="l00165"></a>00165   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00166"></a>00166   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;double&gt;, Prop, ColMajor, Allocator1&gt;&amp; A,
<a name="l00167"></a>00167                       Vector&lt;complex&lt;double&gt;, VectFull, Allocator2&gt;&amp; w,
<a name="l00168"></a>00168                       LapackInfo&amp; info = lapack_info);
<a name="l00169"></a>00169 
<a name="l00170"></a>00170 
<a name="l00171"></a>00171   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00172"></a>00172   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;double&gt;,
<a name="l00173"></a>00173                                   Prop, ColMajor, Allocator1&gt;&amp; A,
<a name="l00174"></a>00174                                   Vector&lt;complex&lt;double&gt;,
<a name="l00175"></a>00175                                   VectFull, Allocator2&gt;&amp; w,
<a name="l00176"></a>00176                                   Matrix&lt;complex&lt;double&gt;,
<a name="l00177"></a>00177                                   General, ColMajor, Allocator3&gt;&amp; z,
<a name="l00178"></a>00178                                   LapackInfo&amp; info = lapack_info);
<a name="l00179"></a>00179 
<a name="l00180"></a>00180 
<a name="l00181"></a>00181   <span class="comment">/* RowSym */</span>
<a name="l00182"></a>00182 
<a name="l00183"></a>00183 
<a name="l00184"></a>00184   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00185"></a>00185   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;float, Prop, RowSym, Allocator1&gt;&amp; A,
<a name="l00186"></a>00186                       Vector&lt;float, VectFull, Allocator2&gt;&amp; w,
<a name="l00187"></a>00187                       LapackInfo&amp; info = lapack_info);
<a name="l00188"></a>00188 
<a name="l00189"></a>00189 
<a name="l00190"></a>00190   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00191"></a>00191   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;float, Prop, RowSym, Allocator1&gt;&amp; A,
<a name="l00192"></a>00192                                   Vector&lt;float, VectFull, Allocator2&gt;&amp; w,
<a name="l00193"></a>00193                                   Matrix&lt;float, General, RowMajor, Allocator3&gt;&amp; z,
<a name="l00194"></a>00194                                   LapackInfo&amp; info = lapack_info);
<a name="l00195"></a>00195 
<a name="l00196"></a>00196   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00197"></a>00197   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;float&gt;, Prop, RowSym, Allocator1&gt;&amp; A,
<a name="l00198"></a>00198                       Vector&lt;complex&lt;float&gt;, VectFull, Allocator2&gt;&amp; w,
<a name="l00199"></a>00199                       LapackInfo&amp; info = lapack_info);
<a name="l00200"></a>00200 
<a name="l00201"></a>00201   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00202"></a>00202   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;float&gt;,
<a name="l00203"></a>00203                                   Prop, RowSym, Allocator1&gt;&amp; A,
<a name="l00204"></a>00204                                   Vector&lt;complex&lt;float&gt;,
<a name="l00205"></a>00205                                   VectFull, Allocator2&gt;&amp; w,
<a name="l00206"></a>00206                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00207"></a>00207                                   General, RowMajor, Allocator3&gt;&amp; z,
<a name="l00208"></a>00208                                   LapackInfo&amp; info = lapack_info);
<a name="l00209"></a>00209 
<a name="l00210"></a>00210 
<a name="l00211"></a>00211   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00212"></a>00212   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;double, Prop, RowSym, Allocator1&gt;&amp; A,
<a name="l00213"></a>00213                       Vector&lt;double, VectFull, Allocator2&gt;&amp; w,
<a name="l00214"></a>00214                       LapackInfo&amp; info = lapack_info);
<a name="l00215"></a>00215 
<a name="l00216"></a>00216 
<a name="l00217"></a>00217   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00218"></a>00218   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;double, Prop, RowSym, Allocator1&gt;&amp; A,
<a name="l00219"></a>00219                                   Vector&lt;double, VectFull, Allocator2&gt;&amp; w,
<a name="l00220"></a>00220                                   Matrix&lt;double, General, RowMajor, Allocator3&gt;&amp; z,
<a name="l00221"></a>00221                                   LapackInfo&amp; info = lapack_info);
<a name="l00222"></a>00222 
<a name="l00223"></a>00223   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00224"></a>00224   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;double&gt;, Prop, RowSym, Allocator1&gt;&amp; A,
<a name="l00225"></a>00225                       Vector&lt;complex&lt;double&gt;, VectFull, Allocator2&gt;&amp; w,
<a name="l00226"></a>00226                       LapackInfo&amp; info = lapack_info);
<a name="l00227"></a>00227 
<a name="l00228"></a>00228   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00229"></a>00229   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;double&gt;,
<a name="l00230"></a>00230                                   Prop, RowSym, Allocator1&gt;&amp; A,
<a name="l00231"></a>00231                                   Vector&lt;complex&lt;double&gt;,
<a name="l00232"></a>00232                                   VectFull, Allocator2&gt;&amp; w,
<a name="l00233"></a>00233                                   Matrix&lt;complex&lt;double&gt;,
<a name="l00234"></a>00234                                   General, RowMajor, Allocator3&gt;&amp; z,
<a name="l00235"></a>00235                                   LapackInfo&amp; info = lapack_info);
<a name="l00236"></a>00236 
<a name="l00237"></a>00237 
<a name="l00238"></a>00238   <span class="comment">/* ColSym */</span>
<a name="l00239"></a>00239 
<a name="l00240"></a>00240 
<a name="l00241"></a>00241   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00242"></a>00242   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;float, Prop, ColSym, Allocator1&gt;&amp; A,
<a name="l00243"></a>00243                       Vector&lt;float, VectFull, Allocator2&gt;&amp; w,
<a name="l00244"></a>00244                       LapackInfo&amp; info = lapack_info);
<a name="l00245"></a>00245 
<a name="l00246"></a>00246 
<a name="l00247"></a>00247   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00248"></a>00248   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;float, Prop, ColSym, Allocator1&gt;&amp; A,
<a name="l00249"></a>00249                                   Vector&lt;float, VectFull, Allocator2&gt;&amp; w,
<a name="l00250"></a>00250                                   Matrix&lt;float, General, ColMajor, Allocator3&gt;&amp; z,
<a name="l00251"></a>00251                                   LapackInfo&amp; info = lapack_info);
<a name="l00252"></a>00252 
<a name="l00253"></a>00253 
<a name="l00254"></a>00254   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00255"></a>00255   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;float&gt;, Prop, ColSym, Allocator1&gt;&amp; A,
<a name="l00256"></a>00256                       Vector&lt;complex&lt;float&gt;, VectFull, Allocator2&gt;&amp; w,
<a name="l00257"></a>00257                       LapackInfo&amp; info = lapack_info);
<a name="l00258"></a>00258 
<a name="l00259"></a>00259 
<a name="l00260"></a>00260   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00261"></a>00261   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;float&gt;,
<a name="l00262"></a>00262                                   Prop, ColSym, Allocator1&gt;&amp; A,
<a name="l00263"></a>00263                                   Vector&lt;complex&lt;float&gt;,
<a name="l00264"></a>00264                                   VectFull, Allocator2&gt;&amp; w,
<a name="l00265"></a>00265                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00266"></a>00266                                   General, ColMajor, Allocator3&gt;&amp; z,
<a name="l00267"></a>00267                                   LapackInfo&amp; info = lapack_info);
<a name="l00268"></a>00268 
<a name="l00269"></a>00269 
<a name="l00270"></a>00270   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00271"></a>00271   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;double, Prop, ColSym, Allocator1&gt;&amp; A,
<a name="l00272"></a>00272                       Vector&lt;double, VectFull, Allocator2&gt;&amp; w,
<a name="l00273"></a>00273                       LapackInfo&amp; info = lapack_info);
<a name="l00274"></a>00274 
<a name="l00275"></a>00275 
<a name="l00276"></a>00276   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00277"></a>00277   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;double, Prop, ColSym, Allocator1&gt;&amp; A,
<a name="l00278"></a>00278                                   Vector&lt;double, VectFull, Allocator2&gt;&amp; w,
<a name="l00279"></a>00279                                   Matrix&lt;double, General, ColMajor, Allocator3&gt;&amp; z,
<a name="l00280"></a>00280                                   LapackInfo&amp; info = lapack_info);
<a name="l00281"></a>00281 
<a name="l00282"></a>00282 
<a name="l00283"></a>00283   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00284"></a>00284   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;double&gt;, Prop, ColSym, Allocator1&gt;&amp; A,
<a name="l00285"></a>00285                       Vector&lt;complex&lt;double&gt;, VectFull, Allocator2&gt;&amp; w,
<a name="l00286"></a>00286                       LapackInfo&amp; info = lapack_info);
<a name="l00287"></a>00287 
<a name="l00288"></a>00288 
<a name="l00289"></a>00289   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00290"></a>00290   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;double&gt;,
<a name="l00291"></a>00291                                   Prop, ColSym, Allocator1&gt;&amp; A,
<a name="l00292"></a>00292                                   Vector&lt;complex&lt;double&gt;,
<a name="l00293"></a>00293                                   VectFull, Allocator2&gt;&amp; w,
<a name="l00294"></a>00294                                   Matrix&lt;complex&lt;double&gt;,
<a name="l00295"></a>00295                                   General, ColMajor, Allocator3&gt;&amp; z,
<a name="l00296"></a>00296                                   LapackInfo&amp; info = lapack_info);
<a name="l00297"></a>00297 
<a name="l00298"></a>00298 
<a name="l00299"></a>00299   <span class="comment">/* RowHerm */</span>
<a name="l00300"></a>00300 
<a name="l00301"></a>00301 
<a name="l00302"></a>00302   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00303"></a>00303   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;float&gt;, Prop, RowHerm, Allocator1&gt;&amp; A,
<a name="l00304"></a>00304                       Vector&lt;float, VectFull, Allocator2&gt;&amp; w,
<a name="l00305"></a>00305                       LapackInfo&amp; info = lapack_info);
<a name="l00306"></a>00306 
<a name="l00307"></a>00307 
<a name="l00308"></a>00308   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00309"></a>00309   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;float&gt;,
<a name="l00310"></a>00310                                   Prop, RowHerm, Allocator1&gt;&amp; A,
<a name="l00311"></a>00311                                   Vector&lt;float, VectFull, Allocator2&gt;&amp; w,
<a name="l00312"></a>00312                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00313"></a>00313                                   General, RowMajor, Allocator3&gt;&amp; z,
<a name="l00314"></a>00314                                   LapackInfo&amp; info = lapack_info);
<a name="l00315"></a>00315 
<a name="l00316"></a>00316   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00317"></a>00317   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;double&gt;, Prop, RowHerm, Allocator1&gt;&amp; A,
<a name="l00318"></a>00318                       Vector&lt;double, VectFull, Allocator2&gt;&amp; w,
<a name="l00319"></a>00319                       LapackInfo&amp; info = lapack_info);
<a name="l00320"></a>00320 
<a name="l00321"></a>00321 
<a name="l00322"></a>00322   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00323"></a>00323   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;double&gt;,
<a name="l00324"></a>00324                                   Prop, RowHerm, Allocator1&gt;&amp; A,
<a name="l00325"></a>00325                                   Vector&lt;double, VectFull, Allocator2&gt;&amp; w,
<a name="l00326"></a>00326                                   Matrix&lt;complex&lt;double&gt;,
<a name="l00327"></a>00327                                   General, RowMajor, Allocator3&gt;&amp; z,
<a name="l00328"></a>00328                                   LapackInfo&amp; info = lapack_info);
<a name="l00329"></a>00329 
<a name="l00330"></a>00330 
<a name="l00331"></a>00331   <span class="comment">/* ColHerm */</span>
<a name="l00332"></a>00332 
<a name="l00333"></a>00333 
<a name="l00334"></a>00334   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00335"></a>00335   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;float&gt;, Prop, ColHerm, Allocator1&gt;&amp; A,
<a name="l00336"></a>00336                       Vector&lt;float, VectFull, Allocator2&gt;&amp; w,
<a name="l00337"></a>00337                       LapackInfo&amp; info = lapack_info);
<a name="l00338"></a>00338 
<a name="l00339"></a>00339 
<a name="l00340"></a>00340   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00341"></a>00341   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;float&gt;,
<a name="l00342"></a>00342                                   Prop, ColHerm, Allocator1&gt;&amp; A,
<a name="l00343"></a>00343                                   Vector&lt;float, VectFull, Allocator2&gt;&amp; w,
<a name="l00344"></a>00344                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00345"></a>00345                                   General, ColMajor, Allocator3&gt;&amp; z,
<a name="l00346"></a>00346                                   LapackInfo&amp; info = lapack_info);
<a name="l00347"></a>00347 
<a name="l00348"></a>00348 
<a name="l00349"></a>00349   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00350"></a>00350   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;double&gt;, Prop, ColHerm, Allocator1&gt;&amp; A,
<a name="l00351"></a>00351                       Vector&lt;double, VectFull, Allocator2&gt;&amp; w,
<a name="l00352"></a>00352                       LapackInfo&amp; info = lapack_info);
<a name="l00353"></a>00353 
<a name="l00354"></a>00354 
<a name="l00355"></a>00355   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00356"></a>00356   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;double&gt;,
<a name="l00357"></a>00357                                   Prop, ColHerm, Allocator1&gt;&amp; A,
<a name="l00358"></a>00358                                   Vector&lt;double, VectFull, Allocator2&gt;&amp; w,
<a name="l00359"></a>00359                                   Matrix&lt;complex&lt;double&gt;,
<a name="l00360"></a>00360                                   General, ColMajor, Allocator3&gt;&amp; z,
<a name="l00361"></a>00361                                   LapackInfo&amp; info = lapack_info);
<a name="l00362"></a>00362 
<a name="l00363"></a>00363 
<a name="l00364"></a>00364   <span class="comment">/* RowSymPacked */</span>
<a name="l00365"></a>00365 
<a name="l00366"></a>00366 
<a name="l00367"></a>00367   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00368"></a>00368   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;float, Prop, RowSymPacked, Allocator1&gt;&amp; A,
<a name="l00369"></a>00369                       Vector&lt;float, VectFull, Allocator2&gt;&amp; w,
<a name="l00370"></a>00370                       LapackInfo&amp; info = lapack_info);
<a name="l00371"></a>00371 
<a name="l00372"></a>00372 
<a name="l00373"></a>00373   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00374"></a>00374   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;float, Prop,RowSymPacked, Allocator1&gt;&amp; A,
<a name="l00375"></a>00375                                   Vector&lt;float, VectFull, Allocator2&gt;&amp; w,
<a name="l00376"></a>00376                                   Matrix&lt;float, General, RowMajor, Allocator3&gt;&amp;z,
<a name="l00377"></a>00377                                   LapackInfo&amp; info = lapack_info);
<a name="l00378"></a>00378 
<a name="l00379"></a>00379 
<a name="l00380"></a>00380   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00381"></a>00381   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;float&gt;, Prop, RowSymPacked, Allocator1&gt;&amp; A,
<a name="l00382"></a>00382                       Vector&lt;complex&lt;float&gt;, VectFull, Allocator2&gt;&amp; w,
<a name="l00383"></a>00383                       LapackInfo&amp; info = lapack_info);
<a name="l00384"></a>00384 
<a name="l00385"></a>00385   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00386"></a>00386   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;float&gt;, Prop,
<a name="l00387"></a>00387                                   RowSymPacked, Allocator1&gt;&amp; A,
<a name="l00388"></a>00388                                   Vector&lt;complex&lt;float&gt;,VectFull, Allocator2&gt;&amp; w,
<a name="l00389"></a>00389                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00390"></a>00390                                   General, RowMajor, Allocator3&gt;&amp; z,
<a name="l00391"></a>00391                                   LapackInfo&amp; info = lapack_info);
<a name="l00392"></a>00392 
<a name="l00393"></a>00393 
<a name="l00394"></a>00394   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00395"></a>00395   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;double, Prop, RowSymPacked, Allocator1&gt;&amp; A,
<a name="l00396"></a>00396                       Vector&lt;double, VectFull, Allocator2&gt;&amp; w,
<a name="l00397"></a>00397                       LapackInfo&amp; info = lapack_info);
<a name="l00398"></a>00398 
<a name="l00399"></a>00399 
<a name="l00400"></a>00400   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00401"></a>00401   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;double,Prop,RowSymPacked, Allocator1&gt;&amp; A,
<a name="l00402"></a>00402                                   Vector&lt;double, VectFull, Allocator2&gt;&amp; w,
<a name="l00403"></a>00403                                   Matrix&lt;double, General, RowMajor, Allocator3&gt;&amp;z,
<a name="l00404"></a>00404                                   LapackInfo&amp; info = lapack_info);
<a name="l00405"></a>00405 
<a name="l00406"></a>00406 
<a name="l00407"></a>00407   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00408"></a>00408   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;double&gt;, Prop, RowSymPacked, Allocator1&gt;&amp; A,
<a name="l00409"></a>00409                       Vector&lt;complex&lt;double&gt;, VectFull, Allocator2&gt;&amp; w,
<a name="l00410"></a>00410                       LapackInfo&amp; info = lapack_info);
<a name="l00411"></a>00411 
<a name="l00412"></a>00412 
<a name="l00413"></a>00413   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00414"></a>00414   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;double&gt;, Prop,
<a name="l00415"></a>00415                                   RowSymPacked, Allocator1&gt;&amp; A,
<a name="l00416"></a>00416                                   Vector&lt;complex&lt;double&gt;,VectFull, Allocator2&gt;&amp; w,
<a name="l00417"></a>00417                                   Matrix&lt;complex&lt;double&gt;,
<a name="l00418"></a>00418                                   General, RowMajor, Allocator3&gt;&amp; z,
<a name="l00419"></a>00419                                   LapackInfo&amp; info = lapack_info);
<a name="l00420"></a>00420 
<a name="l00421"></a>00421 
<a name="l00422"></a>00422   <span class="comment">/* ColSymPacked */</span>
<a name="l00423"></a>00423 
<a name="l00424"></a>00424 
<a name="l00425"></a>00425   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00426"></a>00426   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;float, Prop, ColSymPacked, Allocator1&gt;&amp; A,
<a name="l00427"></a>00427                       Vector&lt;float, VectFull, Allocator2&gt;&amp; w,
<a name="l00428"></a>00428                       LapackInfo&amp; info = lapack_info);
<a name="l00429"></a>00429 
<a name="l00430"></a>00430 
<a name="l00431"></a>00431   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00432"></a>00432   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;float, Prop,ColSymPacked, Allocator1&gt;&amp; A,
<a name="l00433"></a>00433                                   Vector&lt;float, VectFull, Allocator2&gt;&amp; w,
<a name="l00434"></a>00434                                   Matrix&lt;float, General, ColMajor, Allocator3&gt;&amp;z,
<a name="l00435"></a>00435                                   LapackInfo&amp; info = lapack_info);
<a name="l00436"></a>00436 
<a name="l00437"></a>00437 
<a name="l00438"></a>00438   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00439"></a>00439   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;float&gt;,
<a name="l00440"></a>00440                       Prop, ColSymPacked, Allocator1&gt;&amp; A,
<a name="l00441"></a>00441                       Vector&lt;complex&lt;float&gt;, VectFull, Allocator2&gt;&amp; w,
<a name="l00442"></a>00442                       LapackInfo&amp; info = lapack_info);
<a name="l00443"></a>00443 
<a name="l00444"></a>00444 
<a name="l00445"></a>00445   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00446"></a>00446   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;float&gt;,
<a name="l00447"></a>00447                                   Prop, ColSymPacked, Allocator1&gt;&amp; A,
<a name="l00448"></a>00448                                   Vector&lt;complex&lt;float&gt;, VectFull, Allocator2&gt;&amp; w,
<a name="l00449"></a>00449                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00450"></a>00450                                   General, ColMajor, Allocator3&gt;&amp; z,
<a name="l00451"></a>00451                                   LapackInfo&amp; info = lapack_info);
<a name="l00452"></a>00452 
<a name="l00453"></a>00453 
<a name="l00454"></a>00454   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00455"></a>00455   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;double, Prop, ColSymPacked, Allocator1&gt;&amp; A,
<a name="l00456"></a>00456                       Vector&lt;double, VectFull, Allocator2&gt;&amp; w,
<a name="l00457"></a>00457                       LapackInfo&amp; info = lapack_info);
<a name="l00458"></a>00458 
<a name="l00459"></a>00459 
<a name="l00460"></a>00460   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00461"></a>00461   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;double,Prop,ColSymPacked, Allocator1&gt;&amp; A,
<a name="l00462"></a>00462                                   Vector&lt;double, VectFull, Allocator2&gt;&amp; w,
<a name="l00463"></a>00463                                   Matrix&lt;double, General, ColMajor, Allocator3&gt;&amp;z,
<a name="l00464"></a>00464                                   LapackInfo&amp; info = lapack_info);
<a name="l00465"></a>00465 
<a name="l00466"></a>00466 
<a name="l00467"></a>00467   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00468"></a>00468   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;double&gt;,
<a name="l00469"></a>00469                       Prop, ColSymPacked, Allocator1&gt;&amp; A,
<a name="l00470"></a>00470                       Vector&lt;complex&lt;double&gt;, VectFull, Allocator2&gt;&amp; w,
<a name="l00471"></a>00471                       LapackInfo&amp; info = lapack_info);
<a name="l00472"></a>00472 
<a name="l00473"></a>00473 
<a name="l00474"></a>00474   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00475"></a>00475   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;double&gt;,
<a name="l00476"></a>00476                                   Prop, ColSymPacked, Allocator1&gt;&amp; A,
<a name="l00477"></a>00477                                   Vector&lt;complex&lt;double&gt;, VectFull, Allocator2&gt;&amp; w,
<a name="l00478"></a>00478                                   Matrix&lt;complex&lt;double&gt;,
<a name="l00479"></a>00479                                   General, ColMajor, Allocator3&gt;&amp; z,
<a name="l00480"></a>00480                                   LapackInfo&amp; info = lapack_info);
<a name="l00481"></a>00481 
<a name="l00482"></a>00482 
<a name="l00483"></a>00483   <span class="comment">/* RowHermPacked */</span>
<a name="l00484"></a>00484 
<a name="l00485"></a>00485 
<a name="l00486"></a>00486   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00487"></a>00487   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;float&gt;,
<a name="l00488"></a>00488                       Prop, RowHermPacked, Allocator1&gt;&amp; A,
<a name="l00489"></a>00489                       Vector&lt;float, VectFull, Allocator2&gt;&amp; w,
<a name="l00490"></a>00490                       LapackInfo&amp; info = lapack_info);
<a name="l00491"></a>00491 
<a name="l00492"></a>00492 
<a name="l00493"></a>00493   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00494"></a>00494   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;float&gt;,
<a name="l00495"></a>00495                                   Prop, RowHermPacked, Allocator1&gt;&amp; A,
<a name="l00496"></a>00496                                   Vector&lt;float, VectFull, Allocator2&gt;&amp; w,
<a name="l00497"></a>00497                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00498"></a>00498                                   General, RowMajor, Allocator3&gt;&amp;z,
<a name="l00499"></a>00499                                   LapackInfo&amp; info = lapack_info);
<a name="l00500"></a>00500 
<a name="l00501"></a>00501 
<a name="l00502"></a>00502   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00503"></a>00503   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;double&gt;,
<a name="l00504"></a>00504                       Prop, RowHermPacked, Allocator1&gt;&amp; A,
<a name="l00505"></a>00505                       Vector&lt;double, VectFull, Allocator2&gt;&amp; w,
<a name="l00506"></a>00506                       LapackInfo&amp; info = lapack_info);
<a name="l00507"></a>00507 
<a name="l00508"></a>00508 
<a name="l00509"></a>00509   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00510"></a>00510   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;double&gt;,
<a name="l00511"></a>00511                                   Prop, RowHermPacked, Allocator1&gt;&amp; A,
<a name="l00512"></a>00512                                   Vector&lt;double, VectFull, Allocator2&gt;&amp; w,
<a name="l00513"></a>00513                                   Matrix&lt;complex&lt;double&gt;,
<a name="l00514"></a>00514                                   General, RowMajor, Allocator3&gt;&amp;z,
<a name="l00515"></a>00515                                   LapackInfo&amp; info = lapack_info);
<a name="l00516"></a>00516 
<a name="l00517"></a>00517 
<a name="l00518"></a>00518   <span class="comment">/* ColHermPacked */</span>
<a name="l00519"></a>00519 
<a name="l00520"></a>00520 
<a name="l00521"></a>00521   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00522"></a>00522   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;float&gt;,
<a name="l00523"></a>00523                       Prop, ColHermPacked, Allocator1&gt;&amp; A,
<a name="l00524"></a>00524                       Vector&lt;float, VectFull, Allocator2&gt;&amp; w,
<a name="l00525"></a>00525                       LapackInfo&amp; info = lapack_info);
<a name="l00526"></a>00526 
<a name="l00527"></a>00527 
<a name="l00528"></a>00528   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00529"></a>00529   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;float&gt;,
<a name="l00530"></a>00530                                   Prop, ColHermPacked, Allocator1&gt;&amp; A,
<a name="l00531"></a>00531                                   Vector&lt;float, VectFull, Allocator2&gt;&amp; w,
<a name="l00532"></a>00532                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00533"></a>00533                                   General, ColMajor, Allocator3&gt;&amp;z,
<a name="l00534"></a>00534                                   LapackInfo&amp; info = lapack_info);
<a name="l00535"></a>00535 
<a name="l00536"></a>00536 
<a name="l00537"></a>00537   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00538"></a>00538   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;double&gt;,
<a name="l00539"></a>00539                       Prop, ColHermPacked, Allocator1&gt;&amp; A,
<a name="l00540"></a>00540                       Vector&lt;double, VectFull, Allocator2&gt;&amp; w,
<a name="l00541"></a>00541                       LapackInfo&amp; info = lapack_info);
<a name="l00542"></a>00542 
<a name="l00543"></a>00543 
<a name="l00544"></a>00544   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Allocator3&gt;
<a name="l00545"></a>00545   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;double&gt;,
<a name="l00546"></a>00546                                   Prop, ColHermPacked, Allocator1&gt;&amp; A,
<a name="l00547"></a>00547                                   Vector&lt;double, VectFull, Allocator2&gt;&amp; w,
<a name="l00548"></a>00548                                   Matrix&lt;complex&lt;double&gt;,
<a name="l00549"></a>00549                                   General, ColMajor, Allocator3&gt;&amp;z,
<a name="l00550"></a>00550                                   LapackInfo&amp; info = lapack_info);
<a name="l00551"></a>00551 
<a name="l00552"></a>00552 
<a name="l00553"></a>00553   <span class="comment">// STANDARD EIGENVALUE PROBLEM //</span>
<a name="l00555"></a>00555 <span class="comment"></span>
<a name="l00556"></a>00556 
<a name="l00558"></a>00558   <span class="comment">// GENERALIZED EIGENVALUE PROBLEM //</span>
<a name="l00559"></a>00559 
<a name="l00560"></a>00560 
<a name="l00561"></a>00561   <span class="comment">/* RowSym */</span>
<a name="l00562"></a>00562 
<a name="l00563"></a>00563 
<a name="l00564"></a>00564   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l00565"></a>00565            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00566"></a>00566   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;float, Prop1, RowSym, Allocator1&gt;&amp; A,
<a name="l00567"></a>00567                       Matrix&lt;float, Prop2, RowSym, Allocator2&gt;&amp; B,
<a name="l00568"></a>00568                       Vector&lt;float, VectFull, Allocator3&gt;&amp; w,
<a name="l00569"></a>00569                       LapackInfo&amp; info = lapack_info);
<a name="l00570"></a>00570 
<a name="l00571"></a>00571 
<a name="l00572"></a>00572   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00573"></a>00573            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00574"></a>00574   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;float, Prop1, RowSym, Allocator1&gt;&amp; A,
<a name="l00575"></a>00575                                   Matrix&lt;float, Prop2, RowSym, Allocator2&gt;&amp; B,
<a name="l00576"></a>00576                                   Vector&lt;float, VectFull, Allocator3&gt;&amp; w,
<a name="l00577"></a>00577                                   Matrix&lt;float, General, RowMajor, Allocator4&gt;&amp; z,
<a name="l00578"></a>00578                                   LapackInfo&amp; info = lapack_info);
<a name="l00579"></a>00579 
<a name="l00580"></a>00580 
<a name="l00581"></a>00581   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l00582"></a>00582            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator4, <span class="keyword">class </span>Allocator5&gt;
<a name="l00583"></a>00583   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;float&gt;, Prop1, RowSym, Allocator1&gt;&amp; A,
<a name="l00584"></a>00584                       Matrix&lt;complex&lt;float&gt;, Prop2, RowSym, Allocator2&gt;&amp; B,
<a name="l00585"></a>00585                       Vector&lt;complex&lt;float&gt;, VectFull, Allocator4&gt;&amp; alpha,
<a name="l00586"></a>00586                       Vector&lt;complex&lt;float&gt;, VectFull, Allocator5&gt;&amp; beta,
<a name="l00587"></a>00587                       LapackInfo&amp; info = lapack_info);
<a name="l00588"></a>00588 
<a name="l00589"></a>00589 
<a name="l00590"></a>00590   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Prop3, <span class="keyword">class </span>Allocator1,
<a name="l00591"></a>00591            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator4,
<a name="l00592"></a>00592            <span class="keyword">class </span>Allocator5, <span class="keyword">class </span>Allocator6&gt;
<a name="l00593"></a>00593   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;float&gt;,
<a name="l00594"></a>00594                                   Prop1, RowSym, Allocator1&gt;&amp; A,
<a name="l00595"></a>00595                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00596"></a>00596                                   Prop2, RowSym, Allocator2&gt;&amp; B,
<a name="l00597"></a>00597                                   Vector&lt;complex&lt;float&gt;,
<a name="l00598"></a>00598                                   VectFull, Allocator4&gt;&amp; alpha,
<a name="l00599"></a>00599                                   Vector&lt;complex&lt;float&gt;,
<a name="l00600"></a>00600                                   VectFull, Allocator5&gt;&amp; beta,
<a name="l00601"></a>00601                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00602"></a>00602                                   Prop3, RowMajor, Allocator6&gt;&amp; V,
<a name="l00603"></a>00603                                   LapackInfo&amp; info = lapack_info);
<a name="l00604"></a>00604 
<a name="l00605"></a>00605 
<a name="l00606"></a>00606   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l00607"></a>00607            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00608"></a>00608   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;double, Prop1, RowSym, Allocator1&gt;&amp; A,
<a name="l00609"></a>00609                       Matrix&lt;double, Prop2, RowSym, Allocator2&gt;&amp; B,
<a name="l00610"></a>00610                       Vector&lt;double, VectFull, Allocator3&gt;&amp; w,
<a name="l00611"></a>00611                       LapackInfo&amp; info = lapack_info);
<a name="l00612"></a>00612 
<a name="l00613"></a>00613 
<a name="l00614"></a>00614   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00615"></a>00615            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00616"></a>00616   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;double, Prop1, RowSym, Allocator1&gt;&amp; A,
<a name="l00617"></a>00617                                   Matrix&lt;double, Prop2, RowSym, Allocator2&gt;&amp; B,
<a name="l00618"></a>00618                                   Vector&lt;double, VectFull, Allocator3&gt;&amp; w,
<a name="l00619"></a>00619                                   Matrix&lt;double, General, RowMajor, Allocator4&gt;&amp; z,
<a name="l00620"></a>00620                                   LapackInfo&amp; info = lapack_info);
<a name="l00621"></a>00621 
<a name="l00622"></a>00622 
<a name="l00623"></a>00623   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l00624"></a>00624            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator4, <span class="keyword">class </span>Allocator5&gt;
<a name="l00625"></a>00625   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;double&gt;, Prop1, RowSym, Allocator1&gt;&amp; A,
<a name="l00626"></a>00626                       Matrix&lt;complex&lt;double&gt;, Prop2, RowSym, Allocator2&gt;&amp; B,
<a name="l00627"></a>00627                       Vector&lt;complex&lt;double&gt;, VectFull, Allocator4&gt;&amp; alpha,
<a name="l00628"></a>00628                       Vector&lt;complex&lt;double&gt;, VectFull, Allocator5&gt;&amp; beta,
<a name="l00629"></a>00629                       LapackInfo&amp; info = lapack_info);
<a name="l00630"></a>00630 
<a name="l00631"></a>00631 
<a name="l00632"></a>00632   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Prop3, <span class="keyword">class </span>Allocator1,
<a name="l00633"></a>00633            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator4,
<a name="l00634"></a>00634            <span class="keyword">class </span>Allocator5, <span class="keyword">class </span>Allocator6&gt;
<a name="l00635"></a>00635   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;double&gt;,
<a name="l00636"></a>00636                                   Prop1, RowSym, Allocator1&gt;&amp; A,
<a name="l00637"></a>00637                                   Matrix&lt;complex&lt;double&gt;,
<a name="l00638"></a>00638                                   Prop2, RowSym, Allocator2&gt;&amp; B,
<a name="l00639"></a>00639                                   Vector&lt;complex&lt;double&gt;,
<a name="l00640"></a>00640                                   VectFull, Allocator4&gt;&amp; alpha,
<a name="l00641"></a>00641                                   Vector&lt;complex&lt;double&gt;,
<a name="l00642"></a>00642                                   VectFull, Allocator5&gt;&amp; beta,
<a name="l00643"></a>00643                                   Matrix&lt;complex&lt;double&gt;,
<a name="l00644"></a>00644                                   Prop3, RowMajor, Allocator6&gt;&amp; V,
<a name="l00645"></a>00645                                   LapackInfo&amp; info = lapack_info);
<a name="l00646"></a>00646 
<a name="l00647"></a>00647 
<a name="l00648"></a>00648   <span class="comment">/* ColSym */</span>
<a name="l00649"></a>00649 
<a name="l00650"></a>00650 
<a name="l00651"></a>00651   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l00652"></a>00652            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00653"></a>00653   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;float, Prop1, ColSym, Allocator1&gt;&amp; A,
<a name="l00654"></a>00654                       Matrix&lt;float, Prop2, ColSym, Allocator2&gt;&amp; B,
<a name="l00655"></a>00655                       Vector&lt;float, VectFull, Allocator3&gt;&amp; w,
<a name="l00656"></a>00656                       LapackInfo&amp; info = lapack_info);
<a name="l00657"></a>00657 
<a name="l00658"></a>00658 
<a name="l00659"></a>00659   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00660"></a>00660            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00661"></a>00661   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;float, Prop1, ColSym, Allocator1&gt;&amp; A,
<a name="l00662"></a>00662                                   Matrix&lt;float, Prop2, ColSym, Allocator2&gt;&amp; B,
<a name="l00663"></a>00663                                   Vector&lt;float, VectFull, Allocator3&gt;&amp; w,
<a name="l00664"></a>00664                                   Matrix&lt;float, General, ColMajor, Allocator4&gt;&amp; z,
<a name="l00665"></a>00665                                   LapackInfo&amp; info = lapack_info);
<a name="l00666"></a>00666 
<a name="l00667"></a>00667 
<a name="l00668"></a>00668   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l00669"></a>00669            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator4, <span class="keyword">class </span>Allocator5&gt;
<a name="l00670"></a>00670   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;float&gt;, Prop1, ColSym, Allocator1&gt;&amp; A,
<a name="l00671"></a>00671                       Matrix&lt;complex&lt;float&gt;, Prop2, ColSym, Allocator2&gt;&amp; B,
<a name="l00672"></a>00672                       Vector&lt;complex&lt;float&gt;, VectFull, Allocator4&gt;&amp; alpha,
<a name="l00673"></a>00673                       Vector&lt;complex&lt;float&gt;, VectFull, Allocator5&gt;&amp; beta,
<a name="l00674"></a>00674                       LapackInfo&amp; info = lapack_info);
<a name="l00675"></a>00675 
<a name="l00676"></a>00676 
<a name="l00677"></a>00677   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Prop3, <span class="keyword">class </span>Alloc1,
<a name="l00678"></a>00678            <span class="keyword">class </span>Alloc2, <span class="keyword">class </span>Alloc4, <span class="keyword">class </span>Alloc5, <span class="keyword">class </span>Alloc6&gt;
<a name="l00679"></a>00679   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;float&gt;,
<a name="l00680"></a>00680                                   Prop1, ColSym, Alloc1&gt;&amp; A,
<a name="l00681"></a>00681                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00682"></a>00682                                   Prop2, ColSym, Alloc2&gt;&amp; B,
<a name="l00683"></a>00683                                   Vector&lt;complex&lt;float&gt;, VectFull, Alloc4&gt;&amp; alpha,
<a name="l00684"></a>00684                                   Vector&lt;complex&lt;float&gt;, VectFull, Alloc5&gt;&amp; beta,
<a name="l00685"></a>00685                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00686"></a>00686                                   Prop3, ColMajor, Alloc6&gt;&amp; V,
<a name="l00687"></a>00687                                   LapackInfo&amp; info = lapack_info);
<a name="l00688"></a>00688 
<a name="l00689"></a>00689   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l00690"></a>00690            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00691"></a>00691   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;double, Prop1, ColSym, Allocator1&gt;&amp; A,
<a name="l00692"></a>00692                       Matrix&lt;double, Prop2, ColSym, Allocator2&gt;&amp; B,
<a name="l00693"></a>00693                       Vector&lt;double, VectFull, Allocator3&gt;&amp; w,
<a name="l00694"></a>00694                       LapackInfo&amp; info = lapack_info);
<a name="l00695"></a>00695 
<a name="l00696"></a>00696 
<a name="l00697"></a>00697   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00698"></a>00698            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00699"></a>00699   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;double, Prop1, ColSym, Allocator1&gt;&amp; A,
<a name="l00700"></a>00700                                   Matrix&lt;double, Prop2, ColSym, Allocator2&gt;&amp; B,
<a name="l00701"></a>00701                                   Vector&lt;double, VectFull, Allocator3&gt;&amp; w,
<a name="l00702"></a>00702                                   Matrix&lt;double, General, ColMajor, Allocator4&gt;&amp; z,
<a name="l00703"></a>00703                                   LapackInfo&amp; info = lapack_info);
<a name="l00704"></a>00704 
<a name="l00705"></a>00705 
<a name="l00706"></a>00706   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l00707"></a>00707            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator4, <span class="keyword">class </span>Allocator5&gt;
<a name="l00708"></a>00708   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;double&gt;, Prop1, ColSym, Allocator1&gt;&amp; A,
<a name="l00709"></a>00709                       Matrix&lt;complex&lt;double&gt;, Prop2, ColSym, Allocator2&gt;&amp; B,
<a name="l00710"></a>00710                       Vector&lt;complex&lt;double&gt;, VectFull, Allocator4&gt;&amp; alpha,
<a name="l00711"></a>00711                       Vector&lt;complex&lt;double&gt;, VectFull, Allocator5&gt;&amp; beta,
<a name="l00712"></a>00712                       LapackInfo&amp; info = lapack_info);
<a name="l00713"></a>00713 
<a name="l00714"></a>00714 
<a name="l00715"></a>00715   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Prop3, <span class="keyword">class </span>Alloc1,
<a name="l00716"></a>00716            <span class="keyword">class </span>Alloc2, <span class="keyword">class </span>Alloc4, <span class="keyword">class </span>Alloc5, <span class="keyword">class </span>Alloc6&gt;
<a name="l00717"></a>00717   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;double&gt;,
<a name="l00718"></a>00718                                   Prop1, ColSym, Alloc1&gt;&amp; A,
<a name="l00719"></a>00719                                   Matrix&lt;complex&lt;double&gt;,
<a name="l00720"></a>00720                                   Prop2, ColSym, Alloc2&gt;&amp; B,
<a name="l00721"></a>00721                                   Vector&lt;complex&lt;double&gt;, VectFull, Alloc4&gt;&amp; alpha,
<a name="l00722"></a>00722                                   Vector&lt;complex&lt;double&gt;, VectFull, Alloc5&gt;&amp; beta,
<a name="l00723"></a>00723                                   Matrix&lt;complex&lt;double&gt;,
<a name="l00724"></a>00724                                   Prop3, ColMajor, Alloc6&gt;&amp; V,
<a name="l00725"></a>00725                                   LapackInfo&amp; info = lapack_info);
<a name="l00726"></a>00726 
<a name="l00727"></a>00727 
<a name="l00728"></a>00728   <span class="comment">/* RowHerm */</span>
<a name="l00729"></a>00729 
<a name="l00730"></a>00730 
<a name="l00731"></a>00731   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l00732"></a>00732            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00733"></a>00733   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;float&gt;, Prop1, RowHerm, Allocator1&gt;&amp; A,
<a name="l00734"></a>00734                       Matrix&lt;complex&lt;float&gt;, Prop2, RowHerm, Allocator2&gt;&amp; B,
<a name="l00735"></a>00735                       Vector&lt;float, VectFull, Allocator3&gt;&amp; w,
<a name="l00736"></a>00736                       LapackInfo&amp; info = lapack_info);
<a name="l00737"></a>00737 
<a name="l00738"></a>00738 
<a name="l00739"></a>00739   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00740"></a>00740            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00741"></a>00741   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;float&gt;,
<a name="l00742"></a>00742                                   Prop1, RowHerm, Allocator1&gt;&amp; A,
<a name="l00743"></a>00743                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00744"></a>00744                                   Prop2, RowHerm, Allocator2&gt;&amp; B,
<a name="l00745"></a>00745                                   Vector&lt;float, VectFull, Allocator3&gt;&amp; w,
<a name="l00746"></a>00746                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00747"></a>00747                                   General, RowMajor, Allocator4&gt;&amp; z,
<a name="l00748"></a>00748                                   LapackInfo&amp; info = lapack_info);
<a name="l00749"></a>00749 
<a name="l00750"></a>00750 
<a name="l00751"></a>00751   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l00752"></a>00752            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00753"></a>00753   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;double&gt;, Prop1, RowHerm, Allocator1&gt;&amp; A,
<a name="l00754"></a>00754                       Matrix&lt;complex&lt;double&gt;, Prop2, RowHerm, Allocator2&gt;&amp; B,
<a name="l00755"></a>00755                       Vector&lt;double, VectFull, Allocator3&gt;&amp; w,
<a name="l00756"></a>00756                       LapackInfo&amp; info = lapack_info);
<a name="l00757"></a>00757 
<a name="l00758"></a>00758 
<a name="l00759"></a>00759   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00760"></a>00760            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00761"></a>00761   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;double&gt;,
<a name="l00762"></a>00762                                   Prop1, RowHerm, Allocator1&gt;&amp; A,
<a name="l00763"></a>00763                                   Matrix&lt;complex&lt;double&gt;,
<a name="l00764"></a>00764                                   Prop2, RowHerm, Allocator2&gt;&amp; B,
<a name="l00765"></a>00765                                   Vector&lt;double, VectFull, Allocator3&gt;&amp; w,
<a name="l00766"></a>00766                                   Matrix&lt;complex&lt;double&gt;,
<a name="l00767"></a>00767                                   General, RowMajor, Allocator4&gt;&amp; z,
<a name="l00768"></a>00768                                   LapackInfo&amp; info = lapack_info);
<a name="l00769"></a>00769 
<a name="l00770"></a>00770 
<a name="l00771"></a>00771   <span class="comment">/* ColHerm */</span>
<a name="l00772"></a>00772 
<a name="l00773"></a>00773 
<a name="l00774"></a>00774   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l00775"></a>00775            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00776"></a>00776   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;float&gt;, Prop1, ColHerm, Allocator1&gt;&amp; A,
<a name="l00777"></a>00777                       Matrix&lt;complex&lt;float&gt;, Prop2, ColHerm, Allocator2&gt;&amp; B,
<a name="l00778"></a>00778                       Vector&lt;float, VectFull, Allocator3&gt;&amp; w,
<a name="l00779"></a>00779                       LapackInfo&amp; info = lapack_info);
<a name="l00780"></a>00780 
<a name="l00781"></a>00781 
<a name="l00782"></a>00782   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00783"></a>00783            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00784"></a>00784   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;float&gt;,
<a name="l00785"></a>00785                                   Prop1, ColHerm, Allocator1&gt;&amp; A,
<a name="l00786"></a>00786                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00787"></a>00787                                   Prop2, ColHerm, Allocator2&gt;&amp; B,
<a name="l00788"></a>00788                                   Vector&lt;float, VectFull, Allocator3&gt;&amp; w,
<a name="l00789"></a>00789                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00790"></a>00790                                   General, ColMajor, Allocator4&gt;&amp; z,
<a name="l00791"></a>00791                                   LapackInfo&amp; info = lapack_info);
<a name="l00792"></a>00792 
<a name="l00793"></a>00793 
<a name="l00794"></a>00794   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l00795"></a>00795            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00796"></a>00796   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;double&gt;, Prop1, ColHerm, Allocator1&gt;&amp; A,
<a name="l00797"></a>00797                       Matrix&lt;complex&lt;double&gt;, Prop2, ColHerm, Allocator2&gt;&amp; B,
<a name="l00798"></a>00798                       Vector&lt;double, VectFull, Allocator3&gt;&amp; w,
<a name="l00799"></a>00799                       LapackInfo&amp; info = lapack_info);
<a name="l00800"></a>00800 
<a name="l00801"></a>00801 
<a name="l00802"></a>00802   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00803"></a>00803            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00804"></a>00804   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;double&gt;,
<a name="l00805"></a>00805                                   Prop1, ColHerm, Allocator1&gt;&amp; A,
<a name="l00806"></a>00806                                   Matrix&lt;complex&lt;double&gt;,
<a name="l00807"></a>00807                                   Prop2, ColHerm, Allocator2&gt;&amp; B,
<a name="l00808"></a>00808                                   Vector&lt;double, VectFull, Allocator3&gt;&amp; w,
<a name="l00809"></a>00809                                   Matrix&lt;complex&lt;double&gt;,
<a name="l00810"></a>00810                                   General, ColMajor, Allocator4&gt;&amp; z,
<a name="l00811"></a>00811                                   LapackInfo&amp; info = lapack_info);
<a name="l00812"></a>00812 
<a name="l00813"></a>00813 
<a name="l00814"></a>00814   <span class="comment">/* RowSymPacked */</span>
<a name="l00815"></a>00815 
<a name="l00816"></a>00816 
<a name="l00817"></a>00817   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l00818"></a>00818            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00819"></a>00819   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;float, Prop1, RowSymPacked, Allocator1&gt;&amp; A,
<a name="l00820"></a>00820                       Matrix&lt;float, Prop2, RowSymPacked, Allocator2&gt;&amp; B,
<a name="l00821"></a>00821                       Vector&lt;float, VectFull, Allocator3&gt;&amp; w,
<a name="l00822"></a>00822                       LapackInfo&amp; info = lapack_info);
<a name="l00823"></a>00823 
<a name="l00824"></a>00824 
<a name="l00825"></a>00825   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00826"></a>00826            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00827"></a>00827   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;<span class="keywordtype">float</span>,
<a name="l00828"></a>00828                                   Prop1, RowSymPacked, Allocator1&gt;&amp; A,
<a name="l00829"></a>00829                                   Matrix&lt;<span class="keywordtype">float</span>,
<a name="l00830"></a>00830                                   Prop2, RowSymPacked, Allocator2&gt;&amp; B,
<a name="l00831"></a>00831                                   Vector&lt;float, VectFull, Allocator3&gt;&amp; w,
<a name="l00832"></a>00832                                   Matrix&lt;<span class="keywordtype">float</span>,
<a name="l00833"></a>00833                                   General, RowMajor, Allocator4&gt;&amp; z,
<a name="l00834"></a>00834                                   LapackInfo&amp; info = lapack_info);
<a name="l00835"></a>00835 
<a name="l00836"></a>00836 
<a name="l00837"></a>00837   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00838"></a>00838            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00839"></a>00839   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;float&gt;,
<a name="l00840"></a>00840                       Prop1, RowSymPacked, Allocator1&gt;&amp; A,
<a name="l00841"></a>00841                       Matrix&lt;complex&lt;float&gt;,
<a name="l00842"></a>00842                       Prop2, RowSymPacked, Allocator2&gt;&amp; B,
<a name="l00843"></a>00843                       Vector&lt;complex&lt;float&gt;, VectFull, Allocator3&gt;&amp; alpha,
<a name="l00844"></a>00844                       Vector&lt;complex&lt;float&gt;, VectFull, Allocator4&gt;&amp; beta,
<a name="l00845"></a>00845                       LapackInfo&amp; info = lapack_info);
<a name="l00846"></a>00846 
<a name="l00847"></a>00847 
<a name="l00848"></a>00848   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00849"></a>00849            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4, <span class="keyword">class </span>Allocator5&gt;
<a name="l00850"></a>00850   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;float&gt;,
<a name="l00851"></a>00851                                   Prop1, RowSymPacked, Allocator1&gt;&amp; A,
<a name="l00852"></a>00852                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00853"></a>00853                                   Prop2, RowSymPacked, Allocator2&gt;&amp; B,
<a name="l00854"></a>00854                                   Vector&lt;complex&lt;float&gt;,
<a name="l00855"></a>00855                                   VectFull, Allocator3&gt;&amp; alpha,
<a name="l00856"></a>00856                                   Vector&lt;complex&lt;float&gt;,
<a name="l00857"></a>00857                                   VectFull, Allocator4&gt;&amp; beta,
<a name="l00858"></a>00858                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00859"></a>00859                                   General, RowMajor, Allocator5&gt;&amp; z,
<a name="l00860"></a>00860                                   LapackInfo&amp; info = lapack_info);
<a name="l00861"></a>00861 
<a name="l00862"></a>00862 
<a name="l00863"></a>00863   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l00864"></a>00864            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00865"></a>00865   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;double, Prop1, RowSymPacked, Allocator1&gt;&amp; A,
<a name="l00866"></a>00866                       Matrix&lt;double, Prop2, RowSymPacked, Allocator2&gt;&amp; B,
<a name="l00867"></a>00867                       Vector&lt;double, VectFull, Allocator3&gt;&amp; w,
<a name="l00868"></a>00868                       LapackInfo&amp; info = lapack_info);
<a name="l00869"></a>00869 
<a name="l00870"></a>00870 
<a name="l00871"></a>00871   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00872"></a>00872            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00873"></a>00873   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;<span class="keywordtype">double</span>,
<a name="l00874"></a>00874                                   Prop1, RowSymPacked, Allocator1&gt;&amp; A,
<a name="l00875"></a>00875                                   Matrix&lt;<span class="keywordtype">double</span>,
<a name="l00876"></a>00876                                   Prop2, RowSymPacked, Allocator2&gt;&amp; B,
<a name="l00877"></a>00877                                   Vector&lt;double, VectFull, Allocator3&gt;&amp; w,
<a name="l00878"></a>00878                                   Matrix&lt;<span class="keywordtype">double</span>,
<a name="l00879"></a>00879                                   General, RowMajor, Allocator4&gt;&amp; z,
<a name="l00880"></a>00880                                   LapackInfo&amp; info = lapack_info);
<a name="l00881"></a>00881 
<a name="l00882"></a>00882 
<a name="l00883"></a>00883   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00884"></a>00884            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00885"></a>00885   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;double&gt;,
<a name="l00886"></a>00886                       Prop1, RowSymPacked, Allocator1&gt;&amp; A,
<a name="l00887"></a>00887                       Matrix&lt;complex&lt;double&gt;,
<a name="l00888"></a>00888                       Prop2, RowSymPacked, Allocator2&gt;&amp; B,
<a name="l00889"></a>00889                       Vector&lt;complex&lt;double&gt;, VectFull, Allocator3&gt;&amp; alpha,
<a name="l00890"></a>00890                       Vector&lt;complex&lt;double&gt;, VectFull, Allocator4&gt;&amp; beta,
<a name="l00891"></a>00891                       LapackInfo&amp; info = lapack_info);
<a name="l00892"></a>00892 
<a name="l00893"></a>00893 
<a name="l00894"></a>00894   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00895"></a>00895            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4, <span class="keyword">class </span>Allocator5&gt;
<a name="l00896"></a>00896   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;double&gt;,
<a name="l00897"></a>00897                                   Prop1, RowSymPacked, Allocator1&gt;&amp; A,
<a name="l00898"></a>00898                                   Matrix&lt;complex&lt;double&gt;,
<a name="l00899"></a>00899                                   Prop2, RowSymPacked, Allocator2&gt;&amp; B,
<a name="l00900"></a>00900                                   Vector&lt;complex&lt;double&gt;,
<a name="l00901"></a>00901                                   VectFull, Allocator3&gt;&amp; alpha,
<a name="l00902"></a>00902                                   Vector&lt;complex&lt;double&gt;,
<a name="l00903"></a>00903                                   VectFull, Allocator4&gt;&amp; beta,
<a name="l00904"></a>00904                                   Matrix&lt;complex&lt;double&gt;,
<a name="l00905"></a>00905                                   General, RowMajor, Allocator5&gt;&amp; z,
<a name="l00906"></a>00906                                   LapackInfo&amp; info = lapack_info);
<a name="l00907"></a>00907 
<a name="l00908"></a>00908 
<a name="l00909"></a>00909   <span class="comment">/* ColSymPacked */</span>
<a name="l00910"></a>00910 
<a name="l00911"></a>00911 
<a name="l00912"></a>00912   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l00913"></a>00913            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00914"></a>00914   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;float, Prop1, ColSymPacked, Allocator1&gt;&amp; A,
<a name="l00915"></a>00915                       Matrix&lt;float, Prop2, ColSymPacked, Allocator2&gt;&amp; B,
<a name="l00916"></a>00916                       Vector&lt;float, VectFull, Allocator3&gt;&amp; w,
<a name="l00917"></a>00917                       LapackInfo&amp; info = lapack_info);
<a name="l00918"></a>00918 
<a name="l00919"></a>00919 
<a name="l00920"></a>00920   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00921"></a>00921            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00922"></a>00922   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;<span class="keywordtype">float</span>,
<a name="l00923"></a>00923                                   Prop1, ColSymPacked, Allocator1&gt;&amp; A,
<a name="l00924"></a>00924                                   Matrix&lt;<span class="keywordtype">float</span>,
<a name="l00925"></a>00925                                   Prop2, ColSymPacked, Allocator2&gt;&amp; B,
<a name="l00926"></a>00926                                   Vector&lt;float, VectFull, Allocator3&gt;&amp; w,
<a name="l00927"></a>00927                                   Matrix&lt;<span class="keywordtype">float</span>,
<a name="l00928"></a>00928                                   General, ColMajor, Allocator4&gt;&amp; z,
<a name="l00929"></a>00929                                   LapackInfo&amp; info = lapack_info);
<a name="l00930"></a>00930 
<a name="l00931"></a>00931 
<a name="l00932"></a>00932   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00933"></a>00933            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00934"></a>00934   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;float&gt;,
<a name="l00935"></a>00935                       Prop1, ColSymPacked, Allocator1&gt;&amp; A,
<a name="l00936"></a>00936                       Matrix&lt;complex&lt;float&gt;,
<a name="l00937"></a>00937                       Prop2, ColSymPacked, Allocator2&gt;&amp; B,
<a name="l00938"></a>00938                       Vector&lt;complex&lt;float&gt;, VectFull, Allocator3&gt;&amp; alpha,
<a name="l00939"></a>00939                       Vector&lt;complex&lt;float&gt;, VectFull, Allocator4&gt;&amp; beta,
<a name="l00940"></a>00940                       LapackInfo&amp; info = lapack_info);
<a name="l00941"></a>00941 
<a name="l00942"></a>00942 
<a name="l00943"></a>00943   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00944"></a>00944            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4, <span class="keyword">class </span>Allocator5&gt;
<a name="l00945"></a>00945   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;float&gt;,
<a name="l00946"></a>00946                                   Prop1, ColSymPacked, Allocator1&gt;&amp; A,
<a name="l00947"></a>00947                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00948"></a>00948                                   Prop2, ColSymPacked, Allocator2&gt;&amp; B,
<a name="l00949"></a>00949                                   Vector&lt;complex&lt;float&gt;,
<a name="l00950"></a>00950                                   VectFull, Allocator3&gt;&amp; alpha,
<a name="l00951"></a>00951                                   Vector&lt;complex&lt;float&gt;,
<a name="l00952"></a>00952                                   VectFull, Allocator4&gt;&amp; beta,
<a name="l00953"></a>00953                                   Matrix&lt;complex&lt;float&gt;,
<a name="l00954"></a>00954                                   General, ColMajor, Allocator5&gt;&amp; z,
<a name="l00955"></a>00955                                   LapackInfo&amp; info = lapack_info);
<a name="l00956"></a>00956 
<a name="l00957"></a>00957 
<a name="l00958"></a>00958   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l00959"></a>00959            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00960"></a>00960   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;double, Prop1, ColSymPacked, Allocator1&gt;&amp; A,
<a name="l00961"></a>00961                       Matrix&lt;double, Prop2, ColSymPacked, Allocator2&gt;&amp; B,
<a name="l00962"></a>00962                       Vector&lt;double, VectFull, Allocator3&gt;&amp; w,
<a name="l00963"></a>00963                       LapackInfo&amp; info = lapack_info);
<a name="l00964"></a>00964 
<a name="l00965"></a>00965 
<a name="l00966"></a>00966   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00967"></a>00967            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00968"></a>00968   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;<span class="keywordtype">double</span>,
<a name="l00969"></a>00969                                   Prop1, ColSymPacked, Allocator1&gt;&amp; A,
<a name="l00970"></a>00970                                   Matrix&lt;<span class="keywordtype">double</span>,
<a name="l00971"></a>00971                                   Prop2, ColSymPacked, Allocator2&gt;&amp; B,
<a name="l00972"></a>00972                                   Vector&lt;double, VectFull, Allocator3&gt;&amp; w,
<a name="l00973"></a>00973                                   Matrix&lt;<span class="keywordtype">double</span>,
<a name="l00974"></a>00974                                   General, ColMajor, Allocator4&gt;&amp; z,
<a name="l00975"></a>00975                                   LapackInfo&amp; info = lapack_info);
<a name="l00976"></a>00976 
<a name="l00977"></a>00977 
<a name="l00978"></a>00978   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00979"></a>00979            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l00980"></a>00980   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;double&gt;,
<a name="l00981"></a>00981                       Prop1, ColSymPacked, Allocator1&gt;&amp; A,
<a name="l00982"></a>00982                       Matrix&lt;complex&lt;double&gt;,
<a name="l00983"></a>00983                       Prop2, ColSymPacked, Allocator2&gt;&amp; B,
<a name="l00984"></a>00984                       Vector&lt;complex&lt;double&gt;, VectFull, Allocator3&gt;&amp; alpha,
<a name="l00985"></a>00985                       Vector&lt;complex&lt;double&gt;, VectFull, Allocator4&gt;&amp; beta,
<a name="l00986"></a>00986                       LapackInfo&amp; info = lapack_info);
<a name="l00987"></a>00987 
<a name="l00988"></a>00988 
<a name="l00989"></a>00989   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l00990"></a>00990            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4, <span class="keyword">class </span>Allocator5&gt;
<a name="l00991"></a>00991   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;double&gt;,
<a name="l00992"></a>00992                                   Prop1, ColSymPacked, Allocator1&gt;&amp; A,
<a name="l00993"></a>00993                                   Matrix&lt;complex&lt;double&gt;,
<a name="l00994"></a>00994                                   Prop2, ColSymPacked, Allocator2&gt;&amp; B,
<a name="l00995"></a>00995                                   Vector&lt;complex&lt;double&gt;,
<a name="l00996"></a>00996                                   VectFull, Allocator3&gt;&amp; alpha,
<a name="l00997"></a>00997                                   Vector&lt;complex&lt;double&gt;,
<a name="l00998"></a>00998                                   VectFull, Allocator4&gt;&amp; beta,
<a name="l00999"></a>00999                                   Matrix&lt;complex&lt;double&gt;,
<a name="l01000"></a>01000                                   General, ColMajor, Allocator5&gt;&amp; z,
<a name="l01001"></a>01001                                   LapackInfo&amp; info = lapack_info);
<a name="l01002"></a>01002 
<a name="l01003"></a>01003 
<a name="l01004"></a>01004   <span class="comment">/* RowHermPacked */</span>
<a name="l01005"></a>01005 
<a name="l01006"></a>01006 
<a name="l01007"></a>01007   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l01008"></a>01008            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l01009"></a>01009   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;float&gt;,
<a name="l01010"></a>01010                       Prop1, RowHermPacked, Allocator1&gt;&amp; A,
<a name="l01011"></a>01011                       Matrix&lt;complex&lt;float&gt;,
<a name="l01012"></a>01012                       Prop2, RowHermPacked, Allocator2&gt;&amp; B,
<a name="l01013"></a>01013                       Vector&lt;float, VectFull, Allocator3&gt;&amp; w,
<a name="l01014"></a>01014                       LapackInfo&amp; info = lapack_info);
<a name="l01015"></a>01015 
<a name="l01016"></a>01016 
<a name="l01017"></a>01017   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l01018"></a>01018            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l01019"></a>01019   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;float&gt;,
<a name="l01020"></a>01020                                   Prop1, RowHermPacked, Allocator1&gt;&amp; A,
<a name="l01021"></a>01021                                   Matrix&lt;complex&lt;float&gt;,
<a name="l01022"></a>01022                                   Prop2, RowHermPacked, Allocator2&gt;&amp; B,
<a name="l01023"></a>01023                                   Vector&lt;float, VectFull, Allocator3&gt;&amp; w,
<a name="l01024"></a>01024                                   Matrix&lt;complex&lt;float&gt;,
<a name="l01025"></a>01025                                   General, RowMajor, Allocator4&gt;&amp; z,
<a name="l01026"></a>01026                                   LapackInfo&amp; info = lapack_info);
<a name="l01027"></a>01027 
<a name="l01028"></a>01028 
<a name="l01029"></a>01029   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l01030"></a>01030            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l01031"></a>01031   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;double&gt;,
<a name="l01032"></a>01032                       Prop1, RowHermPacked, Allocator1&gt;&amp; A,
<a name="l01033"></a>01033                       Matrix&lt;complex&lt;double&gt;,
<a name="l01034"></a>01034                       Prop2, RowHermPacked, Allocator2&gt;&amp; B,
<a name="l01035"></a>01035                       Vector&lt;double, VectFull, Allocator3&gt;&amp; w,
<a name="l01036"></a>01036                       LapackInfo&amp; info = lapack_info);
<a name="l01037"></a>01037 
<a name="l01038"></a>01038 
<a name="l01039"></a>01039   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l01040"></a>01040            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l01041"></a>01041   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;double&gt;,
<a name="l01042"></a>01042                                   Prop1, RowHermPacked, Allocator1&gt;&amp; A,
<a name="l01043"></a>01043                                   Matrix&lt;complex&lt;double&gt;,
<a name="l01044"></a>01044                                   Prop2, RowHermPacked, Allocator2&gt;&amp; B,
<a name="l01045"></a>01045                                   Vector&lt;double, VectFull, Allocator3&gt;&amp; w,
<a name="l01046"></a>01046                                   Matrix&lt;complex&lt;double&gt;,
<a name="l01047"></a>01047                                   General, RowMajor, Allocator4&gt;&amp; z,
<a name="l01048"></a>01048                                   LapackInfo&amp; info = lapack_info);
<a name="l01049"></a>01049 
<a name="l01050"></a>01050 
<a name="l01051"></a>01051   <span class="comment">/* ColHermPacked */</span>
<a name="l01052"></a>01052 
<a name="l01053"></a>01053 
<a name="l01054"></a>01054   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l01055"></a>01055            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l01056"></a>01056   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;float&gt;,
<a name="l01057"></a>01057                       Prop1, ColHermPacked, Allocator1&gt;&amp; A,
<a name="l01058"></a>01058                       Matrix&lt;complex&lt;float&gt;,
<a name="l01059"></a>01059                       Prop2, ColHermPacked, Allocator2&gt;&amp; B,
<a name="l01060"></a>01060                       Vector&lt;float, VectFull, Allocator3&gt;&amp; w,
<a name="l01061"></a>01061                       LapackInfo&amp; info = lapack_info);
<a name="l01062"></a>01062 
<a name="l01063"></a>01063 
<a name="l01064"></a>01064   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l01065"></a>01065            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l01066"></a>01066   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;float&gt;,
<a name="l01067"></a>01067                                   Prop1, ColHermPacked, Allocator1&gt;&amp; A,
<a name="l01068"></a>01068                                   Matrix&lt;complex&lt;float&gt;,
<a name="l01069"></a>01069                                   Prop2, ColHermPacked, Allocator2&gt;&amp; B,
<a name="l01070"></a>01070                                   Vector&lt;float, VectFull, Allocator3&gt;&amp; w,
<a name="l01071"></a>01071                                   Matrix&lt;complex&lt;float&gt;,
<a name="l01072"></a>01072                                   General, ColMajor, Allocator4&gt;&amp; z,
<a name="l01073"></a>01073                                   LapackInfo&amp; info = lapack_info);
<a name="l01074"></a>01074 
<a name="l01075"></a>01075 
<a name="l01076"></a>01076   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l01077"></a>01077            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l01078"></a>01078   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;double&gt;,
<a name="l01079"></a>01079                       Prop1, ColHermPacked, Allocator1&gt;&amp; A,
<a name="l01080"></a>01080                       Matrix&lt;complex&lt;double&gt;,
<a name="l01081"></a>01081                       Prop2, ColHermPacked, Allocator2&gt;&amp; B,
<a name="l01082"></a>01082                       Vector&lt;double, VectFull, Allocator3&gt;&amp; w,
<a name="l01083"></a>01083                       LapackInfo&amp; info = lapack_info);
<a name="l01084"></a>01084 
<a name="l01085"></a>01085 
<a name="l01086"></a>01086   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l01087"></a>01087            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l01088"></a>01088   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;double&gt;,
<a name="l01089"></a>01089                                   Prop1, ColHermPacked, Allocator1&gt;&amp; A,
<a name="l01090"></a>01090                                   Matrix&lt;complex&lt;double&gt;,
<a name="l01091"></a>01091                                   Prop2, ColHermPacked, Allocator2&gt;&amp; B,
<a name="l01092"></a>01092                                   Vector&lt;double, VectFull, Allocator3&gt;&amp; w,
<a name="l01093"></a>01093                                   Matrix&lt;complex&lt;double&gt;,
<a name="l01094"></a>01094                                   General, ColMajor, Allocator4&gt;&amp; z,
<a name="l01095"></a>01095                                   LapackInfo&amp; info = lapack_info);
<a name="l01096"></a>01096 
<a name="l01097"></a>01097 
<a name="l01098"></a>01098   <span class="comment">/* RowMajor */</span>
<a name="l01099"></a>01099 
<a name="l01100"></a>01100 
<a name="l01101"></a>01101   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l01102"></a>01102            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3,
<a name="l01103"></a>01103            <span class="keyword">class </span>Allocator4, <span class="keyword">class </span>Allocator5&gt;
<a name="l01104"></a>01104   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;float, Prop1, RowMajor, Allocator1&gt;&amp; A,
<a name="l01105"></a>01105                       Matrix&lt;float, Prop2, RowMajor, Allocator2&gt;&amp; B,
<a name="l01106"></a>01106                       Vector&lt;float, VectFull, Allocator3&gt;&amp; alpha_real,
<a name="l01107"></a>01107                       Vector&lt;float, VectFull, Allocator4&gt;&amp; alpha_imag,
<a name="l01108"></a>01108                       Vector&lt;float, VectFull, Allocator5&gt;&amp; beta,
<a name="l01109"></a>01109                       LapackInfo&amp; info = lapack_info);
<a name="l01110"></a>01110 
<a name="l01111"></a>01111 
<a name="l01112"></a>01112   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Prop3, <span class="keyword">class </span>Allocator1,
<a name="l01113"></a>01113            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4,
<a name="l01114"></a>01114            <span class="keyword">class </span>Allocator5, <span class="keyword">class </span>Allocator6&gt;
<a name="l01115"></a>01115   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;float, Prop1, RowMajor, Allocator1&gt;&amp; A,
<a name="l01116"></a>01116                                   Matrix&lt;float, Prop2, RowMajor, Allocator2&gt;&amp; B,
<a name="l01117"></a>01117                                   Vector&lt;float, VectFull, Allocator3&gt;&amp; alpha_real,
<a name="l01118"></a>01118                                   Vector&lt;float, VectFull, Allocator4&gt;&amp; alpha_imag,
<a name="l01119"></a>01119                                   Vector&lt;float, VectFull, Allocator5&gt;&amp; beta,
<a name="l01120"></a>01120                                   Matrix&lt;float, Prop3, RowMajor, Allocator6&gt;&amp; V,
<a name="l01121"></a>01121                                   LapackInfo&amp; info = lapack_info);
<a name="l01122"></a>01122 
<a name="l01123"></a>01123 
<a name="l01124"></a>01124   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l01125"></a>01125            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator4, <span class="keyword">class </span>Allocator5&gt;
<a name="l01126"></a>01126   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;float&gt;, Prop1, RowMajor, Allocator1&gt;&amp; A,
<a name="l01127"></a>01127                       Matrix&lt;complex&lt;float&gt;, Prop2, RowMajor, Allocator2&gt;&amp; B,
<a name="l01128"></a>01128                       Vector&lt;complex&lt;float&gt;, VectFull, Allocator4&gt;&amp; alpha,
<a name="l01129"></a>01129                       Vector&lt;complex&lt;float&gt;, VectFull, Allocator5&gt;&amp; beta,
<a name="l01130"></a>01130                       LapackInfo&amp; info = lapack_info);
<a name="l01131"></a>01131 
<a name="l01132"></a>01132 
<a name="l01133"></a>01133   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Prop3, <span class="keyword">class </span>Allocator1,
<a name="l01134"></a>01134            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator4,
<a name="l01135"></a>01135            <span class="keyword">class </span>Allocator5, <span class="keyword">class </span>Allocator6&gt;
<a name="l01136"></a>01136   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;float&gt;,
<a name="l01137"></a>01137                                   Prop1, RowMajor, Allocator1&gt;&amp; A,
<a name="l01138"></a>01138                                   Matrix&lt;complex&lt;float&gt;,
<a name="l01139"></a>01139                                   Prop2, RowMajor, Allocator2&gt;&amp; B,
<a name="l01140"></a>01140                                   Vector&lt;complex&lt;float&gt;,
<a name="l01141"></a>01141                                   VectFull, Allocator4&gt;&amp; alpha,
<a name="l01142"></a>01142                                   Vector&lt;complex&lt;float&gt;,
<a name="l01143"></a>01143                                   VectFull, Allocator5&gt;&amp; beta,
<a name="l01144"></a>01144                                   Matrix&lt;complex&lt;float&gt;,
<a name="l01145"></a>01145                                   Prop3, RowMajor, Allocator6&gt;&amp; V,
<a name="l01146"></a>01146                                   LapackInfo&amp; info = lapack_info);
<a name="l01147"></a>01147 
<a name="l01148"></a>01148 
<a name="l01149"></a>01149   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l01150"></a>01150            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3,
<a name="l01151"></a>01151            <span class="keyword">class </span>Allocator4, <span class="keyword">class </span>Allocator5&gt;
<a name="l01152"></a>01152   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;double, Prop1, RowMajor, Allocator1&gt;&amp; A,
<a name="l01153"></a>01153                       Matrix&lt;double, Prop2, RowMajor, Allocator2&gt;&amp; B,
<a name="l01154"></a>01154                       Vector&lt;double, VectFull, Allocator3&gt;&amp; alpha_real,
<a name="l01155"></a>01155                       Vector&lt;double, VectFull, Allocator4&gt;&amp; alpha_imag,
<a name="l01156"></a>01156                       Vector&lt;double, VectFull, Allocator5&gt;&amp; beta,
<a name="l01157"></a>01157                       LapackInfo&amp; info = lapack_info);
<a name="l01158"></a>01158 
<a name="l01159"></a>01159 
<a name="l01160"></a>01160   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Prop3, <span class="keyword">class </span>Allocator1,
<a name="l01161"></a>01161            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4,
<a name="l01162"></a>01162            <span class="keyword">class </span>Allocator5, <span class="keyword">class </span>Allocator6&gt;
<a name="l01163"></a>01163   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;double, Prop1, RowMajor, Allocator1&gt;&amp; A,
<a name="l01164"></a>01164                                   Matrix&lt;double, Prop2, RowMajor, Allocator2&gt;&amp; B,
<a name="l01165"></a>01165                                   Vector&lt;double, VectFull, Allocator3&gt;&amp; alpha_real,
<a name="l01166"></a>01166                                   Vector&lt;double, VectFull, Allocator4&gt;&amp; alpha_imag,
<a name="l01167"></a>01167                                   Vector&lt;double, VectFull, Allocator5&gt;&amp; beta,
<a name="l01168"></a>01168                                   Matrix&lt;double, Prop3, RowMajor, Allocator6&gt;&amp; V,
<a name="l01169"></a>01169                                   LapackInfo&amp; info = lapack_info);
<a name="l01170"></a>01170 
<a name="l01171"></a>01171 
<a name="l01172"></a>01172   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l01173"></a>01173            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator4, <span class="keyword">class </span>Allocator5&gt;
<a name="l01174"></a>01174   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;double&gt;, Prop1, RowMajor, Allocator1&gt;&amp; A,
<a name="l01175"></a>01175                       Matrix&lt;complex&lt;double&gt;, Prop2, RowMajor, Allocator2&gt;&amp; B,
<a name="l01176"></a>01176                       Vector&lt;complex&lt;double&gt;, VectFull, Allocator4&gt;&amp; alpha,
<a name="l01177"></a>01177                       Vector&lt;complex&lt;double&gt;, VectFull, Allocator5&gt;&amp; beta,
<a name="l01178"></a>01178                       LapackInfo&amp; info = lapack_info);
<a name="l01179"></a>01179 
<a name="l01180"></a>01180 
<a name="l01181"></a>01181   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Prop3, <span class="keyword">class </span>Allocator1,
<a name="l01182"></a>01182            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator4,
<a name="l01183"></a>01183            <span class="keyword">class </span>Allocator5, <span class="keyword">class </span>Allocator6&gt;
<a name="l01184"></a>01184   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;double&gt;,
<a name="l01185"></a>01185                                   Prop1, RowMajor, Allocator1&gt;&amp; A,
<a name="l01186"></a>01186                                   Matrix&lt;complex&lt;double&gt;,
<a name="l01187"></a>01187                                   Prop2, RowMajor, Allocator2&gt;&amp; B,
<a name="l01188"></a>01188                                   Vector&lt;complex&lt;double&gt;,
<a name="l01189"></a>01189                                   VectFull, Allocator4&gt;&amp; alpha,
<a name="l01190"></a>01190                                   Vector&lt;complex&lt;double&gt;,
<a name="l01191"></a>01191                                   VectFull, Allocator5&gt;&amp; beta,
<a name="l01192"></a>01192                                   Matrix&lt;complex&lt;double&gt;,
<a name="l01193"></a>01193                                   Prop3, RowMajor, Allocator6&gt;&amp; V,
<a name="l01194"></a>01194                                   LapackInfo&amp; info = lapack_info);
<a name="l01195"></a>01195 
<a name="l01196"></a>01196 
<a name="l01197"></a>01197   <span class="comment">/* ColMajor */</span>
<a name="l01198"></a>01198 
<a name="l01199"></a>01199 
<a name="l01200"></a>01200   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l01201"></a>01201            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3,
<a name="l01202"></a>01202            <span class="keyword">class </span>Allocator4, <span class="keyword">class </span>Allocator5&gt;
<a name="l01203"></a>01203   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;float, Prop1, ColMajor, Allocator1&gt;&amp; A,
<a name="l01204"></a>01204                       Matrix&lt;float, Prop2, ColMajor, Allocator2&gt;&amp; B,
<a name="l01205"></a>01205                       Vector&lt;float, VectFull, Allocator3&gt;&amp; alpha_real,
<a name="l01206"></a>01206                       Vector&lt;float, VectFull, Allocator4&gt;&amp; alpha_imag,
<a name="l01207"></a>01207                       Vector&lt;float, VectFull, Allocator5&gt;&amp; beta,
<a name="l01208"></a>01208                       LapackInfo&amp; info = lapack_info);
<a name="l01209"></a>01209 
<a name="l01210"></a>01210 
<a name="l01211"></a>01211   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Prop3, <span class="keyword">class </span>Allocator1,
<a name="l01212"></a>01212            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4,
<a name="l01213"></a>01213            <span class="keyword">class </span>Allocator5, <span class="keyword">class </span>Allocator6&gt;
<a name="l01214"></a>01214   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;float, Prop1, ColMajor, Allocator1&gt;&amp; A,
<a name="l01215"></a>01215                                   Matrix&lt;float, Prop2, ColMajor, Allocator2&gt;&amp; B,
<a name="l01216"></a>01216                                   Vector&lt;<span class="keywordtype">float</span>,
<a name="l01217"></a>01217                                   VectFull, Allocator3&gt;&amp; alpha_real,
<a name="l01218"></a>01218                                   Vector&lt;<span class="keywordtype">float</span>,
<a name="l01219"></a>01219                                   VectFull, Allocator4&gt;&amp; alpha_imag,
<a name="l01220"></a>01220                                   Vector&lt;float, VectFull, Allocator5&gt;&amp; beta,
<a name="l01221"></a>01221                                   Matrix&lt;float, Prop3, ColMajor, Allocator6&gt;&amp; V,
<a name="l01222"></a>01222                                   LapackInfo&amp; info = lapack_info);
<a name="l01223"></a>01223 
<a name="l01224"></a>01224 
<a name="l01225"></a>01225   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l01226"></a>01226            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator4, <span class="keyword">class </span>Allocator5&gt;
<a name="l01227"></a>01227   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;float&gt;, Prop1, ColMajor, Allocator1&gt;&amp; A,
<a name="l01228"></a>01228                       Matrix&lt;complex&lt;float&gt;, Prop2, ColMajor, Allocator2&gt;&amp; B,
<a name="l01229"></a>01229                       Vector&lt;complex&lt;float&gt;, VectFull, Allocator4&gt;&amp; alpha,
<a name="l01230"></a>01230                       Vector&lt;complex&lt;float&gt;, VectFull, Allocator5&gt;&amp; beta,
<a name="l01231"></a>01231                       LapackInfo&amp; info = lapack_info);
<a name="l01232"></a>01232 
<a name="l01233"></a>01233 
<a name="l01234"></a>01234   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Prop3, <span class="keyword">class </span>Allocator1,
<a name="l01235"></a>01235            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator4,
<a name="l01236"></a>01236            <span class="keyword">class </span>Allocator5, <span class="keyword">class </span>Allocator6&gt;
<a name="l01237"></a>01237   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;float&gt;,
<a name="l01238"></a>01238                                   Prop1, ColMajor, Allocator1&gt;&amp; A,
<a name="l01239"></a>01239                                   Matrix&lt;complex&lt;float&gt;,
<a name="l01240"></a>01240                                   Prop2, ColMajor, Allocator2&gt;&amp; B,
<a name="l01241"></a>01241                                   Vector&lt;complex&lt;float&gt;,
<a name="l01242"></a>01242                                   VectFull, Allocator4&gt;&amp; alpha,
<a name="l01243"></a>01243                                   Vector&lt;complex&lt;float&gt;,
<a name="l01244"></a>01244                                   VectFull, Allocator5&gt;&amp; beta,
<a name="l01245"></a>01245                                   Matrix&lt;complex&lt;float&gt;,
<a name="l01246"></a>01246                                   Prop3, ColMajor, Allocator6&gt;&amp; V,
<a name="l01247"></a>01247                                   LapackInfo&amp; info = lapack_info);
<a name="l01248"></a>01248 
<a name="l01249"></a>01249 
<a name="l01250"></a>01250   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l01251"></a>01251            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3,
<a name="l01252"></a>01252            <span class="keyword">class </span>Allocator4, <span class="keyword">class </span>Allocator5&gt;
<a name="l01253"></a>01253   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;double, Prop1, ColMajor, Allocator1&gt;&amp; A,
<a name="l01254"></a>01254                       Matrix&lt;double, Prop2, ColMajor, Allocator2&gt;&amp; B,
<a name="l01255"></a>01255                       Vector&lt;double, VectFull, Allocator3&gt;&amp; alpha_real,
<a name="l01256"></a>01256                       Vector&lt;double, VectFull, Allocator4&gt;&amp; alpha_imag,
<a name="l01257"></a>01257                       Vector&lt;double, VectFull, Allocator5&gt;&amp; beta,
<a name="l01258"></a>01258                       LapackInfo&amp; info = lapack_info);
<a name="l01259"></a>01259 
<a name="l01260"></a>01260 
<a name="l01261"></a>01261   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Prop3, <span class="keyword">class </span>Allocator1,
<a name="l01262"></a>01262            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4,
<a name="l01263"></a>01263            <span class="keyword">class </span>Allocator5, <span class="keyword">class </span>Allocator6&gt;
<a name="l01264"></a>01264   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;double, Prop1, ColMajor, Allocator1&gt;&amp; A,
<a name="l01265"></a>01265                                   Matrix&lt;double, Prop2, ColMajor, Allocator2&gt;&amp; B,
<a name="l01266"></a>01266                                   Vector&lt;<span class="keywordtype">double</span>,
<a name="l01267"></a>01267                                   VectFull, Allocator3&gt;&amp; alpha_real,
<a name="l01268"></a>01268                                   Vector&lt;<span class="keywordtype">double</span>,
<a name="l01269"></a>01269                                   VectFull, Allocator4&gt;&amp; alpha_imag,
<a name="l01270"></a>01270                                   Vector&lt;double, VectFull, Allocator5&gt;&amp; beta,
<a name="l01271"></a>01271                                   Matrix&lt;double, Prop3, ColMajor, Allocator6&gt;&amp; V,
<a name="l01272"></a>01272                                   LapackInfo&amp; info = lapack_info);
<a name="l01273"></a>01273 
<a name="l01274"></a>01274 
<a name="l01275"></a>01275   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator1,
<a name="l01276"></a>01276            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator4, <span class="keyword">class </span>Allocator5&gt;
<a name="l01277"></a>01277   <span class="keywordtype">void</span> GetEigenvalues(Matrix&lt;complex&lt;double&gt;, Prop1, ColMajor, Allocator1&gt;&amp; A,
<a name="l01278"></a>01278                       Matrix&lt;complex&lt;double&gt;, Prop2, ColMajor, Allocator2&gt;&amp; B,
<a name="l01279"></a>01279                       Vector&lt;complex&lt;double&gt;, VectFull, Allocator4&gt;&amp; alpha,
<a name="l01280"></a>01280                       Vector&lt;complex&lt;double&gt;, VectFull, Allocator5&gt;&amp; beta,
<a name="l01281"></a>01281                       LapackInfo&amp; info = lapack_info);
<a name="l01282"></a>01282 
<a name="l01283"></a>01283 
<a name="l01284"></a>01284   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Prop3, <span class="keyword">class </span>Allocator1,
<a name="l01285"></a>01285            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator4,
<a name="l01286"></a>01286            <span class="keyword">class </span>Allocator5, <span class="keyword">class </span>Allocator6&gt;
<a name="l01287"></a>01287   <span class="keywordtype">void</span> GetEigenvaluesEigenvectors(Matrix&lt;complex&lt;double&gt;,
<a name="l01288"></a>01288                                   Prop1, ColMajor, Allocator1&gt;&amp; A,
<a name="l01289"></a>01289                                   Matrix&lt;complex&lt;double&gt;,
<a name="l01290"></a>01290                                   Prop2, ColMajor, Allocator2&gt;&amp; B,
<a name="l01291"></a>01291                                   Vector&lt;complex&lt;double&gt;,
<a name="l01292"></a>01292                                   VectFull, Allocator4&gt;&amp; alpha,
<a name="l01293"></a>01293                                   Vector&lt;complex&lt;double&gt;,
<a name="l01294"></a>01294                                   VectFull, Allocator5&gt;&amp; beta,
<a name="l01295"></a>01295                                   Matrix&lt;complex&lt;double&gt;,
<a name="l01296"></a>01296                                   Prop3, ColMajor, Allocator6&gt;&amp; V,
<a name="l01297"></a>01297                                   LapackInfo&amp; info = lapack_info);
<a name="l01298"></a>01298 
<a name="l01299"></a>01299 
<a name="l01300"></a>01300   <span class="comment">// GENERALIZED EIGENVALUE PROBLEM //</span>
<a name="l01302"></a>01302 <span class="comment"></span>
<a name="l01303"></a>01303 
<a name="l01305"></a>01305   <span class="comment">// SINGULAR VALUE DECOMPOSITION //</span>
<a name="l01306"></a>01306 
<a name="l01307"></a>01307 
<a name="l01308"></a>01308   <span class="comment">/* RowMajor */</span>
<a name="l01309"></a>01309 
<a name="l01310"></a>01310 
<a name="l01311"></a>01311   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l01312"></a>01312            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l01313"></a>01313   <span class="keywordtype">void</span> GetSVD(Matrix&lt;float, Prop1, RowMajor, Allocator1&gt;&amp; A,
<a name="l01314"></a>01314               Vector&lt;float, VectFull, Allocator4&gt;&amp; lambda,
<a name="l01315"></a>01315               Matrix&lt;float, General, RowMajor, Allocator2&gt;&amp; u,
<a name="l01316"></a>01316               Matrix&lt;float, General, RowMajor, Allocator3&gt;&amp; v,
<a name="l01317"></a>01317               LapackInfo&amp; info = lapack_info);
<a name="l01318"></a>01318 
<a name="l01319"></a>01319 
<a name="l01320"></a>01320   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l01321"></a>01321            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l01322"></a>01322   <span class="keywordtype">void</span> GetSVD(Matrix&lt;complex&lt;float&gt;, Prop1, RowMajor, Allocator1&gt;&amp; A,
<a name="l01323"></a>01323               Vector&lt;float, VectFull, Allocator4&gt;&amp; lambda,
<a name="l01324"></a>01324               Matrix&lt;complex&lt;float&gt;, General, RowMajor, Allocator2&gt;&amp; u,
<a name="l01325"></a>01325               Matrix&lt;complex&lt;float&gt;, General, RowMajor, Allocator3&gt;&amp; v,
<a name="l01326"></a>01326               LapackInfo&amp; info = lapack_info);
<a name="l01327"></a>01327 
<a name="l01328"></a>01328 
<a name="l01329"></a>01329   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l01330"></a>01330            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l01331"></a>01331   <span class="keywordtype">void</span> GetSVD(Matrix&lt;double, Prop1, RowMajor, Allocator1&gt;&amp; A,
<a name="l01332"></a>01332               Vector&lt;double, VectFull, Allocator4&gt;&amp; lambda,
<a name="l01333"></a>01333               Matrix&lt;double, General, RowMajor, Allocator2&gt;&amp; u,
<a name="l01334"></a>01334               Matrix&lt;double, General, RowMajor, Allocator3&gt;&amp; v,
<a name="l01335"></a>01335               LapackInfo&amp; info = lapack_info);
<a name="l01336"></a>01336 
<a name="l01337"></a>01337 
<a name="l01338"></a>01338   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l01339"></a>01339            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l01340"></a>01340   <span class="keywordtype">void</span> GetSVD(Matrix&lt;complex&lt;double&gt;, Prop1, RowMajor, Allocator1&gt;&amp; A,
<a name="l01341"></a>01341               Vector&lt;double, VectFull, Allocator4&gt;&amp; lambda,
<a name="l01342"></a>01342               Matrix&lt;complex&lt;double&gt;, General, RowMajor, Allocator2&gt;&amp; u,
<a name="l01343"></a>01343               Matrix&lt;complex&lt;double&gt;, General, RowMajor, Allocator3&gt;&amp; v,
<a name="l01344"></a>01344               LapackInfo&amp; info = lapack_info);
<a name="l01345"></a>01345 
<a name="l01346"></a>01346 
<a name="l01347"></a>01347   <span class="comment">/* ColMajor */</span>
<a name="l01348"></a>01348 
<a name="l01349"></a>01349 
<a name="l01350"></a>01350   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l01351"></a>01351            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l01352"></a>01352   <span class="keywordtype">void</span> GetSVD(Matrix&lt;float, Prop1, ColMajor, Allocator1&gt;&amp; A,
<a name="l01353"></a>01353               Vector&lt;float, VectFull, Allocator4&gt;&amp; lambda,
<a name="l01354"></a>01354               Matrix&lt;float, General, ColMajor, Allocator2&gt;&amp; u,
<a name="l01355"></a>01355               Matrix&lt;float, General, ColMajor, Allocator3&gt;&amp; v,
<a name="l01356"></a>01356               LapackInfo&amp; info = lapack_info);
<a name="l01357"></a>01357 
<a name="l01358"></a>01358 
<a name="l01359"></a>01359   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l01360"></a>01360            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l01361"></a>01361   <span class="keywordtype">void</span> GetSVD(Matrix&lt;complex&lt;float&gt;, Prop1, ColMajor, Allocator1&gt;&amp; A,
<a name="l01362"></a>01362               Vector&lt;float, VectFull, Allocator4&gt;&amp; lambda,
<a name="l01363"></a>01363               Matrix&lt;complex&lt;float&gt;, General, ColMajor, Allocator2&gt;&amp; u,
<a name="l01364"></a>01364               Matrix&lt;complex&lt;float&gt;, General, ColMajor, Allocator3&gt;&amp; v,
<a name="l01365"></a>01365               LapackInfo&amp; info = lapack_info);
<a name="l01366"></a>01366 
<a name="l01367"></a>01367 
<a name="l01368"></a>01368   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l01369"></a>01369            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l01370"></a>01370   <span class="keywordtype">void</span> GetSVD(Matrix&lt;double, Prop1, ColMajor, Allocator1&gt;&amp; A,
<a name="l01371"></a>01371               Vector&lt;double, VectFull, Allocator4&gt;&amp; lambda,
<a name="l01372"></a>01372               Matrix&lt;double, General, ColMajor, Allocator2&gt;&amp; u,
<a name="l01373"></a>01373               Matrix&lt;double, General, ColMajor, Allocator3&gt;&amp; v,
<a name="l01374"></a>01374               LapackInfo&amp; info = lapack_info);
<a name="l01375"></a>01375 
<a name="l01376"></a>01376 
<a name="l01377"></a>01377   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2,
<a name="l01378"></a>01378            <span class="keyword">class </span>Allocator3, <span class="keyword">class </span>Allocator4&gt;
<a name="l01379"></a>01379   <span class="keywordtype">void</span> GetSVD(Matrix&lt;complex&lt;double&gt;, Prop1, ColMajor, Allocator1&gt;&amp; A,
<a name="l01380"></a>01380               Vector&lt;double, VectFull, Allocator4&gt;&amp; sigma,
<a name="l01381"></a>01381               Matrix&lt;complex&lt;double&gt;, General, ColMajor, Allocator2&gt;&amp; u,
<a name="l01382"></a>01382               Matrix&lt;complex&lt;double&gt;, General, ColMajor, Allocator3&gt;&amp; v,
<a name="l01383"></a>01383               LapackInfo&amp; info = lapack_info);
<a name="l01384"></a>01384 
<a name="l01385"></a>01385 
<a name="l01386"></a>01386   <span class="comment">// pseudo inverse</span>
<a name="l01387"></a>01387   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop1, <span class="keyword">class</span> Allocator1&gt;
<a name="l01388"></a>01388   <span class="keywordtype">void</span> GetPseudoInverse(Matrix&lt;double, Prop1, ColMajor, Allocator1&gt;&amp; A,
<a name="l01389"></a>01389                         <span class="keywordtype">double</span> epsilon, LapackInfo&amp; info = lapack_info);
<a name="l01390"></a>01390 
<a name="l01391"></a>01391 
<a name="l01392"></a>01392   <span class="comment">// SINGULAR VALUE DECOMPOSITION //</span>
<a name="l01394"></a>01394 <span class="comment"></span>
<a name="l01395"></a>01395 
<a name="l01397"></a>01397   <span class="comment">// RESOLUTION SYLVESTER EQUATION //</span>
<a name="l01398"></a>01398 
<a name="l01399"></a>01399 
<a name="l01400"></a>01400   <span class="keywordtype">void</span> GetHessenberg(Matrix&lt;complex&lt;double&gt;, General, ColMajor&gt;&amp; A,
<a name="l01401"></a>01401                      Matrix&lt;complex&lt;double&gt;, General, ColMajor&gt;&amp; B,
<a name="l01402"></a>01402                      Matrix&lt;complex&lt;double&gt;, General, ColMajor&gt;&amp; Q,
<a name="l01403"></a>01403                      Matrix&lt;complex&lt;double&gt;, General, ColMajor&gt;&amp; Z);
<a name="l01404"></a>01404 
<a name="l01405"></a>01405 
<a name="l01406"></a>01406   <span class="keywordtype">void</span> GetQZ(Matrix&lt;complex&lt;double&gt;, General, ColMajor&gt;&amp; A,
<a name="l01407"></a>01407              Matrix&lt;complex&lt;double&gt;, General, ColMajor&gt;&amp; B,
<a name="l01408"></a>01408              Matrix&lt;complex&lt;double&gt;, General, ColMajor&gt;&amp; Q,
<a name="l01409"></a>01409              Matrix&lt;complex&lt;double&gt;, General, ColMajor&gt;&amp; Z);
<a name="l01410"></a>01410 
<a name="l01411"></a>01411 
<a name="l01412"></a>01412   <span class="keywordtype">void</span> GetHessenberg(Matrix&lt;complex&lt;double&gt;, General, RowMajor&gt;&amp; A,
<a name="l01413"></a>01413                      Matrix&lt;complex&lt;double&gt;, General, RowMajor&gt;&amp; B,
<a name="l01414"></a>01414                      Matrix&lt;complex&lt;double&gt;, General, RowMajor&gt;&amp; Q,
<a name="l01415"></a>01415                      Matrix&lt;complex&lt;double&gt;, General, RowMajor&gt;&amp; Z);
<a name="l01416"></a>01416 
<a name="l01417"></a>01417 
<a name="l01418"></a>01418   <span class="keywordtype">void</span> GetQZ(Matrix&lt;complex&lt;double&gt;, General, RowMajor&gt;&amp; A,
<a name="l01419"></a>01419              Matrix&lt;complex&lt;double&gt;, General, RowMajor&gt;&amp; B,
<a name="l01420"></a>01420              Matrix&lt;complex&lt;double&gt;, General, RowMajor&gt;&amp; Q,
<a name="l01421"></a>01421              Matrix&lt;complex&lt;double&gt;, General, RowMajor&gt;&amp; Z);
<a name="l01422"></a>01422 
<a name="l01423"></a>01423 
<a name="l01425"></a>01425   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator, <span class="keyword">class</span> Vector1&gt;
<a name="l01426"></a>01426   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#adf7ec49ef745f42bdf79df966e7b32fe" title="Gaussian elimination to solve A X = B with A an Hessenberg matrix.">SolveHessenberg</a>(Matrix&lt;T, Prop, Storage, Allocator&gt;&amp; A, Vector1&amp; B);
<a name="l01427"></a>01427 
<a name="l01428"></a>01428 
<a name="l01431"></a>01431   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator, <span class="keyword">class</span> Vector1&gt;
<a name="l01432"></a>01432   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a843b227c9a3d11624bce323a15ee5554" title="Gaussian elimination to solve A X = B with A matrix so that a_ij = 0 for i &amp;gt; j+2...">SolveHessenbergTwo</a>(Matrix&lt;T, Prop, Storage, Allocator&gt;&amp; A, Vector1&amp; B);
<a name="l01433"></a>01433 
<a name="l01434"></a>01434 
<a name="l01437"></a>01437   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01438"></a>01438   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aa0526895b7a4577316ede5d334d47756" title="Solves A X B^H + C X D^H = E, E is overwritten with result X. A, B, C and D are modified...">SolveSylvester</a>(Matrix&lt;complex&lt;double&gt;, Prop, Storage, Allocator&gt;&amp; A,
<a name="l01439"></a>01439                       Matrix&lt;complex&lt;double&gt;, Prop, Storage, Allocator&gt;&amp; B,
<a name="l01440"></a>01440                       Matrix&lt;complex&lt;double&gt;, Prop, Storage, Allocator&gt;&amp; C,
<a name="l01441"></a>01441                       Matrix&lt;complex&lt;double&gt;, Prop, Storage, Allocator&gt;&amp; D,
<a name="l01442"></a>01442                       Matrix&lt;complex&lt;double&gt;, Prop, Storage, Allocator&gt;&amp; E);
<a name="l01443"></a>01443 
<a name="l01444"></a>01444 
<a name="l01445"></a>01445   <span class="keywordtype">void</span> GetHessenberg(Matrix&lt;double, General, ColMajor&gt;&amp; A,
<a name="l01446"></a>01446                      Matrix&lt;double, General, ColMajor&gt;&amp; B,
<a name="l01447"></a>01447                      Matrix&lt;double, General, ColMajor&gt;&amp; Q,
<a name="l01448"></a>01448                      Matrix&lt;double, General, ColMajor&gt;&amp; Z);
<a name="l01449"></a>01449 
<a name="l01450"></a>01450 
<a name="l01451"></a>01451   <span class="keywordtype">void</span> GetQZ(Matrix&lt;double, General, ColMajor&gt;&amp; A,
<a name="l01452"></a>01452              Matrix&lt;double, General, ColMajor&gt;&amp; B,
<a name="l01453"></a>01453              Matrix&lt;double, General, ColMajor&gt;&amp; Q,
<a name="l01454"></a>01454              Matrix&lt;double, General, ColMajor&gt;&amp; Z);
<a name="l01455"></a>01455 
<a name="l01456"></a>01456 
<a name="l01457"></a>01457   <span class="keywordtype">void</span> GetHessenberg(Matrix&lt;double, General, RowMajor&gt;&amp; A,
<a name="l01458"></a>01458                      Matrix&lt;double, General, RowMajor&gt;&amp; B,
<a name="l01459"></a>01459                      Matrix&lt;double, General, RowMajor&gt;&amp; Q,
<a name="l01460"></a>01460                      Matrix&lt;double, General, RowMajor&gt;&amp; Z);
<a name="l01461"></a>01461 
<a name="l01462"></a>01462 
<a name="l01463"></a>01463   <span class="keywordtype">void</span> GetQZ(Matrix&lt;double, General, RowMajor&gt;&amp; A,
<a name="l01464"></a>01464              Matrix&lt;double, General, RowMajor&gt;&amp; B,
<a name="l01465"></a>01465              Matrix&lt;double, General, RowMajor&gt;&amp; Q,
<a name="l01466"></a>01466              Matrix&lt;double, General, RowMajor&gt;&amp; Z);
<a name="l01467"></a>01467 
<a name="l01468"></a>01468 
<a name="l01471"></a>01471   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01472"></a>01472   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aa0526895b7a4577316ede5d334d47756" title="Solves A X B^H + C X D^H = E, E is overwritten with result X. A, B, C and D are modified...">SolveSylvester</a>(Matrix&lt;double, Prop, Storage, Allocator&gt;&amp; A,
<a name="l01473"></a>01473                       Matrix&lt;double, Prop, Storage, Allocator&gt;&amp; B,
<a name="l01474"></a>01474                       Matrix&lt;double, Prop, Storage, Allocator&gt;&amp; C,
<a name="l01475"></a>01475                       Matrix&lt;double, Prop, Storage, Allocator&gt;&amp; D,
<a name="l01476"></a>01476                       Matrix&lt;double, Prop, Storage, Allocator&gt;&amp; E);
<a name="l01477"></a>01477 
<a name="l01478"></a>01478 
<a name="l01479"></a>01479   <span class="comment">// RESOLUTION SYLVESTER EQUATION //</span>
<a name="l01481"></a>01481 <span class="comment"></span>
<a name="l01482"></a>01482 
<a name="l01483"></a>01483 } <span class="comment">// end namespace</span>
<a name="l01484"></a>01484 
<a name="l01485"></a>01485 <span class="preprocessor">#define SELDON_FILE_LAPACK_EIGENVALUES_HXX</span>
<a name="l01486"></a>01486 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
