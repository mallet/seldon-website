<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix/Functions.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2010 INRIA, Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Author(s): Marc Fragu, Vivien Mallet</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_FUNCTIONS_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;Functions.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="preprocessor">#include &quot;../computation/basic_functions/Functions_Vector.cxx&quot;</span>
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 <span class="keyword">namespace </span>Seldon
<a name="l00028"></a>00028 {
<a name="l00029"></a>00029 
<a name="l00030"></a>00030 
<a name="l00032"></a>00032 
<a name="l00037"></a>00037   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> T1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00038"></a>00038   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad8df46dfc8619f16f3a44ac8a42a777a" title="Extracts a row from a sparse matrix.">GetRow</a>(<span class="keyword">const</span> Matrix&lt;T0, General, RowSparse, Allocator0&gt;&amp; M,
<a name="l00039"></a>00039               <span class="keywordtype">int</span> i, Vector&lt;T1, VectSparse, Allocator1&gt;&amp; X)
<a name="l00040"></a>00040   {
<a name="l00041"></a>00041 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00042"></a>00042 <span class="preprocessor"></span>    <span class="keywordtype">int</span> m = M.GetM();
<a name="l00043"></a>00043     <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= m)
<a name="l00044"></a>00044       <span class="keywordflow">throw</span> WrongIndex(<span class="stringliteral">&quot;GetRow()&quot;</span>,
<a name="l00045"></a>00045                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(m - 1)
<a name="l00046"></a>00046                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00047"></a>00047 <span class="preprocessor">#endif</span>
<a name="l00048"></a>00048 <span class="preprocessor"></span>
<a name="l00049"></a>00049     <span class="keywordtype">int</span>* ptr = M.GetPtr();
<a name="l00050"></a>00050     <span class="keywordtype">int</span>* ind = M.GetInd();
<a name="l00051"></a>00051     <span class="keywordtype">double</span>* data = M.GetData();
<a name="l00052"></a>00052     <span class="keywordtype">int</span> size_row = ptr[i+1] - ptr[i];
<a name="l00053"></a>00053 
<a name="l00054"></a>00054     X.Reallocate(size_row);
<a name="l00055"></a>00055     <span class="keywordtype">int</span> shift = ptr[i];
<a name="l00056"></a>00056     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; size_row; j++)
<a name="l00057"></a>00057       {
<a name="l00058"></a>00058         X.Index(j) = ind[shift + j];
<a name="l00059"></a>00059         X.Value(j) = data[shift + j];
<a name="l00060"></a>00060       }
<a name="l00061"></a>00061   }
<a name="l00062"></a>00062 
<a name="l00063"></a>00063 
<a name="l00065"></a>00065 
<a name="l00070"></a>00070   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00071"></a>00071             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00072"></a>00072   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad8df46dfc8619f16f3a44ac8a42a777a" title="Extracts a row from a sparse matrix.">GetRow</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l00073"></a>00073               <span class="keywordtype">int</span> i, Vector&lt;T1, Storage1, Allocator1&gt;&amp; X)
<a name="l00074"></a>00074   {
<a name="l00075"></a>00075     X.Reallocate(M.GetN());
<a name="l00076"></a>00076     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; M.GetN(); j++)
<a name="l00077"></a>00077       X(j) = M(i, j);
<a name="l00078"></a>00078   }
<a name="l00079"></a>00079 
<a name="l00080"></a>00080 
<a name="l00082"></a>00082 
<a name="l00087"></a>00087   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> T1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00088"></a>00088   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#acb182413f85715422b2289a98d2e9379" title="Extracts a column from a matrix.">GetCol</a>(<span class="keyword">const</span> Matrix&lt;T0, General, RowSparse, Allocator0&gt;&amp; M,
<a name="l00089"></a>00089               <span class="keywordtype">int</span> j, Vector&lt;T1, VectSparse, Allocator1&gt;&amp; X)
<a name="l00090"></a>00090   {
<a name="l00091"></a>00091 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00092"></a>00092 <span class="preprocessor"></span>    <span class="keywordtype">int</span> n = M.GetN();
<a name="l00093"></a>00093     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= n)
<a name="l00094"></a>00094       <span class="keywordflow">throw</span> WrongIndex(<span class="stringliteral">&quot;GetCol()&quot;</span>,
<a name="l00095"></a>00095                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(n - 1)
<a name="l00096"></a>00096                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00097"></a>00097 <span class="preprocessor">#endif</span>
<a name="l00098"></a>00098 <span class="preprocessor"></span>
<a name="l00099"></a>00099     <span class="keywordtype">int</span>* ptr = M.GetPtr();
<a name="l00100"></a>00100     <span class="keywordtype">int</span>* ind = M.GetInd();
<a name="l00101"></a>00101     <span class="keywordtype">double</span>* data = M.GetData();
<a name="l00102"></a>00102     <span class="keywordtype">int</span> m = M.GetM();
<a name="l00103"></a>00103     Vector&lt;int&gt; index;
<a name="l00104"></a>00104     Vector&lt;double&gt; value;
<a name="l00105"></a>00105 
<a name="l00106"></a>00106     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l00107"></a>00107       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr[i]; k &lt; ptr[i+1]; k++)
<a name="l00108"></a>00108         <span class="keywordflow">if</span> (ind[k] == j)
<a name="l00109"></a>00109           {
<a name="l00110"></a>00110             index.PushBack(i);
<a name="l00111"></a>00111             value.PushBack(data[k]);
<a name="l00112"></a>00112           }
<a name="l00113"></a>00113 
<a name="l00114"></a>00114     X.SetData(value, index);
<a name="l00115"></a>00115   }
<a name="l00116"></a>00116 
<a name="l00117"></a>00117 
<a name="l00119"></a>00119 
<a name="l00124"></a>00124   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00125"></a>00125             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00126"></a>00126   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#acb182413f85715422b2289a98d2e9379" title="Extracts a column from a matrix.">GetCol</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, PETScSeqDense, Allocator0&gt;&amp; M,
<a name="l00127"></a>00127               <span class="keywordtype">int</span> j, Vector&lt;T1, PETScSeq, Allocator1&gt;&amp; X)
<a name="l00128"></a>00128   {
<a name="l00129"></a>00129     <span class="keywordtype">int</span> a, b;
<a name="l00130"></a>00130     M.GetProcessorRowRange(a, b);
<a name="l00131"></a>00131     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = a; i &lt; b; i++)
<a name="l00132"></a>00132       X.SetBuffer(i, M(i, j));
<a name="l00133"></a>00133     X.Flush();
<a name="l00134"></a>00134   }
<a name="l00135"></a>00135 
<a name="l00136"></a>00136 
<a name="l00138"></a>00138 
<a name="l00143"></a>00143   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00144"></a>00144             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00145"></a>00145   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#acb182413f85715422b2289a98d2e9379" title="Extracts a column from a matrix.">GetCol</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, PETScMPIDense, Allocator0&gt;&amp; M,
<a name="l00146"></a>00146               <span class="keywordtype">int</span> j, Vector&lt;T1, PETScPar, Allocator1&gt;&amp; X)
<a name="l00147"></a>00147   {
<a name="l00148"></a>00148     <span class="keywordtype">int</span> a, b;
<a name="l00149"></a>00149     M.GetProcessorRowRange(a, b);
<a name="l00150"></a>00150     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = a; i &lt; b; i++)
<a name="l00151"></a>00151       X.SetBuffer(i, M(i, j));
<a name="l00152"></a>00152     X.Flush();
<a name="l00153"></a>00153   }
<a name="l00154"></a>00154 
<a name="l00155"></a>00155 
<a name="l00157"></a>00157 
<a name="l00162"></a>00162   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00163"></a>00163             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00164"></a>00164   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#acb182413f85715422b2289a98d2e9379" title="Extracts a column from a matrix.">GetCol</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l00165"></a>00165               <span class="keywordtype">int</span> j, Vector&lt;T1, Storage1, Allocator1&gt;&amp; X)
<a name="l00166"></a>00166   {
<a name="l00167"></a>00167     X.Reallocate(M.GetM());
<a name="l00168"></a>00168     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; M.GetM(); i++)
<a name="l00169"></a>00169       X(i) = M(i, j);
<a name="l00170"></a>00170   }
<a name="l00171"></a>00171 
<a name="l00172"></a>00172 
<a name="l00174"></a>00174 
<a name="l00181"></a>00181   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00182"></a>00182             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00183"></a>00183   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#acb182413f85715422b2289a98d2e9379" title="Extracts a column from a matrix.">GetCol</a>(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M_in,
<a name="l00184"></a>00184               <span class="keywordtype">int</span> begin, <span class="keywordtype">int</span> end,
<a name="l00185"></a>00185               Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;&amp; M_out)
<a name="l00186"></a>00186   {
<a name="l00187"></a>00187     M_out.Reallocate(M_in.GetM(), end - begin);
<a name="l00188"></a>00188     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; M_in.GetM(); i++)
<a name="l00189"></a>00189       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = begin, k = 0; j &lt; end; j++, k++)
<a name="l00190"></a>00190         M_out(i, k) = M_in(i, j);
<a name="l00191"></a>00191   }
<a name="l00192"></a>00192 
<a name="l00193"></a>00193 
<a name="l00195"></a>00195 
<a name="l00200"></a>00200   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00201"></a>00201             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00202"></a>00202   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ae7c5e5e111b10a46344f75f1e8429a88" title="Sets a row of a matrix.">SetRow</a>(<span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00203"></a>00203               <span class="keywordtype">int</span> i, Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M)
<a name="l00204"></a>00204   {
<a name="l00205"></a>00205     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; M.GetN(); j++)
<a name="l00206"></a>00206       M.Set(i, j, X(j));
<a name="l00207"></a>00207   }
<a name="l00208"></a>00208 
<a name="l00209"></a>00209 
<a name="l00211"></a>00211 
<a name="l00216"></a>00216   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00217"></a>00217             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00218"></a>00218   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ae7c5e5e111b10a46344f75f1e8429a88" title="Sets a row of a matrix.">SetRow</a>(<span class="keyword">const</span> Vector&lt;T1, PETScSeq, Allocator1&gt;&amp; X,
<a name="l00219"></a>00219               <span class="keywordtype">int</span> i, Matrix&lt;T0, Prop0, PETScSeqDense, Allocator0&gt;&amp; M)
<a name="l00220"></a>00220   {
<a name="l00221"></a>00221     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; M.GetN(); j++)
<a name="l00222"></a>00222       M.SetBuffer(i, j, X(j));
<a name="l00223"></a>00223     M.Flush();
<a name="l00224"></a>00224   }
<a name="l00225"></a>00225 
<a name="l00226"></a>00226 
<a name="l00228"></a>00228 
<a name="l00233"></a>00233   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00234"></a>00234             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00235"></a>00235   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ae7c5e5e111b10a46344f75f1e8429a88" title="Sets a row of a matrix.">SetRow</a>(<span class="keyword">const</span> Vector&lt;T1, PETScPar, Allocator1&gt;&amp; X,
<a name="l00236"></a>00236               <span class="keywordtype">int</span> i, Matrix&lt;T0, Prop0, PETScMPIDense, Allocator0&gt;&amp; M)
<a name="l00237"></a>00237   {
<a name="l00238"></a>00238     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; M.GetN(); j++)
<a name="l00239"></a>00239       M.SetBuffer(i, j, X(j));
<a name="l00240"></a>00240     M.Flush();
<a name="l00241"></a>00241   }
<a name="l00242"></a>00242 
<a name="l00243"></a>00243 
<a name="l00245"></a>00245 
<a name="l00250"></a>00250   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> T1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00251"></a>00251   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ae7c5e5e111b10a46344f75f1e8429a88" title="Sets a row of a matrix.">SetRow</a>(<span class="keyword">const</span> Vector&lt;T1, VectSparse, Allocator1&gt;&amp; X,
<a name="l00252"></a>00252               <span class="keywordtype">int</span> i, Matrix&lt;T0, General, RowSparse, Allocator0&gt;&amp; M)
<a name="l00253"></a>00253   {
<a name="l00254"></a>00254     <span class="keywordtype">int</span> m = M.GetM();
<a name="l00255"></a>00255     <span class="keywordtype">int</span> n = M.GetN();
<a name="l00256"></a>00256     <span class="keywordtype">int</span> nnz = M.GetDataSize();
<a name="l00257"></a>00257     <span class="keywordtype">int</span> Nx = X.GetSize();
<a name="l00258"></a>00258 
<a name="l00259"></a>00259 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00260"></a>00260 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= m)
<a name="l00261"></a>00261       <span class="keywordflow">throw</span> WrongIndex(<span class="stringliteral">&quot;SetRow(Vector, int, Matrix&lt;RowSparse&gt;)&quot;</span>,
<a name="l00262"></a>00262                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(m - 1)
<a name="l00263"></a>00263                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00264"></a>00264 <span class="preprocessor">#endif</span>
<a name="l00265"></a>00265 <span class="preprocessor"></span>
<a name="l00266"></a>00266     <span class="keywordtype">int</span> *ptr_vector =  M.GetPtr();
<a name="l00267"></a>00267     <span class="keywordtype">int</span> ptr_i0 =  ptr_vector[i], ptr_i1 = ptr_vector[i + 1];
<a name="l00268"></a>00268     <span class="keywordtype">int</span> row_size_difference = Nx - ptr_i1 + ptr_i0;
<a name="l00269"></a>00269 
<a name="l00270"></a>00270     <span class="keywordflow">if</span> (row_size_difference == 0)
<a name="l00271"></a>00271       {
<a name="l00272"></a>00272         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; Nx; k++)
<a name="l00273"></a>00273           M.GetInd()[k + ptr_i0] = X.Index(k);
<a name="l00274"></a>00274         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; Nx; k++)
<a name="l00275"></a>00275           M.GetData()[k + ptr_i0] = X.Value(k);
<a name="l00276"></a>00276         <span class="keywordflow">return</span>;
<a name="l00277"></a>00277       }
<a name="l00278"></a>00278 
<a name="l00279"></a>00279     Vector&lt;int, VectFull, CallocAlloc&lt;int&gt; &gt;
<a name="l00280"></a>00280       new_ind_vector(nnz + row_size_difference);
<a name="l00281"></a>00281     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt;  ptr_i0; k++)
<a name="l00282"></a>00282       new_ind_vector(k) = M.GetInd()[k];
<a name="l00283"></a>00283     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; Nx; k++)
<a name="l00284"></a>00284       new_ind_vector(k + ptr_i0) = X.Index(k);
<a name="l00285"></a>00285     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; nnz - ptr_i1; k++)
<a name="l00286"></a>00286       new_ind_vector(k + ptr_i0 + Nx) =  M.GetInd()[k + ptr_i1];
<a name="l00287"></a>00287 
<a name="l00288"></a>00288     Vector&lt;T1, VectFull, Allocator0 &gt;
<a name="l00289"></a>00289       new_data_vector(nnz + row_size_difference);
<a name="l00290"></a>00290     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt;  ptr_i0; k++)
<a name="l00291"></a>00291       new_data_vector(k) = M.GetData()[k];
<a name="l00292"></a>00292     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; Nx; k++)
<a name="l00293"></a>00293       new_data_vector(k + ptr_i0) = X.Value(k);
<a name="l00294"></a>00294     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; nnz - ptr_i1; k++)
<a name="l00295"></a>00295       new_data_vector(k + ptr_i0 + Nx) =  M.GetData()[k + ptr_i1];
<a name="l00296"></a>00296 
<a name="l00297"></a>00297     Vector&lt;int, VectFull, CallocAlloc&lt;int&gt; &gt; new_ptr_vector(m + 1);
<a name="l00298"></a>00298     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; i + 1; j++)
<a name="l00299"></a>00299       new_ptr_vector(j) = ptr_vector[j];
<a name="l00300"></a>00300     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = i + 1; j &lt; m+1; j++)
<a name="l00301"></a>00301       new_ptr_vector(j) =  ptr_vector[j] + row_size_difference;
<a name="l00302"></a>00302 
<a name="l00303"></a>00303     M.SetData(m, n, new_data_vector, new_ptr_vector, new_ind_vector);
<a name="l00304"></a>00304   }
<a name="l00305"></a>00305 
<a name="l00306"></a>00306 
<a name="l00308"></a>00308 
<a name="l00313"></a>00313   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00314"></a>00314             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00315"></a>00315   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a47489289334cd178af06072aac3e091b" title="Sets a column of a matrix.">SetCol</a>(<span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00316"></a>00316               <span class="keywordtype">int</span> j, Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M)
<a name="l00317"></a>00317   {
<a name="l00318"></a>00318     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; M.GetM(); i++)
<a name="l00319"></a>00319       M.Set(i, j, X(i));
<a name="l00320"></a>00320   }
<a name="l00321"></a>00321 
<a name="l00322"></a>00322 
<a name="l00324"></a>00324 
<a name="l00329"></a>00329   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00330"></a>00330             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00331"></a>00331   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a47489289334cd178af06072aac3e091b" title="Sets a column of a matrix.">SetCol</a>(<span class="keyword">const</span> Vector&lt;T1, PETScSeq, Allocator1&gt;&amp; X,
<a name="l00332"></a>00332               <span class="keywordtype">int</span> j, Matrix&lt;T0, Prop0, PETScSeqDense, Allocator0&gt;&amp; M)
<a name="l00333"></a>00333   {
<a name="l00334"></a>00334     <span class="keywordtype">int</span> a, b;
<a name="l00335"></a>00335     M.GetProcessorRowRange(a, b);
<a name="l00336"></a>00336     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = a; i &lt; b; i++)
<a name="l00337"></a>00337       M.SetBuffer(i, j, X(i));
<a name="l00338"></a>00338     M.Flush();
<a name="l00339"></a>00339   }
<a name="l00340"></a>00340 
<a name="l00341"></a>00341 
<a name="l00343"></a>00343 
<a name="l00348"></a>00348   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00349"></a>00349             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00350"></a>00350   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a47489289334cd178af06072aac3e091b" title="Sets a column of a matrix.">SetCol</a>(<span class="keyword">const</span> Vector&lt;T1, PETScPar, Allocator1&gt;&amp; X,
<a name="l00351"></a>00351               <span class="keywordtype">int</span> j, Matrix&lt;T0, Prop0, PETScMPIDense, Allocator0&gt;&amp; M)
<a name="l00352"></a>00352   {
<a name="l00353"></a>00353     <span class="keywordtype">int</span> a, b;
<a name="l00354"></a>00354     M.GetProcessorRowRange(a, b);
<a name="l00355"></a>00355     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = a; i &lt; b; i++)
<a name="l00356"></a>00356       M.SetBuffer(i, j, X(i));
<a name="l00357"></a>00357     M.Flush();
<a name="l00358"></a>00358   }
<a name="l00359"></a>00359 
<a name="l00360"></a>00360 
<a name="l00362"></a>00362 
<a name="l00367"></a>00367   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Allocator0,
<a name="l00368"></a>00368             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00369"></a>00369   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a47489289334cd178af06072aac3e091b" title="Sets a column of a matrix.">SetCol</a>(<span class="keyword">const</span> Vector&lt;T1, VectFull, Allocator1&gt;&amp; X,
<a name="l00370"></a>00370               <span class="keywordtype">int</span> j, Matrix&lt;T0, General, RowSparse, Allocator0&gt;&amp; M)
<a name="l00371"></a>00371   {
<a name="l00372"></a>00372     Vector&lt;T1, VectSparse, Allocator1&gt; X_sparse;
<a name="l00373"></a>00373     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; X.GetLength(); k++)
<a name="l00374"></a>00374       {
<a name="l00375"></a>00375         T1 value = X(k);
<a name="l00376"></a>00376         <span class="keywordflow">if</span> (value != T1(0.))
<a name="l00377"></a>00377           X_sparse.AddInteraction(k, value);
<a name="l00378"></a>00378       }
<a name="l00379"></a>00379 
<a name="l00380"></a>00380     <a class="code" href="namespace_seldon.php#a47489289334cd178af06072aac3e091b" title="Sets a column of a matrix.">SetCol</a>(X_sparse, j, M);
<a name="l00381"></a>00381   }
<a name="l00382"></a>00382 
<a name="l00383"></a>00383 
<a name="l00385"></a>00385 
<a name="l00390"></a>00390   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> T1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00391"></a>00391   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a47489289334cd178af06072aac3e091b" title="Sets a column of a matrix.">SetCol</a>(<span class="keyword">const</span> Vector&lt;T1, VectSparse, Allocator1&gt;&amp; X,
<a name="l00392"></a>00392               <span class="keywordtype">int</span> j, Matrix&lt;T0, General, RowSparse, Allocator0&gt;&amp; M)
<a name="l00393"></a>00393   {
<a name="l00394"></a>00394     <span class="keywordtype">int</span> m = M.GetM();
<a name="l00395"></a>00395     <span class="keywordtype">int</span> n = M.GetN();
<a name="l00396"></a>00396     <span class="keywordtype">int</span> nnz = M.GetDataSize();
<a name="l00397"></a>00397     <span class="keywordtype">int</span> Nx = X.GetSize();
<a name="l00398"></a>00398 
<a name="l00399"></a>00399 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00400"></a>00400 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= n)
<a name="l00401"></a>00401       <span class="keywordflow">throw</span> WrongIndex(<span class="stringliteral">&quot;SetCol(Vector, int, Matrix&lt;RowSparse&gt;)&quot;</span>,
<a name="l00402"></a>00402                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(n - 1)
<a name="l00403"></a>00403                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00404"></a>00404 <span class="preprocessor">#endif</span>
<a name="l00405"></a>00405 <span class="preprocessor"></span>
<a name="l00406"></a>00406     <span class="comment">// The column to be changed.</span>
<a name="l00407"></a>00407     Vector&lt;T1, VectSparse, Allocator1&gt; column_j;
<a name="l00408"></a>00408     <a class="code" href="namespace_seldon.php#acb182413f85715422b2289a98d2e9379" title="Extracts a column from a matrix.">GetCol</a>(M, j, column_j);
<a name="l00409"></a>00409     <span class="keywordtype">int</span> Ncolumn_j = column_j.GetSize();
<a name="l00410"></a>00410     <span class="keywordtype">int</span> column_size_difference = Nx - Ncolumn_j;
<a name="l00411"></a>00411 
<a name="l00412"></a>00412     <span class="comment">// Built a vector indexed with the rows of column_j and X.</span>
<a name="l00413"></a>00413     Vector&lt;int, VectSparse&gt; column_j_mask;
<a name="l00414"></a>00414     Vector&lt;int&gt; index_j(Ncolumn_j);
<a name="l00415"></a>00415     Vector&lt;int&gt; value_j(Ncolumn_j);
<a name="l00416"></a>00416     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> p = 0; p &lt; Ncolumn_j; p++)
<a name="l00417"></a>00417       index_j(p) = column_j.Index(p);
<a name="l00418"></a>00418     value_j.Fill(-1);
<a name="l00419"></a>00419     column_j_mask.SetData(value_j, index_j);
<a name="l00420"></a>00420     value_j.Nullify();
<a name="l00421"></a>00421     index_j.Nullify();
<a name="l00422"></a>00422     Vector&lt;int, VectSparse&gt; X_mask;
<a name="l00423"></a>00423     Vector&lt;int&gt; index_x(Nx);
<a name="l00424"></a>00424     Vector&lt;int&gt; value_x(Nx);
<a name="l00425"></a>00425     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> p = 0; p &lt; Nx; p++)
<a name="l00426"></a>00426       index_x(p) = X.Index(p);
<a name="l00427"></a>00427     value_x.Fill(1);
<a name="l00428"></a>00428     X_mask.SetData(value_x, index_x);
<a name="l00429"></a>00429     value_x.Nullify();
<a name="l00430"></a>00430     index_x.Nullify();
<a name="l00431"></a>00431     X_mask.AddInteractionRow(column_j_mask.GetSize(),
<a name="l00432"></a>00432                              column_j_mask.GetIndex(),
<a name="l00433"></a>00433                              column_j_mask.GetData(), <span class="keyword">true</span>);
<a name="l00434"></a>00434 
<a name="l00435"></a>00435     <span class="comment">// Built the new pointer vector.</span>
<a name="l00436"></a>00436     Vector&lt;int, VectFull, CallocAlloc&lt;int&gt; &gt; ptr_vector;
<a name="l00437"></a>00437     ptr_vector.SetData(m + 1, M.GetPtr());
<a name="l00438"></a>00438     Vector&lt;int, VectFull, CallocAlloc&lt;int&gt; &gt; new_ptr_vector(m + 1);
<a name="l00439"></a>00439     new_ptr_vector.Zero();
<a name="l00440"></a>00440     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> p = 0; p &lt; X_mask.GetSize(); p++)
<a name="l00441"></a>00441       new_ptr_vector(X_mask.Index(p) + 1) = X_mask.Value(p);
<a name="l00442"></a>00442     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> p = 0; p &lt; m; p++)
<a name="l00443"></a>00443       new_ptr_vector(p + 1) += new_ptr_vector(p);
<a name="l00444"></a>00444 
<a name="l00445"></a>00445     <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(1, ptr_vector, new_ptr_vector);
<a name="l00446"></a>00446 
<a name="l00447"></a>00447     <span class="comment">// Built the new index and the new data vectors row by row.</span>
<a name="l00448"></a>00448     Vector&lt;int, VectFull, CallocAlloc&lt;int&gt; &gt;
<a name="l00449"></a>00449       new_ind_vector(nnz + column_size_difference);
<a name="l00450"></a>00450     Vector&lt;T0, VectFull, Allocator0&gt;
<a name="l00451"></a>00451       new_data_vector(nnz + column_size_difference);
<a name="l00452"></a>00452 
<a name="l00453"></a>00453     Vector&lt;T0, VectSparse, Allocator0&gt; working_vector;
<a name="l00454"></a>00454     <span class="keywordtype">int</span> Nworking_vector;
<a name="l00455"></a>00455 
<a name="l00456"></a>00456     <span class="keywordtype">int</span> line = 0;
<a name="l00457"></a>00457     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> interaction = 0; interaction &lt; X_mask.GetSize(); interaction++)
<a name="l00458"></a>00458       {
<a name="l00459"></a>00459         <span class="keywordtype">int</span> ind_x =  X_mask.Index(interaction);
<a name="l00460"></a>00460         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; ptr_vector(ind_x) -  ptr_vector(line); k++)
<a name="l00461"></a>00461           new_ind_vector.GetData()[k + new_ptr_vector(line)] =
<a name="l00462"></a>00462             M.GetInd()[k + ptr_vector(line)];
<a name="l00463"></a>00463         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; ptr_vector(ind_x) -  ptr_vector(line); k++)
<a name="l00464"></a>00464           new_data_vector.GetData()[k + new_ptr_vector(line)] =
<a name="l00465"></a>00465             M.GetData()[k + ptr_vector(line)];
<a name="l00466"></a>00466 
<a name="l00467"></a>00467         <span class="keywordtype">int</span> ind_j;
<a name="l00468"></a>00468         Nworking_vector = ptr_vector(ind_x + 1) - ptr_vector(ind_x);
<a name="l00469"></a>00469         working_vector.SetData(Nworking_vector,
<a name="l00470"></a>00470                                M.GetData() + ptr_vector(ind_x),
<a name="l00471"></a>00471                                M.GetInd() + ptr_vector(ind_x));
<a name="l00472"></a>00472         <span class="keywordflow">switch</span>(X_mask.Value(interaction))
<a name="l00473"></a>00473           {
<a name="l00474"></a>00474             <span class="comment">// Collision.</span>
<a name="l00475"></a>00475           <span class="keywordflow">case</span> 0:
<a name="l00476"></a>00476             working_vector.Get(j) = X(ind_x);
<a name="l00477"></a>00477             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; Nworking_vector; k++)
<a name="l00478"></a>00478               new_ind_vector.GetData()[k + new_ptr_vector(ind_x)] =
<a name="l00479"></a>00479                 working_vector.GetIndex()[k];
<a name="l00480"></a>00480             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; Nworking_vector; k++)
<a name="l00481"></a>00481               new_data_vector.GetData()[k + new_ptr_vector(ind_x)] =
<a name="l00482"></a>00482                 working_vector.GetData()[k];
<a name="l00483"></a>00483             <span class="keywordflow">break</span>;
<a name="l00484"></a>00484 
<a name="l00485"></a>00485             <span class="comment">// Suppression.</span>
<a name="l00486"></a>00486           <span class="keywordflow">case</span> -1:
<a name="l00487"></a>00487             ind_j = 0;
<a name="l00488"></a>00488             <span class="keywordflow">while</span> (ind_j &lt; Nworking_vector &amp;&amp;
<a name="l00489"></a>00489                    working_vector.Index(ind_j) != j)
<a name="l00490"></a>00490               ind_j++;
<a name="l00491"></a>00491 
<a name="l00492"></a>00492             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; ind_j; k++)
<a name="l00493"></a>00493               new_ind_vector.GetData()[k + new_ptr_vector(ind_x)] =
<a name="l00494"></a>00494                 working_vector.GetIndex()[k];
<a name="l00495"></a>00495             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; Nworking_vector - ind_j - 1; k++)
<a name="l00496"></a>00496               new_ind_vector.GetData()[k + new_ptr_vector(ind_x) + ind_j] =
<a name="l00497"></a>00497                 working_vector.GetIndex()[k + ind_j + 1];
<a name="l00498"></a>00498 
<a name="l00499"></a>00499             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; ind_j; k++)
<a name="l00500"></a>00500               new_data_vector.GetData()[k + new_ptr_vector(ind_x)] =
<a name="l00501"></a>00501                 working_vector.GetData()[k];
<a name="l00502"></a>00502             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; Nworking_vector - ind_j - 1; k++)
<a name="l00503"></a>00503               new_data_vector.GetData()[k + new_ptr_vector(ind_x) + ind_j] =
<a name="l00504"></a>00504                 working_vector.GetData()[k + ind_j + 1];
<a name="l00505"></a>00505             <span class="keywordflow">break</span>;
<a name="l00506"></a>00506 
<a name="l00507"></a>00507             <span class="comment">// Addition.</span>
<a name="l00508"></a>00508           <span class="keywordflow">case</span> 1:
<a name="l00509"></a>00509             ind_j = 0;
<a name="l00510"></a>00510             <span class="keywordflow">while</span> (ind_j &lt; Nworking_vector &amp;&amp;
<a name="l00511"></a>00511                    working_vector.Index(ind_j) &lt; j)
<a name="l00512"></a>00512               ind_j++;
<a name="l00513"></a>00513             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; ind_j; k++)
<a name="l00514"></a>00514               new_ind_vector.GetData()[k + new_ptr_vector(ind_x)] =
<a name="l00515"></a>00515                 working_vector.GetIndex()[k];
<a name="l00516"></a>00516             new_ind_vector.GetData()[new_ptr_vector(ind_x) + ind_j] = j;
<a name="l00517"></a>00517             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; Nworking_vector - ind_j; k++)
<a name="l00518"></a>00518               new_ind_vector.GetData()[k + new_ptr_vector(ind_x) + ind_j + 1]
<a name="l00519"></a>00519                 = working_vector.GetIndex()[k + ind_j];
<a name="l00520"></a>00520 
<a name="l00521"></a>00521             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; ind_j; k++)
<a name="l00522"></a>00522               new_data_vector.GetData()[k + new_ptr_vector(ind_x)] =
<a name="l00523"></a>00523                 working_vector.GetData()[k];
<a name="l00524"></a>00524             new_data_vector.GetData()[new_ptr_vector(ind_x)  + ind_j]
<a name="l00525"></a>00525               = X(ind_x);
<a name="l00526"></a>00526             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; Nworking_vector - ind_j; k++)
<a name="l00527"></a>00527               new_data_vector.GetData()[k + new_ptr_vector(ind_x) + ind_j + 1]
<a name="l00528"></a>00528                 = working_vector.GetData()[k + ind_j];
<a name="l00529"></a>00529           }
<a name="l00530"></a>00530 
<a name="l00531"></a>00531         line = ind_x + 1;
<a name="l00532"></a>00532         working_vector.Nullify();
<a name="l00533"></a>00533       }
<a name="l00534"></a>00534     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; ptr_vector(m) -  ptr_vector(line); k++)
<a name="l00535"></a>00535       new_ind_vector.GetData()[k + new_ptr_vector(line)] =
<a name="l00536"></a>00536         M.GetInd()[k + ptr_vector(line)];
<a name="l00537"></a>00537     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; ptr_vector(m) -  ptr_vector(line); k++)
<a name="l00538"></a>00538       new_data_vector.GetData()[k + new_ptr_vector(line)] =
<a name="l00539"></a>00539         M.GetData()[k + ptr_vector(line)];
<a name="l00540"></a>00540 
<a name="l00541"></a>00541     M.SetData(m, n, new_data_vector, new_ptr_vector, new_ind_vector);
<a name="l00542"></a>00542     ptr_vector.Nullify();
<a name="l00543"></a>00543     new_data_vector.Nullify();
<a name="l00544"></a>00544     new_ind_vector.Nullify();
<a name="l00545"></a>00545     new_ptr_vector.Nullify();
<a name="l00546"></a>00546   }
<a name="l00547"></a>00547 
<a name="l00548"></a>00548 
<a name="l00550"></a>00550 
<a name="l00553"></a>00553   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00554"></a>00554   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a48069e460ec162c2f6fef6b0ee2e13ec" title="Permutation of a general matrix stored by rows.">ApplyPermutation</a>(Matrix&lt;T, Prop, RowMajor, Allocator&gt;&amp; A,
<a name="l00555"></a>00555                         <span class="keyword">const</span> Vector&lt;int&gt;&amp; row_perm,
<a name="l00556"></a>00556                         <span class="keyword">const</span> Vector&lt;int&gt;&amp; col_perm,
<a name="l00557"></a>00557                         <span class="keywordtype">int</span> starting_index)
<a name="l00558"></a>00558   {
<a name="l00559"></a>00559     Matrix&lt;T, Prop, RowMajor, Allocator&gt; A_copy = A;
<a name="l00560"></a>00560 
<a name="l00561"></a>00561     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetM(); i++)
<a name="l00562"></a>00562       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetN(); j++)
<a name="l00563"></a>00563         A(i, j) = A_copy(row_perm(i) - starting_index,
<a name="l00564"></a>00564                          col_perm(j) - starting_index);
<a name="l00565"></a>00565   }
<a name="l00566"></a>00566 
<a name="l00567"></a>00567 
<a name="l00569"></a>00569 
<a name="l00572"></a>00572   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00573"></a>00573   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a48069e460ec162c2f6fef6b0ee2e13ec" title="Permutation of a general matrix stored by rows.">ApplyPermutation</a>(Matrix&lt;T, Prop, ColMajor, Allocator&gt;&amp; A,
<a name="l00574"></a>00574                         <span class="keyword">const</span> Vector&lt;int&gt;&amp; row_perm,
<a name="l00575"></a>00575                         <span class="keyword">const</span> Vector&lt;int&gt;&amp; col_perm,
<a name="l00576"></a>00576                         <span class="keywordtype">int</span> starting_index)
<a name="l00577"></a>00577   {
<a name="l00578"></a>00578     Matrix&lt;T, Prop, ColMajor, Allocator&gt; A_copy = A;
<a name="l00579"></a>00579 
<a name="l00580"></a>00580     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetN(); j++)
<a name="l00581"></a>00581       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetM(); i++)
<a name="l00582"></a>00582         A(i, j) = A_copy(row_perm(i) - starting_index,
<a name="l00583"></a>00583                          col_perm(j) - starting_index);
<a name="l00584"></a>00584   }
<a name="l00585"></a>00585 
<a name="l00586"></a>00586 
<a name="l00588"></a>00588 
<a name="l00593"></a>00593   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00594"></a>00594   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a48edc9483ed12de114cfde40b9bed96e" title="Inverse permutation of a general matrix stored by rows.">ApplyInversePermutation</a>(Matrix&lt;T, Prop, RowMajor, Allocator&gt;&amp; A,
<a name="l00595"></a>00595                                <span class="keyword">const</span> Vector&lt;int&gt;&amp; row_perm,
<a name="l00596"></a>00596                                <span class="keyword">const</span> Vector&lt;int&gt;&amp; col_perm,
<a name="l00597"></a>00597                                <span class="keywordtype">int</span> starting_index)
<a name="l00598"></a>00598   {
<a name="l00599"></a>00599     Matrix&lt;T, Prop, RowMajor, Allocator&gt; A_copy = A;
<a name="l00600"></a>00600 
<a name="l00601"></a>00601     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetM(); i++)
<a name="l00602"></a>00602       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetN(); j++)
<a name="l00603"></a>00603         A(row_perm(i) - starting_index, col_perm(j) - starting_index)
<a name="l00604"></a>00604           = A_copy(i, j);
<a name="l00605"></a>00605   }
<a name="l00606"></a>00606 
<a name="l00607"></a>00607 
<a name="l00609"></a>00609 
<a name="l00614"></a>00614   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00615"></a>00615   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a48edc9483ed12de114cfde40b9bed96e" title="Inverse permutation of a general matrix stored by rows.">ApplyInversePermutation</a>(Matrix&lt;T, Prop, ColMajor, Allocator&gt;&amp; A,
<a name="l00616"></a>00616                                <span class="keyword">const</span> Vector&lt;int&gt;&amp; row_perm,
<a name="l00617"></a>00617                                <span class="keyword">const</span> Vector&lt;int&gt;&amp; col_perm,
<a name="l00618"></a>00618                                <span class="keywordtype">int</span> starting_index)
<a name="l00619"></a>00619   {
<a name="l00620"></a>00620     Matrix&lt;T, Prop, ColMajor, Allocator&gt; A_copy = A;
<a name="l00621"></a>00621 
<a name="l00622"></a>00622     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetN(); j++)
<a name="l00623"></a>00623       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetM(); i++)
<a name="l00624"></a>00624         A(row_perm(i) - starting_index, col_perm(j) - starting_index)
<a name="l00625"></a>00625           = A_copy(i, j);
<a name="l00626"></a>00626   }
<a name="l00627"></a>00627 
<a name="l00628"></a>00628 
<a name="l00629"></a>00629 } <span class="comment">// namespace Seldon.</span>
<a name="l00630"></a>00630 
<a name="l00631"></a>00631 <span class="preprocessor">#define SELDON_FILE_FUNCTIONS_CXX</span>
<a name="l00632"></a>00632 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
