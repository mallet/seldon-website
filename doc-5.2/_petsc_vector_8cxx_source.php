<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>vector/PetscVector.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2011-2012, INRIA</span>
<a name="l00002"></a>00002 <span class="comment">// Author(s): Marc Fragu</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_VECTOR_PETSCVECTOR_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 
<a name="l00024"></a>00024 <span class="preprocessor">#include &quot;PetscVector.hxx&quot;</span>
<a name="l00025"></a>00025 
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 <span class="keyword">namespace </span>Seldon
<a name="l00028"></a>00028 {
<a name="l00029"></a>00029 
<a name="l00030"></a>00030 
<a name="l00031"></a>00031   <span class="comment">/****************</span>
<a name="l00032"></a>00032 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00033"></a>00033 <span class="comment">   ****************/</span>
<a name="l00034"></a>00034 
<a name="l00035"></a>00035 
<a name="l00037"></a>00037 
<a name="l00040"></a>00040   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00041"></a>00041   <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a6ec09b49f9c964762ab9120380dc54d6" title="Default constructor.">PETScVector&lt;T, Allocator&gt;::PETScVector</a>():
<a name="l00042"></a>00042     Vector_Base&lt;T, Allocator&gt;()
<a name="l00043"></a>00043   {
<a name="l00044"></a>00044     this-&gt;m_ = 0;
<a name="l00045"></a>00045     petsc_vector_deallocated_ = <span class="keyword">false</span>;
<a name="l00046"></a>00046   }
<a name="l00047"></a>00047 
<a name="l00048"></a>00048 
<a name="l00050"></a>00050 
<a name="l00054"></a>00054   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00055"></a>00055   <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a6ec09b49f9c964762ab9120380dc54d6" title="Default constructor.">PETScVector&lt;T, Allocator&gt;::PETScVector</a>(<span class="keywordtype">int</span> i, MPI_Comm mpi_communicator)
<a name="l00056"></a>00056     :Vector_Base&lt;T, Allocator&gt;(i)
<a name="l00057"></a>00057   {
<a name="l00058"></a>00058     this-&gt;m_ = i;
<a name="l00059"></a>00059     petsc_vector_deallocated_ = <span class="keyword">false</span>;
<a name="l00060"></a>00060   }
<a name="l00061"></a>00061 
<a name="l00062"></a>00062 
<a name="l00064"></a>00064 
<a name="l00067"></a>00067   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00068"></a>00068   <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a6ec09b49f9c964762ab9120380dc54d6" title="Default constructor.">PETScVector&lt;T, Allocator&gt;::</a>
<a name="l00069"></a>00069 <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a6ec09b49f9c964762ab9120380dc54d6" title="Default constructor.">  PETScVector</a>(Vec&amp; petsc_vector)
<a name="l00070"></a>00070   {
<a name="l00071"></a>00071     petsc_vector_deallocated_ = <span class="keyword">true</span>;
<a name="l00072"></a>00072     Copy(petsc_vector);
<a name="l00073"></a>00073   }
<a name="l00074"></a>00074 
<a name="l00075"></a>00075 
<a name="l00077"></a>00077 
<a name="l00080"></a>00080   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00081"></a>00081   <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a6ec09b49f9c964762ab9120380dc54d6" title="Default constructor.">PETScVector&lt;T, Allocator&gt;::</a>
<a name="l00082"></a>00082 <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a6ec09b49f9c964762ab9120380dc54d6" title="Default constructor.">  PETScVector</a>(<span class="keyword">const</span> PETScVector&lt;T, Allocator&gt;&amp; V)
<a name="l00083"></a>00083   {
<a name="l00084"></a>00084     Copy(V);
<a name="l00085"></a>00085   }
<a name="l00086"></a>00086 
<a name="l00087"></a>00087 
<a name="l00088"></a>00088   <span class="comment">/**************</span>
<a name="l00089"></a>00089 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00090"></a>00090 <span class="comment">   **************/</span>
<a name="l00091"></a>00091 
<a name="l00092"></a><a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#ae416e60119dccc2e4f8d2ee949290ccc">00092</a> 
<a name="l00094"></a>00094   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00095"></a>00095   <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#ae416e60119dccc2e4f8d2ee949290ccc" title="Destructor.">PETScVector&lt;T, Allocator&gt;::~PETScVector</a>()
<a name="l00096"></a>00096   {
<a name="l00097"></a>00097     <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a87b2f7caa1f91402c0943f4b0fd7a4ac" title="Clears the vector.">Clear</a>();
<a name="l00098"></a>00098   }
<a name="l00099"></a>00099 
<a name="l00100"></a>00100 
<a name="l00102"></a>00102 
<a name="l00105"></a>00105   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00106"></a>00106   Vec&amp; <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#ac288674b5e0cbab4289b783279c20347" title="Returns a reference on the inner petsc vector.">PETScVector&lt;T, Allocator&gt;::GetPetscVector</a>()
<a name="l00107"></a>00107   {
<a name="l00108"></a>00108     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a323f6bd490f539750d525646b15583f6" title="Encapsulated PETSc vector.">petsc_vector_</a>;
<a name="l00109"></a>00109   }
<a name="l00110"></a>00110 
<a name="l00111"></a>00111 
<a name="l00113"></a>00113 
<a name="l00116"></a>00116   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00117"></a>00117   <span class="keyword">const</span> Vec&amp; <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#ac288674b5e0cbab4289b783279c20347" title="Returns a reference on the inner petsc vector.">PETScVector&lt;T, Allocator&gt;::GetPetscVector</a>()<span class="keyword"> const</span>
<a name="l00118"></a>00118 <span class="keyword">  </span>{
<a name="l00119"></a>00119     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a323f6bd490f539750d525646b15583f6" title="Encapsulated PETSc vector.">petsc_vector_</a>;
<a name="l00120"></a>00120   }
<a name="l00121"></a>00121 
<a name="l00122"></a>00122 
<a name="l00124"></a>00124 
<a name="l00127"></a>00127   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00128"></a>00128   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#ac6b7117d6550912d70c1b929be557f27" title="Sets the MPI communicator.">PETScVector&lt;T, Allocator&gt;::SetCommunicator</a>(MPI_Comm mpi_communicator)
<a name="l00129"></a>00129   {
<a name="l00130"></a>00130     <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a70bd7cbade506f2690f742931f6d46c2" title="The MPI communicator to use.">mpi_communicator_</a> = mpi_communicator;
<a name="l00131"></a>00131   }
<a name="l00132"></a>00132 
<a name="l00133"></a>00133 
<a name="l00134"></a>00134   <span class="comment">/*********************</span>
<a name="l00135"></a>00135 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00136"></a>00136 <span class="comment">   *********************/</span>
<a name="l00137"></a>00137 
<a name="l00138"></a>00138 
<a name="l00140"></a>00140 
<a name="l00144"></a>00144   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00145"></a>00145   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a87b2f7caa1f91402c0943f4b0fd7a4ac" title="Clears the vector.">PETScVector&lt;T, Allocator&gt;::Clear</a>()
<a name="l00146"></a>00146   {
<a name="l00147"></a>00147     <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a39bcb5ba22a031f8d6fbebea571a3c03" title="Boolean to indicate if the inner PETSc vector is destroyed or not.">petsc_vector_deallocated_</a>)
<a name="l00148"></a>00148       <span class="keywordflow">return</span>;
<a name="l00149"></a>00149     <span class="keywordtype">int</span> ierr;
<a name="l00150"></a>00150     ierr = VecDestroy(&amp;<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a323f6bd490f539750d525646b15583f6" title="Encapsulated PETSc vector.">petsc_vector_</a>);
<a name="l00151"></a>00151     CHKERRABORT(<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a70bd7cbade506f2690f742931f6d46c2" title="The MPI communicator to use.">mpi_communicator_</a>, ierr);
<a name="l00152"></a>00152     <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a39bcb5ba22a031f8d6fbebea571a3c03" title="Boolean to indicate if the inner PETSc vector is destroyed or not.">petsc_vector_deallocated_</a> = <span class="keyword">true</span>;
<a name="l00153"></a>00153   }
<a name="l00154"></a>00154 
<a name="l00155"></a>00155 
<a name="l00157"></a>00157 
<a name="l00161"></a>00161   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00162"></a>00162   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a713d118c2c4a588ed3d043f45619e27b" title="Changes the length of the vector, and keeps previous values.">PETScVector&lt;T, Allocator&gt;::Resize</a>(<span class="keywordtype">int</span> n)
<a name="l00163"></a>00163   {
<a name="l00164"></a>00164     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;PETScVector&lt;T, Allocator&gt;::Resize(int n)&quot;</span>);
<a name="l00165"></a>00165   }
<a name="l00166"></a>00166 
<a name="l00167"></a>00167 
<a name="l00182"></a>00182   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00183"></a>00183   <span class="keyword">inline</span> <span class="keywordtype">void</span> PETScVector&lt;T, Allocator&gt;
<a name="l00184"></a>00184   ::SetData(<span class="keywordtype">int</span> i, <span class="keyword">typename</span> PETScVector&lt;T, Allocator&gt;::pointer data)
<a name="l00185"></a>00185   {
<a name="l00186"></a>00186     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;PETScVector&lt;T, Allocator&gt;::SetData(int i, &quot;</span>
<a name="l00187"></a>00187                     <span class="stringliteral">&quot;typename PETScVector&lt;T, Allocator&gt;::pointer data)&quot;</span>);
<a name="l00188"></a>00188   }
<a name="l00189"></a>00189 
<a name="l00190"></a>00190 
<a name="l00192"></a>00192 
<a name="l00197"></a>00197   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00198"></a>00198   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a73f72da9e6a83f0b54aa4e6f08339159" title="Clears the vector without releasing memory.">PETScVector&lt;T, Allocator&gt;::Nullify</a>()
<a name="l00199"></a>00199   {
<a name="l00200"></a>00200     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;PETScVector&lt;T, Allocator&gt;::Nullify()&quot;</span>);
<a name="l00201"></a>00201   }
<a name="l00202"></a>00202 
<a name="l00203"></a>00203 
<a name="l00204"></a>00204   <span class="comment">/**********************************</span>
<a name="l00205"></a>00205 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l00206"></a>00206 <span class="comment">   **********************************/</span>
<a name="l00207"></a>00207 
<a name="l00208"></a>00208 
<a name="l00210"></a>00210 
<a name="l00214"></a>00214   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00215"></a>00215   <span class="keyword">inline</span> <span class="keyword">typename</span> PETScVector&lt;T, Allocator&gt;::value_type
<a name="l00216"></a>00216   <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a415d9ec2b7070127f200a40abfa09df8" title="Access operator.">PETScVector&lt;T, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00217"></a>00217 <span class="keyword">  </span>{
<a name="l00218"></a>00218 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00219"></a>00219 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00220"></a>00220       <span class="keywordflow">throw</span> WrongIndex(<span class="stringliteral">&quot;Vector&lt;PETSc&gt;::operator()&quot;</span>,
<a name="l00221"></a>00221                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00222"></a>00222                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00223"></a>00223 <span class="preprocessor">#endif</span>
<a name="l00224"></a>00224 <span class="preprocessor"></span>    <span class="keywordtype">int</span> ierr;
<a name="l00225"></a>00225     value_type ret[1];
<a name="l00226"></a>00226     <span class="keywordtype">int</span> index[1];
<a name="l00227"></a>00227     index[0] = i;
<a name="l00228"></a>00228     ierr = VecGetValues(<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a323f6bd490f539750d525646b15583f6" title="Encapsulated PETSc vector.">petsc_vector_</a>, 1, index, ret);
<a name="l00229"></a>00229     CHKERRABORT(<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a70bd7cbade506f2690f742931f6d46c2" title="The MPI communicator to use.">mpi_communicator_</a>, ierr);
<a name="l00230"></a>00230     <span class="keywordflow">return</span> ret[0];
<a name="l00231"></a>00231   }
<a name="l00232"></a>00232 
<a name="l00233"></a>00233 
<a name="l00235"></a>00235 
<a name="l00243"></a>00243   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00244"></a>00244   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#aa9c4710028825f7e8d4c4b38e28d77bc" title="Inserts or adds values into certain locations of a vector.">PETScVector&lt;T, Allocator&gt;</a>
<a name="l00245"></a>00245 <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#aa9c4710028825f7e8d4c4b38e28d77bc" title="Inserts or adds values into certain locations of a vector.">  ::SetBuffer</a>(<span class="keywordtype">int</span> i, T value, InsertMode insert_mode)
<a name="l00246"></a>00246   {
<a name="l00247"></a>00247     <span class="keywordtype">int</span> ierr;
<a name="l00248"></a>00248     <span class="keywordtype">int</span> ix[1] = {i};
<a name="l00249"></a>00249     T data[1] = {value};
<a name="l00250"></a>00250     ierr = VecSetValues(petsc_vector_, 1, ix,
<a name="l00251"></a>00251                         data, INSERT_VALUES);
<a name="l00252"></a>00252     CHKERRABORT(mpi_communicator_, ierr);
<a name="l00253"></a>00253   }
<a name="l00254"></a>00254 
<a name="l00255"></a>00255 
<a name="l00257"></a>00257   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00258"></a>00258   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a7ece84387ef984021d4606b093c2497d" title="Assembles the PETSc vector.">PETScVector&lt;T, Allocator&gt;</a>
<a name="l00259"></a>00259 <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a7ece84387ef984021d4606b093c2497d" title="Assembles the PETSc vector.">  ::Flush</a>()
<a name="l00260"></a>00260   {
<a name="l00261"></a>00261     <span class="keywordtype">int</span> ierr;
<a name="l00262"></a>00262     ierr = VecAssemblyBegin(petsc_vector_);
<a name="l00263"></a>00263     CHKERRABORT(mpi_communicator_, ierr);
<a name="l00264"></a>00264     ierr = VecAssemblyEnd(petsc_vector_);
<a name="l00265"></a>00265     CHKERRABORT(mpi_communicator_, ierr);
<a name="l00266"></a>00266   }
<a name="l00267"></a>00267 
<a name="l00268"></a>00268 
<a name="l00270"></a>00270 
<a name="l00278"></a>00278   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00279"></a>00279   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a0a0a3a91dc5491fd88f6803b208db9ac" title="Returns the range of indices owned by this processor.">PETScVector&lt;T, Allocator&gt;</a>
<a name="l00280"></a>00280 <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a0a0a3a91dc5491fd88f6803b208db9ac" title="Returns the range of indices owned by this processor.">  ::GetProcessorRange</a>(<span class="keywordtype">int</span>&amp; i, <span class="keywordtype">int</span>&amp; j)<span class="keyword"> const</span>
<a name="l00281"></a>00281 <span class="keyword">  </span>{
<a name="l00282"></a>00282     <span class="keywordtype">int</span> ierr;
<a name="l00283"></a>00283     ierr = VecGetOwnershipRange(petsc_vector_, &amp;i, &amp;j);
<a name="l00284"></a>00284     CHKERRABORT(mpi_communicator_, ierr);
<a name="l00285"></a>00285   }
<a name="l00286"></a>00286 
<a name="l00287"></a>00287 
<a name="l00289"></a>00289 
<a name="l00294"></a>00294   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00295"></a>00295   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a0b19d99fda0bdd859486db905f3e758f" title="Duplicates a vector.">PETScVector&lt;T, Allocator&gt;</a>
<a name="l00296"></a>00296 <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a0b19d99fda0bdd859486db905f3e758f" title="Duplicates a vector.">  ::Copy</a>(<span class="keyword">const</span> PETScVector&lt;T, Allocator&gt;&amp; X)
<a name="l00297"></a>00297   {
<a name="l00298"></a>00298     Copy(X.GetPetscVector());
<a name="l00299"></a>00299   }
<a name="l00300"></a>00300 
<a name="l00301"></a>00301 
<a name="l00303"></a>00303 
<a name="l00308"></a>00308   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00309"></a>00309   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a0b19d99fda0bdd859486db905f3e758f" title="Duplicates a vector.">PETScVector&lt;T, Allocator&gt;</a>
<a name="l00310"></a>00310 <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a0b19d99fda0bdd859486db905f3e758f" title="Duplicates a vector.">  ::Copy</a>(<span class="keyword">const</span> Vec&amp; petsc_vector)
<a name="l00311"></a>00311   {
<a name="l00312"></a>00312     Clear();
<a name="l00313"></a>00313 
<a name="l00314"></a>00314     <span class="keywordtype">int</span> ierr;
<a name="l00315"></a>00315     ierr = VecGetSize(petsc_vector, &amp;this-&gt;m_);
<a name="l00316"></a>00316     CHKERRABORT(mpi_communicator_, ierr);
<a name="l00317"></a>00317     PetscObjectGetComm(reinterpret_cast&lt;PetscObject&gt;(petsc_vector),
<a name="l00318"></a>00318                        &amp;mpi_communicator_);
<a name="l00319"></a>00319     CHKERRABORT(mpi_communicator_, ierr);
<a name="l00320"></a>00320     ierr = VecDuplicate(petsc_vector, &amp;petsc_vector_);
<a name="l00321"></a>00321     CHKERRABORT(mpi_communicator_, ierr);
<a name="l00322"></a>00322     ierr = VecCopy(petsc_vector, petsc_vector_);
<a name="l00323"></a>00323     CHKERRABORT(mpi_communicator_, ierr);
<a name="l00324"></a>00324     petsc_vector_deallocated_ = <span class="keyword">false</span>;
<a name="l00325"></a>00325   }
<a name="l00326"></a>00326 
<a name="l00327"></a>00327 
<a name="l00329"></a>00329 
<a name="l00334"></a>00334   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00335"></a>00335   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a922a0d69853d84d718dc9102fa8a7f4f" title="Appends an element to the vector.">PETScVector&lt;T, Allocator&gt;::Append</a>(<span class="keyword">const</span> T&amp; x)
<a name="l00336"></a>00336   {
<a name="l00337"></a>00337     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;PETScVector&lt;T, Allocator&gt;::Append(const T&amp; x)&quot;</span>);
<a name="l00338"></a>00338   }
<a name="l00339"></a>00339 
<a name="l00340"></a>00340 
<a name="l00341"></a>00341   <span class="comment">/*******************</span>
<a name="l00342"></a>00342 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00343"></a>00343 <span class="comment">   *******************/</span>
<a name="l00344"></a>00344 
<a name="l00345"></a>00345 
<a name="l00347"></a>00347 
<a name="l00350"></a>00350   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00351"></a>00351   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a4b1dfd7f5db9600d9e65826c4462d37e" title="Returns the number of elements stored.">PETScVector&lt;T, Allocator&gt;::GetDataSize</a>()<span class="keyword"> const</span>
<a name="l00352"></a>00352 <span class="keyword">  </span>{
<a name="l00353"></a>00353     <span class="keywordflow">return</span> this-&gt;m_;
<a name="l00354"></a>00354   }
<a name="l00355"></a>00355 
<a name="l00356"></a>00356 
<a name="l00358"></a>00358 
<a name="l00361"></a>00361   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00362"></a>00362   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a0dfb99ee603ed3f578132425f5afed32" title="Returns the number of elements stored.">PETScVector&lt;T, Allocator&gt;::GetLocalM</a>()<span class="keyword"> const</span>
<a name="l00363"></a>00363 <span class="keyword">  </span>{
<a name="l00364"></a>00364     <span class="keywordtype">int</span> size;
<a name="l00365"></a>00365     VecGetLocalSize(<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a323f6bd490f539750d525646b15583f6" title="Encapsulated PETSc vector.">petsc_vector_</a>, &amp;size);
<a name="l00366"></a>00366     <span class="keywordflow">return</span> size;
<a name="l00367"></a>00367   }
<a name="l00368"></a>00368 
<a name="l00369"></a>00369 
<a name="l00370"></a>00370   <span class="comment">/************************</span>
<a name="l00371"></a>00371 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l00372"></a>00372 <span class="comment">   ************************/</span>
<a name="l00373"></a>00373 
<a name="l00374"></a>00374 
<a name="l00376"></a>00376 
<a name="l00380"></a>00380   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00381"></a>00381   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a0b318ae92a2a224db5f8a80332b8d25f" title="Sets all elements to zero.">PETScVector&lt;T, Allocator&gt;::Zero</a>()
<a name="l00382"></a>00382   {
<a name="l00383"></a>00383     <span class="keywordtype">int</span> ierr;
<a name="l00384"></a>00384     ierr = VecSet(<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a323f6bd490f539750d525646b15583f6" title="Encapsulated PETSc vector.">petsc_vector_</a>, 0.);
<a name="l00385"></a>00385     CHKERRABORT(<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a70bd7cbade506f2690f742931f6d46c2" title="The MPI communicator to use.">mpi_communicator_</a>, ierr);
<a name="l00386"></a>00386   }
<a name="l00387"></a>00387 
<a name="l00388"></a><a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#ab1fc92c50dd23964a3ce240c1d5c953a">00388</a> 
<a name="l00390"></a>00390   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00391"></a>00391   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#ab1fc92c50dd23964a3ce240c1d5c953a" title="Fills the vector with 0, 1, 2, ...">PETScVector&lt;T, Allocator&gt;::Fill</a>()
<a name="l00392"></a>00392   {
<a name="l00393"></a>00393     <span class="keywordtype">int</span> ierr;
<a name="l00394"></a>00394     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> index(this-&gt;m_);
<a name="l00395"></a>00395     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double&gt;</a> data(this-&gt;m_);
<a name="l00396"></a>00396     index.Fill();
<a name="l00397"></a>00397     data.Fill();
<a name="l00398"></a>00398     ierr = VecSetValues(<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a323f6bd490f539750d525646b15583f6" title="Encapsulated PETSc vector.">petsc_vector_</a>, this-&gt;m_, index.GetData(),
<a name="l00399"></a>00399                         data.GetData(), INSERT_VALUES);
<a name="l00400"></a>00400     CHKERRABORT(<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a70bd7cbade506f2690f742931f6d46c2" title="The MPI communicator to use.">mpi_communicator_</a>, ierr);
<a name="l00401"></a>00401     <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a7ece84387ef984021d4606b093c2497d" title="Assembles the PETSc vector.">Flush</a>();
<a name="l00402"></a>00402   }
<a name="l00403"></a>00403 
<a name="l00404"></a>00404 
<a name="l00406"></a>00406 
<a name="l00409"></a>00409   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00410"></a>00410   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00411"></a>00411   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#ab1fc92c50dd23964a3ce240c1d5c953a" title="Fills the vector with 0, 1, 2, ...">PETScVector&lt;T, Allocator&gt;::Fill</a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00412"></a>00412   {
<a name="l00413"></a>00413     <span class="keywordtype">int</span> ierr;
<a name="l00414"></a>00414     ierr = VecSet(<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a323f6bd490f539750d525646b15583f6" title="Encapsulated PETSc vector.">petsc_vector_</a>, <span class="keywordtype">double</span>(x));
<a name="l00415"></a>00415     CHKERRABORT(<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a70bd7cbade506f2690f742931f6d46c2" title="The MPI communicator to use.">mpi_communicator_</a>, ierr);
<a name="l00416"></a>00416     <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a7ece84387ef984021d4606b093c2497d" title="Assembles the PETSc vector.">Flush</a>();
<a name="l00417"></a>00417   }
<a name="l00418"></a>00418 
<a name="l00419"></a>00419 
<a name="l00421"></a>00421 
<a name="l00424"></a>00424   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00425"></a>00425   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#aeea94a272f075d781c2a71d21072ccf4" title="Fills the vector randomly.">PETScVector&lt;T, Allocator&gt;::FillRand</a>()
<a name="l00426"></a>00426   {
<a name="l00427"></a>00427     <span class="keywordtype">int</span> ierr;
<a name="l00428"></a>00428     Vector&lt;int&gt; index(this-&gt;m_);
<a name="l00429"></a>00429     Vector&lt;double&gt; data(this-&gt;m_);
<a name="l00430"></a>00430     index.Fill();
<a name="l00431"></a>00431     data.FillRand();
<a name="l00432"></a>00432     ierr = VecSetValues(<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a323f6bd490f539750d525646b15583f6" title="Encapsulated PETSc vector.">petsc_vector_</a>, this-&gt;m_, index.GetData(),
<a name="l00433"></a>00433                         data.GetData(), INSERT_VALUES);
<a name="l00434"></a>00434     CHKERRABORT(<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a70bd7cbade506f2690f742931f6d46c2" title="The MPI communicator to use.">mpi_communicator_</a>, ierr);
<a name="l00435"></a>00435     <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a7ece84387ef984021d4606b093c2497d" title="Assembles the PETSc vector.">Flush</a>();
<a name="l00436"></a>00436   }
<a name="l00437"></a>00437 
<a name="l00438"></a>00438 
<a name="l00439"></a>00439   <span class="comment">/*********</span>
<a name="l00440"></a>00440 <span class="comment">   * NORMS *</span>
<a name="l00441"></a>00441 <span class="comment">   *********/</span>
<a name="l00442"></a>00442 
<a name="l00443"></a>00443 
<a name="l00445"></a>00445 
<a name="l00448"></a>00448   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00449"></a>00449   <span class="keyword">typename</span> PETScVector&lt;T, Allocator&gt;::value_type
<a name="l00450"></a>00450   <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a1216ed94d76cd4cc9a57203781deb864" title="Returns the infinite norm.">PETScVector&lt;T, Allocator&gt;::GetNormInf</a>()<span class="keyword"> const</span>
<a name="l00451"></a>00451 <span class="keyword">  </span>{
<a name="l00452"></a>00452     <span class="keywordtype">int</span> ierr;
<a name="l00453"></a>00453     value_type res;
<a name="l00454"></a>00454     ierr = VecNorm(<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a323f6bd490f539750d525646b15583f6" title="Encapsulated PETSc vector.">petsc_vector_</a>, NORM_INFINITY, &amp;res);
<a name="l00455"></a>00455     CHKERRABORT(<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a70bd7cbade506f2690f742931f6d46c2" title="The MPI communicator to use.">mpi_communicator_</a>, ierr);
<a name="l00456"></a>00456     <span class="keywordflow">return</span> res;
<a name="l00457"></a>00457   }
<a name="l00458"></a>00458 
<a name="l00459"></a>00459 
<a name="l00461"></a>00461 
<a name="l00464"></a>00464   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00465"></a>00465   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a7ecdd5c6469cdaa4e263f6ff71fbbf00" title="Returns the index of the highest absolute value.">PETScVector&lt;T, Allocator&gt;::GetNormInfIndex</a>()<span class="keyword"> const</span>
<a name="l00466"></a>00466 <span class="keyword">  </span>{
<a name="l00467"></a>00467     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;PETScVector&lt;T, Allocator&gt;::GetNormInfIndex()&quot;</span>);
<a name="l00468"></a>00468   }
<a name="l00469"></a>00469 
<a name="l00470"></a>00470 
<a name="l00472"></a>00472   <span class="comment">// SEQUENTIAL PETSC VECTOR //</span>
<a name="l00474"></a>00474 <span class="comment"></span>
<a name="l00475"></a>00475 
<a name="l00476"></a>00476   <span class="comment">/****************</span>
<a name="l00477"></a>00477 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00478"></a>00478 <span class="comment">   ****************/</span>
<a name="l00479"></a>00479 
<a name="l00480"></a>00480 
<a name="l00482"></a>00482 
<a name="l00485"></a>00485   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00486"></a>00486   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a1cf9876e5d323efa8918954a01679394" title="Default constructor.">Vector&lt;T, PETScSeq, Allocator&gt;::Vector</a>():
<a name="l00487"></a>00487     PETScVector&lt;T, Allocator&gt;()
<a name="l00488"></a>00488   {
<a name="l00489"></a>00489     this-&gt;<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a70bd7cbade506f2690f742931f6d46c2" title="The MPI communicator to use.">mpi_communicator_</a> = MPI_COMM_WORLD;
<a name="l00490"></a>00490     this-&gt;m_ = 0;
<a name="l00491"></a>00491     <span class="keywordtype">int</span> ierr;
<a name="l00492"></a>00492     ierr = VecCreateSeq(PETSC_COMM_SELF, 0, &amp;this-&gt;<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a323f6bd490f539750d525646b15583f6" title="Encapsulated PETSc vector.">petsc_vector_</a>);
<a name="l00493"></a>00493     this-&gt;<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a39bcb5ba22a031f8d6fbebea571a3c03" title="Boolean to indicate if the inner PETSc vector is destroyed or not.">petsc_vector_deallocated_</a> = <span class="keyword">false</span>;
<a name="l00494"></a>00494   }
<a name="l00495"></a>00495 
<a name="l00496"></a>00496 
<a name="l00498"></a>00498 
<a name="l00502"></a>00502   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00503"></a>00503   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a1cf9876e5d323efa8918954a01679394" title="Default constructor.">Vector&lt;T, PETScSeq, Allocator&gt;::Vector</a>(<span class="keywordtype">int</span> i, MPI_Comm mpi_communicator)
<a name="l00504"></a>00504     :PETScVector&lt;T, Allocator&gt;(i)
<a name="l00505"></a>00505   {
<a name="l00506"></a>00506     <span class="keywordtype">int</span> ierr;
<a name="l00507"></a>00507     this-&gt;<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a70bd7cbade506f2690f742931f6d46c2" title="The MPI communicator to use.">mpi_communicator_</a> = mpi_communicator;
<a name="l00508"></a>00508     this-&gt;m_ = i;
<a name="l00509"></a>00509     ierr = VecCreateSeq(PETSC_COMM_SELF, i, &amp;this-&gt;<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a323f6bd490f539750d525646b15583f6" title="Encapsulated PETSc vector.">petsc_vector_</a>);
<a name="l00510"></a>00510     CHKERRABORT(this-&gt;<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a70bd7cbade506f2690f742931f6d46c2" title="The MPI communicator to use.">mpi_communicator_</a>, ierr);
<a name="l00511"></a>00511     ierr = VecSet(this-&gt;<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a323f6bd490f539750d525646b15583f6" title="Encapsulated PETSc vector.">petsc_vector_</a>, 0.);
<a name="l00512"></a>00512     CHKERRABORT(this-&gt;<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a70bd7cbade506f2690f742931f6d46c2" title="The MPI communicator to use.">mpi_communicator_</a>, ierr);
<a name="l00513"></a>00513     this-&gt;<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a7ece84387ef984021d4606b093c2497d" title="Assembles the PETSc vector.">Flush</a>();
<a name="l00514"></a>00514   }
<a name="l00515"></a>00515 
<a name="l00516"></a>00516 
<a name="l00518"></a>00518 
<a name="l00521"></a>00521   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00522"></a>00522   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a1cf9876e5d323efa8918954a01679394" title="Default constructor.">Vector&lt;T, PETScSeq, Allocator&gt;::</a>
<a name="l00523"></a>00523 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a1cf9876e5d323efa8918954a01679394" title="Default constructor.">  Vector</a>(Vec&amp; petsc_vector): PETScVector&lt;T, Allocator&gt;(petsc_vector)
<a name="l00524"></a>00524   {
<a name="l00525"></a>00525   }
<a name="l00526"></a>00526 
<a name="l00527"></a>00527 
<a name="l00529"></a>00529 
<a name="l00532"></a>00532   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00533"></a>00533   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a1cf9876e5d323efa8918954a01679394" title="Default constructor.">Vector&lt;T, PETScSeq, Allocator&gt;::</a>
<a name="l00534"></a>00534 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a1cf9876e5d323efa8918954a01679394" title="Default constructor.">  Vector</a>(<span class="keyword">const</span> Vector&lt;T, PETScSeq, Allocator&gt;&amp; V)
<a name="l00535"></a>00535   {
<a name="l00536"></a>00536     Copy(V);
<a name="l00537"></a>00537   }
<a name="l00538"></a>00538 
<a name="l00539"></a>00539 
<a name="l00540"></a>00540   <span class="comment">/**************</span>
<a name="l00541"></a>00541 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00542"></a>00542 <span class="comment">   **************/</span>
<a name="l00543"></a>00543 
<a name="l00544"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a8305af2f1806473a28c7fb71d8189cd2">00544</a> 
<a name="l00546"></a>00546   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00547"></a>00547   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, PETScSeq, Allocator&gt;::~Vector</a>()
<a name="l00548"></a>00548   {
<a name="l00549"></a>00549   }
<a name="l00550"></a>00550 
<a name="l00551"></a>00551 
<a name="l00553"></a>00553 
<a name="l00558"></a>00558   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00559"></a>00559   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, PETScSeq, Allocator&gt;</a>
<a name="l00560"></a>00560 <a class="code" href="class_seldon_1_1_vector.php">  ::Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Vector&lt;T, PETScSeq, Allocator&gt;</a>&amp; X)
<a name="l00561"></a>00561   {
<a name="l00562"></a>00562     Copy(X.<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#ac288674b5e0cbab4289b783279c20347" title="Returns a reference on the inner petsc vector.">GetPetscVector</a>());
<a name="l00563"></a>00563   }
<a name="l00564"></a>00564 
<a name="l00565"></a>00565 
<a name="l00567"></a>00567 
<a name="l00572"></a>00572   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00573"></a>00573   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#ad158e7fe8ecc8df073b85ead2b0c5584" title="Duplicates a vector.">Vector&lt;T, PETScSeq, Allocator&gt;</a>
<a name="l00574"></a>00574 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#ad158e7fe8ecc8df073b85ead2b0c5584" title="Duplicates a vector.">  ::Copy</a>(<span class="keyword">const</span> Vec&amp; X)
<a name="l00575"></a>00575   {
<a name="l00576"></a>00576     <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a0b19d99fda0bdd859486db905f3e758f" title="Duplicates a vector.">PETScVector&lt;T, Allocator&gt;::Copy</a>(X);
<a name="l00577"></a>00577   }
<a name="l00578"></a>00578 
<a name="l00579"></a>00579 
<a name="l00581"></a>00581 
<a name="l00587"></a>00587   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00588"></a>00588   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a1d1b36c91fe69fd44552e22c2808415f" title="Vector reallocation.">Vector&lt;T, PETScSeq, Allocator&gt;</a>
<a name="l00589"></a>00589 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a1d1b36c91fe69fd44552e22c2808415f" title="Vector reallocation.">  ::Reallocate</a>(<span class="keywordtype">int</span> i)
<a name="l00590"></a>00590   {
<a name="l00591"></a>00591     this-&gt;Clear();
<a name="l00592"></a>00592     <span class="keywordtype">int</span> ierr;
<a name="l00593"></a>00593     this-&gt;m_ = i;
<a name="l00594"></a>00594     ierr = VecCreateSeq(PETSC_COMM_SELF, i, &amp;this-&gt;petsc_vector_);
<a name="l00595"></a>00595     CHKERRABORT(this-&gt;mpi_communicator_, ierr);
<a name="l00596"></a>00596     Fill(T(0));
<a name="l00597"></a>00597     this-&gt;Flush();
<a name="l00598"></a>00598     this-&gt;petsc_vector_deallocated_ = <span class="keyword">false</span>;
<a name="l00599"></a>00599   }
<a name="l00600"></a>00600 
<a name="l00601"></a>00601 
<a name="l00603"></a>00603 
<a name="l00608"></a>00608   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00609"></a>00609   <span class="keyword">inline</span> Vector&lt;T, PETScSeq, Allocator&gt;&amp; <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a30a826bc72f41ad614457044c541022e" title="Duplicates a vector (assignment operator).">Vector&lt;T, PETScSeq, Allocator&gt;</a>
<a name="l00610"></a>00610 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a30a826bc72f41ad614457044c541022e" title="Duplicates a vector (assignment operator).">  ::operator= </a>(<span class="keyword">const</span> Vector&lt;T, PETScSeq, Allocator&gt;&amp; X)
<a name="l00611"></a>00611   {
<a name="l00612"></a>00612     this-&gt;Copy(X);
<a name="l00613"></a>00613     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00614"></a>00614   }
<a name="l00615"></a>00615 
<a name="l00616"></a>00616 
<a name="l00618"></a>00618 
<a name="l00621"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#acb9780fe5a7e5771e9a8effb7efb8ca1">00621</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00622"></a>00622   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00623"></a>00623   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Vector&lt;T, PETScSeq, Allocator&gt;</a>&amp;
<a name="l00624"></a>00624   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, PETScSeq, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00625"></a>00625   {
<a name="l00626"></a>00626     this-&gt;Fill(x);
<a name="l00627"></a>00627     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00628"></a>00628   }
<a name="l00629"></a>00629 
<a name="l00630"></a>00630 
<a name="l00632"></a>00632 
<a name="l00635"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#aed8a3631e999991258cc5cc386b4d266">00635</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00636"></a>00636   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l00637"></a>00637   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php">Vector&lt;T, PETScSeq, Allocator&gt;</a>&amp; <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, PETScSeq, Allocator&gt;</a>
<a name="l00638"></a>00638 <a class="code" href="class_seldon_1_1_vector.php">  ::operator*= </a>(<span class="keyword">const</span> T0&amp; alpha)
<a name="l00639"></a>00639   {
<a name="l00640"></a>00640     <span class="keywordtype">int</span> ierr;
<a name="l00641"></a>00641     ierr = VecScale(this-&gt;petsc_vector_, alpha);
<a name="l00642"></a>00642     CHKERRABORT(this-&gt;mpi_communicator_, ierr);
<a name="l00643"></a>00643     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00644"></a>00644   }
<a name="l00645"></a>00645 
<a name="l00646"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#aa12c9192437db3a0d15566311646150d">00646</a> 
<a name="l00648"></a>00648   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00649"></a>00649   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, PETScSeq, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00650"></a>00650 <span class="keyword">  </span>{
<a name="l00651"></a>00651     <span class="keywordtype">int</span> ierr;
<a name="l00652"></a>00652     ierr = VecView(this-&gt;petsc_vector_, PETSC_VIEWER_STDOUT_SELF);
<a name="l00653"></a>00653   }
<a name="l00654"></a>00654 
<a name="l00655"></a>00655 
<a name="l00656"></a>00656   <span class="comment">/**************************</span>
<a name="l00657"></a>00657 <span class="comment">   * OUTPUT/INPUT FUNCTIONS *</span>
<a name="l00658"></a>00658 <span class="comment">   **************************/</span>
<a name="l00659"></a>00659 
<a name="l00660"></a>00660 
<a name="l00662"></a>00662 
<a name="l00668"></a>00668   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00669"></a>00669   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, PETScSeq, Allocator&gt;</a>
<a name="l00670"></a>00670 <a class="code" href="class_seldon_1_1_vector.php">  ::Write</a>(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size)<span class="keyword"> const</span>
<a name="l00671"></a>00671 <span class="keyword">  </span>{
<a name="l00672"></a>00672     <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;Vector&lt;T, PETScSeq, Allocator&gt;&quot;</span>
<a name="l00673"></a>00673                     <span class="stringliteral">&quot;::Write(string FileName, bool with_size) const&quot;</span>);
<a name="l00674"></a>00674   }
<a name="l00675"></a>00675 
<a name="l00676"></a>00676 
<a name="l00678"></a>00678 
<a name="l00684"></a>00684   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00685"></a>00685   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#adb0698f98268c84a3a762b350931501f" title="Writes the vector in a file.">Vector&lt;T, PETScSeq, Allocator&gt;</a>
<a name="l00686"></a>00686 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#adb0698f98268c84a3a762b350931501f" title="Writes the vector in a file.">  ::Write</a>(ostream&amp; FileStream, <span class="keywordtype">bool</span> with_size)<span class="keyword"> const</span>
<a name="l00687"></a>00687 <span class="keyword">  </span>{
<a name="l00688"></a>00688     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;Vector&lt;T, PETScSeq, Allocator&gt;&quot;</span>
<a name="l00689"></a>00689                     <span class="stringliteral">&quot;::Write(string FileName, bool with_size) const&quot;</span>);
<a name="l00690"></a>00690   }
<a name="l00691"></a>00691 
<a name="l00692"></a>00692 
<a name="l00694"></a>00694 
<a name="l00699"></a>00699   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00700"></a>00700   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a10f0723ad305b895314eae22d663a13c" title="Writes the vector in a file.">Vector&lt;T, PETScSeq, Allocator&gt;::WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00701"></a>00701 <span class="keyword">  </span>{
<a name="l00702"></a>00702     ofstream FileStream;
<a name="l00703"></a>00703     FileStream.precision(cout.precision());
<a name="l00704"></a>00704     FileStream.flags(cout.flags());
<a name="l00705"></a>00705     FileStream.open(FileName.c_str());
<a name="l00706"></a>00706 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00707"></a>00707 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00708"></a>00708     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00709"></a>00709       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector&lt;T, PETScSeq, Allocator&gt;&quot;</span>
<a name="l00710"></a>00710                     <span class="stringliteral">&quot;::WriteText(string FileName)&quot;</span>,
<a name="l00711"></a>00711                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00712"></a>00712 <span class="preprocessor">#endif</span>
<a name="l00713"></a>00713 <span class="preprocessor"></span>    this-&gt;WriteText(FileStream);
<a name="l00714"></a>00714     FileStream.close();
<a name="l00715"></a>00715   }
<a name="l00716"></a>00716 
<a name="l00717"></a>00717 
<a name="l00719"></a>00719 
<a name="l00724"></a>00724   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00725"></a>00725   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a10f0723ad305b895314eae22d663a13c" title="Writes the vector in a file.">Vector&lt;T, PETScSeq, Allocator&gt;::WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00726"></a>00726 <span class="keyword">  </span>{
<a name="l00727"></a>00727     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;Vector&lt;T, PETScSeq, Allocator&gt;&quot;</span>
<a name="l00728"></a>00728                     <span class="stringliteral">&quot;::WriteText(ostream&amp; FileStream) const&quot;</span>);
<a name="l00729"></a>00729   }
<a name="l00730"></a>00730 
<a name="l00731"></a>00731 
<a name="l00733"></a>00733 
<a name="l00741"></a>00741   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00742"></a>00742   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a8dd852fadb0f06aee6372585e6e36342" title="Sets the vector from a file.">Vector&lt;T, PETScSeq, Allocator&gt;</a>
<a name="l00743"></a>00743 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a8dd852fadb0f06aee6372585e6e36342" title="Sets the vector from a file.">  ::Read</a>(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size)
<a name="l00744"></a>00744   {
<a name="l00745"></a>00745     ifstream FileStream;
<a name="l00746"></a>00746     FileStream.open(FileName.c_str());
<a name="l00747"></a>00747 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00748"></a>00748 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00749"></a>00749     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00750"></a>00750       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector&lt;T, PETScSeq, Allocator&gt;::Read(string FileName)&quot;</span>,
<a name="l00751"></a>00751                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00752"></a>00752 <span class="preprocessor">#endif</span>
<a name="l00753"></a>00753 <span class="preprocessor"></span>    this-&gt;Read(FileStream, with_size);
<a name="l00754"></a>00754     FileStream.close();
<a name="l00755"></a>00755   }
<a name="l00756"></a>00756 
<a name="l00757"></a>00757 
<a name="l00759"></a>00759 
<a name="l00767"></a>00767   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00768"></a>00768   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a8dd852fadb0f06aee6372585e6e36342" title="Sets the vector from a file.">Vector&lt;T, PETScSeq, Allocator&gt;</a>
<a name="l00769"></a>00769 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a8dd852fadb0f06aee6372585e6e36342" title="Sets the vector from a file.">  ::Read</a>(istream&amp; FileStream, <span class="keywordtype">bool</span> with_size)
<a name="l00770"></a>00770   {
<a name="l00771"></a>00771     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;Vector&lt;T, PETScSeq, Allocator&gt;&quot;</span>
<a name="l00772"></a>00772                     <span class="stringliteral">&quot;::Read(istream&amp; FileStream, bool with_size)&quot;</span>);
<a name="l00773"></a>00773   }
<a name="l00774"></a>00774 
<a name="l00775"></a>00775 
<a name="l00777"></a>00777 
<a name="l00782"></a>00782   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00783"></a>00783   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#acdbdc8ad44161af1fb63e6b8341257d9" title="Sets the vector from a file.">Vector&lt;T, PETScSeq, Allocator&gt;::ReadText</a>(<span class="keywordtype">string</span> FileName)
<a name="l00784"></a>00784   {
<a name="l00785"></a>00785     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;Vector&lt;T, PETScSeq, Allocator&gt;&quot;</span>
<a name="l00786"></a>00786                     <span class="stringliteral">&quot;::ReadText(string FileName)&quot;</span>);
<a name="l00787"></a>00787   }
<a name="l00788"></a>00788 
<a name="l00789"></a>00789 
<a name="l00791"></a>00791 
<a name="l00796"></a>00796   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00797"></a>00797   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#acdbdc8ad44161af1fb63e6b8341257d9" title="Sets the vector from a file.">Vector&lt;T, PETScSeq, Allocator&gt;::ReadText</a>(istream&amp; FileStream)
<a name="l00798"></a>00798   {
<a name="l00799"></a>00799     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;Vector&lt;T, PETScSeq, Allocator&gt;&quot;</span>
<a name="l00800"></a>00800                     <span class="stringliteral">&quot;::ReadText(istream&amp; FileStream)&quot;</span>);
<a name="l00801"></a>00801   }
<a name="l00802"></a>00802 
<a name="l00803"></a>00803 
<a name="l00805"></a>00805 
<a name="l00810"></a>00810   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00811"></a>00811   ostream&amp; <a class="code" href="namespace_seldon.php#a5bd4a6d6f3c83048663d57d2fc032107" title="operator&amp;lt;&amp;lt; overloaded for a 3D array.">operator &lt;&lt; </a>(ostream&amp; out,
<a name="l00812"></a>00812                         <span class="keyword">const</span> Vector&lt;T, PETScSeq, Allocator&gt;&amp; V)
<a name="l00813"></a>00813   {
<a name="l00814"></a>00814     out &lt;&lt; <span class="charliteral">&#39;\t&#39;</span>;
<a name="l00815"></a>00815     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; V.GetLength() - 1; i++)
<a name="l00816"></a>00816       out &lt;&lt; V(i) &lt;&lt; <span class="charliteral">&#39;\t&#39;</span>;
<a name="l00817"></a>00817     <span class="keywordflow">if</span> (V.GetLength() != 0)
<a name="l00818"></a>00818       out &lt;&lt; V(V.GetLength() - 1);
<a name="l00819"></a>00819     <span class="keywordflow">return</span> out;
<a name="l00820"></a>00820   }
<a name="l00821"></a>00821 
<a name="l00822"></a>00822 
<a name="l00824"></a>00824   <span class="comment">// PARALLEL PETSC VECTOR //</span>
<a name="l00826"></a>00826 <span class="comment"></span>
<a name="l00827"></a>00827 
<a name="l00828"></a>00828   <span class="comment">/****************</span>
<a name="l00829"></a>00829 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00830"></a>00830 <span class="comment">   ****************/</span>
<a name="l00831"></a>00831 
<a name="l00832"></a>00832 
<a name="l00834"></a>00834 
<a name="l00837"></a>00837   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00838"></a>00838   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#af2ab08543ecdfb8a09d01e4c024607ff" title="Default constructor.">Vector&lt;T, PETScPar, Allocator&gt;::Vector</a>():
<a name="l00839"></a>00839     PETScVector&lt;T, Allocator&gt;()
<a name="l00840"></a>00840   {
<a name="l00841"></a>00841     this-&gt;mpi_communicator_ = MPI_COMM_WORLD;
<a name="l00842"></a>00842     <span class="keywordtype">int</span> ierr;
<a name="l00843"></a>00843     ierr = VecCreateMPI(this-&gt;mpi_communicator_,
<a name="l00844"></a>00844                         PETSC_DECIDE, 0, &amp;this-&gt;petsc_vector_);
<a name="l00845"></a>00845     CHKERRABORT(this-&gt;mpi_communicator_, ierr);
<a name="l00846"></a>00846   }
<a name="l00847"></a>00847 
<a name="l00848"></a>00848 
<a name="l00850"></a>00850 
<a name="l00854"></a>00854   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00855"></a>00855   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#af2ab08543ecdfb8a09d01e4c024607ff" title="Default constructor.">Vector&lt;T, PETScPar, Allocator&gt;::Vector</a>(<span class="keywordtype">int</span> i, MPI_Comm mpi_communicator):
<a name="l00856"></a>00856     PETScVector&lt;T, Allocator&gt;(i)
<a name="l00857"></a>00857   {
<a name="l00858"></a>00858     <span class="keywordtype">int</span> ierr;
<a name="l00859"></a>00859     this-&gt;mpi_communicator_ = mpi_communicator;
<a name="l00860"></a>00860     ierr = VecCreateMPI(this-&gt;mpi_communicator_, PETSC_DECIDE, i,
<a name="l00861"></a>00861                         &amp;this-&gt;petsc_vector_);
<a name="l00862"></a>00862     CHKERRABORT(this-&gt;mpi_communicator_, ierr);
<a name="l00863"></a>00863     ierr = VecSet(this-&gt;petsc_vector_, 0.);
<a name="l00864"></a>00864     CHKERRABORT(this-&gt;mpi_communicator_, ierr);
<a name="l00865"></a>00865     this-&gt;Flush();
<a name="l00866"></a>00866   }
<a name="l00867"></a>00867 
<a name="l00868"></a>00868 
<a name="l00870"></a>00870 
<a name="l00875"></a>00875   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00876"></a>00876   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#af2ab08543ecdfb8a09d01e4c024607ff" title="Default constructor.">Vector&lt;T, PETScPar, Allocator&gt;::Vector</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> Nlocal,
<a name="l00877"></a>00877                                          MPI_Comm mpi_communicator):
<a name="l00878"></a>00878     PETScVector&lt;T, Allocator&gt;(i)
<a name="l00879"></a>00879   {
<a name="l00880"></a>00880     <span class="keywordtype">int</span> ierr;
<a name="l00881"></a>00881     this-&gt;mpi_communicator_ = mpi_communicator;
<a name="l00882"></a>00882     ierr = VecCreateMPI(this-&gt;mpi_communicator_, Nlocal,
<a name="l00883"></a>00883                         i, &amp;this-&gt;petsc_vector_);
<a name="l00884"></a>00884     CHKERRABORT(this-&gt;mpi_communicator_, ierr);
<a name="l00885"></a>00885     ierr = VecSet(this-&gt;petsc_vector_, 0.);
<a name="l00886"></a>00886     CHKERRABORT(this-&gt;mpi_communicator_, ierr);
<a name="l00887"></a>00887     this-&gt;Flush();
<a name="l00888"></a>00888   }
<a name="l00889"></a>00889 
<a name="l00890"></a>00890 
<a name="l00892"></a>00892 
<a name="l00895"></a>00895   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00896"></a>00896   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#af2ab08543ecdfb8a09d01e4c024607ff" title="Default constructor.">Vector&lt;T, PETScPar, Allocator&gt;::</a>
<a name="l00897"></a>00897 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#af2ab08543ecdfb8a09d01e4c024607ff" title="Default constructor.">  Vector</a>(Vec&amp; petsc_vector): PETScVector&lt;T, Allocator&gt;(petsc_vector)
<a name="l00898"></a>00898   {
<a name="l00899"></a>00899   }
<a name="l00900"></a>00900 
<a name="l00901"></a>00901 
<a name="l00903"></a>00903 
<a name="l00906"></a>00906   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00907"></a>00907   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#af2ab08543ecdfb8a09d01e4c024607ff" title="Default constructor.">Vector&lt;T, PETScPar, Allocator&gt;::</a>
<a name="l00908"></a>00908 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#af2ab08543ecdfb8a09d01e4c024607ff" title="Default constructor.">  Vector</a>(<span class="keyword">const</span> Vector&lt;T, PETScPar, Allocator&gt;&amp; V)
<a name="l00909"></a>00909   {
<a name="l00910"></a>00910     Copy(V);
<a name="l00911"></a>00911   }
<a name="l00912"></a>00912 
<a name="l00913"></a>00913 
<a name="l00914"></a>00914   <span class="comment">/**************</span>
<a name="l00915"></a>00915 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00916"></a>00916 <span class="comment">   **************/</span>
<a name="l00917"></a>00917 
<a name="l00918"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#aea88a28edc2030920f93f83ac1f4952f">00918</a> 
<a name="l00920"></a>00920   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00921"></a>00921   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, PETScPar, Allocator&gt;::~Vector</a>()
<a name="l00922"></a>00922   {
<a name="l00923"></a>00923   }
<a name="l00924"></a>00924 
<a name="l00925"></a>00925 
<a name="l00927"></a>00927 
<a name="l00932"></a>00932   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00933"></a>00933   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, PETScPar, Allocator&gt;</a>
<a name="l00934"></a>00934 <a class="code" href="class_seldon_1_1_vector.php">  ::Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php">Vector&lt;T, PETScPar, Allocator&gt;</a>&amp; X)
<a name="l00935"></a>00935   {
<a name="l00936"></a>00936     Copy(X.<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#ac288674b5e0cbab4289b783279c20347" title="Returns a reference on the inner petsc vector.">GetPetscVector</a>());
<a name="l00937"></a>00937     this-&gt;mpi_communicator_ = X.<a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a70bd7cbade506f2690f742931f6d46c2" title="The MPI communicator to use.">mpi_communicator_</a>;
<a name="l00938"></a>00938   }
<a name="l00939"></a>00939 
<a name="l00940"></a>00940 
<a name="l00942"></a>00942 
<a name="l00947"></a>00947   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00948"></a>00948   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a9ee7458d06d687a0078b368d1788738a" title="Duplicates a vector.">Vector&lt;T, PETScPar, Allocator&gt;</a>
<a name="l00949"></a>00949 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a9ee7458d06d687a0078b368d1788738a" title="Duplicates a vector.">  ::Copy</a>(<span class="keyword">const</span> Vec&amp; X)
<a name="l00950"></a>00950   {
<a name="l00951"></a>00951     <a class="code" href="class_seldon_1_1_p_e_t_sc_vector.php#a0b19d99fda0bdd859486db905f3e758f" title="Duplicates a vector.">PETScVector&lt;T, Allocator&gt;::Copy</a>(X);
<a name="l00952"></a>00952   }
<a name="l00953"></a>00953 
<a name="l00954"></a>00954 
<a name="l00956"></a>00956 
<a name="l00961"></a>00961   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00962"></a>00962   <span class="keyword">inline</span> Vector&lt;T, PETScPar, Allocator&gt;&amp; <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a2251829a7141e9b02e2fef218ad32675" title="Duplicates a vector (assignment operator).">Vector&lt;T, PETScPar, Allocator&gt;</a>
<a name="l00963"></a>00963 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a2251829a7141e9b02e2fef218ad32675" title="Duplicates a vector (assignment operator).">  ::operator= </a>(<span class="keyword">const</span> Vector&lt;T, PETScPar, Allocator&gt;&amp; X)
<a name="l00964"></a>00964   {
<a name="l00965"></a>00965     this-&gt;Copy(X);
<a name="l00966"></a>00966     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00967"></a>00967   }
<a name="l00968"></a>00968 
<a name="l00969"></a>00969 
<a name="l00971"></a>00971 
<a name="l00974"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#ad8ce057df39b8cdee03515c7aeca3964">00974</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00975"></a>00975   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00976"></a>00976   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php">Vector&lt;T, PETScPar, Allocator&gt;</a>&amp;
<a name="l00977"></a>00977   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, PETScPar, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00978"></a>00978   {
<a name="l00979"></a>00979     this-&gt;Fill(x);
<a name="l00980"></a>00980     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00981"></a>00981   }
<a name="l00982"></a>00982 
<a name="l00983"></a>00983 
<a name="l00985"></a>00985 
<a name="l00988"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#ac48afbd3ef8035d87ec1f1be1de7ed5e">00988</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00989"></a>00989   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l00990"></a>00990   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php">Vector&lt;T, PETScPar, Allocator&gt;</a>&amp; <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, PETScPar, Allocator&gt;</a>
<a name="l00991"></a>00991 <a class="code" href="class_seldon_1_1_vector.php">  ::operator*= </a>(<span class="keyword">const</span> T0&amp; alpha)
<a name="l00992"></a>00992   {
<a name="l00993"></a>00993     <span class="keywordtype">int</span> ierr;
<a name="l00994"></a>00994     ierr = VecScale(this-&gt;petsc_vector_, alpha);
<a name="l00995"></a>00995     CHKERRABORT(this-&gt;mpi_communicator_, ierr);
<a name="l00996"></a>00996     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00997"></a>00997   }
<a name="l00998"></a>00998 
<a name="l00999"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a5db0dd7d15f1a4ddf8f66bb68ab2cbd0">00999</a> 
<a name="l01001"></a>01001   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l01002"></a>01002   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, PETScPar, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l01003"></a>01003 <span class="keyword">  </span>{
<a name="l01004"></a>01004     <span class="keywordtype">int</span> ierr;
<a name="l01005"></a>01005     ierr = VecView(this-&gt;petsc_vector_, PETSC_VIEWER_STDOUT_WORLD);
<a name="l01006"></a>01006   }
<a name="l01007"></a>01007 
<a name="l01008"></a>01008 
<a name="l01010"></a>01010 
<a name="l01016"></a>01016   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l01017"></a>01017   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, PETScPar, Allocator&gt;</a>
<a name="l01018"></a>01018 <a class="code" href="class_seldon_1_1_vector.php">  ::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> local_size)
<a name="l01019"></a>01019   {
<a name="l01020"></a>01020     this-&gt;Clear();
<a name="l01021"></a>01021     <span class="keywordtype">int</span> ierr;
<a name="l01022"></a>01022     this-&gt;m_ = i;
<a name="l01023"></a>01023     ierr = VecCreateMPI(this-&gt;mpi_communicator_, local_size, i,
<a name="l01024"></a>01024                         &amp;this-&gt;petsc_vector_);
<a name="l01025"></a>01025     CHKERRABORT(this-&gt;mpi_communicator_, ierr);
<a name="l01026"></a>01026     Fill(T(0));
<a name="l01027"></a>01027     this-&gt;Flush();
<a name="l01028"></a>01028     this-&gt;petsc_vector_deallocated_ = <span class="keyword">false</span>;
<a name="l01029"></a>01029   }
<a name="l01030"></a>01030 
<a name="l01031"></a>01031 
<a name="l01032"></a>01032   <span class="comment">/**************************</span>
<a name="l01033"></a>01033 <span class="comment">   * OUTPUT/INPUT FUNCTIONS *</span>
<a name="l01034"></a>01034 <span class="comment">   **************************/</span>
<a name="l01035"></a>01035 
<a name="l01036"></a>01036 
<a name="l01038"></a>01038 
<a name="l01044"></a>01044   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l01045"></a>01045   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#af1e6f26c236b3a57a51c03f820349168" title="Writes the vector in a file.">Vector&lt;T, PETScPar, Allocator&gt;</a>
<a name="l01046"></a>01046 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#af1e6f26c236b3a57a51c03f820349168" title="Writes the vector in a file.">  ::Write</a>(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size)<span class="keyword"> const</span>
<a name="l01047"></a>01047 <span class="keyword">  </span>{
<a name="l01048"></a>01048     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;Vector&lt;T, PETScPar, Allocator&gt;&quot;</span>
<a name="l01049"></a>01049                     <span class="stringliteral">&quot;::Write(string FileName, bool with_size) const&quot;</span>);
<a name="l01050"></a>01050   }
<a name="l01051"></a>01051 
<a name="l01052"></a>01052 
<a name="l01054"></a>01054 
<a name="l01060"></a>01060   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l01061"></a>01061   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#af1e6f26c236b3a57a51c03f820349168" title="Writes the vector in a file.">Vector&lt;T, PETScPar, Allocator&gt;</a>
<a name="l01062"></a>01062 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#af1e6f26c236b3a57a51c03f820349168" title="Writes the vector in a file.">  ::Write</a>(ostream&amp; FileStream, <span class="keywordtype">bool</span> with_size)<span class="keyword"> const</span>
<a name="l01063"></a>01063 <span class="keyword">  </span>{
<a name="l01064"></a>01064     <span class="keywordtype">int</span> local_n;
<a name="l01065"></a>01065     VecGetLocalSize(this-&gt;petsc_vector_, &amp;local_n);
<a name="l01066"></a>01066 
<a name="l01067"></a>01067     Vector&lt;int&gt; index(local_n);
<a name="l01068"></a>01068     index.Fill();
<a name="l01069"></a>01069     <span class="keywordtype">int</span> i_start, i_end;
<a name="l01070"></a>01070     this-&gt;GetProcessorRange(i_start, i_end);
<a name="l01071"></a>01071     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; local_n; i++)
<a name="l01072"></a>01072       index(i) += i_start;
<a name="l01073"></a>01073     Vector&lt;T&gt; data(local_n);
<a name="l01074"></a>01074     VecGetValues(this-&gt;petsc_vector_, local_n, index.GetData(),
<a name="l01075"></a>01075                  data.GetData());
<a name="l01076"></a>01076     data.Write(FileStream, with_size);
<a name="l01077"></a>01077   }
<a name="l01078"></a>01078 
<a name="l01079"></a>01079 
<a name="l01081"></a>01081 
<a name="l01086"></a>01086   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l01087"></a>01087   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a617424e1aa6f5e3e3f5f87189b6c66aa" title="Writes the vector in a file.">Vector&lt;T, PETScPar, Allocator&gt;::WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l01088"></a>01088 <span class="keyword">  </span>{
<a name="l01089"></a>01089     ofstream FileStream;
<a name="l01090"></a>01090     FileStream.precision(cout.precision());
<a name="l01091"></a>01091     FileStream.flags(cout.flags());
<a name="l01092"></a>01092     FileStream.open(FileName.c_str());
<a name="l01093"></a>01093 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01094"></a>01094 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01095"></a>01095     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01096"></a>01096       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector&lt;T, PETScPar, Allocator&gt;&quot;</span>
<a name="l01097"></a>01097                     <span class="stringliteral">&quot;::WriteText(string FileName)&quot;</span>,
<a name="l01098"></a>01098                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01099"></a>01099 <span class="preprocessor">#endif</span>
<a name="l01100"></a>01100 <span class="preprocessor"></span>    this-&gt;WriteText(FileStream);
<a name="l01101"></a>01101     FileStream.close();
<a name="l01102"></a>01102 
<a name="l01103"></a>01103   }
<a name="l01104"></a>01104 
<a name="l01105"></a>01105 
<a name="l01107"></a>01107 
<a name="l01112"></a>01112   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l01113"></a>01113   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a617424e1aa6f5e3e3f5f87189b6c66aa" title="Writes the vector in a file.">Vector&lt;T, PETScPar, Allocator&gt;::WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l01114"></a>01114 <span class="keyword">  </span>{
<a name="l01115"></a>01115     <span class="keywordtype">int</span> local_n;
<a name="l01116"></a>01116     VecGetLocalSize(this-&gt;petsc_vector_, &amp;local_n);
<a name="l01117"></a>01117 
<a name="l01118"></a>01118     Vector&lt;int&gt; index(local_n);
<a name="l01119"></a>01119     index.Fill();
<a name="l01120"></a>01120     <span class="keywordtype">int</span> i_start, i_end;
<a name="l01121"></a>01121     this-&gt;GetProcessorRange(i_start, i_end);
<a name="l01122"></a>01122     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; local_n; i++)
<a name="l01123"></a>01123       index(i) += i_start;
<a name="l01124"></a>01124     Vector&lt;T&gt; data(local_n);
<a name="l01125"></a>01125     VecGetValues(this-&gt;petsc_vector_, local_n, index.GetData(),
<a name="l01126"></a>01126                  data.GetData());
<a name="l01127"></a>01127     data.WriteText(FileStream);
<a name="l01128"></a>01128   }
<a name="l01129"></a>01129 
<a name="l01130"></a>01130 
<a name="l01132"></a>01132 
<a name="l01140"></a>01140   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l01141"></a>01141   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#acd7cda2c966facc985412cdac9962a0f" title="Sets the vector from a file.">Vector&lt;T, PETScPar, Allocator&gt;</a>
<a name="l01142"></a>01142 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#acd7cda2c966facc985412cdac9962a0f" title="Sets the vector from a file.">  ::Read</a>(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size)
<a name="l01143"></a>01143   {
<a name="l01144"></a>01144     ifstream FileStream;
<a name="l01145"></a>01145     FileStream.open(FileName.c_str());
<a name="l01146"></a>01146 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01147"></a>01147 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01148"></a>01148     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01149"></a>01149       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector&lt;T, PETScPar, Allocator&gt;::Read(string FileName)&quot;</span>,
<a name="l01150"></a>01150                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01151"></a>01151 <span class="preprocessor">#endif</span>
<a name="l01152"></a>01152 <span class="preprocessor"></span>    this-&gt;Read(FileStream, with_size);
<a name="l01153"></a>01153     FileStream.close();
<a name="l01154"></a>01154   }
<a name="l01155"></a>01155 
<a name="l01156"></a>01156 
<a name="l01158"></a>01158 
<a name="l01166"></a>01166   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l01167"></a>01167   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#acd7cda2c966facc985412cdac9962a0f" title="Sets the vector from a file.">Vector&lt;T, PETScPar, Allocator&gt;</a>
<a name="l01168"></a>01168 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#acd7cda2c966facc985412cdac9962a0f" title="Sets the vector from a file.">  ::Read</a>(istream&amp; FileStream, <span class="keywordtype">bool</span> with_size)
<a name="l01169"></a>01169   {
<a name="l01170"></a>01170     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;PETScVector&lt;T, PETScPar, Allocator&gt;&quot;</span>
<a name="l01171"></a>01171                     <span class="stringliteral">&quot;::Read(istream&amp; FileStream, bool with_size)&quot;</span>);
<a name="l01172"></a>01172   }
<a name="l01173"></a>01173 
<a name="l01174"></a>01174 
<a name="l01176"></a>01176 
<a name="l01181"></a>01181   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l01182"></a>01182   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a0a9444914b4d3df7d5b99748ea98150e" title="Sets the vector from a file.">Vector&lt;T, PETScPar, Allocator&gt;::ReadText</a>(<span class="keywordtype">string</span> FileName)
<a name="l01183"></a>01183   {
<a name="l01184"></a>01184     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;PETScVector&lt;T, PETScPar, Allocator&gt;&quot;</span>
<a name="l01185"></a>01185                     <span class="stringliteral">&quot;::ReadText(string FileName)&quot;</span>);
<a name="l01186"></a>01186   }
<a name="l01187"></a>01187 
<a name="l01188"></a>01188 
<a name="l01190"></a>01190 
<a name="l01195"></a>01195   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l01196"></a>01196   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a0a9444914b4d3df7d5b99748ea98150e" title="Sets the vector from a file.">Vector&lt;T, PETScPar, Allocator&gt;::ReadText</a>(istream&amp; FileStream)
<a name="l01197"></a>01197   {
<a name="l01198"></a>01198     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;PETScVector&lt;T, PETScPar, Allocator&gt;&quot;</span>
<a name="l01199"></a>01199                     <span class="stringliteral">&quot;::ReadText(istream&amp; FileStream)&quot;</span>);
<a name="l01200"></a>01200   }
<a name="l01201"></a>01201 
<a name="l01202"></a>01202 
<a name="l01204"></a>01204 
<a name="l01209"></a>01209   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l01210"></a>01210   ostream&amp; <a class="code" href="namespace_seldon.php#a5bd4a6d6f3c83048663d57d2fc032107" title="operator&amp;lt;&amp;lt; overloaded for a 3D array.">operator &lt;&lt; </a>(ostream&amp; out,
<a name="l01211"></a>01211                         <span class="keyword">const</span> Vector&lt;T, PETScPar, Allocator&gt;&amp; V)
<a name="l01212"></a>01212   {
<a name="l01213"></a>01213     out &lt;&lt; <span class="charliteral">&#39;\t&#39;</span>;
<a name="l01214"></a>01214     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; V.GetLength() - 1; i++)
<a name="l01215"></a>01215       out &lt;&lt; V(i) &lt;&lt; <span class="charliteral">&#39;\t&#39;</span>;
<a name="l01216"></a>01216     <span class="keywordflow">if</span> (V.GetLength() != 0)
<a name="l01217"></a>01217       out &lt;&lt; V(V.GetLength() - 1);
<a name="l01218"></a>01218     <span class="keywordflow">return</span> out;
<a name="l01219"></a>01219   }
<a name="l01220"></a>01220 
<a name="l01221"></a>01221 
<a name="l01222"></a>01222 } <span class="comment">// namespace Seldon.</span>
<a name="l01223"></a>01223 
<a name="l01224"></a>01224 
<a name="l01225"></a>01225 <span class="preprocessor">#define SELDON_FILE_PETSCVECTOR_CXX</span>
<a name="l01226"></a>01226 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
