<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix_pastix.php">MatrixPastix</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::MatrixPastix&lt; T &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::MatrixPastix" -->
<p><a href="class_seldon_1_1_matrix_pastix-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a862e60553d3532ab99a3645f219d782d"></a><!-- doxytag: member="Seldon::MatrixPastix::MatrixPastix" ref="a862e60553d3532ab99a3645f219d782d" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#a862e60553d3532ab99a3645f219d782d">MatrixPastix</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a06e7866292ac5b30289cf566d56b0caf"></a><!-- doxytag: member="Seldon::MatrixPastix::~MatrixPastix" ref="a06e7866292ac5b30289cf566d56b0caf" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#a06e7866292ac5b30289cf566d56b0caf">~MatrixPastix</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">destructor <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0840578aaa2639d33d5c47adea893cf8"></a><!-- doxytag: member="Seldon::MatrixPastix::Clear" ref="a0840578aaa2639d33d5c47adea893cf8" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#a0840578aaa2639d33d5c47adea893cf8">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clearing factorization. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afa7e34797e4e8be8c02856c862fe102e"></a><!-- doxytag: member="Seldon::MatrixPastix::CallPastix" ref="afa7e34797e4e8be8c02856c862fe102e" args="(const MPI_Comm &amp;, pastix_int_t *colptr, pastix_int_t *row, T *val, T *b, pastix_int_t nrhs)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>CallPastix</b> (const MPI_Comm &amp;, pastix_int_t *colptr, pastix_int_t *row, T *val, T *b, pastix_int_t nrhs)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a29ac5cb9e5315ae68d6fc5d1e97eef71"></a><!-- doxytag: member="Seldon::MatrixPastix::HideMessages" ref="a29ac5cb9e5315ae68d6fc5d1e97eef71" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#a29ac5cb9e5315ae68d6fc5d1e97eef71">HideMessages</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">no message will be displayed <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0ca315759b1dd24114ff47958c37cb96"></a><!-- doxytag: member="Seldon::MatrixPastix::ShowMessages" ref="a0ca315759b1dd24114ff47958c37cb96" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#a0ca315759b1dd24114ff47958c37cb96">ShowMessages</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Low level of display. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adc33ae8c7f93e468abd3ade499bea393"></a><!-- doxytag: member="Seldon::MatrixPastix::ShowFullHistory" ref="adc33ae8c7f93e468abd3ade499bea393" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#adc33ae8c7f93e468abd3ade499bea393">ShowFullHistory</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displaying all messages. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a477440df6355efb8a7b204e42b35fc9f"></a><!-- doxytag: member="Seldon::MatrixPastix::SelectOrdering" ref="a477440df6355efb8a7b204e42b35fc9f" args="(int type)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SelectOrdering</b> (int type)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3f5c5a6528523454ad3e3e1788b8e4ba"></a><!-- doxytag: member="Seldon::MatrixPastix::SetPermutation" ref="a3f5c5a6528523454ad3e3e1788b8e4ba" args="(const IVect &amp;permut)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetPermutation</b> (const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;permut)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6f6a15165b8961d62505dc2ff4f261e0"></a><!-- doxytag: member="Seldon::MatrixPastix::RefineSolution" ref="a6f6a15165b8961d62505dc2ff4f261e0" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#a6f6a15165b8961d62505dc2ff4f261e0">RefineSolution</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">You can require that solution is refined after LU resolution. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af8a85f37469fa702b6ea1ebd2a2b4b0e"></a><!-- doxytag: member="Seldon::MatrixPastix::DoNotRefineSolution" ref="af8a85f37469fa702b6ea1ebd2a2b4b0e" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#af8a85f37469fa702b6ea1ebd2a2b4b0e">DoNotRefineSolution</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">You can require that solution is not refined (faster). <br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a69e64cad0fe1938748e49c0d0057bd66"></a><!-- doxytag: member="Seldon::MatrixPastix::FindOrdering" ref="a69e64cad0fe1938748e49c0d0057bd66" args="(Matrix&lt; T0, Prop, Storage, Allocator &gt; &amp;mat, Vector&lt; Tint &gt; &amp;numbers, bool keep_matrix=false)" -->
template&lt;class T0 , class Prop , class Storage , class Allocator , class Tint &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#a69e64cad0fe1938748e49c0d0057bd66">FindOrdering</a> (<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T0, Prop, Storage, Allocator &gt; &amp;mat, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; Tint &gt; &amp;numbers, bool keep_matrix=false)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returning ordering found by Scotch. <br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a638bb937143f0aacd9f241d4c89febba"></a><!-- doxytag: member="Seldon::MatrixPastix::FactorizeMatrix" ref="a638bb937143f0aacd9f241d4c89febba" args="(Matrix&lt; T, General, Storage, Allocator &gt; &amp;mat, bool keep_matrix=false)" -->
template&lt;class Storage , class Allocator &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#a638bb937143f0aacd9f241d4c89febba">FactorizeMatrix</a> (<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, <a class="el" href="class_seldon_1_1_general.php">General</a>, Storage, Allocator &gt; &amp;mat, bool keep_matrix=false)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Factorization of unsymmetric matrix. <br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a328852f857f9e71c3065c4f9e4b37b06"></a><!-- doxytag: member="Seldon::MatrixPastix::FactorizeMatrix" ref="a328852f857f9e71c3065c4f9e4b37b06" args="(Matrix&lt; T, Symmetric, Storage, Allocator &gt; &amp;mat, bool keep_matrix=false)" -->
template&lt;class Storage , class Allocator &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#a328852f857f9e71c3065c4f9e4b37b06">FactorizeMatrix</a> (<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, <a class="el" href="class_seldon_1_1_symmetric.php">Symmetric</a>, Storage, Allocator &gt; &amp;mat, bool keep_matrix=false)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Factorization of symmetric matrix. <br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a74f16146dfc8aa871028cfa6086a184a"></a><!-- doxytag: member="Seldon::MatrixPastix::Solve" ref="a74f16146dfc8aa871028cfa6086a184a" args="(Vector&lt; T, VectFull, Allocator2 &gt; &amp;x)" -->
template&lt;class Allocator2 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#a74f16146dfc8aa871028cfa6086a184a">Solve</a> (<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator2 &gt; &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">solving A x = b (A is already factorized) <br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a9f48e18656ea0d118f8e4d60bed06718"></a><!-- doxytag: member="Seldon::MatrixPastix::Solve" ref="a9f48e18656ea0d118f8e4d60bed06718" args="(const Transpose_status &amp;TransA, Vector&lt; T, VectFull, Allocator2 &gt; &amp;x)" -->
template&lt;class Allocator2 , class Transpose_status &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#a9f48e18656ea0d118f8e4d60bed06718">Solve</a> (const Transpose_status &amp;TransA, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator2 &gt; &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">solving A x = b or A^T x = b (A is already factorized) <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac3ebb0a32a3aa30164f54dbaa75bff4a"></a><!-- doxytag: member="Seldon::MatrixPastix::SetNumberThreadPerNode" ref="ac3ebb0a32a3aa30164f54dbaa75bff4a" args="(int num_thread)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#ac3ebb0a32a3aa30164f54dbaa75bff4a">SetNumberThreadPerNode</a> (int num_thread)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Modifies the number of threads per node. <br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a64fb6fb73d7407926c1fbd93ed3182da"></a><!-- doxytag: member="Seldon::MatrixPastix::FactorizeDistributedMatrix" ref="a64fb6fb73d7407926c1fbd93ed3182da" args="(MPI::Comm &amp;comm_facto, Vector&lt; pastix_int_t, VectFull, Alloc1 &gt; &amp;, Vector&lt; pastix_int_t, VectFull, Alloc2 &gt; &amp;, Vector&lt; T, VectFull, Alloc3 &gt; &amp;, const Vector&lt; Tint &gt; &amp;glob_number, bool sym, bool keep_matrix=false)" -->
template&lt;class Alloc1 , class Alloc2 , class Alloc3 , class Tint &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#a64fb6fb73d7407926c1fbd93ed3182da">FactorizeDistributedMatrix</a> (MPI::Comm &amp;comm_facto, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; pastix_int_t, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1 &gt; &amp;, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; pastix_int_t, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc2 &gt; &amp;, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc3 &gt; &amp;, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; Tint &gt; &amp;glob_number, bool sym, bool keep_matrix=false)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Distributed factorization (on several nodes). <br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="ae7fa2cb0737e9082bb06e119315a4fe7"></a><!-- doxytag: member="Seldon::MatrixPastix::SolveDistributed" ref="ae7fa2cb0737e9082bb06e119315a4fe7" args="(MPI::Comm &amp;comm_facto, Vector&lt; T, Vect_Full, Allocator2 &gt; &amp;x, const Vector&lt; Tint &gt; &amp;glob_num)" -->
template&lt;class Allocator2 , class Tint &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><b>SolveDistributed</b> (MPI::Comm &amp;comm_facto, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, Allocator2 &gt; &amp;x, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; Tint &gt; &amp;glob_num)</td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a8bc69064e340c5fc557e736f0b26a62c"></a><!-- doxytag: member="Seldon::MatrixPastix::SolveDistributed" ref="a8bc69064e340c5fc557e736f0b26a62c" args="(MPI::Comm &amp;comm_facto, const Transpose_status &amp;TransA, Vector&lt; T, Vect_Full, Allocator2 &gt; &amp;x, const Vector&lt; Tint &gt; &amp;glob_num)" -->
template&lt;class Allocator2 , class Transpose_status , class Tint &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><b>SolveDistributed</b> (MPI::Comm &amp;comm_facto, const Transpose_status &amp;TransA, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Vect_Full, Allocator2 &gt; &amp;x, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; Tint &gt; &amp;glob_num)</td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a6d0b5ca0b8b0df532ec86b9328a5c17c"></a><!-- doxytag: member="Seldon::MatrixPastix::CallPastix" ref="a6d0b5ca0b8b0df532ec86b9328a5c17c" args="(const MPI_Comm &amp;comm, pastix_int_t *colptr, pastix_int_t *row, double *val, double *b, pastix_int_t nrhs)" -->
template&lt;&gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><b>CallPastix</b> (const MPI_Comm &amp;comm, pastix_int_t *colptr, pastix_int_t *row, double *val, double *b, pastix_int_t nrhs)</td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="aa8fb795dd83266948a2ff897678da767"></a><!-- doxytag: member="Seldon::MatrixPastix::CallPastix" ref="aa8fb795dd83266948a2ff897678da767" args="(const MPI_Comm &amp;comm, pastix_int_t *colptr, pastix_int_t *row, complex&lt; double &gt; *val, complex&lt; double &gt; *b, pastix_int_t nrhs)" -->
template&lt;&gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><b>CallPastix</b> (const MPI_Comm &amp;comm, pastix_int_t *colptr, pastix_int_t *row, complex&lt; double &gt; *val, complex&lt; double &gt; *b, pastix_int_t nrhs)</td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1d728cf85201e0e4b35fd66c985ca95a"></a><!-- doxytag: member="Seldon::MatrixPastix::pastix_data" ref="a1d728cf85201e0e4b35fd66c985ca95a" args="" -->
pastix_data_t *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#a1d728cf85201e0e4b35fd66c985ca95a">pastix_data</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">pastix structure <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3e0e4d255058e66c82419232b28fe178"></a><!-- doxytag: member="Seldon::MatrixPastix::iparm" ref="a3e0e4d255058e66c82419232b28fe178" args="[64]" -->
pastix_int_t&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#a3e0e4d255058e66c82419232b28fe178">iparm</a> [64]</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">options (integers) <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab0e31b30663fb336f638fc847663c1eb"></a><!-- doxytag: member="Seldon::MatrixPastix::dparm" ref="ab0e31b30663fb336f638fc847663c1eb" args="[64]" -->
double&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#ab0e31b30663fb336f638fc847663c1eb">dparm</a> [64]</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">options (floats) <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad07a8774a76b9fdc5a71210922d2924e"></a><!-- doxytag: member="Seldon::MatrixPastix::n" ref="ad07a8774a76b9fdc5a71210922d2924e" args="" -->
pastix_int_t&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e">n</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">number of columns <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0e40bdc9ac7c0ae1bd80e05d9942afee"></a><!-- doxytag: member="Seldon::MatrixPastix::perm" ref="a0e40bdc9ac7c0ae1bd80e05d9942afee" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; pastix_int_t &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#a0e40bdc9ac7c0ae1bd80e05d9942afee">perm</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">permutation arrays <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0efef3f02fb5e5be895e62843d78851a"></a><!-- doxytag: member="Seldon::MatrixPastix::invp" ref="a0efef3f02fb5e5be895e62843d78851a" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; pastix_int_t &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>invp</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aeca666d886a9ab8e8e8e907963a6ed64"></a><!-- doxytag: member="Seldon::MatrixPastix::col_num" ref="aeca666d886a9ab8e8e8e907963a6ed64" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; pastix_int_t &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#aeca666d886a9ab8e8e8e907963a6ed64">col_num</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">local to global <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab0637a34811226bdedf63b3e71c7cef6"></a><!-- doxytag: member="Seldon::MatrixPastix::distributed" ref="ab0637a34811226bdedf63b3e71c7cef6" args="" -->
bool&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#ab0637a34811226bdedf63b3e71c7cef6">distributed</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">if true, resolution on several nodes <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a138482f32cb3046a3faf597aff405fb3"></a><!-- doxytag: member="Seldon::MatrixPastix::print_level" ref="a138482f32cb3046a3faf597aff405fb3" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#a138482f32cb3046a3faf597aff405fb3">print_level</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">level of display <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8c22604c5e88bb75086bde3f344cd4d5"></a><!-- doxytag: member="Seldon::MatrixPastix::refine_solution" ref="a8c22604c5e88bb75086bde3f344cd4d5" args="" -->
bool&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_pastix.php#a8c22604c5e88bb75086bde3f344cd4d5">refine_solution</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">if true, solution is refined <br/></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T&gt;<br/>
 class Seldon::MatrixPastix&lt; T &gt;</h3>


<p>Definition at line <a class="el" href="_pastix_8hxx_source.php#l00033">33</a> of file <a class="el" href="_pastix_8hxx_source.php">Pastix.hxx</a>.</p>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>computation/interfaces/direct/<a class="el" href="_pastix_8hxx_source.php">Pastix.hxx</a></li>
<li>computation/interfaces/direct/<a class="el" href="_pastix_8cxx_source.php">Pastix.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
