<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Matrix_Symmetric</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a> &#124;
<a href="#pro-static-attribs">Static Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Matrix_Symmetric" --><!-- doxytag: inherits="Seldon::Matrix_Base" -->
<p><a class="el" href="class_seldon_1_1_symmetric.php">Symmetric</a> matrix stored in a full matrix.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_matrix___symmetric.png" usemap="#Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;_map" alt=""/>
  <map id="Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;_map" name="Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;_map">
<area href="class_seldon_1_1_matrix___base.php" alt="Seldon::Matrix_Base&lt; T, Allocator &gt;" shape="rect" coords="0,0,337,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_matrix___symmetric-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa9950ea4d3f6d9c31773687e7e4c8792"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::value_type" ref="aa9950ea4d3f6d9c31773687e7e4c8792" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae7698ab73b82faf4bd48bce63fbe4c99"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::pointer" ref="ae7698ab73b82faf4bd48bce63fbe4c99" args="" -->
typedef Allocator::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af59cac85a6fabc7e852371e2a630aa3f"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::const_pointer" ref="af59cac85a6fabc7e852371e2a630aa3f" args="" -->
typedef Allocator::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aedbd4dce98423d281b03ad1df0b6bfaa"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::reference" ref="aedbd4dce98423d281b03ad1df0b6bfaa" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2019f0cc581688ef0041786c223c3593"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::const_reference" ref="a2019f0cc581688ef0041786c223c3593" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5b8010b39cc4d27a01b35d708cd1ed91"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::entry_type" ref="a5b8010b39cc4d27a01b35d708cd1ed91" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>entry_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3544628e05f1643416eaaa7faaed3719"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::access_type" ref="a3544628e05f1643416eaaa7faaed3719" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>access_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af83049d4511cd9a501bf213f3ea9f343"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::const_access_type" ref="af83049d4511cd9a501bf213f3ea9f343" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_access_type</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a82951e97ce120fda463beebe44d9c5e3">Matrix_Symmetric</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#a82951e97ce120fda463beebe44d9c5e3"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#ab4c39b6e45f2f22dbf8dd50ad8509ad7">Matrix_Symmetric</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Main constructor.  <a href="#ab4c39b6e45f2f22dbf8dd50ad8509ad7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8d0b954996edc9ccf67c46eb8ed05ad2"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Matrix_Symmetric" ref="a8d0b954996edc9ccf67c46eb8ed05ad2" args="(const Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt; &amp;A)" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a8d0b954996edc9ccf67c46eb8ed05ad2">Matrix_Symmetric</a> (const <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copy constructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae3c7bcbdfae4e4240505bd3ad72df274"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::~Matrix_Symmetric" ref="ae3c7bcbdfae4e4240505bd3ad72df274" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#ae3c7bcbdfae4e4240505bd3ad72df274">~Matrix_Symmetric</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Destructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a0ab7b853cb5e827503be5e65f2b39150">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the matrix.  <a href="#a0ab7b853cb5e827503be5e65f2b39150"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a8876d406208337c491b2e9d2f56de084">GetDataSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored in memory.  <a href="#a8876d406208337c491b2e9d2f56de084"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a488c4c92d4c5f2e867a8f008c7ba6e5d">Reallocate</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates memory to resize the matrix.  <a href="#a488c4c92d4c5f2e867a8f008c7ba6e5d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ada4cf9ceb82beaaf155f9c048bd19eb5"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::SetData" ref="ada4cf9ceb82beaaf155f9c048bd19eb5" args="(int i, int j, pointer data)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetData</b> (int i, int j, pointer data)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#ae36123ec57e9423228e4815604ce2d5c">Nullify</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the matrix without releasing memory.  <a href="#ae36123ec57e9423228e4815604ce2d5c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a2369e0b64027be77dd3ba9e3b77e4971">Resize</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates memory to resize the matrix and keeps previous entries.  <a href="#a2369e0b64027be77dd3ba9e3b77e4971"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a8a5cd6bfbb855e65a5e585e9e62c72f1">operator()</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a8a5cd6bfbb855e65a5e585e9e62c72f1"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#ab3bf006ebfa36eeafc3bd0b7a57f9d89">operator()</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#ab3bf006ebfa36eeafc3bd0b7a57f9d89"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a140aaa512f551fa698f84fc05c8df4c8">Val</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a140aaa512f551fa698f84fc05c8df4c8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a1db21879907e925a20a58ad332a98446">Val</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a1db21879907e925a20a58ad332a98446"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#af5312b65658c2a50f9c94a784940c1e0">Get</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the element (<em>i</em>, <em>j</em>).  <a href="#af5312b65658c2a50f9c94a784940c1e0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a8b74b9c2fbd076cfb10e87a5c4812094">Get</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the element (<em>i</em>, <em>j</em>).  <a href="#a8b74b9c2fbd076cfb10e87a5c4812094"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#aead099f326217ddb05c0782e58b0b1a5">operator[]</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access to elements of the data array.  <a href="#aead099f326217ddb05c0782e58b0b1a5"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a674815a3c75a714b790ee7dec3a46b4b">operator[]</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access to elements of the data array.  <a href="#a674815a3c75a714b790ee7dec3a46b4b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Matrix_Symmetric</a>&lt; T, Prop, <br class="typebreak"/>
Storage, Allocator &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#af554df9f69fd7674842120a4718f609b">operator=</a> (const <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a matrix (assignment operator).  <a href="#af554df9f69fd7674842120a4718f609b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a66c6a02ed5d50cbf204f7bd145d13880">Set</a> (int i, int j, const T &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets an element of the matrix.  <a href="#a66c6a02ed5d50cbf204f7bd145d13880"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a94f5e47677cc13311ab33879d2b05774">Copy</a> (const <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a matrix.  <a href="#a94f5e47677cc13311ab33879d2b05774"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a73066da717f1163947c11658fc7cba31">Zero</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets all elements to zero.  <a href="#a73066da717f1163947c11658fc7cba31"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7cab27ba8e4ee1d46cb8d5eca3123d42"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::SetIdentity" ref="a7cab27ba8e4ee1d46cb8d5eca3123d42" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a7cab27ba8e4ee1d46cb8d5eca3123d42">SetIdentity</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the matrix to the identity. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a9d34c8b60b6e577504a4264990194805">Fill</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the matrix with 0, 1, 2, ...  <a href="#a9d34c8b60b6e577504a4264990194805"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a966bcf7f7c204b9a8e2130ca93db0757">Fill</a> (const T0 &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the matrix with a given value.  <a href="#a966bcf7f7c204b9a8e2130ca93db0757"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Matrix_Symmetric</a>&lt; T, Prop, <br class="typebreak"/>
Storage, Allocator &gt; &amp;&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a13726b53c0a2db160c719e7d99b15274">operator=</a> (const T0 &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the matrix with a given value.  <a href="#a13726b53c0a2db160c719e7d99b15274"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a01a990a33e3e2b873b69587d8870d0c3">FillRand</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills a matrix randomly.  <a href="#a01a990a33e3e2b873b69587d8870d0c3"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a018e68da0aa00e2a3bb7d5b989b77837">Print</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays the matrix on the standard output.  <a href="#a018e68da0aa00e2a3bb7d5b989b77837"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a424f412164caf3674f49d4c30dbb9606">Print</a> (int a, int b, int m, int n) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays a sub-matrix on the standard output.  <a href="#a424f412164caf3674f49d4c30dbb9606"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#ac03b20710ef1613894a73f8d10b6154c">Print</a> (int l) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays a square sub-matrix on the standard output.  <a href="#ac03b20710ef1613894a73f8d10b6154c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a0af8f548546cddebba11f07e7f55f4d3">Write</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix in a file.  <a href="#a0af8f548546cddebba11f07e7f55f4d3"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a89eb858308a323e175f01cd6f1b70e42">Write</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix to an output stream.  <a href="#a89eb858308a323e175f01cd6f1b70e42"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#aaad47afa4f62b374a14e37d896a677a8">WriteText</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix in a file.  <a href="#aaad47afa4f62b374a14e37d896a677a8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a3ca6951c93777f346d79e8bdd227ea0e">WriteText</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix to an output stream.  <a href="#a3ca6951c93777f346d79e8bdd227ea0e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#af4e2d06a1044dd2c449384c580b347cd">Read</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from a file.  <a href="#af4e2d06a1044dd2c449384c580b347cd"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a911a42c752de06caed9614fed95bb8b5">Read</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from an input stream.  <a href="#a911a42c752de06caed9614fed95bb8b5"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a9ae2c1b1f8f80c87801b92dfc9a9a885">ReadText</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from a file.  <a href="#a9ae2c1b1f8f80c87801b92dfc9a9a885"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___symmetric.php#a72ba96f845054a5be5120c7a175c00c1">ReadText</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from an input stream.  <a href="#a72ba96f845054a5be5120c7a175c00c1"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">GetM</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows.  <a href="#a65e9c3f0db9c7c8c3fa10ead777dd927"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows of the matrix possibly transposed.  <a href="#ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">GetN</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns.  <a href="#a6b134070d1b890ba6b7eb72fee170984"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns of the matrix possibly transposed.  <a href="#a7d27412bc9384a5ce0c0db1ee310d31c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">GetSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements in the matrix.  <a href="#a0fbc3f7030583174eaa69429f655a1b0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer to the data array.  <a href="#a453a269dfe7fadba249064363d5ab92a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a const pointer to the data array.  <a href="#a0f2796430deb08e565df8ced656097bf"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "void*" to the data array.  <a href="#a505cdfee34741c463e0dc1943337bedc"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "const void*" to the data array.  <a href="#a5979450cd8801810229f4a24c36e6639"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">Allocator &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the allocator of the matrix.  <a href="#af88748a55208349367d6860ad76fd691"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af4f899d6546efa52f5ecb45d8ac2d2ef"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::me_" ref="af4f899d6546efa52f5ecb45d8ac2d2ef" args="" -->
pointer *&nbsp;</td><td class="memItemRight" valign="bottom"><b>me_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a96b39ff494d4b7d3e30f320a1c7b4aba"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::m_" ref="a96b39ff494d4b7d3e30f320a1c7b4aba" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>m_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a11cb5d502050e5f14482ff0c9cef5a76"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::n_" ref="a11cb5d502050e5f14482ff0c9cef5a76" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>n_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acb12a8b74699fae7ae3b2b67376795a1"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::data_" ref="acb12a8b74699fae7ae3b2b67376795a1" args="" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-static-attribs"></a>
Static Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a132fa01ce120f352d434fd920e5ad9b4"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::allocator_" ref="a132fa01ce120f352d434fd920e5ad9b4" args="" -->
static Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>allocator_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Prop, class Storage, class Allocator = SELDON_DEFAULT_ALLOCATOR&lt;T&gt;&gt;<br/>
 class Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</h3>

<p><a class="el" href="class_seldon_1_1_symmetric.php">Symmetric</a> matrix stored in a full matrix. </p>

<p>Definition at line <a class="el" href="_matrix___symmetric_8hxx_source.php#l00038">38</a> of file <a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="a82951e97ce120fda463beebe44d9c5e3"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Matrix_Symmetric" ref="a82951e97ce120fda463beebe44d9c5e3" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___symmetric.php">Matrix_Symmetric</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>On exit, the matrix is an empty 0x0 matrix. </p>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00037">37</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab4c39b6e45f2f22dbf8dd50ad8509ad7"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Matrix_Symmetric" ref="ab4c39b6e45f2f22dbf8dd50ad8509ad7" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___symmetric.php">Matrix_Symmetric</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Main constructor. </p>
<p>Builds a i x j full matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>'j' is assumed to be equal to 'i' and is therefore discarded. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00052">52</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="a0ab7b853cb5e827503be5e65f2b39150"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Clear" ref="a0ab7b853cb5e827503be5e65f2b39150" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::Clear </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the matrix. </p>
<p>Destructs the matrix. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>On exit, the matrix is an empty 0x0 matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00202">202</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a94f5e47677cc13311ab33879d2b05774"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Copy" ref="a94f5e47677cc13311ab33879d2b05774" args="(const Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop, class Storage, class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::Copy </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>A</em>&nbsp;</td><td>matrix to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'A' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00746">746</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9d34c8b60b6e577504a4264990194805"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Fill" ref="a9d34c8b60b6e577504a4264990194805" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::Fill </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the matrix with 0, 1, 2, ... </p>
<p>On exit, the matrix is filled with 0, 1, 2, 3, ... The order of those numbers depends on the storage. </p>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00790">790</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a966bcf7f7c204b9a8e2130ca93db0757"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Fill" ref="a966bcf7f7c204b9a8e2130ca93db0757" args="(const T0 &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::Fill </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the matrix with a given value. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>the value to fill the matrix with. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00803">803</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a01a990a33e3e2b873b69587d8870d0c3"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::FillRand" ref="a01a990a33e3e2b873b69587d8870d0c3" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::FillRand </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills a matrix randomly. </p>
<dl class="note"><dt><b>Note:</b></dt><dd>The random generator is very basic. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00830">830</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af5312b65658c2a50f9c94a784940c1e0"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Get" ref="af5312b65658c2a50f9c94a784940c1e0" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::reference <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::Get </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the element (<em>i</em>, <em>j</em>). </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00607">607</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a8b74b9c2fbd076cfb10e87a5c4812094"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Get" ref="a8b74b9c2fbd076cfb10e87a5c4812094" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::const_reference <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::Get </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the element (<em>i</em>, <em>j</em>). </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00639">639</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af88748a55208349367d6860ad76fd691"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::GetAllocator" ref="af88748a55208349367d6860ad76fd691" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">Allocator &amp; <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetAllocator </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the allocator of the matrix. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The allocator. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00258">258</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a453a269dfe7fadba249064363d5ab92a"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::GetData" ref="a453a269dfe7fadba249064363d5ab92a" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer to the data array. </p>
<p>Returns a pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00208">208</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0f2796430deb08e565df8ced656097bf"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::GetDataConst" ref="a0f2796430deb08e565df8ced656097bf" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::const_pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConst </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a const pointer to the data array. </p>
<p>Returns a const pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00221">221</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5979450cd8801810229f4a24c36e6639"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::GetDataConstVoid" ref="a5979450cd8801810229f4a24c36e6639" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConstVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "const void*" to the data array. </p>
<p>Returns a pointer of type "const void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00247">247</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a8876d406208337c491b2e9d2f56de084"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::GetDataSize" ref="a8876d406208337c491b2e9d2f56de084" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::GetDataSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored in memory. </p>
<p>Returns the number of elements stored in memory, i.e. the number of rows multiplied by the number of columns because the matrix is full. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements stored in memory. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00223">223</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a505cdfee34741c463e0dc1943337bedc"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::GetDataVoid" ref="a505cdfee34741c463e0dc1943337bedc" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "void*" to the data array. </p>
<p>Returns a pointer of type "void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00234">234</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a65e9c3f0db9c7c8c3fa10ead777dd927"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::GetM" ref="a65e9c3f0db9c7c8c3fa10ead777dd927" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a8c781e99310aae01786eac0e8e5aa50c">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00107">107</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::GetM" ref="ab6f6c5bba065ca82dad7c3abdbc8ea79" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#aecf3e8c636dbb83335ea588afe2558a8">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00130">130</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6b134070d1b890ba6b7eb72fee170984"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::GetN" ref="a6b134070d1b890ba6b7eb72fee170984" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a40580a92202044416e379c1304ff0220">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00118">118</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7d27412bc9384a5ce0c0db1ee310d31c"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::GetN" ref="a7d27412bc9384a5ce0c0db1ee310d31c" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a867066f7bf531bf7ad15bdcd034dc3c0">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00145">145</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0fbc3f7030583174eaa69429f655a1b0"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::GetSize" ref="a0fbc3f7030583174eaa69429f655a1b0" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements in the matrix. </p>
<p>Returns the number of elements in the matrix, i.e. the number of rows multiplied by the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements in the matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00195">195</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae36123ec57e9423228e4815604ce2d5c"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Nullify" ref="ae36123ec57e9423228e4815604ce2d5c" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::Nullify </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the matrix without releasing memory. </p>
<p>On exit, the matrix is empty and the memory has not been released. It is useful for low level manipulations on a <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> instance. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Memory is not released except for me_. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00401">401</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab3bf006ebfa36eeafc3bd0b7a57f9d89"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::operator()" ref="ab3bf006ebfa36eeafc3bd0b7a57f9d89" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::const_reference <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00510">510</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a8a5cd6bfbb855e65a5e585e9e62c72f1"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::operator()" ref="a8a5cd6bfbb855e65a5e585e9e62c72f1" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::reference <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00478">478</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a13726b53c0a2db160c719e7d99b15274"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::operator=" ref="a13726b53c0a2db160c719e7d99b15274" args="(const T0 &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the matrix with a given value. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>the value to fill the matrix with. </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php#a02437899b3e29faaf714a28429b66de2">Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_00_01_allocator_01_4.php#a206f2265db8f733fbf11d574717d1e01">Seldon::Matrix&lt; T, Prop, RowSym, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00817">817</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af554df9f69fd7674842120a4718f609b"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::operator=" ref="af554df9f69fd7674842120a4718f609b" args="(const Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop, class Storage, class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a matrix (assignment operator). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>A</em>&nbsp;</td><td>matrix to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'A' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00716">716</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aead099f326217ddb05c0782e58b0b1a5"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::operator[]" ref="aead099f326217ddb05c0782e58b0b1a5" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::reference <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::operator[] </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access to elements of the data array. </p>
<p>Provides a direct access to the data array. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>i-th element of the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00668">668</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a674815a3c75a714b790ee7dec3a46b4b"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::operator[]" ref="a674815a3c75a714b790ee7dec3a46b4b" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::const_reference <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::operator[] </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access to elements of the data array. </p>
<p>Provides a direct access to the data array. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>i-th element of the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00692">692</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a018e68da0aa00e2a3bb7d5b989b77837"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Print" ref="a018e68da0aa00e2a3bb7d5b989b77837" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::Print </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Displays the matrix on the standard output. </p>
<p>Displays elements on the standard output, in text format. Each row is displayed on a single line and elements of a row are delimited by tabulations. </p>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00845">845</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac03b20710ef1613894a73f8d10b6154c"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Print" ref="ac03b20710ef1613894a73f8d10b6154c" args="(int l) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::Print </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>l</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Displays a square sub-matrix on the standard output. </p>
<p>The sub-matrix is defined by its bottom-right corner (l, l). So, elements with indices in [0, 0] x [l, l] are displayed on the standard output, in text format. Each row is displayed on a single line and elements of a row are delimited by tabulations. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>l</em>&nbsp;</td><td>dimension of the square matrix to be displayed. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00891">891</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a424f412164caf3674f49d4c30dbb9606"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Print" ref="a424f412164caf3674f49d4c30dbb9606" args="(int a, int b, int m, int n) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::Print </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>a</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>b</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>m</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>n</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Displays a sub-matrix on the standard output. </p>
<p>The sub-matrix is defined by its upper-left corner (a, b) and its bottom-right corner (m, n). So, elements with indices in [a, m] x [b, n] are displayed on the standard output, in text format. Each row is displayed on a single line and elements of a row are delimited by tabulations. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>a</em>&nbsp;</td><td>row index of the upper-left corner. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>b</em>&nbsp;</td><td>column index of the upper-left corner. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>m</em>&nbsp;</td><td>row index of the bottom-right corner. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>n</em>&nbsp;</td><td>column index of the bottom-right corner. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00870">870</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a911a42c752de06caed9614fed95bb8b5"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Read" ref="a911a42c752de06caed9614fed95bb8b5" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from an input stream. </p>
<p>Reads a matrix in binary format from an input stream. The number of rows (integer) and the number of columns (integer) are read, and matrix elements are then read in the same order as it should be in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>input stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l01077">1077</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af4e2d06a1044dd2c449384c580b347cd"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Read" ref="af4e2d06a1044dd2c449384c580b347cd" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from a file. </p>
<p>Reads a matrix stored in binary format in a file. The number of rows (integer) and the number of columns (integer) are read, and matrix elements are then read in the same order as it should be in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>input file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l01049">1049</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9ae2c1b1f8f80c87801b92dfc9a9a885"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::ReadText" ref="a9ae2c1b1f8f80c87801b92dfc9a9a885" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from a file. </p>
<p>Reads a matrix stored in text format in a file. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>input file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l01113">1113</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a72ba96f845054a5be5120c7a175c00c1"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::ReadText" ref="a72ba96f845054a5be5120c7a175c00c1" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from an input stream. </p>
<p>Reads a matrix in text format from an input stream. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>input stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l01138">1138</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a488c4c92d4c5f2e867a8f008c7ba6e5d"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Reallocate" ref="a488c4c92d4c5f2e867a8f008c7ba6e5d" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::Reallocate </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates memory to resize the matrix. </p>
<p>On exit, the matrix is a i x i matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Depending on your allocator, data may be lost. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00243">243</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2369e0b64027be77dd3ba9e3b77e4971"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Resize" ref="a2369e0b64027be77dd3ba9e3b77e4971" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::Resize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates memory to resize the matrix and keeps previous entries. </p>
<p>On exit, the matrix is a i x j matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>new number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>new number of columns. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>The previous entries are kept, extra-entries may not be initialized (depending of the allocator). </dd></dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00441">441</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a66c6a02ed5d50cbf204f7bd145d13880"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Set" ref="a66c6a02ed5d50cbf204f7bd145d13880" args="(int i, int j, const T &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::Set </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const T &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets an element of the matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>new value for the matrix element (<em>i</em>, <em>j</em>). </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00732">732</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1db21879907e925a20a58ad332a98446"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Val" ref="a1db21879907e925a20a58ad332a98446" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::reference <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::Val </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00574">574</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a140aaa512f551fa698f84fc05c8df4c8"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Val" ref="a140aaa512f551fa698f84fc05c8df4c8" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___symmetric.php">Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::const_reference <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::Val </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00541">541</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0af8f548546cddebba11f07e7f55f4d3"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Write" ref="a0af8f548546cddebba11f07e7f55f4d3" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix in a file. </p>
<p>Stores the matrix in a file in binary format. The number of rows (integer) and the number of columns (integer) are written, and matrix elements are then written in the same order as in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00912">912</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a89eb858308a323e175f01cd6f1b70e42"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Write" ref="a89eb858308a323e175f01cd6f1b70e42" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix to an output stream. </p>
<p>Writes the matrix to an output stream in binary format. The number of rows (integer) and the number of columns (integer) are written, and matrix elements are then written in the same order as in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00940">940</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aaad47afa4f62b374a14e37d896a677a8"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::WriteText" ref="aaad47afa4f62b374a14e37d896a677a8" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix in a file. </p>
<p>Stores the matrix in a file in text format. Only matrix elements are written (not dimensions). Each row is written on a single line and elements of a row are delimited by tabulations. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00980">980</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3ca6951c93777f346d79e8bdd227ea0e"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::WriteText" ref="a3ca6951c93777f346d79e8bdd227ea0e" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix to an output stream. </p>
<p>Writes the matrix to an output stream in text format. Only matrix elements are written (not dimensions). Each row is written on a single line and elements of a row are delimited by tabulations. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l01010">1010</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a73066da717f1163947c11658fc7cba31"></a><!-- doxytag: member="Seldon::Matrix_Symmetric::Zero" ref="a73066da717f1163947c11658fc7cba31" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___symmetric.php">Seldon::Matrix_Symmetric</a>&lt; T, Prop, Storage, Allocator &gt;::Zero </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets all elements to zero. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>It fills the memory with zeros. If the matrix stores complex structures, use 'Fill' instead. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___symmetric_8cxx_source.php#l00765">765</a> of file <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>matrix/<a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a></li>
<li>matrix/<a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
