<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="tabs2">
    <ul class="tablist">
      <li class="current"><a href="functions.php"><span>All</span></a></li>
      <li><a href="functions_func.php"><span>Functions</span></a></li>
      <li><a href="functions_vars.php"><span>Variables</span></a></li>
      <li><a href="functions_type.php"><span>Typedefs</span></a></li>
    </ul>
  </div>
  <div class="tabs3">
    <ul class="tablist">
      <li><a href="functions.php#index_a"><span>a</span></a></li>
      <li><a href="functions_0x63.php#index_c"><span>c</span></a></li>
      <li><a href="functions_0x64.php#index_d"><span>d</span></a></li>
      <li><a href="functions_0x65.php#index_e"><span>e</span></a></li>
      <li><a href="functions_0x66.php#index_f"><span>f</span></a></li>
      <li><a href="functions_0x67.php#index_g"><span>g</span></a></li>
      <li><a href="functions_0x68.php#index_h"><span>h</span></a></li>
      <li><a href="functions_0x69.php#index_i"><span>i</span></a></li>
      <li><a href="functions_0x6c.php#index_l"><span>l</span></a></li>
      <li><a href="functions_0x6d.php#index_m"><span>m</span></a></li>
      <li><a href="functions_0x6e.php#index_n"><span>n</span></a></li>
      <li><a href="functions_0x6f.php#index_o"><span>o</span></a></li>
      <li><a href="functions_0x70.php#index_p"><span>p</span></a></li>
      <li><a href="functions_0x72.php#index_r"><span>r</span></a></li>
      <li><a href="functions_0x73.php#index_s"><span>s</span></a></li>
      <li><a href="functions_0x74.php#index_t"><span>t</span></a></li>
      <li><a href="functions_0x75.php#index_u"><span>u</span></a></li>
      <li class="current"><a href="functions_0x76.php#index_v"><span>v</span></a></li>
      <li><a href="functions_0x77.php#index_w"><span>w</span></a></li>
      <li><a href="functions_0x78.php#index_x"><span>x</span></a></li>
      <li><a href="functions_0x7a.php#index_z"><span>z</span></a></li>
      <li><a href="functions_0x7e.php#index_~"><span>~</span></a></li>
    </ul>
  </div>
<div class="contents">
Here is a list of all documented class members with links to the class documentation for each member:

<h3><a class="anchor" id="index_v"></a>- v -</h3><ul>
<li>Val()
: <a class="el" href="class_seldon_1_1_matrix___hermitian.php#a717f22a07d28b33751253ca330667b7c">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a7eda051ec34190ad13528f89d233acdb">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___symmetric.php#a1db21879907e925a20a58ad332a98446">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_petsc_matrix.php#ad780213ae6138a9a9532c02806985625">Seldon::PetscMatrix&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a21f192e6ea09ce5be473e6bb1135c7a9">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a58c123d74c337c990ffeb7f18e911a5a">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a8bcf6acc85a1b7d64735db18587896d9">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a89b88a7b794f756874d0e0a40be8cd01">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7eb05d6902b13cfb7cc4f17093fdc17e">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___pointers.php#afca09e0690510a4a40fe022e62677b6e">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_packed.php#ae7199787c1e2ee658a1590e7e1dd45a8">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a994fbc16dd258d1609f7c7c582205e9f">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#ad114c8c6e002a7a0def70f2413a32659">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a0d4ea94aa72456c633fd26385f28f7a2">Seldon::Matrix_TriangPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a130c7307de87672e3878362386481775">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a7a12553a1f4b81044e5def6cab89a7a1">Seldon::Matrix&lt; T, Prop, ArrayColSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___herm_packed.php#ae7f67ca8d3876a5fbdbce8ac3c914f36">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___pointers.php#a3d83977c9b496a6c1745a8e41f79af5c">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a2106123c9da877618fae231860846f6f">Seldon::Matrix_TriangPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a51307b7343833f1754ad2f359449d202">Seldon::Matrix&lt; T, Prop, ArrayColSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#ae0e5492ee09498fcd376ec76469c1034">Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triangular.php#aca8b82d5764a1943b2ec9a36d502816f">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a97036f8d9ca0c12cf1bb95d8aafce4a0">Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sparse.php#a7f416147b6680860cd489dfa6cb08b58">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___symmetric.php#a140aaa512f551fa698f84fc05c8df4c8">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triangular.php#ab2e953becbaab6664f72752932e14f5c">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sparse.php#a508d4a87841d185e00cbac5223dfc91f">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#aaca494c5d6a5c03b5a76c3cfbfc412fe">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_petsc_matrix.php#a0353d3a236f3f6f3a688b5e422a5e568">Seldon::PetscMatrix&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a30ac9be8e0a8f70f3c2eae3f2e6657bb">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a5805ff818cc9c57a65bc6006c58564a7">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>
</li>
<li>val_
: <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>val_imag_
: <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>val_real_
: <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>ValImag()
: <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aee8de4c9149d47935bf84fc04727c7f2">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a1b6113bbca815576f929be337af47483">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a8584e87cfb5ba75a746ec60a926f23a4">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a9d49fb0a1544b3eda52ee5e002d0822d">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a4cf220882aeebe97ab0cd8afb1eb52c7">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>ValReal()
: <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#abec4edb809f2be635bff90b508f9e9f6">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#abdacf01dc354d0449b16b1d9e111ec54">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a9edb81b7ae21d2118c6cc4d98058d731">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#abc507579e215d0fcd88b8d2bf8e710b9">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a928e2aa74c9ad336e77ab76593b3c8da">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a0e58d558d63fc51d62db121f9732cf1c">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>Value()
: <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#ad733400e0cde2e94b5efd076dc5d956e">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a51c6d9be9bfc1234e17d997f9bcdff07">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>
</li>
<li>ValueImag()
: <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa2d9a2e69a269bf9d15b62d520cb560d">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>ValueReal()
: <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a11fe8648ddd3e6e339eb81735d7e544e">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>Vector()
: <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a95f3ecfa8b557f41194fe506d0af7d5e">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a1afba3b1f5b3d8022dd4e047185968d8">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a1cf9876e5d323efa8918954a01679394">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a3220e19bce6b4a0f72dfca147dbf8b38">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a93033ed7da4e225e1cc183fb963a1315">Seldon::Vector&lt; T, PETScPar, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a1f201ae576bd2dd972fc238206a3a724">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a150f79bf5e3a4c93eb6c3c13ac8e2ea8">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a1c6444722ded7b4443326eeaa32ebe1c">Seldon::Vector&lt; T, PETScPar, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a690f575c3d09274138868ce4ab2e5970">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a95b6b2c8c2be95a9d5dcdbd892f195e2">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#aaef02756d3c6c0a97454f35bffc07962">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#aee3f9a5bfd70e6370ae0c559257624f7">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a5742670006ef46408c215455d8da7b8d">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#af2ab08543ecdfb8a09d01e4c024607ff">Seldon::Vector&lt; T, PETScPar, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#aa33cfa4d38f149671d6d0ea82f5106d8">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php#a58773b566b67c3bc9830a56f5a4e0499">Seldon::Vector&lt; T, PETScPar, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php#a48375793ed0a05a7c62a872568386539">Seldon::Vector&lt; T, PETScSeq, Allocator &gt;</a>
</li>
<li>Vector2()
: <a class="el" href="class_seldon_1_1_vector2.php#a00822908f0850cae2262f884297d579e">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
</li>
<li>Vector3()
: <a class="el" href="class_seldon_1_1_vector3.php#a9fff1c8ce72eb5dc5c002b6f1c0e7b86">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
</li>
<li>Vector_Base()
: <a class="el" href="class_seldon_1_1_vector___base.php#a27796125b93f3a771a5c92e8196c9229">Seldon::Vector_Base&lt; T, Allocator &gt;</a>
</li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
