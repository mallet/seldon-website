<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix_3_01_float_double_00_01_general_00_01_dense_sparse_collection_00_01_allocator_3_01double_01_4_01_4.php">Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a> &#124;
<a href="#pro-static-attribs">Static Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;" --><!-- doxytag: inherits="HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;" -->
<p>Heterogeneous matrix collection class.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_heterogeneous_matrix_collection_8hxx_source.php">HeterogeneousMatrixCollection.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_matrix_3_01_float_double_00_01_general_00_01_dense_sparse_collection_00_01_allocator_3_01double_01_4_01_4.png" usemap="#Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;_map" alt=""/>
  <map id="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;_map" name="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;_map">
<area href="class_seldon_1_1_heterogeneous_matrix_collection.php" alt="Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;" shape="rect" coords="0,56,556,80"/>
<area href="class_seldon_1_1_matrix___base.php" alt="Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;" shape="rect" coords="0,0,556,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_matrix_3_01_float_double_00_01_general_00_01_dense_sparse_collection_00_01_allo09d6e1154850230f01b42a9bb785a3ab.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a51053c98bbbbb8c7aa131a8c828d6bb3"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::property" ref="a51053c98bbbbb8c7aa131a8c828d6bb3" args="" -->
typedef <a class="el" href="class_seldon_1_1_general.php">General</a>&nbsp;</td><td class="memItemRight" valign="bottom"><b>property</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afbba78c118caef916c5cbb676fb27122"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::storage" ref="afbba78c118caef916c5cbb676fb27122" args="" -->
typedef <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>&nbsp;</td><td class="memItemRight" valign="bottom"><b>storage</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ace59cebf229675df507d9a54e8cb8133"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::allocator" ref="ace59cebf229675df507d9a54e8cb8133" args="" -->
typedef Allocator&lt; double &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>allocator</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac35261f04ec95ce8324dcdfca25002f5"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::float_dense_m" ref="ac35261f04ec95ce8324dcdfca25002f5" args="" -->
typedef <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; float, <a class="el" href="class_seldon_1_1_general.php">General</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, Allocator&lt; float &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>float_dense_m</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3a449c59e72ebe42f62f67eadd82db3f"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::float_sparse_m" ref="a3a449c59e72ebe42f62f67eadd82db3f" args="" -->
typedef <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; float, <a class="el" href="class_seldon_1_1_general.php">General</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator&lt; float &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>float_sparse_m</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1529a24b3c2dbe8eb1669962fc1884d6"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::double_dense_m" ref="a1529a24b3c2dbe8eb1669962fc1884d6" args="" -->
typedef <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; double, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_general.php">General</a>, <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, Allocator<br class="typebreak"/>
&lt; double &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>double_dense_m</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afa02c20e34846d05e6c0d5cd5280cc3d"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::double_sparse_m" ref="afa02c20e34846d05e6c0d5cd5280cc3d" args="" -->
typedef <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; double, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_general.php">General</a>, <a class="el" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator<br class="typebreak"/>
&lt; double &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>double_sparse_m</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a33b3985bd348f768034c562fd16a4f3c"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::float_dense_c" ref="a33b3985bd348f768034c562fd16a4f3c" args="" -->
typedef <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; <a class="el" href="class_seldon_1_1_matrix.php">float_dense_m</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_general.php">General</a>, <a class="el" href="class_seldon_1_1_row_major_collection.php">RowMajorCollection</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a>&lt; <a class="el" href="class_seldon_1_1_matrix.php">float_dense_m</a> &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>float_dense_c</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a28f73456cd9a9b5816aa0c80095b7884"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::float_sparse_c" ref="a28f73456cd9a9b5816aa0c80095b7884" args="" -->
typedef <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; <a class="el" href="class_seldon_1_1_matrix.php">float_sparse_m</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_general.php">General</a>, <a class="el" href="class_seldon_1_1_row_major_collection.php">RowMajorCollection</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a>&lt; <a class="el" href="class_seldon_1_1_matrix.php">float_sparse_m</a> &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>float_sparse_c</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7c20722ed8cad089e383b332b7ec30d9"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::double_dense_c" ref="a7c20722ed8cad089e383b332b7ec30d9" args="" -->
typedef <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; <a class="el" href="class_seldon_1_1_matrix.php">double_dense_m</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_general.php">General</a>, <a class="el" href="class_seldon_1_1_row_major_collection.php">RowMajorCollection</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a>&lt; <a class="el" href="class_seldon_1_1_matrix.php">double_dense_m</a> &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>double_dense_c</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab2d2280f2f97f03d80f67e14ec5981a7"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::double_sparse_c" ref="ab2d2280f2f97f03d80f67e14ec5981a7" args="" -->
typedef <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a><br class="typebreak"/>
&lt; <a class="el" href="class_seldon_1_1_matrix.php">double_sparse_m</a>, <a class="el" href="class_seldon_1_1_general.php">General</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_row_major_collection.php">RowMajorCollection</a>, <a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a><br class="typebreak"/>
&lt; <a class="el" href="class_seldon_1_1_matrix.php">double_sparse_m</a> &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>double_sparse_c</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a09596a54396d6e21a9848fe2e8554efd"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::value_type" ref="a09596a54396d6e21a9848fe2e8554efd" args="" -->
typedef Allocator&lt; double &gt;<br class="typebreak"/>
::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9c13a3062dcae2e438ad6845e8920a97"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::pointer" ref="a9c13a3062dcae2e438ad6845e8920a97" args="" -->
typedef Allocator&lt; double &gt;<br class="typebreak"/>
::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adac539c8208073ffd6214e9cefbed0f6"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::const_pointer" ref="adac539c8208073ffd6214e9cefbed0f6" args="" -->
typedef Allocator&lt; double &gt;<br class="typebreak"/>
::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5ed3e9a1cc5071fdfcde5ce231932324"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::reference" ref="a5ed3e9a1cc5071fdfcde5ce231932324" args="" -->
typedef Allocator&lt; double &gt;<br class="typebreak"/>
::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0a4a4810c1d0a16655843bdd62bc8f60"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::const_reference" ref="a0a4a4810c1d0a16655843bdd62bc8f60" args="" -->
typedef Allocator&lt; double &gt;<br class="typebreak"/>
::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_float_double_00_01_general_00_01_dense_sparse_collection_00_01_allocator_3_01double_01_4_01_4.php#a20314db5f8dd0fcf4dd175e4b1a4852f">Matrix</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#a20314db5f8dd0fcf4dd175e4b1a4852f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_float_double_00_01_general_00_01_dense_sparse_collection_00_01_allocator_3_01double_01_4_01_4.php#a80ce2837902243f345157a46095ac8ba">Matrix</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Main constructor.  <a href="#a80ce2837902243f345157a46095ac8ba"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0324574cd55a0799aa9746b36b9b20a1"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::Clear" ref="a0324574cd55a0799aa9746b36b9b20a1" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Clear</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3e48adb67f58b6355dc7ce39611585cc"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::Nullify" ref="a3e48adb67f58b6355dc7ce39611585cc" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Nullify</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a19f1fe843dbb872c589db2116cd204f9"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::Nullify" ref="a19f1fe843dbb872c589db2116cd204f9" args="(int i, int j)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Nullify</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a34264880bdeb3febb8c73b2cad0b4588"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::Deallocate" ref="a34264880bdeb3febb8c73b2cad0b4588" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Deallocate</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7b22f363d1535f911ee271f1e0743c6e"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetM" ref="a7b22f363d1535f911ee271f1e0743c6e" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetM</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac368f84e127c6738f7f06c74bbc6e7da"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetM" ref="ac368f84e127c6738f7f06c74bbc6e7da" args="(int i) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetM</b> (int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetM" ref="ab6f6c5bba065ca82dad7c3abdbc8ea79" args="(const Seldon::SeldonTranspose &amp;status) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetM</b> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a38820efd26db52feaeb8998d2fd43daa"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetMmatrix" ref="a38820efd26db52feaeb8998d2fd43daa" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetMmatrix</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a03dbde09b4beb73bd66628b4db00df0d"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetN" ref="a03dbde09b4beb73bd66628b4db00df0d" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetN</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3b788a9ac72e2f32842dcc9251c6ca0f"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetN" ref="a3b788a9ac72e2f32842dcc9251c6ca0f" args="(int j) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetN</b> (int j) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7d27412bc9384a5ce0c0db1ee310d31c"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetN" ref="a7d27412bc9384a5ce0c0db1ee310d31c" args="(const Seldon::SeldonTranspose &amp;status) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetN</b> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6c59c77733659c863477c9a34fc927fe"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetNmatrix" ref="a6c59c77733659c863477c9a34fc927fe" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetNmatrix</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae5570f5b9a4dac52024ced4061cfe5a8"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetSize" ref="ae5570f5b9a4dac52024ced4061cfe5a8" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetSize</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2a36130a12ef3b4e4a4971f5898ca0bd"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetDataSize" ref="a2a36130a12ef3b4e4a4971f5898ca0bd" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataSize</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a69858677f0823951f46671248091be2e"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetType" ref="a69858677f0823951f46671248091be2e" args="(int i, int j) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetType</b> (int i, int j) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9bd6c070c0738b3425e5e9b3e0118ec6"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetFloatDense" ref="a9bd6c070c0738b3425e5e9b3e0118ec6" args="()" -->
<a class="el" href="class_seldon_1_1_matrix.php">float_dense_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetFloatDense</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa53a233cf9ea59c8711247cc013d75b7"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetFloatDense" ref="aa53a233cf9ea59c8711247cc013d75b7" args="() const" -->
const <a class="el" href="class_seldon_1_1_matrix.php">float_dense_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetFloatDense</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a69cf73dbc6d9055bc8ad28633de18f24"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetFloatSparse" ref="a69cf73dbc6d9055bc8ad28633de18f24" args="()" -->
<a class="el" href="class_seldon_1_1_matrix.php">float_sparse_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetFloatSparse</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afbdb423cb862ab27779c8e526f4f05e6"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetFloatSparse" ref="afbdb423cb862ab27779c8e526f4f05e6" args="() const" -->
const <a class="el" href="class_seldon_1_1_matrix.php">float_sparse_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetFloatSparse</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a289e9f762721738e42b9907450e72e9f"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetDoubleDense" ref="a289e9f762721738e42b9907450e72e9f" args="()" -->
<a class="el" href="class_seldon_1_1_matrix.php">double_dense_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDoubleDense</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9bc398080b4454e0bcbf968fd25fbd78"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetDoubleDense" ref="a9bc398080b4454e0bcbf968fd25fbd78" args="() const" -->
const <a class="el" href="class_seldon_1_1_matrix.php">double_dense_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDoubleDense</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a39301f5774586f67d31f64a3d4acb0af"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetDoubleSparse" ref="a39301f5774586f67d31f64a3d4acb0af" args="()" -->
<a class="el" href="class_seldon_1_1_matrix.php">double_sparse_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDoubleSparse</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1a4f404541bb2b70a36cc1a193ee5cb7"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetDoubleSparse" ref="a1a4f404541bb2b70a36cc1a193ee5cb7" args="() const" -->
const <a class="el" href="class_seldon_1_1_matrix.php">double_sparse_c</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDoubleSparse</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae600c754d08ba0af893d118cd3a38c60"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::Reallocate" ref="ae600c754d08ba0af893d118cd3a38c60" args="(int i, int j)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Reallocate</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3f15b1b92a4f05a353a553597b18a718"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::SetMatrix" ref="a3f15b1b92a4f05a353a553597b18a718" args="(int m, int n, const float_dense_m &amp;)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetMatrix</b> (int m, int n, const <a class="el" href="class_seldon_1_1_matrix.php">float_dense_m</a> &amp;)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa14610a44bc6121227676f7f8a4d3092"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::SetMatrix" ref="aa14610a44bc6121227676f7f8a4d3092" args="(int m, int n, const float_sparse_m &amp;)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetMatrix</b> (int m, int n, const <a class="el" href="class_seldon_1_1_matrix.php">float_sparse_m</a> &amp;)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aba9733f51050379e7d6511a423ed18a2"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::SetMatrix" ref="aba9733f51050379e7d6511a423ed18a2" args="(int m, int n, const double_dense_m &amp;)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetMatrix</b> (int m, int n, const <a class="el" href="class_seldon_1_1_matrix.php">double_dense_m</a> &amp;)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae7b70308ce05f06619b770261c2ec3b4"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::SetMatrix" ref="ae7b70308ce05f06619b770261c2ec3b4" args="(int m, int n, const double_sparse_m &amp;)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetMatrix</b> (int m, int n, const <a class="el" href="class_seldon_1_1_matrix.php">double_sparse_m</a> &amp;)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a652965504371cdec893ace70c4a66502"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetMatrix" ref="a652965504371cdec893ace70c4a66502" args="(int m, int n, float_dense_m &amp;) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetMatrix</b> (int m, int n, <a class="el" href="class_seldon_1_1_matrix.php">float_dense_m</a> &amp;) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab1fe3083dfb9e0e0c55f4ab0551aaeaa"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetMatrix" ref="ab1fe3083dfb9e0e0c55f4ab0551aaeaa" args="(int m, int n, float_sparse_m &amp;) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetMatrix</b> (int m, int n, <a class="el" href="class_seldon_1_1_matrix.php">float_sparse_m</a> &amp;) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad97ac078f1e871776f18f27486e8f1f1"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetMatrix" ref="ad97ac078f1e871776f18f27486e8f1f1" args="(int m, int n, double_dense_m &amp;) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetMatrix</b> (int m, int n, <a class="el" href="class_seldon_1_1_matrix.php">double_dense_m</a> &amp;) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9efe3f3c96b6afca9bcc1c8d60871531"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetMatrix" ref="a9efe3f3c96b6afca9bcc1c8d60871531" args="(int m, int n, double_sparse_m &amp;) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetMatrix</b> (int m, int n, <a class="el" href="class_seldon_1_1_matrix.php">double_sparse_m</a> &amp;) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="accc5a22477245a42cd0e017157f7d17d"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::operator()" ref="accc5a22477245a42cd0e017157f7d17d" args="(int i, int j) const" -->
double&nbsp;</td><td class="memItemRight" valign="bottom"><b>operator()</b> (int i, int j) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a91100b6eb7a2cea5e86d191ee58749bf"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::Copy" ref="a91100b6eb7a2cea5e86d191ee58749bf" args="(const HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt; &amp;A)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Copy</b> (const <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">HeterogeneousMatrixCollection</a>&lt; <a class="el" href="class_seldon_1_1_general.php">General</a>, <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, <a class="el" href="class_seldon_1_1_general.php">General</a>, <a class="el" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator &gt; &amp;A)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a788108f4822afc37c6ef4054b37a03bc"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::Print" ref="a788108f4822afc37c6ef4054b37a03bc" args="() const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Print</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3d95d8a80c5fdd1d5aea9decf6d4e544"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::Write" ref="a3d95d8a80c5fdd1d5aea9decf6d4e544" args="(string FileName, bool with_size) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Write</b> (string FileName, bool with_size) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0c289b98c4bd4ea56dab00a13b88761b"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::Write" ref="a0c289b98c4bd4ea56dab00a13b88761b" args="(ostream &amp;FileStream, bool with_size) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Write</b> (ostream &amp;FileStream, bool with_size) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac56a7ef93f6f0a217795ce3166b9edae"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::WriteText" ref="ac56a7ef93f6f0a217795ce3166b9edae" args="(string FileName) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>WriteText</b> (string FileName) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a85ddf0c1055330e18f77175da318432e"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::WriteText" ref="a85ddf0c1055330e18f77175da318432e" args="(ostream &amp;FileStream) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>WriteText</b> (ostream &amp;FileStream) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a11761a3a59b566c4e2d4ae2e020d1b24"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::Read" ref="a11761a3a59b566c4e2d4ae2e020d1b24" args="(string FileName)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Read</b> (string FileName)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2c15862a4b7347bff455204fab3ea161"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::Read" ref="a2c15862a4b7347bff455204fab3ea161" args="(istream &amp;FileStream)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Read</b> (istream &amp;FileStream)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a453a269dfe7fadba249064363d5ab92a"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetData" ref="a453a269dfe7fadba249064363d5ab92a" args="() const" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetData</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0f2796430deb08e565df8ced656097bf"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetDataConst" ref="a0f2796430deb08e565df8ced656097bf" args="() const" -->
const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataConst</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a505cdfee34741c463e0dc1943337bedc"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetDataVoid" ref="a505cdfee34741c463e0dc1943337bedc" args="() const" -->
void *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataVoid</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5979450cd8801810229f4a24c36e6639"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetDataConstVoid" ref="a5979450cd8801810229f4a24c36e6639" args="() const" -->
const void *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataConstVoid</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af88748a55208349367d6860ad76fd691"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::GetAllocator" ref="af88748a55208349367d6860ad76fd691" args="()" -->
Allocator&lt; double &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetAllocator</b> ()</td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3137de2c1fe161a746dbde1f10c9128e"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::nz_" ref="a3137de2c1fe161a746dbde1f10c9128e" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3137de2c1fe161a746dbde1f10c9128e">nz_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of non-zero elements. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a67cb0eb87ea633caabecdc58714723ec"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::Mmatrix_" ref="a67cb0eb87ea633caabecdc58714723ec" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a67cb0eb87ea633caabecdc58714723ec">Mmatrix_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of rows of matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a37c185f5d163c0f8a32828828fde7252"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::Nmatrix_" ref="a37c185f5d163c0f8a32828828fde7252" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a37c185f5d163c0f8a32828828fde7252">Nmatrix_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of columns of matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a48d1d3d9da13f7923a6356e49815fe66"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::Mlocal_" ref="a48d1d3d9da13f7923a6356e49815fe66" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_calloc_alloc.php">CallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a48d1d3d9da13f7923a6356e49815fe66">Mlocal_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of rows in the underlying matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af934beebf115742be0a29cb452017a7b"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::Mlocal_sum_" ref="af934beebf115742be0a29cb452017a7b" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_calloc_alloc.php">CallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#af934beebf115742be0a29cb452017a7b">Mlocal_sum_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Cumulative number of rows in the underlying matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6946bf5a2c2a2dbaf307ce2ed1feea09"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::Nlocal_" ref="a6946bf5a2c2a2dbaf307ce2ed1feea09" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_calloc_alloc.php">CallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a6946bf5a2c2a2dbaf307ce2ed1feea09">Nlocal_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of columns in the underlying matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0d31be2a9c37790f5e6ec86ef556ac82"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::Nlocal_sum_" ref="a0d31be2a9c37790f5e6ec86ef556ac82" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_calloc_alloc.php">CallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a0d31be2a9c37790f5e6ec86ef556ac82">Nlocal_sum_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Cumulative number of columns in the underlying matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; int, <a class="el" href="class_seldon_1_1_general.php">General</a>, <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_calloc_alloc.php">CallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a068b8212aa98d09c129bb1952aeee722">collection_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Type of the underlying matrices.  <a href="#a068b8212aa98d09c129bb1952aeee722"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9ecb39a2c309d9cec57f7e97b448771d"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::float_dense_c_" ref="a9ecb39a2c309d9cec57f7e97b448771d" args="" -->
<a class="el" href="class_seldon_1_1_matrix.php">float_dense_c</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a9ecb39a2c309d9cec57f7e97b448771d">float_dense_c_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Pointers of the underlying float dense matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a66e2ad59a6007f1dd4be614a8ade4b85"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::float_sparse_c_" ref="a66e2ad59a6007f1dd4be614a8ade4b85" args="" -->
<a class="el" href="class_seldon_1_1_matrix.php">float_sparse_c</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a66e2ad59a6007f1dd4be614a8ade4b85">float_sparse_c_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Pointers of the underlying float sparse matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aedc9e7d9e1d59170fce9cf501899b0f6"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::double_dense_c_" ref="aedc9e7d9e1d59170fce9cf501899b0f6" args="" -->
<a class="el" href="class_seldon_1_1_matrix.php">double_dense_c</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aedc9e7d9e1d59170fce9cf501899b0f6">double_dense_c_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Pointers of the underlying double dense matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8743b791c26f3513a2e911812083e482"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::double_sparse_c_" ref="a8743b791c26f3513a2e911812083e482" args="" -->
<a class="el" href="class_seldon_1_1_matrix.php">double_sparse_c</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a8743b791c26f3513a2e911812083e482">double_sparse_c_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Pointers of the underlying double sparse matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a96b39ff494d4b7d3e30f320a1c7b4aba"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::m_" ref="a96b39ff494d4b7d3e30f320a1c7b4aba" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>m_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a11cb5d502050e5f14482ff0c9cef5a76"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::n_" ref="a11cb5d502050e5f14482ff0c9cef5a76" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>n_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acb12a8b74699fae7ae3b2b67376795a1"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::data_" ref="acb12a8b74699fae7ae3b2b67376795a1" args="" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-static-attribs"></a>
Static Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a132fa01ce120f352d434fd920e5ad9b4"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::allocator_" ref="a132fa01ce120f352d434fd920e5ad9b4" args="" -->
static Allocator&lt; double &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>allocator_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;template&lt; class U &gt; class Allocator&gt;<br/>
 class Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;</h3>

<p>Heterogeneous matrix collection class. </p>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8hxx_source.php#l00179">179</a> of file <a class="el" href="_heterogeneous_matrix_collection_8hxx_source.php">HeterogeneousMatrixCollection.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="a20314db5f8dd0fcf4dd175e4b1a4852f"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::Matrix" ref="a20314db5f8dd0fcf4dd175e4b1a4852f" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_general.php">General</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; double &gt; &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>On exit, the matrix is an empty 0x0 matrix. </p>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l01624">1624</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a80ce2837902243f345157a46095ac8ba"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::Matrix" ref="a80ce2837902243f345157a46095ac8ba" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;template&lt; class U &gt; class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; <a class="el" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="el" href="class_seldon_1_1_general.php">General</a>, <a class="el" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator&lt; double &gt; &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Main constructor. </p>
<p>Builds a i x j collection matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows of matrices. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns of matrices. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php#l01639">1639</a> of file <a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Data Documentation</h2>
<a class="anchor" id="a068b8212aa98d09c129bb1952aeee722"></a><!-- doxytag: member="Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;::collection_" ref="a068b8212aa98d09c129bb1952aeee722" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;int, <a class="el" href="class_seldon_1_1_general.php">General</a>, <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, <a class="el" href="class_seldon_1_1_calloc_alloc.php">CallocAlloc</a>&lt;int&gt; &gt; <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection</a>&lt; <a class="el" href="class_seldon_1_1_general.php">General</a> , <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a> , <a class="el" href="class_seldon_1_1_general.php">General</a> , <a class="el" href="class_seldon_1_1_row_sparse.php">RowSparse</a> , Allocator &gt;::<a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a068b8212aa98d09c129bb1952aeee722">collection_</a><code> [protected, inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Type of the underlying matrices. </p>
<p>Type 0 refers to float dense matrices. Type 1 refers to float sparse matrices. Type 2 refers to double dense matrices. Type 3 refers to double sparse matrices. </p>

<p>Definition at line <a class="el" href="_heterogeneous_matrix_collection_8hxx_source.php#l00088">88</a> of file <a class="el" href="_heterogeneous_matrix_collection_8hxx_source.php">HeterogeneousMatrixCollection.hxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>matrix/<a class="el" href="_heterogeneous_matrix_collection_8hxx_source.php">HeterogeneousMatrixCollection.hxx</a></li>
<li>matrix/<a class="el" href="_heterogeneous_matrix_collection_8cxx_source.php">HeterogeneousMatrixCollection.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
