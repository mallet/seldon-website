<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>vector/Vector.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_VECTOR_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;Vector.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="keyword">namespace </span>Seldon
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 
<a name="l00030"></a>00030   <span class="comment">// C_VECTOR_BASE //</span>
<a name="l00032"></a>00032 <span class="comment"></span>
<a name="l00033"></a>00033 
<a name="l00034"></a>00034   <span class="comment">/****************</span>
<a name="l00035"></a>00035 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00036"></a>00036 <span class="comment">   ****************/</span>
<a name="l00037"></a>00037 
<a name="l00038"></a>00038 
<a name="l00040"></a>00040 
<a name="l00043"></a>00043   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00044"></a><a class="code" href="class_seldon_1_1_vector___base.php#ac8a6cee506f094504138943629b9dcbb">00044</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector___base.php#ac8a6cee506f094504138943629b9dcbb" title="Default constructor.">Vector_Base&lt;T, Allocator&gt;::Vector_Base</a>()
<a name="l00045"></a>00045   {
<a name="l00046"></a>00046     m_ = 0;
<a name="l00047"></a>00047     data_ = NULL;
<a name="l00048"></a>00048   }
<a name="l00049"></a>00049 
<a name="l00050"></a>00050 
<a name="l00052"></a>00052 
<a name="l00056"></a>00056   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00057"></a><a class="code" href="class_seldon_1_1_vector___base.php#aa5d9ca198d4175b9baf9a0543f7376be">00057</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector___base.php#ac8a6cee506f094504138943629b9dcbb" title="Default constructor.">Vector_Base&lt;T, Allocator&gt;::Vector_Base</a>(<span class="keywordtype">int</span> i)
<a name="l00058"></a>00058   {
<a name="l00059"></a>00059     m_ = i;
<a name="l00060"></a>00060     data_ = NULL;
<a name="l00061"></a>00061   }
<a name="l00062"></a>00062 
<a name="l00063"></a>00063 
<a name="l00065"></a>00065 
<a name="l00069"></a>00069   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00070"></a>00070   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector___base.php#ac8a6cee506f094504138943629b9dcbb" title="Default constructor.">Vector_Base&lt;T, Allocator&gt;::</a>
<a name="l00071"></a><a class="code" href="class_seldon_1_1_vector___base.php#a27796125b93f3a771a5c92e8196c9229">00071</a> <a class="code" href="class_seldon_1_1_vector___base.php#ac8a6cee506f094504138943629b9dcbb" title="Default constructor.">  Vector_Base</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector___base.php" title="Base structure for all vectors.">Vector_Base&lt;T, Allocator&gt;</a>&amp; A)
<a name="l00072"></a>00072   {
<a name="l00073"></a>00073     m_ = A.<a class="code" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae" title="Returns the number of elements.">GetM</a>();
<a name="l00074"></a>00074     data_ = NULL;
<a name="l00075"></a>00075   }
<a name="l00076"></a>00076 
<a name="l00077"></a>00077 
<a name="l00078"></a>00078   <span class="comment">/**************</span>
<a name="l00079"></a>00079 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00080"></a>00080 <span class="comment">   **************/</span>
<a name="l00081"></a>00081 
<a name="l00082"></a>00082 
<a name="l00084"></a>00084   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00085"></a><a class="code" href="class_seldon_1_1_vector___base.php#a6da510796ebef867765ea3a037debb1b">00085</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector___base.php#a6da510796ebef867765ea3a037debb1b" title="Destructor.">Vector_Base&lt;T, Allocator&gt;::~Vector_Base</a>()
<a name="l00086"></a>00086   {
<a name="l00087"></a>00087 
<a name="l00088"></a>00088 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00089"></a>00089 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00090"></a>00090       {
<a name="l00091"></a>00091 <span class="preprocessor">#endif</span>
<a name="l00092"></a>00092 <span class="preprocessor"></span>
<a name="l00093"></a>00093         <span class="keywordflow">if</span> (data_ != NULL)
<a name="l00094"></a>00094           {
<a name="l00095"></a>00095             vect_allocator_.deallocate(data_, m_);
<a name="l00096"></a>00096             m_ = 0;
<a name="l00097"></a>00097             data_ = NULL;
<a name="l00098"></a>00098           }
<a name="l00099"></a>00099 
<a name="l00100"></a>00100 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00101"></a>00101 <span class="preprocessor"></span>      }
<a name="l00102"></a>00102     <span class="keywordflow">catch</span> (...)
<a name="l00103"></a>00103       {
<a name="l00104"></a>00104         m_ = 0;
<a name="l00105"></a>00105         data_ = NULL;
<a name="l00106"></a>00106       }
<a name="l00107"></a>00107 <span class="preprocessor">#endif</span>
<a name="l00108"></a>00108 <span class="preprocessor"></span>
<a name="l00109"></a>00109   }
<a name="l00110"></a>00110 
<a name="l00111"></a>00111 
<a name="l00112"></a>00112   <span class="comment">/*******************</span>
<a name="l00113"></a>00113 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00114"></a>00114 <span class="comment">   *******************/</span>
<a name="l00115"></a>00115 
<a name="l00116"></a>00116 
<a name="l00118"></a>00118 
<a name="l00121"></a>00121   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00122"></a><a class="code" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae">00122</a>   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae" title="Returns the number of elements.">Vector_Base&lt;T, Allocator&gt;::GetM</a>()<span class="keyword"> const</span>
<a name="l00123"></a>00123 <span class="keyword">  </span>{
<a name="l00124"></a>00124     <span class="keywordflow">return</span> m_;
<a name="l00125"></a>00125   }
<a name="l00126"></a>00126 
<a name="l00127"></a>00127 
<a name="l00129"></a>00129 
<a name="l00132"></a>00132   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00133"></a><a class="code" href="class_seldon_1_1_vector___base.php#a075daae3607a4767a77a3de60ab30ba7">00133</a>   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector___base.php#a075daae3607a4767a77a3de60ab30ba7" title="Returns the number of elements.">Vector_Base&lt;T, Allocator&gt;::GetLength</a>()<span class="keyword"> const</span>
<a name="l00134"></a>00134 <span class="keyword">  </span>{
<a name="l00135"></a>00135     <span class="keywordflow">return</span> m_;
<a name="l00136"></a>00136   }
<a name="l00137"></a>00137 
<a name="l00138"></a>00138 
<a name="l00140"></a>00140 
<a name="l00143"></a>00143   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00144"></a><a class="code" href="class_seldon_1_1_vector___base.php#a839880a3113c1885ead70d79fade0294">00144</a>   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector___base.php#a839880a3113c1885ead70d79fade0294" title="Returns the number of elements stored.">Vector_Base&lt;T, Allocator&gt;::GetSize</a>()<span class="keyword"> const</span>
<a name="l00145"></a>00145 <span class="keyword">  </span>{
<a name="l00146"></a>00146     <span class="keywordflow">return</span> m_;
<a name="l00147"></a>00147   }
<a name="l00148"></a>00148 
<a name="l00149"></a>00149 
<a name="l00151"></a>00151 
<a name="l00154"></a>00154   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00155"></a>00155   <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector___base.php" title="Base structure for all vectors.">Vector_Base&lt;T, Allocator&gt;::pointer</a>
<a name="l00156"></a><a class="code" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a">00156</a>   <a class="code" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a" title="Returns a pointer to data_ (stored data).">Vector_Base&lt;T, Allocator&gt;::GetData</a>()<span class="keyword"> const</span>
<a name="l00157"></a>00157 <span class="keyword">  </span>{
<a name="l00158"></a>00158     <span class="keywordflow">return</span> data_;
<a name="l00159"></a>00159   }
<a name="l00160"></a>00160 
<a name="l00161"></a>00161 
<a name="l00163"></a>00163 
<a name="l00166"></a>00166   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00167"></a>00167   <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector___base.php" title="Base structure for all vectors.">Vector_Base&lt;T, Allocator&gt;::const_pointer</a>
<a name="l00168"></a><a class="code" href="class_seldon_1_1_vector___base.php#a454aea78c82c4317dfe4160cc7c95afa">00168</a>   <a class="code" href="class_seldon_1_1_vector___base.php#a454aea78c82c4317dfe4160cc7c95afa" title="Returns a const pointer to data_ (stored data).">Vector_Base&lt;T, Allocator&gt;::GetDataConst</a>()<span class="keyword"> const</span>
<a name="l00169"></a>00169 <span class="keyword">  </span>{
<a name="l00170"></a>00170     <span class="keywordflow">return</span> <span class="keyword">reinterpret_cast&lt;</span>typename <a class="code" href="class_seldon_1_1_vector___base.php" title="Base structure for all vectors.">Vector_Base</a>&lt;T,
<a name="l00171"></a>00171       Allocator<span class="keyword">&gt;</span>::const_pointer&gt;(data_);
<a name="l00172"></a>00172   }
<a name="l00173"></a>00173 
<a name="l00174"></a>00174 
<a name="l00176"></a>00176 
<a name="l00179"></a>00179   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00180"></a><a class="code" href="class_seldon_1_1_vector___base.php#ad0f5184bd9eec6fca70c54e459ced78f">00180</a>   <span class="keywordtype">void</span>* <a class="code" href="class_seldon_1_1_vector___base.php#ad0f5184bd9eec6fca70c54e459ced78f" title="Returns a pointer of type &amp;quot;void*&amp;quot; to the data array (data_).">Vector_Base&lt;T, Allocator&gt;::GetDataVoid</a>()<span class="keyword"> const</span>
<a name="l00181"></a>00181 <span class="keyword">  </span>{
<a name="l00182"></a>00182     <span class="keywordflow">return</span> <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">void</span>*<span class="keyword">&gt;</span>(data_);
<a name="l00183"></a>00183   }
<a name="l00184"></a>00184 
<a name="l00185"></a>00185 
<a name="l00187"></a>00187 
<a name="l00190"></a>00190   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00191"></a><a class="code" href="class_seldon_1_1_vector___base.php#a0aadc006977de037eb3ee550683e3f19">00191</a>   <span class="keyword">const</span> <span class="keywordtype">void</span>* <a class="code" href="class_seldon_1_1_vector___base.php#a0aadc006977de037eb3ee550683e3f19" title="Returns a pointer of type &amp;quot;const void*&amp;quot; to the data array (data_).">Vector_Base&lt;T, Allocator&gt;::GetDataConstVoid</a>()<span class="keyword"> const</span>
<a name="l00192"></a>00192 <span class="keyword">  </span>{
<a name="l00193"></a>00193     <span class="keywordflow">return</span> <span class="keyword">reinterpret_cast&lt;</span><span class="keyword">const </span><span class="keywordtype">void</span>*<span class="keyword">&gt;</span>(data_);
<a name="l00194"></a>00194   }
<a name="l00195"></a>00195 
<a name="l00196"></a>00196 
<a name="l00198"></a>00198   <span class="comment">// VECTOR&lt;VECT_FULL&gt; //</span>
<a name="l00200"></a>00200 <span class="comment"></span>
<a name="l00201"></a>00201 
<a name="l00202"></a>00202   <span class="comment">/****************</span>
<a name="l00203"></a>00203 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00204"></a>00204 <span class="comment">   ****************/</span>
<a name="l00205"></a>00205 
<a name="l00206"></a>00206 
<a name="l00208"></a>00208 
<a name="l00211"></a>00211   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00212"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a3220e19bce6b4a0f72dfca147dbf8b38">00212</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Vector</a>():
<a name="l00213"></a>00213     <a class="code" href="class_seldon_1_1_vector___base.php" title="Base structure for all vectors.">Vector_Base</a>&lt;T, Allocator&gt;()
<a name="l00214"></a>00214   {
<a name="l00215"></a>00215   }
<a name="l00216"></a>00216 
<a name="l00217"></a>00217 
<a name="l00219"></a>00219 
<a name="l00222"></a>00222   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00223"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#aaef02756d3c6c0a97454f35bffc07962">00223</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Vector</a>(<span class="keywordtype">int</span> i):
<a name="l00224"></a>00224     <a class="code" href="class_seldon_1_1_vector___base.php" title="Base structure for all vectors.">Vector_Base</a>&lt;T, Allocator&gt;(i)
<a name="l00225"></a>00225   {
<a name="l00226"></a>00226 
<a name="l00227"></a>00227 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00228"></a>00228 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00229"></a>00229       {
<a name="l00230"></a>00230 <span class="preprocessor">#endif</span>
<a name="l00231"></a>00231 <span class="preprocessor"></span>
<a name="l00232"></a>00232         this-&gt;data_ = this-&gt;vect_allocator_.allocate(i, <span class="keyword">this</span>);
<a name="l00233"></a>00233 
<a name="l00234"></a>00234 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00235"></a>00235 <span class="preprocessor"></span>      }
<a name="l00236"></a>00236     <span class="keywordflow">catch</span> (...)
<a name="l00237"></a>00237       {
<a name="l00238"></a>00238         this-&gt;m_ = 0;
<a name="l00239"></a>00239         this-&gt;data_ = NULL;
<a name="l00240"></a>00240       }
<a name="l00241"></a>00241     <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00242"></a>00242       this-&gt;m_ = 0;
<a name="l00243"></a>00243     <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; i != 0)
<a name="l00244"></a>00244       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::Vector(int)&quot;</span>,
<a name="l00245"></a>00245                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate memory for a vector of size &quot;</span>)
<a name="l00246"></a>00246                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i*<span class="keyword">sizeof</span>(T)) + <span class="stringliteral">&quot; bytes (&quot;</span>
<a name="l00247"></a>00247                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00248"></a>00248 <span class="preprocessor">#endif</span>
<a name="l00249"></a>00249 <span class="preprocessor"></span>
<a name="l00250"></a>00250   }
<a name="l00251"></a>00251 
<a name="l00252"></a>00252 
<a name="l00254"></a>00254 
<a name="l00264"></a>00264   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00265"></a>00265   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;</a>
<a name="l00266"></a>00266 <a class="code" href="class_seldon_1_1_vector.php">  ::Vector</a>(<span class="keywordtype">int</span> i, <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::pointer</a> data):
<a name="l00267"></a>00267     <a class="code" href="class_seldon_1_1_vector___base.php" title="Base structure for all vectors.">Vector_Base</a>&lt;T, Allocator&gt;()
<a name="l00268"></a>00268   {
<a name="l00269"></a>00269     SetData(i, data);
<a name="l00270"></a>00270   }
<a name="l00271"></a>00271 
<a name="l00272"></a>00272 
<a name="l00274"></a>00274 
<a name="l00277"></a>00277   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00278"></a>00278   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a3220e19bce6b4a0f72dfca147dbf8b38" title="Default constructor.">Vector&lt;T, VectFull, Allocator&gt;::</a>
<a name="l00279"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a1afba3b1f5b3d8022dd4e047185968d8">00279</a> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a3220e19bce6b4a0f72dfca147dbf8b38" title="Default constructor.">  Vector</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a>&amp; V):
<a name="l00280"></a>00280     <a class="code" href="class_seldon_1_1_vector___base.php" title="Base structure for all vectors.">Vector_Base</a>&lt;T, Allocator&gt;(V)
<a name="l00281"></a>00281   {
<a name="l00282"></a>00282 
<a name="l00283"></a>00283 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00284"></a>00284 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00285"></a>00285       {
<a name="l00286"></a>00286 <span class="preprocessor">#endif</span>
<a name="l00287"></a>00287 <span class="preprocessor"></span>
<a name="l00288"></a>00288         this-&gt;data_ = this-&gt;vect_allocator_.allocate(V.<a class="code" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae" title="Returns the number of elements.">GetM</a>(), <span class="keyword">this</span>);
<a name="l00289"></a>00289 
<a name="l00290"></a>00290 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00291"></a>00291 <span class="preprocessor"></span>      }
<a name="l00292"></a>00292     <span class="keywordflow">catch</span> (...)
<a name="l00293"></a>00293       {
<a name="l00294"></a>00294         this-&gt;m_ = 0;
<a name="l00295"></a>00295         this-&gt;data_ = NULL;
<a name="l00296"></a>00296       }
<a name="l00297"></a>00297     <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00298"></a>00298       this-&gt;m_ = 0;
<a name="l00299"></a>00299     <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; V.<a class="code" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae" title="Returns the number of elements.">GetM</a>() != 0)
<a name="l00300"></a>00300       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::Vector(Vector&lt;VectFull&gt;&amp;)&quot;</span>,
<a name="l00301"></a>00301                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate memory for a vector of size &quot;</span>)
<a name="l00302"></a>00302                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(V.<a class="code" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae" title="Returns the number of elements.">GetM</a>()*<span class="keyword">sizeof</span>(T)) + <span class="stringliteral">&quot; bytes (&quot;</span>
<a name="l00303"></a>00303                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(V.<a class="code" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae" title="Returns the number of elements.">GetM</a>()) + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00304"></a>00304 <span class="preprocessor">#endif</span>
<a name="l00305"></a>00305 <span class="preprocessor"></span>
<a name="l00306"></a>00306     this-&gt;vect_allocator_.memorycpy(this-&gt;data_, V.<a class="code" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a" title="Returns a pointer to data_ (stored data).">GetData</a>(), V.<a class="code" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae" title="Returns the number of elements.">GetM</a>());
<a name="l00307"></a>00307 
<a name="l00308"></a>00308   }
<a name="l00309"></a>00309 
<a name="l00310"></a>00310 
<a name="l00311"></a>00311   <span class="comment">/**************</span>
<a name="l00312"></a>00312 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00313"></a>00313 <span class="comment">   **************/</span>
<a name="l00314"></a>00314 
<a name="l00315"></a>00315 
<a name="l00317"></a>00317   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00318"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#afe4b7f7be7d9dedb8e1275f283706c43">00318</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::~Vector</a>()
<a name="l00319"></a>00319   {
<a name="l00320"></a>00320   }
<a name="l00321"></a>00321 
<a name="l00322"></a>00322 
<a name="l00323"></a>00323   <span class="comment">/*********************</span>
<a name="l00324"></a>00324 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00325"></a>00325 <span class="comment">   *********************/</span>
<a name="l00326"></a>00326 
<a name="l00327"></a>00327 
<a name="l00329"></a>00329 
<a name="l00333"></a>00333   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00334"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a5120b4ecff05d8cf54bbd57fb280fbe1">00334</a>   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Clear</a>()
<a name="l00335"></a>00335   {
<a name="l00336"></a>00336     this-&gt;~<a class="code" href="class_seldon_1_1_vector.php">Vector</a>();
<a name="l00337"></a>00337   }
<a name="l00338"></a>00338 
<a name="l00339"></a>00339 
<a name="l00341"></a>00341 
<a name="l00347"></a>00347   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00348"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a6c1667ecf1265c0870ba2828b33a2d79">00348</a>   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Reallocate</a>(<span class="keywordtype">int</span> i)
<a name="l00349"></a>00349   {
<a name="l00350"></a>00350 
<a name="l00351"></a>00351     <span class="keywordflow">if</span> (i != this-&gt;m_)
<a name="l00352"></a>00352       {
<a name="l00353"></a>00353 
<a name="l00354"></a>00354         this-&gt;m_ = i;
<a name="l00355"></a>00355 
<a name="l00356"></a>00356 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00357"></a>00357 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l00358"></a>00358           {
<a name="l00359"></a>00359 <span class="preprocessor">#endif</span>
<a name="l00360"></a>00360 <span class="preprocessor"></span>
<a name="l00361"></a>00361             this-&gt;data_ =
<a name="l00362"></a>00362               <span class="keyword">reinterpret_cast&lt;</span>pointer<span class="keyword">&gt;</span>(this-&gt;vect_allocator_
<a name="l00363"></a>00363                                         .reallocate(this-&gt;data_, i, <span class="keyword">this</span>));
<a name="l00364"></a>00364 
<a name="l00365"></a>00365 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00366"></a>00366 <span class="preprocessor"></span>          }
<a name="l00367"></a>00367         <span class="keywordflow">catch</span> (...)
<a name="l00368"></a>00368           {
<a name="l00369"></a>00369             this-&gt;m_ = 0;
<a name="l00370"></a>00370             this-&gt;data_ = NULL;
<a name="l00371"></a>00371             <span class="keywordflow">return</span>;
<a name="l00372"></a>00372           }
<a name="l00373"></a>00373         <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00374"></a>00374           {
<a name="l00375"></a>00375             this-&gt;m_ = 0;
<a name="l00376"></a>00376             <span class="keywordflow">return</span>;
<a name="l00377"></a>00377           }
<a name="l00378"></a>00378 <span class="preprocessor">#endif</span>
<a name="l00379"></a>00379 <span class="preprocessor"></span>
<a name="l00380"></a>00380       }
<a name="l00381"></a>00381   }
<a name="l00382"></a>00382 
<a name="l00383"></a>00383 
<a name="l00385"></a>00385 
<a name="l00389"></a>00389   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00390"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a99dda1b202ce562645f890897c593515">00390</a>   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Resize</a>(<span class="keywordtype">int</span> n)
<a name="l00391"></a>00391   {
<a name="l00392"></a>00392 
<a name="l00393"></a>00393     <span class="keywordflow">if</span> (n == this-&gt;m_)
<a name="l00394"></a>00394       <span class="keywordflow">return</span>;
<a name="l00395"></a>00395 
<a name="l00396"></a>00396     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> X_new(n);
<a name="l00397"></a>00397     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; min(this-&gt;m_, n); i++)
<a name="l00398"></a>00398       X_new(i) = this-&gt;data_[i];
<a name="l00399"></a>00399 
<a name="l00400"></a>00400     SetData(n, X_new.<a class="code" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a" title="Returns a pointer to data_ (stored data).">GetData</a>());
<a name="l00401"></a>00401     X_new.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a151dc47113d07ab24775733d0c46dc90" title="Clears the vector without releasing memory.">Nullify</a>();
<a name="l00402"></a>00402   }
<a name="l00403"></a>00403 
<a name="l00404"></a>00404 
<a name="l00420"></a>00420   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00421"></a>00421   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;</a>
<a name="l00422"></a>00422 <a class="code" href="class_seldon_1_1_vector.php">  ::SetData</a>(<span class="keywordtype">int</span> i, <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::pointer</a> data)
<a name="l00423"></a>00423   {
<a name="l00424"></a>00424     this-&gt;Clear();
<a name="l00425"></a>00425 
<a name="l00426"></a>00426     this-&gt;m_ = i;
<a name="l00427"></a>00427 
<a name="l00428"></a>00428     this-&gt;data_ = data;
<a name="l00429"></a>00429   }
<a name="l00430"></a>00430 
<a name="l00431"></a>00431 
<a name="l00433"></a>00433 
<a name="l00445"></a>00445   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00446"></a>00446   <span class="keyword">template</span> &lt;<span class="keyword">class</span> Allocator0&gt;
<a name="l00447"></a>00447   <span class="keyword">inline</span> <span class="keywordtype">void</span> Vector&lt;T, VectFull, Allocator&gt;
<a name="l00448"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ab12cedc7bee632a7b210312f1e7ef28b">00448</a>   ::SetData(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator0&gt;</a>&amp; V)
<a name="l00449"></a>00449   {
<a name="l00450"></a>00450     SetData(V.GetLength(), V.GetData());
<a name="l00451"></a>00451   }
<a name="l00452"></a>00452 
<a name="l00453"></a>00453 
<a name="l00455"></a>00455 
<a name="l00460"></a>00460   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00461"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a151dc47113d07ab24775733d0c46dc90">00461</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Nullify</a>()
<a name="l00462"></a>00462   {
<a name="l00463"></a>00463     this-&gt;m_ = 0;
<a name="l00464"></a>00464     this-&gt;data_ = NULL;
<a name="l00465"></a>00465   }
<a name="l00466"></a>00466 
<a name="l00467"></a>00467 
<a name="l00468"></a>00468   <span class="comment">/**********************************</span>
<a name="l00469"></a>00469 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l00470"></a>00470 <span class="comment">   **********************************/</span>
<a name="l00471"></a>00471 
<a name="l00472"></a>00472 
<a name="l00474"></a>00474 
<a name="l00478"></a>00478   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00479"></a>00479   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::reference</a>
<a name="l00480"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#abb6f9d844071aaa8c8bc1ac62b1dba07">00480</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i)
<a name="l00481"></a>00481   {
<a name="l00482"></a>00482 
<a name="l00483"></a>00483 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00484"></a>00484 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00485"></a>00485       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::operator()&quot;</span>,
<a name="l00486"></a>00486                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00487"></a>00487                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00488"></a>00488 <span class="preprocessor">#endif</span>
<a name="l00489"></a>00489 <span class="preprocessor"></span>
<a name="l00490"></a>00490     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00491"></a>00491   }
<a name="l00492"></a>00492 
<a name="l00493"></a>00493 
<a name="l00495"></a>00495 
<a name="l00499"></a>00499   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00500"></a>00500   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::reference</a>
<a name="l00501"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a85cd9b850be2276e853e81ba567a18e6">00501</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Get</a>(<span class="keywordtype">int</span> i)
<a name="l00502"></a>00502   {
<a name="l00503"></a>00503 
<a name="l00504"></a>00504 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00505"></a>00505 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00506"></a>00506       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::Get(int)&quot;</span>,
<a name="l00507"></a>00507                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00508"></a>00508                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00509"></a>00509 <span class="preprocessor">#endif</span>
<a name="l00510"></a>00510 <span class="preprocessor"></span>
<a name="l00511"></a>00511     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00512"></a>00512   }
<a name="l00513"></a>00513 
<a name="l00514"></a>00514 
<a name="l00516"></a>00516 
<a name="l00520"></a>00520   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00521"></a>00521   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::const_reference</a>
<a name="l00522"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#af6ee550855ccf2a4db736bad2352929f">00522</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00523"></a>00523 <span class="keyword">  </span>{
<a name="l00524"></a>00524 
<a name="l00525"></a>00525 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00526"></a>00526 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00527"></a>00527       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::operator() const&quot;</span>,
<a name="l00528"></a>00528                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00529"></a>00529                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00530"></a>00530 <span class="preprocessor">#endif</span>
<a name="l00531"></a>00531 <span class="preprocessor"></span>
<a name="l00532"></a>00532     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00533"></a>00533   }
<a name="l00534"></a>00534 
<a name="l00535"></a>00535 
<a name="l00537"></a>00537 
<a name="l00541"></a>00541   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00542"></a>00542   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::const_reference</a>
<a name="l00543"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a92b34068ad39cbfec8918d77d2385bee">00543</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Get</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00544"></a>00544 <span class="keyword">  </span>{
<a name="l00545"></a>00545 
<a name="l00546"></a>00546 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00547"></a>00547 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00548"></a>00548       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::Get(int) const&quot;</span>,
<a name="l00549"></a>00549                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00550"></a>00550                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00551"></a>00551 <span class="preprocessor">#endif</span>
<a name="l00552"></a>00552 <span class="preprocessor"></span>
<a name="l00553"></a>00553     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00554"></a>00554   }
<a name="l00555"></a>00555 
<a name="l00556"></a>00556 
<a name="l00558"></a>00558 
<a name="l00563"></a>00563   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00564"></a>00564   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a>&amp; <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;</a>
<a name="l00565"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#abd12db9e14f5ba1c417d4c587e6d359a">00565</a> <a class="code" href="class_seldon_1_1_vector.php">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a>&amp; X)
<a name="l00566"></a>00566   {
<a name="l00567"></a>00567     this-&gt;Copy(X);
<a name="l00568"></a>00568 
<a name="l00569"></a>00569     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00570"></a>00570   }
<a name="l00571"></a>00571 
<a name="l00572"></a>00572 
<a name="l00574"></a>00574 
<a name="l00579"></a>00579   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00580"></a>00580   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;</a>
<a name="l00581"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a790a38cb578627bf17291088f2cd2388">00581</a> <a class="code" href="class_seldon_1_1_vector.php">  ::Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a>&amp; X)
<a name="l00582"></a>00582   {
<a name="l00583"></a>00583     this-&gt;Reallocate(X.<a class="code" href="class_seldon_1_1_vector___base.php#a075daae3607a4767a77a3de60ab30ba7" title="Returns the number of elements.">GetLength</a>());
<a name="l00584"></a>00584 
<a name="l00585"></a>00585     this-&gt;vect_allocator_.memorycpy(this-&gt;data_, X.<a class="code" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a" title="Returns a pointer to data_ (stored data).">GetData</a>(), this-&gt;m_);
<a name="l00586"></a>00586   }
<a name="l00587"></a>00587 
<a name="l00588"></a>00588 
<a name="l00590"></a>00590 
<a name="l00595"></a>00595   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00596"></a>00596   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a>
<a name="l00597"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a6e1000961b639af39a111147cf820d77">00597</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Copy</a>()<span class="keyword"> const</span>
<a name="l00598"></a>00598 <span class="keyword">  </span>{
<a name="l00599"></a>00599     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a>(*this);
<a name="l00600"></a>00600   }
<a name="l00601"></a>00601 
<a name="l00602"></a>00602 
<a name="l00604"></a>00604 
<a name="l00607"></a>00607   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l00608"></a>00608   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a>&amp; <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;</a>
<a name="l00609"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a236fd63d659f7d92f3b12da557380cb3">00609</a> <a class="code" href="class_seldon_1_1_vector.php">  ::operator*= </a>(<span class="keyword">const</span> T0&amp; alpha)
<a name="l00610"></a>00610   {
<a name="l00611"></a>00611     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00612"></a>00612       this-&gt;data_[i] *= alpha;
<a name="l00613"></a>00613 
<a name="l00614"></a>00614     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00615"></a>00615   }
<a name="l00616"></a>00616 
<a name="l00617"></a>00617 
<a name="l00619"></a>00619 
<a name="l00624"></a>00624   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00625"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ab3773a41209f76c5739be8f7f037006c">00625</a>   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Append</a>(<span class="keyword">const</span> T&amp; x)
<a name="l00626"></a>00626   {
<a name="l00627"></a>00627     <span class="keywordtype">int</span> i = this-&gt;GetLength();
<a name="l00628"></a>00628     this-&gt;Reallocate(i + 1);
<a name="l00629"></a>00629     this-&gt;data_[i] = x;
<a name="l00630"></a>00630   }
<a name="l00631"></a>00631 
<a name="l00632"></a>00632 
<a name="l00634"></a>00634 
<a name="l00637"></a>00637   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l00638"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a6a64b54653758dbbdcd650c8ffba4d52">00638</a>   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::PushBack</a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00639"></a>00639   {
<a name="l00640"></a>00640     Resize(this-&gt;m_+1);
<a name="l00641"></a>00641     this-&gt;data_[this-&gt;m_-1] = x;
<a name="l00642"></a>00642   }
<a name="l00643"></a>00643 
<a name="l00644"></a>00644 
<a name="l00646"></a>00646 
<a name="l00649"></a>00649   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator0&gt;
<a name="l00650"></a>00650   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;</a>
<a name="l00651"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#aa0b88a0415692922c36c2b2f284efeca">00651</a> <a class="code" href="class_seldon_1_1_vector.php">  ::PushBack</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator0&gt;</a>&amp; X)
<a name="l00652"></a>00652   {
<a name="l00653"></a>00653     <span class="keywordtype">int</span> Nold = this-&gt;m_;
<a name="l00654"></a>00654     Resize(this-&gt;m_ + X.GetM());
<a name="l00655"></a>00655     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetM(); i++)
<a name="l00656"></a>00656       this-&gt;data_[Nold+i] = X(i);
<a name="l00657"></a>00657   }
<a name="l00658"></a>00658 
<a name="l00659"></a>00659 
<a name="l00660"></a>00660   <span class="comment">/*******************</span>
<a name="l00661"></a>00661 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00662"></a>00662 <span class="comment">   *******************/</span>
<a name="l00663"></a>00663 
<a name="l00664"></a>00664 
<a name="l00666"></a>00666 
<a name="l00669"></a>00669   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00670"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a3906b8279cc318dc0a8a672fa781095a">00670</a>   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::GetDataSize</a>()
<a name="l00671"></a>00671   {
<a name="l00672"></a>00672     <span class="keywordflow">return</span> this-&gt;m_;
<a name="l00673"></a>00673   }
<a name="l00674"></a>00674 
<a name="l00675"></a>00675 
<a name="l00676"></a>00676   <span class="comment">/************************</span>
<a name="l00677"></a>00677 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l00678"></a>00678 <span class="comment">   ************************/</span>
<a name="l00679"></a>00679 
<a name="l00680"></a>00680 
<a name="l00682"></a>00682 
<a name="l00686"></a>00686   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00687"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ae8e4d6b081a6f4c90b2993b047a04dac">00687</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Zero</a>()
<a name="l00688"></a>00688   {
<a name="l00689"></a>00689     this-&gt;vect_allocator_.memoryset(this-&gt;data_, <span class="keywordtype">char</span>(0),
<a name="l00690"></a>00690                                     this-&gt;GetDataSize() * <span class="keyword">sizeof</span>(value_type));
<a name="l00691"></a>00691   }
<a name="l00692"></a>00692 
<a name="l00693"></a>00693 
<a name="l00695"></a>00695   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00696"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a722046c309ee8e50abd68a5a5a77aa60">00696</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Fill</a>()
<a name="l00697"></a>00697   {
<a name="l00698"></a>00698     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00699"></a>00699       this-&gt;data_[i] = i;
<a name="l00700"></a>00700   }
<a name="l00701"></a>00701 
<a name="l00702"></a>00702 
<a name="l00704"></a>00704 
<a name="l00707"></a>00707   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00708"></a>00708   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00709"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a2c103fe432a6f6a9c37cb133bb837153">00709</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Fill</a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00710"></a>00710   {
<a name="l00711"></a>00711     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00712"></a>00712       this-&gt;data_[i] = x;
<a name="l00713"></a>00713   }
<a name="l00714"></a>00714 
<a name="l00715"></a>00715 
<a name="l00717"></a>00717 
<a name="l00720"></a>00720   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00721"></a>00721   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00722"></a>00722   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a>&amp;
<a name="l00723"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a0bf814aab7883056808671f5b6866ba6">00723</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00724"></a>00724   {
<a name="l00725"></a>00725     this-&gt;Fill(x);
<a name="l00726"></a>00726 
<a name="l00727"></a>00727     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00728"></a>00728   }
<a name="l00729"></a>00729 
<a name="l00730"></a>00730 
<a name="l00732"></a>00732 
<a name="l00735"></a>00735   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00736"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a71147cebc8bd36cc103d2e6fd3767f91">00736</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::FillRand</a>()
<a name="l00737"></a>00737   {
<a name="l00738"></a>00738     srand(time(NULL));
<a name="l00739"></a>00739     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00740"></a>00740       this-&gt;data_[i] = rand();
<a name="l00741"></a>00741   }
<a name="l00742"></a>00742 
<a name="l00743"></a>00743 
<a name="l00745"></a>00745   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00746"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#adba43c4d26c5b47cd5868e26418f147c">00746</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00747"></a>00747 <span class="keyword">  </span>{
<a name="l00748"></a>00748     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetLength(); i++)
<a name="l00749"></a>00749       cout &lt;&lt; (*<span class="keyword">this</span>)(i) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00750"></a>00750     cout &lt;&lt; endl;
<a name="l00751"></a>00751   }
<a name="l00752"></a>00752 
<a name="l00753"></a>00753 
<a name="l00754"></a>00754   <span class="comment">/*********</span>
<a name="l00755"></a>00755 <span class="comment">   * NORMS *</span>
<a name="l00756"></a>00756 <span class="comment">   *********/</span>
<a name="l00757"></a>00757 
<a name="l00758"></a>00758 
<a name="l00760"></a>00760 
<a name="l00763"></a>00763   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00764"></a>00764   <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::value_type</a>
<a name="l00765"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a165cbeeddb4a2a356accc00d609b4211">00765</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::GetNormInf</a>()<span class="keyword"> const</span>
<a name="l00766"></a>00766 <span class="keyword">  </span>{
<a name="l00767"></a>00767     value_type res = value_type(0);
<a name="l00768"></a>00768     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetLength(); i++)
<a name="l00769"></a>00769       {
<a name="l00770"></a>00770         res = max(res, this-&gt;data_[i]);
<a name="l00771"></a>00771         res = max(res, T(-this-&gt;data_[i]));
<a name="l00772"></a>00772       }
<a name="l00773"></a>00773 
<a name="l00774"></a>00774     <span class="keywordflow">return</span> res;
<a name="l00775"></a>00775   }
<a name="l00776"></a>00776 
<a name="l00777"></a>00777 
<a name="l00779"></a>00779 
<a name="l00782"></a>00782   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00783"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a50186150708e90f86ded59e33cb172a6">00783</a>   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::GetNormInfIndex</a>()<span class="keyword"> const</span>
<a name="l00784"></a>00784 <span class="keyword">  </span>{
<a name="l00785"></a>00785 
<a name="l00786"></a>00786 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00787"></a>00787 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (this-&gt;GetLength() == 0)
<a name="l00788"></a>00788       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::GetNormInfIndex()&quot;</span>,
<a name="l00789"></a>00789                      <span class="stringliteral">&quot;Vector is null.&quot;</span>);
<a name="l00790"></a>00790 <span class="preprocessor">#endif</span>
<a name="l00791"></a>00791 <span class="preprocessor"></span>
<a name="l00792"></a>00792     value_type res = value_type(0), temp;
<a name="l00793"></a>00793     <span class="keywordtype">int</span> j = 0;
<a name="l00794"></a>00794     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetLength(); i++)
<a name="l00795"></a>00795       {
<a name="l00796"></a>00796         temp = res;
<a name="l00797"></a>00797         res = max(res, this-&gt;data_[i]);
<a name="l00798"></a>00798         res = max(res, T(-this-&gt;data_[i]));
<a name="l00799"></a>00799         <span class="keywordflow">if</span> (temp != res) j = i;
<a name="l00800"></a>00800       }
<a name="l00801"></a>00801 
<a name="l00802"></a>00802     <span class="keywordflow">return</span> j;
<a name="l00803"></a>00803   }
<a name="l00804"></a>00804 
<a name="l00805"></a>00805 
<a name="l00806"></a>00806   <span class="comment">/**************************</span>
<a name="l00807"></a>00807 <span class="comment">   * OUTPUT/INPUT FUNCTIONS *</span>
<a name="l00808"></a>00808 <span class="comment">   **************************/</span>
<a name="l00809"></a>00809 
<a name="l00810"></a>00810 
<a name="l00812"></a>00812 
<a name="l00818"></a>00818   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00819"></a>00819   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;</a>
<a name="l00820"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a51ea9d8b0a3176fc903c0279ae8770d0">00820</a> <a class="code" href="class_seldon_1_1_vector.php">  ::Write</a>(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size)<span class="keyword"> const</span>
<a name="l00821"></a>00821 <span class="keyword">  </span>{
<a name="l00822"></a>00822     ofstream FileStream;
<a name="l00823"></a>00823     FileStream.open(FileName.c_str(), ofstream::binary);
<a name="l00824"></a>00824 
<a name="l00825"></a>00825 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00826"></a>00826 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00827"></a>00827     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00828"></a>00828       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::Write(string FileName, &quot;</span>
<a name="l00829"></a>00829                     <span class="stringliteral">&quot;bool with_size)&quot;</span>,
<a name="l00830"></a>00830                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00831"></a>00831 <span class="preprocessor">#endif</span>
<a name="l00832"></a>00832 <span class="preprocessor"></span>
<a name="l00833"></a>00833     this-&gt;Write(FileStream, with_size);
<a name="l00834"></a>00834 
<a name="l00835"></a>00835     FileStream.close();
<a name="l00836"></a>00836   }
<a name="l00837"></a>00837 
<a name="l00838"></a>00838 
<a name="l00840"></a>00840 
<a name="l00846"></a>00846   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00847"></a>00847   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;</a>
<a name="l00848"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a1a098ef03b3d8cad79b659cd05be220e">00848</a> <a class="code" href="class_seldon_1_1_vector.php">  ::Write</a>(ostream&amp; FileStream, <span class="keywordtype">bool</span> with_size)<span class="keyword"> const</span>
<a name="l00849"></a>00849 <span class="keyword">  </span>{
<a name="l00850"></a>00850 
<a name="l00851"></a>00851 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00852"></a>00852 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00853"></a>00853     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00854"></a>00854       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::Write(ostream&amp; FileStream, &quot;</span>
<a name="l00855"></a>00855                     <span class="stringliteral">&quot;bool with_size)&quot;</span>,
<a name="l00856"></a>00856                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00857"></a>00857 <span class="preprocessor">#endif</span>
<a name="l00858"></a>00858 <span class="preprocessor"></span>
<a name="l00859"></a>00859     <span class="keywordflow">if</span> (with_size)
<a name="l00860"></a>00860       FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l00861"></a>00861                        <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00862"></a>00862 
<a name="l00863"></a>00863     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l00864"></a>00864                      this-&gt;m_ * <span class="keyword">sizeof</span>(value_type));
<a name="l00865"></a>00865 
<a name="l00866"></a>00866 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00867"></a>00867 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00868"></a>00868     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00869"></a>00869       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::Write(ostream&amp; FileStream, &quot;</span>
<a name="l00870"></a>00870                     <span class="stringliteral">&quot;bool with_size)&quot;</span>,
<a name="l00871"></a>00871                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l00872"></a>00872 <span class="preprocessor">#endif</span>
<a name="l00873"></a>00873 <span class="preprocessor"></span>
<a name="l00874"></a>00874   }
<a name="l00875"></a>00875 
<a name="l00876"></a>00876 
<a name="l00878"></a>00878 
<a name="l00883"></a>00883   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00884"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a39fd5c75b22c5c2d37a47587f57abad2">00884</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00885"></a>00885 <span class="keyword">  </span>{
<a name="l00886"></a>00886     ofstream FileStream;
<a name="l00887"></a>00887     FileStream.precision(cout.precision());
<a name="l00888"></a>00888     FileStream.flags(cout.flags());
<a name="l00889"></a>00889     FileStream.open(FileName.c_str());
<a name="l00890"></a>00890 
<a name="l00891"></a>00891 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00892"></a>00892 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00893"></a>00893     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00894"></a>00894       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::WriteText(string FileName)&quot;</span>,
<a name="l00895"></a>00895                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00896"></a>00896 <span class="preprocessor">#endif</span>
<a name="l00897"></a>00897 <span class="preprocessor"></span>
<a name="l00898"></a>00898     this-&gt;WriteText(FileStream);
<a name="l00899"></a>00899 
<a name="l00900"></a>00900     FileStream.close();
<a name="l00901"></a>00901   }
<a name="l00902"></a>00902 
<a name="l00903"></a>00903 
<a name="l00905"></a>00905 
<a name="l00910"></a>00910   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00911"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ab5bb2e8cf1a7ceb1ac941a88ed1635e6">00911</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00912"></a>00912 <span class="keyword">  </span>{
<a name="l00913"></a>00913 
<a name="l00914"></a>00914 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00915"></a>00915 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00916"></a>00916     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00917"></a>00917       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::WriteText(ostream&amp; FileStream)&quot;</span>,
<a name="l00918"></a>00918                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00919"></a>00919 <span class="preprocessor">#endif</span>
<a name="l00920"></a>00920 <span class="preprocessor"></span>
<a name="l00921"></a>00921     <span class="keywordflow">if</span> (this-&gt;GetLength() != 0)
<a name="l00922"></a>00922       FileStream &lt;&lt; (*<span class="keyword">this</span>)(0);
<a name="l00923"></a>00923 
<a name="l00924"></a>00924     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 1; i &lt; this-&gt;GetLength(); i++)
<a name="l00925"></a>00925       FileStream &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span> &lt;&lt; (*<span class="keyword">this</span>)(i);
<a name="l00926"></a>00926 
<a name="l00927"></a>00927 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00928"></a>00928 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00929"></a>00929     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00930"></a>00930       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::WriteText(ostream&amp; FileStream)&quot;</span>,
<a name="l00931"></a>00931                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l00932"></a>00932 <span class="preprocessor">#endif</span>
<a name="l00933"></a>00933 <span class="preprocessor"></span>
<a name="l00934"></a>00934   }
<a name="l00935"></a>00935 
<a name="l00936"></a>00936 
<a name="l00937"></a>00937 <span class="preprocessor">#ifdef SELDON_WITH_HDF5</span>
<a name="l00938"></a>00938 <span class="preprocessor"></span>
<a name="l00939"></a>00939 
<a name="l00945"></a>00945   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00946"></a>00946   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::WriteHDF5</a>(<span class="keywordtype">string</span> FileName,
<a name="l00947"></a>00947                                                  <span class="keywordtype">string</span> group_name,
<a name="l00948"></a>00948                                                  <span class="keywordtype">string</span> dataset_name)<span class="keyword"> const</span>
<a name="l00949"></a>00949 <span class="keyword">  </span>{
<a name="l00950"></a>00950     hid_t file_id = H5Fopen(FileName.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
<a name="l00951"></a>00951 
<a name="l00952"></a>00952 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00953"></a>00953 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00954"></a>00954     <span class="keywordflow">if</span> (!file_id)
<a name="l00955"></a>00955       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::WriteHDF5(string FileName)&quot;</span>,
<a name="l00956"></a>00956                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00957"></a>00957 <span class="preprocessor">#endif</span>
<a name="l00958"></a>00958 <span class="preprocessor"></span>
<a name="l00959"></a>00959     hid_t dataset_id, dataspace_id, group_id;
<a name="l00960"></a>00960     herr_t status;
<a name="l00961"></a>00961 
<a name="l00962"></a>00962     T x(0);
<a name="l00963"></a>00963     hid_t datatype = GetH5Type(x);
<a name="l00964"></a>00964 
<a name="l00965"></a>00965     <span class="keywordflow">if</span> (!H5Lexists(file_id, group_name.c_str(), H5P_DEFAULT))
<a name="l00966"></a>00966       group_id = H5Gcreate(file_id, group_name.c_str(), 0);
<a name="l00967"></a>00967     group_id = H5Gopen(file_id, group_name.c_str());
<a name="l00968"></a>00968 
<a name="l00969"></a>00969     <span class="keywordflow">if</span> (!H5Lexists(group_id, dataset_name.c_str(), H5P_DEFAULT))
<a name="l00970"></a>00970       {
<a name="l00971"></a>00971         <span class="comment">// Create the initial dataspace.</span>
<a name="l00972"></a>00972         hsize_t dim[1] = {0};
<a name="l00973"></a>00973         hsize_t maxdims[1] = {H5S_UNLIMITED};
<a name="l00974"></a>00974         dataspace_id = H5Screate_simple(1, dim, maxdims);
<a name="l00975"></a>00975 
<a name="l00976"></a>00976         <span class="comment">// Define chunking parameters to allow extension of the dataspace.</span>
<a name="l00977"></a>00977         hid_t cparms = H5Pcreate(H5P_DATASET_CREATE);
<a name="l00978"></a>00978         hsize_t chunk_dims[1] = {this-&gt;GetLength()};
<a name="l00979"></a>00979         status = H5Pset_chunk(cparms, 1, chunk_dims);
<a name="l00980"></a>00980 
<a name="l00981"></a>00981         <span class="comment">// Create the dataset.</span>
<a name="l00982"></a>00982         hid_t filetype = H5Tvlen_create(datatype);
<a name="l00983"></a>00983         dataset_id = H5Dcreate(group_id, dataset_name.c_str(),
<a name="l00984"></a>00984                                filetype, dataspace_id, cparms);
<a name="l00985"></a>00985       }
<a name="l00986"></a>00986 
<a name="l00987"></a>00987     <span class="comment">// Opens the dataset, and extend it to store a new vector.</span>
<a name="l00988"></a>00988     dataset_id = H5Dopen(group_id, dataset_name.c_str());
<a name="l00989"></a>00989     dataspace_id = H5Dget_space(dataset_id);
<a name="l00990"></a>00990     hsize_t dims_out[1];
<a name="l00991"></a>00991     status  = H5Sget_simple_extent_dims(dataspace_id, dims_out, NULL);
<a name="l00992"></a>00992     hsize_t new_dim[1]= {dims_out[0] + 1};
<a name="l00993"></a>00993     status = H5Dextend(dataset_id, new_dim);
<a name="l00994"></a>00994 
<a name="l00995"></a>00995     <span class="comment">// Selects the last memory part of the dataset, to store the vector.</span>
<a name="l00996"></a>00996     hsize_t offset[1] = {dims_out[0]};
<a name="l00997"></a>00997     hsize_t dim2[1] = {1};
<a name="l00998"></a>00998     dataspace_id = H5Dget_space(dataset_id);
<a name="l00999"></a>00999     status = H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET,
<a name="l01000"></a>01000                                  offset, NULL,
<a name="l01001"></a>01001                                  dim2, NULL);
<a name="l01002"></a>01002 
<a name="l01003"></a>01003     <span class="comment">// Defines memory space.</span>
<a name="l01004"></a>01004     hid_t dataspace = H5Screate_simple(1, dim2, NULL);
<a name="l01005"></a>01005 
<a name="l01006"></a>01006     <span class="comment">// Writes the data to the memory hyperslab selected.</span>
<a name="l01007"></a>01007     hid_t memtype = H5Tvlen_create(datatype);
<a name="l01008"></a>01008     hvl_t wdata[1];
<a name="l01009"></a>01009     wdata[0].len = this-&gt;GetLength();
<a name="l01010"></a>01010     wdata[0].p = this-&gt;GetDataVoid();
<a name="l01011"></a>01011     status = H5Dwrite(dataset_id, memtype, dataspace,
<a name="l01012"></a>01012                       dataspace_id, H5P_DEFAULT, wdata);
<a name="l01013"></a>01013 
<a name="l01014"></a>01014     <span class="comment">// Closes the dataset, group and file.</span>
<a name="l01015"></a>01015     status = H5Dclose(dataset_id);
<a name="l01016"></a>01016     status = H5Gclose(group_id);
<a name="l01017"></a>01017     status = H5Fclose(file_id);
<a name="l01018"></a>01018   }
<a name="l01019"></a>01019 <span class="preprocessor">#endif</span>
<a name="l01020"></a>01020 <span class="preprocessor"></span>
<a name="l01021"></a>01021 
<a name="l01023"></a>01023 
<a name="l01031"></a>01031   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l01032"></a>01032   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a2ce4a8d2726b069e465d4156b1b531be" title="Sets the vector from a file.">Vector&lt;T, VectFull, Allocator&gt;</a>
<a name="l01033"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a2ce4a8d2726b069e465d4156b1b531be">01033</a> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a2ce4a8d2726b069e465d4156b1b531be" title="Sets the vector from a file.">  ::Read</a>(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size)
<a name="l01034"></a>01034   {
<a name="l01035"></a>01035     ifstream FileStream;
<a name="l01036"></a>01036     FileStream.open(FileName.c_str(), ifstream::binary);
<a name="l01037"></a>01037 
<a name="l01038"></a>01038 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01039"></a>01039 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01040"></a>01040     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01041"></a>01041       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::Read(string FileName, bool with_size)&quot;</span>,
<a name="l01042"></a>01042                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01043"></a>01043 <span class="preprocessor">#endif</span>
<a name="l01044"></a>01044 <span class="preprocessor"></span>
<a name="l01045"></a>01045     this-&gt;Read(FileStream, with_size);
<a name="l01046"></a>01046 
<a name="l01047"></a>01047     FileStream.close();
<a name="l01048"></a>01048   }
<a name="l01049"></a>01049 
<a name="l01050"></a>01050 
<a name="l01052"></a>01052 
<a name="l01060"></a>01060   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l01061"></a>01061   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;</a>
<a name="l01062"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a634f8993354070451e53fd0c29bffa49">01062</a> <a class="code" href="class_seldon_1_1_vector.php">  ::Read</a>(istream&amp; FileStream, <span class="keywordtype">bool</span> with_size)
<a name="l01063"></a>01063   {
<a name="l01064"></a>01064 
<a name="l01065"></a>01065 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01066"></a>01066 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01067"></a>01067     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01068"></a>01068       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::Read(istream&amp; FileStream, &quot;</span>
<a name="l01069"></a>01069                     <span class="stringliteral">&quot;bool with_size)&quot;</span>,
<a name="l01070"></a>01070                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l01071"></a>01071 <span class="preprocessor">#endif</span>
<a name="l01072"></a>01072 <span class="preprocessor"></span>
<a name="l01073"></a>01073     <span class="keywordflow">if</span> (with_size)
<a name="l01074"></a>01074       {
<a name="l01075"></a>01075         <span class="keywordtype">int</span> new_size;
<a name="l01076"></a>01076         FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;new_size), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01077"></a>01077         this-&gt;Reallocate(new_size);
<a name="l01078"></a>01078       }
<a name="l01079"></a>01079 
<a name="l01080"></a>01080     FileStream.read(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l01081"></a>01081                     this-&gt;GetLength() * <span class="keyword">sizeof</span>(value_type));
<a name="l01082"></a>01082 
<a name="l01083"></a>01083 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01084"></a>01084 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l01085"></a>01085     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01086"></a>01086       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::Read(istream&amp; FileStream, &quot;</span>
<a name="l01087"></a>01087                     <span class="stringliteral">&quot;bool with_size)&quot;</span>,
<a name="l01088"></a>01088                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l01089"></a>01089 <span class="preprocessor">#endif</span>
<a name="l01090"></a>01090 <span class="preprocessor"></span>
<a name="l01091"></a>01091   }
<a name="l01092"></a>01092 
<a name="l01093"></a>01093 
<a name="l01095"></a>01095 
<a name="l01100"></a>01100   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l01101"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#af1a8f91e66d729c91fddb0a0c9e030b8">01101</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::ReadText</a>(<span class="keywordtype">string</span> FileName)
<a name="l01102"></a>01102   {
<a name="l01103"></a>01103     ifstream FileStream;
<a name="l01104"></a>01104     FileStream.open(FileName.c_str());
<a name="l01105"></a>01105 
<a name="l01106"></a>01106 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01107"></a>01107 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01108"></a>01108     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01109"></a>01109       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::ReadText(string FileName)&quot;</span>,
<a name="l01110"></a>01110                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01111"></a>01111 <span class="preprocessor">#endif</span>
<a name="l01112"></a>01112 <span class="preprocessor"></span>
<a name="l01113"></a>01113     this-&gt;ReadText(FileStream);
<a name="l01114"></a>01114 
<a name="l01115"></a>01115     FileStream.close();
<a name="l01116"></a>01116   }
<a name="l01117"></a>01117 
<a name="l01118"></a>01118 
<a name="l01120"></a>01120 
<a name="l01125"></a>01125   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l01126"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ae0f8292f742367edca616ab9e2e93e6b">01126</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::ReadText</a>(istream&amp; FileStream)
<a name="l01127"></a>01127   {
<a name="l01128"></a>01128     <span class="comment">// Previous values of the vector are cleared.</span>
<a name="l01129"></a>01129     Clear();
<a name="l01130"></a>01130 
<a name="l01131"></a>01131 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01132"></a>01132 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01133"></a>01133     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01134"></a>01134       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::ReadText(istream&amp; FileStream)&quot;</span>,
<a name="l01135"></a>01135                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l01136"></a>01136 <span class="preprocessor">#endif</span>
<a name="l01137"></a>01137 <span class="preprocessor"></span>
<a name="l01138"></a>01138     T entry;
<a name="l01139"></a>01139     <span class="keywordtype">int</span> number_element = 0;
<a name="l01140"></a>01140     <span class="keywordflow">while</span> (!FileStream.eof())
<a name="l01141"></a>01141       {
<a name="l01142"></a>01142         <span class="comment">// Reads a new entry.</span>
<a name="l01143"></a>01143         FileStream &gt;&gt; entry;
<a name="l01144"></a>01144 
<a name="l01145"></a>01145         <span class="keywordflow">if</span> (FileStream.fail())
<a name="l01146"></a>01146           <span class="keywordflow">break</span>;
<a name="l01147"></a>01147         <span class="keywordflow">else</span>
<a name="l01148"></a>01148           {
<a name="l01149"></a>01149             number_element++;
<a name="l01150"></a>01150 
<a name="l01151"></a>01151             <span class="comment">// If needed, resizes the vector. Its size is already doubled so</span>
<a name="l01152"></a>01152             <span class="comment">// that the vector should be resized a limited number of times.</span>
<a name="l01153"></a>01153             <span class="keywordflow">if</span> (number_element &gt; this-&gt;m_)
<a name="l01154"></a>01154               this-&gt;Resize(2 * number_element);
<a name="l01155"></a>01155 
<a name="l01156"></a>01156             this-&gt;data_[number_element - 1] = entry;
<a name="l01157"></a>01157           }
<a name="l01158"></a>01158       }
<a name="l01159"></a>01159 
<a name="l01160"></a>01160     <span class="comment">// Resizes to the actual size.</span>
<a name="l01161"></a>01161     <span class="keywordflow">if</span> (number_element &gt; 0)
<a name="l01162"></a>01162       this-&gt;Resize(number_element);
<a name="l01163"></a>01163     <span class="keywordflow">else</span>
<a name="l01164"></a>01164       this-&gt;Clear();
<a name="l01165"></a>01165   }
<a name="l01166"></a>01166 
<a name="l01167"></a>01167 
<a name="l01169"></a>01169 
<a name="l01174"></a>01174   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01175"></a>01175   ostream&amp; <a class="code" href="namespace_seldon.php#a5bd4a6d6f3c83048663d57d2fc032107" title="operator&amp;lt;&amp;lt; overloaded for a 3D array.">operator &lt;&lt; </a>(ostream&amp; out,
<a name="l01176"></a>01176                         <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage, Allocator&gt;</a>&amp; V)
<a name="l01177"></a>01177   {
<a name="l01178"></a>01178     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; V.GetLength() - 1; i++)
<a name="l01179"></a>01179       out &lt;&lt; V(i) &lt;&lt; <span class="charliteral">&#39;\t&#39;</span>;
<a name="l01180"></a>01180     <span class="keywordflow">if</span> (V.GetLength() != 0)
<a name="l01181"></a>01181       out &lt;&lt; V(V.GetLength() - 1);
<a name="l01182"></a>01182 
<a name="l01183"></a>01183     <span class="keywordflow">return</span> out;
<a name="l01184"></a>01184   }
<a name="l01185"></a>01185 
<a name="l01186"></a>01186 
<a name="l01187"></a>01187 } <span class="comment">// namespace Seldon.</span>
<a name="l01188"></a>01188 
<a name="l01189"></a>01189 <span class="preprocessor">#define SELDON_FILE_VECTOR_CXX</span>
<a name="l01190"></a>01190 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
