<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix_cholmod.php">MatrixCholmod</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::MatrixCholmod Class Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::MatrixCholmod" -->
<p>Object containing Cholesky factorization.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_cholmod_8hxx_source.php">Cholmod.hxx</a>&gt;</code></p>

<p><a href="class_seldon_1_1_matrix_cholmod-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aebf0d491b91bb68ab85ec38bf71591b6"></a><!-- doxytag: member="Seldon::MatrixCholmod::Clear" ref="aebf0d491b91bb68ab85ec38bf71591b6" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Clear</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0d9b091d2dd4b07e7430c720c8639ff1"></a><!-- doxytag: member="Seldon::MatrixCholmod::HideMessages" ref="a0d9b091d2dd4b07e7430c720c8639ff1" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>HideMessages</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab323aec5e68adc07de39666d05cbffac"></a><!-- doxytag: member="Seldon::MatrixCholmod::ShowMessages" ref="ab323aec5e68adc07de39666d05cbffac" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>ShowMessages</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aaa5788f6686f0172bc07b65f9affc498"></a><!-- doxytag: member="Seldon::MatrixCholmod::ShowFullHistory" ref="aaa5788f6686f0172bc07b65f9affc498" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>ShowFullHistory</b> ()</td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a843d2cfa50c3efdbd68c9a0e6e2c9252"></a><!-- doxytag: member="Seldon::MatrixCholmod::FactorizeMatrix" ref="a843d2cfa50c3efdbd68c9a0e6e2c9252" args="(Matrix&lt; double, Prop, Storage, Allocator &gt; &amp;mat, bool keep_matrix=false)" -->
template&lt;class Prop , class Storage , class Allocator &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><b>FactorizeMatrix</b> (<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; double, Prop, Storage, Allocator &gt; &amp;mat, bool keep_matrix=false)</td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="accc98021ee559ca7e77c22d95acddd69"></a><!-- doxytag: member="Seldon::MatrixCholmod::Solve" ref="accc98021ee559ca7e77c22d95acddd69" args="(const Transpose_status &amp;TransA, Vector&lt; double, VectFull, Allocator &gt; &amp;x)" -->
template&lt;class Transpose_status , class Allocator &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_cholmod.php#accc98021ee559ca7e77c22d95acddd69">Solve</a> (const Transpose_status &amp;TransA, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt; &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Solves L x = b or L^T x = b. <br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="ab6e6e5f448fd70e5e49d11a710a5fd5d"></a><!-- doxytag: member="Seldon::MatrixCholmod::Mlt" ref="ab6e6e5f448fd70e5e49d11a710a5fd5d" args="(const Transpose_status &amp;TransA, Vector&lt; double, VectFull, Allocator &gt; &amp;x)" -->
template&lt;class Transpose_status , class Allocator &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_cholmod.php#ab6e6e5f448fd70e5e49d11a710a5fd5d">Mlt</a> (const Transpose_status &amp;TransA, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt; &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Performs the matrix vector product y = L X or y = L^T X. <br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a75787d8d5eaea05b36825a5c117218c2"></a><!-- doxytag: member="Seldon::MatrixCholmod::param_chol" ref="a75787d8d5eaea05b36825a5c117218c2" args="" -->
cholmod_common&nbsp;</td><td class="memItemRight" valign="bottom"><b>param_chol</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af028030f94228bbbcc41e5ed2678ab59"></a><!-- doxytag: member="Seldon::MatrixCholmod::L" ref="af028030f94228bbbcc41e5ed2678ab59" args="" -->
cholmod_factor *&nbsp;</td><td class="memItemRight" valign="bottom"><b>L</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4e53fb9b12db493268f8f9822669023e"></a><!-- doxytag: member="Seldon::MatrixCholmod::Lsparse" ref="a4e53fb9b12db493268f8f9822669023e" args="" -->
cholmod_sparse *&nbsp;</td><td class="memItemRight" valign="bottom"><b>Lsparse</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7575d7f2ecaf43dc34bcd07e0f2494aa"></a><!-- doxytag: member="Seldon::MatrixCholmod::n" ref="a7575d7f2ecaf43dc34bcd07e0f2494aa" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>n</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<p>Object containing Cholesky factorization. </p>

<p>Definition at line <a class="el" href="_cholmod_8hxx_source.php#l00032">32</a> of file <a class="el" href="_cholmod_8hxx_source.php">Cholmod.hxx</a>.</p>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>computation/interfaces/direct/<a class="el" href="_cholmod_8hxx_source.php">Cholmod.hxx</a></li>
<li>computation/interfaces/direct/<a class="el" href="_cholmod_8cxx_source.php">Cholmod.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
