<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/optimization/NLopt.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (c) 2007-2010 Massachusetts Institute of Technology</span>
<a name="l00002"></a>00002 <span class="comment">// Modifications by Marc Fragu, copyright (C) 2011 INRIA</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// Permission is hereby granted, free of charge, to any person obtaining a</span>
<a name="l00005"></a>00005 <span class="comment">// copy of this software and associated documentation files (the &quot;Software&quot;),</span>
<a name="l00006"></a>00006 <span class="comment">// to deal in the Software without restriction, including without limitation</span>
<a name="l00007"></a>00007 <span class="comment">// the rights to use, copy, modify, merge, publish, distribute, sublicense,</span>
<a name="l00008"></a>00008 <span class="comment">// and/or sell copies of the Software, and to permit persons to whom the</span>
<a name="l00009"></a>00009 <span class="comment">// Software is furnished to do so, subject to the following conditions:</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// The above copyright notice and this permission notice shall be included in</span>
<a name="l00012"></a>00012 <span class="comment">// all copies or substantial portions of the Software.</span>
<a name="l00013"></a>00013 <span class="comment">//</span>
<a name="l00014"></a>00014 <span class="comment">// THE SOFTWARE IS PROVIDED &quot;AS IS&quot;, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR</span>
<a name="l00015"></a>00015 <span class="comment">// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,</span>
<a name="l00016"></a>00016 <span class="comment">// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE</span>
<a name="l00017"></a>00017 <span class="comment">// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER</span>
<a name="l00018"></a>00018 <span class="comment">// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING</span>
<a name="l00019"></a>00019 <span class="comment">// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER</span>
<a name="l00020"></a>00020 <span class="comment">// DEALINGS IN THE SOFTWARE.</span>
<a name="l00021"></a>00021 
<a name="l00022"></a>00022 
<a name="l00023"></a>00023 <span class="preprocessor">#ifndef SELDON_COMPUTATION_OPTIMIZATION_NLOPT_HXX</span>
<a name="l00024"></a>00024 <span class="preprocessor"></span><span class="preprocessor">#define SELDON_COMPUTATION_OPTIMIZATION_NLOPT_HXX</span>
<a name="l00025"></a>00025 <span class="preprocessor"></span>
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 <span class="preprocessor">#include &lt;nlopt.hpp&gt;</span>
<a name="l00028"></a>00028 
<a name="l00029"></a>00029 
<a name="l00030"></a>00030 <span class="keyword">namespace </span>nlopt
<a name="l00031"></a>00031 {
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00034"></a>00034   <span class="keyword">typedef</span> double (*svfunc)(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Seldon::Vector&lt;double&gt;</a> &amp;x,
<a name="l00035"></a>00035                            <a class="code" href="class_seldon_1_1_vector.php">Seldon::Vector&lt;double&gt;</a> &amp;grad, <span class="keywordtype">void</span> *data);
<a name="l00036"></a>00036 
<a name="l00037"></a><a class="code" href="classnlopt_1_1_seldon_opt.php">00037</a>   <span class="keyword">class </span><a class="code" href="classnlopt_1_1_seldon_opt.php">SeldonOpt</a>
<a name="l00038"></a>00038   {
<a name="l00039"></a>00039 
<a name="l00040"></a>00040   <span class="keyword">private</span>:
<a name="l00041"></a>00041 
<a name="l00042"></a>00042     nlopt_opt o;
<a name="l00043"></a>00043 
<a name="l00044"></a>00044     <span class="keywordtype">void</span> mythrow(nlopt_result ret)<span class="keyword"> const</span>
<a name="l00045"></a>00045 <span class="keyword">    </span>{
<a name="l00046"></a>00046       <span class="keywordflow">switch</span> (ret)
<a name="l00047"></a>00047         {
<a name="l00048"></a>00048         <span class="keywordflow">case</span> NLOPT_FAILURE:
<a name="l00049"></a>00049           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_error.php">Seldon::Error</a>(<span class="stringliteral">&quot;SeldonOpt&quot;</span>, <span class="stringliteral">&quot;Nlopt failure.&quot;</span>);
<a name="l00050"></a>00050         <span class="keywordflow">case</span> NLOPT_OUT_OF_MEMORY:
<a name="l00051"></a>00051           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a>(<span class="stringliteral">&quot;SeldonOpt&quot;</span>,
<a name="l00052"></a>00052                                  <span class="stringliteral">&quot;Nlopt failed to allocate the&quot;</span>
<a name="l00053"></a>00053                                  <span class="stringliteral">&quot; requested storage space.&quot;</span>);
<a name="l00054"></a>00054         <span class="keywordflow">case</span> NLOPT_INVALID_ARGS:
<a name="l00055"></a>00055           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">Seldon::WrongArgument</a>(<span class="stringliteral">&quot;SeldonOpt&quot;</span>, <span class="stringliteral">&quot;Nlopt invalid argument.&quot;</span>);
<a name="l00056"></a>00056         <span class="keywordflow">case</span> NLOPT_ROUNDOFF_LIMITED:
<a name="l00057"></a>00057           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_error.php">Seldon::Error</a>(<span class="stringliteral">&quot;SeldonOpt&quot;</span>, <span class="stringliteral">&quot;Nlopt roundoff-limited.&quot;</span>);
<a name="l00058"></a>00058         <span class="keywordflow">case</span> NLOPT_FORCED_STOP:
<a name="l00059"></a>00059           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_error.php">Seldon::Error</a>(<span class="stringliteral">&quot;SeldonOpt&quot;</span>, <span class="stringliteral">&quot;Nlopt forced stop.&quot;</span>);
<a name="l00060"></a>00060         <span class="keywordflow">default</span>:
<a name="l00061"></a>00061           <span class="keywordflow">break</span>;
<a name="l00062"></a>00062         }
<a name="l00063"></a>00063     }
<a name="l00064"></a>00064 
<a name="l00065"></a>00065 
<a name="l00066"></a>00066     <span class="keyword">typedef</span> <span class="keyword">struct</span>
<a name="l00067"></a>00067     {
<a name="l00068"></a>00068       <a class="code" href="classnlopt_1_1_seldon_opt.php">SeldonOpt</a>* o;
<a name="l00069"></a>00069       mfunc mf;
<a name="l00070"></a>00070       func f;
<a name="l00071"></a>00071       <span class="keywordtype">void</span>* f_data;
<a name="l00072"></a>00072       svfunc vf;
<a name="l00073"></a>00073       nlopt_munge munge_destroy, munge_copy;
<a name="l00074"></a>00074     } myfunc_data;
<a name="l00075"></a>00075 
<a name="l00076"></a>00076 
<a name="l00077"></a>00077     <span class="keyword">static</span> <span class="keywordtype">void</span> *free_myfunc_data(<span class="keywordtype">void</span> *p)
<a name="l00078"></a>00078     {
<a name="l00079"></a>00079       myfunc_data *d = (myfunc_data *) p;
<a name="l00080"></a>00080       <span class="keywordflow">if</span> (d)
<a name="l00081"></a>00081         {
<a name="l00082"></a>00082           <span class="keywordflow">if</span> (d-&gt;f_data &amp;&amp; d-&gt;munge_destroy)
<a name="l00083"></a>00083             d-&gt;munge_destroy(d-&gt;f_data);
<a name="l00084"></a>00084           <span class="keyword">delete</span> d;
<a name="l00085"></a>00085         }
<a name="l00086"></a>00086       <span class="keywordflow">return</span> NULL;
<a name="l00087"></a>00087     }
<a name="l00088"></a>00088 
<a name="l00089"></a>00089 
<a name="l00090"></a>00090     <span class="keyword">static</span> <span class="keywordtype">void</span> *dup_myfunc_data(<span class="keywordtype">void</span> *p)
<a name="l00091"></a>00091     {
<a name="l00092"></a>00092       myfunc_data *d = (myfunc_data *) p;
<a name="l00093"></a>00093       <span class="keywordflow">if</span> (d)
<a name="l00094"></a>00094         {
<a name="l00095"></a>00095           <span class="keywordtype">void</span> *f_data;
<a name="l00096"></a>00096           <span class="keywordflow">if</span> (d-&gt;f_data &amp;&amp; d-&gt;munge_copy) {
<a name="l00097"></a>00097             f_data = d-&gt;munge_copy(d-&gt;f_data);
<a name="l00098"></a>00098             <span class="keywordflow">if</span> (!f_data)
<a name="l00099"></a>00099               <span class="keywordflow">return</span> NULL;
<a name="l00100"></a>00100           }
<a name="l00101"></a>00101           <span class="keywordflow">else</span>
<a name="l00102"></a>00102             f_data = d-&gt;f_data;
<a name="l00103"></a>00103           myfunc_data *dnew = <span class="keyword">new</span> myfunc_data;
<a name="l00104"></a>00104           <span class="keywordflow">if</span> (dnew)
<a name="l00105"></a>00105             {
<a name="l00106"></a>00106               *dnew = *d;
<a name="l00107"></a>00107               dnew-&gt;f_data = f_data;
<a name="l00108"></a>00108             }
<a name="l00109"></a>00109           <span class="keywordflow">return</span> (<span class="keywordtype">void</span>*) dnew;
<a name="l00110"></a>00110         }
<a name="l00111"></a>00111       <span class="keywordflow">else</span>
<a name="l00112"></a>00112         <span class="keywordflow">return</span> NULL;
<a name="l00113"></a>00113     }
<a name="l00114"></a>00114 
<a name="l00115"></a>00115 
<a name="l00116"></a>00116     <span class="keyword">static</span> <span class="keywordtype">double</span> myfunc(<span class="keywordtype">unsigned</span> n, <span class="keyword">const</span> <span class="keywordtype">double</span> *x, <span class="keywordtype">double</span> *grad,
<a name="l00117"></a>00117                          <span class="keywordtype">void</span> *d_)
<a name="l00118"></a>00118     {
<a name="l00119"></a>00119       myfunc_data *d = <span class="keyword">reinterpret_cast&lt;</span>myfunc_data*<span class="keyword">&gt;</span>(d_);
<a name="l00120"></a>00120       <span class="keywordflow">return</span> d-&gt;f(n, x, grad, d-&gt;f_data);
<a name="l00121"></a>00121     }
<a name="l00122"></a>00122 
<a name="l00123"></a>00123 
<a name="l00124"></a>00124     <span class="keyword">static</span> <span class="keywordtype">void</span> mymfunc(<span class="keywordtype">unsigned</span> m, <span class="keywordtype">double</span> *result,
<a name="l00125"></a>00125                         <span class="keywordtype">unsigned</span> n, <span class="keyword">const</span> <span class="keywordtype">double</span> *x,
<a name="l00126"></a>00126                         <span class="keywordtype">double</span> *grad, <span class="keywordtype">void</span> *d_)
<a name="l00127"></a>00127     {
<a name="l00128"></a>00128       myfunc_data *d = <span class="keyword">reinterpret_cast&lt;</span>myfunc_data*<span class="keyword">&gt;</span>(d_);
<a name="l00129"></a>00129       d-&gt;mf(m, result, n, x, grad, d-&gt;f_data);
<a name="l00130"></a>00130       <span class="keywordflow">return</span>;
<a name="l00131"></a>00131     }
<a name="l00132"></a>00132 
<a name="l00133"></a>00133 
<a name="l00134"></a>00134     <a class="code" href="class_seldon_1_1_vector.php">Seldon::Vector&lt;double&gt;</a> xtmp, gradtmp, gradtmp0;
<a name="l00135"></a>00135 
<a name="l00136"></a>00136 
<a name="l00137"></a>00137     <span class="keyword">static</span> <span class="keywordtype">double</span> myvfunc(<span class="keywordtype">unsigned</span> n, <span class="keyword">const</span> <span class="keywordtype">double</span> *x, <span class="keywordtype">double</span> *grad,
<a name="l00138"></a>00138                           <span class="keywordtype">void</span> *d_)
<a name="l00139"></a>00139     {
<a name="l00140"></a>00140       myfunc_data *d = <span class="keyword">reinterpret_cast&lt;</span>myfunc_data*<span class="keyword">&gt;</span>(d_);
<a name="l00141"></a>00141       <a class="code" href="class_seldon_1_1_vector.php">Seldon::Vector&lt;double&gt;</a> &amp;xv = d-&gt;o-&gt;xtmp;
<a name="l00142"></a>00142       <span class="keywordflow">if</span> (n)
<a name="l00143"></a>00143         memcpy(xv.GetData(), x, n * <span class="keyword">sizeof</span>(double));
<a name="l00144"></a>00144 
<a name="l00145"></a>00145       <span class="keywordtype">double</span> val=d-&gt;vf(xv, grad ? d-&gt;o-&gt;gradtmp : d-&gt;o-&gt;gradtmp0,
<a name="l00146"></a>00146                        d-&gt;f_data);
<a name="l00147"></a>00147       <span class="keywordflow">if</span> (grad &amp;&amp; n)
<a name="l00148"></a>00148         {
<a name="l00149"></a>00149           <a class="code" href="class_seldon_1_1_vector.php">Seldon::Vector&lt;double&gt;</a> &amp;gradv = d-&gt;o-&gt;gradtmp;
<a name="l00150"></a>00150           memcpy(grad, gradv.GetData(), n * <span class="keyword">sizeof</span>(double));
<a name="l00151"></a>00151         }
<a name="l00152"></a>00152       <span class="keywordflow">return</span> val;
<a name="l00153"></a>00153     }
<a name="l00154"></a>00154 
<a name="l00155"></a>00155 
<a name="l00156"></a>00156     <span class="keywordtype">void</span> alloc_tmp()
<a name="l00157"></a>00157     {
<a name="l00158"></a>00158       <span class="keywordflow">if</span> (xtmp.GetSize() != int(nlopt_get_dimension(o)))
<a name="l00159"></a>00159         {
<a name="l00160"></a>00160           xtmp = <a class="code" href="class_seldon_1_1_vector.php">Seldon::Vector&lt;double&gt;</a>(nlopt_get_dimension(o));
<a name="l00161"></a>00161           gradtmp = <a class="code" href="class_seldon_1_1_vector.php">Seldon::Vector&lt;double&gt;</a>(nlopt_get_dimension(o));
<a name="l00162"></a>00162         }
<a name="l00163"></a>00163     }
<a name="l00164"></a>00164 
<a name="l00165"></a>00165 
<a name="l00166"></a>00166     result last_result;
<a name="l00167"></a>00167     <span class="keywordtype">double</span> last_optf;
<a name="l00168"></a>00168     nlopt_result forced_stop_reason;
<a name="l00169"></a>00169 
<a name="l00170"></a>00170 
<a name="l00171"></a>00171   <span class="keyword">public</span>:
<a name="l00172"></a>00172 
<a name="l00173"></a>00173     <a class="code" href="classnlopt_1_1_seldon_opt.php">SeldonOpt</a>() : o(NULL), xtmp(0), gradtmp(0), gradtmp0(0),
<a name="l00174"></a>00174                   last_result(nlopt::FAILURE), last_optf(HUGE_VAL),
<a name="l00175"></a>00175                   forced_stop_reason(NLOPT_FORCED_STOP)
<a name="l00176"></a>00176     {
<a name="l00177"></a>00177     }
<a name="l00178"></a>00178 
<a name="l00179"></a>00179 
<a name="l00180"></a>00180     ~<a class="code" href="classnlopt_1_1_seldon_opt.php">SeldonOpt</a>()
<a name="l00181"></a>00181     {
<a name="l00182"></a>00182       nlopt_destroy(o);
<a name="l00183"></a>00183     }
<a name="l00184"></a>00184 
<a name="l00185"></a>00185 
<a name="l00186"></a>00186     <a class="code" href="classnlopt_1_1_seldon_opt.php">SeldonOpt</a>(algorithm a, <span class="keywordtype">unsigned</span> n) :
<a name="l00187"></a>00187       o(nlopt_create(nlopt_algorithm(a), n)),
<a name="l00188"></a>00188       xtmp(0), gradtmp(0), gradtmp0(0),
<a name="l00189"></a>00189       last_result(nlopt::FAILURE), last_optf(HUGE_VAL),
<a name="l00190"></a>00190       forced_stop_reason(NLOPT_FORCED_STOP)
<a name="l00191"></a>00191     {
<a name="l00192"></a>00192       <span class="keywordflow">if</span> (!o)
<a name="l00193"></a>00193         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a>(<span class="stringliteral">&quot;SeldonOpt(algorithm a, unsigned n)&quot;</span>,
<a name="l00194"></a>00194                                <span class="stringliteral">&quot;Nlopt failed to allocate the&quot;</span>
<a name="l00195"></a>00195                                <span class="stringliteral">&quot; requested storage space.&quot;</span>);
<a name="l00196"></a>00196       nlopt_set_munge(o, free_myfunc_data, dup_myfunc_data);
<a name="l00197"></a>00197     }
<a name="l00198"></a>00198 
<a name="l00199"></a>00199 
<a name="l00200"></a>00200     <a class="code" href="classnlopt_1_1_seldon_opt.php">SeldonOpt</a>(<span class="keyword">const</span> <a class="code" href="classnlopt_1_1_seldon_opt.php">SeldonOpt</a>&amp; f) :
<a name="l00201"></a>00201       o(nlopt_copy(f.o)),
<a name="l00202"></a>00202       xtmp(f.xtmp), gradtmp(f.gradtmp),
<a name="l00203"></a>00203       gradtmp0(0),
<a name="l00204"></a>00204       last_result(f.last_result),
<a name="l00205"></a>00205       last_optf(f.last_optf),
<a name="l00206"></a>00206       forced_stop_reason(f.forced_stop_reason)
<a name="l00207"></a>00207     {
<a name="l00208"></a>00208       <span class="keywordflow">if</span> (f.o &amp;&amp; !o)
<a name="l00209"></a>00209         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a>(<span class="stringliteral">&quot;SeldonOpt(const SeldonOpt&amp; f)&quot;</span>,
<a name="l00210"></a>00210                                <span class="stringliteral">&quot;Nlopt failed to allocate the&quot;</span>
<a name="l00211"></a>00211                                <span class="stringliteral">&quot; requested storage space.&quot;</span>);
<a name="l00212"></a>00212     }
<a name="l00213"></a>00213 
<a name="l00214"></a>00214 
<a name="l00215"></a>00215     <a class="code" href="classnlopt_1_1_seldon_opt.php">SeldonOpt</a>&amp; operator=(<a class="code" href="classnlopt_1_1_seldon_opt.php">SeldonOpt</a> <span class="keyword">const</span>&amp; f)
<a name="l00216"></a>00216     {
<a name="l00217"></a>00217       <span class="keywordflow">if</span> (<span class="keyword">this</span> == &amp;f)
<a name="l00218"></a>00218         <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00219"></a>00219       nlopt_destroy(o);
<a name="l00220"></a>00220       o = nlopt_copy(f.o);
<a name="l00221"></a>00221       <span class="keywordflow">if</span> (f.o &amp;&amp; !o)
<a name="l00222"></a>00222         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a>(<span class="stringliteral">&quot;SeldonOpt::operator=(const SeldonOpt&amp; f)&quot;</span>,
<a name="l00223"></a>00223                                <span class="stringliteral">&quot;Nlopt failed to allocate the&quot;</span>
<a name="l00224"></a>00224                                <span class="stringliteral">&quot; requested storage space.&quot;</span>);
<a name="l00225"></a>00225       xtmp = f.xtmp;
<a name="l00226"></a>00226       gradtmp = f.gradtmp;
<a name="l00227"></a>00227       last_result = f.last_result;
<a name="l00228"></a>00228       last_optf = f.last_optf;
<a name="l00229"></a>00229       forced_stop_reason = f.forced_stop_reason;
<a name="l00230"></a>00230       <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00231"></a>00231     }
<a name="l00232"></a>00232 
<a name="l00233"></a>00233 
<a name="l00234"></a>00234     result optimize(<a class="code" href="class_seldon_1_1_vector.php">Seldon::Vector&lt;double&gt;</a>&amp; x, <span class="keywordtype">double</span>&amp; opt_f)
<a name="l00235"></a>00235     {
<a name="l00236"></a>00236       <span class="keywordflow">if</span> (o &amp;&amp; <span class="keywordtype">int</span>(nlopt_get_dimension(o)) != x.GetSize())
<a name="l00237"></a>00237         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">Seldon::WrongArgument</a>(<span class="stringliteral">&quot;SeldonOpt::optimize(&quot;</span>
<a name="l00238"></a>00238                                     <span class="stringliteral">&quot;Seldon::Vector&lt;double&gt;&amp; x,&quot;</span>
<a name="l00239"></a>00239                                     <span class="stringliteral">&quot; double&amp; opt_f)&quot;</span>, <span class="stringliteral">&quot;Dimension mismatch.&quot;</span>);
<a name="l00240"></a>00240       forced_stop_reason = NLOPT_FORCED_STOP;
<a name="l00241"></a>00241       nlopt_result ret =
<a name="l00242"></a>00242         nlopt_optimize(o, x.GetSize() == 0 ? NULL : x.GetData(),
<a name="l00243"></a>00243                        &amp;opt_f);
<a name="l00244"></a>00244       last_result = result(ret);
<a name="l00245"></a>00245       last_optf = opt_f;
<a name="l00246"></a>00246       <span class="keywordflow">if</span> (ret == NLOPT_FORCED_STOP)
<a name="l00247"></a>00247         mythrow(forced_stop_reason);
<a name="l00248"></a>00248       mythrow(ret);
<a name="l00249"></a>00249       <span class="keywordflow">return</span> last_result;
<a name="l00250"></a>00250     }
<a name="l00251"></a>00251 
<a name="l00252"></a>00252 
<a name="l00253"></a>00253     <a class="code" href="class_seldon_1_1_vector.php">Seldon::Vector&lt;double&gt;</a> optimize(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Seldon::Vector&lt;double&gt;</a> &amp;x0)
<a name="l00254"></a>00254     {
<a name="l00255"></a>00255       <a class="code" href="class_seldon_1_1_vector.php">Seldon::Vector&lt;double&gt;</a> x(x0);
<a name="l00256"></a>00256       last_result = optimize(x, last_optf);
<a name="l00257"></a>00257       <span class="keywordflow">return</span> x;
<a name="l00258"></a>00258     }
<a name="l00259"></a>00259 
<a name="l00260"></a>00260 
<a name="l00261"></a>00261     result last_optimize_result()<span class="keyword"> const</span>
<a name="l00262"></a>00262 <span class="keyword">    </span>{
<a name="l00263"></a>00263       <span class="keywordflow">return</span> last_result;
<a name="l00264"></a>00264     }
<a name="l00265"></a>00265 
<a name="l00266"></a>00266 
<a name="l00267"></a>00267     <span class="keywordtype">double</span> last_optimum_value()<span class="keyword"> const</span>
<a name="l00268"></a>00268 <span class="keyword">    </span>{
<a name="l00269"></a>00269       <span class="keywordflow">return</span> last_optf;
<a name="l00270"></a>00270     }
<a name="l00271"></a>00271 
<a name="l00272"></a>00272 
<a name="l00273"></a>00273     algorithm get_algorithm()<span class="keyword"> const</span>
<a name="l00274"></a>00274 <span class="keyword">    </span>{
<a name="l00275"></a>00275       <span class="keywordflow">if</span> (!o)
<a name="l00276"></a>00276         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_error.php">Seldon::Error</a>(<span class="stringliteral">&quot;SeldonOpt::get_algorithm() const&quot;</span>,
<a name="l00277"></a>00277                             <span class="stringliteral">&quot;Uninitialized nlopt::SeldonOpt.&quot;</span>);
<a name="l00278"></a>00278       <span class="keywordflow">return</span> algorithm(nlopt_get_algorithm(o));
<a name="l00279"></a>00279     }
<a name="l00280"></a>00280 
<a name="l00281"></a>00281 
<a name="l00282"></a>00282     <span class="keyword">const</span> <span class="keywordtype">char</span> *get_algorithm_name()<span class="keyword"> const</span>
<a name="l00283"></a>00283 <span class="keyword">    </span>{
<a name="l00284"></a>00284       <span class="keywordflow">if</span> (!o)
<a name="l00285"></a>00285         <a class="code" href="class_seldon_1_1_error.php">Seldon::Error</a>(<span class="stringliteral">&quot;SeldonOpt::get_algorithm() const&quot;</span>,
<a name="l00286"></a>00286                       <span class="stringliteral">&quot;Uninitialized nlopt::SeldonOpt.&quot;</span>);
<a name="l00287"></a>00287       <span class="keywordflow">return</span> nlopt_algorithm_name(nlopt_get_algorithm(o));
<a name="l00288"></a>00288     }
<a name="l00289"></a>00289 
<a name="l00290"></a>00290 
<a name="l00291"></a>00291     <span class="keywordtype">unsigned</span> get_dimension()<span class="keyword"> const</span>
<a name="l00292"></a>00292 <span class="keyword">    </span>{
<a name="l00293"></a>00293       <span class="keywordflow">if</span> (!o)
<a name="l00294"></a>00294         <a class="code" href="class_seldon_1_1_error.php">Seldon::Error</a>(<span class="stringliteral">&quot;SeldonOpt::get_algorithm() const&quot;</span>,
<a name="l00295"></a>00295                       <span class="stringliteral">&quot;Uninitialized nlopt::SeldonOpt.&quot;</span>);
<a name="l00296"></a>00296       <span class="keywordflow">return</span> nlopt_get_dimension(o);
<a name="l00297"></a>00297     }
<a name="l00298"></a>00298 
<a name="l00299"></a>00299 
<a name="l00300"></a>00300     <span class="keywordtype">void</span> set_min_objective(func f, <span class="keywordtype">void</span> *f_data)
<a name="l00301"></a>00301     {
<a name="l00302"></a>00302       myfunc_data *d = <span class="keyword">new</span> myfunc_data;
<a name="l00303"></a>00303       <span class="keywordflow">if</span> (!d)
<a name="l00304"></a>00304         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a>(<span class="stringliteral">&quot;SeldonOpt::set_min_objective(func f, &quot;</span>
<a name="l00305"></a>00305                                <span class="stringliteral">&quot;void *f_data)&quot;</span>, <span class="stringliteral">&quot;Nlopt failed to allocate the&quot;</span>
<a name="l00306"></a>00306                                <span class="stringliteral">&quot; requested storage space.&quot;</span>);
<a name="l00307"></a>00307       d-&gt;o = <span class="keyword">this</span>;
<a name="l00308"></a>00308       d-&gt;f = f;
<a name="l00309"></a>00309       d-&gt;f_data = f_data;
<a name="l00310"></a>00310       d-&gt;mf = NULL;
<a name="l00311"></a>00311       d-&gt;vf = NULL;
<a name="l00312"></a>00312       d-&gt;munge_destroy = d-&gt;munge_copy = NULL;
<a name="l00313"></a>00313       mythrow(nlopt_set_min_objective(o, myfunc, d));
<a name="l00314"></a>00314     }
<a name="l00315"></a>00315 
<a name="l00316"></a>00316 
<a name="l00317"></a>00317     <span class="keywordtype">void</span> set_min_objective(svfunc vf, <span class="keywordtype">void</span> *f_data) {
<a name="l00318"></a>00318       myfunc_data *d = <span class="keyword">new</span> myfunc_data;
<a name="l00319"></a>00319       <span class="keywordflow">if</span> (!d)
<a name="l00320"></a>00320         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a>(<span class="stringliteral">&quot;SeldonOpt::set_min_objective(func f, &quot;</span>
<a name="l00321"></a>00321                                <span class="stringliteral">&quot;void *f_data)&quot;</span>, <span class="stringliteral">&quot;Nlopt failed to allocate the&quot;</span>
<a name="l00322"></a>00322                                <span class="stringliteral">&quot; requested storage space.&quot;</span>);
<a name="l00323"></a>00323       d-&gt;o = <span class="keyword">this</span>;
<a name="l00324"></a>00324       d-&gt;f = NULL;
<a name="l00325"></a>00325       d-&gt;f_data = f_data;
<a name="l00326"></a>00326       d-&gt;mf = NULL;
<a name="l00327"></a>00327       d-&gt;vf = vf;
<a name="l00328"></a>00328       d-&gt;munge_destroy = d-&gt;munge_copy = NULL;
<a name="l00329"></a>00329       mythrow(nlopt_set_min_objective(o, myvfunc, d));
<a name="l00330"></a>00330       alloc_tmp();
<a name="l00331"></a>00331     }
<a name="l00332"></a>00332 
<a name="l00333"></a>00333 
<a name="l00334"></a>00334     <span class="keywordtype">void</span> set_max_objective(func f, <span class="keywordtype">void</span> *f_data) {
<a name="l00335"></a>00335       myfunc_data *d = <span class="keyword">new</span> myfunc_data;
<a name="l00336"></a>00336       <span class="keywordflow">if</span> (!d)
<a name="l00337"></a>00337         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a>(<span class="stringliteral">&quot;SeldonOpt::set_min_objective(func f, &quot;</span>
<a name="l00338"></a>00338                                <span class="stringliteral">&quot;void *f_data)&quot;</span>, <span class="stringliteral">&quot;Nlopt failed to allocate the&quot;</span>
<a name="l00339"></a>00339                                <span class="stringliteral">&quot; requested storage space.&quot;</span>);
<a name="l00340"></a>00340       d-&gt;o = <span class="keyword">this</span>;
<a name="l00341"></a>00341       d-&gt;f = f;
<a name="l00342"></a>00342       d-&gt;f_data = f_data;
<a name="l00343"></a>00343       d-&gt;mf = NULL;
<a name="l00344"></a>00344       d-&gt;vf = NULL;
<a name="l00345"></a>00345       d-&gt;munge_destroy = d-&gt;munge_copy = NULL;
<a name="l00346"></a>00346       mythrow(nlopt_set_max_objective(o, myfunc, d));
<a name="l00347"></a>00347     }
<a name="l00348"></a>00348 
<a name="l00349"></a>00349 
<a name="l00350"></a>00350     <span class="keywordtype">void</span> set_max_objective(svfunc vf, <span class="keywordtype">void</span> *f_data)
<a name="l00351"></a>00351     {
<a name="l00352"></a>00352       myfunc_data *d = <span class="keyword">new</span> myfunc_data;
<a name="l00353"></a>00353       <span class="keywordflow">if</span> (!d)
<a name="l00354"></a>00354         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a>(<span class="stringliteral">&quot;SeldonOpt::set_max_objective(func f, &quot;</span>
<a name="l00355"></a>00355                                <span class="stringliteral">&quot;void *f_data)&quot;</span>, <span class="stringliteral">&quot;Nlopt failed to allocate the&quot;</span>
<a name="l00356"></a>00356                                <span class="stringliteral">&quot; requested storage space.&quot;</span>);
<a name="l00357"></a>00357       d-&gt;o = <span class="keyword">this</span>;
<a name="l00358"></a>00358       d-&gt;f = NULL;
<a name="l00359"></a>00359       d-&gt;f_data = f_data;
<a name="l00360"></a>00360       d-&gt;mf = NULL;
<a name="l00361"></a>00361       d-&gt;vf = vf;
<a name="l00362"></a>00362       d-&gt;munge_destroy = d-&gt;munge_copy = NULL;
<a name="l00363"></a>00363       mythrow(nlopt_set_max_objective(o, myvfunc, d));
<a name="l00364"></a>00364       alloc_tmp();
<a name="l00365"></a>00365     }
<a name="l00366"></a>00366 
<a name="l00367"></a>00367 
<a name="l00368"></a>00368     <span class="keywordtype">void</span> set_min_objective(func f, <span class="keywordtype">void</span> *f_data,
<a name="l00369"></a>00369                            nlopt_munge md, nlopt_munge mc)
<a name="l00370"></a>00370     {
<a name="l00371"></a>00371       myfunc_data *d = <span class="keyword">new</span> myfunc_data;
<a name="l00372"></a>00372       <span class="keywordflow">if</span> (!d)
<a name="l00373"></a>00373         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a>(<span class="stringliteral">&quot;SeldonOpt::set_min_objective(func f, &quot;</span>
<a name="l00374"></a>00374                                <span class="stringliteral">&quot;void *f_data)&quot;</span>, <span class="stringliteral">&quot;Nlopt failed to allocate the&quot;</span>
<a name="l00375"></a>00375                                <span class="stringliteral">&quot; requested storage space.&quot;</span>);
<a name="l00376"></a>00376       d-&gt;o = <span class="keyword">this</span>;
<a name="l00377"></a>00377       d-&gt;f = f;
<a name="l00378"></a>00378       d-&gt;f_data = f_data;
<a name="l00379"></a>00379       d-&gt;mf = NULL;
<a name="l00380"></a>00380       d-&gt;vf = NULL;
<a name="l00381"></a>00381       d-&gt;munge_destroy = md;
<a name="l00382"></a>00382       d-&gt;munge_copy = mc;
<a name="l00383"></a>00383       mythrow(nlopt_set_min_objective(o, myfunc, d));
<a name="l00384"></a>00384     }
<a name="l00385"></a>00385 
<a name="l00386"></a>00386 
<a name="l00387"></a>00387     <span class="keywordtype">void</span> set_max_objective(func f, <span class="keywordtype">void</span> *f_data,
<a name="l00388"></a>00388                            nlopt_munge md, nlopt_munge mc)
<a name="l00389"></a>00389     {
<a name="l00390"></a>00390       myfunc_data *d = <span class="keyword">new</span> myfunc_data;
<a name="l00391"></a>00391       <span class="keywordflow">if</span> (!d)
<a name="l00392"></a>00392         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a>(<span class="stringliteral">&quot;SeldonOpt::set_max_objective(func f, &quot;</span>
<a name="l00393"></a>00393                                <span class="stringliteral">&quot;void *f_data)&quot;</span>, <span class="stringliteral">&quot;Nlopt failed to allocate the&quot;</span>
<a name="l00394"></a>00394                                <span class="stringliteral">&quot; requested storage space.&quot;</span>);
<a name="l00395"></a>00395       d-&gt;o = <span class="keyword">this</span>;
<a name="l00396"></a>00396       d-&gt;f = f;
<a name="l00397"></a>00397       d-&gt;f_data = f_data;
<a name="l00398"></a>00398       d-&gt;mf = NULL;
<a name="l00399"></a>00399       d-&gt;vf = NULL;
<a name="l00400"></a>00400       d-&gt;munge_destroy = md;
<a name="l00401"></a>00401       d-&gt;munge_copy = mc;
<a name="l00402"></a>00402       mythrow(nlopt_set_max_objective(o, myfunc, d));
<a name="l00403"></a>00403     }
<a name="l00404"></a>00404 
<a name="l00405"></a>00405 
<a name="l00406"></a>00406     <span class="keywordtype">void</span> remove_inequality_constraints()
<a name="l00407"></a>00407     {
<a name="l00408"></a>00408       nlopt_result ret = nlopt_remove_inequality_constraints(o);
<a name="l00409"></a>00409       mythrow(ret);
<a name="l00410"></a>00410     }
<a name="l00411"></a>00411 
<a name="l00412"></a>00412 
<a name="l00413"></a>00413     <span class="keywordtype">void</span> add_inequality_constraint(func f, <span class="keywordtype">void</span> *f_data, <span class="keywordtype">double</span> tol = 0)
<a name="l00414"></a>00414     {
<a name="l00415"></a>00415       myfunc_data *d = <span class="keyword">new</span> myfunc_data;
<a name="l00416"></a>00416       <span class="keywordflow">if</span> (!d)
<a name="l00417"></a>00417         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a>(<span class="stringliteral">&quot;SeldonOpt::add_inequality_constraint(func f, &quot;</span>
<a name="l00418"></a>00418                                <span class="stringliteral">&quot;void *f_data, double tol = 0)&quot;</span>,
<a name="l00419"></a>00419                                <span class="stringliteral">&quot;Nlopt failed to allocate the&quot;</span>
<a name="l00420"></a>00420                                <span class="stringliteral">&quot; requested storage space.&quot;</span>);
<a name="l00421"></a>00421       d-&gt;o = <span class="keyword">this</span>;
<a name="l00422"></a>00422       d-&gt;f = f;
<a name="l00423"></a>00423       d-&gt;f_data = f_data;
<a name="l00424"></a>00424       d-&gt;mf = NULL;
<a name="l00425"></a>00425       d-&gt;vf = NULL;
<a name="l00426"></a>00426       d-&gt;munge_destroy = d-&gt;munge_copy = NULL;
<a name="l00427"></a>00427       mythrow(nlopt_add_inequality_constraint(o, myfunc, d, tol));
<a name="l00428"></a>00428     }
<a name="l00429"></a>00429 
<a name="l00430"></a>00430 
<a name="l00431"></a>00431     <span class="keywordtype">void</span> add_inequality_constraint(svfunc vf, <span class="keywordtype">void</span> *f_data,
<a name="l00432"></a>00432                                    <span class="keywordtype">double</span> tol = 0)
<a name="l00433"></a>00433     {
<a name="l00434"></a>00434       myfunc_data *d = <span class="keyword">new</span> myfunc_data;
<a name="l00435"></a>00435       <span class="keywordflow">if</span> (!d)
<a name="l00436"></a>00436         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a>(<span class="stringliteral">&quot;SeldonOpt::add_inequality_constraint(func f, &quot;</span>
<a name="l00437"></a>00437                                <span class="stringliteral">&quot;void *f_data, double tol = 0)&quot;</span>,
<a name="l00438"></a>00438                                <span class="stringliteral">&quot;Nlopt failed to allocate the&quot;</span>
<a name="l00439"></a>00439                                <span class="stringliteral">&quot; requested storage space.&quot;</span>);
<a name="l00440"></a>00440       d-&gt;o = <span class="keyword">this</span>;
<a name="l00441"></a>00441       d-&gt;f = NULL;
<a name="l00442"></a>00442       d-&gt;f_data = f_data;
<a name="l00443"></a>00443       d-&gt;mf = NULL;
<a name="l00444"></a>00444       d-&gt;vf = vf;
<a name="l00445"></a>00445       d-&gt;munge_destroy = d-&gt;munge_copy = NULL;
<a name="l00446"></a>00446       mythrow(nlopt_add_inequality_constraint(o, myvfunc, d, tol));
<a name="l00447"></a>00447       alloc_tmp();
<a name="l00448"></a>00448     }
<a name="l00449"></a>00449 
<a name="l00450"></a>00450 
<a name="l00451"></a>00451     <span class="keywordtype">void</span> add_inequality_mconstraint(mfunc mf, <span class="keywordtype">void</span> *f_data,
<a name="l00452"></a>00452                                     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Seldon::Vector&lt;double&gt;</a> &amp;tol)
<a name="l00453"></a>00453     {
<a name="l00454"></a>00454       myfunc_data *d = <span class="keyword">new</span> myfunc_data;
<a name="l00455"></a>00455       <span class="keywordflow">if</span> (!d)
<a name="l00456"></a>00456         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a>(<span class="stringliteral">&quot;SeldonOpt::add_inequality_mconstraint(func f,&quot;</span>
<a name="l00457"></a>00457                                <span class="stringliteral">&quot; void *f_data, double tol = 0)&quot;</span>,
<a name="l00458"></a>00458                                <span class="stringliteral">&quot;Nlopt failed to allocate the&quot;</span>
<a name="l00459"></a>00459                                <span class="stringliteral">&quot; requested storage space.&quot;</span>);
<a name="l00460"></a>00460       d-&gt;o = <span class="keyword">this</span>;
<a name="l00461"></a>00461       d-&gt;mf = mf;
<a name="l00462"></a>00462       d-&gt;f_data = f_data;
<a name="l00463"></a>00463       d-&gt;f = NULL;
<a name="l00464"></a>00464       d-&gt;vf = NULL;
<a name="l00465"></a>00465       d-&gt;munge_destroy = d-&gt;munge_copy = NULL;
<a name="l00466"></a>00466       mythrow(nlopt_add_inequality_mconstraint(o, tol.GetSize(),
<a name="l00467"></a>00467                                                mymfunc, d,
<a name="l00468"></a>00468                                                tol.GetSize() == 0
<a name="l00469"></a>00469                                                ? NULL : tol.GetData()));
<a name="l00470"></a>00470     }
<a name="l00471"></a>00471 
<a name="l00472"></a>00472 
<a name="l00473"></a>00473     <span class="keywordtype">void</span> remove_equality_constraints()
<a name="l00474"></a>00474     {
<a name="l00475"></a>00475       nlopt_result ret = nlopt_remove_equality_constraints(o);
<a name="l00476"></a>00476       mythrow(ret);
<a name="l00477"></a>00477     }
<a name="l00478"></a>00478 
<a name="l00479"></a>00479 
<a name="l00480"></a>00480     <span class="keywordtype">void</span> add_equality_constraint(func f, <span class="keywordtype">void</span> *f_data, <span class="keywordtype">double</span> tol = 0)
<a name="l00481"></a>00481     {
<a name="l00482"></a>00482       myfunc_data *d = <span class="keyword">new</span> myfunc_data;
<a name="l00483"></a>00483       <span class="keywordflow">if</span> (!d)
<a name="l00484"></a>00484         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a>(<span class="stringliteral">&quot;SeldonOpt::add_equality_constraint(func f, &quot;</span>
<a name="l00485"></a>00485                                <span class="stringliteral">&quot;void *f_data, double tol = 0)&quot;</span>,
<a name="l00486"></a>00486                                <span class="stringliteral">&quot;Nlopt failed to allocate the&quot;</span>
<a name="l00487"></a>00487                                <span class="stringliteral">&quot; requested storage space.&quot;</span>);
<a name="l00488"></a>00488       d-&gt;o = <span class="keyword">this</span>;
<a name="l00489"></a>00489       d-&gt;f = f;
<a name="l00490"></a>00490       d-&gt;f_data = f_data;
<a name="l00491"></a>00491       d-&gt;mf = NULL;
<a name="l00492"></a>00492       d-&gt;vf = NULL;
<a name="l00493"></a>00493       d-&gt;munge_destroy = d-&gt;munge_copy = NULL;
<a name="l00494"></a>00494       nlopt_add_equality_constraint(o, myfunc, d, tol);
<a name="l00495"></a>00495       mythrow(nlopt_add_equality_constraint(o, myfunc, d, tol));
<a name="l00496"></a>00496     }
<a name="l00497"></a>00497 
<a name="l00498"></a>00498 
<a name="l00499"></a>00499     <span class="keywordtype">void</span> add_equality_constraint(svfunc vf, <span class="keywordtype">void</span> *f_data, <span class="keywordtype">double</span> tol = 0)
<a name="l00500"></a>00500     {
<a name="l00501"></a>00501       myfunc_data *d = <span class="keyword">new</span> myfunc_data;
<a name="l00502"></a>00502       <span class="keywordflow">if</span> (!d)
<a name="l00503"></a>00503         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a>(<span class="stringliteral">&quot;SeldonOpt::add_equality_constraint(func f, &quot;</span>
<a name="l00504"></a>00504                                <span class="stringliteral">&quot;void *f_data, double tol = 0)&quot;</span>,
<a name="l00505"></a>00505                                <span class="stringliteral">&quot;Nlopt failed to allocate the&quot;</span>
<a name="l00506"></a>00506                                <span class="stringliteral">&quot; requested storage space.&quot;</span>);
<a name="l00507"></a>00507       d-&gt;o = <span class="keyword">this</span>;
<a name="l00508"></a>00508       d-&gt;f = NULL;
<a name="l00509"></a>00509       d-&gt;f_data = f_data;
<a name="l00510"></a>00510       d-&gt;mf = NULL;
<a name="l00511"></a>00511       d-&gt;vf = vf;
<a name="l00512"></a>00512       d-&gt;munge_destroy = d-&gt;munge_copy = NULL;
<a name="l00513"></a>00513       mythrow(nlopt_add_equality_constraint(o, myvfunc, d, tol));
<a name="l00514"></a>00514       alloc_tmp();
<a name="l00515"></a>00515     }
<a name="l00516"></a>00516 
<a name="l00517"></a>00517 
<a name="l00518"></a>00518     <span class="keywordtype">void</span> add_equality_mconstraint(mfunc mf, <span class="keywordtype">void</span> *f_data,
<a name="l00519"></a>00519                                   <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Seldon::Vector&lt;double&gt;</a> &amp;tol)
<a name="l00520"></a>00520     {
<a name="l00521"></a>00521       myfunc_data *d = <span class="keyword">new</span> myfunc_data;
<a name="l00522"></a>00522       <span class="keywordflow">if</span> (!d)
<a name="l00523"></a>00523         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a>(<span class="stringliteral">&quot;SeldonOpt::add_equality_mconstraint(func f, &quot;</span>
<a name="l00524"></a>00524                                <span class="stringliteral">&quot;void *f_data, double tol = 0)&quot;</span>,
<a name="l00525"></a>00525                                <span class="stringliteral">&quot;Nlopt failed to allocate the&quot;</span>
<a name="l00526"></a>00526                                <span class="stringliteral">&quot; requested storage space.&quot;</span>);
<a name="l00527"></a>00527       d-&gt;o = <span class="keyword">this</span>;
<a name="l00528"></a>00528       d-&gt;mf = mf;
<a name="l00529"></a>00529       d-&gt;f_data = f_data;
<a name="l00530"></a>00530       d-&gt;f = NULL;
<a name="l00531"></a>00531       d-&gt;vf = NULL;
<a name="l00532"></a>00532       d-&gt;munge_destroy = d-&gt;munge_copy = NULL;
<a name="l00533"></a>00533       mythrow(nlopt_add_equality_mconstraint(o, tol.GetSize(),
<a name="l00534"></a>00534                                              mymfunc, d,
<a name="l00535"></a>00535                                              tol.GetSize() == 0 ?
<a name="l00536"></a>00536                                              NULL : tol.GetData()));
<a name="l00537"></a>00537     }
<a name="l00538"></a>00538 
<a name="l00539"></a>00539 
<a name="l00540"></a>00540     <span class="comment">// For internal use in SWIG wrappers (see also above)</span>
<a name="l00541"></a>00541     <span class="keywordtype">void</span> add_inequality_constraint(func f, <span class="keywordtype">void</span> *f_data,
<a name="l00542"></a>00542                                    nlopt_munge md, nlopt_munge mc,
<a name="l00543"></a>00543                                    <span class="keywordtype">double</span> tol=0)
<a name="l00544"></a>00544     {
<a name="l00545"></a>00545       myfunc_data *d = <span class="keyword">new</span> myfunc_data;
<a name="l00546"></a>00546       <span class="keywordflow">if</span> (!d)
<a name="l00547"></a>00547         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a>(<span class="stringliteral">&quot;SeldonOpt::add_inequality_constraint(func f, &quot;</span>
<a name="l00548"></a>00548                                <span class="stringliteral">&quot;void *f_data, double tol = 0)&quot;</span>,
<a name="l00549"></a>00549                                <span class="stringliteral">&quot;Nlopt failed to allocate the&quot;</span>
<a name="l00550"></a>00550                                <span class="stringliteral">&quot; requested storage space.&quot;</span>);
<a name="l00551"></a>00551       d-&gt;o = <span class="keyword">this</span>;
<a name="l00552"></a>00552       d-&gt;f = f;
<a name="l00553"></a>00553       d-&gt;f_data = f_data;
<a name="l00554"></a>00554       d-&gt;mf = NULL;
<a name="l00555"></a>00555       d-&gt;vf = NULL;
<a name="l00556"></a>00556       d-&gt;munge_destroy = md;
<a name="l00557"></a>00557       d-&gt;munge_copy = mc;
<a name="l00558"></a>00558       mythrow(nlopt_add_inequality_constraint(o, myfunc, d, tol));
<a name="l00559"></a>00559     }
<a name="l00560"></a>00560 
<a name="l00561"></a>00561 
<a name="l00562"></a>00562     <span class="keywordtype">void</span> add_equality_constraint(func f, <span class="keywordtype">void</span> *f_data,
<a name="l00563"></a>00563                                  nlopt_munge md, nlopt_munge mc,
<a name="l00564"></a>00564                                  <span class="keywordtype">double</span> tol=0)
<a name="l00565"></a>00565     {
<a name="l00566"></a>00566       myfunc_data *d = <span class="keyword">new</span> myfunc_data;
<a name="l00567"></a>00567       <span class="keywordflow">if</span> (!d)
<a name="l00568"></a>00568         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a>(<span class="stringliteral">&quot;SeldonOpt::add_inequality_constraint(func f, &quot;</span>
<a name="l00569"></a>00569                                <span class="stringliteral">&quot;void *f_data, double tol = 0)&quot;</span>,
<a name="l00570"></a>00570                                <span class="stringliteral">&quot;Nlopt failed to allocate the&quot;</span>
<a name="l00571"></a>00571                                <span class="stringliteral">&quot; requested storage space.&quot;</span>);
<a name="l00572"></a>00572       d-&gt;o = <span class="keyword">this</span>;
<a name="l00573"></a>00573       d-&gt;f = f;
<a name="l00574"></a>00574       d-&gt;f_data = f_data;
<a name="l00575"></a>00575       d-&gt;mf = NULL;
<a name="l00576"></a>00576       d-&gt;vf = NULL;
<a name="l00577"></a>00577       d-&gt;munge_destroy = md;
<a name="l00578"></a>00578       d-&gt;munge_copy = mc;
<a name="l00579"></a>00579       mythrow(nlopt_add_equality_constraint(o, myfunc, d, tol));
<a name="l00580"></a>00580     }
<a name="l00581"></a>00581 
<a name="l00582"></a>00582 
<a name="l00583"></a>00583     <span class="keywordtype">void</span> add_inequality_mconstraint(mfunc mf, <span class="keywordtype">void</span> *f_data,
<a name="l00584"></a>00584                                     nlopt_munge md, nlopt_munge mc,
<a name="l00585"></a>00585                                     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Seldon::Vector&lt;double&gt;</a> &amp;tol)
<a name="l00586"></a>00586     {
<a name="l00587"></a>00587       myfunc_data *d = <span class="keyword">new</span> myfunc_data;
<a name="l00588"></a>00588       <span class="keywordflow">if</span> (!d)
<a name="l00589"></a>00589         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a>(<span class="stringliteral">&quot;SeldonOpt::add_inequality_mconstraint(func f,&quot;</span>
<a name="l00590"></a>00590                                <span class="stringliteral">&quot; void *f_data, double tol = 0)&quot;</span>,
<a name="l00591"></a>00591                                <span class="stringliteral">&quot;Nlopt failed to allocate the&quot;</span>
<a name="l00592"></a>00592                                <span class="stringliteral">&quot; requested storage space.&quot;</span>);
<a name="l00593"></a>00593       d-&gt;o = <span class="keyword">this</span>;
<a name="l00594"></a>00594       d-&gt;mf = mf;
<a name="l00595"></a>00595       d-&gt;f_data = f_data;
<a name="l00596"></a>00596       d-&gt;f = NULL;
<a name="l00597"></a>00597       d-&gt;vf = NULL;
<a name="l00598"></a>00598       d-&gt;munge_destroy = md; d-&gt;munge_copy = mc;
<a name="l00599"></a>00599       mythrow(nlopt_add_inequality_mconstraint(o, tol.GetSize(),
<a name="l00600"></a>00600                                                mymfunc, d,
<a name="l00601"></a>00601                                                tol.GetSize() == 0
<a name="l00602"></a>00602                                                ? NULL : tol.GetData()));
<a name="l00603"></a>00603     }
<a name="l00604"></a>00604 
<a name="l00605"></a>00605 
<a name="l00606"></a>00606     <span class="keywordtype">void</span> add_equality_mconstraint(mfunc mf, <span class="keywordtype">void</span> *f_data,
<a name="l00607"></a>00607                                   nlopt_munge md, nlopt_munge mc,
<a name="l00608"></a>00608                                   <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Seldon::Vector&lt;double&gt;</a> &amp;tol)
<a name="l00609"></a>00609     {
<a name="l00610"></a>00610       myfunc_data *d = <span class="keyword">new</span> myfunc_data;
<a name="l00611"></a>00611       <span class="keywordflow">if</span> (!d)
<a name="l00612"></a>00612         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">Seldon::NoMemory</a>(<span class="stringliteral">&quot;SeldonOpt::add_equality_mconstraint(mfunc mf,&quot;</span>
<a name="l00613"></a>00613                                <span class="stringliteral">&quot; void *f_data,&quot;</span>
<a name="l00614"></a>00614                                <span class="stringliteral">&quot;nlopt_munge md, nlopt_munge mc,&quot;</span>
<a name="l00615"></a>00615                                <span class="stringliteral">&quot;const Seldon::Vector&lt;double&gt; &amp;tol)&quot;</span>,
<a name="l00616"></a>00616                                <span class="stringliteral">&quot;Nlopt failed to allocate the&quot;</span>
<a name="l00617"></a>00617                                <span class="stringliteral">&quot; requested storage space.&quot;</span>);
<a name="l00618"></a>00618       d-&gt;o = <span class="keyword">this</span>;
<a name="l00619"></a>00619       d-&gt;mf = mf;
<a name="l00620"></a>00620       d-&gt;f_data = f_data;
<a name="l00621"></a>00621       d-&gt;f = NULL;
<a name="l00622"></a>00622       d-&gt;vf = NULL;
<a name="l00623"></a>00623       d-&gt;munge_destroy = md;
<a name="l00624"></a>00624       d-&gt;munge_copy = mc;
<a name="l00625"></a>00625       mythrow(nlopt_add_equality_mconstraint(o, tol.GetSize(), mymfunc,
<a name="l00626"></a>00626                                              d, tol.GetSize() == 0
<a name="l00627"></a>00627                                              ? NULL :
<a name="l00628"></a>00628                                              tol.GetData()));
<a name="l00629"></a>00629     }
<a name="l00630"></a>00630 
<a name="l00631"></a>00631 
<a name="l00632"></a>00632 <span class="preprocessor">#define SELDON_NLOPT_GETSET_VEC(name)                                   \</span>
<a name="l00633"></a>00633 <span class="preprocessor">    void set_##name(double val) {                                       \</span>
<a name="l00634"></a>00634 <span class="preprocessor">      mythrow(nlopt_set_##name##1(o, val));                             \</span>
<a name="l00635"></a>00635 <span class="preprocessor">    }                                                                   \</span>
<a name="l00636"></a>00636 <span class="preprocessor">    void get_##name(Seldon::Vector&lt;double&gt; &amp;v) const {                  \</span>
<a name="l00637"></a>00637 <span class="preprocessor">      if (o &amp;&amp; int(nlopt_get_dimension(o)) != v.GetSize())              \</span>
<a name="l00638"></a>00638 <span class="preprocessor">        throw Seldon::WrongArgument(&quot;SeldonOpt::get_&quot; #name &quot;(Vector&amp;)&quot; \</span>
<a name="l00639"></a>00639 <span class="preprocessor">                                    &quot; const&quot;,                           \</span>
<a name="l00640"></a>00640 <span class="preprocessor">                                    &quot;Nlopt invalid argument.&quot;);         \</span>
<a name="l00641"></a>00641 <span class="preprocessor">      mythrow(nlopt_get_##name(o, v.GetSize() == 0 ? NULL :             \</span>
<a name="l00642"></a>00642 <span class="preprocessor">                               v.GetData()));                           \</span>
<a name="l00643"></a>00643 <span class="preprocessor">    }                                                                   \</span>
<a name="l00644"></a>00644 <span class="preprocessor">    Seldon::Vector&lt;double&gt; get_##name() const {                         \</span>
<a name="l00645"></a>00645 <span class="preprocessor">      if (!o) throw                                                     \</span>
<a name="l00646"></a>00646 <span class="preprocessor">                Seldon::Error(&quot;SeldonOpt::get_&quot; #name &quot;() const&quot;,       \</span>
<a name="l00647"></a>00647 <span class="preprocessor">                              &quot;Uninitialized nlopt::SeldonOpt.&quot;);       \</span>
<a name="l00648"></a>00648 <span class="preprocessor">      Seldon::Vector&lt;double&gt; v(nlopt_get_dimension(o));                 \</span>
<a name="l00649"></a>00649 <span class="preprocessor">      get_##name(v);                                                    \</span>
<a name="l00650"></a>00650 <span class="preprocessor">      return v;                                                         \</span>
<a name="l00651"></a>00651 <span class="preprocessor">    }                                                                   \</span>
<a name="l00652"></a>00652 <span class="preprocessor">    void set_##name(const Seldon::Vector&lt;double&gt; &amp;v) {                  \</span>
<a name="l00653"></a>00653 <span class="preprocessor">      if (o &amp;&amp; int(nlopt_get_dimension(o)) != v.GetSize())              \</span>
<a name="l00654"></a>00654 <span class="preprocessor">        throw Seldon::WrongArgument(&quot;SeldonOpt::get_&quot; #name &quot;(Vector&amp;)&quot; \</span>
<a name="l00655"></a>00655 <span class="preprocessor">                                    &quot; const&quot;,                           \</span>
<a name="l00656"></a>00656 <span class="preprocessor">                                    &quot;Nlopt invalid argument.&quot;);         \</span>
<a name="l00657"></a>00657 <span class="preprocessor">      mythrow(nlopt_set_##name(o, v.GetSize() == 0 ?                    \</span>
<a name="l00658"></a>00658 <span class="preprocessor">                               NULL : v.GetData()));                    \</span>
<a name="l00659"></a>00659 <span class="preprocessor">    }</span>
<a name="l00660"></a>00660 <span class="preprocessor"></span>
<a name="l00661"></a>00661     SELDON_NLOPT_GETSET_VEC(lower_bounds)
<a name="l00662"></a>00662     SELDON_NLOPT_GETSET_VEC(upper_bounds)
<a name="l00663"></a>00663 
<a name="l00664"></a>00664 
<a name="l00665"></a>00665 <span class="preprocessor">#define SELDON_NLOPT_GETSET(T, name)                                    \</span>
<a name="l00666"></a>00666 <span class="preprocessor">      T get_##name() const {                                            \</span>
<a name="l00667"></a>00667 <span class="preprocessor">        if (!o) throw                                                   \</span>
<a name="l00668"></a>00668 <span class="preprocessor">                  Seldon::Error(&quot;SeldonOpt::get_&quot; #name &quot;() const&quot;,     \</span>
<a name="l00669"></a>00669 <span class="preprocessor">                                &quot;Uninitialized nlopt::SeldonOpt.&quot;);     \</span>
<a name="l00670"></a>00670 <span class="preprocessor">        return nlopt_get_##name(o);                                     \</span>
<a name="l00671"></a>00671 <span class="preprocessor">      }                                                                 \</span>
<a name="l00672"></a>00672 <span class="preprocessor">      void set_##name(T name) {                                         \</span>
<a name="l00673"></a>00673 <span class="preprocessor">        mythrow(nlopt_set_##name(o, name));                             \</span>
<a name="l00674"></a>00674 <span class="preprocessor">      }</span>
<a name="l00675"></a>00675 <span class="preprocessor"></span>
<a name="l00676"></a>00676 
<a name="l00677"></a>00677     SELDON_NLOPT_GETSET(<span class="keywordtype">double</span>, stopval)
<a name="l00678"></a>00678     SELDON_NLOPT_GETSET(<span class="keywordtype">double</span>, ftol_rel)
<a name="l00679"></a>00679     SELDON_NLOPT_GETSET(<span class="keywordtype">double</span>, ftol_abs)
<a name="l00680"></a>00680     SELDON_NLOPT_GETSET(<span class="keywordtype">double</span>, xtol_rel)
<a name="l00681"></a>00681     SELDON_NLOPT_GETSET_VEC(xtol_abs)
<a name="l00682"></a>00682     SELDON_NLOPT_GETSET(<span class="keywordtype">int</span>, maxeval)
<a name="l00683"></a>00683     SELDON_NLOPT_GETSET(<span class="keywordtype">double</span>, maxtime)
<a name="l00684"></a>00684 
<a name="l00685"></a>00685     SELDON_NLOPT_GETSET(<span class="keywordtype">int</span>, force_stop)
<a name="l00686"></a>00686 
<a name="l00687"></a>00687 
<a name="l00688"></a>00688     <span class="keywordtype">void</span> force_stop()
<a name="l00689"></a>00689     {
<a name="l00690"></a>00690       set_force_stop(1);
<a name="l00691"></a>00691     }
<a name="l00692"></a>00692 
<a name="l00693"></a>00693 
<a name="l00694"></a>00694     <span class="keywordtype">void</span> set_local_optimizer(<span class="keyword">const</span> <a class="code" href="classnlopt_1_1_seldon_opt.php">SeldonOpt</a> &amp;lo)
<a name="l00695"></a>00695     {
<a name="l00696"></a>00696       nlopt_result ret = nlopt_set_local_optimizer(o, lo.o);
<a name="l00697"></a>00697       mythrow(ret);
<a name="l00698"></a>00698     }
<a name="l00699"></a>00699 
<a name="l00700"></a>00700 
<a name="l00701"></a>00701     SELDON_NLOPT_GETSET(<span class="keywordtype">unsigned</span>, population)
<a name="l00702"></a>00702     SELDON_NLOPT_GETSET_VEC(initial_step)
<a name="l00703"></a>00703 
<a name="l00704"></a>00704 
<a name="l00705"></a>00705     <span class="keywordtype">void</span> set_default_initial_step(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Seldon::Vector&lt;double&gt;</a>&amp; x)
<a name="l00706"></a>00706     {
<a name="l00707"></a>00707       nlopt_result ret
<a name="l00708"></a>00708         = nlopt_set_default_initial_step(o, x.GetSize() == 0 ?
<a name="l00709"></a>00709                                          NULL : x.GetData());
<a name="l00710"></a>00710       mythrow(ret);
<a name="l00711"></a>00711     }
<a name="l00712"></a>00712 
<a name="l00713"></a>00713 
<a name="l00714"></a>00714     <span class="keywordtype">void</span> get_initial_step(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Seldon::Vector&lt;double&gt;</a> &amp;x,
<a name="l00715"></a>00715                           <a class="code" href="class_seldon_1_1_vector.php">Seldon::Vector&lt;double&gt;</a> &amp;dx)<span class="keyword"> const</span>
<a name="l00716"></a>00716 <span class="keyword">    </span>{
<a name="l00717"></a>00717       <span class="keywordflow">if</span> (o &amp;&amp; (<span class="keywordtype">int</span>(nlopt_get_dimension(o)) != x.GetSize()
<a name="l00718"></a>00718                 || int(nlopt_get_dimension(o)) != dx.GetSize()))
<a name="l00719"></a>00719         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">Seldon::WrongArgument</a>(<span class="stringliteral">&quot;SeldonOpt::get_initial_step(&quot;</span>
<a name="l00720"></a>00720                                     <span class="stringliteral">&quot;Vector&lt;double&gt;&amp; x, double&amp; dx)&quot;</span>,
<a name="l00721"></a>00721                                     <span class="stringliteral">&quot;Dimension mismatch.&quot;</span>);
<a name="l00722"></a>00722       nlopt_result ret = nlopt_get_initial_step(o, x.GetSize() == 0 ?
<a name="l00723"></a>00723                                                 NULL : x.GetData(),
<a name="l00724"></a>00724                                                 dx.GetSize() == 0 ?
<a name="l00725"></a>00725                                                 NULL : dx.GetData());
<a name="l00726"></a>00726       mythrow(ret);
<a name="l00727"></a>00727     }
<a name="l00728"></a>00728 
<a name="l00729"></a>00729 
<a name="l00730"></a>00730     <a class="code" href="class_seldon_1_1_vector.php">Seldon::Vector&lt;double&gt;</a> get_initial_step_(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Seldon::Vector&lt;double&gt;</a>&amp; x)<span class="keyword"></span>
<a name="l00731"></a>00731 <span class="keyword">    const</span>
<a name="l00732"></a>00732 <span class="keyword">    </span>{
<a name="l00733"></a>00733       <span class="keywordflow">if</span> (!o)
<a name="l00734"></a>00734         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_error.php">Seldon::Error</a>(<span class="stringliteral">&quot;SeldonOpt::get_initial_step_&quot;</span>
<a name="l00735"></a>00735                             <span class="stringliteral">&quot;(const Seldon::Vector&lt;double&gt;&amp; x)&quot;</span>,
<a name="l00736"></a>00736                             <span class="stringliteral">&quot;Uninitialized nlopt::SeldonOpt.&quot;</span>);
<a name="l00737"></a>00737       <a class="code" href="class_seldon_1_1_vector.php">Seldon::Vector&lt;double&gt;</a> v(nlopt_get_dimension(o));
<a name="l00738"></a>00738       get_initial_step(x, v);
<a name="l00739"></a>00739       <span class="keywordflow">return</span> v;
<a name="l00740"></a>00740     }
<a name="l00741"></a>00741   };
<a name="l00742"></a>00742 
<a name="l00743"></a>00743 
<a name="l00744"></a>00744 <span class="preprocessor">#undef SELDON_NLOPT_GETSET</span>
<a name="l00745"></a>00745 <span class="preprocessor"></span><span class="preprocessor">#undef SELDON_NLOPT_GETSET_VEC</span>
<a name="l00746"></a>00746 <span class="preprocessor"></span>
<a name="l00747"></a>00747 
<a name="l00748"></a>00748 }
<a name="l00749"></a>00749 
<a name="l00750"></a>00750 <span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
