<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_vector___base.php">Vector_Base</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a> &#124;
<a href="#pro-static-attribs">Static Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Vector_Base&lt; T, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Vector_Base" -->
<p>Base structure for all vectors.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_vector_8hxx_source.php">Vector.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::Vector_Base&lt; T, Allocator &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_vector___base.png" usemap="#Seldon::Vector_Base&lt; T, Allocator &gt;_map" alt=""/>
  <map id="Seldon::Vector_Base&lt; T, Allocator &gt;_map" name="Seldon::Vector_Base&lt; T, Allocator &gt;_map">
<area href="class_seldon_1_1_p_e_t_sc_vector.php" alt="Seldon::PETScVector&lt; T, Allocator &gt;" shape="rect" coords="135,56,396,80"/>
<area href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php" alt="Seldon::Vector&lt; T, Collection, Allocator &gt;" shape="rect" coords="406,56,667,80"/>
<area href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" alt="Seldon::Vector&lt; T, VectFull, Allocator &gt;" shape="rect" coords="677,56,938,80"/>
<area href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_par_00_01_allocator_01_4.php" alt="Seldon::Vector&lt; T, PETScPar, Allocator &gt;" shape="rect" coords="0,112,261,136"/>
<area href="class_seldon_1_1_vector_3_01_t_00_01_p_e_t_sc_seq_00_01_allocator_01_4.php" alt="Seldon::Vector&lt; T, PETScSeq, Allocator &gt;" shape="rect" coords="271,112,532,136"/>
<area href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php" alt="Seldon::Vector&lt; T, VectSparse, Allocator &gt;" shape="rect" coords="677,112,938,136"/>
</map>
</div>

<p><a href="class_seldon_1_1_vector___base-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adbc0377ca27b759613c52091fce99baf"></a><!-- doxytag: member="Seldon::Vector_Base::value_type" ref="adbc0377ca27b759613c52091fce99baf" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a58ff8f594b30f3073ca819d5777931e9"></a><!-- doxytag: member="Seldon::Vector_Base::pointer" ref="a58ff8f594b30f3073ca819d5777931e9" args="" -->
typedef Allocator::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a10d82dbcf34ac7fd64a5e83a9a91523b"></a><!-- doxytag: member="Seldon::Vector_Base::const_pointer" ref="a10d82dbcf34ac7fd64a5e83a9a91523b" args="" -->
typedef Allocator::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a950d1ee76291b86035b7eee7e4871314"></a><!-- doxytag: member="Seldon::Vector_Base::reference" ref="a950d1ee76291b86035b7eee7e4871314" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7b843d8515b1ee847c919303bde64770"></a><!-- doxytag: member="Seldon::Vector_Base::const_reference" ref="a7b843d8515b1ee847c919303bde64770" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#ac8a6cee506f094504138943629b9dcbb">Vector_Base</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#ac8a6cee506f094504138943629b9dcbb"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#aa5d9ca198d4175b9baf9a0543f7376be">Vector_Base</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Main constructor.  <a href="#aa5d9ca198d4175b9baf9a0543f7376be"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a27796125b93f3a771a5c92e8196c9229">Vector_Base</a> (const <a class="el" href="class_seldon_1_1_vector___base.php">Vector_Base</a>&lt; T, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copy constructor.  <a href="#a27796125b93f3a771a5c92e8196c9229"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6da510796ebef867765ea3a037debb1b"></a><!-- doxytag: member="Seldon::Vector_Base::~Vector_Base" ref="a6da510796ebef867765ea3a037debb1b" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a6da510796ebef867765ea3a037debb1b">~Vector_Base</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Destructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae">GetM</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements.  <a href="#a51f50515f0c6d0663465c2cc7de71bae"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a075daae3607a4767a77a3de60ab30ba7">GetLength</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements.  <a href="#a075daae3607a4767a77a3de60ab30ba7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a839880a3113c1885ead70d79fade0294">GetSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored.  <a href="#a839880a3113c1885ead70d79fade0294"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a">GetData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer to data_ (stored data).  <a href="#a4128ad4898e42211d22a7531c9f5f80a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a454aea78c82c4317dfe4160cc7c95afa">GetDataConst</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a const pointer to data_ (stored data).  <a href="#a454aea78c82c4317dfe4160cc7c95afa"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#ad0f5184bd9eec6fca70c54e459ced78f">GetDataVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "void*" to the data array (data_).  <a href="#ad0f5184bd9eec6fca70c54e459ced78f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a0aadc006977de037eb3ee550683e3f19">GetDataConstVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "const void*" to the data array (data_).  <a href="#a0aadc006977de037eb3ee550683e3f19"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac58514f2c122d7538dfc4db4ce0d4da9"></a><!-- doxytag: member="Seldon::Vector_Base::m_" ref="ac58514f2c122d7538dfc4db4ce0d4da9" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>m_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1a6ae822c00f9ee2a798d54a36602430"></a><!-- doxytag: member="Seldon::Vector_Base::data_" ref="a1a6ae822c00f9ee2a798d54a36602430" args="" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-static-attribs"></a>
Static Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6c958ad29a5f7eabd1f085e50046ab85"></a><!-- doxytag: member="Seldon::Vector_Base::vect_allocator_" ref="a6c958ad29a5f7eabd1f085e50046ab85" args="" -->
static Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>vect_allocator_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Allocator = SELDON_DEFAULT_ALLOCATOR&lt;T&gt;&gt;<br/>
 class Seldon::Vector_Base&lt; T, Allocator &gt;</h3>

<p>Base structure for all vectors. </p>
<p>It stores data and the vector size. It defines basic methods as well. </p>

<p>Definition at line <a class="el" href="_vector_8hxx_source.php#l00038">38</a> of file <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="ac8a6cee506f094504138943629b9dcbb"></a><!-- doxytag: member="Seldon::Vector_Base::Vector_Base" ref="ac8a6cee506f094504138943629b9dcbb" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::<a class="el" href="class_seldon_1_1_vector___base.php">Vector_Base</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>Nothing is allocated. <a class="el" href="class_seldon_1_1_vector.php">Vector</a> length is set to zero. </p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00044">44</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa5d9ca198d4175b9baf9a0543f7376be"></a><!-- doxytag: member="Seldon::Vector_Base::Vector_Base" ref="aa5d9ca198d4175b9baf9a0543f7376be" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::<a class="el" href="class_seldon_1_1_vector___base.php">Vector_Base</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline, explicit]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Main constructor. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>length. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Nothing is allocated. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00057">57</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a27796125b93f3a771a5c92e8196c9229"></a><!-- doxytag: member="Seldon::Vector_Base::Vector_Base" ref="a27796125b93f3a771a5c92e8196c9229" args="(const Vector_Base&lt; T, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::<a class="el" href="class_seldon_1_1_vector___base.php">Vector_Base</a> </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector___base.php">Vector_Base</a>&lt; T, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Copy constructor. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>A</em>&nbsp;</td><td>base vector to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Only the length is copied. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00071">71</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="a4128ad4898e42211d22a7531c9f5f80a"></a><!-- doxytag: member="Seldon::Vector_Base::GetData" ref="a4128ad4898e42211d22a7531c9f5f80a" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector___base.php">Vector_Base</a>&lt; T, Allocator &gt;::pointer <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer to data_ (stored data). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer to the data_, i.e. the data array. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00156">156</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a454aea78c82c4317dfe4160cc7c95afa"></a><!-- doxytag: member="Seldon::Vector_Base::GetDataConst" ref="a454aea78c82c4317dfe4160cc7c95afa" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector___base.php">Vector_Base</a>&lt; T, Allocator &gt;::const_pointer <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetDataConst </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a const pointer to data_ (stored data). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer to the data_, i.e. the data array. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00168">168</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0aadc006977de037eb3ee550683e3f19"></a><!-- doxytag: member="Seldon::Vector_Base::GetDataConstVoid" ref="a0aadc006977de037eb3ee550683e3f19" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const void * <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetDataConstVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "const void*" to the data array (data_). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer of type "const void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00191">191</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad0f5184bd9eec6fca70c54e459ced78f"></a><!-- doxytag: member="Seldon::Vector_Base::GetDataVoid" ref="ad0f5184bd9eec6fca70c54e459ced78f" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void * <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetDataVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "void*" to the data array (data_). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00180">180</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a075daae3607a4767a77a3de60ab30ba7"></a><!-- doxytag: member="Seldon::Vector_Base::GetLength" ref="a075daae3607a4767a77a3de60ab30ba7" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetLength </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the vector. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ae501c34e51f6559568846b94e24c249c">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>, and <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a2a2b8d5ad39c79bdcdcd75fa20ae5ae8">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00133">133</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a51f50515f0c6d0663465c2cc7de71bae"></a><!-- doxytag: member="Seldon::Vector_Base::GetM" ref="a51f50515f0c6d0663465c2cc7de71bae" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the vector. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a6f40d71a69e016ae3e1d9db92b6538a4">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>, and <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a9b0a8be2fb500d4f6c2edb4c1a5247ef">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00122">122</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a839880a3113c1885ead70d79fade0294"></a><!-- doxytag: member="Seldon::Vector_Base::GetSize" ref="a839880a3113c1885ead70d79fade0294" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the vector stored. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00144">144</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>vector/<a class="el" href="_vector_8hxx_source.php">Vector.hxx</a></li>
<li>vector/<a class="el" href="_vector_8cxx_source.php">Vector.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
