<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Vector&lt; T, VectSparse, Allocator &gt;</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a> &#124;
<a href="#pro-static-attribs">Static Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Vector&lt; T, VectSparse, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Vector&lt; T, VectSparse, Allocator &gt;" --><!-- doxytag: inherits="Seldon::Vector&lt; T, VectFull, Allocator &gt;" -->
<p>Sparse vector class.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_sparse_vector_8hxx_source.php">SparseVector.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::Vector&lt; T, VectSparse, Allocator &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.png" usemap="#Seldon::Vector&lt; T, VectSparse, Allocator &gt;_map" alt=""/>
  <map id="Seldon::Vector&lt; T, VectSparse, Allocator &gt;_map" name="Seldon::Vector&lt; T, VectSparse, Allocator &gt;_map">
<area href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" alt="Seldon::Vector&lt; T, VectFull, Allocator &gt;" shape="rect" coords="0,56,261,80"/>
<area href="class_seldon_1_1_vector___base.php" alt="Seldon::Vector_Base&lt; T, Allocator &gt;" shape="rect" coords="0,0,261,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9784dbb2ff3a2afc59f219dc730f6c0a"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::value_type" ref="a9784dbb2ff3a2afc59f219dc730f6c0a" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aeab5526384b2018d644421c6e075b855"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::pointer" ref="aeab5526384b2018d644421c6e075b855" args="" -->
typedef Allocator::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aced948575b92facbf4a08a7548228642"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::const_pointer" ref="aced948575b92facbf4a08a7548228642" args="" -->
typedef Allocator::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af272507b55317c2f79bee953f1d1e62f"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::reference" ref="af272507b55317c2f79bee953f1d1e62f" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a961832ca29d590261c4c0f4578695d07"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::const_reference" ref="a961832ca29d590261c4c0f4578695d07" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a862c3ee48a0d8b5bbbb11e5d15341fc6"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::storage" ref="a862c3ee48a0d8b5bbbb11e5d15341fc6" args="" -->
typedef <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>&nbsp;</td><td class="memItemRight" valign="bottom"><b>storage</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#aee3f9a5bfd70e6370ae0c559257624f7">Vector</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#aee3f9a5bfd70e6370ae0c559257624f7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#aa33cfa4d38f149671d6d0ea82f5106d8">Vector</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Main constructor.  <a href="#aa33cfa4d38f149671d6d0ea82f5106d8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a95b6b2c8c2be95a9d5dcdbd892f195e2">Vector</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copy constructor.  <a href="#a95b6b2c8c2be95a9d5dcdbd892f195e2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af0eb42557b7578cfefabbf4d15ad2073"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::~Vector" ref="af0eb42557b7578cfefabbf4d15ad2073" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#af0eb42557b7578cfefabbf4d15ad2073">~Vector</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Destructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a548fa99edadcd9b418912d607b7aa19b">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the vector.  <a href="#a548fa99edadcd9b418912d607b7aa19b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#afcc8fef4210e7703493ad95acf389d19">Reallocate</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight"><a class="el" href="class_seldon_1_1_vector.php">Vector</a> reallocation.  <a href="#afcc8fef4210e7703493ad95acf389d19"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a3637911015d5a0fafa4811ee2204bbc5">Resize</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Changes the number of non-zero entries of the vector.  <a href="#a3637911015d5a0fafa4811ee2204bbc5"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a73034fa2fb67eec70a2d25684f2a0c8c">SetData</a> (int nz, T *data, int *index)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Changes the length of the vector and sets its data array (low level method).  <a href="#a73034fa2fb67eec70a2d25684f2a0c8c"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Allocator2 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#af6c784522606f8bf451b129a8b5069d1">SetData</a> (<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator2 &gt; &amp;data, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt; &amp;index)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Changes the length of the vector and sets its data array (low level method).  <a href="#af6c784522606f8bf451b129a8b5069d1"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Allocator2 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#ab4c128e288cce25e0e8f1998ccf38a1c">SetData</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator2 &gt; &amp;V)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Lets the current vector point to the data of a second vector (low level method).  <a href="#ab4c128e288cce25e0e8f1998ccf38a1c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a985afdfd20e655bb551cf44669acd0e6">Nullify</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the vector without releasing memory.  <a href="#a985afdfd20e655bb551cf44669acd0e6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a51c6d9be9bfc1234e17d997f9bcdff07">Value</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a51c6d9be9bfc1234e17d997f9bcdff07"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#ad733400e0cde2e94b5efd076dc5d956e">Value</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#ad733400e0cde2e94b5efd076dc5d956e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a72d964fb6cbd4e4189473e0298989929">Index</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a72d964fb6cbd4e4189473e0298989929"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a3f0337de5aaabd69cb32432377b5e4d3">Index</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a3f0337de5aaabd69cb32432377b5e4d3"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">value_type&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a001a7d1629309ad90c6d66d695a69e8f">operator()</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a001a7d1629309ad90c6d66d695a69e8f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a61bcf710b6d43a3690561abd27d4b145">Get</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#a61bcf710b6d43a3690561abd27d4b145"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a5805ff818cc9c57a65bc6006c58564a7">Val</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#a5805ff818cc9c57a65bc6006c58564a7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#ad57ac9ab256f25c6f9f244fa6e7b2ccf">Get</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#ad57ac9ab256f25c6f9f244fa6e7b2ccf"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a21f192e6ea09ce5be473e6bb1135c7a9">Val</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#a21f192e6ea09ce5be473e6bb1135c7a9"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a87ef82c6607c68b77a5ec18dbcb15eb0">operator=</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a vector (assignment operator).  <a href="#a87ef82c6607c68b77a5ec18dbcb15eb0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#aefe82d8a7c33e932c20c9fd9e6f499f5">Copy</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a vector.  <a href="#aefe82d8a7c33e932c20c9fd9e6f499f5"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a6a4ed85be713e5a300f94eca786a2c2b">GetIndex</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer to the array containing the indices of the non-zero entries.  <a href="#a6a4ed85be713e5a300f94eca786a2c2b"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt; &amp;&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a1d0196721350369a3154b60dfa105b37">operator=</a> (const T0 &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the vector with a given value.  <a href="#a1d0196721350369a3154b60dfa105b37"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8c5f87f1d06974d31d8ebe8dcc0fcb8e"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Print" ref="a8c5f87f1d06974d31d8ebe8dcc0fcb8e" args="() const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a8c5f87f1d06974d31d8ebe8dcc0fcb8e">Print</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays the vector. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a3f4b6ec8424c938251af5e4b1b130702">Assemble</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Assembles the vector.  <a href="#a3f4b6ec8424c938251af5e4b1b130702"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a6ff2b64975624f059a305a7661c24573">RemoveSmallEntry</a> (const T0 &amp;epsilon)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Removes small entries.  <a href="#a6ff2b64975624f059a305a7661c24573"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a34201f29286c8829e7e8a7e31a266d5b">AddInteraction</a> (int i, const T &amp;val)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds <em>val</em> to the vector component #<em>i</em>.  <a href="#a34201f29286c8829e7e8a7e31a266d5b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a1a60608da32cc0da4be4a1acdbbebc06">AddInteractionRow</a> (int, int *, T *, bool already_sorted=false)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds given values to several components of the vector.  <a href="#a1a60608da32cc0da4be4a1acdbbebc06"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Allocator0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a4625cf1134f2121385cbee020c7fe834">AddInteractionRow</a> (int nb, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt; col, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt; val, bool already_sorted=false)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds given values to several components of the vector.  <a href="#a4625cf1134f2121385cbee020c7fe834"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a246553d78195c2e1c51be7a4e84409c8">Write</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the vector in a file.  <a href="#a246553d78195c2e1c51be7a4e84409c8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a27f19795d0cf6489ab076a6c43ef6756">Write</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the vector in a stream, in binary format.  <a href="#a27f19795d0cf6489ab076a6c43ef6756"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#ab770c6961b3c24de5679fc93530e6e80">WriteText</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the vector in a text file.  <a href="#ab770c6961b3c24de5679fc93530e6e80"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a71b2e3e54ed55920f6b34912382fac80">WriteText</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the vector in a stream, in text format.  <a href="#a71b2e3e54ed55920f6b34912382fac80"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#adc80b24a206e7ef91dd9b51aa33763d0">Read</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the vector from a file in binary format.  <a href="#adc80b24a206e7ef91dd9b51aa33763d0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a4335b7afce7863794f8ffbf76db91cbd">Read</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the vector from a file stream, in binary format.  <a href="#a4335b7afce7863794f8ffbf76db91cbd"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a3b9e7d2529315dfaa75a09d3c34da18a">ReadText</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the vector from a text file.  <a href="#a3b9e7d2529315dfaa75a09d3c34da18a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a554c1d3659fc50f3e119ef14437ad54a">ReadText</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the vector from a file stream, in text format.  <a href="#a554c1d3659fc50f3e119ef14437ad54a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a658e420ab086d76b755b9464bf9a8d66"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::SetData" ref="a658e420ab086d76b755b9464bf9a8d66" args="(int i, pointer data)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetData</b> (int i, pointer data)</td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Allocator0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ab12cedc7bee632a7b210312f1e7ef28b">SetData</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt; &amp;V)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Lets the current vector point to the data of another vector.  <a href="#ab12cedc7bee632a7b210312f1e7ef28b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#abb6f9d844071aaa8c8bc1ac62b1dba07">operator()</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#abb6f9d844071aaa8c8bc1ac62b1dba07"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a790a38cb578627bf17291088f2cd2388">Copy</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt; &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a vector.  <a href="#a790a38cb578627bf17291088f2cd2388"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a6e1000961b639af39a111147cf820d77">Copy</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a vector.  <a href="#a6e1000961b639af39a111147cf820d77"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ab3773a41209f76c5739be8f7f037006c">Append</a> (const T &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Appends an element to the vector.  <a href="#ab3773a41209f76c5739be8f7f037006c"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a6a64b54653758dbbdcd650c8ffba4d52">PushBack</a> (const T0 &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Appends an element at the end of the vector.  <a href="#a6a64b54653758dbbdcd650c8ffba4d52"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Allocator0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#aa0b88a0415692922c36c2b2f284efeca">PushBack</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt; &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Appends a vector X at the end of the vector.  <a href="#aa0b88a0415692922c36c2b2f284efeca"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a3906b8279cc318dc0a8a672fa781095a">GetDataSize</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored.  <a href="#a3906b8279cc318dc0a8a672fa781095a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ae8e4d6b081a6f4c90b2993b047a04dac">Zero</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets all elements to zero.  <a href="#ae8e4d6b081a6f4c90b2993b047a04dac"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a722046c309ee8e50abd68a5a5a77aa60"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Fill" ref="a722046c309ee8e50abd68a5a5a77aa60" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a722046c309ee8e50abd68a5a5a77aa60">Fill</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the vector with 0, 1, 2, ... <br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a2c103fe432a6f6a9c37cb133bb837153">Fill</a> (const T0 &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the vector with a given value.  <a href="#a2c103fe432a6f6a9c37cb133bb837153"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt; &amp;&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a236fd63d659f7d92f3b12da557380cb3">operator*=</a> (const T0 &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Multiplies a vector by a scalar.  <a href="#a236fd63d659f7d92f3b12da557380cb3"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a71147cebc8bd36cc103d2e6fd3767f91">FillRand</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the vector randomly.  <a href="#a71147cebc8bd36cc103d2e6fd3767f91"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">value_type&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a165cbeeddb4a2a356accc00d609b4211">GetNormInf</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the infinite norm.  <a href="#a165cbeeddb4a2a356accc00d609b4211"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a50186150708e90f86ded59e33cb172a6">GetNormInfIndex</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the index of the highest absolute value.  <a href="#a50186150708e90f86ded59e33cb172a6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a51ea9d8b0a3176fc903c0279ae8770d0">Write</a> (string FileName, bool with_size=true) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the vector in a file.  <a href="#a51ea9d8b0a3176fc903c0279ae8770d0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a1a098ef03b3d8cad79b659cd05be220e">Write</a> (ostream &amp;FileStream, bool with_size=true) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the vector in a file stream.  <a href="#a1a098ef03b3d8cad79b659cd05be220e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a2ce4a8d2726b069e465d4156b1b531be">Read</a> (string FileName, bool with_size=true)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the vector from a file.  <a href="#a2ce4a8d2726b069e465d4156b1b531be"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a634f8993354070451e53fd0c29bffa49">Read</a> (istream &amp;FileStream, bool with_size=true)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the vector from a file stream.  <a href="#a634f8993354070451e53fd0c29bffa49"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae">GetM</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements.  <a href="#a51f50515f0c6d0663465c2cc7de71bae"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a075daae3607a4767a77a3de60ab30ba7">GetLength</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements.  <a href="#a075daae3607a4767a77a3de60ab30ba7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a839880a3113c1885ead70d79fade0294">GetSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored.  <a href="#a839880a3113c1885ead70d79fade0294"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a">GetData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer to data_ (stored data).  <a href="#a4128ad4898e42211d22a7531c9f5f80a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a454aea78c82c4317dfe4160cc7c95afa">GetDataConst</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a const pointer to data_ (stored data).  <a href="#a454aea78c82c4317dfe4160cc7c95afa"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#ad0f5184bd9eec6fca70c54e459ced78f">GetDataVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "void*" to the data array (data_).  <a href="#ad0f5184bd9eec6fca70c54e459ced78f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a0aadc006977de037eb3ee550683e3f19">GetDataConstVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "const void*" to the data array (data_).  <a href="#a0aadc006977de037eb3ee550683e3f19"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac58514f2c122d7538dfc4db4ce0d4da9"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::m_" ref="ac58514f2c122d7538dfc4db4ce0d4da9" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>m_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1a6ae822c00f9ee2a798d54a36602430"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::data_" ref="a1a6ae822c00f9ee2a798d54a36602430" args="" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-static-attribs"></a>
Static Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afacfa831b36ad6fc284b48382de204a6"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::index_allocator_" ref="afacfa831b36ad6fc284b48382de204a6" args="" -->
static <br class="typebreak"/>
SELDON_DEFAULT_ALLOCATOR&lt; int &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>index_allocator_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6c958ad29a5f7eabd1f085e50046ab85"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::vect_allocator_" ref="a6c958ad29a5f7eabd1f085e50046ab85" args="" -->
static Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>vect_allocator_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Allocator&gt;<br/>
 class Seldon::Vector&lt; T, VectSparse, Allocator &gt;</h3>

<p>Sparse vector class. </p>

<p>Definition at line <a class="el" href="_sparse_vector_8hxx_source.php#l00029">29</a> of file <a class="el" href="_sparse_vector_8hxx_source.php">SparseVector.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="aee3f9a5bfd70e6370ae0c559257624f7"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Vector" ref="aee3f9a5bfd70e6370ae0c559257624f7" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_vector.php">Vector</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [explicit]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>On exit, the vector is empty. </p>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a3220e19bce6b4a0f72dfca147dbf8b38">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00039">39</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa33cfa4d38f149671d6d0ea82f5106d8"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Vector" ref="aa33cfa4d38f149671d6d0ea82f5106d8" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_vector.php">Vector</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [explicit]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Main constructor. </p>
<p>Builds a vector of a given size. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>length of the vector. </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#aaef02756d3c6c0a97454f35bffc07962">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00051">51</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a95b6b2c8c2be95a9d5dcdbd892f195e2"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Vector" ref="a95b6b2c8c2be95a9d5dcdbd892f195e2" args="(const Vector&lt; T, VectSparse, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_vector.php">Vector</a> </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>V</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Copy constructor. </p>
<p>Builds a copy of a vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>V</em>&nbsp;</td><td>vector to be copied. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00093">93</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="a34201f29286c8829e7e8a7e31a266d5b"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::AddInteraction" ref="a34201f29286c8829e7e8a7e31a266d5b" args="(int i, const T &amp;val)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::AddInteraction </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const T &amp;&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds <em>val</em> to the vector component #<em>i</em>. </p>
<p>If the vector has no entry at <em>i</em>, a new entry with value <em>val</em> is introduced. Otherwise, this method sums the existing value and <em>val</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index of the component. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>value to be added to the vector component <em>i</em>. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00676">676</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1a60608da32cc0da4be4a1acdbbebc06"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::AddInteractionRow" ref="a1a60608da32cc0da4be4a1acdbbebc06" args="(int, int *, T *, bool already_sorted=false)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::AddInteractionRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>n</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int *&nbsp;</td>
          <td class="paramname"> <em>index</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">T *&nbsp;</td>
          <td class="paramname"> <em>value</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>already_sorted</em> = <code>false</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds given values to several components of the vector. </p>
<p>This method sorts the values to be added (according to their indices) and adds them with the vector values. For every component, if the vector has no entry, a new entry is introduced. Otherwise, the method sums the existing value and the corresponsing value in <em>value</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>n</em>&nbsp;</td><td>number of values to be added. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>index</em>&nbsp;</td><td>indices of the values to be added. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>value</em>&nbsp;</td><td>values to be added. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>already_sorted</em>&nbsp;</td><td>true if the indices are already sorted. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00728">728</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a4625cf1134f2121385cbee020c7fe834"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::AddInteractionRow" ref="a4625cf1134f2121385cbee020c7fe834" args="(int nb, Vector&lt; int &gt; col, Vector&lt; T, VectFull, Allocator0 &gt; val, bool already_sorted=false)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Allocator0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::AddInteractionRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>n</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt;&nbsp;</td>
          <td class="paramname"> <em>index</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt;&nbsp;</td>
          <td class="paramname"> <em>value</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>already_sorted</em> = <code>false</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds given values to several components of the vector. </p>
<p>This method sorts the values to be added (according to their indices) and adds them with the vector values. For every component, if the vector has no entry, a new entry is introduced. Otherwise, the method sums the existing value and the corresponsing value in <em>value</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>n</em>&nbsp;</td><td>number of values to be added. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>index</em>&nbsp;</td><td>indices of the values to be added. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>value</em>&nbsp;</td><td>values to be added. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>already_sorted</em>&nbsp;</td><td>true if the indices are already sorted. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00753">753</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab3773a41209f76c5739be8f7f037006c"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Append" ref="ab3773a41209f76c5739be8f7f037006c" args="(const T &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Append </td>
          <td>(</td>
          <td class="paramtype">const T &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline, inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Appends an element to the vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>element to be appended. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>This method will only work if the allocator preserves the elements while reallocating. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00625">625</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3f4b6ec8424c938251af5e4b1b130702"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Assemble" ref="a3f4b6ec8424c938251af5e4b1b130702" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::Assemble </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Assembles the vector. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>If you use the method AddInteraction, you don't need to call that method. </dd></dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00625">625</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a548fa99edadcd9b418912d607b7aa19b"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Clear" ref="a548fa99edadcd9b418912d607b7aa19b" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::Clear </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the vector. </p>
<p>Destructs the vector. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>On exit, the vector is an empty vector. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a5120b4ecff05d8cf54bbd57fb280fbe1">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00154">154</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a790a38cb578627bf17291088f2cd2388"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Copy" ref="a790a38cb578627bf17291088f2cd2388" args="(const Vector&lt; T, VectFull, Allocator &gt; &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Copy </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline, inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>X</em>&nbsp;</td><td>vector to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'X' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00581">581</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6e1000961b639af39a111147cf820d77"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Copy" ref="a6e1000961b639af39a111147cf820d77" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Copy </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline, inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a vector. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A copy of the vector. </dd></dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: the returned vector is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00597">597</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aefe82d8a7c33e932c20c9fd9e6f499f5"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Copy" ref="aefe82d8a7c33e932c20c9fd9e6f499f5" args="(const Vector&lt; T, VectSparse, Allocator &gt; &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::Copy </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>X</em>&nbsp;</td><td>vector to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: <em>X</em> is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00564">564</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2c103fe432a6f6a9c37cb133bb837153"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Fill" ref="a2c103fe432a6f6a9c37cb133bb837153" args="(const T0 &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Fill </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the vector with a given value. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>value to fill the vector with. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00709">709</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a71147cebc8bd36cc103d2e6fd3767f91"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::FillRand" ref="a71147cebc8bd36cc103d2e6fd3767f91" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::FillRand </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the vector randomly. </p>
<dl class="note"><dt><b>Note:</b></dt><dd>The random generator is very basic. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00736">736</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a61bcf710b6d43a3690561abd27d4b145"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Get" ref="a61bcf710b6d43a3690561abd27d4b145" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::reference <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::Get </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the value of element <em>i</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element <em>i</em> of the vector. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a85cd9b850be2276e853e81ba567a18e6">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00448">448</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad57ac9ab256f25c6f9f244fa6e7b2ccf"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Get" ref="ad57ac9ab256f25c6f9f244fa6e7b2ccf" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::const_reference <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::Get </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the value of element <em>i</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element <em>i</em> of the vector. </dd></dl>
<dl><dt><b>Exceptions:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em><a class="el" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a></em>&nbsp;</td><td>No reference can be returned because the element is a zero entry (not stored in the vector). </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a92b34068ad39cbfec8918d77d2385bee">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00472">472</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a4128ad4898e42211d22a7531c9f5f80a"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::GetData" ref="a4128ad4898e42211d22a7531c9f5f80a" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector___base.php">Vector_Base</a>&lt; T, Allocator &gt;::pointer <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer to data_ (stored data). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer to the data_, i.e. the data array. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00156">156</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a454aea78c82c4317dfe4160cc7c95afa"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::GetDataConst" ref="a454aea78c82c4317dfe4160cc7c95afa" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector___base.php">Vector_Base</a>&lt; T, Allocator &gt;::const_pointer <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetDataConst </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a const pointer to data_ (stored data). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer to the data_, i.e. the data array. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00168">168</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0aadc006977de037eb3ee550683e3f19"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::GetDataConstVoid" ref="a0aadc006977de037eb3ee550683e3f19" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const void * <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetDataConstVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "const void*" to the data array (data_). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer of type "const void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00191">191</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3906b8279cc318dc0a8a672fa781095a"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::GetDataSize" ref="a3906b8279cc318dc0a8a672fa781095a" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::GetDataSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements stored in memory. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00670">670</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad0f5184bd9eec6fca70c54e459ced78f"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::GetDataVoid" ref="ad0f5184bd9eec6fca70c54e459ced78f" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void * <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetDataVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "void*" to the data array (data_). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00180">180</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6a4ed85be713e5a300f94eca786a2c2b"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::GetIndex" ref="a6a4ed85be713e5a300f94eca786a2c2b" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int * <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::GetIndex </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer to the array containing the indices of the non-zero entries. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer to the array of the indices of the non-zero entries. </dd></dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00584">584</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a075daae3607a4767a77a3de60ab30ba7"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::GetLength" ref="a075daae3607a4767a77a3de60ab30ba7" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetLength </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the vector. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ae501c34e51f6559568846b94e24c249c">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>, and <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a2a2b8d5ad39c79bdcdcd75fa20ae5ae8">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00133">133</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a51f50515f0c6d0663465c2cc7de71bae"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::GetM" ref="a51f50515f0c6d0663465c2cc7de71bae" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the vector. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a6f40d71a69e016ae3e1d9db92b6538a4">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>, and <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a9b0a8be2fb500d4f6c2edb4c1a5247ef">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00122">122</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a165cbeeddb4a2a356accc00d609b4211"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::GetNormInf" ref="a165cbeeddb4a2a356accc00d609b4211" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::value_type <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::GetNormInf </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the infinite norm. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The infinite norm. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00765">765</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a50186150708e90f86ded59e33cb172a6"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::GetNormInfIndex" ref="a50186150708e90f86ded59e33cb172a6" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::GetNormInfIndex </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the index of the highest absolute value. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The index of the element that has the highest absolute value. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00783">783</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a839880a3113c1885ead70d79fade0294"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::GetSize" ref="a839880a3113c1885ead70d79fade0294" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the vector stored. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00144">144</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a72d964fb6cbd4e4189473e0298989929"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Index" ref="a72d964fb6cbd4e4189473e0298989929" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::Index </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The index of the non-zero element #<em>i</em>. </dd></dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00385">385</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3f0337de5aaabd69cb32432377b5e4d3"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Index" ref="a3f0337de5aaabd69cb32432377b5e4d3" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::Index </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The row number of the non-zero element #<em>i</em>. </dd></dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00405">405</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a985afdfd20e655bb551cf44669acd0e6"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Nullify" ref="a985afdfd20e655bb551cf44669acd0e6" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::Nullify </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the vector without releasing memory. </p>
<p>On exit, the vector is empty and the memory has not been released. It is useful for low level manipulations on a <a class="el" href="class_seldon_1_1_vector.php">Vector</a> instance. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Memory is not released. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a151dc47113d07ab24775733d0c46dc90">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00324">324</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="abb6f9d844071aaa8c8bc1ac62b1dba07"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::operator()" ref="abb6f9d844071aaa8c8bc1ac62b1dba07" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::reference <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline, inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The value of the vector at 'i'. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00480">480</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a001a7d1629309ad90c6d66d695a69e8f"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::operator()" ref="a001a7d1629309ad90c6d66d695a69e8f" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::value_type <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The value of the vector at <em>i</em>. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#af6ee550855ccf2a4db736bad2352929f">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00426">426</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a236fd63d659f7d92f3b12da557380cb3"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::operator*=" ref="a236fd63d659f7d92f3b12da557380cb3" args="(const T0 &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::operator*= </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>alpha</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline, inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Multiplies a vector by a scalar. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>alpha</em>&nbsp;</td><td>scalar. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00609">609</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a87ef82c6607c68b77a5ec18dbcb15eb0"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::operator=" ref="a87ef82c6607c68b77a5ec18dbcb15eb0" args="(const Vector&lt; T, VectSparse, Allocator &gt; &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a vector (assignment operator). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>X</em>&nbsp;</td><td>vector to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: <em>X</em> is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00548">548</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1d0196721350369a3154b60dfa105b37"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::operator=" ref="a1d0196721350369a3154b60dfa105b37" args="(const T0 &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the vector with a given value. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>value to fill the vector with. </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a0bf814aab7883056808671f5b6866ba6">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00602">602</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa0b88a0415692922c36c2b2f284efeca"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::PushBack" ref="aa0b88a0415692922c36c2b2f284efeca" args="(const Vector&lt; T, VectFull, Allocator0 &gt; &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Allocator0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::PushBack </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline, inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Appends a vector X at the end of the vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>X</em>&nbsp;</td><td>vector to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00651">651</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6a64b54653758dbbdcd650c8ffba4d52"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::PushBack" ref="a6a64b54653758dbbdcd650c8ffba4d52" args="(const T0 &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::PushBack </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline, inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Appends an element at the end of the vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>element to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00638">638</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a4335b7afce7863794f8ffbf76db91cbd"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Read" ref="a4335b7afce7863794f8ffbf76db91cbd" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>stream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the vector from a file stream, in binary format. </p>
<p>Sets the vector according to a binary stream that stores the data like method Write(ostream&amp;). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>stream</em>&nbsp;</td><td>stream from which to read the vector values. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00979">979</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2ce4a8d2726b069e465d4156b1b531be"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Read" ref="a2ce4a8d2726b069e465d4156b1b531be" args="(string FileName, bool with_size=true)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the vector from a file. </p>
<p>Sets the vector according to a binary file that stores the length of the vector (integer) and all elements. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>file name. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the length of the vector is not available in the file. In this case, the current size N of the vector is unchanged, and N elements are read in the file. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l01033">1033</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="adc80b24a206e7ef91dd9b51aa33763d0"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Read" ref="adc80b24a206e7ef91dd9b51aa33763d0" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the vector from a file in binary format. </p>
<p>Sets the vector according to a binary file that stores the data like method Write(string). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00955">955</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a634f8993354070451e53fd0c29bffa49"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Read" ref="a634f8993354070451e53fd0c29bffa49" args="(istream &amp;FileStream, bool with_size=true)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the vector from a file stream. </p>
<p>Sets the vector according to a binary file stream that stores the length of the vector (integer) and all elements. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>file stream. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the length of the vector is not available in the stream. In this case, the current size N of the vector is unchanged, and N elements are read in the stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l01062">1062</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3b9e7d2529315dfaa75a09d3c34da18a"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::ReadText" ref="a3b9e7d2529315dfaa75a09d3c34da18a" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the vector from a text file. </p>
<p>Sets the vector according to a text file that stores the data like method WriteText(string). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#af1a8f91e66d729c91fddb0a0c9e030b8">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l01014">1014</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a554c1d3659fc50f3e119ef14437ad54a"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::ReadText" ref="a554c1d3659fc50f3e119ef14437ad54a" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>stream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the vector from a file stream, in text format. </p>
<p>Sets the vector according to a stream, in text format, that stores the data like method WriteText(ostream&amp;). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>stream</em>&nbsp;</td><td>stream from which to read the vector values. </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ae0f8292f742367edca616ab9e2e93e6b">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l01038">1038</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="afcc8fef4210e7703493ad95acf389d19"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Reallocate" ref="afcc8fef4210e7703493ad95acf389d19" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::Reallocate </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p><a class="el" href="class_seldon_1_1_vector.php">Vector</a> reallocation. </p>
<p>The vector is resized. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>new length of the vector. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Depending on your allocator, previous non-zero entries may be lost. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a6c1667ecf1265c0870ba2828b33a2d79">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00168">168</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6ff2b64975624f059a305a7661c24573"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::RemoveSmallEntry" ref="a6ff2b64975624f059a305a7661c24573" args="(const T0 &amp;epsilon)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::RemoveSmallEntry </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>epsilon</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Removes small entries. </p>
<p>Any number whose absolute value is below (or equal) to <em>epsilon</em> is removed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>epsilon</em>&nbsp;</td><td>the threshold value. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00649">649</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3637911015d5a0fafa4811ee2204bbc5"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Resize" ref="a3637911015d5a0fafa4811ee2204bbc5" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::Resize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>n</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Changes the number of non-zero entries of the vector. </p>
<p>Changes the number of non-zero entries to <em>n</em>. If <em>n</em> non-zero entries are available before resizing, they are all kept. Otherwise, only the first <br/>
 non-zero entries are kept. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>n</em>&nbsp;</td><td>new number of non-zero entries of the vector. </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a99dda1b202ce562645f890897c593515">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00217">217</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af6c784522606f8bf451b129a8b5069d1"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::SetData" ref="af6c784522606f8bf451b129a8b5069d1" args="(Vector&lt; T, VectFull, Allocator2 &gt; &amp;data, Vector&lt; int &gt; &amp;index)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::SetData </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator2 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>data</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>index</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Changes the length of the vector and sets its data array (low level method). </p>
<p>Reallocates a vector and sets the new data array. It is useful to create a vector from pre-existing data. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>data</em>&nbsp;</td><td>the new data array. <em>data</em> contains the new elements of the vector and must therefore contain <em>i</em> elements. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>index</em>&nbsp;</td><td>the new index array. <em>index</em> contains the new indices of the non-zero entries and it must therefore contain <em>i</em> elements. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Vectors <em>data</em> and <em>index</em> are empty vector on exit. </dd></dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00279">279</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab12cedc7bee632a7b210312f1e7ef28b"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::SetData" ref="ab12cedc7bee632a7b210312f1e7ef28b" args="(const Vector&lt; T, VectFull, Allocator0 &gt; &amp;V)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Allocator0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::SetData </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>V</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline, inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Lets the current vector point to the data of another vector. </p>
<p>Deallocates memory allocated for the current data array. Then sets the length and the data of the current vector to that of <em>V</em>. On exit, the current vector shares it data array with <em>V</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>V</em>&nbsp;</td><td>vector whose data should be shared with current instance. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>On exit, <em>V</em> and the current instance share the same data array, and they are likely to free it. As a consequence, before the vectors are destructed, it is necessary to call 'Nullify' on either <em>V</em> or the current instance. In addition, if the current instance is to deallocate the data array, its allocator should be compatible with the allocator that allocated the data array (probably the allocator of <em>V</em>). </dd></dl>
<dl class="note"><dt><b>Note:</b></dt><dd>This method should only be used by advanced users. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00448">448</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a73034fa2fb67eec70a2d25684f2a0c8c"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::SetData" ref="a73034fa2fb67eec70a2d25684f2a0c8c" args="(int nz, T *data, int *index)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::SetData </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">T *&nbsp;</td>
          <td class="paramname"> <em>data</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int *&nbsp;</td>
          <td class="paramname"> <em>index</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Changes the length of the vector and sets its data array (low level method). </p>
<p>Reallocates a vector and sets the new data array. It is useful to create a vector from pre-existing data. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>new length of the vector. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>data</em>&nbsp;</td><td>the new data array. <em>data</em> contains the new elements of the vector and must therefore contain <em>i</em> elements. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>index</em>&nbsp;</td><td>the new index array. <em>index</em> contains the new indices of the non-zero entries and it must therefore contain <em>i</em> elements. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd><em>data</em> has to be used carefully outside the object. Unless you use 'Nullify', <em>data</em> will be freed by the destructor, which means that <em>data</em> must have been allocated carefully. The vector allocator should be compatible. </dd></dl>
<dl class="note"><dt><b>Note:</b></dt><dd>This method should only be used by advanced users. </dd></dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00254">254</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab4c128e288cce25e0e8f1998ccf38a1c"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::SetData" ref="ab4c128e288cce25e0e8f1998ccf38a1c" args="(const Vector&lt; T, VectSparse, Allocator2 &gt; &amp;V)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::SetData </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator2 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>V</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Lets the current vector point to the data of a second vector (low level method). </p>
<p>Reallocates the current vector and lets its data point to those of <em>V</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>V</em>&nbsp;</td><td>the vector to which the current vector points to (on exit). </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>On exit, both <em>V</em> and the current vector point to the same arrays in memory. Only one of them should eventually deallocate the memory blocks. The other one should be nullified by the user. In case the current vector is responsible for the deallocations, its allocator should be compatible with the allocator that created the memory blocks (which is probably the allocator of <em>V</em>). </dd></dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00311">311</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5805ff818cc9c57a65bc6006c58564a7"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Val" ref="a5805ff818cc9c57a65bc6006c58564a7" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::reference <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::Val </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the value of element <em>i</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element <em>i</em> of the vector. </dd></dl>
<dl><dt><b>Exceptions:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em><a class="el" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a></em>&nbsp;</td><td>No reference can be returned because the element is a zero entry (not stored in the vector). </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00498">498</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a21f192e6ea09ce5be473e6bb1135c7a9"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Val" ref="a21f192e6ea09ce5be473e6bb1135c7a9" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::const_reference <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::Val </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the value of element <em>i</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element <em>i</em> of the vector. </dd></dl>
<dl><dt><b>Exceptions:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em><a class="el" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a></em>&nbsp;</td><td>No reference can be returned because the element is a zero entry (not stored in the vector). </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00524">524</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a51c6d9be9bfc1234e17d997f9bcdff07"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Value" ref="a51c6d9be9bfc1234e17d997f9bcdff07" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::reference <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::Value </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The value of the non-zero element #<em>i</em>. </dd></dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00344">344</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad733400e0cde2e94b5efd076dc5d956e"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Value" ref="ad733400e0cde2e94b5efd076dc5d956e" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::const_reference <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::Value </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The value of the non-zero element #<em>i</em>. </dd></dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00365">365</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a51ea9d8b0a3176fc903c0279ae8770d0"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Write" ref="a51ea9d8b0a3176fc903c0279ae8770d0" args="(string FileName, bool with_size=true) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the vector in a file. </p>
<p>The length of the vector (integer) and all elements of the vector are stored in binary format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>file name. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the length of the vector is not saved. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00820">820</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a246553d78195c2e1c51be7a4e84409c8"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Write" ref="a246553d78195c2e1c51be7a4e84409c8" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the vector in a file. </p>
<p>It stores in binary format: (1) the number of non-zero entries in the vector (integer), (2) the indices of the non-zero entries (integers), and (3) the non-zero values of the vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00832">832</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a27f19795d0cf6489ab076a6c43ef6756"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Write" ref="a27f19795d0cf6489ab076a6c43ef6756" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>stream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the vector in a stream, in binary format. </p>
<p>It writes in binary format: (1) the number of non-zero entries in the vector (integer), (2) the indices of the non-zero entries (integers), and (3) the non-zero values of the vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>stream</em>&nbsp;</td><td>stream in which the vector is to be written. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00857">857</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1a098ef03b3d8cad79b659cd05be220e"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Write" ref="a1a098ef03b3d8cad79b659cd05be220e" args="(ostream &amp;FileStream, bool with_size=true) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the vector in a file stream. </p>
<p>The length of the vector (integer) and all elements of the vector are stored in binary format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>file stream. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the length of the vector is not saved. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00848">848</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab770c6961b3c24de5679fc93530e6e80"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::WriteText" ref="ab770c6961b3c24de5679fc93530e6e80" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the vector in a text file. </p>
<p>All non-zero elements of the vector are stored in text format: every line of the text file contains one index and one value. The length is not stored. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a39fd5c75b22c5c2d37a47587f57abad2">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00893">893</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a71b2e3e54ed55920f6b34912382fac80"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::WriteText" ref="a71b2e3e54ed55920f6b34912382fac80" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>stream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the vector in a stream, in text format. </p>
<p>All non-zero elements of the vector are stored in text format: every line of the text file contains one index and one value. The length is not stored. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>stream</em>&nbsp;</td><td>stream in which the vector is to be written. </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ab5bb2e8cf1a7ceb1ac941a88ed1635e6">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_sparse_vector_8cxx_source.php#l00920">920</a> of file <a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae8e4d6b081a6f4c90b2993b047a04dac"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectSparse, Allocator &gt;::Zero" ref="ae8e4d6b081a6f4c90b2993b047a04dac" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Zero </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets all elements to zero. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>It fills the memory with zeros. If the vector stores complex structures, use 'Fill' instead. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00687">687</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>vector/<a class="el" href="_sparse_vector_8hxx_source.php">SparseVector.hxx</a></li>
<li>vector/<a class="el" href="_sparse_vector_8cxx_source.php">SparseVector.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
