<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><b>seldon</b>      </li>
      <li><a class="el" href="classseldon_1_1_vector_double.php">VectorDouble</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pub-attribs">Public Attributes</a>  </div>
  <div class="headertitle">
<h1>seldon::VectorDouble Class Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="seldon::VectorDouble" --><!-- doxytag: inherits="seldon::BaseSeldonVectorDouble" --><div class="dynheader">
Inheritance diagram for seldon::VectorDouble:</div>
<div class="dyncontent">
 <div class="center">
  <img src="classseldon_1_1_vector_double.png" usemap="#seldon::VectorDouble_map" alt=""/>
  <map id="seldon::VectorDouble_map" name="seldon::VectorDouble_map">
<area href="classseldon_1_1_base_seldon_vector_double.php" alt="seldon::BaseSeldonVectorDouble" shape="rect" coords="0,56,200,80"/>
<area href="classseldon_1_1__object.php" alt="seldon::_object" shape="rect" coords="0,0,200,24"/>
<area href="classseldon_1_1_vector_sparse_double.php" alt="seldon::VectorSparseDouble" shape="rect" coords="0,168,200,192"/>
</map>
</div>

<p><a href="classseldon_1_1_vector_double-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a256cae90dfee26126df3a0605625e22b"></a><!-- doxytag: member="seldon::VectorDouble::__init__" ref="a256cae90dfee26126df3a0605625e22b" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__init__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac886cc570a14a369d3570e45c72d8345"></a><!-- doxytag: member="seldon::VectorDouble::Clear" ref="ac886cc570a14a369d3570e45c72d8345" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Clear</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aee6d0533dc03211dd1b1d5790e44c0fa"></a><!-- doxytag: member="seldon::VectorDouble::Reallocate" ref="aee6d0533dc03211dd1b1d5790e44c0fa" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Reallocate</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad7302cc5ea443f87d6d1718f4a8b9604"></a><!-- doxytag: member="seldon::VectorDouble::Resize" ref="ad7302cc5ea443f87d6d1718f4a8b9604" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Resize</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="abc5545c213a85c8934e4c4acc5be1b36"></a><!-- doxytag: member="seldon::VectorDouble::SetData" ref="abc5545c213a85c8934e4c4acc5be1b36" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetData</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0447e34857a095bf6575968617620fcc"></a><!-- doxytag: member="seldon::VectorDouble::Nullify" ref="a0447e34857a095bf6575968617620fcc" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Nullify</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a137974ce71035815ca7781bf425a03fd"></a><!-- doxytag: member="seldon::VectorDouble::__call__" ref="a137974ce71035815ca7781bf425a03fd" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__call__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa4f79144088398a5e49de7f9e060fa66"></a><!-- doxytag: member="seldon::VectorDouble::Get" ref="aa4f79144088398a5e49de7f9e060fa66" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Get</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3dbee10e049bf54d96f68c1472af5b0a"></a><!-- doxytag: member="seldon::VectorDouble::Copy" ref="a3dbee10e049bf54d96f68c1472af5b0a" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Copy</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a32e31a7de41681a85e13b50c4d9de6cb"></a><!-- doxytag: member="seldon::VectorDouble::Append" ref="a32e31a7de41681a85e13b50c4d9de6cb" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Append</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0f17928fe79cae91acfeee46f042c2dd"></a><!-- doxytag: member="seldon::VectorDouble::GetDataSize" ref="a0f17928fe79cae91acfeee46f042c2dd" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataSize</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a203ab21708b9bfbb49f30703d417b215"></a><!-- doxytag: member="seldon::VectorDouble::Zero" ref="a203ab21708b9bfbb49f30703d417b215" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Zero</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a140ce493c67f1b271d5ba38ff502226f"></a><!-- doxytag: member="seldon::VectorDouble::Fill" ref="a140ce493c67f1b271d5ba38ff502226f" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Fill</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5227023ccfed19b75301f2e9253bcd98"></a><!-- doxytag: member="seldon::VectorDouble::FillRand" ref="a5227023ccfed19b75301f2e9253bcd98" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>FillRand</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a366f466243c4feff116b0d0b26ef2bdf"></a><!-- doxytag: member="seldon::VectorDouble::Print" ref="a366f466243c4feff116b0d0b26ef2bdf" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Print</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a72f57b61e0221308984e6ac9b24c3da8"></a><!-- doxytag: member="seldon::VectorDouble::GetNormInf" ref="a72f57b61e0221308984e6ac9b24c3da8" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetNormInf</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a594abbd187fcf8ea1a82bf3bbce1af16"></a><!-- doxytag: member="seldon::VectorDouble::GetNormInfIndex" ref="a594abbd187fcf8ea1a82bf3bbce1af16" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetNormInfIndex</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6344aefeee855942da3acf9cbcf70e6c"></a><!-- doxytag: member="seldon::VectorDouble::Write" ref="a6344aefeee855942da3acf9cbcf70e6c" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Write</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af0792cd5af098df34488d08716395154"></a><!-- doxytag: member="seldon::VectorDouble::WriteText" ref="af0792cd5af098df34488d08716395154" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>WriteText</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3c06296eefd1c3e73399ed8ab3390432"></a><!-- doxytag: member="seldon::VectorDouble::Read" ref="a3c06296eefd1c3e73399ed8ab3390432" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>Read</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a38a7f59e87fafadaddba9a6a4273518b"></a><!-- doxytag: member="seldon::VectorDouble::ReadText" ref="a38a7f59e87fafadaddba9a6a4273518b" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>ReadText</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a68f5fca0af7245aa6f26344fcb91fdcf"></a><!-- doxytag: member="seldon::VectorDouble::__getitem__" ref="a68f5fca0af7245aa6f26344fcb91fdcf" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__getitem__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac8f75c246106bb3689dc435b72fea362"></a><!-- doxytag: member="seldon::VectorDouble::__setitem__" ref="ac8f75c246106bb3689dc435b72fea362" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__setitem__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a616987bf166ded6576f2476c62629f3f"></a><!-- doxytag: member="seldon::VectorDouble::__len__" ref="a616987bf166ded6576f2476c62629f3f" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>__len__</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a858bdaf7d2a9c1b91ec5830b99364e51"></a><!-- doxytag: member="seldon::VectorDouble::GetM" ref="a858bdaf7d2a9c1b91ec5830b99364e51" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetM</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab5e0d80b8f82764000d63125990b027f"></a><!-- doxytag: member="seldon::VectorDouble::GetLength" ref="ab5e0d80b8f82764000d63125990b027f" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetLength</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8638f6c59df27427f8d7378ec80a5e1f"></a><!-- doxytag: member="seldon::VectorDouble::GetSize" ref="a8638f6c59df27427f8d7378ec80a5e1f" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetSize</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aca868df1f3bd30778c420b37daca30b3"></a><!-- doxytag: member="seldon::VectorDouble::GetData" ref="aca868df1f3bd30778c420b37daca30b3" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetData</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a960400e2fba31c811b343dfceefa603d"></a><!-- doxytag: member="seldon::VectorDouble::GetDataConst" ref="a960400e2fba31c811b343dfceefa603d" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataConst</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae42bacee4cebd8bc56c595c14d3cd2c0"></a><!-- doxytag: member="seldon::VectorDouble::GetDataVoid" ref="ae42bacee4cebd8bc56c595c14d3cd2c0" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataVoid</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="abae4106cc36f94ad3d9f218f76d937b3"></a><!-- doxytag: member="seldon::VectorDouble::GetDataConstVoid" ref="abae4106cc36f94ad3d9f218f76d937b3" args="" -->
def&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataConstVoid</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-attribs"></a>
Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a15472a917b372a164f16c4931b44afef"></a><!-- doxytag: member="seldon::VectorDouble::this" ref="a15472a917b372a164f16c4931b44afef" args="" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><b>this</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>

<p>Definition at line <a class="el" href="seldon_8py_source.php#l00909">909</a> of file <a class="el" href="seldon_8py_source.php">seldon.py</a>.</p>
<hr/>The documentation for this class was generated from the following file:<ul>
<li><a class="el" href="seldon_8py_source.php">seldon.py</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
