<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix_sparse/Matrix_SymComplexSparse.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="comment">// To be included by Seldon.hxx</span>
<a name="l00021"></a>00021 
<a name="l00022"></a>00022 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_SYMCOMPLEXSPARSE_HXX</span>
<a name="l00023"></a>00023 <span class="preprocessor"></span>
<a name="l00024"></a>00024 <span class="preprocessor">#include &quot;../share/Common.hxx&quot;</span>
<a name="l00025"></a>00025 <span class="preprocessor">#include &quot;../share/Properties.hxx&quot;</span>
<a name="l00026"></a>00026 <span class="preprocessor">#include &quot;../share/Storage.hxx&quot;</span>
<a name="l00027"></a>00027 <span class="preprocessor">#include &quot;../share/Errors.hxx&quot;</span>
<a name="l00028"></a>00028 <span class="preprocessor">#include &quot;../share/Allocator.hxx&quot;</span>
<a name="l00029"></a>00029 
<a name="l00030"></a>00030 <span class="keyword">namespace </span>Seldon
<a name="l00031"></a>00031 {
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00035"></a>00035 
<a name="l00047"></a>00047   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Storage,
<a name="l00048"></a>00048             <span class="keyword">class </span>Allocator = SELDON_DEFAULT_ALLOCATOR&lt;T&gt; &gt;
<a name="l00049"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php">00049</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php" title="Symmetric complex sparse-matrix class.">Matrix_SymComplexSparse</a>: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;
<a name="l00050"></a>00050   {
<a name="l00051"></a>00051     <span class="comment">// typedef declaration.</span>
<a name="l00052"></a>00052   <span class="keyword">public</span>:
<a name="l00053"></a>00053     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00054"></a>00054     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::pointer pointer;
<a name="l00055"></a>00055     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_pointer const_pointer;
<a name="l00056"></a>00056     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::reference reference;
<a name="l00057"></a>00057     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_reference const_reference;
<a name="l00058"></a>00058     <span class="keyword">typedef</span> complex&lt;value_type&gt; entry_type;
<a name="l00059"></a>00059     <span class="keyword">typedef</span> complex&lt;value_type&gt; access_type;
<a name="l00060"></a>00060     <span class="keyword">typedef</span> complex&lt;value_type&gt; const_access_type;
<a name="l00061"></a>00061 
<a name="l00062"></a>00062     <span class="comment">// Attributes.</span>
<a name="l00063"></a>00063   <span class="keyword">protected</span>:
<a name="l00064"></a>00064     <span class="comment">// Number of non-zero (stored) elements.</span>
<a name="l00065"></a>00065     <span class="keywordtype">int</span> real_nz_;
<a name="l00066"></a>00066     <span class="keywordtype">int</span> imag_nz_;
<a name="l00067"></a>00067     <span class="comment">// Index (in data_) of first element stored for each row or column.</span>
<a name="l00068"></a>00068     <span class="keywordtype">int</span>* real_ptr_;
<a name="l00069"></a>00069     <span class="keywordtype">int</span>* imag_ptr_;
<a name="l00070"></a>00070     <span class="comment">// Column or row index (in the matrix) each element.</span>
<a name="l00071"></a>00071     <span class="keywordtype">int</span>* real_ind_;
<a name="l00072"></a>00072     <span class="keywordtype">int</span>* imag_ind_;
<a name="l00073"></a>00073 
<a name="l00074"></a>00074     <span class="comment">// Data.</span>
<a name="l00075"></a>00075     T* real_data_;
<a name="l00076"></a>00076     T* imag_data_;
<a name="l00077"></a>00077 
<a name="l00078"></a>00078     <span class="comment">// Methods.</span>
<a name="l00079"></a>00079   <span class="keyword">public</span>:
<a name="l00080"></a>00080     <span class="comment">// Constructors.</span>
<a name="l00081"></a>00081     <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af4fd673569b7398d84f2d4f277de50a8" title="Default constructor.">Matrix_SymComplexSparse</a>();
<a name="l00082"></a>00082     <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af4fd673569b7398d84f2d4f277de50a8" title="Default constructor.">Matrix_SymComplexSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00083"></a>00083     <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af4fd673569b7398d84f2d4f277de50a8" title="Default constructor.">Matrix_SymComplexSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> real_nz, <span class="keywordtype">int</span> imag_nz);
<a name="l00084"></a>00084     <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00085"></a>00085               <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00086"></a>00086               <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00087"></a>00087     <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af4fd673569b7398d84f2d4f277de50a8" title="Default constructor.">Matrix_SymComplexSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00088"></a>00088                             <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; real_values,
<a name="l00089"></a>00089                             <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; real_ptr,
<a name="l00090"></a>00090                             <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; real_ind,
<a name="l00091"></a>00091                             <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; imag_values,
<a name="l00092"></a>00092                             <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; imag_ptr,
<a name="l00093"></a>00093                             <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; imag_ind);
<a name="l00094"></a>00094     <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af4fd673569b7398d84f2d4f277de50a8" title="Default constructor.">Matrix_SymComplexSparse</a>(<span class="keyword">const</span> Matrix_SymComplexSparse&lt;T, Prop, Storage,
<a name="l00095"></a>00095                             Allocator&gt;&amp; A);
<a name="l00096"></a>00096 
<a name="l00097"></a>00097     <span class="comment">// Destructor.</span>
<a name="l00098"></a>00098     <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#afcf6a5d81a59ad003dfa7f905e0b5aaa" title="Destructor.">~Matrix_SymComplexSparse</a>();
<a name="l00099"></a>00099     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a77439fdc40537903681e7cd912c5f732" title="Clears the matrix.">Clear</a>();
<a name="l00100"></a>00100 
<a name="l00101"></a>00101     <span class="comment">// Memory management.</span>
<a name="l00102"></a>00102     <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00103"></a>00103               <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00104"></a>00104               <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00105"></a>00105     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a3f202fe96b9289912e1cb6acfe24f18b" title="Redefines the matrix.">SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00106"></a>00106                  <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; real_values,
<a name="l00107"></a>00107                  <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; real_ptr,
<a name="l00108"></a>00108                  <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; real_ind,
<a name="l00109"></a>00109                  <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; imag_values,
<a name="l00110"></a>00110                  <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; imag_ptr,
<a name="l00111"></a>00111                  <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; imag_ind);
<a name="l00112"></a>00112     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a3f202fe96b9289912e1cb6acfe24f18b" title="Redefines the matrix.">SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00113"></a>00113                  <span class="keywordtype">int</span> real_nz, pointer real_values, <span class="keywordtype">int</span>* real_ptr,
<a name="l00114"></a>00114                  <span class="keywordtype">int</span>* real_ind,
<a name="l00115"></a>00115                  <span class="keywordtype">int</span> imag_nz, pointer imag_values, <span class="keywordtype">int</span>* imag_ptr,
<a name="l00116"></a>00116                  <span class="keywordtype">int</span>* imag_ind);
<a name="l00117"></a>00117     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af9adb37bee7561a855fef31642b85bfd" title="Clears the matrix without releasing memory.">Nullify</a>();
<a name="l00118"></a>00118     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af6ee0fe9e269657f40a8dccbe5e30b73" title="Initialization of an empty matrix i x j.">Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00119"></a>00119     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af6ee0fe9e269657f40a8dccbe5e30b73" title="Initialization of an empty matrix i x j.">Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> real_nz, <span class="keywordtype">int</span> imag_nz);
<a name="l00120"></a>00120     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a6537ad73d443b7b1e7008f626593811e" title="Reallocates memory to resize the matrix and keeps previous entries.">Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00121"></a>00121     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a6537ad73d443b7b1e7008f626593811e" title="Reallocates memory to resize the matrix and keeps previous entries.">Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> real_nz, <span class="keywordtype">int</span> imag_nz);
<a name="l00122"></a>00122     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a5e1062c93e237401d6bf6b1797861c62" title="Copies a matrix.">Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php" title="Symmetric complex sparse-matrix class.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A);
<a name="l00123"></a>00123 
<a name="l00124"></a>00124     <span class="comment">// Basic methods.</span>
<a name="l00125"></a>00125     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#afda1dbd968e2e4b3047c014ab5931846" title="Returns the number of elements stored in memory.">GetDataSize</a>() <span class="keyword">const</span>;
<a name="l00126"></a>00126     <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a005a3b7389aecf38482cc16e4984787c" title="Returns (row or column) start indices for the real part.">GetRealPtr</a>() <span class="keyword">const</span>;
<a name="l00127"></a>00127     <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad1bb43e80676e9ee420ea692ba06d0ac" title="Returns (row or column) start indices for the imaginary part.">GetImagPtr</a>() <span class="keyword">const</span>;
<a name="l00128"></a>00128     <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a72f50ab4230ffe4af23c8bbf2203ad0b" title="Returns (row or column) indices of non-zero entries for the real part.">GetRealInd</a>() <span class="keyword">const</span>;
<a name="l00129"></a>00129     <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a2bb61950179c75be2150cfae734bb05b" title="Returns (row or column) indices of non-zero entries for / the imaginary part.">GetImagInd</a>() <span class="keyword">const</span>;
<a name="l00130"></a>00130     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a913aadd75d1ed2e076ea4b014e5fb32d" title="Returns the length of the array of start indices for the real part.">GetRealPtrSize</a>() <span class="keyword">const</span>;
<a name="l00131"></a>00131     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#acde6ac68023f92f32eceb4f0a422580f" title="Returns the length of the array of start indices for the imaginary part.">GetImagPtrSize</a>() <span class="keyword">const</span>;
<a name="l00132"></a>00132     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#acdb80353a65a45dfc11b886e7c75a807" title="Returns the length of the array of (column or row) indices for //! the real part...">GetRealIndSize</a>() <span class="keyword">const</span>;
<a name="l00133"></a>00133     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af1c1a606649e03827a3710751238a5bc" title="Returns the length of the array of (column or row) indices //! for the imaginary...">GetImagIndSize</a>() <span class="keyword">const</span>;
<a name="l00134"></a>00134     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad1304eb0b4e7197e603e95b119f24796" title="Returns the length of the array of (column or row) indices for //! the real part...">GetRealDataSize</a>() <span class="keyword">const</span>;
<a name="l00135"></a>00135     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#acd8debd721f3560c317babe090aa09e6" title="Returns the length of the array of (column or row) indices //! for the imaginary...">GetImagDataSize</a>() <span class="keyword">const</span>;
<a name="l00136"></a>00136     T* <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a1a98f711de4b7daf42ea42c8493ca08d" title="Returns the array of values of the real part.">GetRealData</a>() <span class="keyword">const</span>;
<a name="l00137"></a>00137     T* <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a411656148afd39425bd6df81a013d180" title="Returns the array of values of the imaginary part.">GetImagData</a>() <span class="keyword">const</span>;
<a name="l00138"></a>00138 
<a name="l00139"></a>00139     <span class="comment">// Element acess and affectation.</span>
<a name="l00140"></a>00140     complex&lt;value_type&gt; <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad441174c77a9c101560e77794aae6369" title="Access operator.">operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00141"></a>00141     value_type&amp; <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a0e58d558d63fc51d62db121f9732cf1c" title="Access method.">ValReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00142"></a>00142     <span class="keyword">const</span> value_type&amp; <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a0e58d558d63fc51d62db121f9732cf1c" title="Access method.">ValReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00143"></a>00143     value_type&amp; <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a4cf220882aeebe97ab0cd8afb1eb52c7" title="Access method.">ValImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00144"></a>00144     <span class="keyword">const</span> value_type&amp; <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a4cf220882aeebe97ab0cd8afb1eb52c7" title="Access method.">ValImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00145"></a>00145     value_type&amp; <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a1f8cbc14fed12f61ebc5a2bc7085c619" title="Access method.">GetReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00146"></a>00146     <span class="keyword">const</span> value_type&amp; <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a1f8cbc14fed12f61ebc5a2bc7085c619" title="Access method.">GetReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00147"></a>00147     value_type&amp; <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a2be1493a466aee2b75403551ddf42539" title="Access method.">GetImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00148"></a>00148     <span class="keyword">const</span> value_type&amp; <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a2be1493a466aee2b75403551ddf42539" title="Access method.">GetImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00149"></a>00149     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ab4c3a8968e2c79248fc7d1add27f3433" title="Sets an element (i, j) to a value.">Set</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; x);
<a name="l00150"></a>00150     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#aaf8bdd8dd7d3f089b38a6db2c18c4154" title="Add a value to a non-zero entry.">AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; x);
<a name="l00151"></a>00151     <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php" title="Symmetric complex sparse-matrix class.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00152"></a>00152     <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#aba07f35ba600bf49197e1b1802f95c67" title="Duplicates a matrix (assignment operator).">operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php" title="Symmetric complex sparse-matrix class.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A);
<a name="l00153"></a>00153 
<a name="l00154"></a>00154     <span class="comment">// Convenient functions.</span>
<a name="l00155"></a>00155     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a55f35dad8db11e522dde99f236472d9d" title="Resets all non-zero entries to 0-value.">Zero</a>();
<a name="l00156"></a>00156     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a2116e2afc727a7c2ecf920fe41811c97" title="Sets the matrix to identity.">SetIdentity</a>();
<a name="l00157"></a>00157     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ab53e3d0367145d47e83de9480ac29f54" title="Fills the non-zero entries with 0, 1, 2, ...">Fill</a>();
<a name="l00158"></a>00158     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ab53e3d0367145d47e83de9480ac29f54" title="Fills the non-zero entries with 0, 1, 2, ...">Fill</a>(<span class="keyword">const</span> complex&lt;T&gt;&amp; x);
<a name="l00159"></a>00159     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#afda343754417cd33781e8bd3f69f07d4" title="Fills the non-zero entries randomly.">FillRand</a>();
<a name="l00160"></a>00160 
<a name="l00161"></a>00161     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#abe86d6545d4aeca07257de2d8e7a85f2" title="Displays the matrix on the standard output.">Print</a>() <span class="keyword">const</span>;
<a name="l00162"></a>00162     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a8c8179698d7eccc69911fcbb21262d9b" title="Writes the matrix in a file.">Write</a>(<span class="keywordtype">string</span> FileName) <span class="keyword">const</span>;
<a name="l00163"></a>00163     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a8c8179698d7eccc69911fcbb21262d9b" title="Writes the matrix in a file.">Write</a>(ostream&amp; FileStream) <span class="keyword">const</span>;
<a name="l00164"></a>00164     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a1c9015515a2e218c875d739bd9f410e1" title="Writes the matrix in a file.">WriteText</a>(<span class="keywordtype">string</span> FileName) <span class="keyword">const</span>;
<a name="l00165"></a>00165     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a1c9015515a2e218c875d739bd9f410e1" title="Writes the matrix in a file.">WriteText</a>(ostream&amp; FileStream) <span class="keyword">const</span>;
<a name="l00166"></a>00166     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#aa70fdb81ecca409e7410b7bd91c71bc8" title="Reads the matrix from a file.">Read</a>(<span class="keywordtype">string</span> FileName);
<a name="l00167"></a>00167     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#aa70fdb81ecca409e7410b7bd91c71bc8" title="Reads the matrix from a file.">Read</a>(istream&amp; FileStream);
<a name="l00168"></a>00168     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ae5f0883621073e283ebd9ad047e81174" title="Reads the matrix from a file.">ReadText</a>(<span class="keywordtype">string</span> FileName);
<a name="l00169"></a>00169     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ae5f0883621073e283ebd9ad047e81174" title="Reads the matrix from a file.">ReadText</a>(istream&amp; FileStream);
<a name="l00170"></a>00170   };
<a name="l00171"></a>00171 
<a name="l00172"></a>00172 
<a name="l00174"></a>00174   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00175"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_complex_sparse_00_01_allocator_01_4.php">00175</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_sym_complex_sparse.php">ColSymComplexSparse</a>, Allocator&gt;:
<a name="l00176"></a>00176     <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php" title="Symmetric complex sparse-matrix class.">Matrix_SymComplexSparse</a>&lt;T, Prop, ColSymComplexSparse, Allocator&gt;
<a name="l00177"></a>00177   {
<a name="l00178"></a>00178     <span class="comment">// typedef declaration.</span>
<a name="l00179"></a>00179   <span class="keyword">public</span>:
<a name="l00180"></a>00180     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00181"></a>00181     <span class="keyword">typedef</span> Prop property;
<a name="l00182"></a>00182     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_col_sym_complex_sparse.php">ColSymComplexSparse</a> <a class="code" href="class_seldon_1_1_col_sym_complex_sparse.php">storage</a>;
<a name="l00183"></a>00183     <span class="keyword">typedef</span> Allocator allocator;
<a name="l00184"></a>00184 
<a name="l00185"></a>00185   <span class="keyword">public</span>:
<a name="l00186"></a>00186     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>();
<a name="l00187"></a>00187     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00188"></a>00188     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> real_nz, <span class="keywordtype">int</span> imag_nz);
<a name="l00189"></a>00189     <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00190"></a>00190               <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00191"></a>00191               <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00192"></a>00192     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00193"></a>00193            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; real_values,
<a name="l00194"></a>00194            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; real_ptr,
<a name="l00195"></a>00195            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; real_ind,
<a name="l00196"></a>00196            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; imag_values,
<a name="l00197"></a>00197            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; imag_ptr,
<a name="l00198"></a>00198            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; imag_ind);
<a name="l00199"></a>00199   };
<a name="l00200"></a>00200 
<a name="l00201"></a>00201 
<a name="l00203"></a>00203   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00204"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php">00204</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_sym_complex_sparse.php">RowSymComplexSparse</a>, Allocator&gt;:
<a name="l00205"></a>00205     <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php" title="Symmetric complex sparse-matrix class.">Matrix_SymComplexSparse</a>&lt;T, Prop, RowSymComplexSparse, Allocator&gt;
<a name="l00206"></a>00206   {
<a name="l00207"></a>00207     <span class="comment">// typedef declaration.</span>
<a name="l00208"></a>00208   <span class="keyword">public</span>:
<a name="l00209"></a>00209     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00210"></a>00210     <span class="keyword">typedef</span> Prop property;
<a name="l00211"></a>00211     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_row_sym_complex_sparse.php">RowSymComplexSparse</a> <a class="code" href="class_seldon_1_1_row_sym_complex_sparse.php">storage</a>;
<a name="l00212"></a>00212     <span class="keyword">typedef</span> Allocator allocator;
<a name="l00213"></a>00213 
<a name="l00214"></a>00214   <span class="keyword">public</span>:
<a name="l00215"></a>00215     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>();
<a name="l00216"></a>00216     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00217"></a>00217     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> real_nz, <span class="keywordtype">int</span> imag_nz);
<a name="l00218"></a>00218     <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00219"></a>00219               <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00220"></a>00220               <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00221"></a>00221     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00222"></a>00222            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; values,
<a name="l00223"></a>00223            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; ptr,
<a name="l00224"></a>00224            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; ind,
<a name="l00225"></a>00225            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; imag_values,
<a name="l00226"></a>00226            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; imag_ptr,
<a name="l00227"></a>00227            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; imag_ind);
<a name="l00228"></a>00228   };
<a name="l00229"></a>00229 
<a name="l00230"></a>00230 
<a name="l00231"></a>00231 } <span class="comment">// namespace Seldon.</span>
<a name="l00232"></a>00232 
<a name="l00233"></a>00233 <span class="preprocessor">#define SELDON_FILE_MATRIX_SYMCOMPLEXSPARSE_HXX</span>
<a name="l00234"></a>00234 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
