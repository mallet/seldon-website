<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix___sparse.php">Matrix_Sparse</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a> &#124;
<a href="#pro-static-attribs">Static Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Matrix_Sparse" --><!-- doxytag: inherits="Seldon::Matrix_Base" -->
<p>Sparse-matrix class.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_matrix___sparse_8hxx_source.php">Matrix_Sparse.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_matrix___sparse.png" usemap="#Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;_map" alt=""/>
  <map id="Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;_map" name="Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;_map">
<area href="class_seldon_1_1_matrix___base.php" alt="Seldon::Matrix_Base&lt; T, Allocator &gt;" shape="rect" coords="0,0,319,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_matrix___sparse-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a23568387ee0ebb729bad4c9a8458ab0d"></a><!-- doxytag: member="Seldon::Matrix_Sparse::value_type" ref="a23568387ee0ebb729bad4c9a8458ab0d" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a17d6d4a8b25b9b665161964638e0d017"></a><!-- doxytag: member="Seldon::Matrix_Sparse::pointer" ref="a17d6d4a8b25b9b665161964638e0d017" args="" -->
typedef Allocator::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac6bf06062ed7327ed8031389b1814e00"></a><!-- doxytag: member="Seldon::Matrix_Sparse::const_pointer" ref="ac6bf06062ed7327ed8031389b1814e00" args="" -->
typedef Allocator::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7a0a12ac0ce327760b73efa55c546b01"></a><!-- doxytag: member="Seldon::Matrix_Sparse::reference" ref="a7a0a12ac0ce327760b73efa55c546b01" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a155045f042d33151e62ee378ffd65826"></a><!-- doxytag: member="Seldon::Matrix_Sparse::const_reference" ref="a155045f042d33151e62ee378ffd65826" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5d4fad48d2c9129d9a8b25359bca1f87"></a><!-- doxytag: member="Seldon::Matrix_Sparse::entry_type" ref="a5d4fad48d2c9129d9a8b25359bca1f87" args="" -->
typedef value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>entry_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adc15be14ca9f0837f1e1fd9b7be24998"></a><!-- doxytag: member="Seldon::Matrix_Sparse::access_type" ref="adc15be14ca9f0837f1e1fd9b7be24998" args="" -->
typedef value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>access_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="abc9e626619660d853b4a49aac420a220"></a><!-- doxytag: member="Seldon::Matrix_Sparse::const_access_type" ref="abc9e626619660d853b4a49aac420a220" args="" -->
typedef value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_access_type</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#af4624e0ed00838ec34b030103f9dd383">Matrix_Sparse</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#af4624e0ed00838ec34b030103f9dd383"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a71e366ab3e3623e8f9a27fd7796621b4">Matrix_Sparse</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Constructor.  <a href="#a71e366ab3e3623e8f9a27fd7796621b4"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#af29835daf0a5da91592c9f9bcf7ce1a7">Matrix_Sparse</a> (int i, int j, int nz)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Constructor.  <a href="#af29835daf0a5da91592c9f9bcf7ce1a7"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Storage0 , class Allocator0 , class Storage1 , class Allocator1 , class Storage2 , class Allocator2 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a2ff242fb46f84f041e4709a7eff70cd3">Matrix_Sparse</a> (int i, int j, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;values, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;ptr, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;ind)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Constructor.  <a href="#a2ff242fb46f84f041e4709a7eff70cd3"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a49f2012fc254ff9e960b1dfee766ce0f"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Matrix_Sparse" ref="a49f2012fc254ff9e960b1dfee766ce0f" args="(const Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt; &amp;A)" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a49f2012fc254ff9e960b1dfee766ce0f">Matrix_Sparse</a> (const <a class="el" href="class_seldon_1_1_matrix___sparse.php">Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copy constructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a805ce704fffb3c509d908ec3ac45db28"></a><!-- doxytag: member="Seldon::Matrix_Sparse::~Matrix_Sparse" ref="a805ce704fffb3c509d908ec3ac45db28" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a805ce704fffb3c509d908ec3ac45db28">~Matrix_Sparse</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Destructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#ab63b8d9aee1a38054cd525d7fa708f1a">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the matrix.  <a href="#ab63b8d9aee1a38054cd525d7fa708f1a"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Storage0 , class Allocator0 , class Storage1 , class Allocator1 , class Storage2 , class Allocator2 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a5b411ce16d320a1442588af33adefe09">SetData</a> (int i, int j, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;values, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;ptr, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;ind)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Redefines the matrix.  <a href="#a5b411ce16d320a1442588af33adefe09"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afb0445044b6c8db5350908299350de91"></a><!-- doxytag: member="Seldon::Matrix_Sparse::SetData" ref="afb0445044b6c8db5350908299350de91" args="(int i, int j, int nz, pointer values, int *ptr, int *ind)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetData</b> (int i, int j, int nz, pointer values, int *ptr, int *ind)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a805231f8ec04efc5a081b21c9f65d5c9">Nullify</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the matrix without releasing memory.  <a href="#a805231f8ec04efc5a081b21c9f65d5c9"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a68315328fe25c14b9d8bef181c1c2f49">Reallocate</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Initialization of an empty sparse matrix with i rows and j columns.  <a href="#a68315328fe25c14b9d8bef181c1c2f49"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a0e9b834228440d4e0d16e4419b20f3f7">Reallocate</a> (int i, int j, int nz)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Initialization of a sparse matrix with i rows and j columns.  <a href="#a0e9b834228440d4e0d16e4419b20f3f7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a84a417a78587e38b7e9caeecb766aa84">Resize</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates memory to resize the matrix and keeps previous entries.  <a href="#a84a417a78587e38b7e9caeecb766aa84"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#ad08188610a0d5051dad66e43ea2a017f">Resize</a> (int i, int j, int nz)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates memory to resize the matrix and keeps previous entries.  <a href="#ad08188610a0d5051dad66e43ea2a017f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a54604e7e696124d278a4aa198edcab77"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Copy" ref="a54604e7e696124d278a4aa198edcab77" args="(const Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt; &amp;A)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a54604e7e696124d278a4aa198edcab77">Copy</a> (const <a class="el" href="class_seldon_1_1_matrix___sparse.php">Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copies a matrix. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a0a0f412562d73bb7c9361da17565e7fb">GetNonZeros</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of non-zero elements.  <a href="#a0a0f412562d73bb7c9361da17565e7fb"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#aac6b06ab623602d24a80d12adf495cf2">GetDataSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored in memory.  <a href="#aac6b06ab623602d24a80d12adf495cf2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a9126980f0a760e8a73c2d0f480c69342">GetPtr</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns (row or column) start indices.  <a href="#a9126980f0a760e8a73c2d0f480c69342"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a1378a5e93b30065896f0d84b07b18caa">GetInd</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns (row or column) indices of non-zero entries.  <a href="#a1378a5e93b30065896f0d84b07b18caa"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a0c3409f0fec69fb357c493154e5d763b">GetPtrSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the length of the array of start indices.  <a href="#a0c3409f0fec69fb357c493154e5d763b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a7b136934dfbe364dd016ac33e4a5dcc6">GetIndSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the length of the array of (column or row) indices.  <a href="#a7b136934dfbe364dd016ac33e4a5dcc6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">value_type&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a019d20b720889f75c4c2314b40e86954">operator()</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a019d20b720889f75c4c2314b40e86954"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">value_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a7f416147b6680860cd489dfa6cb08b58">Val</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#a7f416147b6680860cd489dfa6cb08b58"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">value_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#afe93d17275283fe4740428f833e633e0">Get</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#afe93d17275283fe4740428f833e633e0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const value_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a508d4a87841d185e00cbac5223dfc91f">Val</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#a508d4a87841d185e00cbac5223dfc91f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const value_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#add8b1015a5844d58351dcd1e82571b26">Get</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#add8b1015a5844d58351dcd1e82571b26"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a6ede24fc389f58952986463da449bfb0">AddInteraction</a> (int i, int j, const T &amp;val)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Add a value to a non-zero entry.  <a href="#a6ede24fc389f58952986463da449bfb0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a059694445d9f150c96fa35c53f05dbe8">Set</a> (int i, int j, const T &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets an element (i, j) to a value.  <a href="#a059694445d9f150c96fa35c53f05dbe8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix___sparse.php">Matrix_Sparse</a>&lt; T, Prop, <br class="typebreak"/>
Storage, Allocator &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a58f047d2d62a3e5e7f7feaae98cd7d7f">operator=</a> (const <a class="el" href="class_seldon_1_1_matrix___sparse.php">Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a matrix (assignment operator).  <a href="#a58f047d2d62a3e5e7f7feaae98cd7d7f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a812985d3c66421787a6dbe687620ea1a">Zero</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Resets all non-zero entries to 0-value.  <a href="#a812985d3c66421787a6dbe687620ea1a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a657c33784aee4b940c6a635c33f2eb5e">SetIdentity</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the matrix to identity.  <a href="#a657c33784aee4b940c6a635c33f2eb5e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#abe41180630623f201ea254e009931abd">Fill</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the non-zero entries with 0, 1, 2, ...  <a href="#abe41180630623f201ea254e009931abd"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#aa2721edc471c0603919afa9365bee419">Fill</a> (const T0 &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the non-zero entries with a given value.  <a href="#aa2721edc471c0603919afa9365bee419"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a27b33a9915ce2922d2f3f0a0270b467f">FillRand</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the non-zero entries randomly.  <a href="#a27b33a9915ce2922d2f3f0a0270b467f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#aa1b2d232d468db2bf6d644fab1e87ee0">FillRand</a> (int Nelement)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the matrix with random elements.  <a href="#aa1b2d232d468db2bf6d644fab1e87ee0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a5055b1045054abd648cfb7daaf2d1042">FillRand</a> (int Nelement, const T &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the matrix with one value inserted at random positions.  <a href="#a5055b1045054abd648cfb7daaf2d1042"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a8d6d48b6f0277f1df2d52b798975407e">Print</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays the matrix on the standard output.  <a href="#a8d6d48b6f0277f1df2d52b798975407e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#aa0120b7bbfbe15d0a24a1b05b304bc2d">Write</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix in a file.  <a href="#aa0120b7bbfbe15d0a24a1b05b304bc2d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#ac7965debbd1eee355b2b2c4d4258e1d2">Write</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix to an output stream.  <a href="#ac7965debbd1eee355b2b2c4d4258e1d2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a45ac6acb158f9b49af3d50a2da34b708">WriteText</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix in a file.  <a href="#a45ac6acb158f9b49af3d50a2da34b708"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a3afe060ada2f12888f0b0121940df392">WriteText</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix to an output stream.  <a href="#a3afe060ada2f12888f0b0121940df392"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#ab3898fc51b20fa37a13cd69a2db3e5a1">Read</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from a file.  <a href="#ab3898fc51b20fa37a13cd69a2db3e5a1"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#aa45e8f50f4227fbcfbc05730c1e65a90">Read</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from an input stream.  <a href="#aa45e8f50f4227fbcfbc05730c1e65a90"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#a6a2bad0b67f1410249057b24a75afcea">ReadText</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from a file.  <a href="#a6a2bad0b67f1410249057b24a75afcea"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#aba9c257bd0cf7681f6be5391dc315d07">ReadText</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from an input stream.  <a href="#aba9c257bd0cf7681f6be5391dc315d07"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T , class Prop , class Storage , class Allocator &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___sparse.php#acf57d5d2ab0730c882b331ce89d4768a">SetData</a> (int i, int j, int nz, typename <a class="el" href="class_seldon_1_1_matrix___sparse.php">Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::pointer values, int *ptr, int *ind)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Redefines the matrix.  <a href="#acf57d5d2ab0730c882b331ce89d4768a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">GetM</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows.  <a href="#a65e9c3f0db9c7c8c3fa10ead777dd927"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows of the matrix possibly transposed.  <a href="#ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">GetN</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns.  <a href="#a6b134070d1b890ba6b7eb72fee170984"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns of the matrix possibly transposed.  <a href="#a7d27412bc9384a5ce0c0db1ee310d31c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">GetSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements in the matrix.  <a href="#a0fbc3f7030583174eaa69429f655a1b0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer to the data array.  <a href="#a453a269dfe7fadba249064363d5ab92a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a const pointer to the data array.  <a href="#a0f2796430deb08e565df8ced656097bf"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "void*" to the data array.  <a href="#a505cdfee34741c463e0dc1943337bedc"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "const void*" to the data array.  <a href="#a5979450cd8801810229f4a24c36e6639"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">Allocator &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the allocator of the matrix.  <a href="#af88748a55208349367d6860ad76fd691"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a00cbdaa5268c6bafd7036c394b17d02d"></a><!-- doxytag: member="Seldon::Matrix_Sparse::nz_" ref="a00cbdaa5268c6bafd7036c394b17d02d" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>nz_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1d4209f710cd11c291c69fe3c0cfdddb"></a><!-- doxytag: member="Seldon::Matrix_Sparse::ptr_" ref="a1d4209f710cd11c291c69fe3c0cfdddb" args="" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>ptr_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac1f3c20bcdb3a1bf06c9df3343b2bc2f"></a><!-- doxytag: member="Seldon::Matrix_Sparse::ind_" ref="ac1f3c20bcdb3a1bf06c9df3343b2bc2f" args="" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>ind_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a96b39ff494d4b7d3e30f320a1c7b4aba"></a><!-- doxytag: member="Seldon::Matrix_Sparse::m_" ref="a96b39ff494d4b7d3e30f320a1c7b4aba" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>m_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a11cb5d502050e5f14482ff0c9cef5a76"></a><!-- doxytag: member="Seldon::Matrix_Sparse::n_" ref="a11cb5d502050e5f14482ff0c9cef5a76" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>n_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acb12a8b74699fae7ae3b2b67376795a1"></a><!-- doxytag: member="Seldon::Matrix_Sparse::data_" ref="acb12a8b74699fae7ae3b2b67376795a1" args="" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-static-attribs"></a>
Static Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a132fa01ce120f352d434fd920e5ad9b4"></a><!-- doxytag: member="Seldon::Matrix_Sparse::allocator_" ref="a132fa01ce120f352d434fd920e5ad9b4" args="" -->
static Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>allocator_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Prop, class Storage, class Allocator = SELDON_DEFAULT_ALLOCATOR&lt;T&gt;&gt;<br/>
 class Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</h3>

<p>Sparse-matrix class. </p>
<p>Sparse matrices are defined by: (1) the number of rows and columns; (2) the number of non-zero entries; (3) an array 'ptr_' of start indices (i.e. indices of the first element of each row or column, depending on the storage); (4) an array 'ind_' of column or row indices of each non-zero entry; (5) values of non-zero entries. </p>

<p>Definition at line <a class="el" href="_matrix___sparse_8hxx_source.php#l00044">44</a> of file <a class="el" href="_matrix___sparse_8hxx_source.php">Matrix_Sparse.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="af4624e0ed00838ec34b030103f9dd383"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Matrix_Sparse" ref="af4624e0ed00838ec34b030103f9dd383" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___sparse.php">Matrix_Sparse</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>Builds an empty 0x0 matrix. </p>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l00039">39</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a71e366ab3e3623e8f9a27fd7796621b4"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Matrix_Sparse" ref="a71e366ab3e3623e8f9a27fd7796621b4" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___sparse.php">Matrix_Sparse</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Constructor. </p>
<p>Builds a i by j sparse matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l00055">55</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af29835daf0a5da91592c9f9bcf7ce1a7"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Matrix_Sparse" ref="af29835daf0a5da91592c9f9bcf7ce1a7" args="(int i, int j, int nz)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___sparse.php">Matrix_Sparse</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nz</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Constructor. </p>
<p>Builds a sparse matrix of size i by j , with nz non-zero elements. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>nz</em>&nbsp;</td><td>number of non-zero elements. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> values are not initialized. Indices of non-zero entries are not initialized either. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l00077">77</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2ff242fb46f84f041e4709a7eff70cd3"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Matrix_Sparse" ref="a2ff242fb46f84f041e4709a7eff70cd3" args="(int i, int j, Vector&lt; T, Storage0, Allocator0 &gt; &amp;values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;ind)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Storage0 , class Allocator0 , class Storage1 , class Allocator1 , class Storage2 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___sparse.php">Matrix_Sparse</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>values</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>ptr</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>ind</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Constructor. </p>
<p>Builds a i by j sparse matrix with non-zero values and indices provided by 'values' (values), 'ptr' (pointers) and 'ind' (indices). Input vectors are released and are empty on exit. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>values</em>&nbsp;</td><td>values of non-zero entries. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>ptr</em>&nbsp;</td><td>row or column start indices. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>ind</em>&nbsp;</td><td>row or column indices. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Input vectors 'values', 'ptr' and 'ind' are empty on exit. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l00105">105</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="a6ede24fc389f58952986463da449bfb0"></a><!-- doxytag: member="Seldon::Matrix_Sparse::AddInteraction" ref="a6ede24fc389f58952986463da449bfb0" args="(int i, int j, const T &amp;val)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::AddInteraction </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const T &amp;&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Add a value to a non-zero entry. </p>
<p>This function adds <em>val</em> to the element (<em>i</em>, <em>j</em>), provided that this element is already a non-zero entry. Otherwise a non-zero entry is inserted equal to <em>val</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>value to be added to the element (<em>i</em>, <em>j</em>). </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01274">1274</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab63b8d9aee1a38054cd525d7fa708f1a"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Clear" ref="ab63b8d9aee1a38054cd525d7fa708f1a" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::Clear </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the matrix. </p>
<p>This methods is equivalent to the destructor. On exit, the matrix is empty (0x0). </p>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l00273">273</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="abe41180630623f201ea254e009931abd"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Fill" ref="abe41180630623f201ea254e009931abd" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::Fill </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the non-zero entries with 0, 1, 2, ... </p>
<p>On exit, the non-zero entries are 0, 1, 2, 3, ... The order of the numbers depends on the storage. </p>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01364">1364</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa2721edc471c0603919afa9365bee419"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Fill" ref="aa2721edc471c0603919afa9365bee419" args="(const T0 &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::Fill </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the non-zero entries with a given value. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>the value to set the non-zero entries to. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01377">1377</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a27b33a9915ce2922d2f3f0a0270b467f"></a><!-- doxytag: member="Seldon::Matrix_Sparse::FillRand" ref="a27b33a9915ce2922d2f3f0a0270b467f" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::FillRand </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the non-zero entries randomly. </p>
<dl class="note"><dt><b>Note:</b></dt><dd>The random generator is very basic. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01389">1389</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa1b2d232d468db2bf6d644fab1e87ee0"></a><!-- doxytag: member="Seldon::Matrix_Sparse::FillRand" ref="aa1b2d232d468db2bf6d644fab1e87ee0" args="(int Nelement)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::FillRand </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>Nelement</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the matrix with random elements. </p>
<p>The matrix is cleared and then filled with <em>n</em> random elements. Both the position of the elements and their values are randomly generated. On exit, the matrix may not have <em>n</em> non-zero elements: it is possible that the randomly-generated positions of two elements are the same. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>Nelement</em>&nbsp;</td><td>the number of random elements to be inserted in the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>The random generator is very basic. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01408">1408</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5055b1045054abd648cfb7daaf2d1042"></a><!-- doxytag: member="Seldon::Matrix_Sparse::FillRand" ref="a5055b1045054abd648cfb7daaf2d1042" args="(int Nelement, const T &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::FillRand </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>Nelement</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const T &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the matrix with one value inserted at random positions. </p>
<p>The matrix is cleared and then filled with <em>n</em> random elements. Only the position of the elements is randomly generated. Their value will always be <em>x</em>. On exit, the matrix may not have <em>n</em> non-zero elements: it is possible that the randomly-generated positions of two elements are the same. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>Nelement</em>&nbsp;</td><td>the number of random elements to be inserted in the matrix. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>x</em>&nbsp;</td><td>the value to be inserted. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>The random generator is very basic. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01455">1455</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="afe93d17275283fe4740428f833e633e0"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Get" ref="afe93d17275283fe4740428f833e633e0" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___sparse.php">Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &amp; <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::Get </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns reference to element (<em>i</em>, <em>j</em>) </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. If the element does not belong to sparsity pattern of the matrix, the matrix is resized. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01196">1196</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="add8b1015a5844d58351dcd1e82571b26"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Get" ref="add8b1015a5844d58351dcd1e82571b26" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_matrix___sparse.php">Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &amp; <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::Get </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns reference to element (<em>i</em>, <em>j</em>) </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01258">1258</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af88748a55208349367d6860ad76fd691"></a><!-- doxytag: member="Seldon::Matrix_Sparse::GetAllocator" ref="af88748a55208349367d6860ad76fd691" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">Allocator &amp; <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetAllocator </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the allocator of the matrix. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The allocator. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00258">258</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a453a269dfe7fadba249064363d5ab92a"></a><!-- doxytag: member="Seldon::Matrix_Sparse::GetData" ref="a453a269dfe7fadba249064363d5ab92a" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer to the data array. </p>
<p>Returns a pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00208">208</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0f2796430deb08e565df8ced656097bf"></a><!-- doxytag: member="Seldon::Matrix_Sparse::GetDataConst" ref="a0f2796430deb08e565df8ced656097bf" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::const_pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConst </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a const pointer to the data array. </p>
<p>Returns a const pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00221">221</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5979450cd8801810229f4a24c36e6639"></a><!-- doxytag: member="Seldon::Matrix_Sparse::GetDataConstVoid" ref="a5979450cd8801810229f4a24c36e6639" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConstVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "const void*" to the data array. </p>
<p>Returns a pointer of type "const void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00247">247</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aac6b06ab623602d24a80d12adf495cf2"></a><!-- doxytag: member="Seldon::Matrix_Sparse::GetDataSize" ref="aac6b06ab623602d24a80d12adf495cf2" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetDataSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored in memory. </p>
<p>Returns the number of elements stored in memory, i.e. the number of non-zero entries. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements stored in memory. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l00975">975</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a505cdfee34741c463e0dc1943337bedc"></a><!-- doxytag: member="Seldon::Matrix_Sparse::GetDataVoid" ref="a505cdfee34741c463e0dc1943337bedc" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "void*" to the data array. </p>
<p>Returns a pointer of type "void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00234">234</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1378a5e93b30065896f0d84b07b18caa"></a><!-- doxytag: member="Seldon::Matrix_Sparse::GetInd" ref="a1378a5e93b30065896f0d84b07b18caa" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int * <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetInd </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns (row or column) indices of non-zero entries. </p>
<p>Returns the array ('ind_') of (row or column) indices of non-zero entries. This array defines non-zero entries indices if coupled with (column or row) start indices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The array of (row or column) indices of non-zero entries. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01002">1002</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7b136934dfbe364dd016ac33e4a5dcc6"></a><!-- doxytag: member="Seldon::Matrix_Sparse::GetIndSize" ref="a7b136934dfbe364dd016ac33e4a5dcc6" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetIndSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the length of the array of (column or row) indices. </p>
<p>Returns the length of the array ('ind_') of (row or column) indices of non-zero entries. This array defines non-zero entries indices if coupled with (column or row) start indices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the array of (column or row) indices. </dd></dl>
<dl class="note"><dt><b>Note:</b></dt><dd>The length of the array of (column or row) indices is the number of non-zero entries. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01029">1029</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a65e9c3f0db9c7c8c3fa10ead777dd927"></a><!-- doxytag: member="Seldon::Matrix_Sparse::GetM" ref="a65e9c3f0db9c7c8c3fa10ead777dd927" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a8c781e99310aae01786eac0e8e5aa50c">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00107">107</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><!-- doxytag: member="Seldon::Matrix_Sparse::GetM" ref="ab6f6c5bba065ca82dad7c3abdbc8ea79" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#aecf3e8c636dbb83335ea588afe2558a8">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00130">130</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6b134070d1b890ba6b7eb72fee170984"></a><!-- doxytag: member="Seldon::Matrix_Sparse::GetN" ref="a6b134070d1b890ba6b7eb72fee170984" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a40580a92202044416e379c1304ff0220">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00118">118</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7d27412bc9384a5ce0c0db1ee310d31c"></a><!-- doxytag: member="Seldon::Matrix_Sparse::GetN" ref="a7d27412bc9384a5ce0c0db1ee310d31c" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a867066f7bf531bf7ad15bdcd034dc3c0">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00145">145</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0a0f412562d73bb7c9361da17565e7fb"></a><!-- doxytag: member="Seldon::Matrix_Sparse::GetNonZeros" ref="a0a0f412562d73bb7c9361da17565e7fb" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetNonZeros </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of non-zero elements. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of non-zero elements. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l00962">962</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9126980f0a760e8a73c2d0f480c69342"></a><!-- doxytag: member="Seldon::Matrix_Sparse::GetPtr" ref="a9126980f0a760e8a73c2d0f480c69342" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int * <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetPtr </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns (row or column) start indices. </p>
<p>Returns the array ('ptr_') of start indices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The array of start indices. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l00987">987</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0c3409f0fec69fb357c493154e5d763b"></a><!-- doxytag: member="Seldon::Matrix_Sparse::GetPtrSize" ref="a0c3409f0fec69fb357c493154e5d763b" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetPtrSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the length of the array of start indices. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the array of start indices. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01013">1013</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0fbc3f7030583174eaa69429f655a1b0"></a><!-- doxytag: member="Seldon::Matrix_Sparse::GetSize" ref="a0fbc3f7030583174eaa69429f655a1b0" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements in the matrix. </p>
<p>Returns the number of elements in the matrix, i.e. the number of rows multiplied by the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements in the matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00195">195</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a805231f8ec04efc5a081b21c9f65d5c9"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Nullify" ref="a805231f8ec04efc5a081b21c9f65d5c9" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::Nullify </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the matrix without releasing memory. </p>
<p>On exit, the matrix is empty and the memory has not been released. It is useful for low level manipulations on a <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> instance. </p>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l00416">416</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a019d20b720889f75c4c2314b40e86954"></a><!-- doxytag: member="Seldon::Matrix_Sparse::operator()" ref="a019d20b720889f75c4c2314b40e86954" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___sparse.php">Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01049">1049</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a58f047d2d62a3e5e7f7feaae98cd7d7f"></a><!-- doxytag: member="Seldon::Matrix_Sparse::operator=" ref="a58f047d2d62a3e5e7f7feaae98cd7d7f" args="(const Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop, class Storage, class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___sparse.php">Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_matrix___sparse.php">Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a matrix (assignment operator). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>A</em>&nbsp;</td><td>matrix to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'A' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01303">1303</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a8d6d48b6f0277f1df2d52b798975407e"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Print" ref="a8d6d48b6f0277f1df2d52b798975407e" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::Print </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Displays the matrix on the standard output. </p>
<p>Displays elements on the standard output, in text format. Each row is displayed on a single line and elements of a row are delimited by tabulations. </p>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01486">1486</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab3898fc51b20fa37a13cd69a2db3e5a1"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Read" ref="ab3898fc51b20fa37a13cd69a2db3e5a1" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from a file. </p>
<p>Reads a matrix stored in binary format in a file. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>input file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01621">1621</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa45e8f50f4227fbcfbc05730c1e65a90"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Read" ref="aa45e8f50f4227fbcfbc05730c1e65a90" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from an input stream. </p>
<p>Reads a matrix in binary format from an input stream. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>input stream </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01646">1646</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6a2bad0b67f1410249057b24a75afcea"></a><!-- doxytag: member="Seldon::Matrix_Sparse::ReadText" ref="a6a2bad0b67f1410249057b24a75afcea" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from a file. </p>
<p>Reads the matrix from a file in text format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>input file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01687">1687</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aba9c257bd0cf7681f6be5391dc315d07"></a><!-- doxytag: member="Seldon::Matrix_Sparse::ReadText" ref="aba9c257bd0cf7681f6be5391dc315d07" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from an input stream. </p>
<p>Reads a matrix from a stream in text format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>input stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01712">1712</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a68315328fe25c14b9d8bef181c1c2f49"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Reallocate" ref="a68315328fe25c14b9d8bef181c1c2f49" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::Reallocate </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Initialization of an empty sparse matrix with i rows and j columns. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l00433">433</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0e9b834228440d4e0d16e4419b20f3f7"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Reallocate" ref="a0e9b834228440d4e0d16e4419b20f3f7" args="(int i, int j, int nz)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::Reallocate </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nz</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Initialization of a sparse matrix with i rows and j columns. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>nz</em>&nbsp;</td><td>number of non-zero entries </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l00492">492</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad08188610a0d5051dad66e43ea2a017f"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Resize" ref="ad08188610a0d5051dad66e43ea2a017f" args="(int i, int j, int nz)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::Resize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nz</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates memory to resize the matrix and keeps previous entries. </p>
<p>On exit, the matrix is a i x j matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>new number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>new number of columns. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>nz</em>&nbsp;</td><td>number of non-zero elements. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l00666">666</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a84a417a78587e38b7e9caeecb766aa84"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Resize" ref="a84a417a78587e38b7e9caeecb766aa84" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::Resize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates memory to resize the matrix and keeps previous entries. </p>
<p>On exit, the matrix is a i x j matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>new number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>new number of columns. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l00648">648</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a059694445d9f150c96fa35c53f05dbe8"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Set" ref="a059694445d9f150c96fa35c53f05dbe8" args="(int i, int j, const T &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::Set </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const T &amp;&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets an element (i, j) to a value. </p>
<p>This function sets <em>val</em> to the element (<em>i</em>, <em>j</em>) </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>A(i, j) = val </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01288">1288</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5b411ce16d320a1442588af33adefe09"></a><!-- doxytag: member="Seldon::Matrix_Sparse::SetData" ref="a5b411ce16d320a1442588af33adefe09" args="(int i, int j, Vector&lt; T, Storage0, Allocator0 &gt; &amp;values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;ind)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Storage0 , class Allocator0 , class Storage1 , class Allocator1 , class Storage2 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::SetData </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>values</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>ptr</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>ind</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Redefines the matrix. </p>
<p>It clears the matrix and sets it to a new matrix defined by 'values' (values), 'ptr' (pointers) and 'ind' (indices). Input vectors are released and are empty on exit. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>values</em>&nbsp;</td><td>values of non-zero entries. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>ptr</em>&nbsp;</td><td>row or column start indices. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>ind</em>&nbsp;</td><td>row or column indices. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Input vectors 'values', 'ptr' and 'ind' are empty on exit. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l00300">300</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="acf57d5d2ab0730c882b331ce89d4768a"></a><!-- doxytag: member="Seldon::Matrix_Sparse::SetData" ref="acf57d5d2ab0730c882b331ce89d4768a" args="(int i, int j, int nz, typename Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;::pointer values, int *ptr, int *ind)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop, class Storage, class Allocator = SELDON_DEFAULT_ALLOCATOR&lt;T&gt;&gt; </div>
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::SetData </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nz</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">typename <a class="el" href="class_seldon_1_1_matrix___sparse.php">Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::pointer&nbsp;</td>
          <td class="paramname"> <em>values</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int *&nbsp;</td>
          <td class="paramname"> <em>ptr</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int *&nbsp;</td>
          <td class="paramname"> <em>ind</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Redefines the matrix. </p>
<p>It clears the matrix and sets it to a new matrix defined by arrays 'values' (values), 'ptr' (pointers) and 'ind' (indices). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>nz</em>&nbsp;</td><td>number of non-zero entries. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>values</em>&nbsp;</td><td>values of non-zero entries. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>ptr</em>&nbsp;</td><td>row or column start indices. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>ind</em>&nbsp;</td><td>row or column indices. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>On exit, arrays 'values', 'ptr' and 'ind' are managed by the matrix. For example, it means that the destructor will released those arrays; therefore, the user mustn't release those arrays. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l00392">392</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a657c33784aee4b940c6a635c33f2eb5e"></a><!-- doxytag: member="Seldon::Matrix_Sparse::SetIdentity" ref="a657c33784aee4b940c6a635c33f2eb5e" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::SetIdentity </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the matrix to identity. </p>
<p>This method fills the diagonal of the matrix with ones. It can be applied to non square matrix. </p>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01331">1331</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7f416147b6680860cd489dfa6cb08b58"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Val" ref="a7f416147b6680860cd489dfa6cb08b58" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___sparse.php">Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &amp; <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::Val </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the value of element (<em>i</em>, <em>j</em>) if it can be returned as a reference. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. </dd></dl>
<dl><dt><b>Exceptions:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em><a class="el" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a></em>&nbsp;</td><td>No reference can be returned because the element is a zero entry (not stored in the matrix). </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01094">1094</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a508d4a87841d185e00cbac5223dfc91f"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Val" ref="a508d4a87841d185e00cbac5223dfc91f" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="class_seldon_1_1_matrix___sparse.php">Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::value_type &amp; <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::Val </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the value of element (<em>i</em>, <em>j</em>) if it can be returned as a reference. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. </dd></dl>
<dl><dt><b>Exceptions:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em><a class="el" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a></em>&nbsp;</td><td>No reference can be returned because the element is a zero entry (not stored in the matrix). </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01146">1146</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac7965debbd1eee355b2b2c4d4258e1d2"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Write" ref="ac7965debbd1eee355b2b2c4d4258e1d2" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix to an output stream. </p>
<p>Stores the matrix in an output stream in binary format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01529">1529</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa0120b7bbfbe15d0a24a1b05b304bc2d"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Write" ref="aa0120b7bbfbe15d0a24a1b05b304bc2d" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix in a file. </p>
<p>Stores the matrix in a file in binary format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01504">1504</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a45ac6acb158f9b49af3d50a2da34b708"></a><!-- doxytag: member="Seldon::Matrix_Sparse::WriteText" ref="a45ac6acb158f9b49af3d50a2da34b708" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix in a file. </p>
<p>Stores the matrix in a file in ascii format. The entries are written in coordinate format (row column value) 1-index convention is used </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01563">1563</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3afe060ada2f12888f0b0121940df392"></a><!-- doxytag: member="Seldon::Matrix_Sparse::WriteText" ref="a3afe060ada2f12888f0b0121940df392" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix to an output stream. </p>
<p>Stores the matrix in a file in ascii format. The entries are written in coordinate format (row column value) 1-index convention is used </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01590">1590</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a812985d3c66421787a6dbe687620ea1a"></a><!-- doxytag: member="Seldon::Matrix_Sparse::Zero" ref="a812985d3c66421787a6dbe687620ea1a" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse</a>&lt; T, Prop, Storage, Allocator &gt;::Zero </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Resets all non-zero entries to 0-value. </p>
<p>The sparsity pattern remains unchanged. </p>

<p>Definition at line <a class="el" href="_matrix___sparse_8cxx_source.php#l01319">1319</a> of file <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>matrix_sparse/<a class="el" href="_matrix___sparse_8hxx_source.php">Matrix_Sparse.hxx</a></li>
<li>matrix_sparse/<a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
