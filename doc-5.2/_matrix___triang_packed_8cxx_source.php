<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix/Matrix_TriangPacked.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_TRIANGPACKED_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;Matrix_TriangPacked.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="keyword">namespace </span>Seldon
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 
<a name="l00029"></a>00029   <span class="comment">/****************</span>
<a name="l00030"></a>00030 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00031"></a>00031 <span class="comment">   ****************/</span>
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00035"></a>00035 
<a name="l00038"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ad9698cd3163185b5794550f516224d65">00038</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00039"></a>00039   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ad9698cd3163185b5794550f516224d65" title="Default constructor.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00040"></a>00040 <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ad9698cd3163185b5794550f516224d65" title="Default constructor.">  ::Matrix_TriangPacked</a>(): <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;()
<a name="l00041"></a>00041   {
<a name="l00042"></a>00042   }
<a name="l00043"></a>00043 
<a name="l00044"></a>00044 
<a name="l00046"></a>00046 
<a name="l00051"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a785e3d146fce6e050f7fbba140a94406">00051</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00052"></a>00052   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ad9698cd3163185b5794550f516224d65" title="Default constructor.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00053"></a>00053 <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ad9698cd3163185b5794550f516224d65" title="Default constructor.">  ::Matrix_TriangPacked</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j): <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, i)
<a name="l00054"></a>00054   {
<a name="l00055"></a>00055 
<a name="l00056"></a>00056 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00057"></a>00057 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00058"></a>00058       {
<a name="l00059"></a>00059 <span class="preprocessor">#endif</span>
<a name="l00060"></a>00060 <span class="preprocessor"></span>
<a name="l00061"></a>00061         this-&gt;data_ = this-&gt;allocator_.allocate((i * (i + 1)) / 2, <span class="keyword">this</span>);
<a name="l00062"></a>00062 
<a name="l00063"></a>00063 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00064"></a>00064 <span class="preprocessor"></span>      }
<a name="l00065"></a>00065     <span class="keywordflow">catch</span> (...)
<a name="l00066"></a>00066       {
<a name="l00067"></a>00067         this-&gt;m_ = 0;
<a name="l00068"></a>00068         this-&gt;n_ = 0;
<a name="l00069"></a>00069         this-&gt;data_ = NULL;
<a name="l00070"></a>00070         <span class="keywordflow">return</span>;
<a name="l00071"></a>00071       }
<a name="l00072"></a>00072     <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00073"></a>00073       {
<a name="l00074"></a>00074         this-&gt;m_ = 0;
<a name="l00075"></a>00075         this-&gt;n_ = 0;
<a name="l00076"></a>00076         <span class="keywordflow">return</span>;
<a name="l00077"></a>00077       }
<a name="l00078"></a>00078 <span class="preprocessor">#endif</span>
<a name="l00079"></a>00079 <span class="preprocessor"></span>
<a name="l00080"></a>00080   }
<a name="l00081"></a>00081 
<a name="l00082"></a>00082 
<a name="l00084"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a61b5e4d1b404eab627203abdfa6287f8">00084</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00085"></a>00085   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ad9698cd3163185b5794550f516224d65" title="Default constructor.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00086"></a>00086 <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ad9698cd3163185b5794550f516224d65" title="Default constructor.">  ::Matrix_TriangPacked</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked</a>&lt;T, Prop,
<a name="l00087"></a>00087                         Storage, Allocator&gt;&amp; A)
<a name="l00088"></a>00088     : <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;()
<a name="l00089"></a>00089   {
<a name="l00090"></a>00090     this-&gt;m_ = 0;
<a name="l00091"></a>00091     this-&gt;n_ = 0;
<a name="l00092"></a>00092     this-&gt;data_ = NULL;
<a name="l00093"></a>00093 
<a name="l00094"></a>00094     this-&gt;Copy(A);
<a name="l00095"></a>00095   }
<a name="l00096"></a>00096 
<a name="l00097"></a>00097 
<a name="l00098"></a>00098   <span class="comment">/**************</span>
<a name="l00099"></a>00099 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00100"></a>00100 <span class="comment">   **************/</span>
<a name="l00101"></a>00101 
<a name="l00102"></a>00102 
<a name="l00104"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a3e4295fa0a1e9a2f3eba8bdda1221565">00104</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00105"></a>00105   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a3e4295fa0a1e9a2f3eba8bdda1221565" title="Destructor.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00106"></a>00106 <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a3e4295fa0a1e9a2f3eba8bdda1221565" title="Destructor.">  ::~Matrix_TriangPacked</a>()
<a name="l00107"></a>00107   {
<a name="l00108"></a>00108 
<a name="l00109"></a>00109 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00110"></a>00110 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00111"></a>00111       {
<a name="l00112"></a>00112 <span class="preprocessor">#endif</span>
<a name="l00113"></a>00113 <span class="preprocessor"></span>
<a name="l00114"></a>00114         <span class="keywordflow">if</span> (this-&gt;data_ != NULL)
<a name="l00115"></a>00115           {
<a name="l00116"></a>00116             this-&gt;allocator_.deallocate(this-&gt;data_,
<a name="l00117"></a>00117                                         (this-&gt;m_ * (this-&gt;m_ + 1)) / 2);
<a name="l00118"></a>00118             this-&gt;data_ = NULL;
<a name="l00119"></a>00119           }
<a name="l00120"></a>00120 
<a name="l00121"></a>00121 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00122"></a>00122 <span class="preprocessor"></span>      }
<a name="l00123"></a>00123     <span class="keywordflow">catch</span> (...)
<a name="l00124"></a>00124       {
<a name="l00125"></a>00125         this-&gt;m_ = 0;
<a name="l00126"></a>00126         this-&gt;n_ = 0;
<a name="l00127"></a>00127         this-&gt;data_ = NULL;
<a name="l00128"></a>00128       }
<a name="l00129"></a>00129 <span class="preprocessor">#endif</span>
<a name="l00130"></a>00130 <span class="preprocessor"></span>
<a name="l00131"></a>00131   }
<a name="l00132"></a>00132 
<a name="l00133"></a>00133 
<a name="l00135"></a>00135 
<a name="l00139"></a>00139   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00140"></a>00140   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a6f0db73087115429868eaf0a35cf213b" title="Clears the matrix.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::Clear</a>()
<a name="l00141"></a>00141   {
<a name="l00142"></a>00142     this-&gt;<a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a3e4295fa0a1e9a2f3eba8bdda1221565" title="Destructor.">~Matrix_TriangPacked</a>();
<a name="l00143"></a>00143     this-&gt;m_ = 0;
<a name="l00144"></a>00144     this-&gt;n_ = 0;
<a name="l00145"></a>00145   }
<a name="l00146"></a>00146 
<a name="l00147"></a>00147 
<a name="l00148"></a>00148   <span class="comment">/*******************</span>
<a name="l00149"></a>00149 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00150"></a>00150 <span class="comment">   *******************/</span>
<a name="l00151"></a>00151 
<a name="l00152"></a>00152 
<a name="l00154"></a>00154 
<a name="l00157"></a>00157   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00158"></a>00158   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a9d331415858c046a3a72699d2bbd8ceb" title="Returns the number of elements stored in memory.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::GetDataSize</a>()<span class="keyword"> const</span>
<a name="l00159"></a>00159 <span class="keyword">  </span>{
<a name="l00160"></a>00160     <span class="keywordflow">return</span> (this-&gt;m_ * (this-&gt;m_ + 1)) / 2;
<a name="l00161"></a>00161   }
<a name="l00162"></a>00162 
<a name="l00163"></a>00163 
<a name="l00164"></a>00164   <span class="comment">/*********************</span>
<a name="l00165"></a>00165 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00166"></a>00166 <span class="comment">   *********************/</span>
<a name="l00167"></a>00167 
<a name="l00168"></a>00168 
<a name="l00170"></a>00170 
<a name="l00176"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ab0a798e9e9eac81e73e3176e8a4f20ac">00176</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00177"></a>00177   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ab0a798e9e9eac81e73e3176e8a4f20ac" title="Reallocates memory to resize the matrix.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00178"></a>00178 <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ab0a798e9e9eac81e73e3176e8a4f20ac" title="Reallocates memory to resize the matrix.">  ::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00179"></a>00179   {
<a name="l00180"></a>00180 
<a name="l00181"></a>00181     <span class="keywordflow">if</span> (i != this-&gt;m_)
<a name="l00182"></a>00182       {
<a name="l00183"></a>00183         this-&gt;m_ = i;
<a name="l00184"></a>00184         this-&gt;n_ = i;
<a name="l00185"></a>00185 
<a name="l00186"></a>00186 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00187"></a>00187 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l00188"></a>00188           {
<a name="l00189"></a>00189 <span class="preprocessor">#endif</span>
<a name="l00190"></a>00190 <span class="preprocessor"></span>
<a name="l00191"></a>00191             this-&gt;data_ =
<a name="l00192"></a>00192               <span class="keyword">reinterpret_cast&lt;</span>pointer<span class="keyword">&gt;</span>(this-&gt;allocator_.reallocate(this-&gt;data_,
<a name="l00193"></a>00193                                                                     (i*(i+1))/2,
<a name="l00194"></a>00194                                                                     <span class="keyword">this</span>) );
<a name="l00195"></a>00195 
<a name="l00196"></a>00196 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00197"></a>00197 <span class="preprocessor"></span>          }
<a name="l00198"></a>00198         <span class="keywordflow">catch</span> (...)
<a name="l00199"></a>00199           {
<a name="l00200"></a>00200             this-&gt;m_ = 0;
<a name="l00201"></a>00201             this-&gt;n_ = 0;
<a name="l00202"></a>00202             this-&gt;data_ = NULL;
<a name="l00203"></a>00203             <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::Reallocate(int, int)&quot;</span>,
<a name="l00204"></a>00204                            <span class="stringliteral">&quot;Unable to reallocate memory for data_.&quot;</span>);
<a name="l00205"></a>00205           }
<a name="l00206"></a>00206         <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00207"></a>00207           {
<a name="l00208"></a>00208             this-&gt;m_ = 0;
<a name="l00209"></a>00209             this-&gt;n_ = 0;
<a name="l00210"></a>00210             <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::Reallocate(int, int)&quot;</span>,
<a name="l00211"></a>00211                            <span class="stringliteral">&quot;Unable to reallocate memory for data_.&quot;</span>);
<a name="l00212"></a>00212           }
<a name="l00213"></a>00213 <span class="preprocessor">#endif</span>
<a name="l00214"></a>00214 <span class="preprocessor"></span>
<a name="l00215"></a>00215       }
<a name="l00216"></a>00216   }
<a name="l00217"></a>00217 
<a name="l00218"></a>00218 
<a name="l00221"></a>00221 
<a name="l00235"></a>00235   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00236"></a>00236   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00237"></a>00237 <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">  SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00238"></a>00238           <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00239"></a>00239           ::pointer data)
<a name="l00240"></a>00240   {
<a name="l00241"></a>00241     this-&gt;<a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a6f0db73087115429868eaf0a35cf213b" title="Clears the matrix.">Clear</a>();
<a name="l00242"></a>00242 
<a name="l00243"></a>00243     this-&gt;m_ = i;
<a name="l00244"></a>00244     this-&gt;n_ = i;
<a name="l00245"></a>00245 
<a name="l00246"></a>00246     this-&gt;data_ = data;
<a name="l00247"></a>00247   }
<a name="l00248"></a>00248 
<a name="l00249"></a>00249 
<a name="l00251"></a>00251 
<a name="l00255"></a>00255   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00256"></a>00256   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a9aa8d70640c7e035ca45aa406c8e6bd1" title="Clears the matrix without releasing memory.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::Nullify</a>()
<a name="l00257"></a>00257   {
<a name="l00258"></a>00258     this-&gt;data_ = NULL;
<a name="l00259"></a>00259     this-&gt;m_ = 0;
<a name="l00260"></a>00260     this-&gt;n_ = 0;
<a name="l00261"></a>00261   }
<a name="l00262"></a>00262 
<a name="l00263"></a>00263 
<a name="l00264"></a>00264   <span class="comment">/**********************************</span>
<a name="l00265"></a>00265 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l00266"></a>00266 <span class="comment">   **********************************/</span>
<a name="l00267"></a>00267 
<a name="l00268"></a>00268 
<a name="l00270"></a>00270 
<a name="l00276"></a>00276   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00277"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#aa72fab293552e87ee528d11b04d361bf">00277</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::value_type</a>
<a name="l00278"></a>00278   <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#aa72fab293552e87ee528d11b04d361bf" title="Access operator.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00279"></a>00279 <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#aa72fab293552e87ee528d11b04d361bf" title="Access operator.">  ::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00280"></a>00280 <span class="keyword">  </span>{
<a name="l00281"></a>00281 
<a name="l00282"></a>00282 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00283"></a>00283 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00284"></a>00284       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::operator()&quot;</span>,
<a name="l00285"></a>00285                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00286"></a>00286                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00287"></a>00287     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00288"></a>00288       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::operator()&quot;</span>,
<a name="l00289"></a>00289                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00290"></a>00290                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00291"></a>00291 <span class="preprocessor">#endif</span>
<a name="l00292"></a>00292 <span class="preprocessor"></span>
<a name="l00293"></a>00293     <span class="keywordflow">if</span> (Storage::UpLo())
<a name="l00294"></a>00294       <span class="keywordflow">if</span> (i &gt; j)
<a name="l00295"></a>00295         <span class="keywordflow">return</span> 0;
<a name="l00296"></a>00296       <span class="keywordflow">else</span>
<a name="l00297"></a>00297         <span class="keywordflow">return</span> this-&gt;data_[Storage::GetFirst(i * this-&gt;n_
<a name="l00298"></a>00298                                              - (i * (i + 1)) / 2 + j,
<a name="l00299"></a>00299                                              (j * (j + 1)) / 2 + i)];
<a name="l00300"></a>00300     <span class="keywordflow">else</span>
<a name="l00301"></a>00301       <span class="keywordflow">if</span> (i &lt; j)
<a name="l00302"></a>00302         <span class="keywordflow">return</span> 0;
<a name="l00303"></a>00303       <span class="keywordflow">else</span>
<a name="l00304"></a>00304         <span class="keywordflow">return</span> this-&gt;data_[Storage::GetFirst((i * (i + 1)) / 2 + j,
<a name="l00305"></a>00305                                              j * this-&gt;m_
<a name="l00306"></a>00306                                              - (j * (j + 1)) / 2 + i)];
<a name="l00307"></a>00307   }
<a name="l00308"></a>00308 
<a name="l00309"></a>00309 
<a name="l00311"></a>00311 
<a name="l00318"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a0d4ea94aa72456c633fd26385f28f7a2">00318</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00319"></a>00319   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00320"></a>00320   <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a0d4ea94aa72456c633fd26385f28f7a2" title="Direct access method.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00321"></a>00321   {
<a name="l00322"></a>00322 
<a name="l00323"></a>00323 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00324"></a>00324 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00325"></a>00325       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::Val(int, int)&quot;</span>,
<a name="l00326"></a>00326                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00327"></a>00327                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00328"></a>00328     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00329"></a>00329       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::Val(int, int)&quot;</span>,
<a name="l00330"></a>00330                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00331"></a>00331                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00332"></a>00332     <span class="keywordflow">if</span> (Storage::UpLo())
<a name="l00333"></a>00333       {
<a name="l00334"></a>00334         <span class="keywordflow">if</span> (i &gt; j)
<a name="l00335"></a>00335           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::Val(int, int)&quot;</span>,
<a name="l00336"></a>00336                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Attempted to access to element (&quot;</span>)
<a name="l00337"></a>00337                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;) but row&quot;</span>)
<a name="l00338"></a>00338                          + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; index should not be strictly more&quot;</span>)
<a name="l00339"></a>00339                          + <span class="stringliteral">&quot; than column index (upper triangular matrix).&quot;</span>);
<a name="l00340"></a>00340         <span class="keywordflow">return</span> this-&gt;data_[Storage::GetFirst(i * this-&gt;n_
<a name="l00341"></a>00341                                              - (i * (i + 1)) / 2 + j,
<a name="l00342"></a>00342                                              (j * (j + 1)) / 2 + i)];
<a name="l00343"></a>00343       }
<a name="l00344"></a>00344     <span class="keywordflow">else</span>
<a name="l00345"></a>00345       {
<a name="l00346"></a>00346         <span class="keywordflow">if</span> (j &gt; i)
<a name="l00347"></a>00347           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::Val(int, int)&quot;</span>,
<a name="l00348"></a>00348                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Attempted to access to element (&quot;</span>)
<a name="l00349"></a>00349                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;) but&quot;</span>)
<a name="l00350"></a>00350                          + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; column index should not be strictly more&quot;</span>)
<a name="l00351"></a>00351                          + <span class="stringliteral">&quot; than row index (lower triangular matrix).&quot;</span>);
<a name="l00352"></a>00352         <span class="keywordflow">return</span> this-&gt;data_[Storage::GetFirst((i * (i + 1)) / 2 + j,
<a name="l00353"></a>00353                                              j * this-&gt;m_
<a name="l00354"></a>00354                                              - (j * (j + 1)) / 2 + i)];
<a name="l00355"></a>00355       }
<a name="l00356"></a>00356 <span class="preprocessor">#endif</span>
<a name="l00357"></a>00357 <span class="preprocessor"></span>
<a name="l00358"></a>00358     <span class="keywordflow">if</span> (Storage::UpLo())
<a name="l00359"></a>00359       <span class="keywordflow">return</span> this-&gt;data_[Storage::GetFirst(i * this-&gt;n_
<a name="l00360"></a>00360                                            - (i * (i + 1)) / 2 + j,
<a name="l00361"></a>00361                                            (j * (j + 1)) / 2 + i)];
<a name="l00362"></a>00362     <span class="keywordflow">else</span>
<a name="l00363"></a>00363       <span class="keywordflow">return</span> this-&gt;data_[Storage::GetFirst((i * (i + 1)) / 2 + j,
<a name="l00364"></a>00364                                            j * this-&gt;m_
<a name="l00365"></a>00365                                            - (j * (j + 1)) / 2 + i)];
<a name="l00366"></a>00366   }
<a name="l00367"></a>00367 
<a name="l00368"></a>00368 
<a name="l00370"></a>00370 
<a name="l00377"></a>00377   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00378"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a2106123c9da877618fae231860846f6f">00378</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00379"></a>00379   ::const_reference
<a name="l00380"></a>00380   <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a0d4ea94aa72456c633fd26385f28f7a2" title="Direct access method.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00381"></a>00381 <span class="keyword">  </span>{
<a name="l00382"></a>00382 
<a name="l00383"></a>00383 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00384"></a>00384 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00385"></a>00385       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::Val(int, int) const&quot;</span>,
<a name="l00386"></a>00386                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00387"></a>00387                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00388"></a>00388     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00389"></a>00389       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::Val(int, int)&quot;</span>,
<a name="l00390"></a>00390                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00391"></a>00391                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00392"></a>00392     <span class="keywordflow">if</span> (Storage::UpLo())
<a name="l00393"></a>00393       {
<a name="l00394"></a>00394         <span class="keywordflow">if</span> (i &gt; j)
<a name="l00395"></a>00395           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::Val(int, int) const&quot;</span>,
<a name="l00396"></a>00396                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Attempted to access to element (&quot;</span>)
<a name="l00397"></a>00397                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;) but row&quot;</span>)
<a name="l00398"></a>00398                          + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; index should not be strictly more&quot;</span>)
<a name="l00399"></a>00399                          + <span class="stringliteral">&quot; than column index (upper triangular matrix).&quot;</span>);
<a name="l00400"></a>00400         <span class="keywordflow">return</span> this-&gt;data_[Storage::GetFirst(i * this-&gt;n_
<a name="l00401"></a>00401                                              - (i * (i + 1)) / 2 + j,
<a name="l00402"></a>00402                                              (j * (j + 1)) / 2 + i)];
<a name="l00403"></a>00403       }
<a name="l00404"></a>00404     <span class="keywordflow">else</span>
<a name="l00405"></a>00405       {
<a name="l00406"></a>00406         <span class="keywordflow">if</span> (j &gt; i)
<a name="l00407"></a>00407           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::Val(int, int) const&quot;</span>,
<a name="l00408"></a>00408                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Attempted to access to element (&quot;</span>)
<a name="l00409"></a>00409                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;) but&quot;</span>)
<a name="l00410"></a>00410                          + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; column index should not be strictly more&quot;</span>)
<a name="l00411"></a>00411                          + <span class="stringliteral">&quot; than row index (lower triangular matrix).&quot;</span>);
<a name="l00412"></a>00412         <span class="keywordflow">return</span> this-&gt;data_[Storage::GetFirst((i * (i + 1)) / 2 + j,
<a name="l00413"></a>00413                                              j * this-&gt;m_
<a name="l00414"></a>00414                                              - (j * (j + 1)) / 2 + i)];
<a name="l00415"></a>00415       }
<a name="l00416"></a>00416 <span class="preprocessor">#endif</span>
<a name="l00417"></a>00417 <span class="preprocessor"></span>
<a name="l00418"></a>00418     <span class="keywordflow">if</span> (Storage::UpLo())
<a name="l00419"></a>00419       <span class="keywordflow">return</span> this-&gt;data_[Storage::GetFirst(i * this-&gt;n_
<a name="l00420"></a>00420                                            - (i * (i + 1)) / 2 + j,
<a name="l00421"></a>00421                                            (j * (j + 1)) / 2 + i)];
<a name="l00422"></a>00422     <span class="keywordflow">else</span>
<a name="l00423"></a>00423       <span class="keywordflow">return</span> this-&gt;data_[Storage::GetFirst((i * (i + 1)) / 2 + j,
<a name="l00424"></a>00424                                            j * this-&gt;m_
<a name="l00425"></a>00425                                            - (j * (j + 1)) / 2 + i)];
<a name="l00426"></a>00426   }
<a name="l00427"></a>00427 
<a name="l00428"></a>00428 
<a name="l00430"></a>00430 
<a name="l00436"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#aa47392b447a0c718d29050d204d5d465">00436</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00437"></a>00437   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00438"></a>00438   <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#aa47392b447a0c718d29050d204d5d465" title="Returns the element (i, j).">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00439"></a>00439   {
<a name="l00440"></a>00440     <span class="keywordflow">return</span> this-&gt;<a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a0d4ea94aa72456c633fd26385f28f7a2" title="Direct access method.">Val</a>(i, j);
<a name="l00441"></a>00441   }
<a name="l00442"></a>00442 
<a name="l00443"></a>00443 
<a name="l00445"></a>00445 
<a name="l00451"></a>00451   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00452"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ae5f7cb3057f4315ee8a12e57223cf7b5">00452</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00453"></a>00453   ::const_reference
<a name="l00454"></a>00454   <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#aa47392b447a0c718d29050d204d5d465" title="Returns the element (i, j).">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::Get</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00455"></a>00455 <span class="keyword">  </span>{
<a name="l00456"></a>00456     <span class="keywordflow">return</span> this-&gt;<a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a0d4ea94aa72456c633fd26385f28f7a2" title="Direct access method.">Val</a>(i, j);
<a name="l00457"></a>00457   }
<a name="l00458"></a>00458 
<a name="l00459"></a>00459 
<a name="l00461"></a>00461 
<a name="l00466"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a086500c5914460971f1a7c4d48c5f5b4">00466</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00467"></a>00467   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00468"></a>00468   <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a086500c5914460971f1a7c4d48c5f5b4" title="Access to elements of the data array.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::operator[] </a>(<span class="keywordtype">int</span> i)
<a name="l00469"></a>00469   {
<a name="l00470"></a>00470 
<a name="l00471"></a>00471 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00472"></a>00472 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a9d331415858c046a3a72699d2bbd8ceb" title="Returns the number of elements stored in memory.">GetDataSize</a>())
<a name="l00473"></a>00473       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::operator[] (int)&quot;</span>,
<a name="l00474"></a>00474                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00475"></a>00475                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a9d331415858c046a3a72699d2bbd8ceb" title="Returns the number of elements stored in memory.">GetDataSize</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00476"></a>00476                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00477"></a>00477 <span class="preprocessor">#endif</span>
<a name="l00478"></a>00478 <span class="preprocessor"></span>
<a name="l00479"></a>00479     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00480"></a>00480   }
<a name="l00481"></a>00481 
<a name="l00482"></a>00482 
<a name="l00484"></a>00484 
<a name="l00489"></a>00489   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00490"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ade6cf6ee8f39e79ae4419b5a4851b32e">00490</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00491"></a>00491   ::const_reference
<a name="l00492"></a>00492   <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a086500c5914460971f1a7c4d48c5f5b4" title="Access to elements of the data array.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::operator[] </a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00493"></a>00493 <span class="keyword">  </span>{
<a name="l00494"></a>00494 
<a name="l00495"></a>00495 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00496"></a>00496 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a9d331415858c046a3a72699d2bbd8ceb" title="Returns the number of elements stored in memory.">GetDataSize</a>())
<a name="l00497"></a>00497       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::operator[] (int) const&quot;</span>,
<a name="l00498"></a>00498                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00499"></a>00499                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a9d331415858c046a3a72699d2bbd8ceb" title="Returns the number of elements stored in memory.">GetDataSize</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00500"></a>00500                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00501"></a>00501 <span class="preprocessor">#endif</span>
<a name="l00502"></a>00502 <span class="preprocessor"></span>
<a name="l00503"></a>00503     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00504"></a>00504   }
<a name="l00505"></a>00505 
<a name="l00506"></a>00506 
<a name="l00508"></a>00508 
<a name="l00513"></a>00513   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00514"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ac8f8519e62ace1acd0e0c514d080471a">00514</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00515"></a>00515   <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ac8f8519e62ace1acd0e0c514d080471a" title="Duplicates a matrix (assignment operator).">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00516"></a>00516 <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ac8f8519e62ace1acd0e0c514d080471a" title="Duplicates a matrix (assignment operator).">  operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00517"></a>00517   {
<a name="l00518"></a>00518     this-&gt;<a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ac3fcc459520d0ec52a599a8def09c4fc" title="Duplicates a matrix.">Copy</a>(A);
<a name="l00519"></a>00519 
<a name="l00520"></a>00520     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00521"></a>00521   }
<a name="l00522"></a>00522 
<a name="l00523"></a>00523 
<a name="l00525"></a>00525 
<a name="l00530"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a600ae1e3aaa10e9e1d4732d69a027ae1">00530</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00531"></a>00531   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a600ae1e3aaa10e9e1d4732d69a027ae1" title="Sets an element of the matrix.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00532"></a>00532 <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a600ae1e3aaa10e9e1d4732d69a027ae1" title="Sets an element of the matrix.">  ::Set</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; x)
<a name="l00533"></a>00533   {
<a name="l00534"></a>00534     this-&gt;Val(i, j) = x;
<a name="l00535"></a>00535   }
<a name="l00536"></a>00536 
<a name="l00537"></a>00537 
<a name="l00539"></a>00539 
<a name="l00544"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ac3fcc459520d0ec52a599a8def09c4fc">00544</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00545"></a>00545   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ac3fcc459520d0ec52a599a8def09c4fc" title="Duplicates a matrix.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00546"></a>00546 <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ac3fcc459520d0ec52a599a8def09c4fc" title="Duplicates a matrix.">  Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00547"></a>00547   {
<a name="l00548"></a>00548     this-&gt;<a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ab0a798e9e9eac81e73e3176e8a4f20ac" title="Reallocates memory to resize the matrix.">Reallocate</a>(A.<a class="code" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927" title="Returns the number of rows.">GetM</a>(), A.<a class="code" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984" title="Returns the number of columns.">GetN</a>());
<a name="l00549"></a>00549 
<a name="l00550"></a>00550     this-&gt;allocator_.memorycpy(this-&gt;data_, A.<a class="code" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a" title="Returns a pointer to the data array.">GetData</a>(), this-&gt;<a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a9d331415858c046a3a72699d2bbd8ceb" title="Returns the number of elements stored in memory.">GetDataSize</a>());
<a name="l00551"></a>00551   }
<a name="l00552"></a>00552 
<a name="l00553"></a>00553 
<a name="l00554"></a>00554   <span class="comment">/************************</span>
<a name="l00555"></a>00555 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l00556"></a>00556 <span class="comment">   ************************/</span>
<a name="l00557"></a>00557 
<a name="l00558"></a>00558 
<a name="l00560"></a>00560 
<a name="l00564"></a>00564   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00565"></a>00565   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a9944e918b0b65d7fc51c5b03ac772635" title="Sets all elements to zero.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::Zero</a>()
<a name="l00566"></a>00566   {
<a name="l00567"></a>00567     this-&gt;allocator_.memoryset(this-&gt;data_, <span class="keywordtype">char</span>(0),
<a name="l00568"></a>00568                                this-&gt;<a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a9d331415858c046a3a72699d2bbd8ceb" title="Returns the number of elements stored in memory.">GetDataSize</a>() * <span class="keyword">sizeof</span>(value_type));
<a name="l00569"></a>00569   }
<a name="l00570"></a>00570 
<a name="l00571"></a>00571 
<a name="l00573"></a>00573   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00574"></a>00574   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a888b8dc8faeb5654fda9a0517cea0c7b" title="Sets the matrix to the identity.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::SetIdentity</a>()
<a name="l00575"></a>00575   {
<a name="l00576"></a>00576     this-&gt;<a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a5e66e9e50e3ab69c85be605d69f287e3" title="Fills the matrix with 0, 1, 2, ...">Fill</a>(T(0));
<a name="l00577"></a>00577 
<a name="l00578"></a>00578     T one(1);
<a name="l00579"></a>00579     <span class="keywordtype">bool</span> storage_col = (Storage::GetFirst(1,0) == 0);
<a name="l00580"></a>00580     <span class="keywordflow">if</span> ((Storage::UpLo() &amp;&amp; storage_col)||(!Storage::UpLo() &amp;&amp; !storage_col))
<a name="l00581"></a>00581       {
<a name="l00582"></a>00582         <span class="keywordtype">int</span> index(-1);
<a name="l00583"></a>00583         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; min(this-&gt;m_, this-&gt;n_); i++)
<a name="l00584"></a>00584           {
<a name="l00585"></a>00585             index += i+1;
<a name="l00586"></a>00586             this-&gt;data_[index] = one;
<a name="l00587"></a>00587           }
<a name="l00588"></a>00588       }
<a name="l00589"></a>00589     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (Storage::UpLo() &amp;&amp; !storage_col)
<a name="l00590"></a>00590       {
<a name="l00591"></a>00591         <span class="keywordtype">int</span> index(0);
<a name="l00592"></a>00592         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; min(this-&gt;m_, this-&gt;n_); i++)
<a name="l00593"></a>00593           {
<a name="l00594"></a>00594             this-&gt;data_[index] = one;
<a name="l00595"></a>00595             index += this-&gt;m_-i;
<a name="l00596"></a>00596           }
<a name="l00597"></a>00597       }
<a name="l00598"></a>00598     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (!Storage::UpLo() &amp;&amp; storage_col)
<a name="l00599"></a>00599       {
<a name="l00600"></a>00600         <span class="keywordtype">int</span> index(0);
<a name="l00601"></a>00601         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; min(this-&gt;m_, this-&gt;n_); i++)
<a name="l00602"></a>00602           {
<a name="l00603"></a>00603             this-&gt;data_[index] = one;
<a name="l00604"></a>00604             index += this-&gt;n_-i;
<a name="l00605"></a>00605           }
<a name="l00606"></a>00606       }
<a name="l00607"></a>00607   }
<a name="l00608"></a>00608 
<a name="l00609"></a>00609 
<a name="l00611"></a>00611 
<a name="l00615"></a>00615   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00616"></a>00616   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a5e66e9e50e3ab69c85be605d69f287e3" title="Fills the matrix with 0, 1, 2, ...">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::Fill</a>()
<a name="l00617"></a>00617   {
<a name="l00618"></a>00618     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a9d331415858c046a3a72699d2bbd8ceb" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00619"></a>00619       this-&gt;data_[i] = i;
<a name="l00620"></a>00620   }
<a name="l00621"></a>00621 
<a name="l00622"></a>00622 
<a name="l00624"></a>00624 
<a name="l00627"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a1d5639ed65870562ab47684054d98e4a">00627</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00628"></a>00628   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00629"></a>00629   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a5e66e9e50e3ab69c85be605d69f287e3" title="Fills the matrix with 0, 1, 2, ...">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::Fill</a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00630"></a>00630   {
<a name="l00631"></a>00631     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a9d331415858c046a3a72699d2bbd8ceb" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00632"></a>00632       this-&gt;data_[i] = x;
<a name="l00633"></a>00633   }
<a name="l00634"></a>00634 
<a name="l00635"></a>00635 
<a name="l00637"></a>00637 
<a name="l00640"></a>00640   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00641"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#aef8d9506c12bb464828cf7967a6616f5">00641</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00642"></a>00642   <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00643"></a>00643   <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ac8f8519e62ace1acd0e0c514d080471a" title="Duplicates a matrix (assignment operator).">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00644"></a>00644   {
<a name="l00645"></a>00645     this-&gt;<a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a5e66e9e50e3ab69c85be605d69f287e3" title="Fills the matrix with 0, 1, 2, ...">Fill</a>(x);
<a name="l00646"></a>00646 
<a name="l00647"></a>00647     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00648"></a>00648   }
<a name="l00649"></a>00649 
<a name="l00650"></a>00650 
<a name="l00652"></a>00652 
<a name="l00655"></a>00655   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00656"></a>00656   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ae77c0cd9d596c959b2be5e9b78750e65" title="Fills the matrix randomly.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::FillRand</a>()
<a name="l00657"></a>00657   {
<a name="l00658"></a>00658     srand(time(NULL));
<a name="l00659"></a>00659     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a9d331415858c046a3a72699d2bbd8ceb" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00660"></a>00660       this-&gt;data_[i] = rand();
<a name="l00661"></a>00661   }
<a name="l00662"></a>00662 
<a name="l00663"></a>00663 
<a name="l00665"></a>00665 
<a name="l00670"></a>00670   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00671"></a>00671   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a931e21546d023c42baf9c2d6cfddb3ee" title="Displays the matrix on the standard output.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00672"></a>00672 <span class="keyword">  </span>{
<a name="l00673"></a>00673     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00674"></a>00674       {
<a name="l00675"></a>00675         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;n_; j++)
<a name="l00676"></a>00676           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00677"></a>00677         cout &lt;&lt; endl;
<a name="l00678"></a>00678       }
<a name="l00679"></a>00679   }
<a name="l00680"></a>00680 
<a name="l00681"></a>00681 
<a name="l00683"></a>00683 
<a name="l00694"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a79f239a5a5a6921e11811349ffacc5f7">00694</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00695"></a>00695   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a931e21546d023c42baf9c2d6cfddb3ee" title="Displays the matrix on the standard output.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00696"></a>00696 <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a931e21546d023c42baf9c2d6cfddb3ee" title="Displays the matrix on the standard output.">  ::Print</a>(<span class="keywordtype">int</span> a, <span class="keywordtype">int</span> b, <span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n)<span class="keyword"> const</span>
<a name="l00697"></a>00697 <span class="keyword">  </span>{
<a name="l00698"></a>00698     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = a; i &lt; min(this-&gt;m_, a + m); i++)
<a name="l00699"></a>00699       {
<a name="l00700"></a>00700         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = b; j &lt; min(this-&gt;n_, b + n); j++)
<a name="l00701"></a>00701           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00702"></a>00702         cout &lt;&lt; endl;
<a name="l00703"></a>00703       }
<a name="l00704"></a>00704   }
<a name="l00705"></a>00705 
<a name="l00706"></a>00706 
<a name="l00708"></a>00708 
<a name="l00716"></a>00716   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00717"></a>00717   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a931e21546d023c42baf9c2d6cfddb3ee" title="Displays the matrix on the standard output.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::Print</a>(<span class="keywordtype">int</span> l)<span class="keyword"> const</span>
<a name="l00718"></a>00718 <span class="keyword">  </span>{
<a name="l00719"></a>00719     <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a931e21546d023c42baf9c2d6cfddb3ee" title="Displays the matrix on the standard output.">Print</a>(0, 0, l, l);
<a name="l00720"></a>00720   }
<a name="l00721"></a>00721 
<a name="l00722"></a>00722 
<a name="l00723"></a>00723   <span class="comment">/**************************</span>
<a name="l00724"></a>00724 <span class="comment">   * INPUT/OUTPUT FUNCTIONS *</span>
<a name="l00725"></a>00725 <span class="comment">   **************************/</span>
<a name="l00726"></a>00726 
<a name="l00727"></a>00727 
<a name="l00729"></a>00729 
<a name="l00736"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ae5e50dae49d4cc0a56532151206184c8">00736</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00737"></a>00737   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ae5e50dae49d4cc0a56532151206184c8" title="Writes the matrix in a file.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00738"></a>00738 <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ae5e50dae49d4cc0a56532151206184c8" title="Writes the matrix in a file.">  ::Write</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00739"></a>00739 <span class="keyword">  </span>{
<a name="l00740"></a>00740     ofstream FileStream;
<a name="l00741"></a>00741     FileStream.open(FileName.c_str(), ofstream::binary);
<a name="l00742"></a>00742 
<a name="l00743"></a>00743 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00744"></a>00744 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00745"></a>00745     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00746"></a>00746       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::Write(string FileName)&quot;</span>,
<a name="l00747"></a>00747                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00748"></a>00748 <span class="preprocessor">#endif</span>
<a name="l00749"></a>00749 <span class="preprocessor"></span>
<a name="l00750"></a>00750     this-&gt;Write(FileStream);
<a name="l00751"></a>00751 
<a name="l00752"></a>00752     FileStream.close();
<a name="l00753"></a>00753   }
<a name="l00754"></a>00754 
<a name="l00755"></a>00755 
<a name="l00757"></a>00757 
<a name="l00764"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#aa060ab9a7f68cb565019133802cb648e">00764</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00765"></a>00765   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ae5e50dae49d4cc0a56532151206184c8" title="Writes the matrix in a file.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00766"></a>00766 <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ae5e50dae49d4cc0a56532151206184c8" title="Writes the matrix in a file.">  ::Write</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00767"></a>00767 <span class="keyword">  </span>{
<a name="l00768"></a>00768 
<a name="l00769"></a>00769 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00770"></a>00770 <span class="preprocessor"></span>    <span class="comment">// Checks if the file is ready.</span>
<a name="l00771"></a>00771     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00772"></a>00772       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l00773"></a>00773                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00774"></a>00774 <span class="preprocessor">#endif</span>
<a name="l00775"></a>00775 <span class="preprocessor"></span>
<a name="l00776"></a>00776     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l00777"></a>00777                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00778"></a>00778     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;n_)),
<a name="l00779"></a>00779                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00780"></a>00780 
<a name="l00781"></a>00781     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l00782"></a>00782                      this-&gt;GetDataSize() * <span class="keyword">sizeof</span>(value_type));
<a name="l00783"></a>00783 
<a name="l00784"></a>00784 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00785"></a>00785 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00786"></a>00786     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00787"></a>00787       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l00788"></a>00788                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Output operation failed.&quot;</span>)
<a name="l00789"></a>00789                     + string(<span class="stringliteral">&quot; The output file may have been removed&quot;</span>)
<a name="l00790"></a>00790                     + <span class="stringliteral">&quot; or there is no space left on device.&quot;</span>);
<a name="l00791"></a>00791 <span class="preprocessor">#endif</span>
<a name="l00792"></a>00792 <span class="preprocessor"></span>
<a name="l00793"></a>00793   }
<a name="l00794"></a>00794 
<a name="l00795"></a>00795 
<a name="l00797"></a>00797 
<a name="l00804"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ad2d7a4bde7fea4f6dbbaabb4adb8c21c">00804</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00805"></a>00805   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ad2d7a4bde7fea4f6dbbaabb4adb8c21c" title="Writes the matrix in a file.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00806"></a>00806 <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ad2d7a4bde7fea4f6dbbaabb4adb8c21c" title="Writes the matrix in a file.">  ::WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00807"></a>00807 <span class="keyword">  </span>{
<a name="l00808"></a>00808     ofstream FileStream;
<a name="l00809"></a>00809     FileStream.precision(cout.precision());
<a name="l00810"></a>00810     FileStream.flags(cout.flags());
<a name="l00811"></a>00811     FileStream.open(FileName.c_str());
<a name="l00812"></a>00812 
<a name="l00813"></a>00813 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00814"></a>00814 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00815"></a>00815     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00816"></a>00816       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::WriteText(string FileName)&quot;</span>,
<a name="l00817"></a>00817                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00818"></a>00818 <span class="preprocessor">#endif</span>
<a name="l00819"></a>00819 <span class="preprocessor"></span>
<a name="l00820"></a>00820     this-&gt;WriteText(FileStream);
<a name="l00821"></a>00821 
<a name="l00822"></a>00822     FileStream.close();
<a name="l00823"></a>00823   }
<a name="l00824"></a>00824 
<a name="l00825"></a>00825 
<a name="l00827"></a>00827 
<a name="l00834"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ae03952200cc98d597c9bc07e91500f70">00834</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00835"></a>00835   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ad2d7a4bde7fea4f6dbbaabb4adb8c21c" title="Writes the matrix in a file.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00836"></a>00836 <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ad2d7a4bde7fea4f6dbbaabb4adb8c21c" title="Writes the matrix in a file.">  ::WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00837"></a>00837 <span class="keyword">  </span>{
<a name="l00838"></a>00838 
<a name="l00839"></a>00839 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00840"></a>00840 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00841"></a>00841     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00842"></a>00842       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::WriteText(ofstream&amp; FileStream)&quot;</span>,
<a name="l00843"></a>00843                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00844"></a>00844 <span class="preprocessor">#endif</span>
<a name="l00845"></a>00845 <span class="preprocessor"></span>
<a name="l00846"></a>00846     <span class="keywordtype">int</span> i, j;
<a name="l00847"></a>00847     <span class="keywordflow">for</span> (i = 0; i &lt; this-&gt;GetM(); i++)
<a name="l00848"></a>00848       {
<a name="l00849"></a>00849         <span class="keywordflow">for</span> (j = 0; j &lt; this-&gt;GetN(); j++)
<a name="l00850"></a>00850           FileStream &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="charliteral">&#39;\t&#39;</span>;
<a name="l00851"></a>00851         FileStream &lt;&lt; endl;
<a name="l00852"></a>00852       }
<a name="l00853"></a>00853 
<a name="l00854"></a>00854 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00855"></a>00855 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00856"></a>00856     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00857"></a>00857       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::WriteText(ofstream&amp; FileStream)&quot;</span>,
<a name="l00858"></a>00858                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Output operation failed.&quot;</span>)
<a name="l00859"></a>00859                     + string(<span class="stringliteral">&quot; The output file may have been removed&quot;</span>)
<a name="l00860"></a>00860                     + <span class="stringliteral">&quot; or there is no space left on device.&quot;</span>);
<a name="l00861"></a>00861 <span class="preprocessor">#endif</span>
<a name="l00862"></a>00862 <span class="preprocessor"></span>
<a name="l00863"></a>00863   }
<a name="l00864"></a>00864 
<a name="l00865"></a>00865 
<a name="l00867"></a>00867 
<a name="l00874"></a>00874   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00875"></a>00875   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a96f1f584b5d04f9b6c92647c1dc2b353" title="Reads the matrix from a file.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::Read</a>(<span class="keywordtype">string</span> FileName)
<a name="l00876"></a>00876   {
<a name="l00877"></a>00877     ifstream FileStream;
<a name="l00878"></a>00878     FileStream.open(FileName.c_str(), ifstream::binary);
<a name="l00879"></a>00879 
<a name="l00880"></a>00880 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00881"></a>00881 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00882"></a>00882     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00883"></a>00883       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::Read(string FileName)&quot;</span>,
<a name="l00884"></a>00884                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00885"></a>00885 <span class="preprocessor">#endif</span>
<a name="l00886"></a>00886 <span class="preprocessor"></span>
<a name="l00887"></a>00887     this-&gt;<a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a96f1f584b5d04f9b6c92647c1dc2b353" title="Reads the matrix from a file.">Read</a>(FileStream);
<a name="l00888"></a>00888 
<a name="l00889"></a>00889     FileStream.close();
<a name="l00890"></a>00890   }
<a name="l00891"></a>00891 
<a name="l00892"></a>00892 
<a name="l00894"></a>00894 
<a name="l00901"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a2d646eec576003a589b1abff16a9f56a">00901</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00902"></a>00902   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a96f1f584b5d04f9b6c92647c1dc2b353" title="Reads the matrix from a file.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00903"></a>00903 <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a96f1f584b5d04f9b6c92647c1dc2b353" title="Reads the matrix from a file.">  ::Read</a>(istream&amp; FileStream)
<a name="l00904"></a>00904   {
<a name="l00905"></a>00905 
<a name="l00906"></a>00906 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00907"></a>00907 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00908"></a>00908     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00909"></a>00909       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::Read(ifstream&amp; FileStream)&quot;</span>,
<a name="l00910"></a>00910                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00911"></a>00911 <span class="preprocessor">#endif</span>
<a name="l00912"></a>00912 <span class="preprocessor"></span>
<a name="l00913"></a>00913     <span class="keywordtype">int</span> new_m, new_n;
<a name="l00914"></a>00914     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;new_m), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00915"></a>00915     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;new_n), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00916"></a>00916     this-&gt;Reallocate(new_m, new_n);
<a name="l00917"></a>00917 
<a name="l00918"></a>00918     FileStream.read(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l00919"></a>00919                     this-&gt;GetDataSize() * <span class="keyword">sizeof</span>(value_type));
<a name="l00920"></a>00920 
<a name="l00921"></a>00921 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00922"></a>00922 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l00923"></a>00923     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00924"></a>00924       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_TriangPacked::Read(ifstream&amp; FileStream)&quot;</span>,
<a name="l00925"></a>00925                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Input operation failed.&quot;</span>)
<a name="l00926"></a>00926                     + string(<span class="stringliteral">&quot; The input file may have been removed&quot;</span>)
<a name="l00927"></a>00927                     + <span class="stringliteral">&quot; or may not contain enough data.&quot;</span>);
<a name="l00928"></a>00928 <span class="preprocessor">#endif</span>
<a name="l00929"></a>00929 <span class="preprocessor"></span>
<a name="l00930"></a>00930   }
<a name="l00931"></a>00931 
<a name="l00932"></a>00932 
<a name="l00934"></a>00934 
<a name="l00938"></a>00938   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00939"></a>00939   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ae204d32dd619e2241945cc3bfbebc018" title="Reads the matrix from a file.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;::ReadText</a>(<span class="keywordtype">string</span> FileName)
<a name="l00940"></a>00940   {
<a name="l00941"></a>00941     ifstream FileStream;
<a name="l00942"></a>00942     FileStream.open(FileName.c_str());
<a name="l00943"></a>00943 
<a name="l00944"></a>00944 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00945"></a>00945 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00946"></a>00946     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00947"></a>00947       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(string FileName)&quot;</span>,
<a name="l00948"></a>00948                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00949"></a>00949 <span class="preprocessor">#endif</span>
<a name="l00950"></a>00950 <span class="preprocessor"></span>
<a name="l00951"></a>00951     this-&gt;<a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ae204d32dd619e2241945cc3bfbebc018" title="Reads the matrix from a file.">ReadText</a>(FileStream);
<a name="l00952"></a>00952 
<a name="l00953"></a>00953     FileStream.close();
<a name="l00954"></a>00954   }
<a name="l00955"></a>00955 
<a name="l00956"></a>00956 
<a name="l00958"></a>00958 
<a name="l00962"></a><a class="code" href="class_seldon_1_1_matrix___triang_packed.php#a2dd37f8c98e4d010227e800a414708d9">00962</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00963"></a>00963   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ae204d32dd619e2241945cc3bfbebc018" title="Reads the matrix from a file.">Matrix_TriangPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00964"></a>00964 <a class="code" href="class_seldon_1_1_matrix___triang_packed.php#ae204d32dd619e2241945cc3bfbebc018" title="Reads the matrix from a file.">  ::ReadText</a>(istream&amp; FileStream)
<a name="l00965"></a>00965   {
<a name="l00966"></a>00966     <span class="comment">// clears previous matrix</span>
<a name="l00967"></a>00967     Clear();
<a name="l00968"></a>00968 
<a name="l00969"></a>00969 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00970"></a>00970 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00971"></a>00971     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00972"></a>00972       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(ifstream&amp; FileStream)&quot;</span>,
<a name="l00973"></a>00973                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00974"></a>00974 <span class="preprocessor">#endif</span>
<a name="l00975"></a>00975 <span class="preprocessor"></span>
<a name="l00976"></a>00976     <span class="comment">// we read first line</span>
<a name="l00977"></a>00977     <span class="keywordtype">string</span> line;
<a name="l00978"></a>00978     getline(FileStream, line);
<a name="l00979"></a>00979 
<a name="l00980"></a>00980     <span class="keywordflow">if</span> (FileStream.fail())
<a name="l00981"></a>00981       {
<a name="l00982"></a>00982         <span class="comment">// empty file ?</span>
<a name="l00983"></a>00983         <span class="keywordflow">return</span>;
<a name="l00984"></a>00984       }
<a name="l00985"></a>00985 
<a name="l00986"></a>00986     <span class="comment">// converting first line into a vector</span>
<a name="l00987"></a>00987     istringstream line_stream(line);
<a name="l00988"></a>00988     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> first_row;
<a name="l00989"></a>00989     first_row.ReadText(line_stream);
<a name="l00990"></a>00990 
<a name="l00991"></a>00991     <span class="comment">// and now the other rows</span>
<a name="l00992"></a>00992     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> other_rows;
<a name="l00993"></a>00993     other_rows.ReadText(FileStream);
<a name="l00994"></a>00994 
<a name="l00995"></a>00995     <span class="comment">// number of rows and columns</span>
<a name="l00996"></a>00996     <span class="keywordtype">int</span> n = first_row.GetM();
<a name="l00997"></a>00997     <span class="keywordtype">int</span> m = 1 + other_rows.GetM()/n;
<a name="l00998"></a>00998 
<a name="l00999"></a>00999 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01000"></a>01000 <span class="preprocessor"></span>    <span class="comment">// Checking number of elements</span>
<a name="l01001"></a>01001     <span class="keywordflow">if</span> (other_rows.GetM() != (m-1)*n)
<a name="l01002"></a>01002       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(ifstream&amp; FileStream)&quot;</span>,
<a name="l01003"></a>01003                     <span class="stringliteral">&quot;The file should contain same number of columns.&quot;</span>);
<a name="l01004"></a>01004 <span class="preprocessor">#endif</span>
<a name="l01005"></a>01005 <span class="preprocessor"></span>
<a name="l01006"></a>01006     this-&gt;Reallocate(m,n);
<a name="l01007"></a>01007     <span class="comment">// filling matrix</span>
<a name="l01008"></a>01008     <span class="keywordflow">if</span> (Storage::UpLo())
<a name="l01009"></a>01009       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l01010"></a>01010         this-&gt;Val(0, j) = first_row(j);
<a name="l01011"></a>01011     <span class="keywordflow">else</span>
<a name="l01012"></a>01012       this-&gt;Val(0, 0) = first_row(0);
<a name="l01013"></a>01013 
<a name="l01014"></a>01014     <span class="keywordtype">int</span> nb = 0;
<a name="l01015"></a>01015     <span class="keywordflow">if</span> (Storage::UpLo())
<a name="l01016"></a>01016       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 1; i &lt; m; i++)
<a name="l01017"></a>01017         {
<a name="l01018"></a>01018           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; i; j++)
<a name="l01019"></a>01019             nb++;
<a name="l01020"></a>01020 
<a name="l01021"></a>01021           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = i; j &lt; n; j++)
<a name="l01022"></a>01022             this-&gt;Val(i, j) = other_rows(nb++);
<a name="l01023"></a>01023         }
<a name="l01024"></a>01024     <span class="keywordflow">else</span>
<a name="l01025"></a>01025       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 1; i &lt; m; i++)
<a name="l01026"></a>01026         {
<a name="l01027"></a>01027           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt;= i; j++)
<a name="l01028"></a>01028             this-&gt;Val(i, j) = other_rows(nb++);
<a name="l01029"></a>01029 
<a name="l01030"></a>01030           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = i+1; j &lt; n; j++)
<a name="l01031"></a>01031             nb++;
<a name="l01032"></a>01032         }
<a name="l01033"></a>01033   }
<a name="l01034"></a>01034 
<a name="l01035"></a>01035 
<a name="l01036"></a>01036 
<a name="l01038"></a>01038   <span class="comment">// MATRIX&lt;COLUPTRIANGPACKED&gt; //</span>
<a name="l01040"></a>01040 <span class="comment"></span>
<a name="l01041"></a>01041 
<a name="l01042"></a>01042   <span class="comment">/****************</span>
<a name="l01043"></a>01043 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01044"></a>01044 <span class="comment">   ****************/</span>
<a name="l01045"></a>01045 
<a name="l01046"></a>01046 
<a name="l01048"></a>01048 
<a name="l01051"></a>01051   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01052"></a>01052   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColUpTriangPacked, Allocator&gt;::Matrix</a>():
<a name="l01053"></a>01053     <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_up_triang_packed.php">ColUpTriangPacked</a>, Allocator&gt;()
<a name="l01054"></a>01054   {
<a name="l01055"></a>01055   }
<a name="l01056"></a>01056 
<a name="l01057"></a>01057 
<a name="l01059"></a>01059 
<a name="l01064"></a>01064   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01065"></a>01065   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_packed_00_01_allocator_01_4.php#aa374b37c0c5f9a8d9d2931c06f28fe9e" title="Default constructor.">Matrix&lt;T, Prop, ColUpTriangPacked, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01066"></a>01066     Matrix_TriangPacked&lt;T, Prop, ColUpTriangPacked, Allocator&gt;(i, i)
<a name="l01067"></a>01067   {
<a name="l01068"></a>01068   }
<a name="l01069"></a>01069 
<a name="l01070"></a>01070 
<a name="l01071"></a>01071   <span class="comment">/*****************</span>
<a name="l01072"></a>01072 <span class="comment">   * OTHER METHODS *</span>
<a name="l01073"></a>01073 <span class="comment">   *****************/</span>
<a name="l01074"></a>01074 
<a name="l01075"></a>01075 
<a name="l01077"></a>01077 
<a name="l01084"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_packed_00_01_allocator_01_4.php#ab1ba02f7e0a06777d9f1fa40b173b376">01084</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01085"></a>01085   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColUpTriangPacked, Allocator&gt;</a>
<a name="l01086"></a>01086 <a class="code" href="class_seldon_1_1_matrix.php">  ::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01087"></a>01087   {
<a name="l01088"></a>01088 
<a name="l01089"></a>01089     <span class="comment">// Storing the old values of the matrix.</span>
<a name="l01090"></a>01090     <span class="keywordtype">int</span> nold = this-&gt;GetDataSize();
<a name="l01091"></a>01091     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> xold(nold);
<a name="l01092"></a>01092     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; nold; k++)
<a name="l01093"></a>01093       xold(k) = this-&gt;data_[k];
<a name="l01094"></a>01094 
<a name="l01095"></a>01095     <span class="comment">// Reallocation.</span>
<a name="l01096"></a>01096     this-&gt;Reallocate(i, j);
<a name="l01097"></a>01097 
<a name="l01098"></a>01098     <span class="comment">// Filling the matrix with its old values.</span>
<a name="l01099"></a>01099     <span class="keywordtype">int</span> nmin = min(nold, this-&gt;GetDataSize());
<a name="l01100"></a>01100     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; nmin; k++)
<a name="l01101"></a>01101       this-&gt;data_[k] = xold(k);
<a name="l01102"></a>01102   }
<a name="l01103"></a>01103 
<a name="l01104"></a>01104 
<a name="l01106"></a>01106 
<a name="l01109"></a>01109   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01110"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_packed_00_01_allocator_01_4.php#abf68ccb2ad56edf5e5332a88677635e9">01110</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01111"></a>01111   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_packed_00_01_allocator_01_4.php" title="Column-major upper-triangular packed matrix class.">Matrix&lt;T, Prop, ColUpTriangPacked, Allocator&gt;</a>&amp;
<a name="l01112"></a>01112   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColUpTriangPacked, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01113"></a>01113   {
<a name="l01114"></a>01114     this-&gt;Fill(x);
<a name="l01115"></a>01115 
<a name="l01116"></a>01116     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01117"></a>01117   }
<a name="l01118"></a>01118 
<a name="l01119"></a>01119 
<a name="l01121"></a>01121 
<a name="l01126"></a>01126   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01127"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_packed_00_01_allocator_01_4.php#a67db3598aac6aadedf6fc31dd2b09766">01127</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_packed_00_01_allocator_01_4.php" title="Column-major upper-triangular packed matrix class.">Matrix&lt;T, Prop, ColUpTriangPacked, Allocator&gt;</a>&amp;
<a name="l01128"></a>01128   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColUpTriangPacked, Allocator&gt;::operator</a>=
<a name="l01129"></a>01129   (<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_packed_00_01_allocator_01_4.php" title="Column-major upper-triangular packed matrix class.">Matrix&lt;T, Prop, ColUpTriangPacked, Allocator&gt;</a>&amp; A)
<a name="l01130"></a>01130   {
<a name="l01131"></a>01131     this-&gt;Copy(A);
<a name="l01132"></a>01132     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01133"></a>01133   }
<a name="l01134"></a>01134 
<a name="l01135"></a>01135 
<a name="l01137"></a>01137 
<a name="l01140"></a>01140   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01141"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_packed_00_01_allocator_01_4.php#a5e92bfa8836941382f706138d3dd91e7">01141</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01142"></a>01142   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_packed_00_01_allocator_01_4.php" title="Column-major upper-triangular packed matrix class.">Matrix&lt;T, Prop, ColUpTriangPacked, Allocator&gt;</a>&amp;
<a name="l01143"></a>01143   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColUpTriangPacked, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01144"></a>01144   {
<a name="l01145"></a>01145     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetDataSize();i++)
<a name="l01146"></a>01146       this-&gt;data_[i] *= x;
<a name="l01147"></a>01147 
<a name="l01148"></a>01148     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01149"></a>01149   }
<a name="l01150"></a>01150 
<a name="l01151"></a>01151 
<a name="l01152"></a>01152 
<a name="l01154"></a>01154   <span class="comment">// MATRIX&lt;COLLOTRIANGPACKED&gt; //</span>
<a name="l01156"></a>01156 <span class="comment"></span>
<a name="l01157"></a>01157 
<a name="l01158"></a>01158   <span class="comment">/****************</span>
<a name="l01159"></a>01159 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01160"></a>01160 <span class="comment">   ****************/</span>
<a name="l01161"></a>01161 
<a name="l01162"></a>01162 
<a name="l01164"></a>01164 
<a name="l01167"></a>01167   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01168"></a>01168   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColLoTriangPacked, Allocator&gt;::Matrix</a>():
<a name="l01169"></a>01169     <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_lo_triang_packed.php">ColLoTriangPacked</a>, Allocator&gt;()
<a name="l01170"></a>01170   {
<a name="l01171"></a>01171   }
<a name="l01172"></a>01172 
<a name="l01173"></a>01173 
<a name="l01175"></a>01175 
<a name="l01180"></a>01180   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01181"></a>01181   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_packed_00_01_allocator_01_4.php#a6dd1142ad7e321f76ccdc3fc1247159f" title="Default constructor.">Matrix&lt;T, Prop, ColLoTriangPacked, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01182"></a>01182     Matrix_TriangPacked&lt;T, Prop, ColLoTriangPacked, Allocator&gt;(i, i)
<a name="l01183"></a>01183   {
<a name="l01184"></a>01184   }
<a name="l01185"></a>01185 
<a name="l01186"></a>01186 
<a name="l01187"></a>01187   <span class="comment">/*****************</span>
<a name="l01188"></a>01188 <span class="comment">   * OTHER METHODS *</span>
<a name="l01189"></a>01189 <span class="comment">   *****************/</span>
<a name="l01190"></a>01190 
<a name="l01191"></a>01191 
<a name="l01193"></a>01193 
<a name="l01200"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_packed_00_01_allocator_01_4.php#ae2cbe0fdaa637692a6858648cadcb761">01200</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01201"></a>01201   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColLoTriangPacked, Allocator&gt;</a>
<a name="l01202"></a>01202 <a class="code" href="class_seldon_1_1_matrix.php">  ::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01203"></a>01203   {
<a name="l01204"></a>01204 
<a name="l01205"></a>01205     <span class="comment">// Storing the old values of the matrix.</span>
<a name="l01206"></a>01206     <span class="keywordtype">int</span> nold = this-&gt;GetDataSize(), iold = this-&gt;m_;
<a name="l01207"></a>01207     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> xold(nold);
<a name="l01208"></a>01208     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; nold; k++)
<a name="l01209"></a>01209       xold(k) = this-&gt;data_[k];
<a name="l01210"></a>01210 
<a name="l01211"></a>01211     <span class="comment">// Reallocation.</span>
<a name="l01212"></a>01212     this-&gt;Reallocate(i, j);
<a name="l01213"></a>01213 
<a name="l01214"></a>01214     <span class="comment">// Filling the matrix with its old values.</span>
<a name="l01215"></a>01215     <span class="keywordtype">int</span> imin = min(iold, i);
<a name="l01216"></a>01216     nold = 0;
<a name="l01217"></a>01217     <span class="keywordtype">int</span> n = 0;
<a name="l01218"></a>01218     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; imin; k++)
<a name="l01219"></a>01219       {
<a name="l01220"></a>01220         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> l = k; l &lt; imin; l++)
<a name="l01221"></a>01221           this-&gt;data_[n+l-k] = xold(nold+l-k);
<a name="l01222"></a>01222 
<a name="l01223"></a>01223         n += i - k;
<a name="l01224"></a>01224         nold += iold - k;
<a name="l01225"></a>01225       }
<a name="l01226"></a>01226   }
<a name="l01227"></a>01227 
<a name="l01228"></a>01228 
<a name="l01230"></a>01230 
<a name="l01233"></a>01233   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01234"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_packed_00_01_allocator_01_4.php#aa2ac6a90200f30d3f7efe7c4592bb95e">01234</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01235"></a>01235   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_packed_00_01_allocator_01_4.php" title="Column-major lower-triangular packed matrix class.">Matrix&lt;T, Prop, ColLoTriangPacked, Allocator&gt;</a>&amp;
<a name="l01236"></a>01236   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColLoTriangPacked, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01237"></a>01237   {
<a name="l01238"></a>01238     this-&gt;Fill(x);
<a name="l01239"></a>01239 
<a name="l01240"></a>01240     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01241"></a>01241   }
<a name="l01242"></a>01242 
<a name="l01243"></a>01243 
<a name="l01245"></a>01245 
<a name="l01250"></a>01250   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01251"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_packed_00_01_allocator_01_4.php#a070c25f61560b1ee16d1c74971dcc87b">01251</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_packed_00_01_allocator_01_4.php" title="Column-major lower-triangular packed matrix class.">Matrix&lt;T, Prop, ColLoTriangPacked, Allocator&gt;</a>&amp;
<a name="l01252"></a>01252   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColLoTriangPacked, Allocator&gt;::operator</a>=
<a name="l01253"></a>01253   (<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_packed_00_01_allocator_01_4.php" title="Column-major lower-triangular packed matrix class.">Matrix&lt;T, Prop, ColLoTriangPacked, Allocator&gt;</a>&amp; A)
<a name="l01254"></a>01254   {
<a name="l01255"></a>01255     this-&gt;Copy(A);
<a name="l01256"></a>01256     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01257"></a>01257   }
<a name="l01258"></a>01258 
<a name="l01259"></a>01259 
<a name="l01261"></a>01261 
<a name="l01264"></a>01264   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01265"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_packed_00_01_allocator_01_4.php#a078e3eb8345ce3294d5d3a40dab61e1d">01265</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01266"></a>01266   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_packed_00_01_allocator_01_4.php" title="Column-major lower-triangular packed matrix class.">Matrix&lt;T, Prop, ColLoTriangPacked, Allocator&gt;</a>&amp;
<a name="l01267"></a>01267   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColLoTriangPacked, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01268"></a>01268   {
<a name="l01269"></a>01269     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetDataSize();i++)
<a name="l01270"></a>01270       this-&gt;data_[i] *= x;
<a name="l01271"></a>01271 
<a name="l01272"></a>01272     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01273"></a>01273   }
<a name="l01274"></a>01274 
<a name="l01275"></a>01275 
<a name="l01276"></a>01276 
<a name="l01278"></a>01278   <span class="comment">// MATRIX&lt;ROWUPTRIANGPACKED&gt; //</span>
<a name="l01280"></a>01280 <span class="comment"></span>
<a name="l01281"></a>01281 
<a name="l01282"></a>01282   <span class="comment">/****************</span>
<a name="l01283"></a>01283 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01284"></a>01284 <span class="comment">   ****************/</span>
<a name="l01285"></a>01285 
<a name="l01286"></a>01286 
<a name="l01288"></a>01288 
<a name="l01291"></a>01291   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01292"></a>01292   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowUpTriangPacked, Allocator&gt;::Matrix</a>():
<a name="l01293"></a>01293     <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_up_triang_packed.php">RowUpTriangPacked</a>, Allocator&gt;()
<a name="l01294"></a>01294   {
<a name="l01295"></a>01295   }
<a name="l01296"></a>01296 
<a name="l01297"></a>01297 
<a name="l01299"></a>01299 
<a name="l01304"></a>01304   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01305"></a>01305   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_packed_00_01_allocator_01_4.php#abeb10c437eb184d23f0e32210dc20498" title="Default constructor.">Matrix&lt;T, Prop, RowUpTriangPacked, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01306"></a>01306     Matrix_TriangPacked&lt;T, Prop, RowUpTriangPacked, Allocator&gt;(i, i)
<a name="l01307"></a>01307   {
<a name="l01308"></a>01308   }
<a name="l01309"></a>01309 
<a name="l01310"></a>01310 
<a name="l01311"></a>01311   <span class="comment">/*****************</span>
<a name="l01312"></a>01312 <span class="comment">   * OTHER METHODS *</span>
<a name="l01313"></a>01313 <span class="comment">   *****************/</span>
<a name="l01314"></a>01314 
<a name="l01315"></a>01315 
<a name="l01317"></a>01317 
<a name="l01324"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_packed_00_01_allocator_01_4.php#a9a9e04477cd9fb0bcded3aa2f5708fed">01324</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01325"></a>01325   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowUpTriangPacked, Allocator&gt;</a>
<a name="l01326"></a>01326 <a class="code" href="class_seldon_1_1_matrix.php">  ::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01327"></a>01327   {
<a name="l01328"></a>01328 
<a name="l01329"></a>01329     <span class="comment">// Storing the old values of the matrix.</span>
<a name="l01330"></a>01330     <span class="keywordtype">int</span> nold = this-&gt;GetDataSize(), iold = this-&gt;m_;
<a name="l01331"></a>01331     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> xold(nold);
<a name="l01332"></a>01332     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; nold; k++)
<a name="l01333"></a>01333       xold(k) = this-&gt;data_[k];
<a name="l01334"></a>01334 
<a name="l01335"></a>01335     <span class="comment">// Reallocation.</span>
<a name="l01336"></a>01336     this-&gt;Reallocate(i, j);
<a name="l01337"></a>01337 
<a name="l01338"></a>01338     <span class="comment">// Filling the matrix with its old values.</span>
<a name="l01339"></a>01339     <span class="keywordtype">int</span> imin = min(iold, i);
<a name="l01340"></a>01340     nold = 0;
<a name="l01341"></a>01341     <span class="keywordtype">int</span> n = 0;
<a name="l01342"></a>01342     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; imin; k++)
<a name="l01343"></a>01343       {
<a name="l01344"></a>01344         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> l = k; l &lt; imin; l++)
<a name="l01345"></a>01345           this-&gt;data_[n+l-k] = xold(nold+l-k);
<a name="l01346"></a>01346 
<a name="l01347"></a>01347         n += i - k;
<a name="l01348"></a>01348         nold += iold - k;
<a name="l01349"></a>01349       }
<a name="l01350"></a>01350   }
<a name="l01351"></a>01351 
<a name="l01352"></a>01352 
<a name="l01354"></a>01354 
<a name="l01357"></a>01357   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01358"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_packed_00_01_allocator_01_4.php#a683f609a9b3b4ab34521d1e9d74b5974">01358</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01359"></a>01359   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_packed_00_01_allocator_01_4.php" title="Row-major upper-triangular packed matrix class.">Matrix&lt;T, Prop, RowUpTriangPacked, Allocator&gt;</a>&amp;
<a name="l01360"></a>01360   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowUpTriangPacked, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01361"></a>01361   {
<a name="l01362"></a>01362     this-&gt;Fill(x);
<a name="l01363"></a>01363 
<a name="l01364"></a>01364     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01365"></a>01365   }
<a name="l01366"></a>01366 
<a name="l01367"></a>01367 
<a name="l01369"></a>01369 
<a name="l01374"></a>01374   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01375"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_packed_00_01_allocator_01_4.php#acb883c8d37950510eddcdafd4c7441ff">01375</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_packed_00_01_allocator_01_4.php" title="Row-major upper-triangular packed matrix class.">Matrix&lt;T, Prop, RowUpTriangPacked, Allocator&gt;</a>&amp;
<a name="l01376"></a>01376   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowUpTriangPacked, Allocator&gt;::operator</a>=
<a name="l01377"></a>01377   (<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_packed_00_01_allocator_01_4.php" title="Row-major upper-triangular packed matrix class.">Matrix&lt;T, Prop, RowUpTriangPacked, Allocator&gt;</a>&amp; A)
<a name="l01378"></a>01378   {
<a name="l01379"></a>01379     this-&gt;Copy(A);
<a name="l01380"></a>01380     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01381"></a>01381   }
<a name="l01382"></a>01382 
<a name="l01383"></a>01383 
<a name="l01385"></a>01385 
<a name="l01388"></a>01388   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01389"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_packed_00_01_allocator_01_4.php#a22d1b5d7b3c8f7dbbce29d36a7124d47">01389</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01390"></a>01390   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_packed_00_01_allocator_01_4.php" title="Row-major upper-triangular packed matrix class.">Matrix&lt;T, Prop, RowUpTriangPacked, Allocator&gt;</a>&amp;
<a name="l01391"></a>01391   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowUpTriangPacked, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01392"></a>01392   {
<a name="l01393"></a>01393     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetDataSize();i++)
<a name="l01394"></a>01394       this-&gt;data_[i] *= x;
<a name="l01395"></a>01395 
<a name="l01396"></a>01396     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01397"></a>01397   }
<a name="l01398"></a>01398 
<a name="l01399"></a>01399 
<a name="l01400"></a>01400 
<a name="l01402"></a>01402   <span class="comment">// MATRIX&lt;ROWLOTRIANGPACKED&gt; //</span>
<a name="l01404"></a>01404 <span class="comment"></span>
<a name="l01405"></a>01405 
<a name="l01406"></a>01406   <span class="comment">/****************</span>
<a name="l01407"></a>01407 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01408"></a>01408 <span class="comment">   ****************/</span>
<a name="l01409"></a>01409 
<a name="l01410"></a>01410 
<a name="l01412"></a>01412 
<a name="l01415"></a>01415   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01416"></a>01416   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowLoTriangPacked, Allocator&gt;::Matrix</a>():
<a name="l01417"></a>01417     <a class="code" href="class_seldon_1_1_matrix___triang_packed.php" title="Triangular packed matrix class.">Matrix_TriangPacked</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_lo_triang_packed.php">RowLoTriangPacked</a>, Allocator&gt;()
<a name="l01418"></a>01418   {
<a name="l01419"></a>01419   }
<a name="l01420"></a>01420 
<a name="l01421"></a>01421 
<a name="l01423"></a>01423 
<a name="l01428"></a>01428   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01429"></a>01429   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php#a607e3089e1ccddf239b481a8ddc556e9" title="Default constructor.">Matrix&lt;T, Prop, RowLoTriangPacked, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01430"></a>01430     Matrix_TriangPacked&lt;T, Prop, RowLoTriangPacked, Allocator&gt;(i, i)
<a name="l01431"></a>01431   {
<a name="l01432"></a>01432   }
<a name="l01433"></a>01433 
<a name="l01434"></a>01434 
<a name="l01435"></a>01435   <span class="comment">/*****************</span>
<a name="l01436"></a>01436 <span class="comment">   * OTHER METHODS *</span>
<a name="l01437"></a>01437 <span class="comment">   *****************/</span>
<a name="l01438"></a>01438 
<a name="l01439"></a>01439 
<a name="l01441"></a>01441 
<a name="l01448"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php#aa5fb51377f06688fd10f00ef5abd12dd">01448</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01449"></a>01449   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowLoTriangPacked, Allocator&gt;</a>
<a name="l01450"></a>01450 <a class="code" href="class_seldon_1_1_matrix.php">  ::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01451"></a>01451   {
<a name="l01452"></a>01452 
<a name="l01453"></a>01453     <span class="comment">// Storing the old values of the matrix.</span>
<a name="l01454"></a>01454     <span class="keywordtype">int</span> nold = this-&gt;GetDataSize();
<a name="l01455"></a>01455     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> xold(nold);
<a name="l01456"></a>01456     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; nold; k++)
<a name="l01457"></a>01457       xold(k) = this-&gt;data_[k];
<a name="l01458"></a>01458 
<a name="l01459"></a>01459     <span class="comment">// Reallocation.</span>
<a name="l01460"></a>01460     this-&gt;Reallocate(i, j);
<a name="l01461"></a>01461 
<a name="l01462"></a>01462     <span class="comment">// Filling the matrix with its old values.</span>
<a name="l01463"></a>01463     <span class="keywordtype">int</span> nmin = min(nold, this-&gt;GetDataSize());
<a name="l01464"></a>01464     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; nmin; k++)
<a name="l01465"></a>01465       this-&gt;data_[k] = xold(k);
<a name="l01466"></a>01466   }
<a name="l01467"></a>01467 
<a name="l01468"></a>01468 
<a name="l01470"></a>01470 
<a name="l01473"></a>01473   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01474"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php#a17e0f0ea723e14c87abfbbecb99abfc9">01474</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01475"></a>01475   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php" title="Row-major lower-triangular packed matrix class.">Matrix&lt;T, Prop, RowLoTriangPacked, Allocator&gt;</a>&amp;
<a name="l01476"></a>01476   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowLoTriangPacked, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01477"></a>01477   {
<a name="l01478"></a>01478     this-&gt;Fill(x);
<a name="l01479"></a>01479 
<a name="l01480"></a>01480     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01481"></a>01481   }
<a name="l01482"></a>01482 
<a name="l01483"></a>01483 
<a name="l01485"></a>01485 
<a name="l01490"></a>01490   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01491"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php#a0cf78d4b42de124bb3fb274c09166a75">01491</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php" title="Row-major lower-triangular packed matrix class.">Matrix&lt;T, Prop, RowLoTriangPacked, Allocator&gt;</a>&amp;
<a name="l01492"></a>01492   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowLoTriangPacked, Allocator&gt;::operator</a>=
<a name="l01493"></a>01493   (<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php" title="Row-major lower-triangular packed matrix class.">Matrix&lt;T, Prop, RowLoTriangPacked, Allocator&gt;</a>&amp; A)
<a name="l01494"></a>01494   {
<a name="l01495"></a>01495     this-&gt;Copy(A);
<a name="l01496"></a>01496     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01497"></a>01497   }
<a name="l01498"></a>01498 
<a name="l01499"></a>01499 
<a name="l01501"></a>01501 
<a name="l01504"></a>01504   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01505"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php#a6d3dfe89ad65b9daf47875ac529f7aa2">01505</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01506"></a>01506   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php" title="Row-major lower-triangular packed matrix class.">Matrix&lt;T, Prop, RowLoTriangPacked, Allocator&gt;</a>&amp;
<a name="l01507"></a>01507   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowLoTriangPacked, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01508"></a>01508   {
<a name="l01509"></a>01509     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetDataSize();i++)
<a name="l01510"></a>01510       this-&gt;data_[i] *= x;
<a name="l01511"></a>01511 
<a name="l01512"></a>01512     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01513"></a>01513   }
<a name="l01514"></a>01514 
<a name="l01515"></a>01515 } <span class="comment">// namespace Seldon.</span>
<a name="l01516"></a>01516 
<a name="l01517"></a>01517 <span class="preprocessor">#define SELDON_FILE_MATRIX_TRIANGPACKED_CXX</span>
<a name="l01518"></a>01518 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
