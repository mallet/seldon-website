<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Vector&lt; T, VectFull, Allocator &gt;</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a> &#124;
<a href="#pro-static-attribs">Static Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Vector&lt; T, VectFull, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Vector&lt; T, VectFull, Allocator &gt;" --><!-- doxytag: inherits="Seldon::Vector_Base" -->
<p>Full vector class.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_vector_8hxx_source.php">Vector.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::Vector&lt; T, VectFull, Allocator &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.png" usemap="#Seldon::Vector&lt; T, VectFull, Allocator &gt;_map" alt=""/>
  <map id="Seldon::Vector&lt; T, VectFull, Allocator &gt;_map" name="Seldon::Vector&lt; T, VectFull, Allocator &gt;_map">
<area href="class_seldon_1_1_vector___base.php" alt="Seldon::Vector_Base&lt; T, Allocator &gt;" shape="rect" coords="0,0,261,24"/>
<area href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php" alt="Seldon::Vector&lt; T, VectSparse, Allocator &gt;" shape="rect" coords="0,112,261,136"/>
</map>
</div>

<p><a href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a46cf758a46654cdf283888c9f46eb69e"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::value_type" ref="a46cf758a46654cdf283888c9f46eb69e" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae647968d425e70e3870fa69bc9963849"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::pointer" ref="ae647968d425e70e3870fa69bc9963849" args="" -->
typedef Allocator::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae7873a6f7a2c932f853da50876bff8ba"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::const_pointer" ref="ae7873a6f7a2c932f853da50876bff8ba" args="" -->
typedef Allocator::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae234c94ac5ed58b90e5181601621d00b"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::reference" ref="ae234c94ac5ed58b90e5181601621d00b" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a20cd28da86cd2b4bdcbb98b5ad8367df"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::const_reference" ref="a20cd28da86cd2b4bdcbb98b5ad8367df" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab8bd98d84b2fa6d643f09ee1453db7d3"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::storage" ref="ab8bd98d84b2fa6d643f09ee1453db7d3" args="" -->
typedef <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>&nbsp;</td><td class="memItemRight" valign="bottom"><b>storage</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a3220e19bce6b4a0f72dfca147dbf8b38">Vector</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#a3220e19bce6b4a0f72dfca147dbf8b38"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#aaef02756d3c6c0a97454f35bffc07962">Vector</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Main constructor.  <a href="#aaef02756d3c6c0a97454f35bffc07962"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9968a7ded038b390777ff8f03188a08d"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::Vector" ref="a9968a7ded038b390777ff8f03188a08d" args="(int i, pointer data)" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><b>Vector</b> (int i, pointer data)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a1afba3b1f5b3d8022dd4e047185968d8">Vector</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copy constructor.  <a href="#a1afba3b1f5b3d8022dd4e047185968d8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afe4b7f7be7d9dedb8e1275f283706c43"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::~Vector" ref="afe4b7f7be7d9dedb8e1275f283706c43" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#afe4b7f7be7d9dedb8e1275f283706c43">~Vector</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Destructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a5120b4ecff05d8cf54bbd57fb280fbe1">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the vector.  <a href="#a5120b4ecff05d8cf54bbd57fb280fbe1"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a6c1667ecf1265c0870ba2828b33a2d79">Reallocate</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight"><a class="el" href="class_seldon_1_1_vector.php">Vector</a> reallocation.  <a href="#a6c1667ecf1265c0870ba2828b33a2d79"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a99dda1b202ce562645f890897c593515">Resize</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Changes the length of the vector, and keeps previous values.  <a href="#a99dda1b202ce562645f890897c593515"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a658e420ab086d76b755b9464bf9a8d66"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::SetData" ref="a658e420ab086d76b755b9464bf9a8d66" args="(int i, pointer data)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetData</b> (int i, pointer data)</td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Allocator0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ab12cedc7bee632a7b210312f1e7ef28b">SetData</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt; &amp;V)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Lets the current vector point to the data of another vector.  <a href="#ab12cedc7bee632a7b210312f1e7ef28b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a151dc47113d07ab24775733d0c46dc90">Nullify</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the vector without releasing memory.  <a href="#a151dc47113d07ab24775733d0c46dc90"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#abb6f9d844071aaa8c8bc1ac62b1dba07">operator()</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#abb6f9d844071aaa8c8bc1ac62b1dba07"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a85cd9b850be2276e853e81ba567a18e6">Get</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access to element <em>i</em>.  <a href="#a85cd9b850be2276e853e81ba567a18e6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#af6ee550855ccf2a4db736bad2352929f">operator()</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#af6ee550855ccf2a4db736bad2352929f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a92b34068ad39cbfec8918d77d2385bee">Get</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access to element <em>i</em>.  <a href="#a92b34068ad39cbfec8918d77d2385bee"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#abd12db9e14f5ba1c417d4c587e6d359a">operator=</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt; &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a vector (assignment operator).  <a href="#abd12db9e14f5ba1c417d4c587e6d359a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a790a38cb578627bf17291088f2cd2388">Copy</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt; &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a vector.  <a href="#a790a38cb578627bf17291088f2cd2388"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a6e1000961b639af39a111147cf820d77">Copy</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a vector.  <a href="#a6e1000961b639af39a111147cf820d77"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ab3773a41209f76c5739be8f7f037006c">Append</a> (const T &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Appends an element to the vector.  <a href="#ab3773a41209f76c5739be8f7f037006c"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a6a64b54653758dbbdcd650c8ffba4d52">PushBack</a> (const T0 &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Appends an element at the end of the vector.  <a href="#a6a64b54653758dbbdcd650c8ffba4d52"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Allocator0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#aa0b88a0415692922c36c2b2f284efeca">PushBack</a> (const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt; &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Appends a vector X at the end of the vector.  <a href="#aa0b88a0415692922c36c2b2f284efeca"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a3906b8279cc318dc0a8a672fa781095a">GetDataSize</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored.  <a href="#a3906b8279cc318dc0a8a672fa781095a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ae8e4d6b081a6f4c90b2993b047a04dac">Zero</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets all elements to zero.  <a href="#ae8e4d6b081a6f4c90b2993b047a04dac"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a722046c309ee8e50abd68a5a5a77aa60"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::Fill" ref="a722046c309ee8e50abd68a5a5a77aa60" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a722046c309ee8e50abd68a5a5a77aa60">Fill</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the vector with 0, 1, 2, ... <br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a2c103fe432a6f6a9c37cb133bb837153">Fill</a> (const T0 &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the vector with a given value.  <a href="#a2c103fe432a6f6a9c37cb133bb837153"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt; &amp;&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a0bf814aab7883056808671f5b6866ba6">operator=</a> (const T0 &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the vector with a given value.  <a href="#a0bf814aab7883056808671f5b6866ba6"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt; &amp;&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a236fd63d659f7d92f3b12da557380cb3">operator*=</a> (const T0 &amp;X)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Multiplies a vector by a scalar.  <a href="#a236fd63d659f7d92f3b12da557380cb3"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a71147cebc8bd36cc103d2e6fd3767f91">FillRand</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the vector randomly.  <a href="#a71147cebc8bd36cc103d2e6fd3767f91"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adba43c4d26c5b47cd5868e26418f147c"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::Print" ref="adba43c4d26c5b47cd5868e26418f147c" args="() const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#adba43c4d26c5b47cd5868e26418f147c">Print</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays the vector. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">value_type&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a165cbeeddb4a2a356accc00d609b4211">GetNormInf</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the infinite norm.  <a href="#a165cbeeddb4a2a356accc00d609b4211"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a50186150708e90f86ded59e33cb172a6">GetNormInfIndex</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the index of the highest absolute value.  <a href="#a50186150708e90f86ded59e33cb172a6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a51ea9d8b0a3176fc903c0279ae8770d0">Write</a> (string FileName, bool with_size=true) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the vector in a file.  <a href="#a51ea9d8b0a3176fc903c0279ae8770d0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a1a098ef03b3d8cad79b659cd05be220e">Write</a> (ostream &amp;FileStream, bool with_size=true) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the vector in a file stream.  <a href="#a1a098ef03b3d8cad79b659cd05be220e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a39fd5c75b22c5c2d37a47587f57abad2">WriteText</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the vector in a file.  <a href="#a39fd5c75b22c5c2d37a47587f57abad2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ab5bb2e8cf1a7ceb1ac941a88ed1635e6">WriteText</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the vector in a file stream.  <a href="#ab5bb2e8cf1a7ceb1ac941a88ed1635e6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a2ce4a8d2726b069e465d4156b1b531be">Read</a> (string FileName, bool with_size=true)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the vector from a file.  <a href="#a2ce4a8d2726b069e465d4156b1b531be"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a634f8993354070451e53fd0c29bffa49">Read</a> (istream &amp;FileStream, bool with_size=true)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the vector from a file stream.  <a href="#a634f8993354070451e53fd0c29bffa49"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#af1a8f91e66d729c91fddb0a0c9e030b8">ReadText</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the vector from a file.  <a href="#af1a8f91e66d729c91fddb0a0c9e030b8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ae0f8292f742367edca616ab9e2e93e6b">ReadText</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the vector from a file stream.  <a href="#ae0f8292f742367edca616ab9e2e93e6b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae">GetM</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements.  <a href="#a51f50515f0c6d0663465c2cc7de71bae"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a075daae3607a4767a77a3de60ab30ba7">GetLength</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements.  <a href="#a075daae3607a4767a77a3de60ab30ba7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a839880a3113c1885ead70d79fade0294">GetSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored.  <a href="#a839880a3113c1885ead70d79fade0294"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a">GetData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer to data_ (stored data).  <a href="#a4128ad4898e42211d22a7531c9f5f80a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a454aea78c82c4317dfe4160cc7c95afa">GetDataConst</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a const pointer to data_ (stored data).  <a href="#a454aea78c82c4317dfe4160cc7c95afa"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#ad0f5184bd9eec6fca70c54e459ced78f">GetDataVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "void*" to the data array (data_).  <a href="#ad0f5184bd9eec6fca70c54e459ced78f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_vector___base.php#a0aadc006977de037eb3ee550683e3f19">GetDataConstVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "const void*" to the data array (data_).  <a href="#a0aadc006977de037eb3ee550683e3f19"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac58514f2c122d7538dfc4db4ce0d4da9"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::m_" ref="ac58514f2c122d7538dfc4db4ce0d4da9" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>m_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1a6ae822c00f9ee2a798d54a36602430"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::data_" ref="a1a6ae822c00f9ee2a798d54a36602430" args="" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-static-attribs"></a>
Static Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6c958ad29a5f7eabd1f085e50046ab85"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::vect_allocator_" ref="a6c958ad29a5f7eabd1f085e50046ab85" args="" -->
static Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>vect_allocator_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Allocator&gt;<br/>
 class Seldon::Vector&lt; T, VectFull, Allocator &gt;</h3>

<p>Full vector class. </p>
<p>Basic vector class (i.e. not sparse). </p>

<p>Definition at line <a class="el" href="_vector_8hxx_source.php#l00091">91</a> of file <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="a3220e19bce6b4a0f72dfca147dbf8b38"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::Vector" ref="a3220e19bce6b4a0f72dfca147dbf8b38" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_vector.php">Vector</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [explicit]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>On exit, the vector is empty. </p>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#aee3f9a5bfd70e6370ae0c559257624f7">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00212">212</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aaef02756d3c6c0a97454f35bffc07962"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::Vector" ref="aaef02756d3c6c0a97454f35bffc07962" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_vector.php">Vector</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [explicit]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Main constructor. </p>
<p>Builds a vector of a given size. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>length of the vector. </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#aa33cfa4d38f149671d6d0ea82f5106d8">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00223">223</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1afba3b1f5b3d8022dd4e047185968d8"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::Vector" ref="a1afba3b1f5b3d8022dd4e047185968d8" args="(const Vector&lt; T, VectFull, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_vector.php">Vector</a> </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>V</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Copy constructor. </p>
<p>Builds a copy of a vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>V</em>&nbsp;</td><td>vector to be copied. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00279">279</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="ab3773a41209f76c5739be8f7f037006c"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::Append" ref="ab3773a41209f76c5739be8f7f037006c" args="(const T &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Append </td>
          <td>(</td>
          <td class="paramtype">const T &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Appends an element to the vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>element to be appended. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>This method will only work if the allocator preserves the elements while reallocating. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00625">625</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5120b4ecff05d8cf54bbd57fb280fbe1"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::Clear" ref="a5120b4ecff05d8cf54bbd57fb280fbe1" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Clear </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the vector. </p>
<p>Destructs the vector. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>On exit, the vector is an empty vector. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a548fa99edadcd9b418912d607b7aa19b">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00334">334</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6e1000961b639af39a111147cf820d77"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::Copy" ref="a6e1000961b639af39a111147cf820d77" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Copy </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a vector. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A copy of the vector. </dd></dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: the returned vector is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00597">597</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a790a38cb578627bf17291088f2cd2388"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::Copy" ref="a790a38cb578627bf17291088f2cd2388" args="(const Vector&lt; T, VectFull, Allocator &gt; &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Copy </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>X</em>&nbsp;</td><td>vector to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'X' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00581">581</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2c103fe432a6f6a9c37cb133bb837153"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::Fill" ref="a2c103fe432a6f6a9c37cb133bb837153" args="(const T0 &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Fill </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the vector with a given value. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>value to fill the vector with. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00709">709</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a71147cebc8bd36cc103d2e6fd3767f91"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::FillRand" ref="a71147cebc8bd36cc103d2e6fd3767f91" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::FillRand </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the vector randomly. </p>
<dl class="note"><dt><b>Note:</b></dt><dd>The random generator is very basic. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00736">736</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a85cd9b850be2276e853e81ba567a18e6"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::Get" ref="a85cd9b850be2276e853e81ba567a18e6" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::reference <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Get </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access to element <em>i</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The value of the vector at <em>i</em>. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a61bcf710b6d43a3690561abd27d4b145">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00501">501</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a92b34068ad39cbfec8918d77d2385bee"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::Get" ref="a92b34068ad39cbfec8918d77d2385bee" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::const_reference <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Get </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access to element <em>i</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The value of the vector at <em>i</em>. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#ad57ac9ab256f25c6f9f244fa6e7b2ccf">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00543">543</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a4128ad4898e42211d22a7531c9f5f80a"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::GetData" ref="a4128ad4898e42211d22a7531c9f5f80a" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector___base.php">Vector_Base</a>&lt; T, Allocator &gt;::pointer <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer to data_ (stored data). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer to the data_, i.e. the data array. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00156">156</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a454aea78c82c4317dfe4160cc7c95afa"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::GetDataConst" ref="a454aea78c82c4317dfe4160cc7c95afa" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector___base.php">Vector_Base</a>&lt; T, Allocator &gt;::const_pointer <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetDataConst </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a const pointer to data_ (stored data). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer to the data_, i.e. the data array. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00168">168</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0aadc006977de037eb3ee550683e3f19"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::GetDataConstVoid" ref="a0aadc006977de037eb3ee550683e3f19" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const void * <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetDataConstVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "const void*" to the data array (data_). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer of type "const void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00191">191</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3906b8279cc318dc0a8a672fa781095a"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::GetDataSize" ref="a3906b8279cc318dc0a8a672fa781095a" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::GetDataSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements stored in memory. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00670">670</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad0f5184bd9eec6fca70c54e459ced78f"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::GetDataVoid" ref="ad0f5184bd9eec6fca70c54e459ced78f" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void * <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetDataVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "void*" to the data array (data_). </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00180">180</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a075daae3607a4767a77a3de60ab30ba7"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::GetLength" ref="a075daae3607a4767a77a3de60ab30ba7" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetLength </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the vector. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ae501c34e51f6559568846b94e24c249c">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>, and <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a2a2b8d5ad39c79bdcdcd75fa20ae5ae8">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00133">133</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a51f50515f0c6d0663465c2cc7de71bae"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::GetM" ref="a51f50515f0c6d0663465c2cc7de71bae" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the vector. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a6f40d71a69e016ae3e1d9db92b6538a4">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>, and <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a9b0a8be2fb500d4f6c2edb4c1a5247ef">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00122">122</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a165cbeeddb4a2a356accc00d609b4211"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::GetNormInf" ref="a165cbeeddb4a2a356accc00d609b4211" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::value_type <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::GetNormInf </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the infinite norm. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The infinite norm. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00765">765</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a50186150708e90f86ded59e33cb172a6"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::GetNormInfIndex" ref="a50186150708e90f86ded59e33cb172a6" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::GetNormInfIndex </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the index of the highest absolute value. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The index of the element that has the highest absolute value. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00783">783</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a839880a3113c1885ead70d79fade0294"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::GetSize" ref="a839880a3113c1885ead70d79fade0294" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base</a>&lt; T, Allocator &gt;::GetSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length of the vector stored. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00144">144</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a151dc47113d07ab24775733d0c46dc90"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::Nullify" ref="a151dc47113d07ab24775733d0c46dc90" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Nullify </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the vector without releasing memory. </p>
<p>On exit, the vector is empty and the memory has not been released. It is useful for low level manipulations on a <a class="el" href="class_seldon_1_1_vector.php">Vector</a> instance. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Memory is not released. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a985afdfd20e655bb551cf44669acd0e6">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00461">461</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af6ee550855ccf2a4db736bad2352929f"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::operator()" ref="af6ee550855ccf2a4db736bad2352929f" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::const_reference <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The value of the vector at 'i'. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a001a7d1629309ad90c6d66d695a69e8f">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00522">522</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="abb6f9d844071aaa8c8bc1ac62b1dba07"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::operator()" ref="abb6f9d844071aaa8c8bc1ac62b1dba07" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::reference <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The value of the vector at 'i'. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00480">480</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a236fd63d659f7d92f3b12da557380cb3"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::operator*=" ref="a236fd63d659f7d92f3b12da557380cb3" args="(const T0 &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::operator*= </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>alpha</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Multiplies a vector by a scalar. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>alpha</em>&nbsp;</td><td>scalar. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00609">609</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0bf814aab7883056808671f5b6866ba6"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::operator=" ref="a0bf814aab7883056808671f5b6866ba6" args="(const T0 &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the vector with a given value. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>value to fill the vector with. </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a1d0196721350369a3154b60dfa105b37">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00723">723</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="abd12db9e14f5ba1c417d4c587e6d359a"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::operator=" ref="abd12db9e14f5ba1c417d4c587e6d359a" args="(const Vector&lt; T, VectFull, Allocator &gt; &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a vector (assignment operator). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>X</em>&nbsp;</td><td>vector to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'X' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00565">565</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa0b88a0415692922c36c2b2f284efeca"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::PushBack" ref="aa0b88a0415692922c36c2b2f284efeca" args="(const Vector&lt; T, VectFull, Allocator0 &gt; &amp;X)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Allocator0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::PushBack </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>X</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Appends a vector X at the end of the vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>X</em>&nbsp;</td><td>vector to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00651">651</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6a64b54653758dbbdcd650c8ffba4d52"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::PushBack" ref="a6a64b54653758dbbdcd650c8ffba4d52" args="(const T0 &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::PushBack </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Appends an element at the end of the vector. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>element to be appended. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00638">638</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a634f8993354070451e53fd0c29bffa49"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::Read" ref="a634f8993354070451e53fd0c29bffa49" args="(istream &amp;FileStream, bool with_size=true)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the vector from a file stream. </p>
<p>Sets the vector according to a binary file stream that stores the length of the vector (integer) and all elements. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>file stream. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the length of the vector is not available in the stream. In this case, the current size N of the vector is unchanged, and N elements are read in the stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l01062">1062</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2ce4a8d2726b069e465d4156b1b531be"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::Read" ref="a2ce4a8d2726b069e465d4156b1b531be" args="(string FileName, bool with_size=true)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the vector from a file. </p>
<p>Sets the vector according to a binary file that stores the length of the vector (integer) and all elements. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>file name. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the length of the vector is not available in the file. In this case, the current size N of the vector is unchanged, and N elements are read in the file. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l01033">1033</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af1a8f91e66d729c91fddb0a0c9e030b8"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::ReadText" ref="af1a8f91e66d729c91fddb0a0c9e030b8" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the vector from a file. </p>
<p>Sets all elements of the vector according to a text format. The length is not stored. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a3b9e7d2529315dfaa75a09d3c34da18a">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l01101">1101</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae0f8292f742367edca616ab9e2e93e6b"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::ReadText" ref="ae0f8292f742367edca616ab9e2e93e6b" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets the vector from a file stream. </p>
<p>Sets all elements of the vector according to a text format. The length is not stored. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>file stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a554c1d3659fc50f3e119ef14437ad54a">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l01126">1126</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6c1667ecf1265c0870ba2828b33a2d79"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::Reallocate" ref="a6c1667ecf1265c0870ba2828b33a2d79" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Reallocate </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p><a class="el" href="class_seldon_1_1_vector.php">Vector</a> reallocation. </p>
<p>The vector is resized. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>new length of the vector. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Depending on your allocator, initial elements of the vector may be lost. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#afcc8fef4210e7703493ad95acf389d19">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00348">348</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a99dda1b202ce562645f890897c593515"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::Resize" ref="a99dda1b202ce562645f890897c593515" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Resize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>n</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Changes the length of the vector, and keeps previous values. </p>
<p>Reallocates the vector to size i. Previous values are kept. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>n</em>&nbsp;</td><td>new length of the vector. </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a3637911015d5a0fafa4811ee2204bbc5">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00390">390</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab12cedc7bee632a7b210312f1e7ef28b"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::SetData" ref="ab12cedc7bee632a7b210312f1e7ef28b" args="(const Vector&lt; T, VectFull, Allocator0 &gt; &amp;V)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Allocator0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::SetData </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>V</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Lets the current vector point to the data of another vector. </p>
<p>Deallocates memory allocated for the current data array. Then sets the length and the data of the current vector to that of <em>V</em>. On exit, the current vector shares it data array with <em>V</em>. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>V</em>&nbsp;</td><td>vector whose data should be shared with current instance. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>On exit, <em>V</em> and the current instance share the same data array, and they are likely to free it. As a consequence, before the vectors are destructed, it is necessary to call 'Nullify' on either <em>V</em> or the current instance. In addition, if the current instance is to deallocate the data array, its allocator should be compatible with the allocator that allocated the data array (probably the allocator of <em>V</em>). </dd></dl>
<dl class="note"><dt><b>Note:</b></dt><dd>This method should only be used by advanced users. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00448">448</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1a098ef03b3d8cad79b659cd05be220e"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::Write" ref="a1a098ef03b3d8cad79b659cd05be220e" args="(ostream &amp;FileStream, bool with_size=true) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the vector in a file stream. </p>
<p>The length of the vector (integer) and all elements of the vector are stored in binary format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>file stream. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the length of the vector is not saved. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00848">848</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a51ea9d8b0a3176fc903c0279ae8770d0"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::Write" ref="a51ea9d8b0a3176fc903c0279ae8770d0" args="(string FileName, bool with_size=true) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>with_size</em> = <code>true</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the vector in a file. </p>
<p>The length of the vector (integer) and all elements of the vector are stored in binary format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>file name. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>with_size</em>&nbsp;</td><td>if set to 'false', the length of the vector is not saved. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00820">820</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a39fd5c75b22c5c2d37a47587f57abad2"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::WriteText" ref="a39fd5c75b22c5c2d37a47587f57abad2" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the vector in a file. </p>
<p>All elements of the vector are stored in text format. The length is not stored. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#ab770c6961b3c24de5679fc93530e6e80">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00884">884</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab5bb2e8cf1a7ceb1ac941a88ed1635e6"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::WriteText" ref="ab5bb2e8cf1a7ceb1ac941a88ed1635e6" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the vector in a file stream. </p>
<p>All elements of the vector are stored in text format. The length is not stored. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>file stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a71b2e3e54ed55920f6b34912382fac80">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00911">911</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae8e4d6b081a6f4c90b2993b047a04dac"></a><!-- doxytag: member="Seldon::Vector&lt; T, VectFull, Allocator &gt;::Zero" ref="ae8e4d6b081a6f4c90b2993b047a04dac" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_vector.php">Seldon::Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;::Zero </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets all elements to zero. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>It fills the memory with zeros. If the vector stores complex structures, use 'Fill' instead. </dd></dl>

<p>Definition at line <a class="el" href="_vector_8cxx_source.php#l00687">687</a> of file <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>vector/<a class="el" href="_vector_8hxx_source.php">Vector.hxx</a></li>
<li>vector/<a class="el" href="_vector_8cxx_source.php">Vector.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
