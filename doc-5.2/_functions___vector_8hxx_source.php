<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "eigenvalue"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "eigenvalue", "Eigenvalue Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/basic_functions/Functions_Vector.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2011 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00003"></a>00003 <span class="comment">// Copyright (C) 2010 INRIA</span>
<a name="l00004"></a>00004 <span class="comment">// Author(s): Marc Fragu</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00007"></a>00007 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00008"></a>00008 <span class="comment">//</span>
<a name="l00009"></a>00009 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00010"></a>00010 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00011"></a>00011 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00012"></a>00012 <span class="comment">// any later version.</span>
<a name="l00013"></a>00013 <span class="comment">//</span>
<a name="l00014"></a>00014 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00015"></a>00015 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00016"></a>00016 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00017"></a>00017 <span class="comment">// more details.</span>
<a name="l00018"></a>00018 <span class="comment">//</span>
<a name="l00019"></a>00019 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00020"></a>00020 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00021"></a>00021 
<a name="l00022"></a>00022 
<a name="l00023"></a>00023 <span class="preprocessor">#ifndef SELDON_FILE_FUNCTIONS_VECTOR_HXX</span>
<a name="l00024"></a>00024 <span class="preprocessor"></span><span class="preprocessor">#define SELDON_FILE_FUNCTIONS_VECTOR_HXX</span>
<a name="l00025"></a>00025 <span class="preprocessor"></span>
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 <span class="comment">/*</span>
<a name="l00028"></a>00028 <span class="comment">  Functions defined in this file:</span>
<a name="l00029"></a>00029 <span class="comment"></span>
<a name="l00030"></a>00030 <span class="comment">  alpha X -&gt; X</span>
<a name="l00031"></a>00031 <span class="comment">  Mlt(alpha, X)</span>
<a name="l00032"></a>00032 <span class="comment"></span>
<a name="l00033"></a>00033 <span class="comment">  alpha X + Y -&gt; Y</span>
<a name="l00034"></a>00034 <span class="comment">  Add(alpha, X, Y)</span>
<a name="l00035"></a>00035 <span class="comment"></span>
<a name="l00036"></a>00036 <span class="comment">  X -&gt; Y</span>
<a name="l00037"></a>00037 <span class="comment">  Copy(X, Y)</span>
<a name="l00038"></a>00038 <span class="comment"></span>
<a name="l00039"></a>00039 <span class="comment">  X &lt;-&gt; Y</span>
<a name="l00040"></a>00040 <span class="comment">  Swap(X, Y)</span>
<a name="l00041"></a>00041 <span class="comment"></span>
<a name="l00042"></a>00042 <span class="comment">  X.Y</span>
<a name="l00043"></a>00043 <span class="comment">  DotProd(X, Y)</span>
<a name="l00044"></a>00044 <span class="comment">  DotProdConj(X, Y)</span>
<a name="l00045"></a>00045 <span class="comment"></span>
<a name="l00046"></a>00046 <span class="comment">  ||X||</span>
<a name="l00047"></a>00047 <span class="comment">  Norm1(X)</span>
<a name="l00048"></a>00048 <span class="comment">  Norm2(X)</span>
<a name="l00049"></a>00049 <span class="comment">  GetMaxAbsIndex(X)</span>
<a name="l00050"></a>00050 <span class="comment"></span>
<a name="l00051"></a>00051 <span class="comment">  Omega X</span>
<a name="l00052"></a>00052 <span class="comment">  GenRot(x, y, cos, sin)</span>
<a name="l00053"></a>00053 <span class="comment">  ApplyRot(x, y, cos, sin)</span>
<a name="l00054"></a>00054 <span class="comment"></span>
<a name="l00055"></a>00055 <span class="comment">*/</span>
<a name="l00056"></a>00056 
<a name="l00057"></a>00057 
<a name="l00058"></a>00058 <span class="keyword">namespace </span>Seldon
<a name="l00059"></a>00059 {
<a name="l00060"></a>00060 
<a name="l00061"></a>00061 
<a name="l00063"></a>00063 <span class="comment">// MLT //</span>
<a name="l00064"></a>00064 
<a name="l00065"></a>00065 
<a name="l00066"></a>00066 <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00067"></a>00067           <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00068"></a>00068 <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab3757078b11c433393c0434fcbe42f0b" title="Multiplication of all elements of a 3D array by a scalar.">Mlt</a>(<span class="keyword">const</span> T0 alpha, Vector&lt;T1, Storage1, Allocator1&gt;&amp; X);
<a name="l00069"></a>00069 
<a name="l00070"></a>00070 
<a name="l00071"></a>00071 <span class="comment">// MLT //</span>
<a name="l00073"></a>00073 <span class="comment"></span>
<a name="l00074"></a>00074 
<a name="l00076"></a>00076 <span class="comment">// ADD //</span>
<a name="l00077"></a>00077 
<a name="l00078"></a>00078 
<a name="l00079"></a>00079 <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00080"></a>00080           <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00081"></a>00081           <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00082"></a>00082 <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00083"></a>00083            <span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00084"></a>00084            Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y);
<a name="l00085"></a>00085 
<a name="l00086"></a>00086   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00087"></a>00087             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00088"></a>00088             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00089"></a>00089   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00090"></a>00090            <span class="keyword">const</span> Vector&lt;T1, PETScSeq, Allocator1&gt;&amp; X,
<a name="l00091"></a>00091            Vector&lt;T2, PETScSeq, Allocator2&gt;&amp; Y);
<a name="l00092"></a>00092 
<a name="l00093"></a>00093   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00094"></a>00094             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00095"></a>00095             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00096"></a>00096   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00097"></a>00097            <span class="keyword">const</span> Vector&lt;T1, PETScPar, Allocator1&gt;&amp; X,
<a name="l00098"></a>00098            Vector&lt;T2, PETScPar, Allocator2&gt;&amp; Y);
<a name="l00099"></a>00099 
<a name="l00100"></a>00100 <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00101"></a>00101           <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00102"></a>00102           <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00103"></a>00103 <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00104"></a>00104            <span class="keyword">const</span> Vector&lt;T1, VectSparse, Allocator1&gt;&amp; X,
<a name="l00105"></a>00105            Vector&lt;T2, VectSparse, Allocator2&gt;&amp; Y);
<a name="l00106"></a>00106 
<a name="l00107"></a>00107 <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00108"></a>00108           <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00109"></a>00109           <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00110"></a>00110 <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00111"></a>00111            <span class="keyword">const</span> Vector&lt;T1, Collection, Allocator1&gt;&amp; X,
<a name="l00112"></a>00112            Vector&lt;T2, Collection, Allocator2&gt;&amp; Y);
<a name="l00113"></a>00113 
<a name="l00114"></a>00114 <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00115"></a>00115           <span class="keyword">class </span>T1, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U1&gt; <span class="keyword">class </span>Allocator1,
<a name="l00116"></a>00116           <span class="keyword">class </span>T2, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U2&gt; <span class="keyword">class </span>Allocator2&gt;
<a name="l00117"></a>00117 <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00118"></a>00118            <span class="keyword">const</span>
<a name="l00119"></a>00119            Vector&lt;FloatDouble, DenseSparseCollection, Allocator1&lt;T1&gt; &gt;&amp; X,
<a name="l00120"></a>00120            Vector&lt;FloatDouble, DenseSparseCollection, Allocator2&lt;T2&gt; &gt;&amp; Y);
<a name="l00121"></a>00121 
<a name="l00122"></a>00122 <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00123"></a>00123           <span class="keyword">class </span>T1, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U1&gt; <span class="keyword">class </span>Allocator1,
<a name="l00124"></a>00124           <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00125"></a>00125 <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00126"></a>00126            <span class="keyword">const</span>
<a name="l00127"></a>00127            Vector&lt;FloatDouble, DenseSparseCollection, Allocator1&lt;T1&gt; &gt;&amp; X,
<a name="l00128"></a>00128            Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y);
<a name="l00129"></a>00129 
<a name="l00130"></a>00130 
<a name="l00131"></a>00131 <span class="comment">// ADD //</span>
<a name="l00133"></a>00133 <span class="comment"></span>
<a name="l00134"></a>00134 
<a name="l00136"></a>00136 <span class="comment">// COPY //</span>
<a name="l00137"></a>00137 
<a name="l00138"></a>00138 
<a name="l00139"></a>00139 <span class="keyword">template</span> &lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00140"></a>00140           <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00141"></a>00141 <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00142"></a>00142             Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y);
<a name="l00143"></a>00143 
<a name="l00144"></a>00144 <span class="keyword">template</span> &lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00145"></a>00145           <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00146"></a>00146 <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Vector&lt;T1, Collection, Allocator1&gt;&amp; X,
<a name="l00147"></a>00147             Vector&lt;T2, VectFull, Allocator2&gt;&amp; Y);
<a name="l00148"></a>00148 
<a name="l00149"></a>00149 
<a name="l00150"></a>00150 <span class="comment">// COPY //</span>
<a name="l00152"></a>00152 <span class="comment"></span>
<a name="l00153"></a>00153 
<a name="l00155"></a>00155 <span class="comment">// SWAP //</span>
<a name="l00156"></a>00156 
<a name="l00157"></a>00157 
<a name="l00158"></a>00158 <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00159"></a>00159 <span class="keywordtype">void</span> Swap(Vector&lt;T, Storage, Allocator&gt;&amp; X,
<a name="l00160"></a>00160             Vector&lt;T, Storage, Allocator&gt;&amp; Y);
<a name="l00161"></a>00161 
<a name="l00162"></a>00162 <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00163"></a>00163 <span class="keywordtype">void</span> Swap(Vector&lt;T, VectSparse, Allocator&gt;&amp; X,
<a name="l00164"></a>00164             Vector&lt;T, VectSparse, Allocator&gt;&amp; Y);
<a name="l00165"></a>00165 
<a name="l00166"></a>00166 
<a name="l00167"></a>00167 <span class="comment">// SWAP //</span>
<a name="l00169"></a>00169 <span class="comment"></span>
<a name="l00170"></a>00170 
<a name="l00172"></a>00172 <span class="comment">// DOTPROD //</span>
<a name="l00173"></a>00173 
<a name="l00174"></a>00174 
<a name="l00175"></a>00175 <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00176"></a>00176          <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00177"></a>00177 T1 <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(<span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00178"></a>00178              <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y);
<a name="l00179"></a>00179 
<a name="l00180"></a>00180 <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00181"></a>00181          <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00182"></a>00182 <span class="keyword">typename</span> T1::value_type
<a name="l00183"></a>00183 <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(<span class="keyword">const</span> Vector&lt;T1, Collection, Allocator1&gt;&amp; X,
<a name="l00184"></a>00184           <span class="keyword">const</span> Vector&lt;T2, Collection, Allocator2&gt;&amp; Y);
<a name="l00185"></a>00185 
<a name="l00186"></a>00186 <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U1&gt; <span class="keyword">class </span>Allocator1,
<a name="l00187"></a>00187          <span class="keyword">class </span>T2, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U2&gt; <span class="keyword">class </span>Allocator2&gt;
<a name="l00188"></a>00188 <span class="keywordtype">double</span>
<a name="l00189"></a>00189 <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(<span class="keyword">const</span>
<a name="l00190"></a>00190 Vector&lt;FloatDouble, DenseSparseCollection, Allocator1&lt;T1&gt; &gt;&amp; X,
<a name="l00191"></a>00191           <span class="keyword">const</span>
<a name="l00192"></a>00192           Vector&lt;FloatDouble, DenseSparseCollection, Allocator2&lt;T2&gt; &gt;&amp; Y);
<a name="l00193"></a>00193 
<a name="l00194"></a>00194 <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00195"></a>00195          <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00196"></a>00196 T1 <a class="code" href="namespace_seldon.php#afd8cf3dd207cb4727edacda863a3f055" title="Scalar product between two vectors.">DotProdConj</a>(<span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00197"></a>00197                  <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y);
<a name="l00198"></a>00198 
<a name="l00199"></a>00199 <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00200"></a>00200          <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00201"></a>00201 complex&lt;T1&gt; <a class="code" href="namespace_seldon.php#afd8cf3dd207cb4727edacda863a3f055" title="Scalar product between two vectors.">DotProdConj</a>(<span class="keyword">const</span> Vector&lt;complex&lt;T1&gt;, Storage1, Allocator1&gt;&amp; X,
<a name="l00202"></a>00202                           <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y);
<a name="l00203"></a>00203 
<a name="l00204"></a>00204 <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00205"></a>00205          <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00206"></a>00206 T1 <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(<span class="keyword">const</span> Vector&lt;T1, VectSparse, Allocator1&gt;&amp; X,
<a name="l00207"></a>00207              <span class="keyword">const</span> Vector&lt;T2, VectSparse, Allocator2&gt;&amp; Y);
<a name="l00208"></a>00208 
<a name="l00209"></a>00209   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00210"></a>00210            <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00211"></a>00211   complex&lt;T1&gt;
<a name="l00212"></a>00212   <a class="code" href="namespace_seldon.php#afd8cf3dd207cb4727edacda863a3f055" title="Scalar product between two vectors.">DotProdConj</a>(<span class="keyword">const</span> Vector&lt;complex&lt;T1&gt;, VectSparse, Allocator1&gt;&amp; X,
<a name="l00213"></a>00213               <span class="keyword">const</span> Vector&lt;T2, VectSparse, Allocator2&gt;&amp; Y);
<a name="l00214"></a>00214 
<a name="l00215"></a>00215 
<a name="l00216"></a>00216   <span class="comment">// DOTPROD //</span>
<a name="l00218"></a>00218 <span class="comment"></span>
<a name="l00219"></a>00219 
<a name="l00221"></a>00221   <span class="comment">// NORM1 //</span>
<a name="l00222"></a>00222 
<a name="l00223"></a>00223 
<a name="l00224"></a>00224   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Storage1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00225"></a>00225   T1 <a class="code" href="namespace_seldon.php#afefce2ae4cd3147f863955efddc4184c" title="Returns the 1-norm of a matrix.">Norm1</a>(<span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X);
<a name="l00226"></a>00226 
<a name="l00227"></a>00227   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Storage1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00228"></a>00228   T1 <a class="code" href="namespace_seldon.php#afefce2ae4cd3147f863955efddc4184c" title="Returns the 1-norm of a matrix.">Norm1</a>(<span class="keyword">const</span> Vector&lt;complex&lt;T1&gt;, Storage1, Allocator1&gt;&amp; X);
<a name="l00229"></a>00229 
<a name="l00230"></a>00230   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00231"></a>00231   T1 <a class="code" href="namespace_seldon.php#afefce2ae4cd3147f863955efddc4184c" title="Returns the 1-norm of a matrix.">Norm1</a>(<span class="keyword">const</span> Vector&lt;T1, VectSparse, Allocator1&gt;&amp; X);
<a name="l00232"></a>00232 
<a name="l00233"></a>00233   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00234"></a>00234   T1 <a class="code" href="namespace_seldon.php#afefce2ae4cd3147f863955efddc4184c" title="Returns the 1-norm of a matrix.">Norm1</a>(<span class="keyword">const</span> Vector&lt;complex&lt;T1&gt;, VectSparse, Allocator1&gt;&amp; X);
<a name="l00235"></a>00235 
<a name="l00236"></a>00236 
<a name="l00237"></a>00237   <span class="comment">// NORM1 //</span>
<a name="l00239"></a>00239 <span class="comment"></span>
<a name="l00240"></a>00240 
<a name="l00242"></a>00242   <span class="comment">// NORM2 //</span>
<a name="l00243"></a>00243 
<a name="l00244"></a>00244 
<a name="l00245"></a>00245   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Storage1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00246"></a>00246   T1 Norm2(<span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X);
<a name="l00247"></a>00247 
<a name="l00248"></a>00248   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Storage1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00249"></a>00249   T1 Norm2(<span class="keyword">const</span> Vector&lt;complex&lt;T1&gt;, Storage1, Allocator1&gt;&amp; X);
<a name="l00250"></a>00250 
<a name="l00251"></a>00251   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00252"></a>00252   T1 Norm2(<span class="keyword">const</span> Vector&lt;T1, VectSparse, Allocator1&gt;&amp; X);
<a name="l00253"></a>00253 
<a name="l00254"></a>00254   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00255"></a>00255   T1 Norm2(<span class="keyword">const</span> Vector&lt;complex&lt;T1&gt;, VectSparse, Allocator1&gt;&amp; X);
<a name="l00256"></a>00256 
<a name="l00257"></a>00257 
<a name="l00258"></a>00258   <span class="comment">// NORM2 //</span>
<a name="l00260"></a>00260 <span class="comment"></span>
<a name="l00261"></a>00261 
<a name="l00263"></a>00263   <span class="comment">// GETMAXABSINDEX //</span>
<a name="l00264"></a>00264 
<a name="l00265"></a>00265 
<a name="l00266"></a>00266   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00267"></a>00267   <span class="keywordtype">int</span> GetMaxAbsIndex(<span class="keyword">const</span> Vector&lt;T, Storage, Allocator&gt;&amp; X);
<a name="l00268"></a>00268 
<a name="l00269"></a>00269 
<a name="l00270"></a>00270   <span class="comment">// GETMAXABSINDEX //</span>
<a name="l00272"></a>00272 <span class="comment"></span>
<a name="l00273"></a>00273 
<a name="l00275"></a>00275   <span class="comment">// APPLYROT //</span>
<a name="l00276"></a>00276 
<a name="l00277"></a>00277 
<a name="l00278"></a>00278   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00279"></a>00279   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a280476959b456ad5aa0aab55257f5c03" title="Computation of rotation between two points.">GenRot</a>(T&amp; a_in, T&amp; b_in, T&amp; c_, T&amp; s_);
<a name="l00280"></a>00280 
<a name="l00281"></a>00281   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00282"></a>00282   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a280476959b456ad5aa0aab55257f5c03" title="Computation of rotation between two points.">GenRot</a>(complex&lt;T&gt;&amp; a_in, complex&lt;T&gt;&amp; b_in, T&amp; c_, complex&lt;T&gt;&amp; s_);
<a name="l00283"></a>00283 
<a name="l00284"></a>00284   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00285"></a>00285   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a5cb9b0f1114a738c5ecb66e2629a3155" title="Rotation of a point in 2-D.">ApplyRot</a>(T&amp; x, T&amp; y, <span class="keyword">const</span> T c_, <span class="keyword">const</span> T s_);
<a name="l00286"></a>00286 
<a name="l00287"></a>00287   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00288"></a>00288   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a5cb9b0f1114a738c5ecb66e2629a3155" title="Rotation of a point in 2-D.">ApplyRot</a>(complex&lt;T&gt;&amp; x, complex&lt;T&gt;&amp; y,
<a name="l00289"></a>00289                 <span class="keyword">const</span> T&amp; c_, <span class="keyword">const</span> complex&lt;T&gt;&amp; s_);
<a name="l00290"></a>00290 
<a name="l00291"></a>00291 
<a name="l00292"></a>00292   <span class="comment">// APPLYROT //</span>
<a name="l00294"></a>00294 <span class="comment"></span>
<a name="l00295"></a>00295 
<a name="l00297"></a>00297   <span class="comment">// CHECKDIM //</span>
<a name="l00298"></a>00298 
<a name="l00299"></a>00299 
<a name="l00300"></a>00300   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00301"></a>00301             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00302"></a>00302   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> Vector&lt;T0, Storage0, Allocator0&gt;&amp; X,
<a name="l00303"></a>00303                 <span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; Y,
<a name="l00304"></a>00304                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> op = <span class="stringliteral">&quot;X + Y&quot;</span>);
<a name="l00305"></a>00305 
<a name="l00306"></a>00306   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Allocator0,
<a name="l00307"></a>00307             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00308"></a>00308   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> Vector&lt;T0, Vect_Sparse, Allocator0&gt;&amp; X,
<a name="l00309"></a>00309                 <span class="keyword">const</span> Vector&lt;T1, Vect_Sparse, Allocator1&gt;&amp; Y,
<a name="l00310"></a>00310                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> op = <span class="stringliteral">&quot;X + Y&quot;</span>);
<a name="l00311"></a>00311 
<a name="l00312"></a>00312   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Allocator0,
<a name="l00313"></a>00313             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00314"></a>00314   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> Vector&lt;T0, Collection, Allocator0&gt;&amp; X,
<a name="l00315"></a>00315                 <span class="keyword">const</span> Vector&lt;T1, Collection, Allocator1&gt;&amp; Y,
<a name="l00316"></a>00316                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> op = <span class="stringliteral">&quot;X + Y&quot;</span>);
<a name="l00317"></a>00317 
<a name="l00318"></a>00318   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Allocator0, <span class="keyword">class </span>Allocator00,
<a name="l00319"></a>00319             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator11&gt;
<a name="l00320"></a>00320   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> Vector&lt;Vector&lt;T0, Vect_Sparse, Allocator0&gt;,
<a name="l00321"></a>00321                 Collection, Allocator00&gt;&amp; X,
<a name="l00322"></a>00322                 <span class="keyword">const</span> Vector&lt;Vector&lt;T1, Vect_Sparse, Allocator1&gt;,
<a name="l00323"></a>00323                 Collection, Allocator11&gt;&amp; Y,
<a name="l00324"></a>00324                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> op = <span class="stringliteral">&quot;X + Y&quot;</span>);
<a name="l00325"></a>00325 
<a name="l00326"></a>00326   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Allocator0,
<a name="l00327"></a>00327             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00328"></a>00328   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> Vector&lt;T0, VectFull, Allocator0&gt;&amp; X,
<a name="l00329"></a>00329                 Vector&lt;T1, Collection, Allocator1&gt;&amp; Y,
<a name="l00330"></a>00330                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> op = <span class="stringliteral">&quot;X + Y&quot;</span>);
<a name="l00331"></a>00331 
<a name="l00332"></a>00332   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U0&gt; <span class="keyword">class </span>Allocator0,
<a name="l00333"></a>00333             <span class="keyword">class </span>T1, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U1&gt; <span class="keyword">class </span>Allocator1&gt;
<a name="l00334"></a>00334   <span class="keywordtype">void</span>
<a name="l00335"></a>00335   <a class="code" href="namespace_seldon.php#ad45a73a30cc717a22119b829266bc180" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span>
<a name="l00336"></a>00336            Vector&lt;FloatDouble, DenseSparseCollection, Allocator0&lt;T0&gt; &gt;&amp; X,
<a name="l00337"></a>00337            <span class="keyword">const</span>
<a name="l00338"></a>00338            Vector&lt;FloatDouble, DenseSparseCollection, Allocator1&lt;T1&gt; &gt;&amp; Y,
<a name="l00339"></a>00339            <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> op = <span class="stringliteral">&quot;X + Y&quot;</span>);
<a name="l00340"></a>00340 
<a name="l00341"></a>00341 
<a name="l00342"></a>00342   <span class="comment">// CHECKDIM //</span>
<a name="l00344"></a>00344 <span class="comment"></span>
<a name="l00345"></a>00345 
<a name="l00347"></a>00347   <span class="comment">// CONJUGATE //</span>
<a name="l00348"></a>00348 
<a name="l00349"></a>00349 
<a name="l00350"></a>00350   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00351"></a>00351   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a90119cfc81b2e157c522a426a0167ccb" title="Sets a vector to its conjugate.">Conjugate</a>(Vector&lt;T, Prop, Allocator&gt;&amp; X);
<a name="l00352"></a>00352 
<a name="l00353"></a>00353   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00354"></a>00354   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a90119cfc81b2e157c522a426a0167ccb" title="Sets a vector to its conjugate.">Conjugate</a>(Vector&lt;T, VectSparse, Allocator&gt;&amp; X);
<a name="l00355"></a>00355 
<a name="l00356"></a>00356 
<a name="l00357"></a>00357   <span class="comment">// CONJUGATE //</span>
<a name="l00359"></a>00359 <span class="comment"></span>
<a name="l00360"></a>00360 
<a name="l00361"></a>00361 } <span class="comment">// namespace Seldon.</span>
<a name="l00362"></a>00362 
<a name="l00363"></a>00363 
<a name="l00364"></a>00364 <span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
