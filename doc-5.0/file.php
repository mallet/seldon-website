<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>

<?php $root='..';?>

<head>
<title><?php echo $_GET["name"]?></title>
<link rel="stylesheet" href="guide.css" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css"/>
<link href="prettify.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="prettify.js"></script>
</head>

<body onload=prettyPrint()>

<div class="page">

<?php include 'header.inc';?>

<div class="doc">

<?php $file=__FILE__; include 'menu.inc';?>

<div class="doccontent">

<?php echo '<code><pre class="prettyprint" style="font-size: 14px;">';
$content = file_get_contents("code/" . $_GET["name"]); echo
htmlentities($content); echo '</pre></code>' ?>

</div> <!-- doccontent -->

</div> <!-- doc -->

<?php include 'footer.inc'?>

</div>

</body>

</html>
