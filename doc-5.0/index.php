<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<title>Seldon user's guide</title>
<link rel="stylesheet" href="guide.css" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css"/>
</head>

<body>

<div class="page">

<?php include 'header.inc';?>

<div class="doc">

<?php $file=__FILE__; include 'menu.inc';?>

<div class="doccontent">

<h1>User's Guide for Seldon 5.0<br/>Introduction</h1>

<p> Seldon is a C++ library for linear algebra. It provides
different <b>matrix</b> and <b>vector</b> structures, and many
functions for computations (<b>linear algebra</b>). Seldon is designed
to be <b>efficient</b> and <b>convenient</b>, which is notably
achieved thanks to <i>template</i> classes. <i>Exception handling</i>
and several <i>debug levels</i> are helpful while coding.  </p>

<p> Seldon provides matrices for two main categories: dense matrices and
sparse matrices. Among dense matrices, there are specific structures for
rectangular matrices, symmetric matrices, hermitian matrices and triangular
matrices. Each type includes several formats. E.g., rectangular dense matrices
may be stored by rows or by columns; symmetric dense matrices may be stored as
rectangular matrices or only upper part of the matrix is stored (this is the
packed form of Blas format).</p>

<p> Seldon is interfaced
 with <b><a href="http://www.netlib.org/blas/">Blas</a></b> (levels 1, 2 and
 3) and <b><a href="http://www.netlib.org/lapack/">Lapack</a></b>, except for
 functions involving banded matrices (since this format is not available for
 the moment). If Blas is not available to the user, a few alternative
 functions (same functions written in C++) may be used.</p>

<p> For sparse matrices, Seldon is interfaced with direct solvers of
<b><a href="http://mumps.enseeiht.fr/">MUMPS</a></b>,
 <b><a href="http://crd.lbl.gov/~xiaoye/SuperLU/">SuperLU</a></b> and
 <b><a href="http://www.cise.ufl.edu/research/sparse/umfpack/">UmfPack</a></b>.
 There is a bunch of iterative solvers available in Seldon such as Gmres, 
BiCgSTAB, Qmr, etc. Thanks to templates, these solvers can be used for
 any type of matrix and preconditioning, not only Seldon matrices. This is
 very useful when the user does not store the matrix, but is able
 to perform a matrix-vector product.
Seldon includes many other features that are described in this
documentation.  </p>

<p>The library has a Python interface generated
by <a href="http://www.swig.org/">Swig</a>.</p>

<p> <b>Seldon is provided under the GNU Lesser General Public License
(LGPL).</b>
</p>

<br/><br/>

<div style="text-align: right;"> Seldon homepage: <a
href="http://seldon.sourceforge.net/">http://seldon.sourceforge.net/</a>.  </div>

</div> <!-- doccontent -->

</div> <!-- doc -->

<?php include 'footer.inc'?>

</div>

</body>

</html>
