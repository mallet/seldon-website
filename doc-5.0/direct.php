<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<title>Direct solvers in Seldon</title>
<link rel="stylesheet" href="guide.css" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css"/>
<link href="prettify.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="prettify.js"></script>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php include 'header.inc';?>

<div class="doc">

<?php $file=__FILE__; include 'menu.inc';?>

<div class="doccontent">

<h1>Direct solvers</h1>


<p>Seldon is interfaced with libraries performing the direct resolution
of very large sparse linear systems : <b><a href="http://mumps.enseeiht.fr/">MUMPS</a></b>,
 <b><a href="http://crd.lbl.gov/~xiaoye/SuperLU/">SuperLU</a></b> and
 <b><a href="http://www.cise.ufl.edu/research/sparse/umfpack/">UmfPack</a></b>. An
 example file is located in test/program/direct_test.cpp. If you want
 to test the interface with <b>MUMPS</b>, assuming that MUMPS has been
 compiled in directory <code>MumpsDir</code>, you can compile this file by
 typing :</p>


<pre class="fragment">g++ -DSELDON_WITH_MUMPS direct_test.cpp -IMumpsDir/include -IMumpsDir/libseq \
  -IMetisDir/Lib -LMumpsDir/lib -ldmumps -lzmumps -lmumps_common -lpord \
  -LMumpsDir/libseq -lmpiseq -LScotchDir/lib -lesmumps -lfax -lsymbol \
  -ldof -lorder -lgraph -lscotch -lscotcherr -lcommon \
  -lgfortran -lm -lpthread -LMetisDir -lmetis -llapack -lblas
</pre>


<p>You can simplify the last command, if you didn't install <a
href="http://glaros.dtc.umn.edu/gkhome/views/metis/">Metis</a> and 
<a href="http://www.labri.fr/perso/pelegrin/scotch/">Scotch</a> and
didn't compile MUMPS with those libraries. For <b>UmfPack</b>, if
<code>UmfPackDir</code> denotes the directory where UmfPack has been
installed, you have to type :</p>


<pre class="fragment">g++ -DSELDON_WITH_UMFPACK direct_test.cpp
  -IUmfPackDir/AMD/Include -IUmfPackDir/UMFPACK/Include \
  -IUmfPackDir/UMFPACK/UFconfig -LUmfPackDir/UMFPACK/Lib \
  -lumfpack -LUmfPackDir/AMD/Lib -lamd -llapack -lblas</pre>


<p>For <b>SuperLU</b>, the compilation line reads
(<code>SuperLUdir</code> is the directory where SuperLU is located) :</p>


<pre class="fragment">g++ -DSELDON_WITH_SUPERLU direct_test.cpp -ISuperLUdir/SRC -LSuperLUdir -lsuperlu</pre>


<p>All in all, <b>MUMPS</b> seems more efficient, and includes more
functionnalities than the other libraries.</p>


<h2>Syntax</h2>


<p>The syntax of all direct solvers is similar </p>


<pre class="syntax-box">
void GetLU(Matrix&amp;, MatrixMumps&amp;);
void SolveLU(MatrixMumps&amp;, Vector&amp;);
</pre>


<p>The interface has been done only for double precision (real or
complex numbers), since single precision is not accurate enough when
very large sparse linear systems are solved.</p>


<h2>Basic use</h2>


<p> We provide an example of direct resolution using SuperLU.</p>


<pre class="prettyprint">
// first we construct a sparse matrix
int n = 1000; // size of linear system
// we assume that you know how to fill arrays values, ptr, ind
Matrix&lt;double, General, RowSparse&gt; A(n, n, nnz, values, ptr, ind);

// then we declare vectors
Vector&lt;double&gt; b(n), x(n);

// you fill right-hand side
b.Fill();

// you perform the factorization (real matrix)
MatrixSuperLU&lt;double&gt; mat_lu;
GetLU(A, mat_lu);

// then you can solve as many linear systems as you want
x = b;
SolveLU(mat_lu, x);

</pre>


<h2>Methods of MatrixMumps :</h2>


<table class="category-table">

<tr class="category-table-tr-2">
<td class="category-table-td"> <a href="#clear"> Clear </a></td>
<td class="category-table-td"> Releases memory used by factorization </td> </tr>

<tr class="category-table-tr-1">
 <td class="category-table-td"> <a href="#initsymmetricmatrix">InitSymmetricMatrix</a></td>
<td class="category-table-td"> Informs the solver that the matrix is
 symmetric </td> </tr>

<tr class="category-table-tr-2">
 <td class="category-table-td"> <a href="#initsymmetricmatrix">InitUnSymmetricMatrix</a></td>
<td class="category-table-td"> Informs the solver that the matrix is
 unsymmetric </td> </tr>

<tr class="category-table-tr-1">
 <td class="category-table-td"> <a href="#selectordering">SelectOrdering</a></td>
<td class="category-table-td"> selects an ordering scheme</td> </tr>

<tr class="category-table-tr-2">
 <td class="category-table-td"> <a href="#messages">HideMessages</a></td>
<td class="category-table-td"> requires to the direct solver to
not display any message</td> </tr>

<tr class="category-table-tr-1">
 <td class="category-table-td"> <a href="#messages">ShowMessages</a></td>
<td class="category-table-td"> requires a normal display by the direct
 solver</td> </tr>

<tr class="category-table-tr-2">
 <td class="category-table-td"> <a href="#getinfofactorization">GetInfoFactorization</a></td>
<td class="category-table-td"> returns the error code generated by the factorization</td> </tr>

<tr class="category-table-tr-1">
 <td class="category-table-td"> <a href="#findordering">FindOrdering</a></td>
<td class="category-table-td"> computes a new ordering of rows and
 columns</td> </tr>

<tr class="category-table-tr-2">
 <td class="category-table-td"> <a href="#factorizematrix">FactorizeMatrix</a></td>
<td class="category-table-td"> performs LU factorization</td> </tr>

<tr class="category-table-tr-1">
 <td class="category-table-td"> <a href="#getschurmatrix">GetSchurMatrix</a></td>
<td class="category-table-td"> forms Schur complement</td> </tr>

<tr class="category-table-tr-2">
 <td class="category-table-td"> <a href="#solve">Solve</a></td>
<td class="category-table-td"> uses LU factorization to solve a linear
 system</td> </tr>

</table>


<h2>Methods of MatrixUmfPack :</h2>


<table class="category-table">

<tr class="category-table-tr-2">
<td class="category-table-td"> <a href="#clear"> Clear </a></td>
<td class="category-table-td"> Releases memory used by factorization </td> </tr>

<tr class="category-table-tr-1">
 <td class="category-table-td"> <a href="#messages">HideMessages</a></td>
<td class="category-table-td"> requires to the direct solver to
not display any message</td> </tr>

<tr class="category-table-tr-2">
 <td class="category-table-td"> <a href="#messages">ShowMessages</a></td>
<td class="category-table-td"> requires a normal display by the direct
 solver</td> </tr>

<tr class="category-table-tr-1">
 <td class="category-table-td"> <a href="#getinfofactorization">GetInfoFactorization</a></td>
<td class="category-table-td"> returns the error code generated by the factorization</td> </tr>

<tr class="category-table-tr-2">
 <td class="category-table-td"> <a href="#factorizematrix">FactorizeMatrix</a></td>
<td class="category-table-td"> performs LU factorization</td> </tr>

<tr class="category-table-tr-1">
 <td class="category-table-td"> <a href="#solve">Solve</a></td>
<td class="category-table-td"> uses LU factorization to solve a linear
 system</td> </tr>

</table>


<h2>Methods of MatrixSuperLU :</h2>


<table class="category-table">

<tr class="category-table-tr-2">
<td class="category-table-td"> <a href="#clear"> Clear </a></td>
<td class="category-table-td"> Releases memory used by factorization </td> </tr>

<tr class="category-table-tr-1">
 <td class="category-table-td"> <a href="#messages">HideMessages</a></td>
<td class="category-table-td"> requires to the direct solver to
not display any message</td> </tr>

<tr class="category-table-tr-2">
 <td class="category-table-td"> <a href="#messages">ShowMessages</a></td>
<td class="category-table-td"> requires a normal display by the direct
 solver</td> </tr>

<tr class="category-table-tr-1">
 <td class="category-table-td"> <a href="#getinfofactorization">GetInfoFactorization</a></td>
<td class="category-table-td"> returns the error code generated by the factorization</td> </tr>

<tr class="category-table-tr-2">
 <td class="category-table-td"> <a href="#factorizematrix">FactorizeMatrix</a></td>
<td class="category-table-td"> performs LU factorization</td> </tr>

<tr class="category-table-tr-1">
 <td class="category-table-td"> <a href="#solve">Solve</a></td>
<td class="category-table-td"> uses LU factorization to solve a linear
 system</td> </tr>

</table>


<h2>Functions :</h2>


<table class="category-table">

<tr class="category-table-tr-2">
<td class="category-table-td"> <a href="#getlu"> GetLU </a></td>
<td class="category-table-td"> performs a LU factorization </td> </tr>

<tr class="category-table-tr-1">
 <td class="category-table-td"> <a href="#solvelu">SolveLU</a></td>
<td class="category-table-td"> uses LU factorization to solve a linear
 system</td> </tr>

<tr class="category-table-tr-2">
 <td class="category-table-td"> <a href="#getschurmatrix_func">GetSchurMatrix</a></td>
<td class="category-table-td"> forms Schur complement</td> </tr>

</table>



<div class="separator"><a name="clear"></a></div>



<h3>Clear</h3>


<h4>Syntax :</h4>
 <pre class="syntax-box">
  void Clear();
</pre>


<p>This method releases the memory used by the factorisation. It is
available for every direct solver. A call to that method is necessary 
before asking for a new factorization.</p>


<h4> Example : </h4>
<pre class="prettyprint">
Matrix&lt;double, General, ArrayRowSparse&gt; A;
MatrixUmfPack&lt;double&gt; mat_lu;
// you fill A as you want
// then a first factorization is achieved
GetLU(A, mat_lu);
// you exploit this factorization
// you need to clear LU matrix before doing another factorization
mat_lu.Clear();
// you fill A with other values
// and refactorize
GetLU(A, mat_lu);
</pre>


<h4>Location :</h4>
<p><a href="file.php?name=computation/interfaces/direct/Mumps.cxx">Mumps.cxx</a><br/>
<a href="file.php?name=computation/interfaces/direct/UmfPack.cxx">UmfPack.cxx</a><br/>
<a href="file.php?name=computation/interfaces/direct/SuperLU.cxx">SuperLU.cxx</a></p>



<div class="separator"><a name="initsymmetricmatrix"></a></div>



<h3>InitSymmetricMatrix, InitUnSymmetricMatrix (only for MatrixMumps)</h3>


<h4>Syntax :</h4>
 <pre class="syntax-box">
  void InitSymmetricMatrix();
  void InitUnSymmetricMatrix();
</pre>


<p>These methods are used before <a
href="#factorizematrix">FactorizeMatrix</a> so that Mumps knows that
the input matrix is symmetric or not. UmfPack and SuperLU don't have that kind of method
since they don't have specific algorithms for symmetric matrices. If
you are using <a href="#getlu">GetLU</a> and <a href="#solvelu">SolveLU</a>, you don't need to call those methods.</p>


<h4>Location :</h4>
<p><a href="file.php?name=computation/interfaces/direct/Mumps.cxx">Mumps.cxx</a></p>



<div class="separator"><a name="selectordering"></a></div>



<h3>SelectOrdering (only for MatrixMumps) </h3>


<h4>Syntax :</h4>
 <pre class="syntax-box">
  void SelectOrdering(int);
</pre>


<p>You can force a specific ordering scheme to be used (Metis, Amd,
Scotch, etc). In the documentation of Mumps, you can find a list of
available orderings and the associated code.</p>


<h4>Example :</h4>
<pre class="prettyprint">
// you fill a sparse matrix
Matrix&lt;double, General, ArrayRowSparse&gt; A;
// you declare a Mumps structure
MatrixMumps&lt;double&gt; mat_lu;
// you change the default ordering scheme
mat_lu.SelectOrdering(2);
// then you can use getlu as usual
GetLU(A, mat_lu);
</pre>


<h4>Location :</h4>
<p><a href="file.php?name=computation/interfaces/direct/Mumps.cxx">Mumps.cxx</a></p>



<div class="separator"><a name="findordering"></a></div>



<h3>FindOrdering (only for MatrixMumps) </h3>


<h4>Syntax :</h4>
 <pre class="syntax-box">
  void FindOrdering(Matrix&amp;, Vector&lt;int&gt;&amp;);
  void FindOrdering(Matrix&amp;, Vector&lt;int&gt;&amp;, bool);
</pre>


<p>This method computes the new row numbers of the matrix by using
available algorithms in Mumps. </p>


<h4>Example :</h4>
<pre class="prettyprint">
// you fill a sparse matrix
Matrix&lt;double, General, ArrayRowSparse&gt; A;
// you declare a Mumps structure
MatrixMumps&lt;double&gt; mat_lu;
IVect permutation;
// we find the best numbering of A 
// by default, the matrix A is erased
mat_lu.FindOrdering(A, permutation);
// if last argument is true, A is not modified
mat_lu.FindOrdering(A, permutation, true);
</pre>


<h4>Location :</h4>
<p><a href="file.php?name=computation/interfaces/direct/Mumps.cxx">Mumps.cxx</a></p>



<div class="separator"><a name="messages"></a></div>



<h3>ShowMessages, HideMessages </h3>


<h4>Syntax :</h4>
 <pre class="syntax-box">
  void ShowMessages();
  void HideMessages();
</pre>


<p><code>ShowMessages</code> allows the direct solver to display informations about
the factorization and resolution phases, while <code>HideMessages</code> forbids any message to be displayed.</p>


<h4>Example :</h4>
<pre class="prettyprint">
// you fill a sparse matrix
Matrix&lt;double, General, ArrayRowSparse&gt; A;
// you declare a Mumps structure
MatrixMumps&lt;double&gt; mat_lu;
// then before GetLU, you can require silencious mode
mat_lu.HideMessages();
GetLU(A, mat_lu);
</pre>


<h4>Location :</h4>
<p><a href="file.php?name=computation/interfaces/direct/Mumps.cxx">Mumps.cxx</a><br/>
<a href="file.php?name=computation/interfaces/direct/UmfPack.cxx">UmfPack.cxx</a><br/>
<a href="file.php?name=computation/interfaces/direct/SuperLU.cxx">SuperLU.cxx</a></p>


<div class="separator"><a name="getinfofactorization"></a></div>



<h3>GetInfoFactorization </h3>


<h4>Syntax :</h4>
 <pre class="syntax-box">
  void GetInfoFactorization();
</pre>


<p>This method returns the error code provided by the used direct solver. </p>

<h4>Example :</h4>
<pre class="prettyprint">
// you fill a sparse matrix
Matrix&lt;double, General, ArrayRowSparse&gt; A;
// you declare a Mumps structure
MatrixMumps&lt;double&gt; mat_lu;
// you factorize the matrix
GetLU(A, mat_lu);
// to know if there is a problem
int info = mat_lu.GetInfoFactorization();
</pre>


<h4>Location :</h4>
<p><a href="file.php?name=computation/interfaces/direct/Mumps.cxx">Mumps.cxx</a><br/>
<a href="file.php?name=computation/interfaces/direct/UmfPack.cxx">UmfPack.cxx</a><br/>
<a href="file.php?name=computation/interfaces/direct/SuperLU.cxx">SuperLU.cxx</a></p>



<div class="separator"><a name="factorizematrix"></a></div>



<h3>FactorizeMatrix </h3>


<h4>Syntax :</h4>
 <pre class="syntax-box">
  void FactorizeMatrix(Matrix&amp;);
  void FactorizeMatrix(Matrix&amp;, bool);
</pre>


<p>This method factorizes the given matrix. It is better to use
<code>GetLU</code>, since <code>FactorizeMatrix</code> needs that
<code>InitSymmetricMatrix</code> is called before (for Mumps).</p>

<h4>Location :</h4>
<p><a href="file.php?name=computation/interfaces/direct/Mumps.cxx">Mumps.cxx</a><br/>
<a href="file.php?name=computation/interfaces/direct/UmfPack.cxx">UmfPack.cxx</a><br/>
<a href="file.php?name=computation/interfaces/direct/SuperLU.cxx">SuperLU.cxx</a></p>



<div class="separator"><a name="getschurmatrix"></a></div>



<h3>GetSchurMatrix (only for MatrixMumps)</h3>


<h4>Syntax :</h4>
 <pre class="syntax-box">
  void GetSchurMatrix(Matrix&amp;, Vector&lt;int&gt;&amp;, Matrix&amp;);
  void GetSchurMatrix(Matrix&amp;, Vector&lt;int&gt;&amp;, Matrix&amp;, bool);
</pre>


<p>This method computes the schur complement when a matrix and row
numbers of the Schur matrix are provided. It is better to use the
function <a href="#getschurmatrix_func">GetSchurMatrix</a> since
you don't need to call <code>InitSymmetricMatrix</code> before.
 </p>


<h4>Location :</h4>
<p><a href="file.php?name=computation/interfaces/direct/Mumps.cxx">Mumps.cxx</a></p>



<div class="separator"><a name="solve"></a></div>



<h3>Solve </h3>


<h4>Syntax :</h4>
 <pre class="syntax-box">
  void Solve(Vector&amp;);
</pre>


<p>This method computes the solution of <code>A x = b</code>, assuming
that <code>GetLU</code> has been called before to factorize matrix A.
 This is equivalent to use function <a href="#solvelu">SolveLU</a>.</p>

<h4>Location :</h4>
<p><a href="file.php?name=computation/interfaces/direct/Mumps.cxx">Mumps.cxx</a><br/>
<a href="file.php?name=computation/interfaces/direct/UmfPack.cxx">UmfPack.cxx</a><br/>
<a href="file.php?name=computation/interfaces/direct/SuperLU.cxx">SuperLU.cxx</a></p>



<div class="separator"><a name="getlu"></a></div>



<h3>GetLU</h3>

<h4>Syntax :</h4>
 <pre class="syntax-box">
  void GetLU(Matrix&amp;, MatrixMumps&amp;);
  void GetLU(Matrix&amp;, MatrixUmfPack&amp;);
  void GetLU(Matrix&amp;, MatrixSuperLU&amp;);
  void GetLU(Matrix&amp;, MatrixMumps&amp;, bool);
  void GetLU(Matrix&amp;, MatrixUmfPack&amp;, bool);
  void GetLU(Matrix&amp;, MatrixSuperLU&amp;, bool);
</pre>


<p>This method performs a LU factorization. The last argument is
optional. When omitted, the initial matrix is erased, when equal to
true, the initial matrix is not modified.</p>


<h4>Example :</h4>
<pre class="prettyprint">
// sparse matrices, use of Mumps for example
MatrixMumps&lt;double&gt; mat_lu;
Matrix&lt;double, General, ArrayRowSparse&gt; Asp(n, n);
// you add all non-zero entries to matrix Asp
// then you call GetLU to factorize the matrix
GetLU(Asp, mat_lu);
// Asp is empty after GetLU
// you can solve Asp x = b 
X = B;
SolveLU(mat_lu, X);

// if you want to keep initial matrix
GetLU(Asp, mat_lu, true);
</pre>

<h4>Location :</h4>
<p><a href="file.php?name=computation/interfaces/direct/Mumps.cxx">Mumps.cxx</a><br/>
<a href="file.php?name=computation/interfaces/direct/UmfPack.cxx">UmfPack.cxx</a><br/>
<a href="file.php?name=computation/interfaces/direct/SuperLU.cxx">SuperLU.cxx</a></p>


<div class="separator"><a name="solvelu"></a></div>



<h3>SolveLU</h3>


<h4>Syntax :</h4>
 <pre class="syntax-box">
  void SolveLU(MatrixMumps&amp;, Vector&amp;);
  void SolveLU(MatrixUmfPack&amp;, Vector&amp;);
  void SolveLU(MatrixSuperLU&amp;, Vector&amp;);
</pre>


<p>This method uses the LU factorization computed by
<code>GetLU</code> in order to solve a linear system. The right hand
side is overwritten by the result.</p>


<h4>Location :</h4>
<p><a href="file.php?name=computation/interfaces/direct/Mumps.cxx">Mumps.cxx</a><br/>
<a href="file.php?name=computation/interfaces/direct/UmfPack.cxx">UmfPack.cxx</a><br/>
<a href="file.php?name=computation/interfaces/direct/SuperLU.cxx">SuperLU.cxx</a></p>



<div class="separator"><a name="getschurmatrix_func"></a></div>



<h3>GetSchurMatrix (only for MatrixMumps)</h3>


<h4>Syntax :</h4>
 <pre class="syntax-box">
  void GetSchurMatrix(Matrix&amp;, MatrixMumps&amp;, Vector&lt;int&gt;&amp;, Matrix&amp;);
</pre>


<p>This method computes the so-called Schur matrix (or Schur
complement) from a given matrix. </p>


<h4>Example :</h4>
<pre class="prettyprint">
MatrixMumps&lt;double&gt; mat_lu;
Matrix&lt;double, General, ArrayRowSparse&gt; A(n, n);
// you add all non-zero entries to matrix A
// then you specify row numbers for schur matrix
IVect num(5); 
num.Fill();
// somehow, A can be written under the form A = [A11 A12; A21 A22]
// A22 is related to row numbers of the Schur matrix
// Schur matrix is dense
Matrix&lt;double&gt; mat_schur(5, 5);
GetSchurMatrix(A, mat_lu, num, mat_schur);

// then you should obtain A22 - A21*inv(A11)*A12 -> mat_schur
</pre>


<h4>Location :</h4>
<p><a href="file.php?name=computation/interfaces/direct/Mumps.cxx">Mumps.cxx</a></p>



</div> <!-- doccontent -->

</div> <!-- doc -->

<?php include 'footer.inc'?>

</div>

</body>

</html>
