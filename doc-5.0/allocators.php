<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<title>Seldon user's guide</title>
<link rel="stylesheet" href="guide.css" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css"/>
<link href="prettify.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="prettify.js"></script>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php include 'header.inc';?>

<div class="doc">

<?php $file=__FILE__; include 'menu.inc';?>

<div class="doccontent">

<h1>Allocators</h1>

<p>Allocators are used to allocate and deallocate memory. The last
template argument of vectors and matrices is the allocator. For a
vector: <code>Vector&lt;double, Vect_Full, CallocAlloc&lt;double&gt;
&gt;</code>. <code>CallocAlloc</code> is an allocator based on
<code>calloc</code>. <code>MallocAlloc</code> is another allocator
based on <code>malloc</code>. The third available allocator is
<code>NewAlloc</code>, based on <code>new</code>. The last one is
<code>NaNAlloc</code>, based on <code>malloc</code> and which
initializes allocated elements to "not a number". If a vector or a
matrix managed by <code>NaNAlloc</code> is not properly filled, there
will still be NaNs in the vector or the matrix, which is easy to
detect.</p>

<p>The default allocator is <code>MallocAlloc</code>. The default
allocator may be changed thanks to
<code>SELDON_DEFAULT_ALLOCATOR</code>:</p>

<p><code>#define SELDON_DEFAULT_ALLOCATOR NewAlloc</code> defines
<code>NewAlloc</code> as the default allocator. This line must be put
before <code>Seldon.hxx</code> is included.
We strongly encourage beginner to set <code>NewAlloc</code> as the default allocator
 since only this allocator will call constructors of objects contained in the vector.
 This property is essential when the elements of the vector are C++ classes.
</p>

<pre class="prettyprint">
#define SELDON_DEFAULT_ALLOCATOR NewAlloc
#include "Seldon.hxx"

using namespace Seldon;

// For vector containing integers, MallocAlloc is okay
Vector&lt;int, Vect_Full, MallocAlloc&lt;int&gt; &gt; X;

// For vector containing vectors, NewAlloc is needed
Vector&lt;Vector&lt;double&gt; &gt; Xvec;
</pre>


</div> <!-- doccontent -->

</div> <!-- doc -->

<?php include 'footer.inc'?>

</div>

</body>

</html>
