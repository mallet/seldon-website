<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<title>Seldon user's guide</title>
<link rel="stylesheet" href="guide.css" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css"/>
<link href="prettify.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="prettify.js"></script>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php include 'header.inc';?>

<div class="doc">

<?php $file=__FILE__; include 'menu.inc';?>

<div class="doccontent">

<h1>Installation</h1>

<h2>The compiler</h2>

<p> Seldon is supposed to be fully compliant with the <b>C++
standard</b>. Therefore, it can be compiled by GNU GCC (>=3.0; tested with
version 3.2, 3.3, 3.4, 4.0, 4.1, 4.2, 4.3 and 4.4) and by the Intel C++ compiler
icc (tested with icc 7.1 and 8.0). No tests were conducted with proprietary
compilers under Unix, but the compliance with the C++ standard should ensure
portability. Decent versions of Microsoft Visual C++ (i.e., from Visual C++
2003) compile Seldon.  </p>

<h2>Installation</h2>

<p>Download the source code (<a
href="http://seldon.sourceforge.net/">Seldon
homepage</a>), usually available in a compressed file,
e.g. Seldon-[version].tar.bz2. Uncompress the file, i.e. under Unix:
<code>bunzip2 Seldon-[version].tar.bz2</code> followed by <code>tar
-xvf Seldon-[version].tar</code>. This will create the directory
<code>Seldon-[version]</code> in which you will find Seldon.</p>

<h2>Installation of CBlas</h2>

<p>If you want to use the interface with Blas, you also need to have
Blas (of course) and CBlas (C interface to the Blas) installed. Seldon
uses CBlas to ensure portability of the interface.</p>

<p>Blas is usually installed. If not, you may refer to the dedicated
documentation. As for CBlas, download it at <a
href="http://www.netlib.org/blas/">http://www.netlib.org/blas/</a>, expand it
(e.g. <code>tar -zxvf cblas.tgz</code>), and compile it (e.g. make all).
 The installation is then complete.</p>

<h2>Tests</h2>

<p>First compile and run <code>test.cpp</code> which is an example provided with
Seldon (in the directory test/program). For example:</p>

<pre class="fragment">g++ -I../.. test.cpp &amp;&amp; ./a.out</pre>

<p>This should return:</p>

<pre class="prettyprint">
Seldon: compilation test
Vector: 0       1       2
Vector: 0       1       2       19
</pre>

<p>In <code>test.cpp</code>, the first line includes Seldon through
<code>Seldon.hxx</code>. <code>Seldon.hxx</code> is the only file to
be included and it is located in the directory
<code>Seldon-[version]</code> expanded from the downloaded file (see
section Installation). If you compile your own code on top of
Seldon, just provide the path to <code>Seldon-[version]</code> to the
compiler, e.g.:</p>

<pre class="fragment">g++ -I/path/to/Seldon-[version] your_code.cpp &amp;&amp;
./a.out</pre>

<p>A second test is provided: <code>test_Blas.cpp</code>. If Blas  and
 CBlas are properly installed and the library file of CBlas is located 
 in <code>[CBlas-library]</code> (e.g. [CBlas-directory]/lib/LINUX/cblas_LINUX.a),
 the compilation line should be:</p>

<pre class="fragment">g++ -I../.. test_Blas.cpp [CBlas-library] -lblas -lg2c &amp;&amp; ./a.out</pre>

<p>This should return:</p>

<pre class="prettyprint">
Seldon: compilation test with Blas
U = 1.3 1.3     1.3
V = 0   1       2
2. * U + V = 2.6     3.6     4.6
</pre>

<p>If Cblas is already installed in your system or if you installed it by using 
apt-get on linux or fink on mac, you may type the following compilation line:</p>

<pre class="fragment">g++ -I../.. test_Blas.cpp -lcblas -lblas -lg2c &amp;&amp; ./a.out</pre>

</div> <!-- doccontent -->

</div> <!-- doc -->

<?php include 'footer.inc'?>

</div>

</body>

</html>
