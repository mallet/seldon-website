<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($file, ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <?php HL($file, "index", "Introduction");?> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "class_vector", "Dense Vectors");?> </li> 
<li class="jelly"> <?php HL($file, "class_sparse_vector", "Sparse Vectors");?> </li>
<li class="jelly"> <?php HL($file, "functions_vector", "Functions");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "class_matrix", "Dense Matrices");?> </li> 
<li class="jelly"> <?php HL($file, "class_sparse_matrix", "Sparse Matrices");?> </li>
<li class="jelly"> <?php HL($file, "functions_matrix", "Functions");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "array3d", "3D&nbsp;Arrays");?>  </li>
<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "functions_blas", "Blas");?> </li> 
<li class="jelly"> <?php HL($file, "functions_lapack", "Lapack");?> </li>
<li class="jelly"> <?php HL($file, "direct", "Direct Solvers");?> </li> 
<li class="jelly"> <?php HL($file, "iterative", "Iterative
Solvers");?> </li> </ul> </li>
<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>
