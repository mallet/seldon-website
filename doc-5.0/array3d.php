<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<title>Seldon user's guide</title>
<link rel="stylesheet" href="guide.css" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css"/>
<link href="prettify.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="prettify.js"></script>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php include 'header.inc';?>

<div class="doc">

<?php $file=__FILE__; include 'menu.inc';?>

<div class="doccontent">

<h1>3D arrays</h1>

<h2>Definition</h2>

<p> 3D arrays are instances of the class <code>Array3D</code>. Class
<code>Array3D</code> is a template class: <code>Array3D&lt;T,
Allocator&gt;</code>. As for vectors, <code>T</code> is the type of
the elements to be stored
(e.g. <code>double</code>). <code>Allocator</code> defines the way
memory is managed. It is close to STL allocators. See the section
"Allocators" for further details.  </p>

<h2>Declaration</h2>

<p> There is a default <code>Allocator</code> (see the section
"Allocators"). It means that the last template parameter may be
omitted. Then a 3D array of integers may be declared thanks to the
line: </p>

<pre class="prettyprint">Array3D&lt;int&gt; A;</pre>

<p> This defines an array of size 0 x 0 x 0, that is to say an empty
array. To define a 3D array of size 5 x 3 x 6, one may write: </p>

<pre class="prettyprint">Array3D&lt;int&gt; A(5, 3, 6);</pre>

<h2>Use of 3D arrays</h2>

<p> Only a few methods are available for 3D arrays because they are
not the main concern of Seldon. Mainly, the access to elements is
achieved through the <code>operator(int, int, int)</code>, and indices
start at 0: </p>

<pre class="prettyprint">
Array3D&lt;double&gt; A(5, 6, 3);
A(0, 1, 3) = -3.1;
A(0, 0, 5) = 1.2 * A(0, 1, 3);
</pre>

<p> One may point out some methods:</p>

<ul>

  <li> <code>GetLength1()</code>, <code>GetLength2()</code> and
  <code>GetLength3()</code> return lengths in dimensions #1, #2 and
  #3.</li>
  
  <li> <code>Fill</code> fills with 0, 1, 2, 3, etc. or fills the array
  with a given value.</li>
  
  <li> <code>Reallocate</code> resizes the array (warning, data may be
  lost, depending on the allocator).</li>
  
  <li> <code>Copy</code> enables to duplicate an array.</li>
  
</ul>

A comprehensive test of this class is done in file <code>test/program/array3d_test.cpp</code>.

</div> <!-- doccontent -->

</div> <!-- doc -->

<?php include 'footer.inc'?>

</div>

</body>

</html>
