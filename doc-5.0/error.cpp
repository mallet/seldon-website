#define SELDON_WITH_ABORT
#define SELDON_DEBUG_LEVEL_4 // which checks bounds.
#include "Seldon.hxx"
using namespace Seldon;

int main(int argc, char** argv)
{
  TRY;

  IVect V(3);
  cout << "Bad access: " << V(3) << endl;

  END;

  return 0;
}
