<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<title>Seldon user's guide</title>
<link rel="stylesheet" href="guide.css" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css"/>
<link href="prettify.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="prettify.js"></script>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php include 'header.inc';?>

<div class="doc">

<?php $file=__FILE__; include 'menu.inc';?>

<div class="doccontent">

<h1>Exceptions</h1>

<h2>Options</h2>

<p>Exceptions are raised if an error is detected. Seldon is able to
check for several mistakes:</p>

<ul>

  <li>SELDON_CHECK_IO: checks input/output operations on disk.</li>

  <li>SELDON_CHECK_MEMORY: checks memory allocations and
  deallocations.</li>

  <li>SELDON_CHECK_DIMENSIONS: checks that the dimensions of involved
  structures are compatible. Notice that there are methods in which
  the compatibility is not checked however.</li>

  <li>SELDON_CHECK_BOUNDS: checks that indices are not out of
  range.</li>

</ul>

<p>To enable <code>SELDON_CHECK_MEMORY</code>, for example, put
(before <code>#include &lt;Seldon.hxx&gt;</code>):</p>

<pre class="prettyprint">#define SELDON_CHECK_MEMORY</pre>

<p>Alternatively, there are debug levels:</p>

<ul>

  <li>SELDON_DEBUG_LEVEL_0: nothing is checked.</li>

  <li>SELDON_DEBUG_LEVEL_1: equivalent to SELDON_CHECK_IO plus
  SELDON_CHECK_MEMORY.</li>

  <li>SELDON_DEBUG_LEVEL_2: equivalent to SELDON_DEBUG_LEVEL_1 plus
  SELDON_CHECK_DIMENSIONS.</li>

  <li>SELDON_DEBUG_LEVEL_3: equivalent to SELDON_DEBUG_LEVEL_2 plus
  SELDON_CHECK_BOUNDS.</li>

  <li>SELDON_DEBUG_LEVEL_4: equivalent to SELDON_DEBUG_LEVEL_3.</li>

</ul>

<p>In practice, it is advocated to choose SELDON_DEBUG_LEVEL_4 in the
development stage and SELDON_DEBUG_LEVEL_2 for the stable
version. Indeed SELDON_DEBUG_LEVEL_4 slows down the program but checks
many things and SELDON_DEBUG_LEVEL_2 should not slow down the program
and ensures that it is reasonably safe.</p>

<p>Development stage:</p>
<pre class="prettyprint">#define SELDON_DEBUG_LEVEL_4</pre>

<p>Stable version:</p>
<pre class="prettyprint">#define SELDON_DEBUG_LEVEL_2</pre>

<h2>Exceptions raised</h2>

<p>The objects that may be launched by Seldon are of type:
<code>WrongArgument</code>, <code>NoMemory</code>, <code>WrongDim</code>,
<code>WrongIndex</code>, <code>WrongRow</code>, <code>WrongCol</code>,
<code>IOError</code> and <code>LapackError</code>. They all derive from
<code>Error</code>. They provide the method <code>What</code> that returns a
string explaining the error, and the method <code>CoutWhat</code> that
displays on screen this explanation. Therefore, to catch an exception raised
by Seldon, put:</p>

<pre class="prettyprint">catch(Seldon::Error&amp; Err)
{
Err.CoutWhat();
}</pre>

<p>Two macros are defined to help:</p>

<p><code>TRY</code>:</p>

<pre class="prettyprint">#define TRY try {</pre>

<p><code>END</code>:</p>

<pre class="prettyprint">#define END                                                     \
  }                                                             \
    catch(Seldon::Error& Err)                                   \
      {                                                         \
        Err.CoutWhat();                                         \
        return 1;                                               \
      }                                                         \
    catch (std::exception& Err)                                 \
      {                                                         \
        cout << "C++ exception: " << Err.what() << endl;        \
        return 1;                                               \
      }                                                         \
    catch (std::string& str)                                    \
      {                                                         \
        cout << str << endl;                                    \
        return 1;                                               \
      }                                                         \
    catch (const char* str)                                     \
      {                                                         \
        cout << str << endl;                                    \
        return 1;                                               \
      }                                                         \
    catch(...)                                                  \
      {                                                         \
        cout << "Unknown exception..." << endl;                 \
        return 1;                                               \
      }
</pre>

<p>It is advocated that you enclose your code (in the <code>main</code>
function) with <code>TRY;</code> and <code>END;</code>:</p>

<pre class="prettyprint">
int main(int argc, char** argv)
{
  TRY;

  // Here goes your code.

  END;

  return 0;
}
</pre>

<h2>Exceptions and debugging</h2>

<p>Suppose your code contains an error and raises an exception. You probably
want to identify the function that raised the exception. The error message
should contain the name of the function. But you probably want to know the
exact line where the error occurred and the sequence of calls. Then, you have
two options, using a debugger.</p>

<p>One option is to place a breakpoint in <code>Error::Error(string function =
"", string comment = "")</code> (see file <a
href="file.php?name=share/Errors.cxx">share/Errors.cxx</a>) because this
constructor should be called before the exception is actually raised.</p>

<p>Another option, more convenient because no breakpoint is to be placed, is
to define <code>SELDON_WITH_ABORT</code>. With that flag activated, if a
Seldon exception is raised, the program will simply abort. The call stack is
then at hand. See the example below, using gdb under Linux. The program <a
href="error.cpp">error.cpp</a> demonstrates the technique:</p>

<?php echo '<pre class="prettyprint">'; $content =
file_get_contents("error.cpp"); echo htmlentities($content); echo '</pre>' ?>

<p>The error and the calls sequence are easy to obtain:</p>

<?php echo '<pre class="fragment">'; $content =
file_get_contents("error.log"); echo htmlentities($content); echo '</pre>' ?>

<h2>Notes</h2>

<p>Apart from exceptions, two useful macros are defined to ease the debugging
activities:</p>

<p><code>ERR</code>:</p>

<pre class="prettyprint">#define ERR(x) cout &lt;&lt; "Hermes - " #x &lt;&lt; endl</pre>

<p><code>DISP</code>:</p>

<pre class="prettyprint">DISP(x) cout &lt;&lt; #x ": " &lt;&lt; x &lt;&lt; endl</pre>

<p>In a code:</p>

<pre class="prettyprint">ERR(5);
ERR(5a);
int i = 7;
DISP(i + 1);
ERR(Last line);</pre>

<p>returns on screen:</p>

<pre class="fragment">Hermes - 5
Hermes - 5a
i + 1: 8
Hermes - Last line
</pre>

</div> <!-- doccontent -->

</div> <!-- doc -->

<?php include 'footer.inc'?>

</div>

</body>

</html>
