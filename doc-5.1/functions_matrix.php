<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Functions for Matrices </h1>  </div>
</div>
<div class="contents">
<p>In that page, we detail functions that are not related to <a href="functions_lapack.php">Lapack</a> </p>
<table  class="category-table">
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#transpose">Transpose </a> </td><td class="category-table-td">replaces a matrix by its transpose   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#transpose">TransposeConj </a> </td><td class="category-table-td">replaces a matrix by its conjugate transpose   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#setrow">SetRow </a> </td><td class="category-table-td">modifies a row of the matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getrow">GetRow </a> </td><td class="category-table-td">extracts a row from the matrix  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#setcol">SetCol </a> </td><td class="category-table-td">modifies a column of the matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getcol">GetCol </a> </td><td class="category-table-td">extracts a column from the matrix   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#maxabs">MaxAbs </a> </td><td class="category-table-td">returns highest absolute value of A   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#norm1">Norm1 </a> </td><td class="category-table-td">returns 1-norm of A   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#norminf">NormInf </a> </td><td class="category-table-td">returns infinity-norm of A   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#permutematrix">ApplyInversePermutation </a> </td><td class="category-table-td">permutes row and column numbers of a matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#scalematrix">ScaleMatrix </a> </td><td class="category-table-td">multiplies rows and columns of a matrix by coefficients   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#scaleleftmatrix">ScaleLeftMatrix </a> </td><td class="category-table-td">multiplies rows of a matrix by coefficients  </td></tr>
</table>
<div class="separator"><a class="anchor" id="transpose"></a></div><h3>Transpose, TransposeConj</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void Transpose(Matrix&amp;);
  void TransposeConj(Matrix&amp;);
</pre><p><code>Transpose</code> overwrites a matrix by its transpose, while <code>TransposeConj</code> overwrites a matrix by its conjugate transpose. These functions are only available for dense matrices.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(5, 5);
A.Fill();

Transpose(A);

Matrix&lt;complex&lt;double&gt; &gt; B(5, 5);
// you fill B as you want, then overwrites it by conj(transpose(B))
TransposeConj(B);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___matrix_8cxx_source.php">Functions_Matrix.cxx</a></p>
<div class="separator"><a class="anchor" id="setrow"></a></div><h3>SetRow</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void SetRow(const Vector&amp;, int, Matrix&amp;);
</pre><p>This function modifies a row in the provided matrix. This function is available only for dense matrices.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(5, 5);
A.Fill();

// now you construct a row
Vector&lt;double&gt; row(5);
row.FillRand();

// and you put it in A
int irow = 1;
SetRow(row, irow, A);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions_8cxx_source.php">Functions.cxx</a></p>
<div class="separator"><a class="anchor" id="getrow"></a></div><h3>GetRow</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void GetRow(const Matrix&amp;, int, Vector&amp;);
</pre><p>This function extracts a row from the provided matrix. This function is available only for dense matrices.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(5, 5);
A.Fill();

// now you extract the first row
Vector&lt;double&gt; row;
GetRow(A, 0, row);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions_8cxx_source.php">Functions.cxx</a></p>
<div class="separator"><a class="anchor" id="setcol"></a></div><h3>SetCol</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void SetCol(const Vector&amp;, int, Matrix&amp;);
</pre><p>This function modifies a column in the provided matrix. This function is available only for dense matrices.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(5, 5);
A.Fill();

// now you construct a column
Vector&lt;double&gt; col(5);
col.FillRand();

// and you put it in A
int icol = 1;
SetCol(col, icol, A);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions_8cxx_source.php">Functions.cxx</a></p>
<div class="separator"><a class="anchor" id="getcol"></a></div><h3>GetCol</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void GetCol(const Matrix&amp;, int, Vector&amp;);
</pre><p>This function extracts a column from the provided matrix. This function is available only for dense matrices.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(5, 5);
A.Fill();

// now you extract the first column
Vector&lt;double&gt; col;
GetCol(A, 0, col);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions_8cxx_source.php">Functions.cxx</a></p>
<div class="separator"><a class="anchor" id="maxabs"></a></div><h3>MaxAbs</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  T MaxAbs(const Matrix&lt;T&gt;&amp;);
</pre><p>This function returns the highest absolute value of a matrix.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;complex&lt;double&gt; &gt; A(5, 5);
A.Fill();

double module_max = MaxAbs(A);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___matrix_8cxx_source.php">Functions_Matrix.cxx</a></p>
<div class="separator"><a class="anchor" id="norm1"></a></div><h3>Norm1</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  T Norm1(const Matrix&lt;T&gt;&amp;);
</pre><p>This function returns the 1-norm of a matrix.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;complex&lt;double&gt; &gt; A(5, 5);
A.Fill();

double anorm_one = Norm1(A);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___matrix_8cxx_source.php">Functions_Matrix.cxx</a></p>
<div class="separator"><a class="anchor" id="norminf"></a></div><h3>NormInf</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  T NormInf(const Matrix&lt;T&gt;&amp;);
</pre><p>This function returns the infinite norm of a matrix.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;complex&lt;double&gt; &gt; A(5, 5);
A.Fill();

double anorm_inf = NormInf(A);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___matrix_8cxx_source.php">Functions_Matrix.cxx</a></p>
<div class="separator"><a class="anchor" id="permutematrix"></a></div><h3>ApplyInversePermutation</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void ApplyInversePermutation(Matrix&amp;, const Vector&lt;int&gt;&amp;, const Vector&lt;int&gt;&amp;);
</pre><p>This function permutes a given matrix with the provided new row numbers and column numbers. <code>ApplyInversePermutation(A, row, col)</code> does the same operation as <code>A(row, col) = A</code> in Matlab. This function is only available for RowMajor, ColMajor, ArrayRowSparse, ArrayRowSymSparse, ArrayRowComplexSparse and ArrayRowSymComplexSparse. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// you fill A as you wish
Matrix&lt;double, Symmetric, ArrayRowSymSparse&gt; A(5, 5);
// then new row and column numbers
IVect row(5);
// for symmetric matrix, row and column permutation must be the same
// if you want to keep symmetry
ApplyInversePermutation(A, row, row);

// for unsymmetric matrices, you can specify different permutations
IVect col(5);
Matrix&lt;double, General, ArrayRowSparse&gt; B(5, 5);
ApplyInversePermutation(B, row, col);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_permutation___scaling_matrix_8cxx_source.php">Permutation_ScalingMatrix.cxx</a></p>
<div class="separator"><a class="anchor" id="scalematrix"></a></div><h3>ScaleMatrix</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void ScaleMatrix(Matrix&amp;, const Vector&amp;, const Vector&amp;);
</pre><p>This function multiplies each row and column with a coefficient, i.e. <code>A</code> is replaced by <code>L*A*R</code> where <code>L</code> and <code>R</code> are diagonal matrices and you provide the diagonal when you call <code>ScaleMatrix</code>. This function is only available for ArrayRowSparse, ArrayRowSymSparse, ArrayRowComplexSparse and ArrayRowSymComplexSparse. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// you fill A as you wish
Matrix&lt;double, Symmetric, ArrayRowSymSparse&gt; A(5, 5);
// then scaling vectors
Vector&lt;double&gt; scale(5);
// for symmetric matrix, row and column scaling must be the same
// if you want to keep symmetry
ScaleMatrix(A, scale, scale);

// for unsymmetric matrices, you can specify different scalings
IVect scale_right(5);
Matrix&lt;double, General, ArrayRowSparse&gt; B(5, 5);
ApplyInversePermutation(B, scale, scale_right);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_permutation___scaling_matrix_8cxx_source.php">Permutation_ScalingMatrix.cxx</a></p>
<div class="separator"><a class="anchor" id="scaleleftmatrix"></a></div><h3>ScaleLeftMatrix</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void ScaleLeftMatrix(Matrix&amp;, const Vector&amp;);
</pre><p>This function multiplies each row with a coefficient, i.e. <code>A</code> is replaced by <code>L*A</code> where <code>L</code> is diagonal and you provide the diagonal when you call <code>ScaleLeftMatrix</code>. This function is only available for ArrayRowSparse, and ArrayRowComplexSparse. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// you fill A as you wish
Matrix&lt;double, General, ArrayRowSparse&gt; A(5, 5);
// then scaling vector
Vector&lt;double&gt; scale(5);
ScaleLeftMatrix(A, scale);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_permutation___scaling_matrix_8cxx_source.php">Permutation_ScalingMatrix.cxx</a> </p>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
