<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix_sparse/Functions_MatrixArray.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_FUNCTIONS_MATRIX_ARRAY_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="comment">/*</span>
<a name="l00024"></a>00024 <span class="comment">  Functions defined in this file:</span>
<a name="l00025"></a>00025 <span class="comment">  (storage ArrayRowSparse, ArrayColSparse, etc)</span>
<a name="l00026"></a>00026 <span class="comment"></span>
<a name="l00027"></a>00027 <span class="comment">  alpha.M*X + beta.Y -&gt; Y</span>
<a name="l00028"></a>00028 <span class="comment">  MltAdd(alpha, M, X, beta, Y)</span>
<a name="l00029"></a>00029 <span class="comment"></span>
<a name="l00030"></a>00030 <span class="comment">  alpha.A + B -&gt; B</span>
<a name="l00031"></a>00031 <span class="comment">  Add(alpha, A, B)</span>
<a name="l00032"></a>00032 <span class="comment"></span>
<a name="l00033"></a>00033 <span class="comment">  alpha.M -&gt; M</span>
<a name="l00034"></a>00034 <span class="comment">  Mlt(alpha, M)</span>
<a name="l00035"></a>00035 <span class="comment"></span>
<a name="l00036"></a>00036 <span class="comment">*/</span>
<a name="l00037"></a>00037 
<a name="l00038"></a>00038 <span class="keyword">namespace </span>Seldon
<a name="l00039"></a>00039 {
<a name="l00040"></a>00040 
<a name="l00041"></a>00041 
<a name="l00043"></a>00043   <span class="comment">// MltAdd //</span>
<a name="l00044"></a>00044 
<a name="l00045"></a>00045 
<a name="l00046"></a>00046   <span class="comment">/*** ArrayRowSymComplexSparse ***/</span>
<a name="l00047"></a>00047 
<a name="l00048"></a>00048 
<a name="l00049"></a>00049   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>T2, <span class="keyword">class </span>T3, <span class="keyword">class </span>T4,
<a name="l00050"></a>00050            <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00051"></a>00051   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0&amp; alpha, <span class="keyword">const</span> Matrix&lt;T1, Symmetric,
<a name="l00052"></a>00052               ArrayRowSymComplexSparse, Allocator1&gt;&amp; A,
<a name="l00053"></a>00053               <span class="keyword">const</span> Vector&lt;T2, VectFull, Allocator2&gt;&amp; B,
<a name="l00054"></a>00054               <span class="keyword">const</span> T4&amp; beta,
<a name="l00055"></a>00055               Vector&lt;T3, VectFull, Allocator3&gt;&amp; C)
<a name="l00056"></a>00056   {
<a name="l00057"></a>00057     <span class="keywordflow">if</span> (beta == T4(0))
<a name="l00058"></a>00058       C.Fill(T3(0));
<a name="l00059"></a>00059     <span class="keywordflow">else</span>
<a name="l00060"></a>00060       <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, C);
<a name="l00061"></a>00061 
<a name="l00062"></a>00062     <span class="keywordtype">int</span> m = A.GetM(), n, p;
<a name="l00063"></a>00063     T1 val;
<a name="l00064"></a>00064     T3 val_cplx;
<a name="l00065"></a>00065     <span class="keywordflow">if</span> (alpha == T0(1))
<a name="l00066"></a>00066       {
<a name="l00067"></a>00067         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00068"></a>00068           {
<a name="l00069"></a>00069             n = A.GetRealRowSize(i);
<a name="l00070"></a>00070             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00071"></a>00071               {
<a name="l00072"></a>00072                 p = A.IndexReal(i, k);
<a name="l00073"></a>00073                 val = A.ValueReal(i, k);
<a name="l00074"></a>00074                 <span class="keywordflow">if</span> (p == i)
<a name="l00075"></a>00075                   C(i) += val * B(i);
<a name="l00076"></a>00076                 <span class="keywordflow">else</span>
<a name="l00077"></a>00077                   {
<a name="l00078"></a>00078                     C(i) += val * B(p);
<a name="l00079"></a>00079                     C(p) += val * B(i);
<a name="l00080"></a>00080                   }
<a name="l00081"></a>00081               }
<a name="l00082"></a>00082             n = A.GetImagRowSize(i);
<a name="l00083"></a>00083             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00084"></a>00084               {
<a name="l00085"></a>00085                 p = A.IndexImag(i, k);
<a name="l00086"></a>00086                 val = A.ValueImag(i, k);
<a name="l00087"></a>00087                 <span class="keywordflow">if</span> (p == i)
<a name="l00088"></a>00088                   C(i) += complex&lt;T1&gt;(0, val) * B(i);
<a name="l00089"></a>00089                 <span class="keywordflow">else</span>
<a name="l00090"></a>00090                   {
<a name="l00091"></a>00091                     C(i) += complex&lt;T1&gt;(0, val) * B(p);
<a name="l00092"></a>00092                     C(p) += complex&lt;T1&gt;(0, val) * B(i);
<a name="l00093"></a>00093                   }
<a name="l00094"></a>00094               }
<a name="l00095"></a>00095           }
<a name="l00096"></a>00096       }
<a name="l00097"></a>00097     <span class="keywordflow">else</span> <span class="comment">// alpha != 1.</span>
<a name="l00098"></a>00098       {
<a name="l00099"></a>00099         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00100"></a>00100           {
<a name="l00101"></a>00101             n = A.GetRealRowSize(i);
<a name="l00102"></a>00102             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00103"></a>00103               {
<a name="l00104"></a>00104                 p = A.IndexReal(i, k);
<a name="l00105"></a>00105                 val_cplx = alpha * A.ValueReal(i, k);
<a name="l00106"></a>00106                 <span class="keywordflow">if</span> (p == i)
<a name="l00107"></a>00107                   C(i) += val_cplx * B(i);
<a name="l00108"></a>00108                 <span class="keywordflow">else</span>
<a name="l00109"></a>00109                   {
<a name="l00110"></a>00110                     C(i) += val_cplx * B(p);
<a name="l00111"></a>00111                     C(p) += val_cplx * B(i);
<a name="l00112"></a>00112                   }
<a name="l00113"></a>00113               }
<a name="l00114"></a>00114             n = A.GetImagRowSize(i);
<a name="l00115"></a>00115             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00116"></a>00116               {
<a name="l00117"></a>00117                 p = A.IndexImag(i, k);
<a name="l00118"></a>00118                 val_cplx = alpha * complex&lt;T1&gt;(0, A.ValueImag(i, k));
<a name="l00119"></a>00119                 <span class="keywordflow">if</span> (p == i)
<a name="l00120"></a>00120                   C(i) += val_cplx * B(i);
<a name="l00121"></a>00121                 <span class="keywordflow">else</span>
<a name="l00122"></a>00122                   {
<a name="l00123"></a>00123                     C(i) += val_cplx * B(p);
<a name="l00124"></a>00124                     C(p) += val_cplx * B(i);
<a name="l00125"></a>00125                   }
<a name="l00126"></a>00126               }
<a name="l00127"></a>00127           }
<a name="l00128"></a>00128       }
<a name="l00129"></a>00129   }
<a name="l00130"></a>00130 
<a name="l00131"></a>00131 
<a name="l00132"></a>00132   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>T2, <span class="keyword">class </span>T3, <span class="keyword">class </span>T4,
<a name="l00133"></a>00133            <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00134"></a>00134   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l00135"></a>00135               <span class="keyword">const</span> class_SeldonNoTrans&amp; Trans, <span class="keyword">const</span> Matrix&lt;T1, Symmetric,
<a name="l00136"></a>00136               ArrayRowSymComplexSparse, Allocator1&gt;&amp; A,
<a name="l00137"></a>00137               <span class="keyword">const</span> Vector&lt;T2, VectFull, Allocator2&gt;&amp; B,
<a name="l00138"></a>00138               <span class="keyword">const</span> T4&amp; beta,
<a name="l00139"></a>00139               Vector&lt;T3, VectFull, Allocator3&gt;&amp; C)
<a name="l00140"></a>00140   {
<a name="l00141"></a>00141     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, A, B, beta, C);
<a name="l00142"></a>00142   }
<a name="l00143"></a>00143 
<a name="l00144"></a>00144 
<a name="l00145"></a>00145   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>T2, <span class="keyword">class </span>T3, <span class="keyword">class </span>T4,
<a name="l00146"></a>00146            <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00147"></a>00147   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l00148"></a>00148               <span class="keyword">const</span> class_SeldonTrans&amp; Trans, <span class="keyword">const</span> Matrix&lt;T1, Symmetric,
<a name="l00149"></a>00149               ArrayRowSymComplexSparse, Allocator1&gt;&amp; A,
<a name="l00150"></a>00150               <span class="keyword">const</span> Vector&lt;T2, VectFull, Allocator2&gt;&amp; B,
<a name="l00151"></a>00151               <span class="keyword">const</span> T4&amp; beta,
<a name="l00152"></a>00152               Vector&lt;T3, VectFull, Allocator3&gt;&amp; C)
<a name="l00153"></a>00153   {
<a name="l00154"></a>00154     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, A, B, beta, C);
<a name="l00155"></a>00155   }
<a name="l00156"></a>00156 
<a name="l00157"></a>00157 
<a name="l00158"></a>00158   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>T2, <span class="keyword">class </span>T3, <span class="keyword">class </span>T4,
<a name="l00159"></a>00159            <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00160"></a>00160   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l00161"></a>00161               <span class="keyword">const</span> class_SeldonConjTrans&amp; Trans, <span class="keyword">const</span> Matrix&lt;T1, Symmetric,
<a name="l00162"></a>00162               ArrayRowSymComplexSparse, Allocator1&gt;&amp; A,
<a name="l00163"></a>00163               <span class="keyword">const</span> Vector&lt;T2, VectFull, Allocator2&gt;&amp; B,
<a name="l00164"></a>00164               <span class="keyword">const</span> T4&amp; beta,
<a name="l00165"></a>00165               Vector&lt;T3, VectFull, Allocator3&gt;&amp; C)
<a name="l00166"></a>00166   {
<a name="l00167"></a>00167     <span class="keywordflow">if</span> (beta == T4(0))
<a name="l00168"></a>00168       C.Fill(T3(0));
<a name="l00169"></a>00169     <span class="keywordflow">else</span>
<a name="l00170"></a>00170       <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, C);
<a name="l00171"></a>00171     <span class="keywordtype">int</span> m = A.GetM(),n,p;
<a name="l00172"></a>00172     T1 val;
<a name="l00173"></a>00173     T3 val_cplx;
<a name="l00174"></a>00174     <span class="keywordflow">if</span> (alpha == T0(1))
<a name="l00175"></a>00175       {
<a name="l00176"></a>00176         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00177"></a>00177           {
<a name="l00178"></a>00178             n = A.GetRealRowSize(i);
<a name="l00179"></a>00179             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00180"></a>00180               {
<a name="l00181"></a>00181                 p = A.IndexReal(i, k);
<a name="l00182"></a>00182                 val = A.ValueReal(i, k);
<a name="l00183"></a>00183                 <span class="keywordflow">if</span> (p == i)
<a name="l00184"></a>00184                   C(i) += val * B(i);
<a name="l00185"></a>00185                 <span class="keywordflow">else</span>
<a name="l00186"></a>00186                   {
<a name="l00187"></a>00187                     C(i) += val * B(p);
<a name="l00188"></a>00188                     C(p) += val * B(i);
<a name="l00189"></a>00189                   }
<a name="l00190"></a>00190               }
<a name="l00191"></a>00191             n = A.GetImagRowSize(i);
<a name="l00192"></a>00192             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00193"></a>00193               {
<a name="l00194"></a>00194                 p = A.IndexImag(i, k);
<a name="l00195"></a>00195                 val = A.ValueImag(i, k);
<a name="l00196"></a>00196                 <span class="keywordflow">if</span> (p == i)
<a name="l00197"></a>00197                   C(i) -= complex&lt;T1&gt;(0, val) * B(i);
<a name="l00198"></a>00198                 <span class="keywordflow">else</span>
<a name="l00199"></a>00199                   {
<a name="l00200"></a>00200                     C(i) -= complex&lt;T1&gt;(0, val) * B(p);
<a name="l00201"></a>00201                     C(p) -= complex&lt;T1&gt;(0, val) * B(i);
<a name="l00202"></a>00202                   }
<a name="l00203"></a>00203               }
<a name="l00204"></a>00204           }
<a name="l00205"></a>00205       }
<a name="l00206"></a>00206     <span class="keywordflow">else</span>
<a name="l00207"></a>00207       {
<a name="l00208"></a>00208         <span class="comment">// alpha different from 1</span>
<a name="l00209"></a>00209         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00210"></a>00210           {
<a name="l00211"></a>00211             n = A.GetRealRowSize(i);
<a name="l00212"></a>00212             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00213"></a>00213               {
<a name="l00214"></a>00214                 p = A.IndexReal(i, k);
<a name="l00215"></a>00215                 val_cplx = alpha * A.ValueReal(i, k);
<a name="l00216"></a>00216                 <span class="keywordflow">if</span> (p == i)
<a name="l00217"></a>00217                   C(i) += val_cplx * B(i);
<a name="l00218"></a>00218                 <span class="keywordflow">else</span>
<a name="l00219"></a>00219                   {
<a name="l00220"></a>00220                     C(i) += val_cplx * B(p);
<a name="l00221"></a>00221                     C(p) += val_cplx * B(i);
<a name="l00222"></a>00222                   }
<a name="l00223"></a>00223               }
<a name="l00224"></a>00224             n = A.GetImagRowSize(i);
<a name="l00225"></a>00225             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00226"></a>00226               {
<a name="l00227"></a>00227                 p = A.IndexImag(i, k);
<a name="l00228"></a>00228                 val_cplx = alpha * complex&lt;T1&gt;(0, A.ValueImag(i, k));
<a name="l00229"></a>00229                 <span class="keywordflow">if</span> (p == i)
<a name="l00230"></a>00230                   C(i) -= val_cplx * B(i);
<a name="l00231"></a>00231                 <span class="keywordflow">else</span>
<a name="l00232"></a>00232                   {
<a name="l00233"></a>00233                     C(i) -= val_cplx * B(p);
<a name="l00234"></a>00234                     C(p) -= val_cplx * B(i);
<a name="l00235"></a>00235                   }
<a name="l00236"></a>00236               }
<a name="l00237"></a>00237           }
<a name="l00238"></a>00238       }
<a name="l00239"></a>00239   }
<a name="l00240"></a>00240 
<a name="l00241"></a>00241 
<a name="l00242"></a>00242   <span class="comment">/*** ArrayRowComplexSparse ***/</span>
<a name="l00243"></a>00243 
<a name="l00244"></a>00244 
<a name="l00245"></a>00245   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>T2, <span class="keyword">class </span>T3, <span class="keyword">class </span>T4,
<a name="l00246"></a>00246            <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00247"></a>00247   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0&amp; alpha, <span class="keyword">const</span> Matrix&lt;T1, General,
<a name="l00248"></a>00248               ArrayRowComplexSparse, Allocator1&gt;&amp; A,
<a name="l00249"></a>00249               <span class="keyword">const</span> Vector&lt;T2, VectFull, Allocator2&gt;&amp; B,
<a name="l00250"></a>00250               <span class="keyword">const</span> T4&amp; beta,
<a name="l00251"></a>00251               Vector&lt;T3, VectFull, Allocator3&gt;&amp; C)
<a name="l00252"></a>00252   {
<a name="l00253"></a>00253     <span class="keywordflow">if</span> (beta == T4(0))
<a name="l00254"></a>00254       C.Fill(T3(0));
<a name="l00255"></a>00255     <span class="keywordflow">else</span>
<a name="l00256"></a>00256       <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, C);
<a name="l00257"></a>00257     <span class="keywordtype">int</span> m = A.GetM(), n, p;
<a name="l00258"></a>00258     T1 val;
<a name="l00259"></a>00259     T3 val_cplx;
<a name="l00260"></a>00260     <span class="keywordflow">if</span> (alpha == T0(1, 0))
<a name="l00261"></a>00261       {
<a name="l00262"></a>00262         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00263"></a>00263           {
<a name="l00264"></a>00264             n = A.GetRealRowSize(i);
<a name="l00265"></a>00265             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00266"></a>00266               {
<a name="l00267"></a>00267                 p = A.IndexReal(i, k);
<a name="l00268"></a>00268                 val = A.ValueReal(i, k);
<a name="l00269"></a>00269                 C(i) += val * B(p);
<a name="l00270"></a>00270               }
<a name="l00271"></a>00271             n = A.GetImagRowSize(i);
<a name="l00272"></a>00272             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00273"></a>00273               {
<a name="l00274"></a>00274                 p = A.IndexImag(i, k);
<a name="l00275"></a>00275                 val = A.ValueImag(i, k);
<a name="l00276"></a>00276                 C(i) += complex&lt;T1&gt;(0, val) * B(p);
<a name="l00277"></a>00277               }
<a name="l00278"></a>00278           }
<a name="l00279"></a>00279       }
<a name="l00280"></a>00280     <span class="keywordflow">else</span>
<a name="l00281"></a>00281       {
<a name="l00282"></a>00282         <span class="comment">// alpha different from 1</span>
<a name="l00283"></a>00283         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00284"></a>00284           {
<a name="l00285"></a>00285             n = A.GetRealRowSize(i);
<a name="l00286"></a>00286             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00287"></a>00287               {
<a name="l00288"></a>00288                 p = A.IndexReal(i, k);
<a name="l00289"></a>00289                 val_cplx = alpha * A.ValueReal(i, k);
<a name="l00290"></a>00290                 C(i) += val_cplx * B(p);
<a name="l00291"></a>00291               }
<a name="l00292"></a>00292             n = A.GetImagRowSize(i);
<a name="l00293"></a>00293             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00294"></a>00294               {
<a name="l00295"></a>00295                 p = A.IndexImag(i, k);
<a name="l00296"></a>00296                 val_cplx = alpha * complex&lt;T1&gt;(0, A.ValueImag(i, k));
<a name="l00297"></a>00297                 C(i) += val_cplx * B(p);
<a name="l00298"></a>00298               }
<a name="l00299"></a>00299           }
<a name="l00300"></a>00300       }
<a name="l00301"></a>00301   }
<a name="l00302"></a>00302 
<a name="l00303"></a>00303 
<a name="l00304"></a>00304   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>T2, <span class="keyword">class </span>T3, <span class="keyword">class </span>T4,
<a name="l00305"></a>00305            <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00306"></a>00306   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l00307"></a>00307               <span class="keyword">const</span> class_SeldonNoTrans&amp; Trans, <span class="keyword">const</span> Matrix&lt;T1, General,
<a name="l00308"></a>00308               ArrayRowComplexSparse, Allocator1&gt;&amp; A,
<a name="l00309"></a>00309               <span class="keyword">const</span> Vector&lt;T2, VectFull, Allocator2&gt;&amp; B,
<a name="l00310"></a>00310               <span class="keyword">const</span> T4&amp; beta,
<a name="l00311"></a>00311               Vector&lt;T3, VectFull, Allocator3&gt;&amp; C)
<a name="l00312"></a>00312   {
<a name="l00313"></a>00313     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, A, B, beta, C);
<a name="l00314"></a>00314   }
<a name="l00315"></a>00315 
<a name="l00316"></a>00316 
<a name="l00317"></a>00317   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>T2, <span class="keyword">class </span>T3, <span class="keyword">class </span>T4,
<a name="l00318"></a>00318            <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00319"></a>00319   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l00320"></a>00320               <span class="keyword">const</span> class_SeldonTrans&amp; Trans, <span class="keyword">const</span> Matrix&lt;T1, General,
<a name="l00321"></a>00321               ArrayRowComplexSparse, Allocator1&gt;&amp; A,
<a name="l00322"></a>00322               <span class="keyword">const</span> Vector&lt;T2, VectFull, Allocator2&gt;&amp; B,
<a name="l00323"></a>00323               <span class="keyword">const</span> T4&amp; beta,
<a name="l00324"></a>00324               Vector&lt;T3, VectFull, Allocator3&gt;&amp; C)
<a name="l00325"></a>00325   {
<a name="l00326"></a>00326     <span class="keywordflow">if</span> (beta == T4(0))
<a name="l00327"></a>00327       C.Fill(T3(0));
<a name="l00328"></a>00328     <span class="keywordflow">else</span>
<a name="l00329"></a>00329       <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, C);
<a name="l00330"></a>00330     <span class="keywordtype">int</span> m = A.GetM(),n,p;
<a name="l00331"></a>00331     T1 val;
<a name="l00332"></a>00332     T3 val_cplx;
<a name="l00333"></a>00333     <span class="keywordflow">if</span> (alpha == T0(1))
<a name="l00334"></a>00334       {
<a name="l00335"></a>00335         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00336"></a>00336           {
<a name="l00337"></a>00337             n = A.GetRealRowSize(i);
<a name="l00338"></a>00338             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00339"></a>00339               {
<a name="l00340"></a>00340                 p = A.IndexReal(i, k);
<a name="l00341"></a>00341                 val = A.ValueReal(i, k);
<a name="l00342"></a>00342                 C(p) += val * B(i);
<a name="l00343"></a>00343               }
<a name="l00344"></a>00344             n = A.GetImagRowSize(i);
<a name="l00345"></a>00345             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00346"></a>00346               {
<a name="l00347"></a>00347                 p = A.IndexImag(i, k);
<a name="l00348"></a>00348                 val = A.ValueImag(i, k);
<a name="l00349"></a>00349                 C(p) += complex&lt;T1&gt;(0, val) * B(i);
<a name="l00350"></a>00350               }
<a name="l00351"></a>00351           }
<a name="l00352"></a>00352       }
<a name="l00353"></a>00353     <span class="keywordflow">else</span>
<a name="l00354"></a>00354       {
<a name="l00355"></a>00355         <span class="comment">// alpha different from 1</span>
<a name="l00356"></a>00356         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00357"></a>00357           {
<a name="l00358"></a>00358             n = A.GetRealRowSize(i);
<a name="l00359"></a>00359             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00360"></a>00360               {
<a name="l00361"></a>00361                 p = A.IndexReal(i, k);
<a name="l00362"></a>00362                 val_cplx = alpha * A.ValueReal(i, k);
<a name="l00363"></a>00363                 C(p) += val_cplx * B(i);
<a name="l00364"></a>00364               }
<a name="l00365"></a>00365             n = A.GetImagRowSize(i);
<a name="l00366"></a>00366             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00367"></a>00367               {
<a name="l00368"></a>00368                 p = A.IndexImag(i, k);
<a name="l00369"></a>00369                 val_cplx = alpha * complex&lt;T1&gt;(0, A.ValueImag(i, k));
<a name="l00370"></a>00370                 C(p) += val_cplx * B(i);
<a name="l00371"></a>00371               }
<a name="l00372"></a>00372           }
<a name="l00373"></a>00373       }
<a name="l00374"></a>00374   }
<a name="l00375"></a>00375 
<a name="l00376"></a>00376 
<a name="l00377"></a>00377   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>T2, <span class="keyword">class </span>T3, <span class="keyword">class </span>T4,
<a name="l00378"></a>00378            <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00379"></a>00379   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l00380"></a>00380               <span class="keyword">const</span> class_SeldonConjTrans&amp; Trans, <span class="keyword">const</span> Matrix&lt;T1, General,
<a name="l00381"></a>00381               ArrayRowComplexSparse, Allocator1&gt;&amp; A,
<a name="l00382"></a>00382               <span class="keyword">const</span> Vector&lt;T2, VectFull, Allocator2&gt;&amp; B,
<a name="l00383"></a>00383               <span class="keyword">const</span> T4&amp; beta,
<a name="l00384"></a>00384               Vector&lt;T3, VectFull, Allocator3&gt;&amp; C)
<a name="l00385"></a>00385   {
<a name="l00386"></a>00386     <span class="keywordflow">if</span> (beta == T4(0))
<a name="l00387"></a>00387       C.Fill(T3(0));
<a name="l00388"></a>00388     <span class="keywordflow">else</span>
<a name="l00389"></a>00389       <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, C);
<a name="l00390"></a>00390     <span class="keywordtype">int</span> m = A.GetM(),n,p;
<a name="l00391"></a>00391     T1 val;
<a name="l00392"></a>00392     T3 val_cplx;
<a name="l00393"></a>00393     <span class="keywordflow">if</span> (alpha == T0(1))
<a name="l00394"></a>00394       {
<a name="l00395"></a>00395         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00396"></a>00396           {
<a name="l00397"></a>00397             n = A.GetRealRowSize(i);
<a name="l00398"></a>00398             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00399"></a>00399               {
<a name="l00400"></a>00400                 p = A.IndexReal(i, k);
<a name="l00401"></a>00401                 val = A.ValueReal(i, k);
<a name="l00402"></a>00402                 C(p) += val * B(i);
<a name="l00403"></a>00403               }
<a name="l00404"></a>00404             n = A.GetImagRowSize(i);
<a name="l00405"></a>00405             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00406"></a>00406               {
<a name="l00407"></a>00407                 p = A.IndexImag(i, k);
<a name="l00408"></a>00408                 val = A.ValueImag(i, k);
<a name="l00409"></a>00409                 C(p) -= complex&lt;T1&gt;(0, val) * B(i);
<a name="l00410"></a>00410               }
<a name="l00411"></a>00411           }
<a name="l00412"></a>00412       }
<a name="l00413"></a>00413     <span class="keywordflow">else</span>
<a name="l00414"></a>00414       {
<a name="l00415"></a>00415         <span class="comment">// alpha different from 1</span>
<a name="l00416"></a>00416         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00417"></a>00417           {
<a name="l00418"></a>00418             n = A.GetRealRowSize(i);
<a name="l00419"></a>00419             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00420"></a>00420               {
<a name="l00421"></a>00421                 p = A.IndexReal(i, k);
<a name="l00422"></a>00422                 val_cplx = alpha * A.ValueReal(i, k);
<a name="l00423"></a>00423                 C(p) += val_cplx * B(i);
<a name="l00424"></a>00424               }
<a name="l00425"></a>00425             n = A.GetImagRowSize(i);
<a name="l00426"></a>00426             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00427"></a>00427               {
<a name="l00428"></a>00428                 p = A.IndexImag(i, k);
<a name="l00429"></a>00429                 val_cplx -= alpha * complex&lt;T1&gt;(0, A.ValueImag(i, k));
<a name="l00430"></a>00430                 C(p) += val_cplx * B(i);
<a name="l00431"></a>00431               }
<a name="l00432"></a>00432           }
<a name="l00433"></a>00433       }
<a name="l00434"></a>00434   }
<a name="l00435"></a>00435 
<a name="l00436"></a>00436 
<a name="l00437"></a>00437   <span class="comment">/*** ArrayRowSymSparse ***/</span>
<a name="l00438"></a>00438 
<a name="l00439"></a>00439 
<a name="l00440"></a>00440   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>T2, <span class="keyword">class </span>T3, <span class="keyword">class </span>T4,
<a name="l00441"></a>00441            <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00442"></a>00442   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0&amp; alpha, <span class="keyword">const</span> Matrix&lt;T1, Symmetric,
<a name="l00443"></a>00443               ArrayRowSymSparse, Allocator1&gt;&amp; A,
<a name="l00444"></a>00444               <span class="keyword">const</span> Vector&lt;T2, VectFull, Allocator2&gt;&amp; B,
<a name="l00445"></a>00445               <span class="keyword">const</span> T4&amp; beta,
<a name="l00446"></a>00446               Vector&lt;T3, VectFull, Allocator3&gt;&amp; C)
<a name="l00447"></a>00447   {
<a name="l00448"></a>00448     <span class="keywordflow">if</span> (B.GetM() &lt;= 0)
<a name="l00449"></a>00449       <span class="keywordflow">return</span>;
<a name="l00450"></a>00450 
<a name="l00451"></a>00451     T3 zero(B(0));
<a name="l00452"></a>00452     zero *= 0;
<a name="l00453"></a>00453 
<a name="l00454"></a>00454     <span class="keywordflow">if</span> (beta == zero)
<a name="l00455"></a>00455       C.Fill(zero);
<a name="l00456"></a>00456     <span class="keywordflow">else</span>
<a name="l00457"></a>00457       <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, C);
<a name="l00458"></a>00458 
<a name="l00459"></a>00459     <span class="keywordtype">int</span> m = A.GetM(), n, p;
<a name="l00460"></a>00460     T3 val;
<a name="l00461"></a>00461     <span class="keywordflow">if</span> (alpha == T0(1))
<a name="l00462"></a>00462       {
<a name="l00463"></a>00463         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00464"></a>00464           {
<a name="l00465"></a>00465             n = A.GetRowSize(i);
<a name="l00466"></a>00466             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00467"></a>00467               {
<a name="l00468"></a>00468                 p = A.Index(i, k);
<a name="l00469"></a>00469                 val = A.Value(i, k);
<a name="l00470"></a>00470 
<a name="l00471"></a>00471                 <span class="keywordflow">if</span> (p == i)
<a name="l00472"></a>00472                   C(i) += val * B(i);
<a name="l00473"></a>00473                 <span class="keywordflow">else</span>
<a name="l00474"></a>00474                   {
<a name="l00475"></a>00475                     C(i) += val * B(p);
<a name="l00476"></a>00476                     C(p) += val * B(i);
<a name="l00477"></a>00477                   }
<a name="l00478"></a>00478               }
<a name="l00479"></a>00479           }
<a name="l00480"></a>00480       }
<a name="l00481"></a>00481     <span class="keywordflow">else</span>
<a name="l00482"></a>00482       {
<a name="l00483"></a>00483         <span class="comment">// alpha different from 1</span>
<a name="l00484"></a>00484         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00485"></a>00485           {
<a name="l00486"></a>00486             n = A.GetRowSize(i);
<a name="l00487"></a>00487             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00488"></a>00488               {
<a name="l00489"></a>00489                 p = A.Index(i, k);
<a name="l00490"></a>00490                 val = alpha * A.Value(i, k);
<a name="l00491"></a>00491 
<a name="l00492"></a>00492                 <span class="keywordflow">if</span> (p==i)
<a name="l00493"></a>00493                   C(i) += val * B(i);
<a name="l00494"></a>00494                 <span class="keywordflow">else</span>
<a name="l00495"></a>00495                   {
<a name="l00496"></a>00496                     C(i) += val * B(p);
<a name="l00497"></a>00497                     C(p) += val * B(i);
<a name="l00498"></a>00498                   }
<a name="l00499"></a>00499               }
<a name="l00500"></a>00500           }
<a name="l00501"></a>00501       }
<a name="l00502"></a>00502   }
<a name="l00503"></a>00503 
<a name="l00504"></a>00504 
<a name="l00505"></a>00505   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>T2, <span class="keyword">class </span>T3, <span class="keyword">class </span>T4,
<a name="l00506"></a>00506            <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00507"></a>00507   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l00508"></a>00508               <span class="keyword">const</span> class_SeldonNoTrans&amp; Trans, <span class="keyword">const</span> Matrix&lt;T1, Symmetric,
<a name="l00509"></a>00509               ArrayRowSymSparse, Allocator1&gt;&amp; A,
<a name="l00510"></a>00510               <span class="keyword">const</span> Vector&lt;T2, VectFull, Allocator2&gt;&amp; B,
<a name="l00511"></a>00511               <span class="keyword">const</span> T4&amp; beta,
<a name="l00512"></a>00512               Vector&lt;T3, VectFull, Allocator3&gt;&amp; C)
<a name="l00513"></a>00513   {
<a name="l00514"></a>00514     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, A, B, beta, C);
<a name="l00515"></a>00515   }
<a name="l00516"></a>00516 
<a name="l00517"></a>00517 
<a name="l00518"></a>00518   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>T2, <span class="keyword">class </span>T3, <span class="keyword">class </span>T4,
<a name="l00519"></a>00519            <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00520"></a>00520   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l00521"></a>00521               <span class="keyword">const</span> class_SeldonTrans&amp; Trans, <span class="keyword">const</span> Matrix&lt;T1, Symmetric,
<a name="l00522"></a>00522               ArrayRowSymSparse, Allocator1&gt;&amp; A,
<a name="l00523"></a>00523               <span class="keyword">const</span> Vector&lt;T2, VectFull, Allocator2&gt;&amp; B,
<a name="l00524"></a>00524               <span class="keyword">const</span> T4&amp; beta,
<a name="l00525"></a>00525               Vector&lt;T3, VectFull, Allocator3&gt;&amp; C)
<a name="l00526"></a>00526   {
<a name="l00527"></a>00527     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, A, B, beta, C);
<a name="l00528"></a>00528   }
<a name="l00529"></a>00529 
<a name="l00530"></a>00530 
<a name="l00531"></a>00531   <span class="comment">/*** ArrayRowSparse ***/</span>
<a name="l00532"></a>00532 
<a name="l00533"></a>00533 
<a name="l00534"></a>00534   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>T2, <span class="keyword">class </span>T3, <span class="keyword">class </span>T4,
<a name="l00535"></a>00535            <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00536"></a>00536   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l00537"></a>00537               <span class="keyword">const</span> Matrix&lt;T1, General, ArrayRowSparse, Allocator1&gt;&amp; A,
<a name="l00538"></a>00538               <span class="keyword">const</span> Vector&lt;T2, VectFull, Allocator2&gt;&amp; B,
<a name="l00539"></a>00539               <span class="keyword">const</span> T4&amp; beta,
<a name="l00540"></a>00540               Vector&lt;T3, VectFull, Allocator3&gt;&amp; C)
<a name="l00541"></a>00541   {
<a name="l00542"></a>00542     <span class="keywordflow">if</span> (beta == T4(0))
<a name="l00543"></a>00543       C.Fill(T3(0));
<a name="l00544"></a>00544     <span class="keywordflow">else</span>
<a name="l00545"></a>00545       <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, C);
<a name="l00546"></a>00546 
<a name="l00547"></a>00547     <span class="keywordtype">int</span> m = A.GetM(), n, p;
<a name="l00548"></a>00548     T1 val;
<a name="l00549"></a>00549     <span class="keywordflow">if</span> (alpha == T0(1))
<a name="l00550"></a>00550       {
<a name="l00551"></a>00551         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00552"></a>00552           {
<a name="l00553"></a>00553             n = A.GetRowSize(i);
<a name="l00554"></a>00554             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00555"></a>00555               {
<a name="l00556"></a>00556                 p = A.Index(i, k);
<a name="l00557"></a>00557                 val = A.Value(i, k);
<a name="l00558"></a>00558                 C(i) += val * B(p);
<a name="l00559"></a>00559               }
<a name="l00560"></a>00560           }
<a name="l00561"></a>00561       }
<a name="l00562"></a>00562     <span class="keywordflow">else</span> <span class="comment">// alpha != 1.</span>
<a name="l00563"></a>00563       {
<a name="l00564"></a>00564         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00565"></a>00565           {
<a name="l00566"></a>00566             n = A.GetRowSize(i);
<a name="l00567"></a>00567             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00568"></a>00568               {
<a name="l00569"></a>00569                 p = A.Index(i, k);
<a name="l00570"></a>00570                 val = A.Value(i, k);
<a name="l00571"></a>00571                 C(i) += alpha * val * B(p);
<a name="l00572"></a>00572               }
<a name="l00573"></a>00573           }
<a name="l00574"></a>00574       }
<a name="l00575"></a>00575   }
<a name="l00576"></a>00576 
<a name="l00577"></a>00577 
<a name="l00578"></a>00578   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>T2, <span class="keyword">class </span>T3, <span class="keyword">class </span>T4,
<a name="l00579"></a>00579            <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00580"></a>00580   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l00581"></a>00581               <span class="keyword">const</span> class_SeldonNoTrans&amp; Trans,
<a name="l00582"></a>00582               <span class="keyword">const</span> Matrix&lt;T1, General, ArrayRowSparse, Allocator1&gt;&amp; A,
<a name="l00583"></a>00583               <span class="keyword">const</span> Vector&lt;T2, VectFull, Allocator2&gt;&amp; B,
<a name="l00584"></a>00584               <span class="keyword">const</span> T4&amp; beta,
<a name="l00585"></a>00585               Vector&lt;T4, VectFull, Allocator3&gt;&amp; C)
<a name="l00586"></a>00586   {
<a name="l00587"></a>00587     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, A, B, beta, C);
<a name="l00588"></a>00588   }
<a name="l00589"></a>00589 
<a name="l00590"></a>00590 
<a name="l00591"></a>00591   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>T2, <span class="keyword">class </span>T3, <span class="keyword">class </span>T4,
<a name="l00592"></a>00592            <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00593"></a>00593   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l00594"></a>00594               <span class="keyword">const</span> class_SeldonTrans&amp; Trans,
<a name="l00595"></a>00595               <span class="keyword">const</span> Matrix&lt;T1, General, ArrayRowSparse, Allocator1&gt;&amp; A,
<a name="l00596"></a>00596               <span class="keyword">const</span> Vector&lt;T2, VectFull, Allocator2&gt;&amp; B,
<a name="l00597"></a>00597               <span class="keyword">const</span> T4&amp; beta,
<a name="l00598"></a>00598               Vector&lt;T3, VectFull, Allocator3&gt;&amp; C)
<a name="l00599"></a>00599   {
<a name="l00600"></a>00600     <span class="keywordflow">if</span> (beta == T4(0))
<a name="l00601"></a>00601       C.Fill(T3(0));
<a name="l00602"></a>00602     <span class="keywordflow">else</span>
<a name="l00603"></a>00603       <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, C);
<a name="l00604"></a>00604     <span class="keywordtype">int</span> m = A.GetM(), n, p;
<a name="l00605"></a>00605     T1 val;
<a name="l00606"></a>00606     <span class="keywordflow">if</span> (alpha == T0(1))
<a name="l00607"></a>00607       {
<a name="l00608"></a>00608         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00609"></a>00609           {
<a name="l00610"></a>00610             n = A.GetRowSize(i);
<a name="l00611"></a>00611             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00612"></a>00612               {
<a name="l00613"></a>00613                 p = A.Index(i, k);
<a name="l00614"></a>00614                 val = A.Value(i, k);
<a name="l00615"></a>00615                 C(p) += val * B(i);
<a name="l00616"></a>00616               }
<a name="l00617"></a>00617           }
<a name="l00618"></a>00618       }
<a name="l00619"></a>00619     <span class="keywordflow">else</span> <span class="comment">// alpha != 1.</span>
<a name="l00620"></a>00620       {
<a name="l00621"></a>00621         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00622"></a>00622           {
<a name="l00623"></a>00623             n = A.GetRowSize(i);
<a name="l00624"></a>00624             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; n ; k++)
<a name="l00625"></a>00625               {
<a name="l00626"></a>00626                 p = A.Index(i, k);
<a name="l00627"></a>00627                 val = A.Value(i, k);
<a name="l00628"></a>00628                 C(p) += alpha * val * B(i);
<a name="l00629"></a>00629               }
<a name="l00630"></a>00630           }
<a name="l00631"></a>00631       }
<a name="l00632"></a>00632   }
<a name="l00633"></a>00633 
<a name="l00634"></a>00634 
<a name="l00635"></a>00635   <span class="comment">// MltAdd //</span>
<a name="l00637"></a>00637 <span class="comment"></span>
<a name="l00638"></a>00638 
<a name="l00639"></a>00639 
<a name="l00641"></a>00641   <span class="comment">// Add //</span>
<a name="l00642"></a>00642 
<a name="l00643"></a>00643 
<a name="l00644"></a>00644   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> T1, <span class="keyword">class</span> T2, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00645"></a>00645   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l00646"></a>00646            <span class="keyword">const</span> Matrix&lt;T1, General, ArrayRowSparse, Allocator1&gt;&amp; A,
<a name="l00647"></a>00647            Matrix&lt;T2, General, ArrayRowSparse, Allocator2&gt;&amp; B)
<a name="l00648"></a>00648   {
<a name="l00649"></a>00649     <span class="keywordtype">int</span> m = B.GetM(),n;
<a name="l00650"></a>00650     Vector&lt;T2, VectFull, Allocator2&gt; value;
<a name="l00651"></a>00651     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00652"></a>00652       {
<a name="l00653"></a>00653         n = A.GetRowSize(i);
<a name="l00654"></a>00654         value.Reallocate(n);
<a name="l00655"></a>00655         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l00656"></a>00656           value(j) = T2(A.Value(i, j));
<a name="l00657"></a>00657 
<a name="l00658"></a>00658         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(alpha, value);
<a name="l00659"></a>00659         B.AddInteractionRow(i, n, A.GetIndex(i), value.GetData());
<a name="l00660"></a>00660       }
<a name="l00661"></a>00661   }
<a name="l00662"></a>00662 
<a name="l00663"></a>00663 
<a name="l00664"></a>00664   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> T1, <span class="keyword">class</span> T2, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00665"></a>00665   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l00666"></a>00666            <span class="keyword">const</span> Matrix&lt;T1, Symmetric, ArrayRowSymSparse, Allocator1&gt;&amp; A,
<a name="l00667"></a>00667            Matrix&lt;T2, Symmetric, ArrayRowSymSparse, Allocator2&gt;&amp; B)
<a name="l00668"></a>00668   {
<a name="l00669"></a>00669     <span class="keywordtype">int</span> m = B.GetM(),n;
<a name="l00670"></a>00670     Vector&lt;T2, VectFull, Allocator2&gt; value;
<a name="l00671"></a>00671     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00672"></a>00672       {
<a name="l00673"></a>00673         n = A.GetRowSize(i);
<a name="l00674"></a>00674         value.Reallocate(n);
<a name="l00675"></a>00675         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l00676"></a>00676           value(j) = A.Value(i, j);
<a name="l00677"></a>00677 
<a name="l00678"></a>00678         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(alpha, value);
<a name="l00679"></a>00679         B.AddInteractionRow(i, n, A.GetIndex(i), value.GetData());
<a name="l00680"></a>00680       }
<a name="l00681"></a>00681   }
<a name="l00682"></a>00682 
<a name="l00683"></a>00683 
<a name="l00684"></a>00684   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> T1, <span class="keyword">class</span> T2, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00685"></a>00685   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l00686"></a>00686            <span class="keyword">const</span> Matrix&lt;T1, Symmetric,
<a name="l00687"></a>00687            ArrayRowSymComplexSparse, Allocator1&gt;&amp; A,
<a name="l00688"></a>00688            Matrix&lt;T2, Symmetric, ArrayRowSymComplexSparse, Allocator2&gt;&amp; B)
<a name="l00689"></a>00689   {
<a name="l00690"></a>00690     <span class="keywordtype">int</span> m = B.GetM(), n, ni;
<a name="l00691"></a>00691     Vector&lt;complex&lt;T2&gt; &gt; value;
<a name="l00692"></a>00692     IVect index;
<a name="l00693"></a>00693     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00694"></a>00694       {
<a name="l00695"></a>00695         n = A.GetRealRowSize(i);
<a name="l00696"></a>00696         ni = A.GetImagRowSize(i);
<a name="l00697"></a>00697         value.Reallocate(n + ni);
<a name="l00698"></a>00698         index.Reallocate(n + ni);
<a name="l00699"></a>00699         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l00700"></a>00700           {
<a name="l00701"></a>00701             value(j) = A.ValueReal(i, j);
<a name="l00702"></a>00702             index(j) = A.IndexReal(i, j);
<a name="l00703"></a>00703           }
<a name="l00704"></a>00704 
<a name="l00705"></a>00705         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ni; j++)
<a name="l00706"></a>00706           {
<a name="l00707"></a>00707             value(j+n) = complex&lt;T2&gt;(0, 1) * A.ValueImag(i, j);
<a name="l00708"></a>00708             index(j+n) = A.IndexImag(i, j);
<a name="l00709"></a>00709           }
<a name="l00710"></a>00710 
<a name="l00711"></a>00711         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(alpha, value);
<a name="l00712"></a>00712         B.AddInteractionRow(i, n+ni, index, value);
<a name="l00713"></a>00713       }
<a name="l00714"></a>00714   }
<a name="l00715"></a>00715 
<a name="l00716"></a>00716 
<a name="l00717"></a>00717   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> T1, <span class="keyword">class</span> T2, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00718"></a>00718   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0&amp; alpha, <span class="keyword">const</span> Matrix&lt;T1, Symmetric,
<a name="l00719"></a>00719            ArrayRowSymComplexSparse, Allocator1&gt;&amp; A,
<a name="l00720"></a>00720            Matrix&lt;T2, Symmetric, ArrayRowSymSparse, Allocator2&gt;&amp; B)
<a name="l00721"></a>00721   {
<a name="l00722"></a>00722     <span class="keywordtype">int</span> m = B.GetM(), n;
<a name="l00723"></a>00723     Vector&lt;T2, VectFull, Allocator2&gt; value;
<a name="l00724"></a>00724     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00725"></a>00725       {
<a name="l00726"></a>00726         n = A.GetRealRowSize(i);
<a name="l00727"></a>00727         value.Reallocate(n);
<a name="l00728"></a>00728         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l00729"></a>00729           value(j) = A.ValueReal(i, j);
<a name="l00730"></a>00730 
<a name="l00731"></a>00731         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(alpha, value);
<a name="l00732"></a>00732         B.AddInteractionRow(i, n, A.GetRealInd(i), value.GetData());
<a name="l00733"></a>00733         n = A.GetImagRowSize(i);
<a name="l00734"></a>00734         value.Reallocate(n);
<a name="l00735"></a>00735         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l00736"></a>00736           value(j) = complex&lt;T1&gt;(0, 1) * A.ValueImag(i, j);
<a name="l00737"></a>00737 
<a name="l00738"></a>00738         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(alpha, value);
<a name="l00739"></a>00739         B.AddInteractionRow(i, n, A.GetImagInd(i), value.GetData());
<a name="l00740"></a>00740       }
<a name="l00741"></a>00741   }
<a name="l00742"></a>00742 
<a name="l00743"></a>00743 
<a name="l00744"></a>00744   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> T1, <span class="keyword">class</span> T2, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00745"></a>00745   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0&amp; alpha, <span class="keyword">const</span> Matrix&lt;T1, General,
<a name="l00746"></a>00746            ArrayRowComplexSparse, Allocator1&gt;&amp; A,
<a name="l00747"></a>00747            Matrix&lt;T2, Symmetric, ArrayRowSparse, Allocator2&gt;&amp; B)
<a name="l00748"></a>00748   {
<a name="l00749"></a>00749     <span class="keywordtype">int</span> m = B.GetM(),n;
<a name="l00750"></a>00750     Vector&lt;T2, VectFull, Allocator2&gt; value;
<a name="l00751"></a>00751     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l00752"></a>00752       {
<a name="l00753"></a>00753         n = A.GetRealRowSize(i);
<a name="l00754"></a>00754         value.Reallocate(n);
<a name="l00755"></a>00755         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l00756"></a>00756           value(j) = A.ValueReal(i, j);
<a name="l00757"></a>00757 
<a name="l00758"></a>00758         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(alpha, value);
<a name="l00759"></a>00759         B.AddInteractionRow(i, n, A.GetRealInd(i), value.GetData());
<a name="l00760"></a>00760         n = A.GetImagRowSize(i);
<a name="l00761"></a>00761         value.Reallocate(n);
<a name="l00762"></a>00762         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l00763"></a>00763           value(j) = complex&lt;T1&gt;(0, 1) * A.ValueImag(i, j);
<a name="l00764"></a>00764 
<a name="l00765"></a>00765         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(alpha, value);
<a name="l00766"></a>00766         B.AddInteractionRow(i, n, A.GetImagInd(i), value.GetData());
<a name="l00767"></a>00767       }
<a name="l00768"></a>00768   }
<a name="l00769"></a>00769 
<a name="l00770"></a>00770 
<a name="l00771"></a>00771   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> T1, <span class="keyword">class</span> T2, <span class="keyword">class</span> Allocator1,<span class="keyword">class</span> Allocator2&gt;
<a name="l00772"></a>00772   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0&amp; alpha, <span class="keyword">const</span> Matrix&lt;T1, General,
<a name="l00773"></a>00773            ArrayRowComplexSparse, Allocator1&gt;&amp; A,
<a name="l00774"></a>00774            Matrix&lt;T2, General, ArrayRowComplexSparse, Allocator2&gt;&amp; B)
<a name="l00775"></a>00775   {
<a name="l00776"></a>00776     <span class="keywordtype">int</span> m = B.GetM(), n, ni;
<a name="l00777"></a>00777     Vector&lt;complex&lt;T2&gt; &gt; value; IVect index;
<a name="l00778"></a>00778     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00779"></a>00779       {
<a name="l00780"></a>00780         n = A.GetRealRowSize(i);
<a name="l00781"></a>00781         ni = A.GetImagRowSize(i);
<a name="l00782"></a>00782         value.Reallocate(n + ni);
<a name="l00783"></a>00783         index.Reallocate(n + ni);
<a name="l00784"></a>00784         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l00785"></a>00785           {
<a name="l00786"></a>00786             value(j) = A.ValueReal(i, j);
<a name="l00787"></a>00787             index(j) = A.IndexReal(i, j);
<a name="l00788"></a>00788           }
<a name="l00789"></a>00789 
<a name="l00790"></a>00790         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ni; j++)
<a name="l00791"></a>00791           {
<a name="l00792"></a>00792             value(n+j) = complex&lt;T2&gt;(0, 1) * A.ValueImag(i, j);
<a name="l00793"></a>00793             index(n+j) = A.IndexImag(i, j);
<a name="l00794"></a>00794           }
<a name="l00795"></a>00795 
<a name="l00796"></a>00796         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(alpha, value);
<a name="l00797"></a>00797         B.AddInteractionRow(i, n+ni, index, value);
<a name="l00798"></a>00798       }
<a name="l00799"></a>00799   }
<a name="l00800"></a>00800 
<a name="l00801"></a>00801 
<a name="l00802"></a>00802   <span class="comment">// C = C + complex(A,B)</span>
<a name="l00803"></a>00803   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>T2, <span class="keyword">class </span>T3, <span class="keyword">class </span>Allocator1,
<a name="l00804"></a>00804            <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00805"></a>00805   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l00806"></a>00806            <span class="keyword">const</span> Matrix&lt;T1, General, ArrayRowSparse, Allocator1&gt;&amp; A,
<a name="l00807"></a>00807            <span class="keyword">const</span> Matrix&lt;T2, General, ArrayRowSparse, Allocator2&gt;&amp; B,
<a name="l00808"></a>00808            Matrix&lt;complex&lt;T3&gt;, General, ArrayRowSparse, Allocator3&gt;&amp; C)
<a name="l00809"></a>00809   {
<a name="l00810"></a>00810     <span class="keywordtype">int</span> m = B.GetM(),n1,n2,size_row;;
<a name="l00811"></a>00811     Vector&lt;complex&lt;T3&gt;, VectFull, Allocator3&gt; val_row;
<a name="l00812"></a>00812     IVect ind_row;
<a name="l00813"></a>00813     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00814"></a>00814       {
<a name="l00815"></a>00815         n1 = A.GetRowSize(i);
<a name="l00816"></a>00816         n2 = B.GetRowSize(i);
<a name="l00817"></a>00817         size_row = n1 + n2;
<a name="l00818"></a>00818         val_row.Reallocate(size_row);
<a name="l00819"></a>00819         ind_row.Reallocate(size_row);
<a name="l00820"></a>00820         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0 ; j &lt; n1 ; j++)
<a name="l00821"></a>00821           {
<a name="l00822"></a>00822             ind_row(j) = A.Index(i, j);
<a name="l00823"></a>00823             val_row(j) = alpha*complex&lt;T3&gt;(A.Value(i, j), 0);
<a name="l00824"></a>00824           }
<a name="l00825"></a>00825 
<a name="l00826"></a>00826         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0 ; j &lt; n2 ; j++)
<a name="l00827"></a>00827           {
<a name="l00828"></a>00828             ind_row(j+n1) = B.Index(i, j);
<a name="l00829"></a>00829             val_row(j+n1) = alpha * complex&lt;T3&gt;(B.Value(i, j));
<a name="l00830"></a>00830           }
<a name="l00831"></a>00831         
<a name="l00832"></a>00832         C.AddInteractionRow(i, size_row, ind_row, val_row);
<a name="l00833"></a>00833       }
<a name="l00834"></a>00834   }
<a name="l00835"></a>00835 
<a name="l00836"></a>00836 
<a name="l00837"></a>00837   <span class="comment">// C = C + complex(A,B)</span>
<a name="l00838"></a>00838   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>T2, <span class="keyword">class </span>T3,
<a name="l00839"></a>00839            <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00840"></a>00840   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l00841"></a>00841            <span class="keyword">const</span> Matrix&lt;T1, Symmetric, ArrayRowSymSparse, Allocator1&gt;&amp; A,
<a name="l00842"></a>00842            <span class="keyword">const</span> Matrix&lt;T2, Symmetric, ArrayRowSymSparse, Allocator2&gt;&amp; B,
<a name="l00843"></a>00843            Matrix&lt;complex&lt;T3&gt;, Symmetric, ArrayRowSymSparse, Allocator3&gt;&amp; C)
<a name="l00844"></a>00844   {
<a name="l00845"></a>00845     <span class="keywordtype">int</span> m = B.GetM(), n1, n2, size_row;
<a name="l00846"></a>00846     Vector&lt;complex&lt;T3&gt;, VectFull, Allocator3&gt; val_row;
<a name="l00847"></a>00847     IVect ind_row;
<a name="l00848"></a>00848     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00849"></a>00849       {
<a name="l00850"></a>00850         n1 = A.GetRowSize(i);
<a name="l00851"></a>00851         n2 = B.GetRowSize(i);
<a name="l00852"></a>00852         size_row = n1 + n2;
<a name="l00853"></a>00853         val_row.Reallocate(size_row);
<a name="l00854"></a>00854         ind_row.Reallocate(size_row);
<a name="l00855"></a>00855         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0 ; j &lt; n1 ; j++)
<a name="l00856"></a>00856           {
<a name="l00857"></a>00857             ind_row(j) = A.Index(i, j);
<a name="l00858"></a>00858             val_row(j) = alpha * complex&lt;T3&gt;(A.Value(i, j), 0);
<a name="l00859"></a>00859           }
<a name="l00860"></a>00860 
<a name="l00861"></a>00861         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0 ; j &lt; n2 ; j++)
<a name="l00862"></a>00862           {
<a name="l00863"></a>00863             ind_row(j+n1) = B.Index(i, j);
<a name="l00864"></a>00864             val_row(j+n1) = alpha * complex&lt;T3&gt;(B.Value(i, j));
<a name="l00865"></a>00865           }
<a name="l00866"></a>00866 
<a name="l00867"></a>00867         C.AddInteractionRow(i, size_row, ind_row, val_row);
<a name="l00868"></a>00868       }
<a name="l00869"></a>00869   }
<a name="l00870"></a>00870 
<a name="l00871"></a>00871 
<a name="l00872"></a>00872   <span class="comment">// C = C + complex(A,B)</span>
<a name="l00873"></a>00873   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>T2, <span class="keyword">class </span>T3,
<a name="l00874"></a>00874            <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00875"></a>00875   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l00876"></a>00876            <span class="keyword">const</span> Matrix&lt;T1, Symmetric, ArrayRowSymSparse, Allocator1&gt;&amp; A,
<a name="l00877"></a>00877            <span class="keyword">const</span> Matrix&lt;T2, Symmetric, ArrayRowSymSparse, Allocator2&gt;&amp; B,
<a name="l00878"></a>00878            Matrix&lt;T3, Symmetric, ArrayRowSymComplexSparse, Allocator3&gt;&amp; C)
<a name="l00879"></a>00879   {
<a name="l00880"></a>00880     <span class="keywordtype">int</span> m = B.GetM(), n, ni;
<a name="l00881"></a>00881     Vector&lt;complex&lt;T3&gt; &gt; value; IVect index;
<a name="l00882"></a>00882     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00883"></a>00883       {
<a name="l00884"></a>00884         n = A.GetRowSize(i);
<a name="l00885"></a>00885         ni = B.GetRowSize(i);
<a name="l00886"></a>00886         value.Reallocate(n+ni);
<a name="l00887"></a>00887         index.Reallocate(n+ni);
<a name="l00888"></a>00888         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l00889"></a>00889           {
<a name="l00890"></a>00890             value(j) = A.Value(i, j);
<a name="l00891"></a>00891             index(j) = A.Index(i, j);
<a name="l00892"></a>00892           }
<a name="l00893"></a>00893 
<a name="l00894"></a>00894         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ni; j++)
<a name="l00895"></a>00895           {
<a name="l00896"></a>00896             value(n+j) = complex&lt;T3&gt;(0, 1) * B.Value(i, j);
<a name="l00897"></a>00897             index(n+j) = B.Index(i, j);
<a name="l00898"></a>00898           }
<a name="l00899"></a>00899 
<a name="l00900"></a>00900         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(alpha, value);
<a name="l00901"></a>00901         C.AddInteractionRow(i, n+ni, index, value);
<a name="l00902"></a>00902       }
<a name="l00903"></a>00903   }
<a name="l00904"></a>00904 
<a name="l00905"></a>00905 
<a name="l00906"></a>00906   <span class="comment">// C = C + complex(A,B)</span>
<a name="l00907"></a>00907   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>T2, <span class="keyword">class </span>T3,
<a name="l00908"></a>00908            <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>Allocator3&gt;
<a name="l00909"></a>00909   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l00910"></a>00910            <span class="keyword">const</span> Matrix&lt;T1, General, ArrayRowSparse, Allocator1&gt;&amp; A,
<a name="l00911"></a>00911            <span class="keyword">const</span> Matrix&lt;T2, General, ArrayRowSparse, Allocator2&gt;&amp; B,
<a name="l00912"></a>00912            Matrix&lt;T3, General, ArrayRowComplexSparse, Allocator3&gt;&amp; C)
<a name="l00913"></a>00913   {
<a name="l00914"></a>00914     <span class="keywordtype">int</span> m = B.GetM(), n, ni;
<a name="l00915"></a>00915     Vector&lt;complex&lt;T3&gt; &gt; value;
<a name="l00916"></a>00916     IVect index;
<a name="l00917"></a>00917     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0 ; i &lt; m ; i++)
<a name="l00918"></a>00918       {
<a name="l00919"></a>00919         n = A.GetRowSize(i);
<a name="l00920"></a>00920         ni = B.GetRowSize(i);
<a name="l00921"></a>00921         value.Reallocate(n + ni);
<a name="l00922"></a>00922         index.Reallocate(n + ni);
<a name="l00923"></a>00923         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l00924"></a>00924           {
<a name="l00925"></a>00925             value(j) = A.Value(i, j);
<a name="l00926"></a>00926             index(j) = A.Index(i, j);
<a name="l00927"></a>00927           }
<a name="l00928"></a>00928 
<a name="l00929"></a>00929         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ni; j++)
<a name="l00930"></a>00930           {
<a name="l00931"></a>00931             value(n+j) = complex&lt;T3&gt;(0, 1) * B.Value(i, j);
<a name="l00932"></a>00932             index(n+j) = B.Index(i, j);
<a name="l00933"></a>00933           }
<a name="l00934"></a>00934 
<a name="l00935"></a>00935         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(alpha, value);
<a name="l00936"></a>00936         C.AddInteractionRow(i, n+ni, index, value);
<a name="l00937"></a>00937       }
<a name="l00938"></a>00938   }
<a name="l00939"></a>00939 
<a name="l00940"></a>00940 
<a name="l00941"></a>00941   <span class="comment">// Add //</span>
<a name="l00943"></a>00943 <span class="comment"></span>
<a name="l00944"></a>00944 
<a name="l00945"></a>00945 
<a name="l00947"></a>00947   <span class="comment">// Mlt //</span>
<a name="l00948"></a>00948 
<a name="l00949"></a>00949 
<a name="l00950"></a>00950   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00951"></a>00951   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keyword">const</span> T0&amp; alpha, Matrix&lt;T, General, ArrayRowSparse, Allocator&gt;&amp; A)
<a name="l00952"></a>00952   {
<a name="l00953"></a>00953     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetM(); i++)
<a name="l00954"></a>00954       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetRowSize(i); j++)
<a name="l00955"></a>00955         A.Value(i,j) *= alpha;
<a name="l00956"></a>00956   }
<a name="l00957"></a>00957 
<a name="l00958"></a>00958 
<a name="l00959"></a>00959   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00960"></a>00960   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keyword">const</span> T0&amp; alpha, Matrix&lt;T, General, ArrayColSparse, Allocator&gt;&amp; A)
<a name="l00961"></a>00961   {
<a name="l00962"></a>00962     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetN(); i++)
<a name="l00963"></a>00963       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetColSize(i); j++)
<a name="l00964"></a>00964         A.Value(i,j) *= alpha;
<a name="l00965"></a>00965   }
<a name="l00966"></a>00966 
<a name="l00967"></a>00967 
<a name="l00968"></a>00968   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00969"></a>00969   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l00970"></a>00970            Matrix&lt;T, Symmetric, ArrayRowSymSparse, Allocator&gt;&amp; A)
<a name="l00971"></a>00971   {
<a name="l00972"></a>00972     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetM(); i++)
<a name="l00973"></a>00973       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetRowSize(i); j++)
<a name="l00974"></a>00974         A.Value(i,j) *= alpha;
<a name="l00975"></a>00975   }
<a name="l00976"></a>00976 
<a name="l00977"></a>00977 
<a name="l00978"></a>00978   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00979"></a>00979   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l00980"></a>00980            Matrix&lt;T, Symmetric, ArrayColSymSparse, Allocator&gt;&amp; A)
<a name="l00981"></a>00981   {
<a name="l00982"></a>00982     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetN(); i++)
<a name="l00983"></a>00983       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetColSize(i); j++)
<a name="l00984"></a>00984         A.Value(i,j) *= alpha;
<a name="l00985"></a>00985   }
<a name="l00986"></a>00986 
<a name="l00987"></a>00987 
<a name="l00988"></a>00988   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00989"></a>00989   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l00990"></a>00990            Matrix&lt;T, General, ArrayRowComplexSparse, Allocator&gt;&amp; A)
<a name="l00991"></a>00991   {
<a name="l00992"></a>00992     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetM(); i++)
<a name="l00993"></a>00993       {
<a name="l00994"></a>00994         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetRealRowSize(i); j++)
<a name="l00995"></a>00995           A.ValueReal(i,j) *= alpha;
<a name="l00996"></a>00996         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetImagRowSize(i); j++)
<a name="l00997"></a>00997           A.ValueImag(i,j) *= alpha;
<a name="l00998"></a>00998       }
<a name="l00999"></a>00999   }
<a name="l01000"></a>01000 
<a name="l01001"></a>01001 
<a name="l01002"></a>01002   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l01003"></a>01003   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keyword">const</span> T0&amp; alpha, Matrix&lt;T, Symmetric,
<a name="l01004"></a>01004            ArrayRowSymComplexSparse, Allocator&gt;&amp; A)
<a name="l01005"></a>01005   {
<a name="l01006"></a>01006     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetM(); i++)
<a name="l01007"></a>01007       {
<a name="l01008"></a>01008         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetRealRowSize(i); j++)
<a name="l01009"></a>01009           A.ValueReal(i,j) *= alpha;
<a name="l01010"></a>01010 
<a name="l01011"></a>01011         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetImagRowSize(i); j++)
<a name="l01012"></a>01012           A.ValueImag(i,j) *= alpha;
<a name="l01013"></a>01013       }
<a name="l01014"></a>01014   }
<a name="l01015"></a>01015 
<a name="l01016"></a>01016 
<a name="l01017"></a>01017   <span class="comment">// Matrix-matrix product (sparse matrix against full matrix)</span>
<a name="l01018"></a>01018   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>T2, <span class="keyword">class </span>Prop2,
<a name="l01019"></a>01019            <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>T3, <span class="keyword">class </span>Prop3,
<a name="l01020"></a>01020            <span class="keyword">class </span>Storage3, <span class="keyword">class </span>Allocator3&gt;
<a name="l01021"></a>01021   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keyword">const</span> Matrix&lt;T1, General, ArrayRowSparse, Allocator1&gt;&amp; A,
<a name="l01022"></a>01022            <span class="keyword">const</span> Matrix&lt;T2, Prop2, Storage2, Allocator2&gt;&amp; B,
<a name="l01023"></a>01023            Matrix&lt;T3, Prop3, Storage3, Allocator3&gt;&amp; C)
<a name="l01024"></a>01024   {
<a name="l01025"></a>01025     <span class="keywordtype">int</span> m = A.GetM();
<a name="l01026"></a>01026     <span class="keywordtype">int</span> n = B.GetN();
<a name="l01027"></a>01027     C.Reallocate(m,n);
<a name="l01028"></a>01028     T3 val;
<a name="l01029"></a>01029     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l01030"></a>01030       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l01031"></a>01031         {
<a name="l01032"></a>01032           val = T3(0);
<a name="l01033"></a>01033           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> ind = 0; ind &lt; A.GetRowSize(i); ind++)
<a name="l01034"></a>01034             {
<a name="l01035"></a>01035               <span class="keywordtype">int</span> k = A.Index(i, ind);
<a name="l01036"></a>01036               val += A.Value(i, ind) * B(k, j);
<a name="l01037"></a>01037             }
<a name="l01038"></a>01038           C(i, j) = val;
<a name="l01039"></a>01039         }
<a name="l01040"></a>01040   }
<a name="l01041"></a>01041 
<a name="l01042"></a>01042 
<a name="l01043"></a>01043   <span class="comment">// Mlt //</span>
<a name="l01045"></a>01045 <span class="comment"></span>
<a name="l01046"></a>01046 
<a name="l01047"></a>01047 } <span class="comment">// namespace Seldon</span>
<a name="l01048"></a>01048 
<a name="l01049"></a>01049 <span class="preprocessor">#define SELDON_FILE_FUNCTIONS_MATRIX_ARRAY_CXX</span>
<a name="l01050"></a>01050 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
