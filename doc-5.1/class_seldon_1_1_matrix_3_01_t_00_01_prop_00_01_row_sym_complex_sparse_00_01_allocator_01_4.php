<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php">Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a> &#124;
<a href="#pro-static-attribs">Static Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;" --><!-- doxytag: inherits="Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;" -->
<p>Row-major complex sparse-matrix class.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_matrix___sym_complex_sparse_8hxx_source.php">Matrix_SymComplexSparse.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.png" usemap="#Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;_map" alt=""/>
  <map id="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;_map" name="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;_map">
<area href="class_seldon_1_1_matrix___sym_complex_sparse.php" alt="Seldon::Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt;" shape="rect" coords="0,56,483,80"/>
<area href="class_seldon_1_1_matrix___base.php" alt="Seldon::Matrix_Base&lt; T, Allocator &gt;" shape="rect" coords="0,0,483,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3374cc9bb8f7f0cd0c345268d5485a4d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::value_type" ref="a3374cc9bb8f7f0cd0c345268d5485a4d" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1029064480b731b0983db80d5a588079"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::property" ref="a1029064480b731b0983db80d5a588079" args="" -->
typedef Prop&nbsp;</td><td class="memItemRight" valign="bottom"><b>property</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3ccba5dd3113645f54e72c780c296ec2"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::storage" ref="a3ccba5dd3113645f54e72c780c296ec2" args="" -->
typedef <a class="el" href="class_seldon_1_1_row_sym_complex_sparse.php">RowSymComplexSparse</a>&nbsp;</td><td class="memItemRight" valign="bottom"><b>storage</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a39854b148cd59e7d6802a914e298c361"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::allocator" ref="a39854b148cd59e7d6802a914e298c361" args="" -->
typedef Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>allocator</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a91d05e834526448fb4cf9dfa85aca66c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::pointer" ref="a91d05e834526448fb4cf9dfa85aca66c" args="" -->
typedef Allocator::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aefe5cc70a26876e3a3dd113b23dab90e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::const_pointer" ref="aefe5cc70a26876e3a3dd113b23dab90e" args="" -->
typedef Allocator::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a43c3660803a61b5c9bd38f3133d427ab"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::reference" ref="a43c3660803a61b5c9bd38f3133d427ab" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a27c5eceb528f581eb2e93216140ffa77"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::const_reference" ref="a27c5eceb528f581eb2e93216140ffa77" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a00d2964761ccb7bf70b99dbc737c0df8"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::entry_type" ref="a00d2964761ccb7bf70b99dbc737c0df8" args="" -->
typedef complex&lt; value_type &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>entry_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a97e7be8ea1b5ce6202d851394e5d419e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::access_type" ref="a97e7be8ea1b5ce6202d851394e5d419e" args="" -->
typedef complex&lt; value_type &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>access_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af20b241a9a3ab6be0f8e7012d6ec8a1c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::const_access_type" ref="af20b241a9a3ab6be0f8e7012d6ec8a1c" args="" -->
typedef complex&lt; value_type &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_access_type</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php#aac12efd56ba2dcb96cbb2ed94cbf440c">Matrix</a> ()  throw ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#aac12efd56ba2dcb96cbb2ed94cbf440c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php#a47f8fde52ccac1a7747c5b66c4c0b46e">Matrix</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Builds a i by j matrix.  <a href="#a47f8fde52ccac1a7747c5b66c4c0b46e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php#a10616155b7471e78fdf59f4f2a760304">Matrix</a> (int i, int j, int real_nz, int imag_nz)</td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Storage0 , class Allocator0 , class Storage1 , class Allocator1 , class Storage2 , class Allocator2 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php#a0df1be9842a16925408e0ed5761218f3">Matrix</a> (int i, int j, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;values, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;ptr, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;ind, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;imag_values, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;imag_ptr, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;imag_ind)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Constructor.  <a href="#a0df1be9842a16925408e0ed5761218f3"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a77439fdc40537903681e7cd912c5f732"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::Clear" ref="a77439fdc40537903681e7cd912c5f732" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Clear</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3f202fe96b9289912e1cb6acfe24f18b"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::SetData" ref="a3f202fe96b9289912e1cb6acfe24f18b" args="(int i, int j, Vector&lt; T, Storage0, Allocator0 &gt; &amp;real_values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;real_ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;real_ind, Vector&lt; T, Storage0, Allocator0 &gt; &amp;imag_values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;imag_ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;imag_ind)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetData</b> (int i, int j, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;real_values, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;real_ptr, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;real_ind, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;imag_values, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;imag_ptr, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;imag_ind)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad94e329d8c48132a374a6cdf72ad2bab"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::SetData" ref="ad94e329d8c48132a374a6cdf72ad2bab" args="(int i, int j, int real_nz, pointer real_values, int *real_ptr, int *real_ind, int imag_nz, pointer imag_values, int *imag_ptr, int *imag_ind)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetData</b> (int i, int j, int real_nz, pointer real_values, int *real_ptr, int *real_ind, int imag_nz, pointer imag_values, int *imag_ptr, int *imag_ind)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af9adb37bee7561a855fef31642b85bfd"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::Nullify" ref="af9adb37bee7561a855fef31642b85bfd" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Nullify</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5e1062c93e237401d6bf6b1797861c62"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::Copy" ref="a5e1062c93e237401d6bf6b1797861c62" args="(const Matrix_SymComplexSparse&lt; T, Prop, RowSymComplexSparse, Allocator &gt; &amp;A)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Copy</b> (const <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php">Matrix_SymComplexSparse</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_sym_complex_sparse.php">RowSymComplexSparse</a>, Allocator &gt; &amp;A)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afda1dbd968e2e4b3047c014ab5931846"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::GetDataSize" ref="afda1dbd968e2e4b3047c014ab5931846" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataSize</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a005a3b7389aecf38482cc16e4984787c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::GetRealPtr" ref="a005a3b7389aecf38482cc16e4984787c" args="() const" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetRealPtr</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad1bb43e80676e9ee420ea692ba06d0ac"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::GetImagPtr" ref="ad1bb43e80676e9ee420ea692ba06d0ac" args="() const" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetImagPtr</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a72f50ab4230ffe4af23c8bbf2203ad0b"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::GetRealInd" ref="a72f50ab4230ffe4af23c8bbf2203ad0b" args="() const" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetRealInd</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2bb61950179c75be2150cfae734bb05b"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::GetImagInd" ref="a2bb61950179c75be2150cfae734bb05b" args="() const" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetImagInd</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a913aadd75d1ed2e076ea4b014e5fb32d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::GetRealPtrSize" ref="a913aadd75d1ed2e076ea4b014e5fb32d" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetRealPtrSize</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acde6ac68023f92f32eceb4f0a422580f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::GetImagPtrSize" ref="acde6ac68023f92f32eceb4f0a422580f" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetImagPtrSize</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acdb80353a65a45dfc11b886e7c75a807"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::GetRealIndSize" ref="acdb80353a65a45dfc11b886e7c75a807" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetRealIndSize</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af1c1a606649e03827a3710751238a5bc"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::GetImagIndSize" ref="af1c1a606649e03827a3710751238a5bc" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetImagIndSize</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1a98f711de4b7daf42ea42c8493ca08d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::GetRealData" ref="a1a98f711de4b7daf42ea42c8493ca08d" args="() const" -->
T *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetRealData</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a411656148afd39425bd6df81a013d180"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::GetImagData" ref="a411656148afd39425bd6df81a013d180" args="() const" -->
T *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetImagData</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad441174c77a9c101560e77794aae6369"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::operator()" ref="ad441174c77a9c101560e77794aae6369" args="(int i, int j) const" -->
complex&lt; value_type &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>operator()</b> (int i, int j) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3b6f94e76e99c7948f1e78a31012e53c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::Val" ref="a3b6f94e76e99c7948f1e78a31012e53c" args="(int i, int j)" -->
complex&lt; value_type &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>Val</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a05132fdf9928e62518a9369f213fad00"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::Val" ref="a05132fdf9928e62518a9369f213fad00" args="(int i, int j) const" -->
const complex&lt; value_type &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>Val</b> (int i, int j) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="abe86d6545d4aeca07257de2d8e7a85f2"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::Print" ref="abe86d6545d4aeca07257de2d8e7a85f2" args="() const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Print</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">GetM</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows.  <a href="#a65e9c3f0db9c7c8c3fa10ead777dd927"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows of the matrix possibly transposed.  <a href="#ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">GetN</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns.  <a href="#a6b134070d1b890ba6b7eb72fee170984"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns of the matrix possibly transposed.  <a href="#a7d27412bc9384a5ce0c0db1ee310d31c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">GetSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements in the matrix.  <a href="#a0fbc3f7030583174eaa69429f655a1b0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer to the data array.  <a href="#a453a269dfe7fadba249064363d5ab92a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a const pointer to the data array.  <a href="#a0f2796430deb08e565df8ced656097bf"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "void*" to the data array.  <a href="#a505cdfee34741c463e0dc1943337bedc"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "const void*" to the data array.  <a href="#a5979450cd8801810229f4a24c36e6639"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">Allocator &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the allocator of the matrix.  <a href="#af88748a55208349367d6860ad76fd691"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aabc20cbf97a5f06fb7542e32ca86a452"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::real_nz_" ref="aabc20cbf97a5f06fb7542e32ca86a452" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>real_nz_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a32157995226016273870a0db60e9fd36"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::imag_nz_" ref="a32157995226016273870a0db60e9fd36" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>imag_nz_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad1f2ace9d9fdc6e21cac9b7a057f0782"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::real_ptr_" ref="ad1f2ace9d9fdc6e21cac9b7a057f0782" args="" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>real_ptr_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a34a2783f923953f461ccab073054f60f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::imag_ptr_" ref="a34a2783f923953f461ccab073054f60f" args="" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>imag_ptr_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="abf49b27e4a3d9c669d6705e29bfc6352"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::real_ind_" ref="abf49b27e4a3d9c669d6705e29bfc6352" args="" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>real_ind_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa0e30f0ff17c81bb638d1bba242992db"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::imag_ind_" ref="aa0e30f0ff17c81bb638d1bba242992db" args="" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>imag_ind_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9bf6ae45b8d302795e88e12c7f98fc2a"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::real_data_" ref="a9bf6ae45b8d302795e88e12c7f98fc2a" args="" -->
T *&nbsp;</td><td class="memItemRight" valign="bottom"><b>real_data_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a87a36b09736188fc4a13061b2c890c4e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::imag_data_" ref="a87a36b09736188fc4a13061b2c890c4e" args="" -->
T *&nbsp;</td><td class="memItemRight" valign="bottom"><b>imag_data_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a96b39ff494d4b7d3e30f320a1c7b4aba"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::m_" ref="a96b39ff494d4b7d3e30f320a1c7b4aba" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>m_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a11cb5d502050e5f14482ff0c9cef5a76"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::n_" ref="a11cb5d502050e5f14482ff0c9cef5a76" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>n_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acb12a8b74699fae7ae3b2b67376795a1"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::data_" ref="acb12a8b74699fae7ae3b2b67376795a1" args="" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-static-attribs"></a>
Static Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a132fa01ce120f352d434fd920e5ad9b4"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::allocator_" ref="a132fa01ce120f352d434fd920e5ad9b4" args="" -->
static Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>allocator_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Prop, class Allocator&gt;<br/>
 class Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</h3>

<p>Row-major complex sparse-matrix class. </p>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8hxx_source.php#l00176">176</a> of file <a class="el" href="_matrix___sym_complex_sparse_8hxx_source.php">Matrix_SymComplexSparse.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="aac12efd56ba2dcb96cbb2ed94cbf440c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::Matrix" ref="aac12efd56ba2dcb96cbb2ed94cbf440c" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_sym_complex_sparse.php">RowSymComplexSparse</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td>  throw ()</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>Builds an empty 0x0 matrix. </p>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l01795">1795</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a47f8fde52ccac1a7747c5b66c4c0b46e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::Matrix" ref="a47f8fde52ccac1a7747c5b66c4c0b46e" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_sym_complex_sparse.php">RowSymComplexSparse</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Builds a i by j matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l01808">1808</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a10616155b7471e78fdf59f4f2a760304"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::Matrix" ref="a10616155b7471e78fdf59f4f2a760304" args="(int i, int j, int real_nz, int imag_nz)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_sym_complex_sparse.php">RowSymComplexSparse</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>real_nz</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>imag_nz</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">
<p>Builds a i by j matrix with real_nz and imag_nz non-zero (stored) elements for the real part and the imaginary part respectively. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_nz</em>&nbsp;</td><td>number of non-zero elements that are stored for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_nz</em>&nbsp;</td><td>number of non-zero elements that are store for the imaginary part. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> values are not initialized. </dd></dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>'j' is assumed to be equal to 'i' so that 'j' is discarded. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l01828">1828</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0df1be9842a16925408e0ed5761218f3"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::Matrix" ref="a0df1be9842a16925408e0ed5761218f3" args="(int i, int j, Vector&lt; T, Storage0, Allocator0 &gt; &amp;values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;ind, Vector&lt; T, Storage0, Allocator0 &gt; &amp;imag_values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;imag_ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;imag_ind)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Storage0 , class Allocator0 , class Storage1 , class Allocator1 , class Storage2 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_sym_complex_sparse.php">RowSymComplexSparse</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>real_values</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>real_ptr</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>real_ind</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, Storage0, Allocator0 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>imag_values</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage1, Allocator1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>imag_ptr</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, Storage2, Allocator2 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>imag_ind</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Constructor. </p>
<p>Builds a i by j sparse matrix with non-zero values and indices provided by 'real_values' (values of the real part), 'real_ptr' (pointers for the real part), 'real_ind' (indices for the real part), 'imag_values' (values of the imaginary part), 'imag_ptr' (pointers for the imaginary part) and 'imag_ind' (indices for the imaginary part). Input vectors are released and are empty on exit. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_values</em>&nbsp;</td><td>values of non-zero entries for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_ptr</em>&nbsp;</td><td>row or column start indices for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>real_ind</em>&nbsp;</td><td>row or column indices for the real part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_values</em>&nbsp;</td><td>values of non-zero entries for the imaginary part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_ptr</em>&nbsp;</td><td>row or column start indices for the imaginary part. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>imag_ind</em>&nbsp;</td><td>row or column indices for the imaginary part. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Input vectors 'real_values', 'real_ptr' and 'real_ind', 'imag_values', 'imag_ptr' and 'imag_ind' are empty on exit. Moreover 'j' is assumed to be equal to 'i' so that 'j' is discarded. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php#l01861">1861</a> of file <a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="af88748a55208349367d6860ad76fd691"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::GetAllocator" ref="af88748a55208349367d6860ad76fd691" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">Allocator &amp; <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetAllocator </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the allocator of the matrix. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The allocator. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00257">257</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a453a269dfe7fadba249064363d5ab92a"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::GetData" ref="a453a269dfe7fadba249064363d5ab92a" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer to the data array. </p>
<p>Returns a pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00207">207</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0f2796430deb08e565df8ced656097bf"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::GetDataConst" ref="a0f2796430deb08e565df8ced656097bf" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::const_pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConst </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a const pointer to the data array. </p>
<p>Returns a const pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00220">220</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5979450cd8801810229f4a24c36e6639"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::GetDataConstVoid" ref="a5979450cd8801810229f4a24c36e6639" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConstVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "const void*" to the data array. </p>
<p>Returns a pointer of type "const void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00246">246</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a505cdfee34741c463e0dc1943337bedc"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::GetDataVoid" ref="a505cdfee34741c463e0dc1943337bedc" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "void*" to the data array. </p>
<p>Returns a pointer of type "void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00233">233</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a65e9c3f0db9c7c8c3fa10ead777dd927"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::GetM" ref="a65e9c3f0db9c7c8c3fa10ead777dd927" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a8c781e99310aae01786eac0e8e5aa50c">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00106">106</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::GetM" ref="ab6f6c5bba065ca82dad7c3abdbc8ea79" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#aecf3e8c636dbb83335ea588afe2558a8">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00129">129</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6b134070d1b890ba6b7eb72fee170984"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::GetN" ref="a6b134070d1b890ba6b7eb72fee170984" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a40580a92202044416e379c1304ff0220">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00117">117</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7d27412bc9384a5ce0c0db1ee310d31c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::GetN" ref="a7d27412bc9384a5ce0c0db1ee310d31c" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a867066f7bf531bf7ad15bdcd034dc3c0">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00144">144</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0fbc3f7030583174eaa69429f655a1b0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;::GetSize" ref="a0fbc3f7030583174eaa69429f655a1b0" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements in the matrix. </p>
<p>Returns the number of elements in the matrix, i.e. the number of rows multiplied by the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements in the matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00194">194</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>matrix_sparse/<a class="el" href="_matrix___sym_complex_sparse_8hxx_source.php">Matrix_SymComplexSparse.hxx</a></li>
<li>matrix_sparse/<a class="el" href="_matrix___sym_complex_sparse_8cxx_source.php">Matrix_SymComplexSparse.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
