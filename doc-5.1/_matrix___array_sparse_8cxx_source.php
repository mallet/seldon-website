<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix_sparse/Matrix_ArraySparse.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_ARRAY_SPARSE_CXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 <span class="preprocessor">#include &quot;Matrix_ArraySparse.hxx&quot;</span>
<a name="l00023"></a>00023 
<a name="l00024"></a>00024 <span class="keyword">namespace </span>Seldon
<a name="l00025"></a>00025 {
<a name="l00026"></a>00026 
<a name="l00028"></a>00028 
<a name="l00031"></a>00031   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00032"></a>00032   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a6d460ce1bd8388adce95c8ad99b24f1d" title="Default constructor.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Matrix_ArraySparse</a>()
<a name="l00033"></a>00033     : val_()
<a name="l00034"></a>00034   {
<a name="l00035"></a>00035     this-&gt;m_ = 0;
<a name="l00036"></a>00036     this-&gt;n_ = 0;
<a name="l00037"></a>00037   }
<a name="l00038"></a>00038 
<a name="l00039"></a>00039 
<a name="l00041"></a>00041 
<a name="l00046"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ad3363505ad6df902486f4496ac0e7ea7">00046</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00047"></a>00047   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a6d460ce1bd8388adce95c8ad99b24f1d" title="Default constructor.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00048"></a>00048 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a6d460ce1bd8388adce95c8ad99b24f1d" title="Default constructor.">  Matrix_ArraySparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) :
<a name="l00049"></a>00049     val_(Storage::GetFirst(i, j))
<a name="l00050"></a>00050   {
<a name="l00051"></a>00051     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a> = i;
<a name="l00052"></a>00052     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a> = j;
<a name="l00053"></a>00053   }
<a name="l00054"></a>00054 
<a name="l00055"></a>00055 
<a name="l00057"></a>00057   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocat&gt;
<a name="l00058"></a>00058   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#adf298ef8550ee9681a384a539802736f" title="Destructor.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocat&gt;::~Matrix_ArraySparse</a>()
<a name="l00059"></a>00059   {
<a name="l00060"></a>00060     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a> = 0;
<a name="l00061"></a>00061     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a> = 0;
<a name="l00062"></a>00062   }
<a name="l00063"></a>00063 
<a name="l00064"></a>00064 
<a name="l00066"></a>00066 
<a name="l00069"></a>00069   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00070"></a>00070   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a61227beea7612dd22bf1f46546bb5cde" title="Clears the matrix.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Clear</a>()
<a name="l00071"></a>00071   {
<a name="l00072"></a>00072     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#adf298ef8550ee9681a384a539802736f" title="Destructor.">~Matrix_ArraySparse</a>();
<a name="l00073"></a>00073   }
<a name="l00074"></a>00074 
<a name="l00075"></a>00075 
<a name="l00076"></a>00076   <span class="comment">/*********************</span>
<a name="l00077"></a>00077 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00078"></a>00078 <span class="comment">   *********************/</span>
<a name="l00079"></a>00079 
<a name="l00080"></a>00080 
<a name="l00082"></a>00082 
<a name="l00088"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#afcd0d4612b325249c24407627b3dd568">00088</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00089"></a>00089   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#afcd0d4612b325249c24407627b3dd568" title="Reallocates memory to resize the matrix.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00090"></a>00090 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#afcd0d4612b325249c24407627b3dd568" title="Reallocates memory to resize the matrix.">  Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00091"></a>00091   {
<a name="l00092"></a>00092     <span class="comment">// Clears previous entries.</span>
<a name="l00093"></a>00093     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a61227beea7612dd22bf1f46546bb5cde" title="Clears the matrix.">Clear</a>();
<a name="l00094"></a>00094 
<a name="l00095"></a>00095     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a> = i;
<a name="l00096"></a>00096     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a> = j;
<a name="l00097"></a>00097 
<a name="l00098"></a>00098     <span class="keywordtype">int</span> n = Storage::GetFirst(i, j);
<a name="l00099"></a>00099     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.Reallocate(n);
<a name="l00100"></a>00100   }
<a name="l00101"></a>00101 
<a name="l00102"></a>00102 
<a name="l00104"></a>00104 
<a name="l00110"></a>00110   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00111"></a>00111   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a2c6f9586424529c8b06b6110c5a01efa" title="Reallocates additional memory to resize the matrix.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00112"></a>00112   {
<a name="l00113"></a>00113     <span class="keywordtype">int</span> n = Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>, <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>);
<a name="l00114"></a>00114     <span class="keywordtype">int</span> new_n = Storage::GetFirst(i, j);
<a name="l00115"></a>00115     <span class="keywordflow">if</span> (n != new_n)
<a name="l00116"></a>00116       {
<a name="l00117"></a>00117         <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;T, VectSparse, Allocator&gt;</a>, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>,
<a name="l00118"></a>00118           <a class="code" href="class_seldon_1_1_new_alloc.php">NewAlloc&lt;Vector&lt;T, VectSparse, Allocator&gt;</a> &gt; &gt; new_val;
<a name="l00119"></a>00119 
<a name="l00120"></a>00120         new_val.Reallocate(new_n);
<a name="l00121"></a>00121 
<a name="l00122"></a>00122         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0 ; k &lt; min(n, new_n) ; k++)
<a name="l00123"></a>00123           Swap(new_val(k), this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(k));
<a name="l00124"></a>00124 
<a name="l00125"></a>00125         <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.SetData(new_n, new_val.GetData());
<a name="l00126"></a>00126         new_val.Nullify();
<a name="l00127"></a>00127 
<a name="l00128"></a>00128       }
<a name="l00129"></a>00129 
<a name="l00130"></a>00130     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a> = i;
<a name="l00131"></a>00131     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a> = j;
<a name="l00132"></a>00132   }
<a name="l00133"></a>00133 
<a name="l00134"></a>00134 
<a name="l00135"></a>00135   <span class="comment">/*******************</span>
<a name="l00136"></a>00136 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00137"></a>00137 <span class="comment">   *******************/</span>
<a name="l00138"></a>00138 
<a name="l00139"></a>00139 
<a name="l00141"></a>00141 
<a name="l00144"></a>00144   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00145"></a>00145   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::GetM</a>()<span class="keyword"> const</span>
<a name="l00146"></a>00146 <span class="keyword">  </span>{
<a name="l00147"></a>00147     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>;
<a name="l00148"></a>00148   }
<a name="l00149"></a>00149 
<a name="l00150"></a>00150 
<a name="l00152"></a>00152 
<a name="l00155"></a>00155   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00156"></a>00156   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3ae65d56c548233051a15054643f6753" title="Returns the number of columns.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::GetN</a>()<span class="keyword"> const</span>
<a name="l00157"></a>00157 <span class="keyword">  </span>{
<a name="l00158"></a>00158     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>;
<a name="l00159"></a>00159   }
<a name="l00160"></a>00160 
<a name="l00161"></a>00161 
<a name="l00163"></a>00163 
<a name="l00167"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#aeb55b9de8946ee89fb40c18612f9f700">00167</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00168"></a>00168   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00169"></a>00169 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">  ::GetM</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; status)<span class="keyword"> const</span>
<a name="l00170"></a>00170 <span class="keyword">  </span>{
<a name="l00171"></a>00171     <span class="keywordflow">if</span> (status.NoTrans())
<a name="l00172"></a>00172       <span class="keywordflow">return</span> m_;
<a name="l00173"></a>00173     <span class="keywordflow">else</span>
<a name="l00174"></a>00174       <span class="keywordflow">return</span> n_;
<a name="l00175"></a>00175   }
<a name="l00176"></a>00176 
<a name="l00177"></a>00177 
<a name="l00179"></a>00179 
<a name="l00183"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a537eebc1c692724958852a83df609036">00183</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00184"></a>00184   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3ae65d56c548233051a15054643f6753" title="Returns the number of columns.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00185"></a>00185 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3ae65d56c548233051a15054643f6753" title="Returns the number of columns.">  ::GetN</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; status)<span class="keyword"> const</span>
<a name="l00186"></a>00186 <span class="keyword">  </span>{
<a name="l00187"></a>00187     <span class="keywordflow">if</span> (status.NoTrans())
<a name="l00188"></a>00188       <span class="keywordflow">return</span> n_;
<a name="l00189"></a>00189     <span class="keywordflow">else</span>
<a name="l00190"></a>00190       <span class="keywordflow">return</span> m_;
<a name="l00191"></a>00191   }
<a name="l00192"></a>00192 
<a name="l00193"></a>00193 
<a name="l00195"></a>00195 
<a name="l00198"></a>00198   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00199"></a>00199   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ac58573fd27b4f0345ae196b1a6c69d0d" title="Returns the number of non-zero entries.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::GetNonZeros</a>()<span class="keyword"></span>
<a name="l00200"></a>00200 <span class="keyword">    const</span>
<a name="l00201"></a>00201 <span class="keyword">  </span>{
<a name="l00202"></a>00202     <span class="keywordtype">int</span> nnz = 0;
<a name="l00203"></a>00203     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.GetM(); i++)
<a name="l00204"></a>00204       nnz += this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).GetM();
<a name="l00205"></a>00205 
<a name="l00206"></a>00206     <span class="keywordflow">return</span> nnz;
<a name="l00207"></a>00207   }
<a name="l00208"></a>00208 
<a name="l00209"></a>00209 
<a name="l00211"></a>00211 
<a name="l00216"></a>00216   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00217"></a>00217   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a01787b806fc996c2ff3c1dcf9b2d54de" title="Returns the number of elements stored in memory.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::GetDataSize</a>()<span class="keyword"></span>
<a name="l00218"></a>00218 <span class="keyword">    const</span>
<a name="l00219"></a>00219 <span class="keyword">  </span>{
<a name="l00220"></a>00220     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ac58573fd27b4f0345ae196b1a6c69d0d" title="Returns the number of non-zero entries.">GetNonZeros</a>();
<a name="l00221"></a>00221   }
<a name="l00222"></a>00222 
<a name="l00223"></a>00223 
<a name="l00225"></a>00225 
<a name="l00230"></a>00230   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00231"></a>00231   <span class="keyword">inline</span> <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ac3d6a23829eb932f41de04c13de22f6b" title="Returns (row or column) indices of non-zero entries in row.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::GetIndex</a>(<span class="keywordtype">int</span> i)<span class="keyword"></span>
<a name="l00232"></a>00232 <span class="keyword">    const</span>
<a name="l00233"></a>00233 <span class="keyword">  </span>{
<a name="l00234"></a>00234     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).GetIndex();
<a name="l00235"></a>00235   }
<a name="l00236"></a>00236 
<a name="l00237"></a>00237 
<a name="l00239"></a>00239 
<a name="l00243"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0c18c6428cc11d175b58205727a4c67d">00243</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00244"></a>00244   <span class="keyword">inline</span> T*
<a name="l00245"></a>00245   <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a550784008495b254998193d94f75bff9" title="Returns values of non-zero entries.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::GetData</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00246"></a>00246 <span class="keyword">  </span>{
<a name="l00247"></a>00247     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).GetData();
<a name="l00248"></a>00248   }
<a name="l00249"></a>00249 
<a name="l00250"></a>00250 
<a name="l00252"></a>00252 
<a name="l00256"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a550784008495b254998193d94f75bff9">00256</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocat&gt;
<a name="l00257"></a>00257   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocat&gt;</a>*
<a name="l00258"></a>00258   <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a550784008495b254998193d94f75bff9" title="Returns values of non-zero entries.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocat&gt;::GetData</a>()<span class="keyword"> const</span>
<a name="l00259"></a>00259 <span class="keyword">  </span>{
<a name="l00260"></a>00260     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.GetData();
<a name="l00261"></a>00261   }
<a name="l00262"></a>00262 
<a name="l00263"></a>00263 
<a name="l00264"></a>00264   <span class="comment">/**********************************</span>
<a name="l00265"></a>00265 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l00266"></a>00266 <span class="comment">   **********************************/</span>
<a name="l00267"></a>00267 
<a name="l00268"></a>00268 
<a name="l00270"></a>00270 
<a name="l00276"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a41e1ef18aa87fce8062d36be5e634dfd">00276</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00277"></a>00277   <span class="keyword">inline</span> T
<a name="l00278"></a>00278   <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a41e1ef18aa87fce8062d36be5e634dfd" title="Access operator.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"></span>
<a name="l00279"></a>00279 <span class="keyword">    const</span>
<a name="l00280"></a>00280 <span class="keyword">  </span>{
<a name="l00281"></a>00281 
<a name="l00282"></a>00282 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00283"></a>00283 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>)
<a name="l00284"></a>00284       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l00285"></a>00285                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>-1) +
<a name="l00286"></a>00286                      <span class="stringliteral">&quot;],but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00287"></a>00287 
<a name="l00288"></a>00288     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>)
<a name="l00289"></a>00289       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l00290"></a>00290                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>-1) +
<a name="l00291"></a>00291                      <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00292"></a>00292 <span class="preprocessor">#endif</span>
<a name="l00293"></a>00293 <span class="preprocessor"></span>
<a name="l00294"></a>00294     <span class="keywordflow">return</span> this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(Storage::GetFirst(i, j))(Storage::GetSecond(i, j));
<a name="l00295"></a>00295   }
<a name="l00296"></a>00296 
<a name="l00297"></a>00297 
<a name="l00299"></a>00299 
<a name="l00305"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5aaa013260fab95db133652459b0bb2f">00305</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00306"></a>00306   <span class="keyword">inline</span> T&amp;
<a name="l00307"></a>00307   <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a41e1ef18aa87fce8062d36be5e634dfd" title="Access operator.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00308"></a>00308   {
<a name="l00309"></a>00309 
<a name="l00310"></a>00310 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00311"></a>00311 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>)
<a name="l00312"></a>00312       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l00313"></a>00313                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>-1) +
<a name="l00314"></a>00314                      <span class="stringliteral">&quot;],but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00315"></a>00315 
<a name="l00316"></a>00316     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>)
<a name="l00317"></a>00317       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l00318"></a>00318                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>-1) +
<a name="l00319"></a>00319                      <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00320"></a>00320 <span class="preprocessor">#endif</span>
<a name="l00321"></a>00321 <span class="preprocessor"></span>
<a name="l00322"></a>00322     <span class="keywordflow">return</span> this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(Storage::GetFirst(i, j))(Storage::GetSecond(i, j));
<a name="l00323"></a>00323   }
<a name="l00324"></a>00324 
<a name="l00325"></a>00325 
<a name="l00327"></a>00327 
<a name="l00332"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ad114c8c6e002a7a0def70f2413a32659">00332</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00333"></a>00333   <span class="keyword">inline</span> T&amp;
<a name="l00334"></a>00334   <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ad114c8c6e002a7a0def70f2413a32659" title="Access method.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00335"></a>00335   {
<a name="l00336"></a>00336 
<a name="l00337"></a>00337 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00338"></a>00338 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>)
<a name="l00339"></a>00339       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l00340"></a>00340                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>-1) +
<a name="l00341"></a>00341                      <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00342"></a>00342 
<a name="l00343"></a>00343     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>)
<a name="l00344"></a>00344       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l00345"></a>00345                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>-1) +
<a name="l00346"></a>00346                      <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00347"></a>00347 <span class="preprocessor">#endif</span>
<a name="l00348"></a>00348 <span class="preprocessor"></span>
<a name="l00349"></a>00349     <span class="keywordflow">return</span>
<a name="l00350"></a>00350       this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(Storage::GetFirst(i, j)).Val(Storage::GetSecond(i, j));
<a name="l00351"></a>00351   }
<a name="l00352"></a>00352 
<a name="l00353"></a>00353 
<a name="l00355"></a>00355 
<a name="l00360"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a130c7307de87672e3878362386481775">00360</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00361"></a>00361   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp;
<a name="l00362"></a>00362   <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ad114c8c6e002a7a0def70f2413a32659" title="Access method.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00363"></a>00363 <span class="keyword">  </span>{
<a name="l00364"></a>00364 
<a name="l00365"></a>00365 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00366"></a>00366 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>)
<a name="l00367"></a>00367       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l00368"></a>00368                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>-1) +
<a name="l00369"></a>00369                      <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00370"></a>00370 
<a name="l00371"></a>00371     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>)
<a name="l00372"></a>00372       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l00373"></a>00373                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>-1) +
<a name="l00374"></a>00374                      <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00375"></a>00375 <span class="preprocessor">#endif</span>
<a name="l00376"></a>00376 <span class="preprocessor"></span>
<a name="l00377"></a>00377     <span class="keywordflow">return</span>
<a name="l00378"></a>00378       this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(Storage::GetFirst(i, j)).Val(Storage::GetSecond(i, j));
<a name="l00379"></a>00379   }
<a name="l00380"></a>00380 
<a name="l00381"></a>00381 
<a name="l00383"></a>00383 
<a name="l00388"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6">00388</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00389"></a>00389   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00390"></a>00390 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">  Value</a> (<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00391"></a>00391 <span class="keyword">  </span>{
<a name="l00392"></a>00392 
<a name="l00393"></a>00393 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00394"></a>00394 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>)
<a name="l00395"></a>00395       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00396"></a>00396                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00397"></a>00397                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00398"></a>00398 
<a name="l00399"></a>00399     <span class="keywordflow">if</span> ((j &lt; 0)||(j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>()))
<a name="l00400"></a>00400       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00401"></a>00401                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00402"></a>00402                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00403"></a>00403 <span class="preprocessor">#endif</span>
<a name="l00404"></a>00404 <span class="preprocessor"></span>
<a name="l00405"></a>00405     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Value(j);
<a name="l00406"></a>00406   }
<a name="l00407"></a>00407 
<a name="l00408"></a>00408 
<a name="l00410"></a>00410 
<a name="l00415"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#afce6920acaa6e6ca7ce340c2db8991ea">00415</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00416"></a>00416   <span class="keyword">inline</span> T&amp;
<a name="l00417"></a>00417   <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Value</a> (<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00418"></a>00418   {
<a name="l00419"></a>00419 
<a name="l00420"></a>00420 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00421"></a>00421 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>)
<a name="l00422"></a>00422       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00423"></a>00423                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00424"></a>00424                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00425"></a>00425 
<a name="l00426"></a>00426     <span class="keywordflow">if</span> ((j &lt; 0)||(j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>()))
<a name="l00427"></a>00427       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00428"></a>00428                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00429"></a>00429                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00430"></a>00430 <span class="preprocessor">#endif</span>
<a name="l00431"></a>00431 <span class="preprocessor"></span>
<a name="l00432"></a>00432     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Value(j);
<a name="l00433"></a>00433   }
<a name="l00434"></a>00434 
<a name="l00435"></a>00435 
<a name="l00437"></a>00437 
<a name="l00442"></a>00442   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l00443"></a>00443   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3534fb2c57f2640c9b8cd5a59e9da0e2" title="Returns column/row number of j-th non-zero value of row/column i.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Index</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"></span>
<a name="l00444"></a>00444 <span class="keyword">    const</span>
<a name="l00445"></a>00445 <span class="keyword">  </span>{
<a name="l00446"></a>00446 
<a name="l00447"></a>00447 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00448"></a>00448 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>)
<a name="l00449"></a>00449       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00450"></a>00450                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00451"></a>00451                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00452"></a>00452 
<a name="l00453"></a>00453     <span class="keywordflow">if</span> ((j &lt; 0)||(j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>()))
<a name="l00454"></a>00454       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00455"></a>00455                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00456"></a>00456                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00457"></a>00457 <span class="preprocessor">#endif</span>
<a name="l00458"></a>00458 <span class="preprocessor"></span>
<a name="l00459"></a>00459     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Index(j);
<a name="l00460"></a>00460   }
<a name="l00461"></a>00461 
<a name="l00462"></a>00462 
<a name="l00464"></a>00464 
<a name="l00469"></a>00469   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l00470"></a>00470   <span class="keywordtype">int</span>&amp; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3534fb2c57f2640c9b8cd5a59e9da0e2" title="Returns column/row number of j-th non-zero value of row/column i.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Index</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00471"></a>00471   {
<a name="l00472"></a>00472 
<a name="l00473"></a>00473 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00474"></a>00474 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>)
<a name="l00475"></a>00475       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00476"></a>00476                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00477"></a>00477                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00478"></a>00478 
<a name="l00479"></a>00479     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>())
<a name="l00480"></a>00480       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00481"></a>00481                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00482"></a>00482                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00483"></a>00483 <span class="preprocessor">#endif</span>
<a name="l00484"></a>00484 <span class="preprocessor"></span>
<a name="l00485"></a>00485     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Index(j);
<a name="l00486"></a>00486   }
<a name="l00487"></a>00487 
<a name="l00488"></a>00488 
<a name="l00490"></a>00490 
<a name="l00496"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a6e429aff71786b06033b047e6c277fab">00496</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00497"></a>00497   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a9fb98c93d5bee7a4be62e1a3e1bb5a46" title="Redefines the matrix.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00498"></a>00498 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a9fb98c93d5bee7a4be62e1a3e1bb5a46" title="Redefines the matrix.">  SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> n, T* val, <span class="keywordtype">int</span>* ind)
<a name="l00499"></a>00499   {
<a name="l00500"></a>00500     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).SetData(n, val, ind);
<a name="l00501"></a>00501   }
<a name="l00502"></a>00502 
<a name="l00503"></a>00503 
<a name="l00505"></a>00505 
<a name="l00509"></a>00509   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00510"></a>00510   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0cceba81597165b0e23ec0c20daed462" title="Clears the matrix without releasing memory.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Nullify</a>(<span class="keywordtype">int</span> i)
<a name="l00511"></a>00511   {
<a name="l00512"></a>00512     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Nullify();
<a name="l00513"></a>00513   }
<a name="l00514"></a>00514 
<a name="l00515"></a>00515 
<a name="l00517"></a>00517 
<a name="l00522"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a9fb98c93d5bee7a4be62e1a3e1bb5a46">00522</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00523"></a>00523   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a9fb98c93d5bee7a4be62e1a3e1bb5a46" title="Redefines the matrix.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00524"></a>00524 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a9fb98c93d5bee7a4be62e1a3e1bb5a46" title="Redefines the matrix.">  SetData</a>(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n, <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php" title="Sparse vector class.">Vector&lt;T, VectSparse, Allocator&gt;</a>* val)
<a name="l00525"></a>00525   {
<a name="l00526"></a>00526     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a> = m;
<a name="l00527"></a>00527     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a> = n;
<a name="l00528"></a>00528     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.SetData(Storage::GetFirst(m, n), val);
<a name="l00529"></a>00529   }
<a name="l00530"></a>00530 
<a name="l00531"></a>00531 
<a name="l00533"></a>00533 
<a name="l00537"></a>00537   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00538"></a>00538   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0cceba81597165b0e23ec0c20daed462" title="Clears the matrix without releasing memory.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Nullify</a>()
<a name="l00539"></a>00539   {
<a name="l00540"></a>00540     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a> = 0;
<a name="l00541"></a>00541     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a> = 0;
<a name="l00542"></a>00542     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.Nullify();
<a name="l00543"></a>00543   }
<a name="l00544"></a>00544 
<a name="l00545"></a>00545 
<a name="l00546"></a>00546   <span class="comment">/************************</span>
<a name="l00547"></a>00547 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l00548"></a>00548 <span class="comment">   ************************/</span>
<a name="l00549"></a>00549 
<a name="l00550"></a>00550 
<a name="l00552"></a>00552 
<a name="l00557"></a>00557   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00558"></a>00558   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a61ff8d09ea73a05eff4fb21e16fb1118" title="Displays the matrix on the standard output.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00559"></a>00559 <span class="keyword">  </span>{
<a name="l00560"></a>00560     <span class="keywordflow">if</span> (Storage::GetFirst(1, 0) == 1)
<a name="l00561"></a>00561       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db" title="Number of rows.">m_</a>; i++)
<a name="l00562"></a>00562         {
<a name="l00563"></a>00563           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).GetM(); j++)
<a name="l00564"></a>00564             cout &lt;&lt; (i+1) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Index(j)+1
<a name="l00565"></a>00565                  &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Value(j) &lt;&lt; endl;
<a name="l00566"></a>00566         }
<a name="l00567"></a>00567     <span class="keywordflow">else</span>
<a name="l00568"></a>00568       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4" title="Number of columns.">n_</a>; i++)
<a name="l00569"></a>00569         {
<a name="l00570"></a>00570           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).GetM(); j++)
<a name="l00571"></a>00571             cout &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Index(j)+1 &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; i+1
<a name="l00572"></a>00572                  &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Value(j) &lt;&lt; endl;
<a name="l00573"></a>00573         }
<a name="l00574"></a>00574   }
<a name="l00575"></a>00575 
<a name="l00576"></a>00576 
<a name="l00578"></a>00578 
<a name="l00584"></a>00584   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00585"></a>00585   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ac469920c36d5a57516a3e53c0c4e3126" title="Assembles the matrix.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Assemble</a>()
<a name="l00586"></a>00586   {
<a name="l00587"></a>00587     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.GetM(); i++)
<a name="l00588"></a>00588       <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Assemble();
<a name="l00589"></a>00589   }
<a name="l00590"></a>00590 
<a name="l00591"></a>00591 
<a name="l00593"></a>00593 
<a name="l00596"></a>00596   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00597"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a4b87eb77f9690ec605e9a1e7e1d155c5">00597</a>   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l00598"></a>00598   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a4b87eb77f9690ec605e9a1e7e1d155c5" title="Removes small coefficients from entries.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00599"></a>00599 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a4b87eb77f9690ec605e9a1e7e1d155c5" title="Removes small coefficients from entries.">  RemoveSmallEntry</a>(<span class="keyword">const</span> T0&amp; epsilon)
<a name="l00600"></a>00600   {
<a name="l00601"></a>00601     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.GetM(); i++)
<a name="l00602"></a>00602       <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).RemoveSmallEntry(epsilon);
<a name="l00603"></a>00603   }
<a name="l00604"></a>00604 
<a name="l00605"></a>00605 
<a name="l00607"></a>00607   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00608"></a>00608   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#af92baadfc393d70b6e3f3ecaa25d16c7" title="Matrix is initialized to the identity matrix.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::SetIdentity</a>()
<a name="l00609"></a>00609   {
<a name="l00610"></a>00610     this-&gt;n_ = this-&gt;m_;
<a name="l00611"></a>00611     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00612"></a>00612       {
<a name="l00613"></a>00613         <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Reallocate(1);
<a name="l00614"></a>00614         <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Index(0) = i;
<a name="l00615"></a>00615         <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Value(0) = T(1);
<a name="l00616"></a>00616       }
<a name="l00617"></a>00617   }
<a name="l00618"></a>00618 
<a name="l00619"></a>00619 
<a name="l00621"></a>00621   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00622"></a>00622   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a06fc483908e9219787dfc9331b07d828" title="Non-zero entries are set to 0 (but not removed).">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Zero</a>()
<a name="l00623"></a>00623   {
<a name="l00624"></a>00624     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.GetM(); i++)
<a name="l00625"></a>00625       <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Zero();
<a name="l00626"></a>00626   }
<a name="l00627"></a>00627 
<a name="l00628"></a>00628 
<a name="l00630"></a>00630   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00631"></a>00631   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a806a12c725733434b08b4276de7fbcee" title="Non-zero entries are filled with values 0, 1, 2, 3 ...">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::Fill</a>()
<a name="l00632"></a>00632   {
<a name="l00633"></a>00633     <span class="keywordtype">int</span> value = 0;
<a name="l00634"></a>00634     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.GetM(); i++)
<a name="l00635"></a>00635       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).GetM(); j++)
<a name="l00636"></a>00636         <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Value(j) = value++;
<a name="l00637"></a>00637   }
<a name="l00638"></a>00638 
<a name="l00639"></a>00639 
<a name="l00641"></a>00641   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allo&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l00642"></a>00642   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a806a12c725733434b08b4276de7fbcee" title="Non-zero entries are filled with values 0, 1, 2, 3 ...">Matrix_ArraySparse&lt;T, Prop, Storage, Allo&gt;::Fill</a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00643"></a>00643   {
<a name="l00644"></a>00644     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.GetM(); i++)
<a name="l00645"></a>00645       <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Fill(x);
<a name="l00646"></a>00646   }
<a name="l00647"></a>00647 
<a name="l00648"></a>00648 
<a name="l00650"></a>00650   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00651"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a28d0b06c82e4bb29f36c872f71fe7b48">00651</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00652"></a>00652   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php" title="Sparse Array-matrix class.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00653"></a>00653   <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a28d0b06c82e4bb29f36c872f71fe7b48" title="Non-zero entries are set to a given value x.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00654"></a>00654   {
<a name="l00655"></a>00655     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a806a12c725733434b08b4276de7fbcee" title="Non-zero entries are filled with values 0, 1, 2, 3 ...">Fill</a>(x);
<a name="l00656"></a>00656   }
<a name="l00657"></a>00657 
<a name="l00658"></a>00658 
<a name="l00660"></a>00660   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00661"></a>00661   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#ae7161fac73344a98f937abbf957fdbb3" title="Non-zero entries take a random value.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::FillRand</a>()
<a name="l00662"></a>00662   {
<a name="l00663"></a>00663     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.GetM(); i++)
<a name="l00664"></a>00664       <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).FillRand();
<a name="l00665"></a>00665   }
<a name="l00666"></a>00666 
<a name="l00667"></a>00667 
<a name="l00669"></a>00669 
<a name="l00676"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a014e4dca632c4955660ad849db2cb363">00676</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00677"></a>00677   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a014e4dca632c4955660ad849db2cb363" title="Writes the matrix in a file.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00678"></a>00678 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a014e4dca632c4955660ad849db2cb363" title="Writes the matrix in a file.">  Write</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00679"></a>00679 <span class="keyword">  </span>{
<a name="l00680"></a>00680     ofstream FileStream;
<a name="l00681"></a>00681     FileStream.open(FileName.c_str());
<a name="l00682"></a>00682 
<a name="l00683"></a>00683 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00684"></a>00684 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00685"></a>00685     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00686"></a>00686       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::Write(string FileName)&quot;</span>,
<a name="l00687"></a>00687                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00688"></a>00688 <span class="preprocessor">#endif</span>
<a name="l00689"></a>00689 <span class="preprocessor"></span>
<a name="l00690"></a>00690     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a014e4dca632c4955660ad849db2cb363" title="Writes the matrix in a file.">Write</a>(FileStream);
<a name="l00691"></a>00691 
<a name="l00692"></a>00692     FileStream.close();
<a name="l00693"></a>00693   }
<a name="l00694"></a>00694 
<a name="l00695"></a>00695 
<a name="l00697"></a>00697 
<a name="l00704"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#afaaf09b0ad951e8b82603e2e6f4d79c4">00704</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00705"></a>00705   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a014e4dca632c4955660ad849db2cb363" title="Writes the matrix in a file.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00706"></a>00706 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a014e4dca632c4955660ad849db2cb363" title="Writes the matrix in a file.">  Write</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00707"></a>00707 <span class="keyword">  </span>{
<a name="l00708"></a>00708 
<a name="l00709"></a>00709 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00710"></a>00710 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00711"></a>00711     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00712"></a>00712       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l00713"></a>00713                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00714"></a>00714 <span class="preprocessor">#endif</span>
<a name="l00715"></a>00715 <span class="preprocessor"></span>
<a name="l00716"></a>00716     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l00717"></a>00717                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00718"></a>00718     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;n_)),
<a name="l00719"></a>00719                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00720"></a>00720 
<a name="l00721"></a>00721     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00722"></a>00722       <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Write(FileStream);
<a name="l00723"></a>00723   }
<a name="l00724"></a>00724 
<a name="l00725"></a>00725 
<a name="l00727"></a>00727 
<a name="l00731"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#adf0d3ec9d5ffeb1b3d46f523051d1412">00731</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00732"></a>00732   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#adf0d3ec9d5ffeb1b3d46f523051d1412" title="Writes the matrix in a file.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00733"></a>00733 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#adf0d3ec9d5ffeb1b3d46f523051d1412" title="Writes the matrix in a file.">  WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00734"></a>00734 <span class="keyword">  </span>{
<a name="l00735"></a>00735     ofstream FileStream; FileStream.precision(14);
<a name="l00736"></a>00736     FileStream.open(FileName.c_str());
<a name="l00737"></a>00737 
<a name="l00738"></a>00738 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00739"></a>00739 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00740"></a>00740     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00741"></a>00741       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::Write(string FileName)&quot;</span>,
<a name="l00742"></a>00742                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00743"></a>00743 <span class="preprocessor">#endif</span>
<a name="l00744"></a>00744 <span class="preprocessor"></span>
<a name="l00745"></a>00745     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#adf0d3ec9d5ffeb1b3d46f523051d1412" title="Writes the matrix in a file.">WriteText</a>(FileStream);
<a name="l00746"></a>00746 
<a name="l00747"></a>00747     FileStream.close();
<a name="l00748"></a>00748   }
<a name="l00749"></a>00749 
<a name="l00750"></a>00750 
<a name="l00752"></a>00752 
<a name="l00756"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a6037e1907164259fa78d2abaa1380577">00756</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00757"></a>00757   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#adf0d3ec9d5ffeb1b3d46f523051d1412" title="Writes the matrix in a file.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00758"></a>00758 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#adf0d3ec9d5ffeb1b3d46f523051d1412" title="Writes the matrix in a file.">  WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00759"></a>00759 <span class="keyword">  </span>{
<a name="l00760"></a>00760 
<a name="l00761"></a>00761 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00762"></a>00762 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00763"></a>00763     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00764"></a>00764       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l00765"></a>00765                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00766"></a>00766 <span class="preprocessor">#endif</span>
<a name="l00767"></a>00767 <span class="preprocessor"></span>
<a name="l00768"></a>00768     <span class="comment">// conversion in coordinate format (1-index convention)</span>
<a name="l00769"></a>00769     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> IndRow, IndCol; <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> Value;
<a name="l00770"></a>00770     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; leaf_class =
<a name="l00771"></a>00771       <span class="keyword">static_cast&lt;</span><span class="keyword">const </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; <span class="keyword">&gt;</span>(*this);
<a name="l00772"></a>00772 
<a name="l00773"></a>00773     <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(leaf_class, IndRow, IndCol,
<a name="l00774"></a>00774                                  Value, 1, <span class="keyword">true</span>);
<a name="l00775"></a>00775 
<a name="l00776"></a>00776     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; IndRow.GetM(); i++)
<a name="l00777"></a>00777       FileStream &lt;&lt; IndRow(i) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; IndCol(i) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">Value</a>(i) &lt;&lt; <span class="charliteral">&#39;\n&#39;</span>;
<a name="l00778"></a>00778 
<a name="l00779"></a>00779   }
<a name="l00780"></a>00780 
<a name="l00781"></a>00781 
<a name="l00783"></a>00783 
<a name="l00790"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a2e644c704eba4e3c531c403952189d86">00790</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00791"></a>00791   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a2e644c704eba4e3c531c403952189d86" title="Reads the matrix from a file.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00792"></a>00792 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a2e644c704eba4e3c531c403952189d86" title="Reads the matrix from a file.">  Read</a>(<span class="keywordtype">string</span> FileName)
<a name="l00793"></a>00793   {
<a name="l00794"></a>00794     ifstream FileStream;
<a name="l00795"></a>00795     FileStream.open(FileName.c_str());
<a name="l00796"></a>00796 
<a name="l00797"></a>00797 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00798"></a>00798 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00799"></a>00799     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00800"></a>00800       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::Read(string FileName)&quot;</span>,
<a name="l00801"></a>00801                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00802"></a>00802 <span class="preprocessor">#endif</span>
<a name="l00803"></a>00803 <span class="preprocessor"></span>
<a name="l00804"></a>00804     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a2e644c704eba4e3c531c403952189d86" title="Reads the matrix from a file.">Read</a>(FileStream);
<a name="l00805"></a>00805 
<a name="l00806"></a>00806     FileStream.close();
<a name="l00807"></a>00807   }
<a name="l00808"></a>00808 
<a name="l00809"></a>00809 
<a name="l00811"></a>00811 
<a name="l00818"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#abeb6e31b268538d3c43fc8f795e994d4">00818</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00819"></a>00819   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a2e644c704eba4e3c531c403952189d86" title="Reads the matrix from a file.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00820"></a>00820 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a2e644c704eba4e3c531c403952189d86" title="Reads the matrix from a file.">  Read</a>(istream&amp; FileStream)
<a name="l00821"></a>00821   {
<a name="l00822"></a>00822 
<a name="l00823"></a>00823 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00824"></a>00824 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00825"></a>00825     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00826"></a>00826       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::Read(ofstream&amp; FileStream)&quot;</span>,
<a name="l00827"></a>00827                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00828"></a>00828 <span class="preprocessor">#endif</span>
<a name="l00829"></a>00829 <span class="preprocessor"></span>
<a name="l00830"></a>00830     FileStream.read(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l00831"></a>00831                     <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00832"></a>00832     FileStream.read(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;n_)),
<a name="l00833"></a>00833                     <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00834"></a>00834 
<a name="l00835"></a>00835     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>.Reallocate(this-&gt;m_);
<a name="l00836"></a>00836     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00837"></a>00837       <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Read(FileStream);
<a name="l00838"></a>00838 
<a name="l00839"></a>00839 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00840"></a>00840 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l00841"></a>00841     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00842"></a>00842       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::Read(istream&amp; FileStream)&quot;</span>,
<a name="l00843"></a>00843                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Input operation failed.&quot;</span>)
<a name="l00844"></a>00844                     + string(<span class="stringliteral">&quot; The input file may have been removed&quot;</span>)
<a name="l00845"></a>00845                     + <span class="stringliteral">&quot; or may not contain enough data.&quot;</span>);
<a name="l00846"></a>00846 <span class="preprocessor">#endif</span>
<a name="l00847"></a>00847 <span class="preprocessor"></span>
<a name="l00848"></a>00848   }
<a name="l00849"></a>00849 
<a name="l00850"></a>00850 
<a name="l00852"></a>00852 
<a name="l00856"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a22e85b66114b00ba89b1615bc1839b0c">00856</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00857"></a>00857   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a22e85b66114b00ba89b1615bc1839b0c" title="Reads the matrix from a file.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00858"></a>00858 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a22e85b66114b00ba89b1615bc1839b0c" title="Reads the matrix from a file.">  ReadText</a>(<span class="keywordtype">string</span> FileName)
<a name="l00859"></a>00859   {
<a name="l00860"></a>00860     ifstream FileStream;
<a name="l00861"></a>00861     FileStream.open(FileName.c_str());
<a name="l00862"></a>00862 
<a name="l00863"></a>00863 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00864"></a>00864 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00865"></a>00865     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00866"></a>00866       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::ReadText(string FileName)&quot;</span>,
<a name="l00867"></a>00867                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00868"></a>00868 <span class="preprocessor">#endif</span>
<a name="l00869"></a>00869 <span class="preprocessor"></span>
<a name="l00870"></a>00870     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a22e85b66114b00ba89b1615bc1839b0c" title="Reads the matrix from a file.">ReadText</a>(FileStream);
<a name="l00871"></a>00871 
<a name="l00872"></a>00872     FileStream.close();
<a name="l00873"></a>00873   }
<a name="l00874"></a>00874 
<a name="l00875"></a>00875 
<a name="l00877"></a>00877 
<a name="l00881"></a><a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a4bac922eaed55ed72c4fcfac89b405ca">00881</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00882"></a>00882   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a22e85b66114b00ba89b1615bc1839b0c" title="Reads the matrix from a file.">Matrix_ArraySparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00883"></a>00883 <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a22e85b66114b00ba89b1615bc1839b0c" title="Reads the matrix from a file.">  ReadText</a>(istream&amp; FileStream)
<a name="l00884"></a>00884   {
<a name="l00885"></a>00885     <span class="comment">// previous elements are removed</span>
<a name="l00886"></a>00886     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a61227beea7612dd22bf1f46546bb5cde" title="Clears the matrix.">Clear</a>();
<a name="l00887"></a>00887 
<a name="l00888"></a>00888 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00889"></a>00889 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00890"></a>00890     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00891"></a>00891       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::ReadText(ofstream&amp; FileStream)&quot;</span>,
<a name="l00892"></a>00892                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00893"></a>00893 <span class="preprocessor">#endif</span>
<a name="l00894"></a>00894 <span class="preprocessor"></span>
<a name="l00895"></a>00895     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> values;
<a name="l00896"></a>00896     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> row_numbers, col_numbers;
<a name="l00897"></a>00897     T entry; <span class="keywordtype">int</span> row = 0, col;
<a name="l00898"></a>00898     <span class="keywordtype">int</span> nb_elt = 0;
<a name="l00899"></a>00899     <span class="keywordflow">while</span> (!FileStream.eof())
<a name="l00900"></a>00900       {
<a name="l00901"></a>00901         <span class="comment">// new entry is read (1-index)</span>
<a name="l00902"></a>00902         FileStream &gt;&gt; row &gt;&gt; col &gt;&gt; entry;
<a name="l00903"></a>00903 
<a name="l00904"></a>00904         <span class="keywordflow">if</span> (FileStream.fail())
<a name="l00905"></a>00905           <span class="keywordflow">break</span>;
<a name="l00906"></a>00906         <span class="keywordflow">else</span>
<a name="l00907"></a>00907           {
<a name="l00908"></a>00908 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00909"></a>00909 <span class="preprocessor"></span>            <span class="keywordflow">if</span> (row &lt; 1)
<a name="l00910"></a>00910               <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ArraySparse::ReadText&quot;</span>)+
<a name="l00911"></a>00911                             <span class="stringliteral">&quot;(istream&amp; FileStream)&quot;</span>,
<a name="l00912"></a>00912                             <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Error : Row number should be greater &quot;</span>)
<a name="l00913"></a>00913                             + <span class="stringliteral">&quot;than 0 but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(row));
<a name="l00914"></a>00914 
<a name="l00915"></a>00915             <span class="keywordflow">if</span> (col &lt; 1)
<a name="l00916"></a>00916               <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ArraySparse::ReadText&quot;</span>)+
<a name="l00917"></a>00917                             <span class="stringliteral">&quot;(istream&amp; FileStream)&quot;</span>,
<a name="l00918"></a>00918                             <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Error : Column number should be greater&quot;</span>)
<a name="l00919"></a>00919                             + <span class="stringliteral">&quot; than 0 but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(col));
<a name="l00920"></a>00920 <span class="preprocessor">#endif</span>
<a name="l00921"></a>00921 <span class="preprocessor"></span>
<a name="l00922"></a>00922             nb_elt++;
<a name="l00923"></a>00923 
<a name="l00924"></a>00924             <span class="comment">// inserting new element</span>
<a name="l00925"></a>00925             <span class="keywordflow">if</span> (nb_elt &gt; values.<a class="code" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae" title="Returns the number of elements.">GetM</a>())
<a name="l00926"></a>00926               {
<a name="l00927"></a>00927                 values.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a99dda1b202ce562645f890897c593515" title="Changes the length of the vector, and keeps previous values.">Resize</a>(2*nb_elt);
<a name="l00928"></a>00928                 row_numbers.Resize(2*nb_elt);
<a name="l00929"></a>00929                 col_numbers.Resize(2*nb_elt);
<a name="l00930"></a>00930               }
<a name="l00931"></a>00931 
<a name="l00932"></a>00932             values(nb_elt-1) = entry;
<a name="l00933"></a>00933             row_numbers(nb_elt-1) = row;
<a name="l00934"></a>00934             col_numbers(nb_elt-1) = col;
<a name="l00935"></a>00935           }
<a name="l00936"></a>00936       }
<a name="l00937"></a>00937 
<a name="l00938"></a>00938     <span class="keywordflow">if</span> (nb_elt &gt; 0)
<a name="l00939"></a>00939       {
<a name="l00940"></a>00940         <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; leaf_class =
<a name="l00941"></a>00941           <span class="keyword">static_cast&lt;</span><a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; <span class="keyword">&gt;</span>(*this);
<a name="l00942"></a>00942         row_numbers.Resize(nb_elt);
<a name="l00943"></a>00943         col_numbers.Resize(nb_elt);
<a name="l00944"></a>00944         values.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a99dda1b202ce562645f890897c593515" title="Changes the length of the vector, and keeps previous values.">Resize</a>(nb_elt);
<a name="l00945"></a>00945         <a class="code" href="namespace_seldon.php#ad4977fcff081edc62cc482b1aedaf0d9" title="Conversion from coordinate format to RowSparse.">ConvertMatrix_from_Coordinates</a>(row_numbers, col_numbers, values,
<a name="l00946"></a>00946                                        leaf_class, 1);
<a name="l00947"></a>00947       }
<a name="l00948"></a>00948   }
<a name="l00949"></a>00949 
<a name="l00950"></a>00950 
<a name="l00952"></a>00952   <span class="comment">// MATRIX&lt;ARRAY_COLSPARSE&gt; //</span>
<a name="l00954"></a>00954 <span class="comment"></span>
<a name="l00955"></a>00955 
<a name="l00957"></a>00957 
<a name="l00960"></a>00960   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00961"></a>00961   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::Matrix</a>()  throw():
<a name="l00962"></a>00962     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php" title="Sparse Array-matrix class.">Matrix_ArraySparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_col_sparse.php">ArrayColSparse</a>, Allocator&gt;()
<a name="l00963"></a>00963   {
<a name="l00964"></a>00964   }
<a name="l00965"></a>00965 
<a name="l00966"></a>00966 
<a name="l00968"></a>00968 
<a name="l00973"></a>00973   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00974"></a>00974   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#a06100dad6cbda6411aeb3e87665175da" title="Default constructor.">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l00975"></a>00975     Matrix_ArraySparse&lt;T, Prop, ArrayColSparse, Allocator&gt;(i, j)
<a name="l00976"></a>00976   {
<a name="l00977"></a>00977   }
<a name="l00978"></a>00978 
<a name="l00979"></a>00979 
<a name="l00981"></a>00981   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00982"></a>00982   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#a00b55880d103fffa13cd1eaa1e49dac9" title="Clears column i.">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::ClearColumn</a>(<span class="keywordtype">int</span> i)
<a name="l00983"></a>00983   {
<a name="l00984"></a>00984     this-&gt;val_(i).Clear();
<a name="l00985"></a>00985   }
<a name="l00986"></a>00986 
<a name="l00987"></a>00987 
<a name="l00989"></a>00989 
<a name="l00993"></a>00993   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Alloc&gt; <span class="keyword">inline</span>
<a name="l00994"></a>00994   <span class="keywordtype">void</span> Matrix&lt;T, Prop, ArrayColSparse, Alloc&gt;::ReallocateColumn(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l00995"></a>00995   {
<a name="l00996"></a>00996     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6" title="rows or columns">val_</a>(i).Reallocate(j);
<a name="l00997"></a>00997   }
<a name="l00998"></a>00998 
<a name="l00999"></a>00999 
<a name="l01001"></a>01001 
<a name="l01005"></a>01005   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01006"></a>01006   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#ad92aa8f008beddbec6d8a41711409655" title="Reallocates column i.">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::ResizeColumn</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01007"></a>01007   {
<a name="l01008"></a>01008     this-&gt;val_(i).Resize(j);
<a name="l01009"></a>01009   }
<a name="l01010"></a>01010 
<a name="l01011"></a>01011 
<a name="l01013"></a>01013 
<a name="l01017"></a>01017   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01018"></a>01018   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#a9007597f6b3c40534bd422990e75da7f" title="Swaps two columns.">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::SwapColumn</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01019"></a>01019   {
<a name="l01020"></a>01020     Swap(this-&gt;val_(i), this-&gt;val_(j));
<a name="l01021"></a>01021   }
<a name="l01022"></a>01022 
<a name="l01023"></a>01023 
<a name="l01025"></a>01025 
<a name="l01029"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#aff85ed83508cf4e0bf75079ae2e73b4f">01029</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01030"></a>01030   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::</a>
<a name="l01031"></a>01031 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceIndexColumn</a>(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l01032"></a>01032   {
<a name="l01033"></a>01033     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_(i).GetM(); j++)
<a name="l01034"></a>01034       this-&gt;val_(i).Index(j) = new_index(j);
<a name="l01035"></a>01035   }
<a name="l01036"></a>01036 
<a name="l01037"></a>01037 
<a name="l01039"></a>01039 
<a name="l01043"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#a6db90999f5bd69e71afcf585b11a00e7">01043</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01044"></a>01044   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::</a>
<a name="l01045"></a>01045 <a class="code" href="class_seldon_1_1_matrix.php">  GetColumnSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01046"></a>01046 <span class="keyword">  </span>{
<a name="l01047"></a>01047     <span class="keywordflow">return</span> this-&gt;val_(i).GetSize();
<a name="l01048"></a>01048   }
<a name="l01049"></a>01049 
<a name="l01050"></a>01050 
<a name="l01052"></a>01052   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01053"></a>01053   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::PrintColumn</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01054"></a>01054 <span class="keyword">  </span>{
<a name="l01055"></a>01055     this-&gt;val_(i).Print();
<a name="l01056"></a>01056   }
<a name="l01057"></a>01057 
<a name="l01058"></a>01058 
<a name="l01060"></a>01060 
<a name="l01065"></a>01065   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01066"></a>01066   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#aa16ced801acf542fc6a0d2588c226500" title="Assembles a column.">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::AssembleColumn</a>(<span class="keywordtype">int</span> i)
<a name="l01067"></a>01067   {
<a name="l01068"></a>01068     this-&gt;val_(i).Assemble();
<a name="l01069"></a>01069   }
<a name="l01070"></a>01070 
<a name="l01071"></a>01071 
<a name="l01073"></a>01073 
<a name="l01078"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#ab2b0465e1e4a7323bff043c74f95b7f3">01078</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01079"></a>01079   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::</a>
<a name="l01080"></a>01080 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; val)
<a name="l01081"></a>01081   {
<a name="l01082"></a>01082     this-&gt;val_(j).AddInteraction(i, val);
<a name="l01083"></a>01083   }
<a name="l01084"></a>01084 
<a name="l01085"></a>01085 
<a name="l01087"></a>01087 
<a name="l01093"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#a62961435a3e36a3b89d8f1d452a26ee7">01093</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01094"></a>01094   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::</a>
<a name="l01095"></a>01095 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keywordtype">int</span>* col_, T* value_)
<a name="l01096"></a>01096   {
<a name="l01097"></a>01097     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> col;
<a name="l01098"></a>01098     col.SetData(nb, col_);
<a name="l01099"></a>01099     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val;
<a name="l01100"></a>01100     val.SetData(nb, value_);
<a name="l01101"></a>01101     AddInteractionRow(i, nb, col, val);
<a name="l01102"></a>01102     col.Nullify();
<a name="l01103"></a>01103     val.Nullify();
<a name="l01104"></a>01104   }
<a name="l01105"></a>01105 
<a name="l01106"></a>01106 
<a name="l01108"></a>01108 
<a name="l01114"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#a5f71f557c041e0d4ee09f656c23e4719">01114</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01115"></a>01115   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::</a>
<a name="l01116"></a>01116 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keywordtype">int</span>* row_, T* value_)
<a name="l01117"></a>01117   {
<a name="l01118"></a>01118     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> row;
<a name="l01119"></a>01119     row.SetData(nb, row_);
<a name="l01120"></a>01120     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val;
<a name="l01121"></a>01121     val.SetData(nb, value_);
<a name="l01122"></a>01122     AddInteractionColumn(i, nb, row, val);
<a name="l01123"></a>01123     row.Nullify();
<a name="l01124"></a>01124     val.Nullify();
<a name="l01125"></a>01125   }
<a name="l01126"></a>01126 
<a name="l01127"></a>01127 
<a name="l01129"></a>01129 
<a name="l01135"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#a3015e7a923f01ee2581975650294ad6e">01135</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01136"></a>01136   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::</a>
<a name="l01137"></a>01137 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l01138"></a>01138                     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val)
<a name="l01139"></a>01139   {
<a name="l01140"></a>01140     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nb; j++)
<a name="l01141"></a>01141       this-&gt;val_(col(j)).AddInteraction(i, val(j));
<a name="l01142"></a>01142   }
<a name="l01143"></a>01143 
<a name="l01144"></a>01144 
<a name="l01146"></a>01146 
<a name="l01152"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#ac658b9f8e33a4656b1eda205677218db">01152</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01153"></a>01153   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;::</a>
<a name="l01154"></a>01154 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l01155"></a>01155                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val)
<a name="l01156"></a>01156   {
<a name="l01157"></a>01157     this-&gt;val_(i).AddInteractionRow(nb, row, val);
<a name="l01158"></a>01158   }
<a name="l01159"></a>01159 
<a name="l01160"></a>01160 
<a name="l01162"></a>01162   <span class="comment">// MATRIX&lt;ARRAY_ROWSPARSE&gt; //</span>
<a name="l01164"></a>01164 <span class="comment"></span>
<a name="l01165"></a>01165 
<a name="l01167"></a>01167 
<a name="l01170"></a>01170   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01171"></a>01171   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::Matrix</a>()  throw():
<a name="l01172"></a>01172     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php" title="Sparse Array-matrix class.">Matrix_ArraySparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_row_sparse.php">ArrayRowSparse</a>, Allocator&gt;()
<a name="l01173"></a>01173   {
<a name="l01174"></a>01174   }
<a name="l01175"></a>01175 
<a name="l01176"></a>01176 
<a name="l01178"></a>01178 
<a name="l01183"></a>01183   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01184"></a>01184   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a45d914bbc71d7f861330e804296906c2" title="Default constructor.">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01185"></a>01185     Matrix_ArraySparse&lt;T, Prop, ArrayRowSparse, Allocator&gt;(i, j)
<a name="l01186"></a>01186   {
<a name="l01187"></a>01187   }
<a name="l01188"></a>01188 
<a name="l01189"></a>01189 
<a name="l01191"></a>01191   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01192"></a>01192   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#ad7e78da914d3e0d3818b8c21ec8e7bbf" title="Clears a row.">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::ClearRow</a>(<span class="keywordtype">int</span> i)
<a name="l01193"></a>01193   {
<a name="l01194"></a>01194     this-&gt;val_(i).Clear();
<a name="l01195"></a>01195   }
<a name="l01196"></a>01196 
<a name="l01197"></a>01197 
<a name="l01199"></a>01199 
<a name="l01204"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a141cfc9e4bacfcb106564df3154a104b">01204</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01205"></a>01205   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::</a>
<a name="l01206"></a>01206 <a class="code" href="class_seldon_1_1_matrix.php">  ReallocateRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01207"></a>01207   {
<a name="l01208"></a>01208     this-&gt;val_(i).Reallocate(j);
<a name="l01209"></a>01209   }
<a name="l01210"></a>01210 
<a name="l01211"></a>01211 
<a name="l01213"></a>01213 
<a name="l01218"></a>01218   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01219"></a>01219   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::ResizeRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01220"></a>01220   {
<a name="l01221"></a>01221     this-&gt;val_(i).Resize(j);
<a name="l01222"></a>01222   }
<a name="l01223"></a>01223 
<a name="l01224"></a>01224 
<a name="l01226"></a>01226 
<a name="l01230"></a>01230   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01231"></a>01231   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a6a476c1d342322cb6b0d16d19de837dc" title="Swaps two rows.">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::SwapRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01232"></a>01232   {
<a name="l01233"></a>01233     Swap(this-&gt;val_(i), this-&gt;val_(j));
<a name="l01234"></a>01234   }
<a name="l01235"></a>01235 
<a name="l01236"></a>01236 
<a name="l01238"></a>01238 
<a name="l01242"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a29c3376f6702a15782a46c240c7ce8bd">01242</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01243"></a>01243   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::</a>
<a name="l01244"></a>01244 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceIndexRow</a>(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l01245"></a>01245   {
<a name="l01246"></a>01246     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_(i).GetM(); j++)
<a name="l01247"></a>01247       this-&gt;val_(i).Index(j) = new_index(j);
<a name="l01248"></a>01248   }
<a name="l01249"></a>01249 
<a name="l01250"></a>01250 
<a name="l01252"></a>01252 
<a name="l01256"></a>01256   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01257"></a>01257   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::GetRowSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01258"></a>01258 <span class="keyword">  </span>{
<a name="l01259"></a>01259     <span class="keywordflow">return</span> this-&gt;val_(i).GetSize();
<a name="l01260"></a>01260   }
<a name="l01261"></a>01261 
<a name="l01262"></a>01262 
<a name="l01264"></a>01264   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01265"></a>01265   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#aedf0c511638d6bcc7f5b8e4cfddf6e93" title="Displays non-zero values of a row.">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::PrintRow</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01266"></a>01266 <span class="keyword">  </span>{
<a name="l01267"></a>01267     this-&gt;val_(i).Print();
<a name="l01268"></a>01268   }
<a name="l01269"></a>01269 
<a name="l01270"></a>01270 
<a name="l01272"></a>01272 
<a name="l01277"></a>01277   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01278"></a>01278   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a73b4fcf4fa577bbe4a88cf0a2f9a0bbc" title="Assembles a row.">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::AssembleRow</a>(<span class="keywordtype">int</span> i)
<a name="l01279"></a>01279   {
<a name="l01280"></a>01280     this-&gt;val_(i).Assemble();
<a name="l01281"></a>01281   }
<a name="l01282"></a>01282 
<a name="l01284"></a>01284 
<a name="l01289"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a4bed47faffc46ab4a08c0869e5f7cdad">01289</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01290"></a>01290   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::</a>
<a name="l01291"></a>01291 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; val)
<a name="l01292"></a>01292   {
<a name="l01293"></a>01293     this-&gt;val_(i).AddInteraction(j, val);
<a name="l01294"></a>01294   }
<a name="l01295"></a>01295 
<a name="l01296"></a>01296 
<a name="l01298"></a>01298 
<a name="l01304"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a6428579a6d7521fb171b28c12f48a030">01304</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01305"></a>01305   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::</a>
<a name="l01306"></a>01306 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keywordtype">int</span>* col_, T* value_)
<a name="l01307"></a>01307   {
<a name="l01308"></a>01308     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> col;
<a name="l01309"></a>01309     col.SetData(nb, col_);
<a name="l01310"></a>01310     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val;
<a name="l01311"></a>01311     val.SetData(nb, value_);
<a name="l01312"></a>01312     AddInteractionRow(i, nb, col, val);
<a name="l01313"></a>01313     col.Nullify();
<a name="l01314"></a>01314     val.Nullify();
<a name="l01315"></a>01315   }
<a name="l01316"></a>01316 
<a name="l01317"></a>01317 
<a name="l01319"></a>01319 
<a name="l01325"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#aba0481a22dc304dc647d1ff3efdf9916">01325</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01326"></a>01326   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::</a>
<a name="l01327"></a>01327 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keywordtype">int</span>* row_, T* value_)
<a name="l01328"></a>01328   {
<a name="l01329"></a>01329     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> row;
<a name="l01330"></a>01330     row.SetData(nb, row_);
<a name="l01331"></a>01331     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val;
<a name="l01332"></a>01332     val.SetData(nb, value_);
<a name="l01333"></a>01333     AddInteractionColumn(i, nb, row, val);
<a name="l01334"></a>01334     row.Nullify();
<a name="l01335"></a>01335     val.Nullify();
<a name="l01336"></a>01336   }
<a name="l01337"></a>01337 
<a name="l01338"></a>01338 
<a name="l01340"></a>01340 
<a name="l01346"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a0563d3c1193b5095374765262957006f">01346</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01347"></a>01347   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::</a>
<a name="l01348"></a>01348 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l01349"></a>01349                     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val)
<a name="l01350"></a>01350   {
<a name="l01351"></a>01351     this-&gt;val_(i).AddInteractionRow(nb, col, val);
<a name="l01352"></a>01352   }
<a name="l01353"></a>01353 
<a name="l01354"></a>01354 
<a name="l01356"></a>01356 
<a name="l01362"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a1f7b4b524875b008fc0aa212d022ac37">01362</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01363"></a>01363   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;::</a>
<a name="l01364"></a>01364 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l01365"></a>01365                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val)
<a name="l01366"></a>01366   {
<a name="l01367"></a>01367     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nb; j++)
<a name="l01368"></a>01368       this-&gt;val_(row(j)).AddInteraction(i, val(j));
<a name="l01369"></a>01369   }
<a name="l01370"></a>01370 
<a name="l01371"></a>01371 
<a name="l01373"></a>01373   <span class="comment">// MATRIX&lt;ARRAY_COLSYMSPARSE&gt; //</span>
<a name="l01375"></a>01375 <span class="comment"></span>
<a name="l01376"></a>01376 
<a name="l01378"></a>01378 
<a name="l01381"></a>01381   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01382"></a>01382   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::Matrix</a>()  throw():
<a name="l01383"></a>01383     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php" title="Sparse Array-matrix class.">Matrix_ArraySparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_col_sym_sparse.php">ArrayColSymSparse</a>, Allocator&gt;()
<a name="l01384"></a>01384   {
<a name="l01385"></a>01385   }
<a name="l01386"></a>01386 
<a name="l01387"></a>01387 
<a name="l01389"></a>01389 
<a name="l01394"></a>01394   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01395"></a>01395   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a49019378c18567a9a3544f5801dd9b33" title="Default constructor.">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01396"></a>01396     Matrix_ArraySparse&lt;T, Prop, ArrayColSymSparse, Allocator&gt;(i, j)
<a name="l01397"></a>01397   {
<a name="l01398"></a>01398   }
<a name="l01399"></a>01399 
<a name="l01400"></a>01400 
<a name="l01401"></a>01401   <span class="comment">/**********************************</span>
<a name="l01402"></a>01402 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l01403"></a>01403 <span class="comment">   **********************************/</span>
<a name="l01404"></a>01404 
<a name="l01405"></a>01405 
<a name="l01407"></a>01407 
<a name="l01413"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a1437cb081d496b576b36ae061cd9c51c">01413</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01414"></a>01414   <span class="keyword">inline</span> T
<a name="l01415"></a>01415   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"></span>
<a name="l01416"></a>01416 <span class="keyword">    const</span>
<a name="l01417"></a>01417 <span class="keyword">  </span>{
<a name="l01418"></a>01418 
<a name="l01419"></a>01419 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01420"></a>01420 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01421"></a>01421       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01422"></a>01422                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01423"></a>01423                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01424"></a>01424     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01425"></a>01425       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01426"></a>01426                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01427"></a>01427                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01428"></a>01428 <span class="preprocessor">#endif</span>
<a name="l01429"></a>01429 <span class="preprocessor"></span>
<a name="l01430"></a>01430     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l01431"></a>01431       <span class="keywordflow">return</span> this-&gt;val_(j)(i);
<a name="l01432"></a>01432 
<a name="l01433"></a>01433     <span class="keywordflow">return</span> this-&gt;val_(i)(j);
<a name="l01434"></a>01434   }
<a name="l01435"></a>01435 
<a name="l01436"></a>01436 
<a name="l01438"></a>01438 
<a name="l01444"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#aaba3e074be42ad7e68fa22407fe485fb">01444</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01445"></a>01445   <span class="keyword">inline</span> T&amp;
<a name="l01446"></a>01446   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01447"></a>01447   {
<a name="l01448"></a>01448 
<a name="l01449"></a>01449 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01450"></a>01450 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01451"></a>01451       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01452"></a>01452                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01453"></a>01453                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01454"></a>01454     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01455"></a>01455       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01456"></a>01456                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01457"></a>01457                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01458"></a>01458 <span class="preprocessor">#endif</span>
<a name="l01459"></a>01459 <span class="preprocessor"></span>
<a name="l01460"></a>01460     <span class="keywordflow">if</span> (i &lt; j)
<a name="l01461"></a>01461       <span class="keywordflow">return</span> this-&gt;val_(j)(i);
<a name="l01462"></a>01462 
<a name="l01463"></a>01463     <span class="keywordflow">return</span> this-&gt;val_(i)(j);
<a name="l01464"></a>01464   }
<a name="l01465"></a>01465 
<a name="l01466"></a>01466 
<a name="l01468"></a>01468   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01469"></a>01469   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::ClearColumn</a>(<span class="keywordtype">int</span> i)
<a name="l01470"></a>01470   {
<a name="l01471"></a>01471     this-&gt;val_(i).Clear();
<a name="l01472"></a>01472   }
<a name="l01473"></a>01473 
<a name="l01474"></a>01474 
<a name="l01476"></a>01476 
<a name="l01481"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a2f50f94ea485c1c81aecdedd76e09116">01481</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01482"></a>01482   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01483"></a>01483 <a class="code" href="class_seldon_1_1_matrix.php">  ReallocateColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01484"></a>01484   {
<a name="l01485"></a>01485     this-&gt;val_(i).Reallocate(j);
<a name="l01486"></a>01486   }
<a name="l01487"></a>01487 
<a name="l01488"></a>01488 
<a name="l01490"></a>01490 
<a name="l01494"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a7350581df0edd83fe9d180db302afd56">01494</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01495"></a>01495   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01496"></a>01496 <a class="code" href="class_seldon_1_1_matrix.php">  ResizeColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01497"></a>01497   {
<a name="l01498"></a>01498     this-&gt;val_(i).Resize(j);
<a name="l01499"></a>01499   }
<a name="l01500"></a>01500 
<a name="l01501"></a>01501 
<a name="l01503"></a>01503 
<a name="l01507"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#ab9de78a2a24aa5fa8cec25f93232c4ff">01507</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01508"></a>01508   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01509"></a>01509 <a class="code" href="class_seldon_1_1_matrix.php">  SwapColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01510"></a>01510   {
<a name="l01511"></a>01511     Swap(this-&gt;val_(i), this-&gt;val_(j));
<a name="l01512"></a>01512   }
<a name="l01513"></a>01513 
<a name="l01514"></a>01514 
<a name="l01516"></a>01516 
<a name="l01520"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#aece56a1f1bdc20ffc86e29d92e8d0c41">01520</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01521"></a>01521   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01522"></a>01522 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceIndexColumn</a>(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l01523"></a>01523   {
<a name="l01524"></a>01524     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_(i).GetM(); j++)
<a name="l01525"></a>01525       this-&gt;val_(i).Index(j) = new_index(j);
<a name="l01526"></a>01526   }
<a name="l01527"></a>01527 
<a name="l01528"></a>01528 
<a name="l01530"></a>01530 
<a name="l01534"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#accfeb4e792ac6f354c2f9c40827e8c3a">01534</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01535"></a>01535   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01536"></a>01536 <a class="code" href="class_seldon_1_1_matrix.php">  GetColumnSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01537"></a>01537 <span class="keyword">  </span>{
<a name="l01538"></a>01538     <span class="keywordflow">return</span> this-&gt;val_(i).GetSize();
<a name="l01539"></a>01539   }
<a name="l01540"></a>01540 
<a name="l01541"></a>01541 
<a name="l01543"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a875318957a4ebbeabb1c01f24ea9e9cf">01543</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01544"></a>01544   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01545"></a>01545 <a class="code" href="class_seldon_1_1_matrix.php">  PrintColumn</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01546"></a>01546 <span class="keyword">  </span>{
<a name="l01547"></a>01547     this-&gt;val_(i).Print();
<a name="l01548"></a>01548   }
<a name="l01549"></a>01549 
<a name="l01550"></a>01550 
<a name="l01552"></a>01552 
<a name="l01557"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a6dc03b72ecdd09350ba69ea8f0188691">01557</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01558"></a>01558   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01559"></a>01559 <a class="code" href="class_seldon_1_1_matrix.php">  AssembleColumn</a>(<span class="keywordtype">int</span> i)
<a name="l01560"></a>01560   {
<a name="l01561"></a>01561     this-&gt;val_(i).Assemble();
<a name="l01562"></a>01562   }
<a name="l01563"></a>01563 
<a name="l01564"></a>01564 
<a name="l01566"></a>01566 
<a name="l01572"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a60c5f3ef5501776c7e57256d2d96d578">01572</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01573"></a>01573   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01574"></a>01574 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keywordtype">int</span>* col_, T* value_)
<a name="l01575"></a>01575   {
<a name="l01576"></a>01576     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> col;
<a name="l01577"></a>01577     col.SetData(nb, col_);
<a name="l01578"></a>01578     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val;
<a name="l01579"></a>01579     val.SetData(nb, value_);
<a name="l01580"></a>01580     AddInteractionRow(i, nb, col, val);
<a name="l01581"></a>01581     col.Nullify();
<a name="l01582"></a>01582     val.Nullify();
<a name="l01583"></a>01583   }
<a name="l01584"></a>01584 
<a name="l01585"></a>01585 
<a name="l01587"></a>01587 
<a name="l01593"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#ae52a1b46f4454ee1591cd6b71c83574c">01593</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01594"></a>01594   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01595"></a>01595 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keywordtype">int</span>* row_, T* value_)
<a name="l01596"></a>01596   {
<a name="l01597"></a>01597     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> row;
<a name="l01598"></a>01598     row.SetData(nb, row_);
<a name="l01599"></a>01599     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val;
<a name="l01600"></a>01600     val.SetData(nb, value_);
<a name="l01601"></a>01601     AddInteractionColumn(i, nb, row, val);
<a name="l01602"></a>01602     row.Nullify();
<a name="l01603"></a>01603     val.Nullify();
<a name="l01604"></a>01604   }
<a name="l01605"></a>01605 
<a name="l01606"></a>01606 
<a name="l01608"></a>01608 
<a name="l01613"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a3d27b66507e6d6fe9e35684eb69e6a3b">01613</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01614"></a>01614   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01615"></a>01615 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; val)
<a name="l01616"></a>01616   {
<a name="l01617"></a>01617     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l01618"></a>01618       this-&gt;val_(j).AddInteraction(i, val);
<a name="l01619"></a>01619   }
<a name="l01620"></a>01620 
<a name="l01621"></a>01621 
<a name="l01623"></a>01623 
<a name="l01629"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#ac128b73917745e8cf0e543abbae95ea6">01629</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01630"></a>01630   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01631"></a>01631 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l01632"></a>01632                     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val)
<a name="l01633"></a>01633   {
<a name="l01634"></a>01634     AddInteractionColumn(i, nb, col, val);
<a name="l01635"></a>01635   }
<a name="l01636"></a>01636 
<a name="l01637"></a>01637 
<a name="l01639"></a>01639 
<a name="l01645"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a3b1e5feec5c47a50c828ff3709380c4d">01645</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01646"></a>01646   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;::</a>
<a name="l01647"></a>01647 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l01648"></a>01648                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val)
<a name="l01649"></a>01649   {
<a name="l01650"></a>01650     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> new_row(nb);
<a name="l01651"></a>01651     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a> new_val(nb);
<a name="l01652"></a>01652     nb = 0;
<a name="l01653"></a>01653     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; new_row.GetM(); j++)
<a name="l01654"></a>01654       <span class="keywordflow">if</span> (row(j) &lt;= i)
<a name="l01655"></a>01655         {
<a name="l01656"></a>01656           new_row(nb) = row(j);
<a name="l01657"></a>01657           new_val(nb) = val(j); nb++;
<a name="l01658"></a>01658         }
<a name="l01659"></a>01659 
<a name="l01660"></a>01660     this-&gt;val_(i).AddInteractionRow(nb, new_row, new_val);
<a name="l01661"></a>01661   }
<a name="l01662"></a>01662 
<a name="l01663"></a>01663 
<a name="l01665"></a>01665   <span class="comment">// MATRIX&lt;ARRAY_ROWSYMSPARSE&gt; //</span>
<a name="l01667"></a>01667 <span class="comment"></span>
<a name="l01668"></a>01668 
<a name="l01670"></a>01670 
<a name="l01673"></a>01673   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01674"></a>01674   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::Matrix</a>()  throw():
<a name="l01675"></a>01675     <a class="code" href="class_seldon_1_1_matrix___array_sparse.php" title="Sparse Array-matrix class.">Matrix_ArraySparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_row_sym_sparse.php">ArrayRowSymSparse</a>, Allocator&gt;()
<a name="l01676"></a>01676   {
<a name="l01677"></a>01677   }
<a name="l01678"></a>01678 
<a name="l01679"></a>01679 
<a name="l01681"></a>01681 
<a name="l01686"></a>01686   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01687"></a>01687   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a9bd699fbd7fe4a62e4216883a322b623" title="Default constructor.">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01688"></a>01688     Matrix_ArraySparse&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;(i, j)
<a name="l01689"></a>01689   {
<a name="l01690"></a>01690   }
<a name="l01691"></a>01691 
<a name="l01692"></a>01692 
<a name="l01693"></a>01693   <span class="comment">/**********************************</span>
<a name="l01694"></a>01694 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l01695"></a>01695 <span class="comment">   **********************************/</span>
<a name="l01696"></a>01696 
<a name="l01697"></a>01697 
<a name="l01699"></a>01699 
<a name="l01705"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a8d872a0e035a2afcc8e40736e8556c44">01705</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01706"></a>01706   <span class="keyword">inline</span> T
<a name="l01707"></a>01707   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"></span>
<a name="l01708"></a>01708 <span class="keyword">    const</span>
<a name="l01709"></a>01709 <span class="keyword">  </span>{
<a name="l01710"></a>01710 
<a name="l01711"></a>01711 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01712"></a>01712 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01713"></a>01713       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l01714"></a>01714                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01715"></a>01715                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01716"></a>01716                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01717"></a>01717     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01718"></a>01718       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>,
<a name="l01719"></a>01719                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01720"></a>01720                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01721"></a>01721                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01722"></a>01722 <span class="preprocessor">#endif</span>
<a name="l01723"></a>01723 <span class="preprocessor"></span>
<a name="l01724"></a>01724     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l01725"></a>01725       <span class="keywordflow">return</span> this-&gt;val_(i)(j);
<a name="l01726"></a>01726 
<a name="l01727"></a>01727     <span class="keywordflow">return</span> this-&gt;val_(j)(i);
<a name="l01728"></a>01728   }
<a name="l01729"></a>01729 
<a name="l01730"></a>01730 
<a name="l01732"></a>01732 
<a name="l01738"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a45ec07c98e6e55fdae57dda5b0c1cda7">01738</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01739"></a>01739   <span class="keyword">inline</span> T&amp;
<a name="l01740"></a>01740   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01741"></a>01741   {
<a name="l01742"></a>01742 
<a name="l01743"></a>01743 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01744"></a>01744 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01745"></a>01745       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01746"></a>01746                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01747"></a>01747                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01748"></a>01748     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01749"></a>01749       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01750"></a>01750                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01751"></a>01751                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01752"></a>01752 <span class="preprocessor">#endif</span>
<a name="l01753"></a>01753 <span class="preprocessor"></span>
<a name="l01754"></a>01754     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l01755"></a>01755       <span class="keywordflow">return</span> this-&gt;val_(i)(j);
<a name="l01756"></a>01756 
<a name="l01757"></a>01757     <span class="keywordflow">return</span> this-&gt;val_(j)(i);
<a name="l01758"></a>01758 
<a name="l01759"></a>01759   }
<a name="l01760"></a>01760 
<a name="l01761"></a>01761 
<a name="l01763"></a>01763   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01764"></a>01764   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::ClearRow</a>(<span class="keywordtype">int</span> i)
<a name="l01765"></a>01765   {
<a name="l01766"></a>01766     this-&gt;val_(i).Clear();
<a name="l01767"></a>01767   }
<a name="l01768"></a>01768 
<a name="l01769"></a>01769 
<a name="l01771"></a>01771 
<a name="l01776"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a02d5086adf71d37c259c1a94a375bd57">01776</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01777"></a>01777   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::</a>
<a name="l01778"></a>01778 <a class="code" href="class_seldon_1_1_matrix.php">  ReallocateRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01779"></a>01779   {
<a name="l01780"></a>01780     this-&gt;val_(i).Reallocate(j);
<a name="l01781"></a>01781   }
<a name="l01782"></a>01782 
<a name="l01783"></a>01783 
<a name="l01785"></a>01785 
<a name="l01789"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#ad70cf7b6b6aadd9c1e51bc1944550d91">01789</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01790"></a>01790   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::</a>
<a name="l01791"></a>01791 <a class="code" href="class_seldon_1_1_matrix.php">  ResizeRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01792"></a>01792   {
<a name="l01793"></a>01793     this-&gt;val_(i).Resize(j);
<a name="l01794"></a>01794   }
<a name="l01795"></a>01795 
<a name="l01796"></a>01796 
<a name="l01798"></a>01798 
<a name="l01802"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#abb74a0467a0295eb8964ac30c9e4922c">01802</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01803"></a>01803   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::</a>
<a name="l01804"></a>01804 <a class="code" href="class_seldon_1_1_matrix.php">  SwapRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01805"></a>01805   {
<a name="l01806"></a>01806     Swap(this-&gt;val_(i), this-&gt;val_(j));
<a name="l01807"></a>01807   }
<a name="l01808"></a>01808 
<a name="l01809"></a>01809 
<a name="l01811"></a>01811 
<a name="l01815"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#af70fc4f3c8267b2a4f528e1fd3056020">01815</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01816"></a>01816   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::</a>
<a name="l01817"></a>01817 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceIndexRow</a>(<span class="keywordtype">int</span> i,<a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l01818"></a>01818   {
<a name="l01819"></a>01819     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_(i).GetM(); j++)
<a name="l01820"></a>01820       this-&gt;val_(i).Index(j) = new_index(j);
<a name="l01821"></a>01821   }
<a name="l01822"></a>01822 
<a name="l01823"></a>01823 
<a name="l01825"></a>01825 
<a name="l01829"></a>01829   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01830"></a>01830   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::GetRowSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"></span>
<a name="l01831"></a>01831 <span class="keyword">    const</span>
<a name="l01832"></a>01832 <span class="keyword">  </span>{
<a name="l01833"></a>01833     <span class="keywordflow">return</span> this-&gt;val_(i).GetSize();
<a name="l01834"></a>01834   }
<a name="l01835"></a>01835 
<a name="l01836"></a>01836 
<a name="l01838"></a>01838   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01839"></a>01839   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a10ca8f311170cfa8b11f6caa63d42635" title="Displays non-zero values of a column.">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::PrintRow</a>(<span class="keywordtype">int</span> i)<span class="keyword"></span>
<a name="l01840"></a>01840 <span class="keyword">    const</span>
<a name="l01841"></a>01841 <span class="keyword">  </span>{
<a name="l01842"></a>01842     this-&gt;val_(i).Print();
<a name="l01843"></a>01843   }
<a name="l01844"></a>01844 
<a name="l01845"></a>01845 
<a name="l01847"></a>01847 
<a name="l01852"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a94a7c41aa9fb70bc83fbadb66e0e7709">01852</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01853"></a>01853   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;</a>
<a name="l01854"></a>01854 <a class="code" href="class_seldon_1_1_matrix.php">  ::AssembleRow</a>(<span class="keywordtype">int</span> i)
<a name="l01855"></a>01855   {
<a name="l01856"></a>01856     this-&gt;val_(i).Assemble();
<a name="l01857"></a>01857   }
<a name="l01858"></a>01858 
<a name="l01859"></a>01859 
<a name="l01861"></a>01861 
<a name="l01866"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#ac8bf415466c0dd580e635a66ae4110a6">01866</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01867"></a>01867   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::</a>
<a name="l01868"></a>01868 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; val)
<a name="l01869"></a>01869   {
<a name="l01870"></a>01870     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l01871"></a>01871       this-&gt;val_(i).AddInteraction(j, val);
<a name="l01872"></a>01872   }
<a name="l01873"></a>01873 
<a name="l01874"></a>01874 
<a name="l01876"></a>01876 
<a name="l01882"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a3c0b4b2b2f82744e655eb119080c309f">01882</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01883"></a>01883   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::</a>
<a name="l01884"></a>01884 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keywordtype">int</span>* col_, T* value_)
<a name="l01885"></a>01885   {
<a name="l01886"></a>01886     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> col;
<a name="l01887"></a>01887     col.SetData(nb, col_);
<a name="l01888"></a>01888     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val;
<a name="l01889"></a>01889     val.SetData(nb, value_);
<a name="l01890"></a>01890     AddInteractionRow(i, nb, col, val);
<a name="l01891"></a>01891     col.Nullify();
<a name="l01892"></a>01892     val.Nullify();
<a name="l01893"></a>01893   }
<a name="l01894"></a>01894 
<a name="l01895"></a>01895 
<a name="l01897"></a>01897 
<a name="l01903"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#ab4d1a9abf1e07714f5652eab9862475c">01903</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01904"></a>01904   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::</a>
<a name="l01905"></a>01905 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keywordtype">int</span>* row_, T* value_)
<a name="l01906"></a>01906   {
<a name="l01907"></a>01907     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> row;
<a name="l01908"></a>01908     row.SetData(nb, row_);
<a name="l01909"></a>01909     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val;
<a name="l01910"></a>01910     val.SetData(nb, value_);
<a name="l01911"></a>01911     AddInteractionColumn(i, nb, row, val);
<a name="l01912"></a>01912     row.Nullify();
<a name="l01913"></a>01913     val.Nullify();
<a name="l01914"></a>01914   }
<a name="l01915"></a>01915 
<a name="l01916"></a>01916 
<a name="l01918"></a>01918 
<a name="l01924"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#af99cb559be02dcf98137b4dc5c4b29d1">01924</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01925"></a>01925   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::</a>
<a name="l01926"></a>01926 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l01927"></a>01927                     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a>&amp; val)
<a name="l01928"></a>01928   {
<a name="l01929"></a>01929     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> new_col(nb);
<a name="l01930"></a>01930     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc1&gt;</a> new_val(nb);
<a name="l01931"></a>01931     nb = 0;
<a name="l01932"></a>01932     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; new_col.GetM(); j++)
<a name="l01933"></a>01933       <span class="keywordflow">if</span> (i &lt;= col(j))
<a name="l01934"></a>01934         {
<a name="l01935"></a>01935           new_col(nb) = col(j);
<a name="l01936"></a>01936           new_val(nb) = val(j); nb++;
<a name="l01937"></a>01937         }
<a name="l01938"></a>01938 
<a name="l01939"></a>01939     this-&gt;val_(i).AddInteractionRow(nb, new_col, new_val);
<a name="l01940"></a>01940   }
<a name="l01941"></a>01941 
<a name="l01942"></a>01942 
<a name="l01944"></a>01944 
<a name="l01950"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a8f6797869ae81e18c8c81aa00fbcdf82">01950</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01951"></a>01951   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;::</a>
<a name="l01952"></a>01952 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l01953"></a>01953                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T,VectFull,Alloc1&gt;</a>&amp; val)
<a name="l01954"></a>01954   {
<a name="l01955"></a>01955     <span class="comment">// Symmetric matrix, row = column.</span>
<a name="l01956"></a>01956     AddInteractionRow(i, nb, row, val);
<a name="l01957"></a>01957   }
<a name="l01958"></a>01958 
<a name="l01959"></a>01959 
<a name="l01960"></a>01960   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01961"></a>01961   ostream&amp; <a class="code" href="namespace_seldon.php#a5bd4a6d6f3c83048663d57d2fc032107" title="operator&amp;lt;&amp;lt; overloaded for a 3D array.">operator &lt;&lt;</a>(ostream&amp; out,
<a name="l01962"></a>01962                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php" title="Row-major sparse-matrix class.">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;</a>&amp; A)
<a name="l01963"></a>01963   {
<a name="l01964"></a>01964     A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#adf0d3ec9d5ffeb1b3d46f523051d1412" title="Writes the matrix in a file.">WriteText</a>(out);
<a name="l01965"></a>01965 
<a name="l01966"></a>01966     <span class="keywordflow">return</span> out;
<a name="l01967"></a>01967   }
<a name="l01968"></a>01968 
<a name="l01969"></a>01969 
<a name="l01970"></a>01970   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01971"></a>01971   ostream&amp; <a class="code" href="namespace_seldon.php#a5bd4a6d6f3c83048663d57d2fc032107" title="operator&amp;lt;&amp;lt; overloaded for a 3D array.">operator &lt;&lt;</a>(ostream&amp; out,
<a name="l01972"></a>01972                        <span class="keyword">const</span> Matrix&lt;T, Prop, ArrayColSparse, Allocator&gt;&amp; A)
<a name="l01973"></a>01973   {
<a name="l01974"></a>01974     A.WriteText(out);
<a name="l01975"></a>01975 
<a name="l01976"></a>01976     <span class="keywordflow">return</span> out;
<a name="l01977"></a>01977   }
<a name="l01978"></a>01978 
<a name="l01979"></a>01979 
<a name="l01980"></a>01980   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01981"></a>01981   ostream&amp; <a class="code" href="namespace_seldon.php#a5bd4a6d6f3c83048663d57d2fc032107" title="operator&amp;lt;&amp;lt; overloaded for a 3D array.">operator &lt;&lt;</a>(ostream&amp; out,
<a name="l01982"></a>01982                        <span class="keyword">const</span> Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;&amp; A)
<a name="l01983"></a>01983   {
<a name="l01984"></a>01984     A.WriteText(out);
<a name="l01985"></a>01985 
<a name="l01986"></a>01986     <span class="keywordflow">return</span> out;
<a name="l01987"></a>01987   }
<a name="l01988"></a>01988 
<a name="l01989"></a>01989 
<a name="l01990"></a>01990   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01991"></a>01991   ostream&amp; <a class="code" href="namespace_seldon.php#a5bd4a6d6f3c83048663d57d2fc032107" title="operator&amp;lt;&amp;lt; overloaded for a 3D array.">operator &lt;&lt;</a>(ostream&amp; out,
<a name="l01992"></a>01992                        <span class="keyword">const</span> Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;&amp; A)
<a name="l01993"></a>01993   {
<a name="l01994"></a>01994     A.WriteText(out);
<a name="l01995"></a>01995 
<a name="l01996"></a>01996     <span class="keywordflow">return</span> out;
<a name="l01997"></a>01997   }
<a name="l01998"></a>01998 
<a name="l01999"></a>01999 
<a name="l02000"></a>02000 } <span class="comment">// namespace Seldon</span>
<a name="l02001"></a>02001 
<a name="l02002"></a>02002 <span class="preprocessor">#define SELDON_FILE_MATRIX_ARRAY_SPARSE_CXX</span>
<a name="l02003"></a>02003 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
