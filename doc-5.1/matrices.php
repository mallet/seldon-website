<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Matrices </h1>  </div>
</div>
<div class="contents">
<h2>Definition</h2>
<p>Matrices are instances of the class <code>Matrix</code>. Class <code>Matrix</code> is a template class: <code>Matrix&lt;T, Prop, Storage, Allocator&gt;</code>. As for vectors, <code>T</code> is the type of the elements to be stored (e.g. <code>double</code>). </p>
<p><code>Prop</code> indicates that the matrix has given properties (symmetric, hermitian, positive definite or whatever). This template parameter is never used by Seldon; so the user may define its own properties. Thanks to this template parameter, the user may overload some functions based on the properties of the matrix. Seldon defines two properties: <code>General</code> (default) and <code>Symmetric</code>. </p>
<p><code>Storage</code> defines how the matrix is stored. Matrices may be stored in several ways:</p>
<ul>
<li>
<p class="startli"><code>ColMajor</code>: full matrix stored by columns (as in Fortran);</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>RowMajor</code>: full matrix stored by rows (recommended in C++);</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>ColUpTriang</code>: upper triangular matrix stored in a full matrix and by columns;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>ColLoTriang</code>: lower triangular matrix stored in a full matrix and by columns;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>RowUpTriang</code>: upper triangular matrix stored in a full matrix and by rows;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>RowLoTriang</code>: lower triangular matrix stored in a full matrix and by rows;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>ColUpTriangPacked</code>: upper triangular matrix stored in packed form (Blas format) and by columns;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>ColLoTriangPacked</code>: lower triangular matrix stored in packed form (Blas format) and by columns;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>RowUpTriangPacked</code>: upper triangular matrix stored in packed form (Blas format) and by rows;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>RowLoTriangPacked</code>: lower triangular matrix stored in packed form (Blas format) and by rows;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>ColSym</code>: symmetric matrix stored in a full matrix and by columns;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>RowSym</code>: symmetric matrix stored in a full matrix and by rows;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>ColSymPacked</code>: symmetric matrix stored in packed form (Blas format) and by columns;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>RowSymPacked</code>: symmetric matrix stored in packed form (Blas format) and by rows;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>ColHerm</code>: hermitian matrix stored in a full matrix and by columns;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>RowHerm</code>: hermitian matrix stored in a full matrix and by rows;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>ColHermPacked</code>: hermitian matrix stored in packed form (Blas format) and by columns;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>RowHermPacked</code>: hermitian matrix stored in packed form (Blas format) and by rows;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>ColSparse</code>: sparse matrix (Harwell-Boeing form) stored by columns;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>RowSparse</code>: sparse matrix (Harwell-Boeing form) stored by rows;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>ColSymSparse</code>: symmetric sparse matrix (Harwell-Boeing form, storing only the upper part) stored by columns;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>RowSymSparse</code>: symmetric sparse matrix (Harwell-Boeing form, storing only the upper part) stored by rows;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>ColComplexSparse</code>: complex sparse matrix (Harwell-Boeing form) stored by columns and for which the real part and the imaginary part are splitted into two sparse matrices;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>RowComplexSparse</code>: complex sparse matrix (Harwell-Boeing form) stored by rows and for which the real part and the imaginary part are splitted into two sparse matrices;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>ColSymComplexSparse</code>: symmetric sparse matrix (Harwell-Boeing form, storing only the upper part) stored by columns and for which the real part and the imaginary part are splitted into two sparse matrices;</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>RowSymComplexSparse</code>: symmetric sparse matrix (Harwell-Boeing form, storing only the upper part) stored by rows and for which the real part and the imaginary part are splitted into two sparse matrices.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code> ArrayRowSparse</code>: sparse matrix, each row being stored as a sparse vector. </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code> ArrayColSparse</code>: sparse matrix, each column being stored as a sparse vector. </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code> ArrayRowSymSparse</code>: symmetric sparse matrix, each row being stored as a sparse vector. Only upper part of the matrix is stored. </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code> ArrayColSymSparse</code>: symmetric sparse matrix, each column being stored as a sparse vector. Only upper part of the matrix is stored. </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code> ArrayRowComplexSparse</code>: sparse matrix, each row being stored as a sparse vector. Real part and imaginary part are stored separately. </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code> ArrayRowSymComplexSparse</code>: symmetric sparse matrix, each row being stored as a sparse vector. Real part and imaginary part are stored separately. </p>
<p class="endli"></p>
</li>
</ul>
<p><code>RowMajor</code> is the default storage. </p>
<p>Finally, <code>Allocator</code> defines the way memory is managed. It is close to STL allocators. See the section "Allocators" for further details. </p>
<h2>Declaration</h2>
<p>There is a default <code>Allocator</code> (see the section "Allocators"), a default <code>Storage</code> (<code>RowMajor</code>) and a default property <code>Prop</code> (<code>General</code>). It means that the three last template parameters may be omitted. Then a matrix of integers may be declared thanks to the line: </p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">Matrix&lt;int&gt; M;</pre></div>  </pre><p>This defines a matrix of size 0 x 0, that is to say an empty matrix. To define a matrix of size 5 x 3, one may write: </p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">Matrix&lt;int&gt; M(5, 3);</pre></div>  </pre><p>Other declarations may be: </p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">Matrix&lt;int, Symmetric&gt; M(20, 30);
Matrix&lt;int, General, ColMajor&gt; M(20, 20);
Matrix&lt;int, General, RowSparse, NewAlloc&lt;int&gt; &gt; M;</pre></div>  </pre><h2>Use of matrices</h2>
<p>Access to elements is achieved through the <code>operator(int, int)</code>, and indices start at 0: </p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">Matrix&lt;double&gt; M(5, 6);
M(0, 1) = -3.1;
M(0, 0) = 1.2 * M(0, 1);
</pre></div>  </pre><p>To display matrices, there are two convenient options: </p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">M.Print();
cout &lt;&lt; M &lt;&lt; endl;
</pre></div>  </pre><p>There are lots of methods that are described in the <a href="class_matrix.php">documentation</a>. One may point out:</p>
<ul>
<li>
<p class="startli"><code>GetM()</code> and <code>GetN()</code> return respectively the number of rows and the number of columns.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>Fill</code> fills with 1, 2, 3, etc. or fills the matrix with a given value.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>Reallocate</code> resizes the matrix (warning, data may be lost, depending on the allocator).</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>Resize</code> resizes the matrix while keeping previous entries </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>Read</code>, <code>ReadText</code>, <code>Write</code>, <code>WriteText</code> are useful methods for input/ouput operations.</p>
<p class="endli"></p>
</li>
</ul>
<p>Symmetric matrices in packed form are managed as square full matrices. There is a difference. If one affects the element (i, j) in the matrix, the element (j, i) will be affected at the same time, without any cost. A comprehensive test of full matrices is achieved in file <code>test/program/matrices_test.cpp</code>. </p>
<h2>Sparse matrices - Harwell-Boeing form </h2>
<p>Sparse matrices are not managed as full matrices. The access operator <code>operator(int, int)</code> is still available, but it doesn't allow any affectation, because it may return a (non-stored) zero of the matrix. In practice, one should deal with the underlying vectors that define elements and their indices: the vector of values <code>data_</code>, the vector of "start indices" <code>ptr_</code> (i.e. indices - in the vector <code>data_</code> - of the first element of each row or column) and the vector of row or column indices <code>ind_</code>. </p>
<p>Six types of storage are available : RowSparse, ColSparse, RowSymSparse, ColSymSparse, RowComplexSparse, RowSymComplexSparse. To deal with sparse matrices, the following methods should be useful:</p>
<ul>
<li>
<p class="startli"><code>SetData</code> (or the constructor with the same arguments) to set the three vectors <code>data_</code>, <code>ptr_</code> and <code>ind_</code>.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>GetData</code>, <code>GetPtr</code> and <code>GetInd</code> which return respectively <code>data_</code>, <code>ptr_</code> and <code>ind_</code> described above.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>GetDataSize</code>, <code>GetPtrSize</code> and <code>GetIndSize</code> which return respectively sizes of <code>data_</code>, <code>ptr_</code> and <code>ind_</code>.</p>
<p class="endli"></p>
</li>
</ul>
<h2>Sparse matrices - array of sparse vectors </h2>
<p>Since the Harwell-Boeing form is difficult to handle, a more flexible form can be used in Seldon. Four types of storage are available : ArrayRowSparse, ArrayRowSymSparse, ArrayRowComplexSparse, ArrayRowSymComplexSparse. Their equivalents with a storage of columns : ArrayColSparse, ArrayColSymSparse, ArrayColComplexSparse, ArrayColSymComplexSparse are available as well, but often functions are implemented only for storage by rows. Therefore the user is strongly encourage to use only storages by rows. In this form, each row is stored as a sparse vector, allowing fast insertions of entries. Moreover, the access operator has the same functionnality as for dense matrices, because it allows affections.</p>
<p>The following methods should be useful:</p>
<ul>
<li>
<p class="startli"><code>Reallocate</code> initialization of the matrix with a number of rows and columns. Previous entries are cleared. </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>Resize</code> The number of rows and columns are modified, previous entries are kept.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>GetDataSize</code> returns number of stored values.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code>AddInteraction</code> adds/inserts an entry in the matrix </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><code> AddInteractionRow</code> adds/inserts severals entries in a row of the matrix </p>
<p class="endli"></p>
</li>
<li>
<code>ReadText/WriteText</code> reads/writes matrix in ascii.  </li>
</ul>
<p>The methods <code>AddInteraction</code> and <code>AddInteractionRow</code> inserts or adds entries in the right position, so that each row contains sorted elements (column numbers are sorted in ascending order). The function <code> Copy</code> converts matrices in any form, especially Harwell-Boeing form, since this last form induces faster matrix-vector product. </p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
int m = 10, n = 5;
Matrix&lt;double, General, ArrayRowSparse&gt; A(m, n);

// you can use AddInteraction
A.AddInteraction(3, 4, 3.0);
// or directly use the operator ()
// if the entry doesn't exist, it is added
A(2, 3) = 2.0;
// or add several entries at the same time
int nb_elt = 3;
IVect col_number(nb_elt);
DVect values(nb_elt);
col_number(0) = 1; col_number(0) = 3; col_number(0) = 2;
values(0) = 1.0; values(1) = -1.5; values(2) = 3.0;
A.AddInteractionRow(2, col_number, values);

// you can read/write the matrix
A.WriteText("Ah.dat"); A.ReadText("Ah.dat");
// and convert the matrix if you want a faster matrix-vector product
Matrix&lt;double, General, RowSparse&gt; Acsr;
Copy(A, Acsr);
A.Clear();
DVect x(n), b(m);
x.Fill();
// b = A*x
MltAdd(1.0, A, x, 0, b);
</pre></div>  </pre><p>A comprehensive test of sparse matrices is achieved in file <code>test/program/sparse_matrices_test.cpp</code>.  </p>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
