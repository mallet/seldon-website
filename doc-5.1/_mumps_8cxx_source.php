<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/interfaces/direct/Mumps.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_MUMPS_CXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 <span class="preprocessor">#include &quot;Mumps.hxx&quot;</span>
<a name="l00023"></a>00023 
<a name="l00024"></a>00024 <span class="keyword">namespace </span>Seldon
<a name="l00025"></a>00025 {
<a name="l00026"></a>00026 
<a name="l00028"></a>00028   <span class="keyword">template</span>&lt;&gt;
<a name="l00029"></a>00029   <span class="keyword">inline</span> <span class="keywordtype">void</span> MatrixMumps&lt;double&gt;::CallMumps()
<a name="l00030"></a>00030   {
<a name="l00031"></a>00031     dmumps_c(&amp;struct_mumps);
<a name="l00032"></a>00032   }
<a name="l00033"></a>00033 
<a name="l00034"></a>00034 
<a name="l00036"></a>00036   <span class="keyword">template</span>&lt;&gt;
<a name="l00037"></a>00037   <span class="keyword">inline</span> <span class="keywordtype">void</span> MatrixMumps&lt;complex&lt;double&gt; &gt;::CallMumps()
<a name="l00038"></a>00038   {
<a name="l00039"></a>00039     zmumps_c(&amp;struct_mumps);
<a name="l00040"></a>00040   }
<a name="l00041"></a>00041 
<a name="l00042"></a>00042 
<a name="l00044"></a>00044   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00045"></a>00045   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#af1dbd7261f5a31bdc48ba7149b952b32" title="initialization">MatrixMumps&lt;T&gt;::MatrixMumps</a>()
<a name="l00046"></a>00046   {
<a name="l00047"></a>00047     <span class="comment">// struct_mumps.comm_fortran = MPI_Comm_c2f(MPI_COMM_WORLD);</span>
<a name="l00048"></a>00048     struct_mumps.comm_fortran = -987654;
<a name="l00049"></a>00049 
<a name="l00050"></a>00050     <span class="comment">// parameters for mumps</span>
<a name="l00051"></a>00051     struct_mumps.job = -1;
<a name="l00052"></a>00052     struct_mumps.par = 1;
<a name="l00053"></a>00053     struct_mumps.sym = 0; <span class="comment">// 0 -&gt; unsymmetric matrix</span>
<a name="l00054"></a>00054 
<a name="l00055"></a>00055     <span class="comment">// mumps is called</span>
<a name="l00056"></a>00056     CallMumps();
<a name="l00057"></a>00057 
<a name="l00058"></a>00058     <span class="comment">// other parameters</span>
<a name="l00059"></a>00059     struct_mumps.n = 0;
<a name="l00060"></a>00060     type_ordering = 7; <span class="comment">// default : we let Mumps choose the ordering</span>
<a name="l00061"></a>00061     print_level = -1;
<a name="l00062"></a>00062     out_of_core = <span class="keyword">false</span>;
<a name="l00063"></a>00063     new_communicator = <span class="keyword">false</span>;
<a name="l00064"></a>00064   }
<a name="l00065"></a>00065 
<a name="l00066"></a>00066 
<a name="l00068"></a><a class="code" href="class_seldon_1_1_matrix_mumps.php#a90879b937c4794ebc45fa3858c6e97a2">00068</a>   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> MatrixSparse&gt;
<a name="l00069"></a>00069   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#a90879b937c4794ebc45fa3858c6e97a2" title="initialization of the computation">MatrixMumps&lt;T&gt;</a>
<a name="l00070"></a>00070 <a class="code" href="class_seldon_1_1_matrix_mumps.php#a90879b937c4794ebc45fa3858c6e97a2" title="initialization of the computation">  ::InitMatrix</a>(<span class="keyword">const</span> MatrixSparse&amp; A, <span class="keywordtype">bool</span> distributed)
<a name="l00071"></a>00071   {
<a name="l00072"></a>00072     <span class="comment">// we clear previous factorization</span>
<a name="l00073"></a>00073     Clear();
<a name="l00074"></a>00074 
<a name="l00075"></a>00075 <span class="preprocessor">#ifdef SELDON_WITH_MPI</span>
<a name="l00076"></a>00076 <span class="preprocessor"></span>    <span class="comment">// MPI initialization for parallel version</span>
<a name="l00077"></a>00077     <span class="keywordflow">if</span> (distributed)
<a name="l00078"></a>00078       {
<a name="l00079"></a>00079         <span class="comment">// for distributed matrix, every processor is assumed to be involved</span>
<a name="l00080"></a>00080         struct_mumps.comm_fortran = -987654;
<a name="l00081"></a>00081       }
<a name="l00082"></a>00082     <span class="keywordflow">else</span>
<a name="l00083"></a>00083       {
<a name="l00084"></a>00084         <span class="comment">// centralized matrix =&gt; a linear system per processor</span>
<a name="l00085"></a>00085         MPI_Comm_rank(MPI_COMM_WORLD, &amp;rank);
<a name="l00086"></a>00086         MPI_Comm_group(MPI_COMM_WORLD, &amp;single_group);
<a name="l00087"></a>00087         MPI_Group_incl(single_group, 1, &amp;rank, &amp;single_group);
<a name="l00088"></a>00088         MPI_Comm_create(MPI_COMM_WORLD, single_group, &amp;single_comm);
<a name="l00089"></a>00089         struct_mumps.comm_fortran = MPI_Comm_c2f(single_comm);
<a name="l00090"></a>00090         new_communicator = <span class="keyword">true</span>;
<a name="l00091"></a>00091       }
<a name="l00092"></a>00092 <span class="preprocessor">#endif</span>
<a name="l00093"></a>00093 <span class="preprocessor"></span>
<a name="l00094"></a>00094     <span class="comment">// symmetry is specified during the initialization stage</span>
<a name="l00095"></a>00095     struct_mumps.job = -1;
<a name="l00096"></a>00096     <span class="keywordflow">if</span> (<a class="code" href="namespace_seldon.php#aa92697611346050d5157af5e936def3d" title="returns true if the matrix is symmetric">IsSymmetricMatrix</a>(A))
<a name="l00097"></a>00097       struct_mumps.sym = 2; <span class="comment">// general symmetric matrix</span>
<a name="l00098"></a>00098     <span class="keywordflow">else</span>
<a name="l00099"></a>00099       struct_mumps.sym = 0; <span class="comment">// unsymmetric matrix</span>
<a name="l00100"></a>00100 
<a name="l00101"></a>00101     <span class="comment">// mumps is called</span>
<a name="l00102"></a>00102     CallMumps();
<a name="l00103"></a>00103 
<a name="l00104"></a>00104     struct_mumps.icntl[13] = 20;
<a name="l00105"></a>00105     struct_mumps.icntl[6] = type_ordering;
<a name="l00106"></a>00106     <span class="comment">// setting out of core parameters</span>
<a name="l00107"></a>00107     <span class="keywordflow">if</span> (out_of_core)
<a name="l00108"></a>00108       struct_mumps.icntl[21] = 1;
<a name="l00109"></a>00109     <span class="keywordflow">else</span>
<a name="l00110"></a>00110       struct_mumps.icntl[21] = 0;
<a name="l00111"></a>00111 
<a name="l00112"></a>00112     struct_mumps.icntl[17] = 0;
<a name="l00113"></a>00113 
<a name="l00114"></a>00114     <span class="comment">// the print level is set in mumps</span>
<a name="l00115"></a>00115     <span class="keywordflow">if</span> (print_level &gt;= 0)
<a name="l00116"></a>00116       {
<a name="l00117"></a>00117         struct_mumps.icntl[0] = 6;
<a name="l00118"></a>00118         struct_mumps.icntl[1] = 0;
<a name="l00119"></a>00119         struct_mumps.icntl[2] = 6;
<a name="l00120"></a>00120         struct_mumps.icntl[3] = 2;
<a name="l00121"></a>00121       }
<a name="l00122"></a>00122     <span class="keywordflow">else</span>
<a name="l00123"></a>00123       {
<a name="l00124"></a>00124         struct_mumps.icntl[0] = -1;
<a name="l00125"></a>00125         struct_mumps.icntl[1] = -1;
<a name="l00126"></a>00126         struct_mumps.icntl[2] = -1;
<a name="l00127"></a>00127         struct_mumps.icntl[3] = 0;
<a name="l00128"></a>00128       }
<a name="l00129"></a>00129   }
<a name="l00130"></a>00130 
<a name="l00131"></a>00131 
<a name="l00133"></a>00133   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00134"></a>00134   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#a752caecc1023fe5da2a71fc83ae8cbf9" title="selects another ordering scheme">MatrixMumps&lt;T&gt;::SelectOrdering</a>(<span class="keywordtype">int</span> num_ordering)
<a name="l00135"></a>00135   {
<a name="l00136"></a>00136     <a class="code" href="class_seldon_1_1_matrix_mumps.php#ad33cdf6539eea328102bfa3918adef6c" title="ordering scheme (AMD, Metis, etc) //! object containing Mumps data structure">type_ordering</a> = num_ordering;
<a name="l00137"></a>00137   }
<a name="l00138"></a>00138 
<a name="l00139"></a>00139 
<a name="l00141"></a>00141   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00142"></a>00142   <a class="code" href="class_seldon_1_1_matrix_mumps.php#af9f7189c4f700303976d3f2ffd13945a" title="clears factorization">MatrixMumps&lt;T&gt;::~MatrixMumps</a>()
<a name="l00143"></a>00143   {
<a name="l00144"></a>00144     <a class="code" href="class_seldon_1_1_matrix_mumps.php#a8d3c03fd1f5e00dbbbb39e8f9e1cc4ee" title="clears factorization">Clear</a>();
<a name="l00145"></a>00145   }
<a name="l00146"></a>00146 
<a name="l00147"></a>00147 
<a name="l00149"></a>00149   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00150"></a>00150   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#a8d3c03fd1f5e00dbbbb39e8f9e1cc4ee" title="clears factorization">MatrixMumps&lt;T&gt;::Clear</a>()
<a name="l00151"></a>00151   {
<a name="l00152"></a>00152     <span class="keywordflow">if</span> (struct_mumps.n &gt; 0)
<a name="l00153"></a>00153       {
<a name="l00154"></a>00154         struct_mumps.job = -2;
<a name="l00155"></a>00155         CallMumps(); <span class="comment">/* Terminate instance */</span>
<a name="l00156"></a>00156 
<a name="l00157"></a>00157         num_row_glob.Clear(); num_col_glob.Clear();
<a name="l00158"></a>00158         struct_mumps.n = 0;
<a name="l00159"></a>00159 
<a name="l00160"></a>00160       }
<a name="l00161"></a>00161 
<a name="l00162"></a>00162 <span class="preprocessor">#ifdef SELDON_WITH_MPI</span>
<a name="l00163"></a>00163 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (new_communicator)
<a name="l00164"></a>00164       {
<a name="l00165"></a>00165         MPI_Comm_free(&amp;single_comm);
<a name="l00166"></a>00166         new_communicator = <span class="keyword">false</span>;
<a name="l00167"></a>00167       }
<a name="l00168"></a>00168 <span class="preprocessor">#endif</span>
<a name="l00169"></a>00169 <span class="preprocessor"></span>
<a name="l00170"></a>00170   }
<a name="l00171"></a>00171 
<a name="l00172"></a>00172 
<a name="l00174"></a>00174   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00175"></a>00175   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#a693fc679e4e3703162cd7e8e976b30d0" title="no display from Mumps">MatrixMumps&lt;T&gt;::HideMessages</a>()
<a name="l00176"></a>00176   {
<a name="l00177"></a>00177     print_level = -1;
<a name="l00178"></a>00178 
<a name="l00179"></a>00179     struct_mumps.icntl[0] = -1;
<a name="l00180"></a>00180     struct_mumps.icntl[1] = -1;
<a name="l00181"></a>00181     struct_mumps.icntl[2] = -1;
<a name="l00182"></a>00182     struct_mumps.icntl[3] = 0;
<a name="l00183"></a>00183 
<a name="l00184"></a>00184   }
<a name="l00185"></a>00185 
<a name="l00186"></a>00186 
<a name="l00188"></a>00188   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00189"></a>00189   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#a9375b89f5cbe3908db7120d22f033beb" title="standard display">MatrixMumps&lt;T&gt;::ShowMessages</a>()
<a name="l00190"></a>00190   {
<a name="l00191"></a>00191     print_level = 0;
<a name="l00192"></a>00192 
<a name="l00193"></a>00193     struct_mumps.icntl[0] = 6;
<a name="l00194"></a>00194     struct_mumps.icntl[1] = 0;
<a name="l00195"></a>00195     struct_mumps.icntl[2] = 6;
<a name="l00196"></a>00196     struct_mumps.icntl[3] = 2;
<a name="l00197"></a>00197 
<a name="l00198"></a>00198   }
<a name="l00199"></a>00199 
<a name="l00200"></a>00200 
<a name="l00201"></a>00201   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00202"></a>00202   <span class="keyword">inline</span> <span class="keywordtype">void</span> MatrixMumps&lt;T&gt;::EnableOutOfCore()
<a name="l00203"></a>00203   {
<a name="l00204"></a>00204     out_of_core = <span class="keyword">true</span>;
<a name="l00205"></a>00205   }
<a name="l00206"></a>00206 
<a name="l00207"></a>00207 
<a name="l00208"></a>00208   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00209"></a>00209   <span class="keyword">inline</span> <span class="keywordtype">void</span> MatrixMumps&lt;T&gt;::DisableOutOfCore()
<a name="l00210"></a>00210   {
<a name="l00211"></a>00211     out_of_core = <span class="keyword">false</span>;
<a name="l00212"></a>00212   }
<a name="l00213"></a>00213 
<a name="l00214"></a>00214 
<a name="l00216"></a>00216 
<a name="l00221"></a>00221   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop,<span class="keyword">class</span> Storage,<span class="keyword">class</span> Allocator&gt;
<a name="l00222"></a>00222   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#a3b7b913e2e23e1d02890df3c332c6637" title="computes row numbers">MatrixMumps&lt;T&gt;::FindOrdering</a>(Matrix&lt;T, Prop, Storage, Allocator&gt; &amp; mat,
<a name="l00223"></a>00223                                     IVect&amp; numbers, <span class="keywordtype">bool</span> keep_matrix)
<a name="l00224"></a>00224   {
<a name="l00225"></a>00225     <a class="code" href="class_seldon_1_1_matrix_mumps.php#a90879b937c4794ebc45fa3858c6e97a2" title="initialization of the computation">InitMatrix</a>(mat);
<a name="l00226"></a>00226 
<a name="l00227"></a>00227     <span class="keywordtype">int</span> n = mat.GetM(), nnz = mat.GetNonZeros();
<a name="l00228"></a>00228     <span class="comment">// conversion in coordinate format</span>
<a name="l00229"></a>00229     IVect num_row, num_col; Vector&lt;T, VectFull, Allocator&gt; values;
<a name="l00230"></a>00230     <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(mat, num_row, num_col, values, 1);
<a name="l00231"></a>00231     <span class="comment">// no values needed to renumber</span>
<a name="l00232"></a>00232     values.Clear();
<a name="l00233"></a>00233     <span class="keywordflow">if</span> (!keep_matrix)
<a name="l00234"></a>00234       mat.Clear();
<a name="l00235"></a>00235 
<a name="l00236"></a>00236     struct_mumps.n = n; struct_mumps.nz = nnz;
<a name="l00237"></a>00237     struct_mumps.irn = num_row.GetData();
<a name="l00238"></a>00238     struct_mumps.jcn = num_col.GetData();
<a name="l00239"></a>00239 
<a name="l00240"></a>00240     <span class="comment">/* Call the MUMPS package. */</span>
<a name="l00241"></a>00241     struct_mumps.job = 1; <span class="comment">// we analyse the system</span>
<a name="l00242"></a>00242     CallMumps();
<a name="l00243"></a>00243 
<a name="l00244"></a>00244     numbers.Reallocate(n);
<a name="l00245"></a>00245     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; n; i++)
<a name="l00246"></a>00246       numbers(i) = struct_mumps.sym_perm[i]-1;
<a name="l00247"></a>00247   }
<a name="l00248"></a>00248 
<a name="l00249"></a>00249 
<a name="l00251"></a>00251 
<a name="l00255"></a>00255   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00256"></a>00256   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#a712e9edf500c0a63ddded960762707c5" title="factorization of a given matrix">MatrixMumps&lt;T&gt;::FactorizeMatrix</a>(Matrix&lt;T,Prop,Storage,Allocator&gt; &amp; mat,
<a name="l00257"></a>00257                                        <span class="keywordtype">bool</span> keep_matrix)
<a name="l00258"></a>00258   {
<a name="l00259"></a>00259     <a class="code" href="class_seldon_1_1_matrix_mumps.php#a90879b937c4794ebc45fa3858c6e97a2" title="initialization of the computation">InitMatrix</a>(mat);
<a name="l00260"></a>00260 
<a name="l00261"></a>00261     <span class="keywordtype">int</span> n = mat.GetM(), nnz = mat.GetNonZeros();
<a name="l00262"></a>00262     <span class="comment">// conversion in coordinate format with fortran convention (1-index)</span>
<a name="l00263"></a>00263     IVect num_row, num_col; Vector&lt;T, VectFull, Allocator&gt; values;
<a name="l00264"></a>00264     <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(mat, num_row, num_col, values, 1);
<a name="l00265"></a>00265     <span class="keywordflow">if</span> (!keep_matrix)
<a name="l00266"></a>00266       mat.Clear();
<a name="l00267"></a>00267 
<a name="l00268"></a>00268     struct_mumps.n = n; struct_mumps.nz = nnz;
<a name="l00269"></a>00269     struct_mumps.irn = num_row.GetData();
<a name="l00270"></a>00270     struct_mumps.jcn = num_col.GetData();
<a name="l00271"></a>00271     struct_mumps.a = <span class="keyword">reinterpret_cast&lt;</span><a class="code" href="class_seldon_1_1_matrix_mumps.php#a7d695b24c99275e0911cb817757f2737" title="double* or complex&amp;lt;double&amp;gt;*">pointer</a><span class="keyword">&gt;</span>(values.GetData());
<a name="l00272"></a>00272 
<a name="l00273"></a>00273     <span class="comment">/* Call the MUMPS package. */</span>
<a name="l00274"></a>00274     struct_mumps.job = 4; <span class="comment">// we analyse and factorize the system</span>
<a name="l00275"></a>00275     CallMumps();
<a name="l00276"></a>00276   }
<a name="l00277"></a>00277 
<a name="l00278"></a>00278 
<a name="l00280"></a><a class="code" href="class_seldon_1_1_matrix_mumps.php#a9fd9e06d1da9a27e63cd2fb266f628c0">00280</a>   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00281"></a>00281   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#a9fd9e06d1da9a27e63cd2fb266f628c0" title="Symbolic factorization.">MatrixMumps&lt;T&gt;</a>
<a name="l00282"></a>00282 <a class="code" href="class_seldon_1_1_matrix_mumps.php#a9fd9e06d1da9a27e63cd2fb266f628c0" title="Symbolic factorization.">  ::PerformAnalysis</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a> &amp; mat)
<a name="l00283"></a>00283   {
<a name="l00284"></a>00284     InitMatrix(mat);
<a name="l00285"></a>00285 
<a name="l00286"></a>00286     <span class="keywordtype">int</span> n = mat.GetM(), nnz = mat.GetNonZeros();
<a name="l00287"></a>00287     <span class="comment">// conversion in coordinate format with fortran convention (1-index)</span>
<a name="l00288"></a>00288     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> values;
<a name="l00289"></a>00289     <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(mat, num_row_glob, num_col_glob, values, 1);
<a name="l00290"></a>00290 
<a name="l00291"></a>00291     struct_mumps.n = n; struct_mumps.nz = nnz;
<a name="l00292"></a>00292     struct_mumps.irn = num_row_glob.GetData();
<a name="l00293"></a>00293     struct_mumps.jcn = num_col_glob.GetData();
<a name="l00294"></a>00294     struct_mumps.a = <span class="keyword">reinterpret_cast&lt;</span>pointer<span class="keyword">&gt;</span>(values.<a class="code" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a" title="Returns a pointer to data_ (stored data).">GetData</a>());
<a name="l00295"></a>00295 
<a name="l00296"></a>00296     <span class="comment">/* Call the MUMPS package. */</span>
<a name="l00297"></a>00297     struct_mumps.job = 1; <span class="comment">// we analyse the system</span>
<a name="l00298"></a>00298     CallMumps();
<a name="l00299"></a>00299   }
<a name="l00300"></a>00300 
<a name="l00301"></a>00301 
<a name="l00303"></a>00303 
<a name="l00309"></a><a class="code" href="class_seldon_1_1_matrix_mumps.php#ab59b1cd5d9c51196ea91dea968ae2c00">00309</a>   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00310"></a>00310   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#ab59b1cd5d9c51196ea91dea968ae2c00" title="Numerical factorization.">MatrixMumps&lt;T&gt;</a>
<a name="l00311"></a>00311 <a class="code" href="class_seldon_1_1_matrix_mumps.php#ab59b1cd5d9c51196ea91dea968ae2c00" title="Numerical factorization.">  ::PerformFactorization</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a> &amp; mat)
<a name="l00312"></a>00312   {
<a name="l00313"></a>00313     <span class="comment">// we consider that the values are corresponding</span>
<a name="l00314"></a>00314     <span class="comment">// to the row/column numbers given for the analysis</span>
<a name="l00315"></a>00315     struct_mumps.a = <span class="keyword">reinterpret_cast&lt;</span>pointer<span class="keyword">&gt;</span>(mat.GetData());
<a name="l00316"></a>00316 
<a name="l00317"></a>00317     <span class="comment">/* Call the MUMPS package. */</span>
<a name="l00318"></a>00318     struct_mumps.job = 2; <span class="comment">// we factorize the system</span>
<a name="l00319"></a>00319     CallMumps();
<a name="l00320"></a>00320   }
<a name="l00321"></a>00321 
<a name="l00322"></a>00322 
<a name="l00324"></a>00324   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00325"></a>00325   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#aab6219d8e5473f80dae2957a3d003ab0" title="returns information about factorization performed">MatrixMumps&lt;T&gt;::GetInfoFactorization</a>()<span class="keyword"> const</span>
<a name="l00326"></a>00326 <span class="keyword">  </span>{
<a name="l00327"></a>00327     <span class="keywordflow">return</span> struct_mumps.info[0];
<a name="l00328"></a>00328   }
<a name="l00329"></a>00329 
<a name="l00330"></a>00330 
<a name="l00332"></a>00332 
<a name="l00338"></a>00338   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator,
<a name="l00339"></a><a class="code" href="class_seldon_1_1_matrix_mumps.php#adb5993784b010142a470da631025ce0a">00339</a>                              <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00340"></a>00340   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php" title="object used to solve linear system by calling mumps subroutines">MatrixMumps&lt;T&gt;::</a>
<a name="l00341"></a>00341 <a class="code" href="class_seldon_1_1_matrix_mumps.php" title="object used to solve linear system by calling mumps subroutines">  GetSchurMatrix</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop1, Storage1, Allocator&gt;</a>&amp; mat, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; num,
<a name="l00342"></a>00342                  <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop2, Storage2, Allocator2&gt;</a> &amp; mat_schur,
<a name="l00343"></a>00343                  <span class="keywordtype">bool</span> keep_matrix)
<a name="l00344"></a>00344   {
<a name="l00345"></a>00345     <a class="code" href="class_seldon_1_1_matrix_mumps.php#a90879b937c4794ebc45fa3858c6e97a2" title="initialization of the computation">InitMatrix</a>(mat);
<a name="l00346"></a>00346 
<a name="l00347"></a>00347     <span class="keywordtype">int</span> n_schur = num.GetM(), n = mat.GetM();
<a name="l00348"></a>00348     <span class="comment">// Subscripts are changed to respect fortran convention</span>
<a name="l00349"></a>00349     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> index_schur(n_schur);
<a name="l00350"></a>00350     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; n_schur; i++)
<a name="l00351"></a>00351       index_schur(i) = num(i)+1;
<a name="l00352"></a>00352 
<a name="l00353"></a>00353     <span class="comment">// array that will contain values of Schur matrix</span>
<a name="l00354"></a>00354     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator2&gt;</a> vec_schur(n_schur*n_schur);
<a name="l00355"></a>00355 
<a name="l00356"></a>00356     struct_mumps.icntl[18] = n_schur;
<a name="l00357"></a>00357     struct_mumps.size_schur = n_schur;
<a name="l00358"></a>00358     struct_mumps.listvar_schur = index_schur.GetData();
<a name="l00359"></a>00359     struct_mumps.schur = <span class="keyword">reinterpret_cast&lt;</span><a class="code" href="class_seldon_1_1_matrix_mumps.php#a7d695b24c99275e0911cb817757f2737" title="double* or complex&amp;lt;double&amp;gt;*">pointer</a><span class="keyword">&gt;</span>(vec_schur.GetData());
<a name="l00360"></a>00360 
<a name="l00361"></a>00361     <span class="comment">// factorization of the matrix</span>
<a name="l00362"></a>00362     <a class="code" href="class_seldon_1_1_matrix_mumps.php#a712e9edf500c0a63ddded960762707c5" title="factorization of a given matrix">FactorizeMatrix</a>(mat, keep_matrix);
<a name="l00363"></a>00363 
<a name="l00364"></a>00364     <span class="comment">// resetting parameters related to Schur complement</span>
<a name="l00365"></a>00365     struct_mumps.icntl[18] = 0;
<a name="l00366"></a>00366     struct_mumps.size_schur = 0;
<a name="l00367"></a>00367     struct_mumps.listvar_schur = NULL;
<a name="l00368"></a>00368     struct_mumps.schur = NULL;
<a name="l00369"></a>00369 
<a name="l00370"></a>00370     <span class="comment">// schur complement stored by rows</span>
<a name="l00371"></a>00371     <span class="keywordtype">int</span> nb = 0;
<a name="l00372"></a>00372     mat_schur.Reallocate(n,n);
<a name="l00373"></a>00373     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; n; i++)
<a name="l00374"></a>00374       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n ;j++)
<a name="l00375"></a>00375         mat_schur(i,j) = vec_schur(nb++);
<a name="l00376"></a>00376 
<a name="l00377"></a>00377     vec_schur.Clear(); index_schur.Clear();
<a name="l00378"></a>00378   }
<a name="l00379"></a>00379 
<a name="l00380"></a>00380 
<a name="l00382"></a>00382 
<a name="l00386"></a>00386   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Transpose_status&gt;
<a name="l00387"></a>00387   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php" title="object used to solve linear system by calling mumps subroutines">MatrixMumps&lt;T&gt;::Solve</a>(<span class="keyword">const</span> Transpose_status&amp; TransA,
<a name="l00388"></a>00388                              <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator2&gt;</a>&amp; x)
<a name="l00389"></a>00389   {
<a name="l00390"></a>00390 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00391"></a>00391 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (x.GetM() != struct_mumps.n)
<a name="l00392"></a>00392       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;Mumps::Solve(TransA, c)&quot;</span>,
<a name="l00393"></a>00393                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The length of x is equal to &quot;</span>)
<a name="l00394"></a>00394                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(x.GetM())
<a name="l00395"></a>00395                      + <span class="stringliteral">&quot; while the size of the matrix is equal to &quot;</span>
<a name="l00396"></a>00396                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(struct_mumps.n) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00397"></a>00397 <span class="preprocessor">#endif</span>
<a name="l00398"></a>00398 <span class="preprocessor"></span>
<a name="l00399"></a>00399     <span class="keywordflow">if</span> (TransA.Trans())
<a name="l00400"></a>00400       struct_mumps.icntl[8] = 0;
<a name="l00401"></a>00401     <span class="keywordflow">else</span>
<a name="l00402"></a>00402       struct_mumps.icntl[8] = 1;
<a name="l00403"></a>00403 
<a name="l00404"></a>00404     struct_mumps.rhs = reinterpret_cast&lt;pointer&gt;(x.GetData());
<a name="l00405"></a>00405     struct_mumps.job = 3; <span class="comment">// we solve system</span>
<a name="l00406"></a>00406     CallMumps();
<a name="l00407"></a>00407   }
<a name="l00408"></a>00408 
<a name="l00409"></a>00409 
<a name="l00410"></a>00410 
<a name="l00411"></a>00411   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2&gt;
<a name="l00412"></a>00412   <span class="keywordtype">void</span> MatrixMumps&lt;T&gt;::Solve(Vector&lt;T, VectFull, Allocator2&gt;&amp; x)
<a name="l00413"></a>00413   {
<a name="l00414"></a>00414     Solve(SeldonNoTrans, x);
<a name="l00415"></a>00415   }
<a name="l00416"></a>00416 
<a name="l00417"></a>00417 
<a name="l00419"></a>00419 
<a name="l00423"></a><a class="code" href="class_seldon_1_1_matrix_mumps.php#a5863d614bd5110fda7cc3d1dcc7c15f4">00423</a>   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00424"></a>00424   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Transpose_status, <span class="keyword">class</span> Prop&gt;
<a name="l00425"></a>00425   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php" title="object used to solve linear system by calling mumps subroutines">MatrixMumps&lt;T&gt;::Solve</a>(<span class="keyword">const</span> Transpose_status&amp; TransA,
<a name="l00426"></a>00426                              <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColMajor, Allocator2&gt;</a>&amp; x)
<a name="l00427"></a>00427   {
<a name="l00428"></a>00428 
<a name="l00429"></a>00429 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00430"></a>00430 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (x.GetM() != struct_mumps.n)
<a name="l00431"></a>00431       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;Mumps::Solve&quot;</span>, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Row size of x is equal to &quot;</span>)
<a name="l00432"></a>00432                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(x.GetM()) + <span class="stringliteral">&quot; while size of matrix is equal to &quot;</span>
<a name="l00433"></a>00433                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(struct_mumps.n));
<a name="l00434"></a>00434 <span class="preprocessor">#endif</span>
<a name="l00435"></a>00435 <span class="preprocessor"></span>
<a name="l00436"></a>00436     <span class="keywordflow">if</span> (TransA.Trans())
<a name="l00437"></a>00437       struct_mumps.icntl[8] = 0;
<a name="l00438"></a>00438     <span class="keywordflow">else</span>
<a name="l00439"></a>00439       struct_mumps.icntl[8] = 1;
<a name="l00440"></a>00440 
<a name="l00441"></a>00441     struct_mumps.nrhs = x.GetN();
<a name="l00442"></a>00442     struct_mumps.lrhs = x.GetM();
<a name="l00443"></a>00443     struct_mumps.rhs = <span class="keyword">reinterpret_cast&lt;</span><a class="code" href="class_seldon_1_1_matrix_mumps.php#a7d695b24c99275e0911cb817757f2737" title="double* or complex&amp;lt;double&amp;gt;*">pointer</a><span class="keyword">&gt;</span>(x.GetData());
<a name="l00444"></a>00444     struct_mumps.job = 3; <span class="comment">// we solve system</span>
<a name="l00445"></a>00445     CallMumps();
<a name="l00446"></a>00446   }
<a name="l00447"></a>00447 
<a name="l00448"></a>00448 
<a name="l00449"></a>00449 <span class="preprocessor">#ifdef SELDON_WITH_MPI</span>
<a name="l00450"></a>00450 <span class="preprocessor"></span>
<a name="l00451"></a>00451 
<a name="l00457"></a>00457   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00458"></a>00458   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php" title="object used to solve linear system by calling mumps subroutines">MatrixMumps&lt;T&gt;::</a>
<a name="l00459"></a>00459 <a class="code" href="class_seldon_1_1_matrix_mumps.php" title="object used to solve linear system by calling mumps subroutines">  FactorizeDistributedMatrix</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, General, ColSparse, Allocator&gt;</a> &amp; mat,
<a name="l00460"></a>00460                              <span class="keyword">const</span> Prop&amp; sym, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; glob_number,
<a name="l00461"></a>00461                              <span class="keywordtype">bool</span> keep_matrix)
<a name="l00462"></a>00462   {
<a name="l00463"></a>00463     <span class="comment">// initialization depending on symmetric of the matrix</span>
<a name="l00464"></a>00464     <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sparse_00_01_allocator_01_4.php" title="Row-major sparse-matrix class.">Matrix&lt;T, Prop, RowSparse, Allocator&gt;</a> Atest;
<a name="l00465"></a>00465     <a class="code" href="class_seldon_1_1_matrix_mumps.php#a90879b937c4794ebc45fa3858c6e97a2" title="initialization of the computation">InitMatrix</a>(Atest, <span class="keyword">true</span>);
<a name="l00466"></a>00466 
<a name="l00467"></a>00467     <span class="comment">// distributed matrix</span>
<a name="l00468"></a>00468     struct_mumps.icntl[17] = 3;
<a name="l00469"></a>00469 
<a name="l00470"></a>00470     <span class="comment">// global number of rows : mat.GetM()</span>
<a name="l00471"></a>00471     <span class="keywordtype">int</span> N = mat.GetM();
<a name="l00472"></a>00472     <span class="keywordtype">int</span> nnz = mat.GetNonZeros();
<a name="l00473"></a>00473     <span class="comment">// conversion in coordinate format with C-convention (0-index)</span>
<a name="l00474"></a>00474     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> num_row, num_col; <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> values;
<a name="l00475"></a>00475     <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(mat, num_row,
<a name="l00476"></a>00476                                  num_col, values, 0);
<a name="l00477"></a>00477 
<a name="l00478"></a>00478     <span class="comment">// we replace num_col with global numbers</span>
<a name="l00479"></a>00479     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; num_row.GetM(); i++)
<a name="l00480"></a>00480       {
<a name="l00481"></a>00481         num_row(i)++;
<a name="l00482"></a>00482         num_col(i) = glob_number(num_col(i)) + 1;
<a name="l00483"></a>00483       }
<a name="l00484"></a>00484 
<a name="l00485"></a>00485     <span class="keywordflow">if</span> (!keep_matrix)
<a name="l00486"></a>00486       mat.Clear();
<a name="l00487"></a>00487 
<a name="l00488"></a>00488     <span class="comment">/* Define the problem on the host */</span>
<a name="l00489"></a>00489     struct_mumps.n = N; struct_mumps.nz_loc = nnz;
<a name="l00490"></a>00490     struct_mumps.irn_loc = num_row.GetData();
<a name="l00491"></a>00491     struct_mumps.jcn_loc = num_col.GetData();
<a name="l00492"></a>00492     struct_mumps.a_loc = <span class="keyword">reinterpret_cast&lt;</span><a class="code" href="class_seldon_1_1_matrix_mumps.php#a7d695b24c99275e0911cb817757f2737" title="double* or complex&amp;lt;double&amp;gt;*">pointer</a><span class="keyword">&gt;</span>(values.<a class="code" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a" title="Returns a pointer to data_ (stored data).">GetData</a>());
<a name="l00493"></a>00493 
<a name="l00494"></a>00494     <span class="comment">/* Call the MUMPS package. */</span>
<a name="l00495"></a>00495     struct_mumps.job = 4; <span class="comment">// we analyse and factorize the system</span>
<a name="l00496"></a>00496     CallMumps();
<a name="l00497"></a>00497     cout&lt;&lt;<span class="stringliteral">&quot;Factorization completed&quot;</span>&lt;&lt;endl;
<a name="l00498"></a>00498   }
<a name="l00499"></a>00499 
<a name="l00500"></a>00500 
<a name="l00502"></a>00502   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Alloc1, <span class="keyword">class</span> Alloc2, <span class="keyword">class</span> Alloc3, <span class="keyword">class</span> T<span class="keywordtype">int</span>&gt;
<a name="l00503"></a>00503   <span class="keywordtype">void</span> FactorizeDistributedMatrix(Vector&lt;int, VectFull, Alloc1&gt;&amp;,
<a name="l00504"></a>00504                                   Vector&lt;int, VectFull, Alloc2&gt;&amp;,
<a name="l00505"></a>00505                                   Vector&lt;T, VectFull, Alloc3&gt;&amp;,
<a name="l00506"></a>00506                                   <span class="keyword">const</span> Vector&lt;Tint&gt;&amp; glob_number,
<a name="l00507"></a>00507                                   <span class="keywordtype">bool</span> sym, <span class="keywordtype">bool</span> keep_matrix)
<a name="l00508"></a>00508   {
<a name="l00509"></a>00509     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;FactorizeDistributedMatrix(Vector&lt;int&gt;&amp;, Vector&lt;int&gt;&amp;, &quot;</span>
<a name="l00510"></a>00510                     <span class="stringliteral">&quot;Vector&lt;T&gt;&amp;, Vector&lt;Tint&gt;, bool, bool)&quot;</span>);
<a name="l00511"></a>00511   }
<a name="l00512"></a>00512 
<a name="l00513"></a>00513 
<a name="l00515"></a>00515 
<a name="l00520"></a>00520   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Transpose_status&gt;
<a name="l00521"></a>00521   <span class="keywordtype">void</span> MatrixMumps&lt;T&gt;::SolveDistributed(<span class="keyword">const</span> Transpose_status&amp; TransA,
<a name="l00522"></a>00522                                         Vector&lt;T, VectFull, Allocator2&gt;&amp; x,
<a name="l00523"></a>00523                                         <span class="keyword">const</span> IVect&amp; glob_num)
<a name="l00524"></a>00524   {
<a name="l00525"></a>00525     Vector&lt;T, VectFull, Allocator2&gt; rhs;
<a name="l00526"></a>00526     <span class="keywordtype">int</span> cplx = <span class="keyword">sizeof</span>(T)/8;
<a name="l00527"></a>00527     <span class="comment">// allocating the global right hand side</span>
<a name="l00528"></a>00528     rhs.Reallocate(struct_mumps.n); rhs.Zero();
<a name="l00529"></a>00529 
<a name="l00530"></a>00530     <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_matrix_mumps.php#a06ba655df528009ca61f4dd767850f68" title="rank of processor">rank</a> == 0)
<a name="l00531"></a>00531       {
<a name="l00532"></a>00532         <span class="comment">// on the host, we retrieve datas of all the other processors</span>
<a name="l00533"></a>00533         <span class="keywordtype">int</span> nb_procs; MPI_Status status;
<a name="l00534"></a>00534         MPI_Comm_size(MPI_COMM_WORLD, &amp;nb_procs);
<a name="l00535"></a>00535         <span class="keywordflow">if</span> (nb_procs &gt; 1)
<a name="l00536"></a>00536           {
<a name="l00537"></a>00537             <span class="comment">// assembling the right hand side</span>
<a name="l00538"></a>00538             Vector&lt;T, VectFull, Allocator2&gt; xp;
<a name="l00539"></a>00539             IVect nump;
<a name="l00540"></a>00540             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; nb_procs; i++)
<a name="l00541"></a>00541               {
<a name="l00542"></a>00542 
<a name="l00543"></a>00543                 <span class="keywordflow">if</span> (i != 0)
<a name="l00544"></a>00544                   {
<a name="l00545"></a>00545                     <span class="keywordtype">int</span> nb_dof;
<a name="l00546"></a>00546                     MPI_Recv(&amp;nb_dof, 1, MPI_INT, i, 34,
<a name="l00547"></a>00547                              MPI_COMM_WORLD, &amp;status);
<a name="l00548"></a>00548                     xp.Reallocate(nb_dof);
<a name="l00549"></a>00549                     nump.Reallocate(nb_dof);
<a name="l00550"></a>00550                     MPI_Recv(xp.GetDataVoid(), cplx*nb_dof,
<a name="l00551"></a>00551                              MPI_DOUBLE, i, 35, MPI_COMM_WORLD, &amp;status);
<a name="l00552"></a>00552                     MPI_Recv(nump.GetData(), nb_dof, MPI_INT, i, 36,
<a name="l00553"></a>00553                              MPI_COMM_WORLD, &amp;status);
<a name="l00554"></a>00554                   }
<a name="l00555"></a>00555                 <span class="keywordflow">else</span>
<a name="l00556"></a>00556                   {
<a name="l00557"></a>00557                     xp = x; nump = glob_num;
<a name="l00558"></a>00558                   }
<a name="l00559"></a>00559 
<a name="l00560"></a>00560                 <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nump.GetM(); j++)
<a name="l00561"></a>00561                   rhs(nump(j)) = xp(j);
<a name="l00562"></a>00562               }
<a name="l00563"></a>00563           }
<a name="l00564"></a>00564         <span class="keywordflow">else</span>
<a name="l00565"></a>00565           Copy(x, rhs);
<a name="l00566"></a>00566 
<a name="l00567"></a>00567         struct_mumps.rhs = <span class="keyword">reinterpret_cast&lt;</span><a class="code" href="class_seldon_1_1_matrix_mumps.php#a7d695b24c99275e0911cb817757f2737" title="double* or complex&amp;lt;double&amp;gt;*">pointer</a><span class="keyword">&gt;</span>(rhs.GetData());
<a name="l00568"></a>00568       }
<a name="l00569"></a>00569     <span class="keywordflow">else</span>
<a name="l00570"></a>00570       {
<a name="l00571"></a>00571         <span class="comment">// on other processors, we send solution</span>
<a name="l00572"></a>00572         <span class="keywordtype">int</span> nb = x.GetM();
<a name="l00573"></a>00573         MPI_Send(&amp;nb, 1, MPI_INT, 0, 34, MPI_COMM_WORLD);
<a name="l00574"></a>00574         MPI_Send(x.GetDataVoid(), cplx*nb, MPI_DOUBLE, 0, 35, MPI_COMM_WORLD);
<a name="l00575"></a>00575         MPI_Send(glob_num.GetData(), nb, MPI_INT, 0, 36, MPI_COMM_WORLD);
<a name="l00576"></a>00576       }
<a name="l00577"></a>00577 
<a name="l00578"></a>00578     <span class="comment">// we solve system</span>
<a name="l00579"></a>00579     <span class="keywordflow">if</span> (TransA.Trans())
<a name="l00580"></a>00580       struct_mumps.icntl[8] = 0;
<a name="l00581"></a>00581     <span class="keywordflow">else</span>
<a name="l00582"></a>00582       struct_mumps.icntl[8] = 1;
<a name="l00583"></a>00583 
<a name="l00584"></a>00584     struct_mumps.job = 3;
<a name="l00585"></a>00585     CallMumps();
<a name="l00586"></a>00586 
<a name="l00587"></a>00587     <span class="comment">// we distribute solution on all the processors</span>
<a name="l00588"></a>00588     MPI_Bcast(rhs.GetDataVoid(), cplx*rhs.GetM(),
<a name="l00589"></a>00589               MPI_DOUBLE, 0, MPI_COMM_WORLD);
<a name="l00590"></a>00590 
<a name="l00591"></a>00591     <span class="comment">// and we extract the solution on provided numbers</span>
<a name="l00592"></a>00592     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; x.GetM(); i++)
<a name="l00593"></a>00593       x(i) = rhs(glob_num(i));
<a name="l00594"></a>00594   }
<a name="l00595"></a>00595 
<a name="l00596"></a>00596 
<a name="l00597"></a>00597   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2&gt;
<a name="l00598"></a>00598   <span class="keywordtype">void</span> MatrixMumps&lt;T&gt;::SolveDistributed(Vector&lt;T, VectFull, Allocator2&gt;&amp; x,
<a name="l00599"></a>00599                                         <span class="keyword">const</span> IVect&amp; glob_num)
<a name="l00600"></a>00600   {
<a name="l00601"></a>00601     SolveDistributed(SeldonNoTrans, x, glob_num);
<a name="l00602"></a>00602   }
<a name="l00603"></a>00603 <span class="preprocessor">#endif</span>
<a name="l00604"></a>00604 <span class="preprocessor"></span>
<a name="l00605"></a>00605 
<a name="l00606"></a>00606   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00607"></a>00607   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a76d3f187a877c6c58245b0716e1adc00" title="Returns the LU factorization of a matrix.">GetLU</a>(Matrix&lt;T,Symmetric,Storage,Allocator&gt;&amp; A, MatrixMumps&lt;T&gt;&amp; mat_lu,
<a name="l00608"></a>00608              <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>)
<a name="l00609"></a>00609   {
<a name="l00610"></a>00610     mat_lu.FactorizeMatrix(A, keep_matrix);
<a name="l00611"></a>00611   }
<a name="l00612"></a>00612 
<a name="l00613"></a>00613 
<a name="l00614"></a>00614   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00615"></a>00615   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a76d3f187a877c6c58245b0716e1adc00" title="Returns the LU factorization of a matrix.">GetLU</a>(Matrix&lt;T,General,Storage,Allocator&gt;&amp; A, MatrixMumps&lt;T&gt;&amp; mat_lu,
<a name="l00616"></a>00616              <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>)
<a name="l00617"></a>00617   {
<a name="l00618"></a>00618     mat_lu.FactorizeMatrix(A, keep_matrix);
<a name="l00619"></a>00619   }
<a name="l00620"></a>00620 
<a name="l00621"></a>00621 
<a name="l00622"></a>00622   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator, <span class="keyword">class</span> MatrixFull&gt;
<a name="l00623"></a>00623   <span class="keywordtype">void</span> GetSchurMatrix(Matrix&lt;T, Symmetric, Storage, Allocator&gt;&amp; A,
<a name="l00624"></a>00624                       MatrixMumps&lt;T&gt;&amp; mat_lu, <span class="keyword">const</span> IVect&amp; num,
<a name="l00625"></a>00625                       MatrixFull&amp; schur_matrix, <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>)
<a name="l00626"></a>00626   {
<a name="l00627"></a>00627     mat_lu.GetSchurMatrix(A, num, schur_matrix, keep_matrix);
<a name="l00628"></a>00628   }
<a name="l00629"></a>00629 
<a name="l00630"></a>00630 
<a name="l00631"></a>00631   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator, <span class="keyword">class</span> MatrixFull&gt;
<a name="l00632"></a>00632   <span class="keywordtype">void</span> GetSchurMatrix(Matrix&lt;T, General, Storage, Allocator&gt;&amp; A,
<a name="l00633"></a>00633                       MatrixMumps&lt;T&gt;&amp; mat_lu, <span class="keyword">const</span> IVect&amp; num,
<a name="l00634"></a>00634                       MatrixFull&amp; schur_matrix, <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>)
<a name="l00635"></a>00635   {
<a name="l00636"></a>00636     mat_lu.GetSchurMatrix(A, num, schur_matrix, keep_matrix);
<a name="l00637"></a>00637   }
<a name="l00638"></a>00638 
<a name="l00639"></a>00639 
<a name="l00640"></a>00640   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00641"></a>00641   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(MatrixMumps&lt;T&gt;&amp; mat_lu, Vector&lt;T, VectFull, Allocator&gt;&amp; x)
<a name="l00642"></a>00642   {
<a name="l00643"></a>00643     mat_lu.Solve(x);
<a name="l00644"></a>00644   }
<a name="l00645"></a>00645 
<a name="l00646"></a>00646 
<a name="l00647"></a>00647   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator, <span class="keyword">class</span> Transpose_status&gt;
<a name="l00648"></a>00648   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(<span class="keyword">const</span> Transpose_status&amp; TransA,
<a name="l00649"></a>00649                MatrixMumps&lt;T&gt;&amp; mat_lu, Vector&lt;T, VectFull, Allocator&gt;&amp; x)
<a name="l00650"></a>00650   {
<a name="l00651"></a>00651     mat_lu.Solve(TransA, x);
<a name="l00652"></a>00652   }
<a name="l00653"></a>00653 
<a name="l00654"></a>00654 
<a name="l00655"></a>00655   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00656"></a>00656   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(MatrixMumps&lt;T&gt;&amp; mat_lu,
<a name="l00657"></a>00657                Matrix&lt;T, Prop, ColMajor, Allocator&gt;&amp; x)
<a name="l00658"></a>00658   {
<a name="l00659"></a>00659     mat_lu.Solve(SeldonNoTrans, x);
<a name="l00660"></a>00660   }
<a name="l00661"></a>00661 
<a name="l00662"></a>00662 
<a name="l00663"></a>00663   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator, <span class="keyword">class</span> Transpose_status&gt;
<a name="l00664"></a>00664   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(<span class="keyword">const</span> Transpose_status&amp; TransA,
<a name="l00665"></a>00665                MatrixMumps&lt;T&gt;&amp; mat_lu, Matrix&lt;T, ColMajor, Allocator&gt;&amp; x)
<a name="l00666"></a>00666   {
<a name="l00667"></a>00667     mat_lu.Solve(TransA, x);
<a name="l00668"></a>00668   }
<a name="l00669"></a>00669 
<a name="l00670"></a>00670 }
<a name="l00671"></a>00671 
<a name="l00672"></a>00672 <span class="preprocessor">#define SELDON_FILE_MUMPS_CXX</span>
<a name="l00673"></a>00673 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
