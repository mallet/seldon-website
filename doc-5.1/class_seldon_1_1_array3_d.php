<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_array3_d.php">Array3D</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a> &#124;
<a href="#pro-static-attribs">Static Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Array3D&lt; T, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Array3D" -->
<p>3D array.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_array3_d_8hxx_source.php">Array3D.hxx</a>&gt;</code></p>

<p><a href="class_seldon_1_1_array3_d-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8588f965c5a0b0dce36fa98f25a6d72c"></a><!-- doxytag: member="Seldon::Array3D::value_type" ref="a8588f965c5a0b0dce36fa98f25a6d72c" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="abfe8a2d5e959b7578fd6b9416c8d78cf"></a><!-- doxytag: member="Seldon::Array3D::pointer" ref="abfe8a2d5e959b7578fd6b9416c8d78cf" args="" -->
typedef Allocator::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a91b0e2267da2615a6e1fc19e0f26757d"></a><!-- doxytag: member="Seldon::Array3D::const_pointer" ref="a91b0e2267da2615a6e1fc19e0f26757d" args="" -->
typedef Allocator::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0483123eda54a60f11a9540d05695a9a"></a><!-- doxytag: member="Seldon::Array3D::reference" ref="a0483123eda54a60f11a9540d05695a9a" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2853465abcffe9faa39f616263746058"></a><!-- doxytag: member="Seldon::Array3D::const_reference" ref="a2853465abcffe9faa39f616263746058" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#af53eaf5dca82c2d4ae7762ea532789ef">Array3D</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#af53eaf5dca82c2d4ae7762ea532789ef"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#a503b4ac2ccd0b34ecb12946bd5b6f9ce">Array3D</a> (int i, int j, int k)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Main constructor.  <a href="#a503b4ac2ccd0b34ecb12946bd5b6f9ce"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa3e0b21aaa3c72280962842605687005"></a><!-- doxytag: member="Seldon::Array3D::Array3D" ref="aa3e0b21aaa3c72280962842605687005" args="(const Array3D&lt; T, Allocator &gt; &amp;A)" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#aa3e0b21aaa3c72280962842605687005">Array3D</a> (const <a class="el" href="class_seldon_1_1_array3_d.php">Array3D</a>&lt; T, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copy constructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0acf50b4c510976bdde062f03787f06b"></a><!-- doxytag: member="Seldon::Array3D::~Array3D" ref="a0acf50b4c510976bdde062f03787f06b" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#a0acf50b4c510976bdde062f03787f06b">~Array3D</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Destructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#ab792b6b72a04b557792b6d5ad3e86afc">GetLength1</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the length in dimension #1.  <a href="#ab792b6b72a04b557792b6d5ad3e86afc"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#af8acaef0c21a50f4ab6128710d534f8b">GetLength2</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the length in dimension #2.  <a href="#af8acaef0c21a50f4ab6128710d534f8b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#afae5f3ffbbcb66b09ec27d1cfc81759a">GetLength3</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the length in dimension #3.  <a href="#afae5f3ffbbcb66b09ec27d1cfc81759a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#a99dc59f2c62979e78c43d8ec31c02e68">GetSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements in the 3D array.  <a href="#a99dc59f2c62979e78c43d8ec31c02e68"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#a64e6b9afc92a4c7fe4a99f5017959cb6">GetDataSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored in memory.  <a href="#a64e6b9afc92a4c7fe4a99f5017959cb6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#a41def646fbd18d0a86b2fced6e80faf1">GetData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer to the data array.  <a href="#a41def646fbd18d0a86b2fced6e80faf1"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#ae37a5c9ebc684062db0c1be3604641e8">Reallocate</a> (int i, int j, int k)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates memory to resize the 3D array.  <a href="#ae37a5c9ebc684062db0c1be3604641e8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#aa9bf1047ace17bd5c2bf2d4863d46838">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the array.  <a href="#aa9bf1047ace17bd5c2bf2d4863d46838"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#aa29212ec08a08bbda48cf91cce043dd2">operator()</a> (int i, int j, int k)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#aa29212ec08a08bbda48cf91cce043dd2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#ab596d570ea5b72f77f189b8ebbd7796e">operator()</a> (int i, int j, int k) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#ab596d570ea5b72f77f189b8ebbd7796e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_array3_d.php">Array3D</a>&lt; T, Allocator &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#a0cb6b0016cddb1a1e4c25c8e538e69be">operator=</a> (const <a class="el" href="class_seldon_1_1_array3_d.php">Array3D</a>&lt; T, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a 3D array (assignment operator).  <a href="#a0cb6b0016cddb1a1e4c25c8e538e69be"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#a2f8287363e777b0072017405e182ccc0">Copy</a> (const <a class="el" href="class_seldon_1_1_array3_d.php">Array3D</a>&lt; T, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a 3D array.  <a href="#a2f8287363e777b0072017405e182ccc0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#ab965837a5d4a255c217bb33e08b45ead">Zero</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets all elements to zero.  <a href="#ab965837a5d4a255c217bb33e08b45ead"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#a3a5ea3c1352767c1ef1f88219660c045">Fill</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the array.  <a href="#a3a5ea3c1352767c1ef1f88219660c045"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#a33ec62315a11c44f98aed69cc7808bb3">Fill</a> (const T0 &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the 3D array with a given value.  <a href="#a33ec62315a11c44f98aed69cc7808bb3"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#af32714dd9d5bcad70aad958c1e6299e8">FillRand</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the 3D array randomly.  <a href="#af32714dd9d5bcad70aad958c1e6299e8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#aca40f1ee2914601787fceafab5d8c654">Print</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays the array on the standard output.  <a href="#aca40f1ee2914601787fceafab5d8c654"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#ac4711cf67da3bca2527438ad5b1c2eaa">Write</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the 3D array in a file.  <a href="#ac4711cf67da3bca2527438ad5b1c2eaa"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#ad134dc852a94cf207fe3c33f7393b547">Write</a> (ofstream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the 3D array to an output stream.  <a href="#ad134dc852a94cf207fe3c33f7393b547"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#a943c1fb9b12c7e781bf4e39887636ab6">Read</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the 3D array from a file.  <a href="#a943c1fb9b12c7e781bf4e39887636ab6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_array3_d.php#a9b0f2c088d6b6f06ecefe17f2d616d21">Read</a> (ifstream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the 3D array from an input stream.  <a href="#a9b0f2c088d6b6f06ecefe17f2d616d21"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae66ef29cf25e8a4738193175b4ac3cb9"></a><!-- doxytag: member="Seldon::Array3D::length1_" ref="ae66ef29cf25e8a4738193175b4ac3cb9" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>length1_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0b13d84148b5630a63343f078ade81ab"></a><!-- doxytag: member="Seldon::Array3D::length2_" ref="a0b13d84148b5630a63343f078ade81ab" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>length2_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a880f51183186ab7c07abf7984534a9bc"></a><!-- doxytag: member="Seldon::Array3D::length3_" ref="a880f51183186ab7c07abf7984534a9bc" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>length3_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a656dacc826e1f935e4ad79f9c24c1181"></a><!-- doxytag: member="Seldon::Array3D::length23_" ref="a656dacc826e1f935e4ad79f9c24c1181" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>length23_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3c2adc8751bd5795beca3be82dbfc14c"></a><!-- doxytag: member="Seldon::Array3D::data_" ref="a3c2adc8751bd5795beca3be82dbfc14c" args="" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-static-attribs"></a>
Static Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa59e22296324aa679adffb95c24471f7"></a><!-- doxytag: member="Seldon::Array3D::array3D_allocator_" ref="aa59e22296324aa679adffb95c24471f7" args="" -->
static Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>array3D_allocator_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Allocator = SELDON_DEFAULT_ALLOCATOR&lt;T&gt;&gt;<br/>
 class Seldon::Array3D&lt; T, Allocator &gt;</h3>

<p>3D array. </p>
<p>This class implements 3D arrays. </p>

<p>Definition at line <a class="el" href="_array3_d_8hxx_source.php#l00038">38</a> of file <a class="el" href="_array3_d_8hxx_source.php">Array3D.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="af53eaf5dca82c2d4ae7762ea532789ef"></a><!-- doxytag: member="Seldon::Array3D::Array3D" ref="af53eaf5dca82c2d4ae7762ea532789ef" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::<a class="el" href="class_seldon_1_1_array3_d.php">Array3D</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>On exit, the array is an empty 0x0x0 3D array. </p>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00037">37</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a503b4ac2ccd0b34ecb12946bd5b6f9ce"></a><!-- doxytag: member="Seldon::Array3D::Array3D" ref="a503b4ac2ccd0b34ecb12946bd5b6f9ce" args="(int i, int j, int k)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::<a class="el" href="class_seldon_1_1_array3_d.php">Array3D</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>k</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Main constructor. </p>
<p>Builds a i x j x k 3D array, but data is not initialized. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>length in dimension #1. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>length in dimension #2. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>k</em>&nbsp;</td><td>length in dimension #3. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00056">56</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="aa9bf1047ace17bd5c2bf2d4863d46838"></a><!-- doxytag: member="Seldon::Array3D::Clear" ref="aa9bf1047ace17bd5c2bf2d4863d46838" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::Clear </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the array. </p>
<p>Destructs the array. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>On exit, the 3D array is empty. </dd></dl>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00213">213</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2f8287363e777b0072017405e182ccc0"></a><!-- doxytag: member="Seldon::Array3D::Copy" ref="a2f8287363e777b0072017405e182ccc0" args="(const Array3D&lt; T, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::Copy </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_array3_d.php">Array3D</a>&lt; T, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a 3D array. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>A</em>&nbsp;</td><td>3D array to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'A' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00397">397</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3a5ea3c1352767c1ef1f88219660c045"></a><!-- doxytag: member="Seldon::Array3D::Fill" ref="a3a5ea3c1352767c1ef1f88219660c045" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::Fill </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the array. </p>
<p>On exit, the 3D array is filled with 1, 2, 3, 4, ... The order of those numbers depends on the storage. </p>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00429">429</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a33ec62315a11c44f98aed69cc7808bb3"></a><!-- doxytag: member="Seldon::Array3D::Fill" ref="a33ec62315a11c44f98aed69cc7808bb3" args="(const T0 &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::Fill </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the 3D array with a given value. </p>
<p>On exit, the 3D array is filled with 'x'. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>the value to fill the 3D array with. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00443">443</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af32714dd9d5bcad70aad958c1e6299e8"></a><!-- doxytag: member="Seldon::Array3D::FillRand" ref="af32714dd9d5bcad70aad958c1e6299e8" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::FillRand </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the 3D array randomly. </p>
<p>On exit, the 3D array is filled with random values. </p>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00455">455</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a41def646fbd18d0a86b2fced6e80faf1"></a><!-- doxytag: member="Seldon::Array3D::GetData" ref="a41def646fbd18d0a86b2fced6e80faf1" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_array3_d.php">Array3D</a>&lt; T, Allocator &gt;::pointer <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::GetData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer to the data array. </p>
<p>Returns a pointer to data, i.e. the data array 'data_' which stores the values. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00295">295</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a64e6b9afc92a4c7fe4a99f5017959cb6"></a><!-- doxytag: member="Seldon::Array3D::GetDataSize" ref="a64e6b9afc92a4c7fe4a99f5017959cb6" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::GetDataSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored in memory. </p>
<p>Returns the number of elements stored in memory by the array, i.e. the product of lengths in the three dimensions. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements stored in the array. </dd></dl>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00281">281</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab792b6b72a04b557792b6d5ad3e86afc"></a><!-- doxytag: member="Seldon::Array3D::GetLength1" ref="ab792b6b72a04b557792b6d5ad3e86afc" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::GetLength1 </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the length in dimension #1. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length in dimension #1. </dd></dl>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00232">232</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af8acaef0c21a50f4ab6128710d534f8b"></a><!-- doxytag: member="Seldon::Array3D::GetLength2" ref="af8acaef0c21a50f4ab6128710d534f8b" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::GetLength2 </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the length in dimension #2. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length in dimension #2. </dd></dl>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00243">243</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="afae5f3ffbbcb66b09ec27d1cfc81759a"></a><!-- doxytag: member="Seldon::Array3D::GetLength3" ref="afae5f3ffbbcb66b09ec27d1cfc81759a" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::GetLength3 </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the length in dimension #3. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The length in dimension #3. </dd></dl>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00254">254</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a99dc59f2c62979e78c43d8ec31c02e68"></a><!-- doxytag: member="Seldon::Array3D::GetSize" ref="a99dc59f2c62979e78c43d8ec31c02e68" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::GetSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements in the 3D array. </p>
<p>Returns the number of elements stored by the 3D array, i.e. the product of the lengths in the three dimensions. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements in the 3D array. </dd></dl>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00267">267</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa29212ec08a08bbda48cf91cce043dd2"></a><!-- doxytag: member="Seldon::Array3D::operator()" ref="aa29212ec08a08bbda48cf91cce043dd2" args="(int i, int j, int k)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_array3_d.php">Array3D</a>&lt; T, Allocator &gt;::reference <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>k</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j, k). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>index along dimension #1. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>index along dimension #2. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>k</em>&nbsp;</td><td>index along dimension #3. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j, k) of the 3D array. </dd></dl>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00316">316</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab596d570ea5b72f77f189b8ebbd7796e"></a><!-- doxytag: member="Seldon::Array3D::operator()" ref="ab596d570ea5b72f77f189b8ebbd7796e" args="(int i, int j, int k) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_array3_d.php">Array3D</a>&lt; T, Allocator &gt;::const_reference <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>k</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j, k). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>index along dimension #1. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>index along dimension #2. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>k</em>&nbsp;</td><td>index along dimension #3. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j, k) of the 3D array. </dd></dl>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00351">351</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0cb6b0016cddb1a1e4c25c8e538e69be"></a><!-- doxytag: member="Seldon::Array3D::operator=" ref="a0cb6b0016cddb1a1e4c25c8e538e69be" args="(const Array3D&lt; T, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_array3_d.php">Array3D</a>&lt; T, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_array3_d.php">Array3D</a>&lt; T, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a 3D array (assignment operator). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>A</em>&nbsp;</td><td>3D array to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'A' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00383">383</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aca40f1ee2914601787fceafab5d8c654"></a><!-- doxytag: member="Seldon::Array3D::Print" ref="aca40f1ee2914601787fceafab5d8c654" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::Print </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Displays the array on the standard output. </p>
<p>Displays elements on the standard output, in text format. </p>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00468">468</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9b0f2c088d6b6f06ecefe17f2d616d21"></a><!-- doxytag: member="Seldon::Array3D::Read" ref="a9b0f2c088d6b6f06ecefe17f2d616d21" args="(ifstream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">ifstream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the 3D array from an input stream. </p>
<p>Reads a 3D array in binary format from an input stream. The dimensions of the array are read (i,j, k three integers), and array elements are then read in the same order as it should be in memory </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>input stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00595">595</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a943c1fb9b12c7e781bf4e39887636ab6"></a><!-- doxytag: member="Seldon::Array3D::Read" ref="a943c1fb9b12c7e781bf4e39887636ab6" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the 3D array from a file. </p>
<p>Reads a 3D array stored in binary format in a file. The dimensions of the array are read (i,j, k three integers), and array elements are then read in the same order as it should be in memory </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>input file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00567">567</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae37a5c9ebc684062db0c1be3604641e8"></a><!-- doxytag: member="Seldon::Array3D::Reallocate" ref="ae37a5c9ebc684062db0c1be3604641e8" args="(int i, int j, int k)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::Reallocate </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>k</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates memory to resize the 3D array. </p>
<p>On exit, the array is a i x j x k 3D array. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>length in dimension #1. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>length in dimension #2. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>k</em>&nbsp;</td><td>length in dimension #3. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Depending on your allocator, data may be lost. </dd></dl>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00161">161</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad134dc852a94cf207fe3c33f7393b547"></a><!-- doxytag: member="Seldon::Array3D::Write" ref="ad134dc852a94cf207fe3c33f7393b547" args="(ofstream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">ofstream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the 3D array to an output stream. </p>
<p>Writes the 3D array to an output stream in binary format. The number of rows (integer) and the number of columns (integer) are written, and array elements are then written in the same order as in memory </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00526">526</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac4711cf67da3bca2527438ad5b1c2eaa"></a><!-- doxytag: member="Seldon::Array3D::Write" ref="ac4711cf67da3bca2527438ad5b1c2eaa" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the 3D array in a file. </p>
<p>Stores the 3D array in a file in binary format. The number of rows (integer) and the number of columns (integer) are written, and matrix elements are then written in the same order as in memory </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00499">499</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab965837a5d4a255c217bb33e08b45ead"></a><!-- doxytag: member="Seldon::Array3D::Zero" ref="ab965837a5d4a255c217bb33e08b45ead" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D</a>&lt; T, Allocator &gt;::Zero </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets all elements to zero. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>It fills the memory with zeros. If the 3D array stores complex structures, use 'Fill' instead. </dd></dl>

<p>Definition at line <a class="el" href="_array3_d_8cxx_source.php#l00416">416</a> of file <a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>array3d/<a class="el" href="_array3_d_8hxx_source.php">Array3D.hxx</a></li>
<li>array3d/<a class="el" href="_array3_d_8cxx_source.php">Array3D.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
