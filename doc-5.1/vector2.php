<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Vector2 </h1>  </div>
</div>
<div class="contents">
<h2>Definition</h2>
<p><a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Seldon::Vector2</a> is a structure that acts like a vector of full vectors. The inner vectors can be of any dimension, so that this structure is more flexible than a matrix. </p>
<p>Vector2 is a template class: <code> Vector2&lt;T, Allocator0, Allocator1&gt;</code>. <code>T</code> is the numerical type of the inner vectors. <code>Allocator0</code> is the allocator for the inner vectors. It has the same default value as for vectors and matrices: SELDON_DEFAULT_ALLOCATOR. <code>Allocator1</code> is the allocator for the vector of vectors. It is recommended to choose NewAlloc or, for more efficient in reallocations, MallocObject: these allocators can manage an array of inner vectors. The default allocator is MallocObject, in which case <code>Allocator1</code> is the same as <code>MallocObject&lt;Vector&lt;T, VectFull, Allocator0&gt; &gt;</code>. </p>
<h2>Declaration</h2>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector2&lt;double&gt; V;
</pre></div>  </pre><p>This defines an empty vector (of vectors). </p>
<p>To define a Vector2 with 5 empty inner vectors: </p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector2&lt;double&gt; V(5);
</pre></div>  </pre><p>To define a Vector2 with 3 inner vectors of size 2, 3 and 7: </p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;int&gt; length(3);
length(0) = 2;
length(1) = 3;
length(2) = 7;
Vector2&lt;double&gt; V(length);
</pre></div>  </pre><h2>Use of Vector2</h2>
<p><a class="el" href="class_seldon_1_1_vector2.php" title="Vector of vectors.">Seldon::Vector2</a> comes with the following methods: </p>
<ul>
<li>
<p class="startli"><a class="el" href="class_seldon_1_1_vector2.php#ab0e6ad584c49c4182153a4e523e177ea">Reallocate(int M)</a> and <a class="el" href="class_seldon_1_1_vector2.php#aef69b526bcb070821a83f15ec44f0678">Reallocate(int i, int N)</a> which allow to reallocate the vector of vectors and the i-th inner vector, respectively.</p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><a class="el" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8">GetLength()</a> which returns the number of inner vectors. </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><a class="el" href="">GetLength(int i)</a> which returns the length of an inner vector. </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><a class="el" href="class_seldon_1_1_vector2.php#ad8b3dd46b3b18c63fbf91ff266151ce4">operator()(int i)</a> which returns the i-th inner vector. </p>
<p class="endli"></p>
</li>
<li>
<p class="startli"><a class="el" href="class_seldon_1_1_vector2.php#a4fe142f0dce38f333d19854401394d86">operator()(int i, int j)</a> which returns the j-th element of the i-th inner vector. </p>
<p class="endli"></p>
</li>
</ul>
<p>The other methods are described in the <a class="el" href="class_seldon_1_1_vector2.php">reference documentation</a>.</p>
<h2><a class="anchor" id="example"></a>
An Example</h2>
 <div style="text-align:right;"><a href="src/doc/example/vector2.cpp">doc/example/vector2.cpp</a></div>  <div class="fragment"><pre class="fragment"><span class="preprocessor">#define SELDON_DEBUG_LEVEL_4</span>
<span class="preprocessor"></span><span class="preprocessor">#include &quot;Seldon.hxx&quot;</span>
<span class="keyword">using namespace </span>Seldon;

<span class="preprocessor">#include &quot;vector/Vector2.cxx&quot;</span>

<span class="keywordtype">int</span> main()
{

  TRY;

  Vector&lt;int&gt; length(3);
  length(0) = 2;
  length(1) = 3;
  length(2) = 7;
  Vector2&lt;double&gt; V(length);

  <span class="comment">// Fills all inner vectors with 2.</span>
  V.Fill(2.);
  <span class="comment">// Access to the second inner vector, to fill it with 5.</span>
  V(1).Fill(5.);

  V.Print();
  cout &lt;&lt; <span class="stringliteral">&quot;First element of the second inner vector: &quot;</span> &lt;&lt; V(1, 0) &lt;&lt; endl;
  <span class="comment">// Note that V(1)(0) would have returned the same element.</span>

  Vector&lt;double&gt; inner_vector(4);
  inner_vector.Fill();
  <span class="comment">// Appends a new inner vector.</span>
  V.PushBack(inner_vector);
  V.Print();

  cout &lt;&lt; <span class="stringliteral">&quot;After setting to -10 the second element of the last inner vector:&quot;</span>
       &lt;&lt; endl;
  V(3, 1) = -10.;
  V.Print();

  END;

  <span class="keywordflow">return</span> 0;

}
</pre></div><p>Output:</p>
<div class="fragment"><pre class="fragment">
Vector 0: 2     2       
Vector 1: 5     5       5       
Vector 2: 2     2       2       2       2       2       2       
First element of the second inner vector: 5
Vector 0: 2     2       
Vector 1: 5     5       5       
Vector 2: 2     2       2       2       2       2       2       
Vector 3: 0     1       2       3       
After setting to -10 the second element of the last inner vector:
Vector 0: 2     2       
Vector 1: 5     5       5       
Vector 2: 2     2       2       2       2       2       2       
Vector 3: 0     -10     2       3       
</pre></div> </div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
