<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>vector/Functions_Arrays.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_FUNCTIONS_ARRAYS_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="comment">/*</span>
<a name="l00024"></a>00024 <span class="comment">  Functions defined in this file</span>
<a name="l00025"></a>00025 <span class="comment"></span>
<a name="l00026"></a>00026 <span class="comment">  QuickSort(m, n, X);</span>
<a name="l00027"></a>00027 <span class="comment">  QuickSort(m, n, X, Y);</span>
<a name="l00028"></a>00028 <span class="comment">  QuickSort(m, n, X, Y, Z);</span>
<a name="l00029"></a>00029 <span class="comment"></span>
<a name="l00030"></a>00030 <span class="comment">  MergeSort(m, n, X);</span>
<a name="l00031"></a>00031 <span class="comment">  MergeSort(m, n, X, Y);</span>
<a name="l00032"></a>00032 <span class="comment">  MergeSort(m, n, X, Y, Z);</span>
<a name="l00033"></a>00033 <span class="comment"></span>
<a name="l00034"></a>00034 <span class="comment">  Sort(m, n, X);</span>
<a name="l00035"></a>00035 <span class="comment">  Sort(m, n, X, Y);</span>
<a name="l00036"></a>00036 <span class="comment">  Sort(m, n, X, Y, Z);</span>
<a name="l00037"></a>00037 <span class="comment">  Sort(m, X);</span>
<a name="l00038"></a>00038 <span class="comment">  Sort(m, X, Y);</span>
<a name="l00039"></a>00039 <span class="comment">  Sort(m, X, Y, Z);</span>
<a name="l00040"></a>00040 <span class="comment">  Sort(X);</span>
<a name="l00041"></a>00041 <span class="comment">  Sort(X, Y);</span>
<a name="l00042"></a>00042 <span class="comment">  Sort(X, Y, Z);</span>
<a name="l00043"></a>00043 <span class="comment"></span>
<a name="l00044"></a>00044 <span class="comment">  Assemble(m, X);</span>
<a name="l00045"></a>00045 <span class="comment">  Assemble(m, X, Y);</span>
<a name="l00046"></a>00046 <span class="comment"></span>
<a name="l00047"></a>00047 <span class="comment">  RemoveDuplicate(m, X);</span>
<a name="l00048"></a>00048 <span class="comment">  RemoveDuplicate(m, X, Y);</span>
<a name="l00049"></a>00049 <span class="comment">  RemoveDuplicate(X);</span>
<a name="l00050"></a>00050 <span class="comment">  RemoveDuplicate(X, Y);</span>
<a name="l00051"></a>00051 <span class="comment">*/</span>
<a name="l00052"></a>00052 
<a name="l00053"></a>00053 <span class="keyword">namespace </span>Seldon
<a name="l00054"></a>00054 {
<a name="l00055"></a>00055 
<a name="l00056"></a>00056 
<a name="l00058"></a>00058   <span class="comment">//  SORT  //</span>
<a name="l00059"></a>00059 
<a name="l00060"></a>00060 
<a name="l00062"></a>00062   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00063"></a><a class="code" href="namespace_seldon.php#af953a28959a47b5efb4d32c3fd1928be">00063</a>   <span class="keywordtype">int</span> <a class="code" href="namespace_seldon.php#af953a28959a47b5efb4d32c3fd1928be" title="Intermediary function used for quick sort algorithm.">PartitionQuickSort</a>(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n,
<a name="l00064"></a>00064                          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage, Allocator&gt;</a>&amp; t)
<a name="l00065"></a>00065   {
<a name="l00066"></a>00066     T temp, v;
<a name="l00067"></a>00067     v = t(m);
<a name="l00068"></a>00068     <span class="keywordtype">int</span> i = m - 1;
<a name="l00069"></a>00069     <span class="keywordtype">int</span> j = n + 1;
<a name="l00070"></a>00070 
<a name="l00071"></a>00071     <span class="keywordflow">while</span> (<span class="keyword">true</span>)
<a name="l00072"></a>00072       {
<a name="l00073"></a>00073         <span class="keywordflow">do</span>
<a name="l00074"></a>00074           {
<a name="l00075"></a>00075             j--;
<a name="l00076"></a>00076           }
<a name="l00077"></a>00077         <span class="keywordflow">while</span> (t(j) &gt; v);
<a name="l00078"></a>00078 
<a name="l00079"></a>00079         <span class="keywordflow">do</span>
<a name="l00080"></a>00080           {
<a name="l00081"></a>00081             i++;
<a name="l00082"></a>00082           }
<a name="l00083"></a>00083         <span class="keywordflow">while</span> (t(i) &lt; v);
<a name="l00084"></a>00084 
<a name="l00085"></a>00085         <span class="keywordflow">if</span> (i &lt; j)
<a name="l00086"></a>00086           {
<a name="l00087"></a>00087             temp = t(i);
<a name="l00088"></a>00088             t(i) = t(j);
<a name="l00089"></a>00089             t(j) = temp;
<a name="l00090"></a>00090           }
<a name="l00091"></a>00091         <span class="keywordflow">else</span>
<a name="l00092"></a>00092           {
<a name="l00093"></a>00093             <span class="keywordflow">return</span> j;
<a name="l00094"></a>00094           }
<a name="l00095"></a>00095       }
<a name="l00096"></a>00096   }
<a name="l00097"></a>00097 
<a name="l00098"></a>00098 
<a name="l00100"></a>00100 
<a name="l00103"></a>00103   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00104"></a><a class="code" href="namespace_seldon.php#a5d4c8aa9d77ecd7153e3bb71bfce79bb">00104</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a5d4c8aa9d77ecd7153e3bb71bfce79bb" title="Vector t is sorted by using QuickSort algorithm.">QuickSort</a>(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n,
<a name="l00105"></a>00105                  <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage, Allocator&gt;</a>&amp; t)
<a name="l00106"></a>00106   {
<a name="l00107"></a>00107     <span class="keywordflow">if</span> (m &lt; n)
<a name="l00108"></a>00108       {
<a name="l00109"></a>00109         <span class="keywordtype">int</span> p = <a class="code" href="namespace_seldon.php#af953a28959a47b5efb4d32c3fd1928be" title="Intermediary function used for quick sort algorithm.">PartitionQuickSort</a>(m, n, t);
<a name="l00110"></a>00110         <a class="code" href="namespace_seldon.php#a5d4c8aa9d77ecd7153e3bb71bfce79bb" title="Vector t is sorted by using QuickSort algorithm.">QuickSort</a>(m, p, t);
<a name="l00111"></a>00111         <a class="code" href="namespace_seldon.php#a5d4c8aa9d77ecd7153e3bb71bfce79bb" title="Vector t is sorted by using QuickSort algorithm.">QuickSort</a>(p+1, n, t);
<a name="l00112"></a>00112       }
<a name="l00113"></a>00113   }
<a name="l00114"></a>00114 
<a name="l00115"></a>00115 
<a name="l00117"></a>00117   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00118"></a>00118            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00119"></a><a class="code" href="namespace_seldon.php#a103ccbab97a1bf370652eaa9997c23ac">00119</a>   <span class="keywordtype">int</span> <a class="code" href="namespace_seldon.php#af953a28959a47b5efb4d32c3fd1928be" title="Intermediary function used for quick sort algorithm.">PartitionQuickSort</a>(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n,
<a name="l00120"></a>00120                          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; t1,
<a name="l00121"></a>00121                          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; t2)
<a name="l00122"></a>00122   {
<a name="l00123"></a>00123     T1 temp1, v;
<a name="l00124"></a>00124     T2 temp2;
<a name="l00125"></a>00125     v = t1(m);
<a name="l00126"></a>00126     <span class="keywordtype">int</span> i = m - 1;
<a name="l00127"></a>00127     <span class="keywordtype">int</span> j = n + 1;
<a name="l00128"></a>00128 
<a name="l00129"></a>00129     <span class="keywordflow">while</span> (<span class="keyword">true</span>)
<a name="l00130"></a>00130       {
<a name="l00131"></a>00131         <span class="keywordflow">do</span>
<a name="l00132"></a>00132           {
<a name="l00133"></a>00133             j--;
<a name="l00134"></a>00134           }
<a name="l00135"></a>00135         <span class="keywordflow">while</span> (t1(j) &gt; v);
<a name="l00136"></a>00136         <span class="keywordflow">do</span>
<a name="l00137"></a>00137           {
<a name="l00138"></a>00138             i++;
<a name="l00139"></a>00139           }
<a name="l00140"></a>00140         <span class="keywordflow">while</span> (t1(i) &lt; v);
<a name="l00141"></a>00141 
<a name="l00142"></a>00142         <span class="keywordflow">if</span> (i &lt; j)
<a name="l00143"></a>00143           {
<a name="l00144"></a>00144             temp1 = t1(i);
<a name="l00145"></a>00145             t1(i) = t1(j);
<a name="l00146"></a>00146             t1(j) = temp1;
<a name="l00147"></a>00147             temp2 = t2(i);
<a name="l00148"></a>00148             t2(i) = t2(j);
<a name="l00149"></a>00149             t2(j) = temp2;
<a name="l00150"></a>00150           }
<a name="l00151"></a>00151         <span class="keywordflow">else</span>
<a name="l00152"></a>00152           {
<a name="l00153"></a>00153             <span class="keywordflow">return</span> j;
<a name="l00154"></a>00154           }
<a name="l00155"></a>00155       }
<a name="l00156"></a>00156   }
<a name="l00157"></a>00157 
<a name="l00158"></a>00158 
<a name="l00160"></a>00160 
<a name="l00163"></a>00163   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00164"></a>00164            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00165"></a><a class="code" href="namespace_seldon.php#a2ae04b5e66324a06b4cc0fce19b72ca6">00165</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a5d4c8aa9d77ecd7153e3bb71bfce79bb" title="Vector t is sorted by using QuickSort algorithm.">QuickSort</a>(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n,
<a name="l00166"></a>00166                  <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; t1,
<a name="l00167"></a>00167                  <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; t2)
<a name="l00168"></a>00168   {
<a name="l00169"></a>00169     <span class="keywordflow">if</span> (m &lt; n)
<a name="l00170"></a>00170       {
<a name="l00171"></a>00171         <span class="keywordtype">int</span> p = <a class="code" href="namespace_seldon.php#af953a28959a47b5efb4d32c3fd1928be" title="Intermediary function used for quick sort algorithm.">PartitionQuickSort</a>(m, n, t1, t2);
<a name="l00172"></a>00172         <a class="code" href="namespace_seldon.php#a5d4c8aa9d77ecd7153e3bb71bfce79bb" title="Vector t is sorted by using QuickSort algorithm.">QuickSort</a>(m, p, t1, t2);
<a name="l00173"></a>00173         <a class="code" href="namespace_seldon.php#a5d4c8aa9d77ecd7153e3bb71bfce79bb" title="Vector t is sorted by using QuickSort algorithm.">QuickSort</a>(p+1, n, t1, t2);
<a name="l00174"></a>00174       }
<a name="l00175"></a>00175   }
<a name="l00176"></a>00176 
<a name="l00177"></a>00177 
<a name="l00179"></a>00179   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00180"></a>00180            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00181"></a>00181            <span class="keyword">class </span>T3, <span class="keyword">class </span>Storage3, <span class="keyword">class </span>Allocator3&gt;
<a name="l00182"></a><a class="code" href="namespace_seldon.php#a5b88cdfad7fd3708f8f48bfbef00e932">00182</a>   <span class="keywordtype">int</span> <a class="code" href="namespace_seldon.php#af953a28959a47b5efb4d32c3fd1928be" title="Intermediary function used for quick sort algorithm.">PartitionQuickSort</a>(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n,
<a name="l00183"></a>00183                          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; t1,
<a name="l00184"></a>00184                          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; t2,
<a name="l00185"></a>00185                          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T3, Storage3, Allocator3&gt;</a>&amp; t3)
<a name="l00186"></a>00186   {
<a name="l00187"></a>00187     T1 temp1, v;
<a name="l00188"></a>00188     T2 temp2;
<a name="l00189"></a>00189     T3 temp3;
<a name="l00190"></a>00190     v = t1(m);
<a name="l00191"></a>00191     <span class="keywordtype">int</span> i = m - 1;
<a name="l00192"></a>00192     <span class="keywordtype">int</span> j = n + 1;
<a name="l00193"></a>00193 
<a name="l00194"></a>00194     <span class="keywordflow">while</span> (<span class="keyword">true</span>)
<a name="l00195"></a>00195       {
<a name="l00196"></a>00196         <span class="keywordflow">do</span>
<a name="l00197"></a>00197           {
<a name="l00198"></a>00198             j--;
<a name="l00199"></a>00199           }
<a name="l00200"></a>00200         <span class="keywordflow">while</span> (t1(j) &gt; v);
<a name="l00201"></a>00201 
<a name="l00202"></a>00202         <span class="keywordflow">do</span>
<a name="l00203"></a>00203           {
<a name="l00204"></a>00204             i++;
<a name="l00205"></a>00205           }
<a name="l00206"></a>00206         <span class="keywordflow">while</span> (t1(i) &lt; v);
<a name="l00207"></a>00207 
<a name="l00208"></a>00208         <span class="keywordflow">if</span> (i &lt; j)
<a name="l00209"></a>00209           {
<a name="l00210"></a>00210             temp1 = t1(i);
<a name="l00211"></a>00211             t1(i) = t1(j);
<a name="l00212"></a>00212             t1(j) = temp1;
<a name="l00213"></a>00213             temp2 = t2(i);
<a name="l00214"></a>00214             t2(i) = t2(j);
<a name="l00215"></a>00215             t2(j) = temp2;
<a name="l00216"></a>00216             temp3 = t3(i);
<a name="l00217"></a>00217             t3(i) = t3(j);
<a name="l00218"></a>00218             t3(j) = temp3;
<a name="l00219"></a>00219           }
<a name="l00220"></a>00220         <span class="keywordflow">else</span>
<a name="l00221"></a>00221           {
<a name="l00222"></a>00222             <span class="keywordflow">return</span> j;
<a name="l00223"></a>00223           }
<a name="l00224"></a>00224       }
<a name="l00225"></a>00225   }
<a name="l00226"></a>00226 
<a name="l00227"></a>00227 
<a name="l00229"></a>00229 
<a name="l00232"></a>00232   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00233"></a>00233            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00234"></a>00234            <span class="keyword">class </span>T3, <span class="keyword">class </span>Storage3, <span class="keyword">class </span>Allocator3&gt;
<a name="l00235"></a><a class="code" href="namespace_seldon.php#a1793a4f6c61bf4d19bc94035ae86ce28">00235</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a5d4c8aa9d77ecd7153e3bb71bfce79bb" title="Vector t is sorted by using QuickSort algorithm.">QuickSort</a>(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n,
<a name="l00236"></a>00236                  <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; t1,
<a name="l00237"></a>00237                  <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; t2,
<a name="l00238"></a>00238                  <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T3, Storage3, Allocator3&gt;</a>&amp; t3)
<a name="l00239"></a>00239   {
<a name="l00240"></a>00240     <span class="keywordflow">if</span> (m &lt; n)
<a name="l00241"></a>00241       {
<a name="l00242"></a>00242         <span class="keywordtype">int</span> p = <a class="code" href="namespace_seldon.php#af953a28959a47b5efb4d32c3fd1928be" title="Intermediary function used for quick sort algorithm.">PartitionQuickSort</a>(m, n, t1, t2, t3);
<a name="l00243"></a>00243         <a class="code" href="namespace_seldon.php#a5d4c8aa9d77ecd7153e3bb71bfce79bb" title="Vector t is sorted by using QuickSort algorithm.">QuickSort</a>(m, p, t1, t2, t3);
<a name="l00244"></a>00244         <a class="code" href="namespace_seldon.php#a5d4c8aa9d77ecd7153e3bb71bfce79bb" title="Vector t is sorted by using QuickSort algorithm.">QuickSort</a>(p+1, n, t1, t2, t3);
<a name="l00245"></a>00245       }
<a name="l00246"></a>00246   }
<a name="l00247"></a>00247 
<a name="l00248"></a>00248 
<a name="l00250"></a>00250 
<a name="l00253"></a>00253   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00254"></a><a class="code" href="namespace_seldon.php#a310936614a04226eed19b4ed08391b8d">00254</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a310936614a04226eed19b4ed08391b8d" title="Vector tab1 is sorted by using MergeSort algorithm.">MergeSort</a>(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n, <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage, Allocator&gt;</a>&amp; tab1)
<a name="l00255"></a>00255   {
<a name="l00256"></a>00256     <span class="keywordflow">if</span> (m &gt;= n)
<a name="l00257"></a>00257       <span class="keywordflow">return</span>;
<a name="l00258"></a>00258 
<a name="l00259"></a>00259     <span class="keywordtype">int</span> inc = 1, ind = 0, current, i, j, sup;
<a name="l00260"></a>00260     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage, Allocator&gt;</a> tab1t(n - m + 1);
<a name="l00261"></a>00261     <span class="comment">// Performs a merge sort with a recurrence.</span>
<a name="l00262"></a>00262     <span class="comment">// inc = 1, 2, 4, 8, ...</span>
<a name="l00263"></a>00263     <span class="keywordflow">while</span> (inc &lt; (n - m + 1) )
<a name="l00264"></a>00264       {
<a name="l00265"></a>00265         <span class="comment">// &#39;i&#39; is the first index of the sub array of size 2*inc.</span>
<a name="l00266"></a>00266         <span class="comment">// A loop is performed on these sub-arrays.</span>
<a name="l00267"></a>00267         <span class="comment">// Each sub array is divided in two sub-arrays of size &#39;inc&#39;.</span>
<a name="l00268"></a>00268         <span class="comment">// These two sub-arrays are then merged.</span>
<a name="l00269"></a>00269         <span class="keywordflow">for</span> (i = m; i &lt;= n - inc; i += 2 * inc)
<a name="l00270"></a>00270           {
<a name="l00271"></a>00271             ind = i;
<a name="l00272"></a>00272             current = i + inc; <span class="comment">// Index of the second sub-array.</span>
<a name="l00273"></a>00273             sup = i + 2 * inc; <span class="comment">// End of the merged array.</span>
<a name="l00274"></a>00274             <span class="keywordflow">if</span> (sup &gt;= n + 1)
<a name="l00275"></a>00275               sup = n + 1;
<a name="l00276"></a>00276 
<a name="l00277"></a>00277             j = i;
<a name="l00278"></a>00278             <span class="comment">// Loop on values of the first sub-array.</span>
<a name="l00279"></a>00279             <span class="keywordflow">while</span> (j &lt; i + inc)
<a name="l00280"></a>00280               {
<a name="l00281"></a>00281                 <span class="comment">// If the second sub-array has still unsorted elements.</span>
<a name="l00282"></a>00282                 <span class="keywordflow">if</span> (current &lt; sup)
<a name="l00283"></a>00283                   {
<a name="l00284"></a>00284                     <span class="comment">// Insert the elements of the second sub-array in the</span>
<a name="l00285"></a>00285                     <span class="comment">// merged array until tab1(j) &lt; tab1(current).</span>
<a name="l00286"></a>00286                     <span class="keywordflow">while</span> (current &lt; sup &amp;&amp; tab1(j) &gt; tab1(current))
<a name="l00287"></a>00287                       {
<a name="l00288"></a>00288                         tab1t(ind-m) = tab1(current);
<a name="l00289"></a>00289                         current++;
<a name="l00290"></a>00290                         ind++;
<a name="l00291"></a>00291                       }
<a name="l00292"></a>00292 
<a name="l00293"></a>00293                     <span class="comment">// Inserts the element of the first sub-array now.</span>
<a name="l00294"></a>00294                     tab1t(ind - m) = tab1(j);
<a name="l00295"></a>00295                     ind++;
<a name="l00296"></a>00296                     j++;
<a name="l00297"></a>00297 
<a name="l00298"></a>00298                     <span class="comment">// If the first sub-array is sorted, all remaining</span>
<a name="l00299"></a>00299                     <span class="comment">// elements of the second sub-array are inserted.</span>
<a name="l00300"></a>00300                     <span class="keywordflow">if</span> (j == i + inc)
<a name="l00301"></a>00301                       {
<a name="l00302"></a>00302                         <span class="keywordflow">for</span> (j = current; j &lt; sup; j++)
<a name="l00303"></a>00303                           {
<a name="l00304"></a>00304                             tab1t(ind - m) = tab1(j);
<a name="l00305"></a>00305                             ind++;
<a name="l00306"></a>00306                           }
<a name="l00307"></a>00307                       }
<a name="l00308"></a>00308                   }
<a name="l00309"></a>00309                 <span class="keywordflow">else</span>
<a name="l00310"></a>00310                   {
<a name="l00311"></a>00311                     <span class="comment">// If the second sub-array is sorted, all remaining</span>
<a name="l00312"></a>00312                     <span class="comment">// elements of the first sub-array are inserted.</span>
<a name="l00313"></a>00313                     <span class="keywordflow">for</span> (current = j; current &lt; i + inc; current++)
<a name="l00314"></a>00314                       {
<a name="l00315"></a>00315                         tab1t(ind - m) = tab1(current);
<a name="l00316"></a>00316                         ind++;
<a name="l00317"></a>00317                       }
<a name="l00318"></a>00318                     j = current + 1;
<a name="l00319"></a>00319                   }
<a name="l00320"></a>00320               }
<a name="l00321"></a>00321           }
<a name="l00322"></a>00322 
<a name="l00323"></a>00323         <span class="keywordflow">for</span> (i = m; i &lt; ind; i++)
<a name="l00324"></a>00324           tab1(i) = tab1t(i - m);
<a name="l00325"></a>00325 
<a name="l00326"></a>00326         inc = 2 * inc;
<a name="l00327"></a>00327       }
<a name="l00328"></a>00328   }
<a name="l00329"></a>00329 
<a name="l00330"></a>00330 
<a name="l00332"></a>00332 
<a name="l00335"></a>00335   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00336"></a>00336            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00337"></a><a class="code" href="namespace_seldon.php#a54bfed74efd43816113f314ceb8bf4ff">00337</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a310936614a04226eed19b4ed08391b8d" title="Vector tab1 is sorted by using MergeSort algorithm.">MergeSort</a>(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n, <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; tab1,
<a name="l00338"></a>00338                  <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; tab2)
<a name="l00339"></a>00339   {
<a name="l00340"></a>00340     <span class="keywordflow">if</span> (m &gt;= n)
<a name="l00341"></a>00341       <span class="keywordflow">return</span>;
<a name="l00342"></a>00342 
<a name="l00343"></a>00343     <span class="keywordtype">int</span> inc = 1, ind = 0, current, i, j, sup;
<a name="l00344"></a>00344     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a> tab1t(n - m + 1);
<a name="l00345"></a>00345     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a> tab2t(n - m + 1);
<a name="l00346"></a>00346 
<a name="l00347"></a>00347     <span class="keywordflow">while</span> (inc &lt; n - m + 1)
<a name="l00348"></a>00348       {
<a name="l00349"></a>00349         <span class="keywordflow">for</span> (i = m; i &lt;= n - inc; i += 2 * inc)
<a name="l00350"></a>00350           {
<a name="l00351"></a>00351             ind = i;
<a name="l00352"></a>00352             current = i + inc;
<a name="l00353"></a>00353             sup = i + 2 * inc;
<a name="l00354"></a>00354             <span class="keywordflow">if</span> (sup &gt;= n+1)
<a name="l00355"></a>00355               sup = n+1;
<a name="l00356"></a>00356 
<a name="l00357"></a>00357             j = i;
<a name="l00358"></a>00358             <span class="keywordflow">while</span> (j &lt; i + inc)
<a name="l00359"></a>00359               {
<a name="l00360"></a>00360                 <span class="keywordflow">if</span> (current &lt; sup)
<a name="l00361"></a>00361                   {
<a name="l00362"></a>00362                     <span class="keywordflow">while</span> (current &lt; sup &amp;&amp; tab1(j) &gt; tab1(current))
<a name="l00363"></a>00363                       {
<a name="l00364"></a>00364                         tab1t(ind - m) = tab1(current);
<a name="l00365"></a>00365                         tab2t(ind - m) = tab2(current);
<a name="l00366"></a>00366                         current++;
<a name="l00367"></a>00367                         ind++;
<a name="l00368"></a>00368                       }
<a name="l00369"></a>00369                     tab1t(ind - m) = tab1(j);
<a name="l00370"></a>00370                     tab2t(ind - m) = tab2(j);
<a name="l00371"></a>00371                     ind++;
<a name="l00372"></a>00372                     j++;
<a name="l00373"></a>00373                     <span class="keywordflow">if</span> (j == i + inc)
<a name="l00374"></a>00374                       {
<a name="l00375"></a>00375                         <span class="keywordflow">for</span> (j = current; j &lt; sup; j++)
<a name="l00376"></a>00376                           {
<a name="l00377"></a>00377                             tab1t(ind - m) = tab1(j);
<a name="l00378"></a>00378                             tab2t(ind - m) = tab2(j);
<a name="l00379"></a>00379                             ind++;
<a name="l00380"></a>00380                           }
<a name="l00381"></a>00381                       }
<a name="l00382"></a>00382                   }
<a name="l00383"></a>00383                 <span class="keywordflow">else</span>
<a name="l00384"></a>00384                   {
<a name="l00385"></a>00385                     <span class="keywordflow">for</span> (current = j; current &lt; i + inc; current++)
<a name="l00386"></a>00386                       {
<a name="l00387"></a>00387                         tab1t(ind - m) = tab1(current);
<a name="l00388"></a>00388                         tab2t(ind - m) = tab2(current);
<a name="l00389"></a>00389                         ind++;
<a name="l00390"></a>00390                       }
<a name="l00391"></a>00391                     j = current + 1;
<a name="l00392"></a>00392                   }
<a name="l00393"></a>00393               }
<a name="l00394"></a>00394           }
<a name="l00395"></a>00395         <span class="keywordflow">for</span> (i = m; i &lt; ind; i++)
<a name="l00396"></a>00396           {
<a name="l00397"></a>00397             tab1(i) = tab1t(i - m);
<a name="l00398"></a>00398             tab2(i) = tab2t(i - m);
<a name="l00399"></a>00399           }
<a name="l00400"></a>00400         inc = 2 * inc;
<a name="l00401"></a>00401       }
<a name="l00402"></a>00402   }
<a name="l00403"></a>00403 
<a name="l00404"></a>00404 
<a name="l00406"></a>00406 
<a name="l00409"></a>00409   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00410"></a>00410            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00411"></a>00411            <span class="keyword">class </span>T3, <span class="keyword">class </span>Storage3, <span class="keyword">class </span>Allocator3&gt;
<a name="l00412"></a><a class="code" href="namespace_seldon.php#a7dd5ed9204d6526a032a768c1a1b7776">00412</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a310936614a04226eed19b4ed08391b8d" title="Vector tab1 is sorted by using MergeSort algorithm.">MergeSort</a>(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n, <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; tab1,
<a name="l00413"></a>00413                  <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; tab2,
<a name="l00414"></a>00414                  <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T3, Storage3, Allocator3&gt;</a>&amp; tab3)
<a name="l00415"></a>00415   {
<a name="l00416"></a>00416     <span class="keywordflow">if</span> (m &gt;= n)
<a name="l00417"></a>00417       <span class="keywordflow">return</span>;
<a name="l00418"></a>00418 
<a name="l00419"></a>00419     <span class="keywordtype">int</span> inc = 1, ind = 0, current, i, j, sup;
<a name="l00420"></a>00420     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a> tab1t(n - m + 1);
<a name="l00421"></a>00421     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a> tab2t(n - m + 1);
<a name="l00422"></a>00422     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T3, Storage3, Allocator3&gt;</a> tab3t(n - m + 1);
<a name="l00423"></a>00423 
<a name="l00424"></a>00424     <span class="keywordflow">while</span> (inc &lt; n - m + 1)
<a name="l00425"></a>00425       {
<a name="l00426"></a>00426         <span class="keywordflow">for</span> (i = m; i &lt;= n - inc; i += 2 * inc)
<a name="l00427"></a>00427           {
<a name="l00428"></a>00428             ind = i;
<a name="l00429"></a>00429             current = i + inc;
<a name="l00430"></a>00430             sup = i + 2 * inc;
<a name="l00431"></a>00431             <span class="keywordflow">if</span> (sup &gt;= n+1)
<a name="l00432"></a>00432               sup = n+1;
<a name="l00433"></a>00433 
<a name="l00434"></a>00434             j = i;
<a name="l00435"></a>00435             <span class="keywordflow">while</span> (j &lt; i + inc)
<a name="l00436"></a>00436               {
<a name="l00437"></a>00437                 <span class="keywordflow">if</span> (current &lt; sup)
<a name="l00438"></a>00438                   {
<a name="l00439"></a>00439                     <span class="keywordflow">while</span> (current &lt; sup &amp;&amp; tab1(j) &gt; tab1(current))
<a name="l00440"></a>00440                       {
<a name="l00441"></a>00441                         tab1t(ind - m) = tab1(current);
<a name="l00442"></a>00442                         tab2t(ind - m) = tab2(current);
<a name="l00443"></a>00443                         tab3t(ind - m) = tab3(current);
<a name="l00444"></a>00444                         current++;
<a name="l00445"></a>00445                         ind++;
<a name="l00446"></a>00446                       }
<a name="l00447"></a>00447                     tab1t(ind - m) = tab1(j);
<a name="l00448"></a>00448                     tab2t(ind - m) = tab2(j);
<a name="l00449"></a>00449                     tab3t(ind - m) = tab3(j);
<a name="l00450"></a>00450                     ind++;
<a name="l00451"></a>00451                     j++;
<a name="l00452"></a>00452                     <span class="keywordflow">if</span> (j == i + inc)
<a name="l00453"></a>00453                       {
<a name="l00454"></a>00454                         <span class="keywordflow">for</span> (j = current; j &lt; sup; j++)
<a name="l00455"></a>00455                           {
<a name="l00456"></a>00456                             tab1t(ind - m) = tab1(j);
<a name="l00457"></a>00457                             tab2t(ind - m) = tab2(j);
<a name="l00458"></a>00458                             tab3t(ind - m) = tab3(j);
<a name="l00459"></a>00459                             ind++;
<a name="l00460"></a>00460                           }
<a name="l00461"></a>00461                       }
<a name="l00462"></a>00462                   }
<a name="l00463"></a>00463                 <span class="keywordflow">else</span>
<a name="l00464"></a>00464                   {
<a name="l00465"></a>00465                     <span class="keywordflow">for</span> (current = j; current &lt; i + inc; current++)
<a name="l00466"></a>00466                       {
<a name="l00467"></a>00467                         tab1t(ind - m) = tab1(current);
<a name="l00468"></a>00468                         tab2t(ind - m) = tab2(current);
<a name="l00469"></a>00469                         tab3t(ind - m) = tab3(current);
<a name="l00470"></a>00470                         ind++;
<a name="l00471"></a>00471                       }
<a name="l00472"></a>00472                     j = current+1;
<a name="l00473"></a>00473                   }
<a name="l00474"></a>00474               }
<a name="l00475"></a>00475           }
<a name="l00476"></a>00476         <span class="keywordflow">for</span> (i = m; i &lt; ind; i++)
<a name="l00477"></a>00477           {
<a name="l00478"></a>00478             tab1(i) = tab1t(i - m);
<a name="l00479"></a>00479             tab2(i) = tab2t(i - m);
<a name="l00480"></a>00480             tab3(i) = tab3t(i - m);
<a name="l00481"></a>00481           }
<a name="l00482"></a>00482         inc = 2 * inc;
<a name="l00483"></a>00483       }
<a name="l00484"></a>00484   }
<a name="l00485"></a>00485 
<a name="l00486"></a>00486 
<a name="l00488"></a>00488 
<a name="l00500"></a>00500   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00501"></a>00501            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2 &gt;
<a name="l00502"></a><a class="code" href="namespace_seldon.php#a3e269a2dbcd9a3d99011cee07da8c230">00502</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a3e269a2dbcd9a3d99011cee07da8c230" title="Assembles a sparse vector.">Assemble</a>(<span class="keywordtype">int</span>&amp; n, <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; Node,
<a name="l00503"></a>00503                 <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; Vect)
<a name="l00504"></a>00504   {
<a name="l00505"></a>00505     <span class="keywordflow">if</span> (n &lt;= 1)
<a name="l00506"></a>00506       <span class="keywordflow">return</span>;
<a name="l00507"></a>00507 
<a name="l00508"></a>00508     <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(n, Node, Vect);
<a name="l00509"></a>00509     <span class="keywordtype">int</span> prec = Node(0);
<a name="l00510"></a>00510     <span class="keywordtype">int</span> nb = 0;
<a name="l00511"></a>00511     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 1; i &lt; n; i++)
<a name="l00512"></a>00512       <span class="keywordflow">if</span> (Node(i) == prec)
<a name="l00513"></a>00513         {
<a name="l00514"></a>00514           Vect(nb) += Vect(i);
<a name="l00515"></a>00515         }
<a name="l00516"></a>00516       <span class="keywordflow">else</span>
<a name="l00517"></a>00517         {
<a name="l00518"></a>00518           nb++;
<a name="l00519"></a>00519           Node(nb) = Node(i);
<a name="l00520"></a>00520           Vect(nb) = Vect(i);
<a name="l00521"></a>00521           prec = Node(nb);
<a name="l00522"></a>00522         }
<a name="l00523"></a>00523     n = nb + 1;
<a name="l00524"></a>00524   }
<a name="l00525"></a>00525 
<a name="l00526"></a>00526 
<a name="l00528"></a>00528 
<a name="l00534"></a>00534   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00535"></a><a class="code" href="namespace_seldon.php#a8a6d1405b01beba2f85b132fdde91bea">00535</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a3e269a2dbcd9a3d99011cee07da8c230" title="Assembles a sparse vector.">Assemble</a>(<span class="keywordtype">int</span>&amp; n, <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage1, Allocator1&gt;</a>&amp; Node)
<a name="l00536"></a>00536   {
<a name="l00537"></a>00537     <span class="keywordflow">if</span> (n &lt;= 1)
<a name="l00538"></a>00538       <span class="keywordflow">return</span>;
<a name="l00539"></a>00539 
<a name="l00540"></a>00540     <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(n, Node);
<a name="l00541"></a>00541     T prec = Node(0);
<a name="l00542"></a>00542     <span class="keywordtype">int</span> nb = 1;
<a name="l00543"></a>00543     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 1; i &lt; n; i++)
<a name="l00544"></a>00544       <span class="keywordflow">if</span> (Node(i) != prec)
<a name="l00545"></a>00545         {
<a name="l00546"></a>00546           Node(nb) = Node(i);
<a name="l00547"></a>00547           prec = Node(nb);
<a name="l00548"></a>00548           nb++;
<a name="l00549"></a>00549         }
<a name="l00550"></a>00550     n = nb;
<a name="l00551"></a>00551   }
<a name="l00552"></a>00552 
<a name="l00553"></a>00553 
<a name="l00555"></a>00555   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00556"></a><a class="code" href="namespace_seldon.php#abd6c2488767c3d88ddc759551327b82e">00556</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a3e269a2dbcd9a3d99011cee07da8c230" title="Assembles a sparse vector.">Assemble</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage1, Allocator1&gt;</a>&amp; Node)
<a name="l00557"></a>00557   {
<a name="l00558"></a>00558     <span class="keywordtype">int</span> nb = Node.GetM();
<a name="l00559"></a>00559     <a class="code" href="namespace_seldon.php#a3e269a2dbcd9a3d99011cee07da8c230" title="Assembles a sparse vector.">Assemble</a>(nb, Node);
<a name="l00560"></a>00560     Node.Resize(nb);
<a name="l00561"></a>00561   }
<a name="l00562"></a>00562 
<a name="l00563"></a>00563 
<a name="l00565"></a>00565 
<a name="l00568"></a>00568   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00569"></a>00569            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00570"></a><a class="code" href="namespace_seldon.php#a114b40f8110b98af37b448d75fe8608d">00570</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a114b40f8110b98af37b448d75fe8608d" title="Sorts and removes duplicate entries of a vector.">RemoveDuplicate</a>(<span class="keywordtype">int</span>&amp; n, <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage1, Allocator1&gt;</a>&amp; Node,
<a name="l00571"></a>00571                        <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; Node2)
<a name="l00572"></a>00572   {
<a name="l00573"></a>00573     <span class="keywordflow">if</span> (n &lt;= 1)
<a name="l00574"></a>00574       <span class="keywordflow">return</span>;
<a name="l00575"></a>00575 
<a name="l00576"></a>00576     <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(n, Node, Node2);
<a name="l00577"></a>00577     T prec = Node(0);
<a name="l00578"></a>00578     <span class="keywordtype">int</span> nb = 1;
<a name="l00579"></a>00579     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 1; i &lt; n; i++)
<a name="l00580"></a>00580       <span class="keywordflow">if</span> (Node(i) != prec)
<a name="l00581"></a>00581         {
<a name="l00582"></a>00582           Node(nb) = Node(i);
<a name="l00583"></a>00583           Node2(nb) = Node2(i);
<a name="l00584"></a>00584           prec = Node(nb);
<a name="l00585"></a>00585           nb++;
<a name="l00586"></a>00586         }
<a name="l00587"></a>00587     n = nb;
<a name="l00588"></a>00588   }
<a name="l00589"></a>00589 
<a name="l00590"></a>00590 
<a name="l00592"></a>00592   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00593"></a><a class="code" href="namespace_seldon.php#a3ab11273249276e3ba2aeb4b7a1e2f2a">00593</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a114b40f8110b98af37b448d75fe8608d" title="Sorts and removes duplicate entries of a vector.">RemoveDuplicate</a>(<span class="keywordtype">int</span>&amp; n, <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage1, Allocator1&gt;</a>&amp; Node)
<a name="l00594"></a>00594   {
<a name="l00595"></a>00595     <a class="code" href="namespace_seldon.php#a3e269a2dbcd9a3d99011cee07da8c230" title="Assembles a sparse vector.">Assemble</a>(n, Node);
<a name="l00596"></a>00596   }
<a name="l00597"></a>00597 
<a name="l00598"></a>00598 
<a name="l00600"></a>00600 
<a name="l00603"></a>00603   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00604"></a>00604            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00605"></a><a class="code" href="namespace_seldon.php#ad4ea82f2eaa662276f387ddf03480686">00605</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a114b40f8110b98af37b448d75fe8608d" title="Sorts and removes duplicate entries of a vector.">RemoveDuplicate</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage1, Allocator1&gt;</a>&amp; Node,
<a name="l00606"></a>00606                        <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; Node2)
<a name="l00607"></a>00607   {
<a name="l00608"></a>00608     <span class="keywordtype">int</span> n = Node.GetM();
<a name="l00609"></a>00609     <span class="keywordflow">if</span> (n &lt;= 1)
<a name="l00610"></a>00610       <span class="keywordflow">return</span>;
<a name="l00611"></a>00611 
<a name="l00612"></a>00612     <a class="code" href="namespace_seldon.php#a114b40f8110b98af37b448d75fe8608d" title="Sorts and removes duplicate entries of a vector.">RemoveDuplicate</a>(n, Node, Node2);
<a name="l00613"></a>00613     Node.Resize(n);
<a name="l00614"></a>00614     Node2.Resize(n);
<a name="l00615"></a>00615   }
<a name="l00616"></a>00616 
<a name="l00617"></a>00617 
<a name="l00619"></a>00619   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00620"></a><a class="code" href="namespace_seldon.php#ae90dd7bc55cbefc7e9e35ce9b109a55f">00620</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a114b40f8110b98af37b448d75fe8608d" title="Sorts and removes duplicate entries of a vector.">RemoveDuplicate</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage1, Allocator1&gt;</a>&amp; Node)
<a name="l00621"></a>00621   {
<a name="l00622"></a>00622     <span class="keywordtype">int</span> n = Node.GetM();
<a name="l00623"></a>00623     <span class="keywordflow">if</span> (n &lt;= 1)
<a name="l00624"></a>00624       <span class="keywordflow">return</span>;
<a name="l00625"></a>00625 
<a name="l00626"></a>00626     <a class="code" href="namespace_seldon.php#a3e269a2dbcd9a3d99011cee07da8c230" title="Assembles a sparse vector.">Assemble</a>(n, Node);
<a name="l00627"></a>00627     Node.Resize(n);
<a name="l00628"></a>00628   }
<a name="l00629"></a>00629 
<a name="l00630"></a>00630 
<a name="l00632"></a>00632   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00633"></a><a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5">00633</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n, <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage, Allocator&gt;</a>&amp; V)
<a name="l00634"></a>00634   {
<a name="l00635"></a>00635     <a class="code" href="namespace_seldon.php#a310936614a04226eed19b4ed08391b8d" title="Vector tab1 is sorted by using MergeSort algorithm.">MergeSort</a>(m, n, V);
<a name="l00636"></a>00636   }
<a name="l00637"></a>00637 
<a name="l00638"></a>00638 
<a name="l00640"></a>00640 
<a name="l00643"></a>00643   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00644"></a>00644            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00645"></a><a class="code" href="namespace_seldon.php#a2622c3b9f060d438f19f58fc9780bab9">00645</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n, <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; V,
<a name="l00646"></a>00646             <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; V2)
<a name="l00647"></a>00647   {
<a name="l00648"></a>00648     <a class="code" href="namespace_seldon.php#a310936614a04226eed19b4ed08391b8d" title="Vector tab1 is sorted by using MergeSort algorithm.">MergeSort</a>(m, n, V, V2);
<a name="l00649"></a>00649   }
<a name="l00650"></a>00650 
<a name="l00651"></a>00651 
<a name="l00653"></a>00653 
<a name="l00656"></a>00656   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00657"></a>00657            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00658"></a>00658            <span class="keyword">class </span>T3, <span class="keyword">class </span>Storage3, <span class="keyword">class </span>Allocator3&gt;
<a name="l00659"></a><a class="code" href="namespace_seldon.php#a7befc2b024ac2856e06a5e19ddb5b56c">00659</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n, <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; V,
<a name="l00660"></a>00660             <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; V2,
<a name="l00661"></a>00661             <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T3, Storage3, Allocator3&gt;</a>&amp; V3)
<a name="l00662"></a>00662   {
<a name="l00663"></a>00663     <a class="code" href="namespace_seldon.php#a310936614a04226eed19b4ed08391b8d" title="Vector tab1 is sorted by using MergeSort algorithm.">MergeSort</a>(m, n, V, V2, V3);
<a name="l00664"></a>00664   }
<a name="l00665"></a>00665 
<a name="l00666"></a>00666 
<a name="l00668"></a>00668   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00669"></a><a class="code" href="namespace_seldon.php#a8b40b6a71ebc57900da6669c6be02861">00669</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(<span class="keywordtype">int</span> n, <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage, Allocator&gt;</a>&amp; V)
<a name="l00670"></a>00670   {
<a name="l00671"></a>00671     <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(0, n - 1, V);
<a name="l00672"></a>00672   }
<a name="l00673"></a>00673 
<a name="l00674"></a>00674 
<a name="l00676"></a>00676 
<a name="l00679"></a>00679   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00680"></a>00680            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00681"></a><a class="code" href="namespace_seldon.php#ac91b947010f21679a184f3e21a40f666">00681</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(<span class="keywordtype">int</span> n, <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; V,
<a name="l00682"></a>00682             <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; V2)
<a name="l00683"></a>00683   {
<a name="l00684"></a>00684     <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(0, n - 1, V, V2);
<a name="l00685"></a>00685   }
<a name="l00686"></a>00686 
<a name="l00687"></a>00687 
<a name="l00689"></a>00689 
<a name="l00692"></a>00692   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00693"></a>00693            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00694"></a>00694            <span class="keyword">class </span>T3, <span class="keyword">class </span>Storage3, <span class="keyword">class </span>Allocator3&gt;
<a name="l00695"></a><a class="code" href="namespace_seldon.php#aba061b11a3646614d19b45126af2e9b8">00695</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(<span class="keywordtype">int</span> n, <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; V,
<a name="l00696"></a>00696             <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; V2,
<a name="l00697"></a>00697             <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T3, Storage3, Allocator3&gt;</a>&amp; V3)
<a name="l00698"></a>00698   {
<a name="l00699"></a>00699     <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(0, n - 1, V, V2, V3);
<a name="l00700"></a>00700   }
<a name="l00701"></a>00701 
<a name="l00702"></a>00702 
<a name="l00704"></a>00704   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00705"></a><a class="code" href="namespace_seldon.php#a948ae2983248d2f8548cca1a438a9444">00705</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage, Allocator&gt;</a>&amp; V)
<a name="l00706"></a>00706   {
<a name="l00707"></a>00707     <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(0, V.GetM() - 1, V);
<a name="l00708"></a>00708   }
<a name="l00709"></a>00709 
<a name="l00710"></a>00710 
<a name="l00712"></a>00712 
<a name="l00715"></a>00715   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00716"></a>00716            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00717"></a><a class="code" href="namespace_seldon.php#a8eae909d3f1587c4b2712bc4cc158922">00717</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; V,
<a name="l00718"></a>00718             <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; V2)
<a name="l00719"></a>00719   {
<a name="l00720"></a>00720     <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(0, V.GetM() - 1, V, V2);
<a name="l00721"></a>00721   }
<a name="l00722"></a>00722 
<a name="l00723"></a>00723 
<a name="l00725"></a>00725 
<a name="l00728"></a>00728   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00729"></a>00729            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00730"></a>00730            <span class="keyword">class </span>T3, <span class="keyword">class </span>Storage3, <span class="keyword">class </span>Allocator3&gt;
<a name="l00731"></a><a class="code" href="namespace_seldon.php#aa055f15ba365f6f961ebf4d85e30352e">00731</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; V,
<a name="l00732"></a>00732             <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; V2,
<a name="l00733"></a>00733             <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T3, Storage3, Allocator3&gt;</a>&amp; V3)
<a name="l00734"></a>00734   {
<a name="l00735"></a>00735     <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(0, V.GetM() - 1, V, V2, V3);
<a name="l00736"></a>00736   }
<a name="l00737"></a>00737 
<a name="l00738"></a>00738 
<a name="l00739"></a>00739   <span class="comment">//  SORT  //</span>
<a name="l00741"></a>00741 <span class="comment"></span>
<a name="l00742"></a>00742 
<a name="l00743"></a>00743 } <span class="comment">// namespace Seldon</span>
<a name="l00744"></a>00744 
<a name="l00745"></a>00745 <span class="preprocessor">#define SELDON_FILE_FUNCTIONS_ARRAYS_CXX</span>
<a name="l00746"></a>00746 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
