<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Python interface </h1>  </div>
</div>
<div class="contents">
<p>Seldon comes with a Python interface generated by <a href="http://www.swig.org/">Swig</a>. In Seldon 5.0, the interface is limited: it only contains one vector structure (full vector, double precision) and one matrix structure (full row-major matrix, double precision). Extending the interface is easy, so a more complete interface is planned for version 5.1.</p>
<p>This page only addresses the compilation and the use of the interface under Linux. The generation of the interface was not tested on another platform yet. No known issue should prevent the interface from running successfully on another platform.</p>
<h2>Compiling the interface</h2>
<p>In addition to a C++ compiler, one needs Swig 1.3.x. Swig 1.1 cannot generate the interface. You will also need Python (say, 2.5 or 2.6, but previous versions should work too) and its headers.</p>
<p>In Seldon directory (root), you may simply launch <code>scons</code> if you have <a href="http://www.scons.org/">SCons</a> installed (version &gt;=1.0). You may also execute lines similar to those launched by SCons:</p>
<div class="fragment"><pre class="fragment">
$ g++ -o Seldon.os -c -fPIC -DSELDON_DEBUG_LEVEL_4 \
  -I/usr/include/python2.6 Seldon.cpp
$ swig -o seldon_wrap.cc -Wall -c++ -python seldon.i
$ g++ -o seldon_wrap.os -c -fPIC -DSELDON_DEBUG_LEVEL_4 \
  -I/usr/include/python2.6 seldon_wrap.cc
$ g++ -o _seldon.so -shared Seldon.os seldon_wrap.os
</pre></div><p>This should generate the Python module <code>seldon.py</code> and the shared library <code>_seldon.so</code>. You may want to place these two files in a directory of your <code>$PYTHONPATH</code>, where Python searches for modules (in addition to the local directory <code>.</code>).</p>
<h2>Using the interface</h2>
<p>In Seldon directory (root), or in any place if <code>seldon.py</code> and <code>_seldon.so</code> are in a directory of <code>$PYTHONPATH</code>, you may launch Python and load the module.</p>
<p>It is recommended to use <a href="http://ipython.scipy.org/">IPython</a> instead of the regular Python shell, for convenience:</p>
<div class="fragment"><pre class="fragment">
$ ls
SConstruct      Seldon.hxx  SeldonHeader.hxx  _seldon.so  computation/
Seldon.cpp      Seldon.os   SeldonSolver.hxx  array3d/    license
matrix/         seldon.i    seldon_wrap.cc    share/      vector/
matrix_sparse/  seldon.py   seldon_wrap.os    test/       version
$ ipython
Python 2.6.2 (release26-maint, Apr 19 2009, 01:58:18)
Type "copyright", "credits" or "license" for more information.

IPython 0.9.1 -- An enhanced Interactive Python.
?         -&gt; Introduction and overview of IPython's features.
%quickref -&gt; Quick reference.
help      -&gt; Python's own help system.
object?   -&gt; Details about 'object'. ?object also works, ?? prints more.

In [1]: import seldon

In [2]:
</pre></div><p>The module mainly provides <code>VectorDouble</code> and <code>MatrixDouble</code>. The former is the same as <code>Vector&lt;double, VectFull&gt;</code> and the latter is the same as <code>Matrix&lt;double, General, RowMajor&gt;</code>. Essentially all methods of these two classes are available. Example with <code>VectorDouble</code>:</p>
<div class="fragment"><pre class="fragment">
In [1]: import seldon

In [2]: x = seldon.VectorDouble(5)

In [3]: x.Fill()

In [4]: x.Print()
0       1       2       3       4

In [5]: x.Reallocate(2)

In [6]: x.Print()
0       1
</pre></div><p>Example with <code>MatrixDouble</code>:</p>
<div class="fragment"><pre class="fragment">
In [1]: from seldon import *

In [2]: M = MatrixDouble(3, 3)

In [3]: M.SetIdentity()

In [4]: M.Print()
1       0       0
0       1       0
0       0       1

In [5]: M.GetM()
Out[5]: 3

In [6]: M.GetM() * M.GetN()
Out[6]: 9
</pre></div><p>The main difference lies in the access operator: it is <code>[]</code> instead of <code>()</code>, to be consistent with <a href="http://numpy.scipy.org/">NumPy</a> conventions:</p>
<div class="fragment"><pre class="fragment">
In [1]: import seldon

In [2]: x = seldon.VectorDouble(3)

In [3]: x[0] = 2.; x[1] = -1.; x[2] = 10.

In [4]: x.Print()
2       -1      10

In [5]: M = seldon.MatrixDouble(3, 3)

In [6]: M.SetIdentity()

In [7]: M[1, 0] = -5

In [8]: M.Print()
1       0       0
-5      1       0
0       0       1

In [8]: M[2, 2] + x[0]
Out[9]: 3.0
</pre></div><p>C++ exceptions are converted to Python exceptions:</p>
<div class="fragment"><pre class="fragment">
In [2]: x = seldon.VectorDouble(3)

In [3]: x[3] = 2.
---------------------------------------------------------------------------
Exception                                 Traceback (most recent call last)

/home/mallet/src/seldon/&lt;ipython console&gt; in &lt;module&gt;()

/home/mallet/src/seldon/seldon.pyc in __setitem__(*args)
   1091     def Read(*args): return _seldon.VectorDouble_Read(*args)
   1092     def __getitem__(*args): return _seldon.VectorDouble___getitem__(*args)
-&gt; 1093     def __setitem__(*args): return _seldon.VectorDouble___setitem__(*args)
   1094     def __len__(*args): return _seldon.VectorDouble___len__(*args)
   1095 VectorDouble_swigregister = _seldon.VectorDouble_swigregister

Exception: ERROR!
Index out of range in Vector&lt;VectFull&gt;::operator().
   Index should be in [0, 2], but is equal to 3.
</pre></div><p>You can convert a vector or a matrix to a <a href="http://numpy.scipy.org/">NumPy</a> array:</p>
<div class="fragment"><pre class="fragment">
In [1]: import seldon

In [2]: import numpy

In [3]: M = seldon.MatrixDouble(3, 3)

In [4]: M.Fill()

In [5]: M = numpy.array(M)

In [6]: M.shape
Out[6]: (3, 3)

In [7]: M
Out[7]:
array([[ 0.,  1.,  2.],
       [ 3.,  4.,  5.],
       [ 6.,  7.,  8.]])

In [8]: M.sum()
Out[8]: 36.0
</pre></div> </div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
