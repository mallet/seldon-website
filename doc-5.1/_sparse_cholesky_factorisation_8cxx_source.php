<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/solver/SparseCholeskyFactorisation.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2010 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_SPARSE_CHOLESKY_FACTORIZATION_CXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 <span class="keyword">namespace </span>Seldon
<a name="l00023"></a>00023 {
<a name="l00024"></a>00024 
<a name="l00026"></a>00026 
<a name="l00031"></a>00031   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00032"></a><a class="code" href="namespace_seldon.php#aff5fde720c13624bb56ea87fa1d958f5">00032</a>   <span class="keywordtype">void</span> GetCholesky(<a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php" title="Row-major symmetric sparse-matrix class.">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;</a>&amp; A)
<a name="l00033"></a>00033   {
<a name="l00034"></a>00034     <span class="keywordtype">int</span> n = A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3ae65d56c548233051a15054643f6753" title="Returns the number of columns.">GetN</a>();
<a name="l00035"></a>00035     T t, s, fact;
<a name="l00036"></a>00036     <span class="keywordtype">int</span> j_col, jrow, index_lu, jpos;
<a name="l00037"></a>00037     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> Row_Val(n);
<a name="l00038"></a>00038     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> Index(n), Row_Ind(n);
<a name="l00039"></a>00039     Row_Val.Fill(0);
<a name="l00040"></a>00040     Row_Ind.Fill(-1);
<a name="l00041"></a>00041 
<a name="l00042"></a>00042     Index.Fill(-1);
<a name="l00043"></a>00043 
<a name="l00044"></a>00044     <span class="comment">// Conversion to unsymmetric matrix.</span>
<a name="l00045"></a>00045     <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, General, ArrayRowSparse, Allocator&gt;</a> B;
<a name="l00046"></a>00046     Copy(A, B);
<a name="l00047"></a>00047 
<a name="l00048"></a>00048     <span class="comment">// A is cleared.</span>
<a name="l00049"></a>00049     A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a61227beea7612dd22bf1f46546bb5cde" title="Clears the matrix.">Clear</a>();
<a name="l00050"></a>00050     A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#afcd0d4612b325249c24407627b3dd568" title="Reallocates memory to resize the matrix.">Reallocate</a>(n, n);
<a name="l00051"></a>00051 
<a name="l00052"></a>00052     <span class="comment">// Main loop over rows.</span>
<a name="l00053"></a>00053     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i_row = 0; i_row &lt; n; i_row++)
<a name="l00054"></a>00054       {
<a name="l00055"></a>00055         <span class="keywordtype">int</span> size_row = B.GetRowSize(i_row);
<a name="l00056"></a>00056 
<a name="l00057"></a>00057         <span class="comment">// we are separating lower from upper part</span>
<a name="l00058"></a>00058         <span class="keywordtype">int</span> length_lower = 0, length_upper = 1;
<a name="l00059"></a>00059         Row_Ind(i_row) = i_row;
<a name="l00060"></a>00060         Row_Val(i_row) = 0.0;
<a name="l00061"></a>00061         Index(i_row) = i_row;
<a name="l00062"></a>00062 
<a name="l00063"></a>00063         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; size_row; j++)
<a name="l00064"></a>00064           {
<a name="l00065"></a>00065             <span class="keywordtype">int</span> k = B.Index(i_row, j);
<a name="l00066"></a>00066             t = B.Value(i_row, j);
<a name="l00067"></a>00067             <span class="keywordflow">if</span> (k &lt; i_row)
<a name="l00068"></a>00068               {
<a name="l00069"></a>00069                 Row_Ind(length_lower) = k;
<a name="l00070"></a>00070                 Row_Val(length_lower) = t;
<a name="l00071"></a>00071                 Index(k) = length_lower;
<a name="l00072"></a>00072                 length_lower++;
<a name="l00073"></a>00073               }
<a name="l00074"></a>00074             <span class="keywordflow">else</span> <span class="keywordflow">if</span> (k == i_row)
<a name="l00075"></a>00075               Row_Val(i_row) = t;
<a name="l00076"></a>00076             <span class="keywordflow">else</span>
<a name="l00077"></a>00077               {
<a name="l00078"></a>00078                 jpos = i_row + length_upper;
<a name="l00079"></a>00079                 Row_Ind(jpos) = k;
<a name="l00080"></a>00080                 Row_Val(jpos) = t;
<a name="l00081"></a>00081                 Index(k) = jpos;
<a name="l00082"></a>00082                 length_upper++;
<a name="l00083"></a>00083               }
<a name="l00084"></a>00084           }
<a name="l00085"></a>00085 
<a name="l00086"></a>00086         B.ClearRow(i_row);
<a name="l00087"></a>00087 
<a name="l00088"></a>00088         j_col = 0;
<a name="l00089"></a>00089         <span class="keywordtype">int</span> length = 0;
<a name="l00090"></a>00090 
<a name="l00091"></a>00091         <span class="comment">// Previous rows are eliminated.</span>
<a name="l00092"></a>00092         <span class="keywordflow">while</span> (j_col &lt; length_lower)
<a name="l00093"></a>00093           {
<a name="l00094"></a>00094             jrow = Row_Ind(j_col);
<a name="l00095"></a>00095 
<a name="l00096"></a>00096             <span class="comment">// We search first element in lower part.</span>
<a name="l00097"></a>00097             <span class="keywordtype">int</span> k = j_col;
<a name="l00098"></a>00098             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = (j_col+1) ; j &lt; length_lower; j++)
<a name="l00099"></a>00099               {
<a name="l00100"></a>00100                 <span class="keywordflow">if</span> (Row_Ind(j) &lt; jrow)
<a name="l00101"></a>00101                   {
<a name="l00102"></a>00102                     jrow = Row_Ind(j);
<a name="l00103"></a>00103                     k = j;
<a name="l00104"></a>00104                   }
<a name="l00105"></a>00105               }
<a name="l00106"></a>00106 
<a name="l00107"></a>00107 
<a name="l00108"></a>00108             <span class="keywordflow">if</span> (k != j_col)
<a name="l00109"></a>00109               {
<a name="l00110"></a>00110                 <span class="comment">// If k different from j_col, we are exchanging positions.</span>
<a name="l00111"></a>00111                 <span class="keywordtype">int</span> j = Row_Ind(j_col);
<a name="l00112"></a>00112                 Row_Ind(j_col) = Row_Ind(k);
<a name="l00113"></a>00113                 Row_Ind(k) = j;
<a name="l00114"></a>00114 
<a name="l00115"></a>00115                 Index(jrow) = j_col;
<a name="l00116"></a>00116                 Index(j) = k;
<a name="l00117"></a>00117 
<a name="l00118"></a>00118                 s = Row_Val(j_col);
<a name="l00119"></a>00119                 Row_Val(j_col) = Row_Val(k);
<a name="l00120"></a>00120                 Row_Val(k) = s;
<a name="l00121"></a>00121               }
<a name="l00122"></a>00122 
<a name="l00123"></a>00123             <span class="comment">// Zero out element in row.</span>
<a name="l00124"></a>00124             Index(jrow) = -1;
<a name="l00125"></a>00125             fact = Row_Val(j_col) * A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">Value</a>(jrow, 0);
<a name="l00126"></a>00126 
<a name="l00127"></a>00127             <span class="comment">// Combines current row and row jrow.</span>
<a name="l00128"></a>00128             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 1; k &lt; A.<a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#af202c14cefd0cb751e4cf906ca5804e7" title="Returns the number of non-zero entries of a row.">GetRowSize</a>(jrow); k++)
<a name="l00129"></a>00129               {
<a name="l00130"></a>00130                 s = fact * A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">Value</a>(jrow, k);
<a name="l00131"></a>00131                 <span class="keywordtype">int</span> j = A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3534fb2c57f2640c9b8cd5a59e9da0e2" title="Returns column/row number of j-th non-zero value of row/column i.">Index</a>(jrow, k);
<a name="l00132"></a>00132 
<a name="l00133"></a>00133                 jpos = Index(j);
<a name="l00134"></a>00134                 <span class="keywordflow">if</span> (j &gt;= i_row)
<a name="l00135"></a>00135                   {
<a name="l00136"></a>00136                     <span class="comment">// Dealing with upper part.</span>
<a name="l00137"></a>00137                     <span class="keywordflow">if</span> (jpos == -1)
<a name="l00138"></a>00138                       {
<a name="l00139"></a>00139                         <span class="comment">// This is a fill-in element.</span>
<a name="l00140"></a>00140                         <span class="keywordtype">int</span> i = i_row + length_upper;
<a name="l00141"></a>00141                         Row_Ind(i) = j;
<a name="l00142"></a>00142                         Index(j) = i;
<a name="l00143"></a>00143                         Row_Val(i) = -s;
<a name="l00144"></a>00144                         length_upper++;
<a name="l00145"></a>00145                       }
<a name="l00146"></a>00146                     <span class="keywordflow">else</span>
<a name="l00147"></a>00147                       {
<a name="l00148"></a>00148                         <span class="comment">// This is not a fill-in element.</span>
<a name="l00149"></a>00149                         Row_Val(jpos) -= s;
<a name="l00150"></a>00150                       }
<a name="l00151"></a>00151                   }
<a name="l00152"></a>00152                 <span class="keywordflow">else</span>
<a name="l00153"></a>00153                   {
<a name="l00154"></a>00154                     <span class="comment">// Dealing  with lower part.</span>
<a name="l00155"></a>00155                     <span class="keywordflow">if</span> (jpos == -1)
<a name="l00156"></a>00156                       {
<a name="l00157"></a>00157                         <span class="comment">// This is a fill-in element.</span>
<a name="l00158"></a>00158                         Row_Ind(length_lower) = j;
<a name="l00159"></a>00159                         Index(j) = length_lower;
<a name="l00160"></a>00160                         Row_Val(length_lower) = -s;
<a name="l00161"></a>00161                         length_lower++;
<a name="l00162"></a>00162                       }
<a name="l00163"></a>00163                     <span class="keywordflow">else</span>
<a name="l00164"></a>00164                       {
<a name="l00165"></a>00165                         <span class="comment">// This is not a fill-in element.</span>
<a name="l00166"></a>00166                         Row_Val(jpos) -= s;
<a name="l00167"></a>00167                       }
<a name="l00168"></a>00168                   }
<a name="l00169"></a>00169               }
<a name="l00170"></a>00170 
<a name="l00171"></a>00171             <span class="comment">// Stores this pivot element</span>
<a name="l00172"></a>00172             <span class="comment">// (from left to right -- no danger of overlap</span>
<a name="l00173"></a>00173             <span class="comment">// with the working elements in L (pivots).</span>
<a name="l00174"></a>00174             Row_Val(length) = fact;
<a name="l00175"></a>00175             Row_Ind(length) = jrow;
<a name="l00176"></a>00176             ++length;
<a name="l00177"></a>00177             j_col++;
<a name="l00178"></a>00178           }
<a name="l00179"></a>00179 
<a name="l00180"></a>00180         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; length_upper; k++)
<a name="l00181"></a>00181           Index(Row_Ind(i_row + k)) = -1;
<a name="l00182"></a>00182 
<a name="l00183"></a>00183         <span class="comment">// Now we can store the uppert part of row.</span>
<a name="l00184"></a>00184         size_row = length_upper;
<a name="l00185"></a>00185         A.<a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a02d5086adf71d37c259c1a94a375bd57" title="Reallocates row i.">ReallocateRow</a>(i_row, length_upper);
<a name="l00186"></a>00186 
<a name="l00187"></a>00187         <span class="comment">// We store inverse of square root of diagonal element of u.</span>
<a name="l00188"></a>00188         <span class="keywordflow">if</span> (Row_Val(i_row) &lt; 0)
<a name="l00189"></a>00189           {
<a name="l00190"></a>00190             cout &lt;&lt; <span class="stringliteral">&quot;Error during Cholesky factorization &quot;</span> &lt;&lt; endl;
<a name="l00191"></a>00191             cout &lt;&lt; <span class="stringliteral">&quot;Matrix must be definite positive &quot;</span> &lt;&lt; endl;
<a name="l00192"></a>00192             cout &lt;&lt; <span class="stringliteral">&quot;but diagonal element of row &quot;</span> &lt;&lt; i_row
<a name="l00193"></a>00193                  &lt;&lt; <span class="stringliteral">&quot;is equal to &quot;</span> &lt;&lt; Row_Val(i_row) &lt;&lt; endl;
<a name="l00194"></a>00194 
<a name="l00195"></a>00195 <span class="preprocessor">#ifdef SELDON_WITH_ABORT</span>
<a name="l00196"></a>00196 <span class="preprocessor"></span>            abort();
<a name="l00197"></a>00197 <span class="preprocessor">#endif</span>
<a name="l00198"></a>00198 <span class="preprocessor"></span>          }
<a name="l00199"></a>00199 
<a name="l00200"></a>00200         A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">Value</a>(i_row, 0) = 1.0 / Row_Val(i_row);
<a name="l00201"></a>00201         A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3534fb2c57f2640c9b8cd5a59e9da0e2" title="Returns column/row number of j-th non-zero value of row/column i.">Index</a>(i_row, 0) = i_row;
<a name="l00202"></a>00202         index_lu = 1;
<a name="l00203"></a>00203 
<a name="l00204"></a>00204         <span class="comment">// and extra-diagonal terms</span>
<a name="l00205"></a>00205         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = i_row + 1; k &lt; i_row + length_upper; k++)
<a name="l00206"></a>00206           {
<a name="l00207"></a>00207             A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3534fb2c57f2640c9b8cd5a59e9da0e2" title="Returns column/row number of j-th non-zero value of row/column i.">Index</a>(i_row, index_lu) = Row_Ind(k);
<a name="l00208"></a>00208             A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">Value</a>(i_row, index_lu) = Row_Val(k);
<a name="l00209"></a>00209             index_lu++;
<a name="l00210"></a>00210           }
<a name="l00211"></a>00211       }
<a name="l00212"></a>00212 
<a name="l00213"></a>00213     <span class="comment">// Diagonal of A is replaced by its square root.</span>
<a name="l00214"></a>00214     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; n; i++)
<a name="l00215"></a>00215       {
<a name="l00216"></a>00216         A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">Value</a>(i, 0) = sqrt(A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">Value</a>(i,0));
<a name="l00217"></a>00217         <span class="comment">// and other elements multiplied by this value.</span>
<a name="l00218"></a>00218         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 1; k &lt; A.<a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#af202c14cefd0cb751e4cf906ca5804e7" title="Returns the number of non-zero entries of a row.">GetRowSize</a>(i); k++)
<a name="l00219"></a>00219           A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">Value</a>(i, k) *= A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">Value</a>(i, 0);
<a name="l00220"></a>00220       }
<a name="l00221"></a>00221   }
<a name="l00222"></a>00222 
<a name="l00223"></a>00223 
<a name="l00224"></a>00224   <span class="comment">// Resolution of L x = y.</span>
<a name="l00225"></a>00225   <span class="keyword">template</span>&lt;<span class="keyword">class </span>classTrans,
<a name="l00226"></a>00226            <span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Storage,
<a name="l00227"></a>00227            <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2&gt;
<a name="l00228"></a>00228   <span class="keywordtype">void</span> SolveCholesky(<span class="keyword">const</span> classTrans&amp; TransA,
<a name="l00229"></a>00229                      <span class="keyword">const</span> Matrix&lt;T0, Prop, ArrayRowSymSparse, Allocator1&gt;&amp; A,
<a name="l00230"></a>00230                      Vector&lt;T1, Storage, Allocator2&gt;&amp; x)
<a name="l00231"></a>00231   {
<a name="l00232"></a>00232     <span class="keywordtype">int</span> n = A.GetM();
<a name="l00233"></a>00233     <span class="keywordflow">if</span> (n &lt;= 0)
<a name="l00234"></a>00234       <span class="keywordflow">return</span>;
<a name="l00235"></a>00235 
<a name="l00236"></a>00236     <span class="keywordflow">if</span> (TransA.Trans())
<a name="l00237"></a>00237       {
<a name="l00238"></a>00238         <span class="comment">// We solve L^T x = x</span>
<a name="l00239"></a>00239         <span class="keywordtype">int</span> j;
<a name="l00240"></a>00240         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = n - 1; i &gt;= 0; i--)
<a name="l00241"></a>00241           {
<a name="l00242"></a>00242             T1 val = x(i);
<a name="l00243"></a>00243             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 1; k &lt; A.GetRowSize(i) ; k++)
<a name="l00244"></a>00244               {
<a name="l00245"></a>00245                 j = A.Index(i, k);
<a name="l00246"></a>00246                 val -= A.Value(i, k) * x(j);
<a name="l00247"></a>00247               }
<a name="l00248"></a>00248 
<a name="l00249"></a>00249             x(i) = val * A.Value(i, 0);
<a name="l00250"></a>00250           }
<a name="l00251"></a>00251       }
<a name="l00252"></a>00252     <span class="keywordflow">else</span>
<a name="l00253"></a>00253       {
<a name="l00254"></a>00254         <span class="comment">// We solve L x = x</span>
<a name="l00255"></a>00255         <span class="keywordtype">int</span> j;
<a name="l00256"></a>00256         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; n; i++)
<a name="l00257"></a>00257           {
<a name="l00258"></a>00258             x(i) *= A.Value(i, 0);
<a name="l00259"></a>00259             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 1; k &lt; A.GetRowSize(i) ; k++)
<a name="l00260"></a>00260               {
<a name="l00261"></a>00261                 j = A.Index(i, k);
<a name="l00262"></a>00262                 x(j) -= A.Value(i, k) * x(i);
<a name="l00263"></a>00263               }
<a name="l00264"></a>00264           }
<a name="l00265"></a>00265       }
<a name="l00266"></a>00266   }
<a name="l00267"></a>00267 
<a name="l00268"></a>00268 
<a name="l00269"></a>00269   <span class="comment">// Computation of y = L x.</span>
<a name="l00270"></a>00270   <span class="keyword">template</span>&lt;<span class="keyword">class </span>classTrans,
<a name="l00271"></a>00271            <span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Storage,
<a name="l00272"></a>00272            <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2&gt;
<a name="l00273"></a>00273   <span class="keywordtype">void</span> MltCholesky(<span class="keyword">const</span> classTrans&amp; TransA,
<a name="l00274"></a>00274                    <span class="keyword">const</span> Matrix&lt;T0, Prop, ArrayRowSymSparse, Allocator1&gt;&amp; A,
<a name="l00275"></a>00275                    Vector&lt;T1, Storage, Allocator2&gt;&amp; x)
<a name="l00276"></a>00276   {
<a name="l00277"></a>00277     <span class="keywordtype">int</span> n = A.GetM();
<a name="l00278"></a>00278     <span class="keywordflow">if</span> (n &lt;= 0)
<a name="l00279"></a>00279       <span class="keywordflow">return</span>;
<a name="l00280"></a>00280 
<a name="l00281"></a>00281     <span class="keywordflow">if</span> (TransA.Trans())
<a name="l00282"></a>00282       {
<a name="l00283"></a>00283         <span class="comment">// We overwrite x by L^T x</span>
<a name="l00284"></a>00284         <span class="keywordtype">int</span> j;
<a name="l00285"></a>00285         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; n; i++)
<a name="l00286"></a>00286           {
<a name="l00287"></a>00287             T1 val = x(i) / A.Value(i, 0);
<a name="l00288"></a>00288             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 1; k &lt; A.GetRowSize(i) ; k++)
<a name="l00289"></a>00289               {
<a name="l00290"></a>00290                 j = A.Index(i, k);
<a name="l00291"></a>00291                 val += A.Value(i, k)*x(j);
<a name="l00292"></a>00292               }
<a name="l00293"></a>00293 
<a name="l00294"></a>00294             x(i) = val;
<a name="l00295"></a>00295           }
<a name="l00296"></a>00296       }
<a name="l00297"></a>00297     <span class="keywordflow">else</span>
<a name="l00298"></a>00298       {
<a name="l00299"></a>00299         <span class="comment">// We overwrite x with L x</span>
<a name="l00300"></a>00300         <span class="keywordtype">int</span> j;
<a name="l00301"></a>00301         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = n - 1; i &gt;= 0; i--)
<a name="l00302"></a>00302           {
<a name="l00303"></a>00303             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 1; k &lt; A.GetRowSize(i) ; k++)
<a name="l00304"></a>00304               {
<a name="l00305"></a>00305                 j = A.Index(i, k);
<a name="l00306"></a>00306                 x(j) += A.Value(i, k)*x(i);
<a name="l00307"></a>00307               }
<a name="l00308"></a>00308 
<a name="l00309"></a>00309             x(i) /= A.Value(i, 0);
<a name="l00310"></a>00310           }
<a name="l00311"></a>00311       }
<a name="l00312"></a>00312   }
<a name="l00313"></a>00313 
<a name="l00314"></a>00314 }
<a name="l00315"></a>00315 
<a name="l00316"></a>00316 <span class="preprocessor">#define SELDON_FILE_SPARSE_CHOLESKY_FACTORIZATION_CXX</span>
<a name="l00317"></a>00317 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
