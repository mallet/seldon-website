<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Vector&lt; T, VectSparse, Allocator &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>, including all inherited members.<table>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a34201f29286c8829e7e8a7e31a266d5b">AddInteraction</a>(int i, const T &amp;val)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a1a60608da32cc0da4be4a1acdbbebc06">AddInteractionRow</a>(int, int *, T *, bool already_sorted=false)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a4625cf1134f2121385cbee020c7fe834">AddInteractionRow</a>(int nb, Vector&lt; int &gt; col, Vector&lt; T, VectFull, Allocator0 &gt; val, bool already_sorted=false)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ab3773a41209f76c5739be8f7f037006c">Append</a>(const T &amp;x)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a3f4b6ec8424c938251af5e4b1b130702">Assemble</a>()</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a548fa99edadcd9b418912d607b7aa19b">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#aefe82d8a7c33e932c20c9fd9e6f499f5">Copy</a>(const Vector&lt; T, VectSparse, Allocator &gt; &amp;X)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a790a38cb578627bf17291088f2cd2388">Seldon::Vector&lt; T, VectFull, Allocator &gt;::Copy</a>(const Vector&lt; T, VectFull, Allocator &gt; &amp;X)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>data_</b> (defined in <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a722046c309ee8e50abd68a5a5a77aa60">Fill</a>()</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a2c103fe432a6f6a9c37cb133bb837153">Fill</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a71147cebc8bd36cc103d2e6fd3767f91">FillRand</a>()</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a">GetData</a>() const </td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a454aea78c82c4317dfe4160cc7c95afa">GetDataConst</a>() const </td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a0aadc006977de037eb3ee550683e3f19">GetDataConstVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a3906b8279cc318dc0a8a672fa781095a">GetDataSize</a>()</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#ad0f5184bd9eec6fca70c54e459ced78f">GetDataVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a6a4ed85be713e5a300f94eca786a2c2b">GetIndex</a>() const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a075daae3607a4767a77a3de60ab30ba7">GetLength</a>() const </td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae">GetM</a>() const </td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a165cbeeddb4a2a356accc00d609b4211">GetNormInf</a>() const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a50186150708e90f86ded59e33cb172a6">GetNormInfIndex</a>() const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a839880a3113c1885ead70d79fade0294">GetSize</a>() const </td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a72d964fb6cbd4e4189473e0298989929">Index</a>(int i)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a3f0337de5aaabd69cb32432377b5e4d3">Index</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>index_allocator_</b> (defined in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td><code> [protected, static]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>m_</b> (defined in <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a985afdfd20e655bb551cf44669acd0e6">Nullify</a>()</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#afd992c6ba8e53a2c556459388fc42293">operator()</a>(int i)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a001a7d1629309ad90c6d66d695a69e8f">operator()</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a236fd63d659f7d92f3b12da557380cb3">operator*=</a>(const T0 &amp;X)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a87ef82c6607c68b77a5ec18dbcb15eb0">operator=</a>(const Vector&lt; T, VectSparse, Allocator &gt; &amp;X)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a1d0196721350369a3154b60dfa105b37">operator=</a>(const T0 &amp;X)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#abd12db9e14f5ba1c417d4c587e6d359a">Seldon::Vector&lt; T, VectFull, Allocator &gt;::operator=</a>(const Vector&lt; T, VectFull, Allocator &gt; &amp;X)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a8c5f87f1d06974d31d8ebe8dcc0fcb8e">Print</a>() const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a6a64b54653758dbbdcd650c8ffba4d52">PushBack</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#aa0b88a0415692922c36c2b2f284efeca">PushBack</a>(const Vector&lt; T, VectFull, Allocator0 &gt; &amp;X)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#adc80b24a206e7ef91dd9b51aa33763d0">Read</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a4335b7afce7863794f8ffbf76db91cbd">Read</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a2ce4a8d2726b069e465d4156b1b531be">Seldon::Vector&lt; T, VectFull, Allocator &gt;::Read</a>(string FileName, bool with_size=true)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a634f8993354070451e53fd0c29bffa49">Seldon::Vector&lt; T, VectFull, Allocator &gt;::Read</a>(istream &amp;FileStream, bool with_size=true)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a3b9e7d2529315dfaa75a09d3c34da18a">ReadText</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a554c1d3659fc50f3e119ef14437ad54a">ReadText</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#afcc8fef4210e7703493ad95acf389d19">Reallocate</a>(int i)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a6ff2b64975624f059a305a7661c24573">RemoveSmallEntry</a>(const T0 &amp;epsilon)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a3637911015d5a0fafa4811ee2204bbc5">Resize</a>(int i)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a73034fa2fb67eec70a2d25684f2a0c8c">SetData</a>(int nz, T *data, int *index)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#af6c784522606f8bf451b129a8b5069d1">SetData</a>(Vector&lt; T, VectFull, Allocator2 &gt; &amp;data, Vector&lt; int &gt; &amp;index)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#ab4c128e288cce25e0e8f1998ccf38a1c">SetData</a>(const Vector&lt; T, VectSparse, Allocator2 &gt; &amp;V)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SetData</b>(int i, pointer data) (defined in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ab12cedc7bee632a7b210312f1e7ef28b">Seldon::Vector&lt; T, VectFull, Allocator &gt;::SetData</a>(const Vector&lt; T, VectFull, Allocator0 &gt; &amp;V)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>storage</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a5805ff818cc9c57a65bc6006c58564a7">Val</a>(int i)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a21f192e6ea09ce5be473e6bb1135c7a9">Val</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a51c6d9be9bfc1234e17d997f9bcdff07">Value</a>(int i)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#ad733400e0cde2e94b5efd076dc5d956e">Value</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>value_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>vect_allocator_</b> (defined in <a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td><code> [protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#aee3f9a5bfd70e6370ae0c559257624f7">Vector</a>()</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td><code> [explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#aa33cfa4d38f149671d6d0ea82f5106d8">Vector</a>(int i)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td><code> [explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a95b6b2c8c2be95a9d5dcdbd892f195e2">Vector</a>(const Vector&lt; T, VectSparse, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>Vector</b>(int i, pointer data) (defined in <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a1afba3b1f5b3d8022dd4e047185968d8">Seldon::Vector&lt; T, VectFull, Allocator &gt;::Vector</a>(const Vector&lt; T, VectFull, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#ac8a6cee506f094504138943629b9dcbb">Vector_Base</a>()</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#aa5d9ca198d4175b9baf9a0543f7376be">Vector_Base</a>(int i)</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td><code> [inline, explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a27796125b93f3a771a5c92e8196c9229">Vector_Base</a>(const Vector_Base&lt; T, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a246553d78195c2e1c51be7a4e84409c8">Write</a>(string FileName) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a27f19795d0cf6489ab076a6c43ef6756">Write</a>(ostream &amp;FileStream) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a51ea9d8b0a3176fc903c0279ae8770d0">Seldon::Vector&lt; T, VectFull, Allocator &gt;::Write</a>(string FileName, bool with_size=true) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a1a098ef03b3d8cad79b659cd05be220e">Seldon::Vector&lt; T, VectFull, Allocator &gt;::Write</a>(ostream &amp;FileStream, bool with_size=true) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#ab770c6961b3c24de5679fc93530e6e80">WriteText</a>(string FileName) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a71b2e3e54ed55920f6b34912382fac80">WriteText</a>(ostream &amp;FileStream) const </td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ae8e4d6b081a6f4c90b2993b047a04dac">Zero</a>()</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#af0eb42557b7578cfefabbf4d15ad2073">~Vector</a>()</td><td><a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector___base.php#a6da510796ebef867765ea3a037debb1b">~Vector_Base</a>()</td><td><a class="el" href="class_seldon_1_1_vector___base.php">Seldon::Vector_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
