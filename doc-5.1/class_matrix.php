<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Dense Matrices </h1>  </div>
</div>
<div class="contents">
<p>In that page, methods and functions related to dense matrices are detailed. </p>
<h2>Basic declaration :</h2>
<h4>Classes :</h4>
<p>Matrix&lt;T,Prop,RowMajor&gt; <br/>
 Matrix&lt;T,Prop,RowSymPacked&gt; <br/>
 Matrix&lt;T,Prop,RowHermPacked&gt; <br/>
 Matrix&lt;T,Prop,RowUpTriangPacked&gt;<br/>
 Matrix&lt;T,Prop,RowLoTriangPacked&gt;</p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// dense matrix of doubles
Matrix&lt;double&gt; A;

// dense symmetric matrix
Matrix&lt;float, Symmetric, RowSymPacked&gt; B;

// dense hermitian matrix
Matrix&lt;double, General, RowHermPacked&gt; &gt; C;

// dense lower triangular matrix
Matrix&lt;double, General, RowLoTriangPacked&gt; &gt; D;

// dense upper triangular matrix
Matrix&lt;double, General, RowUpTriangPacked&gt; &gt; E;
</pre></div>  </pre><h2>Methods :</h2>
<table  class="category-table">
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#constructor">Matrix constructors </a>   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#operator">Matrix operators </a>   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getm">GetM </a>  </td><td class="category-table-td">returns the number of rows in the matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getn">GetN </a>  </td><td class="category-table-td">returns the number of columns in the matrix   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getsize">GetSize </a>  </td><td class="category-table-td">returns the number of elements in the matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getdatasize">GetDataSize </a>  </td><td class="category-table-td">returns the number of elements effectively stored   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getdata">GetData </a>  </td><td class="category-table-td">returns a pointer to the array containing the values   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getdata">GetDataConst </a>  </td><td class="category-table-td">returns a pointer to the array containing the values   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getdata">GetDataVoid </a>  </td><td class="category-table-td">returns a pointer to the array containing the values   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getdata">GetDataConstVoid </a>  </td><td class="category-table-td">returns a pointer to the array containing the values   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#clear">Clear </a>  </td><td class="category-table-td">removes all elements of the matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#reallocate">Reallocate </a>  </td><td class="category-table-td">changes the size of matrix (does not keep previous elements)   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#resize">Resize </a>  </td><td class="category-table-td">changes the size of matrix (keeps previous elements)   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#setdata">SetData </a>  </td><td class="category-table-td">sets the pointer to the array containing the values  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#nullify">Nullify </a>  </td><td class="category-table-td">clears the matrix without releasing memory  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#val">Val </a>  </td><td class="category-table-td">access to a matrix element  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#copy">Copy </a>  </td><td class="category-table-td">copies a matrix  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#zero">Zero </a>  </td><td class="category-table-td">sets all elements to zero   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#setidentity">SetIdentity </a>  </td><td class="category-table-td">sets matrix to identity matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#fill">Fill </a>  </td><td class="category-table-td">sets all elements to a given value   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#fillrand">FillRand </a>  </td><td class="category-table-td">fills randomly the matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#print">Print </a>  </td><td class="category-table-td">displays the matrix   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#write">Write </a>  </td><td class="category-table-td">writes the matrix in binary format   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#read">Read </a>  </td><td class="category-table-td">reads the matrix in binary format   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#writetext">WriteText </a>  </td><td class="category-table-td">writes the matrix in text format   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#readtext">ReadText </a>  </td><td class="category-table-td">reads the matrix in text format   </td></tr>
</table>
<h2>Functions :</h2>
<table  class="category-table">
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_blas.php#mlt">Mlt </a> </td><td class="category-table-td">multiplication by a scalar or matrix-vector product   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_blas.php#mltadd">MltAdd </a> </td><td class="category-table-td">performs a matrix-vector or matrix-matrix product   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_blas.php#add">Add </a> </td><td class="category-table-td">adds two matrices   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_blas.php#copy">Copy </a> </td><td class="category-table-td">copies a matrix into another one   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_blas.php#rank1update">Rank1Update </a> </td><td class="category-table-td">Adds a contribution X.Y' to a matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_blas.php#rank2update">Rank2Update </a> </td><td class="category-table-td">Adds a contribution X.Y' + Y.X' to a symmetric matrix   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_blas.php#solve">Solve </a> </td><td class="category-table-td">solves a triangular system   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_matrix.php#transpose">Transpose </a> </td><td class="category-table-td">replaces a matrix by its transpose   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_matrix.php#transpose">TransposeConj </a> </td><td class="category-table-td">replaces a matrix by its conjugate transpose   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_matrix.php#maxabs">MaxAbs </a> </td><td class="category-table-td">returns highest absolute value of A   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_matrix.php#norm1">Norm1 </a> </td><td class="category-table-td">returns 1-norm of A   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_matrix.php#norminf">NormInf </a> </td><td class="category-table-td">returns infinity-norm of A   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_matrix.php#getrow">GetRow </a> </td><td class="category-table-td">returns a matrix row   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_matrix.php#setrow">SetRow </a> </td><td class="category-table-td">changes a matrix row   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_matrix.php#getcol">GetCol </a> </td><td class="category-table-td">returns a matrix column   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_matrix.php#setcol">SetCol </a> </td><td class="category-table-td">changes a matrix column   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_lapack.php#getlu">GetLU</a> </td><td class="category-table-td">performs a LU (or LDL^t) factorization  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_lapack.php#getlu">SolveLU</a> </td><td class="category-table-td">solve linear system by using LU factorization  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_lapack.php#refinesolutionlu">RefineSolutionLU</a> </td><td class="category-table-td">improves solution computed by SolveLU  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_lapack.php#reciprocalconditionnumber">ReciprocalConditionNumber </a> </td><td class="category-table-td">computes the inverse of matrix condition number  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_lapack.php#getscalingfactors">GetScalingFactors </a> </td><td class="category-table-td">computes row and column scalings to equilibrate a matrix  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_lapack.php#getinverse">GetInverse </a> </td><td class="category-table-td">computes the matrix inverse   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_lapack.php#getqr">GetQR </a> </td><td class="category-table-td">QR factorization of matrix   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_lapack.php#getlq">GetLQ </a> </td><td class="category-table-td">LQ factorization of matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_lapack.php#getq_fromqr">GetQ_FromQR </a> </td><td class="category-table-td">Forms explicitely Q from QR factorization  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_lapack.php#mltq_fromqr">MltQ_FromQR </a> </td><td class="category-table-td">multiplies vector by Q   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_lapack.php#getqr">SolveQR </a> </td><td class="category-table-td">solves least-square problems by using QR factorization  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_lapack.php#getlq">SolveLQ </a> </td><td class="category-table-td">solves least-square problems by using LQ factorization   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_lapack.php#geteigenvalues">GetEigenvalues </a> </td><td class="category-table-td">computes eigenvalues  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_lapack.php#geteigenvalueseigenvec">GetEigenvaluesEigenvectors </a> </td><td class="category-table-td">computes eigenvalues and eigenvectors  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_lapack.php#getsvd">GetSVD </a> </td><td class="category-table-td">performs singular value decomposition (SVD)   </td></tr>
</table>
<div class="separator"><a class="anchor" id="constructor"></a></div><h3>Matrix constructors</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  Matrix();
  Matrix(int, int );
</pre><h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// default constructor -&gt; empty matrix
Matrix&lt;int&gt; V;
cout &lt;&lt; "Number of elements "&lt;&lt; V.GetSize() &lt;&lt; endl; // should return 0 
// then you can use Reallocate to set the number of rows and columns
V.Reallocate(3, 2);
V.Fill();

// we construct matrix with 4 rows and 3 columns
Matrix&lt;double&gt; W(4, 3);
// W is not initialized, you have to fill it
W.Fill(1.0);
</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="#reallocate">Reallocate</a><br/>
 <a href="#fill">Fill</a></p>
<h4>Location :</h4>
<p>Class Matrix_Pointers<br/>
 Class Matrix_Symmetric<br/>
 Class Matrix_SymPacked<br/>
 Class Matrix_Hermitian<br/>
 Class Matrix_HermPacked<br/>
 Class Matrix_Triangular<br/>
 Class Matrix_TriangPacked<br/>
 <a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a> <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a><br/>
 <a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a> <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a><br/>
 <a class="el" href="_matrix___sym_packed_8hxx_source.php">Matrix_SymPacked.hxx</a> <a class="el" href="_matrix___sym_packed_8cxx_source.php">Matrix_SymPacked.cxx</a><br/>
 <a class="el" href="_matrix___hermitian_8hxx_source.php">Matrix_Hermitian.hxx</a> <a class="el" href="_matrix___hermitian_8cxx_source.php">Matrix_Hermitian.cxx</a><br/>
 <a class="el" href="_matrix___herm_packed_8hxx_source.php">Matrix_HermPacked.hxx</a> <a class="el" href="_matrix___herm_packed_8cxx_source.php">Matrix_HermPacked.cxx</a><br/>
 <a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a> <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a><br/>
 <a class="el" href="_matrix___triang_packed_8hxx_source.php">Matrix_TriangPacked.hxx</a> <a class="el" href="_matrix___triang_packed_8cxx_source.php">Matrix_TriangPacked.cxx</a></p>
<div class="separator"><a class="anchor" id="operator"></a></div><h3>Matrix operators</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  const T&amp; operator (int i, int j) const;
  T&amp; operator (int i, int j);
  T&amp; operator [int i];
  const T&amp; operator [int i] const;
  Matrix&amp; operator =(const Matrix&amp; )
  Vector&amp; operator =(const T0&amp; alpha)
  Vector&amp; operator *=(const T0&amp; alpha)
</pre><p>The operator [] should not be used in normal use. The operator () can't be used to modify values of matrix for specific storages, e.g. RowSym, RowHerm, RowUpTriang.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; V(3, 3);
// use of operator () to modify matrix
V(0, 0) = 2.0;
V(1, 0) = V(0, 0) + 1.0;

// operator [] should be used with caution
V[3] = V[0] + 1.4;

Matrix&lt;double&gt; W;
// use of operator = to copy contents of vector V
W = V;

// set all elements to a given value
W = 1;

// multiplication by a scalar
Matrix&lt;double&gt; A(3, 2);
A.Fill();
A *= 1.5;
</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="#copy">Copy</a><br/>
 <a href="#val">Val</a></p>
<h4>Location :</h4>
<p>Class Matrix_Pointers<br/>
 Class Matrix_Symmetric<br/>
 Class Matrix_SymPacked<br/>
 Class Matrix_Hermitian<br/>
 Class Matrix_HermPacked<br/>
 Class Matrix_Triangular<br/>
 Class Matrix_TriangPacked<br/>
 <a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a> <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a><br/>
 <a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a> <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a><br/>
 <a class="el" href="_matrix___sym_packed_8hxx_source.php">Matrix_SymPacked.hxx</a> <a class="el" href="_matrix___sym_packed_8cxx_source.php">Matrix_SymPacked.cxx</a><br/>
 <a class="el" href="_matrix___hermitian_8hxx_source.php">Matrix_Hermitian.hxx</a> <a class="el" href="_matrix___hermitian_8cxx_source.php">Matrix_Hermitian.cxx</a><br/>
 <a class="el" href="_matrix___herm_packed_8hxx_source.php">Matrix_HermPacked.hxx</a> <a class="el" href="_matrix___herm_packed_8cxx_source.php">Matrix_HermPacked.cxx</a><br/>
 <a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a> <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a><br/>
 <a class="el" href="_matrix___triang_packed_8hxx_source.php">Matrix_TriangPacked.hxx</a> <a class="el" href="_matrix___triang_packed_8cxx_source.php">Matrix_TriangPacked.cxx</a></p>
<div class="separator"><a class="anchor" id="getm"></a></div><h3>GetM</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int GetM() const;
</pre><p>This method returns the number of rows </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;float&gt; V(3, 2);
// V.GetM() should return 3 
cout &lt;&lt; "Number of rows of V " &lt;&lt; V.GetM() &lt;&lt; endl;
</pre></div>  </pre><h4>Location :</h4>
<p>Class Matrix_Base<br/>
 <a class="el" href="_matrix___base_8hxx_source.php">Matrix_Base.hxx</a><br/>
 <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a></p>
<div class="separator"><a class="anchor" id="getn"></a></div><h3>GetN</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int GetN() const;
</pre><p>This method returns the number of columns.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;float&gt; V(3, 2);
// V.GetN() should return 2 
cout &lt;&lt; "Number of rows of V " &lt;&lt; V.GetM() &lt;&lt; endl;
</pre></div>  </pre><h4>Location :</h4>
<p>Class Matrix_Base<br/>
 <a class="el" href="_matrix___base_8hxx_source.php">Matrix_Base.hxx</a><br/>
 <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a></p>
<div class="separator"><a class="anchor" id="getsize"></a></div><h3>GetSize</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int GetSize() const;
</pre><p>This method returns the number of elements in the matrix</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;float, Symmetric, RowSymPacked&gt; V(3, 3);
// V.GetSize() should return 9
cout &lt;&lt; "Number of elements of V " &lt;&lt; V.GetSize() &lt;&lt; endl;
</pre></div>  </pre><h4>Location :</h4>
<p>Class Matrix_Base<br/>
 <a class="el" href="_matrix___base_8hxx_source.php">Matrix_Base.hxx</a><br/>
 <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a></p>
<div class="separator"><a class="anchor" id="getdatasize"></a></div><h3>GetDataSize</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int GetDataSize() const;
</pre><p>This method returns the number of elements effectively stored in the matrix. This is different from <code>GetSize</code> for some storages, e.g. RowSymPacked, RowHermPacked, RowUpTriangPacked.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;float, Symmetric, RowSymPacked&gt; V(3, 3);
// V.GetDataSize() should return 6
cout &lt;&lt; "Number of elements of V " &lt;&lt; V.GetDataSize() &lt;&lt; endl;
</pre></div>  </pre><h4>Location :</h4>
<p>Class Matrix_Pointers<br/>
 Class Matrix_Symmetric<br/>
 Class Matrix_SymPacked<br/>
 Class Matrix_Hermitian<br/>
 Class Matrix_HermPacked<br/>
 Class Matrix_Triangular<br/>
 Class Matrix_TriangPacked<br/>
 <a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a> <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a><br/>
 <a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a> <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a><br/>
 <a class="el" href="_matrix___sym_packed_8hxx_source.php">Matrix_SymPacked.hxx</a> <a class="el" href="_matrix___sym_packed_8cxx_source.php">Matrix_SymPacked.cxx</a><br/>
 <a class="el" href="_matrix___hermitian_8hxx_source.php">Matrix_Hermitian.hxx</a> <a class="el" href="_matrix___hermitian_8cxx_source.php">Matrix_Hermitian.cxx</a><br/>
 <a class="el" href="_matrix___herm_packed_8hxx_source.php">Matrix_HermPacked.hxx</a> <a class="el" href="_matrix___herm_packed_8cxx_source.php">Matrix_HermPacked.cxx</a><br/>
 <a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a> <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a><br/>
 <a class="el" href="_matrix___triang_packed_8hxx_source.php">Matrix_TriangPacked.hxx</a> <a class="el" href="_matrix___triang_packed_8cxx_source.php">Matrix_TriangPacked.cxx</a></p>
<div class="separator"><a class="anchor" id="getdata"></a></div><h3>GetData, GetDataConst, GetDataVoid, GetDataConstVoid</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  T* GetData() const;
  const T* GetDataConst() const;
  void* GetDataVoid() const;
  const void* GetDataConstVoid() const;
</pre><p>These methods are useful to retrieve the pointer to the values. In practice, you can use those methods in order to interface with C/fortran subroutines or to perform some low level operations. But in this last case, you have to be careful, because debugging operations will be more tedious.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; V(3, 4); V.Fill();
double* data = V.GetData();
// you can use data as a normal C array
// here the sum of elements is computed
double sum = 0;
for (int i = 0; i &lt; V.GetDataSize(); i++)
  sum += data[i];

// if you want to call a fortran subroutine daxpy
Matrix&lt;double&gt; X(3, 3); 
double coef = 2.0;
int m = X.GetM();
int n = X.GetN();
daxpy_(&amp;coef, &amp;m, &amp;n, X.GetData(),);

// for complex numbers, conversion to void* is needed :
Matrix&lt;complex&lt;double&gt; &gt; Xc(4, 4);
complex&lt;double&gt; beta(1,1);
zaxpy(reinterpret_cast&lt;const void*&gt;(beta), Xc.GetDataVoid());
</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="#setdata">SetData</a><br/>
 <a href="#nullify">Nullify</a></p>
<h4>Location :</h4>
<p>Class Matrix_Base<br/>
 <a class="el" href="_matrix___base_8hxx_source.php">Matrix_Base.hxx</a><br/>
 <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a></p>
<div class="separator"><a class="anchor" id="clear"></a></div><h3>Clear</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Clear();
</pre><p>This method removes all the elements of the matrix.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(3, 2);
A.Fill();
// clears matrix A
A.Clear();
</pre></div>  </pre><h4>Location :</h4>
<p>Class Matrix_Pointers<br/>
 Class Matrix_Symmetric<br/>
 Class Matrix_SymPacked<br/>
 Class Matrix_Hermitian<br/>
 Class Matrix_HermPacked<br/>
 Class Matrix_Triangular<br/>
 Class Matrix_TriangPacked<br/>
 <a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a> <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a><br/>
 <a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a> <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a><br/>
 <a class="el" href="_matrix___sym_packed_8hxx_source.php">Matrix_SymPacked.hxx</a> <a class="el" href="_matrix___sym_packed_8cxx_source.php">Matrix_SymPacked.cxx</a><br/>
 <a class="el" href="_matrix___hermitian_8hxx_source.php">Matrix_Hermitian.hxx</a> <a class="el" href="_matrix___hermitian_8cxx_source.php">Matrix_Hermitian.cxx</a><br/>
 <a class="el" href="_matrix___herm_packed_8hxx_source.php">Matrix_HermPacked.hxx</a> <a class="el" href="_matrix___herm_packed_8cxx_source.php">Matrix_HermPacked.cxx</a><br/>
 <a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a> <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a><br/>
 <a class="el" href="_matrix___triang_packed_8hxx_source.php">Matrix_TriangPacked.hxx</a> <a class="el" href="_matrix___triang_packed_8cxx_source.php">Matrix_TriangPacked.cxx</a></p>
<div class="separator"><a class="anchor" id="reallocate"></a></div><h3>Reallocate</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Reallocate(int, int);
</pre><p>This method changes the size of the matrix, but removes previous elements.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;long int&gt; A(5, 4);
V.Fill();
// resizes matrix A
A.Reallocate(4, 3);
// you need to initialize all elements of A
A.Zero();
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#resize">Resize</a></p>
<h4>Location :</h4>
<p>Class Matrix_Pointers<br/>
 Class Matrix_Symmetric<br/>
 Class Matrix_SymPacked<br/>
 Class Matrix_Hermitian<br/>
 Class Matrix_HermPacked<br/>
 Class Matrix_Triangular<br/>
 Class Matrix_TriangPacked<br/>
 <a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a> <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a><br/>
 <a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a> <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a><br/>
 <a class="el" href="_matrix___sym_packed_8hxx_source.php">Matrix_SymPacked.hxx</a> <a class="el" href="_matrix___sym_packed_8cxx_source.php">Matrix_SymPacked.cxx</a><br/>
 <a class="el" href="_matrix___hermitian_8hxx_source.php">Matrix_Hermitian.hxx</a> <a class="el" href="_matrix___hermitian_8cxx_source.php">Matrix_Hermitian.cxx</a><br/>
 <a class="el" href="_matrix___herm_packed_8hxx_source.php">Matrix_HermPacked.hxx</a> <a class="el" href="_matrix___herm_packed_8cxx_source.php">Matrix_HermPacked.cxx</a><br/>
 <a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a> <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a><br/>
 <a class="el" href="_matrix___triang_packed_8hxx_source.php">Matrix_TriangPacked.hxx</a> <a class="el" href="_matrix___triang_packed_8cxx_source.php">Matrix_TriangPacked.cxx</a></p>
<div class="separator"><a class="anchor" id="resize"></a></div><h3>Resize</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Resize(int, int);
</pre><p>This method changes the size of the matrix, and keeps previous elements.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;long double&gt; A(3,3);
A.Fill();
// resizes matrix A
A.Resize(4,4);
// you need to initialize new elements if there are new
for (int i = 0; i &lt; 4; i++)
  A(4,i) = A(i,4) = 0;
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#reallocate">Reallocate</a></p>
<h4>Location :</h4>
<p>Class Matrix_Pointers<br/>
 Class Matrix_Symmetric<br/>
 Class Matrix_SymPacked<br/>
 Class Matrix_Hermitian<br/>
 Class Matrix_HermPacked<br/>
 Class Matrix_Triangular<br/>
 Class Matrix_TriangPacked<br/>
 <a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a> <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a><br/>
 <a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a> <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a><br/>
 <a class="el" href="_matrix___sym_packed_8hxx_source.php">Matrix_SymPacked.hxx</a> <a class="el" href="_matrix___sym_packed_8cxx_source.php">Matrix_SymPacked.cxx</a><br/>
 <a class="el" href="_matrix___hermitian_8hxx_source.php">Matrix_Hermitian.hxx</a> <a class="el" href="_matrix___hermitian_8cxx_source.php">Matrix_Hermitian.cxx</a><br/>
 <a class="el" href="_matrix___herm_packed_8hxx_source.php">Matrix_HermPacked.hxx</a> <a class="el" href="_matrix___herm_packed_8cxx_source.php">Matrix_HermPacked.cxx</a><br/>
 <a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a> <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a><br/>
 <a class="el" href="_matrix___triang_packed_8hxx_source.php">Matrix_TriangPacked.hxx</a> <a class="el" href="_matrix___triang_packed_8cxx_source.php">Matrix_TriangPacked.cxx</a></p>
<div class="separator"><a class="anchor" id="setdata"></a></div><h3>SetData</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void SetData(int, int, T*);
</pre><p>This method sets the pointer to the array containing elements. This method should be used carefully, and generally in conjunction with method Nullify. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// for example, you can define a function with a pointer as argument
void f(int m, int n, double* data)
{
  // and sets this array into a Matrix instance
  Matrix&lt;double&gt; A;
  // m : number of rows, n : number of columns
  A.SetData(m, n, data);
  // then you use a C++ method
  double coef = Norm1(A);
  // you don't release memory, because data is used after the function
  A.Nullify();
}
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#getdata">GetData</a><br/>
 <a href="#nullify">Nullify</a></p>
<h4>Location :</h4>
<p>Class Matrix_Pointers<br/>
 Class Matrix_Symmetric<br/>
 Class Matrix_SymPacked<br/>
 Class Matrix_Hermitian<br/>
 Class Matrix_HermPacked<br/>
 Class Matrix_Triangular<br/>
 Class Matrix_TriangPacked<br/>
 <a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a> <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a><br/>
 <a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a> <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a><br/>
 <a class="el" href="_matrix___sym_packed_8hxx_source.php">Matrix_SymPacked.hxx</a> <a class="el" href="_matrix___sym_packed_8cxx_source.php">Matrix_SymPacked.cxx</a><br/>
 <a class="el" href="_matrix___hermitian_8hxx_source.php">Matrix_Hermitian.hxx</a> <a class="el" href="_matrix___hermitian_8cxx_source.php">Matrix_Hermitian.cxx</a><br/>
 <a class="el" href="_matrix___herm_packed_8hxx_source.php">Matrix_HermPacked.hxx</a> <a class="el" href="_matrix___herm_packed_8cxx_source.php">Matrix_HermPacked.cxx</a><br/>
 <a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a> <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a><br/>
 <a class="el" href="_matrix___triang_packed_8hxx_source.php">Matrix_TriangPacked.hxx</a> <a class="el" href="_matrix___triang_packed_8cxx_source.php">Matrix_TriangPacked.cxx</a></p>
<div class="separator"><a class="anchor" id="nullify"></a></div><h3>Nullify</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Nullify();
</pre><p>This method clears the matrix without releasing memory. This method should be used carefully, and generally in conjunction with method Nullify. You can look at the example shown in the explanation of method SetData. </p>
<h4>Related topics :</h4>
<p><a href="#setdata">SetData</a><br/>
 <a href="#getdata">GetData</a></p>
<h4>Location :</h4>
<p>Class Matrix_Pointers<br/>
 Class Matrix_Symmetric<br/>
 Class Matrix_SymPacked<br/>
 Class Matrix_Hermitian<br/>
 Class Matrix_HermPacked<br/>
 Class Matrix_Triangular<br/>
 Class Matrix_TriangPacked<br/>
 <a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a> <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a><br/>
 <a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a> <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a><br/>
 <a class="el" href="_matrix___sym_packed_8hxx_source.php">Matrix_SymPacked.hxx</a> <a class="el" href="_matrix___sym_packed_8cxx_source.php">Matrix_SymPacked.cxx</a><br/>
 <a class="el" href="_matrix___hermitian_8hxx_source.php">Matrix_Hermitian.hxx</a> <a class="el" href="_matrix___hermitian_8cxx_source.php">Matrix_Hermitian.cxx</a><br/>
 <a class="el" href="_matrix___herm_packed_8hxx_source.php">Matrix_HermPacked.hxx</a> <a class="el" href="_matrix___herm_packed_8cxx_source.php">Matrix_HermPacked.cxx</a><br/>
 <a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a> <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a><br/>
 <a class="el" href="_matrix___triang_packed_8hxx_source.php">Matrix_TriangPacked.hxx</a> <a class="el" href="_matrix___triang_packed_8cxx_source.php">Matrix_TriangPacked.cxx</a></p>
<div class="separator"><a class="anchor" id="copy"></a></div><h3>Copy</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Copy(const Matrix&amp;);
</pre><p>This method copies a matrix into the current matrix.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// copy of a matrix M
Matrix&lt;double&gt; M(3, 3), A;
M.FillRand();
A.Copy(M);
// this is equivalent to use operator =
A = M;
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#operator">Matrix operators</a></p>
<h4>Location :</h4>
<p>Class Matrix_Pointers<br/>
 Class Matrix_Symmetric<br/>
 Class Matrix_SymPacked<br/>
 Class Matrix_Hermitian<br/>
 Class Matrix_HermPacked<br/>
 Class Matrix_Triangular<br/>
 Class Matrix_TriangPacked<br/>
 <a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a> <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a><br/>
 <a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a> <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a><br/>
 <a class="el" href="_matrix___sym_packed_8hxx_source.php">Matrix_SymPacked.hxx</a> <a class="el" href="_matrix___sym_packed_8cxx_source.php">Matrix_SymPacked.cxx</a><br/>
 <a class="el" href="_matrix___hermitian_8hxx_source.php">Matrix_Hermitian.hxx</a> <a class="el" href="_matrix___hermitian_8cxx_source.php">Matrix_Hermitian.cxx</a><br/>
 <a class="el" href="_matrix___herm_packed_8hxx_source.php">Matrix_HermPacked.hxx</a> <a class="el" href="_matrix___herm_packed_8cxx_source.php">Matrix_HermPacked.cxx</a><br/>
 <a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a> <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a><br/>
 <a class="el" href="_matrix___triang_packed_8hxx_source.php">Matrix_TriangPacked.hxx</a> <a class="el" href="_matrix___triang_packed_8cxx_source.php">Matrix_TriangPacked.cxx</a></p>
<div class="separator"><a class="anchor" id="val"></a></div><h3>Val</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  T&amp; Val(int, int);
  const T&amp; Val(int, int) const;
</pre><p>This method is similar to operator (), except that it always works, especially for storages like RowSym, RowHerm, RowUpTriang.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, RowUpTriang&gt; A(3,3);
// operator () does not work to change the value
// A(0,0) = 1;  =&gt; Error during compilation
A.Val(0,0) = 2.0;  // Okay it works
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#operator">Operators</a></p>
<h4>Location :</h4>
<p>Class Matrix_Pointers<br/>
 Class Matrix_Symmetric<br/>
 Class Matrix_SymPacked<br/>
 Class Matrix_Hermitian<br/>
 Class Matrix_HermPacked<br/>
 Class Matrix_Triangular<br/>
 Class Matrix_TriangPacked<br/>
 <a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a> <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a><br/>
 <a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a> <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a><br/>
 <a class="el" href="_matrix___sym_packed_8hxx_source.php">Matrix_SymPacked.hxx</a> <a class="el" href="_matrix___sym_packed_8cxx_source.php">Matrix_SymPacked.cxx</a><br/>
 <a class="el" href="_matrix___hermitian_8hxx_source.php">Matrix_Hermitian.hxx</a> <a class="el" href="_matrix___hermitian_8cxx_source.php">Matrix_Hermitian.cxx</a><br/>
 <a class="el" href="_matrix___herm_packed_8hxx_source.php">Matrix_HermPacked.hxx</a> <a class="el" href="_matrix___herm_packed_8cxx_source.php">Matrix_HermPacked.cxx</a><br/>
 <a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a> <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a><br/>
 <a class="el" href="_matrix___triang_packed_8hxx_source.php">Matrix_TriangPacked.hxx</a> <a class="el" href="_matrix___triang_packed_8cxx_source.php">Matrix_TriangPacked.cxx</a></p>
<div class="separator"><a class="anchor" id="zero"></a></div><h3>Zero</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Zero();
</pre><p>This method fills memory of 0, is convenient for matrices made of doubles, integers, floats, but not for more complicated types. In that case, it is better to use the method <a href="#fill">Fill</a>.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; V(5, 3);
// initialization
V.Fill();

Matrix&lt;IVect&gt; W(10, 2);
// W.Zero() is incorrect and would generate an error at the execution
// a good initialization is to use Fill
IVect zero(5); zero.Zero();
W.Fill(zero);
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#fill">Fill</a></p>
<h4>Location :</h4>
<p>Class Matrix_Pointers<br/>
 Class Matrix_Symmetric<br/>
 Class Matrix_SymPacked<br/>
 Class Matrix_Hermitian<br/>
 Class Matrix_HermPacked<br/>
 Class Matrix_Triangular<br/>
 Class Matrix_TriangPacked<br/>
 <a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a> <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a><br/>
 <a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a> <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a><br/>
 <a class="el" href="_matrix___sym_packed_8hxx_source.php">Matrix_SymPacked.hxx</a> <a class="el" href="_matrix___sym_packed_8cxx_source.php">Matrix_SymPacked.cxx</a><br/>
 <a class="el" href="_matrix___hermitian_8hxx_source.php">Matrix_Hermitian.hxx</a> <a class="el" href="_matrix___hermitian_8cxx_source.php">Matrix_Hermitian.cxx</a><br/>
 <a class="el" href="_matrix___herm_packed_8hxx_source.php">Matrix_HermPacked.hxx</a> <a class="el" href="_matrix___herm_packed_8cxx_source.php">Matrix_HermPacked.cxx</a><br/>
 <a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a> <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a><br/>
 <a class="el" href="_matrix___triang_packed_8hxx_source.php">Matrix_TriangPacked.hxx</a> <a class="el" href="_matrix___triang_packed_8cxx_source.php">Matrix_TriangPacked.cxx</a></p>
<div class="separator"><a class="anchor" id="setidentity"></a></div><h3>SetIdentity</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void SetIdentity();
</pre><p>This method sets all elements to 0, except on the diagonal set to 1. This forms the so-called identity matrix.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; V(5, 5);
// initialization
V.SetIdentity();
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#fill">Fill</a></p>
<h4>Location :</h4>
<p>Class Matrix_Pointers<br/>
 Class Matrix_Symmetric<br/>
 Class Matrix_SymPacked<br/>
 Class Matrix_Hermitian<br/>
 Class Matrix_HermPacked<br/>
 Class Matrix_Triangular<br/>
 Class Matrix_TriangPacked<br/>
 <a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a> <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a><br/>
 <a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a> <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a><br/>
 <a class="el" href="_matrix___sym_packed_8hxx_source.php">Matrix_SymPacked.hxx</a> <a class="el" href="_matrix___sym_packed_8cxx_source.php">Matrix_SymPacked.cxx</a><br/>
 <a class="el" href="_matrix___hermitian_8hxx_source.php">Matrix_Hermitian.hxx</a> <a class="el" href="_matrix___hermitian_8cxx_source.php">Matrix_Hermitian.cxx</a><br/>
 <a class="el" href="_matrix___herm_packed_8hxx_source.php">Matrix_HermPacked.hxx</a> <a class="el" href="_matrix___herm_packed_8cxx_source.php">Matrix_HermPacked.cxx</a><br/>
 <a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a> <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a><br/>
 <a class="el" href="_matrix___triang_packed_8hxx_source.php">Matrix_TriangPacked.hxx</a> <a class="el" href="_matrix___triang_packed_8cxx_source.php">Matrix_TriangPacked.cxx</a></p>
<div class="separator"><a class="anchor" id="fill"></a></div><h3>Fill</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Fill();
  template&lt;class T0&gt;
  void Fill(const T0&amp; );
</pre><p>This method fills matrix with 0, 1, 2, etc or with a given value.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;int&gt; A(2,2);
A.Fill();
// A should contain [0 1; 2 3]

A.Fill(2);
// A should contain [2 2; 2 2]
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#zero">Zero</a></p>
<h4>Location :</h4>
<p>Class Matrix_Pointers<br/>
 Class Matrix_Symmetric<br/>
 Class Matrix_SymPacked<br/>
 Class Matrix_Hermitian<br/>
 Class Matrix_HermPacked<br/>
 Class Matrix_Triangular<br/>
 Class Matrix_TriangPacked<br/>
 <a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a> <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a><br/>
 <a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a> <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a><br/>
 <a class="el" href="_matrix___sym_packed_8hxx_source.php">Matrix_SymPacked.hxx</a> <a class="el" href="_matrix___sym_packed_8cxx_source.php">Matrix_SymPacked.cxx</a><br/>
 <a class="el" href="_matrix___hermitian_8hxx_source.php">Matrix_Hermitian.hxx</a> <a class="el" href="_matrix___hermitian_8cxx_source.php">Matrix_Hermitian.cxx</a><br/>
 <a class="el" href="_matrix___herm_packed_8hxx_source.php">Matrix_HermPacked.hxx</a> <a class="el" href="_matrix___herm_packed_8cxx_source.php">Matrix_HermPacked.cxx</a><br/>
 <a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a> <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a><br/>
 <a class="el" href="_matrix___triang_packed_8hxx_source.php">Matrix_TriangPacked.hxx</a> <a class="el" href="_matrix___triang_packed_8cxx_source.php">Matrix_TriangPacked.cxx</a></p>
<div class="separator"><a class="anchor" id="fillrand"></a></div><h3>FillRand</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void FillRand();
</pre><p>This method fills the matrix with random values.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(5, 3);
A.FillRand();
// A should contain 15 random values
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#fill">Fill</a></p>
<h4>Location :</h4>
<p>Class Matrix_Pointers<br/>
 Class Matrix_Symmetric<br/>
 Class Matrix_SymPacked<br/>
 Class Matrix_Hermitian<br/>
 Class Matrix_HermPacked<br/>
 Class Matrix_Triangular<br/>
 Class Matrix_TriangPacked<br/>
 <a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a> <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a><br/>
 <a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a> <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a><br/>
 <a class="el" href="_matrix___sym_packed_8hxx_source.php">Matrix_SymPacked.hxx</a> <a class="el" href="_matrix___sym_packed_8cxx_source.php">Matrix_SymPacked.cxx</a><br/>
 <a class="el" href="_matrix___hermitian_8hxx_source.php">Matrix_Hermitian.hxx</a> <a class="el" href="_matrix___hermitian_8cxx_source.php">Matrix_Hermitian.cxx</a><br/>
 <a class="el" href="_matrix___herm_packed_8hxx_source.php">Matrix_HermPacked.hxx</a> <a class="el" href="_matrix___herm_packed_8cxx_source.php">Matrix_HermPacked.cxx</a><br/>
 <a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a> <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a><br/>
 <a class="el" href="_matrix___triang_packed_8hxx_source.php">Matrix_TriangPacked.hxx</a> <a class="el" href="_matrix___triang_packed_8cxx_source.php">Matrix_TriangPacked.cxx</a></p>
<div class="separator"><a class="anchor" id="print"></a></div><h3>Print</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Print() const;
  void Print(int) const;
  void Print(int, int, int, int) const;
</pre><p>This method displays the matrix.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;string&gt; A(2, 2);
A(0,0) = string("hello");
A(0,1) = string("world");
A(1,0) = string("you");
A(1,1) = string("welcome");
A.Print(); 
// should display :
// hello world
// you welcome

// you can also display a sub-matrix
A.Print(0, 0, 0, 1); 
// should display "hello world"

// A.Print(2); is equivalent to A.Print(0, 0, 2, 2);

</pre></div>  </pre><h4>Location :</h4>
<p>Class Matrix_Pointers<br/>
 Class Matrix_Symmetric<br/>
 Class Matrix_SymPacked<br/>
 Class Matrix_Hermitian<br/>
 Class Matrix_HermPacked<br/>
 Class Matrix_Triangular<br/>
 Class Matrix_TriangPacked<br/>
 <a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a> <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a><br/>
 <a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a> <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a><br/>
 <a class="el" href="_matrix___sym_packed_8hxx_source.php">Matrix_SymPacked.hxx</a> <a class="el" href="_matrix___sym_packed_8cxx_source.php">Matrix_SymPacked.cxx</a><br/>
 <a class="el" href="_matrix___hermitian_8hxx_source.php">Matrix_Hermitian.hxx</a> <a class="el" href="_matrix___hermitian_8cxx_source.php">Matrix_Hermitian.cxx</a><br/>
 <a class="el" href="_matrix___herm_packed_8hxx_source.php">Matrix_HermPacked.hxx</a> <a class="el" href="_matrix___herm_packed_8cxx_source.php">Matrix_HermPacked.cxx</a><br/>
 <a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a> <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a><br/>
 <a class="el" href="_matrix___triang_packed_8hxx_source.php">Matrix_TriangPacked.hxx</a> <a class="el" href="_matrix___triang_packed_8cxx_source.php">Matrix_TriangPacked.cxx</a></p>
<div class="separator"><a class="anchor" id="write"></a></div><h3>Write</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Write(string) const;
  void Write(ofstream&amp;) const;
</pre><p>This method writes the matrix on a file/stream in binary format. The file will contain the number of rows, columns, then the list of elements.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(2, 2); 
// you can write directly in a file
A.Fill();
A.Write("matrix.dat");

// or open a stream with other datas
ofstream file_out("matrix.dat");
int my_info = 3;
file_out.write(reinterpret_cast&lt;char*&gt;(&gt;my_info), sizeof(int));
A.Write(file_out);
file_out.close();
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#read">Read</a><br/>
 <a href="#writetext">WriteText</a><br/>
 <a href="#readtext">ReadText</a></p>
<h4>Location :</h4>
<p>Class Matrix_Pointers<br/>
 Class Matrix_Symmetric<br/>
 Class Matrix_SymPacked<br/>
 Class Matrix_Hermitian<br/>
 Class Matrix_HermPacked<br/>
 Class Matrix_Triangular<br/>
 Class Matrix_TriangPacked<br/>
 <a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a> <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a><br/>
 <a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a> <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a><br/>
 <a class="el" href="_matrix___sym_packed_8hxx_source.php">Matrix_SymPacked.hxx</a> <a class="el" href="_matrix___sym_packed_8cxx_source.php">Matrix_SymPacked.cxx</a><br/>
 <a class="el" href="_matrix___hermitian_8hxx_source.php">Matrix_Hermitian.hxx</a> <a class="el" href="_matrix___hermitian_8cxx_source.php">Matrix_Hermitian.cxx</a><br/>
 <a class="el" href="_matrix___herm_packed_8hxx_source.php">Matrix_HermPacked.hxx</a> <a class="el" href="_matrix___herm_packed_8cxx_source.php">Matrix_HermPacked.cxx</a><br/>
 <a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a> <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a><br/>
 <a class="el" href="_matrix___triang_packed_8hxx_source.php">Matrix_TriangPacked.hxx</a> <a class="el" href="_matrix___triang_packed_8cxx_source.php">Matrix_TriangPacked.cxx</a></p>
<div class="separator"><a class="anchor" id="read"></a></div><h3>Read</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Read(string);
  void Read(ifstream&amp;);
</pre><p>This method sets the matrix from a file/stream in binary format. The file contains the number of rows, columns, then the list of elements.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; V(3, 4); 
// you can read directly on a file
V.Read("matrix.dat");

// or read from a stream
ifstream file_in("matrix.dat");
int my_info;
file_in.read(reinterpret_cast&lt;char*&lt;(&gt;my_info), sizeof(int));
V.Read(file_in);
file_in.close();
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#write">Write</a><br/>
 <a href="#writetext">WriteText</a><br/>
 <a href="#readtext">ReadText</a></p>
<h4>Location :</h4>
<p>Class Matrix_Pointers<br/>
 Class Matrix_Symmetric<br/>
 Class Matrix_SymPacked<br/>
 Class Matrix_Hermitian<br/>
 Class Matrix_HermPacked<br/>
 Class Matrix_Triangular<br/>
 Class Matrix_TriangPacked<br/>
 <a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a> <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a><br/>
 <a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a> <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a><br/>
 <a class="el" href="_matrix___sym_packed_8hxx_source.php">Matrix_SymPacked.hxx</a> <a class="el" href="_matrix___sym_packed_8cxx_source.php">Matrix_SymPacked.cxx</a><br/>
 <a class="el" href="_matrix___hermitian_8hxx_source.php">Matrix_Hermitian.hxx</a> <a class="el" href="_matrix___hermitian_8cxx_source.php">Matrix_Hermitian.cxx</a><br/>
 <a class="el" href="_matrix___herm_packed_8hxx_source.php">Matrix_HermPacked.hxx</a> <a class="el" href="_matrix___herm_packed_8cxx_source.php">Matrix_HermPacked.cxx</a><br/>
 <a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a> <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a><br/>
 <a class="el" href="_matrix___triang_packed_8hxx_source.php">Matrix_TriangPacked.hxx</a> <a class="el" href="_matrix___triang_packed_8cxx_source.php">Matrix_TriangPacked.cxx</a></p>
<div class="separator"><a class="anchor" id="writetext"></a></div><h3>WriteText</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void WriteText(string) const;
  void WriteText(ofstream&amp;) const;
</pre><p>This method writes the matrix on a file/stream in text format. The file will contain the list of elements.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; V(2); 
// you can write directly in a file
V.Fill();
V.WriteText("matrix.dat");

// or open a stream with other datas
ofstream file_out("matrix.dat");
int my_info = 3;
file_out &lt;&lt; my_info &lt;&lt; '\n';
V.WriteText(file_out);
file_out.close();
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#write">Write</a><br/>
 <a href="#read">Read</a><br/>
 <a href="#readtext">ReadText</a></p>
<h4>Location :</h4>
<p>Class Matrix_Pointers<br/>
 Class Matrix_Symmetric<br/>
 Class Matrix_SymPacked<br/>
 Class Matrix_Hermitian<br/>
 Class Matrix_HermPacked<br/>
 Class Matrix_Triangular<br/>
 Class Matrix_TriangPacked<br/>
 <a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a> <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a><br/>
 <a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a> <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a><br/>
 <a class="el" href="_matrix___sym_packed_8hxx_source.php">Matrix_SymPacked.hxx</a> <a class="el" href="_matrix___sym_packed_8cxx_source.php">Matrix_SymPacked.cxx</a><br/>
 <a class="el" href="_matrix___hermitian_8hxx_source.php">Matrix_Hermitian.hxx</a> <a class="el" href="_matrix___hermitian_8cxx_source.php">Matrix_Hermitian.cxx</a><br/>
 <a class="el" href="_matrix___herm_packed_8hxx_source.php">Matrix_HermPacked.hxx</a> <a class="el" href="_matrix___herm_packed_8cxx_source.php">Matrix_HermPacked.cxx</a><br/>
 <a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a> <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a><br/>
 <a class="el" href="_matrix___triang_packed_8hxx_source.php">Matrix_TriangPacked.hxx</a> <a class="el" href="_matrix___triang_packed_8cxx_source.php">Matrix_TriangPacked.cxx</a></p>
<div class="separator"><a class="anchor" id="readtext"></a></div><h3>ReadText</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void ReadText(string);
  void ReadText(ifstream&amp;);
</pre><p>This method sets the matrix from a file/stream in text format. The file contains the list of elements.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; V; 
// you can read directly on a file
V.ReadText("matrix.dat");

// or read from a stream
ifstream file_in("matrix.dat");
int my_info;
file_in &gt;&gt; my_info;
V.ReadText(file_in);
file_in.close();
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#write">Write</a><br/>
 <a href="#read">Read</a><br/>
 <a href="#writetext">WriteText</a></p>
<h4>Location :</h4>
<p>Class Matrix_Pointers<br/>
 Class Matrix_Symmetric<br/>
 Class Matrix_SymPacked<br/>
 Class Matrix_Hermitian<br/>
 Class Matrix_HermPacked<br/>
 Class Matrix_Triangular<br/>
 Class Matrix_TriangPacked<br/>
 <a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a> <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a><br/>
 <a class="el" href="_matrix___symmetric_8hxx_source.php">Matrix_Symmetric.hxx</a> <a class="el" href="_matrix___symmetric_8cxx_source.php">Matrix_Symmetric.cxx</a><br/>
 <a class="el" href="_matrix___sym_packed_8hxx_source.php">Matrix_SymPacked.hxx</a> <a class="el" href="_matrix___sym_packed_8cxx_source.php">Matrix_SymPacked.cxx</a><br/>
 <a class="el" href="_matrix___hermitian_8hxx_source.php">Matrix_Hermitian.hxx</a> <a class="el" href="_matrix___hermitian_8cxx_source.php">Matrix_Hermitian.cxx</a><br/>
 <a class="el" href="_matrix___herm_packed_8hxx_source.php">Matrix_HermPacked.hxx</a> <a class="el" href="_matrix___herm_packed_8cxx_source.php">Matrix_HermPacked.cxx</a><br/>
 <a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a> <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a><br/>
 <a class="el" href="_matrix___triang_packed_8hxx_source.php">Matrix_TriangPacked.hxx</a> <a class="el" href="_matrix___triang_packed_8cxx_source.php">Matrix_TriangPacked.cxx</a> </p>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
