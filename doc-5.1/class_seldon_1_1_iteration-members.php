<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Iteration&lt; Titer &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a>, including all inherited members.<table>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a493984d8abaec3ac0d4f73c29d2a0e60">error_code</a></td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#aa84736520dcca50ab5b8c72449ef8ed4">ErrorCode</a>() const </td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#adf29aa77b9dd1ecdc57c132712b02811">facteur_reste</a></td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a32dd5ec2e2b6216c50fbc1d0e27607a7">Fail</a>(int i, const string &amp;s)</td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#afea45227a5d730ac1464b3c33dabfc9e">fail_convergence</a></td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a07c3b0af0412cfc7fe6fd3ed5476c4e0">Finished</a>(const Vector1 &amp;r) const </td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#af217fa4792dee453e14b48f5f6cc30a4">Finished</a>(const Titer &amp;r) const </td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#adaeada1a17ad527435684f5f8ad33f33">First</a>() const </td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a617c6d99afeb2b5a2d18144f367dba88">GetFactor</a>() const </td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#ab426008d4bfc40842d86c8c6042a6594">GetNumberIteration</a>() const </td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#ae383df46b41aff26fe97418cab8a070f">GetRestart</a>() const </td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#af31105a0169bb625d24a4ba76183b3a5">GetTolerance</a>() const </td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#ad57613fa7b74a7a7a4e758262957ca0f">GetTypeSolver</a>() const </td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a305d9d618a978830c3d2e36953c001c3">HideMessages</a>()</td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a047b05d60ecc8b6f8f0789255472bfe3">Init</a>(const Vector1 &amp;r)</td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a363306d1242c5a2b78737ace82ea465b">init_guess_null</a></td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#aa02c78783891fabcb4f7517f60f4912e">IsInitGuess_Null</a>() const </td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a9c42765a055d9dd271b2b8a52a0baa57">Iteration</a>()</td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a7631992edbb680c8840bc586410a016a">Iteration</a>(int max_iteration, const Titer &amp;tol)</td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a6463cf08af3be211d0c5c397b27b177d">Iteration</a>(const Iteration&lt; Titer &gt; &amp;outer)</td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a75359d82e80a14fa10907b203fc5a2ff">max_iter</a></td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a4ff83b0b358c1574c6c0ababbbe04745">nb_iter</a></td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a105cecfc4c5d1c1c27278eaa5807e32d">operator++</a>(void)</td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a5c4d36f6bff1c44d6e7b31bb3c70b603">parameter_restart</a></td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a82adaef364a9a20a7525ab9299b6bfd5">print_level</a></td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SetInitGuess</b>(bool type) (defined in <a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#afed55f067231a1defcf50acd0755aef1">SetMaxNumberIteration</a>(int max_iteration)</td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a24c69ffb62504d708354f8874a5f0f00">SetNumberIteration</a>(int nb)</td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a3a5874b5b76075698ceeb99944261311">SetRestart</a>(int m)</td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a7906239fb42957439f236258d4423465">SetSolver</a>(int type_resolution, int param_restart, int type_prec)</td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a1393846a1b9090c4bb3516b28e70561b">SetTolerance</a>(Titer stopping_criterion)</td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a69000baf75577546e3d78e8bcfe1576a">ShowFullHistory</a>()</td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a18f2bdb38c22716d640422ead8998e1f">ShowMessages</a>()</td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#acf9550c0ec96e51fc5d395446342c4e8">tolerance</a></td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#a19f60dd0946c1a5a212b0e28b5be55a9">type_preconditioning</a></td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_iteration.php#aced354c0bf1e3868ae5b77283982b8fa">type_solver</a></td><td><a class="el" href="class_seldon_1_1_iteration.php">Seldon::Iteration&lt; Titer &gt;</a></td><td><code> [protected]</code></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
