<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Sub-Matrices </h1>  </div>
</div>
<div class="contents">
<p>A sub-matrix can be defined with any instance of a dense or sparse matrix. Arbitrary rows and columns may be selected.</p>
<h2><a class="anchor" id="declaration"></a>
Declaration</h2>
<p>Given a base matrix <code>A</code>, a sub-matrix is declared with a list of rows <code>row_list</code> and a list of columns <code>column_list</code>:</p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(4, 6);

Vector&lt;int&gt; row_list(2);
row_list(0) = 1;
row_list(1) = 2;
Vector&lt;int&gt; column_list(3);
column_list(0) = 0;
column_list(1) = 1;
column_list(2) = 5;

SubMatrix&lt;Matrix&lt;double&gt; &gt; SubA(A, row_list, column_list);
</pre></div>  </pre><p>The template argument of <code>SubMatrix</code> must be the exact type of the base matrix <code>A</code>.</p>
<h2><a class="anchor" id="more"></a>
Flexible Definition</h2>
<p>The row and column lists can be any index list, providing the indexes refer to an existing row or column in the base matrix. The same index may appear several times in a list, and the lists do not need to be sorted. As a consequence, the sub-matrices might be used for a more general purpose than simply extracting a sub-set of the rows and columns. For example, one may swap two columns and duplicate a line:</p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(3, 3);
A.Fill();
cout &lt;&lt; "Complete matrix:" &lt;&lt; endl;
A.Print();

Vector&lt;int&gt; row_list(4);
row_list.Fill();
row_list(3) = 2;
Vector&lt;int&gt; column_list(3);
column_list(0) = 1;
column_list(1) = 0;
column_list(2) = 2;
SubMatrix&lt;Matrix&lt;double&gt; &gt; SubA(A, row_list, column_list);

cout &lt;&lt; "Sub-matrix:" &lt;&lt; endl;
SubA.Print();
</pre></div>  </pre><p>will result in</p>
<div class="fragment"><pre class="fragment">
Complete matrix:
0       1       2
3       4       5
6       7       8
Sub-matrix:
1       0       2
4       3       5
7       6       8
7       6       8
</pre></div><h2><a class="anchor" id="base"></a>
Relation with the Base Matrix</h2>
<p>A sub-matrix is a light structure. It essentially stores the list of rows and columns, along with a pointer to the base matrix. When an element of the sub-matrix is accessed, the sub-matrix performs an indirection to the corresponding element in the base matrix. Hence, if the sub-matrix is modified, so will be the base matrix. This also means that the sub-matrix should not be manipulated once the base matrix is destroyed.</p>
<h2><a class="anchor" id="method"></a>
Available Methods</h2>
<p>For a description of all available methods, see the documentation for class <a class="el" href="class_seldon_1_1_sub_matrix.php" title="Sub-matrix class.">Seldon::SubMatrix</a>.</p>
<h2><a class="anchor" id="functions"></a>
Available Functions</h2>
<p>A sub-matrix is viewed as a matrix by Seldon. Therefore, all generic functions that are implemented in C++ can be called, which includes:</p>
<ul>
<li><a class="el" href="namespace_seldon.php#a6b94850cd394c6afee2463526ec1a7da">Mlt(M, X, Y) </a></li>
<li><a class="el" href="">Mlt(alpha, M, X, Y) </a></li>
<li><a class="el" href="namespace_seldon.php#ac3739907f79a615c21ec43e8762d17e4">MltAdd(alpha, M, X, beta, Y) </a></li>
<li><a class="el" href="namespace_seldon.php#ac9fbb323d131b8e1e8f8fc99f92b16e8">MltAdd(alpha, Trans, M, X, beta, Y) </a></li>
<li><a class="el" href="namespace_seldon.php#abc3136bfd007c68e1ff005a9efac7953">Mlt(alpha, A, B, C) </a></li>
<li><a class="el" href="namespace_seldon.php#a1fab5465a02b7912de8298553598a545">Mlt(A, B, C) </a></li>
<li><a class="el" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3">MltAdd(alpha, A, B, beta, C) </a></li>
<li><a class="el" href="">Add(alpha, A, B) </a></li>
<li><a class="el" href="namespace_seldon.php#a76d3f187a877c6c58245b0716e1adc00">GetLU(A) </a></li>
<li><a class="el" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c">SolveLU(A, Y) </a></li>
<li><a class="el" href="namespace_seldon.php#ab03e0763091dcf33a7f2d59a2cfdefdc">MaxAbs(A) </a></li>
<li><a class="el" href="namespace_seldon.php#afefce2ae4cd3147f863955efddc4184c">Norm1(A) </a></li>
<li><a class="el" href="namespace_seldon.php#ad345433f1f078dbb4cdc2829b1811e50">NormInf(A) </a></li>
</ul>
<p><b>Warning:</b> In the case of sub-matrices, these functions will <em>not</em> call Blas, Lapack or another external library. As a consequence, they may be slow.</p>
<h2><a class="anchor" id="example"></a>
An Example</h2>
 <div style="text-align:right;"><a href="src/doc/example/submatrix.cpp">doc/example/submatrix.cpp</a></div>  <div class="fragment"><pre class="fragment"><span class="preprocessor">#define SELDON_DEBUG_LEVEL_4</span>
<span class="preprocessor"></span><span class="preprocessor">#include &quot;Seldon.hxx&quot;</span>
<span class="keyword">using namespace </span>Seldon;

<span class="keywordtype">int</span> main()
{

  TRY;

  <span class="comment">/*** Full matrix ***/</span>

  Matrix&lt;double&gt; A(4, 6);
  A.Fill();
  cout &lt;&lt; <span class="stringliteral">&quot;Complete matrix:&quot;</span> &lt;&lt; endl;
  A.Print();

  Vector&lt;int&gt; row_list(2);
  row_list(0) = 1;
  row_list(1) = 2;
  Vector&lt;int&gt; column_list(3);
  column_list(0) = 0;
  column_list(1) = 1;
  column_list(2) = 5;
  SubMatrix&lt;Matrix&lt;double&gt; &gt; SubA(A, row_list, column_list);

  cout &lt;&lt; <span class="stringliteral">&quot;Sub-matrix:&quot;</span> &lt;&lt; endl;
  SubA.Print();

  <span class="comment">// Basic operations are supported, but they are slow (Blas/Lapack will not</span>
  <span class="comment">// be called).</span>
  Vector&lt;double&gt; X(3), Y(2);
  X.Fill();
  cout &lt;&lt; <span class="stringliteral">&quot;Multiplied by X = [&quot;</span> &lt;&lt; X &lt;&lt; <span class="stringliteral">&quot;]:&quot;</span> &lt;&lt; endl;
  <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(SubA, X, Y);
  Y.Print();

  <span class="comment">/*** Symmetric matrix ***/</span>

  Matrix&lt;double, General, ColSymPacked&gt; B(4);
  B.Fill();
  cout &lt;&lt; <span class="stringliteral">&quot;\nComplete matrix:&quot;</span> &lt;&lt; endl;
  B.Print();

  row_list(0) = 1;
  row_list(1) = 3;
  column_list(0) = 0;
  column_list(1) = 2;
  column_list(2) = 3;
  SubMatrix&lt;Matrix&lt;double, General, ColSymPacked&gt; &gt;
    SubB(B, row_list, column_list);

  cout &lt;&lt; <span class="stringliteral">&quot;Sub-matrix (no more symmetric):&quot;</span> &lt;&lt; endl;
  SubB.Print();

  <span class="comment">// Assignments in the sub-matrix.</span>
  <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; 2; i++)
    <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; 3; j++)
      SubB(i, j) = -1.;

  <span class="comment">// &#39;B&#39; will remain symmetric.</span>
  cout &lt;&lt; <span class="stringliteral">&quot;Complete matrix after the sub-matrix is filled with -1:&quot;</span> &lt;&lt; endl;
  B.Print();

  END;

  <span class="keywordflow">return</span> 0;

}
</pre></div><p>Output:</p>
<div class="fragment"><pre class="fragment">
Complete matrix:
0	1	2	3	4	5	
6	7	8	9	10	11	
12	13	14	15	16	17	
18	19	20	21	22	23	
Sub-matrix:
6	7	11	
12	13	17	
Multiplied by X = [0	1	2]:
29	47	

Complete matrix:
0	1	3	6	
1	2	4	7	
3	4	5	8	
6	7	8	9	
Sub-matrix (no more symmetric):
1	4	7	
6	8	9	
Complete matrix after the sub-matrix is filled with -1:
0	-1	3	-1	
-1	2	-1	-1	
3	-1	5	-1	
-1	-1	-1	-1	
</pre></div> </div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
