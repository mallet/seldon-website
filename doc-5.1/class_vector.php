<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Dense Vectors </h1>  </div>
</div>
<div class="contents">
<p>Vectors contains contiguous elements. In that page, methods and functions related to dense vectors are detailed. </p>
<h2>Basic declaration :</h2>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// dense vector of doubles
Vector&lt;double&gt; U;

// dense vector of integers : IVect or Vector&lt;int&gt;
IVect num;

// with a different allocator
Vector&lt;float, Vect_Full, NewAlloc&lt;float&gt; &gt; V;
</pre></div>  </pre><h2>Methods :</h2>
<table  class="category-table">
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#constructor">Vector constructors </a>   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#operator">Vector operators </a>   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getm">GetM </a>  </td><td class="category-table-td">returns the number of elements in the vector   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getm">GetLength </a>  </td><td class="category-table-td">returns the number of elements in the vector   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getm">GetSize </a>  </td><td class="category-table-td">returns the number of elements in the vector   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getdata">GetData </a>  </td><td class="category-table-td">returns a pointer to the array contained in the vector   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getdata">GetDataConst </a>  </td><td class="category-table-td">returns a pointer to the array contained in the vector   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getdata">GetDataVoid </a>  </td><td class="category-table-td">returns a pointer to the array contained in the vector   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getdata">GetDataConstVoid </a>  </td><td class="category-table-td">returns a pointer to the array contained in the vector   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#clear">Clear </a>  </td><td class="category-table-td">removes all elements of the vector   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#reallocate">Reallocate </a>  </td><td class="category-table-td">changes the size of vector (removes previous elements)   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#resize">Resize </a>  </td><td class="category-table-td">changes the size of vector (keeps previous elements)   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#setdata">SetData </a>  </td><td class="category-table-td">sets the pointer to the array contained in the vector  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#nullify">Nullify </a>  </td><td class="category-table-td">clears the vector without releasing memory  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#copy">Copy </a>  </td><td class="category-table-td">copies a vector  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#pushback">PushBack </a>  </td><td class="category-table-td">adds an element to the end of the vector  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getm">GetDataSize </a>  </td><td class="category-table-td">returns the number of elements in the vector  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#zero">Zero </a>  </td><td class="category-table-td">sets all elements to zero   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#fill">Fill </a>  </td><td class="category-table-td">sets all elements to a given value   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#fillrand">FillRand </a>  </td><td class="category-table-td">fills randomly the vector   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getnorminf">GetNormInf </a>  </td><td class="category-table-td">returns highest absolute value   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getnorminf">GetNormInfIndex </a>  </td><td class="category-table-td">returns the index of the highest absolute value   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#print">Print </a>  </td><td class="category-table-td">displays the vector   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#write">Write </a>  </td><td class="category-table-td">writes the vector in binary format   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#read">Read </a>  </td><td class="category-table-td">reads the vector in binary format   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#writetext">WriteText </a>  </td><td class="category-table-td">writes the vector in text format   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#readtext">ReadText </a>  </td><td class="category-table-td">reads the vector in text format   </td></tr>
</table>
<h2>Functions :</h2>
<table  class="category-table">
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_blas.php#mlt">Mlt </a> </td><td class="category-table-td">multiplies the elements of the vector by a scalar   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_blas.php#add">Add </a> </td><td class="category-table-td">adds two vectors   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_blas.php#copy">Copy </a> </td><td class="category-table-td">copies one vector into another one   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_blas.php#swap">Swap </a> </td><td class="category-table-td">exchanges two vectors   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_blas.php#genrot">ApplyRot </a> </td><td class="category-table-td">applies rotation to 2-D points   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_blas.php#genrot">ApplyModifRot </a> </td><td class="category-table-td">applies rotation to 2-D points   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_blas.php#dotprod">DotProd </a> </td><td class="category-table-td">scalar product between two vectors  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_blas.php#dotprod">DotProdConj </a> </td><td class="category-table-td">scalar product between two vectors, first vector being conjugated  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_blas.php#conjugate">Conjugate </a> </td><td class="category-table-td">conjugates a vector  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_blas.php#getmaxabsindex">GetMaxAbsIndex </a> </td><td class="category-table-td">returns index where highest absolute value is reached   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_blas.php#norm1">Norm1 </a> </td><td class="category-table-td">returns 1-norm of a vector   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_blas.php#norm2">Norm2 </a> </td><td class="category-table-td">returns 2-norm of a vector  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_vector.php#quicksort">QuickSort </a> </td><td class="category-table-td">sorts a vector with quick sort algorithm  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_vector.php#mergesort">MergeSort </a> </td><td class="category-table-td">sorts a vector with merge sort algorithm  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_vector.php#sort">Sort </a> </td><td class="category-table-td">sorts a vector  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_vector.php#assemble">Assemble </a> </td><td class="category-table-td">assembles a vector  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_vector.php#removeduplicate">RemoveDuplicate </a> </td><td class="category-table-td">sorts and removes duplicate elements of a vector  </td></tr>
</table>
<div class="separator"><a class="anchor" id="constructor"></a></div><h3>Vector constructors</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  Vector();
  Vector(const Vector&amp; X );
  Vector(int n);
</pre><h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// default constructor -&gt; empty vector
Vector&lt;int&gt; V;
cout &lt;&lt; "Number of elements "&lt;&lt; V.GetM() &lt;&lt; endl; // It should return 0 
// then you can use Reallocate to set the size
V.Reallocate(3);
V.Fill();

// copy constructor (V -&gt; U)
Vector&lt;int&gt; V = U;

// constructor specifying the size of V
Vector&lt;double&gt; W(4);
// W is not initialized, you have to fill it
W.Fill(1.0);
</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="#reallocate">Reallocate</a><br/>
 <a href="#fill">Fill</a></p>
<h4>Location :</h4>
<p>Class <code>Vector&lt;T, Vect_Full&gt;</code><br/>
 <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a><br/>
 <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a></p>
<div class="separator"><a class="anchor" id="operator"></a></div><h3>Vector operators</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  const T&amp; operator (int) const;
  T&amp; operator (int);
  Vector&amp; operator =(const Vector&amp; )
  Vector&amp; operator =(const T0&amp; alpha)
  Vector&amp; operator *=(const T0&amp; alpha)
</pre><h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;string&gt; V(3);
// use of operator () to modify vector
V(0) = string("element 0");
V(1) = V(0) + string(" and 1");

Vector&lt;string&gt; W;
// use of operator = to copy contents of vector V
W = V;

// you can set all elements to a given value
Vector&lt;double&gt; U(3);
U = 1; // all elements of U are equal to 1

// multiplication by a scalar
U.Fill();
U *= 1.5;
</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="#copy">Copy</a></p>
<h4>Location :</h4>
<p>Class <code>Vector&lt;T, Vect_Full&gt;</code><br/>
 <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a><br/>
 <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a></p>
<div class="separator"><a class="anchor" id="getm"></a></div><h3>GetM, GetLength, GetSize, GetDataSize</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int GetM() const;
  int GetLength() const;
  int GetSize() const;
  int GetDataSize() const;
</pre><p>All those methods are identic and return the number of elements contained in the vector. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;float&gt; V(3);
cout &lt;&lt; "Number of elements of V " &lt;&lt; V.GetM() &lt;&lt; endl;
V.Reallocate(5);
cout &lt;&lt; "Number of elements of V " &lt;&lt; V.GetSize() &lt;&lt; endl;
</pre></div>  </pre><h4>Location :</h4>
<p>Class <code>Vector_Base</code><br/>
 <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a><br/>
 <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a></p>
<div class="separator"><a class="anchor" id="getdata"></a></div><h3>GetData, GetDataConst, GetDataVoid, GetDataConstVoid</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  T* GetData() const;
  const T* GetDataConst() const;
  void* GetDataVoid() const;
  const void* GetDataConstVoid() const;
</pre><p>Those methods are useful to retrieve the pointer to the array. In practice, you can use those methods in order to interface with C/fortran subroutines or to perform some low level operations. But in this last case, you have to be careful, because debugging operations will be more tedious.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;double&gt; V(3); V.Fill();
double* data = V.GetData();
// you can use data as a normal C array
// here the sum of elements is computed
double sum = 0;
for (int i = 0; i &lt; V.GetM(); i++)
  sum += data[i];

// this would be equivalent and safer to write
sum = 0;
for (int i = 0; i &lt; V.GetM(); i++)
  sum += V(i);

// if you want to call a fortran subroutine daxpy
Vector&lt;double&gt; X(3), Y(3); 
double coef = 2.0;
// in this case, X is constant
int n = X.GetM();
daxpy_(&amp;amp; coef, &amp;amp; n, X.GetDataConst(), Y.GetData());

// for complex numbers, conversion to void* is needed :
Vector&lt;complex&lt;double&gt; &gt; Xc(4), Yc(4);
complex&lt;double&gt; beta(1,1);
zaxpy(reinterpret_cast&lt;const void*&gt;(beta),
      Xc.GetDataConstVoid(), Yc.GetDataVoid());
</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="#setdata">SetData</a><br/>
 <a href="#nullify">Nullify</a></p>
<h4>Location :</h4>
<p>Class <code>Vector_Base</code><br/>
 <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a><br/>
 <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a></p>
<div class="separator"><a class="anchor" id="clear"></a></div><h3>Clear</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Clear();
</pre><p>This method removes all the elements of the vector.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;double&gt; V(3);
V.Fill();
// clears vector V
V.Clear();
</pre></div>  </pre><h4>Location :</h4>
<p>Class <code>Vector&lt;T, Vect_Full&gt;</code><br/>
 <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a><br/>
 <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a></p>
<div class="separator"><a class="anchor" id="reallocate"></a></div><h3>Reallocate</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Reallocate(int);
</pre><p>This method changes the size of the vector, but removes previous elements.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;long int&gt; V(5);
V.Fill();
// resizes vector V
V.Reallocate(20);
// you need to initialize all vector
V.Zero();
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#resize">Resize</a></p>
<h4>Location :</h4>
<p>Class <code>Vector&lt;T, Vect_Full&gt;</code><br/>
 <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a><br/>
 <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a></p>
<div class="separator"><a class="anchor" id="resize"></a></div><h3>Resize</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Resize(int);
</pre><p>This method changes the size of the vector, and keeps previous elements. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;long double&gt; V(5); V.Fill();
// resizes vector V
V.Resize(20);
// you need to initialize new elements if there are new
for (int i = 5; i &lt; 20; i++)
  V(i) = 0;
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#reallocate">Reallocate</a></p>
<h4>Location :</h4>
<p>Class <code>Vector&lt;T, Vect_Full&gt;</code><br/>
 <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a><br/>
 <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a></p>
<div class="separator"><a class="anchor" id="setdata"></a></div><h3>SetData</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void SetData(int, T*);
</pre><p>This method sets the pointer to the array containing elements. This method should be used carefully, and generally in conjunction with method Nullify.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// for example, you can define a function with a pointer as argument
void f(double* data)
{
  // and sets this array into a Vector instance
  Vector&lt;double&gt; V;
  V.SetData(5, data);
  // then you use a C++ method
  double rhs = Norm2(V);
  // you don't release memory, because data is used after the function
  V.Nullify();
}
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#getdata">GetData</a><br/>
 <a href="#nullify">Nullify</a></p>
<h4>Location :</h4>
<p>Class <code>Vector&lt;T, Vect_Full&gt;</code><br/>
 <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a><br/>
 <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a></p>
<div class="separator"><a class="anchor" id="nullify"></a></div><h3>Nullify</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Nullify();
</pre><p>This method clears the vector without releasing memory. This method should be used carefully, and generally in conjunction with method SetData. You can look at the example shown in the explanation of method SetData.</p>
<h4>Related topics :</h4>
<p><a href="#setdata">SetData</a><br/>
 <a href="#getdata">GetData</a></p>
<h4>Location :</h4>
<p>Class <code>Vector&lt;T, Vect_Full&gt;</code><br/>
 <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a><br/>
 <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a></p>
<div class="separator"><a class="anchor" id="copy"></a></div><h3>Copy</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Copy(const Vector&lt;T&gt;&amp;);
</pre><p>This method copies a vector into the current vector.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// copy of a vector V
Vector&lt;double&gt; V(10), W;
V.FillRand();
W.Copy(V);
// this is equivalent to use operator =
W = V;
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#operator">Vector operators</a></p>
<h4>Location :</h4>
<p>Class <code>Vector&lt;T, Vect_Full&gt;</code><br/>
 <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a><br/>
 <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a></p>
<div class="separator"><a class="anchor" id="pushback"></a></div><h3>PushBack</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void PushBack(const T0&amp;);
  void PushBack(const Vector&lt;T&gt;&amp;);
</pre><p>This method inserts a single element, or a vector to the end of the vector.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;double&gt; V;
// a single element is appended
V.PushBack(1.0);
// now another vector is appended
Vector&lt;double&gt; W(2);
W(0) = 1.5; W(1) = -1.0;
V.PushBack(W);
// W should contain [1.0 1.5 -1.0]
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#resize">Resize</a></p>
<h4>Location :</h4>
<p>Class <code>Vector&lt;T, Vect_Full&gt;</code><br/>
 <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a><br/>
 <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a></p>
<div class="separator"><a class="anchor" id="zero"></a></div><h3>Zero</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Zero();
</pre><p>This method fills memory of 0, is convenient for vector made of doubles, integers, floats, but not for more complicated types. In that case, it is better to use the method <a href="#fill">Fill</a>. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;double&gt; V(5);
// initialization
V.Fill();

Vector&lt;IVect&gt; W(10);
// W.Zero() is incorrect and would generate an error at the execution
// a good initialization is to use Fill
IVect zero(5); zero.Zero();
W.Fill(zero);
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#fill">Fill</a></p>
<h4>Location :</h4>
<p>Class <code>Vector&lt;T, Vect_Full&gt;</code><br/>
 <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a><br/>
 <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a></p>
<div class="separator"><a class="anchor" id="fill"></a></div><h3>Fill</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Fill();
  void Fill(const T0&amp; );
</pre><p>This method fills vector with 0, 1, 2, etc or with a given value.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;int&gt; V(5);
V.Fill();
// V should contain [0 1 2 3 4]

V.Fill(2);
// V should contain [2 2 2 2 2]
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#zero">Zero</a></p>
<h4>Location :</h4>
<p>Class <code>Vector&lt;T, Vect_Full&gt;</code><br/>
 <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a><br/>
 <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a></p>
<div class="separator"><a class="anchor" id="fillrand"></a></div><h3>FillRand</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void FillRand();
</pre><p>This method fills the vector randomly.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;double&gt; V(5);
V.FillRand();
// V should contain 5 random values
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#fill">Fill</a></p>
<h4>Location :</h4>
<p>Class <code>Vector&lt;T, Vect_Full&gt;</code><br/>
 <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a><br/>
 <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a></p>
<div class="separator"><a class="anchor" id="getnorminf"></a></div><h3>GetNormInf, GetNormInfIndex</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  T GetNormInf() const;
  int GetNormInfIndex() const;
</pre><p><code>GetNormInf</code> returns the highest absolute value whereas <code>GetNormInfIndex</code> returns the index where this maximum is reached. This method does not work for complex numbers.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;int&gt; V(3);
V(0) = 1; V(1) = -2; V(3) = 0;
int imax = V.GetNormInf(); // should return 2
int pos = V.GetNormInfIndex(); // should return 1
</pre></div>  </pre><h4>Location :</h4>
<p>Class <code>Vector&lt;T, Vect_Full&gt;</code><br/>
 <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a><br/>
 <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a></p>
<div class="separator"><a class="anchor" id="print"></a></div><h3>Print</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Print() const;
</pre><p>This method displays the vector.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;string&gt; V(2);
V.PushBack("hello");
V.PushBack("world");
V.Print(); // should display "hello world"
</pre></div>  </pre><h4>Location :</h4>
<p>Class <code>Vector&lt;T, Vect_Full&gt;</code><br/>
 <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a><br/>
 <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a></p>
<div class="separator"><a class="anchor" id="write"></a></div><h3>Write</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Write(string) const;
  void Write(ofstream&amp;) const;
</pre><p>This method writes the vector on a file/stream in binary format. The file will contain the number of elements, then the list of elements.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;double&gt; V(2); 
// you can write directly in a file
V.Fill();
V.Write("vector.dat");

// or open a stream with other datas
ofstream file_out("vector.dat");
int my_info = 3;
file_out.write(reinterpret_cast&lt;char*&gt;(&amp;amp;my_info), sizeof(int));
V.Write(file_out);
file_out.close();
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#read">Read</a><br/>
 <a href="#writetext">WriteText</a><br/>
 <a href="#readtext">ReadText</a></p>
<h4>Location :</h4>
<p>Class <code>Vector&lt;T, Vect_Full&gt;</code><br/>
 <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a><br/>
 <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a></p>
<div class="separator"><a class="anchor" id="read"></a></div><h3>Read</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Read(string);
  void Read(istream&amp;);
</pre><p>This method sets the vector from a file/stream in binary format. The file contains the number of elements, then the list of elements. The method ReadText can also be useful to initialize an array with a string. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;double&gt; V; 
// you can read directly on a file
V.Read("vector.dat");

// or read from a stream
ifstream file_in("vector.dat");
int my_info;
file_in.read(reinterpret_cast&lt;char*&lt;(&amp;amp;my_info), sizeof(int));
V.Read(file_in);
file_in.close();

// or initialize values of a vector with a string
string values("1.23  -4.1  2.5  0.1 0.6 -0.7");
istringstream stream_data(values);
V.ReadText(stream_data);
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#write">Write</a><br/>
 <a href="#writetext">WriteText</a><br/>
 <a href="#readtext">ReadText</a></p>
<h4>Location :</h4>
<p>Class <code>Vector&lt;T, Vect_Full&gt;</code><br/>
 <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a><br/>
 <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a></p>
<div class="separator"><a class="anchor" id="writetext"></a></div><h3>WriteText</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void WriteText(string) const;
  void WriteText(ofstream&amp;) const;
</pre><p>This method writes the vector on a file/stream in text format. The file will contain the list of elements.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;double&gt; V(2); 
// you can write directly in a file
V.Fill();
V.WriteText("vector.dat");

// or open a stream with other datas
ofstream file_out("vector.dat");
int my_info = 3;
file_out &lt;&lt; my_info &lt;&lt; '\n';
V.WriteText(file_out);
file_out.close();
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#write">Write</a><br/>
 <a href="#read">Read</a><br/>
 <a href="#readtext">ReadText</a></p>
<h4>Location :</h4>
<p>Class <code>Vector&lt;T, Vect_Full&gt;</code><br/>
 <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a><br/>
 <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a></p>
<div class="separator"><a class="anchor" id="readtext"></a></div><h3>ReadText</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void ReadText(string);
  void ReadText(istream&amp;);
</pre><p>This method sets the vector from a file/stream in text format. The file contains the list of elements.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;double&gt; V; 
// you can read directly on a file
V.ReadText("vector.dat");

// or read from a stream
ifstream file_in("vector.dat");
int my_info;
file_in &gt;&gt; my_info;
V.ReadText(file_in);
file_in.close();
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#write">Write</a><br/>
 <a href="#read">Read</a><br/>
 <a href="#writetext">WriteText</a></p>
<h4>Location :</h4>
<p>Class <code>Vector&lt;T, Vect_Full&gt;</code><br/>
 <a class="el" href="_vector_8hxx_source.php">Vector.hxx</a><br/>
 <a class="el" href="_vector_8cxx_source.php">Vector.cxx</a> </p>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
