<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/interfaces/direct/Pastix.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2010 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 <span class="preprocessor">#ifndef SELDON_FILE_PASTIX_CXX</span>
<a name="l00020"></a>00020 <span class="preprocessor"></span>
<a name="l00021"></a>00021 <span class="preprocessor">#include &quot;Pastix.hxx&quot;</span>
<a name="l00022"></a>00022 
<a name="l00023"></a>00023 <span class="keyword">namespace </span>Seldon
<a name="l00024"></a>00024 {
<a name="l00025"></a>00025 
<a name="l00027"></a>00027   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00028"></a>00028   <a class="code" href="class_seldon_1_1_matrix_pastix.php#a862e60553d3532ab99a3645f219d782d" title="Default constructor.">MatrixPastix&lt;T&gt;::MatrixPastix</a>()
<a name="l00029"></a>00029   {
<a name="l00030"></a>00030     pastix_data = NULL;
<a name="l00031"></a>00031     n = 0;
<a name="l00032"></a>00032     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; 64; i++)
<a name="l00033"></a>00033       {
<a name="l00034"></a>00034         iparm[i] = 0;
<a name="l00035"></a>00035         dparm[i] = 0;
<a name="l00036"></a>00036       }
<a name="l00037"></a>00037 
<a name="l00038"></a>00038     <span class="comment">// Factorization of a matrix on a single processor.</span>
<a name="l00039"></a>00039     distributed = <span class="keyword">false</span>;
<a name="l00040"></a>00040 
<a name="l00041"></a>00041     <span class="comment">// No refinement by default.</span>
<a name="l00042"></a>00042     refine_solution = <span class="keyword">false</span>;
<a name="l00043"></a>00043 
<a name="l00044"></a>00044     <span class="comment">// initializing parameters</span>
<a name="l00045"></a>00045     pastix_initParam(iparm, dparm);
<a name="l00046"></a>00046 
<a name="l00047"></a>00047     iparm[IPARM_RHS_MAKING] = API_RHS_B;
<a name="l00048"></a>00048     iparm[IPARM_VERBOSE] = API_VERBOSE_NOT;
<a name="l00049"></a>00049     <span class="comment">// iparm[IPARM_VERBOSE] = API_VERBOSE_NO;</span>
<a name="l00050"></a>00050   }
<a name="l00051"></a>00051 
<a name="l00052"></a>00052 
<a name="l00054"></a>00054   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00055"></a>00055   <a class="code" href="class_seldon_1_1_matrix_pastix.php#a06e7866292ac5b30289cf566d56b0caf" title="destructor">MatrixPastix&lt;T&gt;::~MatrixPastix</a>()
<a name="l00056"></a>00056   {
<a name="l00057"></a>00057     <a class="code" href="class_seldon_1_1_matrix_mumps.php#a8d3c03fd1f5e00dbbbb39e8f9e1cc4ee" title="clears factorization">Clear</a>();
<a name="l00058"></a>00058   }
<a name="l00059"></a>00059 
<a name="l00060"></a>00060 
<a name="l00062"></a>00062   <span class="keyword">template</span>&lt;&gt;
<a name="l00063"></a>00063   <span class="keywordtype">void</span> MatrixPastix&lt;double&gt;::
<a name="l00064"></a>00064   CallPastix(<span class="keyword">const</span> MPI_Comm&amp; comm, pastix_int_t* colptr, pastix_int_t* row,
<a name="l00065"></a>00065              <span class="keywordtype">double</span>* val, <span class="keywordtype">double</span>* b, pastix_int_t nrhs)
<a name="l00066"></a>00066   {
<a name="l00067"></a>00067     <span class="keywordflow">if</span> (distributed)
<a name="l00068"></a>00068       d_dpastix(&amp;pastix_data, comm, n, colptr, row, val,
<a name="l00069"></a>00069                 col_num.GetData(), perm.GetData(), invp.GetData(),
<a name="l00070"></a>00070                 b, nrhs, iparm, dparm);
<a name="l00071"></a>00071     <span class="keywordflow">else</span>
<a name="l00072"></a>00072       d_pastix(&amp;pastix_data, comm, n, colptr, row, val,
<a name="l00073"></a>00073                perm.GetData(), invp.GetData(), b, nrhs, iparm, dparm);
<a name="l00074"></a>00074   }
<a name="l00075"></a>00075 
<a name="l00076"></a>00076 
<a name="l00078"></a>00078   <span class="keyword">template</span>&lt;&gt;
<a name="l00079"></a>00079   <span class="keywordtype">void</span> MatrixPastix&lt;complex&lt;double&gt; &gt;::
<a name="l00080"></a>00080   CallPastix(<span class="keyword">const</span> MPI_Comm&amp; comm, pastix_int_t* colptr, pastix_int_t* row,
<a name="l00081"></a>00081              complex&lt;double&gt;* val, complex&lt;double&gt;* b, pastix_int_t nrhs)
<a name="l00082"></a>00082   {
<a name="l00083"></a>00083     <span class="keywordflow">if</span> (distributed)
<a name="l00084"></a>00084       z_dpastix(&amp;pastix_data, comm, n, colptr, row,
<a name="l00085"></a>00085                 reinterpret_cast&lt;DCOMPLEX*&gt;(val),
<a name="l00086"></a>00086                 col_num.GetData(), perm.GetData(), invp.GetData(),
<a name="l00087"></a>00087                 <span class="keyword">reinterpret_cast&lt;</span>DCOMPLEX*<span class="keyword">&gt;</span>(b), nrhs, iparm, dparm);
<a name="l00088"></a>00088     <span class="keywordflow">else</span>
<a name="l00089"></a>00089       <span class="comment">/* z_dpastix(&amp;pastix_data, comm, n, colptr, row,</span>
<a name="l00090"></a>00090 <span class="comment">         reinterpret_cast&lt;DCOMPLEX*&gt;(val),</span>
<a name="l00091"></a>00091 <span class="comment">         col_num.GetData(), perm.GetData(), invp.GetData(),</span>
<a name="l00092"></a>00092 <span class="comment">         reinterpret_cast&lt;DCOMPLEX*&gt;(b), nrhs, iparm, dparm); */</span>
<a name="l00093"></a>00093       z_pastix(&amp;pastix_data, comm, n, colptr, row,
<a name="l00094"></a>00094                reinterpret_cast&lt;DCOMPLEX*&gt;(val),
<a name="l00095"></a>00095                perm.GetData(), invp.GetData(),
<a name="l00096"></a>00096                <span class="keyword">reinterpret_cast&lt;</span>DCOMPLEX*<span class="keyword">&gt;</span>(b), nrhs, iparm, dparm);
<a name="l00097"></a>00097   }
<a name="l00098"></a>00098 
<a name="l00099"></a>00099 
<a name="l00101"></a>00101   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00102"></a>00102   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a0840578aaa2639d33d5c47adea893cf8" title="Clearing factorization.">MatrixPastix&lt;T&gt;::Clear</a>()
<a name="l00103"></a>00103   {
<a name="l00104"></a>00104     <span class="keywordflow">if</span> (n &gt; 0)
<a name="l00105"></a>00105       {
<a name="l00106"></a>00106         pastix_int_t nrhs = 1;
<a name="l00107"></a>00107         iparm[IPARM_START_TASK] = API_TASK_CLEAN;
<a name="l00108"></a>00108         iparm[IPARM_END_TASK] = API_TASK_CLEAN;
<a name="l00109"></a>00109 
<a name="l00110"></a>00110         CallPastix(MPI_COMM_WORLD, NULL, NULL, NULL, NULL, nrhs);
<a name="l00111"></a>00111 
<a name="l00112"></a>00112         perm.Clear();
<a name="l00113"></a>00113         invp.Clear();
<a name="l00114"></a>00114         col_num.Clear();
<a name="l00115"></a>00115         n = 0;
<a name="l00116"></a>00116         pastix_data = NULL;
<a name="l00117"></a>00117         distributed = <span class="keyword">false</span>;
<a name="l00118"></a>00118 
<a name="l00119"></a>00119         MPI_Comm_free(&amp;comm_facto);
<a name="l00120"></a>00120       }
<a name="l00121"></a>00121   }
<a name="l00122"></a>00122 
<a name="l00123"></a>00123 
<a name="l00125"></a>00125   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00126"></a>00126   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a29ac5cb9e5315ae68d6fc5d1e97eef71" title="no message will be displayed">MatrixPastix&lt;T&gt;::HideMessages</a>()
<a name="l00127"></a>00127   {
<a name="l00128"></a>00128     iparm[IPARM_VERBOSE] = API_VERBOSE_NOT;
<a name="l00129"></a>00129   }
<a name="l00130"></a>00130 
<a name="l00131"></a>00131 
<a name="l00133"></a>00133   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00134"></a>00134   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a0ca315759b1dd24114ff47958c37cb96" title="Low level of display.">MatrixPastix&lt;T&gt;::ShowMessages</a>()
<a name="l00135"></a>00135   {
<a name="l00136"></a>00136     iparm[IPARM_VERBOSE] = API_VERBOSE_NO;
<a name="l00137"></a>00137   }
<a name="l00138"></a>00138 
<a name="l00140"></a>00140   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00141"></a>00141   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a6f6a15165b8961d62505dc2ff4f261e0" title="You can require that solution is refined after LU resolution.">MatrixPastix&lt;T&gt;::RefineSolution</a>()
<a name="l00142"></a>00142   {
<a name="l00143"></a>00143     refine_solution = <span class="keyword">true</span>;
<a name="l00144"></a>00144   }
<a name="l00145"></a>00145 
<a name="l00146"></a>00146 
<a name="l00148"></a>00148   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00149"></a>00149   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#af8a85f37469fa702b6ea1ebd2a2b4b0e" title="You can require that solution is not refined (faster).">MatrixPastix&lt;T&gt;::DoNotRefineSolution</a>()
<a name="l00150"></a>00150   {
<a name="l00151"></a>00151     refine_solution = <span class="keyword">false</span>;
<a name="l00152"></a>00152   }
<a name="l00153"></a>00153 
<a name="l00154"></a>00154 
<a name="l00155"></a>00155   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00156"></a>00156   <span class="keywordtype">void</span> MatrixPastix&lt;T&gt;::CreateCommunicator()
<a name="l00157"></a>00157   {
<a name="l00158"></a>00158     MPI_Group single_group;
<a name="l00159"></a>00159     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#a06ba655df528009ca61f4dd767850f68" title="rank of processor">rank</a>, nb_cpu;
<a name="l00160"></a>00160     MPI_Comm_rank(MPI_COMM_WORLD, &amp;rank);
<a name="l00161"></a>00161     MPI_Comm_size(MPI_COMM_WORLD, &amp;nb_cpu);
<a name="l00162"></a>00162 
<a name="l00163"></a>00163     <span class="keywordflow">if</span> (distributed)
<a name="l00164"></a>00164       {
<a name="l00165"></a>00165         IVect rank_(nb_cpu); rank_.Fill();
<a name="l00166"></a>00166         MPI_Comm_group(MPI_COMM_WORLD, &amp;single_group);
<a name="l00167"></a>00167         MPI_Group_incl(single_group, nb_cpu, rank_.GetData(), &amp;single_group);
<a name="l00168"></a>00168         MPI_Comm_create(MPI_COMM_WORLD, single_group, &amp;comm_facto);
<a name="l00169"></a>00169       }
<a name="l00170"></a>00170     <span class="keywordflow">else</span>
<a name="l00171"></a>00171       {
<a name="l00172"></a>00172         MPI_Comm_group(MPI_COMM_WORLD, &amp;single_group);
<a name="l00173"></a>00173         MPI_Group_incl(single_group, 1, &amp;rank, &amp;single_group);
<a name="l00174"></a>00174         MPI_Comm_create(MPI_COMM_WORLD, single_group, &amp;comm_facto);
<a name="l00175"></a>00175       }
<a name="l00176"></a>00176   }
<a name="l00177"></a>00177 
<a name="l00178"></a>00178 
<a name="l00180"></a>00180   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00181"></a><a class="code" href="class_seldon_1_1_matrix_pastix.php#a69e64cad0fe1938748e49c0d0057bd66">00181</a>   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator, <span class="keyword">class</span> T<span class="keywordtype">int</span>&gt;
<a name="l00182"></a>00182   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a69e64cad0fe1938748e49c0d0057bd66" title="Returning ordering found by Scotch.">MatrixPastix&lt;T&gt;::</a>
<a name="l00183"></a>00183 <a class="code" href="class_seldon_1_1_matrix_pastix.php#a69e64cad0fe1938748e49c0d0057bd66" title="Returning ordering found by Scotch.">  FindOrdering</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop, Storage, Allocator&gt;</a> &amp; mat,
<a name="l00184"></a>00184                <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Tint&gt;</a>&amp; numbers, <span class="keywordtype">bool</span> keep_matrix)
<a name="l00185"></a>00185   {
<a name="l00186"></a>00186     <span class="comment">// We clear the previous factorization, if any.</span>
<a name="l00187"></a>00187     <a class="code" href="class_seldon_1_1_matrix_pastix.php#a0840578aaa2639d33d5c47adea893cf8" title="Clearing factorization.">Clear</a>();
<a name="l00188"></a>00188 
<a name="l00189"></a>00189     <a class="code" href="class_seldon_1_1_matrix_pastix.php#ab0637a34811226bdedf63b3e71c7cef6" title="if true, resolution on several nodes">distributed</a> = <span class="keyword">false</span>;
<a name="l00190"></a>00190     CreateCommunicator();
<a name="l00191"></a>00191 
<a name="l00192"></a>00192     <a class="code" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e" title="number of columns">n</a> = mat.GetN();
<a name="l00193"></a>00193     <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e" title="number of columns">n</a> &lt;= 0)
<a name="l00194"></a>00194       <span class="keywordflow">return</span>;
<a name="l00195"></a>00195 
<a name="l00196"></a>00196     pastix_int_t nrhs = 1, nnz = 0;
<a name="l00197"></a>00197     pastix_int_t* ptr_ = NULL;
<a name="l00198"></a>00198     pastix_int_t* ind_ = NULL;
<a name="l00199"></a>00199     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;pastix_int_t&gt;</a> Ptr, Ind;
<a name="l00200"></a>00200 
<a name="l00201"></a>00201     iparm[IPARM_SYM] = API_SYM_YES;
<a name="l00202"></a>00202     iparm[IPARM_FACTORIZATION] = API_FACT_LDLT;
<a name="l00203"></a>00203     GetSymmetricPattern(mat, Ptr, Ind);
<a name="l00204"></a>00204     <span class="keywordflow">if</span> (!keep_matrix)
<a name="l00205"></a>00205       mat.Clear();
<a name="l00206"></a>00206 
<a name="l00207"></a>00207     ptr_ = Ptr.GetData();
<a name="l00208"></a>00208     <span class="comment">// Changing to 1-index notation.</span>
<a name="l00209"></a>00209     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt;= <a class="code" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e" title="number of columns">n</a>; i++)
<a name="l00210"></a>00210       ptr_[i]++;
<a name="l00211"></a>00211 
<a name="l00212"></a>00212     nnz = Ind.GetM();
<a name="l00213"></a>00213     ind_ = Ind.GetData();
<a name="l00214"></a>00214     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; nnz; i++)
<a name="l00215"></a>00215       ind_[i]++;
<a name="l00216"></a>00216 
<a name="l00217"></a>00217     <a class="code" href="class_seldon_1_1_matrix_pastix.php#a0e40bdc9ac7c0ae1bd80e05d9942afee" title="permutation arrays">perm</a>.Reallocate(<a class="code" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e" title="number of columns">n</a>); invp.Reallocate(<a class="code" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e" title="number of columns">n</a>);
<a name="l00218"></a>00218     <a class="code" href="class_seldon_1_1_matrix_pastix.php#a0e40bdc9ac7c0ae1bd80e05d9942afee" title="permutation arrays">perm</a>.Fill(); invp.Fill();
<a name="l00219"></a>00219 
<a name="l00220"></a>00220     <span class="comment">// We get ordering only.</span>
<a name="l00221"></a>00221     iparm[IPARM_START_TASK] = API_TASK_ORDERING;
<a name="l00222"></a>00222     iparm[IPARM_END_TASK] = API_TASK_ORDERING;
<a name="l00223"></a>00223 
<a name="l00224"></a>00224     CallPastix(<a class="code" href="class_seldon_1_1_matrix_pastix.php#a92ec4775edc88ab697268cc96b7c5848" title="MPI communicator.">comm_facto</a>, ptr_, ind_, NULL, NULL, nrhs);
<a name="l00225"></a>00225 
<a name="l00226"></a>00226     numbers = <a class="code" href="class_seldon_1_1_matrix_pastix.php#a0e40bdc9ac7c0ae1bd80e05d9942afee" title="permutation arrays">perm</a>;
<a name="l00227"></a>00227   }
<a name="l00228"></a>00228 
<a name="l00229"></a>00229 
<a name="l00231"></a><a class="code" href="class_seldon_1_1_matrix_pastix.php#a638bb937143f0aacd9f241d4c89febba">00231</a>   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00232"></a>00232   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a638bb937143f0aacd9f241d4c89febba" title="Factorization of unsymmetric matrix.">MatrixPastix&lt;T&gt;</a>
<a name="l00233"></a>00233 <a class="code" href="class_seldon_1_1_matrix_pastix.php#a638bb937143f0aacd9f241d4c89febba" title="Factorization of unsymmetric matrix.">  ::FactorizeMatrix</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, General, Storage, Allocator&gt;</a> &amp; mat,
<a name="l00234"></a>00234                     <span class="keywordtype">bool</span> keep_matrix)
<a name="l00235"></a>00235   {
<a name="l00236"></a>00236     <span class="comment">// we clear previous factorization if present</span>
<a name="l00237"></a>00237     Clear();
<a name="l00238"></a>00238 
<a name="l00239"></a>00239     distributed = <span class="keyword">false</span>;
<a name="l00240"></a>00240     CreateCommunicator();
<a name="l00241"></a>00241     n = mat.GetN();
<a name="l00242"></a>00242     <span class="keywordflow">if</span> (n &lt;= 0)
<a name="l00243"></a>00243       <span class="keywordflow">return</span>;
<a name="l00244"></a>00244 
<a name="l00245"></a>00245     pastix_int_t nrhs = 1, nnz = 0;
<a name="l00246"></a>00246     pastix_int_t* ptr_ = NULL;
<a name="l00247"></a>00247     pastix_int_t* ind_ = NULL;
<a name="l00248"></a>00248     T* values_ = NULL;
<a name="l00249"></a>00249     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;pastix_int_t, VectFull, MallocAlloc&lt;pastix_int_t&gt;</a> &gt; Ptr, IndRow;
<a name="l00250"></a>00250     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, MallocAlloc&lt;T&gt;</a> &gt; Val;
<a name="l00251"></a>00251 
<a name="l00252"></a>00252     iparm[IPARM_SYM] = API_SYM_NO;
<a name="l00253"></a>00253     iparm[IPARM_FACTORIZATION] = API_FACT_LU;
<a name="l00254"></a>00254 
<a name="l00255"></a>00255     <a class="code" href="class_seldon_1_1_general.php">General</a> prop;
<a name="l00256"></a>00256     ConvertToCSC(mat, prop, Ptr, IndRow, Val, <span class="keyword">true</span>);
<a name="l00257"></a>00257     <span class="keywordflow">if</span> (!keep_matrix)
<a name="l00258"></a>00258       mat.Clear();
<a name="l00259"></a>00259 
<a name="l00260"></a>00260     ptr_ = Ptr.GetData();
<a name="l00261"></a>00261     <span class="comment">// changing to 1-index notation</span>
<a name="l00262"></a>00262     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt;= n; i++)
<a name="l00263"></a>00263       ptr_[i]++;
<a name="l00264"></a>00264 
<a name="l00265"></a>00265     nnz = IndRow.GetM();
<a name="l00266"></a>00266     ind_ = IndRow.GetData();
<a name="l00267"></a>00267     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; nnz; i++)
<a name="l00268"></a>00268       ind_[i]++;
<a name="l00269"></a>00269 
<a name="l00270"></a>00270     values_ = Val.GetData();
<a name="l00271"></a>00271 
<a name="l00272"></a>00272     perm.Reallocate(n); invp.Reallocate(n);
<a name="l00273"></a>00273     perm.Fill(); invp.Fill();
<a name="l00274"></a>00274 
<a name="l00275"></a>00275     <span class="comment">// factorization only</span>
<a name="l00276"></a>00276     iparm[IPARM_START_TASK] = API_TASK_ORDERING;
<a name="l00277"></a>00277     iparm[IPARM_END_TASK] = API_TASK_NUMFACT;
<a name="l00278"></a>00278 
<a name="l00279"></a>00279     CallPastix(comm_facto, ptr_, ind_, values_, NULL, nrhs);
<a name="l00280"></a>00280 
<a name="l00281"></a>00281     <span class="keywordflow">if</span> (iparm[IPARM_VERBOSE] != API_VERBOSE_NOT)
<a name="l00282"></a>00282       cout &lt;&lt; <span class="stringliteral">&quot;Factorization successful&quot;</span> &lt;&lt; endl;
<a name="l00283"></a>00283   }
<a name="l00284"></a>00284 
<a name="l00285"></a>00285 
<a name="l00287"></a><a class="code" href="class_seldon_1_1_matrix_pastix.php#a328852f857f9e71c3065c4f9e4b37b06">00287</a>   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00288"></a>00288   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a638bb937143f0aacd9f241d4c89febba" title="Factorization of unsymmetric matrix.">MatrixPastix&lt;T&gt;::</a>
<a name="l00289"></a>00289 <a class="code" href="class_seldon_1_1_matrix_pastix.php#a638bb937143f0aacd9f241d4c89febba" title="Factorization of unsymmetric matrix.">  FactorizeMatrix</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Symmetric, Storage, Allocator&gt;</a> &amp; mat,
<a name="l00290"></a>00290                   <span class="keywordtype">bool</span> keep_matrix)
<a name="l00291"></a>00291   {
<a name="l00292"></a>00292     <span class="comment">// we clear previous factorization if present</span>
<a name="l00293"></a>00293     <a class="code" href="class_seldon_1_1_matrix_pastix.php#a0840578aaa2639d33d5c47adea893cf8" title="Clearing factorization.">Clear</a>();
<a name="l00294"></a>00294 
<a name="l00295"></a>00295     <a class="code" href="class_seldon_1_1_matrix_pastix.php#ab0637a34811226bdedf63b3e71c7cef6" title="if true, resolution on several nodes">distributed</a> = <span class="keyword">false</span>;
<a name="l00296"></a>00296     CreateCommunicator();
<a name="l00297"></a>00297     <a class="code" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e" title="number of columns">n</a> = mat.GetN();
<a name="l00298"></a>00298     <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e" title="number of columns">n</a> &lt;= 0)
<a name="l00299"></a>00299       <span class="keywordflow">return</span>;
<a name="l00300"></a>00300 
<a name="l00301"></a>00301     pastix_int_t nrhs = 1, nnz = 0;
<a name="l00302"></a>00302     pastix_int_t* ptr_ = NULL;
<a name="l00303"></a>00303     pastix_int_t* ind_ = NULL;
<a name="l00304"></a>00304 
<a name="l00305"></a>00305     T* values_ = NULL;
<a name="l00306"></a>00306     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;pastix_int_t, VectFull, MallocAlloc&lt;pastix_int_t&gt;</a> &gt; Ptr, IndRow;
<a name="l00307"></a>00307     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, MallocAlloc&lt;T&gt;</a> &gt; Val;
<a name="l00308"></a>00308 
<a name="l00309"></a>00309     <a class="code" href="class_seldon_1_1_symmetric.php">Symmetric</a> prop;
<a name="l00310"></a>00310     ConvertToCSR(mat, prop, Ptr, IndRow, Val);
<a name="l00311"></a>00311 
<a name="l00312"></a>00312     iparm[IPARM_SYM]           = API_SYM_YES;
<a name="l00313"></a>00313     iparm[IPARM_FACTORIZATION] = API_FACT_LDLT;
<a name="l00314"></a>00314     <span class="keywordflow">if</span> (!keep_matrix)
<a name="l00315"></a>00315       mat.Clear();
<a name="l00316"></a>00316 
<a name="l00317"></a>00317     ptr_ = Ptr.GetData();
<a name="l00318"></a>00318     <span class="comment">// changing to 1-index notation</span>
<a name="l00319"></a>00319     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt;= <a class="code" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e" title="number of columns">n</a>; i++)
<a name="l00320"></a>00320       ptr_[i]++;
<a name="l00321"></a>00321 
<a name="l00322"></a>00322     nnz = IndRow.GetM();
<a name="l00323"></a>00323     ind_ = IndRow.GetData();
<a name="l00324"></a>00324     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; nnz; i++)
<a name="l00325"></a>00325       ind_[i]++;
<a name="l00326"></a>00326 
<a name="l00327"></a>00327     values_ = Val.GetData();
<a name="l00328"></a>00328 
<a name="l00329"></a>00329     <a class="code" href="class_seldon_1_1_matrix_pastix.php#a0e40bdc9ac7c0ae1bd80e05d9942afee" title="permutation arrays">perm</a>.Reallocate(<a class="code" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e" title="number of columns">n</a>); invp.Reallocate(<a class="code" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e" title="number of columns">n</a>);
<a name="l00330"></a>00330     <a class="code" href="class_seldon_1_1_matrix_pastix.php#a0e40bdc9ac7c0ae1bd80e05d9942afee" title="permutation arrays">perm</a>.Fill(); invp.Fill();
<a name="l00331"></a>00331 
<a name="l00332"></a>00332     <span class="comment">// factorization only</span>
<a name="l00333"></a>00333     iparm[IPARM_START_TASK] = API_TASK_ORDERING;
<a name="l00334"></a>00334     iparm[IPARM_END_TASK] = API_TASK_NUMFACT;
<a name="l00335"></a>00335     <span class="comment">// iparm[IPARM_VERBOSE]          = 4;</span>
<a name="l00336"></a>00336 
<a name="l00337"></a>00337     CallPastix(<a class="code" href="class_seldon_1_1_matrix_pastix.php#a92ec4775edc88ab697268cc96b7c5848" title="MPI communicator.">comm_facto</a>, ptr_, ind_, values_, NULL, nrhs);
<a name="l00338"></a>00338   }
<a name="l00339"></a>00339 
<a name="l00340"></a>00340 
<a name="l00342"></a>00342   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2&gt;
<a name="l00343"></a>00343   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a74f16146dfc8aa871028cfa6086a184a" title="solving A x = b (A is already factorized)">MatrixPastix&lt;T&gt;::Solve</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator2&gt;</a>&amp; x)
<a name="l00344"></a>00344   {
<a name="l00345"></a>00345     <a class="code" href="class_seldon_1_1_matrix_pastix.php#a74f16146dfc8aa871028cfa6086a184a" title="solving A x = b (A is already factorized)">Solve</a>(SeldonNoTrans, x);
<a name="l00346"></a>00346   }
<a name="l00347"></a>00347 
<a name="l00348"></a>00348 
<a name="l00350"></a>00350   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Transpose_status&gt;
<a name="l00351"></a>00351   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_pastix.php#a74f16146dfc8aa871028cfa6086a184a" title="solving A x = b (A is already factorized)">MatrixPastix&lt;T&gt;::Solve</a>(<span class="keyword">const</span> Transpose_status&amp; TransA,
<a name="l00352"></a>00352                               Vector&lt;T, VectFull, Allocator2&gt;&amp; x)
<a name="l00353"></a>00353   {
<a name="l00354"></a>00354     pastix_int_t nrhs = 1;
<a name="l00355"></a>00355     T* rhs_ = x.GetData();
<a name="l00356"></a>00356 
<a name="l00357"></a>00357     iparm[IPARM_START_TASK] = API_TASK_SOLVE;
<a name="l00358"></a>00358     <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_matrix_pastix.php#a8c22604c5e88bb75086bde3f344cd4d5" title="if true, solution is refined">refine_solution</a>)
<a name="l00359"></a>00359       iparm[IPARM_END_TASK] = API_TASK_REFINE;
<a name="l00360"></a>00360     <span class="keywordflow">else</span>
<a name="l00361"></a>00361       iparm[IPARM_END_TASK] = API_TASK_SOLVE;
<a name="l00362"></a>00362 
<a name="l00363"></a>00363     CallPastix(<a class="code" href="class_seldon_1_1_matrix_pastix.php#a92ec4775edc88ab697268cc96b7c5848" title="MPI communicator.">comm_facto</a>, NULL, NULL, NULL, rhs_, nrhs);
<a name="l00364"></a>00364   }
<a name="l00365"></a>00365 
<a name="l00366"></a>00366 
<a name="l00367"></a>00367 <span class="preprocessor">#ifdef SELDON_WITH_MPI</span>
<a name="l00368"></a>00368 <span class="preprocessor"></span>
<a name="l00370"></a>00370   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00371"></a>00371   <span class="keywordtype">void</span> MatrixPastix&lt;T&gt;::SetNbThreadPerNode(<span class="keywordtype">int</span> nb_thread)
<a name="l00372"></a>00372   {
<a name="l00373"></a>00373     iparm[IPARM_THREAD_NBR] = nb_thread;
<a name="l00374"></a>00374   }
<a name="l00375"></a>00375 
<a name="l00376"></a>00376 
<a name="l00378"></a>00378   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00379"></a>00379   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Alloc1, <span class="keyword">class</span> Alloc2, <span class="keyword">class</span> Alloc3, <span class="keyword">class</span> T<span class="keywordtype">int</span>&gt;
<a name="l00380"></a>00380   <span class="keywordtype">void</span> MatrixPastix&lt;T&gt;::
<a name="l00381"></a>00381   FactorizeDistributedMatrix(Vector&lt;pastix_int_t, VectFull, Alloc1&gt;&amp; Ptr,
<a name="l00382"></a>00382                              Vector&lt;pastix_int_t, VectFull, Alloc2&gt;&amp; IndRow,
<a name="l00383"></a>00383                              Vector&lt;T, VectFull, Alloc3&gt;&amp; Val,
<a name="l00384"></a>00384                              <span class="keyword">const</span> Vector&lt;Tint&gt;&amp; glob_number,
<a name="l00385"></a>00385                              <span class="keywordtype">bool</span> sym, <span class="keywordtype">bool</span> keep_matrix)
<a name="l00386"></a>00386   {
<a name="l00387"></a>00387     <span class="comment">// we clear previous factorization if present</span>
<a name="l00388"></a>00388     <a class="code" href="class_seldon_1_1_matrix_pastix.php#a0840578aaa2639d33d5c47adea893cf8" title="Clearing factorization.">Clear</a>();
<a name="l00389"></a>00389 
<a name="l00390"></a>00390     <a class="code" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e" title="number of columns">n</a> = Ptr.GetM() - 1;
<a name="l00391"></a>00391     <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e" title="number of columns">n</a> &lt;= 0)
<a name="l00392"></a>00392       <span class="keywordflow">return</span>;
<a name="l00393"></a>00393 
<a name="l00394"></a>00394     <a class="code" href="class_seldon_1_1_matrix_pastix.php#ab0637a34811226bdedf63b3e71c7cef6" title="if true, resolution on several nodes">distributed</a> = <span class="keyword">true</span>;
<a name="l00395"></a>00395 
<a name="l00396"></a>00396     CreateCommunicator();
<a name="l00397"></a>00397 
<a name="l00398"></a>00398     iparm[IPARM_SYM] = API_SYM_YES;
<a name="l00399"></a>00399 
<a name="l00400"></a>00400     iparm[IPARM_FACTORIZATION] = API_FACT_LDLT;
<a name="l00401"></a>00401     iparm[IPARM_GRAPHDIST] = API_YES;
<a name="l00402"></a>00402 
<a name="l00403"></a>00403     <span class="keywordtype">int</span> rank;
<a name="l00404"></a>00404     MPI_Comm_rank(MPI_COMM_WORLD, &amp;rank);
<a name="l00405"></a>00405 
<a name="l00406"></a>00406     pastix_int_t* ptr_ = Ptr.GetData();
<a name="l00407"></a>00407     pastix_int_t nrhs = 1;
<a name="l00408"></a>00408     <span class="comment">// changing to 1-index notation</span>
<a name="l00409"></a>00409     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt;= <a class="code" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e" title="number of columns">n</a>; i++)
<a name="l00410"></a>00410       ptr_[i]++;
<a name="l00411"></a>00411 
<a name="l00412"></a>00412     pastix_int_t nnz = IndRow.GetM();
<a name="l00413"></a>00413     pastix_int_t* ind_ = IndRow.GetData();
<a name="l00414"></a>00414     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; nnz; i++)
<a name="l00415"></a>00415       ind_[i]++;
<a name="l00416"></a>00416 
<a name="l00417"></a>00417     T* values_ = Val.GetData();
<a name="l00418"></a>00418 
<a name="l00419"></a>00419     <a class="code" href="class_seldon_1_1_matrix_pastix.php#aeca666d886a9ab8e8e8e907963a6ed64" title="local to global">col_num</a>.Reallocate(<a class="code" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e" title="number of columns">n</a>);
<a name="l00420"></a>00420     <a class="code" href="class_seldon_1_1_matrix_pastix.php#a0e40bdc9ac7c0ae1bd80e05d9942afee" title="permutation arrays">perm</a>.Reallocate(<a class="code" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e" title="number of columns">n</a>); invp.Reallocate(<a class="code" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e" title="number of columns">n</a>);
<a name="l00421"></a>00421     <a class="code" href="class_seldon_1_1_matrix_pastix.php#a0e40bdc9ac7c0ae1bd80e05d9942afee" title="permutation arrays">perm</a>.Fill(); invp.Fill();
<a name="l00422"></a>00422     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e" title="number of columns">n</a>; i++)
<a name="l00423"></a>00423       <a class="code" href="class_seldon_1_1_matrix_pastix.php#aeca666d886a9ab8e8e8e907963a6ed64" title="local to global">col_num</a>(i) = glob_number(i)+1;
<a name="l00424"></a>00424 
<a name="l00425"></a>00425     <span class="comment">// factorization only</span>
<a name="l00426"></a>00426     <span class="comment">// iparm[IPARM_VERBOSE] = API_VERBOSE_YES;</span>
<a name="l00427"></a>00427     iparm[IPARM_START_TASK] = API_TASK_ORDERING;
<a name="l00428"></a>00428     iparm[IPARM_END_TASK] = API_TASK_NUMFACT;
<a name="l00429"></a>00429 
<a name="l00430"></a>00430     CallPastix(<a class="code" href="class_seldon_1_1_matrix_pastix.php#a92ec4775edc88ab697268cc96b7c5848" title="MPI communicator.">comm_facto</a>, ptr_, ind_, values_, NULL, nrhs);
<a name="l00431"></a>00431   }
<a name="l00432"></a>00432 
<a name="l00433"></a>00433 
<a name="l00434"></a>00434   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2, <span class="keyword">class</span> T<span class="keywordtype">int</span>&gt;
<a name="l00435"></a>00435   <span class="keywordtype">void</span> MatrixPastix&lt;T&gt;::SolveDistributed(Vector&lt;T, Vect_Full, Allocator2&gt;&amp; x,
<a name="l00436"></a>00436                                          <span class="keyword">const</span> Vector&lt;Tint&gt;&amp; glob_num)
<a name="l00437"></a>00437   {
<a name="l00438"></a>00438     SolveDistributed(SeldonNoTrans, x, glob_num);
<a name="l00439"></a>00439   }
<a name="l00440"></a>00440 
<a name="l00441"></a>00441 
<a name="l00442"></a>00442   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00443"></a>00443   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Transpose_status, <span class="keyword">class</span> T<span class="keywordtype">int</span>&gt;
<a name="l00444"></a>00444   <span class="keywordtype">void</span> MatrixPastix&lt;T&gt;::SolveDistributed(<span class="keyword">const</span> Transpose_status&amp; TransA,
<a name="l00445"></a>00445                                          Vector&lt;T, Vect_Full, Allocator2&gt;&amp; x,
<a name="l00446"></a>00446                                          <span class="keyword">const</span> Vector&lt;Tint&gt;&amp; glob_num)
<a name="l00447"></a>00447   {
<a name="l00448"></a>00448     pastix_int_t nrhs = 1;
<a name="l00449"></a>00449     T* rhs_ = x.GetData();
<a name="l00450"></a>00450 
<a name="l00451"></a>00451     iparm[IPARM_START_TASK] = API_TASK_SOLVE;
<a name="l00452"></a>00452     <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_matrix_pastix.php#a8c22604c5e88bb75086bde3f344cd4d5" title="if true, solution is refined">refine_solution</a>)
<a name="l00453"></a>00453       iparm[IPARM_END_TASK] = API_TASK_REFINE;
<a name="l00454"></a>00454     <span class="keywordflow">else</span>
<a name="l00455"></a>00455       iparm[IPARM_END_TASK] = API_TASK_SOLVE;
<a name="l00456"></a>00456 
<a name="l00457"></a>00457     CallPastix(<a class="code" href="class_seldon_1_1_matrix_pastix.php#a92ec4775edc88ab697268cc96b7c5848" title="MPI communicator.">comm_facto</a>, NULL, NULL, NULL, rhs_, nrhs);
<a name="l00458"></a>00458   }
<a name="l00459"></a>00459 <span class="preprocessor">#endif</span>
<a name="l00460"></a>00460 <span class="preprocessor"></span>
<a name="l00461"></a>00461 
<a name="l00462"></a>00462   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00463"></a>00463   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a76d3f187a877c6c58245b0716e1adc00" title="Returns the LU factorization of a matrix.">GetLU</a>(Matrix&lt;T, Prop, Storage, Allocator&gt;&amp; A, MatrixPastix&lt;T&gt;&amp; mat_lu,
<a name="l00464"></a>00464              <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>)
<a name="l00465"></a>00465   {
<a name="l00466"></a>00466     mat_lu.FactorizeMatrix(A, keep_matrix);
<a name="l00467"></a>00467   }
<a name="l00468"></a>00468 
<a name="l00469"></a>00469 
<a name="l00470"></a>00470   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00471"></a>00471   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(MatrixPastix&lt;T&gt;&amp; mat_lu, Vector&lt;T, VectFull, Allocator&gt;&amp; x)
<a name="l00472"></a>00472   {
<a name="l00473"></a>00473     mat_lu.Solve(x);
<a name="l00474"></a>00474   }
<a name="l00475"></a>00475 
<a name="l00476"></a>00476 
<a name="l00477"></a>00477   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator, <span class="keyword">class</span> Transpose_status&gt;
<a name="l00478"></a>00478   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(<span class="keyword">const</span> Transpose_status&amp; TransA,
<a name="l00479"></a>00479                MatrixPastix&lt;T&gt;&amp; mat_lu, Vector&lt;T, VectFull, Allocator&gt;&amp; x)
<a name="l00480"></a>00480   {
<a name="l00481"></a>00481     mat_lu.Solve(TransA, x);
<a name="l00482"></a>00482   }
<a name="l00483"></a>00483 
<a name="l00484"></a>00484 } <span class="comment">// end namespace</span>
<a name="l00485"></a>00485 
<a name="l00486"></a>00486 <span class="preprocessor">#define SELDON_FILE_PASTIX_CXX</span>
<a name="l00487"></a>00487 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
