<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/interfaces/direct/Cholmod.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2010 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_CHOLMOD_CXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 <span class="preprocessor">#include &quot;Cholmod.hxx&quot;</span>
<a name="l00023"></a>00023 
<a name="l00024"></a>00024 <span class="keyword">namespace </span>Seldon
<a name="l00025"></a>00025 {
<a name="l00026"></a>00026 
<a name="l00027"></a>00027   MatrixCholmod::MatrixCholmod()
<a name="l00028"></a>00028   {
<a name="l00029"></a>00029     <span class="comment">// Sets default parameters.</span>
<a name="l00030"></a>00030     cholmod_start(&amp;param_chol);
<a name="l00031"></a>00031 
<a name="l00032"></a>00032     n = 0;
<a name="l00033"></a>00033   }
<a name="l00034"></a>00034 
<a name="l00035"></a>00035 
<a name="l00036"></a>00036 
<a name="l00037"></a>00037   MatrixCholmod::~MatrixCholmod()
<a name="l00038"></a>00038   {
<a name="l00039"></a>00039     <span class="keywordflow">if</span> (n &gt; 0)
<a name="l00040"></a>00040       {
<a name="l00041"></a>00041         Clear();
<a name="l00042"></a>00042         cholmod_finish(&amp;param_chol);
<a name="l00043"></a>00043       }
<a name="l00044"></a>00044   }
<a name="l00045"></a>00045 
<a name="l00046"></a>00046 
<a name="l00047"></a>00047   <span class="keywordtype">void</span> MatrixCholmod::Clear()
<a name="l00048"></a>00048   {
<a name="l00049"></a>00049     <span class="keywordflow">if</span> (n &gt; 0)
<a name="l00050"></a>00050       {
<a name="l00051"></a>00051         n = 0;
<a name="l00052"></a>00052         cholmod_free_factor(&amp;L, &amp;param_chol);
<a name="l00053"></a>00053       }
<a name="l00054"></a>00054   }
<a name="l00055"></a>00055 
<a name="l00056"></a>00056 
<a name="l00057"></a>00057   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00058"></a>00058   <span class="keywordtype">void</span> MatrixCholmod::
<a name="l00059"></a>00059   FactorizeMatrix(Matrix&lt;double, Prop, Storage, Allocator&gt; &amp; mat,
<a name="l00060"></a>00060                   <span class="keywordtype">bool</span> keep_matrix)
<a name="l00061"></a>00061   {
<a name="l00062"></a>00062     Clear();
<a name="l00063"></a>00063 
<a name="l00064"></a>00064     n = mat.GetM();
<a name="l00065"></a>00065     Matrix&lt;double, Symmetric, RowSymSparse, MallocAlloc&lt;double&gt; &gt; Acsc;
<a name="l00066"></a>00066     Copy(mat, Acsc);
<a name="l00067"></a>00067     <span class="keywordflow">if</span> (!keep_matrix)
<a name="l00068"></a>00068       mat.Clear();
<a name="l00069"></a>00069 
<a name="l00070"></a>00070     <span class="comment">// Initialization of sparse matrix.</span>
<a name="l00071"></a>00071     cholmod_sparse A;
<a name="l00072"></a>00072 
<a name="l00073"></a>00073     A.nrow = n;
<a name="l00074"></a>00074     A.ncol = n;
<a name="l00075"></a>00075     A.nzmax = Acsc.GetDataSize();
<a name="l00076"></a>00076     A.nz = NULL;
<a name="l00077"></a>00077     A.p = Acsc.GetPtr();
<a name="l00078"></a>00078     A.i = Acsc.GetInd();
<a name="l00079"></a>00079     A.x = Acsc.GetData();
<a name="l00080"></a>00080     A.z = NULL;
<a name="l00081"></a>00081     A.stype = -1;
<a name="l00082"></a>00082     A.xtype = CHOLMOD_REAL;
<a name="l00083"></a>00083     A.dtype = CHOLMOD_DOUBLE;
<a name="l00084"></a>00084     A.sorted = <span class="keyword">true</span>;
<a name="l00085"></a>00085     A.packed = <span class="keyword">true</span>;
<a name="l00086"></a>00086 
<a name="l00087"></a>00087     L = cholmod_analyze(&amp;A, &amp;param_chol);
<a name="l00088"></a>00088 
<a name="l00089"></a>00089     cholmod_factorize(&amp;A, L, &amp;param_chol);
<a name="l00090"></a>00090 
<a name="l00091"></a>00091     cholmod_change_factor(CHOLMOD_REAL, <span class="keyword">true</span>, L-&gt;is_super,
<a name="l00092"></a>00092                           <span class="keyword">true</span>, L-&gt;is_monotonic, L, &amp;param_chol);
<a name="l00093"></a>00093   }
<a name="l00094"></a>00094 
<a name="l00095"></a>00095 
<a name="l00096"></a>00096   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Transpose_status, <span class="keyword">class</span> Allocator&gt;
<a name="l00097"></a>00097   <span class="keywordtype">void</span> MatrixCholmod::Solve(<span class="keyword">const</span> Transpose_status&amp; TransA,
<a name="l00098"></a>00098                             Vector&lt;double, VectFull, Allocator&gt;&amp; x)
<a name="l00099"></a>00099   {
<a name="l00100"></a>00100     <span class="comment">// and dense right hand side</span>
<a name="l00101"></a>00101     cholmod_dense b_rhs;
<a name="l00102"></a>00102     b_rhs.nrow = x.GetM();
<a name="l00103"></a>00103     b_rhs.ncol = 1;
<a name="l00104"></a>00104     b_rhs.nzmax = b_rhs.nrow;
<a name="l00105"></a>00105     b_rhs.d = b_rhs.nrow;
<a name="l00106"></a>00106     b_rhs.x = x.GetData();
<a name="l00107"></a>00107     b_rhs.z = NULL;
<a name="l00108"></a>00108     b_rhs.xtype = CHOLMOD_REAL;
<a name="l00109"></a>00109     b_rhs.dtype = CHOLMOD_DOUBLE;
<a name="l00110"></a>00110 
<a name="l00111"></a>00111     cholmod_dense* x_sol, *y;
<a name="l00112"></a>00112     <span class="keywordflow">if</span> (TransA.Trans())
<a name="l00113"></a>00113       {
<a name="l00114"></a>00114         y = cholmod_solve(CHOLMOD_Lt, L, &amp;b_rhs, &amp;param_chol);
<a name="l00115"></a>00115         x_sol = cholmod_solve(CHOLMOD_Pt, L, y, &amp;param_chol);
<a name="l00116"></a>00116       }
<a name="l00117"></a>00117     <span class="keywordflow">else</span>
<a name="l00118"></a>00118       {
<a name="l00119"></a>00119         y = cholmod_solve(CHOLMOD_P, L, &amp;b_rhs, &amp;param_chol);
<a name="l00120"></a>00120         x_sol = cholmod_solve(CHOLMOD_L, L, y, &amp;param_chol);
<a name="l00121"></a>00121       }
<a name="l00122"></a>00122 
<a name="l00123"></a>00123     <span class="keywordtype">double</span>* data = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">double</span>*<span class="keyword">&gt;</span>(x_sol-&gt;x);
<a name="l00124"></a>00124     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; x.GetM(); i++)
<a name="l00125"></a>00125       x(i) = data[i];
<a name="l00126"></a>00126 
<a name="l00127"></a>00127     cholmod_free_dense(&amp;x_sol, &amp;param_chol);
<a name="l00128"></a>00128     cholmod_free_dense(&amp;y, &amp;param_chol);
<a name="l00129"></a>00129   }
<a name="l00130"></a>00130 
<a name="l00131"></a>00131 
<a name="l00132"></a>00132   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00133"></a>00133   <span class="keywordtype">void</span> GetCholesky(Matrix&lt;T, Prop, Storage, Allocator&gt;&amp; A,
<a name="l00134"></a>00134                    MatrixCholmod&amp; mat_chol, <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>)
<a name="l00135"></a>00135   {
<a name="l00136"></a>00136     mat_chol.FactorizeMatrix(A, keep_matrix);
<a name="l00137"></a>00137   }
<a name="l00138"></a>00138 
<a name="l00139"></a>00139   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator, <span class="keyword">class</span> Transpose_status&gt;
<a name="l00140"></a>00140   <span class="keywordtype">void</span>
<a name="l00141"></a>00141   SolveCholesky(<span class="keyword">const</span> Transpose_status&amp; TransA,
<a name="l00142"></a>00142                 MatrixCholmod&amp; mat_chol, Vector&lt;T, VectFull, Allocator&gt;&amp; x)
<a name="l00143"></a>00143   {
<a name="l00144"></a>00144     mat_chol.Solve(TransA, x);
<a name="l00145"></a>00145   }
<a name="l00146"></a>00146 
<a name="l00147"></a>00147 }
<a name="l00148"></a>00148 
<a name="l00149"></a>00149 <span class="preprocessor">#define SELDON_FILE_CHOLMOD_CXX</span>
<a name="l00150"></a>00150 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
