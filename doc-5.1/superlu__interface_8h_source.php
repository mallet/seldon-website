<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/interfaces/direct/superlu_interface.h</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="preprocessor">#ifndef __SUPERLU_INTERFACE </span><span class="comment">/* allow multiple inclusions */</span>
<a name="l00002"></a>00002 <span class="preprocessor">#define __SUPERLU_INTERFACE</span>
<a name="l00003"></a>00003 <span class="preprocessor"></span>
<a name="l00004"></a>00004 <span class="preprocessor">#include &quot;superlu/slu_zdefs.h&quot;</span>
<a name="l00005"></a>00005 
<a name="l00007"></a>00007 <span class="keyword">extern</span> <span class="keywordtype">void</span>
<a name="l00008"></a>00008 dgssv(superlu_options_t *, SuperMatrix *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, SuperMatrix *,
<a name="l00009"></a>00009       SuperMatrix *, SuperMatrix *, SuperLUStat_t *, <span class="keywordtype">int</span> *);
<a name="l00010"></a>00010 <span class="keyword">extern</span> <span class="keywordtype">void</span>
<a name="l00011"></a>00011 dgssvx(superlu_options_t *, SuperMatrix *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *,
<a name="l00012"></a>00012        <span class="keywordtype">char</span> *, <span class="keywordtype">double</span> *, <span class="keywordtype">double</span> *, SuperMatrix *, SuperMatrix *,
<a name="l00013"></a>00013        <span class="keywordtype">void</span> *, <span class="keywordtype">int</span>, SuperMatrix *, SuperMatrix *,
<a name="l00014"></a>00014        <span class="keywordtype">double</span> *, <span class="keywordtype">double</span> *, <span class="keywordtype">double</span> *, <span class="keywordtype">double</span> *,
<a name="l00015"></a>00015        mem_usage_t *, SuperLUStat_t *, <span class="keywordtype">int</span> *);
<a name="l00016"></a>00016 
<a name="l00018"></a>00018 <span class="keyword">extern</span> <span class="keywordtype">void</span>
<a name="l00019"></a>00019 dCreate_CompCol_Matrix(SuperMatrix *, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">double</span> *,
<a name="l00020"></a>00020                        <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, Stype_t, Dtype_t, Mtype_t);
<a name="l00021"></a>00021 <span class="keyword">extern</span> <span class="keywordtype">void</span>
<a name="l00022"></a>00022 dCreate_CompRow_Matrix(SuperMatrix *, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">double</span> *,
<a name="l00023"></a>00023                        <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, Stype_t, Dtype_t, Mtype_t);
<a name="l00024"></a>00024 <span class="keyword">extern</span> <span class="keywordtype">void</span>
<a name="l00025"></a>00025 dCopy_CompCol_Matrix(SuperMatrix *, SuperMatrix *);
<a name="l00026"></a>00026 <span class="keyword">extern</span> <span class="keywordtype">void</span>
<a name="l00027"></a>00027 dCreate_Dense_Matrix(SuperMatrix *, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">double</span> *, <span class="keywordtype">int</span>,
<a name="l00028"></a>00028                      Stype_t, Dtype_t, Mtype_t);
<a name="l00029"></a>00029 <span class="keyword">extern</span> <span class="keywordtype">void</span>
<a name="l00030"></a>00030 dCreate_SuperNode_Matrix(SuperMatrix *, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">double</span> *, 
<a name="l00031"></a>00031                          <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *,
<a name="l00032"></a>00032                          Stype_t, Dtype_t, Mtype_t);
<a name="l00033"></a>00033 <span class="keyword">extern</span> <span class="keywordtype">void</span>
<a name="l00034"></a>00034 dCopy_Dense_Matrix(<span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">double</span> *, <span class="keywordtype">int</span>, <span class="keywordtype">double</span> *, <span class="keywordtype">int</span>);
<a name="l00035"></a>00035 
<a name="l00036"></a>00036 <span class="keyword">extern</span> <span class="keywordtype">void</span>    dgstrf (superlu_options_t*, SuperMatrix*, <span class="keywordtype">double</span>, 
<a name="l00037"></a>00037                        <span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>*, <span class="keywordtype">void</span> *, <span class="keywordtype">int</span>, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, 
<a name="l00038"></a>00038                        SuperMatrix *, SuperMatrix *, SuperLUStat_t*, <span class="keywordtype">int</span> *);
<a name="l00039"></a>00039 <span class="keyword">extern</span> <span class="keywordtype">int</span>     dsnode_dfs (<span class="keyword">const</span> <span class="keywordtype">int</span>, <span class="keyword">const</span> <span class="keywordtype">int</span>, <span class="keyword">const</span> <span class="keywordtype">int</span> *, <span class="keyword">const</span> <span class="keywordtype">int</span> *,
<a name="l00040"></a>00040                              <span class="keyword">const</span> <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, GlobalLU_t *);
<a name="l00041"></a>00041 <span class="keyword">extern</span> <span class="keywordtype">int</span>     dsnode_bmod (<span class="keyword">const</span> <span class="keywordtype">int</span>, <span class="keyword">const</span> <span class="keywordtype">int</span>, <span class="keyword">const</span> <span class="keywordtype">int</span>, <span class="keywordtype">double</span> *,
<a name="l00042"></a>00042                               <span class="keywordtype">double</span> *, GlobalLU_t *, SuperLUStat_t*);
<a name="l00043"></a>00043 <span class="keyword">extern</span> <span class="keywordtype">void</span>    dpanel_dfs (<span class="keyword">const</span> <span class="keywordtype">int</span>, <span class="keyword">const</span> <span class="keywordtype">int</span>, <span class="keyword">const</span> <span class="keywordtype">int</span>, SuperMatrix *,
<a name="l00044"></a>00044                            <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">double</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *,
<a name="l00045"></a>00045                            <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, GlobalLU_t *);
<a name="l00046"></a>00046 <span class="keyword">extern</span> <span class="keywordtype">void</span>    dpanel_bmod (<span class="keyword">const</span> <span class="keywordtype">int</span>, <span class="keyword">const</span> <span class="keywordtype">int</span>, <span class="keyword">const</span> <span class="keywordtype">int</span>, <span class="keyword">const</span> <span class="keywordtype">int</span>,
<a name="l00047"></a>00047                            <span class="keywordtype">double</span> *, <span class="keywordtype">double</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *,
<a name="l00048"></a>00048                            GlobalLU_t *, SuperLUStat_t*);
<a name="l00049"></a>00049 <span class="keyword">extern</span> <span class="keywordtype">int</span>     dcolumn_dfs (<span class="keyword">const</span> <span class="keywordtype">int</span>, <span class="keyword">const</span> <span class="keywordtype">int</span>, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *,
<a name="l00050"></a>00050                            <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, GlobalLU_t *);
<a name="l00051"></a>00051 <span class="keyword">extern</span> <span class="keywordtype">int</span>     dcolumn_bmod (<span class="keyword">const</span> <span class="keywordtype">int</span>, <span class="keyword">const</span> <span class="keywordtype">int</span>, <span class="keywordtype">double</span> *,
<a name="l00052"></a>00052                            <span class="keywordtype">double</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span>,
<a name="l00053"></a>00053                            GlobalLU_t *, SuperLUStat_t*);
<a name="l00054"></a>00054 <span class="keyword">extern</span> <span class="keywordtype">int</span>     dcopy_to_ucol (<span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *,
<a name="l00055"></a>00055                               <span class="keywordtype">double</span> *, GlobalLU_t *);         
<a name="l00056"></a>00056 <span class="keyword">extern</span> <span class="keywordtype">int</span>     dpivotL (<span class="keyword">const</span> <span class="keywordtype">int</span>, <span class="keyword">const</span> <span class="keywordtype">double</span>, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, 
<a name="l00057"></a>00057                          <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, GlobalLU_t *, SuperLUStat_t*);
<a name="l00058"></a>00058 <span class="keyword">extern</span> <span class="keywordtype">void</span>    dpruneL (<span class="keyword">const</span> <span class="keywordtype">int</span>, <span class="keyword">const</span> <span class="keywordtype">int</span> *, <span class="keyword">const</span> <span class="keywordtype">int</span>, <span class="keyword">const</span> <span class="keywordtype">int</span>,
<a name="l00059"></a>00059                           <span class="keyword">const</span> <span class="keywordtype">int</span> *, <span class="keyword">const</span> <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, GlobalLU_t *);
<a name="l00060"></a>00060 <span class="keyword">extern</span> <span class="keywordtype">void</span>    dreadmt (<span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">double</span> **, <span class="keywordtype">int</span> **, <span class="keywordtype">int</span> **);
<a name="l00061"></a>00061 <span class="keyword">extern</span> <span class="keywordtype">void</span>    dGenXtrue (<span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">double</span> *, <span class="keywordtype">int</span>);
<a name="l00062"></a>00062 <span class="keyword">extern</span> <span class="keywordtype">void</span>    dFillRHS (trans_t, <span class="keywordtype">int</span>, <span class="keywordtype">double</span> *, <span class="keywordtype">int</span>, SuperMatrix *,
<a name="l00063"></a>00063                           SuperMatrix *);
<a name="l00064"></a>00064 <span class="keyword">extern</span> <span class="keywordtype">void</span>    dgstrs (trans_t, SuperMatrix *, SuperMatrix *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *,
<a name="l00065"></a>00065                         SuperMatrix *, SuperLUStat_t*, <span class="keywordtype">int</span> *);
<a name="l00066"></a>00066 
<a name="l00067"></a>00067 
<a name="l00070"></a>00070 <span class="keyword">extern</span> <span class="keywordtype">void</span>    dgsequ (SuperMatrix *, <span class="keywordtype">double</span> *, <span class="keywordtype">double</span> *, <span class="keywordtype">double</span> *,
<a name="l00071"></a>00071                         <span class="keywordtype">double</span> *, <span class="keywordtype">double</span> *, <span class="keywordtype">int</span> *);
<a name="l00072"></a>00072 <span class="keyword">extern</span> <span class="keywordtype">void</span>    dlaqgs (SuperMatrix *, <span class="keywordtype">double</span> *, <span class="keywordtype">double</span> *, <span class="keywordtype">double</span>,
<a name="l00073"></a>00073                         <span class="keywordtype">double</span>, <span class="keywordtype">double</span>, <span class="keywordtype">char</span> *);
<a name="l00074"></a>00074 <span class="keyword">extern</span> <span class="keywordtype">void</span>    dgscon (<span class="keywordtype">char</span> *, SuperMatrix *, SuperMatrix *, 
<a name="l00075"></a>00075                          <span class="keywordtype">double</span>, <span class="keywordtype">double</span> *, SuperLUStat_t*, <span class="keywordtype">int</span> *);
<a name="l00076"></a>00076 <span class="keyword">extern</span> <span class="keywordtype">double</span>   dPivotGrowth(<span class="keywordtype">int</span>, SuperMatrix *, <span class="keywordtype">int</span> *, 
<a name="l00077"></a>00077                             SuperMatrix *, SuperMatrix *);
<a name="l00078"></a>00078 <span class="keyword">extern</span> <span class="keywordtype">void</span>    dgsrfs (trans_t, SuperMatrix *, SuperMatrix *,
<a name="l00079"></a>00079                        SuperMatrix *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">char</span> *, <span class="keywordtype">double</span> *, 
<a name="l00080"></a>00080                        <span class="keywordtype">double</span> *, SuperMatrix *, SuperMatrix *,
<a name="l00081"></a>00081                        <span class="keywordtype">double</span> *, <span class="keywordtype">double</span> *, SuperLUStat_t*, <span class="keywordtype">int</span> *);
<a name="l00082"></a>00082 
<a name="l00083"></a>00083 <span class="keyword">extern</span> <span class="keywordtype">int</span>     sp_dtrsv (<span class="keywordtype">char</span> *, <span class="keywordtype">char</span> *, <span class="keywordtype">char</span> *, SuperMatrix *,
<a name="l00084"></a>00084                         SuperMatrix *, <span class="keywordtype">double</span> *, SuperLUStat_t*, <span class="keywordtype">int</span> *);
<a name="l00085"></a>00085 <span class="keyword">extern</span> <span class="keywordtype">int</span>     sp_dgemv (<span class="keywordtype">char</span> *, <span class="keywordtype">double</span>, SuperMatrix *, <span class="keywordtype">double</span> *,
<a name="l00086"></a>00086                         <span class="keywordtype">int</span>, <span class="keywordtype">double</span>, <span class="keywordtype">double</span> *, <span class="keywordtype">int</span>);
<a name="l00087"></a>00087 
<a name="l00088"></a>00088 <span class="keyword">extern</span> <span class="keywordtype">int</span>     sp_dgemm (<span class="keywordtype">char</span> *, <span class="keywordtype">char</span> *, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">double</span>,
<a name="l00089"></a>00089                         SuperMatrix *, <span class="keywordtype">double</span> *, <span class="keywordtype">int</span>, <span class="keywordtype">double</span>, 
<a name="l00090"></a>00090                         <span class="keywordtype">double</span> *, <span class="keywordtype">int</span>);
<a name="l00091"></a>00091 
<a name="l00093"></a>00093 <span class="keyword">extern</span> <span class="keywordtype">int</span>     dLUMemInit (fact_t, <span class="keywordtype">void</span> *, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>,
<a name="l00094"></a>00094                              SuperMatrix *, SuperMatrix *,
<a name="l00095"></a>00095                              GlobalLU_t *, <span class="keywordtype">int</span> **, <span class="keywordtype">double</span> **);
<a name="l00096"></a>00096 <span class="keyword">extern</span> <span class="keywordtype">void</span>    dSetRWork (<span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">double</span> *, <span class="keywordtype">double</span> **, <span class="keywordtype">double</span> **);
<a name="l00097"></a>00097 <span class="keyword">extern</span> <span class="keywordtype">void</span>    dLUWorkFree (<span class="keywordtype">int</span> *, <span class="keywordtype">double</span> *, GlobalLU_t *);
<a name="l00098"></a>00098 <span class="keyword">extern</span> <span class="keywordtype">int</span>     dLUMemXpand (<span class="keywordtype">int</span>, <span class="keywordtype">int</span>, MemType, <span class="keywordtype">int</span> *, GlobalLU_t *);
<a name="l00099"></a>00099 
<a name="l00100"></a>00100 <span class="keyword">extern</span> <span class="keywordtype">double</span>  *doubleMalloc(<span class="keywordtype">int</span>);
<a name="l00101"></a>00101 <span class="keyword">extern</span> <span class="keywordtype">double</span>  *doubleCalloc(<span class="keywordtype">int</span>);
<a name="l00102"></a>00102 <span class="keyword">extern</span> <span class="keywordtype">int</span>     dmemory_usage(<span class="keyword">const</span> <span class="keywordtype">int</span>, <span class="keyword">const</span> <span class="keywordtype">int</span>, <span class="keyword">const</span> <span class="keywordtype">int</span>, <span class="keyword">const</span> <span class="keywordtype">int</span>);
<a name="l00103"></a>00103 <span class="keyword">extern</span> <span class="keywordtype">int</span>     dQuerySpace (SuperMatrix *, SuperMatrix *, mem_usage_t *);
<a name="l00104"></a>00104 
<a name="l00106"></a>00106 <span class="keyword">extern</span> <span class="keywordtype">void</span>    dreadhb(<span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">int</span> *, <span class="keywordtype">double</span> **, <span class="keywordtype">int</span> **, <span class="keywordtype">int</span> **);
<a name="l00107"></a>00107 <span class="keyword">extern</span> <span class="keywordtype">void</span>    dCompRow_to_CompCol(<span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">int</span>, <span class="keywordtype">double</span>*, <span class="keywordtype">int</span>*, <span class="keywordtype">int</span>*,
<a name="l00108"></a>00108                                    <span class="keywordtype">double</span> **, <span class="keywordtype">int</span> **, <span class="keywordtype">int</span> **);
<a name="l00109"></a>00109 <span class="keyword">extern</span> <span class="keywordtype">void</span>    dfill (<span class="keywordtype">double</span> *, <span class="keywordtype">int</span>, <span class="keywordtype">double</span>);
<a name="l00110"></a>00110 <span class="keyword">extern</span> <span class="keywordtype">void</span>    dinf_norm_error (<span class="keywordtype">int</span>, SuperMatrix *, <span class="keywordtype">double</span> *);
<a name="l00111"></a>00111 
<a name="l00113"></a>00113 <span class="keyword">extern</span> <span class="keywordtype">void</span>    dPrint_CompCol_Matrix(<span class="keywordtype">char</span> *, SuperMatrix *);
<a name="l00114"></a>00114 <span class="keyword">extern</span> <span class="keywordtype">void</span>    dPrint_SuperNode_Matrix(<span class="keywordtype">char</span> *, SuperMatrix *);
<a name="l00115"></a>00115 <span class="keyword">extern</span> <span class="keywordtype">void</span>    dPrint_Dense_Matrix(<span class="keywordtype">char</span> *, SuperMatrix *);
<a name="l00116"></a>00116 
<a name="l00117"></a>00117 <span class="preprocessor">#endif </span><span class="comment">/* __SUPERLU_INTERFACE */</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
