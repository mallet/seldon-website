<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/solver/iterative/TfQmr.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_ITERATIVE_TFQMR_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="keyword">namespace </span>Seldon
<a name="l00024"></a>00024 {
<a name="l00025"></a>00025 
<a name="l00027"></a>00027 
<a name="l00044"></a>00044   <span class="keyword">template</span> &lt;<span class="keyword">class</span> Titer, <span class="keyword">class</span> Matrix1, <span class="keyword">class</span> Vector1, <span class="keyword">class</span> Preconditioner&gt;
<a name="l00045"></a><a class="code" href="namespace_seldon.php#afe1e9e5f2508d3560f077fb84b1eac56">00045</a>   <span class="keywordtype">int</span> <a class="code" href="namespace_seldon.php#afe1e9e5f2508d3560f077fb84b1eac56" title="Solves a linear system by using Transpose Free Quasi-Minimal Residual.">TfQmr</a>(Matrix1&amp; A, Vector1&amp; x, <span class="keyword">const</span> Vector1&amp; b,
<a name="l00046"></a>00046             Preconditioner&amp; M, <a class="code" href="class_seldon_1_1_iteration.php" title="Class containing parameters for an iterative resolution.">Iteration&lt;Titer&gt;</a> &amp; iter)
<a name="l00047"></a>00047   {
<a name="l00048"></a>00048     <span class="keyword">const</span> <span class="keywordtype">int</span> N = A.GetM();
<a name="l00049"></a>00049     <span class="keywordflow">if</span> (N &lt;= 0)
<a name="l00050"></a>00050       <span class="keywordflow">return</span> 0;
<a name="l00051"></a>00051 
<a name="l00052"></a>00052     Vector1 tmp(b), r0(b), v(b), h(b), w(b),
<a name="l00053"></a>00053       y1(b), g(b), y0(b), rtilde(b), d(b);
<a name="l00054"></a>00054 
<a name="l00055"></a>00055     <span class="keyword">typedef</span> <span class="keyword">typename</span> Vector1::value_type Complexe;
<a name="l00056"></a>00056     Complexe sigma, alpha, beta, eta, rho, rho0;
<a name="l00057"></a>00057     Titer c, kappa, tau, theta;
<a name="l00058"></a>00058 
<a name="l00059"></a>00059     tmp.Zero();
<a name="l00060"></a>00060     <span class="comment">// x is initial value</span>
<a name="l00061"></a>00061     <span class="comment">// 1. r0 = M^{-1} (b - A x)</span>
<a name="l00062"></a>00062     Copy(b, tmp);
<a name="l00063"></a>00063     <span class="keywordflow">if</span> (!iter.<a class="code" href="class_seldon_1_1_iteration.php#aa02c78783891fabcb4f7517f60f4912e" title="Returns true if the initial guess is null.">IsInitGuess_Null</a>())
<a name="l00064"></a>00064       <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(Complexe(-1), A, x, Complexe(1), tmp);
<a name="l00065"></a>00065     <span class="keywordflow">else</span>
<a name="l00066"></a>00066       x.Zero();
<a name="l00067"></a>00067 
<a name="l00068"></a>00068     M.Solve(A, tmp, r0);
<a name="l00069"></a>00069 
<a name="l00070"></a>00070     <span class="comment">// we initialize iter</span>
<a name="l00071"></a>00071     <span class="keywordtype">int</span> success_init = iter.<a class="code" href="class_seldon_1_1_iteration.php#a047b05d60ecc8b6f8f0789255472bfe3" title="Initialization with the right hand side.">Init</a>(r0);
<a name="l00072"></a>00072     <span class="keywordflow">if</span> (success_init != 0)
<a name="l00073"></a>00073       <span class="keywordflow">return</span> iter.<a class="code" href="class_seldon_1_1_iteration.php#aa84736520dcca50ab5b8c72449ef8ed4" title="Returns the error code (if an error occured).">ErrorCode</a>();
<a name="l00074"></a>00074 
<a name="l00075"></a>00075     <span class="comment">// 2. w=y=r</span>
<a name="l00076"></a>00076     Copy(r0, w);
<a name="l00077"></a>00077     Copy(r0, y1);
<a name="l00078"></a>00078 
<a name="l00079"></a>00079     <span class="comment">// 3. g=v=M^{-1}Ay</span>
<a name="l00080"></a>00080     Copy(y1, v);
<a name="l00081"></a>00081     <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(A, v, tmp);
<a name="l00082"></a>00082     M.Solve(A, tmp, v);
<a name="l00083"></a>00083     ++iter;
<a name="l00084"></a>00084 
<a name="l00085"></a>00085     Copy(v, g);
<a name="l00086"></a>00086 
<a name="l00087"></a>00087     <span class="comment">// 4. d=0</span>
<a name="l00088"></a>00088     d.Zero();
<a name="l00089"></a>00089 
<a name="l00090"></a>00090     <span class="comment">// 5. tau = ||r||</span>
<a name="l00091"></a>00091     tau = Norm2(r0);
<a name="l00092"></a>00092 
<a name="l00093"></a>00093     <span class="comment">// 6. theta = eta = 0</span>
<a name="l00094"></a>00094     theta = 0.0;
<a name="l00095"></a>00095     eta = 0.0;
<a name="l00096"></a>00096 
<a name="l00097"></a>00097     <span class="comment">// 7. rtilde = r</span>
<a name="l00098"></a>00098     Copy(r0, rtilde);
<a name="l00099"></a>00099 
<a name="l00100"></a>00100     <span class="comment">// 8. rho=dot(rtilde, r)</span>
<a name="l00101"></a>00101     rho = <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(rtilde, r0);
<a name="l00102"></a>00102     rho0 = rho;
<a name="l00103"></a>00103 
<a name="l00104"></a>00104     iter.<a class="code" href="class_seldon_1_1_iteration.php#a24c69ffb62504d708354f8874a5f0f00" title="Changes the number of iterations.">SetNumberIteration</a>(0);
<a name="l00105"></a>00105     <span class="comment">// Loop until the stopping criteria are reached</span>
<a name="l00106"></a>00106     <span class="keywordflow">for</span> (;;)
<a name="l00107"></a>00107       {
<a name="l00108"></a>00108         <span class="comment">// 9. 10. 11.</span>
<a name="l00109"></a>00109         <span class="comment">// sigma=dot(rtilde,v)</span>
<a name="l00110"></a>00110         <span class="comment">// alpha=rho/sigma</span>
<a name="l00111"></a>00111         <span class="comment">// y2k=y(2k-1)-alpha*v</span>
<a name="l00112"></a>00112         sigma = <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(rtilde, v);
<a name="l00113"></a>00113 
<a name="l00114"></a>00114         <span class="keywordflow">if</span> (sigma == Complexe(0))
<a name="l00115"></a>00115           {
<a name="l00116"></a>00116             iter.<a class="code" href="class_seldon_1_1_iteration.php#a32dd5ec2e2b6216c50fbc1d0e27607a7" title="Informs of a failure in the iterative solver.">Fail</a>(1, <span class="stringliteral">&quot;Tfqmr breakdown: sigma=0&quot;</span>);
<a name="l00117"></a>00117             <span class="keywordflow">break</span>;
<a name="l00118"></a>00118           }
<a name="l00119"></a>00119         alpha = rho / sigma;
<a name="l00120"></a>00120 
<a name="l00121"></a>00121         <span class="comment">//y0 = y1 - alpha * v;</span>
<a name="l00122"></a>00122         Copy(y1, y0);
<a name="l00123"></a>00123         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(-alpha, v, y0);
<a name="l00124"></a>00124 
<a name="l00125"></a>00125         <span class="comment">// 12. h=M^{-1}*A*y</span>
<a name="l00126"></a>00126         Copy(y0, h);
<a name="l00127"></a>00127         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(A, h, tmp);
<a name="l00128"></a>00128         M.Solve(A, tmp, h);
<a name="l00129"></a>00129         <span class="comment">//split the loop of &quot;for m = 2k-1, 2k&quot;</span>
<a name="l00130"></a>00130 
<a name="l00131"></a>00131         <span class="comment">//The first one</span>
<a name="l00132"></a>00132         <span class="comment">// 13. w = w-alpha*M^{-1} A y0</span>
<a name="l00133"></a>00133         <span class="comment">//w = w - alpha * g;</span>
<a name="l00134"></a>00134         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(-alpha, g, w);
<a name="l00135"></a>00135         <span class="comment">// 18. d=y0+((theta0^2)*eta0/alpha)*d         //need check breakdown</span>
<a name="l00136"></a>00136         <span class="keywordflow">if</span> (alpha == Complexe(0))
<a name="l00137"></a>00137           {
<a name="l00138"></a>00138             iter.<a class="code" href="class_seldon_1_1_iteration.php#a32dd5ec2e2b6216c50fbc1d0e27607a7" title="Informs of a failure in the iterative solver.">Fail</a>(2, <span class="stringliteral">&quot;Tfqmr breakdown: alpha=0&quot;</span>);
<a name="l00139"></a>00139             <span class="keywordflow">break</span>;
<a name="l00140"></a>00140           }
<a name="l00141"></a>00141         <span class="comment">//d = y1 + ( theta * theta * eta / alpha ) * d;</span>
<a name="l00142"></a>00142         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(theta * theta * eta / alpha,d);
<a name="l00143"></a>00143         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(Complexe(1), y1, d);
<a name="l00144"></a>00144 
<a name="l00145"></a>00145         <span class="comment">// 14. theta=||w||_2/tau0       //need check breakdown</span>
<a name="l00146"></a>00146         <span class="keywordflow">if</span> (tau == Titer(0))
<a name="l00147"></a>00147           {
<a name="l00148"></a>00148             iter.<a class="code" href="class_seldon_1_1_iteration.php#a32dd5ec2e2b6216c50fbc1d0e27607a7" title="Informs of a failure in the iterative solver.">Fail</a>(3, <span class="stringliteral">&quot;Tfqmr breakdown: tau=0&quot;</span>);
<a name="l00149"></a>00149             <span class="keywordflow">break</span>;
<a name="l00150"></a>00150           }
<a name="l00151"></a>00151         theta  = Norm2(w) / tau;
<a name="l00152"></a>00152 
<a name="l00153"></a>00153         <span class="comment">// 15. c = 1/sqrt(1+theta^2)</span>
<a name="l00154"></a>00154         c = Titer(1) / sqrt(Titer(1) + theta * theta);
<a name="l00155"></a>00155 
<a name="l00156"></a>00156         <span class="comment">// 16. tau = tau0*theta*c</span>
<a name="l00157"></a>00157         tau = tau * c * theta;
<a name="l00158"></a>00158 
<a name="l00159"></a>00159         <span class="comment">// 17.  eta = (c^2)*alpha</span>
<a name="l00160"></a>00160         eta = c * c * alpha;
<a name="l00161"></a>00161 
<a name="l00162"></a>00162         <span class="comment">// 19. x = x+eta*d</span>
<a name="l00163"></a>00163         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(eta, d, x);
<a name="l00164"></a>00164         <span class="comment">// 20. kappa = tau*sqrt(m+1)</span>
<a name="l00165"></a>00165         kappa = tau * sqrt( 2.* (iter.<a class="code" href="class_seldon_1_1_iteration.php#ab426008d4bfc40842d86c8c6042a6594" title="Returns the number of iterations.">GetNumberIteration</a>()+1) );
<a name="l00166"></a>00166 
<a name="l00167"></a>00167         <span class="comment">// 21. check stopping criterion</span>
<a name="l00168"></a>00168         <span class="keywordflow">if</span> ( iter.<a class="code" href="class_seldon_1_1_iteration.php#a07c3b0af0412cfc7fe6fd3ed5476c4e0" title="Returns true if the iterative solver has reached its end.">Finished</a>(kappa) )
<a name="l00169"></a>00169           {
<a name="l00170"></a>00170             <span class="keywordflow">break</span>;
<a name="l00171"></a>00171           }
<a name="l00172"></a>00172         ++iter;
<a name="l00173"></a>00173         <span class="comment">// g = h;</span>
<a name="l00174"></a>00174         Copy(h, g);
<a name="l00175"></a>00175         <span class="comment">//The second one</span>
<a name="l00176"></a>00176 
<a name="l00177"></a>00177         <span class="comment">// 13. w = w-alpha*M^{-1} A y0</span>
<a name="l00178"></a>00178         <span class="comment">// w = w - alpha * g;</span>
<a name="l00179"></a>00179         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(-alpha,g,w);
<a name="l00180"></a>00180         <span class="comment">// 18. d = y0+((theta0^2)*eta0/alpha)*d</span>
<a name="l00181"></a>00181         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(theta * theta * eta / alpha,d);
<a name="l00182"></a>00182         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(Complexe(1), y0, d);
<a name="l00183"></a>00183         <span class="comment">// 14. theta=||w||_2/tau0</span>
<a name="l00184"></a>00184         <span class="keywordflow">if</span> (tau == Titer(0))
<a name="l00185"></a>00185           {
<a name="l00186"></a>00186             iter.<a class="code" href="class_seldon_1_1_iteration.php#a32dd5ec2e2b6216c50fbc1d0e27607a7" title="Informs of a failure in the iterative solver.">Fail</a>(4, <span class="stringliteral">&quot;Tfqmr breakdown: tau=0&quot;</span>);
<a name="l00187"></a>00187             <span class="keywordflow">break</span>;
<a name="l00188"></a>00188           }
<a name="l00189"></a>00189         theta = Norm2(w) / tau;
<a name="l00190"></a>00190 
<a name="l00191"></a>00191         <span class="comment">// 15. c = 1/sqrt(1+theta^2)</span>
<a name="l00192"></a>00192         c = Titer(1) / sqrt(Titer(1) + theta * theta);
<a name="l00193"></a>00193 
<a name="l00194"></a>00194         <span class="comment">// 16. tau = tau0*theta*c</span>
<a name="l00195"></a>00195         tau = tau * c * theta;
<a name="l00196"></a>00196 
<a name="l00197"></a>00197         <span class="comment">// 17.  eta = (c^2)*alpha</span>
<a name="l00198"></a>00198         eta = c * c * alpha;
<a name="l00199"></a>00199 
<a name="l00200"></a>00200         <span class="comment">// 19. x = x+eta*d</span>
<a name="l00201"></a>00201         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(eta, d, x);
<a name="l00202"></a>00202 
<a name="l00203"></a>00203         <span class="comment">// 20. kappa = tau*sqrt(m+1)</span>
<a name="l00204"></a>00204         kappa = tau * sqrt(2.* (iter.<a class="code" href="class_seldon_1_1_iteration.php#ab426008d4bfc40842d86c8c6042a6594" title="Returns the number of iterations.">GetNumberIteration</a>()+1)  + 1.);
<a name="l00205"></a>00205 
<a name="l00206"></a>00206         <span class="comment">// 21. check stopping criterion</span>
<a name="l00207"></a>00207         <span class="keywordflow">if</span> ( iter.<a class="code" href="class_seldon_1_1_iteration.php#a07c3b0af0412cfc7fe6fd3ed5476c4e0" title="Returns true if the iterative solver has reached its end.">Finished</a>(kappa) )
<a name="l00208"></a>00208           {
<a name="l00209"></a>00209             <span class="keywordflow">break</span>;
<a name="l00210"></a>00210           }
<a name="l00211"></a>00211 
<a name="l00212"></a>00212         <span class="comment">// 22. rho = dot(rtilde,w)</span>
<a name="l00213"></a>00213         <span class="comment">// 23. beta = rho/rho0                     //need check breakdown</span>
<a name="l00214"></a>00214 
<a name="l00215"></a>00215         rho0 = rho;
<a name="l00216"></a>00216         rho = <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(rtilde, w);
<a name="l00217"></a>00217         <span class="keywordflow">if</span> (rho0 == Complexe(0))
<a name="l00218"></a>00218           {
<a name="l00219"></a>00219             iter.<a class="code" href="class_seldon_1_1_iteration.php#a32dd5ec2e2b6216c50fbc1d0e27607a7" title="Informs of a failure in the iterative solver.">Fail</a>(5, <span class="stringliteral">&quot;tfqmr breakdown: beta=0&quot;</span>);
<a name="l00220"></a>00220             <span class="keywordflow">break</span>;
<a name="l00221"></a>00221           }
<a name="l00222"></a>00222         beta = rho/rho0;
<a name="l00223"></a>00223 
<a name="l00224"></a>00224         <span class="comment">// 24. y = w+beta*y0</span>
<a name="l00225"></a>00225         Copy(w, y1);
<a name="l00226"></a>00226         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(beta, y0, y1);
<a name="l00227"></a>00227 
<a name="l00228"></a>00228         <span class="comment">// 25. g=M^{-1} A y</span>
<a name="l00229"></a>00229         Copy(y1, g);
<a name="l00230"></a>00230         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(A, g, tmp);
<a name="l00231"></a>00231         M.Solve(A, tmp, g);
<a name="l00232"></a>00232 
<a name="l00233"></a>00233         <span class="comment">// 26. v = M^{-1}A y + beta*( M^{-1} A y0 + beta*v)</span>
<a name="l00234"></a>00234 
<a name="l00235"></a>00235         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta*beta, v);
<a name="l00236"></a>00236         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(beta, h, v);
<a name="l00237"></a>00237         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(Complexe(1), g, v);
<a name="l00238"></a>00238 
<a name="l00239"></a>00239         ++iter;
<a name="l00240"></a>00240       }
<a name="l00241"></a>00241 
<a name="l00242"></a>00242     <span class="keywordflow">return</span> iter.<a class="code" href="class_seldon_1_1_iteration.php#aa84736520dcca50ab5b8c72449ef8ed4" title="Returns the error code (if an error occured).">ErrorCode</a>();
<a name="l00243"></a>00243   }
<a name="l00244"></a>00244 
<a name="l00245"></a>00245 } <span class="comment">// end namespace</span>
<a name="l00246"></a>00246 
<a name="l00247"></a>00247 <span class="preprocessor">#define SELDON_FILE_ITERATIVE_TFQMR_CXX</span>
<a name="l00248"></a>00248 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
