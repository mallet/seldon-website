<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/interfaces/direct/UmfPack.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_UMFPACK_HXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 <span class="keyword">extern</span> <span class="stringliteral">&quot;C&quot;</span>
<a name="l00023"></a>00023 {
<a name="l00024"></a>00024 <span class="preprocessor">#include &quot;umfpack.h&quot;</span>
<a name="l00025"></a>00025 }
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 <span class="keyword">namespace </span>Seldon
<a name="l00028"></a>00028 {
<a name="l00030"></a>00030   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00031"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php">00031</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php" title="&amp;lt; base class to solve linear system by using UmfPack">MatrixUmfPack_Base</a>
<a name="l00032"></a>00032   {
<a name="l00033"></a>00033   <span class="keyword">public</span> :
<a name="l00034"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a4a6b504d370b37ef4951626d2a3ce439">00034</a>     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double&gt;</a> Control, <a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a4a6b504d370b37ef4951626d2a3ce439" title="parameters for UmfPack">Info</a>; 
<a name="l00035"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a7a91b3ba9d23982fc5b75ebf8f4d2eea">00035</a>     <span class="keywordtype">void</span> *Symbolic, *<a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a7a91b3ba9d23982fc5b75ebf8f4d2eea" title="pointers of UmfPack objects">Numeric</a> ; 
<a name="l00036"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#aa442e0754359877423d73f857aa75731">00036</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#aa442e0754359877423d73f857aa75731" title="number of rows in the matrix">n</a>; 
<a name="l00037"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a00b2252a6082727f89fcba9ca33fbf7d">00037</a>     <span class="keywordtype">bool</span> <a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a00b2252a6082727f89fcba9ca33fbf7d" title="true if display is allowed">display_info</a>; 
<a name="l00038"></a>00038     <span class="keywordtype">bool</span> transpose; 
<a name="l00039"></a>00039 
<a name="l00040"></a>00040   <span class="keyword">public</span> :
<a name="l00041"></a>00041     <a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#a50e9165bfdf52428e6f405d959f9f863" title="transpose system to solve ?">MatrixUmfPack_Base</a>();
<a name="l00042"></a>00042 
<a name="l00043"></a>00043     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#ad88543986d6cf6273c9f56654820b2f1" title="no message will be displayed by UmfPack">HideMessages</a>();
<a name="l00044"></a>00044     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php#af5a60fc35fefccab49c6888cdb148c94" title="normal amount of message displayed by UmfPack">ShowMessages</a>();
<a name="l00045"></a>00045 
<a name="l00046"></a>00046   };
<a name="l00047"></a>00047 
<a name="l00049"></a>00049   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00050"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack.php">00050</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack</a> : <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php" title="&amp;lt; base class to solve linear system by using UmfPack">MatrixUmfPack_Base</a>&lt;T&gt;
<a name="l00051"></a>00051   {
<a name="l00052"></a>00052   };
<a name="l00053"></a>00053 
<a name="l00055"></a>00055   <span class="keyword">template</span>&lt;&gt;
<a name="l00056"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php">00056</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack</a>&lt;double&gt; : <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php" title="&amp;lt; base class to solve linear system by using UmfPack">MatrixUmfPack_Base</a>&lt;double&gt;
<a name="l00057"></a>00057   {
<a name="l00058"></a>00058 
<a name="l00059"></a>00059   <span class="keyword">protected</span> :
<a name="l00061"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#a80c2fab0cdd04087069ed6aee40e5ac6">00061</a>     <span class="keywordtype">int</span>* ind_, *ptr_;
<a name="l00063"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#ad43800f106a0be99442759b9c63ae487">00063</a>     <span class="keywordtype">double</span>* data_;
<a name="l00064"></a>00064 
<a name="l00065"></a>00065   <span class="keyword">public</span> :
<a name="l00066"></a>00066 
<a name="l00067"></a>00067     <a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack</a>();
<a name="l00068"></a>00068     ~<a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack</a>();
<a name="l00069"></a>00069 
<a name="l00070"></a>00070     <span class="keywordtype">void</span> Clear();
<a name="l00071"></a>00071 
<a name="l00072"></a>00072     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00073"></a>00073     <span class="keywordtype">void</span> FactorizeMatrix(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, Prop, Storage, Allocator&gt;</a> &amp; mat,
<a name="l00074"></a>00074                          <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>);
<a name="l00075"></a>00075 
<a name="l00076"></a>00076     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00077"></a>00077     <span class="keywordtype">void</span> PerformAnalysis(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, Prop, RowSparse, Allocator&gt;</a> &amp; mat);
<a name="l00078"></a>00078 
<a name="l00079"></a>00079     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00080"></a>00080     <span class="keywordtype">void</span>
<a name="l00081"></a>00081     PerformFactorization(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, Prop, RowSparse, Allocator&gt;</a> &amp; mat);
<a name="l00082"></a>00082 
<a name="l00083"></a>00083     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2&gt;
<a name="l00084"></a>00084     <span class="keywordtype">void</span> Solve(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double, VectFull, Allocator2&gt;</a>&amp; x);
<a name="l00085"></a>00085 
<a name="l00086"></a>00086   };
<a name="l00087"></a>00087 
<a name="l00088"></a>00088 
<a name="l00090"></a>00090   <span class="keyword">template</span>&lt;&gt;
<a name="l00091"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php">00091</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack</a>&lt;complex&lt;double&gt; &gt;
<a name="l00092"></a>00092     : <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix_umf_pack___base.php" title="&amp;lt; base class to solve linear system by using UmfPack">MatrixUmfPack_Base</a>&lt;complex&lt;double&gt; &gt;
<a name="l00093"></a>00093   {
<a name="l00094"></a>00094 
<a name="l00095"></a>00095   <span class="keyword">protected</span>:
<a name="l00097"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a9a9a32db0c5d5edb1f6c7bc5e7560876">00097</a>     <span class="keywordtype">int</span>* ptr_, *ind_;
<a name="l00099"></a><a class="code" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a7cfdb169fedc7f0b9a6233645011c637">00099</a>     <span class="keywordtype">double</span>* data_real_, *data_imag_;
<a name="l00100"></a>00100 
<a name="l00101"></a>00101   <span class="keyword">public</span> :
<a name="l00102"></a>00102 
<a name="l00103"></a>00103     <a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack</a>();
<a name="l00104"></a>00104     ~<a class="code" href="class_seldon_1_1_matrix_umf_pack.php" title="empty class">MatrixUmfPack</a>();
<a name="l00105"></a>00105 
<a name="l00106"></a>00106     <span class="keywordtype">void</span> Clear();
<a name="l00107"></a>00107 
<a name="l00108"></a>00108     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00109"></a>00109     <span class="keywordtype">void</span>
<a name="l00110"></a>00110     FactorizeMatrix(<a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;complex&lt;double&gt;, Prop, Storage, Allocator&gt; &amp; mat,
<a name="l00111"></a>00111                     <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>);
<a name="l00112"></a>00112 
<a name="l00113"></a>00113     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2&gt;
<a name="l00114"></a>00114     <span class="keywordtype">void</span> Solve(<a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;double&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator2&gt;&amp; x);
<a name="l00115"></a>00115 
<a name="l00116"></a>00116   };
<a name="l00117"></a>00117 
<a name="l00118"></a>00118 }
<a name="l00119"></a>00119 
<a name="l00120"></a>00120 <span class="preprocessor">#define SELDON_FILE_UMFPACK_HXX</span>
<a name="l00121"></a>00121 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
