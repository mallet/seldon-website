<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix/Matrix_HermPacked.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_HERMPACKED_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;Matrix_HermPacked.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="keyword">namespace </span>Seldon
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 
<a name="l00029"></a>00029   <span class="comment">/****************</span>
<a name="l00030"></a>00030 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00031"></a>00031 <span class="comment">   ****************/</span>
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00035"></a>00035 
<a name="l00038"></a>00038   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00039"></a>00039   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ad6584a1ca7199865d2a4d5dc6af246fe" title="Default constructor.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Matrix_HermPacked</a>():
<a name="l00040"></a>00040     Matrix_Base&lt;T, Allocator&gt;()
<a name="l00041"></a>00041   {
<a name="l00042"></a>00042   }
<a name="l00043"></a>00043 
<a name="l00044"></a>00044 
<a name="l00046"></a>00046 
<a name="l00051"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ad5435d242af5fabbac0f1941972045fe">00051</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00052"></a>00052   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ad6584a1ca7199865d2a4d5dc6af246fe" title="Default constructor.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00053"></a>00053 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ad6584a1ca7199865d2a4d5dc6af246fe" title="Default constructor.">  ::Matrix_HermPacked</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l00054"></a>00054     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, i)
<a name="l00055"></a>00055   {
<a name="l00056"></a>00056 
<a name="l00057"></a>00057 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00058"></a>00058 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00059"></a>00059       {
<a name="l00060"></a>00060 <span class="preprocessor">#endif</span>
<a name="l00061"></a>00061 <span class="preprocessor"></span>
<a name="l00062"></a>00062         this-&gt;data_ = this-&gt;allocator_.allocate((i * (i + 1)) / 2, <span class="keyword">this</span>);
<a name="l00063"></a>00063 
<a name="l00064"></a>00064 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00065"></a>00065 <span class="preprocessor"></span>      }
<a name="l00066"></a>00066     <span class="keywordflow">catch</span> (...)
<a name="l00067"></a>00067       {
<a name="l00068"></a>00068         this-&gt;m_ = 0;
<a name="l00069"></a>00069         this-&gt;n_ = 0;
<a name="l00070"></a>00070         this-&gt;data_ = NULL;
<a name="l00071"></a>00071         <span class="keywordflow">return</span>;
<a name="l00072"></a>00072       }
<a name="l00073"></a>00073     <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00074"></a>00074       {
<a name="l00075"></a>00075         this-&gt;m_ = 0;
<a name="l00076"></a>00076         this-&gt;n_ = 0;
<a name="l00077"></a>00077         <span class="keywordflow">return</span>;
<a name="l00078"></a>00078       }
<a name="l00079"></a>00079 <span class="preprocessor">#endif</span>
<a name="l00080"></a>00080 <span class="preprocessor"></span>
<a name="l00081"></a>00081   }
<a name="l00082"></a>00082 
<a name="l00083"></a>00083 
<a name="l00085"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a0c7b7a12f90ac04b5c8d2c76089ebee1">00085</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00086"></a>00086   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ad6584a1ca7199865d2a4d5dc6af246fe" title="Default constructor.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00087"></a>00087 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ad6584a1ca7199865d2a4d5dc6af246fe" title="Default constructor.">  ::Matrix_HermPacked</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked</a>&lt;T, Prop,
<a name="l00088"></a>00088                       Storage, Allocator&gt;&amp; A):
<a name="l00089"></a>00089     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;()
<a name="l00090"></a>00090   {
<a name="l00091"></a>00091     this-&gt;m_ = 0;
<a name="l00092"></a>00092     this-&gt;n_ = 0;
<a name="l00093"></a>00093     this-&gt;data_ = NULL;
<a name="l00094"></a>00094 
<a name="l00095"></a>00095     this-&gt;Copy(A);
<a name="l00096"></a>00096   }
<a name="l00097"></a>00097 
<a name="l00098"></a>00098 
<a name="l00099"></a>00099   <span class="comment">/**************</span>
<a name="l00100"></a>00100 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00101"></a>00101 <span class="comment">   **************/</span>
<a name="l00102"></a>00102 
<a name="l00103"></a>00103 
<a name="l00105"></a>00105   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00106"></a>00106   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a2d9134dbc8606170ccaaf6aed472df26" title="Destructor.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::~Matrix_HermPacked</a>()
<a name="l00107"></a>00107   {
<a name="l00108"></a>00108 
<a name="l00109"></a>00109 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00110"></a>00110 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00111"></a>00111       {
<a name="l00112"></a>00112 <span class="preprocessor">#endif</span>
<a name="l00113"></a>00113 <span class="preprocessor"></span>
<a name="l00114"></a>00114         <span class="keywordflow">if</span> (this-&gt;data_ != NULL)
<a name="l00115"></a>00115           {
<a name="l00116"></a>00116             this-&gt;allocator_.deallocate(this-&gt;data_,
<a name="l00117"></a>00117                                         (this-&gt;m_ * (this-&gt;m_ + 1)) / 2);
<a name="l00118"></a>00118             this-&gt;data_ = NULL;
<a name="l00119"></a>00119           }
<a name="l00120"></a>00120 
<a name="l00121"></a>00121 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00122"></a>00122 <span class="preprocessor"></span>      }
<a name="l00123"></a>00123     <span class="keywordflow">catch</span> (...)
<a name="l00124"></a>00124       {
<a name="l00125"></a>00125         this-&gt;m_ = 0;
<a name="l00126"></a>00126         this-&gt;n_ = 0;
<a name="l00127"></a>00127         this-&gt;data_ = NULL;
<a name="l00128"></a>00128       }
<a name="l00129"></a>00129 <span class="preprocessor">#endif</span>
<a name="l00130"></a>00130 <span class="preprocessor"></span>
<a name="l00131"></a>00131   }
<a name="l00132"></a>00132 
<a name="l00133"></a>00133 
<a name="l00135"></a>00135 
<a name="l00139"></a>00139   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00140"></a>00140   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a98226901bde3cf19ae74571c38280792" title="Clears the matrix.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Clear</a>()
<a name="l00141"></a>00141   {
<a name="l00142"></a>00142     this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a2d9134dbc8606170ccaaf6aed472df26" title="Destructor.">~Matrix_HermPacked</a>();
<a name="l00143"></a>00143     this-&gt;m_ = 0;
<a name="l00144"></a>00144     this-&gt;n_ = 0;
<a name="l00145"></a>00145   }
<a name="l00146"></a>00146 
<a name="l00147"></a>00147 
<a name="l00148"></a>00148   <span class="comment">/*******************</span>
<a name="l00149"></a>00149 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00150"></a>00150 <span class="comment">   *******************/</span>
<a name="l00151"></a>00151 
<a name="l00152"></a>00152 
<a name="l00154"></a>00154 
<a name="l00157"></a>00157   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00158"></a>00158   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a8bd2747b6c90c4114cd2f54620d1f230" title="Returns the number of elements stored in memory.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::GetDataSize</a>()<span class="keyword"> const</span>
<a name="l00159"></a>00159 <span class="keyword">  </span>{
<a name="l00160"></a>00160     <span class="keywordflow">return</span> (this-&gt;m_ * (this-&gt;m_ + 1)) / 2;
<a name="l00161"></a>00161   }
<a name="l00162"></a>00162 
<a name="l00163"></a>00163 
<a name="l00164"></a>00164   <span class="comment">/*********************</span>
<a name="l00165"></a>00165 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00166"></a>00166 <span class="comment">   *********************/</span>
<a name="l00167"></a>00167 
<a name="l00168"></a>00168 
<a name="l00170"></a>00170 
<a name="l00176"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#aa2584f5a75b2ae0a2beb12fa45de3c11">00176</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00177"></a>00177   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#aa2584f5a75b2ae0a2beb12fa45de3c11" title="Reallocates memory to resize the matrix.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00178"></a>00178 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#aa2584f5a75b2ae0a2beb12fa45de3c11" title="Reallocates memory to resize the matrix.">  ::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00179"></a>00179   {
<a name="l00180"></a>00180 
<a name="l00181"></a>00181     <span class="keywordflow">if</span> (i != this-&gt;m_)
<a name="l00182"></a>00182       {
<a name="l00183"></a>00183         this-&gt;m_ = i;
<a name="l00184"></a>00184         this-&gt;n_ = i;
<a name="l00185"></a>00185 
<a name="l00186"></a>00186 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00187"></a>00187 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l00188"></a>00188           {
<a name="l00189"></a>00189 <span class="preprocessor">#endif</span>
<a name="l00190"></a>00190 <span class="preprocessor"></span>
<a name="l00191"></a>00191             this-&gt;data_ =
<a name="l00192"></a>00192               <span class="keyword">reinterpret_cast&lt;</span>pointer<span class="keyword">&gt;</span>(this-&gt;allocator_.reallocate(this-&gt;data_,
<a name="l00193"></a>00193                                                                     (i*(i + 1)) / 2,
<a name="l00194"></a>00194                                                                     <span class="keyword">this</span>));
<a name="l00195"></a>00195 
<a name="l00196"></a>00196 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00197"></a>00197 <span class="preprocessor"></span>          }
<a name="l00198"></a>00198         <span class="keywordflow">catch</span> (...)
<a name="l00199"></a>00199           {
<a name="l00200"></a>00200             this-&gt;m_ = 0;
<a name="l00201"></a>00201             this-&gt;n_ = 0;
<a name="l00202"></a>00202             this-&gt;data_ = NULL;
<a name="l00203"></a>00203             <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Reallocate(int, int)&quot;</span>,
<a name="l00204"></a>00204                            <span class="stringliteral">&quot;Unable to reallocate memory for data_.&quot;</span>);
<a name="l00205"></a>00205           }
<a name="l00206"></a>00206         <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00207"></a>00207           {
<a name="l00208"></a>00208             this-&gt;m_ = 0;
<a name="l00209"></a>00209             this-&gt;n_ = 0;
<a name="l00210"></a>00210             <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Reallocate(int, int)&quot;</span>,
<a name="l00211"></a>00211                            <span class="stringliteral">&quot;Unable to reallocate memory for data_.&quot;</span>);
<a name="l00212"></a>00212           }
<a name="l00213"></a>00213 <span class="preprocessor">#endif</span>
<a name="l00214"></a>00214 <span class="preprocessor"></span>
<a name="l00215"></a>00215       }
<a name="l00216"></a>00216   }
<a name="l00217"></a>00217 
<a name="l00218"></a>00218 
<a name="l00221"></a>00221 
<a name="l00235"></a>00235   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00236"></a>00236   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00237"></a>00237 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">  ::SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00238"></a>00238             <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00239"></a>00239             ::pointer data)
<a name="l00240"></a>00240   {
<a name="l00241"></a>00241     this-&gt;Clear();
<a name="l00242"></a>00242 
<a name="l00243"></a>00243     this-&gt;m_ = i;
<a name="l00244"></a>00244     this-&gt;n_ = i;
<a name="l00245"></a>00245 
<a name="l00246"></a>00246     this-&gt;data_ = data;
<a name="l00247"></a>00247   }
<a name="l00248"></a>00248 
<a name="l00249"></a>00249 
<a name="l00251"></a>00251 
<a name="l00255"></a>00255   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00256"></a>00256   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a4837520f92011b63c5115a19c8c49ad3" title="Clears the matrix without releasing memory.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Nullify</a>()
<a name="l00257"></a>00257   {
<a name="l00258"></a>00258     this-&gt;data_ = NULL;
<a name="l00259"></a>00259     this-&gt;m_ = 0;
<a name="l00260"></a>00260     this-&gt;n_ = 0;
<a name="l00261"></a>00261   }
<a name="l00262"></a>00262 
<a name="l00263"></a>00263 
<a name="l00264"></a>00264   <span class="comment">/**********************************</span>
<a name="l00265"></a>00265 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l00266"></a>00266 <span class="comment">   **********************************/</span>
<a name="l00267"></a>00267 
<a name="l00268"></a>00268 
<a name="l00270"></a>00270 
<a name="l00276"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a6123f522a052278ebc1853848056c9e3">00276</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00277"></a>00277   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00278"></a>00278   <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a6123f522a052278ebc1853848056c9e3" title="Access operator.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00279"></a>00279   {
<a name="l00280"></a>00280 
<a name="l00281"></a>00281 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00282"></a>00282 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00283"></a>00283       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_HermPacked::operator()&quot;</span>,
<a name="l00284"></a>00284                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00285"></a>00285                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00286"></a>00286     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00287"></a>00287       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_HermPacked::operator()&quot;</span>,
<a name="l00288"></a>00288                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00289"></a>00289                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00290"></a>00290 
<a name="l00291"></a>00291     <span class="keywordflow">if</span> (i &gt; j)
<a name="l00292"></a>00292       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_HermPacked::operator()&quot;</span>,
<a name="l00293"></a>00293                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Attempted to access to element (&quot;</span>)
<a name="l00294"></a>00294                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00295"></a>00295                      + <span class="stringliteral">&quot;) but row index should not be strictly&quot;</span>
<a name="l00296"></a>00296                      + <span class="stringliteral">&quot; more than column index.&quot;</span>);
<a name="l00297"></a>00297 <span class="preprocessor">#endif</span>
<a name="l00298"></a>00298 <span class="preprocessor"></span>
<a name="l00299"></a>00299     <span class="keywordflow">return</span> this-&gt;data_[Storage::GetFirst(i * this-&gt;n_ - (i*(i+1)) / 2 + j,
<a name="l00300"></a>00300                                          (j*(j+1)) / 2 + i)];
<a name="l00301"></a>00301   }
<a name="l00302"></a>00302 
<a name="l00303"></a>00303 
<a name="l00305"></a>00305 
<a name="l00311"></a>00311   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00312"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a165b523d2483eaf716a43f2a89363d73">00312</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::value_type</a>
<a name="l00313"></a>00313   <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a6123f522a052278ebc1853848056c9e3" title="Access operator.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00314"></a>00314 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a6123f522a052278ebc1853848056c9e3" title="Access operator.">  ::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00315"></a>00315 <span class="keyword">  </span>{
<a name="l00316"></a>00316 
<a name="l00317"></a>00317 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00318"></a>00318 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00319"></a>00319       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_HermPacked::operator()&quot;</span>,
<a name="l00320"></a>00320                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00321"></a>00321                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00322"></a>00322     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00323"></a>00323       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_HermPacked::operator()&quot;</span>,
<a name="l00324"></a>00324                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00325"></a>00325                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00326"></a>00326 <span class="preprocessor">#endif</span>
<a name="l00327"></a>00327 <span class="preprocessor"></span>
<a name="l00328"></a>00328     <span class="keywordflow">if</span> (i &gt; j)
<a name="l00329"></a>00329       <span class="keywordflow">return</span> conj(this-&gt;data_[Storage::GetFirst(j * this-&gt;m_
<a name="l00330"></a>00330                                                 - (j*(j+1)) / 2 + i,
<a name="l00331"></a>00331                                                 (i*(i+1)) / 2 + j)]);
<a name="l00332"></a>00332     <span class="keywordflow">else</span>
<a name="l00333"></a>00333       <span class="keywordflow">return</span> this-&gt;data_[Storage::GetFirst(i * this-&gt;n_ - (i*(i+1)) / 2 + j,
<a name="l00334"></a>00334                                            (j*(j+1)) / 2 + i)];
<a name="l00335"></a>00335   }
<a name="l00336"></a>00336 
<a name="l00337"></a>00337 
<a name="l00339"></a>00339 
<a name="l00346"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ae7f67ca8d3876a5fbdbce8ac3c914f36">00346</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00347"></a>00347   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00348"></a>00348   <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ae7f67ca8d3876a5fbdbce8ac3c914f36" title="Direct access method.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00349"></a>00349   {
<a name="l00350"></a>00350 
<a name="l00351"></a>00351 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00352"></a>00352 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00353"></a>00353       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Val(int, int)&quot;</span>,
<a name="l00354"></a>00354                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00355"></a>00355                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00356"></a>00356     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00357"></a>00357       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Val(int, int)&quot;</span>,
<a name="l00358"></a>00358                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00359"></a>00359                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00360"></a>00360     <span class="keywordflow">if</span> (i &gt; j)
<a name="l00361"></a>00361       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Val(int, int)&quot;</span>,
<a name="l00362"></a>00362                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Attempted to access to element (&quot;</span>)
<a name="l00363"></a>00363                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00364"></a>00364                      + <span class="stringliteral">&quot;) but row index should not be strictly&quot;</span>
<a name="l00365"></a>00365                      + <span class="stringliteral">&quot; more than column index.&quot;</span>);
<a name="l00366"></a>00366 <span class="preprocessor">#endif</span>
<a name="l00367"></a>00367 <span class="preprocessor"></span>
<a name="l00368"></a>00368     <span class="keywordflow">return</span> this-&gt;data_[Storage::GetFirst(i * this-&gt;n_ - (i*(i+1)) / 2 + j,
<a name="l00369"></a>00369                                          (j*(j+1)) / 2 + i)];
<a name="l00370"></a>00370   }
<a name="l00371"></a>00371 
<a name="l00372"></a>00372 
<a name="l00374"></a>00374 
<a name="l00381"></a>00381   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00382"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a7eda051ec34190ad13528f89d233acdb">00382</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00383"></a>00383   ::const_reference
<a name="l00384"></a>00384   <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ae7f67ca8d3876a5fbdbce8ac3c914f36" title="Direct access method.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00385"></a>00385 <span class="keyword">  </span>{
<a name="l00386"></a>00386 
<a name="l00387"></a>00387 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00388"></a>00388 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00389"></a>00389       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Val(int, int) const&quot;</span>,
<a name="l00390"></a>00390                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00391"></a>00391                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00392"></a>00392     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00393"></a>00393       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Val(int, int) cont&quot;</span>,
<a name="l00394"></a>00394                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00395"></a>00395                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00396"></a>00396     <span class="keywordflow">if</span> (i &gt; j)
<a name="l00397"></a>00397       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Val(int, int) const&quot;</span>,
<a name="l00398"></a>00398                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Attempted to access to element (&quot;</span>)
<a name="l00399"></a>00399                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00400"></a>00400                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;) but row index should not be strictly&quot;</span>)
<a name="l00401"></a>00401                      + <span class="stringliteral">&quot; more than column index.&quot;</span>);
<a name="l00402"></a>00402 <span class="preprocessor">#endif</span>
<a name="l00403"></a>00403 <span class="preprocessor"></span>
<a name="l00404"></a>00404     <span class="keywordflow">return</span> this-&gt;data_[Storage::GetFirst(i * this-&gt;n_ - (i*(i+1)) / 2 + j,
<a name="l00405"></a>00405                                          (j*(j+1)) / 2 + i)];
<a name="l00406"></a>00406   }
<a name="l00407"></a>00407 
<a name="l00408"></a>00408 
<a name="l00410"></a>00410 
<a name="l00415"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a804fa785b8d17fd2d56d01226bce844a">00415</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00416"></a>00416   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00417"></a>00417   <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a804fa785b8d17fd2d56d01226bce844a" title="Access to elements of the data array.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::operator[] </a>(<span class="keywordtype">int</span> i)
<a name="l00418"></a>00418   {
<a name="l00419"></a>00419 
<a name="l00420"></a>00420 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00421"></a>00421 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a8bd2747b6c90c4114cd2f54620d1f230" title="Returns the number of elements stored in memory.">GetDataSize</a>())
<a name="l00422"></a>00422       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Matrix_HermPacked::operator[] (int)&quot;</span>,
<a name="l00423"></a>00423                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00424"></a>00424                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a8bd2747b6c90c4114cd2f54620d1f230" title="Returns the number of elements stored in memory.">GetDataSize</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00425"></a>00425                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00426"></a>00426 <span class="preprocessor">#endif</span>
<a name="l00427"></a>00427 <span class="preprocessor"></span>
<a name="l00428"></a>00428     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00429"></a>00429   }
<a name="l00430"></a>00430 
<a name="l00431"></a>00431 
<a name="l00433"></a>00433 
<a name="l00438"></a>00438   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00439"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a77a7f1c5d26dc807a7fe5c7cebabd94b">00439</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00440"></a>00440   ::const_reference
<a name="l00441"></a>00441   <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a804fa785b8d17fd2d56d01226bce844a" title="Access to elements of the data array.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::operator[] </a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00442"></a>00442 <span class="keyword">  </span>{
<a name="l00443"></a>00443 
<a name="l00444"></a>00444 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00445"></a>00445 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a8bd2747b6c90c4114cd2f54620d1f230" title="Returns the number of elements stored in memory.">GetDataSize</a>())
<a name="l00446"></a>00446       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Matrix_HermPacked::operator[] (int) const&quot;</span>,
<a name="l00447"></a>00447                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00448"></a>00448                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a8bd2747b6c90c4114cd2f54620d1f230" title="Returns the number of elements stored in memory.">GetDataSize</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00449"></a>00449                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00450"></a>00450 <span class="preprocessor">#endif</span>
<a name="l00451"></a>00451 <span class="preprocessor"></span>
<a name="l00452"></a>00452     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00453"></a>00453   }
<a name="l00454"></a>00454 
<a name="l00455"></a>00455 
<a name="l00457"></a>00457 
<a name="l00462"></a>00462   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00463"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ad57f72a50f4f8bdb421c9f02251bde85">00463</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00464"></a>00464   <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ad57f72a50f4f8bdb421c9f02251bde85" title="Duplicates a matrix (assignment operator).">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00465"></a>00465 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ad57f72a50f4f8bdb421c9f02251bde85" title="Duplicates a matrix (assignment operator).">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00466"></a>00466   {
<a name="l00467"></a>00467     this-&gt;Copy(A);
<a name="l00468"></a>00468 
<a name="l00469"></a>00469     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00470"></a>00470   }
<a name="l00471"></a>00471 
<a name="l00472"></a>00472 
<a name="l00474"></a>00474 
<a name="l00479"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a43f929eaa7b183e23c49d9ad31e5699e">00479</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00480"></a>00480   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a43f929eaa7b183e23c49d9ad31e5699e" title="Duplicates a matrix.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00481"></a>00481 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a43f929eaa7b183e23c49d9ad31e5699e" title="Duplicates a matrix.">  ::Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00482"></a>00482   {
<a name="l00483"></a>00483     this-&gt;Reallocate(A.<a class="code" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927" title="Returns the number of rows.">GetM</a>(), A.<a class="code" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984" title="Returns the number of columns.">GetN</a>());
<a name="l00484"></a>00484 
<a name="l00485"></a>00485     this-&gt;allocator_.memorycpy(this-&gt;data_, A.<a class="code" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a" title="Returns a pointer to the data array.">GetData</a>(), this-&gt;GetDataSize());
<a name="l00486"></a>00486   }
<a name="l00487"></a>00487 
<a name="l00488"></a>00488 
<a name="l00489"></a>00489   <span class="comment">/************************</span>
<a name="l00490"></a>00490 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l00491"></a>00491 <span class="comment">   ************************/</span>
<a name="l00492"></a>00492 
<a name="l00493"></a>00493 
<a name="l00495"></a>00495 
<a name="l00499"></a>00499   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00500"></a>00500   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a6222342fc8d2e7fccf530c892e259782" title="Sets all elements to zero.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Zero</a>()
<a name="l00501"></a>00501   {
<a name="l00502"></a>00502     this-&gt;allocator_.memoryset(this-&gt;data_, <span class="keywordtype">char</span>(0),
<a name="l00503"></a>00503                                this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a8bd2747b6c90c4114cd2f54620d1f230" title="Returns the number of elements stored in memory.">GetDataSize</a>() * <span class="keyword">sizeof</span>(value_type));
<a name="l00504"></a>00504   }
<a name="l00505"></a>00505 
<a name="l00506"></a>00506 
<a name="l00508"></a>00508   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00509"></a>00509   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a5e62a678cb672277c536d1ad9aad96f1" title="Sets the matrix to the identity.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::SetIdentity</a>()
<a name="l00510"></a>00510   {
<a name="l00511"></a>00511     this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#acb848b52cbdb82504a1e79fd5e75a87a" title="Fills the matrix with 0, 1, 2, ...">Fill</a>(T(0));
<a name="l00512"></a>00512 
<a name="l00513"></a>00513     T one(1);
<a name="l00514"></a>00514     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; min(this-&gt;m_, this-&gt;n_); i++)
<a name="l00515"></a>00515       (*<span class="keyword">this</span>)(i,i) = one;
<a name="l00516"></a>00516   }
<a name="l00517"></a>00517 
<a name="l00518"></a>00518 
<a name="l00520"></a>00520 
<a name="l00524"></a>00524   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00525"></a>00525   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#acb848b52cbdb82504a1e79fd5e75a87a" title="Fills the matrix with 0, 1, 2, ...">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Fill</a>()
<a name="l00526"></a>00526   {
<a name="l00527"></a>00527     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a8bd2747b6c90c4114cd2f54620d1f230" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00528"></a>00528       this-&gt;data_[i] = i;
<a name="l00529"></a>00529   }
<a name="l00530"></a>00530 
<a name="l00531"></a>00531 
<a name="l00533"></a>00533 
<a name="l00536"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a498b54d93b35a42bfb524cc697bb8d5b">00536</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00537"></a>00537   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00538"></a>00538   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#acb848b52cbdb82504a1e79fd5e75a87a" title="Fills the matrix with 0, 1, 2, ...">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Fill</a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00539"></a>00539   {
<a name="l00540"></a>00540     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a8bd2747b6c90c4114cd2f54620d1f230" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00541"></a>00541       this-&gt;data_[i] = x;
<a name="l00542"></a>00542   }
<a name="l00543"></a>00543 
<a name="l00544"></a>00544 
<a name="l00546"></a>00546 
<a name="l00549"></a>00549   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00550"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a892540112f14801e1b48f3cf18b112ab">00550</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00551"></a>00551   <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00552"></a>00552   <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#ad57f72a50f4f8bdb421c9f02251bde85" title="Duplicates a matrix (assignment operator).">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00553"></a>00553   {
<a name="l00554"></a>00554     this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#acb848b52cbdb82504a1e79fd5e75a87a" title="Fills the matrix with 0, 1, 2, ...">Fill</a>(x);
<a name="l00555"></a>00555 
<a name="l00556"></a>00556     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00557"></a>00557   }
<a name="l00558"></a>00558 
<a name="l00559"></a>00559 
<a name="l00561"></a>00561 
<a name="l00564"></a>00564   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00565"></a>00565   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a24f99cb9628f8259310d685a55ecbb86" title="Fills the matrix randomly.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::FillRand</a>()
<a name="l00566"></a>00566   {
<a name="l00567"></a>00567     srand(time(NULL));
<a name="l00568"></a>00568     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a8bd2747b6c90c4114cd2f54620d1f230" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00569"></a>00569       this-&gt;data_[i] = rand();
<a name="l00570"></a>00570   }
<a name="l00571"></a>00571 
<a name="l00572"></a>00572 
<a name="l00574"></a>00574 
<a name="l00579"></a>00579   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00580"></a>00580   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a149556a4c7e8b8e9c952a7f3bc2f5942" title="Displays the matrix on the standard output.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00581"></a>00581 <span class="keyword">  </span>{
<a name="l00582"></a>00582     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00583"></a>00583       {
<a name="l00584"></a>00584         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;n_; j++)
<a name="l00585"></a>00585           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00586"></a>00586         cout &lt;&lt; endl;
<a name="l00587"></a>00587       }
<a name="l00588"></a>00588   }
<a name="l00589"></a>00589 
<a name="l00590"></a>00590 
<a name="l00592"></a>00592 
<a name="l00603"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a379079777210c4dfd9377f5fbc843c85">00603</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00604"></a>00604   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a149556a4c7e8b8e9c952a7f3bc2f5942" title="Displays the matrix on the standard output.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00605"></a>00605 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a149556a4c7e8b8e9c952a7f3bc2f5942" title="Displays the matrix on the standard output.">  ::Print</a>(<span class="keywordtype">int</span> a, <span class="keywordtype">int</span> b, <span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n)<span class="keyword"> const</span>
<a name="l00606"></a>00606 <span class="keyword">  </span>{
<a name="l00607"></a>00607     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = a; i &lt; min(this-&gt;m_, a+m); i++)
<a name="l00608"></a>00608       {
<a name="l00609"></a>00609         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = b; j &lt; min(this-&gt;n_, b+n); j++)
<a name="l00610"></a>00610           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00611"></a>00611         cout &lt;&lt; endl;
<a name="l00612"></a>00612       }
<a name="l00613"></a>00613   }
<a name="l00614"></a>00614 
<a name="l00615"></a>00615 
<a name="l00617"></a>00617 
<a name="l00625"></a>00625   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00626"></a>00626   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a149556a4c7e8b8e9c952a7f3bc2f5942" title="Displays the matrix on the standard output.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Print</a>(<span class="keywordtype">int</span> l)<span class="keyword"> const</span>
<a name="l00627"></a>00627 <span class="keyword">  </span>{
<a name="l00628"></a>00628     <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a149556a4c7e8b8e9c952a7f3bc2f5942" title="Displays the matrix on the standard output.">Print</a>(0, 0, l, l);
<a name="l00629"></a>00629   }
<a name="l00630"></a>00630 
<a name="l00631"></a>00631 
<a name="l00632"></a>00632   <span class="comment">/**************************</span>
<a name="l00633"></a>00633 <span class="comment">   * INPUT/OUTPUT FUNCTIONS *</span>
<a name="l00634"></a>00634 <span class="comment">   **************************/</span>
<a name="l00635"></a>00635 
<a name="l00636"></a>00636 
<a name="l00638"></a>00638 
<a name="l00645"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a3a8a9450a83c1e5aaa301a8f33cc3eb1">00645</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00646"></a>00646   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a3a8a9450a83c1e5aaa301a8f33cc3eb1" title="Writes the matrix in a file.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00647"></a>00647 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a3a8a9450a83c1e5aaa301a8f33cc3eb1" title="Writes the matrix in a file.">  ::Write</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00648"></a>00648 <span class="keyword">  </span>{
<a name="l00649"></a>00649 
<a name="l00650"></a>00650     ofstream FileStream;
<a name="l00651"></a>00651     FileStream.open(FileName.c_str());
<a name="l00652"></a>00652 
<a name="l00653"></a>00653 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00654"></a>00654 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00655"></a>00655     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00656"></a>00656       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Write(string FileName)&quot;</span>,
<a name="l00657"></a>00657                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00658"></a>00658 <span class="preprocessor">#endif</span>
<a name="l00659"></a>00659 <span class="preprocessor"></span>
<a name="l00660"></a>00660     this-&gt;Write(FileStream);
<a name="l00661"></a>00661 
<a name="l00662"></a>00662     FileStream.close();
<a name="l00663"></a>00663 
<a name="l00664"></a>00664   }
<a name="l00665"></a>00665 
<a name="l00666"></a>00666 
<a name="l00668"></a>00668 
<a name="l00675"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a57a7e0eb6fa3a34edfe3334ff460faec">00675</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00676"></a>00676   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a3a8a9450a83c1e5aaa301a8f33cc3eb1" title="Writes the matrix in a file.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00677"></a>00677 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a3a8a9450a83c1e5aaa301a8f33cc3eb1" title="Writes the matrix in a file.">  ::Write</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00678"></a>00678 <span class="keyword">  </span>{
<a name="l00679"></a>00679 
<a name="l00680"></a>00680 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00681"></a>00681 <span class="preprocessor"></span>    <span class="comment">// Checks if the file is ready.</span>
<a name="l00682"></a>00682     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00683"></a>00683       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l00684"></a>00684                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00685"></a>00685 <span class="preprocessor">#endif</span>
<a name="l00686"></a>00686 <span class="preprocessor"></span>
<a name="l00687"></a>00687     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l00688"></a>00688                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00689"></a>00689     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;n_)),
<a name="l00690"></a>00690                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00691"></a>00691 
<a name="l00692"></a>00692     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l00693"></a>00693                      this-&gt;GetDataSize() * <span class="keyword">sizeof</span>(value_type));
<a name="l00694"></a>00694 
<a name="l00695"></a>00695 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00696"></a>00696 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00697"></a>00697     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00698"></a>00698       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l00699"></a>00699                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Output operation failed.&quot;</span>)
<a name="l00700"></a>00700                     + string(<span class="stringliteral">&quot; The output file may have been removed&quot;</span>)
<a name="l00701"></a>00701                     + <span class="stringliteral">&quot; or there is no space left on device.&quot;</span>);
<a name="l00702"></a>00702 <span class="preprocessor">#endif</span>
<a name="l00703"></a>00703 <span class="preprocessor"></span>
<a name="l00704"></a>00704   }
<a name="l00705"></a>00705 
<a name="l00706"></a>00706 
<a name="l00708"></a>00708 
<a name="l00715"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a33aa63ce5334152d99d56de03a103f45">00715</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00716"></a>00716   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a33aa63ce5334152d99d56de03a103f45" title="Writes the matrix in a file.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00717"></a>00717 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a33aa63ce5334152d99d56de03a103f45" title="Writes the matrix in a file.">  ::WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00718"></a>00718 <span class="keyword">  </span>{
<a name="l00719"></a>00719     ofstream FileStream;
<a name="l00720"></a>00720     FileStream.precision(cout.precision());
<a name="l00721"></a>00721     FileStream.flags(cout.flags());
<a name="l00722"></a>00722     FileStream.open(FileName.c_str());
<a name="l00723"></a>00723 
<a name="l00724"></a>00724 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00725"></a>00725 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00726"></a>00726     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00727"></a>00727       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_HermPacked::WriteText(string FileName)&quot;</span>,
<a name="l00728"></a>00728                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00729"></a>00729 <span class="preprocessor">#endif</span>
<a name="l00730"></a>00730 <span class="preprocessor"></span>
<a name="l00731"></a>00731     this-&gt;WriteText(FileStream);
<a name="l00732"></a>00732 
<a name="l00733"></a>00733     FileStream.close();
<a name="l00734"></a>00734   }
<a name="l00735"></a>00735 
<a name="l00736"></a>00736 
<a name="l00738"></a>00738 
<a name="l00745"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a15b7c7126f1e630b644a29d0c1d38ecd">00745</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00746"></a>00746   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a33aa63ce5334152d99d56de03a103f45" title="Writes the matrix in a file.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00747"></a>00747 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a33aa63ce5334152d99d56de03a103f45" title="Writes the matrix in a file.">  ::WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00748"></a>00748 <span class="keyword">  </span>{
<a name="l00749"></a>00749 
<a name="l00750"></a>00750 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00751"></a>00751 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00752"></a>00752     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00753"></a>00753       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_HermPacked::WriteText(ofstream&amp; FileStream)&quot;</span>,
<a name="l00754"></a>00754                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00755"></a>00755 <span class="preprocessor">#endif</span>
<a name="l00756"></a>00756 <span class="preprocessor"></span>
<a name="l00757"></a>00757     <span class="keywordtype">int</span> i, j;
<a name="l00758"></a>00758     <span class="keywordflow">for</span> (i = 0; i &lt; this-&gt;GetM(); i++)
<a name="l00759"></a>00759       {
<a name="l00760"></a>00760         <span class="keywordflow">for</span> (j = 0; j &lt; this-&gt;GetN(); j++)
<a name="l00761"></a>00761           FileStream &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="charliteral">&#39;\t&#39;</span>;
<a name="l00762"></a>00762         FileStream &lt;&lt; endl;
<a name="l00763"></a>00763       }
<a name="l00764"></a>00764 
<a name="l00765"></a>00765 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00766"></a>00766 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00767"></a>00767     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00768"></a>00768       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_HermPacked::WriteText(ofstream&amp; FileStream)&quot;</span>,
<a name="l00769"></a>00769                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Output operation failed.&quot;</span>)
<a name="l00770"></a>00770                     + string(<span class="stringliteral">&quot; The output file may have been removed&quot;</span>)
<a name="l00771"></a>00771                     + <span class="stringliteral">&quot; or there is no space left on device.&quot;</span>);
<a name="l00772"></a>00772 <span class="preprocessor">#endif</span>
<a name="l00773"></a>00773 <span class="preprocessor"></span>
<a name="l00774"></a>00774   }
<a name="l00775"></a>00775 
<a name="l00776"></a>00776 
<a name="l00778"></a>00778 
<a name="l00785"></a>00785   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00786"></a>00786   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a19e44f4967fe55af83ef524f0f9be125" title="Reads the matrix from a file.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::Read</a>(<span class="keywordtype">string</span> FileName)
<a name="l00787"></a>00787   {
<a name="l00788"></a>00788     ifstream FileStream;
<a name="l00789"></a>00789     FileStream.open(FileName.c_str());
<a name="l00790"></a>00790 
<a name="l00791"></a>00791 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00792"></a>00792 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00793"></a>00793     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00794"></a>00794       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Read(string FileName)&quot;</span>,
<a name="l00795"></a>00795                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00796"></a>00796 <span class="preprocessor">#endif</span>
<a name="l00797"></a>00797 <span class="preprocessor"></span>
<a name="l00798"></a>00798     this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a19e44f4967fe55af83ef524f0f9be125" title="Reads the matrix from a file.">Read</a>(FileStream);
<a name="l00799"></a>00799 
<a name="l00800"></a>00800     FileStream.close();
<a name="l00801"></a>00801   }
<a name="l00802"></a>00802 
<a name="l00803"></a>00803 
<a name="l00805"></a>00805 
<a name="l00812"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a973fdab38a4f413ceec28d4c6b696d0c">00812</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00813"></a>00813   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a19e44f4967fe55af83ef524f0f9be125" title="Reads the matrix from a file.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00814"></a>00814 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a19e44f4967fe55af83ef524f0f9be125" title="Reads the matrix from a file.">  ::Read</a>(istream&amp; FileStream)
<a name="l00815"></a>00815   {
<a name="l00816"></a>00816 
<a name="l00817"></a>00817 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00818"></a>00818 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00819"></a>00819     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00820"></a>00820       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Read(ifstream&amp; FileStream)&quot;</span>,
<a name="l00821"></a>00821                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00822"></a>00822 <span class="preprocessor">#endif</span>
<a name="l00823"></a>00823 <span class="preprocessor"></span>
<a name="l00824"></a>00824     <span class="keywordtype">int</span> new_m, new_n;
<a name="l00825"></a>00825     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;new_m), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00826"></a>00826     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;new_n), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00827"></a>00827     this-&gt;Reallocate(new_m, new_n);
<a name="l00828"></a>00828 
<a name="l00829"></a>00829     FileStream.read(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l00830"></a>00830                     this-&gt;GetDataSize() * <span class="keyword">sizeof</span>(value_type));
<a name="l00831"></a>00831 
<a name="l00832"></a>00832 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00833"></a>00833 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l00834"></a>00834     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00835"></a>00835       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_HermPacked::Read(ifstream&amp; FileStream)&quot;</span>,
<a name="l00836"></a>00836                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Output operation failed.&quot;</span>)
<a name="l00837"></a>00837                     + string(<span class="stringliteral">&quot; The intput file may have been removed&quot;</span>)
<a name="l00838"></a>00838                     + <span class="stringliteral">&quot; or may not contain enough data.&quot;</span>);
<a name="l00839"></a>00839 <span class="preprocessor">#endif</span>
<a name="l00840"></a>00840 <span class="preprocessor"></span>
<a name="l00841"></a>00841   }
<a name="l00842"></a>00842 
<a name="l00843"></a>00843 
<a name="l00845"></a>00845 
<a name="l00849"></a>00849   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00850"></a>00850   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#aacf187c2102385c003476b6674d27fa1" title="Reads the matrix from a file.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;::ReadText</a>(<span class="keywordtype">string</span> FileName)
<a name="l00851"></a>00851   {
<a name="l00852"></a>00852     ifstream FileStream;
<a name="l00853"></a>00853     FileStream.open(FileName.c_str());
<a name="l00854"></a>00854 
<a name="l00855"></a>00855 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00856"></a>00856 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00857"></a>00857     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00858"></a>00858       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(string FileName)&quot;</span>,
<a name="l00859"></a>00859                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00860"></a>00860 <span class="preprocessor">#endif</span>
<a name="l00861"></a>00861 <span class="preprocessor"></span>
<a name="l00862"></a>00862     this-&gt;<a class="code" href="class_seldon_1_1_matrix___herm_packed.php#aacf187c2102385c003476b6674d27fa1" title="Reads the matrix from a file.">ReadText</a>(FileStream);
<a name="l00863"></a>00863 
<a name="l00864"></a>00864     FileStream.close();
<a name="l00865"></a>00865   }
<a name="l00866"></a>00866 
<a name="l00867"></a>00867 
<a name="l00869"></a>00869 
<a name="l00873"></a><a class="code" href="class_seldon_1_1_matrix___herm_packed.php#a2c77fe462fbafce6965c88e55ebec79d">00873</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00874"></a>00874   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#aacf187c2102385c003476b6674d27fa1" title="Reads the matrix from a file.">Matrix_HermPacked&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00875"></a>00875 <a class="code" href="class_seldon_1_1_matrix___herm_packed.php#aacf187c2102385c003476b6674d27fa1" title="Reads the matrix from a file.">  ::ReadText</a>(istream&amp; FileStream)
<a name="l00876"></a>00876   {
<a name="l00877"></a>00877     <span class="comment">// clears previous matrix</span>
<a name="l00878"></a>00878     Clear();
<a name="l00879"></a>00879 
<a name="l00880"></a>00880 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00881"></a>00881 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00882"></a>00882     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00883"></a>00883       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(ifstream&amp; FileStream)&quot;</span>,
<a name="l00884"></a>00884                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00885"></a>00885 <span class="preprocessor">#endif</span>
<a name="l00886"></a>00886 <span class="preprocessor"></span>
<a name="l00887"></a>00887     <span class="comment">// we read first line</span>
<a name="l00888"></a>00888     <span class="keywordtype">string</span> line;
<a name="l00889"></a>00889     getline(FileStream, line);
<a name="l00890"></a>00890 
<a name="l00891"></a>00891     <span class="keywordflow">if</span> (FileStream.fail())
<a name="l00892"></a>00892       {
<a name="l00893"></a>00893         <span class="comment">// empty file ?</span>
<a name="l00894"></a>00894         <span class="keywordflow">return</span>;
<a name="l00895"></a>00895       }
<a name="l00896"></a>00896 
<a name="l00897"></a>00897     <span class="comment">// converting first line into a vector</span>
<a name="l00898"></a>00898     istringstream line_stream(line);
<a name="l00899"></a>00899     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> first_row;
<a name="l00900"></a>00900     first_row.ReadText(line_stream);
<a name="l00901"></a>00901 
<a name="l00902"></a>00902     <span class="comment">// and now the other rows</span>
<a name="l00903"></a>00903     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> other_rows;
<a name="l00904"></a>00904     other_rows.ReadText(FileStream);
<a name="l00905"></a>00905 
<a name="l00906"></a>00906     <span class="comment">// number of rows and columns</span>
<a name="l00907"></a>00907     <span class="keywordtype">int</span> n = first_row.GetM();
<a name="l00908"></a>00908     <span class="keywordtype">int</span> m = 1 + other_rows.GetM()/n;
<a name="l00909"></a>00909 
<a name="l00910"></a>00910 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00911"></a>00911 <span class="preprocessor"></span>    <span class="comment">// Checking number of elements</span>
<a name="l00912"></a>00912     <span class="keywordflow">if</span> (other_rows.GetM() != (m-1)*n)
<a name="l00913"></a>00913       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(ifstream&amp; FileStream)&quot;</span>,
<a name="l00914"></a>00914                     <span class="stringliteral">&quot;The file should contain same number of columns.&quot;</span>);
<a name="l00915"></a>00915 <span class="preprocessor">#endif</span>
<a name="l00916"></a>00916 <span class="preprocessor"></span>
<a name="l00917"></a>00917     this-&gt;Reallocate(m,n);
<a name="l00918"></a>00918     <span class="comment">// filling matrix</span>
<a name="l00919"></a>00919     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l00920"></a>00920       this-&gt;Val(0, j) = first_row(j);
<a name="l00921"></a>00921 
<a name="l00922"></a>00922     <span class="keywordtype">int</span> nb = 0;
<a name="l00923"></a>00923     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 1; i &lt; m; i++)
<a name="l00924"></a>00924       {
<a name="l00925"></a>00925         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; i; j++)
<a name="l00926"></a>00926           nb++;
<a name="l00927"></a>00927 
<a name="l00928"></a>00928         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = i; j &lt; n; j++)
<a name="l00929"></a>00929           this-&gt;Val(i, j) = other_rows(nb++);
<a name="l00930"></a>00930       }
<a name="l00931"></a>00931   }
<a name="l00932"></a>00932 
<a name="l00933"></a>00933 
<a name="l00934"></a>00934 
<a name="l00936"></a>00936   <span class="comment">// MATRIX&lt;COLHERMPACKED&gt; //</span>
<a name="l00938"></a>00938 <span class="comment"></span>
<a name="l00939"></a>00939 
<a name="l00940"></a>00940   <span class="comment">/****************</span>
<a name="l00941"></a>00941 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00942"></a>00942 <span class="comment">   ****************/</span>
<a name="l00943"></a>00943 
<a name="l00944"></a>00944 
<a name="l00946"></a>00946 
<a name="l00949"></a>00949   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00950"></a>00950   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColHermPacked, Allocator&gt;::Matrix</a>():
<a name="l00951"></a>00951     <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_herm_packed.php">ColHermPacked</a>, Allocator&gt;()
<a name="l00952"></a>00952   {
<a name="l00953"></a>00953   }
<a name="l00954"></a>00954 
<a name="l00955"></a>00955 
<a name="l00957"></a>00957 
<a name="l00962"></a>00962   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00963"></a>00963   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php#a45762f0c3a34c78f8d6e99b74f269e1f" title="Default constructor.">Matrix&lt;T, Prop, ColHermPacked, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l00964"></a>00964     Matrix_HermPacked&lt;T, Prop, ColHermPacked, Allocator&gt;(i, j)
<a name="l00965"></a>00965   {
<a name="l00966"></a>00966   }
<a name="l00967"></a>00967 
<a name="l00968"></a>00968 
<a name="l00969"></a>00969   <span class="comment">/*******************</span>
<a name="l00970"></a>00970 <span class="comment">   * OTHER FUNCTIONS *</span>
<a name="l00971"></a>00971 <span class="comment">   *******************/</span>
<a name="l00972"></a>00972 
<a name="l00973"></a>00973 
<a name="l00975"></a>00975 
<a name="l00978"></a>00978   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00979"></a>00979   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00980"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php#ad8f1511c8abd5cf290c32c1ec43dad01">00980</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php" title="Column-major hermitian packed matrix class.">Matrix&lt;T, Prop, ColHermPacked, Allocator&gt;</a>&amp;
<a name="l00981"></a>00981   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColHermPacked, Allocator&gt;</a>
<a name="l00982"></a>00982 <a class="code" href="class_seldon_1_1_matrix.php">  ::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00983"></a>00983   {
<a name="l00984"></a>00984     this-&gt;Fill(x);
<a name="l00985"></a>00985 
<a name="l00986"></a>00986     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00987"></a>00987   }
<a name="l00988"></a>00988 
<a name="l00989"></a>00989 
<a name="l00991"></a>00991 
<a name="l00994"></a>00994   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00995"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php#a53e8c9f819dad5bb1d40a9b6edd24005">00995</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00996"></a>00996   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php" title="Column-major hermitian packed matrix class.">Matrix&lt;T, Prop, ColHermPacked, Allocator&gt;</a>&amp;
<a name="l00997"></a>00997   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColHermPacked, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00998"></a>00998   {
<a name="l00999"></a>00999     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetDataSize();i++)
<a name="l01000"></a>01000       this-&gt;data_[i] *= x;
<a name="l01001"></a>01001 
<a name="l01002"></a>01002     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01003"></a>01003   }
<a name="l01004"></a>01004 
<a name="l01005"></a>01005 
<a name="l01007"></a>01007 
<a name="l01014"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php#a46426f0740af4a12d2873b9b692f6278">01014</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01015"></a>01015   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColHermPacked, Allocator&gt;</a>
<a name="l01016"></a>01016 <a class="code" href="class_seldon_1_1_matrix.php">  ::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01017"></a>01017   {
<a name="l01018"></a>01018 
<a name="l01019"></a>01019     <span class="comment">// Storing the old values of the matrix.</span>
<a name="l01020"></a>01020     <span class="keywordtype">int</span> nold = this-&gt;GetDataSize();
<a name="l01021"></a>01021     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> xold(nold);
<a name="l01022"></a>01022     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; nold; k++)
<a name="l01023"></a>01023       xold(k) = this-&gt;data_[k];
<a name="l01024"></a>01024 
<a name="l01025"></a>01025     <span class="comment">// Reallocation.</span>
<a name="l01026"></a>01026     this-&gt;Reallocate(i, j);
<a name="l01027"></a>01027 
<a name="l01028"></a>01028     <span class="comment">// Filling the matrix with its old values.</span>
<a name="l01029"></a>01029     <span class="keywordtype">int</span> nmin = min(nold, this-&gt;GetDataSize());
<a name="l01030"></a>01030     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; nmin; k++)
<a name="l01031"></a>01031       this-&gt;data_[k] = xold(k);
<a name="l01032"></a>01032   }
<a name="l01033"></a>01033 
<a name="l01034"></a>01034 
<a name="l01036"></a>01036   <span class="comment">// MATRIX&lt;ROWHERMPACKED&gt; //</span>
<a name="l01038"></a>01038 <span class="comment"></span>
<a name="l01039"></a>01039 
<a name="l01040"></a>01040   <span class="comment">/****************</span>
<a name="l01041"></a>01041 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01042"></a>01042 <span class="comment">   ****************/</span>
<a name="l01043"></a>01043 
<a name="l01044"></a>01044 
<a name="l01046"></a>01046 
<a name="l01049"></a>01049   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01050"></a>01050   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowHermPacked, Allocator&gt;::Matrix</a>():
<a name="l01051"></a>01051     <a class="code" href="class_seldon_1_1_matrix___herm_packed.php" title="Hermitian packed matrix class.">Matrix_HermPacked</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_herm_packed.php">RowHermPacked</a>, Allocator&gt;()
<a name="l01052"></a>01052   {
<a name="l01053"></a>01053   }
<a name="l01054"></a>01054 
<a name="l01055"></a>01055 
<a name="l01057"></a>01057 
<a name="l01062"></a>01062   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01063"></a>01063   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_packed_00_01_allocator_01_4.php#ab2ca86f35ad38650084f05f6b15ca476" title="Default constructor.">Matrix&lt;T, Prop, RowHermPacked, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01064"></a>01064     Matrix_HermPacked&lt;T, Prop, RowHermPacked, Allocator&gt;(i, j)
<a name="l01065"></a>01065   {
<a name="l01066"></a>01066   }
<a name="l01067"></a>01067 
<a name="l01068"></a>01068 
<a name="l01069"></a>01069   <span class="comment">/*******************</span>
<a name="l01070"></a>01070 <span class="comment">   * OTHER FUNCTIONS *</span>
<a name="l01071"></a>01071 <span class="comment">   *******************/</span>
<a name="l01072"></a>01072 
<a name="l01073"></a>01073 
<a name="l01075"></a>01075 
<a name="l01078"></a>01078   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01079"></a>01079   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01080"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_packed_00_01_allocator_01_4.php#a4edcaa25fa32c27fa993e46fcc3472a0">01080</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_packed_00_01_allocator_01_4.php" title="Row-major hermitian packed matrix class.">Matrix&lt;T, Prop, RowHermPacked, Allocator&gt;</a>&amp;
<a name="l01081"></a>01081   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowHermPacked, Allocator&gt;</a>
<a name="l01082"></a>01082 <a class="code" href="class_seldon_1_1_matrix.php">  ::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01083"></a>01083   {
<a name="l01084"></a>01084     this-&gt;Fill(x);
<a name="l01085"></a>01085 
<a name="l01086"></a>01086     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01087"></a>01087   }
<a name="l01088"></a>01088 
<a name="l01089"></a>01089 
<a name="l01091"></a>01091 
<a name="l01094"></a>01094   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01095"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_packed_00_01_allocator_01_4.php#a9079c6a12adc5c1b0ef054bc03bdf5b6">01095</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01096"></a>01096   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_packed_00_01_allocator_01_4.php" title="Row-major hermitian packed matrix class.">Matrix&lt;T, Prop, RowHermPacked, Allocator&gt;</a>&amp;
<a name="l01097"></a>01097   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowHermPacked, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01098"></a>01098   {
<a name="l01099"></a>01099     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetDataSize();i++)
<a name="l01100"></a>01100       this-&gt;data_[i] *= x;
<a name="l01101"></a>01101 
<a name="l01102"></a>01102     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01103"></a>01103   }
<a name="l01104"></a>01104 
<a name="l01105"></a>01105 
<a name="l01107"></a>01107 
<a name="l01114"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_packed_00_01_allocator_01_4.php#aa4e1fe59977fb95839539692ff8ed832">01114</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01115"></a>01115   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowHermPacked, Allocator&gt;</a>
<a name="l01116"></a>01116 <a class="code" href="class_seldon_1_1_matrix.php">  ::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01117"></a>01117   {
<a name="l01118"></a>01118     <span class="comment">// Storing the old values of the matrix.</span>
<a name="l01119"></a>01119     <span class="keywordtype">int</span> nold = this-&gt;GetDataSize(), iold = this-&gt;m_;
<a name="l01120"></a>01120     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> xold(nold);
<a name="l01121"></a>01121     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; nold; k++)
<a name="l01122"></a>01122       xold(k) = this-&gt;data_[k];
<a name="l01123"></a>01123 
<a name="l01124"></a>01124     <span class="comment">// Reallocation.</span>
<a name="l01125"></a>01125     this-&gt;Reallocate(i, j);
<a name="l01126"></a>01126 
<a name="l01127"></a>01127     <span class="comment">// Filling the matrix with its old values.</span>
<a name="l01128"></a>01128     <span class="keywordtype">int</span> imin = min(iold, i);
<a name="l01129"></a>01129     nold = 0;
<a name="l01130"></a>01130     <span class="keywordtype">int</span> n = 0;
<a name="l01131"></a>01131     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; imin; k++)
<a name="l01132"></a>01132       {
<a name="l01133"></a>01133         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> l = k; l &lt; imin; l++)
<a name="l01134"></a>01134           this-&gt;data_[n+l-k] = xold(nold+l-k);
<a name="l01135"></a>01135 
<a name="l01136"></a>01136         n += i - k;
<a name="l01137"></a>01137         nold += iold - k;
<a name="l01138"></a>01138       }
<a name="l01139"></a>01139   }
<a name="l01140"></a>01140 
<a name="l01141"></a>01141 
<a name="l01142"></a>01142 } <span class="comment">// namespace Seldon.</span>
<a name="l01143"></a>01143 
<a name="l01144"></a>01144 <span class="preprocessor">#define SELDON_FILE_MATRIX_HERMPACKED_CXX</span>
<a name="l01145"></a>01145 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
