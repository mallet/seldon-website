<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php">Matrix&lt; T, Prop, RowMajor, Allocator &gt;</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a> &#124;
<a href="#pro-static-attribs">Static Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;" --><!-- doxytag: inherits="Matrix_Pointers&lt; T, Prop, RowMajor, Allocator &gt;" -->
<p>Row-major full-matrix class.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.png" usemap="#Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;_map" alt=""/>
  <map id="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;_map" name="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;_map">
<area href="class_seldon_1_1_matrix___pointers.php" alt="Seldon::Matrix_Pointers&lt; T, Prop, RowMajor, Allocator &gt;" shape="rect" coords="0,56,338,80"/>
<area href="class_seldon_1_1_matrix___base.php" alt="Seldon::Matrix_Base&lt; T, Allocator &gt;" shape="rect" coords="0,0,338,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aeb1fca69326b063f1b9c7488a57d9480"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::value_type" ref="aeb1fca69326b063f1b9c7488a57d9480" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0944da60659c531bea528d88027fe7ac"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::property" ref="a0944da60659c531bea528d88027fe7ac" args="" -->
typedef Prop&nbsp;</td><td class="memItemRight" valign="bottom"><b>property</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afaa5c27081bd3aad17688060f7b170c5"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::storage" ref="afaa5c27081bd3aad17688060f7b170c5" args="" -->
typedef <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>&nbsp;</td><td class="memItemRight" valign="bottom"><b>storage</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa116526a215ef7c566b332080ed7e67c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::allocator" ref="aa116526a215ef7c566b332080ed7e67c" args="" -->
typedef Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>allocator</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a91492e35ee7cd936e2b9db024c3a994d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::pointer" ref="a91492e35ee7cd936e2b9db024c3a994d" args="" -->
typedef Allocator::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a90820ccdb63acabd4f2e0bcdf1b81bff"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::const_pointer" ref="a90820ccdb63acabd4f2e0bcdf1b81bff" args="" -->
typedef Allocator::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afd9fad8a481da28a1e0f8d679206fe03"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::reference" ref="afd9fad8a481da28a1e0f8d679206fe03" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7d8aac45bb068ca6560103564b3d9aa1"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::const_reference" ref="a7d8aac45bb068ca6560103564b3d9aa1" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1f247aba29d840e5cceed5561c12be2d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::entry_type" ref="a1f247aba29d840e5cceed5561c12be2d" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>entry_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5736f8e9940b1a0b770ee53eab406fb0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::access_type" ref="a5736f8e9940b1a0b770ee53eab406fb0" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>access_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3c5dec5d3b1695386ab9e1a255dd3238"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::const_access_type" ref="a3c5dec5d3b1695386ab9e1a255dd3238" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_access_type</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#ad6540bf8d5f0d58e12915162e1d6b852">Matrix</a> ()  throw ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#ad6540bf8d5f0d58e12915162e1d6b852"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#a2473d447fd3e4efac53e674b94841b98">Matrix</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Main constructor.  <a href="#a2473d447fd3e4efac53e674b94841b98"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4951a370ce9457e8625cc5257a349f96"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::Matrix" ref="a4951a370ce9457e8625cc5257a349f96" args="(const Matrix&lt; T, Prop, RowMajor, Allocator &gt; &amp;A)" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#a4951a370ce9457e8625cc5257a349f96">Matrix</a> (const <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copy constructor. <br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, <br class="typebreak"/>
Allocator &gt; &amp;&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#a3727350f7283b6e8e08ccb2f6733362b">operator=</a> (const T0 &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the matrix with a given value.  <a href="#a3727350f7283b6e8e08ccb2f6733362b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, <br class="typebreak"/>
Allocator &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#a11d3b562dc6263cad3299de5cbff269a">operator=</a> (const <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a matrix (assignment operator).  <a href="#a11d3b562dc6263cad3299de5cbff269a"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, <br class="typebreak"/>
Allocator &gt; &amp;&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#a1f8c93e3abfe109f258ab5b31e6382e4">operator*=</a> (const T0 &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Multiplies the matrix by a scalar.  <a href="#a1f8c93e3abfe109f258ab5b31e6382e4"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a361729faf34aaedf14ad0b3f044e093c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::Clear" ref="a361729faf34aaedf14ad0b3f044e093c" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Clear</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a36ac8e7e9bee92ae266201dfb60bf32e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::GetDataSize" ref="a36ac8e7e9bee92ae266201dfb60bf32e" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataSize</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a782143339736b347e5a1c7ecd4d0c793"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::Reallocate" ref="a782143339736b347e5a1c7ecd4d0c793" args="(int i, int j)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Reallocate</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4553ae986c158b88f6677d6c588eaabc"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::SetData" ref="a4553ae986c158b88f6677d6c588eaabc" args="(int i, int j, pointer data)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetData</b> (int i, int j, pointer data)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2898e791b8d8e9b282bc36a59da5a0ec"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::Nullify" ref="a2898e791b8d8e9b282bc36a59da5a0ec" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Nullify</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae5cee7b46956ee19dbbb3308613378ec"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::Resize" ref="ae5cee7b46956ee19dbbb3308613378ec" args="(int i, int j)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Resize</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a859b5a76ca8ff32246c610145151ebac"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::operator()" ref="a859b5a76ca8ff32246c610145151ebac" args="(int i, int j)" -->
reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>operator()</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1152a779c807df6022715b8635be7bf7"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::operator()" ref="a1152a779c807df6022715b8635be7bf7" args="(int i, int j) const" -->
const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>operator()</b> (int i, int j) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afca09e0690510a4a40fe022e62677b6e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::Val" ref="afca09e0690510a4a40fe022e62677b6e" args="(int i, int j)" -->
reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>Val</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3d83977c9b496a6c1745a8e41f79af5c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::Val" ref="a3d83977c9b496a6c1745a8e41f79af5c" args="(int i, int j) const" -->
const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>Val</b> (int i, int j) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac3be1736d3b8f78244483b879b72b255"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::operator[]" ref="ac3be1736d3b8f78244483b879b72b255" args="(int i)" -->
reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>operator[]</b> (int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af98cb1a88c5cd8675ce73ccff9f37db9"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::operator[]" ref="af98cb1a88c5cd8675ce73ccff9f37db9" args="(int i) const" -->
const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>operator[]</b> (int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a043929510eae4908750348b4862e190c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::Copy" ref="a043929510eae4908750348b4862e190c" args="(const Matrix_Pointers&lt; T, Prop, RowMajor, Allocator &gt; &amp;A)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Copy</b> (const <a class="el" href="class_seldon_1_1_matrix___pointers.php">Matrix_Pointers</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, Allocator &gt; &amp;A)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af93c5e73fc737a2f6e4f0b15164dd46d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::GetLD" ref="af93c5e73fc737a2f6e4f0b15164dd46d" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetLD</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="add248e51552e9cec9a6214a8c1328ed0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::Zero" ref="add248e51552e9cec9a6214a8c1328ed0" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Zero</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a35fc7bad6fe6cf252737916dfafae0ff"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::SetIdentity" ref="a35fc7bad6fe6cf252737916dfafae0ff" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetIdentity</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2d3aeda0fa734518dd700f8568aaccde"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::Fill" ref="a2d3aeda0fa734518dd700f8568aaccde" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Fill</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aff7bbc1161aeb1ffcf7894e4b21a270f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::Fill" ref="aff7bbc1161aeb1ffcf7894e4b21a270f" args="(const T0 &amp;x)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Fill</b> (const T0 &amp;x)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a656390ed0505de78a077a6533586428e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::FillRand" ref="a656390ed0505de78a077a6533586428e" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>FillRand</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a648346709f078652cb0a76f4001c901e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::Print" ref="a648346709f078652cb0a76f4001c901e" args="() const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Print</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac26e9b3c26a513df732755cec5821a6d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::Print" ref="ac26e9b3c26a513df732755cec5821a6d" args="(int a, int b, int m, int n) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Print</b> (int a, int b, int m, int n) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab905f1761772bdd8607e212860d3b25d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::Print" ref="ab905f1761772bdd8607e212860d3b25d" args="(int l) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Print</b> (int l) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a05252b9ce76816127365574721a97e44"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::Write" ref="a05252b9ce76816127365574721a97e44" args="(string FileName, bool with_size=true) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Write</b> (string FileName, bool with_size=true) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a150457071742a4d3a4bfd503a0e10c29"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::Write" ref="a150457071742a4d3a4bfd503a0e10c29" args="(ostream &amp;FileStream, bool with_size=true) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Write</b> (ostream &amp;FileStream, bool with_size=true) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa1b41e8fe89480269acdc9b8cd36c21e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::WriteText" ref="aa1b41e8fe89480269acdc9b8cd36c21e" args="(string FileName) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>WriteText</b> (string FileName) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af1aa0dd65f43e9ab7fc47dddc38d2cd6"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::WriteText" ref="af1aa0dd65f43e9ab7fc47dddc38d2cd6" args="(ostream &amp;FileStream) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>WriteText</b> (ostream &amp;FileStream) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a45b2cbff13b8da2a247def9e87e910a6"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::Read" ref="a45b2cbff13b8da2a247def9e87e910a6" args="(string FileName, bool with_size=true)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Read</b> (string FileName, bool with_size=true)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a697168859e965ebfe3e0c722544217de"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::Read" ref="a697168859e965ebfe3e0c722544217de" args="(istream &amp;FileStream, bool with_size=true)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Read</b> (istream &amp;FileStream, bool with_size=true)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2ec5770914f507080dac5452b8ddaee5"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::ReadText" ref="a2ec5770914f507080dac5452b8ddaee5" args="(string FileName)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>ReadText</b> (string FileName)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a96dc6a908187241ae7fea78ed2f12e93"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::ReadText" ref="a96dc6a908187241ae7fea78ed2f12e93" args="(istream &amp;FileStream)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>ReadText</b> (istream &amp;FileStream)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">GetM</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows.  <a href="#a65e9c3f0db9c7c8c3fa10ead777dd927"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows of the matrix possibly transposed.  <a href="#ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">GetN</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns.  <a href="#a6b134070d1b890ba6b7eb72fee170984"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns of the matrix possibly transposed.  <a href="#a7d27412bc9384a5ce0c0db1ee310d31c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">GetSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements in the matrix.  <a href="#a0fbc3f7030583174eaa69429f655a1b0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer to the data array.  <a href="#a453a269dfe7fadba249064363d5ab92a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a const pointer to the data array.  <a href="#a0f2796430deb08e565df8ced656097bf"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "void*" to the data array.  <a href="#a505cdfee34741c463e0dc1943337bedc"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "const void*" to the data array.  <a href="#a5979450cd8801810229f4a24c36e6639"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">Allocator &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the allocator of the matrix.  <a href="#af88748a55208349367d6860ad76fd691"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad3b518bdaa349ab872ad6610e11785ce"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::me_" ref="ad3b518bdaa349ab872ad6610e11785ce" args="" -->
pointer *&nbsp;</td><td class="memItemRight" valign="bottom"><b>me_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a96b39ff494d4b7d3e30f320a1c7b4aba"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::m_" ref="a96b39ff494d4b7d3e30f320a1c7b4aba" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>m_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a11cb5d502050e5f14482ff0c9cef5a76"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::n_" ref="a11cb5d502050e5f14482ff0c9cef5a76" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>n_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acb12a8b74699fae7ae3b2b67376795a1"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::data_" ref="acb12a8b74699fae7ae3b2b67376795a1" args="" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-static-attribs"></a>
Static Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a132fa01ce120f352d434fd920e5ad9b4"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::allocator_" ref="a132fa01ce120f352d434fd920e5ad9b4" args="" -->
static Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>allocator_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Prop, class Allocator&gt;<br/>
 class Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;</h3>

<p>Row-major full-matrix class. </p>

<p>Definition at line <a class="el" href="_matrix___pointers_8hxx_source.php#l00147">147</a> of file <a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="ad6540bf8d5f0d58e12915162e1d6b852"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::Matrix" ref="ad6540bf8d5f0d58e12915162e1d6b852" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td>  throw ()</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>Builds a empty matrix. </p>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l01205">1205</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2473d447fd3e4efac53e674b94841b98"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::Matrix" ref="a2473d447fd3e4efac53e674b94841b98" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Main constructor. </p>
<p>Builds a i by j full row-major matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l01217">1217</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="af88748a55208349367d6860ad76fd691"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::GetAllocator" ref="af88748a55208349367d6860ad76fd691" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">Allocator &amp; <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetAllocator </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the allocator of the matrix. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The allocator. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00257">257</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a453a269dfe7fadba249064363d5ab92a"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::GetData" ref="a453a269dfe7fadba249064363d5ab92a" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer to the data array. </p>
<p>Returns a pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00207">207</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0f2796430deb08e565df8ced656097bf"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::GetDataConst" ref="a0f2796430deb08e565df8ced656097bf" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::const_pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConst </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a const pointer to the data array. </p>
<p>Returns a const pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00220">220</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5979450cd8801810229f4a24c36e6639"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::GetDataConstVoid" ref="a5979450cd8801810229f4a24c36e6639" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConstVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "const void*" to the data array. </p>
<p>Returns a pointer of type "const void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00246">246</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a505cdfee34741c463e0dc1943337bedc"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::GetDataVoid" ref="a505cdfee34741c463e0dc1943337bedc" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "void*" to the data array. </p>
<p>Returns a pointer of type "void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00233">233</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a65e9c3f0db9c7c8c3fa10ead777dd927"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::GetM" ref="a65e9c3f0db9c7c8c3fa10ead777dd927" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a8c781e99310aae01786eac0e8e5aa50c">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00106">106</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::GetM" ref="ab6f6c5bba065ca82dad7c3abdbc8ea79" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#aecf3e8c636dbb83335ea588afe2558a8">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00129">129</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7d27412bc9384a5ce0c0db1ee310d31c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::GetN" ref="a7d27412bc9384a5ce0c0db1ee310d31c" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a867066f7bf531bf7ad15bdcd034dc3c0">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00144">144</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6b134070d1b890ba6b7eb72fee170984"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::GetN" ref="a6b134070d1b890ba6b7eb72fee170984" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a40580a92202044416e379c1304ff0220">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00117">117</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0fbc3f7030583174eaa69429f655a1b0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::GetSize" ref="a0fbc3f7030583174eaa69429f655a1b0" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements in the matrix. </p>
<p>Returns the number of elements in the matrix, i.e. the number of rows multiplied by the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements in the matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00194">194</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1f8c93e3abfe109f258ab5b31e6382e4"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::operator*=" ref="a1f8c93e3abfe109f258ab5b31e6382e4" args="(const T0 &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, Allocator &gt;::operator*= </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>alpha</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Multiplies the matrix by a scalar. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>alpha</em>&nbsp;</td><td>scalar. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l01275">1275</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a11d3b562dc6263cad3299de5cbff269a"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::operator=" ref="a11d3b562dc6263cad3299de5cbff269a" args="(const Matrix&lt; T, Prop, RowMajor, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a matrix (assignment operator). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>A</em>&nbsp;</td><td>matrix to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: <em>A</em> is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l01261">1261</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3727350f7283b6e8e08ccb2f6733362b"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;::operator=" ref="a3727350f7283b6e8e08ccb2f6733362b" args="(const T0 &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_major.php">RowMajor</a>, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the matrix with a given value. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>the value to fill the matrix with. </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___pointers.php#ac18163af8e9002a336f71ad38fd69230">Seldon::Matrix_Pointers&lt; T, Prop, RowMajor, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___pointers_8cxx_source.php#l01244">1244</a> of file <a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>matrix/<a class="el" href="_matrix___pointers_8hxx_source.php">Matrix_Pointers.hxx</a></li>
<li>matrix/<a class="el" href="_matrix___pointers_8cxx_source.php">Matrix_Pointers.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
