<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Sparse Matrices </h1>  </div>
</div>
<div class="contents">
<p>First, methods and functions related to sparse matrices stored as a vector of sparse rows are detailed. This type of matrix is interesting because it allows fast insertion of elements in the matrix. Then the matrices using Harwell-Boeing format are detailed. Functions related to sparse matrices are explained. We advise the user to use always storage of matrix by rows, since this last storage is more efficient for the matrix-vector product. </p>
<h2>Basic declaration of easily modifiable sparse matrices:</h2>
<p><em>These matrices are available only after <code><a class="el" href="_seldon_solver_8hxx_source.php">SeldonSolver.hxx</a></code> has been included.</em></p>
<h4>Classes :</h4>
<p>Matrix&lt;T, Prop, ArrayRowSparse&gt; <br/>
 Matrix&lt;T, Prop, ArrayColSparse&gt; <br/>
 Matrix&lt;T, Prop, ArrayRowSymSparse&gt; <br/>
 Matrix&lt;T, Prop, ArrayColSymSparse&gt;</p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// sparse matrix of doubles
Matrix&lt;double, General, ArrayRowSparse&gt; A;

// sparse symmetric matrix
Matrix&lt;float, Symmetric, ArrayRowSymSparse&gt; B;
</pre></div>  </pre><h2>Methods for easily modifiable sparse matrices :</h2>
<table  class="category-table">
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#constructor">Matrix constructors </a>   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#operator">Matrix operators </a>   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getm">GetM </a>  </td><td class="category-table-td">returns the number of rows in the matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getn">GetN </a>  </td><td class="category-table-td">returns the number of columns in the matrix   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getdatasize">GetDataSize </a>  </td><td class="category-table-td">returns the number of elements effectively stored   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getdatasize">GetNonZeros </a>  </td><td class="category-table-td">returns the number of elements effectively stored   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getdata">GetData </a>  </td><td class="category-table-td">returns a pointer to the array containing the values   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getindex">GetIndex </a>  </td><td class="category-table-td">returns a pointer to the array containing column numbers   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#setdata">SetData </a>  </td><td class="category-table-td">sets the pointer to the arrays containing values and column numbers   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#nullify">Nullify </a>  </td><td class="category-table-td">removes elements of the matrix without releasing memory  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#clear">Clear </a>  </td><td class="category-table-td">removes all elements of the matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#reallocate">Reallocate </a>  </td><td class="category-table-td">changes the size of matrix (does not keep previous elements)   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#resize">Resize </a>  </td><td class="category-table-td">changes the size of matrix (keeps previous elements)   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#value">Value </a>  </td><td class="category-table-td">direct access to a value  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#index">Index </a>  </td><td class="category-table-td">direct access to a column number  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#zero">Zero </a>  </td><td class="category-table-td">sets all elements to zero   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#setidentity">SetIdentity </a>  </td><td class="category-table-td">sets matrix to identity matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#fill">Fill </a>  </td><td class="category-table-td">sets all elements to a given value   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#fillrand">FillRand </a>  </td><td class="category-table-td">fills randomly the matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#reallocaterow">ReallocateRow / ReallocateColumn </a>  </td><td class="category-table-td">changes the size of a row   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#resizerow">ResizeRow / ResizeColumn </a>  </td><td class="category-table-td">changes the size of a row and keeps previous values  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#clearrow">ClearRow / ClearColumn </a>  </td><td class="category-table-td">clears a row   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#swaprow">SwapRow / SwapColumn </a>  </td><td class="category-table-td">exchanges two rows   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#replaceindexrow">ReplaceIndexRow / ReplaceIndexColumn </a>  </td><td class="category-table-td">changes column numbers   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getrowsize">GetRowSize / GetColumnSize </a>  </td><td class="category-table-td">returns the number of elements in the row   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#printrow">PrintRow / PrintColumn </a>  </td><td class="category-table-td">prints a row   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#assemblerow">AssembleRow / AssembleColumn </a>  </td><td class="category-table-td">assembles a row   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#addinteraction">AddInteraction </a>  </td><td class="category-table-td">adds/inserts an element in the matrix   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#addinteractionrow">AddInteractionRow </a>  </td><td class="category-table-td">adds/inserts an element in a matrix row   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#addinteractioncolumn">AddInteractionColumn </a>  </td><td class="category-table-td">adds/inserts elements in a matrix column   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#print">Print </a>  </td><td class="category-table-td">displays the matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#write">Write </a>  </td><td class="category-table-td">writes the vector in binary format   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#read">Read </a>  </td><td class="category-table-td">reads the vector in binary format   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#writetext">WriteText </a>  </td><td class="category-table-td">writes the vector in text format   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#readtext">ReadText </a>  </td><td class="category-table-td">reads the vector in text format   </td></tr>
</table>
<p>Another class of storage of sparse matrices is available, the so-called Harwell-Boeing format. The values are stored in a single array, preventing from a fast insertion of elements. Nevertheless, this type of storage requires slightly less memory and the associated matrix-vector product is more efficient. </p>
<h2>Basic declaration of Harwell-Boeing sparse matrices:</h2>
<h4>Classes :</h4>
<p>Matrix&lt;T, Prop, RowSparse&gt; <br/>
 Matrix&lt;T, Prop, ColSparse&gt; <br/>
 Matrix&lt;T, Prop, RowSymSparse&gt; <br/>
 Matrix&lt;T, Prop, ColSymSparse&gt;</p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// sparse matrix of doubles
Matrix&lt;double, General, RowSparse&gt; A;

// sparse symmetric matrix
Matrix&lt;float, Symmetric, RowSymSparse&gt; B;
</pre></div>  </pre><h2>Methods for Harwell-Boeing sparse matrices :</h2>
<table  class="category-table">
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#csr_constructor">Matrix constructors </a>   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#csr_operator">Matrix operators </a>   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getm">GetM </a>  </td><td class="category-table-td">returns the number of rows in the matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getn">GetN </a>  </td><td class="category-table-td">returns the number of columns in the matrix   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getdatasize">GetDataSize </a>  </td><td class="category-table-td">returns the number of elements effectively stored   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getdatasize">GetNonZeros </a>  </td><td class="category-table-td">returns the number of elements effectively stored   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#csr_getdata">GetData </a>  </td><td class="category-table-td">returns a pointer to the array containing the values   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#csr_getind">GetInd </a>  </td><td class="category-table-td">returns a pointer to the array containing column numbers   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#csr_getptr">GetPtr </a>  </td><td class="category-table-td">returns a pointer to the array containing row numbers   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#csr_getptr">GetPtrSize </a>  </td><td class="category-table-td">returns size of array Ptr   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#csr_getind">GetIndSize </a>  </td><td class="category-table-td">returns size of array Ind   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#clear">Clear </a>  </td><td class="category-table-td">removes all elements of the matrix   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#csr_setdata">SetData </a>  </td><td class="category-table-td">sets the pointer to the array containing the values  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#csr_nullify">Nullify </a>  </td><td class="category-table-td">clears the matrix without releasing memory  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#print">Print </a>  </td><td class="category-table-td">displays the matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#write">Write </a>  </td><td class="category-table-td">writes the vector in binary format   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#read">Read </a>  </td><td class="category-table-td">reads the vector in binary format   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#writetext">WriteText </a>  </td><td class="category-table-td">writes the vector in text format   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#readtext">ReadText </a>  </td><td class="category-table-td">reads the vector in text format   </td></tr>
</table>
<h2>Functions :</h2>
<table  class="category-table">
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_blas.php#mlt">Mlt </a> </td><td class="category-table-td">multiplication by a scalar or matrix-vector product   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_blas.php#mltadd">MltAdd </a> </td><td class="category-table-td">performs a matrix-vector product  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_blas.php#add">Add </a> </td><td class="category-table-td">adds two matrices   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_blas.php#copy">Copy </a> </td><td class="category-table-td">copies/converts one matrix into another one   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_matrix.php#permutematrix">PermuteMatrix </a> </td><td class="category-table-td">applies permutation to matrix  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_matrix.php#scalematrix">ScaleMatrix </a> </td><td class="category-table-td">applies a scaling both for columns and rows  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_matrix.php#scaleleftmatrix">ScaleLeftMatrix </a> </td><td class="category-table-td">applies a left scaling (on rows)   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_matrix.php#getrow">GetRow </a> </td><td class="category-table-td">returns a matrix row   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_matrix.php#setrow">SetRow </a> </td><td class="category-table-td">changes a matrix row   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_matrix.php#getcol">GetCol </a> </td><td class="category-table-td">returns a matrix column   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_matrix.php#setcol">SetCol </a> </td><td class="category-table-td">changes a matrix column   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="functions_lapack.php#getlu">GetLU</a> </td><td class="category-table-td">performs a LU (or LDL^t) factorization  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="functions_lapack.php#getlu">SolveLU</a> </td><td class="category-table-td">solves linear system by using LU factorization  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="iterative.php#sor">SOR</a> </td><td class="category-table-td">applies successive over-relaxations to matrix  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="iterative.php#solvers">Cg, Gmres, BiCgSTAB, etc</a> </td><td class="category-table-td">solves iteratively a linear system  </td></tr>
</table>
<div class="separator"><a class="anchor" id="constructor"></a></div><h3>Matrix constructors for ArrayRowSparse, ArrayRowSymSparse</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  Matrix();
  Matrix(int m, int n);
</pre><h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// default constructor -&gt; empty matrix
Matrix&lt;double, General, ArrayRowSparse&gt; V;
cout &lt;&lt; "Number of elements "&lt;&lt; V.GetDataSize() &lt;&lt; endl; // should return 0 
// then you can use Reallocate to set the number of rows and columns
V.Reallocate(3, 2);
// the last command doesn't create any non-zero element
// you can create one with AddInteraction for example
V.AddInteraction(0, 1, 1.5);

// we construct symmetric matrix with 4 rows
Matrix&lt;double, Symmetric, ArrayRowSymSparse&gt; W(4, 4);
</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="#reallocate">Reallocate</a><br/>
 <a href="#addinteraction">AddInteraction</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse <br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="csr_constructor"></a></div><h3>Matrix constructors for RowSparse, RowSymSparse</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  Matrix();
  Matrix(int m, int n);
  Matrix(int m, int n, int nnz);
  Matrix(m, n, values, ptr, ind);
</pre><h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// default constructor -&gt; empty matrix
Matrix&lt;double, General, RowSparse&gt; V;
cout &lt;&lt; "Number of elements "&lt;&lt; V.GetDataSize() &lt;&lt; endl; // should return 0 

// the two other constructors are useless
Matrix&lt;double, Symmetric, ArrayRowSymSparse&gt; W(4, 4);
Matrix&lt;double, General, RowSparse&gt; A(3, 4, 10);

// in the last constructor you provide
int m = 5; // number of rows
int n = 6; // number of columns
int nnz = 12; // number of non-zero entries
Vector&lt;double&gt; values(nnz); // values
values.FillRand(); // you have to initialize values
Vector&lt;int&gt; columns(nnz); // for each value column number
// for each row, column numbers are sorted
// first row
columns(0) = 0;
columns(1) = 2;
columns(2) = 4;
// second row
columns(3) = 1;
// third row
columns(4) = 0;
columns(5) = 1;
columns(6) = 3;
// fourth row
columns(7) = 4;
columns(8) = 5;
// last row
columns(9) = 2;
columns(10) = 3;
columns(11) = 5;
// for each row, index of first value
Vector&lt;int&gt; ptr(m+1); 
ptr(0) = 0;
ptr(1) = 3;
ptr(2) = 4;
ptr(3) = 7;
ptr(4) = 9;
ptr(5) = 12;
// values on the row are to be seeked between ptr(i) and ptr(i+1)

// Now the constructor:
Matrix&lt;double, General, RowSparse&gt; M(m, n, values, ptr, columns);
// Note that 'values', 'ptr' and 'columns' are nullified (that is, emptied):
// the arrays to which they pointed have been transferred to M.
</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="#reallocate">Reallocate</a><br/>
 <a href="#csr_setdata">SetData</a></p>
<h4>Location :</h4>
<p>Class Matrix_Sparse<br/>
 Class Matrix_SymSparse<br/>
 <a class="el" href="_matrix___sparse_8hxx_source.php">Matrix_Sparse.hxx</a> <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a><br/>
 <a class="el" href="_matrix___sym_sparse_8hxx_source.php">Matrix_SymSparse.hxx</a> <a class="el" href="_matrix___sym_sparse_8cxx_source.php">Matrix_SymSparse.cxx</a></p>
<div class="separator"><a class="anchor" id="operator"></a></div><h3>Matrix operators for ArrayRowSparse, ArrayRowSymSparse</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  const T&amp; operator (int i, int j) const;
  T&amp; operator (int i, int j);
  Matrix&amp; operator =(const T0&amp; alpha)
</pre><p>The operator () can be used to insert or modify elements of matrix.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; V(3, 3);
// use of operator () to modify matrix
V(0, 0) = 2.0;
V(1, 0) = V(0, 0) + 1.0;

// set all elements to a given value
V = 1;
</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="#addinteraction">AddInteraction</a><br/>
 <a href="#fill">Fill</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="csr_operator"></a></div><h3>Matrix operators for RowSparse, RowSymSparse</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  T operator (int i, int j) const;
</pre><p>The operator () cannot be used to insert or modify elements of matrix.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, RowSparse&gt; V(3, 3);
// use of operator () to read elements of matrix
cout &lt;&lt; "V(0,0) = " &lt;&lt; V(0, 0) &lt;&lt; endl;
</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="#setdata">SetData</a><br/>
</p>
<h4>Location :</h4>
<p>Class Matrix_Sparse<br/>
 Class Matrix_SymSparse<br/>
 <a class="el" href="_matrix___sparse_8hxx_source.php">Matrix_Sparse.hxx</a> <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a><br/>
 <a class="el" href="_matrix___sym_sparse_8hxx_source.php">Matrix_SymSparse.hxx</a> <a class="el" href="_matrix___sym_sparse_8cxx_source.php">Matrix_SymSparse.cxx</a></p>
<div class="separator"><a class="anchor" id="getm"></a></div><h3>GetM</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int GetM() const;
</pre><p>This method returns the number of rows.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;float, General, ArrayRowSparse&gt; V(3, 2);
// V.GetM() should return 3 
cout &lt;&lt; "Number of rows of V " &lt;&lt; V.GetM() &lt;&lt; endl;
</pre></div>  </pre><h4>Location :</h4>
<p>Class Matrix_Base<br/>
 Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___base_8hxx_source.php">Matrix_Base.hxx</a> <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a><br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="getn"></a></div><h3>GetN</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int GetN() const;
</pre><p>This method returns the number of columns</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;float, Symmetric, ArrayRowSymSparse&gt; V(3, 2);
// V.GetN() should return 2 
cout &lt;&lt; "Number of rows of V " &lt;&lt; V.GetN() &lt;&lt; endl;
</pre></div>  </pre><h4>Location :</h4>
<p>Class Matrix_Base<br/>
 Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___base_8hxx_source.php">Matrix_Base.hxx</a> <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a><br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="getdatasize"></a></div><h3>GetDataSize, GetNonZeros</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int GetDataSize() const;
  int GetNonZeros() const;
</pre><p>These methods returns the number of elements effectively stored in the matrix.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;float, Symmetric, ArrayRowSymSparse&gt; V(3, 3);
V.AddInteraction(0, 1, 1.0);
// V.GetDataSize() should return 1
cout &lt;&lt; "Number of elements of V " &lt;&lt; V.GetSize() &lt;&lt; endl;
</pre></div>  </pre><h4>Location :</h4>
<p>Class Matrix_Sparse<br/>
 Class Matrix_SymSparse<br/>
 Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___sparse_8hxx_source.php">Matrix_Sparse.hxx</a> <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a><br/>
 <a class="el" href="_matrix___sym_sparse_8hxx_source.php">Matrix_SymSparse.hxx</a> <a class="el" href="_matrix___sym_sparse_8cxx_source.php">Matrix_SymSparse.cxx</a><br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="getdata"></a></div><h3>GetData for ArrayRowSparse, ArrayRowSymSparse</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  T* GetData(int i) const;
  Vector&lt;T, Vect_Sparse, Allocator&gt;* GetData() const;
 </pre><p>These methods are useful to retrieve the pointer to the values, but they need to be used with caution.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; V(3, 4); V.Fill();
// values of second row are retrieved
int irow = 1;
double* data = V.GetData(irow);
// you can use data as a normal C array
// here the sum of elements of the row is computed
double sum = 0;
for (int i = 0; i &lt; V.GetRowSize(irow); i++)
  sum += data[i];

// you can also retrieve all the rows
Vector&lt;double, Vect_Sparse&gt;* all_rows = V.GetData();

</pre></div>  </pre><h4>Location :</h4>
<p>Class Matrix_ArraySparse <br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="clear"></a></div><h3>Clear</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Clear();
</pre><p>This method removes all the elements of the matrix.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; A(3, 3);
A.AddInteraction(0, 2, 2.8);
// clears matrix A
A.Clear();
</pre></div>  </pre><h4>Location :</h4>
<p>Class Matrix_Sparse<br/>
 Class Matrix_SymSparse<br/>
 Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___sparse_8hxx_source.php">Matrix_Sparse.hxx</a> <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a><br/>
 <a class="el" href="_matrix___sym_sparse_8hxx_source.php">Matrix_SymSparse.hxx</a> <a class="el" href="_matrix___sym_sparse_8cxx_source.php">Matrix_SymSparse.cxx</a><br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="reallocate"></a></div><h3>Reallocate</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Reallocate(int, int);
</pre><p>This method changes the size of the matrix, but removes previous elements.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; A(5, 4);
A.AddInteraction(0, 2, 2.8);
// resizes matrix A
A.Reallocate(4, 3);
// previous elements are removed : A is empty
cout &lt;&lt; "number of non-zero entries " &lt;&lt; A.GetDataSize() &lt;&lt; endl;
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#resize">Resize</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="resize"></a></div><h3>Resize</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Resize(int, int);
</pre><p>This method changes the size of the matrix, and keeps previous elements.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, Symmetric, ArrayRowSymSparse&gt; A(4,4);
A(1,3) = 2.0;
A(0,2) = -1.0;
// resizes matrix A and keeps previous added interactions
A.Resize(5,5);
// GetDataSize() should return 2
cout &lt;&lt; "number of non-zero entries of A " &lt;&lt; A.GetDataSize() &lt;&lt; endl;
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#reallocate">Reallocate</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="setdata"></a></div><h3>SetData for ArrayRowSparse, ArrayRowSymSparse</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void SetData(int, int, T*, int*);
  void SetData(int, int, Vector&lt;T, Vect_Sparse&gt;*);
</pre><p>This method changes a row of the matrix or all the rows by directly changing pointers. This is a low-level method and should be used very cautiously. It is better to use other methods to modify the matrix.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// first use, you construct all the rows
int m = 10, n = 9;
Vector&lt;Vector&lt;double, Vect_Sparse&gt; &gt; rows(m);

rows(0).AddInteraction(1, 2.4);
rows(1).AddInteraction(3, -1.2);
// ...

// then you can feed a sparse matrix with rows
Matrix&lt;double, General, ArrayRowSparse&gt; A;
A.SetData(m, n, rows.GetData());
// you nullify rows to avoid segmentation fault
rows.Nullify();

// second use, you specify one row
Vector&lt;double, Vect_Sparse&gt; one_row;
one_row.AddInteraction(4, 2.3);
one_row.AddInteraction(0, -0.7);
// and you put it in the matrix
int irow = 2;
A.SetData(irow, one_row.GetM(), one_row.GetData(), one_row.GetIndex());
one_row.Nullify();
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#getdata">GetData</a><br/>
 <a href="#nullify">Nullify</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="nullify"></a></div><h3>Nullify for ArrayRowSparse, ArrayRowSymSparse</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Nullify();
  void Nullify(int);
</pre><p>This method clears the matrix without releasing memory. You can also clear one single row. This method should be used carefully, and generally in conjunction with method SetData. </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; A;
// you retrieve one row of A
int irow = 2;
Vector&lt;double, Vect_Sparse&gt; one_row;
one_row.SetData(A.GetRowSize(irow), A.GetData(irow), A.GetIndex(irow));
// you need to call Nullify to avoid segmentation fault
A.Nullify(irow);

// you can retrieve all the rows
Vector&lt;Vector&lt;double, Vect_Sparse&gt; &gt; rows;
rows.SetData(m, A.GetData());
// again Nullify is necessary
A.Nullify();
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#setdata">SetData</a><br/>
 <a href="#getdata">GetData</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="getindex"></a></div><h3>GetIndex for ArrayRowSparse, ArrayRowSymSparse</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void GetIndex(int);
</pre><p>This method returns the pointer to the array containing column numbers of the specified row. </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; A;
// you retrieve column numbers of row 1
IVect col;
int irow = 1;
col.SetData(A.GetRowSize(irow), A.GetIndex(irow));
// after use of col, don't forget to call Nullify
col.Nullify();
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#setdata">SetData</a><br/>
 <a href="#getdata">GetData</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="csr_setdata"></a></div><h3>SetData for RowSparse, RowSymSparse</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void SetData(int, int, int, T*, int*, int*);
  void SetData(int, int, Vector&lt;T&gt;, Vector&lt;int&gt;, Vector&lt;int&gt;);
</pre><p>This method initializes the matrix and is equivalent to a call to the <a href="#csr_constructor">constructor</a>.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// first you construct the matrix by using arrays ptr, values, columns
int m = 5; // number of rows
int n = 6; // number of columns
int nnz = 12; // number of non-zero entries
Vector&lt;double&gt; values(nnz); // values
values.FillRand(); // you have to initialize values
Vector&lt;int&gt; columns(nnz); // for each value column number
// for each row, column numbers are sorted
// first row
columns(0) = 0;
columns(1) = 2;
columns(2) = 4;
// second row
columns(3) = 1;
// third row
columns(4) = 0;
columns(5) = 1;
columns(6) = 3;
// fourth row
columns(7) = 4;
columns(8) = 5;
// last row
columns(9) = 2;
columns(10) = 3;
columns(11) = 5;
// for each row, index of first value
Vector&lt;int&gt; ptr(m+1); 
ptr(0) = 0;
ptr(1) = 3;
ptr(2) = 4;
ptr(3) = 7;
ptr(4) = 9;
ptr(5) = 12;

// then you call SetData
Matrix&lt;double, General, RowSparse&gt; A;
A.SetData(m, n, values, ptr, columns);
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#csr_getdata">GetData</a><br/>
 <a href="#nullify">Nullify</a></p>
<h4>Location :</h4>
<p>Class Matrix_Sparse<br/>
 Class Matrix_SymSparse<br/>
 <a class="el" href="_matrix___sparse_8hxx_source.php">Matrix_Sparse.hxx</a> <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a><br/>
 <a class="el" href="_matrix___sym_sparse_8hxx_source.php">Matrix_SymSparse.hxx</a> <a class="el" href="_matrix___sym_sparse_8cxx_source.php">Matrix_SymSparse.cxx</a></p>
<div class="separator"><a class="anchor" id="csr_nullify"></a></div><h3>Nullify</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Nullify();
</pre><p>This method clears the matrix without releasing memory. This method should be used carefully, and generally in conjunction with method SetData. You can look at the example shown in the explanation of method SetData. </p>
<h4>Related topics :</h4>
<p><a href="#csr_setdata">SetData</a><br/>
 <a href="#csr_getdata">GetData</a></p>
<h4>Location :</h4>
<p>Class Matrix_Sparse<br/>
 Class Matrix_SymSparse<br/>
 <a class="el" href="_matrix___sparse_8hxx_source.php">Matrix_Sparse.hxx</a> <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a><br/>
 <a class="el" href="_matrix___sym_sparse_8hxx_source.php">Matrix_SymSparse.hxx</a> <a class="el" href="_matrix___sym_sparse_8cxx_source.php">Matrix_SymSparse.cxx</a></p>
<div class="separator"><a class="anchor" id="csr_getdata"></a></div><h3>GetData for RowSparse, RowSymSparse</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  T* GetData() const;
 </pre><p>This method retrieves the pointer to the values.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// construction of a sparse matrix
Matrix&lt;double, General, RowSparse&gt; V(m, n, nnz, values, ptr, columns);
// values of non-zero entries are retrieved
double* data = V.GetData();
// you can use data as a normal C array
// here the sum of elements is computed
double sum = 0;
for (int i = 0; i &lt; V.GetDataSize(); i++)
  sum += data[i];

</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="#csr_setdata">SetData</a><br/>
 <a href="#nullify">Nullify</a></p>
<h4>Location :</h4>
<p>Class Matrix_Base<br/>
 <a class="el" href="_matrix___base_8hxx_source.php">Matrix_Base.hxx</a> <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a></p>
<div class="separator"><a class="anchor" id="csr_getptr"></a></div><h3>GetPtr, GetPtrSize for RowSparse, RowSymSparse</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  int* GetPtr() const;
  int GetPtrSize() const;
 </pre><p>This method retrieves the pointer to the indices of the first element of each row.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// construction of a sparse matrix (m = number of rows)
Matrix&lt;double, General, RowSparse&gt; V(m, n, nnz, values, ptr, columns);
// we get indices of the first element of each row
int* ptr = V.GetPtr();
// and also the size of this array (should be number of rows + 1)
int size = V.GetPtrSize();
// you can use data as a normal C array
// here the number of elements of each row is computed
Vector&lt;int&gt; size_row(m);
for (int i = 0; i &lt; m; i++)
  size_row(i) = ptr[i+1] - ptr[i];

</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="#csr_setdata">SetData</a><br/>
 <a href="#csr_getdata">GetData</a><br/>
 <a href="#csr_getind">GetInd</a></p>
<h4>Location :</h4>
<p>Class Matrix_Sparse<br/>
 Class Matrix_SymSparse<br/>
 <a class="el" href="_matrix___sparse_8hxx_source.php">Matrix_Sparse.hxx</a> <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a><br/>
 <a class="el" href="_matrix___sym_sparse_8hxx_source.php">Matrix_SymSparse.hxx</a> <a class="el" href="_matrix___sym_sparse_8cxx_source.php">Matrix_SymSparse.cxx</a></p>
<div class="separator"><a class="anchor" id="csr_getind"></a></div><h3>GetInd, GetIndSize for RowSparse, RowSymSparse</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  int* GetInd() const;
  int GetIndSize() const;
 </pre><p>This method retrieves the pointer to the column numbers of each row.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// construction of a sparse matrix (m = number of rows)
Matrix&lt;double, General, RowSparse&gt; V(m, n, nnz, values, ptr, columns);
// we get column numbers
int* ind = V.GetInd();
// and also the size of this array (should be nnz)
int size = V.GetIndSize();
// you can use data as a normal C array
// here the number of elements of each column is computed
Vector&lt;int&gt; size_col(n);
size_col.Zero();
for (int i = 0; i &lt; nnz; i++)
  size_col(ind[i])++;

</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="#csr_setdata">SetData</a><br/>
 <a href="#csr_getdata">GetData</a><br/>
 <a href="#csr_getptr">GetPtr</a></p>
<h4>Location :</h4>
<p>Class Matrix_Sparse<br/>
 Class Matrix_SymSparse<br/>
 <a class="el" href="_matrix___sparse_8hxx_source.php">Matrix_Sparse.hxx</a> <a class="el" href="_matrix___sparse_8cxx_source.php">Matrix_Sparse.cxx</a><br/>
 <a class="el" href="_matrix___sym_sparse_8hxx_source.php">Matrix_SymSparse.hxx</a> <a class="el" href="_matrix___sym_sparse_8cxx_source.php">Matrix_SymSparse.cxx</a></p>
<div class="separator"><a class="anchor" id="index"></a></div><h3>Index</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  int Index(int, int) const;
  int&amp; Index(int, int);
 </pre><p>This method allows the user to modify directly the column numbers. The drawback of this method is that the user has to take care that the column numbers are sorted and unique (no duplicate).</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// declaration of a sparse matrix
int m = 5;
int n = 4;
Matrix&lt;double, General, ArrayRowSparse&gt; V(m, n);

// we set the number of non-zero entries of first row
V.ReallocateRow(0, 2);
// you initialize column numbers with Index, and values with Value
V.Index(0, 0) = 1; V.Value(0, 0) = 0.5;
V.Index(0, 1) = 3; V.Value(0, 1) = -0.5;
// now matrix should be equal to [0 1 -0.5; 0 3 -0.5]
cout &lt;&lt; " V = " &lt;&lt; V &lt;&lt; endl;

// if you add a new value on the first row
V.ResizeRow(0, 3);
V.Index(0, 2) = 0; V.Value(0, 2) = 1.1;
// you better be careful because the new entry is not at the correct
position
// one way to fix that problem is to call Assemble
V.AssembleRow(0);
</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="#reallocaterow">ReallocateRow</a><br/>
 <a href="#resizerow">ResizeRow</a><br/>
 <a href="#assemblerow">AssembleRow</a><br/>
 <a href="#addinteractionrow">AddInteractionRow</a><br/>
 <a href="#value">Value</a><br/>
</p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="value"></a></div><h3>Value</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  const T&amp; Value(int, int) const;
  T&amp; Value(int, int);
 </pre><p>This method allows the user to modify directly the values.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// declaration of a sparse matrix
int m = 5;
int n = 4;
Matrix&lt;double, General, ArrayRowSparse&gt; V(m, n);

// we set the number of non-zero entries of first row
V.ReallocateRow(0, 2);
// you initialize column numbers with Index, and values with Value
V.Index(0, 0) = 1; V.Value(0, 0) = 0.5;
V.Index(0, 1) = 3; V.Value(0, 1) = -0.5;
// now matrix should be equal to [0 1 -0.5; 0 3 -0.5]
cout &lt;&lt; " V = " &lt;&lt; V &lt;&lt; endl;
</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="#reallocaterow">ReallocateRow</a><br/>
 <a href="#index">Index</a><br/>
</p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="zero"></a></div><h3>Zero</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Zero();
</pre><p>This method sets to 0 all values contained in non-zero entries. This is useful when you want to keep the pattern of the sparse matrix, but initialize values to 0. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; V(5, 3);
// creation of pattern
V.AddInteraction(0, 3, 1.0);
V.AddInteraction(2, 4, -1.0);

// values are set to 0
V.Zero();

// then you can increment values of the pattern
V(0, 3) += 1.5;
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#fill">Fill</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="setidentity"></a></div><h3>SetIdentity</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void SetIdentity();
</pre><p>This method sets all elements to 0, except on the diagonal set to 1. This forms the so-called identity matrix.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, Symmetric, ArrayRowSymSparse&gt; V(5, 5);
// initialization
V.SetIdentity();
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#fill">Fill</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="fill"></a></div><h3>Fill</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Fill();
  void Fill(const T0&amp; );
</pre><p>This method fills matrix with 0, 1, 2, etc or with a given value.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; A(5,5);
A(1,2) = 3;
A(2,4) = 5;
A(3,0) = 6;
A.Fill();
// A should contain [1 2 0.0, 2 4 1.0, 3 0 2.0]

A.Fill(2);
// A should contain [1 2 2.0, 2 4 2.0, 3 0 2.0]
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#zero">Zero</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="fillrand"></a></div><h3>FillRand</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void FillRand();
</pre><p>This method sets values of non-zero entries randomly.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; A(5, 3);
A(1,2) = 0.1;
A(2,4) = 0.1;
A.FillRand();
// A should contain 2 non-zero entries with random values
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#fill">Fill</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="reallocaterow"></a></div><h3>ReallocateRow / ReallocateColumn</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void ReallocateRow(int, int );
</pre><p>ReallocateColumn is the name used for storages ArrayColSparse and ArrayColSymSparse. This method creates n non-zero entries in specified row.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; A(5, 6);
// for second row, we create 3 non-zero entries
int irow = 1;
int n = 3;
A.ReallocateRow(irow, n);
// you need to initialize elements of the row
A.Index(irow, 0) = 0; A.Value(irow, 0) = 0.5;
A.Index(irow, 1) = 2; A.Value(irow, 1) = -0.4;
A.Index(irow, 2) = 3; A.Value(irow, 2) = 0.6;
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#index">Index</a><br/>
 <a href="#value">Value</a><br/>
 <a href="#resizerow">ResizeRow</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="resizerow"></a></div><h3>ResizeRow / ResizeColumn</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void ResizeRow(int, int );
</pre><p>ResizeColumn is the name used for storages ArrayColSparse and ArrayColSymSparse. This method creates n non-zero entries in specified row, and keeps previous elements.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; A(5, 6);
// for second row, we create 3 non-zero entries
int irow = 1;
int n = 3;
A.ReallocateRow(irow, n);
// you need to initialize elements of the row
A.Index(irow, 0) = 0; A.Value(irow, 0) = 0.5;
A.Index(irow, 1) = 2; A.Value(irow, 1) = -0.4;
A.Index(irow, 2) = 3; A.Value(irow, 2) = 0.6;

// you can change the size of the row
A.ResizeRow(irow, n+1);
// you need to initialize only the new entry
A.Index(irow, 3) = 5; A.Index(irow, 3) = 3.5;
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#index">Index</a><br/>
 <a href="#value">Value</a><br/>
 <a href="#reallocaterow">ReallocateRow</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="clearrow"></a></div><h3>ClearRow / ClearColumn</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void ClearRow(int,);
</pre><p>ClearColumn is the name used for storages ArrayColSparse and ArrayColSymSparse. This method removes non-zero entries in specified row.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; A(5, 6);
A(1, 3) = 4.0; // one non-zero entry in second row
// we clear second row
A.ClearRow(1);
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#index">Index</a><br/>
 <a href="#value">Value</a><br/>
 <a href="#resizerow">ResizeRow</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="swaprow"></a></div><h3>SwapRow / SwapColumn</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void ClearRow(int);
</pre><p>SwapColumn is the name used for storages ArrayColSparse and ArrayColSymSparse. This method swaps two rows. This method should be used very cautiously for symmetric matrices, since in that case only upper part of the matrix is stored.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; A(5, 6);
A(1, 3) = 4.0; // one non-zero entry in second row
A(2, 4) = -1.0;
// we swap third and second row
A.SwapRow(1, 2);
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="functions_matrix.php#permutematrix">PermuteMatrix</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="replaceindexrow"></a></div><h3>ReplaceIndexRow / ReplaceIndexColumn</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void ReplaceIndexRow(int, Vector&lt;int&gt;&amp;);
</pre><p>ReplaceIndexColumn is the name used for storages ArrayColSparse and ArrayColSymSparse. This method changes column numbers of the specified row.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; A(5, 6);
A(1, 3) = 4.0;
A(1, 4) = -1.0;
// we change column numbers of second row
IVect new_number(2);
new_number(0) = 0;
new_number(1) = 2;
A.ReplaceIndexRow(1, new_number);
</pre></div>  </pre><h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="getrowsize"></a></div><h3>GetRowSize / GetColumnSize</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void GetRowSize(int) const;
</pre><p>GetColumnSize is the name used for storages ArrayColSparse and ArrayColSymSparse. This method returns the number of non-zero entries in the specified row.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; A(5, 6);
A(1, 3) = 4.0; 
A(1, 4) = -1.0;
// two non-zero entries in second row
cout &lt;&lt; "Number of elements in second row " &lt;&lt; A.GetRowSize(1) &lt;&lt; endl;
</pre></div>  </pre><h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="printrow"></a></div><h3>PrintRow / PrintColumn</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void PrintRow(int) const;
</pre><p>PrintColumn is the name used for storages ArrayColSparse and ArrayColSymSparse. This method returns the number of non-zero entries in the specified row.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; A(5, 6);
A(1, 3) = 4.0; 
A(1, 4) = -1.0;
// we print second row
cout &lt;&lt; "Second row of A " &lt;&lt; A.PrintRow(1) &lt;&lt; endl;
</pre></div>  </pre><h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="assemblerow"></a></div><h3>AssembleRow / AssembleColumn</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void AssembleRow(int) const;
</pre><p>AssembleColumn is the name used for storages ArrayColSparse and ArrayColSymSparse. This method sorts and merges non-zero entries of a row. You don't need to call this method if you are using methods <a href="#addinteraction">AddInteraction</a>, <a href="#addinteractionrow">AddInteractionRow</a> or the access operator ().</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; A(5, 5);
A.ReallocateRow(1, 3);
// we initialize non-zero entries but non-sorted and with duplicates
A.Index(1, 0) = 4; A.Value(1, 0) = 1.2;
A.Index(1, 1) = 2; A.Value(1, 1) = -0.7;
A.Index(1, 2) = 4; A.Value(1, 2) = 2.5;

// we sort column numbers of row 1
A.AssembleRow(1);
// now A should be equal to [1 2 -0.7, 1 4 3.7]
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#index">Index</a><br/>
 <a href="#value">Value</a><br/>
 <a href="#addinteraction">AddInteraction</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="addinteraction"></a></div><h3>AddInteraction</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void AddInteraction(int, int, T);
</pre><p>This methods adds/inserts a value in the matrix. This is equivalent to use the access operator ().</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; A(5, 5);
// insertion
A.AddInteraction(1, 3, 2.6);
// we add
A.AddInteraction(1, 3, -1.0);
// now A should be equal to [1 3 1.6]
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#addinteractionrow">AddInteractionRow</a><br/>
 <a href="#addinteractioncolumn">AddInteractionColumn</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="addinteractionrow"></a></div><h3>AddInteractionRow</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void AddInteractionRow(int, int, Vector&lt;int&gt;, Vector&lt;T&gt;);
</pre><p>This methods adds/inserts several values in a row of the matrix. This is more efficient to use that method rather than calling AddInteraction several times.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; A(5, 5);
// insertion, you don't need to sort column numbers
int irow = 2;
int nb_values = 3;
IVect col(nb_values);
Vector&lt;double&gt; values(nb_values);
col(0) = 0; values(0) = 0.1;
col(1) = 3; values(1) = 0.6;
col(2) = 2; values(2) = -1.4;

A.AddInteractionRow(irow, nb_values, col, values);

</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#addinteraction">AddInteraction</a><br/>
 <a href="#addinteractioncolumn">AddInteractionColumn</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="addinteractioncolumn"></a></div><h3>AddInteractionColumn</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void AddInteractionColumns(int, int, Vector&lt;int&gt;, Vector&lt;T&gt;);
</pre><p>This methods adds/inserts several values in a column of the matrix. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; A(5, 5);
// insertion, you don't need to sort row numbers
int icol = 2;
int nb_values = 3;
IVect row(nb_values);
Vector&lt;double&gt; values(nb_values);
row(0) = 0; values(0) = 0.1;
row(1) = 3; values(1) = 0.6;
row(2) = 2; values(2) = -1.4;

A.AddInteractionColumn(icol, nb_values, row, values);

</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#addinteraction">AddInteraction</a><br/>
 <a href="#addinteractionrow">AddInteractionRow</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="print"></a></div><h3>Print</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Print() const;
</pre><p>This method displays the matrix.</p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="write"></a></div><h3>Write</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Write(string) const;
  void Write(ofstream&amp;) const;
</pre><p>This method writes the matrix on a file/stream in binary format. The file will contain the number of rows, columns, non-zero entries, then the array of values, indices of first elements of rows, and finals the array of column numbers.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// construction of a sparse matrix
Matrix&lt;double, General, RowSparse&gt; A(m, n, nnz, values, ptr, columns); 
// you can write directly in a file
A.Write("matrix.dat");

// or open a stream with other datas
ofstream file_out("matrix.dat");
int my_info = 3;
file_out.write(reinterpret_cast&lt;char*&gt;(&gt;my_info), sizeof(int));
A.Write(file_out);
file_out.close();
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#read">Read</a><br/>
 <a href="#writetext">WriteText</a><br/>
 <a href="#readtext">ReadText</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="read"></a></div><h3>Read</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Read(string);
  void Read(ifstream&amp;);
</pre><p>This method sets the matrix from a file/stream in binary format. The file contains the number of rows, columns, non-zero entries, then the array of values, indices of first elements of each row, column numbers.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General RowSparse&gt; A; 
// you can read directly on a file
A.Read("matrix.dat");

// or read from a stream
ifstream file_in("matrix.dat");
int my_info;
file_in.read(reinterpret_cast&lt;char*&lt;(&gt;my_info), sizeof(int));
A.Read(file_in);
file_in.close();
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#write">Write</a><br/>
 <a href="#writetext">WriteText</a><br/>
 <a href="#readtext">ReadText</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="writetext"></a></div><h3>WriteText</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void WriteText(string) const;
  void WriteText(ofstream&amp;) const;
</pre><p>This method writes the matrix on a file/stream in text format. The file will contain on each line a non-zero entry, i.e. :</p>
<p><em> row_number col_number value </em> </p>
<p>The row numbers and columns numbers used 1-index convention, i.e. the first row has its row number equal to 1. An exemple of output file is  </p>
<p><b> 1 1 0.123<br/>
 1 3 0.254<br/>
 1 4 -0.928<br/>
 2 1 -0.2<br/>
 2 5 -0.3 </b> </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; A(4,4);
A(1,3) = -1.0;
A(2,2) = 2.5;
// you can write directly in a file
A.WriteText("matrix.dat");

// or open a stream with other datas
ofstream file_out("matrix.dat");
int my_info = 3;
file_out &lt;&lt; my_info &lt;&lt; '\n';
A.WriteText(file_out);
file_out.close();
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#write">Write</a><br/>
 <a href="#read">Read</a><br/>
 <a href="#readtext">ReadText</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></p>
<div class="separator"><a class="anchor" id="readtext"></a></div><h3>ReadText</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void ReadText(string);
  void ReadText(ifstream&amp;);
</pre><p>This method sets the matrix from a file/stream in text format. The format is detailed in method <a href="#writetext">WriteText</a>.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, Symmetric, ArrayRowSymSparse&gt; V; 
// you can read directly on a file
V.ReadText("matrix.dat");

// or read from a stream
ifstream file_in("matrix.dat");
int my_info;
file_in &gt;&gt; my_info;
V.ReadText(file_in);
file_in.close();
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#write">Write</a><br/>
 <a href="#read">Read</a><br/>
 <a href="#writetext">WriteText</a></p>
<h4>Location :</h4>
<p>Class Matrix_ArraySparse<br/>
 <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a> <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a> </p>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
