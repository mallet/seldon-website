<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/interfaces/direct/SuperLU.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_SUPERLU_CXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 <span class="preprocessor">#include &quot;SuperLU.hxx&quot;</span>
<a name="l00023"></a>00023 
<a name="l00024"></a>00024 <span class="comment">// The function comes from the Matlab interface to SuperLU. It is part of</span>
<a name="l00025"></a>00025 <span class="comment">// SuperLU package. Its copyright is held by University of California</span>
<a name="l00026"></a>00026 <span class="comment">// Berkeley, Xerox Palo Alto Research Center and Lawrence Berkeley National</span>
<a name="l00027"></a>00027 <span class="comment">// Lab. It is released under a license compatible with the GNU LGPL.</span>
<a name="l00028"></a>00028 <span class="keywordtype">void</span> LUextract(SuperMatrix *L, SuperMatrix *U, <span class="keywordtype">double</span> *Lval, <span class="keywordtype">int</span> *Lrow,
<a name="l00029"></a>00029                <span class="keywordtype">int</span> *Lcol, <span class="keywordtype">double</span> *Uval, <span class="keywordtype">int</span> *Urow, <span class="keywordtype">int</span> *Ucol, <span class="keywordtype">int</span> *snnzL,
<a name="l00030"></a>00030                <span class="keywordtype">int</span> *snnzU)
<a name="l00031"></a>00031 {
<a name="l00032"></a>00032   <span class="keywordtype">int</span>         i, j, k;
<a name="l00033"></a>00033   <span class="keywordtype">int</span>         upper;
<a name="l00034"></a>00034   <span class="keywordtype">int</span>         fsupc, istart, nsupr;
<a name="l00035"></a>00035   <span class="keywordtype">int</span>         lastl = 0, lastu = 0;
<a name="l00036"></a>00036   SCformat    *Lstore;
<a name="l00037"></a>00037   NCformat    *Ustore;
<a name="l00038"></a>00038   <span class="keywordtype">double</span>      *SNptr;
<a name="l00039"></a>00039 
<a name="l00040"></a>00040   Lstore = <span class="keyword">static_cast&lt;</span>SCformat*<span class="keyword">&gt;</span>(L-&gt;Store);
<a name="l00041"></a>00041   Ustore = <span class="keyword">static_cast&lt;</span>NCformat*<span class="keyword">&gt;</span>(U-&gt;Store);
<a name="l00042"></a>00042   Lcol[0] = 0;
<a name="l00043"></a>00043   Ucol[0] = 0;
<a name="l00044"></a>00044 
<a name="l00045"></a>00045   <span class="comment">/* for each supernode */</span>
<a name="l00046"></a>00046   <span class="keywordflow">for</span> (k = 0; k &lt;= Lstore-&gt;nsuper; ++k) {
<a name="l00047"></a>00047 
<a name="l00048"></a>00048     fsupc = L_FST_SUPC(k);
<a name="l00049"></a>00049     istart = L_SUB_START(fsupc);
<a name="l00050"></a>00050     nsupr = L_SUB_START(fsupc+1) - istart;
<a name="l00051"></a>00051     upper = 1;
<a name="l00052"></a>00052 
<a name="l00053"></a>00053     <span class="comment">/* for each column in the supernode */</span>
<a name="l00054"></a>00054     <span class="keywordflow">for</span> (j = fsupc; j &lt; L_FST_SUPC(k+1); ++j) {
<a name="l00055"></a>00055       SNptr = &amp;(<span class="keyword">static_cast&lt;</span><span class="keywordtype">double</span>*<span class="keyword">&gt;</span>(Lstore-&gt;nzval))[L_NZ_START(j)];
<a name="l00056"></a>00056 
<a name="l00057"></a>00057       <span class="comment">/* Extract U */</span>
<a name="l00058"></a>00058       <span class="keywordflow">for</span> (i = U_NZ_START(j); i &lt; U_NZ_START(j+1); ++i) {
<a name="l00059"></a>00059         Uval[lastu] = (<span class="keyword">static_cast&lt;</span><span class="keywordtype">double</span>*<span class="keyword">&gt;</span>(Ustore-&gt;nzval))[i];
<a name="l00060"></a>00060         Urow[lastu++] = U_SUB(i);
<a name="l00061"></a>00061       }
<a name="l00062"></a>00062       <span class="keywordflow">for</span> (i = 0; i &lt; upper; ++i) { <span class="comment">/* upper triangle in the supernode */</span>
<a name="l00063"></a>00063         Uval[lastu] = SNptr[i];
<a name="l00064"></a>00064         Urow[lastu++] = L_SUB(istart+i);
<a name="l00065"></a>00065       }
<a name="l00066"></a>00066       Ucol[j+1] = lastu;
<a name="l00067"></a>00067 
<a name="l00068"></a>00068       <span class="comment">/* Extract L */</span>
<a name="l00069"></a>00069       Lval[lastl] = 1.0; <span class="comment">/* unit diagonal */</span>
<a name="l00070"></a>00070       Lrow[lastl++] = L_SUB(istart + upper - 1);
<a name="l00071"></a>00071       <span class="keywordflow">for</span> (i = upper; i &lt; nsupr; ++i) {
<a name="l00072"></a>00072         Lval[lastl] = SNptr[i];
<a name="l00073"></a>00073         Lrow[lastl++] = L_SUB(istart+i);
<a name="l00074"></a>00074       }
<a name="l00075"></a>00075       Lcol[j+1] = lastl;
<a name="l00076"></a>00076 
<a name="l00077"></a>00077       ++upper;
<a name="l00078"></a>00078 
<a name="l00079"></a>00079     } <span class="comment">/* for j ... */</span>
<a name="l00080"></a>00080 
<a name="l00081"></a>00081   } <span class="comment">/* for k ... */</span>
<a name="l00082"></a>00082 
<a name="l00083"></a>00083   *snnzL = lastl;
<a name="l00084"></a>00084   *snnzU = lastu;
<a name="l00085"></a>00085 }
<a name="l00086"></a>00086 
<a name="l00087"></a>00087 
<a name="l00088"></a>00088 <span class="keyword">namespace </span>Seldon
<a name="l00089"></a>00089 {
<a name="l00091"></a>00091   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00092"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a526c7f0e29c86b81fc4705b437e167f9">00092</a>   <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a526c7f0e29c86b81fc4705b437e167f9" title="default constructor">MatrixSuperLU_Base&lt;T&gt;::MatrixSuperLU_Base</a>()
<a name="l00093"></a>00093   {
<a name="l00094"></a>00094     <span class="comment">//   permc_spec = 0: use the natural ordering</span>
<a name="l00095"></a>00095     <span class="comment">//   permc_spec = 1: use minimum degree ordering on structure of A&#39;*A</span>
<a name="l00096"></a>00096     <span class="comment">//   permc_spec = 2: use minimum degree ordering on structure of A&#39;+A</span>
<a name="l00097"></a>00097     <span class="comment">//   permc_spec = 3: use approximate mininum degree column ordering</span>
<a name="l00098"></a>00098     <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a3a2831b9dac4fa42c7815015627f5f38" title="number of rows">n</a> = 0;
<a name="l00099"></a>00099     <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a8d4acdd57e0a5e8a8a0651e1de0a8547" title="ordering scheme">permc_spec</a> = 2;
<a name="l00100"></a>00100     Lstore = NULL;
<a name="l00101"></a>00101     Ustore = NULL;
<a name="l00102"></a>00102     StatInit(&amp;<a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a51a947858f30367b812df38f97ec3aa4" title="statistics">stat</a>);
<a name="l00103"></a>00103     set_default_options(&amp;<a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a3de025ec4fdc83b5c793d69becc051c1" title="options //! permutation array">options</a>);
<a name="l00104"></a>00104     <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#af5322d1293240d5d3ab6aeb93183acc7" title="allows messages from SuperLU">ShowMessages</a>();
<a name="l00105"></a>00105     <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a5b067c2c8ef0a0fcbfec31d399fd2bc3" title="display information about factorization ?">display_info</a> = <span class="keyword">false</span>;
<a name="l00106"></a>00106   }
<a name="l00107"></a>00107 
<a name="l00108"></a>00108 
<a name="l00110"></a>00110   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00111"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a8f0a2adf458ec4f187f3936fea612106">00111</a>   <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a8f0a2adf458ec4f187f3936fea612106" title="destructor">MatrixSuperLU_Base&lt;T&gt;::~MatrixSuperLU_Base</a>()
<a name="l00112"></a>00112   {
<a name="l00113"></a>00113     <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#acac4d12b1d9e464f630b7d56b2c61a23" title="same effect as a call to the destructor">Clear</a>();
<a name="l00114"></a>00114   }
<a name="l00115"></a>00115 
<a name="l00116"></a>00116 
<a name="l00118"></a>00118 
<a name="l00127"></a>00127   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00128"></a>00128   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00129"></a>00129   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a6492cdcde57b8524db187be6f4602ef5" title="Returns the LU factorization.">MatrixSuperLU_Base&lt;T&gt;</a>
<a name="l00130"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a6492cdcde57b8524db187be6f4602ef5">00130</a> <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a6492cdcde57b8524db187be6f4602ef5" title="Returns the LU factorization.">  ::GetLU</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, Prop, ColSparse, Allocator&gt;</a>&amp; Lmat,
<a name="l00131"></a>00131           <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, Prop, ColSparse, Allocator&gt;</a>&amp; Umat,
<a name="l00132"></a>00132           <span class="keywordtype">bool</span> permuted)
<a name="l00133"></a>00133   {
<a name="l00134"></a>00134     Lstore = <span class="keyword">static_cast&lt;</span>SCformat*<span class="keyword">&gt;</span>(L.Store);
<a name="l00135"></a>00135     Ustore = <span class="keyword">static_cast&lt;</span>NCformat*<span class="keyword">&gt;</span>(U.Store);
<a name="l00136"></a>00136 
<a name="l00137"></a>00137     <span class="keywordtype">int</span> Lnnz = Lstore-&gt;nnz;
<a name="l00138"></a>00138     <span class="keywordtype">int</span> Unnz = Ustore-&gt;nnz;
<a name="l00139"></a>00139 
<a name="l00140"></a>00140     <span class="keywordtype">int</span> m = U.nrow;
<a name="l00141"></a>00141     <span class="keywordtype">int</span> n = U.ncol;
<a name="l00142"></a>00142 
<a name="l00143"></a>00143     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double, VectFull, Allocator&gt;</a> Lval(Lnnz);
<a name="l00144"></a>00144     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; Lrow(Lnnz);
<a name="l00145"></a>00145     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; Lcol(n + 1);
<a name="l00146"></a>00146 
<a name="l00147"></a>00147     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double, VectFull, Allocator&gt;</a> Uval(Unnz);
<a name="l00148"></a>00148     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; Urow(Unnz);
<a name="l00149"></a>00149     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; Ucol(n + 1);
<a name="l00150"></a>00150 
<a name="l00151"></a>00151     <span class="keywordtype">int</span> Lsnnz;
<a name="l00152"></a>00152     <span class="keywordtype">int</span> Usnnz;
<a name="l00153"></a>00153     LUextract(&amp;L, &amp;U, Lval.GetData(), Lrow.GetData(), Lcol.GetData(),
<a name="l00154"></a>00154               Uval.GetData(), Urow.GetData(), Ucol.GetData(), &amp;Lsnnz, &amp;Usnnz);
<a name="l00155"></a>00155 
<a name="l00156"></a>00156     Lmat.SetData(m, n, Lval, Lcol, Lrow);
<a name="l00157"></a>00157     Umat.SetData(m, n, Uval, Ucol, Urow);
<a name="l00158"></a>00158 
<a name="l00159"></a>00159     <span class="keywordflow">if</span> (!permuted)
<a name="l00160"></a>00160       {
<a name="l00161"></a>00161         <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> row_perm_orig = perm_r;
<a name="l00162"></a>00162         <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> col_perm_orig = perm_c;
<a name="l00163"></a>00163 
<a name="l00164"></a>00164         <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> row_perm(n);
<a name="l00165"></a>00165         <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> col_perm(n);
<a name="l00166"></a>00166         row_perm.Fill();
<a name="l00167"></a>00167         col_perm.Fill();
<a name="l00168"></a>00168 
<a name="l00169"></a>00169         <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(row_perm_orig, row_perm);
<a name="l00170"></a>00170         <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(col_perm_orig, col_perm);
<a name="l00171"></a>00171 
<a name="l00172"></a>00172         <a class="code" href="namespace_seldon.php#a48edc9483ed12de114cfde40b9bed96e" title="Inverse permutation of a general matrix stored by rows.">ApplyInversePermutation</a>(Lmat, row_perm, col_perm);
<a name="l00173"></a>00173         <a class="code" href="namespace_seldon.php#a48edc9483ed12de114cfde40b9bed96e" title="Inverse permutation of a general matrix stored by rows.">ApplyInversePermutation</a>(Umat, row_perm, col_perm);
<a name="l00174"></a>00174       }
<a name="l00175"></a>00175   }
<a name="l00176"></a>00176 
<a name="l00177"></a>00177 
<a name="l00179"></a>00179 
<a name="l00190"></a>00190   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00191"></a>00191   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00192"></a>00192   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a6492cdcde57b8524db187be6f4602ef5" title="Returns the LU factorization.">MatrixSuperLU_Base&lt;T&gt;</a>
<a name="l00193"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a7713db1a177053fdee0cd121411ae986">00193</a> <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a6492cdcde57b8524db187be6f4602ef5" title="Returns the LU factorization.">  ::GetLU</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, Prop, RowSparse, Allocator&gt;</a>&amp; Lmat,
<a name="l00194"></a>00194           <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, Prop, RowSparse, Allocator&gt;</a>&amp; Umat,
<a name="l00195"></a>00195           <span class="keywordtype">bool</span> permuted)
<a name="l00196"></a>00196   {
<a name="l00197"></a>00197     Lmat.Clear();
<a name="l00198"></a>00198     Umat.Clear();
<a name="l00199"></a>00199 
<a name="l00200"></a>00200     <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, Prop, ColSparse, Allocator&gt;</a> Lmat_col;
<a name="l00201"></a>00201     <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, Prop, ColSparse, Allocator&gt;</a> Umat_col;
<a name="l00202"></a>00202     <a class="code" href="namespace_seldon.php#a76d3f187a877c6c58245b0716e1adc00" title="Returns the LU factorization of a matrix.">GetLU</a>(Lmat_col, Umat_col, permuted);
<a name="l00203"></a>00203 
<a name="l00204"></a>00204     Copy(Lmat_col, Lmat);
<a name="l00205"></a>00205     Lmat_col.Clear();
<a name="l00206"></a>00206     Copy(Umat_col, Umat);
<a name="l00207"></a>00207     Umat_col.Clear();
<a name="l00208"></a>00208   }
<a name="l00209"></a>00209 
<a name="l00210"></a>00210 
<a name="l00212"></a>00212 
<a name="l00218"></a>00218   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00219"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a27aca5955b9d58ab561f652f153139ee">00219</a>   <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a>&amp; <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a27aca5955b9d58ab561f652f153139ee" title="Returns the permutation of rows.">MatrixSuperLU_Base&lt;T&gt;::GetRowPermutation</a>()<span class="keyword"> const</span>
<a name="l00220"></a>00220 <span class="keyword">  </span>{
<a name="l00221"></a>00221     <span class="keywordflow">return</span> perm_r;
<a name="l00222"></a>00222   }
<a name="l00223"></a>00223 
<a name="l00224"></a>00224 
<a name="l00226"></a>00226 
<a name="l00232"></a>00232   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00233"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#acf6c8120e481e56c99e77bea79b30f84">00233</a>   <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a>&amp; <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#acf6c8120e481e56c99e77bea79b30f84" title="Returns the permutation of columns.">MatrixSuperLU_Base&lt;T&gt;::GetColPermutation</a>()<span class="keyword"> const</span>
<a name="l00234"></a>00234 <span class="keyword">  </span>{
<a name="l00235"></a>00235     <span class="keywordflow">return</span> perm_c;
<a name="l00236"></a>00236   }
<a name="l00237"></a>00237 
<a name="l00238"></a>00238 
<a name="l00240"></a>00240   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00241"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#acac4d12b1d9e464f630b7d56b2c61a23">00241</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#acac4d12b1d9e464f630b7d56b2c61a23" title="same effect as a call to the destructor">MatrixSuperLU_Base&lt;T&gt;::Clear</a>()
<a name="l00242"></a>00242   {
<a name="l00243"></a>00243     <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a3a2831b9dac4fa42c7815015627f5f38" title="number of rows">n</a> &gt; 0)
<a name="l00244"></a>00244       {
<a name="l00245"></a>00245         <span class="comment">// SuperLU objects are cleared</span>
<a name="l00246"></a>00246         Destroy_CompCol_Matrix(&amp;<a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a6dd29bcb497b1c7175d6dc9f10ac8e53" title="objects of SuperLU">A</a>);
<a name="l00247"></a>00247         Destroy_SuperMatrix_Store(&amp;B);
<a name="l00248"></a>00248         Destroy_SuperNode_Matrix(&amp;L);
<a name="l00249"></a>00249         Destroy_CompCol_Matrix(&amp;U);
<a name="l00250"></a>00250         perm_r.Clear(); perm_c.Clear();
<a name="l00251"></a>00251         <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a3a2831b9dac4fa42c7815015627f5f38" title="number of rows">n</a> = 0;
<a name="l00252"></a>00252       }
<a name="l00253"></a>00253   }
<a name="l00254"></a>00254 
<a name="l00255"></a>00255 
<a name="l00257"></a>00257   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00258"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a733cd4e38035ae701435f9f298442e4d">00258</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a733cd4e38035ae701435f9f298442e4d" title="no message from SuperLU">MatrixSuperLU_Base&lt;T&gt;::HideMessages</a>()
<a name="l00259"></a>00259   {
<a name="l00260"></a>00260     <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a5b067c2c8ef0a0fcbfec31d399fd2bc3" title="display information about factorization ?">display_info</a> = <span class="keyword">false</span>;
<a name="l00261"></a>00261   }
<a name="l00262"></a>00262 
<a name="l00263"></a>00263 
<a name="l00265"></a>00265   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00266"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#af5322d1293240d5d3ab6aeb93183acc7">00266</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#af5322d1293240d5d3ab6aeb93183acc7" title="allows messages from SuperLU">MatrixSuperLU_Base&lt;T&gt;::ShowMessages</a>()
<a name="l00267"></a>00267   {
<a name="l00268"></a>00268     <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a5b067c2c8ef0a0fcbfec31d399fd2bc3" title="display information about factorization ?">display_info</a> = <span class="keyword">true</span>;
<a name="l00269"></a>00269   }
<a name="l00270"></a>00270 
<a name="l00271"></a>00271 
<a name="l00273"></a>00273   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00274"></a>00274   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u.php" title="empty matrix">MatrixSuperLU&lt;double&gt;::</a>
<a name="l00275"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u_3_01double_01_4.php#a5d2143c9a6bdc69399bf719e704701b1">00275</a> <a class="code" href="class_seldon_1_1_matrix_super_l_u.php" title="empty matrix">  FactorizeMatrix</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, Prop, Storage, Allocator&gt;</a> &amp; mat,
<a name="l00276"></a>00276                   <span class="keywordtype">bool</span> keep_matrix)
<a name="l00277"></a>00277   {
<a name="l00278"></a>00278     <span class="comment">// clearing previous factorization</span>
<a name="l00279"></a>00279     <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#acac4d12b1d9e464f630b7d56b2c61a23" title="same effect as a call to the destructor">Clear</a>();
<a name="l00280"></a>00280 
<a name="l00281"></a>00281     <span class="comment">// conversion in CSC format</span>
<a name="l00282"></a>00282     <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a3a2831b9dac4fa42c7815015627f5f38" title="number of rows">n</a> = mat.GetN();
<a name="l00283"></a>00283     <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, General, ColSparse&gt;</a> Acsr;
<a name="l00284"></a>00284     Copy(mat, Acsr);
<a name="l00285"></a>00285     <span class="keywordflow">if</span> (!keep_matrix)
<a name="l00286"></a>00286       mat.Clear();
<a name="l00287"></a>00287 
<a name="l00288"></a>00288     <span class="comment">// we get renumbering vectors perm_r and perm_c</span>
<a name="l00289"></a>00289     <span class="keywordtype">int</span> nnz = Acsr.GetDataSize();
<a name="l00290"></a>00290     dCreate_CompCol_Matrix(&amp;<a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a6dd29bcb497b1c7175d6dc9f10ac8e53" title="objects of SuperLU">A</a>, <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a3a2831b9dac4fa42c7815015627f5f38" title="number of rows">n</a>, <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a3a2831b9dac4fa42c7815015627f5f38" title="number of rows">n</a>, nnz, Acsr.GetData(), Acsr.GetInd(),
<a name="l00291"></a>00291                            Acsr.GetPtr(), SLU_NC, SLU_D, SLU_GE);
<a name="l00292"></a>00292 
<a name="l00293"></a>00293     perm_r.Reallocate(<a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a3a2831b9dac4fa42c7815015627f5f38" title="number of rows">n</a>);
<a name="l00294"></a>00294     perm_c.Reallocate(<a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a3a2831b9dac4fa42c7815015627f5f38" title="number of rows">n</a>);
<a name="l00295"></a>00295 
<a name="l00296"></a>00296     <span class="comment">// factorization -&gt; no right hand side</span>
<a name="l00297"></a>00297     <span class="keywordtype">int</span> nb_rhs = 0, info;
<a name="l00298"></a>00298     dCreate_Dense_Matrix(&amp;B, <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a3a2831b9dac4fa42c7815015627f5f38" title="number of rows">n</a>, nb_rhs, NULL, <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a3a2831b9dac4fa42c7815015627f5f38" title="number of rows">n</a>, SLU_DN, SLU_D, SLU_GE);
<a name="l00299"></a>00299 
<a name="l00300"></a>00300     dgssv(&amp;<a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a3de025ec4fdc83b5c793d69becc051c1" title="options //! permutation array">options</a>, &amp;<a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a6dd29bcb497b1c7175d6dc9f10ac8e53" title="objects of SuperLU">A</a>, perm_c.GetData(), perm_r.GetData(),
<a name="l00301"></a>00301           &amp;L, &amp;U, &amp;B, &amp;<a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a51a947858f30367b812df38f97ec3aa4" title="statistics">stat</a>, &amp;info);
<a name="l00302"></a>00302 
<a name="l00303"></a>00303     <span class="keywordflow">if</span> ((info==0)&amp;&amp;(<a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a5b067c2c8ef0a0fcbfec31d399fd2bc3" title="display information about factorization ?">display_info</a>))
<a name="l00304"></a>00304       {
<a name="l00305"></a>00305         mem_usage_t mem_usage;
<a name="l00306"></a>00306         Lstore = (SCformat *) L.Store;
<a name="l00307"></a>00307         Ustore = (NCformat *) U.Store;
<a name="l00308"></a>00308         cout&lt;&lt;<span class="stringliteral">&quot;No of nonzeros in factor L = &quot;</span>&lt;&lt;Lstore-&gt;nnz&lt;&lt;endl;
<a name="l00309"></a>00309         cout&lt;&lt;<span class="stringliteral">&quot;No of nonzeros in factor U = &quot;</span>&lt;&lt;Ustore-&gt;nnz&lt;&lt;endl;
<a name="l00310"></a>00310         cout&lt;&lt;<span class="stringliteral">&quot;No of nonzeros in L+U     = &quot;</span>&lt;&lt;(Lstore-&gt;nnz+Ustore-&gt;nnz)&lt;&lt;endl;
<a name="l00311"></a>00311         dQuerySpace(&amp;L, &amp;U, &amp;mem_usage);
<a name="l00312"></a>00312         cout&lt;&lt;<span class="stringliteral">&quot;Memory used for factorisation in Mo &quot;</span>
<a name="l00313"></a>00313             &lt;&lt;mem_usage.total_needed/(1024*1024)&lt;&lt;endl;
<a name="l00314"></a>00314       }
<a name="l00315"></a>00315 
<a name="l00316"></a>00316     Acsr.Nullify();
<a name="l00317"></a>00317   }
<a name="l00318"></a>00318 
<a name="l00319"></a>00319 
<a name="l00321"></a>00321   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2&gt;
<a name="l00322"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u_3_01double_01_4.php#a983be1d0a4172a365493da4752757317">00322</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u.php" title="empty matrix">MatrixSuperLU&lt;double&gt;::Solve</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double, VectFull, Allocator2&gt;</a>&amp; x)
<a name="l00323"></a>00323   {
<a name="l00324"></a>00324     trans_t trans = NOTRANS;
<a name="l00325"></a>00325     <span class="keywordtype">int</span> nb_rhs = 1, info;
<a name="l00326"></a>00326     dCreate_Dense_Matrix(&amp;B, x.GetM(), nb_rhs,
<a name="l00327"></a>00327                          x.GetData(), x.GetM(), SLU_DN, SLU_D, SLU_GE);
<a name="l00328"></a>00328 
<a name="l00329"></a>00329     SuperLUStat_t <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a51a947858f30367b812df38f97ec3aa4" title="statistics">stat</a>;
<a name="l00330"></a>00330     StatInit(&amp;stat);
<a name="l00331"></a>00331     dgstrs(trans, &amp;L, &amp;U, perm_r.GetData(),
<a name="l00332"></a>00332            perm_c.GetData(), &amp;B, &amp;stat, &amp;info);
<a name="l00333"></a>00333   }
<a name="l00334"></a>00334 
<a name="l00335"></a>00335 
<a name="l00337"></a>00337   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2&gt;
<a name="l00338"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u_3_01double_01_4.php#ab77e7d1e2809ce3a53995476c7dd8f4c">00338</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u.php" title="empty matrix">MatrixSuperLU&lt;double&gt;::Solve</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; TransA,
<a name="l00339"></a>00339                                     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double, VectFull, Allocator2&gt;</a>&amp; x)
<a name="l00340"></a>00340   {
<a name="l00341"></a>00341     <span class="keywordflow">if</span> (TransA.NoTrans())
<a name="l00342"></a>00342       {
<a name="l00343"></a>00343         Solve(x);
<a name="l00344"></a>00344         <span class="keywordflow">return</span>;
<a name="l00345"></a>00345       }
<a name="l00346"></a>00346 
<a name="l00347"></a>00347     trans_t trans = TRANS;
<a name="l00348"></a>00348     <span class="keywordtype">int</span> nb_rhs = 1, info;
<a name="l00349"></a>00349     dCreate_Dense_Matrix(&amp;B, x.GetM(), nb_rhs,
<a name="l00350"></a>00350                          x.GetData(), x.GetM(), SLU_DN, SLU_D, SLU_GE);
<a name="l00351"></a>00351 
<a name="l00352"></a>00352     SuperLUStat_t <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a51a947858f30367b812df38f97ec3aa4" title="statistics">stat</a>;
<a name="l00353"></a>00353     StatInit(&amp;stat);
<a name="l00354"></a>00354     dgstrs(trans, &amp;L, &amp;U, perm_r.GetData(),
<a name="l00355"></a>00355            perm_c.GetData(), &amp;B, &amp;stat, &amp;info);
<a name="l00356"></a>00356   }
<a name="l00357"></a>00357 
<a name="l00358"></a>00358 
<a name="l00360"></a>00360   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00361"></a>00361   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u.php" title="empty matrix">MatrixSuperLU&lt;complex&lt;double&gt;</a> &gt;::
<a name="l00362"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u_3_01complex_3_01double_01_4_01_4.php#aff33f45fbe34f32a2e8db108e406c51b">00362</a>   FactorizeMatrix(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;complex&lt;double&gt;</a>, Prop, Storage, Allocator&gt; &amp; mat,
<a name="l00363"></a>00363                   <span class="keywordtype">bool</span> keep_matrix)
<a name="l00364"></a>00364   {
<a name="l00365"></a>00365     <span class="comment">// clearing previous factorization</span>
<a name="l00366"></a>00366     Clear();
<a name="l00367"></a>00367 
<a name="l00368"></a>00368     <span class="comment">// conversion in CSR format</span>
<a name="l00369"></a>00369     n = mat.GetN();
<a name="l00370"></a>00370     <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;complex&lt;double&gt;</a>, <a class="code" href="class_seldon_1_1_general.php">General</a>, <a class="code" href="class_seldon_1_1_col_sparse.php">ColSparse</a>&gt; Acsr;
<a name="l00371"></a>00371     Copy(mat, Acsr);
<a name="l00372"></a>00372     <span class="keywordflow">if</span> (!keep_matrix)
<a name="l00373"></a>00373       mat.Clear();
<a name="l00374"></a>00374 
<a name="l00375"></a>00375     <span class="comment">// we get renumbering vectors perm_r and perm_c</span>
<a name="l00376"></a>00376     <span class="keywordtype">int</span> nnz = Acsr.GetDataSize();
<a name="l00377"></a>00377     zCreate_CompCol_Matrix(&amp;A, n, n, nnz,
<a name="l00378"></a>00378                            reinterpret_cast&lt;doublecomplex*&gt;(Acsr.GetData()),
<a name="l00379"></a>00379                            Acsr.GetInd(), Acsr.GetPtr(),
<a name="l00380"></a>00380                            SLU_NC, SLU_Z, SLU_GE);
<a name="l00381"></a>00381 
<a name="l00382"></a>00382     perm_r.Reallocate(n);
<a name="l00383"></a>00383     perm_c.Reallocate(n);
<a name="l00384"></a>00384 
<a name="l00385"></a>00385     <span class="keywordtype">int</span> nb_rhs = 0, info;
<a name="l00386"></a>00386     zCreate_Dense_Matrix(&amp;B, n, nb_rhs, NULL, n, SLU_DN, SLU_Z, SLU_GE);
<a name="l00387"></a>00387 
<a name="l00388"></a>00388     zgssv(&amp;options, &amp;A, perm_c.GetData(), perm_r.GetData(),
<a name="l00389"></a>00389           &amp;L, &amp;U, &amp;B, &amp;stat, &amp;info);
<a name="l00390"></a>00390 
<a name="l00391"></a>00391     <span class="keywordflow">if</span> ((info==0)&amp;&amp;(display_info))
<a name="l00392"></a>00392       {
<a name="l00393"></a>00393         mem_usage_t mem_usage;
<a name="l00394"></a>00394         Lstore = (SCformat *) L.Store;
<a name="l00395"></a>00395         Ustore = (NCformat *) U.Store;
<a name="l00396"></a>00396         cout&lt;&lt;<span class="stringliteral">&quot;No of nonzeros in factor L = &quot;</span>&lt;&lt;Lstore-&gt;nnz&lt;&lt;endl;
<a name="l00397"></a>00397         cout&lt;&lt;<span class="stringliteral">&quot;No of nonzeros in factor U = &quot;</span>&lt;&lt;Ustore-&gt;nnz&lt;&lt;endl;
<a name="l00398"></a>00398         cout&lt;&lt;<span class="stringliteral">&quot;No of nonzeros in L+U     = &quot;</span>&lt;&lt;(Lstore-&gt;nnz+Ustore-&gt;nnz)&lt;&lt;endl;
<a name="l00399"></a>00399         zQuerySpace(&amp;L, &amp;U, &amp;mem_usage);
<a name="l00400"></a>00400         cout&lt;&lt;<span class="stringliteral">&quot;Memory used for factorisation in Mo &quot;</span>
<a name="l00401"></a>00401             &lt;&lt;mem_usage.total_needed/1e6&lt;&lt;endl;
<a name="l00402"></a>00402       }
<a name="l00403"></a>00403 
<a name="l00404"></a>00404     Acsr.Nullify();
<a name="l00405"></a>00405   }
<a name="l00406"></a>00406 
<a name="l00407"></a>00407 
<a name="l00409"></a>00409   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2&gt;
<a name="l00410"></a>00410   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u.php" title="empty matrix">MatrixSuperLU&lt;complex&lt;double&gt;</a> &gt;::
<a name="l00411"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u_3_01complex_3_01double_01_4_01_4.php#a13625fddc750bb908d481b2c789344eb">00411</a>   Solve(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;complex&lt;double&gt;</a>, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator2&gt;&amp; x)
<a name="l00412"></a>00412   {
<a name="l00413"></a>00413     trans_t trans = NOTRANS;
<a name="l00414"></a>00414     <span class="keywordtype">int</span> nb_rhs = 1, info;
<a name="l00415"></a>00415     zCreate_Dense_Matrix(&amp;B, x.GetM(), nb_rhs,
<a name="l00416"></a>00416                          <span class="keyword">reinterpret_cast&lt;</span>doublecomplex*<span class="keyword">&gt;</span>(x.GetData()),
<a name="l00417"></a>00417                          x.GetM(), SLU_DN, SLU_Z, SLU_GE);
<a name="l00418"></a>00418 
<a name="l00419"></a>00419     zgstrs (trans, &amp;L, &amp;U, perm_r.GetData(),
<a name="l00420"></a>00420             perm_c.GetData(), &amp;B, &amp;stat, &amp;info);
<a name="l00421"></a>00421   }
<a name="l00422"></a>00422 
<a name="l00423"></a>00423 
<a name="l00425"></a>00425   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2&gt;
<a name="l00426"></a>00426   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u.php" title="empty matrix">MatrixSuperLU&lt;complex&lt;double&gt;</a> &gt;::
<a name="l00427"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u_3_01complex_3_01double_01_4_01_4.php#a81a49129375211e439ffc2e96d494d0e">00427</a>   Solve(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; TransA,
<a name="l00428"></a>00428         <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;complex&lt;double&gt;</a>, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator2&gt;&amp; x)
<a name="l00429"></a>00429   {
<a name="l00430"></a>00430     <span class="keywordflow">if</span> (TransA.NoTrans())
<a name="l00431"></a>00431       {
<a name="l00432"></a>00432         Solve(x);
<a name="l00433"></a>00433         <span class="keywordflow">return</span>;
<a name="l00434"></a>00434       }
<a name="l00435"></a>00435 
<a name="l00436"></a>00436     trans_t trans = TRANS;
<a name="l00437"></a>00437     <span class="keywordtype">int</span> nb_rhs = 1, info;
<a name="l00438"></a>00438     zCreate_Dense_Matrix(&amp;B, x.GetM(), nb_rhs,
<a name="l00439"></a>00439                          <span class="keyword">reinterpret_cast&lt;</span>doublecomplex*<span class="keyword">&gt;</span>(x.GetData()),
<a name="l00440"></a>00440                          x.GetM(), SLU_DN, SLU_Z, SLU_GE);
<a name="l00441"></a>00441 
<a name="l00442"></a>00442     zgstrs (trans, &amp;L, &amp;U, perm_r.GetData(),
<a name="l00443"></a>00443             perm_c.GetData(), &amp;B, &amp;stat, &amp;info);
<a name="l00444"></a>00444   }
<a name="l00445"></a>00445 
<a name="l00446"></a>00446 
<a name="l00447"></a>00447   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00448"></a>00448   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a76d3f187a877c6c58245b0716e1adc00" title="Returns the LU factorization of a matrix.">GetLU</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A, <a class="code" href="class_seldon_1_1_matrix_super_l_u.php" title="empty matrix">MatrixSuperLU&lt;T&gt;</a>&amp; mat_lu,
<a name="l00449"></a>00449              <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>)
<a name="l00450"></a>00450   {
<a name="l00451"></a>00451     mat_lu.FactorizeMatrix(A, keep_matrix);
<a name="l00452"></a>00452   }
<a name="l00453"></a>00453 
<a name="l00454"></a>00454 
<a name="l00455"></a>00455   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00456"></a>00456   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(MatrixSuperLU&lt;T&gt;&amp; mat_lu, Vector&lt;T, VectFull, Allocator&gt;&amp; x)
<a name="l00457"></a>00457   {
<a name="l00458"></a>00458     mat_lu.Solve(x);
<a name="l00459"></a>00459   }
<a name="l00460"></a>00460 
<a name="l00461"></a>00461 
<a name="l00462"></a>00462   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00463"></a>00463   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(<span class="keyword">const</span> SeldonTranspose&amp; TransA,
<a name="l00464"></a>00464                MatrixSuperLU&lt;T&gt;&amp; mat_lu, Vector&lt;T, VectFull, Allocator&gt;&amp; x)
<a name="l00465"></a>00465   {
<a name="l00466"></a>00466     mat_lu.Solve(TransA, x);
<a name="l00467"></a>00467   }
<a name="l00468"></a>00468 
<a name="l00469"></a>00469 }
<a name="l00470"></a>00470 
<a name="l00471"></a>00471 <span class="preprocessor">#define SELDON_FILE_SUPERLU_CXX</span>
<a name="l00472"></a>00472 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
