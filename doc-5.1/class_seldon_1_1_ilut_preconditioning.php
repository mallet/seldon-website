<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_ilut_preconditioning.php">IlutPreconditioning</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::IlutPreconditioning" -->
<p><a href="class_seldon_1_1_ilut_preconditioning-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">enum &nbsp;</td><td class="memItemRight" valign="bottom">{ <br/>
&nbsp;&nbsp;<b>ILUT</b>, 
<b>ILU_D</b>, 
<b>ILUT_K</b>, 
<b>ILU_0</b>, 
<br/>
&nbsp;&nbsp;<b>MILU_0</b>, 
<b>ILU_K</b>
<br/>
 }</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight"><p>Available types of incomplete factorization. </p>
<br/></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa874cdc7e6ac2c42cf182101c41a922e"></a><!-- doxytag: member="Seldon::IlutPreconditioning::Clear" ref="aa874cdc7e6ac2c42cf182101c41a922e" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Clear</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0b349349304281444b99d6b636f15279"></a><!-- doxytag: member="Seldon::IlutPreconditioning::GetFactorisationType" ref="a0b349349304281444b99d6b636f15279" args="() const " -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetFactorisationType</b> () const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5fa91acede8482fd733539540a1aa759"></a><!-- doxytag: member="Seldon::IlutPreconditioning::GetFillLevel" ref="a5fa91acede8482fd733539540a1aa759" args="() const " -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetFillLevel</b> () const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a996cb92d8e41fa38c9239a583eb62d2f"></a><!-- doxytag: member="Seldon::IlutPreconditioning::GetAdditionalFillNumber" ref="a996cb92d8e41fa38c9239a583eb62d2f" args="() const " -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetAdditionalFillNumber</b> () const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a52bed9fe9ac114e418ec2184c057b0bb"></a><!-- doxytag: member="Seldon::IlutPreconditioning::GetPrintLevel" ref="a52bed9fe9ac114e418ec2184c057b0bb" args="() const " -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetPrintLevel</b> () const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a659f53b4db4dd04d8cf7df241a29c39f"></a><!-- doxytag: member="Seldon::IlutPreconditioning::GetPivotBlockInteger" ref="a659f53b4db4dd04d8cf7df241a29c39f" args="() const " -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetPivotBlockInteger</b> () const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a03d2c8814c4f27e29605d2c8b8ebaad5"></a><!-- doxytag: member="Seldon::IlutPreconditioning::SetFactorisationType" ref="a03d2c8814c4f27e29605d2c8b8ebaad5" args="(int)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetFactorisationType</b> (int)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab13a66e83c18ded5f1a0afe35a9f6b14"></a><!-- doxytag: member="Seldon::IlutPreconditioning::SetFillLevel" ref="ab13a66e83c18ded5f1a0afe35a9f6b14" args="(int)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetFillLevel</b> (int)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8a3552888aac75626f7b81a5da95a2cd"></a><!-- doxytag: member="Seldon::IlutPreconditioning::SetAdditionalFillNumber" ref="a8a3552888aac75626f7b81a5da95a2cd" args="(int)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetAdditionalFillNumber</b> (int)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4886eee487edfa88b8b786f0289e2b7a"></a><!-- doxytag: member="Seldon::IlutPreconditioning::SetPrintLevel" ref="a4886eee487edfa88b8b786f0289e2b7a" args="(int)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetPrintLevel</b> (int)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae6569b57ce4c500cd48386c024bbf4d9"></a><!-- doxytag: member="Seldon::IlutPreconditioning::SetPivotBlockInteger" ref="ae6569b57ce4c500cd48386c024bbf4d9" args="(int)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetPivotBlockInteger</b> (int)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a957f1a043dd241632439afb36a677ae9"></a><!-- doxytag: member="Seldon::IlutPreconditioning::SetSymmetricAlgorithm" ref="a957f1a043dd241632439afb36a677ae9" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetSymmetricAlgorithm</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae64582b5c81eb3d370697d7a99a27176"></a><!-- doxytag: member="Seldon::IlutPreconditioning::SetUnsymmetricAlgorithm" ref="ae64582b5c81eb3d370697d7a99a27176" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetUnsymmetricAlgorithm</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a15b3466f7a1b9ac051594e4e410f8cce"></a><!-- doxytag: member="Seldon::IlutPreconditioning::GetDroppingThreshold" ref="a15b3466f7a1b9ac051594e4e410f8cce" args="() const " -->
real&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDroppingThreshold</b> () const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2775f0e4d22cd256324cfbca3eb6f775"></a><!-- doxytag: member="Seldon::IlutPreconditioning::GetDiagonalCoefficient" ref="a2775f0e4d22cd256324cfbca3eb6f775" args="() const " -->
real&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDiagonalCoefficient</b> () const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a43f08974f1c35ef7d3746aec7b83b5cc"></a><!-- doxytag: member="Seldon::IlutPreconditioning::GetPivotThreshold" ref="a43f08974f1c35ef7d3746aec7b83b5cc" args="() const " -->
real&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetPivotThreshold</b> () const </td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aeff500349169adb9f1ff9404c28b4df8"></a><!-- doxytag: member="Seldon::IlutPreconditioning::SetDroppingThreshold" ref="aeff500349169adb9f1ff9404c28b4df8" args="(real)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetDroppingThreshold</b> (real)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afe3f359b8f066faeda196654f83ae389"></a><!-- doxytag: member="Seldon::IlutPreconditioning::SetDiagonalCoefficient" ref="afe3f359b8f066faeda196654f83ae389" args="(real)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetDiagonalCoefficient</b> (real)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a46306fdb477fa9efe5f65db34c1d7fdc"></a><!-- doxytag: member="Seldon::IlutPreconditioning::SetPivotThreshold" ref="a46306fdb477fa9efe5f65db34c1d7fdc" args="(real)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetPivotThreshold</b> (real)</td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="aa97b0e2af8ee7b0daded1b9aecedabe1"></a><!-- doxytag: member="Seldon::IlutPreconditioning::FactorizeSymMatrix" ref="aa97b0e2af8ee7b0daded1b9aecedabe1" args="(const IVect &amp;perm, MatrixSparse &amp;mat, bool keep_matrix=false)" -->
template&lt;class MatrixSparse &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><b>FactorizeSymMatrix</b> (const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;perm, MatrixSparse &amp;mat, bool keep_matrix=false)</td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a335e2bc8325f03f8e32b5bfc674d9ee4"></a><!-- doxytag: member="Seldon::IlutPreconditioning::FactorizeUnsymMatrix" ref="a335e2bc8325f03f8e32b5bfc674d9ee4" args="(const IVect &amp;perm, MatrixSparse &amp;mat, bool keep_matrix=false)" -->
template&lt;class MatrixSparse &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><b>FactorizeUnsymMatrix</b> (const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;perm, MatrixSparse &amp;mat, bool keep_matrix=false)</td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="ab6883004e314d036fffa45f6cb244f62"></a><!-- doxytag: member="Seldon::IlutPreconditioning::FactorizeMatrix" ref="ab6883004e314d036fffa45f6cb244f62" args="(const IVect &amp;perm, Matrix&lt; T0, General, Storage0, Allocator0 &gt; &amp;mat, bool keep_matrix=false)" -->
template&lt;class T0 , class Storage0 , class Allocator0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><b>FactorizeMatrix</b> (const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;perm, <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T0, <a class="el" href="class_seldon_1_1_general.php">General</a>, Storage0, Allocator0 &gt; &amp;mat, bool keep_matrix=false)</td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a2b7c3eb39569b9f81c6c314b7675a915"></a><!-- doxytag: member="Seldon::IlutPreconditioning::FactorizeMatrix" ref="a2b7c3eb39569b9f81c6c314b7675a915" args="(const IVect &amp;perm, Matrix&lt; T0, Symmetric, Storage0, Allocator0 &gt; &amp;mat, bool keep_matrix=false)" -->
template&lt;class T0 , class Storage0 , class Allocator0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><b>FactorizeMatrix</b> (const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;perm, <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T0, <a class="el" href="class_seldon_1_1_symmetric.php">Symmetric</a>, Storage0, Allocator0 &gt; &amp;mat, bool keep_matrix=false)</td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="ad3cba7f7f6dd05005238553b0e858c32"></a><!-- doxytag: member="Seldon::IlutPreconditioning::TransSolve" ref="ad3cba7f7f6dd05005238553b0e858c32" args="(const Matrix1 &amp;A, const Vector1 &amp;r, Vector1 &amp;z)" -->
template&lt;class Matrix1 , class Vector1 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><b>TransSolve</b> (const Matrix1 &amp;A, const Vector1 &amp;r, Vector1 &amp;z)</td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a31731a31076cfbd907391e9637256110"></a><!-- doxytag: member="Seldon::IlutPreconditioning::Solve" ref="a31731a31076cfbd907391e9637256110" args="(const Matrix1 &amp;A, const Vector1 &amp;r, Vector1 &amp;z)" -->
template&lt;class Matrix1 , class Vector1 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><b>Solve</b> (const Matrix1 &amp;A, const Vector1 &amp;r, Vector1 &amp;z)</td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a10f497c50c5177f0cf974fc6de74c7e0"></a><!-- doxytag: member="Seldon::IlutPreconditioning::TransSolve" ref="a10f497c50c5177f0cf974fc6de74c7e0" args="(Vector1 &amp;z)" -->
template&lt;class Vector1 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><b>TransSolve</b> (Vector1 &amp;z)</td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a11f5247e543280181c4cc39e9f794cec"></a><!-- doxytag: member="Seldon::IlutPreconditioning::Solve" ref="a11f5247e543280181c4cc39e9f794cec" args="(Vector1 &amp;z)" -->
template&lt;class Vector1 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><b>Solve</b> (Vector1 &amp;z)</td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a11647ecc317646a9560944199040df92"></a><!-- doxytag: member="Seldon::IlutPreconditioning::print_level" ref="a11647ecc317646a9560944199040df92" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_ilut_preconditioning.php#a11647ecc317646a9560944199040df92">print_level</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Verbosity level. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a87e3208fe6fed093679b21210726be46"></a><!-- doxytag: member="Seldon::IlutPreconditioning::symmetric_algorithm" ref="a87e3208fe6fed093679b21210726be46" args="" -->
bool&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_ilut_preconditioning.php#a87e3208fe6fed093679b21210726be46">symmetric_algorithm</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">True if symmetric matrix is constructed. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a91394b185bd5974beb1f929ecf716e94"></a><!-- doxytag: member="Seldon::IlutPreconditioning::type_ilu" ref="a91394b185bd5974beb1f929ecf716e94" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_ilut_preconditioning.php#a91394b185bd5974beb1f929ecf716e94">type_ilu</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Type of incomplete factorization. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aec89b77ae63f96dba008a092da78af17"></a><!-- doxytag: member="Seldon::IlutPreconditioning::fill_level" ref="aec89b77ae63f96dba008a092da78af17" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_ilut_preconditioning.php#aec89b77ae63f96dba008a092da78af17">fill_level</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Maximum number of elements on a row of L or U. For Ilu(k), fill_level = k. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6330d5dc074f658ce57e9516c47e62c4"></a><!-- doxytag: member="Seldon::IlutPreconditioning::additional_fill" ref="a6330d5dc074f658ce57e9516c47e62c4" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_ilut_preconditioning.php#a6330d5dc074f658ce57e9516c47e62c4">additional_fill</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Additional number of elements for each row. This number is only used for ILUT(k). <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="accd555595ce35fcf2e2a2288c4585494"></a><!-- doxytag: member="Seldon::IlutPreconditioning::mbloc" ref="accd555595ce35fcf2e2a2288c4585494" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_ilut_preconditioning.php#accd555595ce35fcf2e2a2288c4585494">mbloc</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Size of block where the pivot is searched. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adf9a5b3757a33be59c6406c1515c92da"></a><!-- doxytag: member="Seldon::IlutPreconditioning::alpha" ref="adf9a5b3757a33be59c6406c1515c92da" args="" -->
real&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_ilut_preconditioning.php#adf9a5b3757a33be59c6406c1515c92da">alpha</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Diagonal compensation parameter (alpha = 0 -&gt; ILU, alpha = 1 -&gt; MILU). <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1a9cb311683ab176d463234521dd7cad"></a><!-- doxytag: member="Seldon::IlutPreconditioning::droptol" ref="a1a9cb311683ab176d463234521dd7cad" args="" -->
real&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_ilut_preconditioning.php#a1a9cb311683ab176d463234521dd7cad">droptol</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Threshold used for dropping small terms. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af02ed9cb6916f9cb848ad774b7686d94"></a><!-- doxytag: member="Seldon::IlutPreconditioning::permtol" ref="af02ed9cb6916f9cb848ad774b7686d94" args="" -->
real&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_ilut_preconditioning.php#af02ed9cb6916f9cb848ad774b7686d94">permtol</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Threshold for pivoting. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a81f338ca2bcf311a3fb70d4086512632"></a><!-- doxytag: member="Seldon::IlutPreconditioning::permutation_row" ref="a81f338ca2bcf311a3fb70d4086512632" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">IVect</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_ilut_preconditioning.php#a81f338ca2bcf311a3fb70d4086512632">permutation_row</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Permutation arrays. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aff9a99eb5da4f4f93ab9696b339569d6"></a><!-- doxytag: member="Seldon::IlutPreconditioning::permutation_col" ref="aff9a99eb5da4f4f93ab9696b339569d6" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">IVect</a>&nbsp;</td><td class="memItemRight" valign="bottom"><b>permutation_col</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a450694545524ae93de1df9810b135b91"></a><!-- doxytag: member="Seldon::IlutPreconditioning::mat_sym" ref="a450694545524ae93de1df9810b135b91" args="" -->
<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; cplx, <a class="el" href="class_seldon_1_1_symmetric.php">Symmetric</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_array_row_sym_sparse.php">ArrayRowSymSparse</a>, Allocator &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_ilut_preconditioning.php#a450694545524ae93de1df9810b135b91">mat_sym</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight"><a class="el" href="class_seldon_1_1_symmetric.php">Symmetric</a> matrix. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8e30b497ecb4698bb22d1c0c3ea19f7e"></a><!-- doxytag: member="Seldon::IlutPreconditioning::mat_unsym" ref="a8e30b497ecb4698bb22d1c0c3ea19f7e" args="" -->
<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; cplx, <a class="el" href="class_seldon_1_1_general.php">General</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_array_row_sparse.php">ArrayRowSparse</a>, Allocator &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_ilut_preconditioning.php#a8e30b497ecb4698bb22d1c0c3ea19f7e">mat_unsym</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Unsymmetric matrix. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad57d0e7c6dc5468f6c8fbf032e0a9327"></a><!-- doxytag: member="Seldon::IlutPreconditioning::xtmp" ref="ad57d0e7c6dc5468f6c8fbf032e0a9327" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; cplx, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_ilut_preconditioning.php#ad57d0e7c6dc5468f6c8fbf032e0a9327">xtmp</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Temporary vector. <br/></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class real, class cplx, class Allocator = SELDON_DEFAULT_ALLOCATOR&lt;cplx&gt;&gt;<br/>
 class Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</h3>


<p>Definition at line <a class="el" href="_ilut_preconditioning_8hxx_source.php#l00027">27</a> of file <a class="el" href="_ilut_preconditioning_8hxx_source.php">IlutPreconditioning.hxx</a>.</p>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>computation/solver/preconditioner/<a class="el" href="_ilut_preconditioning_8hxx_source.php">IlutPreconditioning.hxx</a></li>
<li>computation/solver/preconditioner/<a class="el" href="_ilut_preconditioning_8cxx_source.php">IlutPreconditioning.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
