<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;" --><!-- doxytag: inherits="Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;" -->
<p>Row-major sparse-matrix class.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_matrix___array_complex_sparse_8hxx_source.php">Matrix_ArrayComplexSparse.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.png" usemap="#Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;_map" alt=""/>
  <map id="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;_map" name="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;_map">
<area href="class_seldon_1_1_matrix___array_complex_sparse.php" alt="Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;" shape="rect" coords="0,0,501,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="abc292cb50ef1801f9264bcd9cff9c82d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::value_type" ref="abc292cb50ef1801f9264bcd9cff9c82d" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1b063d328d4212a686dc8ea4d055e8b9"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::property" ref="a1b063d328d4212a686dc8ea4d055e8b9" args="" -->
typedef Prop&nbsp;</td><td class="memItemRight" valign="bottom"><b>property</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8e0d35fc5ec664f0221ead1bddd6ad18"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::storage" ref="a8e0d35fc5ec664f0221ead1bddd6ad18" args="" -->
typedef <a class="el" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>&nbsp;</td><td class="memItemRight" valign="bottom"><b>storage</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac8e6a7fbf8d40b258439d2768f970b91"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::allocator" ref="ac8e6a7fbf8d40b258439d2768f970b91" args="" -->
typedef Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>allocator</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5325ffba5ca6253c1df46e37283142d0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::pointer" ref="a5325ffba5ca6253c1df46e37283142d0" args="" -->
typedef Allocator::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a87436f000442ff05b2fc557a77dae13c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::const_pointer" ref="a87436f000442ff05b2fc557a77dae13c" args="" -->
typedef Allocator::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1ab28bb0ce6a3c1bba3b7680b0d2f6a2"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::reference" ref="a1ab28bb0ce6a3c1bba3b7680b0d2f6a2" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a963d0f91721a1d53fb2b4d5bc322abb8"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::const_reference" ref="a963d0f91721a1d53fb2b4d5bc322abb8" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2b4d88a19897b82ba5d8a37198cf4426"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::entry_type" ref="a2b4d88a19897b82ba5d8a37198cf4426" args="" -->
typedef complex&lt; T &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>entry_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a55a8cb7922d8fc8bbacd2b5548b03359"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::access_type" ref="a55a8cb7922d8fc8bbacd2b5548b03359" args="" -->
typedef complex&lt; T &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>access_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aaa7b45cfa45845f340f819e31f877f34"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::const_access_type" ref="aaa7b45cfa45845f340f819e31f877f34" args="" -->
typedef complex&lt; T &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_access_type</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#ab98aecad772f00d59ea17248a04720d3">Matrix</a> ()  throw ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#ab98aecad772f00d59ea17248a04720d3"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a8bcc8c567a536ff51cba0d9afce34507">Matrix</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Constructor.  <a href="#a8bcc8c567a536ff51cba0d9afce34507"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a480f759c9bdd3e69206f18d81ed44f5a"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::ClearRealRow" ref="a480f759c9bdd3e69206f18d81ed44f5a" args="(int i)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a480f759c9bdd3e69206f18d81ed44f5a">ClearRealRow</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears a row. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a16289d6a8f38a4303f75113667f8af74"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::ClearImagRow" ref="a16289d6a8f38a4303f75113667f8af74" args="(int i)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a16289d6a8f38a4303f75113667f8af74">ClearImagRow</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears a row. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#aefd387926aad7c3609a9f6bc062252e0">ReallocateRealRow</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Changes the size of a row.  <a href="#aefd387926aad7c3609a9f6bc062252e0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#aa798b00f5793c414777a5e3b421095a0">ReallocateImagRow</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Changes the size of a row.  <a href="#aa798b00f5793c414777a5e3b421095a0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#ac668aac85ed864892bd3f4209ced4cf2">ResizeRealRow</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Changes the size of a row.  <a href="#ac668aac85ed864892bd3f4209ced4cf2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#af89faca49416df1ad0b4262d440adec9">ResizeImagRow</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Changes the size of a row.  <a href="#af89faca49416df1ad0b4262d440adec9"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a8ae984b95031fd72c6ecab86418d60ac">SwapRealRow</a> (int i, int i_)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Swaps two rows.  <a href="#a8ae984b95031fd72c6ecab86418d60ac"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a4b6b31fa7468c32bddf17fd1a22888ae">SwapImagRow</a> (int i, int i_)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Swaps two rows.  <a href="#a4b6b31fa7468c32bddf17fd1a22888ae"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a350b6c00041f9e9db6bf111ac431bdf9">ReplaceRealIndexRow</a> (int i, <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;new_index)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets column numbers of non-zero entries of a row.  <a href="#a350b6c00041f9e9db6bf111ac431bdf9"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a3e6d41ebaccab9d346d6fd291e28ff5d">ReplaceImagIndexRow</a> (int i, <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;new_index)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets column numbers of non-zero entries of a row.  <a href="#a3e6d41ebaccab9d346d6fd291e28ff5d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a794411e243ead33b91cb750921602cdd">GetRealRowSize</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of non-zero entries of a row.  <a href="#a794411e243ead33b91cb750921602cdd"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a5477a0086e76fbee8db94fcfa1cd9703">GetImagRowSize</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of non-zero entries of a row.  <a href="#a5477a0086e76fbee8db94fcfa1cd9703"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aeeca29af2514e61e003925875e7f7c33"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::PrintRealRow" ref="aeeca29af2514e61e003925875e7f7c33" args="(int i) const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#aeeca29af2514e61e003925875e7f7c33">PrintRealRow</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays non-zero values of a row. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aebbf5322f0709878e15c379391a8aec8"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::PrintImagRow" ref="aebbf5322f0709878e15c379391a8aec8" args="(int i) const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#aebbf5322f0709878e15c379391a8aec8">PrintImagRow</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays non-zero values of a row. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a70fa643e503c063be37de261f89aab22">AssembleRealRow</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Assembles a row.  <a href="#a70fa643e503c063be37de261f89aab22"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a48ce7e0631521cc72c564be088917148">AssembleImagRow</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Assembles a row.  <a href="#a48ce7e0631521cc72c564be088917148"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#ab2d2137e557ad1acd76fed600f5c3584">AddInteraction</a> (int i, int j, const complex&lt; T &gt; &amp;val)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds a coefficient in the matrix.  <a href="#ab2d2137e557ad1acd76fed600f5c3584"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Alloc1 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a445de4f86d354e07251b8a9da3eff045">AddInteractionRow</a> (int i, int nb, const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;col, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; complex&lt; T &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1 &gt; &amp;val)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds coefficients in a row.  <a href="#a445de4f86d354e07251b8a9da3eff045"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Alloc1 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a3a9584691f4c544d683819e70202db20">AddInteractionColumn</a> (int i, int nb, const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;row, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; complex&lt; T &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1 &gt; &amp;val)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds coefficients in a column.  <a href="#a3a9584691f4c544d683819e70202db20"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0ee918c8c04a1a9434e157c15fdeb596"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::Clear" ref="a0ee918c8c04a1a9434e157c15fdeb596" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Clear</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad2348fb25206ef8516c7e835b7ce2287"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::Reallocate" ref="ad2348fb25206ef8516c7e835b7ce2287" args="(int i, int j)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Reallocate</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9166689334cc2f731220f76b730d692f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::Resize" ref="a9166689334cc2f731220f76b730d692f" args="(int i, int j)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Resize</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a35ea621d2e5351259ce5bca87febc821"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::GetM" ref="a35ea621d2e5351259ce5bca87febc821" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetM</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab3c6aad66c9dc6ad9addfb7de9a4ebdd"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::GetM" ref="ab3c6aad66c9dc6ad9addfb7de9a4ebdd" args="(const SeldonTranspose &amp;status) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetM</b> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a> &amp;status) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae413915ed95545b40df8b43737fa43c1"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::GetN" ref="ae413915ed95545b40df8b43737fa43c1" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetN</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a53e20518a8557211c89ca8ad0d607825"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::GetN" ref="a53e20518a8557211c89ca8ad0d607825" args="(const SeldonTranspose &amp;status) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetN</b> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a> &amp;status) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a11efc1bee6a95da705238628d7e3d958"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::GetRealNonZeros" ref="a11efc1bee6a95da705238628d7e3d958" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetRealNonZeros</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a33ebac042eda90796d7f674c250e85be"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::GetImagNonZeros" ref="a33ebac042eda90796d7f674c250e85be" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetImagNonZeros</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa40775992c738c7fd1e9543bfddb2694"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::GetRealDataSize" ref="aa40775992c738c7fd1e9543bfddb2694" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetRealDataSize</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad54321edab8e2633985e198f1a576340"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::GetImagDataSize" ref="ad54321edab8e2633985e198f1a576340" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetImagDataSize</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2e5c6c10af220eea29f9c1565b14a93f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::GetDataSize" ref="a2e5c6c10af220eea29f9c1565b14a93f" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataSize</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afae45969eda952b43748cf7756f0e6d6"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::GetRealInd" ref="afae45969eda952b43748cf7756f0e6d6" args="(int i) const" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetRealInd</b> (int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0a423d7e93a0ad9c19ed6de2b1052dca"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::GetImagInd" ref="a0a423d7e93a0ad9c19ed6de2b1052dca" args="(int i) const" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetImagInd</b> (int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aaa912365c7b8c191cf8b3fb2e2da4789"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::GetRealData" ref="aaa912365c7b8c191cf8b3fb2e2da4789" args="(int i) const" -->
T *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetRealData</b> (int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1d33377770e234c75aae39bed6f996ff"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::GetRealData" ref="a1d33377770e234c75aae39bed6f996ff" args="() const" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt; *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetRealData</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a578281c64cbe05a542a6b0e9642b326a"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::GetImagData" ref="a578281c64cbe05a542a6b0e9642b326a" args="(int i) const" -->
T *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetImagData</b> (int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac3940bc9993d64730333c83be0f839ce"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::GetImagData" ref="ac3940bc9993d64730333c83be0f839ce" args="() const" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt; *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetImagData</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a30be6306dbdd79b0a282ef99289b0c2b"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::operator()" ref="a30be6306dbdd79b0a282ef99289b0c2b" args="(int i, int j) const" -->
complex&lt; T &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>operator()</b> (int i, int j) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7eb05d6902b13cfb7cc4f17093fdc17e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::Val" ref="a7eb05d6902b13cfb7cc4f17093fdc17e" args="(int i, int j)" -->
complex&lt; T &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>Val</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a994fbc16dd258d1609f7c7c582205e9f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::Val" ref="a994fbc16dd258d1609f7c7c582205e9f" args="(int i, int j) const" -->
const complex&lt; T &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>Val</b> (int i, int j) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a041c76033f939293aa871f2fe45ef22a"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::ValueReal" ref="a041c76033f939293aa871f2fe45ef22a" args="(int num_row, int i) const" -->
const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>ValueReal</b> (int num_row, int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a11fe8648ddd3e6e339eb81735d7e544e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::ValueReal" ref="a11fe8648ddd3e6e339eb81735d7e544e" args="(int num_row, int i)" -->
T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>ValueReal</b> (int num_row, int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2a3cd76154ac61a43afa42563805ce3b"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::IndexReal" ref="a2a3cd76154ac61a43afa42563805ce3b" args="(int num_row, int i) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>IndexReal</b> (int num_row, int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2fdb9482dad6378fb8ced1982b6860b1"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::IndexReal" ref="a2fdb9482dad6378fb8ced1982b6860b1" args="(int num_row, int i)" -->
int &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>IndexReal</b> (int num_row, int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa2d9a2e69a269bf9d15b62d520cb560d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::ValueImag" ref="aa2d9a2e69a269bf9d15b62d520cb560d" args="(int num_row, int i) const" -->
const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>ValueImag</b> (int num_row, int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab410f72a305938081fbc07e868eb161b"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::ValueImag" ref="ab410f72a305938081fbc07e868eb161b" args="(int num_row, int i)" -->
T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>ValueImag</b> (int num_row, int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aedba64fa9b01bce6bb75e9e5a14b26c5"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::IndexImag" ref="aedba64fa9b01bce6bb75e9e5a14b26c5" args="(int num_row, int i) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>IndexImag</b> (int num_row, int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a011b15c1909f712a693de10f2c0c2328"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::IndexImag" ref="a011b15c1909f712a693de10f2c0c2328" args="(int num_row, int i)" -->
int &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>IndexImag</b> (int num_row, int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aab6c31240cd6dcbd442f9a29658f51ea"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::SetRealData" ref="aab6c31240cd6dcbd442f9a29658f51ea" args="(int, int, Vector&lt; T, VectSparse, Allocator &gt; *)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetRealData</b> (int, int, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; *)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1ebb3dcbf317ec99e6463f0aafea10b9"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::SetRealData" ref="a1ebb3dcbf317ec99e6463f0aafea10b9" args="(int, int, T *, int *)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetRealData</b> (int, int, T *, int *)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8e3c2dfa0ea5aa57cd8607cca15562f2"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::SetImagData" ref="a8e3c2dfa0ea5aa57cd8607cca15562f2" args="(int, int, Vector&lt; T, VectSparse, Allocator &gt; *)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetImagData</b> (int, int, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; *)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a50622f29a8e112821c68e5a28236258f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::SetImagData" ref="a50622f29a8e112821c68e5a28236258f" args="(int, int, T *, int *)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetImagData</b> (int, int, T *, int *)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9f0922d865cdd4a627912583a9c73151"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::NullifyReal" ref="a9f0922d865cdd4a627912583a9c73151" args="(int i)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>NullifyReal</b> (int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aed1192a75fe2c7d19e545ff6054298e2"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::NullifyReal" ref="aed1192a75fe2c7d19e545ff6054298e2" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>NullifyReal</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad5e87ca37baabfb8cdc6f841f5125d7d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::NullifyImag" ref="ad5e87ca37baabfb8cdc6f841f5125d7d" args="(int i)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>NullifyImag</b> (int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adbdb03e02fd8617a23188d6bc3155ec0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::NullifyImag" ref="adbdb03e02fd8617a23188d6bc3155ec0" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>NullifyImag</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac24abb2e047eb8b91728023074b3fb13"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::Print" ref="ac24abb2e047eb8b91728023074b3fb13" args="() const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Print</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7095c684ecdb9ef6e9fe9b84462e22ce"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::WriteText" ref="a7095c684ecdb9ef6e9fe9b84462e22ce" args="(string FileName) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>WriteText</b> (string FileName) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a884e62c07aa29b59b259d232f7771c94"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::WriteText" ref="a884e62c07aa29b59b259d232f7771c94" args="(ostream &amp;FileStream) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>WriteText</b> (ostream &amp;FileStream) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a889d3baa1c4aa6a96b83ba8ca7b4cf4a"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::Assemble" ref="a889d3baa1c4aa6a96b83ba8ca7b4cf4a" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Assemble</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa9a81eead891c0ee85f2563dc8be8a99"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::RemoveSmallEntry" ref="aa9a81eead891c0ee85f2563dc8be8a99" args="(const T0 &amp;epsilon)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>RemoveSmallEntry</b> (const T0 &amp;epsilon)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a19b48691b03953dffd387ace5cfddd37"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::SetIdentity" ref="a19b48691b03953dffd387ace5cfddd37" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetIdentity</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa6f51cb3c7779c309578058fd34c5d25"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::Zero" ref="aa6f51cb3c7779c309578058fd34c5d25" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Zero</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1fce8c713bc87ca4e06df819ced39554"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::Fill" ref="a1fce8c713bc87ca4e06df819ced39554" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Fill</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a08a5f1de809bbb4565e90d954ca053f6"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::Fill" ref="a08a5f1de809bbb4565e90d954ca053f6" args="(const complex&lt; T0 &gt; &amp;x)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Fill</b> (const complex&lt; T0 &gt; &amp;x)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad1ff9883f030ca9eaada43616865acdb"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::FillRand" ref="ad1ff9883f030ca9eaada43616865acdb" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>FillRand</b> ()</td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2032848b78edab7ef6721eda9e2784de"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::m_" ref="a2032848b78edab7ef6721eda9e2784de" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de">m_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of rows. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acfa7a2c6c7f77dc8ebcee2dd411ebf98"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::n_" ref="acfa7a2c6c7f77dc8ebcee2dd411ebf98" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98">n_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of columns. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af1766a47a3d45c0b904b07870204e4a6"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::val_real_" ref="af1766a47a3d45c0b904b07870204e4a6" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6">val_real_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">real part rows or columns <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afa77c763d3b3a280dfef2a4410ed0bd8"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::val_imag_" ref="afa77c763d3b3a280dfef2a4410ed0bd8" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8">val_imag_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">imaginary part rows or columns <br/></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Prop, class Allocator&gt;<br/>
 class Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</h3>

<p>Row-major sparse-matrix class. </p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8hxx_source.php#l00186">186</a> of file <a class="el" href="_matrix___array_complex_sparse_8hxx_source.php">Matrix_ArrayComplexSparse.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="ab98aecad772f00d59ea17248a04720d3"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::Matrix" ref="ab98aecad772f00d59ea17248a04720d3" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td>  throw ()<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>Builds an empty matrix. </p>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01234">1234</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a8bcc8c567a536ff51cba0d9afce34507"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::Matrix" ref="a8bcc8c567a536ff51cba0d9afce34507" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Constructor. </p>
<p>Builds a i by j matrix </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> values are not initialized. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01247">1247</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="ab2d2137e557ad1acd76fed600f5c3584"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::AddInteraction" ref="ab2d2137e557ad1acd76fed600f5c3584" args="(int i, int j, const complex&lt; T &gt; &amp;val)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>, Allocator &gt;::AddInteraction </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const complex&lt; T &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds a coefficient in the matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>coefficient to add. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01449">1449</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3a9584691f4c544d683819e70202db20"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::AddInteractionColumn" ref="a3a9584691f4c544d683819e70202db20" args="(int i, int nb, const IVect &amp;row, const Vector&lt; complex&lt; T &gt;, VectFull, Alloc1 &gt; &amp;val)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Alloc1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>, Allocator &gt;::AddInteractionColumn </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nb</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>row</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; complex&lt; T &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds coefficients in a column. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>nb</em>&nbsp;</td><td>number of coefficients to add. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>row</em>&nbsp;</td><td>row numbers of coefficients. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>values of coefficients. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01509">1509</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a445de4f86d354e07251b8a9da3eff045"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::AddInteractionRow" ref="a445de4f86d354e07251b8a9da3eff045" args="(int i, int nb, const IVect &amp;col, const Vector&lt; complex&lt; T &gt;, VectFull, Alloc1 &gt; &amp;val)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Alloc1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>, Allocator &gt;::AddInteractionRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nb</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>col</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; complex&lt; T &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds coefficients in a row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>nb</em>&nbsp;</td><td>number of coefficients to add. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>col</em>&nbsp;</td><td>column numbers of coefficients. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>values of coefficients. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01468">1468</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a48ce7e0631521cc72c564be088917148"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::AssembleImagRow" ref="a48ce7e0631521cc72c564be088917148" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>, Allocator &gt;::AssembleImagRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Assembles a row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>If you are using the methods AddInteraction, you don't need to call that method. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01435">1435</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a70fa643e503c063be37de261f89aab22"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::AssembleRealRow" ref="a70fa643e503c063be37de261f89aab22" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>, Allocator &gt;::AssembleRealRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Assembles a row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>If you are using the methods AddInteraction, you don't need to call that method. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01422">1422</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5477a0086e76fbee8db94fcfa1cd9703"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::GetImagRowSize" ref="a5477a0086e76fbee8db94fcfa1cd9703" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>, Allocator &gt;::GetImagRowSize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of non-zero entries of a row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of non-zero entries of the row i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01393">1393</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a794411e243ead33b91cb750921602cdd"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::GetRealRowSize" ref="a794411e243ead33b91cb750921602cdd" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>, Allocator &gt;::GetRealRowSize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of non-zero entries of a row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of non-zero entries of the row i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01381">1381</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa798b00f5793c414777a5e3b421095a0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::ReallocateImagRow" ref="aa798b00f5793c414777a5e3b421095a0" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>, Allocator &gt;::ReallocateImagRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Changes the size of a row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>new number of non-zero entries of the row. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Data may be lost. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01291">1291</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aefd387926aad7c3609a9f6bc062252e0"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::ReallocateRealRow" ref="aefd387926aad7c3609a9f6bc062252e0" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>, Allocator &gt;::ReallocateRealRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Changes the size of a row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>new number of non-zero entries of the row. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Data may be lost. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01277">1277</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3e6d41ebaccab9d346d6fd291e28ff5d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::ReplaceImagIndexRow" ref="a3e6d41ebaccab9d346d6fd291e28ff5d" args="(int i, IVect &amp;new_index)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>, Allocator &gt;::ReplaceImagIndexRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>new_index</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets column numbers of non-zero entries of a row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>new_index</em>&nbsp;</td><td>new column numbers. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01368">1368</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a350b6c00041f9e9db6bf111ac431bdf9"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::ReplaceRealIndexRow" ref="a350b6c00041f9e9db6bf111ac431bdf9" args="(int i, IVect &amp;new_index)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>, Allocator &gt;::ReplaceRealIndexRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>new_index</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets column numbers of non-zero entries of a row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>new_index</em>&nbsp;</td><td>new column numbers. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01354">1354</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af89faca49416df1ad0b4262d440adec9"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::ResizeImagRow" ref="af89faca49416df1ad0b4262d440adec9" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>, Allocator &gt;::ResizeImagRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Changes the size of a row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>new number of non-zero entries of the row. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Data is kept. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01317">1317</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac668aac85ed864892bd3f4209ced4cf2"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::ResizeRealRow" ref="ac668aac85ed864892bd3f4209ced4cf2" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>, Allocator &gt;::ResizeRealRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Changes the size of a row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>new number of non-zero entries of the row. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Data is kept. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01304">1304</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a4b6b31fa7468c32bddf17fd1a22888ae"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::SwapImagRow" ref="a4b6b31fa7468c32bddf17fd1a22888ae" args="(int i, int i_)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>, Allocator &gt;::SwapImagRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Swaps two rows. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>first row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>second row number. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01341">1341</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a8ae984b95031fd72c6ecab86418d60ac"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;::SwapRealRow" ref="a8ae984b95031fd72c6ecab86418d60ac" args="(int i, int i_)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>, Allocator &gt;::SwapRealRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Swaps two rows. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>first row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>second row number. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php#l01329">1329</a> of file <a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>matrix_sparse/<a class="el" href="_matrix___array_complex_sparse_8hxx_source.php">Matrix_ArrayComplexSparse.hxx</a></li>
<li>matrix_sparse/<a class="el" href="_matrix___array_complex_sparse_8cxx_source.php">Matrix_ArrayComplexSparse.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
