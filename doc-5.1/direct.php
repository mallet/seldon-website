<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Direct Solvers </h1>  </div>
</div>
<div class="contents">
<p>Seldon is interfaced with libraries performing the direct resolution of very large sparse linear systems : <b><a href="http://mumps.enseeiht.fr/">MUMPS</a></b>, <b><a href="http://crd.lbl.gov/~xiaoye/SuperLU/">SuperLU</a></b> and <b><a href="http://www.cise.ufl.edu/research/sparse/umfpack/">UmfPack</a></b>. An example file is located in test/program/direct_test.cpp. If you want to test the interface with <b>MUMPS</b>, assuming that MUMPS has been compiled in directory <code>MumpsDir</code>, you can compile this file by typing :</p>
<pre class="fragment">g++ -DSELDON_WITH_MUMPS direct_test.cpp -IMumpsDir/include -IMumpsDir/libseq \
  -IMetisDir/Lib -LMumpsDir/lib -ldmumps -lzmumps -lmumps_common -lpord \
  -LMumpsDir/libseq -lmpiseq -LScotchDir/lib -lesmumps -lfax -lsymbol \
  -ldof -lorder -lgraph -lscotch -lscotcherr -lcommon \
  -lgfortran -lm -lpthread -LMetisDir -lmetis -llapack -lblas
</pre><p>You can simplify the last command, if you didn't install <a href="http://glaros.dtc.umn.edu/gkhome/views/metis/">Metis</a> and <a href="http://www.labri.fr/perso/pelegrin/scotch/">Scotch</a> and didn't compile MUMPS with those libraries. For <b>UmfPack</b>, if <code>UmfPackDir</code> denotes the directory where UmfPack has been installed, you have to type :</p>
<pre class="fragment">g++ -DSELDON_WITH_UMFPACK direct_test.cpp
  -IUmfPackDir/AMD/Include -IUmfPackDir/UMFPACK/Include \
  -IUmfPackDir/UMFPACK/UFconfig -LUmfPackDir/UMFPACK/Lib \
  -lumfpack -LUmfPackDir/AMD/Lib -lamd -llapack -lblas</pre><p>For <b>SuperLU</b>, the compilation line reads (<code>SuperLUdir</code> is the directory where SuperLU is located) :</p>
<pre class="fragment">g++ -DSELDON_WITH_SUPERLU direct_test.cpp -ISuperLUdir/SRC -LSuperLUdir -lsuperlu</pre><p>All in all, <b>MUMPS</b> seems more efficient, and includes more functionnalities than the other libraries.</p>
<h2>Syntax</h2>
<p>The syntax of all direct solvers is similar </p>
<pre class="syntax-box">
void GetLU(Matrix&amp;, MatrixMumps&amp;);
void SolveLU(MatrixMumps&amp;, Vector&amp;);
</pre><p>The interface has been done only for double precision (real or complex numbers), since single precision is not accurate enough when very large sparse linear systems are solved.</p>
<h2>Basic use</h2>
<p>We provide an example of direct resolution using SuperLU.</p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// first we construct a sparse matrix
int n = 1000; // size of linear system
// we assume that you know how to fill arrays values, ptr, ind
Matrix&lt;double, General, RowSparse&gt; A(n, n, nnz, values, ptr, ind);

// then we declare vectors
Vector&lt;double&gt; b(n), x(n);

// you fill right-hand side
b.Fill();

// you perform the factorization (real matrix)
MatrixSuperLU&lt;double&gt; mat_lu;
GetLU(A, mat_lu);

// then you can solve as many linear systems as you want
x = b;
SolveLU(mat_lu, x);

</pre></div>  </pre><h2>Methods of MatrixMumps :</h2>
<table  class="category-table">
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#clear">Clear </a> </td><td class="category-table-td">Releases memory used by factorization   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#initsymmetricmatrix">InitSymmetricMatrix</a> </td><td class="category-table-td">Informs the solver that the matrix is symmetric   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#initsymmetricmatrix">InitUnSymmetricMatrix</a> </td><td class="category-table-td">Informs the solver that the matrix is unsymmetric   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#selectordering">SelectOrdering</a> </td><td class="category-table-td">selects an ordering scheme  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#messages">HideMessages</a> </td><td class="category-table-td">requires to the direct solver to not display any message  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#messages">ShowMessages</a> </td><td class="category-table-td">requires a normal display by the direct solver  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getinfofactorization">GetInfoFactorization</a> </td><td class="category-table-td">returns the error code generated by the factorization  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#findordering">FindOrdering</a> </td><td class="category-table-td">computes a new ordering of rows and columns  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#factorizematrix">FactorizeMatrix</a> </td><td class="category-table-td">performs LU factorization  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getschurmatrix">GetSchurMatrix</a> </td><td class="category-table-td">forms Schur complement  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#solve">Solve</a> </td><td class="category-table-td">uses LU factorization to solve a linear system  </td></tr>
</table>
<h2>Methods of MatrixUmfPack :</h2>
<table  class="category-table">
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#clear">Clear </a> </td><td class="category-table-td">Releases memory used by factorization   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#messages">HideMessages</a> </td><td class="category-table-td">requires to the direct solver to not display any message  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#messages">ShowMessages</a> </td><td class="category-table-td">requires a normal display by the direct solver  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getinfofactorization">GetInfoFactorization</a> </td><td class="category-table-td">returns the error code generated by the factorization  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#factorizematrix">FactorizeMatrix</a> </td><td class="category-table-td">performs LU factorization  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#solve">Solve</a> </td><td class="category-table-td">uses LU factorization to solve a linear system  </td></tr>
</table>
<h2>Methods of MatrixSuperLU :</h2>
<table  class="category-table">
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#clear">Clear </a> </td><td class="category-table-td">Releases memory used by factorization   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#messages">HideMessages</a> </td><td class="category-table-td">requires to the direct solver to not display any message  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#messages">ShowMessages</a> </td><td class="category-table-td">requires a normal display by the direct solver  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getinfofactorization">GetInfoFactorization</a> </td><td class="category-table-td">returns the error code generated by the factorization  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#factorizematrix">FactorizeMatrix</a> </td><td class="category-table-td">performs LU factorization  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#solve">Solve</a> </td><td class="category-table-td">uses LU factorization to solve a linear system  </td></tr>
</table>
<h2>Functions :</h2>
<table  class="category-table">
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getlu">GetLU </a> </td><td class="category-table-td">performs a LU factorization   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#solvelu">SolveLU</a> </td><td class="category-table-td">uses LU factorization to solve a linear system  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getschurmatrix_func">GetSchurMatrix</a> </td><td class="category-table-td">forms Schur complement  </td></tr>
</table>
<div class="separator"><a class="anchor" id="clear"></a></div><h3>Clear</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void Clear();
</pre><p>This method releases the memory used by the factorisation. It is available for every direct solver. A call to that method is necessary before asking for a new factorization.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double, General, ArrayRowSparse&gt; A;
MatrixUmfPack&lt;double&gt; mat_lu;
// you fill A as you want
// then a first factorization is achieved
GetLU(A, mat_lu);
// you exploit this factorization
// you need to clear LU matrix before doing another factorization
mat_lu.Clear();
// you fill A with other values
// and refactorize
GetLU(A, mat_lu);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_mumps_8cxx_source.php">Mumps.cxx</a><br/>
 <a class="el" href="_umf_pack_8cxx_source.php">UmfPack.cxx</a><br/>
 <a class="el" href="_super_l_u_8cxx_source.php">SuperLU.cxx</a></p>
<div class="separator"><a class="anchor" id="initsymmetricmatrix"></a></div><h3>InitSymmetricMatrix, InitUnSymmetricMatrix (only for MatrixMumps)</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void InitSymmetricMatrix();
  void InitUnSymmetricMatrix();
</pre><p>These methods are used before <a href="#factorizematrix">FactorizeMatrix</a> so that Mumps knows that the input matrix is symmetric or not. UmfPack and SuperLU don't have that kind of method since they don't have specific algorithms for symmetric matrices. If you are using <a href="#getlu">GetLU</a> and <a href="#solvelu">SolveLU</a>, you don't need to call those methods.</p>
<h4>Location :</h4>
<p><a class="el" href="_mumps_8cxx_source.php">Mumps.cxx</a></p>
<div class="separator"><a class="anchor" id="selectordering"></a></div><h3>SelectOrdering (only for MatrixMumps) </h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void SelectOrdering(int);
</pre><p>You can force a specific ordering scheme to be used (Metis, Amd, Scotch, etc). In the documentation of Mumps, you can find a list of available orderings and the associated code.</p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// you fill a sparse matrix
Matrix&lt;double, General, ArrayRowSparse&gt; A;
// you declare a Mumps structure
MatrixMumps&lt;double&gt; mat_lu;
// you change the default ordering scheme
mat_lu.SelectOrdering(2);
// then you can use getlu as usual
GetLU(A, mat_lu);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_mumps_8cxx_source.php">Mumps.cxx</a></p>
<div class="separator"><a class="anchor" id="findordering"></a></div><h3>FindOrdering (only for MatrixMumps) </h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void FindOrdering(Matrix&amp;, Vector&lt;int&gt;&amp;);
  void FindOrdering(Matrix&amp;, Vector&lt;int&gt;&amp;, bool);
</pre><p>This method computes the new row numbers of the matrix by using available algorithms in Mumps. </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// you fill a sparse matrix
Matrix&lt;double, General, ArrayRowSparse&gt; A;
// you declare a Mumps structure
MatrixMumps&lt;double&gt; mat_lu;
IVect permutation;
// we find the best numbering of A 
// by default, the matrix A is erased
mat_lu.FindOrdering(A, permutation);
// if last argument is true, A is not modified
mat_lu.FindOrdering(A, permutation, true);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_mumps_8cxx_source.php">Mumps.cxx</a></p>
<div class="separator"><a class="anchor" id="messages"></a></div><h3>ShowMessages, HideMessages </h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void ShowMessages();
  void HideMessages();
</pre><p><code>ShowMessages</code> allows the direct solver to display informations about the factorization and resolution phases, while <code>HideMessages</code> forbids any message to be displayed.</p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// you fill a sparse matrix
Matrix&lt;double, General, ArrayRowSparse&gt; A;
// you declare a Mumps structure
MatrixMumps&lt;double&gt; mat_lu;
// then before GetLU, you can require silencious mode
mat_lu.HideMessages();
GetLU(A, mat_lu);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_mumps_8cxx_source.php">Mumps.cxx</a><br/>
 <a class="el" href="_umf_pack_8cxx_source.php">UmfPack.cxx</a><br/>
 <a class="el" href="_super_l_u_8cxx_source.php">SuperLU.cxx</a></p>
<div class="separator"><a class="anchor" id="getinfofactorization"></a></div><h3>GetInfoFactorization </h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void GetInfoFactorization();
</pre><p>This method returns the error code provided by the used direct solver. </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// you fill a sparse matrix
Matrix&lt;double, General, ArrayRowSparse&gt; A;
// you declare a Mumps structure
MatrixMumps&lt;double&gt; mat_lu;
// you factorize the matrix
GetLU(A, mat_lu);
// to know if there is a problem
int info = mat_lu.GetInfoFactorization();
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_mumps_8cxx_source.php">Mumps.cxx</a><br/>
 <a class="el" href="_umf_pack_8cxx_source.php">UmfPack.cxx</a><br/>
 <a class="el" href="_super_l_u_8cxx_source.php">SuperLU.cxx</a></p>
<div class="separator"><a class="anchor" id="factorizematrix"></a></div><h3>FactorizeMatrix </h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void FactorizeMatrix(Matrix&amp;);
  void FactorizeMatrix(Matrix&amp;, bool);
</pre><p>This method factorizes the given matrix. It is better to use <code>GetLU</code>, since <code>FactorizeMatrix</code> needs that <code>InitSymmetricMatrix</code> is called before (for Mumps).</p>
<h4>Location :</h4>
<p><a class="el" href="_mumps_8cxx_source.php">Mumps.cxx</a><br/>
 <a class="el" href="_umf_pack_8cxx_source.php">UmfPack.cxx</a><br/>
 <a class="el" href="_super_l_u_8cxx_source.php">SuperLU.cxx</a></p>
<div class="separator"><a class="anchor" id="getschurmatrix"></a></div><h3>GetSchurMatrix (only for MatrixMumps)</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void GetSchurMatrix(Matrix&amp;, Vector&lt;int&gt;&amp;, Matrix&amp;);
  void GetSchurMatrix(Matrix&amp;, Vector&lt;int&gt;&amp;, Matrix&amp;, bool);
</pre><p>This method computes the schur complement when a matrix and row numbers of the Schur matrix are provided. It is better to use the function <a href="#getschurmatrix_func">GetSchurMatrix</a> since you don't need to call <code>InitSymmetricMatrix</code> before. </p>
<h4>Location :</h4>
<p><a class="el" href="_mumps_8cxx_source.php">Mumps.cxx</a></p>
<div class="separator"><a class="anchor" id="solve"></a></div><h3>Solve </h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void Solve(Vector&amp;);
</pre><p>This method computes the solution of <code>A x = b</code>, assuming that <code>GetLU</code> has been called before to factorize matrix A. This is equivalent to use function <a href="#solvelu">SolveLU</a>.</p>
<h4>Location :</h4>
<p><a class="el" href="_mumps_8cxx_source.php">Mumps.cxx</a><br/>
 <a class="el" href="_umf_pack_8cxx_source.php">UmfPack.cxx</a><br/>
 <a class="el" href="_super_l_u_8cxx_source.php">SuperLU.cxx</a></p>
<div class="separator"><a class="anchor" id="getlu"></a></div><h3>GetLU</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void GetLU(Matrix&amp;, MatrixMumps&amp;);
  void GetLU(Matrix&amp;, MatrixUmfPack&amp;);
  void GetLU(Matrix&amp;, MatrixSuperLU&amp;);
  void GetLU(Matrix&amp;, MatrixMumps&amp;, bool);
  void GetLU(Matrix&amp;, MatrixUmfPack&amp;, bool);
  void GetLU(Matrix&amp;, MatrixSuperLU&amp;, bool);
</pre><p>This method performs a LU factorization. The last argument is optional. When omitted, the initial matrix is erased, when equal to true, the initial matrix is not modified.</p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// sparse matrices, use of Mumps for example
MatrixMumps&lt;double&gt; mat_lu;
Matrix&lt;double, General, ArrayRowSparse&gt; Asp(n, n);
// you add all non-zero entries to matrix Asp
// then you call GetLU to factorize the matrix
GetLU(Asp, mat_lu);
// Asp is empty after GetLU
// you can solve Asp x = b 
X = B;
SolveLU(mat_lu, X);

// if you want to keep initial matrix
GetLU(Asp, mat_lu, true);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_mumps_8cxx_source.php">Mumps.cxx</a><br/>
 <a class="el" href="_umf_pack_8cxx_source.php">UmfPack.cxx</a><br/>
 <a class="el" href="_super_l_u_8cxx_source.php">SuperLU.cxx</a></p>
<div class="separator"><a class="anchor" id="solvelu"></a></div><h3>SolveLU</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void SolveLU(MatrixMumps&amp;, Vector&amp;);
  void SolveLU(MatrixUmfPack&amp;, Vector&amp;);
  void SolveLU(MatrixSuperLU&amp;, Vector&amp;);
</pre><p>This method uses the LU factorization computed by <code>GetLU</code> in order to solve a linear system. The right hand side is overwritten by the result.</p>
<h4>Location :</h4>
<p><a class="el" href="_mumps_8cxx_source.php">Mumps.cxx</a><br/>
 <a class="el" href="_umf_pack_8cxx_source.php">UmfPack.cxx</a><br/>
 <a class="el" href="_super_l_u_8cxx_source.php">SuperLU.cxx</a></p>
<div class="separator"><a class="anchor" id="getschurmatrix_func"></a></div><h3>GetSchurMatrix (only for MatrixMumps)</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void GetSchurMatrix(Matrix&amp;, MatrixMumps&amp;, Vector&lt;int&gt;&amp;, Matrix&amp;);
</pre><p>This method computes the so-called Schur matrix (or Schur complement) from a given matrix. </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
MatrixMumps&lt;double&gt; mat_lu;
Matrix&lt;double, General, ArrayRowSparse&gt; A(n, n);
// you add all non-zero entries to matrix A
// then you specify row numbers for schur matrix
IVect num(5); 
num.Fill();
// somehow, A can be written under the form A = [A11 A12; A21 A22]
// A22 is related to row numbers of the Schur matrix
// Schur matrix is dense
Matrix&lt;double&gt; mat_schur(5, 5);
GetSchurMatrix(A, mat_lu, num, mat_schur);

// then you should obtain A22 - A21*inv(A11)*A12 -&gt; mat_schur
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_mumps_8cxx_source.php">Mumps.cxx</a> </p>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
