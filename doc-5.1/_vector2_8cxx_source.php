<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>vector/Vector2.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2010, INRIA</span>
<a name="l00002"></a>00002 <span class="comment">// Author(s): Marc Fragu, Vivien Mallet</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_VECTOR_VECTOR2_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 
<a name="l00024"></a>00024 <span class="preprocessor">#include &quot;Vector2.hxx&quot;</span>
<a name="l00025"></a>00025 
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 <span class="keyword">namespace </span>Seldon
<a name="l00028"></a>00028 {
<a name="l00029"></a>00029 
<a name="l00030"></a>00030 
<a name="l00032"></a>00032   <span class="comment">// VECTOR2 //</span>
<a name="l00034"></a>00034 <span class="comment"></span>
<a name="l00035"></a>00035 
<a name="l00036"></a>00036   <span class="comment">/***************</span>
<a name="l00037"></a>00037 <span class="comment">   * CONSTRUCTOR *</span>
<a name="l00038"></a>00038 <span class="comment">   ***************/</span>
<a name="l00039"></a>00039 
<a name="l00040"></a>00040 
<a name="l00042"></a>00042 
<a name="l00045"></a>00045   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00046"></a>00046   <a class="code" href="class_seldon_1_1_vector2.php#ac0902a15a512d012c9825d69345c0cc2" title="Default constructor.">Vector2&lt;T, Allocator0, Allocator1&gt;::Vector2</a>()
<a name="l00047"></a>00047   {
<a name="l00048"></a>00048   }
<a name="l00049"></a>00049 
<a name="l00050"></a>00050 
<a name="l00052"></a>00052 
<a name="l00055"></a>00055   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00056"></a>00056   <a class="code" href="class_seldon_1_1_vector2.php#ac0902a15a512d012c9825d69345c0cc2" title="Default constructor.">Vector2&lt;T, Allocator0, Allocator1&gt;::Vector2</a>(<span class="keywordtype">int</span> length)
<a name="l00057"></a>00057   {
<a name="l00058"></a>00058     data_.Reallocate(length);
<a name="l00059"></a>00059   }
<a name="l00060"></a>00060 
<a name="l00061"></a>00061 
<a name="l00063"></a>00063 
<a name="l00067"></a>00067   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00068"></a>00068   <a class="code" href="class_seldon_1_1_vector2.php#ac0902a15a512d012c9825d69345c0cc2" title="Default constructor.">Vector2&lt;T, Allocator0, Allocator1&gt;::Vector2</a>(<span class="keyword">const</span> Vector&lt;int&gt;&amp; length)
<a name="l00069"></a>00069   {
<a name="l00070"></a>00070     data_.Clear();
<a name="l00071"></a>00071     <span class="keywordtype">int</span> m = length.GetSize();
<a name="l00072"></a>00072     data_.Reallocate(m);
<a name="l00073"></a>00073     <span class="keywordflow">for</span>(<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l00074"></a>00074       data_(i).Reallocate(length(i));
<a name="l00075"></a>00075   }
<a name="l00076"></a>00076 
<a name="l00077"></a>00077 
<a name="l00078"></a>00078   <span class="comment">/**************</span>
<a name="l00079"></a>00079 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00080"></a>00080 <span class="comment">   **************/</span>
<a name="l00081"></a>00081 
<a name="l00082"></a><a class="code" href="class_seldon_1_1_vector2.php#a07190b6e16f512ee68feddf636c2698d">00082</a> 
<a name="l00084"></a>00084   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00085"></a>00085   <a class="code" href="class_seldon_1_1_vector2.php#a07190b6e16f512ee68feddf636c2698d" title="Destructor. The vector of vectors and the inner vectors are deallocated.">Vector2&lt;T, Allocator0, Allocator1&gt;::~Vector2</a>()
<a name="l00086"></a>00086   {
<a name="l00087"></a>00087   }
<a name="l00088"></a>00088 
<a name="l00089"></a>00089 
<a name="l00090"></a>00090   <span class="comment">/*****************************</span>
<a name="l00091"></a>00091 <span class="comment">   * MANAGEMENT OF THE VECTORS *</span>
<a name="l00092"></a>00092 <span class="comment">   *****************************/</span>
<a name="l00093"></a>00093 
<a name="l00094"></a>00094 
<a name="l00096"></a>00096 
<a name="l00099"></a>00099   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00100"></a>00100   <span class="keywordtype">bool</span> <a class="code" href="class_seldon_1_1_vector2.php#a4dadbc6f7e013e21ceacc75efcb42c58" title="Checks whether no elements are contained in the inner vectors.">Vector2&lt;T, Allocator0, Allocator1&gt;::IsEmpty</a>()<span class="keyword"> const</span>
<a name="l00101"></a>00101 <span class="keyword">  </span>{
<a name="l00102"></a>00102     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8" title="Returns the size along dimension 1.">GetLength</a>(); i++)
<a name="l00103"></a>00103       <span class="keywordflow">if</span> (<a class="code" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8" title="Returns the size along dimension 1.">GetLength</a>(i) &gt; 0)
<a name="l00104"></a>00104         <span class="keywordflow">return</span> <span class="keyword">false</span>;
<a name="l00105"></a>00105     <span class="keywordflow">return</span> <span class="keyword">true</span>;
<a name="l00106"></a>00106   }
<a name="l00107"></a>00107 
<a name="l00108"></a>00108 
<a name="l00110"></a>00110 
<a name="l00113"></a>00113   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00114"></a>00114   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector2.php#a1eb80b01b78bb858b72a3daace5b7fa2" title="Returns the size along dimension 1.">Vector2&lt;T, Allocator0, Allocator1&gt;::GetSize</a>()<span class="keyword"> const</span>
<a name="l00115"></a>00115 <span class="keyword">  </span>{
<a name="l00116"></a>00116     <span class="keywordflow">return</span> data_.GetSize();
<a name="l00117"></a>00117   }
<a name="l00118"></a>00118 
<a name="l00119"></a>00119 
<a name="l00121"></a>00121 
<a name="l00124"></a>00124   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00125"></a>00125   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8" title="Returns the size along dimension 1.">Vector2&lt;T, Allocator0, Allocator1&gt;::GetLength</a>()<span class="keyword"> const</span>
<a name="l00126"></a>00126 <span class="keyword">  </span>{
<a name="l00127"></a>00127     <span class="keywordflow">return</span> data_.GetLength();
<a name="l00128"></a>00128   }
<a name="l00129"></a>00129 
<a name="l00130"></a>00130 
<a name="l00132"></a>00132 
<a name="l00136"></a>00136   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00137"></a>00137   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector2.php#a1eb80b01b78bb858b72a3daace5b7fa2" title="Returns the size along dimension 1.">Vector2&lt;T, Allocator0, Allocator1&gt;::GetSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00138"></a>00138 <span class="keyword">  </span>{
<a name="l00139"></a>00139     <span class="keywordflow">return</span> data_(i).GetSize();
<a name="l00140"></a>00140   }
<a name="l00141"></a>00141 
<a name="l00142"></a>00142 
<a name="l00144"></a>00144 
<a name="l00148"></a>00148   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00149"></a>00149   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8" title="Returns the size along dimension 1.">Vector2&lt;T, Allocator0, Allocator1&gt;::GetLength</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00150"></a>00150 <span class="keyword">  </span>{
<a name="l00151"></a>00151     <span class="keywordflow">return</span> data_(i).GetLength();
<a name="l00152"></a>00152   }
<a name="l00153"></a>00153 
<a name="l00154"></a>00154 
<a name="l00156"></a>00156 
<a name="l00159"></a>00159   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00160"></a>00160   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector2.php#aff7d041efb4e03152ecb03966ae92948" title="Returns the total number of elements in the inner vectors.">Vector2&lt;T, Allocator0, Allocator1&gt;::GetNelement</a>()<span class="keyword"> const</span>
<a name="l00161"></a>00161 <span class="keyword">  </span>{
<a name="l00162"></a>00162     <span class="keywordtype">int</span> total = 0;
<a name="l00163"></a>00163     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8" title="Returns the size along dimension 1.">GetLength</a>(); i++)
<a name="l00164"></a>00164       total += <a class="code" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8" title="Returns the size along dimension 1.">GetLength</a>(i);
<a name="l00165"></a>00165     <span class="keywordflow">return</span> total;
<a name="l00166"></a>00166   }
<a name="l00167"></a>00167 
<a name="l00168"></a>00168 
<a name="l00170"></a>00170 
<a name="l00177"></a>00177   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00178"></a>00178   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector2.php#aff7d041efb4e03152ecb03966ae92948" title="Returns the total number of elements in the inner vectors.">Vector2&lt;T, Allocator0, Allocator1&gt;::GetNelement</a>(<span class="keywordtype">int</span> beg, <span class="keywordtype">int</span> end)<span class="keyword"> const</span>
<a name="l00179"></a>00179 <span class="keyword">  </span>{
<a name="l00180"></a>00180     <span class="keywordflow">if</span> (beg &gt; end)
<a name="l00181"></a>00181       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Vector2::GetNelement(int beg, int end)&quot;</span>,
<a name="l00182"></a>00182                           <span class="stringliteral">&quot;The lower bound of the range of inner vectors, [&quot;</span>
<a name="l00183"></a>00183                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end)
<a name="l00184"></a>00184                           + <span class="stringliteral">&quot;[, is strictly greater than its upper bound.&quot;</span>);
<a name="l00185"></a>00185     <span class="keywordflow">if</span> (beg &lt; 0 || end &gt; <a class="code" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8" title="Returns the size along dimension 1.">GetLength</a>())
<a name="l00186"></a>00186       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Vector2::GetNelement(int beg, int end)&quot;</span>,
<a name="l00187"></a>00187                           <span class="stringliteral">&quot;The inner-vector indexes should be in [0,&quot;</span>
<a name="l00188"></a>00188                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<a class="code" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8" title="Returns the size along dimension 1.">GetLength</a>()) + <span class="stringliteral">&quot;] but [&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg)
<a name="l00189"></a>00189                           + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end) + <span class="stringliteral">&quot;[ was provided.&quot;</span>);
<a name="l00190"></a>00190 
<a name="l00191"></a>00191     <span class="keywordtype">int</span> total = 0;
<a name="l00192"></a>00192     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = beg; i &lt; end; i++)
<a name="l00193"></a>00193       total += <a class="code" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8" title="Returns the size along dimension 1.">GetLength</a>(i);
<a name="l00194"></a>00194     <span class="keywordflow">return</span> total;
<a name="l00195"></a>00195   }
<a name="l00196"></a>00196 
<a name="l00197"></a>00197 
<a name="l00199"></a>00199 
<a name="l00202"></a>00202   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00203"></a>00203   Vector&lt;int&gt; <a class="code" href="class_seldon_1_1_vector2.php#a82efefffc4e2e7ebaf120e080a79fb95" title="Returns the shape.">Vector2&lt;T, Allocator0, Allocator1&gt;::GetShape</a>()<span class="keyword"> const</span>
<a name="l00204"></a>00204 <span class="keyword">  </span>{
<a name="l00205"></a>00205     Vector&lt;int&gt; shape(<a class="code" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8" title="Returns the size along dimension 1.">GetLength</a>());
<a name="l00206"></a>00206     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8" title="Returns the size along dimension 1.">GetLength</a>(); i++)
<a name="l00207"></a>00207       shape(i) = <a class="code" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8" title="Returns the size along dimension 1.">GetLength</a>(i);
<a name="l00208"></a>00208     <span class="keywordflow">return</span> shape;
<a name="l00209"></a>00209   }
<a name="l00210"></a>00210 
<a name="l00211"></a>00211 
<a name="l00213"></a>00213 
<a name="l00216"></a>00216   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00217"></a>00217   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector2.php#a82efefffc4e2e7ebaf120e080a79fb95" title="Returns the shape.">Vector2&lt;T, Allocator0, Allocator1&gt;::GetShape</a>(Vector&lt;int&gt;&amp; shape)<span class="keyword"> const</span>
<a name="l00218"></a>00218 <span class="keyword">  </span>{
<a name="l00219"></a>00219     shape.Reallocate(<a class="code" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8" title="Returns the size along dimension 1.">GetLength</a>());
<a name="l00220"></a>00220     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8" title="Returns the size along dimension 1.">GetLength</a>(); i++)
<a name="l00221"></a>00221       shape(i) = <a class="code" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8" title="Returns the size along dimension 1.">GetLength</a>(i);
<a name="l00222"></a>00222   }
<a name="l00223"></a>00223 
<a name="l00224"></a>00224 
<a name="l00226"></a>00226 
<a name="l00229"></a>00229   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00230"></a>00230   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector2.php#ab0e6ad584c49c4182153a4e523e177ea" title="Reallocates the vector of vector.">Vector2&lt;T, Allocator0, Allocator1&gt;::Reallocate</a>(<span class="keywordtype">int</span> M)
<a name="l00231"></a>00231   {
<a name="l00232"></a>00232     data_.Reallocate(M);
<a name="l00233"></a>00233   }
<a name="l00234"></a>00234 
<a name="l00235"></a>00235 
<a name="l00237"></a>00237 
<a name="l00241"></a>00241   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00242"></a>00242   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector2.php#ab0e6ad584c49c4182153a4e523e177ea" title="Reallocates the vector of vector.">Vector2&lt;T, Allocator0, Allocator1&gt;::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> N)
<a name="l00243"></a>00243   {
<a name="l00244"></a>00244     data_(i).Reallocate(N);
<a name="l00245"></a>00245   }
<a name="l00246"></a>00246 
<a name="l00247"></a>00247 
<a name="l00249"></a>00249 
<a name="l00253"></a>00253   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00254"></a>00254   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector2.php#ab0e6ad584c49c4182153a4e523e177ea" title="Reallocates the vector of vector.">Vector2&lt;T, Allocator0, Allocator1&gt;</a>
<a name="l00255"></a>00255 <a class="code" href="class_seldon_1_1_vector2.php#ab0e6ad584c49c4182153a4e523e177ea" title="Reallocates the vector of vector.">  ::Reallocate</a>(<span class="keyword">const</span> Vector&lt;int&gt;&amp; length)
<a name="l00256"></a>00256   {
<a name="l00257"></a>00257     <span class="keywordtype">int</span> m = length.GetSize();
<a name="l00258"></a>00258     data_.Reallocate(m);
<a name="l00259"></a>00259     <span class="keywordflow">for</span>(<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l00260"></a>00260       data_(i).Reallocate(length(i));
<a name="l00261"></a>00261   }
<a name="l00262"></a>00262 
<a name="l00263"></a>00263 
<a name="l00265"></a>00265 
<a name="l00270"></a>00270   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00271"></a>00271   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector2.php#a2cf9e05eacb905603b0f0cfc8e7cdc5a" title="Selects a range of inner vectors.">Vector2&lt;T, Allocator0, Allocator1&gt;::Select</a>(<span class="keywordtype">int</span> beg, <span class="keywordtype">int</span> end)
<a name="l00272"></a>00272   {
<a name="l00273"></a>00273     <span class="keywordflow">if</span> (beg &gt; end)
<a name="l00274"></a>00274       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Vector2::SelectInnerVector(int beg, int end)&quot;</span>,
<a name="l00275"></a>00275                           <span class="stringliteral">&quot;The lower bound of the range of inner vectors, [&quot;</span>
<a name="l00276"></a>00276                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end)
<a name="l00277"></a>00277                           + <span class="stringliteral">&quot;[, is strictly greater than its upper bound.&quot;</span>);
<a name="l00278"></a>00278     <span class="keywordflow">if</span> (beg &lt; 0 || end &gt; <a class="code" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8" title="Returns the size along dimension 1.">GetLength</a>())
<a name="l00279"></a>00279       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Vector2::SelectInnerVector(int beg, int end)&quot;</span>,
<a name="l00280"></a>00280                           <span class="stringliteral">&quot;The inner-vector indexes should be in [0,&quot;</span>
<a name="l00281"></a>00281                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<a class="code" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8" title="Returns the size along dimension 1.">GetLength</a>()) + <span class="stringliteral">&quot;] but [&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg)
<a name="l00282"></a>00282                           + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end) + <span class="stringliteral">&quot;[ was provided.&quot;</span>);
<a name="l00283"></a>00283 
<a name="l00284"></a>00284     <span class="keywordflow">if</span> (beg &gt; 0)
<a name="l00285"></a>00285       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; end - beg; i++)
<a name="l00286"></a>00286         data_(i) = data_(beg + i);
<a name="l00287"></a>00287     data_.Reallocate(end - beg);
<a name="l00288"></a>00288   }
<a name="l00289"></a>00289 
<a name="l00290"></a>00290 
<a name="l00292"></a>00292 
<a name="l00296"></a><a class="code" href="class_seldon_1_1_vector2.php#a3be94439c78312344107d23fbb290e0e">00296</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00297"></a>00297   <span class="keyword">template</span> &lt;<span class="keyword">class</span> Td, <span class="keyword">class</span> Allocatord&gt;
<a name="l00298"></a>00298   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector2.php#a3be94439c78312344107d23fbb290e0e" title="Returns all values in a vector.">Vector2&lt;T, Allocator0, Allocator1&gt;</a>
<a name="l00299"></a>00299 <a class="code" href="class_seldon_1_1_vector2.php#a3be94439c78312344107d23fbb290e0e" title="Returns all values in a vector.">  ::Flatten</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Td, VectFull, Allocatord&gt;</a>&amp; data)<span class="keyword"> const</span>
<a name="l00300"></a>00300 <span class="keyword">  </span>{
<a name="l00301"></a>00301     data.Reallocate(GetNelement());
<a name="l00302"></a>00302     <span class="keywordtype">int</span> i, j, n(0);
<a name="l00303"></a>00303     <span class="keywordflow">for</span> (i = 0; i &lt; GetLength(); i++)
<a name="l00304"></a>00304       <span class="keywordflow">for</span> (j = 0; j &lt; GetLength(i); j++)
<a name="l00305"></a>00305         data(n++) = data_(i)(j);
<a name="l00306"></a>00306   }
<a name="l00307"></a>00307 
<a name="l00308"></a>00308 
<a name="l00310"></a>00310 
<a name="l00318"></a><a class="code" href="class_seldon_1_1_vector2.php#a98d6e0c5dda92cbd0d70b1bfa16bd2ee">00318</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00319"></a>00319   <span class="keyword">template</span> &lt;<span class="keyword">class</span> Td, <span class="keyword">class</span> Allocatord&gt;
<a name="l00320"></a>00320   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector2.php#a3be94439c78312344107d23fbb290e0e" title="Returns all values in a vector.">Vector2&lt;T, Allocator0, Allocator1&gt;</a>
<a name="l00321"></a>00321 <a class="code" href="class_seldon_1_1_vector2.php#a3be94439c78312344107d23fbb290e0e" title="Returns all values in a vector.">  ::Flatten</a>(<span class="keywordtype">int</span> beg, <span class="keywordtype">int</span> end, <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Td, VectFull, Allocatord&gt;</a>&amp; data)<span class="keyword"> const</span>
<a name="l00322"></a>00322 <span class="keyword">  </span>{
<a name="l00323"></a>00323     <span class="keywordflow">if</span> (beg &gt; end)
<a name="l00324"></a>00324       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Vector2::Flatten(int beg, int end, Vector&amp; data)&quot;</span>,
<a name="l00325"></a>00325                           <span class="stringliteral">&quot;The lower bound of the range of inner vectors, [&quot;</span>
<a name="l00326"></a>00326                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end)
<a name="l00327"></a>00327                           + <span class="stringliteral">&quot;[, is strictly greater than its upper bound.&quot;</span>);
<a name="l00328"></a>00328     <span class="keywordflow">if</span> (beg &lt; 0 || end &gt; GetLength())
<a name="l00329"></a>00329       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Vector2::Flatten(int beg, int end, Vector&amp; data)&quot;</span>,
<a name="l00330"></a>00330                           <span class="stringliteral">&quot;The inner-vector indexes should be in [0,&quot;</span>
<a name="l00331"></a>00331                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(GetLength()) + <span class="stringliteral">&quot;] but [&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg)
<a name="l00332"></a>00332                           + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end) + <span class="stringliteral">&quot;[ was provided.&quot;</span>);
<a name="l00333"></a>00333 
<a name="l00334"></a>00334     data.Reallocate(GetNelement(beg, end));
<a name="l00335"></a>00335     <span class="keywordtype">int</span> i, j, n(0);
<a name="l00336"></a>00336     <span class="keywordflow">for</span> (i = beg; i &lt; end; i++)
<a name="l00337"></a>00337       <span class="keywordflow">for</span> (j = 0; j &lt; GetLength(i); j++)
<a name="l00338"></a>00338         data(n++) = data_(i)(j);
<a name="l00339"></a>00339   }
<a name="l00340"></a>00340 
<a name="l00341"></a>00341 
<a name="l00343"></a>00343 
<a name="l00347"></a>00347   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00348"></a>00348   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector2.php#a1d386f766b75c53d17b66bc843a4db54" title="Appends an element at the end of the inner vector #i.">Vector2&lt;T, Allocator0, Allocator1&gt;::PushBack</a>(<span class="keywordtype">int</span> i, <span class="keyword">const</span> T&amp; x)
<a name="l00349"></a>00349   {
<a name="l00350"></a>00350     data_(i).PushBack(x);
<a name="l00351"></a>00351   }
<a name="l00352"></a>00352 
<a name="l00353"></a>00353 
<a name="l00355"></a>00355 
<a name="l00358"></a>00358   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00359"></a>00359   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector2.php#a1d386f766b75c53d17b66bc843a4db54" title="Appends an element at the end of the inner vector #i.">Vector2&lt;T, Allocator0, Allocator1&gt;</a>
<a name="l00360"></a>00360 <a class="code" href="class_seldon_1_1_vector2.php#a1d386f766b75c53d17b66bc843a4db54" title="Appends an element at the end of the inner vector #i.">  ::PushBack</a>(<span class="keyword">const</span> Vector&lt;T, VectFull, Allocator0&gt;&amp; X)
<a name="l00361"></a>00361   {
<a name="l00362"></a>00362     data_.PushBack(X);
<a name="l00363"></a>00363   }
<a name="l00364"></a>00364 
<a name="l00365"></a>00365 
<a name="l00367"></a>00367 
<a name="l00371"></a>00371   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00372"></a>00372   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector2.php#a1d386f766b75c53d17b66bc843a4db54" title="Appends an element at the end of the inner vector #i.">Vector2&lt;T, Allocator0, Allocator1&gt;</a>
<a name="l00373"></a>00373 <a class="code" href="class_seldon_1_1_vector2.php#a1d386f766b75c53d17b66bc843a4db54" title="Appends an element at the end of the inner vector #i.">  ::PushBack</a>(<span class="keyword">const</span> Vector&lt;Vector&lt;T, VectFull, Allocator0&gt;,
<a name="l00374"></a>00374              VectFull, Allocator1&gt;&amp; V)
<a name="l00375"></a>00375   {
<a name="l00376"></a>00376     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; V.GetLength(); i++)
<a name="l00377"></a>00377       data_.PushBack(V(i));
<a name="l00378"></a>00378   }
<a name="l00379"></a>00379 
<a name="l00380"></a>00380 
<a name="l00382"></a>00382 
<a name="l00386"></a>00386   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00387"></a>00387   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector2.php#a1d386f766b75c53d17b66bc843a4db54" title="Appends an element at the end of the inner vector #i.">Vector2&lt;T, Allocator0, Allocator1&gt;</a>
<a name="l00388"></a>00388 <a class="code" href="class_seldon_1_1_vector2.php#a1d386f766b75c53d17b66bc843a4db54" title="Appends an element at the end of the inner vector #i.">  ::PushBack</a>(<span class="keyword">const</span> Vector2&lt;T, Allocator0, Allocator1&gt;&amp; V)
<a name="l00389"></a>00389   {
<a name="l00390"></a>00390     PushBack(V.GetVector());
<a name="l00391"></a>00391   }
<a name="l00392"></a>00392 
<a name="l00393"></a><a class="code" href="class_seldon_1_1_vector2.php#a8c34a780a5b3f8b83bf36a14a4e7f21a">00393</a> 
<a name="l00395"></a>00395   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00396"></a>00396   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector2.php#a8c34a780a5b3f8b83bf36a14a4e7f21a" title="Clears the vector.">Vector2&lt;T, Allocator0, Allocator1&gt;::Clear</a>()
<a name="l00397"></a>00397   {
<a name="l00398"></a>00398     data_.Clear();
<a name="l00399"></a>00399   }
<a name="l00400"></a>00400 
<a name="l00401"></a>00401 
<a name="l00403"></a>00403 
<a name="l00406"></a>00406   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00407"></a>00407   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector2.php#a8c34a780a5b3f8b83bf36a14a4e7f21a" title="Clears the vector.">Vector2&lt;T, Allocator0, Allocator1&gt;::Clear</a>(<span class="keywordtype">int</span> i)
<a name="l00408"></a>00408   {
<a name="l00409"></a>00409     data_(i).Clear();
<a name="l00410"></a>00410   }
<a name="l00411"></a>00411 
<a name="l00412"></a>00412 
<a name="l00414"></a>00414 
<a name="l00417"></a>00417   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00418"></a>00418   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector2.php#a2794e1d6c95f016265e70453f13c88d3" title="Fills the vector with a given value.">Vector2&lt;T, Allocator0, Allocator1&gt;::Fill</a>(<span class="keyword">const</span> T&amp; x)
<a name="l00419"></a>00419   {
<a name="l00420"></a>00420     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; data_.GetLength(); i++)
<a name="l00421"></a>00421       data_(i).Fill(x);
<a name="l00422"></a>00422   }
<a name="l00423"></a>00423 
<a name="l00424"></a>00424 
<a name="l00426"></a>00426 
<a name="l00429"></a>00429   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00430"></a>00430   Vector&lt;Vector&lt;T, VectFull, Allocator0&gt;, VectFull, Allocator1&gt;&amp;
<a name="l00431"></a>00431   <a class="code" href="class_seldon_1_1_vector2.php#a6955ada9427842710beeaa19ae65e299" title="Returns the vector of vectors.">Vector2&lt;T, Allocator0, Allocator1&gt;::GetVector</a>()
<a name="l00432"></a>00432   {
<a name="l00433"></a>00433     <span class="keywordflow">return</span> data_;
<a name="l00434"></a>00434   }
<a name="l00435"></a>00435 
<a name="l00436"></a>00436 
<a name="l00438"></a>00438 
<a name="l00441"></a>00441   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00442"></a>00442   <span class="keyword">const</span> Vector&lt;Vector&lt;T, VectFull, Allocator0&gt;, VectFull, Allocator1&gt;
<a name="l00443"></a>00443   <a class="code" href="class_seldon_1_1_vector2.php#a6955ada9427842710beeaa19ae65e299" title="Returns the vector of vectors.">Vector2&lt;T, Allocator0, Allocator1&gt;::GetVector</a>()<span class="keyword"> const</span>
<a name="l00444"></a>00444 <span class="keyword">  </span>{
<a name="l00445"></a>00445     <span class="keywordflow">return</span> data_;
<a name="l00446"></a>00446   }
<a name="l00447"></a>00447 
<a name="l00448"></a>00448 
<a name="l00450"></a>00450 
<a name="l00454"></a>00454   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00455"></a>00455   Vector&lt;T, VectFull, Allocator0&gt;&amp;
<a name="l00456"></a>00456   <a class="code" href="class_seldon_1_1_vector2.php#a6955ada9427842710beeaa19ae65e299" title="Returns the vector of vectors.">Vector2&lt;T, Allocator0, Allocator1&gt;::GetVector</a>(<span class="keywordtype">int</span> i)
<a name="l00457"></a>00457   {
<a name="l00458"></a>00458     <span class="keywordflow">return</span> data_(i);
<a name="l00459"></a>00459   }
<a name="l00460"></a>00460 
<a name="l00461"></a>00461 
<a name="l00463"></a>00463 
<a name="l00467"></a>00467   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00468"></a>00468   <span class="keyword">const</span> Vector&lt;T, VectFull, Allocator0&gt;&amp;
<a name="l00469"></a>00469   <a class="code" href="class_seldon_1_1_vector2.php#a6955ada9427842710beeaa19ae65e299" title="Returns the vector of vectors.">Vector2&lt;T, Allocator0, Allocator1&gt;::GetVector</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00470"></a>00470 <span class="keyword">  </span>{
<a name="l00471"></a>00471     <span class="keywordflow">return</span> data_(i);
<a name="l00472"></a>00472   }
<a name="l00473"></a>00473 
<a name="l00474"></a>00474 
<a name="l00476"></a>00476 
<a name="l00481"></a>00481   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00482"></a>00482   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector2.php#a7074df5f337f30702d6b6d59f4855a30" title="Copies a Vector2 instance.">Vector2&lt;T, Allocator0, Allocator1&gt;</a>
<a name="l00483"></a>00483 <a class="code" href="class_seldon_1_1_vector2.php#a7074df5f337f30702d6b6d59f4855a30" title="Copies a Vector2 instance.">  ::Copy</a>(<span class="keyword">const</span> Vector2&lt;T, Allocator0, Allocator1&gt;&amp; V)
<a name="l00484"></a>00484   {
<a name="l00485"></a>00485     Clear();
<a name="l00486"></a>00486     Reallocate(V.GetLength());
<a name="l00487"></a>00487     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; V.GetLength(); i++)
<a name="l00488"></a>00488       data_(i) = V(i);
<a name="l00489"></a>00489   }
<a name="l00490"></a>00490 
<a name="l00491"></a>00491 
<a name="l00492"></a>00492   <span class="comment">/*********************************</span>
<a name="l00493"></a>00493 <span class="comment">   * ELEMENT ACCESS AND ASSIGNMENT *</span>
<a name="l00494"></a>00494 <span class="comment">   *********************************/</span>
<a name="l00495"></a>00495 
<a name="l00496"></a>00496 
<a name="l00498"></a>00498 
<a name="l00502"></a>00502   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00503"></a>00503   <span class="keyword">const</span> Vector&lt;T, VectFull, Allocator0&gt;&amp;
<a name="l00504"></a>00504   <a class="code" href="class_seldon_1_1_vector2.php#ae163011cc1d13382280abd54c1f4af93" title="Returns a given inner vector.">Vector2&lt;T, Allocator0, Allocator1&gt;::operator() </a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00505"></a>00505 <span class="keyword">  </span>{
<a name="l00506"></a>00506     <span class="keywordflow">return</span> data_(i);
<a name="l00507"></a>00507   }
<a name="l00508"></a>00508 
<a name="l00509"></a>00509 
<a name="l00511"></a>00511 
<a name="l00515"></a>00515   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00516"></a>00516   Vector&lt;T, VectFull, Allocator0&gt;&amp;
<a name="l00517"></a>00517   <a class="code" href="class_seldon_1_1_vector2.php#ae163011cc1d13382280abd54c1f4af93" title="Returns a given inner vector.">Vector2&lt;T, Allocator0, Allocator1&gt;::operator() </a>(<span class="keywordtype">int</span> i)
<a name="l00518"></a>00518   {
<a name="l00519"></a>00519     <span class="keywordflow">return</span> data_(i);
<a name="l00520"></a>00520   }
<a name="l00521"></a>00521 
<a name="l00522"></a>00522 
<a name="l00524"></a>00524 
<a name="l00529"></a>00529   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00530"></a>00530   <span class="keyword">typename</span> Vector2&lt;T, Allocator0, Allocator1&gt;::const_reference
<a name="l00531"></a>00531   <a class="code" href="class_seldon_1_1_vector2.php#ae163011cc1d13382280abd54c1f4af93" title="Returns a given inner vector.">Vector2&lt;T, Allocator0, Allocator1&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00532"></a>00532 <span class="keyword">  </span>{
<a name="l00533"></a>00533     <span class="keywordflow">return</span> data_(i)(j);
<a name="l00534"></a>00534   }
<a name="l00535"></a>00535 
<a name="l00536"></a>00536 
<a name="l00538"></a>00538 
<a name="l00543"></a>00543   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00544"></a>00544   <span class="keyword">typename</span> Vector2&lt;T, Allocator0, Allocator1&gt;::reference
<a name="l00545"></a>00545   <a class="code" href="class_seldon_1_1_vector2.php#ae163011cc1d13382280abd54c1f4af93" title="Returns a given inner vector.">Vector2&lt;T, Allocator0, Allocator1&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00546"></a>00546   {
<a name="l00547"></a>00547     <span class="keywordflow">return</span> data_(i)(j);
<a name="l00548"></a>00548   }
<a name="l00549"></a>00549 
<a name="l00550"></a>00550 
<a name="l00551"></a>00551   <span class="comment">/**********************</span>
<a name="l00552"></a>00552 <span class="comment">   * CONVENIENT METHODS *</span>
<a name="l00553"></a>00553 <span class="comment">   **********************/</span>
<a name="l00554"></a>00554 
<a name="l00555"></a>00555 
<a name="l00557"></a>00557 
<a name="l00565"></a>00565   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00566"></a>00566   <span class="keyword">template</span> &lt;<span class="keyword">class</span> V2&gt;
<a name="l00567"></a>00567   <span class="keywordtype">bool</span> <a class="code" href="class_seldon_1_1_vector2.php#ab3a611bb7d4dd2bfc0f7c6540665db38" title="Checks whether another Vector2 instance has the same shape.">Vector2&lt;T, Allocator0, Allocator1&gt;::HasSameShape</a>(<span class="keyword">const</span> V2&amp; V)<span class="keyword"> const</span>
<a name="l00568"></a>00568 <span class="keyword">  </span>{
<a name="l00569"></a>00569     <span class="keywordflow">if</span> (V.GetLength() != <a class="code" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8" title="Returns the size along dimension 1.">GetLength</a>())
<a name="l00570"></a>00570       <span class="keywordflow">return</span> <span class="keyword">false</span>;
<a name="l00571"></a>00571     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8" title="Returns the size along dimension 1.">GetLength</a>(); i++)
<a name="l00572"></a>00572       <span class="keywordflow">if</span> (V.GetLength(i) != <a class="code" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8" title="Returns the size along dimension 1.">GetLength</a>(i))
<a name="l00573"></a>00573         <span class="keywordflow">return</span> <span class="keyword">false</span>;
<a name="l00574"></a>00574     <span class="keywordflow">return</span> <span class="keyword">true</span>;
<a name="l00575"></a>00575   }
<a name="l00576"></a>00576 
<a name="l00577"></a><a class="code" href="class_seldon_1_1_vector2.php#ab283856136c91c88d2f944eabed7bdc8">00577</a> 
<a name="l00579"></a>00579   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1&gt;
<a name="l00580"></a>00580   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector2.php#ab283856136c91c88d2f944eabed7bdc8" title="Displays the vector.">Vector2&lt;T, Allocator0, Allocator1&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00581"></a>00581 <span class="keyword">  </span>{
<a name="l00582"></a>00582     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; data_.GetLength(); i++)
<a name="l00583"></a>00583       {
<a name="l00584"></a>00584         cout &lt;&lt; <span class="stringliteral">&quot;Vector &quot;</span> &lt;&lt; i &lt;&lt; <span class="stringliteral">&quot;: &quot;</span>;
<a name="l00585"></a>00585         data_(i).Print();
<a name="l00586"></a>00586       }
<a name="l00587"></a>00587   }
<a name="l00588"></a>00588 
<a name="l00589"></a>00589 
<a name="l00590"></a>00590 } <span class="comment">// namespace Seldon.</span>
<a name="l00591"></a>00591 
<a name="l00592"></a>00592 
<a name="l00593"></a>00593 <span class="preprocessor">#define SELDON_FILE_VECTOR_VECTOR2_CXX</span>
<a name="l00594"></a>00594 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
