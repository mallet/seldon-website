<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php">Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;" --><!-- doxytag: inherits="Matrix_ArraySparse&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;" -->
<p>Row-major symmetric sparse-matrix class.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.png" usemap="#Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;_map" alt=""/>
  <map id="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;_map" name="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;_map">
<area href="class_seldon_1_1_matrix___array_sparse.php" alt="Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;" shape="rect" coords="0,0,429,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab799aa23816c841985227b0bd7bc2470"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::value_type" ref="ab799aa23816c841985227b0bd7bc2470" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4511b34daff97810f21d0dc7a160d745"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::property" ref="a4511b34daff97810f21d0dc7a160d745" args="" -->
typedef Prop&nbsp;</td><td class="memItemRight" valign="bottom"><b>property</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ada68b1a306446d9c173312bca11abdb8"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::storage" ref="ada68b1a306446d9c173312bca11abdb8" args="" -->
typedef <a class="el" href="class_seldon_1_1_array_row_sym_sparse.php">ArrayRowSymSparse</a>&nbsp;</td><td class="memItemRight" valign="bottom"><b>storage</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a34fb258bcc073834b5264d8a869f3456"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::allocator" ref="a34fb258bcc073834b5264d8a869f3456" args="" -->
typedef Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>allocator</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad942446ee432becc1e9e0b55223d60b1"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::pointer" ref="ad942446ee432becc1e9e0b55223d60b1" args="" -->
typedef Allocator::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa51e4113603906cb6ccb1770886f9777"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::const_pointer" ref="aa51e4113603906cb6ccb1770886f9777" args="" -->
typedef Allocator::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a78e6a6b658209682bb428916b5cb4eab"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::reference" ref="a78e6a6b658209682bb428916b5cb4eab" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a416fcb271e492f2c28ca01ea54488814"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::const_reference" ref="a416fcb271e492f2c28ca01ea54488814" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a96f809f4258c19a7786058627a25d9ee"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::entry_type" ref="a96f809f4258c19a7786058627a25d9ee" args="" -->
typedef T&nbsp;</td><td class="memItemRight" valign="bottom"><b>entry_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab491c58b73fe652ffdd7faa975be18dc"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::access_type" ref="ab491c58b73fe652ffdd7faa975be18dc" args="" -->
typedef T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>access_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a42c803f09cef5e281e8061cceb9098b2"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::const_access_type" ref="a42c803f09cef5e281e8061cceb9098b2" args="" -->
typedef T&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_access_type</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a9bd699fbd7fe4a62e4216883a322b623">Matrix</a> ()  throw ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#a9bd699fbd7fe4a62e4216883a322b623"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#aab5b076238e96f89b33b8c7a09f0b490">Matrix</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Constructor.  <a href="#aab5b076238e96f89b33b8c7a09f0b490"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a8d872a0e035a2afcc8e40736e8556c44">operator()</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a8d872a0e035a2afcc8e40736e8556c44"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a45ec07c98e6e55fdae57dda5b0c1cda7">operator()</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a45ec07c98e6e55fdae57dda5b0c1cda7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1221b2abf3eb604ab208e9f839ccfa5c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::ClearRow" ref="a1221b2abf3eb604ab208e9f839ccfa5c" args="(int i)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a1221b2abf3eb604ab208e9f839ccfa5c">ClearRow</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears a row. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a02d5086adf71d37c259c1a94a375bd57">ReallocateRow</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates row i.  <a href="#a02d5086adf71d37c259c1a94a375bd57"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#ad70cf7b6b6aadd9c1e51bc1944550d91">ResizeRow</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates row i.  <a href="#ad70cf7b6b6aadd9c1e51bc1944550d91"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#abb74a0467a0295eb8964ac30c9e4922c">SwapRow</a> (int i, int i_)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Swaps two rows.  <a href="#abb74a0467a0295eb8964ac30c9e4922c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#af70fc4f3c8267b2a4f528e1fd3056020">ReplaceIndexRow</a> (int i, <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;new_index)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets column numbers of non-zero entries of a row.  <a href="#af70fc4f3c8267b2a4f528e1fd3056020"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#af202c14cefd0cb751e4cf906ca5804e7">GetRowSize</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of non-zero entries of a row.  <a href="#af202c14cefd0cb751e4cf906ca5804e7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a10ca8f311170cfa8b11f6caa63d42635"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::PrintRow" ref="a10ca8f311170cfa8b11f6caa63d42635" args="(int i) const " -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a10ca8f311170cfa8b11f6caa63d42635">PrintRow</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays non-zero values of a column. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a94a7c41aa9fb70bc83fbadb66e0e7709">AssembleRow</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Assembles a column.  <a href="#a94a7c41aa9fb70bc83fbadb66e0e7709"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#ac8bf415466c0dd580e635a66ae4110a6">AddInteraction</a> (int i, int j, const T &amp;val)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds a coefficient in the matrix.  <a href="#ac8bf415466c0dd580e635a66ae4110a6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a3c0b4b2b2f82744e655eb119080c309f">AddInteractionRow</a> (int, int, int *, T *)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds coefficients in a row.  <a href="#a3c0b4b2b2f82744e655eb119080c309f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#ab4d1a9abf1e07714f5652eab9862475c">AddInteractionColumn</a> (int, int, int *, T *)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds coefficients in a column.  <a href="#ab4d1a9abf1e07714f5652eab9862475c"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Alloc1 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#af99cb559be02dcf98137b4dc5c4b29d1">AddInteractionRow</a> (int i, int nb, const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;col, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1 &gt; &amp;val)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds coefficients in a row.  <a href="#af99cb559be02dcf98137b4dc5c4b29d1"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Alloc1 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a8f6797869ae81e18c8c81aa00fbcdf82">AddInteractionColumn</a> (int i, int nb, const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;row, const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1 &gt; &amp;val)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Adds coefficients in a column.  <a href="#a8f6797869ae81e18c8c81aa00fbcdf82"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a61227beea7612dd22bf1f46546bb5cde"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Clear" ref="a61227beea7612dd22bf1f46546bb5cde" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Clear</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afcd0d4612b325249c24407627b3dd568"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Reallocate" ref="afcd0d4612b325249c24407627b3dd568" args="(int i, int j)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Reallocate</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2c6f9586424529c8b06b6110c5a01efa"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Resize" ref="a2c6f9586424529c8b06b6110c5a01efa" args="(int i, int j)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Resize</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a156ebb8be6f9fbf5d2e7ef01822a341f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::GetM" ref="a156ebb8be6f9fbf5d2e7ef01822a341f" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetM</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aeb55b9de8946ee89fb40c18612f9f700"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::GetM" ref="aeb55b9de8946ee89fb40c18612f9f700" args="(const SeldonTranspose &amp;status) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetM</b> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a> &amp;status) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3ae65d56c548233051a15054643f6753"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::GetN" ref="a3ae65d56c548233051a15054643f6753" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetN</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a537eebc1c692724958852a83df609036"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::GetN" ref="a537eebc1c692724958852a83df609036" args="(const SeldonTranspose &amp;status) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetN</b> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a> &amp;status) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac58573fd27b4f0345ae196b1a6c69d0d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::GetNonZeros" ref="ac58573fd27b4f0345ae196b1a6c69d0d" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetNonZeros</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a01787b806fc996c2ff3c1dcf9b2d54de"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::GetDataSize" ref="a01787b806fc996c2ff3c1dcf9b2d54de" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataSize</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac3d6a23829eb932f41de04c13de22f6b"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::GetIndex" ref="ac3d6a23829eb932f41de04c13de22f6b" args="(int i) const" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetIndex</b> (int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0c18c6428cc11d175b58205727a4c67d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::GetData" ref="a0c18c6428cc11d175b58205727a4c67d" args="(int i) const" -->
T *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetData</b> (int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a550784008495b254998193d94f75bff9"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::GetData" ref="a550784008495b254998193d94f75bff9" args="() const" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt; *&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetData</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad114c8c6e002a7a0def70f2413a32659"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Val" ref="ad114c8c6e002a7a0def70f2413a32659" args="(int i, int j)" -->
T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>Val</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a130c7307de87672e3878362386481775"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Val" ref="a130c7307de87672e3878362386481775" args="(int i, int j) const" -->
const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>Val</b> (int i, int j) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0073048e11b166e4152be498e9c497f6"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Value" ref="a0073048e11b166e4152be498e9c497f6" args="(int num_row, int i) const" -->
const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>Value</b> (int num_row, int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afce6920acaa6e6ca7ce340c2db8991ea"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Value" ref="afce6920acaa6e6ca7ce340c2db8991ea" args="(int num_row, int i)" -->
T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>Value</b> (int num_row, int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3534fb2c57f2640c9b8cd5a59e9da0e2"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Index" ref="a3534fb2c57f2640c9b8cd5a59e9da0e2" args="(int num_row, int i) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>Index</b> (int num_row, int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a34c0904b139171db42c46d46413f414f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Index" ref="a34c0904b139171db42c46d46413f414f" args="(int num_row, int i)" -->
int &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>Index</b> (int num_row, int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9fb98c93d5bee7a4be62e1a3e1bb5a46"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::SetData" ref="a9fb98c93d5bee7a4be62e1a3e1bb5a46" args="(int, int, Vector&lt; T, VectSparse, Allocator &gt; *)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetData</b> (int, int, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; *)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6e429aff71786b06033b047e6c277fab"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::SetData" ref="a6e429aff71786b06033b047e6c277fab" args="(int, int, T *, int *)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetData</b> (int, int, T *, int *)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1931ba4ea2000131dbc4bd4577fd9f00"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Nullify" ref="a1931ba4ea2000131dbc4bd4577fd9f00" args="(int i)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Nullify</b> (int i)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0cceba81597165b0e23ec0c20daed462"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Nullify" ref="a0cceba81597165b0e23ec0c20daed462" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Nullify</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a61ff8d09ea73a05eff4fb21e16fb1118"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Print" ref="a61ff8d09ea73a05eff4fb21e16fb1118" args="() const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Print</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac469920c36d5a57516a3e53c0c4e3126"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Assemble" ref="ac469920c36d5a57516a3e53c0c4e3126" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Assemble</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4b87eb77f9690ec605e9a1e7e1d155c5"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::RemoveSmallEntry" ref="a4b87eb77f9690ec605e9a1e7e1d155c5" args="(const T0 &amp;epsilon)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>RemoveSmallEntry</b> (const T0 &amp;epsilon)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af92baadfc393d70b6e3f3ecaa25d16c7"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::SetIdentity" ref="af92baadfc393d70b6e3f3ecaa25d16c7" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetIdentity</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a06fc483908e9219787dfc9331b07d828"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Zero" ref="a06fc483908e9219787dfc9331b07d828" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Zero</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a806a12c725733434b08b4276de7fbcee"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Fill" ref="a806a12c725733434b08b4276de7fbcee" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Fill</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae89973954b1f1372112ebb14045cf5d5"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Fill" ref="ae89973954b1f1372112ebb14045cf5d5" args="(const T0 &amp;x)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Fill</b> (const T0 &amp;x)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae7161fac73344a98f937abbf957fdbb3"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::FillRand" ref="ae7161fac73344a98f937abbf957fdbb3" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>FillRand</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a014e4dca632c4955660ad849db2cb363"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Write" ref="a014e4dca632c4955660ad849db2cb363" args="(string FileName) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Write</b> (string FileName) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afaaf09b0ad951e8b82603e2e6f4d79c4"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Write" ref="afaaf09b0ad951e8b82603e2e6f4d79c4" args="(ostream &amp;FileStream) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Write</b> (ostream &amp;FileStream) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adf0d3ec9d5ffeb1b3d46f523051d1412"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::WriteText" ref="adf0d3ec9d5ffeb1b3d46f523051d1412" args="(string FileName) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>WriteText</b> (string FileName) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6037e1907164259fa78d2abaa1380577"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::WriteText" ref="a6037e1907164259fa78d2abaa1380577" args="(ostream &amp;FileStream) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>WriteText</b> (ostream &amp;FileStream) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2e644c704eba4e3c531c403952189d86"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Read" ref="a2e644c704eba4e3c531c403952189d86" args="(string FileName)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Read</b> (string FileName)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="abeb6e31b268538d3c43fc8f795e994d4"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Read" ref="abeb6e31b268538d3c43fc8f795e994d4" args="(istream &amp;FileStream)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Read</b> (istream &amp;FileStream)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a22e85b66114b00ba89b1615bc1839b0c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::ReadText" ref="a22e85b66114b00ba89b1615bc1839b0c" args="(string FileName)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>ReadText</b> (string FileName)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4bac922eaed55ed72c4fcfac89b405ca"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::ReadText" ref="a4bac922eaed55ed72c4fcfac89b405ca" args="(istream &amp;FileStream)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>ReadText</b> (istream &amp;FileStream)</td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acc9d4e1d9bdb13e8449c7b2cc92dc3db"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::m_" ref="acc9d4e1d9bdb13e8449c7b2cc92dc3db" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db">m_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of rows. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a03fe364e494d6f0dd31186462681b3a4"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::n_" ref="a03fe364e494d6f0dd31186462681b3a4" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4">n_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of columns. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5d9a9091c665cea7f54ac8ba70d11fb6"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::val_" ref="a5d9a9091c665cea7f54ac8ba70d11fb6" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6">val_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">rows or columns <br/></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Prop, class Allocator&gt;<br/>
 class Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</h3>

<p>Row-major symmetric sparse-matrix class. </p>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8hxx_source.php#l00257">257</a> of file <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="a9bd699fbd7fe4a62e4216883a322b623"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Matrix" ref="a9bd699fbd7fe4a62e4216883a322b623" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_sparse.php">ArrayRowSymSparse</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td>  throw ()<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>Builds an empty matrix. </p>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l01672">1672</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aab5b076238e96f89b33b8c7a09f0b490"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::Matrix" ref="aab5b076238e96f89b33b8c7a09f0b490" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_sparse.php">ArrayRowSymSparse</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Constructor. </p>
<p>Builds a i by j matrix </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> values are not initialized. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l01685">1685</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="ac8bf415466c0dd580e635a66ae4110a6"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::AddInteraction" ref="ac8bf415466c0dd580e635a66ae4110a6" args="(int i, int j, const T &amp;val)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_sparse.php">ArrayRowSymSparse</a>, Allocator &gt;::AddInteraction </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const T &amp;&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds a coefficient in the matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>coefficient to add. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l01866">1866</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab4d1a9abf1e07714f5652eab9862475c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::AddInteractionColumn" ref="ab4d1a9abf1e07714f5652eab9862475c" args="(int, int, int *, T *)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_sparse.php">ArrayRowSymSparse</a>, Allocator &gt;::AddInteractionColumn </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nb</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int *&nbsp;</td>
          <td class="paramname"> <em>row_</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">T *&nbsp;</td>
          <td class="paramname"> <em>value_</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds coefficients in a column. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>nb</em>&nbsp;</td><td>number of coefficients to add. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>row_</em>&nbsp;</td><td>row numbers of coefficients. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>value_</em>&nbsp;</td><td>values of coefficients. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l01903">1903</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a8f6797869ae81e18c8c81aa00fbcdf82"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::AddInteractionColumn" ref="a8f6797869ae81e18c8c81aa00fbcdf82" args="(int i, int nb, const IVect &amp;row, const Vector&lt; T, VectFull, Alloc1 &gt; &amp;val)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Alloc1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_sparse.php">ArrayRowSymSparse</a>, Allocator &gt;::AddInteractionColumn </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nb</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>row</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds coefficients in a column. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>nb</em>&nbsp;</td><td>number of coefficients to add. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>row</em>&nbsp;</td><td>row numbers of coefficients. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>values of coefficients. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l01950">1950</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3c0b4b2b2f82744e655eb119080c309f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::AddInteractionRow" ref="a3c0b4b2b2f82744e655eb119080c309f" args="(int, int, int *, T *)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_sparse.php">ArrayRowSymSparse</a>, Allocator &gt;::AddInteractionRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nb</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int *&nbsp;</td>
          <td class="paramname"> <em>col_</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">T *&nbsp;</td>
          <td class="paramname"> <em>value_</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds coefficients in a row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>nb</em>&nbsp;</td><td>number of coefficients to add. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>col_</em>&nbsp;</td><td>column numbers of coefficients. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>value_</em>&nbsp;</td><td>values of coefficients. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l01882">1882</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af99cb559be02dcf98137b4dc5c4b29d1"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::AddInteractionRow" ref="af99cb559be02dcf98137b4dc5c4b29d1" args="(int i, int nb, const IVect &amp;col, const Vector&lt; T, VectFull, Alloc1 &gt; &amp;val)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class Alloc1 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_sparse.php">ArrayRowSymSparse</a>, Allocator &gt;::AddInteractionRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nb</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>col</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Adds coefficients in a row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>nb</em>&nbsp;</td><td>number of coefficients to add. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>col</em>&nbsp;</td><td>column numbers of coefficients. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>values of coefficients. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l01924">1924</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a94a7c41aa9fb70bc83fbadb66e0e7709"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::AssembleRow" ref="a94a7c41aa9fb70bc83fbadb66e0e7709" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_sparse.php">ArrayRowSymSparse</a>, Allocator &gt;::AssembleRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Assembles a column. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>If you are using the methods AddInteraction, you don't need to call that method. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l01852">1852</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af202c14cefd0cb751e4cf906ca5804e7"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::GetRowSize" ref="af202c14cefd0cb751e4cf906ca5804e7" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_sparse.php">ArrayRowSymSparse</a>, Allocator &gt;::GetRowSize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of non-zero entries of a row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of non-zero entries of the row i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l01828">1828</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a45ec07c98e6e55fdae57dda5b0c1cda7"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::operator()" ref="a45ec07c98e6e55fdae57dda5b0c1cda7" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T &amp; <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_sparse.php">ArrayRowSymSparse</a>, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a5aaa013260fab95db133652459b0bb2f">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l01738">1738</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a8d872a0e035a2afcc8e40736e8556c44"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::operator()" ref="a8d872a0e035a2afcc8e40736e8556c44" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_sparse.php">ArrayRowSymSparse</a>, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a41e1ef18aa87fce8062d36be5e634dfd">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l01705">1705</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a02d5086adf71d37c259c1a94a375bd57"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::ReallocateRow" ref="a02d5086adf71d37c259c1a94a375bd57" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_sparse.php">ArrayRowSymSparse</a>, Allocator &gt;::ReallocateRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates row i. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>new number of non-zero entries in the row. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Data may be lost. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l01776">1776</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af70fc4f3c8267b2a4f528e1fd3056020"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::ReplaceIndexRow" ref="af70fc4f3c8267b2a4f528e1fd3056020" args="(int i, IVect &amp;new_index)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_sparse.php">ArrayRowSymSparse</a>, Allocator &gt;::ReplaceIndexRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>new_index</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets column numbers of non-zero entries of a row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>new_index</em>&nbsp;</td><td>new column numbers. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l01815">1815</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad70cf7b6b6aadd9c1e51bc1944550d91"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::ResizeRow" ref="ad70cf7b6b6aadd9c1e51bc1944550d91" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_sparse.php">ArrayRowSymSparse</a>, Allocator &gt;::ResizeRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates row i. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>new number of non-zero entries in the row. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l01789">1789</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="abb74a0467a0295eb8964ac30c9e4922c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;::SwapRow" ref="abb74a0467a0295eb8964ac30c9e4922c" args="(int i, int i_)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_array_row_sym_sparse.php">ArrayRowSymSparse</a>, Allocator &gt;::SwapRow </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Swaps two rows. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>first row number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>second row number. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l01802">1802</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>matrix_sparse/<a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a></li>
<li>matrix_sparse/<a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
