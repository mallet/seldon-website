<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>share/MatrixFlag.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2010 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_SHARE_MATRIXFLAG_HXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 
<a name="l00023"></a>00023 <span class="keyword">namespace </span>Seldon
<a name="l00024"></a>00024 {
<a name="l00025"></a>00025 
<a name="l00026"></a>00026 
<a name="l00028"></a>00028   <span class="comment">// SELDONTRANSPOSE //</span>
<a name="l00030"></a>00030 <span class="comment"></span>
<a name="l00031"></a>00031 
<a name="l00032"></a><a class="code" href="class_seldon_1_1_seldon_transpose.php">00032</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>
<a name="l00033"></a>00033   {
<a name="l00034"></a>00034 <span class="preprocessor">#ifdef SELDON_WITH_BLAS</span>
<a name="l00035"></a>00035 <span class="preprocessor"></span>  <span class="keyword">protected</span>:
<a name="l00036"></a>00036     CBLAS_TRANSPOSE cblas_status_;
<a name="l00037"></a>00037 <span class="preprocessor">#endif</span>
<a name="l00038"></a>00038 <span class="preprocessor"></span>
<a name="l00039"></a>00039   <span class="keyword">protected</span>:
<a name="l00040"></a>00040     <span class="comment">// 0: Trans, 1: NoTrans, 2: ConjTrans.</span>
<a name="l00041"></a>00041     <span class="keywordtype">int</span> status_;
<a name="l00042"></a>00042 
<a name="l00043"></a>00043   <span class="keyword">public</span>:
<a name="l00044"></a>00044     <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>(<span class="keywordtype">int</span> status);
<a name="l00045"></a>00045 <span class="preprocessor">#ifdef SELDON_WITH_BLAS</span>
<a name="l00046"></a>00046 <span class="preprocessor"></span>    <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE status);
<a name="l00047"></a>00047     operator CBLAS_TRANSPOSE() <span class="keyword">const</span>;
<a name="l00048"></a>00048 <span class="preprocessor">#endif</span>
<a name="l00049"></a>00049 <span class="preprocessor"></span>    <span class="keywordtype">char</span> Char() <span class="keyword">const</span>;
<a name="l00050"></a>00050     <span class="keywordtype">char</span> RevChar() <span class="keyword">const</span>;
<a name="l00051"></a>00051     <span class="keywordtype">bool</span> Trans() <span class="keyword">const</span>;
<a name="l00052"></a>00052     <span class="keywordtype">bool</span> NoTrans() <span class="keyword">const</span>;
<a name="l00053"></a>00053     <span class="keywordtype">bool</span> ConjTrans() <span class="keyword">const</span>;
<a name="l00054"></a>00054   };
<a name="l00055"></a>00055 
<a name="l00056"></a>00056 
<a name="l00057"></a><a class="code" href="class_seldon_1_1class___seldon_trans.php">00057</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1class___seldon_trans.php">class_SeldonTrans</a>: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>
<a name="l00058"></a>00058   {
<a name="l00059"></a>00059   <span class="keyword">public</span>:
<a name="l00060"></a>00060     <a class="code" href="class_seldon_1_1class___seldon_trans.php">class_SeldonTrans</a>();
<a name="l00061"></a>00061   };
<a name="l00062"></a>00062 
<a name="l00063"></a>00063 
<a name="l00064"></a><a class="code" href="class_seldon_1_1class___seldon_no_trans.php">00064</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1class___seldon_no_trans.php">class_SeldonNoTrans</a>: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>
<a name="l00065"></a>00065   {
<a name="l00066"></a>00066   <span class="keyword">public</span>:
<a name="l00067"></a>00067     <a class="code" href="class_seldon_1_1class___seldon_no_trans.php">class_SeldonNoTrans</a>();
<a name="l00068"></a>00068   };
<a name="l00069"></a>00069 
<a name="l00070"></a>00070 
<a name="l00071"></a><a class="code" href="class_seldon_1_1class___seldon_conj_trans.php">00071</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1class___seldon_conj_trans.php">class_SeldonConjTrans</a>: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>
<a name="l00072"></a>00072   {
<a name="l00073"></a>00073   <span class="keyword">public</span>:
<a name="l00074"></a>00074     <a class="code" href="class_seldon_1_1class___seldon_conj_trans.php">class_SeldonConjTrans</a>();
<a name="l00075"></a>00075   };
<a name="l00076"></a>00076 
<a name="l00077"></a>00077 
<a name="l00078"></a>00078   <span class="keyword">extern</span> <a class="code" href="class_seldon_1_1class___seldon_trans.php">class_SeldonTrans</a> SeldonTrans;
<a name="l00079"></a>00079   <span class="keyword">extern</span> <a class="code" href="class_seldon_1_1class___seldon_no_trans.php">class_SeldonNoTrans</a> SeldonNoTrans;
<a name="l00080"></a>00080   <span class="keyword">extern</span> <a class="code" href="class_seldon_1_1class___seldon_conj_trans.php">class_SeldonConjTrans</a> SeldonConjTrans;
<a name="l00081"></a>00081 
<a name="l00082"></a>00082 
<a name="l00084"></a>00084   <span class="comment">// SELDONDIAG //</span>
<a name="l00086"></a>00086 <span class="comment"></span>
<a name="l00087"></a>00087 
<a name="l00088"></a><a class="code" href="class_seldon_1_1_seldon_diag.php">00088</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_seldon_diag.php">SeldonDiag</a>
<a name="l00089"></a>00089   {
<a name="l00090"></a>00090 <span class="preprocessor">#ifdef SELDON_WITH_BLAS</span>
<a name="l00091"></a>00091 <span class="preprocessor"></span>  <span class="keyword">protected</span>:
<a name="l00092"></a>00092     CBLAS_DIAG cblas_status_;
<a name="l00093"></a>00093 <span class="preprocessor">#endif</span>
<a name="l00094"></a>00094 <span class="preprocessor"></span>
<a name="l00095"></a>00095   <span class="keyword">protected</span>:
<a name="l00096"></a>00096     <span class="comment">// 0: NonUnit, 1: Unit.</span>
<a name="l00097"></a>00097     <span class="keywordtype">int</span> status_;
<a name="l00098"></a>00098 
<a name="l00099"></a>00099   <span class="keyword">public</span>:
<a name="l00100"></a>00100     <a class="code" href="class_seldon_1_1_seldon_diag.php">SeldonDiag</a>(<span class="keywordtype">int</span> status);
<a name="l00101"></a>00101 <span class="preprocessor">#ifdef SELDON_WITH_BLAS</span>
<a name="l00102"></a>00102 <span class="preprocessor"></span>    operator CBLAS_DIAG() <span class="keyword">const</span>;
<a name="l00103"></a>00103 <span class="preprocessor">#endif</span>
<a name="l00104"></a>00104 <span class="preprocessor"></span>    <span class="keywordtype">char</span> Char() <span class="keyword">const</span>;
<a name="l00105"></a>00105     <span class="keywordtype">bool</span> NonUnit() <span class="keyword">const</span>;
<a name="l00106"></a>00106     <span class="keywordtype">bool</span> Unit() <span class="keyword">const</span>;
<a name="l00107"></a>00107   };
<a name="l00108"></a>00108 
<a name="l00109"></a>00109 
<a name="l00110"></a><a class="code" href="class_seldon_1_1class___seldon_non_unit.php">00110</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1class___seldon_non_unit.php">class_SeldonNonUnit</a>: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_seldon_diag.php">SeldonDiag</a>
<a name="l00111"></a>00111   {
<a name="l00112"></a>00112   <span class="keyword">public</span>:
<a name="l00113"></a>00113     <a class="code" href="class_seldon_1_1class___seldon_non_unit.php">class_SeldonNonUnit</a>();
<a name="l00114"></a>00114   };
<a name="l00115"></a>00115 
<a name="l00116"></a>00116 
<a name="l00117"></a><a class="code" href="class_seldon_1_1class___seldon_unit.php">00117</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1class___seldon_unit.php">class_SeldonUnit</a>: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_seldon_diag.php">SeldonDiag</a>
<a name="l00118"></a>00118   {
<a name="l00119"></a>00119   <span class="keyword">public</span>:
<a name="l00120"></a>00120     <a class="code" href="class_seldon_1_1class___seldon_unit.php">class_SeldonUnit</a>();
<a name="l00121"></a>00121   };
<a name="l00122"></a>00122 
<a name="l00123"></a>00123 
<a name="l00124"></a>00124   <span class="keyword">extern</span> <a class="code" href="class_seldon_1_1class___seldon_non_unit.php">class_SeldonNonUnit</a> SeldonNonUnit;
<a name="l00125"></a>00125   <span class="keyword">extern</span> <a class="code" href="class_seldon_1_1class___seldon_unit.php">class_SeldonUnit</a> SeldonUnit;
<a name="l00126"></a>00126 
<a name="l00127"></a>00127 
<a name="l00129"></a>00129   <span class="comment">// SELDONUPLO //</span>
<a name="l00131"></a>00131 <span class="comment"></span>
<a name="l00132"></a>00132 
<a name="l00133"></a><a class="code" href="class_seldon_1_1_seldon_uplo.php">00133</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_seldon_uplo.php">SeldonUplo</a>
<a name="l00134"></a>00134   {
<a name="l00135"></a>00135 <span class="preprocessor">#ifdef SELDON_WITH_BLAS</span>
<a name="l00136"></a>00136 <span class="preprocessor"></span>  <span class="keyword">protected</span>:
<a name="l00137"></a>00137     CBLAS_UPLO cblas_status_;
<a name="l00138"></a>00138 <span class="preprocessor">#endif</span>
<a name="l00139"></a>00139 <span class="preprocessor"></span>
<a name="l00140"></a>00140   <span class="keyword">protected</span>:
<a name="l00141"></a>00141     <span class="comment">// 0: Upper, 1: Lower.</span>
<a name="l00142"></a>00142     <span class="keywordtype">int</span> status_;
<a name="l00143"></a>00143 
<a name="l00144"></a>00144   <span class="keyword">public</span>:
<a name="l00145"></a>00145     <a class="code" href="class_seldon_1_1_seldon_uplo.php">SeldonUplo</a>(<span class="keywordtype">int</span> status);
<a name="l00146"></a>00146 <span class="preprocessor">#ifdef SELDON_WITH_BLAS</span>
<a name="l00147"></a>00147 <span class="preprocessor"></span>    operator CBLAS_UPLO() <span class="keyword">const</span>;
<a name="l00148"></a>00148 <span class="preprocessor">#endif</span>
<a name="l00149"></a>00149 <span class="preprocessor"></span>    operator char() <span class="keyword">const</span>;
<a name="l00150"></a>00150     <span class="keywordtype">bool</span> Upper() <span class="keyword">const</span>;
<a name="l00151"></a>00151     <span class="keywordtype">bool</span> Lower() <span class="keyword">const</span>;
<a name="l00152"></a>00152     <span class="keywordtype">char</span> Char() <span class="keyword">const</span>;
<a name="l00153"></a>00153     <span class="keywordtype">char</span> RevChar() <span class="keyword">const</span>;
<a name="l00154"></a>00154   };
<a name="l00155"></a>00155 
<a name="l00156"></a>00156 
<a name="l00157"></a>00157   <span class="keyword">extern</span> <a class="code" href="class_seldon_1_1_seldon_uplo.php">SeldonUplo</a> SeldonUpper;
<a name="l00158"></a>00158   <span class="keyword">extern</span> <a class="code" href="class_seldon_1_1_seldon_uplo.php">SeldonUplo</a> SeldonLower;
<a name="l00159"></a>00159 
<a name="l00160"></a>00160 
<a name="l00162"></a>00162   <span class="comment">// SELDONNORM //</span>
<a name="l00164"></a>00164 <span class="comment"></span>
<a name="l00165"></a>00165 
<a name="l00166"></a><a class="code" href="class_seldon_1_1_seldon_norm.php">00166</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_seldon_norm.php">SeldonNorm</a>
<a name="l00167"></a>00167   {
<a name="l00168"></a>00168   <span class="keyword">protected</span>:
<a name="l00169"></a>00169     <span class="comment">// 0: Infinity-norm, 1: 1-norm.</span>
<a name="l00170"></a>00170     <span class="keywordtype">int</span> status_;
<a name="l00171"></a>00171 
<a name="l00172"></a>00172   <span class="keyword">public</span>:
<a name="l00173"></a>00173     <a class="code" href="class_seldon_1_1_seldon_norm.php">SeldonNorm</a>(<span class="keywordtype">int</span> status);
<a name="l00174"></a>00174     operator char() <span class="keyword">const</span>;
<a name="l00175"></a>00175     <span class="keywordtype">char</span> Char() <span class="keyword">const</span>;
<a name="l00176"></a>00176     <span class="keywordtype">char</span> RevChar() <span class="keyword">const</span>;
<a name="l00177"></a>00177   };
<a name="l00178"></a>00178 
<a name="l00179"></a>00179 
<a name="l00180"></a>00180   <span class="keyword">extern</span> <a class="code" href="class_seldon_1_1_seldon_norm.php">SeldonNorm</a> SeldonNormInf;
<a name="l00181"></a>00181   <span class="keyword">extern</span> <a class="code" href="class_seldon_1_1_seldon_norm.php">SeldonNorm</a> SeldonNorm1;
<a name="l00182"></a>00182 
<a name="l00183"></a>00183 
<a name="l00185"></a>00185   <span class="comment">// SELDONCONJUGATE //</span>
<a name="l00187"></a>00187 <span class="comment"></span>
<a name="l00188"></a>00188 
<a name="l00189"></a><a class="code" href="class_seldon_1_1_seldon_conjugate.php">00189</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_seldon_conjugate.php">SeldonConjugate</a>
<a name="l00190"></a>00190   {
<a name="l00191"></a>00191   <span class="keyword">protected</span>:
<a name="l00192"></a>00192     <span class="comment">// false: Unconj, true: Conj.</span>
<a name="l00193"></a>00193     <span class="keywordtype">bool</span> status_;
<a name="l00194"></a>00194 
<a name="l00195"></a>00195   <span class="keyword">public</span>:
<a name="l00196"></a>00196     <a class="code" href="class_seldon_1_1_seldon_conjugate.php">SeldonConjugate</a>(<span class="keywordtype">bool</span> status);
<a name="l00197"></a>00197     <span class="keyword">inline</span> <span class="keywordtype">bool</span> Conj() <span class="keyword">const</span>;
<a name="l00198"></a>00198   };
<a name="l00199"></a>00199 
<a name="l00200"></a>00200 
<a name="l00201"></a>00201   <span class="keyword">extern</span> <a class="code" href="class_seldon_1_1_seldon_conjugate.php">SeldonConjugate</a> SeldonUnconj;
<a name="l00202"></a>00202   <span class="keyword">extern</span> <a class="code" href="class_seldon_1_1_seldon_conjugate.php">SeldonConjugate</a> SeldonConj;
<a name="l00203"></a>00203 
<a name="l00204"></a>00204 
<a name="l00206"></a>00206   <span class="comment">// SELDONSIDE //</span>
<a name="l00208"></a>00208 <span class="comment"></span>
<a name="l00209"></a>00209 
<a name="l00210"></a><a class="code" href="class_seldon_1_1_seldon_side.php">00210</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_seldon_side.php">SeldonSide</a>
<a name="l00211"></a>00211   {
<a name="l00212"></a>00212 <span class="preprocessor">#ifdef SELDON_WITH_BLAS</span>
<a name="l00213"></a>00213 <span class="preprocessor"></span>  <span class="keyword">protected</span>:
<a name="l00214"></a>00214     CBLAS_SIDE cblas_status_;
<a name="l00215"></a>00215 <span class="preprocessor">#endif</span>
<a name="l00216"></a>00216 <span class="preprocessor"></span>
<a name="l00217"></a>00217   <span class="keyword">protected</span>:
<a name="l00218"></a>00218     <span class="comment">// 0: Left, 1: Right.</span>
<a name="l00219"></a>00219     <span class="keywordtype">int</span> status_;
<a name="l00220"></a>00220 
<a name="l00221"></a>00221   <span class="keyword">public</span>:
<a name="l00222"></a>00222     <a class="code" href="class_seldon_1_1_seldon_side.php">SeldonSide</a>(<span class="keywordtype">int</span> status);
<a name="l00223"></a>00223 <span class="preprocessor">#ifdef SELDON_WITH_BLAS</span>
<a name="l00224"></a>00224 <span class="preprocessor"></span>    <a class="code" href="class_seldon_1_1_seldon_side.php">SeldonSide</a>(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_SIDE status);
<a name="l00225"></a>00225     operator CBLAS_SIDE() <span class="keyword">const</span>;
<a name="l00226"></a>00226 <span class="preprocessor">#endif</span>
<a name="l00227"></a>00227 <span class="preprocessor"></span>    <span class="keywordtype">bool</span> Left() <span class="keyword">const</span>;
<a name="l00228"></a>00228     <span class="keywordtype">bool</span> Right() <span class="keyword">const</span>;
<a name="l00229"></a>00229   };
<a name="l00230"></a>00230 
<a name="l00231"></a>00231 
<a name="l00232"></a><a class="code" href="class_seldon_1_1class___seldon_left.php">00232</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1class___seldon_left.php">class_SeldonLeft</a>: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_seldon_side.php">SeldonSide</a>
<a name="l00233"></a>00233   {
<a name="l00234"></a>00234   <span class="keyword">public</span>:
<a name="l00235"></a>00235     <a class="code" href="class_seldon_1_1class___seldon_left.php">class_SeldonLeft</a>();
<a name="l00236"></a>00236   };
<a name="l00237"></a>00237 
<a name="l00238"></a>00238 
<a name="l00239"></a><a class="code" href="class_seldon_1_1class___seldon_right.php">00239</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1class___seldon_right.php">class_SeldonRight</a>: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_seldon_side.php">SeldonSide</a>
<a name="l00240"></a>00240   {
<a name="l00241"></a>00241   <span class="keyword">public</span>:
<a name="l00242"></a>00242     <a class="code" href="class_seldon_1_1class___seldon_right.php">class_SeldonRight</a>();
<a name="l00243"></a>00243   };
<a name="l00244"></a>00244 
<a name="l00245"></a>00245 
<a name="l00246"></a>00246   <span class="keyword">extern</span> <a class="code" href="class_seldon_1_1class___seldon_left.php">class_SeldonLeft</a> SeldonLeft;
<a name="l00247"></a>00247   <span class="keyword">extern</span> <a class="code" href="class_seldon_1_1class___seldon_right.php">class_SeldonRight</a> SeldonRight;
<a name="l00248"></a>00248 
<a name="l00249"></a>00249 
<a name="l00250"></a>00250 }
<a name="l00251"></a>00251 
<a name="l00252"></a>00252 
<a name="l00253"></a>00253 <span class="preprocessor">#define SELDON_FILE_SHARE_MATRIXFLAG_HXX</span>
<a name="l00254"></a>00254 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
