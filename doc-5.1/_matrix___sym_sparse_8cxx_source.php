<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix_sparse/Matrix_SymSparse.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_SYMSPARSE_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;Matrix_SymSparse.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="keyword">namespace </span>Seldon
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 
<a name="l00029"></a>00029   <span class="comment">/****************</span>
<a name="l00030"></a>00030 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00031"></a>00031 <span class="comment">   ****************/</span>
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00035"></a>00035 
<a name="l00038"></a>00038   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00039"></a>00039   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaffa73e2f7d10d2f2dfff4a701e72405" title="Default constructor.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::Matrix_SymSparse</a>():
<a name="l00040"></a>00040     Matrix_Base&lt;T, Allocator&gt;()
<a name="l00041"></a>00041   {
<a name="l00042"></a>00042     nz_ = 0;
<a name="l00043"></a>00043     ptr_ = NULL;
<a name="l00044"></a>00044     ind_ = NULL;
<a name="l00045"></a>00045   }
<a name="l00046"></a>00046 
<a name="l00047"></a>00047 
<a name="l00049"></a>00049 
<a name="l00055"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#afa0d65cf9cf27bd033588a54dae20f09">00055</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00056"></a>00056   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaffa73e2f7d10d2f2dfff4a701e72405" title="Default constructor.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00057"></a>00057 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaffa73e2f7d10d2f2dfff4a701e72405" title="Default constructor.">  ::Matrix_SymSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j): <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, i)
<a name="l00058"></a>00058   {
<a name="l00059"></a>00059     nz_ = 0;
<a name="l00060"></a>00060     ptr_ = NULL;
<a name="l00061"></a>00061     ind_ = NULL;
<a name="l00062"></a>00062   }
<a name="l00063"></a>00063 
<a name="l00064"></a>00064 
<a name="l00066"></a>00066 
<a name="l00075"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a14fd7e8b9ecea1af9d25faf494dc6431">00075</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00076"></a>00076   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaffa73e2f7d10d2f2dfff4a701e72405" title="Default constructor.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00077"></a>00077 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaffa73e2f7d10d2f2dfff4a701e72405" title="Default constructor.">  Matrix_SymSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> nz):
<a name="l00078"></a>00078     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, i)
<a name="l00079"></a>00079   {
<a name="l00080"></a>00080     this-&gt;nz_ = nz;
<a name="l00081"></a>00081 
<a name="l00082"></a>00082 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00083"></a>00083 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (static_cast&lt;long int&gt;(2 * nz_ - 2) / static_cast&lt;long int&gt;(i + 1)
<a name="l00084"></a>00084         &gt;= static_cast&lt;long int&gt;(i))
<a name="l00085"></a>00085       {
<a name="l00086"></a>00086         this-&gt;m_ = 0;
<a name="l00087"></a>00087         this-&gt;n_ = 0;
<a name="l00088"></a>00088         nz_ = 0;
<a name="l00089"></a>00089         ptr_ = NULL;
<a name="l00090"></a>00090         ind_ = NULL;
<a name="l00091"></a>00091         this-&gt;data_ = NULL;
<a name="l00092"></a>00092         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, int)&quot;</span>,
<a name="l00093"></a>00093                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00094"></a>00094                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; values) than elements in the upper&quot;</span>)
<a name="l00095"></a>00095                        + <span class="stringliteral">&quot; part of the matrix (&quot;</span>
<a name="l00096"></a>00096                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00097"></a>00097       }
<a name="l00098"></a>00098 <span class="preprocessor">#endif</span>
<a name="l00099"></a>00099 <span class="preprocessor"></span>
<a name="l00100"></a>00100 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00101"></a>00101 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00102"></a>00102       {
<a name="l00103"></a>00103 <span class="preprocessor">#endif</span>
<a name="l00104"></a>00104 <span class="preprocessor"></span>
<a name="l00105"></a>00105         ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(i + 1, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00106"></a>00106 
<a name="l00107"></a>00107 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00108"></a>00108 <span class="preprocessor"></span>      }
<a name="l00109"></a>00109     <span class="keywordflow">catch</span> (...)
<a name="l00110"></a>00110       {
<a name="l00111"></a>00111         this-&gt;m_ = 0;
<a name="l00112"></a>00112         this-&gt;n_ = 0;
<a name="l00113"></a>00113         nz_ = 0;
<a name="l00114"></a>00114         ptr_ = NULL;
<a name="l00115"></a>00115         ind_ = NULL;
<a name="l00116"></a>00116         this-&gt;data_ = NULL;
<a name="l00117"></a>00117       }
<a name="l00118"></a>00118     <span class="keywordflow">if</span> (ptr_ == NULL)
<a name="l00119"></a>00119       {
<a name="l00120"></a>00120         this-&gt;m_ = 0;
<a name="l00121"></a>00121         this-&gt;n_ = 0;
<a name="l00122"></a>00122         nz_ = 0;
<a name="l00123"></a>00123         ind_ = NULL;
<a name="l00124"></a>00124         this-&gt;data_ = NULL;
<a name="l00125"></a>00125       }
<a name="l00126"></a>00126     <span class="keywordflow">if</span> (ptr_ == NULL &amp;&amp; i != 0)
<a name="l00127"></a>00127       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, int)&quot;</span>,
<a name="l00128"></a>00128                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00129"></a>00129                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (i+1) ) + <span class="stringliteral">&quot; bytes to store &quot;</span>
<a name="l00130"></a>00130                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i+1) + <span class="stringliteral">&quot; row or column start indices, for a &quot;</span>
<a name="l00131"></a>00131                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00132"></a>00132 <span class="preprocessor">#endif</span>
<a name="l00133"></a>00133 <span class="preprocessor"></span>
<a name="l00134"></a>00134 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00135"></a>00135 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00136"></a>00136       {
<a name="l00137"></a>00137 <span class="preprocessor">#endif</span>
<a name="l00138"></a>00138 <span class="preprocessor"></span>
<a name="l00139"></a>00139         ind_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(nz_, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00140"></a>00140 
<a name="l00141"></a>00141 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00142"></a>00142 <span class="preprocessor"></span>      }
<a name="l00143"></a>00143     <span class="keywordflow">catch</span> (...)
<a name="l00144"></a>00144       {
<a name="l00145"></a>00145         this-&gt;m_ = 0;
<a name="l00146"></a>00146         this-&gt;n_ = 0;
<a name="l00147"></a>00147         nz_ = 0;
<a name="l00148"></a>00148         free(ptr_);
<a name="l00149"></a>00149         ptr_ = NULL;
<a name="l00150"></a>00150         ind_ = NULL;
<a name="l00151"></a>00151         this-&gt;data_ = NULL;
<a name="l00152"></a>00152       }
<a name="l00153"></a>00153     <span class="keywordflow">if</span> (ind_ == NULL)
<a name="l00154"></a>00154       {
<a name="l00155"></a>00155         this-&gt;m_ = 0;
<a name="l00156"></a>00156         this-&gt;n_ = 0;
<a name="l00157"></a>00157         nz_ = 0;
<a name="l00158"></a>00158         free(ptr_);
<a name="l00159"></a>00159         ptr_ = NULL;
<a name="l00160"></a>00160         this-&gt;data_ = NULL;
<a name="l00161"></a>00161       }
<a name="l00162"></a>00162     <span class="keywordflow">if</span> (ind_ == NULL &amp;&amp; i != 0)
<a name="l00163"></a>00163       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, int)&quot;</span>,
<a name="l00164"></a>00164                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * nz)
<a name="l00165"></a>00165                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00166"></a>00166                      + <span class="stringliteral">&quot; row or column indices, for a &quot;</span>
<a name="l00167"></a>00167                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00168"></a>00168 <span class="preprocessor">#endif</span>
<a name="l00169"></a>00169 <span class="preprocessor"></span>
<a name="l00170"></a>00170 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00171"></a>00171 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00172"></a>00172       {
<a name="l00173"></a>00173 <span class="preprocessor">#endif</span>
<a name="l00174"></a>00174 <span class="preprocessor"></span>
<a name="l00175"></a>00175         this-&gt;data_ = this-&gt;allocator_.allocate(nz_, <span class="keyword">this</span>);
<a name="l00176"></a>00176 
<a name="l00177"></a>00177 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00178"></a>00178 <span class="preprocessor"></span>      }
<a name="l00179"></a>00179     <span class="keywordflow">catch</span> (...)
<a name="l00180"></a>00180       {
<a name="l00181"></a>00181         this-&gt;m_ = 0;
<a name="l00182"></a>00182         this-&gt;n_ = 0;
<a name="l00183"></a>00183         free(ptr_);
<a name="l00184"></a>00184         ptr_ = NULL;
<a name="l00185"></a>00185         free(ind_);
<a name="l00186"></a>00186         ind_ = NULL;
<a name="l00187"></a>00187         this-&gt;data_ = NULL;
<a name="l00188"></a>00188       }
<a name="l00189"></a>00189     <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00190"></a>00190       {
<a name="l00191"></a>00191         this-&gt;m_ = 0;
<a name="l00192"></a>00192         this-&gt;n_ = 0;
<a name="l00193"></a>00193         free(ptr_);
<a name="l00194"></a>00194         ptr_ = NULL;
<a name="l00195"></a>00195         free(ind_);
<a name="l00196"></a>00196         ind_ = NULL;
<a name="l00197"></a>00197       }
<a name="l00198"></a>00198     <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; i != 0)
<a name="l00199"></a>00199       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, int)&quot;</span>,
<a name="l00200"></a>00200                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * nz)
<a name="l00201"></a>00201                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz) + <span class="stringliteral">&quot; values, for a &quot;</span>
<a name="l00202"></a>00202                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00203"></a>00203 <span class="preprocessor">#endif</span>
<a name="l00204"></a>00204 <span class="preprocessor"></span>
<a name="l00205"></a>00205   }
<a name="l00206"></a>00206 
<a name="l00207"></a>00207 
<a name="l00209"></a>00209 
<a name="l00221"></a>00221   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00222"></a>00222   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00223"></a>00223             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00224"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a626e591de87845d39c44796cd9b19bc9">00224</a>             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00225"></a>00225   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaffa73e2f7d10d2f2dfff4a701e72405" title="Default constructor.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00226"></a>00226 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaffa73e2f7d10d2f2dfff4a701e72405" title="Default constructor.">  Matrix_SymSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00227"></a>00227                    <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; values,
<a name="l00228"></a>00228                    <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; ptr,
<a name="l00229"></a>00229                    <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; ind):
<a name="l00230"></a>00230     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, j)
<a name="l00231"></a>00231   {
<a name="l00232"></a>00232     nz_ = values.GetLength();
<a name="l00233"></a>00233 
<a name="l00234"></a>00234 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00235"></a>00235 <span class="preprocessor"></span>    <span class="comment">// Checks whether vector sizes are acceptable.</span>
<a name="l00236"></a>00236 
<a name="l00237"></a>00237     <span class="keywordflow">if</span> (ind.GetLength() != nz_)
<a name="l00238"></a>00238       {
<a name="l00239"></a>00239         this-&gt;m_ = 0;
<a name="l00240"></a>00240         this-&gt;n_ = 0;
<a name="l00241"></a>00241         nz_ = 0;
<a name="l00242"></a>00242         ptr_ = NULL;
<a name="l00243"></a>00243         ind_ = NULL;
<a name="l00244"></a>00244         this-&gt;data_ = NULL;
<a name="l00245"></a>00245         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, &quot;</span>)
<a name="l00246"></a>00246                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00247"></a>00247                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz_) + <span class="stringliteral">&quot; values but &quot;</span>
<a name="l00248"></a>00248                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(ind.GetLength()) + <span class="stringliteral">&quot; row or column indices.&quot;</span>);
<a name="l00249"></a>00249       }
<a name="l00250"></a>00250 
<a name="l00251"></a>00251     <span class="keywordflow">if</span> (ptr.GetLength() - 1 != i)
<a name="l00252"></a>00252       {
<a name="l00253"></a>00253         this-&gt;m_ = 0;
<a name="l00254"></a>00254         this-&gt;n_ = 0;
<a name="l00255"></a>00255         nz_ = 0;
<a name="l00256"></a>00256         ptr_ = NULL;
<a name="l00257"></a>00257         ind_ = NULL;
<a name="l00258"></a>00258         this-&gt;data_ = NULL;
<a name="l00259"></a>00259         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, &quot;</span>)
<a name="l00260"></a>00260                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00261"></a>00261                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The vector of start indices contains &quot;</span>)
<a name="l00262"></a>00262                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(ptr.GetLength()-1) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column &quot;</span>)
<a name="l00263"></a>00263                        + string(<span class="stringliteral">&quot;start  indices (plus the number of non-zero&quot;</span>)
<a name="l00264"></a>00264                        + <span class="stringliteral">&quot; entries) but there are &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i)
<a name="l00265"></a>00265                        + <span class="stringliteral">&quot; rows or columns (&quot;</span>
<a name="l00266"></a>00266                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix).&quot;</span>);
<a name="l00267"></a>00267       }
<a name="l00268"></a>00268 
<a name="l00269"></a>00269     <span class="keywordflow">if</span> (static_cast&lt;long int&gt;(2 * nz_ - 2) / <span class="keyword">static_cast&lt;</span><span class="keywordtype">long</span> <span class="keywordtype">int</span><span class="keyword">&gt;</span>(i + 1)
<a name="l00270"></a>00270         &gt;= static_cast&lt;long int&gt;(i))
<a name="l00271"></a>00271       {
<a name="l00272"></a>00272         this-&gt;m_ = 0;
<a name="l00273"></a>00273         this-&gt;n_ = 0;
<a name="l00274"></a>00274         nz_ = 0;
<a name="l00275"></a>00275         ptr_ = NULL;
<a name="l00276"></a>00276         ind_ = NULL;
<a name="l00277"></a>00277         this-&gt;data_ = NULL;
<a name="l00278"></a>00278         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, &quot;</span>)
<a name="l00279"></a>00279                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00280"></a>00280                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>)
<a name="l00281"></a>00281                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(values.GetLength())
<a name="l00282"></a>00282                        + <span class="stringliteral">&quot; values) than elements in the matrix (&quot;</span>
<a name="l00283"></a>00283                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00284"></a>00284       }
<a name="l00285"></a>00285 <span class="preprocessor">#endif</span>
<a name="l00286"></a>00286 <span class="preprocessor"></span>
<a name="l00287"></a>00287     this-&gt;ptr_ = ptr.GetData();
<a name="l00288"></a>00288     this-&gt;ind_ = ind.GetData();
<a name="l00289"></a>00289     this-&gt;data_ = values.GetData();
<a name="l00290"></a>00290 
<a name="l00291"></a>00291     ptr.Nullify();
<a name="l00292"></a>00292     ind.Nullify();
<a name="l00293"></a>00293     values.Nullify();
<a name="l00294"></a>00294   }
<a name="l00295"></a>00295 
<a name="l00296"></a>00296 
<a name="l00298"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a6895a30236bf5f7ec632cba1916c2e4b">00298</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00299"></a>00299   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaffa73e2f7d10d2f2dfff4a701e72405" title="Default constructor.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00300"></a>00300 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaffa73e2f7d10d2f2dfff4a701e72405" title="Default constructor.">  Matrix_SymSparse</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00301"></a>00301   {
<a name="l00302"></a>00302     this-&gt;m_ = 0;
<a name="l00303"></a>00303     this-&gt;n_ = 0;
<a name="l00304"></a>00304     this-&gt;nz_ = 0;
<a name="l00305"></a>00305     ptr_ = NULL;
<a name="l00306"></a>00306     ind_ = NULL;
<a name="l00307"></a>00307     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a13715cb4e64fbe58e24fa86897131e25" title="Copies a matrix.">Copy</a>(A);
<a name="l00308"></a>00308   }
<a name="l00309"></a>00309 
<a name="l00310"></a>00310 
<a name="l00311"></a>00311   <span class="comment">/**************</span>
<a name="l00312"></a>00312 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00313"></a>00313 <span class="comment">   **************/</span>
<a name="l00314"></a>00314 
<a name="l00315"></a>00315 
<a name="l00317"></a>00317   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00318"></a>00318   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a6f46818c1d832ff9be950a5b6f0fc9c7" title="Destructor.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::~Matrix_SymSparse</a>()
<a name="l00319"></a>00319   {
<a name="l00320"></a>00320     this-&gt;m_ = 0;
<a name="l00321"></a>00321     this-&gt;n_ = 0;
<a name="l00322"></a>00322 
<a name="l00323"></a>00323 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00324"></a>00324 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00325"></a>00325       {
<a name="l00326"></a>00326 <span class="preprocessor">#endif</span>
<a name="l00327"></a>00327 <span class="preprocessor"></span>
<a name="l00328"></a>00328         <span class="keywordflow">if</span> (ptr_ != NULL)
<a name="l00329"></a>00329           {
<a name="l00330"></a>00330             free(ptr_);
<a name="l00331"></a>00331             ptr_ = NULL;
<a name="l00332"></a>00332           }
<a name="l00333"></a>00333 
<a name="l00334"></a>00334 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00335"></a>00335 <span class="preprocessor"></span>      }
<a name="l00336"></a>00336     <span class="keywordflow">catch</span> (...)
<a name="l00337"></a>00337       {
<a name="l00338"></a>00338         ptr_ = NULL;
<a name="l00339"></a>00339       }
<a name="l00340"></a>00340 <span class="preprocessor">#endif</span>
<a name="l00341"></a>00341 <span class="preprocessor"></span>
<a name="l00342"></a>00342 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00343"></a>00343 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00344"></a>00344       {
<a name="l00345"></a>00345 <span class="preprocessor">#endif</span>
<a name="l00346"></a>00346 <span class="preprocessor"></span>
<a name="l00347"></a>00347         <span class="keywordflow">if</span> (ind_ != NULL)
<a name="l00348"></a>00348           {
<a name="l00349"></a>00349             free(ind_);
<a name="l00350"></a>00350             ind_ = NULL;
<a name="l00351"></a>00351           }
<a name="l00352"></a>00352 
<a name="l00353"></a>00353 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00354"></a>00354 <span class="preprocessor"></span>      }
<a name="l00355"></a>00355     <span class="keywordflow">catch</span> (...)
<a name="l00356"></a>00356       {
<a name="l00357"></a>00357         ind_ = NULL;
<a name="l00358"></a>00358       }
<a name="l00359"></a>00359 <span class="preprocessor">#endif</span>
<a name="l00360"></a>00360 <span class="preprocessor"></span>
<a name="l00361"></a>00361 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00362"></a>00362 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00363"></a>00363       {
<a name="l00364"></a>00364 <span class="preprocessor">#endif</span>
<a name="l00365"></a>00365 <span class="preprocessor"></span>
<a name="l00366"></a>00366         <span class="keywordflow">if</span> (this-&gt;data_ != NULL)
<a name="l00367"></a>00367           {
<a name="l00368"></a>00368             this-&gt;allocator_.deallocate(this-&gt;data_, nz_);
<a name="l00369"></a>00369             this-&gt;data_ = NULL;
<a name="l00370"></a>00370           }
<a name="l00371"></a>00371 
<a name="l00372"></a>00372 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00373"></a>00373 <span class="preprocessor"></span>      }
<a name="l00374"></a>00374     <span class="keywordflow">catch</span> (...)
<a name="l00375"></a>00375       {
<a name="l00376"></a>00376         this-&gt;nz_ = 0;
<a name="l00377"></a>00377         this-&gt;data_ = NULL;
<a name="l00378"></a>00378       }
<a name="l00379"></a>00379 <span class="preprocessor">#endif</span>
<a name="l00380"></a>00380 <span class="preprocessor"></span>
<a name="l00381"></a>00381     this-&gt;nz_ = 0;
<a name="l00382"></a>00382   }
<a name="l00383"></a>00383 
<a name="l00384"></a>00384 
<a name="l00386"></a>00386 
<a name="l00389"></a>00389   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00390"></a>00390   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a71256dab229c463457ff8848aa6d1fa7" title="Clears the matrix.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::Clear</a>()
<a name="l00391"></a>00391   {
<a name="l00392"></a>00392     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a6f46818c1d832ff9be950a5b6f0fc9c7" title="Destructor.">~Matrix_SymSparse</a>();
<a name="l00393"></a>00393   }
<a name="l00394"></a>00394 
<a name="l00395"></a>00395 
<a name="l00396"></a>00396   <span class="comment">/*********************</span>
<a name="l00397"></a>00397 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00398"></a>00398 <span class="comment">   *********************/</span>
<a name="l00399"></a>00399 
<a name="l00400"></a>00400 
<a name="l00402"></a>00402 
<a name="l00413"></a>00413   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00414"></a>00414   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00415"></a>00415             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00416"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aeb3fdc796d3c31e4b4729d3e9e15834c">00416</a>             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00417"></a>00417   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aeb3fdc796d3c31e4b4729d3e9e15834c" title="Redefines the matrix.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00418"></a>00418 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aeb3fdc796d3c31e4b4729d3e9e15834c" title="Redefines the matrix.">  SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00419"></a>00419           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; values,
<a name="l00420"></a>00420           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; ptr,
<a name="l00421"></a>00421           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; ind)
<a name="l00422"></a>00422   {
<a name="l00423"></a>00423     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a71256dab229c463457ff8848aa6d1fa7" title="Clears the matrix.">Clear</a>();
<a name="l00424"></a>00424     this-&gt;m_ = i;
<a name="l00425"></a>00425     this-&gt;n_ = i;
<a name="l00426"></a>00426     this-&gt;nz_ = values.GetLength();
<a name="l00427"></a>00427 
<a name="l00428"></a>00428 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00429"></a>00429 <span class="preprocessor"></span>    <span class="comment">// Checks whether vector sizes are acceptable.</span>
<a name="l00430"></a>00430 
<a name="l00431"></a>00431     <span class="keywordflow">if</span> (ind.GetLength() != nz_)
<a name="l00432"></a>00432       {
<a name="l00433"></a>00433         this-&gt;m_ = 0;
<a name="l00434"></a>00434         this-&gt;n_ = 0;
<a name="l00435"></a>00435         nz_ = 0;
<a name="l00436"></a>00436         ptr_ = NULL;
<a name="l00437"></a>00437         ind_ = NULL;
<a name="l00438"></a>00438         this-&gt;data_ = NULL;
<a name="l00439"></a>00439         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymSparse::Reallocate(int, int, &quot;</span>)
<a name="l00440"></a>00440                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00441"></a>00441                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz_) + <span class="stringliteral">&quot; values but &quot;</span>
<a name="l00442"></a>00442                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(ind.GetLength()) + <span class="stringliteral">&quot; row or column indices.&quot;</span>);
<a name="l00443"></a>00443       }
<a name="l00444"></a>00444 
<a name="l00445"></a>00445     <span class="keywordflow">if</span> (ptr.GetLength()-1 != i)
<a name="l00446"></a>00446       {
<a name="l00447"></a>00447         this-&gt;m_ = 0;
<a name="l00448"></a>00448         this-&gt;n_ = 0;
<a name="l00449"></a>00449         nz_ = 0;
<a name="l00450"></a>00450         ptr_ = NULL;
<a name="l00451"></a>00451         ind_ = NULL;
<a name="l00452"></a>00452         this-&gt;data_ = NULL;
<a name="l00453"></a>00453         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymSparse::Reallocate(int, int, &quot;</span>)
<a name="l00454"></a>00454                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00455"></a>00455                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The vector of start indices contains &quot;</span>)
<a name="l00456"></a>00456                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(ptr.GetLength()-1) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column&quot;</span>)
<a name="l00457"></a>00457                        + string(<span class="stringliteral">&quot; start indices (plus the number of&quot;</span>)
<a name="l00458"></a>00458                        + string(<span class="stringliteral">&quot; non-zero entries) but there are &quot;</span>)
<a name="l00459"></a>00459                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; rows or columns (&quot;</span>
<a name="l00460"></a>00460                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix).&quot;</span>);
<a name="l00461"></a>00461       }
<a name="l00462"></a>00462 
<a name="l00463"></a>00463     <span class="keywordflow">if</span> (static_cast&lt;long int&gt;(2 * nz_ - 2) / <span class="keyword">static_cast&lt;</span><span class="keywordtype">long</span> <span class="keywordtype">int</span><span class="keyword">&gt;</span>(i + 1)
<a name="l00464"></a>00464         &gt;= static_cast&lt;long int&gt;(i))
<a name="l00465"></a>00465       {
<a name="l00466"></a>00466         this-&gt;m_ = 0;
<a name="l00467"></a>00467         this-&gt;n_ = 0;
<a name="l00468"></a>00468         nz_ = 0;
<a name="l00469"></a>00469         ptr_ = NULL;
<a name="l00470"></a>00470         ind_ = NULL;
<a name="l00471"></a>00471         this-&gt;data_ = NULL;
<a name="l00472"></a>00472         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymSparse::Reallocate(int, int, &quot;</span>)
<a name="l00473"></a>00473                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00474"></a>00474                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>)
<a name="l00475"></a>00475                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(values.GetLength())
<a name="l00476"></a>00476                        + <span class="stringliteral">&quot; values) than elements in the matrix (&quot;</span>
<a name="l00477"></a>00477                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00478"></a>00478       }
<a name="l00479"></a>00479 <span class="preprocessor">#endif</span>
<a name="l00480"></a>00480 <span class="preprocessor"></span>
<a name="l00481"></a>00481     this-&gt;ptr_ = ptr.GetData();
<a name="l00482"></a>00482     this-&gt;ind_ = ind.GetData();
<a name="l00483"></a>00483     this-&gt;data_ = values.GetData();
<a name="l00484"></a>00484 
<a name="l00485"></a>00485     ptr.Nullify();
<a name="l00486"></a>00486     ind.Nullify();
<a name="l00487"></a>00487     values.Nullify();
<a name="l00488"></a>00488   }
<a name="l00489"></a>00489 
<a name="l00490"></a>00490 
<a name="l00492"></a>00492 
<a name="l00506"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#ad647993f653414c5a34b00052aab3c78">00506</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00507"></a>00507   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aeb3fdc796d3c31e4b4729d3e9e15834c" title="Redefines the matrix.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00508"></a>00508 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aeb3fdc796d3c31e4b4729d3e9e15834c" title="Redefines the matrix.">  ::SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> nz,
<a name="l00509"></a>00509             <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00510"></a>00510             ::pointer values,
<a name="l00511"></a>00511             <span class="keywordtype">int</span>* ptr,
<a name="l00512"></a>00512             <span class="keywordtype">int</span>* ind)
<a name="l00513"></a>00513   {
<a name="l00514"></a>00514     this-&gt;Clear();
<a name="l00515"></a>00515 
<a name="l00516"></a>00516     this-&gt;m_ = i;
<a name="l00517"></a>00517     this-&gt;n_ = i;
<a name="l00518"></a>00518 
<a name="l00519"></a>00519     this-&gt;nz_ = nz;
<a name="l00520"></a>00520 
<a name="l00521"></a>00521     this-&gt;data_ = values;
<a name="l00522"></a>00522     ind_ = ind;
<a name="l00523"></a>00523     ptr_ = ptr;
<a name="l00524"></a>00524   }
<a name="l00525"></a>00525 
<a name="l00526"></a>00526 
<a name="l00528"></a>00528 
<a name="l00532"></a>00532   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00533"></a>00533   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a99f9900aa0c5bd7c6040686a53d39a85" title="Clears the matrix without releasing memory.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::Nullify</a>()
<a name="l00534"></a>00534   {
<a name="l00535"></a>00535     this-&gt;data_ = NULL;
<a name="l00536"></a>00536     this-&gt;m_ = 0;
<a name="l00537"></a>00537     this-&gt;n_ = 0;
<a name="l00538"></a>00538     nz_ = 0;
<a name="l00539"></a>00539     ptr_ = NULL;
<a name="l00540"></a>00540     ind_ = NULL;
<a name="l00541"></a>00541   }
<a name="l00542"></a>00542 
<a name="l00543"></a>00543 
<a name="l00545"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a13715cb4e64fbe58e24fa86897131e25">00545</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00546"></a>00546   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a13715cb4e64fbe58e24fa86897131e25" title="Copies a matrix.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00547"></a>00547 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a13715cb4e64fbe58e24fa86897131e25" title="Copies a matrix.">  Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00548"></a>00548   {
<a name="l00549"></a>00549     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a71256dab229c463457ff8848aa6d1fa7" title="Clears the matrix.">Clear</a>();
<a name="l00550"></a>00550     <span class="keywordtype">int</span> nz = A.<a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a4be3dd5024addbf8bfa332193c4030cd" title="Returns the number of elements stored in memory.">GetNonZeros</a>();
<a name="l00551"></a>00551     <span class="keywordtype">int</span> i = A.<a class="code" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927" title="Returns the number of rows.">GetM</a>();
<a name="l00552"></a>00552     <span class="keywordtype">int</span> j = A.<a class="code" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984" title="Returns the number of columns.">GetN</a>();
<a name="l00553"></a>00553     this-&gt;nz_ = nz;
<a name="l00554"></a>00554     this-&gt;m_ = i;
<a name="l00555"></a>00555     this-&gt;n_ = j;
<a name="l00556"></a>00556     <span class="keywordflow">if</span> ((i == 0)||(j == 0))
<a name="l00557"></a>00557       {
<a name="l00558"></a>00558         this-&gt;m_ = 0;
<a name="l00559"></a>00559         this-&gt;n_ = 0;
<a name="l00560"></a>00560         this-&gt;nz_ = 0;
<a name="l00561"></a>00561         <span class="keywordflow">return</span>;
<a name="l00562"></a>00562       }
<a name="l00563"></a>00563 
<a name="l00564"></a>00564 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00565"></a>00565 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (static_cast&lt;long int&gt;(2 * nz_ - 2) / static_cast&lt;long int&gt;(i + 1)
<a name="l00566"></a>00566         &gt;= static_cast&lt;long int&gt;(i))
<a name="l00567"></a>00567       {
<a name="l00568"></a>00568         this-&gt;m_ = 0;
<a name="l00569"></a>00569         this-&gt;n_ = 0;
<a name="l00570"></a>00570         nz_ = 0;
<a name="l00571"></a>00571         ptr_ = NULL;
<a name="l00572"></a>00572         ind_ = NULL;
<a name="l00573"></a>00573         this-&gt;data_ = NULL;
<a name="l00574"></a>00574         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, int)&quot;</span>,
<a name="l00575"></a>00575                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00576"></a>00576                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; values) than elements in the upper&quot;</span>)
<a name="l00577"></a>00577                        + <span class="stringliteral">&quot; part of the matrix (&quot;</span>
<a name="l00578"></a>00578                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00579"></a>00579       }
<a name="l00580"></a>00580 <span class="preprocessor">#endif</span>
<a name="l00581"></a>00581 <span class="preprocessor"></span>
<a name="l00582"></a>00582 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00583"></a>00583 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00584"></a>00584       {
<a name="l00585"></a>00585 <span class="preprocessor">#endif</span>
<a name="l00586"></a>00586 <span class="preprocessor"></span>
<a name="l00587"></a>00587         ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(i + 1, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00588"></a>00588         memcpy(this-&gt;ptr_, A.ptr_, (i + 1) * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00589"></a>00589 
<a name="l00590"></a>00590 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00591"></a>00591 <span class="preprocessor"></span>      }
<a name="l00592"></a>00592     <span class="keywordflow">catch</span> (...)
<a name="l00593"></a>00593       {
<a name="l00594"></a>00594         this-&gt;m_ = 0;
<a name="l00595"></a>00595         this-&gt;n_ = 0;
<a name="l00596"></a>00596         nz_ = 0;
<a name="l00597"></a>00597         ptr_ = NULL;
<a name="l00598"></a>00598         ind_ = NULL;
<a name="l00599"></a>00599         this-&gt;data_ = NULL;
<a name="l00600"></a>00600       }
<a name="l00601"></a>00601     <span class="keywordflow">if</span> (ptr_ == NULL)
<a name="l00602"></a>00602       {
<a name="l00603"></a>00603         this-&gt;m_ = 0;
<a name="l00604"></a>00604         this-&gt;n_ = 0;
<a name="l00605"></a>00605         nz_ = 0;
<a name="l00606"></a>00606         ind_ = NULL;
<a name="l00607"></a>00607         this-&gt;data_ = NULL;
<a name="l00608"></a>00608       }
<a name="l00609"></a>00609     <span class="keywordflow">if</span> (ptr_ == NULL &amp;&amp; i != 0)
<a name="l00610"></a>00610       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, int)&quot;</span>,
<a name="l00611"></a>00611                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00612"></a>00612                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (i+1) ) + <span class="stringliteral">&quot; bytes to store &quot;</span>
<a name="l00613"></a>00613                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i+1) + <span class="stringliteral">&quot; row or column start indices, for a &quot;</span>
<a name="l00614"></a>00614                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00615"></a>00615 <span class="preprocessor">#endif</span>
<a name="l00616"></a>00616 <span class="preprocessor"></span>
<a name="l00617"></a>00617 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00618"></a>00618 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00619"></a>00619       {
<a name="l00620"></a>00620 <span class="preprocessor">#endif</span>
<a name="l00621"></a>00621 <span class="preprocessor"></span>
<a name="l00622"></a>00622         ind_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(nz_, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00623"></a>00623         memcpy(this-&gt;ind_, A.ind_, nz_ * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00624"></a>00624 
<a name="l00625"></a>00625 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00626"></a>00626 <span class="preprocessor"></span>      }
<a name="l00627"></a>00627     <span class="keywordflow">catch</span> (...)
<a name="l00628"></a>00628       {
<a name="l00629"></a>00629         this-&gt;m_ = 0;
<a name="l00630"></a>00630         this-&gt;n_ = 0;
<a name="l00631"></a>00631         nz_ = 0;
<a name="l00632"></a>00632         free(ptr_);
<a name="l00633"></a>00633         ptr_ = NULL;
<a name="l00634"></a>00634         ind_ = NULL;
<a name="l00635"></a>00635         this-&gt;data_ = NULL;
<a name="l00636"></a>00636       }
<a name="l00637"></a>00637     <span class="keywordflow">if</span> (ind_ == NULL)
<a name="l00638"></a>00638       {
<a name="l00639"></a>00639         this-&gt;m_ = 0;
<a name="l00640"></a>00640         this-&gt;n_ = 0;
<a name="l00641"></a>00641         nz_ = 0;
<a name="l00642"></a>00642         free(ptr_);
<a name="l00643"></a>00643         ptr_ = NULL;
<a name="l00644"></a>00644         this-&gt;data_ = NULL;
<a name="l00645"></a>00645       }
<a name="l00646"></a>00646     <span class="keywordflow">if</span> (ind_ == NULL &amp;&amp; i != 0)
<a name="l00647"></a>00647       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, int)&quot;</span>,
<a name="l00648"></a>00648                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * nz)
<a name="l00649"></a>00649                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00650"></a>00650                      + <span class="stringliteral">&quot; row or column indices, for a &quot;</span>
<a name="l00651"></a>00651                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00652"></a>00652 <span class="preprocessor">#endif</span>
<a name="l00653"></a>00653 <span class="preprocessor"></span>
<a name="l00654"></a>00654 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00655"></a>00655 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00656"></a>00656       {
<a name="l00657"></a>00657 <span class="preprocessor">#endif</span>
<a name="l00658"></a>00658 <span class="preprocessor"></span>
<a name="l00659"></a>00659         this-&gt;data_ = this-&gt;allocator_.allocate(nz_, <span class="keyword">this</span>);
<a name="l00660"></a>00660         this-&gt;allocator_.memorycpy(this-&gt;data_, A.data_, nz_);
<a name="l00661"></a>00661 
<a name="l00662"></a>00662 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00663"></a>00663 <span class="preprocessor"></span>      }
<a name="l00664"></a>00664     <span class="keywordflow">catch</span> (...)
<a name="l00665"></a>00665       {
<a name="l00666"></a>00666         this-&gt;m_ = 0;
<a name="l00667"></a>00667         this-&gt;n_ = 0;
<a name="l00668"></a>00668         free(ptr_);
<a name="l00669"></a>00669         ptr_ = NULL;
<a name="l00670"></a>00670         free(ind_);
<a name="l00671"></a>00671         ind_ = NULL;
<a name="l00672"></a>00672         this-&gt;data_ = NULL;
<a name="l00673"></a>00673       }
<a name="l00674"></a>00674     <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00675"></a>00675       {
<a name="l00676"></a>00676         this-&gt;m_ = 0;
<a name="l00677"></a>00677         this-&gt;n_ = 0;
<a name="l00678"></a>00678         free(ptr_);
<a name="l00679"></a>00679         ptr_ = NULL;
<a name="l00680"></a>00680         free(ind_);
<a name="l00681"></a>00681         ind_ = NULL;
<a name="l00682"></a>00682       }
<a name="l00683"></a>00683     <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; i != 0)
<a name="l00684"></a>00684       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Matrix_SymSparse(int, int, int)&quot;</span>,
<a name="l00685"></a>00685                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * nz)
<a name="l00686"></a>00686                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz) + <span class="stringliteral">&quot; values, for a &quot;</span>
<a name="l00687"></a>00687                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00688"></a>00688 <span class="preprocessor">#endif</span>
<a name="l00689"></a>00689 <span class="preprocessor"></span>
<a name="l00690"></a>00690   }
<a name="l00691"></a>00691 
<a name="l00692"></a>00692 
<a name="l00693"></a>00693   <span class="comment">/*******************</span>
<a name="l00694"></a>00694 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00695"></a>00695 <span class="comment">   *******************/</span>
<a name="l00696"></a>00696 
<a name="l00697"></a>00697 
<a name="l00699"></a>00699 
<a name="l00703"></a>00703   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00704"></a>00704   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a4be3dd5024addbf8bfa332193c4030cd" title="Returns the number of elements stored in memory.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::GetNonZeros</a>()<span class="keyword"> const</span>
<a name="l00705"></a>00705 <span class="keyword">  </span>{
<a name="l00706"></a>00706     <span class="keywordflow">return</span> nz_;
<a name="l00707"></a>00707   }
<a name="l00708"></a>00708 
<a name="l00709"></a>00709 
<a name="l00711"></a>00711 
<a name="l00715"></a>00715   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00716"></a>00716   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a8950827192f33de71909f783032c9f5f" title="Returns the number of elements stored in memory.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::GetDataSize</a>()<span class="keyword"> const</span>
<a name="l00717"></a>00717 <span class="keyword">  </span>{
<a name="l00718"></a>00718     <span class="keywordflow">return</span> nz_;
<a name="l00719"></a>00719   }
<a name="l00720"></a>00720 
<a name="l00721"></a>00721 
<a name="l00723"></a>00723 
<a name="l00727"></a>00727   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00728"></a>00728   <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#ae6576503580320224c84cc46d675781d" title="Returns (row or column) start indices.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::GetPtr</a>()<span class="keyword"> const</span>
<a name="l00729"></a>00729 <span class="keyword">  </span>{
<a name="l00730"></a>00730     <span class="keywordflow">return</span> ptr_;
<a name="l00731"></a>00731   }
<a name="l00732"></a>00732 
<a name="l00733"></a>00733 
<a name="l00735"></a>00735 
<a name="l00742"></a>00742   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00743"></a>00743   <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a9d4ddb6035ed47fadc9f35df03c3764e" title="Returns (row or column) indices of non-zero entries.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::GetInd</a>()<span class="keyword"> const</span>
<a name="l00744"></a>00744 <span class="keyword">  </span>{
<a name="l00745"></a>00745     <span class="keywordflow">return</span> ind_;
<a name="l00746"></a>00746   }
<a name="l00747"></a>00747 
<a name="l00748"></a>00748 
<a name="l00750"></a>00750 
<a name="l00753"></a>00753   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00754"></a>00754   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a046fce2e283dc38a9ed0ce5b570e6f11" title="Returns the length of the array of start indices.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::GetPtrSize</a>()<span class="keyword"> const</span>
<a name="l00755"></a>00755 <span class="keyword">  </span>{
<a name="l00756"></a>00756     <span class="keywordflow">return</span> (this-&gt;m_ + 1);
<a name="l00757"></a>00757   }
<a name="l00758"></a>00758 
<a name="l00759"></a>00759 
<a name="l00761"></a>00761 
<a name="l00770"></a>00770   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00771"></a>00771   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a6ffb37230f5629d9f85a6b962bc8127d" title="Returns the length of the array of (column or row) indices.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::GetIndSize</a>()<span class="keyword"> const</span>
<a name="l00772"></a>00772 <span class="keyword">  </span>{
<a name="l00773"></a>00773     <span class="keywordflow">return</span> nz_;
<a name="l00774"></a>00774   }
<a name="l00775"></a>00775 
<a name="l00776"></a>00776 
<a name="l00777"></a>00777   <span class="comment">/**********************************</span>
<a name="l00778"></a>00778 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l00779"></a>00779 <span class="comment">   **********************************/</span>
<a name="l00780"></a>00780 
<a name="l00781"></a>00781 
<a name="l00783"></a>00783 
<a name="l00789"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aa899e8ece6a4210b5199a37e3e60b401">00789</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00790"></a>00790   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::value_type</a>
<a name="l00791"></a>00791   <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aa899e8ece6a4210b5199a37e3e60b401" title="Access operator.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i,
<a name="l00792"></a>00792                                                              <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00793"></a>00793 <span class="keyword">  </span>{
<a name="l00794"></a>00794 
<a name="l00795"></a>00795 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00796"></a>00796 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00797"></a>00797       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_SymSparse::operator()&quot;</span>,
<a name="l00798"></a>00798                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00799"></a>00799                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00800"></a>00800     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00801"></a>00801       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_SymSparse::operator()&quot;</span>,
<a name="l00802"></a>00802                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00803"></a>00803                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00804"></a>00804 <span class="preprocessor">#endif</span>
<a name="l00805"></a>00805 <span class="preprocessor"></span>
<a name="l00806"></a>00806     <span class="keywordtype">int</span> k, l;
<a name="l00807"></a>00807     <span class="keywordtype">int</span> a, b;
<a name="l00808"></a>00808 
<a name="l00809"></a>00809     <span class="comment">// Only the upper part is stored.</span>
<a name="l00810"></a>00810     <span class="keywordflow">if</span> (i&gt;j)
<a name="l00811"></a>00811       {
<a name="l00812"></a>00812         l = i;
<a name="l00813"></a>00813         i = j;
<a name="l00814"></a>00814         j = l;
<a name="l00815"></a>00815       }
<a name="l00816"></a>00816 
<a name="l00817"></a>00817     a = ptr_[Storage::GetFirst(i, j)];
<a name="l00818"></a>00818     b = ptr_[Storage::GetFirst(i, j) + 1];
<a name="l00819"></a>00819 
<a name="l00820"></a>00820     <span class="keywordflow">if</span> (a == b)
<a name="l00821"></a>00821       <span class="keywordflow">return</span> T(0);
<a name="l00822"></a>00822 
<a name="l00823"></a>00823     l = Storage::GetSecond(i, j);
<a name="l00824"></a>00824 
<a name="l00825"></a>00825     <span class="keywordflow">for</span> (k = a; (k&lt;b-1) &amp;&amp; (ind_[k]&lt;l); k++);
<a name="l00826"></a>00826 
<a name="l00827"></a>00827     <span class="keywordflow">if</span> (ind_[k] == l)
<a name="l00828"></a>00828       <span class="keywordflow">return</span> this-&gt;data_[k];
<a name="l00829"></a>00829     <span class="keywordflow">else</span>
<a name="l00830"></a>00830       <span class="keywordflow">return</span> T(0);
<a name="l00831"></a>00831   }
<a name="l00832"></a>00832 
<a name="l00833"></a>00833 
<a name="l00835"></a>00835 
<a name="l00843"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaca494c5d6a5c03b5a76c3cfbfc412fe">00843</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00844"></a>00844   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::value_type</a>&amp;
<a name="l00845"></a>00845   <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaca494c5d6a5c03b5a76c3cfbfc412fe" title="Access method.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00846"></a>00846   {
<a name="l00847"></a>00847 
<a name="l00848"></a>00848 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00849"></a>00849 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00850"></a>00850       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Val(int, int)&quot;</span>,
<a name="l00851"></a>00851                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00852"></a>00852                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00853"></a>00853     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00854"></a>00854       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Val(int, int)&quot;</span>,
<a name="l00855"></a>00855                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00856"></a>00856                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00857"></a>00857 <span class="preprocessor">#endif</span>
<a name="l00858"></a>00858 <span class="preprocessor"></span>
<a name="l00859"></a>00859     <span class="keywordtype">int</span> k, l;
<a name="l00860"></a>00860     <span class="keywordtype">int</span> a, b;
<a name="l00861"></a>00861 
<a name="l00862"></a>00862     <span class="comment">// Only the upper part is stored.</span>
<a name="l00863"></a>00863     <span class="keywordflow">if</span> (i&gt;j)
<a name="l00864"></a>00864       {
<a name="l00865"></a>00865         l = i;
<a name="l00866"></a>00866         i = j;
<a name="l00867"></a>00867         j = l;
<a name="l00868"></a>00868       }
<a name="l00869"></a>00869 
<a name="l00870"></a>00870     a = ptr_[Storage::GetFirst(i, j)];
<a name="l00871"></a>00871     b = ptr_[Storage::GetFirst(i, j) + 1];
<a name="l00872"></a>00872 
<a name="l00873"></a>00873     <span class="keywordflow">if</span> (a == b)
<a name="l00874"></a>00874       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Val(int, int)&quot;</span>,
<a name="l00875"></a>00875                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l00876"></a>00876                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00877"></a>00877                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l00878"></a>00878 
<a name="l00879"></a>00879     l = Storage::GetSecond(i, j);
<a name="l00880"></a>00880 
<a name="l00881"></a>00881     <span class="keywordflow">for</span> (k = a; (k&lt;b-1) &amp;&amp; (ind_[k]&lt;l); k++);
<a name="l00882"></a>00882 
<a name="l00883"></a>00883     <span class="keywordflow">if</span> (ind_[k] == l)
<a name="l00884"></a>00884       <span class="keywordflow">return</span> this-&gt;data_[k];
<a name="l00885"></a>00885     <span class="keywordflow">else</span>
<a name="l00886"></a>00886       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Val(int, int)&quot;</span>,
<a name="l00887"></a>00887                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l00888"></a>00888                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00889"></a>00889                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l00890"></a>00890   }
<a name="l00891"></a>00891 
<a name="l00892"></a>00892 
<a name="l00894"></a>00894 
<a name="l00902"></a>00902   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00903"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a30ac9be8e0a8f70f3c2eae3f2e6657bb">00903</a>   <span class="keyword">inline</span>
<a name="l00904"></a>00904   <span class="keyword">const</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::value_type</a>&amp;
<a name="l00905"></a>00905   <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aaca494c5d6a5c03b5a76c3cfbfc412fe" title="Access method.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00906"></a>00906 <span class="keyword">  </span>{
<a name="l00907"></a>00907 
<a name="l00908"></a>00908 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00909"></a>00909 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00910"></a>00910       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Val(int, int)&quot;</span>,
<a name="l00911"></a>00911                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00912"></a>00912                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00913"></a>00913     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00914"></a>00914       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Val(int, int)&quot;</span>,
<a name="l00915"></a>00915                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00916"></a>00916                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00917"></a>00917 <span class="preprocessor">#endif</span>
<a name="l00918"></a>00918 <span class="preprocessor"></span>
<a name="l00919"></a>00919     <span class="keywordtype">int</span> k, l;
<a name="l00920"></a>00920     <span class="keywordtype">int</span> a, b;
<a name="l00921"></a>00921 
<a name="l00922"></a>00922     <span class="comment">// Only the upper part is stored.</span>
<a name="l00923"></a>00923     <span class="keywordflow">if</span> (i&gt;j)
<a name="l00924"></a>00924       {
<a name="l00925"></a>00925         l = i;
<a name="l00926"></a>00926         i = j;
<a name="l00927"></a>00927         j = l;
<a name="l00928"></a>00928       }
<a name="l00929"></a>00929 
<a name="l00930"></a>00930     a = ptr_[Storage::GetFirst(i, j)];
<a name="l00931"></a>00931     b = ptr_[Storage::GetFirst(i, j) + 1];
<a name="l00932"></a>00932 
<a name="l00933"></a>00933     <span class="keywordflow">if</span> (a == b)
<a name="l00934"></a>00934       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Val(int, int)&quot;</span>,
<a name="l00935"></a>00935                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l00936"></a>00936                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00937"></a>00937                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l00938"></a>00938 
<a name="l00939"></a>00939     l = Storage::GetSecond(i, j);
<a name="l00940"></a>00940 
<a name="l00941"></a>00941     <span class="keywordflow">for</span> (k = a; (k&lt;b-1) &amp;&amp; (ind_[k]&lt;l); k++);
<a name="l00942"></a>00942 
<a name="l00943"></a>00943     <span class="keywordflow">if</span> (ind_[k] == l)
<a name="l00944"></a>00944       <span class="keywordflow">return</span> this-&gt;data_[k];
<a name="l00945"></a>00945     <span class="keywordflow">else</span>
<a name="l00946"></a>00946       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix_SymSparse::Val(int, int)&quot;</span>,
<a name="l00947"></a>00947                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l00948"></a>00948                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00949"></a>00949                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l00950"></a>00950   }
<a name="l00951"></a>00951 
<a name="l00952"></a>00952 
<a name="l00954"></a>00954 
<a name="l00959"></a>00959   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00960"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#ad9e277dce0daf0cfb76e81e6087bcb75">00960</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00961"></a>00961   <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#ad9e277dce0daf0cfb76e81e6087bcb75" title="Duplicates a matrix (assignment operator).">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00962"></a>00962 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#ad9e277dce0daf0cfb76e81e6087bcb75" title="Duplicates a matrix (assignment operator).">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00963"></a>00963   {
<a name="l00964"></a>00964     this-&gt;Copy(A);
<a name="l00965"></a>00965 
<a name="l00966"></a>00966     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00967"></a>00967   }
<a name="l00968"></a>00968 
<a name="l00969"></a>00969 
<a name="l00970"></a>00970   <span class="comment">/************************</span>
<a name="l00971"></a>00971 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l00972"></a>00972 <span class="comment">   ************************/</span>
<a name="l00973"></a>00973 
<a name="l00974"></a>00974 
<a name="l00976"></a>00976 
<a name="l00981"></a>00981   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00982"></a>00982   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#a20b7ac2a821a2f54f4e325c07846b6cd" title="Displays the matrix on the standard output.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00983"></a>00983 <span class="keyword">  </span>{
<a name="l00984"></a>00984     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00985"></a>00985       {
<a name="l00986"></a>00986         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;n_; j++)
<a name="l00987"></a>00987           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00988"></a>00988         cout &lt;&lt; endl;
<a name="l00989"></a>00989       }
<a name="l00990"></a>00990   }
<a name="l00991"></a>00991 
<a name="l00992"></a>00992 
<a name="l00994"></a>00994 
<a name="l00999"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#ab1a9893cc7b8823fe6a2a0eee9520460">00999</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01000"></a>01000   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#ab1a9893cc7b8823fe6a2a0eee9520460" title="Writes the matrix in a file.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01001"></a>01001 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#ab1a9893cc7b8823fe6a2a0eee9520460" title="Writes the matrix in a file.">  ::WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l01002"></a>01002 <span class="keyword">  </span>{
<a name="l01003"></a>01003     ofstream FileStream; FileStream.precision(14);
<a name="l01004"></a>01004     FileStream.open(FileName.c_str());
<a name="l01005"></a>01005 
<a name="l01006"></a>01006 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01007"></a>01007 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01008"></a>01008     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01009"></a>01009       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_SymSparse::WriteText(string FileName)&quot;</span>,
<a name="l01010"></a>01010                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01011"></a>01011 <span class="preprocessor">#endif</span>
<a name="l01012"></a>01012 <span class="preprocessor"></span>
<a name="l01013"></a>01013     this-&gt;WriteText(FileStream);
<a name="l01014"></a>01014 
<a name="l01015"></a>01015     FileStream.close();
<a name="l01016"></a>01016   }
<a name="l01017"></a>01017 
<a name="l01018"></a>01018 
<a name="l01020"></a>01020 
<a name="l01025"></a><a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#aa28e902cd297fe56f7a88cea69c154d5">01025</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01026"></a>01026   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#ab1a9893cc7b8823fe6a2a0eee9520460" title="Writes the matrix in a file.">Matrix_SymSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01027"></a>01027 <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php#ab1a9893cc7b8823fe6a2a0eee9520460" title="Writes the matrix in a file.">  ::WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l01028"></a>01028 <span class="keyword">  </span>{
<a name="l01029"></a>01029 
<a name="l01030"></a>01030 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01031"></a>01031 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01032"></a>01032     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01033"></a>01033       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_SymSparse::WriteText(ofstream&amp; FileStream)&quot;</span>,
<a name="l01034"></a>01034                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l01035"></a>01035 <span class="preprocessor">#endif</span>
<a name="l01036"></a>01036 <span class="preprocessor"></span>
<a name="l01037"></a>01037     <span class="comment">// Conversion to coordinate format (1-index convention).</span>
<a name="l01038"></a>01038     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> IndRow, IndCol;
<a name="l01039"></a>01039     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> Value;
<a name="l01040"></a>01040     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; leaf_class =
<a name="l01041"></a>01041       <span class="keyword">static_cast&lt;</span><span class="keyword">const </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; <span class="keyword">&gt;</span>(*this);
<a name="l01042"></a>01042 
<a name="l01043"></a>01043     <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(leaf_class, IndRow, IndCol,
<a name="l01044"></a>01044                                  Value, 1, <span class="keyword">true</span>);
<a name="l01045"></a>01045 
<a name="l01046"></a>01046     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; IndRow.GetM(); i++)
<a name="l01047"></a>01047       FileStream &lt;&lt; IndRow(i) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; IndCol(i) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; Value(i) &lt;&lt; <span class="charliteral">&#39;\n&#39;</span>;
<a name="l01048"></a>01048   }
<a name="l01049"></a>01049 
<a name="l01050"></a>01050 
<a name="l01052"></a>01052   <span class="comment">// MATRIX&lt;COLSYMSPARSE&gt; //</span>
<a name="l01054"></a>01054 <span class="comment"></span>
<a name="l01055"></a>01055 
<a name="l01056"></a>01056   <span class="comment">/****************</span>
<a name="l01057"></a>01057 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01058"></a>01058 <span class="comment">   ****************/</span>
<a name="l01059"></a>01059 
<a name="l01061"></a>01061 
<a name="l01064"></a>01064   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01065"></a>01065   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSymSparse, Allocator&gt;::Matrix</a>()  throw():
<a name="l01066"></a>01066     <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_sym_sparse.php">ColSymSparse</a>, Allocator&gt;()
<a name="l01067"></a>01067   {
<a name="l01068"></a>01068   }
<a name="l01069"></a>01069 
<a name="l01070"></a>01070 
<a name="l01072"></a>01072 
<a name="l01076"></a>01076   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01077"></a>01077   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_sparse_00_01_allocator_01_4.php#ab59d8490dc7405a4fc91b4e019bfa10f" title="Default constructor.">Matrix&lt;T, Prop, ColSymSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01078"></a>01078     Matrix_SymSparse&lt;T, Prop, ColSymSparse, Allocator&gt;(i, j, 0)
<a name="l01079"></a>01079   {
<a name="l01080"></a>01080   }
<a name="l01081"></a>01081 
<a name="l01082"></a>01082 
<a name="l01084"></a>01084 
<a name="l01091"></a>01091   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01092"></a>01092   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_sparse_00_01_allocator_01_4.php#ab59d8490dc7405a4fc91b4e019bfa10f" title="Default constructor.">Matrix&lt;T, Prop, ColSymSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> nz):
<a name="l01093"></a>01093     Matrix_SymSparse&lt;T, Prop, ColSymSparse, Allocator&gt;(i, j, nz)
<a name="l01094"></a>01094   {
<a name="l01095"></a>01095   }
<a name="l01096"></a>01096 
<a name="l01097"></a>01097 
<a name="l01099"></a>01099 
<a name="l01111"></a>01111   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01112"></a>01112   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01113"></a>01113             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01114"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_sparse_00_01_allocator_01_4.php#a496b0a427faaea22d2d342b1263e4d00">01114</a>             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01115"></a>01115   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSymSparse, Allocator&gt;::</a>
<a name="l01116"></a>01116 <a class="code" href="class_seldon_1_1_matrix.php">  Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l01117"></a>01117          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; values,
<a name="l01118"></a>01118          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; ptr,
<a name="l01119"></a>01119          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; ind):
<a name="l01120"></a>01120     <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_sym_sparse.php">ColSymSparse</a>, Allocator&gt;(i, j, values, ptr, ind)
<a name="l01121"></a>01121   {
<a name="l01122"></a>01122   }
<a name="l01123"></a>01123 
<a name="l01124"></a>01124 
<a name="l01125"></a>01125 
<a name="l01127"></a>01127   <span class="comment">// MATRIX&lt;ROWSYMSPARSE&gt; //</span>
<a name="l01129"></a>01129 <span class="comment"></span>
<a name="l01130"></a>01130 
<a name="l01131"></a>01131   <span class="comment">/****************</span>
<a name="l01132"></a>01132 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01133"></a>01133 <span class="comment">   ****************/</span>
<a name="l01134"></a>01134 
<a name="l01135"></a>01135 
<a name="l01137"></a>01137 
<a name="l01140"></a>01140   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01141"></a>01141   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSymSparse, Allocator&gt;::Matrix</a>()  throw():
<a name="l01142"></a>01142     <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_sym_sparse.php">RowSymSparse</a>, Allocator&gt;()
<a name="l01143"></a>01143   {
<a name="l01144"></a>01144   }
<a name="l01145"></a>01145 
<a name="l01146"></a>01146 
<a name="l01148"></a>01148 
<a name="l01152"></a>01152   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01153"></a>01153   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php#a11e9550e00334880bb869d69aaf76546" title="Default constructor.">Matrix&lt;T, Prop, RowSymSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01154"></a>01154     Matrix_SymSparse&lt;T, Prop, RowSymSparse, Allocator&gt;(i, j, 0)
<a name="l01155"></a>01155   {
<a name="l01156"></a>01156   }
<a name="l01157"></a>01157 
<a name="l01158"></a>01158 
<a name="l01160"></a>01160 
<a name="l01167"></a>01167   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01168"></a>01168   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php#a11e9550e00334880bb869d69aaf76546" title="Default constructor.">Matrix&lt;T, Prop, RowSymSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> nz):
<a name="l01169"></a>01169     Matrix_SymSparse&lt;T, Prop, RowSymSparse, Allocator&gt;(i, j, nz)
<a name="l01170"></a>01170   {
<a name="l01171"></a>01171   }
<a name="l01172"></a>01172 
<a name="l01173"></a>01173 
<a name="l01175"></a>01175 
<a name="l01187"></a>01187   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01188"></a>01188   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01189"></a>01189             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01190"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php#aca58932742ad99038531a482692d83f7">01190</a>             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01191"></a>01191   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSymSparse, Allocator&gt;::</a>
<a name="l01192"></a>01192 <a class="code" href="class_seldon_1_1_matrix.php">  Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l01193"></a>01193          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; values,
<a name="l01194"></a>01194          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; ptr,
<a name="l01195"></a>01195          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; ind):
<a name="l01196"></a>01196     <a class="code" href="class_seldon_1_1_matrix___sym_sparse.php" title="Symmetric sparse-matrix class.">Matrix_SymSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_sym_sparse.php">RowSymSparse</a>, Allocator&gt;(i, j, values, ptr, ind)
<a name="l01197"></a>01197   {
<a name="l01198"></a>01198   }
<a name="l01199"></a>01199 
<a name="l01200"></a>01200 
<a name="l01201"></a>01201 } <span class="comment">// namespace Seldon.</span>
<a name="l01202"></a>01202 
<a name="l01203"></a>01203 <span class="preprocessor">#define SELDON_FILE_MATRIX_SYMSPARSE_CXX</span>
<a name="l01204"></a>01204 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
