<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix_mumps.php">MatrixMumps</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-types">Protected Types</a> &#124;
<a href="#pro-methods">Protected Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::MatrixMumps&lt; T &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::MatrixMumps" -->
<p>object used to solve linear system by calling mumps subroutines  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_mumps_8hxx_source.php">Mumps.hxx</a>&gt;</code></p>

<p><a href="class_seldon_1_1_matrix_mumps-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af1dbd7261f5a31bdc48ba7149b952b32"></a><!-- doxytag: member="Seldon::MatrixMumps::MatrixMumps" ref="af1dbd7261f5a31bdc48ba7149b952b32" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_mumps.php#af1dbd7261f5a31bdc48ba7149b952b32">MatrixMumps</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">initialization <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af9f7189c4f700303976d3f2ffd13945a"></a><!-- doxytag: member="Seldon::MatrixMumps::~MatrixMumps" ref="af9f7189c4f700303976d3f2ffd13945a" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_mumps.php#af9f7189c4f700303976d3f2ffd13945a">~MatrixMumps</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">clears factorization <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a8d3c03fd1f5e00dbbbb39e8f9e1cc4ee"></a><!-- doxytag: member="Seldon::MatrixMumps::Clear" ref="a8d3c03fd1f5e00dbbbb39e8f9e1cc4ee" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_mumps.php#a8d3c03fd1f5e00dbbbb39e8f9e1cc4ee">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">clears factorization <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a752caecc1023fe5da2a71fc83ae8cbf9"></a><!-- doxytag: member="Seldon::MatrixMumps::SelectOrdering" ref="a752caecc1023fe5da2a71fc83ae8cbf9" args="(int num_ordering)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_mumps.php#a752caecc1023fe5da2a71fc83ae8cbf9">SelectOrdering</a> (int num_ordering)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">selects another ordering scheme <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a693fc679e4e3703162cd7e8e976b30d0"></a><!-- doxytag: member="Seldon::MatrixMumps::HideMessages" ref="a693fc679e4e3703162cd7e8e976b30d0" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_mumps.php#a693fc679e4e3703162cd7e8e976b30d0">HideMessages</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">no display from Mumps <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9375b89f5cbe3908db7120d22f033beb"></a><!-- doxytag: member="Seldon::MatrixMumps::ShowMessages" ref="a9375b89f5cbe3908db7120d22f033beb" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_mumps.php#a9375b89f5cbe3908db7120d22f033beb">ShowMessages</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">standard display <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2973bf648b35a1680e7ace2947cf2a95"></a><!-- doxytag: member="Seldon::MatrixMumps::EnableOutOfCore" ref="a2973bf648b35a1680e7ace2947cf2a95" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>EnableOutOfCore</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a85fbe11f8537cc8c78fed0b4e1e909e9"></a><!-- doxytag: member="Seldon::MatrixMumps::DisableOutOfCore" ref="a85fbe11f8537cc8c78fed0b4e1e909e9" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>DisableOutOfCore</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aab6219d8e5473f80dae2957a3d003ab0"></a><!-- doxytag: member="Seldon::MatrixMumps::GetInfoFactorization" ref="aab6219d8e5473f80dae2957a3d003ab0" args="() const " -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_mumps.php#aab6219d8e5473f80dae2957a3d003ab0">GetInfoFactorization</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">returns information about factorization performed <br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Prop , class Storage , class Allocator &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_mumps.php#a3b7b913e2e23e1d02890df3c332c6637">FindOrdering</a> (<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, Storage, Allocator &gt; &amp;mat, <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;numbers, bool keep_matrix=false)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">computes row numbers  <a href="#a3b7b913e2e23e1d02890df3c332c6637"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Prop , class Storage , class Allocator &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_mumps.php#a712e9edf500c0a63ddded960762707c5">FactorizeMatrix</a> (<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, Storage, Allocator &gt; &amp;mat, bool keep_matrix=false)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">factorization of a given matrix  <a href="#a712e9edf500c0a63ddded960762707c5"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a9fd9e06d1da9a27e63cd2fb266f628c0"></a><!-- doxytag: member="Seldon::MatrixMumps::PerformAnalysis" ref="a9fd9e06d1da9a27e63cd2fb266f628c0" args="(Matrix&lt; T, Prop, Storage, Allocator &gt; &amp;mat)" -->
template&lt;class Prop , class Storage , class Allocator &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_mumps.php#a9fd9e06d1da9a27e63cd2fb266f628c0">PerformAnalysis</a> (<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, Storage, Allocator &gt; &amp;mat)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Symbolic factorization. <br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Prop , class Storage , class Allocator &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_mumps.php#ab59b1cd5d9c51196ea91dea968ae2c00">PerformFactorization</a> (<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, Storage, Allocator &gt; &amp;mat)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Numerical factorization.  <a href="#ab59b1cd5d9c51196ea91dea968ae2c00"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="ae347ebc94ea576e81ee873ae1a8b7143"></a><!-- doxytag: member="Seldon::MatrixMumps::GetSchurMatrix" ref="ae347ebc94ea576e81ee873ae1a8b7143" args="(Matrix&lt; T, Prop1, Storage1, Allocator1 &gt; &amp;mat, const IVect &amp;num, Matrix&lt; T, Prop2, Storage2, Allocator2 &gt; &amp;mat_schur, bool keep_matrix=false)" -->
template&lt;class Prop1 , class Storage1 , class Allocator1 , class Prop2 , class Storage2 , class Allocator2 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><b>GetSchurMatrix</b> (<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop1, Storage1, Allocator1 &gt; &amp;mat, const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;num, <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop2, Storage2, Allocator2 &gt; &amp;mat_schur, bool keep_matrix=false)</td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="aa74d628e9c8b8256fbd4f9e29a746dd6"></a><!-- doxytag: member="Seldon::MatrixMumps::Solve" ref="aa74d628e9c8b8256fbd4f9e29a746dd6" args="(Vector&lt; T, VectFull, Allocator2 &gt; &amp;x)" -->
template&lt;class Allocator2 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><b>Solve</b> (<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator2 &gt; &amp;x)</td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Allocator2 , class Transpose_status &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_mumps.php#ac2ed47d3a9f1f5c99e9a01a8c6f58083">Solve</a> (const Transpose_status &amp;TransA, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator2 &gt; &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">resolution of a linear system using the computed factorization  <a href="#ac2ed47d3a9f1f5c99e9a01a8c6f58083"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Allocator2 , class Transpose_status , class Prop &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_mumps.php#a5863d614bd5110fda7cc3d1dcc7c15f4">Solve</a> (const Transpose_status &amp;TransA, <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_col_major.php">ColMajor</a>, Allocator2 &gt; &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Resolution of a linear system using a computed factorization.  <a href="#a5863d614bd5110fda7cc3d1dcc7c15f4"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class Prop1 , class Storage1 , class Allocator , class Prop2 , class Storage2 , class Allocator2 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_mumps.php#adb5993784b010142a470da631025ce0a">GetSchurMatrix</a> (<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop1, Storage1, Allocator &gt; &amp;mat, const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;num, <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop2, Storage2, Allocator2 &gt; &amp;mat_schur, bool keep_matrix)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Computation of Schur complement.  <a href="#adb5993784b010142a470da631025ce0a"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-types"></a>
Protected Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7d695b24c99275e0911cb817757f2737"></a><!-- doxytag: member="Seldon::MatrixMumps::pointer" ref="a7d695b24c99275e0911cb817757f2737" args="" -->
typedef <a class="el" href="class_seldon_1_1_type_mumps.php">TypeMumps</a>&lt; T &gt;::<a class="el" href="class_seldon_1_1_matrix_mumps.php#a7d695b24c99275e0911cb817757f2737">pointer</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_mumps.php#a7d695b24c99275e0911cb817757f2737">pointer</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">double* or complex&lt;double&gt;* <br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-methods"></a>
Protected Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a081205fadf8136037eed58cec3825a56"></a><!-- doxytag: member="Seldon::MatrixMumps::CallMumps" ref="a081205fadf8136037eed58cec3825a56" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>CallMumps</b> ()</td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a90879b937c4794ebc45fa3858c6e97a2"></a><!-- doxytag: member="Seldon::MatrixMumps::InitMatrix" ref="a90879b937c4794ebc45fa3858c6e97a2" args="(const MatrixSparse &amp;, bool dist=false)" -->
template&lt;class MatrixSparse &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_mumps.php#a90879b937c4794ebc45fa3858c6e97a2">InitMatrix</a> (const MatrixSparse &amp;, bool dist=false)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">initialization of the computation <br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="aeb4a518db3d87060b470e01fe2ea1486"></a><!-- doxytag: member="Seldon::MatrixMumps::CallMumps" ref="aeb4a518db3d87060b470e01fe2ea1486" args="()" -->
template&lt;&gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><b>CallMumps</b> ()</td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a792b8c0398b3b45cc3563d45b21a0f3c"></a><!-- doxytag: member="Seldon::MatrixMumps::CallMumps" ref="a792b8c0398b3b45cc3563d45b21a0f3c" args="()" -->
template&lt;&gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><b>CallMumps</b> ()</td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a06ba655df528009ca61f4dd767850f68"></a><!-- doxytag: member="Seldon::MatrixMumps::rank" ref="a06ba655df528009ca61f4dd767850f68" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_mumps.php#a06ba655df528009ca61f4dd767850f68">rank</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">rank of processor <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad33cdf6539eea328102bfa3918adef6c"></a><!-- doxytag: member="Seldon::MatrixMumps::type_ordering" ref="ad33cdf6539eea328102bfa3918adef6c" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_mumps.php#ad33cdf6539eea328102bfa3918adef6c">type_ordering</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">ordering scheme (AMD, Metis, etc) //! object containing Mumps data structure <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4e5b0273d86acf5ba88c9a5cb4debeac"></a><!-- doxytag: member="Seldon::MatrixMumps::struct_mumps" ref="a4e5b0273d86acf5ba88c9a5cb4debeac" args="" -->
<a class="el" href="class_seldon_1_1_type_mumps.php">TypeMumps</a>&lt; T &gt;::data&nbsp;</td><td class="memItemRight" valign="bottom"><b>struct_mumps</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a684239285d0ff374076921218a0c2fe7"></a><!-- doxytag: member="Seldon::MatrixMumps::print_level" ref="a684239285d0ff374076921218a0c2fe7" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>print_level</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3a5ecf5f66a8f065647ace0da83bc2b1"></a><!-- doxytag: member="Seldon::MatrixMumps::out_of_core" ref="a3a5ecf5f66a8f065647ace0da83bc2b1" args="" -->
bool&nbsp;</td><td class="memItemRight" valign="bottom"><b>out_of_core</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1e3b995b2d3aa966ee2c272a8a277fb3"></a><!-- doxytag: member="Seldon::MatrixMumps::num_row_glob" ref="a1e3b995b2d3aa966ee2c272a8a277fb3" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">IVect</a>&nbsp;</td><td class="memItemRight" valign="bottom"><b>num_row_glob</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7bc391845522420e9ec01293de2d71dc"></a><!-- doxytag: member="Seldon::MatrixMumps::num_col_glob" ref="a7bc391845522420e9ec01293de2d71dc" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">IVect</a>&nbsp;</td><td class="memItemRight" valign="bottom"><b>num_col_glob</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae4af590a082dc4bece6f7b1b18001de7"></a><!-- doxytag: member="Seldon::MatrixMumps::new_communicator" ref="ae4af590a082dc4bece6f7b1b18001de7" args="" -->
bool&nbsp;</td><td class="memItemRight" valign="bottom"><b>new_communicator</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T&gt;<br/>
 class Seldon::MatrixMumps&lt; T &gt;</h3>

<p>object used to solve linear system by calling mumps subroutines </p>

<p>Definition at line <a class="el" href="_mumps_8hxx_source.php#l00066">66</a> of file <a class="el" href="_mumps_8hxx_source.php">Mumps.hxx</a>.</p>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="a712e9edf500c0a63ddded960762707c5"></a><!-- doxytag: member="Seldon::MatrixMumps::FactorizeMatrix" ref="a712e9edf500c0a63ddded960762707c5" args="(Matrix&lt; T, Prop, Storage, Allocator &gt; &amp;mat, bool keep_matrix=false)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T &gt; </div>
<div class="memtemplate">
template&lt;class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps</a>&lt; T &gt;::FactorizeMatrix </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, Storage, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>mat</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>keep_matrix</em> = <code>false</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>factorization of a given matrix </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in,out]</tt>&nbsp;</td><td valign="top"><em>mat</em>&nbsp;</td><td>matrix to factorize </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>keep_matrix</em>&nbsp;</td><td>if false, the given matrix is cleared </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_mumps_8cxx_source.php#l00254">254</a> of file <a class="el" href="_mumps_8cxx_source.php">Mumps.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3b7b913e2e23e1d02890df3c332c6637"></a><!-- doxytag: member="Seldon::MatrixMumps::FindOrdering" ref="a3b7b913e2e23e1d02890df3c332c6637" args="(Matrix&lt; T, Prop, Storage, Allocator &gt; &amp;mat, IVect &amp;numbers, bool keep_matrix=false)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T &gt; </div>
<div class="memtemplate">
template&lt;class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps</a>&lt; T &gt;::FindOrdering </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, Storage, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>mat</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>numbers</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>keep_matrix</em> = <code>false</code></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>computes row numbers </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in,out]</tt>&nbsp;</td><td valign="top"><em>mat</em>&nbsp;</td><td>matrix whose we want to find the ordering </td></tr>
    <tr><td valign="top"><tt>[out]</tt>&nbsp;</td><td valign="top"><em>numbers</em>&nbsp;</td><td>new row numbers </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>keep_matrix</em>&nbsp;</td><td>if false, the given matrix is cleared </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_mumps_8cxx_source.php#l00220">220</a> of file <a class="el" href="_mumps_8cxx_source.php">Mumps.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="adb5993784b010142a470da631025ce0a"></a><!-- doxytag: member="Seldon::MatrixMumps::GetSchurMatrix" ref="adb5993784b010142a470da631025ce0a" args="(Matrix&lt; T, Prop1, Storage1, Allocator &gt; &amp;mat, const IVect &amp;num, Matrix&lt; T, Prop2, Storage2, Allocator2 &gt; &amp;mat_schur, bool keep_matrix)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T &gt; </div>
<div class="memtemplate">
template&lt;class Prop1 , class Storage1 , class Allocator , class Prop2 , class Storage2 , class Allocator2 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps</a>&lt; T &gt;::GetSchurMatrix </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop1, Storage1, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>mat</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_vector.php">IVect</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>num</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop2, Storage2, Allocator2 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>mat_schur</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">bool&nbsp;</td>
          <td class="paramname"> <em>keep_matrix</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Computation of Schur complement. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in,out]</tt>&nbsp;</td><td valign="top"><em>mat</em>&nbsp;</td><td>initial matrix. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>num</em>&nbsp;</td><td>numbers to keep in Schur complement. </td></tr>
    <tr><td valign="top"><tt>[out]</tt>&nbsp;</td><td valign="top"><em>mat_schur</em>&nbsp;</td><td>Schur matrix. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>keep_matrix</em>&nbsp;</td><td>if false, <em>mat</em> is cleared. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_mumps_8cxx_source.php#l00339">339</a> of file <a class="el" href="_mumps_8cxx_source.php">Mumps.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab59b1cd5d9c51196ea91dea968ae2c00"></a><!-- doxytag: member="Seldon::MatrixMumps::PerformFactorization" ref="ab59b1cd5d9c51196ea91dea968ae2c00" args="(Matrix&lt; T, Prop, Storage, Allocator &gt; &amp;mat)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T &gt; </div>
<div class="memtemplate">
template&lt;class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps</a>&lt; T &gt;::PerformFactorization </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, Storage, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>mat</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Numerical factorization. </p>
<p>Be careful, because no conversion is performed in the method, so you have to choose RowSparse/ColSparse for unsymmetric matrices and RowSymSparse/ColSymSparse for symmetric matrices. The other formats should not work </p>

<p>Definition at line <a class="el" href="_mumps_8cxx_source.php#l00309">309</a> of file <a class="el" href="_mumps_8cxx_source.php">Mumps.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5863d614bd5110fda7cc3d1dcc7c15f4"></a><!-- doxytag: member="Seldon::MatrixMumps::Solve" ref="a5863d614bd5110fda7cc3d1dcc7c15f4" args="(const Transpose_status &amp;TransA, Matrix&lt; T, Prop, ColMajor, Allocator2 &gt; &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T &gt; </div>
<div class="memtemplate">
template&lt;class Allocator2 , class Transpose_status , class Prop &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps</a>&lt; T &gt;::Solve </td>
          <td>(</td>
          <td class="paramtype">const Transpose_status &amp;&nbsp;</td>
          <td class="paramname"> <em>TransA</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_col_major.php">ColMajor</a>, Allocator2 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Resolution of a linear system using a computed factorization. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in,out]</tt>&nbsp;</td><td valign="top"><em>x</em>&nbsp;</td><td>on entry, the right-hand-side; on exit, the solution. It is assumed that 'FactorizeMatrix' has already been called. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_mumps_8cxx_source.php#l00423">423</a> of file <a class="el" href="_mumps_8cxx_source.php">Mumps.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac2ed47d3a9f1f5c99e9a01a8c6f58083"></a><!-- doxytag: member="Seldon::MatrixMumps::Solve" ref="ac2ed47d3a9f1f5c99e9a01a8c6f58083" args="(const Transpose_status &amp;TransA, Vector&lt; T, VectFull, Allocator2 &gt; &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T &gt; </div>
<div class="memtemplate">
template&lt;class Allocator2 , class Transpose_status &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps</a>&lt; T &gt;::Solve </td>
          <td>(</td>
          <td class="paramtype">const Transpose_status &amp;&nbsp;</td>
          <td class="paramname"> <em>TransA</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator2 &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>resolution of a linear system using the computed factorization </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in,out]</tt>&nbsp;</td><td valign="top"><em>x</em>&nbsp;</td><td>right-hand-side on input, solution on output It is assumed that a call to FactorizeMatrix has been done before </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_mumps_8cxx_source.php#l00385">385</a> of file <a class="el" href="_mumps_8cxx_source.php">Mumps.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>computation/interfaces/direct/<a class="el" href="_mumps_8hxx_source.php">Mumps.hxx</a></li>
<li>computation/interfaces/direct/<a class="el" href="_mumps_8cxx_source.php">Mumps.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
