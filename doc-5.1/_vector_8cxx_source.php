<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>vector/Vector.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_VECTOR_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;Vector.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="keyword">namespace </span>Seldon
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 
<a name="l00030"></a>00030   <span class="comment">// C_VECTOR_BASE //</span>
<a name="l00032"></a>00032 <span class="comment"></span>
<a name="l00033"></a>00033 
<a name="l00034"></a>00034   <span class="comment">/****************</span>
<a name="l00035"></a>00035 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00036"></a>00036 <span class="comment">   ****************/</span>
<a name="l00037"></a>00037 
<a name="l00038"></a>00038 
<a name="l00040"></a>00040 
<a name="l00043"></a>00043   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00044"></a><a class="code" href="class_seldon_1_1_vector___base.php#ac8a6cee506f094504138943629b9dcbb">00044</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector___base.php#ac8a6cee506f094504138943629b9dcbb" title="Default constructor.">Vector_Base&lt;T, Allocator&gt;::Vector_Base</a>()
<a name="l00045"></a>00045   {
<a name="l00046"></a>00046     m_ = 0;
<a name="l00047"></a>00047     data_ = NULL;
<a name="l00048"></a>00048   }
<a name="l00049"></a>00049 
<a name="l00050"></a>00050 
<a name="l00052"></a>00052 
<a name="l00056"></a>00056   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00057"></a><a class="code" href="class_seldon_1_1_vector___base.php#aa5d9ca198d4175b9baf9a0543f7376be">00057</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector___base.php#ac8a6cee506f094504138943629b9dcbb" title="Default constructor.">Vector_Base&lt;T, Allocator&gt;::Vector_Base</a>(<span class="keywordtype">int</span> i)
<a name="l00058"></a>00058   {
<a name="l00059"></a>00059     m_ = i;
<a name="l00060"></a>00060     data_ = NULL;
<a name="l00061"></a>00061   }
<a name="l00062"></a>00062 
<a name="l00063"></a>00063 
<a name="l00065"></a>00065 
<a name="l00069"></a>00069   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00070"></a>00070   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector___base.php#ac8a6cee506f094504138943629b9dcbb" title="Default constructor.">Vector_Base&lt;T, Allocator&gt;::</a>
<a name="l00071"></a><a class="code" href="class_seldon_1_1_vector___base.php#a27796125b93f3a771a5c92e8196c9229">00071</a> <a class="code" href="class_seldon_1_1_vector___base.php#ac8a6cee506f094504138943629b9dcbb" title="Default constructor.">  Vector_Base</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector___base.php" title="Base structure for all vectors.">Vector_Base&lt;T, Allocator&gt;</a>&amp; A)
<a name="l00072"></a>00072   {
<a name="l00073"></a>00073     m_ = A.<a class="code" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae" title="Returns the number of elements.">GetM</a>();
<a name="l00074"></a>00074     data_ = NULL;
<a name="l00075"></a>00075   }
<a name="l00076"></a>00076 
<a name="l00077"></a>00077 
<a name="l00078"></a>00078   <span class="comment">/**************</span>
<a name="l00079"></a>00079 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00080"></a>00080 <span class="comment">   **************/</span>
<a name="l00081"></a>00081 
<a name="l00082"></a>00082 
<a name="l00084"></a>00084   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00085"></a><a class="code" href="class_seldon_1_1_vector___base.php#a6da510796ebef867765ea3a037debb1b">00085</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector___base.php#a6da510796ebef867765ea3a037debb1b" title="Destructor.">Vector_Base&lt;T, Allocator&gt;::~Vector_Base</a>()
<a name="l00086"></a>00086   {
<a name="l00087"></a>00087 
<a name="l00088"></a>00088 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00089"></a>00089 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00090"></a>00090       {
<a name="l00091"></a>00091 <span class="preprocessor">#endif</span>
<a name="l00092"></a>00092 <span class="preprocessor"></span>
<a name="l00093"></a>00093         <span class="keywordflow">if</span> (data_ != NULL)
<a name="l00094"></a>00094           {
<a name="l00095"></a>00095             vect_allocator_.deallocate(data_, m_);
<a name="l00096"></a>00096             m_ = 0;
<a name="l00097"></a>00097             data_ = NULL;
<a name="l00098"></a>00098           }
<a name="l00099"></a>00099 
<a name="l00100"></a>00100 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00101"></a>00101 <span class="preprocessor"></span>      }
<a name="l00102"></a>00102     <span class="keywordflow">catch</span> (...)
<a name="l00103"></a>00103       {
<a name="l00104"></a>00104         m_ = 0;
<a name="l00105"></a>00105         data_ = NULL;
<a name="l00106"></a>00106       }
<a name="l00107"></a>00107 <span class="preprocessor">#endif</span>
<a name="l00108"></a>00108 <span class="preprocessor"></span>
<a name="l00109"></a>00109   }
<a name="l00110"></a>00110 
<a name="l00111"></a>00111 
<a name="l00112"></a>00112   <span class="comment">/*******************</span>
<a name="l00113"></a>00113 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00114"></a>00114 <span class="comment">   *******************/</span>
<a name="l00115"></a>00115 
<a name="l00116"></a>00116 
<a name="l00118"></a>00118 
<a name="l00121"></a>00121   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00122"></a><a class="code" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae">00122</a>   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae" title="Returns the number of elements.">Vector_Base&lt;T, Allocator&gt;::GetM</a>()<span class="keyword"> const</span>
<a name="l00123"></a>00123 <span class="keyword">  </span>{
<a name="l00124"></a>00124     <span class="keywordflow">return</span> m_;
<a name="l00125"></a>00125   }
<a name="l00126"></a>00126 
<a name="l00127"></a>00127 
<a name="l00129"></a>00129 
<a name="l00132"></a>00132   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00133"></a><a class="code" href="class_seldon_1_1_vector___base.php#a075daae3607a4767a77a3de60ab30ba7">00133</a>   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector___base.php#a075daae3607a4767a77a3de60ab30ba7" title="Returns the number of elements.">Vector_Base&lt;T, Allocator&gt;::GetLength</a>()<span class="keyword"> const</span>
<a name="l00134"></a>00134 <span class="keyword">  </span>{
<a name="l00135"></a>00135     <span class="keywordflow">return</span> m_;
<a name="l00136"></a>00136   }
<a name="l00137"></a>00137 
<a name="l00138"></a>00138 
<a name="l00140"></a>00140 
<a name="l00143"></a>00143   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00144"></a><a class="code" href="class_seldon_1_1_vector___base.php#a839880a3113c1885ead70d79fade0294">00144</a>   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector___base.php#a839880a3113c1885ead70d79fade0294" title="Returns the number of elements stored.">Vector_Base&lt;T, Allocator&gt;::GetSize</a>()<span class="keyword"> const</span>
<a name="l00145"></a>00145 <span class="keyword">  </span>{
<a name="l00146"></a>00146     <span class="keywordflow">return</span> m_;
<a name="l00147"></a>00147   }
<a name="l00148"></a>00148 
<a name="l00149"></a>00149 
<a name="l00151"></a>00151 
<a name="l00154"></a>00154   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00155"></a>00155   <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector___base.php" title="Base structure for all vectors.">Vector_Base&lt;T, Allocator&gt;::pointer</a>
<a name="l00156"></a><a class="code" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a">00156</a>   <a class="code" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a" title="Returns a pointer to data_ (stored data).">Vector_Base&lt;T, Allocator&gt;::GetData</a>()<span class="keyword"> const</span>
<a name="l00157"></a>00157 <span class="keyword">  </span>{
<a name="l00158"></a>00158     <span class="keywordflow">return</span> data_;
<a name="l00159"></a>00159   }
<a name="l00160"></a>00160 
<a name="l00161"></a>00161 
<a name="l00163"></a>00163 
<a name="l00166"></a>00166   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00167"></a>00167   <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector___base.php" title="Base structure for all vectors.">Vector_Base&lt;T, Allocator&gt;::const_pointer</a>
<a name="l00168"></a><a class="code" href="class_seldon_1_1_vector___base.php#a454aea78c82c4317dfe4160cc7c95afa">00168</a>   <a class="code" href="class_seldon_1_1_vector___base.php#a454aea78c82c4317dfe4160cc7c95afa" title="Returns a const pointer to data_ (stored data).">Vector_Base&lt;T, Allocator&gt;::GetDataConst</a>()<span class="keyword"> const</span>
<a name="l00169"></a>00169 <span class="keyword">  </span>{
<a name="l00170"></a>00170     <span class="keywordflow">return</span> <span class="keyword">reinterpret_cast&lt;</span>typename <a class="code" href="class_seldon_1_1_vector___base.php" title="Base structure for all vectors.">Vector_Base</a>&lt;T,
<a name="l00171"></a>00171       Allocator<span class="keyword">&gt;</span>::const_pointer&gt;(data_);
<a name="l00172"></a>00172   }
<a name="l00173"></a>00173 
<a name="l00174"></a>00174 
<a name="l00176"></a>00176 
<a name="l00179"></a>00179   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00180"></a><a class="code" href="class_seldon_1_1_vector___base.php#ad0f5184bd9eec6fca70c54e459ced78f">00180</a>   <span class="keywordtype">void</span>* <a class="code" href="class_seldon_1_1_vector___base.php#ad0f5184bd9eec6fca70c54e459ced78f" title="Returns a pointer of type &amp;quot;void*&amp;quot; to the data array (data_).">Vector_Base&lt;T, Allocator&gt;::GetDataVoid</a>()<span class="keyword"> const</span>
<a name="l00181"></a>00181 <span class="keyword">  </span>{
<a name="l00182"></a>00182     <span class="keywordflow">return</span> <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">void</span>*<span class="keyword">&gt;</span>(data_);
<a name="l00183"></a>00183   }
<a name="l00184"></a>00184 
<a name="l00185"></a>00185 
<a name="l00187"></a>00187 
<a name="l00190"></a>00190   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00191"></a><a class="code" href="class_seldon_1_1_vector___base.php#a0aadc006977de037eb3ee550683e3f19">00191</a>   <span class="keyword">const</span> <span class="keywordtype">void</span>* <a class="code" href="class_seldon_1_1_vector___base.php#a0aadc006977de037eb3ee550683e3f19" title="Returns a pointer of type &amp;quot;const void*&amp;quot; to the data array (data_).">Vector_Base&lt;T, Allocator&gt;::GetDataConstVoid</a>()<span class="keyword"> const</span>
<a name="l00192"></a>00192 <span class="keyword">  </span>{
<a name="l00193"></a>00193     <span class="keywordflow">return</span> <span class="keyword">reinterpret_cast&lt;</span><span class="keyword">const </span><span class="keywordtype">void</span>*<span class="keyword">&gt;</span>(data_);
<a name="l00194"></a>00194   }
<a name="l00195"></a>00195 
<a name="l00196"></a>00196 
<a name="l00198"></a>00198   <span class="comment">// VECTOR&lt;VECT_FULL&gt; //</span>
<a name="l00200"></a>00200 <span class="comment"></span>
<a name="l00201"></a>00201 
<a name="l00202"></a>00202   <span class="comment">/****************</span>
<a name="l00203"></a>00203 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00204"></a>00204 <span class="comment">   ****************/</span>
<a name="l00205"></a>00205 
<a name="l00206"></a>00206 
<a name="l00208"></a>00208 
<a name="l00211"></a>00211   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00212"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a3220e19bce6b4a0f72dfca147dbf8b38">00212</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Vector</a>()  throw():
<a name="l00213"></a>00213     <a class="code" href="class_seldon_1_1_vector___base.php" title="Base structure for all vectors.">Vector_Base</a>&lt;T, Allocator&gt;()
<a name="l00214"></a>00214   {
<a name="l00215"></a>00215   }
<a name="l00216"></a>00216 
<a name="l00217"></a>00217 
<a name="l00219"></a>00219 
<a name="l00222"></a>00222   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00223"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#aaef02756d3c6c0a97454f35bffc07962">00223</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Vector</a>(<span class="keywordtype">int</span> i):
<a name="l00224"></a>00224     <a class="code" href="class_seldon_1_1_vector___base.php" title="Base structure for all vectors.">Vector_Base</a>&lt;T, Allocator&gt;(i)
<a name="l00225"></a>00225   {
<a name="l00226"></a>00226 
<a name="l00227"></a>00227 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00228"></a>00228 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00229"></a>00229       {
<a name="l00230"></a>00230 <span class="preprocessor">#endif</span>
<a name="l00231"></a>00231 <span class="preprocessor"></span>
<a name="l00232"></a>00232         this-&gt;data_ = this-&gt;vect_allocator_.allocate(i, <span class="keyword">this</span>);
<a name="l00233"></a>00233 
<a name="l00234"></a>00234 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00235"></a>00235 <span class="preprocessor"></span>      }
<a name="l00236"></a>00236     <span class="keywordflow">catch</span> (...)
<a name="l00237"></a>00237       {
<a name="l00238"></a>00238         this-&gt;m_ = 0;
<a name="l00239"></a>00239         this-&gt;data_ = NULL;
<a name="l00240"></a>00240       }
<a name="l00241"></a>00241     <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00242"></a>00242       this-&gt;m_ = 0;
<a name="l00243"></a>00243     <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; i != 0)
<a name="l00244"></a>00244       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::Vector(int)&quot;</span>,
<a name="l00245"></a>00245                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate memory for a vector of size &quot;</span>)
<a name="l00246"></a>00246                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i*<span class="keyword">sizeof</span>(T)) + <span class="stringliteral">&quot; bytes (&quot;</span>
<a name="l00247"></a>00247                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00248"></a>00248 <span class="preprocessor">#endif</span>
<a name="l00249"></a>00249 <span class="preprocessor"></span>
<a name="l00250"></a>00250   }
<a name="l00251"></a>00251 
<a name="l00252"></a>00252 
<a name="l00254"></a>00254 
<a name="l00264"></a>00264   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00265"></a>00265   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;</a>
<a name="l00266"></a>00266 <a class="code" href="class_seldon_1_1_vector.php">  ::Vector</a>(<span class="keywordtype">int</span> i, <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::pointer</a> data):
<a name="l00267"></a>00267     <a class="code" href="class_seldon_1_1_vector___base.php" title="Base structure for all vectors.">Vector_Base</a>&lt;T, Allocator&gt;()
<a name="l00268"></a>00268   {
<a name="l00269"></a>00269     SetData(i, data);
<a name="l00270"></a>00270   }
<a name="l00271"></a>00271 
<a name="l00272"></a>00272 
<a name="l00274"></a>00274 
<a name="l00277"></a>00277   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00278"></a>00278   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a3220e19bce6b4a0f72dfca147dbf8b38" title="Default constructor.">Vector&lt;T, VectFull, Allocator&gt;::</a>
<a name="l00279"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a1afba3b1f5b3d8022dd4e047185968d8">00279</a> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a3220e19bce6b4a0f72dfca147dbf8b38" title="Default constructor.">  Vector</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a>&amp; V):
<a name="l00280"></a>00280     <a class="code" href="class_seldon_1_1_vector___base.php" title="Base structure for all vectors.">Vector_Base</a>&lt;T, Allocator&gt;(V)
<a name="l00281"></a>00281   {
<a name="l00282"></a>00282 
<a name="l00283"></a>00283 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00284"></a>00284 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00285"></a>00285       {
<a name="l00286"></a>00286 <span class="preprocessor">#endif</span>
<a name="l00287"></a>00287 <span class="preprocessor"></span>
<a name="l00288"></a>00288         this-&gt;data_ = this-&gt;vect_allocator_.allocate(V.<a class="code" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae" title="Returns the number of elements.">GetM</a>(), <span class="keyword">this</span>);
<a name="l00289"></a>00289 
<a name="l00290"></a>00290 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00291"></a>00291 <span class="preprocessor"></span>      }
<a name="l00292"></a>00292     <span class="keywordflow">catch</span> (...)
<a name="l00293"></a>00293       {
<a name="l00294"></a>00294         this-&gt;m_ = 0;
<a name="l00295"></a>00295         this-&gt;data_ = NULL;
<a name="l00296"></a>00296       }
<a name="l00297"></a>00297     <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00298"></a>00298       this-&gt;m_ = 0;
<a name="l00299"></a>00299     <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; V.<a class="code" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae" title="Returns the number of elements.">GetM</a>() != 0)
<a name="l00300"></a>00300       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::Vector(Vector&lt;VectFull&gt;&amp;)&quot;</span>,
<a name="l00301"></a>00301                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate memory for a vector of size &quot;</span>)
<a name="l00302"></a>00302                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(V.<a class="code" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae" title="Returns the number of elements.">GetM</a>()*<span class="keyword">sizeof</span>(T)) + <span class="stringliteral">&quot; bytes (&quot;</span>
<a name="l00303"></a>00303                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(V.<a class="code" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae" title="Returns the number of elements.">GetM</a>()) + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00304"></a>00304 <span class="preprocessor">#endif</span>
<a name="l00305"></a>00305 <span class="preprocessor"></span>
<a name="l00306"></a>00306     this-&gt;vect_allocator_.memorycpy(this-&gt;data_, V.<a class="code" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a" title="Returns a pointer to data_ (stored data).">GetData</a>(), V.<a class="code" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae" title="Returns the number of elements.">GetM</a>());
<a name="l00307"></a>00307 
<a name="l00308"></a>00308   }
<a name="l00309"></a>00309 
<a name="l00310"></a>00310 
<a name="l00311"></a>00311   <span class="comment">/**************</span>
<a name="l00312"></a>00312 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00313"></a>00313 <span class="comment">   **************/</span>
<a name="l00314"></a>00314 
<a name="l00315"></a>00315 
<a name="l00317"></a>00317   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00318"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#afe4b7f7be7d9dedb8e1275f283706c43">00318</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::~Vector</a>()
<a name="l00319"></a>00319   {
<a name="l00320"></a>00320   }
<a name="l00321"></a>00321 
<a name="l00322"></a>00322 
<a name="l00323"></a>00323   <span class="comment">/*********************</span>
<a name="l00324"></a>00324 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00325"></a>00325 <span class="comment">   *********************/</span>
<a name="l00326"></a>00326 
<a name="l00327"></a>00327 
<a name="l00329"></a>00329 
<a name="l00333"></a>00333   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00334"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a5120b4ecff05d8cf54bbd57fb280fbe1">00334</a>   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Clear</a>()
<a name="l00335"></a>00335   {
<a name="l00336"></a>00336     this-&gt;~<a class="code" href="class_seldon_1_1_vector.php">Vector</a>();
<a name="l00337"></a>00337   }
<a name="l00338"></a>00338 
<a name="l00339"></a>00339 
<a name="l00341"></a>00341 
<a name="l00347"></a>00347   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00348"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a6c1667ecf1265c0870ba2828b33a2d79">00348</a>   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Reallocate</a>(<span class="keywordtype">int</span> i)
<a name="l00349"></a>00349   {
<a name="l00350"></a>00350 
<a name="l00351"></a>00351     <span class="keywordflow">if</span> (i != this-&gt;m_)
<a name="l00352"></a>00352       {
<a name="l00353"></a>00353 
<a name="l00354"></a>00354         this-&gt;m_ = i;
<a name="l00355"></a>00355 
<a name="l00356"></a>00356 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00357"></a>00357 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l00358"></a>00358           {
<a name="l00359"></a>00359 <span class="preprocessor">#endif</span>
<a name="l00360"></a>00360 <span class="preprocessor"></span>
<a name="l00361"></a>00361             this-&gt;data_ =
<a name="l00362"></a>00362               <span class="keyword">reinterpret_cast&lt;</span>pointer<span class="keyword">&gt;</span>(this-&gt;vect_allocator_
<a name="l00363"></a>00363                                         .reallocate(this-&gt;data_, i, <span class="keyword">this</span>));
<a name="l00364"></a>00364 
<a name="l00365"></a>00365 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00366"></a>00366 <span class="preprocessor"></span>          }
<a name="l00367"></a>00367         <span class="keywordflow">catch</span> (...)
<a name="l00368"></a>00368           {
<a name="l00369"></a>00369             this-&gt;m_ = 0;
<a name="l00370"></a>00370             this-&gt;data_ = NULL;
<a name="l00371"></a>00371             <span class="keywordflow">return</span>;
<a name="l00372"></a>00372           }
<a name="l00373"></a>00373         <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00374"></a>00374           {
<a name="l00375"></a>00375             this-&gt;m_ = 0;
<a name="l00376"></a>00376             <span class="keywordflow">return</span>;
<a name="l00377"></a>00377           }
<a name="l00378"></a>00378 <span class="preprocessor">#endif</span>
<a name="l00379"></a>00379 <span class="preprocessor"></span>
<a name="l00380"></a>00380       }
<a name="l00381"></a>00381   }
<a name="l00382"></a>00382 
<a name="l00383"></a>00383 
<a name="l00385"></a>00385 
<a name="l00389"></a>00389   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00390"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a99dda1b202ce562645f890897c593515">00390</a>   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Resize</a>(<span class="keywordtype">int</span> n)
<a name="l00391"></a>00391   {
<a name="l00392"></a>00392 
<a name="l00393"></a>00393     <span class="keywordflow">if</span> (n == this-&gt;m_)
<a name="l00394"></a>00394       <span class="keywordflow">return</span>;
<a name="l00395"></a>00395 
<a name="l00396"></a>00396     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> X_new(n);
<a name="l00397"></a>00397     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; min(this-&gt;m_, n); i++)
<a name="l00398"></a>00398       X_new(i) = this-&gt;data_[i];
<a name="l00399"></a>00399 
<a name="l00400"></a>00400     SetData(n, X_new.<a class="code" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a" title="Returns a pointer to data_ (stored data).">GetData</a>());
<a name="l00401"></a>00401     X_new.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a151dc47113d07ab24775733d0c46dc90" title="Clears the vector without releasing memory.">Nullify</a>();
<a name="l00402"></a>00402   }
<a name="l00403"></a>00403 
<a name="l00404"></a>00404 
<a name="l00407"></a>00407 
<a name="l00419"></a>00419   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00420"></a>00420   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;</a>
<a name="l00421"></a>00421 <a class="code" href="class_seldon_1_1_vector.php">  ::SetData</a>(<span class="keywordtype">int</span> i, <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::pointer</a> data)
<a name="l00422"></a>00422   {
<a name="l00423"></a>00423     this-&gt;Clear();
<a name="l00424"></a>00424 
<a name="l00425"></a>00425     this-&gt;m_ = i;
<a name="l00426"></a>00426 
<a name="l00427"></a>00427     this-&gt;data_ = data;
<a name="l00428"></a>00428   }
<a name="l00429"></a>00429 
<a name="l00430"></a>00430 
<a name="l00432"></a>00432 
<a name="l00443"></a>00443   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00444"></a>00444   <span class="keyword">template</span> &lt;<span class="keyword">class</span> Allocator0&gt;
<a name="l00445"></a>00445   <span class="keyword">inline</span> <span class="keywordtype">void</span> Vector&lt;T, VectFull, Allocator&gt;
<a name="l00446"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ab12cedc7bee632a7b210312f1e7ef28b">00446</a>   ::SetData(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator0&gt;</a>&amp; V)
<a name="l00447"></a>00447   {
<a name="l00448"></a>00448     SetData(V.GetLength(), V.GetData());
<a name="l00449"></a>00449   }
<a name="l00450"></a>00450 
<a name="l00451"></a>00451 
<a name="l00453"></a>00453 
<a name="l00458"></a>00458   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00459"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a151dc47113d07ab24775733d0c46dc90">00459</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Nullify</a>()
<a name="l00460"></a>00460   {
<a name="l00461"></a>00461     this-&gt;m_ = 0;
<a name="l00462"></a>00462     this-&gt;data_ = NULL;
<a name="l00463"></a>00463   }
<a name="l00464"></a>00464 
<a name="l00465"></a>00465 
<a name="l00466"></a>00466   <span class="comment">/**********************************</span>
<a name="l00467"></a>00467 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l00468"></a>00468 <span class="comment">   **********************************/</span>
<a name="l00469"></a>00469 
<a name="l00470"></a>00470 
<a name="l00472"></a>00472 
<a name="l00476"></a>00476   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00477"></a>00477   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::reference</a>
<a name="l00478"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#abb6f9d844071aaa8c8bc1ac62b1dba07">00478</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i)
<a name="l00479"></a>00479   {
<a name="l00480"></a>00480 
<a name="l00481"></a>00481 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00482"></a>00482 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00483"></a>00483       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::operator()&quot;</span>,
<a name="l00484"></a>00484                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00485"></a>00485                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00486"></a>00486 <span class="preprocessor">#endif</span>
<a name="l00487"></a>00487 <span class="preprocessor"></span>
<a name="l00488"></a>00488     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00489"></a>00489   }
<a name="l00490"></a>00490 
<a name="l00491"></a>00491 
<a name="l00493"></a>00493 
<a name="l00497"></a>00497   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00498"></a>00498   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::const_reference</a>
<a name="l00499"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#af6ee550855ccf2a4db736bad2352929f">00499</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00500"></a>00500 <span class="keyword">  </span>{
<a name="l00501"></a>00501 
<a name="l00502"></a>00502 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00503"></a>00503 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00504"></a>00504       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::operator()&quot;</span>,
<a name="l00505"></a>00505                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00506"></a>00506                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00507"></a>00507 <span class="preprocessor">#endif</span>
<a name="l00508"></a>00508 <span class="preprocessor"></span>
<a name="l00509"></a>00509     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00510"></a>00510   }
<a name="l00511"></a>00511 
<a name="l00512"></a>00512 
<a name="l00514"></a>00514 
<a name="l00519"></a>00519   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00520"></a>00520   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a>&amp; <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;</a>
<a name="l00521"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#abd12db9e14f5ba1c417d4c587e6d359a">00521</a> <a class="code" href="class_seldon_1_1_vector.php">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a>&amp; X)
<a name="l00522"></a>00522   {
<a name="l00523"></a>00523     this-&gt;Copy(X);
<a name="l00524"></a>00524 
<a name="l00525"></a>00525     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00526"></a>00526   }
<a name="l00527"></a>00527 
<a name="l00528"></a>00528 
<a name="l00530"></a>00530 
<a name="l00535"></a>00535   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00536"></a>00536   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;</a>
<a name="l00537"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a790a38cb578627bf17291088f2cd2388">00537</a> <a class="code" href="class_seldon_1_1_vector.php">  ::Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a>&amp; X)
<a name="l00538"></a>00538   {
<a name="l00539"></a>00539     this-&gt;Reallocate(X.<a class="code" href="class_seldon_1_1_vector___base.php#a075daae3607a4767a77a3de60ab30ba7" title="Returns the number of elements.">GetLength</a>());
<a name="l00540"></a>00540 
<a name="l00541"></a>00541     this-&gt;vect_allocator_.memorycpy(this-&gt;data_, X.<a class="code" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a" title="Returns a pointer to data_ (stored data).">GetData</a>(), this-&gt;m_);
<a name="l00542"></a>00542   }
<a name="l00543"></a>00543 
<a name="l00544"></a>00544 
<a name="l00546"></a>00546 
<a name="l00549"></a>00549   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l00550"></a>00550   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a>&amp; <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;</a>
<a name="l00551"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a236fd63d659f7d92f3b12da557380cb3">00551</a> <a class="code" href="class_seldon_1_1_vector.php">  ::operator*= </a>(<span class="keyword">const</span> T0&amp; alpha)
<a name="l00552"></a>00552   {
<a name="l00553"></a>00553     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00554"></a>00554       this-&gt;data_[i] *= alpha;
<a name="l00555"></a>00555 
<a name="l00556"></a>00556     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00557"></a>00557   }
<a name="l00558"></a>00558 
<a name="l00559"></a>00559 
<a name="l00561"></a>00561 
<a name="l00566"></a>00566   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00567"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ab3773a41209f76c5739be8f7f037006c">00567</a>   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Append</a>(<span class="keyword">const</span> T&amp; x)
<a name="l00568"></a>00568   {
<a name="l00569"></a>00569     <span class="keywordtype">int</span> i = this-&gt;GetLength();
<a name="l00570"></a>00570     this-&gt;Reallocate(i + 1);
<a name="l00571"></a>00571     this-&gt;data_[i] = x;
<a name="l00572"></a>00572   }
<a name="l00573"></a>00573 
<a name="l00574"></a>00574 
<a name="l00576"></a>00576 
<a name="l00579"></a>00579   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l00580"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a6a64b54653758dbbdcd650c8ffba4d52">00580</a>   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::PushBack</a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00581"></a>00581   {
<a name="l00582"></a>00582     Resize(this-&gt;m_+1);
<a name="l00583"></a>00583     this-&gt;data_[this-&gt;m_-1] = x;
<a name="l00584"></a>00584   }
<a name="l00585"></a>00585 
<a name="l00586"></a>00586 
<a name="l00588"></a>00588 
<a name="l00591"></a>00591   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator0&gt;
<a name="l00592"></a>00592   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;</a>
<a name="l00593"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#aa0b88a0415692922c36c2b2f284efeca">00593</a> <a class="code" href="class_seldon_1_1_vector.php">  ::PushBack</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator0&gt;</a>&amp; X)
<a name="l00594"></a>00594   {
<a name="l00595"></a>00595     <span class="keywordtype">int</span> Nold = this-&gt;m_;
<a name="l00596"></a>00596     Resize(this-&gt;m_ + X.GetM());
<a name="l00597"></a>00597     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetM(); i++)
<a name="l00598"></a>00598       this-&gt;data_[Nold+i] = X(i);
<a name="l00599"></a>00599   }
<a name="l00600"></a>00600 
<a name="l00601"></a>00601 
<a name="l00602"></a>00602   <span class="comment">/*******************</span>
<a name="l00603"></a>00603 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00604"></a>00604 <span class="comment">   *******************/</span>
<a name="l00605"></a>00605 
<a name="l00606"></a>00606 
<a name="l00608"></a>00608 
<a name="l00611"></a>00611   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00612"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a3906b8279cc318dc0a8a672fa781095a">00612</a>   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::GetDataSize</a>()
<a name="l00613"></a>00613   {
<a name="l00614"></a>00614     <span class="keywordflow">return</span> this-&gt;m_;
<a name="l00615"></a>00615   }
<a name="l00616"></a>00616 
<a name="l00617"></a>00617 
<a name="l00618"></a>00618   <span class="comment">/************************</span>
<a name="l00619"></a>00619 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l00620"></a>00620 <span class="comment">   ************************/</span>
<a name="l00621"></a>00621 
<a name="l00622"></a>00622 
<a name="l00624"></a>00624 
<a name="l00628"></a>00628   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00629"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ae8e4d6b081a6f4c90b2993b047a04dac">00629</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Zero</a>()
<a name="l00630"></a>00630   {
<a name="l00631"></a>00631     this-&gt;vect_allocator_.memoryset(this-&gt;data_, <span class="keywordtype">char</span>(0),
<a name="l00632"></a>00632                                     this-&gt;GetDataSize() * <span class="keyword">sizeof</span>(value_type));
<a name="l00633"></a>00633   }
<a name="l00634"></a>00634 
<a name="l00635"></a>00635 
<a name="l00637"></a>00637   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00638"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a722046c309ee8e50abd68a5a5a77aa60">00638</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Fill</a>()
<a name="l00639"></a>00639   {
<a name="l00640"></a>00640     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00641"></a>00641       this-&gt;data_[i] = i;
<a name="l00642"></a>00642   }
<a name="l00643"></a>00643 
<a name="l00644"></a>00644 
<a name="l00646"></a>00646 
<a name="l00649"></a>00649   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00650"></a>00650   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00651"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a2c103fe432a6f6a9c37cb133bb837153">00651</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Fill</a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00652"></a>00652   {
<a name="l00653"></a>00653     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00654"></a>00654       this-&gt;data_[i] = x;
<a name="l00655"></a>00655   }
<a name="l00656"></a>00656 
<a name="l00657"></a>00657 
<a name="l00659"></a>00659 
<a name="l00662"></a>00662   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00663"></a>00663   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00664"></a>00664   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a>&amp;
<a name="l00665"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a0bf814aab7883056808671f5b6866ba6">00665</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00666"></a>00666   {
<a name="l00667"></a>00667     this-&gt;Fill(x);
<a name="l00668"></a>00668 
<a name="l00669"></a>00669     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00670"></a>00670   }
<a name="l00671"></a>00671 
<a name="l00672"></a>00672 
<a name="l00674"></a>00674 
<a name="l00677"></a>00677   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00678"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a71147cebc8bd36cc103d2e6fd3767f91">00678</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::FillRand</a>()
<a name="l00679"></a>00679   {
<a name="l00680"></a>00680     srand(time(NULL));
<a name="l00681"></a>00681     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00682"></a>00682       this-&gt;data_[i] = rand();
<a name="l00683"></a>00683   }
<a name="l00684"></a>00684 
<a name="l00685"></a>00685 
<a name="l00687"></a>00687   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00688"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#adba43c4d26c5b47cd5868e26418f147c">00688</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00689"></a>00689 <span class="keyword">  </span>{
<a name="l00690"></a>00690     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetLength(); i++)
<a name="l00691"></a>00691       cout &lt;&lt; (*<span class="keyword">this</span>)(i) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00692"></a>00692     cout &lt;&lt; endl;
<a name="l00693"></a>00693   }
<a name="l00694"></a>00694 
<a name="l00695"></a>00695 
<a name="l00696"></a>00696   <span class="comment">/*********</span>
<a name="l00697"></a>00697 <span class="comment">   * NORMS *</span>
<a name="l00698"></a>00698 <span class="comment">   *********/</span>
<a name="l00699"></a>00699 
<a name="l00700"></a>00700 
<a name="l00702"></a>00702 
<a name="l00705"></a>00705   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00706"></a>00706   <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::value_type</a>
<a name="l00707"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a165cbeeddb4a2a356accc00d609b4211">00707</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::GetNormInf</a>()<span class="keyword"> const</span>
<a name="l00708"></a>00708 <span class="keyword">  </span>{
<a name="l00709"></a>00709     value_type res = value_type(0);
<a name="l00710"></a>00710     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetLength(); i++)
<a name="l00711"></a>00711       {
<a name="l00712"></a>00712         res = max(res, this-&gt;data_[i]);
<a name="l00713"></a>00713         res = max(res, T(-this-&gt;data_[i]));
<a name="l00714"></a>00714       }
<a name="l00715"></a>00715 
<a name="l00716"></a>00716     <span class="keywordflow">return</span> res;
<a name="l00717"></a>00717   }
<a name="l00718"></a>00718 
<a name="l00719"></a>00719 
<a name="l00721"></a>00721 
<a name="l00724"></a>00724   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00725"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a50186150708e90f86ded59e33cb172a6">00725</a>   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::GetNormInfIndex</a>()<span class="keyword"> const</span>
<a name="l00726"></a>00726 <span class="keyword">  </span>{
<a name="l00727"></a>00727 
<a name="l00728"></a>00728 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00729"></a>00729 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (this-&gt;GetLength() == 0)
<a name="l00730"></a>00730       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::GetNormInfIndex()&quot;</span>,
<a name="l00731"></a>00731                      <span class="stringliteral">&quot;Vector is null.&quot;</span>);
<a name="l00732"></a>00732 <span class="preprocessor">#endif</span>
<a name="l00733"></a>00733 <span class="preprocessor"></span>
<a name="l00734"></a>00734     value_type res = value_type(0), temp;
<a name="l00735"></a>00735     <span class="keywordtype">int</span> j = 0;
<a name="l00736"></a>00736     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetLength(); i++)
<a name="l00737"></a>00737       {
<a name="l00738"></a>00738         temp = res;
<a name="l00739"></a>00739         res = max(res, this-&gt;data_[i]);
<a name="l00740"></a>00740         res = max(res, T(-this-&gt;data_[i]));
<a name="l00741"></a>00741         <span class="keywordflow">if</span> (temp != res) j = i;
<a name="l00742"></a>00742       }
<a name="l00743"></a>00743 
<a name="l00744"></a>00744     <span class="keywordflow">return</span> j;
<a name="l00745"></a>00745   }
<a name="l00746"></a>00746 
<a name="l00747"></a>00747 
<a name="l00748"></a>00748   <span class="comment">/**************************</span>
<a name="l00749"></a>00749 <span class="comment">   * OUTPUT/INPUT FUNCTIONS *</span>
<a name="l00750"></a>00750 <span class="comment">   **************************/</span>
<a name="l00751"></a>00751 
<a name="l00752"></a>00752 
<a name="l00754"></a>00754 
<a name="l00760"></a>00760   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00761"></a>00761   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;</a>
<a name="l00762"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a51ea9d8b0a3176fc903c0279ae8770d0">00762</a> <a class="code" href="class_seldon_1_1_vector.php">  ::Write</a>(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size)<span class="keyword"> const</span>
<a name="l00763"></a>00763 <span class="keyword">  </span>{
<a name="l00764"></a>00764     ofstream FileStream;
<a name="l00765"></a>00765     FileStream.open(FileName.c_str());
<a name="l00766"></a>00766 
<a name="l00767"></a>00767 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00768"></a>00768 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00769"></a>00769     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00770"></a>00770       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::Write(string FileName)&quot;</span>,
<a name="l00771"></a>00771                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00772"></a>00772 <span class="preprocessor">#endif</span>
<a name="l00773"></a>00773 <span class="preprocessor"></span>
<a name="l00774"></a>00774     this-&gt;Write(FileStream, with_size);
<a name="l00775"></a>00775 
<a name="l00776"></a>00776     FileStream.close();
<a name="l00777"></a>00777   }
<a name="l00778"></a>00778 
<a name="l00779"></a>00779 
<a name="l00781"></a>00781 
<a name="l00787"></a>00787   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00788"></a>00788   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;</a>
<a name="l00789"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a1a098ef03b3d8cad79b659cd05be220e">00789</a> <a class="code" href="class_seldon_1_1_vector.php">  ::Write</a>(ostream&amp; FileStream, <span class="keywordtype">bool</span> with_size)<span class="keyword"> const</span>
<a name="l00790"></a>00790 <span class="keyword">  </span>{
<a name="l00791"></a>00791 
<a name="l00792"></a>00792 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00793"></a>00793 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00794"></a>00794     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00795"></a>00795       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::Write(ostream&amp; FileStream)&quot;</span>,
<a name="l00796"></a>00796                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00797"></a>00797 <span class="preprocessor">#endif</span>
<a name="l00798"></a>00798 <span class="preprocessor"></span>
<a name="l00799"></a>00799     <span class="keywordflow">if</span> (with_size)
<a name="l00800"></a>00800       FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l00801"></a>00801                        <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00802"></a>00802 
<a name="l00803"></a>00803     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l00804"></a>00804                      this-&gt;m_ * <span class="keyword">sizeof</span>(value_type));
<a name="l00805"></a>00805 
<a name="l00806"></a>00806 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00807"></a>00807 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00808"></a>00808     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00809"></a>00809       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::Write(ostream&amp; FileStream)&quot;</span>,
<a name="l00810"></a>00810                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l00811"></a>00811 <span class="preprocessor">#endif</span>
<a name="l00812"></a>00812 <span class="preprocessor"></span>
<a name="l00813"></a>00813   }
<a name="l00814"></a>00814 
<a name="l00815"></a>00815 
<a name="l00817"></a>00817 
<a name="l00822"></a>00822   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00823"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a39fd5c75b22c5c2d37a47587f57abad2">00823</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00824"></a>00824 <span class="keyword">  </span>{
<a name="l00825"></a>00825     ofstream FileStream;
<a name="l00826"></a>00826     FileStream.precision(cout.precision());
<a name="l00827"></a>00827     FileStream.flags(cout.flags());
<a name="l00828"></a>00828     FileStream.open(FileName.c_str());
<a name="l00829"></a>00829 
<a name="l00830"></a>00830 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00831"></a>00831 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00832"></a>00832     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00833"></a>00833       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::WriteText(string FileName)&quot;</span>,
<a name="l00834"></a>00834                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00835"></a>00835 <span class="preprocessor">#endif</span>
<a name="l00836"></a>00836 <span class="preprocessor"></span>
<a name="l00837"></a>00837     this-&gt;WriteText(FileStream);
<a name="l00838"></a>00838 
<a name="l00839"></a>00839     FileStream.close();
<a name="l00840"></a>00840   }
<a name="l00841"></a>00841 
<a name="l00842"></a>00842 
<a name="l00844"></a>00844 
<a name="l00849"></a>00849   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00850"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ab5bb2e8cf1a7ceb1ac941a88ed1635e6">00850</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00851"></a>00851 <span class="keyword">  </span>{
<a name="l00852"></a>00852 
<a name="l00853"></a>00853 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00854"></a>00854 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00855"></a>00855     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00856"></a>00856       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::WriteText(ostream&amp; FileStream)&quot;</span>,
<a name="l00857"></a>00857                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00858"></a>00858 <span class="preprocessor">#endif</span>
<a name="l00859"></a>00859 <span class="preprocessor"></span>
<a name="l00860"></a>00860     <span class="keywordflow">if</span> (this-&gt;GetLength() != 0)
<a name="l00861"></a>00861       FileStream &lt;&lt; (*<span class="keyword">this</span>)(0);
<a name="l00862"></a>00862 
<a name="l00863"></a>00863     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 1; i &lt; this-&gt;GetLength(); i++)
<a name="l00864"></a>00864       FileStream &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span> &lt;&lt; (*<span class="keyword">this</span>)(i);
<a name="l00865"></a>00865 
<a name="l00866"></a>00866 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00867"></a>00867 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00868"></a>00868     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00869"></a>00869       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::WriteText(ostream&amp; FileStream)&quot;</span>,
<a name="l00870"></a>00870                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l00871"></a>00871 <span class="preprocessor">#endif</span>
<a name="l00872"></a>00872 <span class="preprocessor"></span>
<a name="l00873"></a>00873   }
<a name="l00874"></a>00874 
<a name="l00875"></a>00875 
<a name="l00877"></a>00877 
<a name="l00885"></a>00885   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00886"></a>00886   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;</a>
<a name="l00887"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a2ce4a8d2726b069e465d4156b1b531be">00887</a> <a class="code" href="class_seldon_1_1_vector.php">  ::Read</a>(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size)
<a name="l00888"></a>00888   {
<a name="l00889"></a>00889     ifstream FileStream;
<a name="l00890"></a>00890     FileStream.open(FileName.c_str());
<a name="l00891"></a>00891 
<a name="l00892"></a>00892 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00893"></a>00893 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00894"></a>00894     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00895"></a>00895       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::Read(string FileName)&quot;</span>,
<a name="l00896"></a>00896                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00897"></a>00897 <span class="preprocessor">#endif</span>
<a name="l00898"></a>00898 <span class="preprocessor"></span>
<a name="l00899"></a>00899     this-&gt;Read(FileStream, with_size);
<a name="l00900"></a>00900 
<a name="l00901"></a>00901     FileStream.close();
<a name="l00902"></a>00902   }
<a name="l00903"></a>00903 
<a name="l00904"></a>00904 
<a name="l00906"></a>00906 
<a name="l00914"></a>00914   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00915"></a>00915   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;</a>
<a name="l00916"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a634f8993354070451e53fd0c29bffa49">00916</a> <a class="code" href="class_seldon_1_1_vector.php">  ::Read</a>(istream&amp; FileStream, <span class="keywordtype">bool</span> with_size)
<a name="l00917"></a>00917   {
<a name="l00918"></a>00918 
<a name="l00919"></a>00919 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00920"></a>00920 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00921"></a>00921     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00922"></a>00922       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::Read(istream&amp; FileStream)&quot;</span>,
<a name="l00923"></a>00923                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00924"></a>00924 <span class="preprocessor">#endif</span>
<a name="l00925"></a>00925 <span class="preprocessor"></span>
<a name="l00926"></a>00926     <span class="keywordflow">if</span> (with_size)
<a name="l00927"></a>00927       {
<a name="l00928"></a>00928         <span class="keywordtype">int</span> new_size;
<a name="l00929"></a>00929         FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;new_size), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00930"></a>00930         this-&gt;Reallocate(new_size);
<a name="l00931"></a>00931       }
<a name="l00932"></a>00932 
<a name="l00933"></a>00933     FileStream.read(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l00934"></a>00934                     this-&gt;GetLength() * <span class="keyword">sizeof</span>(value_type));
<a name="l00935"></a>00935 
<a name="l00936"></a>00936 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00937"></a>00937 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l00938"></a>00938     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00939"></a>00939       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::Read(istream&amp; FileStream)&quot;</span>,
<a name="l00940"></a>00940                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l00941"></a>00941 <span class="preprocessor">#endif</span>
<a name="l00942"></a>00942 <span class="preprocessor"></span>
<a name="l00943"></a>00943   }
<a name="l00944"></a>00944 
<a name="l00945"></a>00945 
<a name="l00947"></a>00947 
<a name="l00952"></a>00952   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00953"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#af1a8f91e66d729c91fddb0a0c9e030b8">00953</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::ReadText</a>(<span class="keywordtype">string</span> FileName)
<a name="l00954"></a>00954   {
<a name="l00955"></a>00955     ifstream FileStream;
<a name="l00956"></a>00956     FileStream.open(FileName.c_str());
<a name="l00957"></a>00957 
<a name="l00958"></a>00958 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00959"></a>00959 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00960"></a>00960     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00961"></a>00961       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::ReadText(string FileName)&quot;</span>,
<a name="l00962"></a>00962                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00963"></a>00963 <span class="preprocessor">#endif</span>
<a name="l00964"></a>00964 <span class="preprocessor"></span>
<a name="l00965"></a>00965     this-&gt;ReadText(FileStream);
<a name="l00966"></a>00966 
<a name="l00967"></a>00967     FileStream.close();
<a name="l00968"></a>00968   }
<a name="l00969"></a>00969 
<a name="l00970"></a>00970 
<a name="l00972"></a>00972 
<a name="l00977"></a>00977   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00978"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#ae0f8292f742367edca616ab9e2e93e6b">00978</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator&gt;::ReadText</a>(istream&amp; FileStream)
<a name="l00979"></a>00979   {
<a name="l00980"></a>00980     <span class="comment">// Previous values of the vector are cleared.</span>
<a name="l00981"></a>00981     Clear();
<a name="l00982"></a>00982 
<a name="l00983"></a>00983 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00984"></a>00984 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00985"></a>00985     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00986"></a>00986       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectFull&gt;::ReadText(istream&amp; FileStream)&quot;</span>,
<a name="l00987"></a>00987                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00988"></a>00988 <span class="preprocessor">#endif</span>
<a name="l00989"></a>00989 <span class="preprocessor"></span>
<a name="l00990"></a>00990     T entry;
<a name="l00991"></a>00991     <span class="keywordtype">int</span> number_element = 0;
<a name="l00992"></a>00992     <span class="keywordflow">while</span> (!FileStream.eof())
<a name="l00993"></a>00993       {
<a name="l00994"></a>00994         <span class="comment">// Reads a new entry.</span>
<a name="l00995"></a>00995         FileStream &gt;&gt; entry;
<a name="l00996"></a>00996 
<a name="l00997"></a>00997         <span class="keywordflow">if</span> (FileStream.fail())
<a name="l00998"></a>00998           <span class="keywordflow">break</span>;
<a name="l00999"></a>00999         <span class="keywordflow">else</span>
<a name="l01000"></a>01000           {
<a name="l01001"></a>01001             number_element++;
<a name="l01002"></a>01002 
<a name="l01003"></a>01003             <span class="comment">// If needed, resizes the vector. Its size is already doubled so</span>
<a name="l01004"></a>01004             <span class="comment">// that the vector should be resized a limited number of times.</span>
<a name="l01005"></a>01005             <span class="keywordflow">if</span> (number_element &gt; this-&gt;m_)
<a name="l01006"></a>01006               this-&gt;Resize(2 * number_element);
<a name="l01007"></a>01007 
<a name="l01008"></a>01008             this-&gt;data_[number_element - 1] = entry;
<a name="l01009"></a>01009           }
<a name="l01010"></a>01010       }
<a name="l01011"></a>01011 
<a name="l01012"></a>01012     <span class="comment">// Resizes to the actual size.</span>
<a name="l01013"></a>01013     <span class="keywordflow">if</span> (number_element &gt; 0)
<a name="l01014"></a>01014       this-&gt;Resize(number_element);
<a name="l01015"></a>01015     <span class="keywordflow">else</span>
<a name="l01016"></a>01016       this-&gt;Clear();
<a name="l01017"></a>01017   }
<a name="l01018"></a>01018 
<a name="l01019"></a>01019 
<a name="l01021"></a>01021 
<a name="l01026"></a>01026   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01027"></a>01027   ostream&amp; <a class="code" href="namespace_seldon.php#a5bd4a6d6f3c83048663d57d2fc032107" title="operator&amp;lt;&amp;lt; overloaded for a 3D array.">operator &lt;&lt; </a>(ostream&amp; out,
<a name="l01028"></a>01028                         <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage, Allocator&gt;</a>&amp; V)
<a name="l01029"></a>01029   {
<a name="l01030"></a>01030     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; V.GetLength() - 1; i++)
<a name="l01031"></a>01031       out &lt;&lt; V(i) &lt;&lt; <span class="charliteral">&#39;\t&#39;</span>;
<a name="l01032"></a>01032     <span class="keywordflow">if</span> (V.GetLength() != 0)
<a name="l01033"></a>01033       out &lt;&lt; V(V.GetLength() - 1);
<a name="l01034"></a>01034 
<a name="l01035"></a>01035     <span class="keywordflow">return</span> out;
<a name="l01036"></a>01036   }
<a name="l01037"></a>01037 
<a name="l01038"></a>01038 
<a name="l01039"></a>01039 } <span class="comment">// namespace Seldon.</span>
<a name="l01040"></a>01040 
<a name="l01041"></a>01041 <span class="preprocessor">#define SELDON_FILE_VECTOR_CXX</span>
<a name="l01042"></a>01042 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
