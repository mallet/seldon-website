<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/interfaces/direct/SuperLU.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_SUPERLU_HXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 <span class="keyword">extern</span> <span class="stringliteral">&quot;C&quot;</span>
<a name="l00023"></a>00023 {
<a name="l00024"></a>00024 <span class="preprocessor">#include &quot;superlu_interface.h&quot;</span>
<a name="l00025"></a>00025 }
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 <span class="keyword">namespace </span>Seldon
<a name="l00028"></a>00028 {
<a name="l00030"></a>00030   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00031"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php">00031</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php" title="class interfacing SuperLU functions">MatrixSuperLU_Base</a>
<a name="l00032"></a>00032   {
<a name="l00033"></a>00033   <span class="keyword">protected</span> :
<a name="l00035"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a6dd29bcb497b1c7175d6dc9f10ac8e53">00035</a>     SuperMatrix <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a6dd29bcb497b1c7175d6dc9f10ac8e53" title="objects of SuperLU">A</a>, L, U, B;
<a name="l00036"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a3639abafe150759f173dc6d6a654c534">00036</a>     SCformat *<a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a3639abafe150759f173dc6d6a654c534" title="object of SuperLU">Lstore</a>;  
<a name="l00037"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a3c0030723c06e7bffc470c94b344c00e">00037</a>     NCformat *<a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a3c0030723c06e7bffc470c94b344c00e" title="object of SuperLU">Ustore</a>;  
<a name="l00038"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a51a947858f30367b812df38f97ec3aa4">00038</a>     SuperLUStat_t <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a51a947858f30367b812df38f97ec3aa4" title="statistics">stat</a>; 
<a name="l00039"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a3de025ec4fdc83b5c793d69becc051c1">00039</a>     superlu_options_t <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a3de025ec4fdc83b5c793d69becc051c1" title="options //! permutation array">options</a>; 
<a name="l00040"></a>00040 
<a name="l00041"></a>00041     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> perm_r, perm_c;
<a name="l00042"></a>00042 
<a name="l00043"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a8d4acdd57e0a5e8a8a0651e1de0a8547">00043</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a8d4acdd57e0a5e8a8a0651e1de0a8547" title="ordering scheme">permc_spec</a>; 
<a name="l00044"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a3a2831b9dac4fa42c7815015627f5f38">00044</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a3a2831b9dac4fa42c7815015627f5f38" title="number of rows">n</a>; 
<a name="l00045"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a5b067c2c8ef0a0fcbfec31d399fd2bc3">00045</a>     <span class="keywordtype">bool</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a5b067c2c8ef0a0fcbfec31d399fd2bc3" title="display information about factorization ?">display_info</a>; 
<a name="l00046"></a>00046 
<a name="l00047"></a>00047   <span class="keyword">public</span> :
<a name="l00048"></a>00048     <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a526c7f0e29c86b81fc4705b437e167f9" title="default constructor">MatrixSuperLU_Base</a>();
<a name="l00049"></a>00049     <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a8f0a2adf458ec4f187f3936fea612106" title="destructor">~MatrixSuperLU_Base</a>();
<a name="l00050"></a>00050 
<a name="l00051"></a>00051     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00052"></a>00052     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a6492cdcde57b8524db187be6f4602ef5" title="Returns the LU factorization.">GetLU</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, Prop, ColSparse, Allocator&gt;</a>&amp; Lmat,
<a name="l00053"></a>00053                <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, Prop, ColSparse, Allocator&gt;</a>&amp; Umat,
<a name="l00054"></a>00054                <span class="keywordtype">bool</span> permuted = <span class="keyword">true</span>);
<a name="l00055"></a>00055     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00056"></a>00056     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a6492cdcde57b8524db187be6f4602ef5" title="Returns the LU factorization.">GetLU</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, Prop, RowSparse, Allocator&gt;</a>&amp; Lmat,
<a name="l00057"></a>00057                <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, Prop, RowSparse, Allocator&gt;</a>&amp; Umat,
<a name="l00058"></a>00058                <span class="keywordtype">bool</span> permuted = <span class="keyword">true</span>);
<a name="l00059"></a>00059     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a>&amp; <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a27aca5955b9d58ab561f652f153139ee" title="Returns the permutation of rows.">GetRowPermutation</a>() <span class="keyword">const</span>;
<a name="l00060"></a>00060     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a>&amp; <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#acf6c8120e481e56c99e77bea79b30f84" title="Returns the permutation of columns.">GetColPermutation</a>() <span class="keyword">const</span>;
<a name="l00061"></a>00061 
<a name="l00062"></a>00062     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#acac4d12b1d9e464f630b7d56b2c61a23" title="same effect as a call to the destructor">Clear</a>();
<a name="l00063"></a>00063     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#a733cd4e38035ae701435f9f298442e4d" title="no message from SuperLU">HideMessages</a>();
<a name="l00064"></a>00064     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php#af5322d1293240d5d3ab6aeb93183acc7" title="allows messages from SuperLU">ShowMessages</a>();
<a name="l00065"></a>00065 
<a name="l00066"></a>00066   };
<a name="l00067"></a>00067 
<a name="l00069"></a>00069   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00070"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u.php">00070</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix_super_l_u.php" title="empty matrix">MatrixSuperLU</a> : <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php" title="class interfacing SuperLU functions">MatrixSuperLU_Base</a>&lt;T&gt;
<a name="l00071"></a>00071   {
<a name="l00072"></a>00072   };
<a name="l00073"></a>00073 
<a name="l00075"></a>00075   <span class="keyword">template</span>&lt;&gt;
<a name="l00076"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u_3_01double_01_4.php">00076</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix_super_l_u.php" title="empty matrix">MatrixSuperLU</a>&lt;double&gt; : <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php" title="class interfacing SuperLU functions">MatrixSuperLU_Base</a>&lt;double&gt;
<a name="l00077"></a>00077   {
<a name="l00078"></a>00078   <span class="keyword">public</span>:
<a name="l00079"></a>00079     <a class="code" href="class_seldon_1_1_matrix_super_l_u.php" title="empty matrix">MatrixSuperLU</a>() : <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php">MatrixSuperLU_Base&lt;double&gt;</a>() {}
<a name="l00080"></a>00080 
<a name="l00081"></a>00081     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00082"></a>00082     <span class="keywordtype">void</span> FactorizeMatrix(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;double, Prop, Storage, Allocator&gt;</a> &amp; mat,
<a name="l00083"></a>00083                          <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>);
<a name="l00084"></a>00084 
<a name="l00085"></a>00085     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2&gt;
<a name="l00086"></a>00086     <span class="keywordtype">void</span> Solve(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double, VectFull, Allocator2&gt;</a>&amp; x);
<a name="l00087"></a>00087 
<a name="l00088"></a>00088     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2&gt;
<a name="l00089"></a>00089     <span class="keywordtype">void</span> Solve(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; TransA,
<a name="l00090"></a>00090                <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;double, VectFull, Allocator2&gt;</a>&amp; x);
<a name="l00091"></a>00091   };
<a name="l00092"></a>00092 
<a name="l00093"></a>00093 
<a name="l00095"></a>00095   <span class="keyword">template</span>&lt;&gt;
<a name="l00096"></a><a class="code" href="class_seldon_1_1_matrix_super_l_u_3_01complex_3_01double_01_4_01_4.php">00096</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix_super_l_u.php" title="empty matrix">MatrixSuperLU</a>&lt;complex&lt;double&gt; &gt;
<a name="l00097"></a>00097     : <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php" title="class interfacing SuperLU functions">MatrixSuperLU_Base</a>&lt;complex&lt;double&gt; &gt;
<a name="l00098"></a>00098   {
<a name="l00099"></a>00099   <span class="keyword">public</span>:
<a name="l00100"></a>00100     <a class="code" href="class_seldon_1_1_matrix_super_l_u.php" title="empty matrix">MatrixSuperLU</a>() : <a class="code" href="class_seldon_1_1_matrix_super_l_u___base.php" title="class interfacing SuperLU functions">MatrixSuperLU_Base&lt;complex&lt;double&gt;</a> &gt;() {}
<a name="l00101"></a>00101 
<a name="l00102"></a>00102     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00103"></a>00103     <span class="keywordtype">void</span> FactorizeMatrix(<a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;complex&lt;double&gt;, Prop,
<a name="l00104"></a>00104                          Storage, Allocator&gt; &amp; mat,
<a name="l00105"></a>00105                          <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>);
<a name="l00106"></a>00106 
<a name="l00107"></a>00107     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2&gt;
<a name="l00108"></a>00108     <span class="keywordtype">void</span> Solve(<a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;double&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator2&gt;&amp; x);
<a name="l00109"></a>00109 
<a name="l00110"></a>00110     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2&gt;
<a name="l00111"></a>00111     <span class="keywordtype">void</span> Solve(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; TransA,
<a name="l00112"></a>00112                <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;double&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator2&gt;&amp; x);
<a name="l00113"></a>00113 
<a name="l00114"></a>00114   };
<a name="l00115"></a>00115 }
<a name="l00116"></a>00116 
<a name="l00117"></a>00117 <span class="preprocessor">#define SELDON_FILE_SUPERLU_HXX</span>
<a name="l00118"></a>00118 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
