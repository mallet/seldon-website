<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="tabs2">
    <ul class="tablist">
      <li class="current"><a href="functions.php"><span>All</span></a></li>
      <li><a href="functions_func.php"><span>Functions</span></a></li>
      <li><a href="functions_vars.php"><span>Variables</span></a></li>
      <li><a href="functions_type.php"><span>Typedefs</span></a></li>
    </ul>
  </div>
  <div class="tabs3">
    <ul class="tablist">
      <li><a href="functions.php#index_a"><span>a</span></a></li>
      <li><a href="functions_0x63.php#index_c"><span>c</span></a></li>
      <li><a href="functions_0x64.php#index_d"><span>d</span></a></li>
      <li><a href="functions_0x65.php#index_e"><span>e</span></a></li>
      <li><a href="functions_0x66.php#index_f"><span>f</span></a></li>
      <li><a href="functions_0x67.php#index_g"><span>g</span></a></li>
      <li><a href="functions_0x68.php#index_h"><span>h</span></a></li>
      <li><a href="functions_0x69.php#index_i"><span>i</span></a></li>
      <li><a href="functions_0x6c.php#index_l"><span>l</span></a></li>
      <li class="current"><a href="functions_0x6d.php#index_m"><span>m</span></a></li>
      <li><a href="functions_0x6e.php#index_n"><span>n</span></a></li>
      <li><a href="functions_0x6f.php#index_o"><span>o</span></a></li>
      <li><a href="functions_0x70.php#index_p"><span>p</span></a></li>
      <li><a href="functions_0x72.php#index_r"><span>r</span></a></li>
      <li><a href="functions_0x73.php#index_s"><span>s</span></a></li>
      <li><a href="functions_0x74.php#index_t"><span>t</span></a></li>
      <li><a href="functions_0x75.php#index_u"><span>u</span></a></li>
      <li><a href="functions_0x76.php#index_v"><span>v</span></a></li>
      <li><a href="functions_0x77.php#index_w"><span>w</span></a></li>
      <li><a href="functions_0x78.php#index_x"><span>x</span></a></li>
      <li><a href="functions_0x7a.php#index_z"><span>z</span></a></li>
      <li><a href="functions_0x7e.php#index_~"><span>~</span></a></li>
    </ul>
  </div>
<div class="contents">
Here is a list of all documented class members with links to the class documentation for each member:

<h3><a class="anchor" id="index_m"></a>- m -</h3><ul>
<li>m_
: <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>mat_sym
: <a class="el" href="class_seldon_1_1_ilut_preconditioning.php#a450694545524ae93de1df9810b135b91">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a>
</li>
<li>mat_unsym
: <a class="el" href="class_seldon_1_1_ilut_preconditioning.php#a8e30b497ecb4698bb22d1c0c3ea19f7e">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a>
</li>
<li>Matrix()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_00_01_allocator_01_4.php#a525e0f659382eef4dd0e86e37790fa9f">Seldon::Matrix&lt; T, Prop, ColHerm, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php#a7affa2d13b83b7bac1c2bfae69b672cc">Seldon::Matrix&lt; T, Prop, ColMajor, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_packed_00_01_allocator_01_4.php#ac35b12d236d8b04b19f6710c05893610">Seldon::Matrix&lt; T, Prop, ColLoTriangPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a45d914bbc71d7f861330e804296906c2">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_packed_00_01_allocator_01_4.php#abeb10c437eb184d23f0e32210dc20498">Seldon::Matrix&lt; T, Prop, RowUpTriangPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a49019378c18567a9a3544f5801dd9b33">Seldon::Matrix&lt; T, Prop, ArrayColSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php#a20c877edd580054313c623edfdaf8837">Seldon::Matrix&lt; T, Prop, ColMajor, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_packed_00_01_allocator_01_4.php#a86c46a1585b523ba8e6d402defd4862e">Seldon::Matrix&lt; T, Prop, RowUpTriangPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a9bd699fbd7fe4a62e4216883a322b623">Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php#a607e3089e1ccddf239b481a8ddc556e9">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_complex_sparse_00_01_allocator_01_4.php#a3480b30876e7433f0ce962194bbf7a93">Seldon::Matrix&lt; T, Prop, ColComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_00_01_allocator_01_4.php#a5634cb9ee0148e17b4314b9db0a887ff">Seldon::Matrix&lt; T, Prop, RowHerm, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#ad6540bf8d5f0d58e12915162e1d6b852">Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php#ab80e64a037043f8ab6d77403ad62d9f3">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_complex_sparse_00_01_allocator_01_4.php#a8e6c48255742e981bfad3b98339ac1f5">Seldon::Matrix&lt; T, Prop, ColComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_00_01_allocator_01_4.php#a4666563d07bce1c82424eb8371145a60">Seldon::Matrix&lt; T, Prop, ColUpTriang, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_complex_sparse_00_01_allocator_01_4.php#ac7efc201b54c0a4eeea3753a1d9c5412">Seldon::Matrix&lt; T, Prop, RowComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#a2473d447fd3e4efac53e674b94841b98">Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_00_01_allocator_01_4.php#a3125ae5176db8aacf2481295eb1b9a26">Seldon::Matrix&lt; T, Prop, ColUpTriang, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_complex_sparse_00_01_allocator_01_4.php#a2c4255f9912668b1b93af5149898e974">Seldon::Matrix&lt; T, Prop, RowComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_00_01_allocator_01_4.php#a223ddc59cbc385192a72028c63f91ca5">Seldon::Matrix&lt; T, Prop, ColLoTriang, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sparse_00_01_allocator_01_4.php#a02ccefd04db099e355bf432daf2b8d0b">Seldon::Matrix&lt; T, Prop, ColSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_float_double_00_01_general_00_01_dense_sparse_collection_00_01_allocator_3_01double_01_4_01_4.php#a20314db5f8dd0fcf4dd175e4b1a4852f">Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_00_01_allocator_01_4.php#a9bbce915373f5fed84d468a463d18af8">Seldon::Matrix&lt; T, Prop, RowHerm, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#a4951a370ce9457e8625cc5257a349f96">Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_00_01_allocator_01_4.php#a41bffb94b8929a4598a8354f77412c68">Seldon::Matrix&lt; T, Prop, ColLoTriang, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sparse_00_01_allocator_01_4.php#a611908ecc9ddfce869d63f5cc528cbe5">Seldon::Matrix&lt; T, Prop, ColSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php#a5d54235702d1a8347394a5b63846568a">Seldon::Matrix&lt; T, Prop, RowUpTriang, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sparse_00_01_allocator_01_4.php#ac2887e1f84a81ce9755b4613d2ee3f6f">Seldon::Matrix&lt; T, Prop, RowSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php#abd70ad2d9d8d9da0d9e5210447aed290">Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php#a1899bdb8a6a9b11589b14150837c011a">Seldon::Matrix&lt; T, Prop, RowUpTriang, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sparse_00_01_allocator_01_4.php#ae49a78d5b561f1e2463c0ab95804c393">Seldon::Matrix&lt; T, Prop, RowSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_00_01_allocator_01_4.php#ac212f7adb2afae45023db30083b8a5c0">Seldon::Matrix&lt; T, Prop, RowLoTriang, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_complex_sparse_00_01_allocator_01_4.php#a9cde0f59f5bc4e5d5547d6bcc30afbcb">Seldon::Matrix&lt; T, Prop, ColSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php#a45762f0c3a34c78f8d6e99b74f269e1f">Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php#aa3eb2f4294b73154b78971e46213e527">Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_00_01_allocator_01_4.php#a5c08ccdc3e2bd785f531d4259e1900c1">Seldon::Matrix&lt; T, Prop, RowLoTriang, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_complex_sparse_00_01_allocator_01_4.php#a4ceea53fab2126b9642b4d03ab2137fd">Seldon::Matrix&lt; T, Prop, ColSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_collection_00_01_allocator_01_4.php#ac5858ad9d7f0e3a1a30dc73ca25be70a">Seldon::Matrix&lt; T, Prop, ColMajorCollection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php#aac12efd56ba2dcb96cbb2ed94cbf440c">Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_00_01_allocator_01_4.php#a25bc08edf0f71641b5f56d059ebed262">Seldon::Matrix&lt; T, Prop, RowSym, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_collection_00_01_allocator_01_4.php#a90cf5eb5fefd953854468f237bbaf1dd">Seldon::Matrix&lt; T, Prop, ColMajorCollection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php#a10616155b7471e78fdf59f4f2a760304">Seldon::Matrix&lt; T, Prop, RowSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_collection_00_01_allocator_01_4.php#a5a2c326ccb8beb999bd7dd53b6f1ae5b">Seldon::Matrix&lt; T, Prop, RowMajorCollection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_sparse_00_01_allocator_01_4.php#ab59d8490dc7405a4fc91b4e019bfa10f">Seldon::Matrix&lt; T, Prop, ColSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_float_double_00_01_general_00_01_dense_sparse_collection_00_01_allocator_3_01double_01_4_01_4.php#a80ce2837902243f345157a46095ac8ba">Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php#a11105f396c82d8936279b6ae815b5505">Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_00_01_allocator_01_4.php#a8ca2e66d22226f0892770f9f045133e5">Seldon::Matrix&lt; T, Prop, RowSym, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php#aca58932742ad99038531a482692d83f7">Seldon::Matrix&lt; T, Prop, RowSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_collection_00_01_allocator_01_4.php#aee28bdf11cf37fdf9b2f7ed2b9b4cc51">Seldon::Matrix&lt; T, Prop, RowMajorCollection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_sparse_00_01_allocator_01_4.php#a5c7e45ef8482a43d1a2c5468e8d863aa">Seldon::Matrix&lt; T, Prop, ColSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_packed_collection_00_01_allocator_01_4.php#a16f9fae88d8080b207a38199d90c76d6">Seldon::Matrix&lt; T, Prop, ColSymPackedCollection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php#a11e9550e00334880bb869d69aaf76546">Seldon::Matrix&lt; T, Prop, RowSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_packed_00_01_allocator_01_4.php#a751c1a6931312ab459584f2f73008a24">Seldon::Matrix&lt; T, Prop, ColSymPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_packed_collection_00_01_allocator_01_4.php#aafb8855d662e0a20220076522df67819">Seldon::Matrix&lt; T, Prop, ColSymPackedCollection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_collection_00_01_allocator_01_4.php#aafc716f71526b10a5221833661109bf8">Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_packed_00_01_allocator_01_4.php#ab2ca86f35ad38650084f05f6b15ca476">Seldon::Matrix&lt; T, Prop, RowHermPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_packed_00_01_allocator_01_4.php#a733a92c47fdce88950146a5e50a67553">Seldon::Matrix&lt; T, Prop, ColSymPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_collection_00_01_allocator_01_4.php#ab9e699fa7ccc95baf087b92bd4834465">Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_sub_storage_3_01_m_01_4_00_01_allocator_01_4.php#afe62d5f6bf8aca4d17fd0f44f9d8bd97">Seldon::Matrix&lt; T, Prop, SubStorage&lt; M &gt;, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_00_01_allocator_01_4.php#a77ff941e5bcf4841e7763800d327c470">Seldon::Matrix&lt; T, Prop, RowSymPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a39f7b5286ed8bbdbe816c7809e60872c">Seldon::Matrix&lt; T, Prop, ArrayColComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_00_01_allocator_01_4.php#a98135088d49660a050e9aeefc188d20f">Seldon::Matrix&lt; T, Prop, ColHerm, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_packed_00_01_allocator_01_4.php#aa6ded3d3e9e0f3d4542e59558dfe18d1">Seldon::Matrix&lt; T, Prop, RowHermPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_00_01_allocator_01_4.php#ac170fa7d62e15724a52e3caa238ee94d">Seldon::Matrix&lt; T, Prop, RowSymPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#ab98aecad772f00d59ea17248a04720d3">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_packed_00_01_allocator_01_4.php#aa374b37c0c5f9a8d9d2931c06f28fe9e">Seldon::Matrix&lt; T, Prop, ColUpTriangPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ac0a88fcc183d15c9d86cfdde270a1d30">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php#aa20ef1ed118ce455350561b4e7446bbb">Seldon::Matrix&lt; T, Prop, ColMajor, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_packed_00_01_allocator_01_4.php#a12353af9d23a32ee93faa58cc7a6ea9e">Seldon::Matrix&lt; T, Prop, ColUpTriangPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a5436a5d9f8bf73dbceb229a0649facfe">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_packed_00_01_allocator_01_4.php#a6dd1142ad7e321f76ccdc3fc1247159f">Seldon::Matrix&lt; T, Prop, ColLoTriangPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#a06100dad6cbda6411aeb3e87665175da">Seldon::Matrix&lt; T, Prop, ArrayColSparse, Allocator &gt;</a>
</li>
<li>matrix_
: <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a39e77d13e783db34a7ea38c9ba6fc812">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_collection.php#a66297d4139bb2b5b07979ef929358399">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>Matrix_ArrayComplexSparse()
: <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a410f415f4976918fc7678bc770611043">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>Matrix_ArraySparse()
: <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a6d460ce1bd8388adce95c8ad99b24f1d">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>Matrix_Base()
: <a class="el" href="class_seldon_1_1_matrix___base.php#a47988d7972247286332e853c13bbc00b">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>
</li>
<li>Matrix_ComplexSparse()
: <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a3df0b27785c83bf1b8809cdd263c48c5">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>Matrix_Hermitian()
: <a class="el" href="class_seldon_1_1_matrix___hermitian.php#a17c1f964ce877a9488b17b834f9e1216">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>Matrix_HermPacked()
: <a class="el" href="class_seldon_1_1_matrix___herm_packed.php#ad6584a1ca7199865d2a4d5dc6af246fe">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>Matrix_Pointers()
: <a class="el" href="class_seldon_1_1_matrix___pointers.php#a5f99e5a5a2f9a8f8e7873f829d82ed33">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>Matrix_Sparse()
: <a class="el" href="class_seldon_1_1_matrix___sparse.php#a2ff242fb46f84f041e4709a7eff70cd3">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>Matrix_SymComplexSparse()
: <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af4fd673569b7398d84f2d4f277de50a8">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>Matrix_Symmetric()
: <a class="el" href="class_seldon_1_1_matrix___symmetric.php#ab4c39b6e45f2f22dbf8dd50ad8509ad7">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>Matrix_SymPacked()
: <a class="el" href="class_seldon_1_1_matrix___sym_packed.php#ac06fa9e352440052fc5e4c8315688dee">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>Matrix_SymSparse()
: <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a626e591de87845d39c44796cd9b19bc9">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>Matrix_TriangPacked()
: <a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a785e3d146fce6e050f7fbba140a94406">Seldon::Matrix_TriangPacked&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>Matrix_Triangular()
: <a class="el" href="class_seldon_1_1_matrix___triangular.php#a3ecbb30f1d5aee888ef396bbfde1f61b">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>MatrixCollection()
: <a class="el" href="class_seldon_1_1_matrix_collection.php#aac3566347d1654f0fd5748568ba11d60">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>MatrixMumps()
: <a class="el" href="class_seldon_1_1_matrix_mumps.php#af1dbd7261f5a31bdc48ba7149b952b32">Seldon::MatrixMumps&lt; T &gt;</a>
</li>
<li>MatrixPastix()
: <a class="el" href="class_seldon_1_1_matrix_pastix.php#a862e60553d3532ab99a3645f219d782d">Seldon::MatrixPastix&lt; T &gt;</a>
</li>
<li>MatrixSuperLU_Base()
: <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a526c7f0e29c86b81fc4705b437e167f9">Seldon::MatrixSuperLU_Base&lt; T &gt;</a>
</li>
<li>MatrixUmfPack()
: <a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#a70dd6831eed0355caedc69e8d3c99920">Seldon::MatrixUmfPack&lt; double &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a83049b296e185003b45f3d02d664adb2">Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;</a>
</li>
<li>MatrixUmfPack_Base()
: <a class="el" href="class_seldon_1_1_matrix_umf_pack___base.php#a50e9165bfdf52428e6f405d959f9f863">Seldon::MatrixUmfPack_Base&lt; T &gt;</a>
</li>
<li>max_iter
: <a class="el" href="class_seldon_1_1_iteration.php#a75359d82e80a14fa10907b203fc5a2ff">Seldon::Iteration&lt; Titer &gt;</a>
</li>
<li>mbloc
: <a class="el" href="class_seldon_1_1_ilut_preconditioning.php#accd555595ce35fcf2e2a2288c4585494">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a>
</li>
<li>Mlocal_
: <a class="el" href="class_seldon_1_1_matrix_collection.php#a234a716260de98d93fc40fc2a3fd44fe">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a48d1d3d9da13f7923a6356e49815fe66">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
</li>
<li>Mlocal_sum_
: <a class="el" href="class_seldon_1_1_matrix_collection.php#aa16e80513c77e58a2c1b3974e360f220">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#af934beebf115742be0a29cb452017a7b">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
</li>
<li>Mmatrix_
: <a class="el" href="class_seldon_1_1_matrix_collection.php#a094131b6edf39309cf51ec6aea9c271b">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a67cb0eb87ea633caabecdc58714723ec">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
</li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
