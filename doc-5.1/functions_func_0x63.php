<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="tabs2">
    <ul class="tablist">
      <li><a href="functions.php"><span>All</span></a></li>
      <li class="current"><a href="functions_func.php"><span>Functions</span></a></li>
      <li><a href="functions_vars.php"><span>Variables</span></a></li>
      <li><a href="functions_type.php"><span>Typedefs</span></a></li>
    </ul>
  </div>
  <div class="tabs3">
    <ul class="tablist">
      <li><a href="functions_func.php#index_a"><span>a</span></a></li>
      <li class="current"><a href="functions_func_0x63.php#index_c"><span>c</span></a></li>
      <li><a href="functions_func_0x64.php#index_d"><span>d</span></a></li>
      <li><a href="functions_func_0x65.php#index_e"><span>e</span></a></li>
      <li><a href="functions_func_0x66.php#index_f"><span>f</span></a></li>
      <li><a href="functions_func_0x67.php#index_g"><span>g</span></a></li>
      <li><a href="functions_func_0x68.php#index_h"><span>h</span></a></li>
      <li><a href="functions_func_0x69.php#index_i"><span>i</span></a></li>
      <li><a href="functions_func_0x6c.php#index_l"><span>l</span></a></li>
      <li><a href="functions_func_0x6d.php#index_m"><span>m</span></a></li>
      <li><a href="functions_func_0x6e.php#index_n"><span>n</span></a></li>
      <li><a href="functions_func_0x6f.php#index_o"><span>o</span></a></li>
      <li><a href="functions_func_0x70.php#index_p"><span>p</span></a></li>
      <li><a href="functions_func_0x72.php#index_r"><span>r</span></a></li>
      <li><a href="functions_func_0x73.php#index_s"><span>s</span></a></li>
      <li><a href="functions_func_0x74.php#index_t"><span>t</span></a></li>
      <li><a href="functions_func_0x75.php#index_u"><span>u</span></a></li>
      <li><a href="functions_func_0x76.php#index_v"><span>v</span></a></li>
      <li><a href="functions_func_0x77.php#index_w"><span>w</span></a></li>
      <li><a href="functions_func_0x7a.php#index_z"><span>z</span></a></li>
      <li><a href="functions_func_0x7e.php#index_~"><span>~</span></a></li>
    </ul>
  </div>
<div class="contents">
&nbsp;

<h3><a class="anchor" id="index_c"></a>- c -</h3><ul>
<li>Clear()
: <a class="el" href="class_seldon_1_1_array3_d.php#aa9bf1047ace17bd5c2bf2d4863d46838">Seldon::Array3D&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_mumps.php#a8d3c03fd1f5e00dbbbb39e8f9e1cc4ee">Seldon::MatrixMumps&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#acac4d12b1d9e464f630b7d56b2c61a23">Seldon::MatrixSuperLU_Base&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___hermitian.php#a977f6c67028b64777aef5d414e115ee2">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0ee918c8c04a1a9434e157c15fdeb596">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a61227beea7612dd22bf1f46546bb5cde">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a98226901bde3cf19ae74571c38280792">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a8940cdf1faedc44051919b9774132fa9">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sparse.php#ab63b8d9aee1a38054cd525d7fa708f1a">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#a24db3569fa50301554e9ca73ebd167e3">Seldon::MatrixUmfPack&lt; double &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___pointers.php#a361729faf34aaedf14ad0b3f044e093c">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a77439fdc40537903681e7cd912c5f732">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a71256dab229c463457ff8848aa6d1fa7">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___symmetric.php#a0ab7b853cb5e827503be5e65f2b39150">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ab5cd5f6b57ca76ac3659b02fe928d23d">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a548fa99edadcd9b418912d607b7aa19b">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_pastix.php#a0840578aaa2639d33d5c47adea893cf8">Seldon::MatrixPastix&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a490fccbcfd57c7e3ad07c8ea3739afab">Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a230f90f867e62f789bdfb7ea2310dc40">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a5120b4ecff05d8cf54bbd57fb280fbe1">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector2.php#a8c34a780a5b3f8b83bf36a14a4e7f21a">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a6f0db73087115429868eaf0a35cf213b">Seldon::Matrix_TriangPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector2.php#ad95c6d7a9a6dd52aae5aef1ffc608b71">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#a0e178a2f8fad92f0929f4ea1b1dd1604">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a0324574cd55a0799aa9746b36b9b20a1">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triangular.php#a01cf3952c69288e68376db61cf322c77">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#a173d2e7734fb3098785523c63488a577">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_collection.php#a508ae5c18005383193f2a289bf63d608">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a44d2428beb7f6d5a26921ba0158ca415">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
</li>
<li>ClearColumn()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#abee36512d3f1fe6dd19693e9585395c4">Seldon::Matrix&lt; T, Prop, ArrayColSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#a00b55880d103fffa13cd1eaa1e49dac9">Seldon::Matrix&lt; T, Prop, ArrayColSparse, Allocator &gt;</a>
</li>
<li>ClearImagColumn()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a5e616d97f5b568c8adb47ccc15540300">Seldon::Matrix&lt; T, Prop, ArrayColComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#addf758fad26c1595b8398590e138fa2f">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>
</li>
<li>ClearImagRow()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a16289d6a8f38a4303f75113667f8af74">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a47212adda8f7586925272cf2e4c620bb">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>
</li>
<li>ClearRealColumn()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a4962f5db5a03299da3789ff2c7ab653b">Seldon::Matrix&lt; T, Prop, ArrayColComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ab22d91b2a8aa62a3d5d0ea295749b651">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>
</li>
<li>ClearRealRow()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#aff6987ec6d06c3a4c14ebc06b14ae355">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a480f759c9bdd3e69206f18d81ed44f5a">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>
</li>
<li>ClearRow()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#ad7e78da914d3e0d3818b8c21ec8e7bbf">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a1221b2abf3eb604ab208e9f839ccfa5c">Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</a>
</li>
<li>Copy()
: <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a91100b6eb7a2cea5e86d191ee58749bf">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_packed.php#afcaec454efa976f5e61e9c6188df5ae7">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#aefe82d8a7c33e932c20c9fd9e6f499f5">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_collection.php#a0654c385392b3f0634a8721a797e249a">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector2.php#a7074df5f337f30702d6b6d59f4855a30">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
, <a class="el" href="class_seldon_1_1_array3_d.php#a2f8287363e777b0072017405e182ccc0">Seldon::Array3D&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___symmetric.php#a94f5e47677cc13311ab33879d2b05774">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a5e1062c93e237401d6bf6b1797861c62">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sparse.php#a54604e7e696124d278a4aa198edcab77">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___hermitian.php#a9a424460d2d3c28f0ba2c208efc6420f">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triang_packed.php#ac3fcc459520d0ec52a599a8def09c4fc">Seldon::Matrix_TriangPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a43f929eaa7b183e23c49d9ad31e5699e">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#acec939c56c4c6b8f3db9cb2d47111644">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a790a38cb578627bf17291088f2cd2388">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#afc9462324875b41fae1a8ff1645d831d">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ae583202802b373763ce3780fe2382f4f">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triangular.php#a22a6a81034ad25df2123e33d1fb9052d">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a13715cb4e64fbe58e24fa86897131e25">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___pointers.php#a043929510eae4908750348b4862e190c">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>CoutWhat()
: <a class="el" href="class_seldon_1_1_error.php#a19ea259b22ab7970a19beae646267ba3">Seldon::Error</a>
</li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
