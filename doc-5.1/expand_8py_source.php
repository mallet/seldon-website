<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>share/expand.py</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment"># Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">#</span>
<a name="l00003"></a>00003 <span class="comment"># This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment"># http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">#</span>
<a name="l00006"></a>00006 <span class="comment"># Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment"># terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment"># Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment"># any later version.</span>
<a name="l00010"></a>00010 <span class="comment">#</span>
<a name="l00011"></a>00011 <span class="comment"># Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment"># WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment"># FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more</span>
<a name="l00014"></a>00014 <span class="comment"># details.</span>
<a name="l00015"></a>00015 <span class="comment">#</span>
<a name="l00016"></a>00016 <span class="comment"># You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment"># along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="keyword">import</span> sys, os, re
<a name="l00021"></a>00021 
<a name="l00022"></a>00022 
<a name="l00023"></a>00023 <span class="comment">#######################</span>
<a name="l00024"></a>00024 <span class="comment"># PARSES COMMAND LINE #</span>
<a name="l00025"></a>00025 <span class="comment">#######################</span>
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 <span class="keywordflow">if</span> len(sys.argv) != 3:
<a name="l00029"></a>00029     <span class="keywordflow">print</span> <span class="stringliteral">&quot;Please provide exactly two input files.&quot;</span>
<a name="l00030"></a>00030     sys.exit(1)
<a name="l00031"></a>00031 
<a name="l00032"></a>00032 template_definition = sys.argv[1]
<a name="l00033"></a>00033 input_file = sys.argv[2]
<a name="l00034"></a>00034 <span class="keywordflow">if</span> len(input_file) &lt; 9 <span class="keywordflow">or</span> input_file[:9] != <span class="stringliteral">&quot;template-&quot;</span>:
<a name="l00035"></a>00035     <span class="keywordflow">print</span> <span class="stringliteral">&quot;Input filename should start with \&quot;template-\&quot;.&quot;</span>
<a name="l00036"></a>00036     sys.exit(1)
<a name="l00037"></a>00037 
<a name="l00038"></a>00038 <span class="keywordflow">if</span> <span class="keywordflow">not</span> os.path.isfile(input_file):
<a name="l00039"></a>00039     <span class="keywordflow">print</span> <span class="stringliteral">&quot;\&quot;&quot;</span> + input_file + <span class="stringliteral">&quot;\&quot; is not a file!&quot;</span>
<a name="l00040"></a>00040     sys.exit(1)
<a name="l00041"></a>00041 
<a name="l00042"></a>00042 output_file = input_file[9:]
<a name="l00043"></a>00043 <span class="keywordflow">if</span> output_file == <span class="stringliteral">&quot;&quot;</span>:
<a name="l00044"></a>00044     <span class="keywordflow">print</span> <span class="stringliteral">&quot;Error: output filename is empty.&quot;</span>
<a name="l00045"></a>00045     sys.exit(1)
<a name="l00046"></a>00046 
<a name="l00047"></a>00047 
<a name="l00048"></a>00048 <span class="comment">####################</span>
<a name="l00049"></a>00049 <span class="comment"># USEFUL FUNCTIONS #</span>
<a name="l00050"></a>00050 <span class="comment">####################</span>
<a name="l00051"></a>00051 
<a name="l00052"></a>00052 
<a name="l00053"></a>00053 <span class="keyword">def </span>combine(list_of_list):
<a name="l00054"></a>00054     <span class="stringliteral">&quot;&quot;&quot;</span>
<a name="l00055"></a>00055 <span class="stringliteral">    Generates all combinations (x, y, ...) where x is an element of</span>
<a name="l00056"></a>00056 <span class="stringliteral">    list_of_list[0], y an element of list_of_list[1], ...</span>
<a name="l00057"></a>00057 <span class="stringliteral"></span>
<a name="l00058"></a>00058 <span class="stringliteral">    \param list_of_list the list of lists of possible values.</span>
<a name="l00059"></a>00059 <span class="stringliteral">    \return The list of all possible combinations (therefore, a list of</span>
<a name="l00060"></a>00060 <span class="stringliteral">    lists).</span>
<a name="l00061"></a>00061 <span class="stringliteral">    &quot;&quot;&quot;</span>
<a name="l00062"></a>00062     <span class="keywordflow">if</span> len(list_of_list) == 0:
<a name="l00063"></a>00063         <span class="keywordflow">return</span> []
<a name="l00064"></a>00064     <span class="keywordflow">elif</span> len(list_of_list) == 1:
<a name="l00065"></a>00065         <span class="keywordflow">return</span> [[x] <span class="keywordflow">for</span> x <span class="keywordflow">in</span> list_of_list[0]]
<a name="l00066"></a>00066     <span class="keywordflow">elif</span> len(list_of_list) == 2:
<a name="l00067"></a>00067         <span class="keywordflow">return</span> [[x, y] <span class="keywordflow">for</span> x <span class="keywordflow">in</span> list_of_list[0] <span class="keywordflow">for</span> y <span class="keywordflow">in</span> list_of_list[1]]
<a name="l00068"></a>00068     <span class="keywordflow">else</span>:
<a name="l00069"></a>00069         middle = len(list_of_list) / 2
<a name="l00070"></a>00070         <span class="keywordflow">return</span> [x + y <span class="keywordflow">for</span> x <span class="keywordflow">in</span> combine(list_of_list[:middle])
<a name="l00071"></a>00071                 <span class="keywordflow">for</span> y <span class="keywordflow">in</span> combine(list_of_list[middle:])]
<a name="l00072"></a>00072 
<a name="l00073"></a>00073 
<a name="l00074"></a>00074 <span class="keyword">def </span>expand_string(input_string, replacement_list, recursive = True):
<a name="l00075"></a>00075     <span class="stringliteral">&quot;&quot;&quot;</span>
<a name="l00076"></a>00076 <span class="stringliteral">    Expands markups in a string, based on all possible values that may take</span>
<a name="l00077"></a>00077 <span class="stringliteral">    these markups.</span>
<a name="l00078"></a>00078 <span class="stringliteral">    </span>
<a name="l00079"></a>00079 <span class="stringliteral">    \param input_string the input string in which the markups are to be</span>
<a name="l00080"></a>00080 <span class="stringliteral">    replaced.</span>
<a name="l00081"></a>00081 <span class="stringliteral">    \param replacement_list a dictionary which markups in keys and a list of</span>
<a name="l00082"></a>00082 <span class="stringliteral">    possible values for each key.</span>
<a name="l00083"></a>00083 <span class="stringliteral">    \param recursive are recursive expansions enabled?</span>
<a name="l00084"></a>00084 <span class="stringliteral">    \return The list of all possible strings.</span>
<a name="l00085"></a>00085 <span class="stringliteral">    &quot;&quot;&quot;</span>
<a name="l00086"></a>00086     key_list = replacement_list.keys()
<a name="l00087"></a>00087     <span class="comment"># key_list should be sorted from the longest string to the smallest</span>
<a name="l00088"></a>00088     <span class="comment"># string, so that the replacements do not interfere with each other.</span>
<a name="l00089"></a>00089     key_list.sort(key = len)
<a name="l00090"></a>00090     key_list.reverse()
<a name="l00091"></a>00091     <span class="comment"># Active markups.</span>
<a name="l00092"></a>00092     active = []
<a name="l00093"></a>00093     tmp_string = input_string
<a name="l00094"></a>00094     <span class="keywordflow">for</span> markup <span class="keywordflow">in</span> key_list:
<a name="l00095"></a>00095         active.append(markup <span class="keywordflow">in</span> tmp_string)
<a name="l00096"></a>00096         <span class="comment"># If any active markup is not to be expanded, an empty list should be</span>
<a name="l00097"></a>00097         <span class="comment"># returned.</span>
<a name="l00098"></a>00098         <span class="keywordflow">if</span> markup <span class="keywordflow">in</span> tmp_string <span class="keywordflow">and</span> replacement_list[markup] == []:
<a name="l00099"></a>00099             <span class="keywordflow">return</span> []
<a name="l00100"></a>00100         tmp_string = tmp_string.replace(markup, <span class="stringliteral">&quot;&quot;</span>)
<a name="l00101"></a>00101     <span class="comment"># Is any replacement needed?</span>
<a name="l00102"></a>00102     <span class="keywordflow">if</span> any(active):
<a name="l00103"></a>00103         output_list = []
<a name="l00104"></a>00104         <span class="comment"># Selects the replacements actually needed.</span>
<a name="l00105"></a>00105         active_key = [key <span class="keywordflow">for</span> key, act <span class="keywordflow">in</span> zip(key_list, active) <span class="keywordflow">if</span> act]
<a name="l00106"></a>00106         active_replacement = [replacement_list[key] <span class="keywordflow">for</span> key <span class="keywordflow">in</span> active_key]
<a name="l00107"></a>00107         <span class="comment"># Generates all possible combinations.</span>
<a name="l00108"></a>00108         active_replacement = combine(active_replacement)
<a name="l00109"></a>00109         <span class="keywordflow">for</span> replacement <span class="keywordflow">in</span> active_replacement:
<a name="l00110"></a>00110             output_string = input_string
<a name="l00111"></a>00111             <span class="comment"># Expands the markups one by one.</span>
<a name="l00112"></a>00112             <span class="keywordflow">for</span> key, value <span class="keywordflow">in</span> zip(active_key, replacement):
<a name="l00113"></a>00113                 output_string = output_string.replace(key, value)
<a name="l00114"></a>00114             output_list.append(output_string)
<a name="l00115"></a>00115         <span class="comment"># Recursion may be needed to complete the expansion.</span>
<a name="l00116"></a>00116         <span class="keywordflow">if</span> recursive:
<a name="l00117"></a>00117             raw_list = output_list
<a name="l00118"></a>00118             output_list = []
<a name="l00119"></a>00119             <span class="keywordflow">for</span> item <span class="keywordflow">in</span> raw_list:
<a name="l00120"></a>00120                 output_list += expand_string(item, replacement_list, <span class="keyword">True</span>)
<a name="l00121"></a>00121         <span class="keywordflow">return</span> output_list
<a name="l00122"></a>00122     <span class="keywordflow">else</span>:
<a name="l00123"></a>00123         <span class="keywordflow">return</span> [input_string]
<a name="l00124"></a>00124 
<a name="l00125"></a>00125 
<a name="l00126"></a>00126 <span class="keyword">def </span>read_markup_list(filename):
<a name="l00127"></a>00127     <span class="stringliteral">&quot;&quot;&quot;</span>
<a name="l00128"></a>00128 <span class="stringliteral">    Extracts a replacement list from a file.</span>
<a name="l00129"></a>00129 <span class="stringliteral"></span>
<a name="l00130"></a>00130 <span class="stringliteral">    \param filename name of the file that defines the markups.</span>
<a name="l00131"></a>00131 <span class="stringliteral">    \return A dictionary with the markups and their possible values.</span>
<a name="l00132"></a>00132 <span class="stringliteral">    &quot;&quot;&quot;</span>
<a name="l00133"></a>00133     <span class="comment"># File contents.</span>
<a name="l00134"></a>00134     file_stream = open(filename)
<a name="l00135"></a>00135     file_line = file_stream.readlines()
<a name="l00136"></a>00136     file_stream.close()
<a name="l00137"></a>00137     Nline = len(file_line)
<a name="l00138"></a>00138 
<a name="l00139"></a>00139     <span class="comment"># Searches for list definition: (define ...).</span>
<a name="l00140"></a>00140     define_list = []
<a name="l00141"></a>00141     i = 0
<a name="l00142"></a>00142     <span class="keywordflow">while</span> i != Nline:
<a name="l00143"></a>00143         <span class="keywordflow">if</span> <span class="stringliteral">&quot;(define&quot;</span> <span class="keywordflow">in</span> file_line[i].strip():
<a name="l00144"></a>00144             line = <span class="stringliteral">&quot;&quot;</span>
<a name="l00145"></a>00145             <span class="keywordflow">while</span> <span class="keyword">True</span>:
<a name="l00146"></a>00146                 line += file_line[i].strip() + <span class="stringliteral">&quot; &quot;</span>
<a name="l00147"></a>00147                 <span class="keywordflow">if</span> <span class="stringliteral">&quot;)&quot;</span> <span class="keywordflow">in</span> line:
<a name="l00148"></a>00148                     define_list.append(line.strip())
<a name="l00149"></a>00149                     i += 1
<a name="l00150"></a>00150                     <span class="keywordflow">break</span>
<a name="l00151"></a>00151                 <span class="keywordflow">elif</span> i == Nline:
<a name="l00152"></a>00152                     <span class="keywordflow">raise</span> Exception, <span class="stringliteral">&quot;Syntax error: \&quot;define\&quot; not closed.&quot;</span>
<a name="l00153"></a>00153                 i += 1
<a name="l00154"></a>00154                 <span class="keywordflow">if</span> i == Nline:
<a name="l00155"></a>00155                     <span class="keywordflow">break</span>
<a name="l00156"></a>00156         <span class="keywordflow">else</span>:
<a name="l00157"></a>00157             i += 1
<a name="l00158"></a>00158 
<a name="l00159"></a>00159     <span class="comment"># Now parses the definitions.</span>
<a name="l00160"></a>00160     dictionary = {}
<a name="l00161"></a>00161     <span class="keywordflow">for</span> line <span class="keywordflow">in</span> define_list:
<a name="l00162"></a>00162         <span class="keywordflow">if</span> len(line) &lt; 13 <span class="keywordflow">or</span> line[:8] != <span class="stringliteral">&quot;(define &quot;</span> <span class="keywordflow">or</span> line[-2:] != <span class="stringliteral">&quot;);&quot;</span>:
<a name="l00163"></a>00163             <span class="keywordflow">raise</span> Exception, <span class="stringliteral">&quot;Syntax error in \&quot;define\&quot;:\n&quot;</span> + line
<a name="l00164"></a>00164         <span class="keywordflow">if</span> <span class="stringliteral">&quot;(&quot;</span> <span class="keywordflow">in</span> line[8:-2]:
<a name="l00165"></a>00165             <span class="keywordflow">raise</span> Exception, <span class="stringliteral">&quot;Syntax error in \&quot;define\&quot;:\n&quot;</span> + line
<a name="l00166"></a>00166         line_list = re.split(<span class="stringliteral">&quot;[ \n\t,:=]+&quot;</span>, line[8:-2].strip())
<a name="l00167"></a>00167         <span class="keywordflow">if</span> line_list != [] <span class="keywordflow">and</span> line_list[-1] == <span class="stringliteral">&quot;&quot;</span>:
<a name="l00168"></a>00168             <span class="comment"># Removes the empty string.</span>
<a name="l00169"></a>00169             line_list = line_list[:-1]
<a name="l00170"></a>00170         <span class="keywordflow">if</span> line_list == []:
<a name="l00171"></a>00171             <span class="keywordflow">raise</span> Exception, <span class="stringliteral">&quot;Syntax error in \&quot;define\&quot;:\n&quot;</span> + line
<a name="l00172"></a>00172         <span class="keywordflow">if</span> len(line_list) == 1:
<a name="l00173"></a>00173             <span class="comment"># Empty: no replacement.</span>
<a name="l00174"></a>00174             dictionary[line_list[0]] = []
<a name="l00175"></a>00175         <span class="keywordflow">else</span>:
<a name="l00176"></a>00176             dictionary[line_list[0]] = line_list[1:]
<a name="l00177"></a>00177 
<a name="l00178"></a>00178     <span class="keywordflow">return</span> dictionary
<a name="l00179"></a>00179         
<a name="l00180"></a>00180 
<a name="l00181"></a>00181 <span class="keyword">def </span>read_expand(filename, replacement_list):
<a name="l00182"></a>00182     <span class="stringliteral">&quot;&quot;&quot;</span>
<a name="l00183"></a>00183 <span class="stringliteral">    Applies a replacement list to a file. Instructions &quot;(define ...)&quot; are</span>
<a name="l00184"></a>00184 <span class="stringliteral">    filtered out.</span>
<a name="l00185"></a>00185 <span class="stringliteral"></span>
<a name="l00186"></a>00186 <span class="stringliteral">    \param filename name of the file to proceed with.</span>
<a name="l00187"></a>00187 <span class="stringliteral">    \return A list of the line with the markups expanded.</span>
<a name="l00188"></a>00188 <span class="stringliteral">    &quot;&quot;&quot;</span>
<a name="l00189"></a>00189     <span class="comment"># File contents.</span>
<a name="l00190"></a>00190     file_stream = open(filename)
<a name="l00191"></a>00191     file_line = file_stream.readlines()
<a name="l00192"></a>00192     file_stream.close()
<a name="l00193"></a>00193     Nline = len(file_line)
<a name="l00194"></a>00194 
<a name="l00195"></a>00195     <span class="comment"># Searches for code lines, that is, all lines but those with a definition:</span>
<a name="l00196"></a>00196     <span class="comment"># (define ...).</span>
<a name="l00197"></a>00197     code_line = []
<a name="l00198"></a>00198     i = 0
<a name="l00199"></a>00199     <span class="keywordflow">while</span> i != Nline:
<a name="l00200"></a>00200         <span class="keywordflow">if</span> <span class="stringliteral">&quot;(define&quot;</span> <span class="keywordflow">in</span> file_line[i].strip():
<a name="l00201"></a>00201             <span class="keywordflow">while</span> <span class="keyword">True</span>:
<a name="l00202"></a>00202                 <span class="keywordflow">if</span> <span class="stringliteral">&quot;)&quot;</span> <span class="keywordflow">in</span> file_line[i]:
<a name="l00203"></a>00203                     i += 1
<a name="l00204"></a>00204                     <span class="keywordflow">break</span>
<a name="l00205"></a>00205                 <span class="keywordflow">elif</span> i == Nline:
<a name="l00206"></a>00206                     <span class="keywordflow">raise</span> Exception, <span class="stringliteral">&quot;Syntax error: \&quot;define\&quot; not closed.&quot;</span>
<a name="l00207"></a>00207                 i += 1
<a name="l00208"></a>00208                 <span class="keywordflow">if</span> i == Nline:
<a name="l00209"></a>00209                     <span class="keywordflow">break</span>
<a name="l00210"></a>00210         <span class="keywordflow">else</span>:
<a name="l00211"></a>00211             code_line.append(file_line[i])
<a name="l00212"></a>00212             i += 1
<a name="l00213"></a>00213 
<a name="l00214"></a>00214     <span class="comment"># Now proceeds with the replacements.</span>
<a name="l00215"></a>00215     output_line = []
<a name="l00216"></a>00216     <span class="keywordflow">for</span> line <span class="keywordflow">in</span> code_line:
<a name="l00217"></a>00217         output_line += expand_string(line, replacement_list)
<a name="l00218"></a>00218 
<a name="l00219"></a>00219     <span class="keywordflow">return</span> output_line
<a name="l00220"></a>00220         
<a name="l00221"></a>00221 
<a name="l00222"></a>00222 <span class="comment">###################</span>
<a name="l00223"></a>00223 <span class="comment"># FILE GENERATION #</span>
<a name="l00224"></a>00224 <span class="comment">###################</span>
<a name="l00225"></a>00225 
<a name="l00226"></a>00226 
<a name="l00227"></a>00227 replacement_list = read_markup_list(template_definition)
<a name="l00228"></a>00228 
<a name="l00229"></a>00229 output_stream = open(output_file, <span class="stringliteral">&quot;w&quot;</span>)
<a name="l00230"></a>00230 output_stream.write(<span class="stringliteral">&quot;&quot;</span>.join(read_expand(input_file, replacement_list)))
<a name="l00231"></a>00231 output_stream.close()
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
