<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix/SubMatrix_Base.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_SUBMATRIX_BASE_CXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;SubMatrix_Base.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 
<a name="l00026"></a>00026 <span class="keyword">namespace </span>Seldon
<a name="l00027"></a>00027 {
<a name="l00028"></a>00028 
<a name="l00029"></a>00029 
<a name="l00031"></a>00031   <span class="comment">// SUBMATRIX_BASE //</span>
<a name="l00033"></a>00033 <span class="comment"></span>
<a name="l00034"></a>00034 
<a name="l00035"></a>00035   <span class="comment">/***************</span>
<a name="l00036"></a>00036 <span class="comment">   * CONSTRUCTOR *</span>
<a name="l00037"></a>00037 <span class="comment">   ***************/</span>
<a name="l00038"></a>00038 
<a name="l00039"></a>00039 
<a name="l00041"></a>00041   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> M, <span class="keyword">class</span> Allocator&gt;
<a name="l00042"></a>00042   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_sub_matrix___base.php#a495411ec39b561f75a7fecbf589e4f2c" title="Main constructor.">SubMatrix_Base&lt;T, Prop, M, Allocator&gt;</a>
<a name="l00043"></a>00043 <a class="code" href="class_seldon_1_1_sub_matrix___base.php#a495411ec39b561f75a7fecbf589e4f2c" title="Main constructor.">  ::SubMatrix_Base</a>(M&amp; A, Vector&lt;int&gt;&amp; row_list, Vector&lt;int&gt;&amp; column_list):
<a name="l00044"></a>00044     Matrix_Base&lt;T, Allocator&gt;(row_list.GetLength(), column_list.GetLength()),
<a name="l00045"></a>00045     matrix_(&amp;A), row_list_(row_list), column_list_(column_list)
<a name="l00046"></a>00046   {
<a name="l00047"></a>00047   }
<a name="l00048"></a>00048 
<a name="l00049"></a>00049 
<a name="l00050"></a>00050   <span class="comment">/**************</span>
<a name="l00051"></a>00051 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00052"></a>00052 <span class="comment">   **************/</span>
<a name="l00053"></a>00053 
<a name="l00054"></a><a class="code" href="class_seldon_1_1_sub_matrix___base.php#a2142c411e3e6b939ef7bc99ed6b4a7f9">00054</a> 
<a name="l00056"></a>00056   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> M, <span class="keyword">class</span> Allocator&gt;
<a name="l00057"></a>00057   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_sub_matrix___base.php#a2142c411e3e6b939ef7bc99ed6b4a7f9" title="Destructor.">SubMatrix_Base&lt;T, Prop, M, Allocator&gt;::~SubMatrix_Base</a>()
<a name="l00058"></a>00058   {
<a name="l00059"></a>00059   }
<a name="l00060"></a>00060 
<a name="l00061"></a>00061 
<a name="l00062"></a>00062   <span class="comment">/************************</span>
<a name="l00063"></a>00063 <span class="comment">   * ACCESS AND AFFECTION *</span>
<a name="l00064"></a>00064 <span class="comment">   ************************/</span>
<a name="l00065"></a>00065 
<a name="l00066"></a>00066 
<a name="l00068"></a>00068 
<a name="l00074"></a>00074   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> M, <span class="keyword">class</span> Allocator&gt;
<a name="l00075"></a>00075   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_sub_matrix___base.php" title="Sub-matrix base class.">SubMatrix_Base&lt;T, Prop, M, Allocator&gt;::access_type</a>
<a name="l00076"></a>00076   <a class="code" href="class_seldon_1_1_sub_matrix___base.php#a5784a509a63b4a0e4f0758e342218e2a" title="Access operator.">SubMatrix_Base&lt;T, Prop, M, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00077"></a>00077   {
<a name="l00078"></a>00078     <span class="keywordflow">return</span> (*this-&gt;<a class="code" href="class_seldon_1_1_sub_matrix___base.php#a39e77d13e783db34a7ea38c9ba6fc812" title="Pointer to the base matrix.">matrix_</a>)(this-&gt;<a class="code" href="class_seldon_1_1_sub_matrix___base.php#a7af6e9c2c869742e856f10bc83f43ece" title="List of rows.">row_list_</a>(i), this-&gt;<a class="code" href="class_seldon_1_1_sub_matrix___base.php#aa0699422389286f5dab0b65a83ca0c2c" title="List of columns.">column_list_</a>(j));
<a name="l00079"></a>00079   }
<a name="l00080"></a>00080 
<a name="l00081"></a>00081 
<a name="l00083"></a>00083 
<a name="l00089"></a>00089   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> M, <span class="keyword">class</span> Allocator&gt;
<a name="l00090"></a>00090   <span class="keyword">inline</span> <span class="keyword">typename</span> SubMatrix_Base&lt;T, Prop, M, Allocator&gt;::const_access_type
<a name="l00091"></a>00091   <a class="code" href="class_seldon_1_1_sub_matrix___base.php#a5784a509a63b4a0e4f0758e342218e2a" title="Access operator.">SubMatrix_Base&lt;T, Prop, M, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00092"></a>00092 <span class="keyword">  </span>{
<a name="l00093"></a>00093     <span class="keywordflow">return</span> (*this-&gt;<a class="code" href="class_seldon_1_1_sub_matrix___base.php#a39e77d13e783db34a7ea38c9ba6fc812" title="Pointer to the base matrix.">matrix_</a>)(this-&gt;<a class="code" href="class_seldon_1_1_sub_matrix___base.php#a7af6e9c2c869742e856f10bc83f43ece" title="List of rows.">row_list_</a>(i), this-&gt;<a class="code" href="class_seldon_1_1_sub_matrix___base.php#aa0699422389286f5dab0b65a83ca0c2c" title="List of columns.">column_list_</a>(j));
<a name="l00094"></a>00094   }
<a name="l00095"></a>00095 
<a name="l00096"></a>00096 
<a name="l00098"></a>00098 
<a name="l00104"></a>00104   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> M, <span class="keyword">class</span> Allocator&gt;
<a name="l00105"></a>00105   <span class="keyword">inline</span> <span class="keyword">typename</span> SubMatrix_Base&lt;T, Prop, M, Allocator&gt;::entry_type&amp;
<a name="l00106"></a>00106   <a class="code" href="class_seldon_1_1_sub_matrix___base.php#a58c123d74c337c990ffeb7f18e911a5a" title="Access operator.">SubMatrix_Base&lt;T, Prop, M, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00107"></a>00107   {
<a name="l00108"></a>00108     <span class="keywordflow">return</span> this-&gt;<a class="code" href="class_seldon_1_1_sub_matrix___base.php#a39e77d13e783db34a7ea38c9ba6fc812" title="Pointer to the base matrix.">matrix_</a>-&gt;Val(this-&gt;<a class="code" href="class_seldon_1_1_sub_matrix___base.php#a7af6e9c2c869742e856f10bc83f43ece" title="List of rows.">row_list_</a>(i), this-&gt;<a class="code" href="class_seldon_1_1_sub_matrix___base.php#aa0699422389286f5dab0b65a83ca0c2c" title="List of columns.">column_list_</a>(j));
<a name="l00109"></a>00109   }
<a name="l00110"></a>00110 
<a name="l00111"></a>00111 
<a name="l00113"></a>00113 
<a name="l00119"></a>00119   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> M, <span class="keyword">class</span> Allocator&gt;
<a name="l00120"></a>00120   <span class="keyword">inline</span> <span class="keyword">const</span> <span class="keyword">typename</span> SubMatrix_Base&lt;T, Prop, M, Allocator&gt;::entry_type&amp;
<a name="l00121"></a>00121   <a class="code" href="class_seldon_1_1_sub_matrix___base.php#a58c123d74c337c990ffeb7f18e911a5a" title="Access operator.">SubMatrix_Base&lt;T, Prop, M, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00122"></a>00122 <span class="keyword">  </span>{
<a name="l00123"></a>00123     <span class="keywordflow">return</span> this-&gt;<a class="code" href="class_seldon_1_1_sub_matrix___base.php#a39e77d13e783db34a7ea38c9ba6fc812" title="Pointer to the base matrix.">matrix_</a>-&gt;Val(this-&gt;<a class="code" href="class_seldon_1_1_sub_matrix___base.php#a7af6e9c2c869742e856f10bc83f43ece" title="List of rows.">row_list_</a>(i), this-&gt;<a class="code" href="class_seldon_1_1_sub_matrix___base.php#aa0699422389286f5dab0b65a83ca0c2c" title="List of columns.">column_list_</a>(j));
<a name="l00124"></a>00124   }
<a name="l00125"></a>00125 
<a name="l00126"></a>00126 
<a name="l00127"></a>00127   <span class="comment">/*****************</span>
<a name="l00128"></a>00128 <span class="comment">   * BASIC METHODS *</span>
<a name="l00129"></a>00129 <span class="comment">   *****************/</span>
<a name="l00130"></a>00130 
<a name="l00131"></a>00131 
<a name="l00133"></a>00133 
<a name="l00136"></a>00136   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> M, <span class="keyword">class</span> Allocator&gt;
<a name="l00137"></a>00137   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sub_matrix___base.php#a8c781e99310aae01786eac0e8e5aa50c" title="Returns the number of rows.">SubMatrix_Base&lt;T, Prop, M, Allocator&gt;::GetM</a>()<span class="keyword"> const</span>
<a name="l00138"></a>00138 <span class="keyword">  </span>{
<a name="l00139"></a>00139     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_sub_matrix___base.php#a7af6e9c2c869742e856f10bc83f43ece" title="List of rows.">row_list_</a>.GetLength();
<a name="l00140"></a>00140   }
<a name="l00141"></a>00141 
<a name="l00142"></a>00142 
<a name="l00144"></a>00144 
<a name="l00147"></a>00147   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> M, <span class="keyword">class</span> Allocator&gt;
<a name="l00148"></a>00148   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sub_matrix___base.php#a40580a92202044416e379c1304ff0220" title="Returns the number of columns.">SubMatrix_Base&lt;T, Prop, M, Allocator&gt;::GetN</a>()<span class="keyword"> const</span>
<a name="l00149"></a>00149 <span class="keyword">  </span>{
<a name="l00150"></a>00150     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_sub_matrix___base.php#aa0699422389286f5dab0b65a83ca0c2c" title="List of columns.">column_list_</a>.GetLength();
<a name="l00151"></a>00151   }
<a name="l00152"></a>00152 
<a name="l00153"></a>00153 
<a name="l00155"></a>00155 
<a name="l00159"></a>00159   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> M, <span class="keyword">class</span> Allocator&gt;
<a name="l00160"></a>00160   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sub_matrix___base.php#a8c781e99310aae01786eac0e8e5aa50c" title="Returns the number of rows.">SubMatrix_Base&lt;T, Prop, M, Allocator&gt;</a>
<a name="l00161"></a>00161 <a class="code" href="class_seldon_1_1_sub_matrix___base.php#a8c781e99310aae01786eac0e8e5aa50c" title="Returns the number of rows.">  ::GetM</a>(<span class="keyword">const</span> SeldonTranspose&amp; status)<span class="keyword"> const</span>
<a name="l00162"></a>00162 <span class="keyword">  </span>{
<a name="l00163"></a>00163     <span class="keywordflow">if</span> (status.NoTrans())
<a name="l00164"></a>00164       <span class="keywordflow">return</span> row_list_.GetLength();
<a name="l00165"></a>00165     <span class="keywordflow">else</span>
<a name="l00166"></a>00166       <span class="keywordflow">return</span> column_list_.GetLength();
<a name="l00167"></a>00167   }
<a name="l00168"></a>00168 
<a name="l00169"></a>00169 
<a name="l00171"></a>00171 
<a name="l00175"></a>00175   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> M, <span class="keyword">class</span> Allocator&gt;
<a name="l00176"></a>00176   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_sub_matrix___base.php#a40580a92202044416e379c1304ff0220" title="Returns the number of columns.">SubMatrix_Base&lt;T, Prop, M, Allocator&gt;</a>
<a name="l00177"></a>00177 <a class="code" href="class_seldon_1_1_sub_matrix___base.php#a40580a92202044416e379c1304ff0220" title="Returns the number of columns.">  ::GetN</a>(<span class="keyword">const</span> SeldonTranspose&amp; status)<span class="keyword"> const</span>
<a name="l00178"></a>00178 <span class="keyword">  </span>{
<a name="l00179"></a>00179     <span class="keywordflow">if</span> (status.NoTrans())
<a name="l00180"></a>00180       <span class="keywordflow">return</span> column_list_.GetLength();
<a name="l00181"></a>00181     <span class="keywordflow">else</span>
<a name="l00182"></a>00182       <span class="keywordflow">return</span> row_list_.GetLength();
<a name="l00183"></a>00183   }
<a name="l00184"></a>00184 
<a name="l00185"></a>00185 
<a name="l00187"></a>00187 
<a name="l00191"></a>00191   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> M, <span class="keyword">class</span> Allocator&gt;
<a name="l00192"></a>00192   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_sub_matrix___base.php#afa5beff801e73f6b95958d983a4e2c23" title="Prints a matrix on screen.">SubMatrix_Base&lt;T, Prop, M, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00193"></a>00193 <span class="keyword">  </span>{
<a name="l00194"></a>00194     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_sub_matrix___base.php#a8c781e99310aae01786eac0e8e5aa50c" title="Returns the number of rows.">GetM</a>(); i++)
<a name="l00195"></a>00195       {
<a name="l00196"></a>00196         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;<a class="code" href="class_seldon_1_1_sub_matrix___base.php#a40580a92202044416e379c1304ff0220" title="Returns the number of columns.">GetN</a>(); j++)
<a name="l00197"></a>00197           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00198"></a>00198         cout &lt;&lt; endl;
<a name="l00199"></a>00199       }
<a name="l00200"></a>00200   }
<a name="l00201"></a>00201 
<a name="l00202"></a>00202 
<a name="l00203"></a>00203 } <span class="comment">// namespace Seldon.</span>
<a name="l00204"></a>00204 
<a name="l00205"></a>00205 
<a name="l00206"></a>00206 <span class="preprocessor">#define SELDON_FILE_SUBMATRIX_BASE_CXX</span>
<a name="l00207"></a>00207 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
