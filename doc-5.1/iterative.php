<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Iterative Solvers </h1>  </div>
</div>
<div class="contents">
<h2>Syntax</h2>
<p>The syntax of all iterative solvers is the same </p>
<pre class="fragment">
int Gmres(const Matrix&amp; A, Vector&amp; x, const Vector&amp; b,
          Preconditioner&amp; M, Iteration&amp; iter);
</pre><p>The first argument <code>A</code> is the matrix to solve. The type of this argument is template, therefore you can provide your own class defining the matrix. The second and third argument are vectors, <code>x</code> contains the initial guess on input, the solution on output, <code>b</code> contains the right-hand-side. Again, the type of x and b is template, so you can have your own class to define the vectors. The fourth template argument is the preconditioner. For the moment, there is an implementation of the identity preconditioner (no preconditioning) and a SOR (Successive Over Relaxation) preconditioner. The last argument is a Seldon structure defining the parameters of the iteration.</p>
<h2>Basic use</h2>
<p>We provide an example of iterative resolution using Seldon structures for the matrices and the vectors.</p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// first we construct a sparse matrix
int n = 1000; // size of linear system
// we assume that you know how to fill arrays values, ptr, ind
Matrix&lt;double, General, RowSparse&lt; A(n, n, nnz, values, ptr, ind);

// then we declare vectors
Vector&lt;double&gt; b(n), x(n);

// you fill right-hand side and initial guess
b.Fill();
x.Fill(0);

// initialization of iteration parameters
int nb_max_iter = 1000;
// relative stopping criterion
double tolerance = 1e-6;
Iteration&lt;double&gt; iter(nb_max_iter, tolerance);

// identity preconditioner -&gt; no preconditioning
Preconditioner_Base precond;

// then you can call an iterative solver, Cg for example
Cg(A, x, b, precond, iter);

// if you are using Gmres, you can set the restart parameter
// by default, this parameter is equal to 10
iter.SetRestart(5);
Gmres(A, x, b, precond, iter);
</pre></div>  </pre><h2>Advanced use</h2>
<p>Now, we will show an example where we construct a new class for a matrix that we don't store. If you want to test iterative solvers with that kind of strategy, you can take a look at the file test/program/iterative_test.cpp. The main thing to do is to overload the functions <a href="functions_blas.php#mlt">Mlt</a> and <a href="functions_blas.php#mltadd">MltAdd</a> that are called by iterative solvers to perform the matrix-vector product.</p>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// Class for a new type of matrix.
template&lt;class T&gt;
class BlackBoxMatrix
{
protected :
  // internal data used to represent the matrix
  int n;
  T beta;
  Seldon::Vector&lt;T&gt; lambda;
  
public :
  // basic constructor  
  BlackBoxMatrix(int n_, const T&amp; beta_)
  {
    beta = beta_; n = n_;
    lambda.Reallocate(n);
    for (int i = 0; i &lt; n; i++)
      lambda(i) = i+1;
  }
  
  // this method is used by iterative solvers
  // this should return the number of rows of the matrix
  int GetM() const
  {
    return n;
  }
  
};


// We need to define MltAdd(alpha, A, X, beta, Y) : Y &lt;- beta Y + alpha A X
template&lt;class T, class Vector&gt;
void MltAdd(const T&amp; alpha, const BlackBoxMatrix&lt;T&gt;&amp; A,
	    const Vector&amp; X, const T&amp; beta, Vector&amp; Y)
{
  if (beta == T(0))
    Y.Fill(T(0));
  else
    Mlt(beta, Y);
  
  int n = A.GetM();
  Vector C(X);
  // C = S^{-1} C
  for (int i = (n-2); i &gt;= 0; i--)
    C(i) -= A.beta*C(i+1);
  
  // C = B C
  for (int i = 0; i &lt; n; i++)
    C(i) *= A.lambda(i);
  
  // C = S C
  for (int i = 0; i &lt; (n-1); i++)
    C(i) += A.beta*C(i+1);
  
  // Y = Y + alpha C
  Add(alpha, C, Y);
}


// and the transpose...
template&lt;class T, class Vector&gt;
void MltAdd(const T&amp; alpha, const class_SeldonTrans&amp; Trans,
	    const BlackBoxMatrix&lt;T&gt;&amp; A,
	    const Vector&amp; X, const T&amp; beta, Vector&amp; Y)
{
  if (beta == T(0))
    Y.Fill(T(0));
  else
    Mlt(beta, Y);
  
  int n = A.GetM();
  // Transpose of S B S^{-1} is S^{-t} B S^t
  Vector C(X);
  // Y = S^t Y
  for (int i = (n-1); i &gt;= 1; i--)
    C(i) += A.beta*C(i-1);
  
  // Y = B Y
  for (int i = 0; i &lt; n; i++)
    C(i) *= A.lambda(i);
  
  // Y = S^{-t} Y
  for (int i = 1; i &lt; n; i++)
    C(i) -= A.beta*C(i-1);
  
  // Y = Y + alpha C
  Add(alpha, C, Y);
}


template&lt;class T, class Vector&gt;
void Mlt(const BlackBoxMatrix&lt;T&gt;&amp; A, const Vector&amp; X, Vector&amp; Y)
{
  Y.Zero();
  MltAdd(T(1), A, X, T(0), Y);
}


template&lt;class T, class Vector&gt;
void Mlt(const class_SeldonTrans&amp; Trans, const BlackBoxMatrix&lt;T&gt;&amp; A,
	 const Vector&amp; X, Vector&amp; Y)
{
  Y.Zero();
  MltAdd(T(1), Trans, A, X, T(0), Y);
}

// class for preconditioning
template&lt;class T&gt;
class MyPreconditioner : public BlackBoxMatrix&lt;T&gt;
{
public : 
  
  MyPreconditioner(int n, T beta) : BlackBoxMatrix&lt;T&gt;(n, beta) { }  
  
  // solving M r = z
  template&lt;class Matrix, class Vector&gt;
  void Solve(const Matrix&amp; A, const Vector&amp; r, Vector&amp; z)
  {
    Mlt(*this, r, z);
  }
  
  // solving transpose(M) r = z
  template&lt;class Matrix, class Vector&gt;
  void TransSolve(const Matrix&amp; A, const Vector&amp; r, Vector&amp; z)
  {
    Mlt(SeldonTrans, *this, r, z);
  }
  
};

int main()
{
  // now it is very classical like the previous example
  int n = 20; double beta = 0.5;
  BlackBoxMatrix&lt;double&gt;; A(n, beta);
  Vector&lt;double&gt; b(n), x(n);
  
  // you fill right-hand side and initial guess
  b.Fill();
  x.Fill(0);
  
  // initialization of iteration parameters
  int nb_max_iter = 1000;
  double tolerance = 1e-6;
  Iteration&lt;double&gt; iter(nb_max_iter, tolerance);

  // your own preconditioner
  MyPreconditioner&lt;double&gt; precond(n, 1.2);
  
  // then you can call an iterative solver, Qmr for example
  Qmr(A, x, b, precond, iter);

  return 0;
}
</pre></div>  </pre><p>If you want to use your own class of vector, there are other functions to define : <a href="functions_blas.php#add">Add</a>, <a href="functions_blas.php#norm2">Norm2</a>, <a href="functions_blas.php#dotprod">DotProd</a>, <a href="functions_blas.php#dotprod">DotProdConj</a> , <a href="functions_blas.php#copy">Copy</a>, <a href="functions_blas.php#copy">Copy</a> and the method <a href="class_vector.php#zero">Zero</a>. </p>
<h2>Methods of Preconditioner_Base:</h2>
<table  class="category-table">
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#solve">Solve </a> </td><td class="category-table-td">Applies the preconditioner   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#solve">TransSolve</a> </td><td class="category-table-td">Applies the transpose of the preconditioner   </td></tr>
</table>
<h2>Methods of SorPreconditioner:</h2>
<table  class="category-table">
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#initsymmetricpreconditioning">InitSymmetricPreconditioning</a> </td><td class="category-table-td">Symmetric SOR will be used   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#initsymmetricpreconditioning">InitUnsymmetricPreconditioning </a> </td><td class="category-table-td">SOR will be used   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#setparameterrelaxation">SetParameterRelaxation </a> </td><td class="category-table-td">changes the relaxation parameter   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#setparameterrelaxation">SetNumberIterations </a> </td><td class="category-table-td">changes the number of iterations   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#sor_solve">Solve</a> </td><td class="category-table-td">Applies the preconditioner   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#sor_solve">TransSolve</a> </td><td class="category-table-td">Applies the transpose of the preconditioner   </td></tr>
</table>
<h2>Methods of Iteration:</h2>
<table  class="category-table">
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#constructor">Constructors</a> </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#restart">GetRestart</a> </td><td class="category-table-td">returns restart parameter   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#restart">SetRestart</a> </td><td class="category-table-td">changes restart parameter   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#tolerance">GetTolerance</a> </td><td class="category-table-td">returns stopping criterion   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#tolerance">SetTolerance</a> </td><td class="category-table-td">changes stopping criterion   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#maxiter">SetMaxIterationNumber</a> </td><td class="category-table-td">changes maximum number of iterations   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#iter">GetIterationNumber</a> </td><td class="category-table-td">returns iteration number   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#iter">SetIterationNumber</a> </td><td class="category-table-td">changes iteration number   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#messages">ShowMessages</a> </td><td class="category-table-td">displays residual each 100 iterations  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#messages">HideMessages</a> </td><td class="category-table-td">displays nothing  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#messages">ShowFullHistory</a> </td><td class="category-table-td">displays residual each iteration  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#init">Init</a> </td><td class="category-table-td">provides right hand side  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#first">First</a> </td><td class="category-table-td">returns true for the first iteration  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#initguessnull">IsInitGuess_Null</a> </td><td class="category-table-td">returns true if the initial guess is zero  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#initguessnull">SetInitGuess</a> </td><td class="category-table-td">informs if the initial guess is zero or no  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#finished">Finished</a> </td><td class="category-table-td">returns true if the stopping criteria are satisfied  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#fail">Fail</a> </td><td class="category-table-td">informs that the iterative solver failed  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#errorcode">ErrorCode</a> </td><td class="category-table-td">returns error code   </td></tr>
</table>
<h2><a class="anchor" id="solvers"></a>Functions :</h2>
<table  class="category-table">
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#sor">SOR</a> </td><td class="category-table-td">Performs SOR iterations </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#bicg">BiCg</a> </td><td class="category-table-td">BIConjugate Gradient  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#bicgcr">BiCgcr</a> </td><td class="category-table-td">BIConjugate Gradient Conjugate Residual </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#bicgstab">BiCgStab</a> </td><td class="category-table-td">BIConjugate Gradient STABilized </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#bicgstabl">BiCgStabl</a> </td><td class="category-table-td">BIConjugate Gradient STABilized (L) </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#cg">Cg</a> </td><td class="category-table-td">Conjugate Gradient </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#cgne">Cgne</a> </td><td class="category-table-td">Conjugate Gradient Normal Equation </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#cgs">Cgs</a> </td><td class="category-table-td">Conjugate Gradient Squared </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#cocg">CoCg</a> </td><td class="category-table-td">Conjugate Orthogonal Conjugate Gradient </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#gcr">Gcr</a> </td><td class="category-table-td">Generalized Conjugate Residual </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#gmres">Gmres</a> </td><td class="category-table-td">Generalized Minimum RESidual </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#lsqr">Lsqr</a> </td><td class="category-table-td">Least SQuaRes </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#minres">MinRes</a> </td><td class="category-table-td">Minimum RESidual </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#qcgs">QCgs</a> </td><td class="category-table-td">Quasi Conjugate Gradient Squared </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#qmr">Qmr</a> </td><td class="category-table-td">Quasi Minimum Residual </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#qmrsym">QmrSym</a> </td><td class="category-table-td">Quasi Minimum Residual SYMmetric </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#symmlq">Symmlq</a> </td><td class="category-table-td">SYMMetric Least sQuares </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#tfqmr">TfQmr</a> </td><td class="category-table-td">Transpose Free Quasi Minimum Residual </td></tr>
</table>
<div class="separator"><a class="anchor" id="solve"></a></div><h3>Solve, TransSolve for Preconditioner_Base</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void Solve(const Matrix&amp;, const Vector&amp;, Vector&amp;);
  void TransSolve(const Matrix&amp;, const Vector&amp;, Vector&amp;);
</pre><p>These methods should be overloaded if you want to define your own preconditioner since they define the application of the preconditioner and its transpose to a vector.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// constructor of a matrix
int n = 20;
Matrix&lt;double&gt; A(n, n);
A.Fill();

// declaration of a preconditioner
Preconditioner_Base M;

// vectors
Vector&lt;double&gt; r(n), z(n);
r.Fill();

// now we apply preconditioning, i.e. solving M z = r
// for Preconditioner_Base, it will set z = r (identity preconditioner)
M.Solve(A, r, z);

// we can also apply the transpose of preconditioner
// i.e. solving transpose(M) z = r
M.TransSolve(A, r, z);

</pre></div>  </pre><h4>Location :</h4>
<p>Class Preconditioner_Base<br/>
 <a class="el" href="_iterative_8hxx_source.php">Iterative.hxx</a><br/>
 <a class="el" href="_iterative_8cxx_source.php">Iterative.cxx</a></p>
<div class="separator"><a class="anchor" id="sor_solve"></a></div><h3>Solve, TransSolve for SorPreconditioner</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void Solve(const Matrix&amp;, const Vector&amp;, Vector&amp;);
  void TransSolve(const Matrix&amp;, const Vector&amp;, Vector&amp;);
</pre><p>These methods define the application of the preconditioner and its transpose to a vector. The used preconditioner is SOR (Successive Over Relaxation). It can be used in its symmetric version (SSOR), or the unsymmetric version. In this last case, the application of the preconditioner consists of a forward sweep while the transpose consists of a backward sweep. If you are using Seldon structures of sparse matrices, the function <a href="#sor">SOR</a> is available. If you are using other structures, it is necessary to overload the function SOR.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// constructor of a matrix
int n = 20;
Matrix&lt;double&gt; A(n, n);
A.Fill();

// declaration of a preconditioner
SorPreconditioner&lt;double&gt; M;

// by default, relaxation parameter omega = 1
// number of iterations = 1
// you can change that
M.SetParameterRelaxation(1.5);
M.SetNumberIterations(2);

// if you prefer to use symmetric version
M.InitSymmetricPreconditioning();

// vectors
Vector&lt;double&gt; r(n), z(n);
r.Fill();

// now we apply preconditioning, i.e. solving M z = r
// for Preconditioner_Base, it will set z = r (identity preconditioner)
M.Solve(A, r, z);

// we can also apply the transpose of preconditioner
// i.e. solving transpose(M) z = r
M.TransSolve(A, r, z);

</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="#sor">SOR</a></p>
<h4>Location :</h4>
<p>Class SorPreconditioner<br/>
 <a class="el" href="_precond___ssor_8cxx_source.php">Precond_Ssor.cxx</a></p>
<div class="separator"><a class="anchor" id="initsymmetricpreconditioning"></a></div><h3>InitSymmetricPreconditioning, InitUnSymmetricPreconditioning for SorPreconditioner</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void InitSymmetricPreconditioning();
  void InitUnsymmetricPreconditioning();
</pre><p><code>InitSymmetricPreconditioning</code> sets SSOR as preconditioning. The symmetric SOR consists of a forward sweep followed by a backward sweep. <code>InitSymmetricPreconditioning</code> sets SOR, it consists of a forward sweep for the preconditioner, and a backward sweep for the transpose of the preconditioner.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// declaration of a preconditioner
SorPreconditioner&lt;double&gt; M;

// by default, symmetric preconditioning
M.InitUnsymmetricPreconditioning();
</pre></div>  </pre><h4>Related topics:</h4>
<p><a href="#sor">SOR</a></p>
<h4>Location :</h4>
<p>Class SorPreconditioner<br/>
 <a class="el" href="_precond___ssor_8cxx_source.php">Precond_Ssor.cxx</a></p>
<div class="separator"><a class="anchor" id="setparameterrelaxation"></a></div><h3>SetParameterRelaxation for SorPreconditioner</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void SetParameterRelaxation(const T&amp; omega);
</pre><p>This methods changes the relaxation parameter.</p>
<h4>Location :</h4>
<p>Class SorPreconditioner<br/>
 <a class="el" href="_precond___ssor_8cxx_source.php">Precond_Ssor.cxx</a></p>
<div class="separator"><a class="anchor" id="setnumberiterations"></a></div><h3>SetNumberIterations for SorPreconditioner</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void SetNumberIterations(int);
</pre><p>This methods changes the number of SOR iterations.</p>
<h4>Location :</h4>
<p>Class SorPreconditioner<br/>
 <a class="el" href="_precond___ssor_8cxx_source.php">Precond_Ssor.cxx</a></p>
<div class="separator"><a class="anchor" id="constructor"></a></div><h3>Constructors for Iteration</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  Iteration();
  Iteration(int, const T&amp;);
</pre><p>The second constructor specifies the maximum number of iterations and the stopping criterion</p>
<h4>Location :</h4>
<p>Class Iteration<br/>
 <a class="el" href="_iterative_8hxx_source.php">Iterative.hxx</a><br/>
 <a class="el" href="_iterative_8cxx_source.php">Iterative.cxx</a></p>
<div class="separator"><a class="anchor" id="restart"></a></div><h3>GetRestart, SetRestart for Iteration</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int GetRestart();
  void SetRestart(int);
</pre><p>These methods give access to the restart parameter, which is used by some iterative solvers, e.g. BiCgSTAB(l), Gmres and Gcr. The default value is equal to 10.</p>
<h4>Location :</h4>
<p>Class Iteration<br/>
 <a class="el" href="_iterative_8hxx_source.php">Iterative.hxx</a><br/>
 <a class="el" href="_iterative_8cxx_source.php">Iterative.cxx</a></p>
<div class="separator"><a class="anchor" id="tolerance"></a></div><h3>GetTolerance, SetTolerance for Iteration</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  T GetTolerance();
  void SetTolerance(T);
</pre><p>These methods give access to the stopping criterion.</p>
<h4>Location :</h4>
<p>Class Iteration<br/>
 <a class="el" href="_iterative_8hxx_source.php">Iterative.hxx</a><br/>
 <a class="el" href="_iterative_8cxx_source.php">Iterative.cxx</a></p>
<div class="separator"><a class="anchor" id="maxiter"></a></div><h3>SetMaxIterationNumber for Iteration</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void SetMaxIterationNumber(int);
</pre><p>This method gives access to the maximum iteration number.</p>
<h4>Location :</h4>
<p>Class Iteration<br/>
 <a class="el" href="_iterative_8hxx_source.php">Iterative.hxx</a><br/>
 <a class="el" href="_iterative_8cxx_source.php">Iterative.cxx</a></p>
<div class="separator"><a class="anchor" id="iter"></a></div><h3>GetIterationNumber, SetIterationNumber for Iteration</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int GetIterationNumber();
  void SetIterationNumber(int);
</pre><p>This method gives access to the iteration number.</p>
<h4>Location :</h4>
<p>Class Iteration<br/>
 <a class="el" href="_iterative_8hxx_source.php">Iterative.hxx</a><br/>
 <a class="el" href="_iterative_8cxx_source.php">Iterative.cxx</a></p>
<div class="separator"><a class="anchor" id="messages"></a></div><h3>ShowMessages, HideMessages, ShowFullHistory for Iteration</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void ShowMessages();
  void HideMessages();
  void ShowFullHistory();
</pre><p>If <code>ShowMessages</code> is called, it will display residual each 100 iterations during the resolution. If <code>HideMessages</code> is called, there is no display at all, whereas <code>ShowFullHistory</code> displays residual each iteration.</p>
<h4>Location :</h4>
<p>Class Iteration<br/>
 <a class="el" href="_iterative_8hxx_source.php">Iterative.hxx</a><br/>
 <a class="el" href="_iterative_8cxx_source.php">Iterative.cxx</a></p>
<div class="separator"><a class="anchor" id="init"></a></div><h3>Init for Iteration</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void Init(const Vector&amp;);
</pre><p>This method is used by iterative solvers to initialize the computation of residuals. Since relative residual is computed, we need to know the norm of the first residual.</p>
<h4>Location :</h4>
<p>Class Iteration<br/>
 <a class="el" href="_iterative_8hxx_source.php">Iterative.hxx</a><br/>
 <a class="el" href="_iterative_8cxx_source.php">Iterative.cxx</a></p>
<div class="separator"><a class="anchor" id="first"></a></div><h3>First for Iteration</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  bool First();
</pre><p>This method returns true for the first iteration.</p>
<h4>Location :</h4>
<p>Class Iteration<br/>
 <a class="el" href="_iterative_8hxx_source.php">Iterative.hxx</a><br/>
 <a class="el" href="_iterative_8cxx_source.php">Iterative.cxx</a></p>
<div class="separator"><a class="anchor" id="initguessnull"></a></div><h3>SetInitGuess, IsInitGuess_Null for Iteration </h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  bool IsInitGuess_Null();
  void SetInitGuess(bool);
</pre><p><code>SetInitGuess</code> allows you to inform the iterative solver that your initial guess is null. This can spare one matrix-vector product, which is good if the expected number of iterations is small.</p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
int n = 10;
Matrix&lt;double&gt; A(n, n);
Vector&lt;double&gt; x(n), b(n);
A.Fill();
b.Fill();

Iteration&lt;double&gt; iter(100, 1e-6);
// you inform that initial guess is null
iter.SetInitGuess(true);

// if the initial guess is null
// and x is non-null, the solver enforces x to be 0
Preconditioner_Base M;
Gmres(A, x, b, M, iter);

</pre></div>  </pre><h4>Location :</h4>
<p>Class Iteration<br/>
 <a class="el" href="_iterative_8hxx_source.php">Iterative.hxx</a><br/>
 <a class="el" href="_iterative_8cxx_source.php">Iterative.cxx</a></p>
<div class="separator"><a class="anchor" id="finished"></a></div><h3>Finished for Iteration</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  bool Finished(const Vector&amp;);
</pre><p>This method is used by iterative solvers to know if the stopping criterion is reached. This method also displays the residual if required. </p>
<h4>Location :</h4>
<p>Class Iteration<br/>
 <a class="el" href="_iterative_8hxx_source.php">Iterative.hxx</a><br/>
 <a class="el" href="_iterative_8cxx_source.php">Iterative.cxx</a></p>
<div class="separator"><a class="anchor" id="fail"></a></div><h3>Fail for Iteration</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  bool Fail(int, const string&amp;);
</pre><p>This method is used by iterative solvers when a breakdown occurs, often due to a division by 0. </p>
<h4>Location :</h4>
<p>Class Iteration<br/>
 <a class="el" href="_iterative_8hxx_source.php">Iterative.hxx</a><br/>
 <a class="el" href="_iterative_8cxx_source.php">Iterative.cxx</a></p>
<div class="separator"><a class="anchor" id="errorcode"></a></div><h3>ErrorCode for Iteration</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int ErrorCode();
</pre><p>This method returns the error code after the resolution. If the resolution has been successful, it should return 0. </p>
<h4>Location :</h4>
<p>Class Iteration<br/>
 <a class="el" href="_iterative_8hxx_source.php">Iterative.hxx</a><br/>
 <a class="el" href="_iterative_8cxx_source.php">Iterative.cxx</a></p>
<div class="separator"><a class="anchor" id="sor"></a></div><h3>SOR</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void SOR(const Matrix&amp;, Vector&amp;, const Vector&amp;, const T&amp;, int);
  void SOR(const Matrix&amp;, Vector&amp;, const Vector&amp;, const T&amp;, int, int);
</pre><p>This method tries to solve <code>A x = b</code> with n iterations of SOR. </p>
<h4>Example :</h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
int n = 1000;
Matrix&lt;double, General, RowSparse&gt; A;
// you initialize A as you want (SetData for example)

// then vectors
Vector&lt;double&gt; x(n), b(n); 
x.Zero();
b.Fill();

// we want to solve A x = b
// 2 Sor iterations (forward sweep) with omega = 0.5
double omega = 0.5;
int nb_iterations = 2;
SOR(A, x, b, omega, nb_iterations);

// you can ask for symmetric SOR: forward sweeps followed
//                                by backward sweeps
SOR(A, x, b, omega, nb_iterations, 0);

// if you need backward sweep
SOR(A, x, b, omega, nb_iterations, 3);

</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_relaxation___mat_vect_8cxx_source.php">Relaxation_MatVect.cxx</a></p>
<div class="separator"><a class="anchor" id="bicg"></a></div><h3>BiCg</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int BiCg(const Matrix&amp;, Vector&amp;, const Vector&amp;,
           Preconditioner&amp;, Iteration&amp;);
</pre><p>This method tries to solve <code>A x = b</code> by using BICG algorithm. This algorithm can solve complex general linear systems and calls matrix-vector products with the transpose matrix. </p>
<h4>Location :</h4>
<p><a class="el" href="_bi_cg_8cxx_source.php">BiCg.cxx</a></p>
<div class="separator"><a class="anchor" id="bicgcr"></a></div><h3>BiCgcr</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int BiCgcr(const Matrix&amp;, Vector&amp;, const Vector&amp;,
             Preconditioner&amp;, Iteration&amp;);
</pre><p>This method tries to solve <code>A x = b</code> by using BICGCR algorithm. This algorithm can solve symmetric complex linear systems. </p>
<h4>Location :</h4>
<p><a class="el" href="_bi_cgcr_8cxx_source.php">BiCgcr.cxx</a></p>
<div class="separator"><a class="anchor" id="bicgstab"></a></div><h3>BiCgStab</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int BiCgStab(const Matrix&amp;, Vector&amp;, const Vector&amp;,
               Preconditioner&amp;, Iteration&amp;);
</pre><p>This method tries to solve <code>A x = b</code> by using BICGSTAB algorithm. This algorithm can solve complex general linear systems and doesn't call matrix vector products with the transpose matrix. </p>
<h4>Location :</h4>
<p><a class="el" href="_bi_cg_stab_8cxx_source.php">BiCgStab.cxx</a></p>
<div class="separator"><a class="anchor" id="bicgstabl"></a></div><h3>BiCgStabl</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int BiCgStabl(const Matrix&amp;, Vector&amp;, const Vector&amp;,
                Preconditioner&amp;, Iteration&amp;);
</pre><p>This method tries to solve <code>A x = b</code> by using BICGSTAB(l) algorithm. This algorithm can solve complex general linear systems and doesn't call matrix vector products with the transpose matrix. </p>
<h4>Location :</h4>
<p><a class="el" href="_bi_cg_stabl_8cxx_source.php">BiCgStabl.cxx</a></p>
<div class="separator"><a class="anchor" id="cg"></a></div><h3>Cg</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int Cg(const Matrix&amp;, Vector&amp;, const Vector&amp;,
         Preconditioner&amp;, Iteration&amp;);
</pre><p>This method tries to solve <code>A x = b</code> by using CG algorithm. This algorithm can solve real symmetric or hermitian linear systems. </p>
<h4>Location :</h4>
<p><a class="el" href="_cg_8cxx_source.php">Cg.cxx</a></p>
<div class="separator"><a class="anchor" id="cgne"></a></div><h3>Cgne</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int Cgne(const Matrix&amp;, Vector&amp;, const Vector&amp;,
           Preconditioner&amp;, Iteration&amp;);
</pre><p>This method tries to solve <code>A x = b</code> by using CGNE algorithm. This algorithm can solve complex general linear systems and calls matrix vector products with the transpose matrix. </p>
<h4>Location :</h4>
<p><a class="el" href="_cgne_8cxx_source.php">Cgne.cxx</a></p>
<div class="separator"><a class="anchor" id="cgs"></a></div><h3>Cgs</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int Cgs(const Matrix&amp;, Vector&amp;, const Vector&amp;,
          Preconditioner&amp;, Iteration&amp;);
</pre><p>This method tries to solve <code>A x = b</code> by using CGS algorithm. This algorithm can solve complex general linear systems and doesn't call matrix vector products with the transpose matrix. </p>
<h4>Location :</h4>
<p><a class="el" href="_cgs_8cxx_source.php">Cgs.cxx</a></p>
<div class="separator"><a class="anchor" id="cocg"></a></div><h3>Cocg</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int Cocg(const Matrix&amp;, Vector&amp;, const Vector&amp;,
           Preconditioner&amp;, Iteration&amp;);
</pre><p>This method tries to solve <code>A x = b</code> by using BICGSTAB algorithm. This algorithm can solve complex symmetric linear systems. </p>
<h4>Location :</h4>
<p><a class="el" href="_co_cg_8cxx_source.php">CoCg.cxx</a></p>
<div class="separator"><a class="anchor" id="gcr"></a></div><h3>Gcr</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int Gcr(const Matrix&amp;, Vector&amp;, const Vector&amp;,
          Preconditioner&amp;, Iteration&amp;);
</pre><p>This method tries to solve <code>A x = b</code> by using BICGSTAB algorithm. This algorithm can solve complex general linear systems and doesn't call matrix vector products with the transpose matrix. </p>
<h4>Location :</h4>
<p><a class="el" href="_gcr_8cxx_source.php">Gcr.cxx</a></p>
<div class="separator"><a class="anchor" id="gmres"></a></div><h3>Gmres</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int Gmres(const Matrix&amp;, Vector&amp;, const Vector&amp;,
            Preconditioner&amp;, Iteration&amp;);
</pre><p>This method tries to solve <code>A x = b</code> by using restarted GMRES algorithm. This algorithm can solve complex general linear systems and doesn't call matrix vector products with the transpose matrix. </p>
<h4>Location :</h4>
<p><a class="el" href="_gmres_8cxx_source.php">Gmres.cxx</a></p>
<div class="separator"><a class="anchor" id="lsqr"></a></div><h3>Lsqr</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int Lsqr(const Matrix&amp;, Vector&amp;, const Vector&amp;,
           Preconditioner&amp;, Iteration&amp;);
</pre><p>This method tries to solve <code>A x = b</code> by using LSQR algorithm. This algorithm can solve complex general linear systems and calls matrix vector products with the transpose matrix. </p>
<h4>Location :</h4>
<p><a class="el" href="_lsqr_8cxx_source.php">Lsqr.cxx</a></p>
<div class="separator"><a class="anchor" id="minres"></a></div><h3>MinRes</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int MinRes(const Matrix&amp;, Vector&amp;, const Vector&amp;,
             Preconditioner&amp;, Iteration&amp;);
</pre><p>This method tries to solve <code>A x = b</code> by using BICGSTAB algorithm. This algorithm can solve complex symmetric linear systems and doesn't call matrix vector products with the transpose matrix. </p>
<h4>Location :</h4>
<p><a class="el" href="_min_res_8cxx_source.php">MinRes.cxx</a></p>
<div class="separator"><a class="anchor" id="qcgs"></a></div><h3>QCgs</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int QCgs(const Matrix&amp;, Vector&amp;, const Vector&amp;,
           Preconditioner&amp;, Iteration&amp;);
</pre><p>This method tries to solve <code>A x = b</code> by using QCGS algorithm. This algorithm can solve complex general linear systems and doesn't call matrix vector products with the transpose matrix. </p>
<h4>Location :</h4>
<p><a class="el" href="_q_cgs_8cxx_source.php">QCgs.cxx</a></p>
<div class="separator"><a class="anchor" id="qmr"></a></div><h3>Qmr</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int Qmr(const Matrix&amp;, Vector&amp;, const Vector&amp;,
          Preconditioner&amp;, Iteration&amp;);
</pre><p>This method tries to solve <code>A x = b</code> by using QMR algorithm. This algorithm can solve complex general linear systems and calls matrix vector products with the transpose matrix. </p>
<h4>Location :</h4>
<p><a class="el" href="_qmr_8cxx_source.php">Qmr.cxx</a></p>
<div class="separator"><a class="anchor" id="qmrsym"></a></div><h3>QmrSym</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int QmrSym(const Matrix&amp;, Vector&amp;, const Vector&amp;,
             Preconditioner&amp;, Iteration&amp;);
</pre><p>This method tries to solve <code>A x = b</code> by using symmetric QMR algorithm. This algorithm can solve complex symmetric linear systems. </p>
<h4>Location :</h4>
<p><a class="el" href="_qmr_sym_8cxx_source.php">QmrSym.cxx</a></p>
<div class="separator"><a class="anchor" id="symmlq"></a></div><h3>Symmlq</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int Symmlq(const Matrix&amp;, Vector&amp;, const Vector&amp;,
             Preconditioner&amp;, Iteration&amp;);
</pre><p>This method tries to solve <code>A x = b</code> by using SYMMLQ algorithm. This algorithm can solve complex symmetric linear systems. </p>
<h4>Location :</h4>
<p><a class="el" href="_symmlq_8cxx_source.php">Symmlq.cxx</a></p>
<div class="separator"><a class="anchor" id="tfqmr"></a></div><h3>TfQmr</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  int TfQmr(const Matrix&amp;, Vector&amp;, const Vector&amp;,
            Preconditioner&amp;, Iteration&amp;);
</pre><p>This method tries to solve <code>A x = b</code> by using TFQMR algorithm. This algorithm can solve complex general linear systems and doesn't call matrix vector products with the transpose matrix. </p>
<h4>Location :</h4>
<p><a class="el" href="_tf_qmr_8cxx_source.php">TfQmr.cxx</a> </p>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
