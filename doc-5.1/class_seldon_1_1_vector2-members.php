<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>, including all inherited members.<table>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a8c34a780a5b3f8b83bf36a14a4e7f21a">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#ad95c6d7a9a6dd52aae5aef1ffc608b71">Clear</a>(int i)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a7074df5f337f30702d6b6d59f4855a30">Copy</a>(const Vector2&lt; T, Allocator0, Allocator1 &gt; &amp;V)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>data_</b> (defined in <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a2794e1d6c95f016265e70453f13c88d3">Fill</a>(const T &amp;x)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a3be94439c78312344107d23fbb290e0e">Flatten</a>(Vector&lt; Td, VectFull, Allocatord &gt; &amp;data) const </td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a98d6e0c5dda92cbd0d70b1bfa16bd2ee">Flatten</a>(int beg, int end, Vector&lt; Td, VectFull, Allocatord &gt; &amp;data) const </td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8">GetLength</a>() const </td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a77b5138b5a35df2dbbd8088782b161a1">GetLength</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#aff7d041efb4e03152ecb03966ae92948">GetNelement</a>() const </td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a482333565b8f18465e2b10a7a5b8f2d9">GetNelement</a>(int beg, int end) const </td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a82efefffc4e2e7ebaf120e080a79fb95">GetShape</a>() const </td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a2800ef7139ca54d9386bab9a94910ebe">GetShape</a>(Vector&lt; int &gt; &amp;shape) const </td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a1eb80b01b78bb858b72a3daace5b7fa2">GetSize</a>() const </td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a1d5851ecabcb996725793f8aeb9ac7fa">GetSize</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a6955ada9427842710beeaa19ae65e299">GetVector</a>()</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a3f2341c313e4d593476afe4f94257330">GetVector</a>() const </td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a9db5289640f107404e5a4d536c991511">GetVector</a>(int i)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a223c85f251be71548ad21b3f9844be7d">GetVector</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#ab3a611bb7d4dd2bfc0f7c6540665db38">HasSameShape</a>(const V2 &amp;V) const </td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a4dadbc6f7e013e21ceacc75efcb42c58">IsEmpty</a>() const </td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#ae163011cc1d13382280abd54c1f4af93">operator()</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#ad8b3dd46b3b18c63fbf91ff266151ce4">operator()</a>(int i)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a90309917cc1fe070d182ed32bf97a0ed">operator()</a>(int i, int j) const </td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a4fe142f0dce38f333d19854401394d86">operator()</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#ab283856136c91c88d2f944eabed7bdc8">Print</a>() const </td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a1d386f766b75c53d17b66bc843a4db54">PushBack</a>(int i, const T &amp;x)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#acbc05b0873524168d21b8f465a22670a">PushBack</a>(const Vector&lt; T, VectFull, Allocator0 &gt; &amp;X)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a424df1ac3a13eb1120c1afd07c6b0271">PushBack</a>(const Vector&lt; Vector&lt; T, VectFull, Allocator0 &gt;, VectFull, Allocator1 &gt; &amp;V)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#acd15ae896e68859a4d7f6a2aa29f09d2">PushBack</a>(const Vector2&lt; T, Allocator0, Allocator1 &gt; &amp;V)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#ab0e6ad584c49c4182153a4e523e177ea">Reallocate</a>(int M)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#aef69b526bcb070821a83f15ec44f0678">Reallocate</a>(int i, int N)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#ad7b662c330a05a55bd927f9b9ae8fc24">Reallocate</a>(const Vector&lt; int &gt; &amp;length)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a2cf9e05eacb905603b0f0cfc8e7cdc5a">Select</a>(int beg, int end)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>value_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#ac0902a15a512d012c9825d69345c0cc2">Vector2</a>()</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a75217df57cc3aa01f13a20668ba410fb">Vector2</a>(int length)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a00822908f0850cae2262f884297d579e">Vector2</a>(const Vector&lt; int &gt; &amp;length)</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_vector2.php#a07190b6e16f512ee68feddf636c2698d">~Vector2</a>()</td><td><a class="el" href="class_seldon_1_1_vector2.php">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a></td><td></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
