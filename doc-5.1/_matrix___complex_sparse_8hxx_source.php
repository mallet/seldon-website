<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix_sparse/Matrix_ComplexSparse.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="comment">// To be included by Seldon.hxx</span>
<a name="l00021"></a>00021 
<a name="l00022"></a>00022 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_COMPLEXSPARSE_HXX</span>
<a name="l00023"></a>00023 <span class="preprocessor"></span>
<a name="l00024"></a>00024 <span class="preprocessor">#include &quot;../share/Common.hxx&quot;</span>
<a name="l00025"></a>00025 <span class="preprocessor">#include &quot;../share/Properties.hxx&quot;</span>
<a name="l00026"></a>00026 <span class="preprocessor">#include &quot;../share/Storage.hxx&quot;</span>
<a name="l00027"></a>00027 <span class="preprocessor">#include &quot;../share/Errors.hxx&quot;</span>
<a name="l00028"></a>00028 <span class="preprocessor">#include &quot;../share/Allocator.hxx&quot;</span>
<a name="l00029"></a>00029 
<a name="l00030"></a>00030 <span class="keyword">namespace </span>Seldon
<a name="l00031"></a>00031 {
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00035"></a>00035 
<a name="l00046"></a>00046   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T, <span class="keyword">class </span>Prop, <span class="keyword">class </span>Storage,
<a name="l00047"></a>00047             <span class="keyword">class </span>Allocator = SELDON_DEFAULT_ALLOCATOR&lt;T&gt; &gt;
<a name="l00048"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php">00048</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse</a>: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;
<a name="l00049"></a>00049   {
<a name="l00050"></a>00050     <span class="comment">// typedef declaration.</span>
<a name="l00051"></a>00051   <span class="keyword">public</span>:
<a name="l00052"></a>00052     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00053"></a>00053     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::pointer pointer;
<a name="l00054"></a>00054     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_pointer const_pointer;
<a name="l00055"></a>00055     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::reference reference;
<a name="l00056"></a>00056     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_reference const_reference;
<a name="l00057"></a>00057     <span class="keyword">typedef</span> complex&lt;value_type&gt; entry_type;
<a name="l00058"></a>00058     <span class="keyword">typedef</span> complex&lt;value_type&gt; access_type;
<a name="l00059"></a>00059     <span class="keyword">typedef</span> complex&lt;value_type&gt; const_access_type;
<a name="l00060"></a>00060 
<a name="l00061"></a>00061     <span class="comment">// Attributes.</span>
<a name="l00062"></a>00062   <span class="keyword">protected</span>:
<a name="l00063"></a>00063     <span class="comment">// Number of non-zero elements.</span>
<a name="l00064"></a>00064     <span class="keywordtype">int</span> real_nz_;
<a name="l00065"></a>00065     <span class="keywordtype">int</span> imag_nz_;
<a name="l00066"></a>00066     <span class="comment">// Index (in data_) of first element stored for each row or column.</span>
<a name="l00067"></a>00067     <span class="keywordtype">int</span>* real_ptr_;
<a name="l00068"></a>00068     <span class="keywordtype">int</span>* imag_ptr_;
<a name="l00069"></a>00069     <span class="comment">// Column or row index (in the matrix) each element.</span>
<a name="l00070"></a>00070     <span class="keywordtype">int</span>* real_ind_;
<a name="l00071"></a>00071     <span class="keywordtype">int</span>* imag_ind_;
<a name="l00072"></a>00072 
<a name="l00073"></a>00073     <span class="comment">// Data.</span>
<a name="l00074"></a>00074     T* real_data_;
<a name="l00075"></a>00075     T* imag_data_;
<a name="l00076"></a>00076 
<a name="l00077"></a>00077     <span class="comment">// Methods.</span>
<a name="l00078"></a>00078   <span class="keyword">public</span>:
<a name="l00079"></a>00079     <span class="comment">// Constructors.</span>
<a name="l00080"></a>00080     <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">Matrix_ComplexSparse</a>();
<a name="l00081"></a>00081     <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">Matrix_ComplexSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00082"></a>00082     <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">Matrix_ComplexSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> real_nz, <span class="keywordtype">int</span> imag_nz);
<a name="l00083"></a>00083     <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00084"></a>00084               <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00085"></a>00085               <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00086"></a>00086     <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">Matrix_ComplexSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00087"></a>00087                          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; real_values,
<a name="l00088"></a>00088                          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; real_ptr,
<a name="l00089"></a>00089                          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; real_ind,
<a name="l00090"></a>00090                          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; imag_values,
<a name="l00091"></a>00091                          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; imag_ptr,
<a name="l00092"></a>00092                          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; imag_ind);
<a name="l00093"></a>00093     <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">Matrix_ComplexSparse</a>(<span class="keyword">const</span> Matrix_ComplexSparse&lt;T, Prop,
<a name="l00094"></a>00094                          Storage, Allocator&gt;&amp; A);
<a name="l00095"></a>00095 
<a name="l00096"></a>00096     <span class="comment">// Destructor.</span>
<a name="l00097"></a>00097     <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3b785e50c55d9c0e00180bc7d3332d75" title="Destructor.">~Matrix_ComplexSparse</a>();
<a name="l00098"></a>00098     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a8940cdf1faedc44051919b9774132fa9" title="Clears the matrix.">Clear</a>();
<a name="l00099"></a>00099 
<a name="l00100"></a>00100     <span class="comment">// Memory management.</span>
<a name="l00101"></a>00101     <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00102"></a>00102               <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00103"></a>00103               <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00104"></a>00104     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3729058de11ead808a09e98a10702d85" title="Redefines the matrix.">SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00105"></a>00105                  <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; real_values,
<a name="l00106"></a>00106                  <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; real_ptr,
<a name="l00107"></a>00107                  <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; real_ind,
<a name="l00108"></a>00108                  <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; imag_values,
<a name="l00109"></a>00109                  <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; imag_ptr,
<a name="l00110"></a>00110                  <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; imag_ind);
<a name="l00111"></a>00111     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3729058de11ead808a09e98a10702d85" title="Redefines the matrix.">SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00112"></a>00112                  <span class="keywordtype">int</span> real_nz, pointer real_values, <span class="keywordtype">int</span>* real_ptr,
<a name="l00113"></a>00113                  <span class="keywordtype">int</span>* real_ind,
<a name="l00114"></a>00114                  <span class="keywordtype">int</span> imag_nz, pointer imag_values, <span class="keywordtype">int</span>* imag_ptr,
<a name="l00115"></a>00115                  <span class="keywordtype">int</span>* imag_ind);
<a name="l00116"></a>00116     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a10672a9a570060c9bd8f17eb54dc46fa" title="Clears the matrix without releasing memory.">Nullify</a>();
<a name="l00117"></a>00117     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae583202802b373763ce3780fe2382f4f" title="Copies a matrix.">Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A);
<a name="l00118"></a>00118 
<a name="l00119"></a>00119     <span class="comment">// Basic methods.</span>
<a name="l00120"></a>00120     <span class="keywordtype">int</span> GetNonZeros() <span class="keyword">const</span>;
<a name="l00121"></a>00121     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a543d8834abfbd7888c6d2516612bde7b" title="Returns the number of elements stored in memory.">GetDataSize</a>() <span class="keyword">const</span>;
<a name="l00122"></a>00122     <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#afa783bff3e823f8ad2480a50d4d28929" title="Returns (row or column) start indices for the real part.">GetRealPtr</a>() <span class="keyword">const</span>;
<a name="l00123"></a>00123     <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a6766b6db59aa35b4fa7458a12e531e9b" title="Returns (row or column) start indices for the imaginary part.">GetImagPtr</a>() <span class="keyword">const</span>;
<a name="l00124"></a>00124     <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a4eb0b0535cb51c9c0886da147f6e0cb0" title="Returns (row or column) indices of non-zero entries for the real part.">GetRealInd</a>() <span class="keyword">const</span>;
<a name="l00125"></a>00125     <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a08f3d91609557b1650066765343909db" title="Returns (row or column) indices of non-zero entries //! for the imaginary part.">GetImagInd</a>() <span class="keyword">const</span>;
<a name="l00126"></a>00126     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3be4bf190b7c5957d81ddf7e7a7b7882" title="Returns the length of the array of start indices for the real part.">GetRealPtrSize</a>() <span class="keyword">const</span>;
<a name="l00127"></a>00127     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae97357d4afa167ca95d28e8897e44ef2" title="Returns the length of the array of start indices for the imaginary part.">GetImagPtrSize</a>() <span class="keyword">const</span>;
<a name="l00128"></a>00128     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae573e32219c095276106254a84e7f684" title="Returns the length of the array of (column or row) indices //! for the real part...">GetRealIndSize</a>() <span class="keyword">const</span>;
<a name="l00129"></a>00129     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad61069ad96f1bfd7b57f6a4e9fe85764" title="Returns the length of the array of (column or row) indices //! for the imaginary...">GetImagIndSize</a>() <span class="keyword">const</span>;
<a name="l00130"></a>00130     T* <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a74325c4f424c0043682b5cb3ad3df501" title="Returns the array of values of the real part.">GetRealData</a>() <span class="keyword">const</span>;
<a name="l00131"></a>00131     T* <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a5637e2dd95b0e62a7f42c0f3fd1383b1" title="Returns the array of values of the imaginary part.">GetImagData</a>() <span class="keyword">const</span>;
<a name="l00132"></a>00132 
<a name="l00133"></a>00133     <span class="comment">// Element acess and affectation.</span>
<a name="l00134"></a>00134     complex&lt;value_type&gt; <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad9e481e5ea6de45867d8952dd9676bea" title="Access operator.">operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00135"></a>00135     complex&lt;value_type&gt;&amp; <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a63364150adf37907e6622c21d5f0a5f9" title="Unavailable access method.">Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00136"></a>00136     <span class="keyword">const</span> complex&lt;value_type&gt;&amp; <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a63364150adf37907e6622c21d5f0a5f9" title="Unavailable access method.">Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00137"></a>00137     <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00138"></a>00138     <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3e54cf340623d3c2c03f4926d9846383" title="Duplicates a matrix (assignment operator).">operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A);
<a name="l00139"></a>00139 
<a name="l00140"></a>00140     <span class="comment">// Convenient functions.</span>
<a name="l00141"></a>00141     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3bf9cba0b13332169dd7bf67810387dd" title="Displays the matrix on the standard output.">Print</a>() <span class="keyword">const</span>;
<a name="l00142"></a>00142   };
<a name="l00143"></a>00143 
<a name="l00144"></a>00144 
<a name="l00146"></a>00146   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00147"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_complex_sparse_00_01_allocator_01_4.php">00147</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_complex_sparse.php">ColComplexSparse</a>, Allocator&gt;:
<a name="l00148"></a>00148     <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse</a>&lt;T, Prop, ColComplexSparse, Allocator&gt;
<a name="l00149"></a>00149   {
<a name="l00150"></a>00150     <span class="comment">// typedef declaration.</span>
<a name="l00151"></a>00151   <span class="keyword">public</span>:
<a name="l00152"></a>00152     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00153"></a>00153     <span class="keyword">typedef</span> Prop property;
<a name="l00154"></a>00154     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_col_complex_sparse.php">ColComplexSparse</a> <a class="code" href="class_seldon_1_1_col_complex_sparse.php">storage</a>;
<a name="l00155"></a>00155     <span class="keyword">typedef</span> Allocator allocator;
<a name="l00156"></a>00156 
<a name="l00157"></a>00157   <span class="keyword">public</span>:
<a name="l00158"></a>00158     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>()  <span class="keywordflow">throw</span>();
<a name="l00159"></a>00159     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00160"></a>00160     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> real_nz, <span class="keywordtype">int</span> imag_nz);
<a name="l00161"></a>00161     <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00162"></a>00162               <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00163"></a>00163               <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00164"></a>00164     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00165"></a>00165            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; real_values,
<a name="l00166"></a>00166            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; real_ptr,
<a name="l00167"></a>00167            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; real_ind,
<a name="l00168"></a>00168            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; imag_values,
<a name="l00169"></a>00169            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; imag_ptr,
<a name="l00170"></a>00170            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; imag_ind);
<a name="l00171"></a>00171   };
<a name="l00172"></a>00172 
<a name="l00173"></a>00173 
<a name="l00175"></a>00175   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00176"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_complex_sparse_00_01_allocator_01_4.php">00176</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_complex_sparse.php">RowComplexSparse</a>, Allocator&gt;:
<a name="l00177"></a>00177     <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse</a>&lt;T, Prop, RowComplexSparse, Allocator&gt;
<a name="l00178"></a>00178   {
<a name="l00179"></a>00179     <span class="comment">// typedef declaration.</span>
<a name="l00180"></a>00180   <span class="keyword">public</span>:
<a name="l00181"></a>00181     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00182"></a>00182     <span class="keyword">typedef</span> Prop property;
<a name="l00183"></a>00183     <span class="keyword">typedef</span> <a class="code" href="class_seldon_1_1_row_complex_sparse.php">RowComplexSparse</a> <a class="code" href="class_seldon_1_1_row_complex_sparse.php">storage</a>;
<a name="l00184"></a>00184     <span class="keyword">typedef</span> Allocator allocator;
<a name="l00185"></a>00185 
<a name="l00186"></a>00186   <span class="keyword">public</span>:
<a name="l00187"></a>00187     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>()  <span class="keywordflow">throw</span>();
<a name="l00188"></a>00188     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00189"></a>00189     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> real_nz, <span class="keywordtype">int</span> imag_nz);
<a name="l00190"></a>00190     <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00191"></a>00191               <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00192"></a>00192               <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00193"></a>00193     <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00194"></a>00194            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; values,
<a name="l00195"></a>00195            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; ptr,
<a name="l00196"></a>00196            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; ind,
<a name="l00197"></a>00197            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; imag_values,
<a name="l00198"></a>00198            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; imag_ptr,
<a name="l00199"></a>00199            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; imag_ind);
<a name="l00200"></a>00200   };
<a name="l00201"></a>00201 
<a name="l00202"></a>00202 
<a name="l00203"></a>00203 } <span class="comment">// namespace Seldon.</span>
<a name="l00204"></a>00204 
<a name="l00205"></a>00205 <span class="preprocessor">#define SELDON_FILE_MATRIX_COMPLEXSPARSE_HXX</span>
<a name="l00206"></a>00206 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
