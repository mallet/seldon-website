<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>, including all inherited members.<table>
  <tr bgcolor="#f0f0f0"><td><b>access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#ab2d2137e557ad1acd76fed600f5c3584">AddInteraction</a>(int i, int j, const complex&lt; T &gt; &amp;val)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a3a9584691f4c544d683819e70202db20">AddInteractionColumn</a>(int i, int nb, const IVect &amp;row, const Vector&lt; complex&lt; T &gt;, VectFull, Alloc1 &gt; &amp;val)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a445de4f86d354e07251b8a9da3eff045">AddInteractionRow</a>(int i, int nb, const IVect &amp;col, const Vector&lt; complex&lt; T &gt;, VectFull, Alloc1 &gt; &amp;val)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>allocator</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a889d3baa1c4aa6a96b83ba8ca7b4cf4a">Assemble</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a48ce7e0631521cc72c564be088917148">AssembleImagRow</a>(int i)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a70fa643e503c063be37de261f89aab22">AssembleRealRow</a>(int i)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0ee918c8c04a1a9434e157c15fdeb596">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a16289d6a8f38a4303f75113667f8af74">ClearImagRow</a>(int i)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a480f759c9bdd3e69206f18d81ed44f5a">ClearRealRow</a>(int i)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>entry_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a1fce8c713bc87ca4e06df819ced39554">Fill</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a08a5f1de809bbb4565e90d954ca053f6">Fill</a>(const complex&lt; T0 &gt; &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad1ff9883f030ca9eaada43616865acdb">FillRand</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2e5c6c10af220eea29f9c1565b14a93f">GetDataSize</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a578281c64cbe05a542a6b0e9642b326a">GetImagData</a>(int i) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>GetImagData</b>() const (defined in <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad54321edab8e2633985e198f1a576340">GetImagDataSize</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0a423d7e93a0ad9c19ed6de2b1052dca">GetImagInd</a>(int i) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a33ebac042eda90796d7f674c250e85be">GetImagNonZeros</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a5477a0086e76fbee8db94fcfa1cd9703">GetImagRowSize</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821">GetM</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ab3c6aad66c9dc6ad9addfb7de9a4ebdd">GetM</a>(const SeldonTranspose &amp;status) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ae413915ed95545b40df8b43737fa43c1">GetN</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a53e20518a8557211c89ca8ad0d607825">GetN</a>(const SeldonTranspose &amp;status) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aaa912365c7b8c191cf8b3fb2e2da4789">GetRealData</a>(int i) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>GetRealData</b>() const (defined in <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa40775992c738c7fd1e9543bfddb2694">GetRealDataSize</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#afae45969eda952b43748cf7756f0e6d6">GetRealInd</a>(int i) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a11efc1bee6a95da705238628d7e3d958">GetRealNonZeros</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a794411e243ead33b91cb750921602cdd">GetRealRowSize</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aedba64fa9b01bce6bb75e9e5a14b26c5">IndexImag</a>(int num_row, int i) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a011b15c1909f712a693de10f2c0c2328">IndexImag</a>(int num_row, int i)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2a3cd76154ac61a43afa42563805ce3b">IndexReal</a>(int num_row, int i) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2fdb9482dad6378fb8ced1982b6860b1">IndexReal</a>(int num_row, int i)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de">m_</a></td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#ab98aecad772f00d59ea17248a04720d3">Matrix</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a8bcc8c567a536ff51cba0d9afce34507">Matrix</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a415bb8c2cb2379647933ddc5d9dbcc32">Matrix_ArrayComplexSparse</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a410f415f4976918fc7678bc770611043">Matrix_ArrayComplexSparse</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98">n_</a></td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad5e87ca37baabfb8cdc6f841f5125d7d">NullifyImag</a>(int i)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#adbdb03e02fd8617a23188d6bc3155ec0">NullifyImag</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a9f0922d865cdd4a627912583a9c73151">NullifyReal</a>(int i)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aed1192a75fe2c7d19e545ff6054298e2">NullifyReal</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a30be6306dbdd79b0a282ef99289b0c2b">operator()</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a95e0c06209ddfeea67740ecb9c0c1451">operator=</a>(const complex&lt; T0 &gt; &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ac24abb2e047eb8b91728023074b3fb13">Print</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#aebbf5322f0709878e15c379391a8aec8">PrintImagRow</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#aeeca29af2514e61e003925875e7f7c33">PrintRealRow</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>property</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad2348fb25206ef8516c7e835b7ce2287">Reallocate</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#aa798b00f5793c414777a5e3b421095a0">ReallocateImagRow</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#aefd387926aad7c3609a9f6bc062252e0">ReallocateRealRow</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>RemoveSmallEntry</b>(const T0 &amp;epsilon) (defined in <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a3e6d41ebaccab9d346d6fd291e28ff5d">ReplaceImagIndexRow</a>(int i, IVect &amp;new_index)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a350b6c00041f9e9db6bf111ac431bdf9">ReplaceRealIndexRow</a>(int i, IVect &amp;new_index)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a9166689334cc2f731220f76b730d692f">Resize</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#af89faca49416df1ad0b4262d440adec9">ResizeImagRow</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#ac668aac85ed864892bd3f4209ced4cf2">ResizeRealRow</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a19b48691b03953dffd387ace5cfddd37">SetIdentity</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a8e3c2dfa0ea5aa57cd8607cca15562f2">SetImagData</a>(int, int, Vector&lt; T, VectSparse, Allocator &gt; *)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a50622f29a8e112821c68e5a28236258f">SetImagData</a>(int, int, T *, int *)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aab6c31240cd6dcbd442f9a29658f51ea">SetRealData</a>(int, int, Vector&lt; T, VectSparse, Allocator &gt; *)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a1ebb3dcbf317ec99e6463f0aafea10b9">SetRealData</a>(int, int, T *, int *)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>storage</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a4b6b31fa7468c32bddf17fd1a22888ae">SwapImagRow</a>(int i, int i_)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a8ae984b95031fd72c6ecab86418d60ac">SwapRealRow</a>(int i, int i_)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7eb05d6902b13cfb7cc4f17093fdc17e">Val</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a994fbc16dd258d1609f7c7c582205e9f">Val</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8">val_imag_</a></td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6">val_real_</a></td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>value_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa2d9a2e69a269bf9d15b62d520cb560d">ValueImag</a>(int num_row, int i) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ab410f72a305938081fbc07e868eb161b">ValueImag</a>(int num_row, int i)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a041c76033f939293aa871f2fe45ef22a">ValueReal</a>(int num_row, int i) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a11fe8648ddd3e6e339eb81735d7e544e">ValueReal</a>(int num_row, int i)</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7095c684ecdb9ef6e9fe9b84462e22ce">WriteText</a>(string FileName) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a884e62c07aa29b59b259d232f7771c94">WriteText</a>(ostream &amp;FileStream) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa6f51cb3c7779c309578058fd34c5d25">Zero</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#acec6f5d4aa6488c9389a2065944568b0">~Matrix_ArrayComplexSparse</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a></td><td></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
