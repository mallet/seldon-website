<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>, including all inherited members.<table>
  <tr bgcolor="#f0f0f0"><td><b>access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a6ede24fc389f58952986463da449bfb0">AddInteraction</a>(int i, int j, const T &amp;val)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>allocator_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#ab63b8d9aee1a38054cd525d7fa708f1a">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a54604e7e696124d278a4aa198edcab77">Copy</a>(const Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>data_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>entry_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#abe41180630623f201ea254e009931abd">Fill</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#aa2721edc471c0603919afa9365bee419">Fill</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a27b33a9915ce2922d2f3f0a0270b467f">FillRand</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#aac6b06ab623602d24a80d12adf495cf2">GetDataSize</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a1378a5e93b30065896f0d84b07b18caa">GetInd</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a7b136934dfbe364dd016ac33e4a5dcc6">GetIndSize</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">GetM</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">GetN</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a0a0f412562d73bb7c9361da17565e7fb">GetNonZeros</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a9126980f0a760e8a73c2d0f480c69342">GetPtr</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a0c3409f0fec69fb357c493154e5d763b">GetPtrSize</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">GetSize</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>ind_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>m_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a47988d7972247286332e853c13bbc00b">Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ad2ea11b0880dc32ba9472da9310c332b">Matrix_Base</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline, explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a127e0229931486cea3e6007988b446a0">Matrix_Base</a>(const Matrix_Base&lt; T, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#af4624e0ed00838ec34b030103f9dd383">Matrix_Sparse</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a71e366ab3e3623e8f9a27fd7796621b4">Matrix_Sparse</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#af29835daf0a5da91592c9f9bcf7ce1a7">Matrix_Sparse</a>(int i, int j, int nz)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a2ff242fb46f84f041e4709a7eff70cd3">Matrix_Sparse</a>(int i, int j, Vector&lt; T, Storage0, Allocator0 &gt; &amp;values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;ind)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a49f2012fc254ff9e960b1dfee766ce0f">Matrix_Sparse</a>(const Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>n_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a805231f8ec04efc5a081b21c9f65d5c9">Nullify</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>nz_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a019d20b720889f75c4c2314b40e86954">operator()</a>(int i, int j) const </td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a58f047d2d62a3e5e7f7feaae98cd7d7f">operator=</a>(const Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a8d6d48b6f0277f1df2d52b798975407e">Print</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>ptr_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a5b411ce16d320a1442588af33adefe09">SetData</a>(int i, int j, Vector&lt; T, Storage0, Allocator0 &gt; &amp;values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;ind)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SetData</b>(int i, int j, int nz, pointer values, int *ptr, int *ind) (defined in <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#acf57d5d2ab0730c882b331ce89d4768a">SetData</a>(int i, int j, int nz, typename Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;::pointer values, int *ptr, int *ind)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a657c33784aee4b940c6a635c33f2eb5e">SetIdentity</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a7f416147b6680860cd489dfa6cb08b58">Val</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a508d4a87841d185e00cbac5223dfc91f">Val</a>(int i, int j) const </td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>value_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a45ac6acb158f9b49af3d50a2da34b708">WriteText</a>(string FileName) const </td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a3afe060ada2f12888f0b0121940df392">WriteText</a>(ostream &amp;FileStream) const </td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a812985d3c66421787a6dbe687620ea1a">Zero</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af34a03c0cc56f757a83cd56f97c74ed8">~Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sparse.php#a805ce704fffb3c509d908ec3ac45db28">~Matrix_Sparse</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sparse.php">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
