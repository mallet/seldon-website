<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix/Matrix_Hermitian.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_HERMITIAN_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;Matrix_Hermitian.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="keyword">namespace </span>Seldon
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 
<a name="l00029"></a>00029   <span class="comment">/****************</span>
<a name="l00030"></a>00030 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00031"></a>00031 <span class="comment">   ****************/</span>
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00035"></a>00035 
<a name="l00038"></a>00038   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00039"></a>00039   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a17c1f964ce877a9488b17b834f9e1216" title="Default constructor.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::Matrix_Hermitian</a>():
<a name="l00040"></a>00040     Matrix_Base&lt;T, Allocator&gt;()
<a name="l00041"></a>00041   {
<a name="l00042"></a>00042     me_ = NULL;
<a name="l00043"></a>00043   }
<a name="l00044"></a>00044 
<a name="l00045"></a>00045 
<a name="l00047"></a>00047 
<a name="l00052"></a><a class="code" href="class_seldon_1_1_matrix___hermitian.php#a881b798874606009cbbb87661ec621d8">00052</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00053"></a>00053   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a17c1f964ce877a9488b17b834f9e1216" title="Default constructor.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00054"></a>00054 <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a17c1f964ce877a9488b17b834f9e1216" title="Default constructor.">  ::Matrix_Hermitian</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l00055"></a>00055     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, i)
<a name="l00056"></a>00056   {
<a name="l00057"></a>00057 
<a name="l00058"></a>00058 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00059"></a>00059 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00060"></a>00060       {
<a name="l00061"></a>00061 <span class="preprocessor">#endif</span>
<a name="l00062"></a>00062 <span class="preprocessor"></span>
<a name="l00063"></a>00063         me_ = <span class="keyword">reinterpret_cast&lt;</span>pointer*<span class="keyword">&gt;</span>(calloc(i, <span class="keyword">sizeof</span>(pointer)));
<a name="l00064"></a>00064 
<a name="l00065"></a>00065 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00066"></a>00066 <span class="preprocessor"></span>      }
<a name="l00067"></a>00067     <span class="keywordflow">catch</span> (...)
<a name="l00068"></a>00068       {
<a name="l00069"></a>00069         this-&gt;m_ = 0;
<a name="l00070"></a>00070         this-&gt;n_ = 0;
<a name="l00071"></a>00071         me_ = NULL;
<a name="l00072"></a>00072         this-&gt;data_ = NULL;
<a name="l00073"></a>00073       }
<a name="l00074"></a>00074     <span class="keywordflow">if</span> (me_ == NULL)
<a name="l00075"></a>00075       {
<a name="l00076"></a>00076         this-&gt;m_ = 0;
<a name="l00077"></a>00077         this-&gt;n_ = 0;
<a name="l00078"></a>00078         this-&gt;data_ = NULL;
<a name="l00079"></a>00079       }
<a name="l00080"></a>00080     <span class="keywordflow">if</span> ((me_ == NULL) &amp;&amp; (i != 0))
<a name="l00081"></a>00081       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Hermitian::Matrix_Hermitian(int, int)&quot;</span>,
<a name="l00082"></a>00082                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate memory for a matrix of size &quot;</span>)
<a name="l00083"></a>00083                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(static_cast&lt;long int&gt;(i)
<a name="l00084"></a>00084                               * static_cast&lt;long int&gt;(i)
<a name="l00085"></a>00085                               * static_cast&lt;long int&gt;(<span class="keyword">sizeof</span>(T)))
<a name="l00086"></a>00086                      + <span class="stringliteral">&quot; bytes (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; x &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i)
<a name="l00087"></a>00087                      + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00088"></a>00088 <span class="preprocessor">#endif</span>
<a name="l00089"></a>00089 <span class="preprocessor"></span>
<a name="l00090"></a>00090 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00091"></a>00091 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00092"></a>00092       {
<a name="l00093"></a>00093 <span class="preprocessor">#endif</span>
<a name="l00094"></a>00094 <span class="preprocessor"></span>
<a name="l00095"></a>00095         this-&gt;data_ = this-&gt;allocator_.allocate(i * i, <span class="keyword">this</span>);
<a name="l00096"></a>00096 
<a name="l00097"></a>00097 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00098"></a>00098 <span class="preprocessor"></span>      }
<a name="l00099"></a>00099     <span class="keywordflow">catch</span> (...)
<a name="l00100"></a>00100       {
<a name="l00101"></a>00101         this-&gt;m_ = 0;
<a name="l00102"></a>00102         this-&gt;n_ = 0;
<a name="l00103"></a>00103         free(me_);
<a name="l00104"></a>00104         me_ = NULL;
<a name="l00105"></a>00105         this-&gt;data_ = NULL;
<a name="l00106"></a>00106       }
<a name="l00107"></a>00107     <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00108"></a>00108       {
<a name="l00109"></a>00109         this-&gt;m_ = 0;
<a name="l00110"></a>00110         this-&gt;n_ = 0;
<a name="l00111"></a>00111         free(me_);
<a name="l00112"></a>00112         me_ = NULL;
<a name="l00113"></a>00113       }
<a name="l00114"></a>00114     <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; i != 0)
<a name="l00115"></a>00115       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Hermitian::Matrix_Hermitian(int, int)&quot;</span>,
<a name="l00116"></a>00116                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate memory for a matrix of size &quot;</span>)
<a name="l00117"></a>00117                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(static_cast&lt;long int&gt;(i)
<a name="l00118"></a>00118                               * static_cast&lt;long int&gt;(i)
<a name="l00119"></a>00119                               * static_cast&lt;long int&gt;(<span class="keyword">sizeof</span>(T)))
<a name="l00120"></a>00120                      + <span class="stringliteral">&quot; bytes (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; x &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i)
<a name="l00121"></a>00121                      + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00122"></a>00122 <span class="preprocessor">#endif</span>
<a name="l00123"></a>00123 <span class="preprocessor"></span>
<a name="l00124"></a>00124     pointer ptr = this-&gt;data_;
<a name="l00125"></a>00125     <span class="keywordtype">int</span> lgth = i;
<a name="l00126"></a>00126     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; i; k++, ptr += lgth)
<a name="l00127"></a>00127       me_[k] = ptr;
<a name="l00128"></a>00128   }
<a name="l00129"></a>00129 
<a name="l00130"></a>00130 
<a name="l00132"></a><a class="code" href="class_seldon_1_1_matrix___hermitian.php#ad840eff3e3a2591a175e7bfbdb5fccbc">00132</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00133"></a>00133   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a17c1f964ce877a9488b17b834f9e1216" title="Default constructor.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00134"></a>00134 <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a17c1f964ce877a9488b17b834f9e1216" title="Default constructor.">  ::Matrix_Hermitian</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php" title="Hermitian matrix stored in a full matrix.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A):
<a name="l00135"></a>00135     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;()
<a name="l00136"></a>00136   {
<a name="l00137"></a>00137     this-&gt;m_ = 0;
<a name="l00138"></a>00138     this-&gt;n_ = 0;
<a name="l00139"></a>00139     this-&gt;data_ = NULL;
<a name="l00140"></a>00140     this-&gt;me_ = NULL;
<a name="l00141"></a>00141 
<a name="l00142"></a>00142     this-&gt;Copy(A);
<a name="l00143"></a>00143   }
<a name="l00144"></a>00144 
<a name="l00145"></a>00145 
<a name="l00146"></a>00146   <span class="comment">/**************</span>
<a name="l00147"></a>00147 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00148"></a>00148 <span class="comment">   **************/</span>
<a name="l00149"></a>00149 
<a name="l00150"></a>00150 
<a name="l00152"></a>00152   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00153"></a>00153   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#aa31430150e01ad4acc8b2a8e5e55e5c3" title="Destructor.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::~Matrix_Hermitian</a>()
<a name="l00154"></a>00154   {
<a name="l00155"></a>00155 
<a name="l00156"></a>00156 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00157"></a>00157 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00158"></a>00158       {
<a name="l00159"></a>00159 <span class="preprocessor">#endif</span>
<a name="l00160"></a>00160 <span class="preprocessor"></span>
<a name="l00161"></a>00161         <span class="keywordflow">if</span> (this-&gt;data_ != NULL)
<a name="l00162"></a>00162           {
<a name="l00163"></a>00163             this-&gt;allocator_.deallocate(this-&gt;data_, this-&gt;m_ * this-&gt;n_);
<a name="l00164"></a>00164             this-&gt;data_ = NULL;
<a name="l00165"></a>00165           }
<a name="l00166"></a>00166 
<a name="l00167"></a>00167 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00168"></a>00168 <span class="preprocessor"></span>      }
<a name="l00169"></a>00169     <span class="keywordflow">catch</span> (...)
<a name="l00170"></a>00170       {
<a name="l00171"></a>00171         this-&gt;data_ = NULL;
<a name="l00172"></a>00172       }
<a name="l00173"></a>00173 <span class="preprocessor">#endif</span>
<a name="l00174"></a>00174 <span class="preprocessor"></span>
<a name="l00175"></a>00175 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00176"></a>00176 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00177"></a>00177       {
<a name="l00178"></a>00178 <span class="preprocessor">#endif</span>
<a name="l00179"></a>00179 <span class="preprocessor"></span>
<a name="l00180"></a>00180         <span class="keywordflow">if</span> (me_ != NULL)
<a name="l00181"></a>00181           {
<a name="l00182"></a>00182             free(me_);
<a name="l00183"></a>00183             me_ = NULL;
<a name="l00184"></a>00184           }
<a name="l00185"></a>00185 
<a name="l00186"></a>00186 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00187"></a>00187 <span class="preprocessor"></span>      }
<a name="l00188"></a>00188     <span class="keywordflow">catch</span> (...)
<a name="l00189"></a>00189       {
<a name="l00190"></a>00190         this-&gt;m_ = 0;
<a name="l00191"></a>00191         this-&gt;n_ = 0;
<a name="l00192"></a>00192         me_ = NULL;
<a name="l00193"></a>00193       }
<a name="l00194"></a>00194 <span class="preprocessor">#endif</span>
<a name="l00195"></a>00195 <span class="preprocessor"></span>
<a name="l00196"></a>00196   }
<a name="l00197"></a>00197 
<a name="l00198"></a>00198 
<a name="l00200"></a>00200 
<a name="l00204"></a>00204   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00205"></a>00205   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a977f6c67028b64777aef5d414e115ee2" title="Clears the matrix.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::Clear</a>()
<a name="l00206"></a>00206   {
<a name="l00207"></a>00207     this-&gt;<a class="code" href="class_seldon_1_1_matrix___hermitian.php#aa31430150e01ad4acc8b2a8e5e55e5c3" title="Destructor.">~Matrix_Hermitian</a>();
<a name="l00208"></a>00208     this-&gt;m_ = 0;
<a name="l00209"></a>00209     this-&gt;n_ = 0;
<a name="l00210"></a>00210   }
<a name="l00211"></a>00211 
<a name="l00212"></a>00212 
<a name="l00213"></a>00213   <span class="comment">/*******************</span>
<a name="l00214"></a>00214 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00215"></a>00215 <span class="comment">   *******************/</span>
<a name="l00216"></a>00216 
<a name="l00217"></a>00217 
<a name="l00219"></a>00219 
<a name="l00225"></a>00225   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00226"></a>00226   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a0ce699921fe6da8089f6708fbbfc959a" title="Returns the number of elements stored in memory.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::GetDataSize</a>()<span class="keyword"> const</span>
<a name="l00227"></a>00227 <span class="keyword">  </span>{
<a name="l00228"></a>00228     <span class="keywordflow">return</span> this-&gt;m_ * this-&gt;n_;
<a name="l00229"></a>00229   }
<a name="l00230"></a>00230 
<a name="l00231"></a>00231 
<a name="l00232"></a>00232   <span class="comment">/*********************</span>
<a name="l00233"></a>00233 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00234"></a>00234 <span class="comment">   *********************/</span>
<a name="l00235"></a>00235 
<a name="l00236"></a>00236 
<a name="l00238"></a>00238 
<a name="l00244"></a><a class="code" href="class_seldon_1_1_matrix___hermitian.php#a87adcb1dbffb11e49fb698c6c5c9b7d7">00244</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00245"></a>00245   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a87adcb1dbffb11e49fb698c6c5c9b7d7" title="Reallocates memory to resize the matrix.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00246"></a>00246 <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a87adcb1dbffb11e49fb698c6c5c9b7d7" title="Reallocates memory to resize the matrix.">  ::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00247"></a>00247   {
<a name="l00248"></a>00248 
<a name="l00249"></a>00249     <span class="keywordflow">if</span> (i != this-&gt;m_)
<a name="l00250"></a>00250       {
<a name="l00251"></a>00251         this-&gt;m_ = i;
<a name="l00252"></a>00252         this-&gt;n_ = i;
<a name="l00253"></a>00253 
<a name="l00254"></a>00254 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00255"></a>00255 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l00256"></a>00256           {
<a name="l00257"></a>00257 <span class="preprocessor">#endif</span>
<a name="l00258"></a>00258 <span class="preprocessor"></span>
<a name="l00259"></a>00259             me_ = <span class="keyword">reinterpret_cast&lt;</span>pointer*<span class="keyword">&gt;</span>( realloc(me_,
<a name="l00260"></a>00260                                                       i * <span class="keyword">sizeof</span>(pointer)) );
<a name="l00261"></a>00261 
<a name="l00262"></a>00262 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00263"></a>00263 <span class="preprocessor"></span>          }
<a name="l00264"></a>00264         <span class="keywordflow">catch</span> (...)
<a name="l00265"></a>00265           {
<a name="l00266"></a>00266             this-&gt;m_ = 0;
<a name="l00267"></a>00267             this-&gt;n_ = 0;
<a name="l00268"></a>00268             me_ = NULL;
<a name="l00269"></a>00269             this-&gt;data_ = NULL;
<a name="l00270"></a>00270           }
<a name="l00271"></a>00271         <span class="keywordflow">if</span> (me_ == NULL)
<a name="l00272"></a>00272           {
<a name="l00273"></a>00273             this-&gt;m_ = 0;
<a name="l00274"></a>00274             this-&gt;n_ = 0;
<a name="l00275"></a>00275             this-&gt;data_ = NULL;
<a name="l00276"></a>00276           }
<a name="l00277"></a>00277         <span class="keywordflow">if</span> (me_ == NULL &amp;&amp; i != 0)
<a name="l00278"></a>00278           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Hermitian::Reallocate(int, int)&quot;</span>,
<a name="l00279"></a>00279                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to reallocate memory&quot;</span>)
<a name="l00280"></a>00280                          + <span class="stringliteral">&quot; for a matrix of size &quot;</span>
<a name="l00281"></a>00281                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(static_cast&lt;long int&gt;(i)
<a name="l00282"></a>00282                                   * static_cast&lt;long int&gt;(i)
<a name="l00283"></a>00283                                   * static_cast&lt;long int&gt;(<span class="keyword">sizeof</span>(T)))
<a name="l00284"></a>00284                          + <span class="stringliteral">&quot; bytes (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; x &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i)
<a name="l00285"></a>00285                          + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00286"></a>00286 <span class="preprocessor">#endif</span>
<a name="l00287"></a>00287 <span class="preprocessor"></span>
<a name="l00288"></a>00288 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00289"></a>00289 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l00290"></a>00290           {
<a name="l00291"></a>00291 <span class="preprocessor">#endif</span>
<a name="l00292"></a>00292 <span class="preprocessor"></span>
<a name="l00293"></a>00293             this-&gt;data_ =
<a name="l00294"></a>00294               <span class="keyword">reinterpret_cast&lt;</span>pointer<span class="keyword">&gt;</span>(this-&gt;allocator_.reallocate(this-&gt;data_,
<a name="l00295"></a>00295                                                                     i * i,
<a name="l00296"></a>00296                                                                     <span class="keyword">this</span>));
<a name="l00297"></a>00297 
<a name="l00298"></a>00298 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00299"></a>00299 <span class="preprocessor"></span>          }
<a name="l00300"></a>00300         <span class="keywordflow">catch</span> (...)
<a name="l00301"></a>00301           {
<a name="l00302"></a>00302             this-&gt;m_ = 0;
<a name="l00303"></a>00303             this-&gt;n_ = 0;
<a name="l00304"></a>00304             free(me_);
<a name="l00305"></a>00305             me_ = NULL;
<a name="l00306"></a>00306             this-&gt;data_ = NULL;
<a name="l00307"></a>00307           }
<a name="l00308"></a>00308         <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00309"></a>00309           {
<a name="l00310"></a>00310             this-&gt;m_ = 0;
<a name="l00311"></a>00311             this-&gt;n_ = 0;
<a name="l00312"></a>00312             free(me_);
<a name="l00313"></a>00313             me_ = NULL;
<a name="l00314"></a>00314           }
<a name="l00315"></a>00315         <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; i != 0)
<a name="l00316"></a>00316           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Hermitian::Reallocate(int, int)&quot;</span>,
<a name="l00317"></a>00317                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to reallocate memory&quot;</span>)
<a name="l00318"></a>00318                          + <span class="stringliteral">&quot; for a matrix of size &quot;</span>
<a name="l00319"></a>00319                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(static_cast&lt;long int&gt;(i)
<a name="l00320"></a>00320                                   * static_cast&lt;long int&gt;(i)
<a name="l00321"></a>00321                                   * static_cast&lt;long int&gt;(<span class="keyword">sizeof</span>(T)))
<a name="l00322"></a>00322                          + <span class="stringliteral">&quot; bytes (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; x &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i)
<a name="l00323"></a>00323                          + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00324"></a>00324 <span class="preprocessor">#endif</span>
<a name="l00325"></a>00325 <span class="preprocessor"></span>
<a name="l00326"></a>00326         pointer ptr = this-&gt;data_;
<a name="l00327"></a>00327         <span class="keywordtype">int</span> lgth = Storage::GetSecond(i, i);
<a name="l00328"></a>00328         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; Storage::GetFirst(i, i); k++, ptr += lgth)
<a name="l00329"></a>00329           me_[k] = ptr;
<a name="l00330"></a>00330       }
<a name="l00331"></a>00331   }
<a name="l00332"></a>00332 
<a name="l00333"></a>00333 
<a name="l00336"></a>00336 
<a name="l00350"></a>00350   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00351"></a>00351   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php" title="Hermitian matrix stored in a full matrix.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00352"></a>00352 <a class="code" href="class_seldon_1_1_matrix___hermitian.php" title="Hermitian matrix stored in a full matrix.">  ::SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00353"></a>00353             <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php" title="Hermitian matrix stored in a full matrix.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00354"></a>00354             ::pointer data)
<a name="l00355"></a>00355   {
<a name="l00356"></a>00356     this-&gt;Clear();
<a name="l00357"></a>00357 
<a name="l00358"></a>00358     this-&gt;m_ = i;
<a name="l00359"></a>00359     this-&gt;n_ = i;
<a name="l00360"></a>00360 
<a name="l00361"></a>00361 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00362"></a>00362 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00363"></a>00363       {
<a name="l00364"></a>00364 <span class="preprocessor">#endif</span>
<a name="l00365"></a>00365 <span class="preprocessor"></span>
<a name="l00366"></a>00366         me_ = <span class="keyword">reinterpret_cast&lt;</span>pointer*<span class="keyword">&gt;</span>(calloc(i, <span class="keyword">sizeof</span>(pointer)));
<a name="l00367"></a>00367 
<a name="l00368"></a>00368 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00369"></a>00369 <span class="preprocessor"></span>      }
<a name="l00370"></a>00370     <span class="keywordflow">catch</span> (...)
<a name="l00371"></a>00371       {
<a name="l00372"></a>00372         this-&gt;m_ = 0;
<a name="l00373"></a>00373         this-&gt;n_ = 0;
<a name="l00374"></a>00374         me_ = NULL;
<a name="l00375"></a>00375         this-&gt;data_ = NULL;
<a name="l00376"></a>00376         <span class="keywordflow">return</span>;
<a name="l00377"></a>00377       }
<a name="l00378"></a>00378     <span class="keywordflow">if</span> (me_ == NULL)
<a name="l00379"></a>00379       {
<a name="l00380"></a>00380         this-&gt;m_ = 0;
<a name="l00381"></a>00381         this-&gt;n_ = 0;
<a name="l00382"></a>00382         this-&gt;data_ = NULL;
<a name="l00383"></a>00383         <span class="keywordflow">return</span>;
<a name="l00384"></a>00384       }
<a name="l00385"></a>00385 <span class="preprocessor">#endif</span>
<a name="l00386"></a>00386 <span class="preprocessor"></span>
<a name="l00387"></a>00387     this-&gt;data_ = data;
<a name="l00388"></a>00388 
<a name="l00389"></a>00389     pointer ptr = this-&gt;data_;
<a name="l00390"></a>00390     <span class="keywordtype">int</span> lgth = i;
<a name="l00391"></a>00391     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; i; k++, ptr += lgth)
<a name="l00392"></a>00392       me_[k] = ptr;
<a name="l00393"></a>00393   }
<a name="l00394"></a>00394 
<a name="l00395"></a>00395 
<a name="l00397"></a>00397 
<a name="l00402"></a>00402   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00403"></a>00403   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a1e831af1188b11a36874088712dd971a" title="Clears the matrix without releasing memory.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::Nullify</a>()
<a name="l00404"></a>00404   {
<a name="l00405"></a>00405     this-&gt;m_ = 0;
<a name="l00406"></a>00406     this-&gt;n_ = 0;
<a name="l00407"></a>00407 
<a name="l00408"></a>00408 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00409"></a>00409 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00410"></a>00410       {
<a name="l00411"></a>00411 <span class="preprocessor">#endif</span>
<a name="l00412"></a>00412 <span class="preprocessor"></span>
<a name="l00413"></a>00413         <span class="keywordflow">if</span> (me_ != NULL)
<a name="l00414"></a>00414           {
<a name="l00415"></a>00415             free(me_);
<a name="l00416"></a>00416             me_ = NULL;
<a name="l00417"></a>00417           }
<a name="l00418"></a>00418 
<a name="l00419"></a>00419 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00420"></a>00420 <span class="preprocessor"></span>      }
<a name="l00421"></a>00421     <span class="keywordflow">catch</span> (...)
<a name="l00422"></a>00422       {
<a name="l00423"></a>00423         this-&gt;m_ = 0;
<a name="l00424"></a>00424         this-&gt;n_ = 0;
<a name="l00425"></a>00425         me_ = NULL;
<a name="l00426"></a>00426       }
<a name="l00427"></a>00427 <span class="preprocessor">#endif</span>
<a name="l00428"></a>00428 <span class="preprocessor"></span>
<a name="l00429"></a>00429     this-&gt;data_ = NULL;
<a name="l00430"></a>00430   }
<a name="l00431"></a>00431 
<a name="l00432"></a>00432 
<a name="l00434"></a>00434 
<a name="l00441"></a><a class="code" href="class_seldon_1_1_matrix___hermitian.php#a6fe439e0f65eb7acd72730b4853e5e91">00441</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00442"></a>00442   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a6fe439e0f65eb7acd72730b4853e5e91" title="Reallocates memory to resize the matrix and keeps previous entries.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00443"></a>00443 <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a6fe439e0f65eb7acd72730b4853e5e91" title="Reallocates memory to resize the matrix and keeps previous entries.">  ::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00444"></a>00444   {
<a name="l00445"></a>00445 
<a name="l00446"></a>00446     <span class="comment">// Storing the old values of the matrix.</span>
<a name="l00447"></a>00447     <span class="keywordtype">int</span> iold = Storage::GetFirst(this-&gt;m_, this-&gt;n_);
<a name="l00448"></a>00448     <span class="keywordtype">int</span> jold = Storage::GetSecond(this-&gt;m_, this-&gt;n_);
<a name="l00449"></a>00449     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;value_type, VectFull, Allocator&gt;</a> xold(this-&gt;GetDataSize());
<a name="l00450"></a>00450     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; this-&gt;GetDataSize(); k++)
<a name="l00451"></a>00451       xold(k) = this-&gt;data_[k];
<a name="l00452"></a>00452 
<a name="l00453"></a>00453     <span class="comment">// Reallocation.</span>
<a name="l00454"></a>00454     <span class="keywordtype">int</span> inew = Storage::GetFirst(i, j);
<a name="l00455"></a>00455     <span class="keywordtype">int</span> jnew = Storage::GetSecond(i, j);
<a name="l00456"></a>00456     this-&gt;Reallocate(i, j);
<a name="l00457"></a>00457 
<a name="l00458"></a>00458     <span class="comment">// Filling the matrix with its old values.</span>
<a name="l00459"></a>00459     <span class="keywordtype">int</span> imin = min(iold, inew), jmin = min(jold, jnew);
<a name="l00460"></a>00460     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; imin; k++)
<a name="l00461"></a>00461       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> l = 0; l &lt; jmin; l++)
<a name="l00462"></a>00462         this-&gt;data_[k*jnew+l] = xold(l+jold*k);
<a name="l00463"></a>00463   }
<a name="l00464"></a>00464 
<a name="l00465"></a>00465 
<a name="l00466"></a>00466   <span class="comment">/**********************************</span>
<a name="l00467"></a>00467 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l00468"></a>00468 <span class="comment">   **********************************/</span>
<a name="l00469"></a>00469 
<a name="l00470"></a>00470 
<a name="l00472"></a>00472 
<a name="l00478"></a><a class="code" href="class_seldon_1_1_matrix___hermitian.php#a17491e8cc9822f3ff66fb23e8a42a7e4">00478</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00479"></a>00479   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php" title="Hermitian matrix stored in a full matrix.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::value_type</a>
<a name="l00480"></a>00480   <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a17491e8cc9822f3ff66fb23e8a42a7e4" title="Access operator.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00481"></a>00481   {
<a name="l00482"></a>00482 
<a name="l00483"></a>00483 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00484"></a>00484 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00485"></a>00485       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Hermitian::operator()&quot;</span>,
<a name="l00486"></a>00486                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00487"></a>00487                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00488"></a>00488     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00489"></a>00489       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Hermitian::operator()&quot;</span>,
<a name="l00490"></a>00490                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00491"></a>00491                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00492"></a>00492 <span class="preprocessor">#endif</span>
<a name="l00493"></a>00493 <span class="preprocessor"></span>
<a name="l00494"></a>00494     <span class="keywordflow">if</span> (i &gt; j)
<a name="l00495"></a>00495       <span class="keywordflow">return</span> conj(me_[Storage::GetSecond(i, j)][Storage::GetFirst(i, j)]);
<a name="l00496"></a>00496     <span class="keywordflow">else</span>
<a name="l00497"></a>00497       <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00498"></a>00498   }
<a name="l00499"></a>00499 
<a name="l00500"></a>00500 
<a name="l00502"></a>00502 
<a name="l00508"></a>00508   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00509"></a><a class="code" href="class_seldon_1_1_matrix___hermitian.php#a520c43fa72c7b7e550dbb3bdac184f59">00509</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php" title="Hermitian matrix stored in a full matrix.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::value_type</a>
<a name="l00510"></a>00510   <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a17491e8cc9822f3ff66fb23e8a42a7e4" title="Access operator.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00511"></a>00511 <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a17491e8cc9822f3ff66fb23e8a42a7e4" title="Access operator.">  ::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00512"></a>00512 <span class="keyword">  </span>{
<a name="l00513"></a>00513 
<a name="l00514"></a>00514 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00515"></a>00515 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00516"></a>00516       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Hermitian::operator() const&quot;</span>,
<a name="l00517"></a>00517                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00518"></a>00518                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00519"></a>00519     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00520"></a>00520       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Hermitian::operator() const&quot;</span>,
<a name="l00521"></a>00521                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00522"></a>00522                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00523"></a>00523 <span class="preprocessor">#endif</span>
<a name="l00524"></a>00524 <span class="preprocessor"></span>
<a name="l00525"></a>00525     <span class="keywordflow">if</span> (i &gt; j)
<a name="l00526"></a>00526       <span class="keywordflow">return</span> conj(me_[Storage::GetSecond(i, j)][Storage::GetFirst(i, j)]);
<a name="l00527"></a>00527     <span class="keywordflow">else</span>
<a name="l00528"></a>00528       <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00529"></a>00529 
<a name="l00530"></a>00530   }
<a name="l00531"></a>00531 
<a name="l00532"></a>00532 
<a name="l00534"></a>00534 
<a name="l00540"></a>00540   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00541"></a><a class="code" href="class_seldon_1_1_matrix___hermitian.php#a717f22a07d28b33751253ca330667b7c">00541</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php" title="Hermitian matrix stored in a full matrix.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00542"></a>00542   ::const_reference
<a name="l00543"></a>00543   <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a717f22a07d28b33751253ca330667b7c" title="Access operator.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00544"></a>00544 <span class="keyword">  </span>{
<a name="l00545"></a>00545 
<a name="l00546"></a>00546 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00547"></a>00547 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00548"></a>00548       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Hermitian::Val(int, int) const&quot;</span>,
<a name="l00549"></a>00549                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00550"></a>00550                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00551"></a>00551     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00552"></a>00552       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Hermitian::Val(int, int) const&quot;</span>,
<a name="l00553"></a>00553                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00554"></a>00554                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00555"></a>00555 <span class="preprocessor">#endif</span>
<a name="l00556"></a>00556 <span class="preprocessor"></span>
<a name="l00557"></a>00557     <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00558"></a>00558   }
<a name="l00559"></a>00559 
<a name="l00560"></a>00560 
<a name="l00562"></a>00562 
<a name="l00568"></a><a class="code" href="class_seldon_1_1_matrix___hermitian.php#a2745d5a40840fb7692a82de5352f5e4a">00568</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00569"></a>00569   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php" title="Hermitian matrix stored in a full matrix.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00570"></a>00570   <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a717f22a07d28b33751253ca330667b7c" title="Access operator.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00571"></a>00571   {
<a name="l00572"></a>00572 
<a name="l00573"></a>00573 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00574"></a>00574 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00575"></a>00575       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Hermitian::Val(int, int)&quot;</span>,
<a name="l00576"></a>00576                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00577"></a>00577                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00578"></a>00578     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00579"></a>00579       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Hermitian::Val(int, int)&quot;</span>,
<a name="l00580"></a>00580                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00581"></a>00581                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00582"></a>00582 <span class="preprocessor">#endif</span>
<a name="l00583"></a>00583 <span class="preprocessor"></span>
<a name="l00584"></a>00584     <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00585"></a>00585   }
<a name="l00586"></a>00586 
<a name="l00587"></a>00587 
<a name="l00589"></a>00589 
<a name="l00594"></a><a class="code" href="class_seldon_1_1_matrix___hermitian.php#a23f10cdfae40711e79c2be1e0c682f59">00594</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00595"></a>00595   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php" title="Hermitian matrix stored in a full matrix.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00596"></a>00596   <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a23f10cdfae40711e79c2be1e0c682f59" title="Access to elements of the data array.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::operator[] </a>(<span class="keywordtype">int</span> i)
<a name="l00597"></a>00597   {
<a name="l00598"></a>00598 
<a name="l00599"></a>00599 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00600"></a>00600 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___hermitian.php#a0ce699921fe6da8089f6708fbbfc959a" title="Returns the number of elements stored in memory.">GetDataSize</a>())
<a name="l00601"></a>00601       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Matrix_Hermitian::operator[] (int)&quot;</span>,
<a name="l00602"></a>00602                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00603"></a>00603                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___hermitian.php#a0ce699921fe6da8089f6708fbbfc959a" title="Returns the number of elements stored in memory.">GetDataSize</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00604"></a>00604                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00605"></a>00605 <span class="preprocessor">#endif</span>
<a name="l00606"></a>00606 <span class="preprocessor"></span>
<a name="l00607"></a>00607     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00608"></a>00608   }
<a name="l00609"></a>00609 
<a name="l00610"></a>00610 
<a name="l00612"></a>00612 
<a name="l00617"></a>00617   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00618"></a><a class="code" href="class_seldon_1_1_matrix___hermitian.php#a92663437cbb2ca113e78f461a4689879">00618</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php" title="Hermitian matrix stored in a full matrix.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00619"></a>00619   ::const_reference
<a name="l00620"></a>00620   <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a23f10cdfae40711e79c2be1e0c682f59" title="Access to elements of the data array.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::operator[] </a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00621"></a>00621 <span class="keyword">  </span>{
<a name="l00622"></a>00622 
<a name="l00623"></a>00623 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00624"></a>00624 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___hermitian.php#a0ce699921fe6da8089f6708fbbfc959a" title="Returns the number of elements stored in memory.">GetDataSize</a>())
<a name="l00625"></a>00625       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Matrix_Hermitian::operator[] (int) const&quot;</span>,
<a name="l00626"></a>00626                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00627"></a>00627                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___hermitian.php#a0ce699921fe6da8089f6708fbbfc959a" title="Returns the number of elements stored in memory.">GetDataSize</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00628"></a>00628                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00629"></a>00629 <span class="preprocessor">#endif</span>
<a name="l00630"></a>00630 <span class="preprocessor"></span>
<a name="l00631"></a>00631     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00632"></a>00632   }
<a name="l00633"></a>00633 
<a name="l00634"></a>00634 
<a name="l00636"></a>00636 
<a name="l00641"></a>00641   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00642"></a><a class="code" href="class_seldon_1_1_matrix___hermitian.php#acea882121d471497d730d032054e8c14">00642</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php" title="Hermitian matrix stored in a full matrix.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00643"></a>00643   <a class="code" href="class_seldon_1_1_matrix___hermitian.php#acea882121d471497d730d032054e8c14" title="Duplicates a matrix (assignement operator).">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00644"></a>00644 <a class="code" href="class_seldon_1_1_matrix___hermitian.php#acea882121d471497d730d032054e8c14" title="Duplicates a matrix (assignement operator).">  ::operator=</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php" title="Hermitian matrix stored in a full matrix.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00645"></a>00645   {
<a name="l00646"></a>00646     this-&gt;Copy(A);
<a name="l00647"></a>00647 
<a name="l00648"></a>00648     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00649"></a>00649   }
<a name="l00650"></a>00650 
<a name="l00651"></a>00651 
<a name="l00653"></a>00653 
<a name="l00658"></a><a class="code" href="class_seldon_1_1_matrix___hermitian.php#a9a424460d2d3c28f0ba2c208efc6420f">00658</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00659"></a>00659   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a9a424460d2d3c28f0ba2c208efc6420f" title="Duplicates a matrix.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00660"></a>00660 <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a9a424460d2d3c28f0ba2c208efc6420f" title="Duplicates a matrix.">  ::Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php" title="Hermitian matrix stored in a full matrix.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00661"></a>00661   {
<a name="l00662"></a>00662     this-&gt;Reallocate(A.<a class="code" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927" title="Returns the number of rows.">GetM</a>(), A.<a class="code" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984" title="Returns the number of columns.">GetN</a>());
<a name="l00663"></a>00663 
<a name="l00664"></a>00664     this-&gt;allocator_.memorycpy(this-&gt;data_, A.<a class="code" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a" title="Returns a pointer to the data array.">GetData</a>(), this-&gt;GetDataSize());
<a name="l00665"></a>00665   }
<a name="l00666"></a>00666 
<a name="l00667"></a>00667 
<a name="l00668"></a>00668   <span class="comment">/************************</span>
<a name="l00669"></a>00669 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l00670"></a>00670 <span class="comment">   ************************/</span>
<a name="l00671"></a>00671 
<a name="l00672"></a>00672 
<a name="l00674"></a>00674 
<a name="l00678"></a>00678   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00679"></a>00679   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#ae5de483eb3484823b7c74844bc6c9984" title="Sets all elements to zero.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::Zero</a>()
<a name="l00680"></a>00680   {
<a name="l00681"></a>00681     this-&gt;allocator_.memoryset(this-&gt;data_, <span class="keywordtype">char</span>(0),
<a name="l00682"></a>00682                                this-&gt;<a class="code" href="class_seldon_1_1_matrix___hermitian.php#a0ce699921fe6da8089f6708fbbfc959a" title="Returns the number of elements stored in memory.">GetDataSize</a>() * <span class="keyword">sizeof</span>(value_type));
<a name="l00683"></a>00683   }
<a name="l00684"></a>00684 
<a name="l00685"></a>00685 
<a name="l00687"></a>00687 
<a name="l00691"></a>00691   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00692"></a>00692   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#aa71e81ad230567c0624e278ae7e0a9cb" title="Sets the matrix to the identity.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::SetIdentity</a>()
<a name="l00693"></a>00693   {
<a name="l00694"></a>00694     this-&gt;<a class="code" href="class_seldon_1_1_matrix___hermitian.php#a86ddb1d1806dc0d7f51108210fb2c36f" title="Fills the matrix with 0, 1, 2, ...">Fill</a>(T(0));
<a name="l00695"></a>00695 
<a name="l00696"></a>00696     T one(1);
<a name="l00697"></a>00697     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; min(this-&gt;m_, this-&gt;n_); i++)
<a name="l00698"></a>00698       this-&gt;<a class="code" href="class_seldon_1_1_matrix___hermitian.php#a717f22a07d28b33751253ca330667b7c" title="Access operator.">Val</a>(i,i) = one;
<a name="l00699"></a>00699   }
<a name="l00700"></a>00700 
<a name="l00701"></a>00701 
<a name="l00703"></a>00703 
<a name="l00707"></a>00707   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00708"></a>00708   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a86ddb1d1806dc0d7f51108210fb2c36f" title="Fills the matrix with 0, 1, 2, ...">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::Fill</a>()
<a name="l00709"></a>00709   {
<a name="l00710"></a>00710     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___hermitian.php#a0ce699921fe6da8089f6708fbbfc959a" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00711"></a>00711       this-&gt;data_[i] = i;
<a name="l00712"></a>00712   }
<a name="l00713"></a>00713 
<a name="l00714"></a>00714 
<a name="l00716"></a>00716 
<a name="l00719"></a><a class="code" href="class_seldon_1_1_matrix___hermitian.php#ad4cc714bed5f5a7b2ab25c5530dc0e96">00719</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00720"></a>00720   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00721"></a>00721   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a86ddb1d1806dc0d7f51108210fb2c36f" title="Fills the matrix with 0, 1, 2, ...">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::Fill</a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00722"></a>00722   {
<a name="l00723"></a>00723     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___hermitian.php#a0ce699921fe6da8089f6708fbbfc959a" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00724"></a>00724       this-&gt;data_[i] = x;
<a name="l00725"></a>00725   }
<a name="l00726"></a>00726 
<a name="l00727"></a>00727 
<a name="l00729"></a>00729 
<a name="l00732"></a>00732   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00733"></a><a class="code" href="class_seldon_1_1_matrix___hermitian.php#abcc77ed81be43dc542e2804ac075df56">00733</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00734"></a>00734   <a class="code" href="class_seldon_1_1_matrix___hermitian.php" title="Hermitian matrix stored in a full matrix.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00735"></a>00735   <a class="code" href="class_seldon_1_1_matrix___hermitian.php#acea882121d471497d730d032054e8c14" title="Duplicates a matrix (assignement operator).">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00736"></a>00736   {
<a name="l00737"></a>00737     this-&gt;<a class="code" href="class_seldon_1_1_matrix___hermitian.php#a86ddb1d1806dc0d7f51108210fb2c36f" title="Fills the matrix with 0, 1, 2, ...">Fill</a>(x);
<a name="l00738"></a>00738 
<a name="l00739"></a>00739     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00740"></a>00740   }
<a name="l00741"></a>00741 
<a name="l00742"></a>00742 
<a name="l00744"></a>00744 
<a name="l00747"></a>00747   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00748"></a>00748   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a2e348d1dc81d5e2da41bd2f41bd1e7dd" title="Fills the matrix randomly.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::FillRand</a>()
<a name="l00749"></a>00749   {
<a name="l00750"></a>00750     srand(time(NULL));
<a name="l00751"></a>00751     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___hermitian.php#a0ce699921fe6da8089f6708fbbfc959a" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00752"></a>00752       this-&gt;data_[i] = rand();
<a name="l00753"></a>00753   }
<a name="l00754"></a>00754 
<a name="l00755"></a>00755 
<a name="l00757"></a>00757 
<a name="l00762"></a>00762   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00763"></a>00763   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a433db64775f698010e5f13c57fa17c8c" title="Displays the matrix on the standard output.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00764"></a>00764 <span class="keyword">  </span>{
<a name="l00765"></a>00765     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00766"></a>00766       {
<a name="l00767"></a>00767         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;n_; j++)
<a name="l00768"></a>00768           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00769"></a>00769         cout &lt;&lt; endl;
<a name="l00770"></a>00770       }
<a name="l00771"></a>00771   }
<a name="l00772"></a>00772 
<a name="l00773"></a>00773 
<a name="l00775"></a>00775 
<a name="l00786"></a><a class="code" href="class_seldon_1_1_matrix___hermitian.php#a46a74bc40dd065078ea0d44fd1d8eb02">00786</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00787"></a>00787   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a433db64775f698010e5f13c57fa17c8c" title="Displays the matrix on the standard output.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00788"></a>00788 <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a433db64775f698010e5f13c57fa17c8c" title="Displays the matrix on the standard output.">  ::Print</a>(<span class="keywordtype">int</span> a, <span class="keywordtype">int</span> b, <span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n)<span class="keyword"> const</span>
<a name="l00789"></a>00789 <span class="keyword">  </span>{
<a name="l00790"></a>00790     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = a; i &lt; min(this-&gt;m_, a+m); i++)
<a name="l00791"></a>00791       {
<a name="l00792"></a>00792         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = b; j &lt; min(this-&gt;n_, b+n); j++)
<a name="l00793"></a>00793           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00794"></a>00794         cout &lt;&lt; endl;
<a name="l00795"></a>00795       }
<a name="l00796"></a>00796   }
<a name="l00797"></a>00797 
<a name="l00798"></a>00798 
<a name="l00800"></a>00800 
<a name="l00808"></a>00808   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00809"></a>00809   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a433db64775f698010e5f13c57fa17c8c" title="Displays the matrix on the standard output.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::Print</a>(<span class="keywordtype">int</span> l)<span class="keyword"> const</span>
<a name="l00810"></a>00810 <span class="keyword">  </span>{
<a name="l00811"></a>00811     <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a433db64775f698010e5f13c57fa17c8c" title="Displays the matrix on the standard output.">Print</a>(0, 0, l, l);
<a name="l00812"></a>00812   }
<a name="l00813"></a>00813 
<a name="l00814"></a>00814 
<a name="l00815"></a>00815   <span class="comment">/**************************</span>
<a name="l00816"></a>00816 <span class="comment">   * INPUT/OUTPUT FUNCTIONS *</span>
<a name="l00817"></a>00817 <span class="comment">   **************************/</span>
<a name="l00818"></a>00818 
<a name="l00819"></a>00819 
<a name="l00821"></a>00821 
<a name="l00828"></a><a class="code" href="class_seldon_1_1_matrix___hermitian.php#af7be31fbc2759b842b75f899bb03fe42">00828</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00829"></a>00829   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#af7be31fbc2759b842b75f899bb03fe42" title="Writes the matrix in a file.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00830"></a>00830 <a class="code" href="class_seldon_1_1_matrix___hermitian.php#af7be31fbc2759b842b75f899bb03fe42" title="Writes the matrix in a file.">  ::Write</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00831"></a>00831 <span class="keyword">  </span>{
<a name="l00832"></a>00832     ofstream FileStream;
<a name="l00833"></a>00833     FileStream.open(FileName.c_str());
<a name="l00834"></a>00834 
<a name="l00835"></a>00835 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00836"></a>00836 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00837"></a>00837     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00838"></a>00838       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Hermitian::Write(string FileName)&quot;</span>,
<a name="l00839"></a>00839                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00840"></a>00840 <span class="preprocessor">#endif</span>
<a name="l00841"></a>00841 <span class="preprocessor"></span>
<a name="l00842"></a>00842     this-&gt;Write(FileStream);
<a name="l00843"></a>00843 
<a name="l00844"></a>00844     FileStream.close();
<a name="l00845"></a>00845   }
<a name="l00846"></a>00846 
<a name="l00847"></a>00847 
<a name="l00849"></a>00849 
<a name="l00856"></a><a class="code" href="class_seldon_1_1_matrix___hermitian.php#a5d7125ef8eab121b8bfa3ee94e510efe">00856</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00857"></a>00857   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#af7be31fbc2759b842b75f899bb03fe42" title="Writes the matrix in a file.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00858"></a>00858 <a class="code" href="class_seldon_1_1_matrix___hermitian.php#af7be31fbc2759b842b75f899bb03fe42" title="Writes the matrix in a file.">  ::Write</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00859"></a>00859 <span class="keyword">  </span>{
<a name="l00860"></a>00860 
<a name="l00861"></a>00861 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00862"></a>00862 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00863"></a>00863     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00864"></a>00864       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Hermitian::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l00865"></a>00865                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00866"></a>00866 <span class="preprocessor">#endif</span>
<a name="l00867"></a>00867 <span class="preprocessor"></span>
<a name="l00868"></a>00868     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l00869"></a>00869                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00870"></a>00870     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;n_)),
<a name="l00871"></a>00871                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00872"></a>00872 
<a name="l00873"></a>00873     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l00874"></a>00874                      this-&gt;m_ * this-&gt;n_ * <span class="keyword">sizeof</span>(value_type));
<a name="l00875"></a>00875 
<a name="l00876"></a>00876 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00877"></a>00877 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00878"></a>00878     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00879"></a>00879       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Hermitian::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l00880"></a>00880                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Output operation failed.&quot;</span>)
<a name="l00881"></a>00881                     + string(<span class="stringliteral">&quot; The output file may have been removed&quot;</span>)
<a name="l00882"></a>00882                     + <span class="stringliteral">&quot; or there is no space left on device.&quot;</span>);
<a name="l00883"></a>00883 <span class="preprocessor">#endif</span>
<a name="l00884"></a>00884 <span class="preprocessor"></span>
<a name="l00885"></a>00885   }
<a name="l00886"></a>00886 
<a name="l00887"></a>00887 
<a name="l00889"></a>00889 
<a name="l00896"></a><a class="code" href="class_seldon_1_1_matrix___hermitian.php#af934c4402798de88c98750f7eff04cb9">00896</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00897"></a>00897   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#af934c4402798de88c98750f7eff04cb9" title="Writes the matrix in a file.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00898"></a>00898 <a class="code" href="class_seldon_1_1_matrix___hermitian.php#af934c4402798de88c98750f7eff04cb9" title="Writes the matrix in a file.">  ::WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00899"></a>00899 <span class="keyword">  </span>{
<a name="l00900"></a>00900     ofstream FileStream;
<a name="l00901"></a>00901     FileStream.precision(cout.precision());
<a name="l00902"></a>00902     FileStream.flags(cout.flags());
<a name="l00903"></a>00903     FileStream.open(FileName.c_str());
<a name="l00904"></a>00904 
<a name="l00905"></a>00905 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00906"></a>00906 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00907"></a>00907     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00908"></a>00908       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Hermitian::WriteText(string FileName)&quot;</span>,
<a name="l00909"></a>00909                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00910"></a>00910 <span class="preprocessor">#endif</span>
<a name="l00911"></a>00911 <span class="preprocessor"></span>
<a name="l00912"></a>00912     this-&gt;WriteText(FileStream);
<a name="l00913"></a>00913 
<a name="l00914"></a>00914     FileStream.close();
<a name="l00915"></a>00915   }
<a name="l00916"></a>00916 
<a name="l00917"></a>00917 
<a name="l00919"></a>00919 
<a name="l00926"></a><a class="code" href="class_seldon_1_1_matrix___hermitian.php#acda477352c0500436acea5ca3b28c943">00926</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00927"></a>00927   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#af934c4402798de88c98750f7eff04cb9" title="Writes the matrix in a file.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00928"></a>00928 <a class="code" href="class_seldon_1_1_matrix___hermitian.php#af934c4402798de88c98750f7eff04cb9" title="Writes the matrix in a file.">  ::WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00929"></a>00929 <span class="keyword">  </span>{
<a name="l00930"></a>00930 
<a name="l00931"></a>00931 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00932"></a>00932 <span class="preprocessor"></span>    <span class="comment">// Checks if the file is ready.</span>
<a name="l00933"></a>00933     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00934"></a>00934       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Hermitian::WriteText(ofstream&amp; FileStream)&quot;</span>,
<a name="l00935"></a>00935                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00936"></a>00936 <span class="preprocessor">#endif</span>
<a name="l00937"></a>00937 <span class="preprocessor"></span>
<a name="l00938"></a>00938     <span class="keywordtype">int</span> i, j;
<a name="l00939"></a>00939     <span class="keywordflow">for</span> (i = 0; i &lt; this-&gt;GetM(); i++)
<a name="l00940"></a>00940       {
<a name="l00941"></a>00941         <span class="keywordflow">for</span> (j = 0; j &lt; this-&gt;GetN(); j++)
<a name="l00942"></a>00942           FileStream &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="charliteral">&#39;\t&#39;</span>;
<a name="l00943"></a>00943         FileStream &lt;&lt; endl;
<a name="l00944"></a>00944       }
<a name="l00945"></a>00945 
<a name="l00946"></a>00946 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00947"></a>00947 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00948"></a>00948     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00949"></a>00949       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Hermitian::WriteText(ofstream&amp; FileStream)&quot;</span>,
<a name="l00950"></a>00950                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Output operation failed.&quot;</span>)
<a name="l00951"></a>00951                     + string(<span class="stringliteral">&quot; The output file may have been removed&quot;</span>)
<a name="l00952"></a>00952                     + <span class="stringliteral">&quot; or there is no space left on device.&quot;</span>);
<a name="l00953"></a>00953 <span class="preprocessor">#endif</span>
<a name="l00954"></a>00954 <span class="preprocessor"></span>
<a name="l00955"></a>00955   }
<a name="l00956"></a>00956 
<a name="l00957"></a>00957 
<a name="l00959"></a>00959 
<a name="l00966"></a>00966   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00967"></a>00967   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a4b2b0c1560503f1c81ac03230a118b47" title="Reads the matrix from a file.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::Read</a>(<span class="keywordtype">string</span> FileName)
<a name="l00968"></a>00968   {
<a name="l00969"></a>00969     ifstream FileStream;
<a name="l00970"></a>00970     FileStream.open(FileName.c_str());
<a name="l00971"></a>00971 
<a name="l00972"></a>00972 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00973"></a>00973 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00974"></a>00974     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00975"></a>00975       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Hermitian::Read(string FileName)&quot;</span>,
<a name="l00976"></a>00976                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00977"></a>00977 <span class="preprocessor">#endif</span>
<a name="l00978"></a>00978 <span class="preprocessor"></span>
<a name="l00979"></a>00979     this-&gt;<a class="code" href="class_seldon_1_1_matrix___hermitian.php#a4b2b0c1560503f1c81ac03230a118b47" title="Reads the matrix from a file.">Read</a>(FileStream);
<a name="l00980"></a>00980 
<a name="l00981"></a>00981     FileStream.close();
<a name="l00982"></a>00982   }
<a name="l00983"></a>00983 
<a name="l00984"></a>00984 
<a name="l00986"></a>00986 
<a name="l00993"></a><a class="code" href="class_seldon_1_1_matrix___hermitian.php#a6ac597e0a71778f33e9e1b7b98a26fea">00993</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00994"></a>00994   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a4b2b0c1560503f1c81ac03230a118b47" title="Reads the matrix from a file.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00995"></a>00995 <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a4b2b0c1560503f1c81ac03230a118b47" title="Reads the matrix from a file.">  ::Read</a>(istream&amp; FileStream)
<a name="l00996"></a>00996   {
<a name="l00997"></a>00997 
<a name="l00998"></a>00998 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00999"></a>00999 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01000"></a>01000     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01001"></a>01001       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Hermitian::Read(ifstream&amp; FileStream)&quot;</span>,
<a name="l01002"></a>01002                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l01003"></a>01003 <span class="preprocessor">#endif</span>
<a name="l01004"></a>01004 <span class="preprocessor"></span>
<a name="l01005"></a>01005     <span class="keywordtype">int</span> new_m, new_n;
<a name="l01006"></a>01006     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;new_m), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01007"></a>01007     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;new_n), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01008"></a>01008     this-&gt;Reallocate(new_m, new_n);
<a name="l01009"></a>01009 
<a name="l01010"></a>01010     FileStream.read(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l01011"></a>01011                     new_m * new_n * <span class="keyword">sizeof</span>(value_type));
<a name="l01012"></a>01012 
<a name="l01013"></a>01013 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01014"></a>01014 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l01015"></a>01015     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01016"></a>01016       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Hermitian::Read(ifstream&amp; FileStream)&quot;</span>,
<a name="l01017"></a>01017                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Output operation failed.&quot;</span>)
<a name="l01018"></a>01018                     + string(<span class="stringliteral">&quot; The intput file may have been removed&quot;</span>)
<a name="l01019"></a>01019                     + <span class="stringliteral">&quot; or may not contain enough data.&quot;</span>);
<a name="l01020"></a>01020 <span class="preprocessor">#endif</span>
<a name="l01021"></a>01021 <span class="preprocessor"></span>
<a name="l01022"></a>01022   }
<a name="l01023"></a>01023 
<a name="l01024"></a>01024 
<a name="l01026"></a>01026 
<a name="l01030"></a>01030   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01031"></a>01031   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a4457b2914ba7b363dcc0c339822231f9" title="Reads the matrix from a file.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;::ReadText</a>(<span class="keywordtype">string</span> FileName)
<a name="l01032"></a>01032   {
<a name="l01033"></a>01033     ifstream FileStream;
<a name="l01034"></a>01034     FileStream.open(FileName.c_str());
<a name="l01035"></a>01035 
<a name="l01036"></a>01036 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01037"></a>01037 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01038"></a>01038     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01039"></a>01039       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(string FileName)&quot;</span>,
<a name="l01040"></a>01040                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01041"></a>01041 <span class="preprocessor">#endif</span>
<a name="l01042"></a>01042 <span class="preprocessor"></span>
<a name="l01043"></a>01043     this-&gt;<a class="code" href="class_seldon_1_1_matrix___hermitian.php#a4457b2914ba7b363dcc0c339822231f9" title="Reads the matrix from a file.">ReadText</a>(FileStream);
<a name="l01044"></a>01044 
<a name="l01045"></a>01045     FileStream.close();
<a name="l01046"></a>01046   }
<a name="l01047"></a>01047 
<a name="l01048"></a>01048 
<a name="l01050"></a>01050 
<a name="l01054"></a><a class="code" href="class_seldon_1_1_matrix___hermitian.php#af42b2a4dc8d06f9b1dba683b58ca73e7">01054</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01055"></a>01055   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a4457b2914ba7b363dcc0c339822231f9" title="Reads the matrix from a file.">Matrix_Hermitian&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01056"></a>01056 <a class="code" href="class_seldon_1_1_matrix___hermitian.php#a4457b2914ba7b363dcc0c339822231f9" title="Reads the matrix from a file.">  ::ReadText</a>(istream&amp; FileStream)
<a name="l01057"></a>01057   {
<a name="l01058"></a>01058     <span class="comment">// clears previous matrix</span>
<a name="l01059"></a>01059     Clear();
<a name="l01060"></a>01060 
<a name="l01061"></a>01061 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01062"></a>01062 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01063"></a>01063     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01064"></a>01064       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(ifstream&amp; FileStream)&quot;</span>,
<a name="l01065"></a>01065                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l01066"></a>01066 <span class="preprocessor">#endif</span>
<a name="l01067"></a>01067 <span class="preprocessor"></span>
<a name="l01068"></a>01068     <span class="comment">// we read first line</span>
<a name="l01069"></a>01069     <span class="keywordtype">string</span> line;
<a name="l01070"></a>01070     getline(FileStream, line);
<a name="l01071"></a>01071 
<a name="l01072"></a>01072     <span class="keywordflow">if</span> (FileStream.fail())
<a name="l01073"></a>01073       {
<a name="l01074"></a>01074         <span class="comment">// empty file ?</span>
<a name="l01075"></a>01075         <span class="keywordflow">return</span>;
<a name="l01076"></a>01076       }
<a name="l01077"></a>01077 
<a name="l01078"></a>01078     <span class="comment">// converting first line into a vector</span>
<a name="l01079"></a>01079     istringstream line_stream(line);
<a name="l01080"></a>01080     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> first_row;
<a name="l01081"></a>01081     first_row.ReadText(line_stream);
<a name="l01082"></a>01082 
<a name="l01083"></a>01083     <span class="comment">// and now the other rows</span>
<a name="l01084"></a>01084     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> other_rows;
<a name="l01085"></a>01085     other_rows.ReadText(FileStream);
<a name="l01086"></a>01086 
<a name="l01087"></a>01087     <span class="comment">// number of rows and columns</span>
<a name="l01088"></a>01088     <span class="keywordtype">int</span> n = first_row.GetM();
<a name="l01089"></a>01089     <span class="keywordtype">int</span> m = 1 + other_rows.GetM()/n;
<a name="l01090"></a>01090 
<a name="l01091"></a>01091 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01092"></a>01092 <span class="preprocessor"></span>    <span class="comment">// Checking number of elements</span>
<a name="l01093"></a>01093     <span class="keywordflow">if</span> (other_rows.GetM() != (m-1)*n)
<a name="l01094"></a>01094       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(ifstream&amp; FileStream)&quot;</span>,
<a name="l01095"></a>01095                     <span class="stringliteral">&quot;The file should contain same number of columns.&quot;</span>);
<a name="l01096"></a>01096 <span class="preprocessor">#endif</span>
<a name="l01097"></a>01097 <span class="preprocessor"></span>
<a name="l01098"></a>01098     this-&gt;Reallocate(m,n);
<a name="l01099"></a>01099     <span class="comment">// filling matrix</span>
<a name="l01100"></a>01100     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l01101"></a>01101       this-&gt;Val(0, j) = first_row(j);
<a name="l01102"></a>01102 
<a name="l01103"></a>01103     <span class="keywordtype">int</span> nb = 0;
<a name="l01104"></a>01104     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 1; i &lt; m; i++)
<a name="l01105"></a>01105       {
<a name="l01106"></a>01106         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; i; j++)
<a name="l01107"></a>01107           nb++;
<a name="l01108"></a>01108 
<a name="l01109"></a>01109         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = i; j &lt; n; j++)
<a name="l01110"></a>01110           this-&gt;Val(i, j) = other_rows(nb++);
<a name="l01111"></a>01111       }
<a name="l01112"></a>01112   }
<a name="l01113"></a>01113 
<a name="l01114"></a>01114 
<a name="l01115"></a>01115 
<a name="l01117"></a>01117   <span class="comment">// MATRIX&lt;COLHERM&gt; //</span>
<a name="l01119"></a>01119 <span class="comment"></span>
<a name="l01120"></a>01120 
<a name="l01121"></a>01121   <span class="comment">/****************</span>
<a name="l01122"></a>01122 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01123"></a>01123 <span class="comment">   ****************/</span>
<a name="l01124"></a>01124 
<a name="l01125"></a>01125 
<a name="l01127"></a>01127 
<a name="l01130"></a>01130   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01131"></a>01131   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColHerm, Allocator&gt;::Matrix</a>()  throw():
<a name="l01132"></a>01132     <a class="code" href="class_seldon_1_1_matrix___hermitian.php" title="Hermitian matrix stored in a full matrix.">Matrix_Hermitian</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_herm.php">ColHerm</a>, Allocator&gt;()
<a name="l01133"></a>01133   {
<a name="l01134"></a>01134   }
<a name="l01135"></a>01135 
<a name="l01136"></a>01136 
<a name="l01138"></a>01138 
<a name="l01142"></a>01142   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01143"></a>01143   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_00_01_allocator_01_4.php#a98135088d49660a050e9aeefc188d20f" title="Default constructor.">Matrix&lt;T, Prop, ColHerm, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01144"></a>01144     Matrix_Hermitian&lt;T, Prop, ColHerm, Allocator&gt;(i, j)
<a name="l01145"></a>01145   {
<a name="l01146"></a>01146   }
<a name="l01147"></a>01147 
<a name="l01148"></a>01148 
<a name="l01149"></a>01149   <span class="comment">/*****************</span>
<a name="l01150"></a>01150 <span class="comment">   * OTHER METHODS *</span>
<a name="l01151"></a>01151 <span class="comment">   *****************/</span>
<a name="l01152"></a>01152 
<a name="l01153"></a>01153 
<a name="l01155"></a>01155 
<a name="l01158"></a>01158   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01159"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_00_01_allocator_01_4.php#a9ce3f192a24e1d1845a9377435bbb99c">01159</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01160"></a>01160   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_00_01_allocator_01_4.php" title="Column-major hermitian full-matrix class.">Matrix&lt;T, Prop, ColHerm, Allocator&gt;</a>&amp;
<a name="l01161"></a>01161   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColHerm, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01162"></a>01162   {
<a name="l01163"></a>01163     this-&gt;Fill(x);
<a name="l01164"></a>01164 
<a name="l01165"></a>01165     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01166"></a>01166   }
<a name="l01167"></a>01167 
<a name="l01168"></a>01168 
<a name="l01170"></a>01170 
<a name="l01173"></a>01173   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01174"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_00_01_allocator_01_4.php#a7ae16201a85d4196867631346db84c85">01174</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01175"></a>01175   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_00_01_allocator_01_4.php" title="Column-major hermitian full-matrix class.">Matrix&lt;T, Prop, ColHerm, Allocator&gt;</a>&amp;
<a name="l01176"></a>01176   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColHerm, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01177"></a>01177   {
<a name="l01178"></a>01178     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetDataSize();i++)
<a name="l01179"></a>01179       this-&gt;data_[i] *= x;
<a name="l01180"></a>01180 
<a name="l01181"></a>01181     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01182"></a>01182   }
<a name="l01183"></a>01183 
<a name="l01184"></a>01184 
<a name="l01185"></a>01185 
<a name="l01187"></a>01187   <span class="comment">// MATRIX&lt;ROWHERM&gt; //</span>
<a name="l01189"></a>01189 <span class="comment"></span>
<a name="l01190"></a>01190 
<a name="l01191"></a>01191   <span class="comment">/****************</span>
<a name="l01192"></a>01192 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01193"></a>01193 <span class="comment">   ****************/</span>
<a name="l01194"></a>01194 
<a name="l01195"></a>01195 
<a name="l01197"></a>01197 
<a name="l01200"></a>01200   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01201"></a>01201   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowHerm, Allocator&gt;::Matrix</a>()  throw():
<a name="l01202"></a>01202     <a class="code" href="class_seldon_1_1_matrix___hermitian.php" title="Hermitian matrix stored in a full matrix.">Matrix_Hermitian</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_herm.php">RowHerm</a>, Allocator&gt;()
<a name="l01203"></a>01203   {
<a name="l01204"></a>01204   }
<a name="l01205"></a>01205 
<a name="l01206"></a>01206 
<a name="l01208"></a>01208 
<a name="l01212"></a>01212   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01213"></a>01213   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_00_01_allocator_01_4.php#a5634cb9ee0148e17b4314b9db0a887ff" title="Default constructor.">Matrix&lt;T, Prop, RowHerm, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01214"></a>01214     Matrix_Hermitian&lt;T, Prop, RowHerm, Allocator&gt;(i, j)
<a name="l01215"></a>01215   {
<a name="l01216"></a>01216   }
<a name="l01217"></a>01217 
<a name="l01218"></a>01218 
<a name="l01219"></a>01219   <span class="comment">/*****************</span>
<a name="l01220"></a>01220 <span class="comment">   * OTHER METHODS *</span>
<a name="l01221"></a>01221 <span class="comment">   *****************/</span>
<a name="l01222"></a>01222 
<a name="l01223"></a>01223 
<a name="l01225"></a>01225 
<a name="l01228"></a>01228   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01229"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_00_01_allocator_01_4.php#aa8571c2671559a14854607d20ff9008d">01229</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01230"></a>01230   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_00_01_allocator_01_4.php" title="Row-major hermitian full-matrix class.">Matrix&lt;T, Prop, RowHerm, Allocator&gt;</a>&amp;
<a name="l01231"></a>01231   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowHerm, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01232"></a>01232   {
<a name="l01233"></a>01233     this-&gt;Fill(x);
<a name="l01234"></a>01234 
<a name="l01235"></a>01235     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01236"></a>01236   }
<a name="l01237"></a>01237 
<a name="l01238"></a>01238 
<a name="l01240"></a>01240 
<a name="l01243"></a>01243   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01244"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_00_01_allocator_01_4.php#adc2b4012e48ecdcbce64604a7319f5ce">01244</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01245"></a>01245   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_00_01_allocator_01_4.php" title="Row-major hermitian full-matrix class.">Matrix&lt;T, Prop, RowHerm, Allocator&gt;</a>&amp;
<a name="l01246"></a>01246   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowHerm, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01247"></a>01247   {
<a name="l01248"></a>01248     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetDataSize();i++)
<a name="l01249"></a>01249       this-&gt;data_[i] *= x;
<a name="l01250"></a>01250 
<a name="l01251"></a>01251     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01252"></a>01252   }
<a name="l01253"></a>01253 
<a name="l01254"></a>01254 } <span class="comment">// namespace Seldon.</span>
<a name="l01255"></a>01255 
<a name="l01256"></a>01256 <span class="preprocessor">#define SELDON_FILE_MATRIX_HERMITIAN_CXX</span>
<a name="l01257"></a>01257 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
