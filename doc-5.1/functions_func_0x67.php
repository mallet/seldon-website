<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="tabs2">
    <ul class="tablist">
      <li><a href="functions.php"><span>All</span></a></li>
      <li class="current"><a href="functions_func.php"><span>Functions</span></a></li>
      <li><a href="functions_vars.php"><span>Variables</span></a></li>
      <li><a href="functions_type.php"><span>Typedefs</span></a></li>
    </ul>
  </div>
  <div class="tabs3">
    <ul class="tablist">
      <li><a href="functions_func.php#index_a"><span>a</span></a></li>
      <li><a href="functions_func_0x63.php#index_c"><span>c</span></a></li>
      <li><a href="functions_func_0x64.php#index_d"><span>d</span></a></li>
      <li><a href="functions_func_0x65.php#index_e"><span>e</span></a></li>
      <li><a href="functions_func_0x66.php#index_f"><span>f</span></a></li>
      <li class="current"><a href="functions_func_0x67.php#index_g"><span>g</span></a></li>
      <li><a href="functions_func_0x68.php#index_h"><span>h</span></a></li>
      <li><a href="functions_func_0x69.php#index_i"><span>i</span></a></li>
      <li><a href="functions_func_0x6c.php#index_l"><span>l</span></a></li>
      <li><a href="functions_func_0x6d.php#index_m"><span>m</span></a></li>
      <li><a href="functions_func_0x6e.php#index_n"><span>n</span></a></li>
      <li><a href="functions_func_0x6f.php#index_o"><span>o</span></a></li>
      <li><a href="functions_func_0x70.php#index_p"><span>p</span></a></li>
      <li><a href="functions_func_0x72.php#index_r"><span>r</span></a></li>
      <li><a href="functions_func_0x73.php#index_s"><span>s</span></a></li>
      <li><a href="functions_func_0x74.php#index_t"><span>t</span></a></li>
      <li><a href="functions_func_0x75.php#index_u"><span>u</span></a></li>
      <li><a href="functions_func_0x76.php#index_v"><span>v</span></a></li>
      <li><a href="functions_func_0x77.php#index_w"><span>w</span></a></li>
      <li><a href="functions_func_0x7a.php#index_z"><span>z</span></a></li>
      <li><a href="functions_func_0x7e.php#index_~"><span>~</span></a></li>
    </ul>
  </div>
<div class="contents">
&nbsp;

<h3><a class="anchor" id="index_g"></a>- g -</h3><ul>
<li>GetAllocator()
: <a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>
</li>
<li>GetCollectionIndex()
: <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a5bdd2a3d741868fc687c2e62388dd348">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
</li>
<li>GetColPermutation()
: <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#acf6c8120e481e56c99e77bea79b30f84">Seldon::MatrixSuperLU_Base&lt; T &gt;</a>
</li>
<li>GetColumnSize()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sparse_00_01_allocator_01_4.php#a6db90999f5bd69e71afcf585b11a00e7">Seldon::Matrix&lt; T, Prop, ArrayColSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#accfeb4e792ac6f354c2f9c40827e8c3a">Seldon::Matrix&lt; T, Prop, ArrayColSymSparse, Allocator &gt;</a>
</li>
<li>GetData()
: <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a0c18c6428cc11d175b58205727a4c67d">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a">Seldon::Vector_Base&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_array3_d.php#a41def646fbd18d0a86b2fced6e80faf1">Seldon::Array3D&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>
</li>
<li>GetDataConst()
: <a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector___base.php#a454aea78c82c4317dfe4160cc7c95afa">Seldon::Vector_Base&lt; T, Allocator &gt;</a>
</li>
<li>GetDataConstVoid()
: <a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector___base.php#a0aadc006977de037eb3ee550683e3f19">Seldon::Vector_Base&lt; T, Allocator &gt;</a>
</li>
<li>GetDataSize()
: <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a2a36130a12ef3b4e4a4971f5898ca0bd">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___hermitian.php#a0ce699921fe6da8089f6708fbbfc959a">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a8bd2747b6c90c4114cd2f54620d1f230">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___pointers.php#a36ac8e7e9bee92ae266201dfb60bf32e">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___symmetric.php#a8876d406208337c491b2e9d2f56de084">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a3fed51d6dd6d46d8e611ce98f9d8f724">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a9d331415858c046a3a72699d2bbd8ceb">Seldon::Matrix_TriangPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triangular.php#a812d8809554264f3b8f64429231b4e39">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_collection.php#a1162607a4f1255a388598a6dc5eea1f8">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2e5c6c10af220eea29f9c1565b14a93f">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a01787b806fc996c2ff3c1dcf9b2d54de">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a543d8834abfbd7888c6d2516612bde7b">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sparse.php#aac6b06ab623602d24a80d12adf495cf2">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#afda1dbd968e2e4b3047c014ab5931846">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_array3_d.php#a64e6b9afc92a4c7fe4a99f5017959cb6">Seldon::Array3D&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a8950827192f33de71909f783032c9f5f">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a3906b8279cc318dc0a8a672fa781095a">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
</li>
<li>GetDataVoid()
: <a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector___base.php#ad0f5184bd9eec6fca70c54e459ced78f">Seldon::Vector_Base&lt; T, Allocator &gt;</a>
</li>
<li>GetDoubleDense()
: <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a289e9f762721738e42b9907450e72e9f">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a48e3d61c043a065c53b41470e706dfee">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
</li>
<li>GetDoubleSparse()
: <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a39301f5774586f67d31f64a3d4acb0af">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a1add5fecb61b4984003fc903f5e4a71b">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
</li>
<li>GetFactor()
: <a class="el" href="class_seldon_1_1_iteration.php#a617c6d99afeb2b5a2d18144f367dba88">Seldon::Iteration&lt; Titer &gt;</a>
</li>
<li>GetFloatDense()
: <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a9bd6c070c0738b3425e5e9b3e0118ec6">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#addef6ea1f486b2b836b2ca54f8273ccb">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
</li>
<li>GetFloatSparse()
: <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a69cf73dbc6d9055bc8ad28633de18f24">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a919fc21cc5754175f976df5e1c62f1aa">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
</li>
<li>GetImagColumnSize()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#ad6f24240d54e489ab302c91a10b62276">Seldon::Matrix&lt; T, Prop, ArrayColComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a6cebdd4865f5c2c9e90d5b4f6961414a">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>
</li>
<li>GetImagData()
: <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a578281c64cbe05a542a6b0e9642b326a">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a5637e2dd95b0e62a7f42c0f3fd1383b1">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a411656148afd39425bd6df81a013d180">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetImagDataSize()
: <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad54321edab8e2633985e198f1a576340">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetImagInd()
: <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0a423d7e93a0ad9c19ed6de2b1052dca">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a08f3d91609557b1650066765343909db">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a2bb61950179c75be2150cfae734bb05b">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetImagIndSize()
: <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ad61069ad96f1bfd7b57f6a4e9fe85764">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af1c1a606649e03827a3710751238a5bc">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetImagNonZeros()
: <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a33ebac042eda90796d7f674c250e85be">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetImagPtr()
: <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad1bb43e80676e9ee420ea692ba06d0ac">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a6766b6db59aa35b4fa7458a12e531e9b">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetImagPtrSize()
: <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ae97357d4afa167ca95d28e8897e44ef2">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#acde6ac68023f92f32eceb4f0a422580f">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetImagRowSize()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a5477a0086e76fbee8db94fcfa1cd9703">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#af38c69e307e68125ec3304929b672e95">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>
</li>
<li>GetInd()
: <a class="el" href="class_seldon_1_1_matrix___sparse.php#a1378a5e93b30065896f0d84b07b18caa">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a9d4ddb6035ed47fadc9f35df03c3764e">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetIndex()
: <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#ac3d6a23829eb932f41de04c13de22f6b">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a6a4ed85be713e5a300f94eca786a2c2b">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>
</li>
<li>GetIndSize()
: <a class="el" href="class_seldon_1_1_matrix___sparse.php#a7b136934dfbe364dd016ac33e4a5dcc6">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a6ffb37230f5629d9f85a6b962bc8127d">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetInfoFactorization()
: <a class="el" href="class_seldon_1_1_matrix_mumps.php#aab6219d8e5473f80dae2957a3d003ab0">Seldon::MatrixMumps&lt; T &gt;</a>
</li>
<li>GetLD()
: <a class="el" href="class_seldon_1_1_matrix___pointers.php#af93c5e73fc737a2f6e4f0b15164dd46d">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetLength()
: <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ae501c34e51f6559568846b94e24c249c">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_vector___base.php#a075daae3607a4767a77a3de60ab30ba7">Seldon::Vector_Base&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector2.php#a5817c617540d8147b15a6296a3fa49b8">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#a4a694febe8dba0a1203fd628cd1e3167">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a2a2b8d5ad39c79bdcdcd75fa20ae5ae8">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
</li>
<li>GetLength1()
: <a class="el" href="class_seldon_1_1_array3_d.php#ab792b6b72a04b557792b6d5ad3e86afc">Seldon::Array3D&lt; T, Allocator &gt;</a>
</li>
<li>GetLength2()
: <a class="el" href="class_seldon_1_1_array3_d.php#af8acaef0c21a50f4ab6128710d534f8b">Seldon::Array3D&lt; T, Allocator &gt;</a>
</li>
<li>GetLength3()
: <a class="el" href="class_seldon_1_1_array3_d.php#afae5f3ffbbcb66b09ec27d1cfc81759a">Seldon::Array3D&lt; T, Allocator &gt;</a>
</li>
<li>GetLengthSum()
: <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a750b9d392301df88e3cdb76cf11f87a0">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a947fa0953ce1de93ef0de93d23ffba38">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
</li>
<li>GetLU()
: <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a6492cdcde57b8524db187be6f4602ef5">Seldon::MatrixSuperLU_Base&lt; T &gt;</a>
</li>
<li>GetM()
: <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a8c781e99310aae01786eac0e8e5aa50c">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a6f40d71a69e016ae3e1d9db92b6538a4">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae">Seldon::Vector_Base&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a9b0a8be2fb500d4f6c2edb4c1a5247ef">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
</li>
<li>GetMatrix()
: <a class="el" href="class_seldon_1_1_matrix_collection.php#ad18c52c1ed65330e6443e4519df390a4">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetMmatrix()
: <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a38820efd26db52feaeb8998d2fd43daa">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_collection.php#aaa428778f995ce6bbe08f1181fbb51e4">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetN()
: <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#ae413915ed95545b40df8b43737fa43c1">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a40580a92202044416e379c1304ff0220">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a53e20518a8557211c89ca8ad0d607825">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a3ae65d56c548233051a15054643f6753">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetNelement()
: <a class="el" href="class_seldon_1_1_vector2.php#aff7d041efb4e03152ecb03966ae92948">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#a497379d28c87524052ee202702db9c91">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
</li>
<li>GetNmatrix()
: <a class="el" href="class_seldon_1_1_matrix_collection.php#a4a1165a73495d9eb4edfd49ba8e1f9b7">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a6c59c77733659c863477c9a34fc927fe">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
</li>
<li>GetNonZeros()
: <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#ac58573fd27b4f0345ae196b1a6c69d0d">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sparse.php#a0a0f412562d73bb7c9361da17565e7fb">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a4be3dd5024addbf8bfa332193c4030cd">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetNormInf()
: <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a165cbeeddb4a2a356accc00d609b4211">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
</li>
<li>GetNormInfIndex()
: <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a50186150708e90f86ded59e33cb172a6">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
</li>
<li>GetNumberIteration()
: <a class="el" href="class_seldon_1_1_iteration.php#ab426008d4bfc40842d86c8c6042a6594">Seldon::Iteration&lt; Titer &gt;</a>
</li>
<li>GetNvector()
: <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a35ec50dd32f08ecf3a8ce073a6b4bfff">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a696c512cf3692f2e7193446373aa8c97">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
</li>
<li>GetPtr()
: <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#ae6576503580320224c84cc46d675781d">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sparse.php#a9126980f0a760e8a73c2d0f480c69342">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetPtrSize()
: <a class="el" href="class_seldon_1_1_matrix___sparse.php#a0c3409f0fec69fb357c493154e5d763b">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a046fce2e283dc38a9ed0ce5b570e6f11">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetRealColumnSize()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ab488dd8cd29a05f016d63b30e90b4f2d">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a790b5cd9f59aed77430dfbceb834a33e">Seldon::Matrix&lt; T, Prop, ArrayColComplexSparse, Allocator &gt;</a>
</li>
<li>GetRealData()
: <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a74325c4f424c0043682b5cb3ad3df501">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a1a98f711de4b7daf42ea42c8493ca08d">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aaa912365c7b8c191cf8b3fb2e2da4789">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetRealDataSize()
: <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa40775992c738c7fd1e9543bfddb2694">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetRealInd()
: <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a4eb0b0535cb51c9c0886da147f6e0cb0">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#afae45969eda952b43748cf7756f0e6d6">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a72f50ab4230ffe4af23c8bbf2203ad0b">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetRealIndSize()
: <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ae573e32219c095276106254a84e7f684">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#acdb80353a65a45dfc11b886e7c75a807">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetRealNonZeros()
: <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a11efc1bee6a95da705238628d7e3d958">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetRealPtr()
: <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#afa783bff3e823f8ad2480a50d4d28929">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a005a3b7389aecf38482cc16e4984787c">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetRealPtrSize()
: <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a3be4bf190b7c5957d81ddf7e7a7b7882">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a913aadd75d1ed2e076ea4b014e5fb32d">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>GetRealRowSize()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a794411e243ead33b91cb750921602cdd">Seldon::Matrix&lt; T, Prop, ArrayRowComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#afc6d830c2b6290ab90af0733ca2991d9">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>
</li>
<li>GetRestart()
: <a class="el" href="class_seldon_1_1_iteration.php#ae383df46b41aff26fe97418cab8a070f">Seldon::Iteration&lt; Titer &gt;</a>
</li>
<li>GetRowPermutation()
: <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a27aca5955b9d58ab561f652f153139ee">Seldon::MatrixSuperLU_Base&lt; T &gt;</a>
</li>
<li>GetRowSize()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a1db215224f5b1ac332a82b55da858445">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#af202c14cefd0cb751e4cf906ca5804e7">Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</a>
</li>
<li>GetSchurMatrix()
: <a class="el" href="class_seldon_1_1_matrix_mumps.php#adb5993784b010142a470da631025ce0a">Seldon::MatrixMumps&lt; T &gt;</a>
</li>
<li>GetShape()
: <a class="el" href="class_seldon_1_1_vector2.php#a82efefffc4e2e7ebaf120e080a79fb95">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
</li>
<li>GetSize()
: <a class="el" href="class_seldon_1_1_vector3.php#a6b29a36baf352da31f2a97ec972fd761">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector___base.php#a839880a3113c1885ead70d79fade0294">Seldon::Vector_Base&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_array3_d.php#a99dc59f2c62979e78c43d8ec31c02e68">Seldon::Array3D&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#a66946f1764cc14abec8a51d4e310f6b5">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector2.php#a1eb80b01b78bb858b72a3daace5b7fa2">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
</li>
<li>GetSubvectorIndex()
: <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a6872e6bac0e998a5b2e7dbf2d9b1807f">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
</li>
<li>GetTolerance()
: <a class="el" href="class_seldon_1_1_iteration.php#af31105a0169bb625d24a4ba76183b3a5">Seldon::Iteration&lt; Titer &gt;</a>
</li>
<li>GetType()
: <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#adc5a4e9e00f74737c6dc4b69e810e26d">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a69858677f0823951f46671248091be2e">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
</li>
<li>GetTypeSolver()
: <a class="el" href="class_seldon_1_1_iteration.php#ad57613fa7b74a7a7a4e758262957ca0f">Seldon::Iteration&lt; Titer &gt;</a>
</li>
<li>GetVector()
: <a class="el" href="class_seldon_1_1_vector2.php#a3f2341c313e4d593476afe4f94257330">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a3c948376419d25cb11ada8d39c134c5d">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#a1770eeb944cb8eb3d8600a9e5fdba6b5">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ab6992616430aa5bcf17bb251885c0219">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#acc38f6e8f4660dc1c98999b08a467454">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#acd6707f21f36322b042319d4f34ee4c0">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#ad5fd1f0b6dac7bd14faf617633a3a364">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector2.php#a9db5289640f107404e5a4d536c991511">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#a356bbfa79f98b46d264aaf80def94eb0">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a303b32b6b47ed190c93988264fdc7a43">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ae15d81d9568c3d4383af9e4cf81427cd">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#a0d684b2dc3e14d471e6af816d3d11e36">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector2.php#a6955ada9427842710beeaa19ae65e299">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ab47050d774f3ae4f7133c71de7a6f5fb">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
</li>
<li>GetVectorLength()
: <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ad2b29185865f3f2e7a4bf7e43b55c434">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#af8b3604aebbc0b77cb6be1548cbd7be4">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
</li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
