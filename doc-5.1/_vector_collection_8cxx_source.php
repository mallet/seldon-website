<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>vector/VectorCollection.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2010, INRIA</span>
<a name="l00002"></a>00002 <span class="comment">// Author(s): Marc Fragu, Vivien Mallet</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_VECTOR_VECTORCOLLECTION_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 
<a name="l00024"></a>00024 <span class="preprocessor">#include &quot;VectorCollection.hxx&quot;</span>
<a name="l00025"></a>00025 
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 <span class="keyword">namespace </span>Seldon
<a name="l00028"></a>00028 {
<a name="l00029"></a>00029 
<a name="l00030"></a>00030 
<a name="l00032"></a>00032   <span class="comment">// VECTORCOLLECTION //</span>
<a name="l00034"></a>00034 <span class="comment"></span>
<a name="l00035"></a>00035 
<a name="l00036"></a>00036   <span class="comment">/***************</span>
<a name="l00037"></a>00037 <span class="comment">   * CONSTRUCTOR *</span>
<a name="l00038"></a>00038 <span class="comment">   ***************/</span>
<a name="l00039"></a>00039 
<a name="l00040"></a>00040 
<a name="l00042"></a>00042 
<a name="l00045"></a>00045   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00046"></a>00046   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a150f79bf5e3a4c93eb6c3c13ac8e2ea8" title="Methods.">Vector&lt;T, Collection, Allocator&gt;::Vector</a>() throw():
<a name="l00047"></a>00047     Vector_Base&lt;T, Allocator&gt;(), label_map_(), label_vector_()
<a name="l00048"></a>00048   {
<a name="l00049"></a>00049     Nvector_ = 0;
<a name="l00050"></a>00050   }
<a name="l00051"></a>00051 
<a name="l00052"></a>00052 
<a name="l00054"></a>00054 
<a name="l00057"></a>00057   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00058"></a>00058   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a150f79bf5e3a4c93eb6c3c13ac8e2ea8" title="Methods.">Vector&lt;T, Collection, Allocator&gt;::Vector</a>(<span class="keywordtype">int</span> i):
<a name="l00059"></a>00059     Vector_Base&lt;T, Allocator&gt;(0), length_(i), length_sum_(i), vector_(i),
<a name="l00060"></a>00060     label_map_(), label_vector_()
<a name="l00061"></a>00061   {
<a name="l00062"></a>00062     Nvector_ = i;
<a name="l00063"></a>00063     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; i; k++)
<a name="l00064"></a>00064       length_(k) = length_sum_(k) = 0;
<a name="l00065"></a>00065   }
<a name="l00066"></a>00066 
<a name="l00067"></a>00067 
<a name="l00069"></a>00069 
<a name="l00072"></a>00072   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00073"></a>00073   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a150f79bf5e3a4c93eb6c3c13ac8e2ea8" title="Methods.">Vector&lt;T, Collection, Allocator&gt;::</a>
<a name="l00074"></a>00074 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a150f79bf5e3a4c93eb6c3c13ac8e2ea8" title="Methods.">  Vector</a>(<span class="keyword">const</span> Vector&lt;T, Collection, Allocator&gt;&amp; V):
<a name="l00075"></a>00075     Vector_Base&lt;T, Allocator&gt;(V), Nvector_(0)
<a name="l00076"></a>00076   {
<a name="l00077"></a>00077     Copy(V);
<a name="l00078"></a>00078   }
<a name="l00079"></a>00079 
<a name="l00080"></a>00080 
<a name="l00081"></a>00081   <span class="comment">/**************</span>
<a name="l00082"></a>00082 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00083"></a>00083 <span class="comment">   **************/</span>
<a name="l00084"></a>00084 
<a name="l00085"></a>00085 
<a name="l00087"></a>00087 
<a name="l00090"></a>00090   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00091"></a>00091   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#aeee7fe1b1be642339c63037d1245334b" title="Destructor.">Vector&lt;T, Collection, Allocator&gt;::~Vector</a>()
<a name="l00092"></a>00092   {
<a name="l00093"></a>00093     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Nvector_; i++)
<a name="l00094"></a>00094       vector_(i).Nullify();
<a name="l00095"></a>00095     label_map_.clear();
<a name="l00096"></a>00096     label_vector_.clear();
<a name="l00097"></a>00097   }
<a name="l00098"></a>00098 
<a name="l00099"></a>00099 
<a name="l00101"></a>00101 
<a name="l00104"></a>00104   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00105"></a>00105   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a44d2428beb7f6d5a26921ba0158ca415" title="Clears the vector collection.">Vector&lt;T, Collection, Allocator&gt;::Clear</a>()
<a name="l00106"></a>00106   {
<a name="l00107"></a>00107     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Nvector_; i++)
<a name="l00108"></a>00108       vector_(i).Nullify();
<a name="l00109"></a>00109     vector_.Clear();
<a name="l00110"></a>00110     length_sum_.Clear();
<a name="l00111"></a>00111     length_.Clear();
<a name="l00112"></a>00112     Nvector_ = 0;
<a name="l00113"></a>00113     this-&gt;m_ = 0;
<a name="l00114"></a>00114     label_map_.clear();
<a name="l00115"></a>00115     label_vector_.clear();
<a name="l00116"></a>00116   }
<a name="l00117"></a>00117 
<a name="l00118"></a>00118 
<a name="l00120"></a>00120 
<a name="l00123"></a>00123   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00124"></a>00124   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a0110a69b7655a03280256be0746908eb" title="Clears the vector collection.">Vector&lt;T, Collection, Allocator&gt;::Deallocate</a>()
<a name="l00125"></a>00125   {
<a name="l00126"></a>00126     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Nvector_; i++)
<a name="l00127"></a>00127       vector_(i).Clear();
<a name="l00128"></a>00128     vector_.Clear();
<a name="l00129"></a>00129     length_sum_.Clear();
<a name="l00130"></a>00130     length_.Clear();
<a name="l00131"></a>00131     Nvector_ = 0;
<a name="l00132"></a>00132     this-&gt;m_ = 0;
<a name="l00133"></a>00133     label_map_.clear();
<a name="l00134"></a>00134     label_vector_.clear();
<a name="l00135"></a>00135   }
<a name="l00136"></a>00136 
<a name="l00137"></a>00137 
<a name="l00138"></a>00138   <span class="comment">/**********************</span>
<a name="l00139"></a>00139 <span class="comment">   * VECTORS MANAGEMENT *</span>
<a name="l00140"></a>00140 <span class="comment">   **********************/</span>
<a name="l00141"></a>00141 
<a name="l00142"></a>00142 
<a name="l00144"></a>00144 
<a name="l00147"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ac6040d5124519a6bd15177e34381fdd6">00147</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00148"></a>00148   <span class="keyword">template</span> &lt;<span class="keyword">class</span> Allocator0&gt;
<a name="l00149"></a>00149   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Collection, Allocator&gt;</a>
<a name="l00150"></a>00150 <a class="code" href="class_seldon_1_1_vector.php">  ::AddVector</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;<span class="keyword">typename</span> T::value_type,
<a name="l00151"></a>00151               <span class="keyword">typename</span> T::storage, Allocator0&gt;&amp; vector)
<a name="l00152"></a>00152   {
<a name="l00153"></a>00153     Nvector_++;
<a name="l00154"></a>00154     length_.PushBack(0);
<a name="l00155"></a>00155     length_sum_.PushBack(this-&gt;m_);
<a name="l00156"></a>00156 
<a name="l00157"></a>00157     <span class="comment">// Resizes &#39;vector_&#39;.</span>
<a name="l00158"></a>00158     <a class="code" href="class_seldon_1_1_vector.php">collection_type</a> new_vector(Nvector_);
<a name="l00159"></a>00159     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Nvector_ - 1; i++)
<a name="l00160"></a>00160       {
<a name="l00161"></a>00161         new_vector(i).SetData(vector_(i));
<a name="l00162"></a>00162         vector_(i).Nullify();
<a name="l00163"></a>00163       }
<a name="l00164"></a>00164     vector_.Clear();
<a name="l00165"></a>00165     vector_.SetData(new_vector);
<a name="l00166"></a>00166     new_vector.Nullify();
<a name="l00167"></a>00167 
<a name="l00168"></a>00168     <span class="comment">// Adds the new vector.</span>
<a name="l00169"></a>00169     SetVector(Nvector_ - 1, vector);
<a name="l00170"></a>00170   }
<a name="l00171"></a>00171 
<a name="l00172"></a>00172 
<a name="l00174"></a>00174 
<a name="l00178"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a45d7337d75ce747194ac3fee9e5691bd">00178</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00179"></a>00179   <span class="keyword">template</span> &lt;<span class="keyword">class</span> Allocator0&gt;
<a name="l00180"></a>00180   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Collection, Allocator&gt;</a>
<a name="l00181"></a>00181 <a class="code" href="class_seldon_1_1_vector.php">  ::AddVector</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;<span class="keyword">typename</span> T::value_type,
<a name="l00182"></a>00182               <span class="keyword">typename</span> T::storage, Allocator0&gt;&amp; vector,
<a name="l00183"></a>00183               <span class="keywordtype">string</span> name)
<a name="l00184"></a>00184   {
<a name="l00185"></a>00185     AddVector(vector);
<a name="l00186"></a>00186     SetName(Nvector_ - 1, name);
<a name="l00187"></a>00187   }
<a name="l00188"></a>00188 
<a name="l00189"></a>00189 
<a name="l00191"></a>00191 
<a name="l00195"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a9b1565824d3e0725eb1e564cc6bca418">00195</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00196"></a>00196   <span class="keyword">template</span> &lt;<span class="keyword">class</span> Allocator0&gt;
<a name="l00197"></a>00197   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Collection, Allocator&gt;</a>
<a name="l00198"></a>00198 <a class="code" href="class_seldon_1_1_vector.php">  ::SetVector</a>(<span class="keywordtype">int</span> i, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;<span class="keyword">typename</span> T::value_type,
<a name="l00199"></a>00199               <span class="keyword">typename</span> T::storage, Allocator0&gt;&amp; vector)
<a name="l00200"></a>00200   {
<a name="l00201"></a>00201     <span class="keywordtype">int</span> size_difference;
<a name="l00202"></a>00202     size_difference = vector.GetM() - vector_(i).GetM();
<a name="l00203"></a>00203     this-&gt;m_ += size_difference;
<a name="l00204"></a>00204     length_(i) = vector.GetM();
<a name="l00205"></a>00205     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = i; k &lt; Nvector_; k++)
<a name="l00206"></a>00206       length_sum_(k) += size_difference;
<a name="l00207"></a>00207 
<a name="l00208"></a>00208     vector_(i).Nullify();
<a name="l00209"></a>00209     vector_(i).SetData(vector);
<a name="l00210"></a>00210   }
<a name="l00211"></a>00211 
<a name="l00212"></a>00212 
<a name="l00214"></a>00214 
<a name="l00219"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#af5cd74d778152011b4e0e95325198e9c">00219</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00220"></a>00220   <span class="keyword">template</span> &lt;<span class="keyword">class</span> Allocator0&gt;
<a name="l00221"></a>00221   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Collection, Allocator&gt;</a>
<a name="l00222"></a>00222 <a class="code" href="class_seldon_1_1_vector.php">  ::SetVector</a>(<span class="keywordtype">int</span> i, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;<span class="keyword">typename</span> T::value_type,
<a name="l00223"></a>00223               <span class="keyword">typename</span> T::storage, Allocator0&gt;&amp; vector,
<a name="l00224"></a>00224               <span class="keywordtype">string</span> name)
<a name="l00225"></a>00225   {
<a name="l00226"></a>00226     SetVector(i, vector);
<a name="l00227"></a>00227     SetName(i, name);
<a name="l00228"></a>00228   }
<a name="l00229"></a>00229 
<a name="l00230"></a>00230 
<a name="l00232"></a>00232 
<a name="l00236"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#aa020ba820b7421cd6b7536f46e2b242c">00236</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00237"></a>00237   <span class="keyword">template</span> &lt;<span class="keyword">class</span> Allocator0&gt;
<a name="l00238"></a>00238   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Collection, Allocator&gt;</a>
<a name="l00239"></a>00239 <a class="code" href="class_seldon_1_1_vector.php">  ::SetVector</a>(<span class="keywordtype">string</span> name, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;<span class="keyword">typename</span> T::value_type,
<a name="l00240"></a>00240               <span class="keyword">typename</span> T::storage, Allocator0&gt;&amp; vector)
<a name="l00241"></a>00241   {
<a name="l00242"></a>00242     map&lt;string,int&gt;::iterator label_iterator;
<a name="l00243"></a>00243     label_iterator = label_map_.find(name);
<a name="l00244"></a>00244     <span class="keywordflow">if</span> (label_iterator == label_map_.end())
<a name="l00245"></a>00245       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;VectorCollection::SetVector(string name, Vector)&quot;</span>,
<a name="l00246"></a>00246                           <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unknown vector name: \&quot;&quot;</span>) + name + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00247"></a>00247     SetVector(label_iterator-&gt;second, vector);
<a name="l00248"></a>00248   }
<a name="l00249"></a>00249 
<a name="l00250"></a>00250 
<a name="l00252"></a>00252 
<a name="l00256"></a>00256   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00257"></a>00257   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Collection, Allocator&gt;</a>
<a name="l00258"></a>00258 <a class="code" href="class_seldon_1_1_vector.php">  ::SetName</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">string</span> name)
<a name="l00259"></a>00259   {
<a name="l00260"></a>00260 
<a name="l00261"></a>00261 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00262"></a>00262 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Nvector_)
<a name="l00263"></a>00263       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;VectorCollection::SetName(int i, string name)&quot;</span>,
<a name="l00264"></a>00264                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00265"></a>00265                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nvector_ - 1)
<a name="l00266"></a>00266                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00267"></a>00267 <span class="preprocessor">#endif</span>
<a name="l00268"></a>00268 <span class="preprocessor"></span>
<a name="l00269"></a>00269     <span class="keywordflow">if</span> (i &gt;= <span class="keywordtype">int</span>(label_vector_.size()))
<a name="l00270"></a>00270       label_vector_.resize(Nvector_, <span class="stringliteral">&quot;&quot;</span>);
<a name="l00271"></a>00271 
<a name="l00272"></a>00272     <span class="keywordflow">if</span> (label_vector_[i] != <span class="stringliteral">&quot;&quot;</span>)
<a name="l00273"></a>00273       label_map_.erase(label_vector_[i]);
<a name="l00274"></a>00274 
<a name="l00275"></a>00275     label_vector_[i] = name;
<a name="l00276"></a>00276     label_map_[name] = i;
<a name="l00277"></a>00277   }
<a name="l00278"></a>00278 
<a name="l00279"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ab2675e90bf57700a64dd1a11f6a3cda8">00279</a> 
<a name="l00281"></a>00281   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00282"></a>00282   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Collection, Allocator&gt;::Nullify</a>()
<a name="l00283"></a>00283   {
<a name="l00284"></a>00284     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Nvector_; i++)
<a name="l00285"></a>00285       vector_(i).Nullify();
<a name="l00286"></a>00286     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Nvector_; i++)
<a name="l00287"></a>00287       length_(i) = length_sum_(i) = 0;
<a name="l00288"></a>00288   }
<a name="l00289"></a>00289 
<a name="l00290"></a>00290 
<a name="l00291"></a>00291   <span class="comment">/*****************</span>
<a name="l00292"></a>00292 <span class="comment">   * BASIC METHODS *</span>
<a name="l00293"></a>00293 <span class="comment">   *****************/</span>
<a name="l00294"></a>00294 
<a name="l00295"></a>00295 
<a name="l00297"></a>00297 
<a name="l00300"></a>00300   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00301"></a>00301   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Collection, Allocator&gt;::GetM</a>()<span class="keyword"> const</span>
<a name="l00302"></a>00302 <span class="keyword">  </span>{
<a name="l00303"></a>00303     <span class="keywordflow">return</span> this-&gt;m_;
<a name="l00304"></a>00304   }
<a name="l00305"></a>00305 
<a name="l00306"></a>00306 
<a name="l00308"></a>00308 
<a name="l00311"></a>00311   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00312"></a>00312   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a2a2b8d5ad39c79bdcdcd75fa20ae5ae8" title="Returns the total number of elements.">Vector&lt;T, Collection, Allocator&gt;::GetLength</a>()<span class="keyword"> const</span>
<a name="l00313"></a>00313 <span class="keyword">  </span>{
<a name="l00314"></a>00314     <span class="keywordflow">return</span> this-&gt;m_;
<a name="l00315"></a>00315   }
<a name="l00316"></a>00316 
<a name="l00317"></a>00317 
<a name="l00319"></a>00319 
<a name="l00322"></a>00322   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00323"></a>00323   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a35ec50dd32f08ecf3a8ce073a6b4bfff" title="Returns the number of aggregated vectors.">Vector&lt;T, Collection, Allocator&gt;::GetNvector</a>()<span class="keyword"> const</span>
<a name="l00324"></a>00324 <span class="keyword">  </span>{
<a name="l00325"></a>00325     <span class="keywordflow">return</span> Nvector_;
<a name="l00326"></a>00326   }
<a name="l00327"></a>00327 
<a name="l00328"></a>00328 
<a name="l00330"></a>00330 
<a name="l00333"></a>00333   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00334"></a>00334   <span class="keyword">inline</span> <span class="keyword">const</span> Vector&lt;int, VectFull, MallocAlloc&lt;int&gt; &gt;&amp;
<a name="l00335"></a>00335   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#af8b3604aebbc0b77cb6be1548cbd7be4" title="Returns the length vector of the underlying vectors.">Vector&lt;T, Collection, Allocator&gt;::GetVectorLength</a>()<span class="keyword"> const</span>
<a name="l00336"></a>00336 <span class="keyword">  </span>{
<a name="l00337"></a>00337     <span class="keywordflow">return</span> length_;
<a name="l00338"></a>00338   }
<a name="l00339"></a>00339 
<a name="l00340"></a>00340 
<a name="l00342"></a>00342 
<a name="l00345"></a>00345   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00346"></a>00346   <span class="keyword">inline</span> <span class="keyword">const</span> Vector&lt;int, VectFull, MallocAlloc&lt;int&gt; &gt;&amp;
<a name="l00347"></a>00347   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a947fa0953ce1de93ef0de93d23ffba38" title="Returns the cumulative sum of the lengths of the underlying vectors.">Vector&lt;T, Collection, Allocator&gt;::GetLengthSum</a>()<span class="keyword"> const</span>
<a name="l00348"></a>00348 <span class="keyword">  </span>{
<a name="l00349"></a>00349     <span class="keywordflow">return</span> length_sum_;
<a name="l00350"></a>00350   }
<a name="l00351"></a>00351 
<a name="l00352"></a>00352 
<a name="l00354"></a>00354 
<a name="l00357"></a>00357   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00358"></a>00358   <span class="keyword">inline</span> <span class="keyword">typename</span> Vector&lt;T, Collection, Allocator&gt;::collection_reference
<a name="l00359"></a>00359   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ab47050d774f3ae4f7133c71de7a6f5fb" title="Returns the list of vectors.">Vector&lt;T, Collection, Allocator&gt;::GetVector</a>()
<a name="l00360"></a>00360   {
<a name="l00361"></a>00361     <span class="keywordflow">return</span> vector_;
<a name="l00362"></a>00362   }
<a name="l00363"></a>00363 
<a name="l00364"></a>00364 
<a name="l00366"></a>00366 
<a name="l00369"></a>00369   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00370"></a>00370   <span class="keyword">inline</span> <span class="keyword">typename</span> Vector&lt;T, Collection, Allocator&gt;::const_collection_reference
<a name="l00371"></a>00371   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ab47050d774f3ae4f7133c71de7a6f5fb" title="Returns the list of vectors.">Vector&lt;T, Collection, Allocator&gt;::GetVector</a>()<span class="keyword"> const</span>
<a name="l00372"></a>00372 <span class="keyword">  </span>{
<a name="l00373"></a>00373     <span class="keywordflow">return</span> vector_;
<a name="l00374"></a>00374   }
<a name="l00375"></a>00375 
<a name="l00376"></a>00376 
<a name="l00378"></a>00378 
<a name="l00382"></a>00382   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00383"></a>00383   <span class="keyword">inline</span> <span class="keyword">typename</span> Vector&lt;T, Collection, Allocator&gt;::vector_reference
<a name="l00384"></a>00384   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ab47050d774f3ae4f7133c71de7a6f5fb" title="Returns the list of vectors.">Vector&lt;T, Collection, Allocator&gt;::GetVector</a>(<span class="keywordtype">int</span> i)
<a name="l00385"></a>00385   {
<a name="l00386"></a>00386     <span class="keywordflow">return</span> vector_(i);
<a name="l00387"></a>00387   }
<a name="l00388"></a>00388 
<a name="l00389"></a>00389 
<a name="l00391"></a>00391 
<a name="l00395"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ab6992616430aa5bcf17bb251885c0219">00395</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00396"></a>00396   <span class="keyword">inline</span> <span class="keyword">typename</span>
<a name="l00397"></a>00397   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Collection, Allocator&gt;::const_vector_reference</a>
<a name="l00398"></a>00398   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Collection, Allocator&gt;::GetVector</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00399"></a>00399 <span class="keyword">  </span>{
<a name="l00400"></a>00400     <span class="keywordflow">return</span> vector_(i);
<a name="l00401"></a>00401   }
<a name="l00402"></a>00402 
<a name="l00403"></a>00403 
<a name="l00405"></a>00405 
<a name="l00409"></a>00409   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00410"></a>00410   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Collection, Allocator&gt;::vector_reference</a>
<a name="l00411"></a>00411   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Collection, Allocator&gt;::GetVector</a>(<span class="keywordtype">string</span> name)
<a name="l00412"></a>00412   {
<a name="l00413"></a>00413     map&lt;string,int&gt;::iterator label_iterator;
<a name="l00414"></a>00414     label_iterator = label_map_.find(name);
<a name="l00415"></a>00415     <span class="keywordflow">if</span> (label_iterator == label_map_.end())
<a name="l00416"></a>00416       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;VectorCollection::SetVector(string name)&quot;</span>,
<a name="l00417"></a>00417                           <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unknown vector name: \&quot;&quot;</span>) + name + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00418"></a>00418     <span class="keywordflow">return</span> GetVector(label_iterator-&gt;second);
<a name="l00419"></a>00419   }
<a name="l00420"></a>00420 
<a name="l00421"></a>00421 
<a name="l00423"></a>00423 
<a name="l00427"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a00b1884bfdd116ca248368c26ef9299d">00427</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00428"></a>00428   <span class="keyword">inline</span> <span class="keyword">typename</span>
<a name="l00429"></a>00429   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Collection, Allocator&gt;::const_vector_reference</a>
<a name="l00430"></a>00430   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Collection, Allocator&gt;::GetVector</a>(<span class="keywordtype">string</span> name)<span class="keyword"> const</span>
<a name="l00431"></a>00431 <span class="keyword">  </span>{
<a name="l00432"></a>00432     map&lt;string,int&gt;::const_iterator label_iterator;
<a name="l00433"></a>00433     label_iterator = label_map_.find(name);
<a name="l00434"></a>00434     <span class="keywordflow">if</span> (label_iterator == label_map_.end())
<a name="l00435"></a>00435       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;VectorCollection::SetVector(string name)&quot;</span>,
<a name="l00436"></a>00436                           <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unknown vector name: \&quot;&quot;</span>) + name + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00437"></a>00437     <span class="keywordflow">return</span> GetVector(label_iterator-&gt;second);
<a name="l00438"></a>00438   }
<a name="l00439"></a>00439 
<a name="l00440"></a>00440 
<a name="l00441"></a>00441   <span class="comment">/*********************************</span>
<a name="l00442"></a>00442 <span class="comment">   * ELEMENT ACCESS AND ASSIGNMENT *</span>
<a name="l00443"></a>00443 <span class="comment">   *********************************/</span>
<a name="l00444"></a>00444 
<a name="l00445"></a>00445 
<a name="l00447"></a>00447 
<a name="l00451"></a>00451   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00452"></a>00452   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Collection, Allocator&gt;::reference</a>
<a name="l00453"></a>00453   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Collection, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i)
<a name="l00454"></a>00454   {
<a name="l00455"></a>00455 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00456"></a>00456 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00457"></a>00457       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;VectorCollection::operator()&quot;</span>,
<a name="l00458"></a>00458                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00459"></a>00459                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_ - 1)
<a name="l00460"></a>00460                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00461"></a>00461 <span class="preprocessor">#endif</span>
<a name="l00462"></a>00462 <span class="preprocessor"></span>
<a name="l00463"></a>00463     <span class="keywordtype">int</span> j = 0;
<a name="l00464"></a>00464     <span class="keywordflow">while</span> (i &gt;= length_sum_(j))
<a name="l00465"></a>00465       j++;
<a name="l00466"></a>00466     <span class="keywordflow">return</span> (j == 0) ? vector_(j)(i) : vector_(j)(i - length_sum_(j - 1));
<a name="l00467"></a>00467   }
<a name="l00468"></a>00468 
<a name="l00469"></a>00469 
<a name="l00471"></a>00471 
<a name="l00475"></a>00475   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00476"></a>00476   <span class="keyword">inline</span> <span class="keyword">typename</span> Vector&lt;T, Collection, Allocator&gt;::const_reference
<a name="l00477"></a>00477   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ae10c68e40c5383c9d822e8cfd2647e8f" title="Access operator.">Vector&lt;T, Collection, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00478"></a>00478 <span class="keyword">  </span>{
<a name="l00479"></a>00479 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00480"></a>00480 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00481"></a>00481       <span class="keywordflow">throw</span> WrongIndex(<span class="stringliteral">&quot;VectorCollection::operator()&quot;</span>,
<a name="l00482"></a>00482                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00483"></a>00483                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_ - 1)
<a name="l00484"></a>00484                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00485"></a>00485 <span class="preprocessor">#endif</span>
<a name="l00486"></a>00486 <span class="preprocessor"></span>
<a name="l00487"></a>00487     <span class="keywordtype">int</span> j = 0;
<a name="l00488"></a>00488     <span class="keywordflow">while</span> (i &gt;= length_sum_(j))
<a name="l00489"></a>00489       j++;
<a name="l00490"></a>00490     <span class="keywordflow">return</span> (j == 0) ? vector_(j)(i) : vector_(j)(i - length_sum_(j - 1));
<a name="l00491"></a>00491   }
<a name="l00492"></a>00492 
<a name="l00493"></a>00493 
<a name="l00495"></a>00495 
<a name="l00500"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ab33349edcc4ad9275540eaebc0279cc5">00500</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00501"></a>00501   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php" title="Structure for distributed vectors.">Vector&lt;T, Collection, Allocator&gt;</a>&amp;
<a name="l00502"></a>00502   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Collection, Allocator&gt;::operator</a>=
<a name="l00503"></a>00503   (<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php" title="Structure for distributed vectors.">Vector&lt;T, Collection, Allocator&gt;</a>&amp; X)
<a name="l00504"></a>00504   {
<a name="l00505"></a>00505     this-&gt;Copy(X);
<a name="l00506"></a>00506     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00507"></a>00507   }
<a name="l00508"></a>00508 
<a name="l00509"></a>00509 
<a name="l00511"></a>00511 
<a name="l00516"></a>00516   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00517"></a>00517   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Collection, Allocator&gt;</a>
<a name="l00518"></a>00518 <a class="code" href="class_seldon_1_1_vector.php">  ::Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php" title="Structure for distributed vectors.">Vector&lt;T, Collection, Allocator&gt;</a>&amp; X)
<a name="l00519"></a>00519   {
<a name="l00520"></a>00520     Clear();
<a name="l00521"></a>00521     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a35ec50dd32f08ecf3a8ce073a6b4bfff" title="Returns the number of aggregated vectors.">GetNvector</a>(); i++)
<a name="l00522"></a>00522       AddVector(X.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ab47050d774f3ae4f7133c71de7a6f5fb" title="Returns the list of vectors.">GetVector</a>(i));
<a name="l00523"></a>00523     label_map_.insert(X.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a03d0c667f3a6eb4034ab75a1dd133a58" title="Indexes of the inner vectors that have a name.">label_map_</a>.begin(), X.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a03d0c667f3a6eb4034ab75a1dd133a58" title="Indexes of the inner vectors that have a name.">label_map_</a>.end());
<a name="l00524"></a>00524     label_vector_.assign(X.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ad0185be3cff90de10f820787aafe58ba" title="Names associated with the inner vectors.">label_vector_</a>.begin(), X.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ad0185be3cff90de10f820787aafe58ba" title="Names associated with the inner vectors.">label_vector_</a>.end());
<a name="l00525"></a>00525   }
<a name="l00526"></a>00526 
<a name="l00527"></a>00527 
<a name="l00529"></a>00529 
<a name="l00532"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a2279bc596ff24069515312c1332f2d8e">00532</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00533"></a>00533   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l00534"></a>00534   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php" title="Structure for distributed vectors.">Vector&lt;T, Collection, Allocator&gt;</a>&amp;
<a name="l00535"></a>00535   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Collection, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; alpha)
<a name="l00536"></a>00536   {
<a name="l00537"></a>00537     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;Nvector_; i++)
<a name="l00538"></a>00538       this-&gt;vector_(i) *= alpha;
<a name="l00539"></a>00539 
<a name="l00540"></a>00540     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00541"></a>00541   }
<a name="l00542"></a>00542 
<a name="l00543"></a>00543 
<a name="l00545"></a>00545   <span class="comment">// CONVENIENT METHOD //</span>
<a name="l00547"></a>00547 <span class="comment"></span>
<a name="l00548"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a7539ec920e545a23259b7fc5bcda7b25">00548</a> 
<a name="l00550"></a>00550   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00551"></a>00551   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Collection, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00552"></a>00552 <span class="keyword">  </span>{
<a name="l00553"></a>00553     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; GetNvector(); i++)
<a name="l00554"></a>00554       {
<a name="l00555"></a>00555         <span class="keywordflow">if</span> (i &lt; <span class="keywordtype">int</span>(label_vector_.size()) &amp;&amp; label_vector_[i] != <span class="stringliteral">&quot;&quot;</span>)
<a name="l00556"></a>00556           cout &lt;&lt; label_vector_[i] &lt;&lt; <span class="stringliteral">&quot;:&quot;</span> &lt;&lt; endl;
<a name="l00557"></a>00557         <span class="keywordflow">else</span>
<a name="l00558"></a>00558           cout &lt;&lt; <span class="stringliteral">&quot;(noname):&quot;</span> &lt;&lt; endl;
<a name="l00559"></a>00559         vector_(i).Print();
<a name="l00560"></a>00560       }
<a name="l00561"></a>00561     cout &lt;&lt; endl;
<a name="l00562"></a>00562   }
<a name="l00563"></a>00563 
<a name="l00564"></a>00564 
<a name="l00566"></a>00566 
<a name="l00572"></a>00572   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00573"></a>00573   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Collection, Allocator&gt;</a>
<a name="l00574"></a>00574 <a class="code" href="class_seldon_1_1_vector.php">  ::Write</a>(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size = <span class="keyword">true</span>)<span class="keyword"> const</span>
<a name="l00575"></a>00575 <span class="keyword">  </span>{
<a name="l00576"></a>00576     ofstream FileStream;
<a name="l00577"></a>00577     FileStream.open(FileName.c_str());
<a name="l00578"></a>00578 
<a name="l00579"></a>00579 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00580"></a>00580 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00581"></a>00581     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00582"></a>00582       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;Collection&gt;::Write(string FileName)&quot;</span>,
<a name="l00583"></a>00583                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00584"></a>00584 <span class="preprocessor">#endif</span>
<a name="l00585"></a>00585 <span class="preprocessor"></span>
<a name="l00586"></a>00586     this-&gt;Write(FileStream, with_size);
<a name="l00587"></a>00587 
<a name="l00588"></a>00588     FileStream.close();
<a name="l00589"></a>00589   }
<a name="l00590"></a>00590 
<a name="l00591"></a>00591 
<a name="l00593"></a>00593 
<a name="l00599"></a>00599   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00600"></a>00600   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a299f621f50ccfb406add3a63fb5a240c" title="Writes the inner vectors in a file.">Vector&lt;T, Collection, Allocator&gt;</a>
<a name="l00601"></a>00601 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a299f621f50ccfb406add3a63fb5a240c" title="Writes the inner vectors in a file.">  ::Write</a>(ostream&amp; FileStream, <span class="keywordtype">bool</span> with_size = <span class="keyword">true</span>)<span class="keyword"> const</span>
<a name="l00602"></a>00602 <span class="keyword">  </span>{
<a name="l00603"></a>00603 
<a name="l00604"></a>00604 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00605"></a>00605 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00606"></a>00606     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00607"></a>00607       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector&lt;Collection&gt;::Write(ostream&amp; FileStream)&quot;</span>,
<a name="l00608"></a>00608                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00609"></a>00609 <span class="preprocessor">#endif</span>
<a name="l00610"></a>00610 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (with_size)
<a name="l00611"></a>00611       FileStream
<a name="l00612"></a>00612         .write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;Nvector_)),
<a name="l00613"></a>00613                <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00614"></a>00614 
<a name="l00615"></a>00615     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; GetNvector(); i++)
<a name="l00616"></a>00616       vector_(i).Write(FileStream, with_size);
<a name="l00617"></a>00617 
<a name="l00618"></a>00618 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00619"></a>00619 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00620"></a>00620     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00621"></a>00621       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector&lt;Collection&gt;::Write(ostream&amp; FileStream)&quot;</span>,
<a name="l00622"></a>00622                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l00623"></a>00623 <span class="preprocessor">#endif</span>
<a name="l00624"></a>00624 <span class="preprocessor"></span>  }
<a name="l00625"></a>00625 
<a name="l00626"></a>00626 
<a name="l00628"></a>00628 
<a name="l00633"></a>00633   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00634"></a>00634   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a371264abfa98af555c5aacd43426aec2" title="Writes the vector in a file.">Vector&lt;T, Collection, Allocator&gt;</a>
<a name="l00635"></a>00635 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a371264abfa98af555c5aacd43426aec2" title="Writes the vector in a file.">  ::WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00636"></a>00636 <span class="keyword">  </span>{
<a name="l00637"></a>00637     ofstream FileStream;
<a name="l00638"></a>00638     FileStream.precision(cout.precision());
<a name="l00639"></a>00639     FileStream.flags(cout.flags());
<a name="l00640"></a>00640     FileStream.open(FileName.c_str());
<a name="l00641"></a>00641 
<a name="l00642"></a>00642 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00643"></a>00643 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00644"></a>00644     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00645"></a>00645       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector&lt;Collection&gt;::WriteText(string FileName)&quot;</span>,
<a name="l00646"></a>00646                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00647"></a>00647 <span class="preprocessor">#endif</span>
<a name="l00648"></a>00648 <span class="preprocessor"></span>
<a name="l00649"></a>00649     this-&gt;WriteText(FileStream);
<a name="l00650"></a>00650 
<a name="l00651"></a>00651     FileStream.close();
<a name="l00652"></a>00652   }
<a name="l00653"></a>00653 
<a name="l00654"></a>00654 
<a name="l00656"></a>00656 
<a name="l00661"></a>00661   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00662"></a>00662   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a371264abfa98af555c5aacd43426aec2" title="Writes the vector in a file.">Vector&lt;T, Collection, Allocator&gt;</a>
<a name="l00663"></a>00663 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a371264abfa98af555c5aacd43426aec2" title="Writes the vector in a file.">  ::WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00664"></a>00664 <span class="keyword">  </span>{
<a name="l00665"></a>00665 
<a name="l00666"></a>00666 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00667"></a>00667 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00668"></a>00668     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00669"></a>00669       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector&lt;Collection&gt;::Write(ostream&amp; FileStream)&quot;</span>,
<a name="l00670"></a>00670                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00671"></a>00671 <span class="preprocessor">#endif</span>
<a name="l00672"></a>00672 <span class="preprocessor"></span>
<a name="l00673"></a>00673     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; GetNvector(); i++)
<a name="l00674"></a>00674       vector_(i).WriteText(FileStream);
<a name="l00675"></a>00675 
<a name="l00676"></a>00676 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00677"></a>00677 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00678"></a>00678     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00679"></a>00679       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector&lt;Collection&gt;::Write(ostream&amp; FileStream)&quot;</span>,
<a name="l00680"></a>00680                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l00681"></a>00681 <span class="preprocessor">#endif</span>
<a name="l00682"></a>00682 <span class="preprocessor"></span>  }
<a name="l00683"></a>00683 
<a name="l00684"></a>00684 
<a name="l00686"></a>00686 
<a name="l00691"></a>00691   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00692"></a>00692   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a6e0e46939ea2d1b4e33b905b98654d4d" title="Sets the vector from a file.">Vector&lt;T, Collection, Allocator&gt;</a>
<a name="l00693"></a>00693 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a6e0e46939ea2d1b4e33b905b98654d4d" title="Sets the vector from a file.">  ::Read</a>(<span class="keywordtype">string</span> FileName)
<a name="l00694"></a>00694   {
<a name="l00695"></a>00695     ifstream FileStream;
<a name="l00696"></a>00696     FileStream.open(FileName.c_str());
<a name="l00697"></a>00697 
<a name="l00698"></a>00698 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00699"></a>00699 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00700"></a>00700     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00701"></a>00701       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector&lt;Collection&gt;::Read(string FileName)&quot;</span>,
<a name="l00702"></a>00702                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00703"></a>00703 <span class="preprocessor">#endif</span>
<a name="l00704"></a>00704 <span class="preprocessor"></span>
<a name="l00705"></a>00705     this-&gt;Read(FileStream);
<a name="l00706"></a>00706 
<a name="l00707"></a>00707     FileStream.close();
<a name="l00708"></a>00708   }
<a name="l00709"></a>00709 
<a name="l00710"></a>00710 
<a name="l00712"></a>00712 
<a name="l00717"></a>00717   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator &gt;
<a name="l00718"></a>00718   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a6e0e46939ea2d1b4e33b905b98654d4d" title="Sets the vector from a file.">Vector&lt;T, Collection, Allocator&gt;</a>
<a name="l00719"></a>00719 <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a6e0e46939ea2d1b4e33b905b98654d4d" title="Sets the vector from a file.">  ::Read</a>(istream&amp; FileStream)
<a name="l00720"></a>00720   {
<a name="l00721"></a>00721 
<a name="l00722"></a>00722 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00723"></a>00723 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00724"></a>00724     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00725"></a>00725       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector&lt;Collection&gt;::Read(istream&amp; FileStream)&quot;</span>,
<a name="l00726"></a>00726                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00727"></a>00727 <span class="preprocessor">#endif</span>
<a name="l00728"></a>00728 <span class="preprocessor"></span>
<a name="l00729"></a>00729     <span class="keywordtype">int</span>* Nvector = <span class="keyword">new</span> int;
<a name="l00730"></a>00730     FileStream.read(reinterpret_cast&lt;char*&gt;(Nvector), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00731"></a>00731 
<a name="l00732"></a>00732     Clear();
<a name="l00733"></a>00733     T U;
<a name="l00734"></a>00734     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; *Nvector; i++)
<a name="l00735"></a>00735       {
<a name="l00736"></a>00736         U.Read(FileStream);
<a name="l00737"></a>00737         AddVector(U);
<a name="l00738"></a>00738         U.Nullify();
<a name="l00739"></a>00739       }
<a name="l00740"></a>00740 
<a name="l00741"></a>00741     <span class="keyword">delete</span> Nvector;
<a name="l00742"></a>00742 
<a name="l00743"></a>00743 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00744"></a>00744 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l00745"></a>00745     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00746"></a>00746       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector&lt;Collection&gt;::Read(istream&amp; FileStream)&quot;</span>,
<a name="l00747"></a>00747                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l00748"></a>00748 <span class="preprocessor">#endif</span>
<a name="l00749"></a>00749 <span class="preprocessor"></span>
<a name="l00750"></a>00750   }
<a name="l00751"></a>00751 
<a name="l00752"></a>00752 
<a name="l00754"></a>00754 
<a name="l00759"></a>00759   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00760"></a>00760   ostream&amp;  <span class="keyword">operator</span> &lt;&lt;
<a name="l00761"></a>00761   (ostream&amp; out, <span class="keyword">const</span> Vector&lt;T, Collection, Allocator&gt;&amp; V)
<a name="l00762"></a>00762   {
<a name="l00763"></a>00763     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; V.GetNvector() - 1; i++)
<a name="l00764"></a>00764       out &lt;&lt; V.GetVector(i) &lt;&lt; <span class="charliteral">&#39;\t&#39;</span>;
<a name="l00765"></a>00765     <span class="keywordflow">if</span> (V.GetNvector() != 0)
<a name="l00766"></a>00766       out &lt;&lt; V.GetVector(V.GetNvector() - 1);
<a name="l00767"></a>00767     <span class="keywordflow">return</span> out;
<a name="l00768"></a>00768   }
<a name="l00769"></a>00769 
<a name="l00770"></a>00770 
<a name="l00771"></a>00771 } <span class="comment">// namespace Seldon.</span>
<a name="l00772"></a>00772 
<a name="l00773"></a>00773 
<a name="l00774"></a>00774 <span class="preprocessor">#define SELDON_FILE_VECTOR_VECTORCOLLECTION_CXX</span>
<a name="l00775"></a>00775 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
