<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>vector/HeterogeneousCollection.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 INRIA</span>
<a name="l00002"></a>00002 <span class="comment">// Author(s): Marc Fragu, Vivien Mallet</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_VECTOR_HETEROGENEOUSCOLLECTION_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 
<a name="l00024"></a>00024 <span class="preprocessor">#include &quot;HeterogeneousCollection.hxx&quot;</span>
<a name="l00025"></a>00025 
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 <span class="keyword">namespace </span>Seldon
<a name="l00028"></a>00028 {
<a name="l00029"></a>00029 
<a name="l00030"></a>00030 
<a name="l00032"></a>00032   <span class="comment">// VECTOR HETEROGENEOUSCOLLECTION //</span>
<a name="l00034"></a>00034 <span class="comment"></span>
<a name="l00035"></a>00035 
<a name="l00036"></a>00036   <span class="comment">/***************</span>
<a name="l00037"></a>00037 <span class="comment">   * CONSTRUCTOR *</span>
<a name="l00038"></a>00038 <span class="comment">   ***************/</span>
<a name="l00039"></a>00039 
<a name="l00040"></a>00040 
<a name="l00042"></a>00042 
<a name="l00045"></a>00045   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00046"></a>00046   <span class="keyword">inline</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l00047"></a>00047   ::Vector(): Vector_Base&lt;T, Allocator&lt;T&gt; &gt;(), label_map_(),
<a name="l00048"></a>00048               label_vector_()
<a name="l00049"></a>00049   {
<a name="l00050"></a>00050     Nvector_ = 0;
<a name="l00051"></a>00051   }
<a name="l00052"></a>00052 
<a name="l00053"></a>00053 
<a name="l00055"></a>00055 
<a name="l00058"></a>00058   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00059"></a>00059   Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l00060"></a>00060   ::Vector(<span class="keyword">const</span>
<a name="l00061"></a>00061            Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;&amp; V):
<a name="l00062"></a>00062     Vector_Base&lt;T, Allocator&lt;T&gt; &gt;(V), label_map_(), label_vector_()
<a name="l00063"></a>00063   {
<a name="l00064"></a>00064     Copy(V);
<a name="l00065"></a>00065   }
<a name="l00066"></a>00066 
<a name="l00067"></a>00067 
<a name="l00068"></a>00068   <span class="comment">/**************</span>
<a name="l00069"></a>00069 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00070"></a>00070 <span class="comment">   **************/</span>
<a name="l00071"></a>00071 
<a name="l00072"></a>00072 
<a name="l00074"></a>00074 
<a name="l00077"></a>00077   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00078"></a>00078   <span class="keyword">inline</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;::~Vector()
<a name="l00079"></a>00079   {
<a name="l00080"></a>00080     float_dense_c_.Clear();
<a name="l00081"></a>00081     float_sparse_c_.Clear();
<a name="l00082"></a>00082     double_dense_c_.Clear();
<a name="l00083"></a>00083     double_sparse_c_.Clear();
<a name="l00084"></a>00084     collection_.Clear();
<a name="l00085"></a>00085     subvector_.Clear();
<a name="l00086"></a>00086     length_sum_.Clear();
<a name="l00087"></a>00087     length_.Clear();
<a name="l00088"></a>00088     Nvector_ = 0;
<a name="l00089"></a>00089     this-&gt;m_ = 0;
<a name="l00090"></a>00090     label_map_.clear();
<a name="l00091"></a>00091     label_vector_.clear();
<a name="l00092"></a>00092   }
<a name="l00093"></a>00093 
<a name="l00094"></a>00094 
<a name="l00096"></a>00096 
<a name="l00099"></a>00099   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00100"></a>00100   <span class="keyword">inline</span> <span class="keywordtype">void</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l00101"></a>00101   ::Clear()
<a name="l00102"></a>00102   {
<a name="l00103"></a>00103     float_dense_c_.Clear();
<a name="l00104"></a>00104     float_sparse_c_.Clear();
<a name="l00105"></a>00105     double_dense_c_.Clear();
<a name="l00106"></a>00106     double_sparse_c_.Clear();
<a name="l00107"></a>00107     collection_.Clear();
<a name="l00108"></a>00108     subvector_.Clear();
<a name="l00109"></a>00109     length_sum_.Clear();
<a name="l00110"></a>00110     length_.Clear();
<a name="l00111"></a>00111     Nvector_ = 0;
<a name="l00112"></a>00112     this-&gt;m_ = 0;
<a name="l00113"></a>00113     label_map_.clear();
<a name="l00114"></a>00114     label_vector_.clear();
<a name="l00115"></a>00115   }
<a name="l00116"></a>00116 
<a name="l00117"></a>00117 
<a name="l00119"></a>00119 
<a name="l00122"></a>00122   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00123"></a>00123   <span class="keyword">inline</span> <span class="keywordtype">void</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l00124"></a>00124   ::Deallocate()
<a name="l00125"></a>00125   {
<a name="l00126"></a>00126     float_dense_c_.Deallocate();
<a name="l00127"></a>00127     float_sparse_c_.Deallocate();
<a name="l00128"></a>00128     double_dense_c_.Deallocate();
<a name="l00129"></a>00129     double_sparse_c_.Deallocate();
<a name="l00130"></a>00130     collection_.Clear();
<a name="l00131"></a>00131     subvector_.Clear();
<a name="l00132"></a>00132     length_sum_.Clear();
<a name="l00133"></a>00133     length_.Clear();
<a name="l00134"></a>00134     Nvector_ = 0;
<a name="l00135"></a>00135     this-&gt;m_ = 0;
<a name="l00136"></a>00136     label_map_.clear();
<a name="l00137"></a>00137     label_vector_.clear();
<a name="l00138"></a>00138   }
<a name="l00139"></a>00139 
<a name="l00140"></a>00140 
<a name="l00141"></a>00141   <span class="comment">/**********************</span>
<a name="l00142"></a>00142 <span class="comment">   * VECTORS MANAGEMENT *</span>
<a name="l00143"></a>00143 <span class="comment">   **********************/</span>
<a name="l00144"></a>00144 
<a name="l00145"></a>00145 
<a name="l00147"></a>00147 
<a name="l00150"></a>00150   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00151"></a>00151   <span class="keywordtype">void</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l00152"></a>00152   ::AddVector(<span class="keyword">const</span> Vector&lt;float, VectFull, Allocator&lt;float&gt; &gt;&amp; vector)
<a name="l00153"></a>00153   {
<a name="l00154"></a>00154     Nvector_++;
<a name="l00155"></a>00155     <span class="keywordtype">int</span> m = vector.GetM();
<a name="l00156"></a>00156     this-&gt;m_+= m;
<a name="l00157"></a>00157     length_.PushBack(m);
<a name="l00158"></a>00158     length_sum_.PushBack(this-&gt;m_);
<a name="l00159"></a>00159     collection_.PushBack(0);
<a name="l00160"></a>00160     subvector_.PushBack(float_dense_c_.GetNvector());
<a name="l00161"></a>00161     float_dense_c_.AddVector(vector);
<a name="l00162"></a>00162   }
<a name="l00163"></a>00163 
<a name="l00164"></a>00164 
<a name="l00166"></a>00166 
<a name="l00169"></a>00169   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00170"></a>00170   <span class="keywordtype">void</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l00171"></a>00171   ::AddVector(<span class="keyword">const</span> Vector&lt;float, VectSparse, Allocator&lt;float&gt; &gt;&amp; vector)
<a name="l00172"></a>00172   {
<a name="l00173"></a>00173     Nvector_++;
<a name="l00174"></a>00174     <span class="keywordtype">int</span> m = vector.GetM();
<a name="l00175"></a>00175     this-&gt;m_+= m;
<a name="l00176"></a>00176     length_.PushBack(m);
<a name="l00177"></a>00177     length_sum_.PushBack(this-&gt;m_);
<a name="l00178"></a>00178     collection_.PushBack(1);
<a name="l00179"></a>00179     subvector_.PushBack(float_sparse_c_.GetNvector());
<a name="l00180"></a>00180     float_sparse_c_.AddVector(vector);
<a name="l00181"></a>00181   }
<a name="l00182"></a>00182 
<a name="l00183"></a>00183 
<a name="l00185"></a>00185 
<a name="l00188"></a>00188   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00189"></a>00189   <span class="keywordtype">void</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l00190"></a>00190   ::AddVector(<span class="keyword">const</span> Vector&lt;double, VectFull, Allocator&lt;double&gt; &gt;&amp; vector)
<a name="l00191"></a>00191   {
<a name="l00192"></a>00192     Nvector_++;
<a name="l00193"></a>00193     <span class="keywordtype">int</span> m = vector.GetM();
<a name="l00194"></a>00194     this-&gt;m_+= m;
<a name="l00195"></a>00195     length_.PushBack(m);
<a name="l00196"></a>00196     length_sum_.PushBack(this-&gt;m_);
<a name="l00197"></a>00197     collection_.PushBack(2);
<a name="l00198"></a>00198     subvector_.PushBack(double_dense_c_.GetNvector());
<a name="l00199"></a>00199     double_dense_c_.AddVector(vector);
<a name="l00200"></a>00200   }
<a name="l00201"></a>00201 
<a name="l00202"></a>00202 
<a name="l00204"></a>00204 
<a name="l00207"></a>00207   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00208"></a>00208   <span class="keywordtype">void</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l00209"></a>00209   ::AddVector(<span class="keyword">const</span> Vector&lt;double, VectSparse, Allocator&lt;double&gt; &gt;&amp; vector)
<a name="l00210"></a>00210   {
<a name="l00211"></a>00211     Nvector_++;
<a name="l00212"></a>00212     <span class="keywordtype">int</span> m = vector.GetM();
<a name="l00213"></a>00213     this-&gt;m_+= m;
<a name="l00214"></a>00214     length_.PushBack(m);
<a name="l00215"></a>00215     length_sum_.PushBack(this-&gt;m_);
<a name="l00216"></a>00216     collection_.PushBack(3);
<a name="l00217"></a>00217     subvector_.PushBack(double_sparse_c_.GetNvector());
<a name="l00218"></a>00218     double_sparse_c_.AddVector(vector);
<a name="l00219"></a>00219   }
<a name="l00220"></a>00220 
<a name="l00221"></a>00221 
<a name="l00223"></a>00223 
<a name="l00227"></a><a class="code" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#afcca9799bf261c112c99e806b8907884">00227</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00228"></a>00228   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Storage0, <span class="keyword">class</span> Allocator0&gt;
<a name="l00229"></a>00229   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;
<a name="l00230"></a>00230   ::AddVector(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T0, Storage0, Allocator0&gt;</a>&amp; vector, <span class="keywordtype">string</span> name)
<a name="l00231"></a>00231   {
<a name="l00232"></a>00232     AddVector(vector);
<a name="l00233"></a>00233     SetName(Nvector_ - 1, name);
<a name="l00234"></a>00234   }
<a name="l00235"></a>00235 
<a name="l00236"></a>00236 
<a name="l00238"></a>00238 
<a name="l00242"></a>00242   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00243"></a>00243   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;
<a name="l00244"></a>00244   ::SetVector(<span class="keywordtype">int</span> i, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;float, VectFull, Allocator&lt;float&gt;</a> &gt;&amp; vector)
<a name="l00245"></a>00245   {
<a name="l00246"></a>00246 
<a name="l00247"></a>00247 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00248"></a>00248 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Nvector_)
<a name="l00249"></a>00249       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00250"></a>00250                        <span class="stringliteral">&quot;::SetVector(int i, Vector&lt;float, VectFull&gt;)&quot;</span>,
<a name="l00251"></a>00251                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00252"></a>00252                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nvector_ - 1)
<a name="l00253"></a>00253                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00254"></a>00254 <span class="preprocessor">#endif</span>
<a name="l00255"></a>00255 <span class="preprocessor"></span>
<a name="l00256"></a>00256     <span class="keywordflow">if</span> (collection_(i) != 0)
<a name="l00257"></a>00257       <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00258"></a>00258                     <span class="stringliteral">&quot;::SetVector(int i, Vector&lt;float, VectFull&gt;)&quot;</span>,
<a name="l00259"></a>00259                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;-th inner vector &quot;</span>
<a name="l00260"></a>00260                     <span class="stringliteral">&quot;should be of type &quot;</span> + GetType(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00261"></a>00261 
<a name="l00262"></a>00262     <span class="keywordtype">int</span> size_difference;
<a name="l00263"></a>00263     size_difference = vector.GetM() - length_(i);
<a name="l00264"></a>00264     this-&gt;m_ += size_difference;
<a name="l00265"></a>00265     length_(i) = vector.GetM();
<a name="l00266"></a>00266     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = i; k &lt; Nvector_; k++)
<a name="l00267"></a>00267       length_sum_(k) += size_difference;
<a name="l00268"></a>00268     float_dense_c_.SetVector(subvector_(i), vector);
<a name="l00269"></a>00269   }
<a name="l00270"></a>00270 
<a name="l00271"></a>00271 
<a name="l00273"></a>00273 
<a name="l00277"></a>00277   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00278"></a>00278   <span class="keywordtype">void</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l00279"></a>00279   ::SetVector(<span class="keywordtype">int</span> i,
<a name="l00280"></a>00280               <span class="keyword">const</span> Vector&lt;float, VectSparse, Allocator&lt;float&gt; &gt;&amp; vector)
<a name="l00281"></a>00281   {
<a name="l00282"></a>00282 
<a name="l00283"></a>00283 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00284"></a>00284 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Nvector_)
<a name="l00285"></a>00285       <span class="keywordflow">throw</span> WrongIndex(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00286"></a>00286                        <span class="stringliteral">&quot;::SetVector(int i, Vector&lt;float, VectSparse&gt;)&quot;</span>,
<a name="l00287"></a>00287                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00288"></a>00288                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nvector_ - 1)
<a name="l00289"></a>00289                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00290"></a>00290 <span class="preprocessor">#endif</span>
<a name="l00291"></a>00291 <span class="preprocessor"></span>
<a name="l00292"></a>00292     <span class="keywordflow">if</span> (collection_(i) != 1)
<a name="l00293"></a>00293       WrongArgument(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00294"></a>00294                     <span class="stringliteral">&quot;::SetVector(int i, Vector&lt;float, VectSparse&gt;)&quot;</span>,
<a name="l00295"></a>00295                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;-th inner vector &quot;</span>
<a name="l00296"></a>00296                     <span class="stringliteral">&quot;should be of type &quot;</span> + GetType(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00297"></a>00297 
<a name="l00298"></a>00298     <span class="keywordtype">int</span> size_difference;
<a name="l00299"></a>00299     size_difference = vector.GetM() - length_(i);
<a name="l00300"></a>00300     this-&gt;m_ += size_difference;
<a name="l00301"></a>00301     length_(i) = vector.GetM();
<a name="l00302"></a>00302     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = i; k &lt; Nvector_; k++)
<a name="l00303"></a>00303       length_sum_(k) += size_difference;
<a name="l00304"></a>00304     float_sparse_c_.SetVector(subvector_(i), vector);
<a name="l00305"></a>00305   }
<a name="l00306"></a>00306 
<a name="l00307"></a>00307 
<a name="l00309"></a>00309 
<a name="l00313"></a>00313   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00314"></a>00314   <span class="keywordtype">void</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l00315"></a>00315   ::SetVector(<span class="keywordtype">int</span> i,
<a name="l00316"></a>00316               <span class="keyword">const</span> Vector&lt;double, VectFull, Allocator&lt;double&gt; &gt;&amp; vector)
<a name="l00317"></a>00317   {
<a name="l00318"></a>00318 
<a name="l00319"></a>00319 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00320"></a>00320 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Nvector_)
<a name="l00321"></a>00321       <span class="keywordflow">throw</span> WrongIndex(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00322"></a>00322                        <span class="stringliteral">&quot;::SetVector(int i, Vector&lt;double, VectFull&gt;)&quot;</span>,
<a name="l00323"></a>00323                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00324"></a>00324                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nvector_ - 1)
<a name="l00325"></a>00325                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00326"></a>00326 <span class="preprocessor">#endif</span>
<a name="l00327"></a>00327 <span class="preprocessor"></span>
<a name="l00328"></a>00328     <span class="keywordflow">if</span> (collection_(i) != 2)
<a name="l00329"></a>00329       WrongArgument(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00330"></a>00330                     <span class="stringliteral">&quot;::SetVector(int i, Vector&lt;double, VectFull&gt;)&quot;</span>,
<a name="l00331"></a>00331                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;-th inner vector &quot;</span>
<a name="l00332"></a>00332                     <span class="stringliteral">&quot;should be of type &quot;</span> + GetType(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00333"></a>00333 
<a name="l00334"></a>00334     <span class="keywordtype">int</span> size_difference;
<a name="l00335"></a>00335     size_difference = vector.GetM() - length_(i);
<a name="l00336"></a>00336     this-&gt;m_ += size_difference;
<a name="l00337"></a>00337     length_(i) = vector.GetM();
<a name="l00338"></a>00338     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = i; k &lt; Nvector_; k++)
<a name="l00339"></a>00339       length_sum_(k) += size_difference;
<a name="l00340"></a>00340     double_dense_c_.SetVector(subvector_(i), vector);
<a name="l00341"></a>00341   }
<a name="l00342"></a>00342 
<a name="l00343"></a>00343 
<a name="l00345"></a>00345 
<a name="l00349"></a>00349   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00350"></a>00350   <span class="keywordtype">void</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l00351"></a>00351   ::SetVector(<span class="keywordtype">int</span> i,
<a name="l00352"></a>00352               <span class="keyword">const</span> Vector&lt;double, VectSparse, Allocator&lt;double&gt; &gt;&amp; vector)
<a name="l00353"></a>00353   {
<a name="l00354"></a>00354 
<a name="l00355"></a>00355 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00356"></a>00356 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Nvector_)
<a name="l00357"></a>00357       <span class="keywordflow">throw</span> WrongIndex(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00358"></a>00358                        <span class="stringliteral">&quot;::SetVector(int i, Vector&lt;double, VectSparse&gt;)&quot;</span>,
<a name="l00359"></a>00359                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00360"></a>00360                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nvector_ - 1)
<a name="l00361"></a>00361                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00362"></a>00362 <span class="preprocessor">#endif</span>
<a name="l00363"></a>00363 <span class="preprocessor"></span>
<a name="l00364"></a>00364     <span class="keywordflow">if</span> (collection_(i) != 3)
<a name="l00365"></a>00365       WrongArgument(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00366"></a>00366                     <span class="stringliteral">&quot;::SetVector(int i, Vector&lt;double, VectSparse&gt;)&quot;</span>,
<a name="l00367"></a>00367                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;-th inner vector &quot;</span>
<a name="l00368"></a>00368                     <span class="stringliteral">&quot;should be of type &quot;</span> + GetType(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00369"></a>00369 
<a name="l00370"></a>00370     <span class="keywordtype">int</span> size_difference;
<a name="l00371"></a>00371     size_difference = vector.GetM() - length_(i);
<a name="l00372"></a>00372     this-&gt;m_ += size_difference;
<a name="l00373"></a>00373     length_(i) = vector.GetM();
<a name="l00374"></a>00374     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = i; k &lt; Nvector_; k++)
<a name="l00375"></a>00375       length_sum_(k) += size_difference;
<a name="l00376"></a>00376     double_sparse_c_.SetVector(subvector_(i), vector);
<a name="l00377"></a>00377   }
<a name="l00378"></a>00378 
<a name="l00379"></a>00379 
<a name="l00381"></a>00381 
<a name="l00386"></a><a class="code" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a96adbda8dc019e9d87778deb12e93b39">00386</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00387"></a>00387   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Storage0, <span class="keyword">class</span> Allocator0&gt;
<a name="l00388"></a>00388   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;
<a name="l00389"></a>00389   ::SetVector(<span class="keywordtype">int</span> i, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T0, Storage0, Allocator0&gt;</a>&amp; vector,
<a name="l00390"></a>00390               <span class="keywordtype">string</span> name)
<a name="l00391"></a>00391   {
<a name="l00392"></a>00392     SetVector(i, vector);
<a name="l00393"></a>00393     SetName(i, name);
<a name="l00394"></a>00394   }
<a name="l00395"></a>00395 
<a name="l00396"></a>00396 
<a name="l00398"></a>00398 
<a name="l00402"></a><a class="code" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#adf98501c23f89625bb971e10e44b605f">00402</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00403"></a>00403   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Storage0, <span class="keyword">class</span> Allocator0&gt;
<a name="l00404"></a>00404   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;
<a name="l00405"></a>00405   ::SetVector(<span class="keywordtype">string</span> name, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T0, Storage0, Allocator0&gt;</a>&amp; vector)
<a name="l00406"></a>00406   {
<a name="l00407"></a>00407     map&lt;string,int&gt;::iterator label_iterator;
<a name="l00408"></a>00408     label_iterator = label_map_.find(name);
<a name="l00409"></a>00409     <span class="keywordflow">if</span> (label_iterator == label_map_.end())
<a name="l00410"></a>00410       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00411"></a>00411                           <span class="stringliteral">&quot;::SetVector(string name, Vector)&quot;</span>,
<a name="l00412"></a>00412                           <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unknown vector name: \&quot;&quot;</span>) + name + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00413"></a>00413     SetVector(label_iterator-&gt;second, vector);
<a name="l00414"></a>00414   }
<a name="l00415"></a>00415 
<a name="l00416"></a>00416 
<a name="l00418"></a>00418 
<a name="l00422"></a>00422   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00423"></a>00423   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;
<a name="l00424"></a>00424   ::SetName(<span class="keywordtype">int</span> i, <span class="keywordtype">string</span> name)
<a name="l00425"></a>00425   {
<a name="l00426"></a>00426 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00427"></a>00427 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Nvector_)
<a name="l00428"></a>00428       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00429"></a>00429                        <span class="stringliteral">&quot;::SetVector(int i, string name)&quot;</span>,
<a name="l00430"></a>00430                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00431"></a>00431                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nvector_ - 1)
<a name="l00432"></a>00432                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00433"></a>00433 <span class="preprocessor">#endif</span>
<a name="l00434"></a>00434 <span class="preprocessor"></span>
<a name="l00435"></a>00435     <span class="keywordflow">if</span> (i &gt;= <span class="keywordtype">int</span>(label_vector_.size()))
<a name="l00436"></a>00436       label_vector_.resize(Nvector_, <span class="stringliteral">&quot;&quot;</span>);
<a name="l00437"></a>00437 
<a name="l00438"></a>00438     <span class="keywordflow">if</span> (label_vector_[i] != <span class="stringliteral">&quot;&quot;</span>)
<a name="l00439"></a>00439       label_map_.erase(label_vector_[i]);
<a name="l00440"></a>00440 
<a name="l00441"></a>00441     label_vector_[i] = name;
<a name="l00442"></a>00442     label_map_[name] = i;
<a name="l00443"></a>00443   }
<a name="l00444"></a>00444 
<a name="l00445"></a><a class="code" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ada438f06bc34a5a98b630f07e0e80346">00445</a> 
<a name="l00447"></a>00447   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00448"></a>00448   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;::Nullify()
<a name="l00449"></a>00449   {
<a name="l00450"></a>00450     Clear();
<a name="l00451"></a>00451   }
<a name="l00452"></a>00452 
<a name="l00453"></a>00453 
<a name="l00454"></a>00454   <span class="comment">/*****************</span>
<a name="l00455"></a>00455 <span class="comment">   * BASIC METHODS *</span>
<a name="l00456"></a>00456 <span class="comment">   *****************/</span>
<a name="l00457"></a>00457 
<a name="l00458"></a>00458 
<a name="l00460"></a>00460 
<a name="l00463"></a>00463   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00464"></a>00464   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;::GetM()
<a name="l00465"></a>00465     <span class="keyword">const</span>
<a name="l00466"></a>00466   {
<a name="l00467"></a>00467     <span class="keywordflow">return</span> this-&gt;m_;
<a name="l00468"></a>00468   }
<a name="l00469"></a>00469 
<a name="l00470"></a>00470 
<a name="l00472"></a>00472 
<a name="l00475"></a>00475   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00476"></a>00476   <span class="keyword">inline</span> <span class="keywordtype">int</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l00477"></a>00477   ::GetLength() <span class="keyword">const</span>
<a name="l00478"></a>00478   {
<a name="l00479"></a>00479     <span class="keywordflow">return</span> this-&gt;m_;
<a name="l00480"></a>00480   }
<a name="l00481"></a>00481 
<a name="l00482"></a>00482 
<a name="l00484"></a>00484 
<a name="l00487"></a>00487   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00488"></a>00488   <span class="keyword">inline</span> <span class="keywordtype">int</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l00489"></a>00489   ::GetNvector() <span class="keyword">const</span>
<a name="l00490"></a>00490   {
<a name="l00491"></a>00491     <span class="keywordflow">return</span> Nvector_;
<a name="l00492"></a>00492   }
<a name="l00493"></a>00493 
<a name="l00494"></a>00494 
<a name="l00496"></a>00496 
<a name="l00499"></a><a class="code" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ad2b29185865f3f2e7a4bf7e43b55c434">00499</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00500"></a>00500   <span class="keyword">inline</span> <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, MallocAlloc&lt;int&gt;</a> &gt;&amp;
<a name="l00501"></a>00501   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;
<a name="l00502"></a>00502   ::GetVectorLength() <span class="keyword">const</span>
<a name="l00503"></a>00503   {
<a name="l00504"></a>00504     <span class="keywordflow">return</span> length_;
<a name="l00505"></a>00505   }
<a name="l00506"></a>00506 
<a name="l00507"></a>00507 
<a name="l00509"></a>00509 
<a name="l00512"></a><a class="code" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a750b9d392301df88e3cdb76cf11f87a0">00512</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00513"></a>00513   <span class="keyword">inline</span> <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, MallocAlloc&lt;int&gt;</a> &gt;&amp;
<a name="l00514"></a>00514   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;
<a name="l00515"></a>00515   ::GetLengthSum() <span class="keyword">const</span>
<a name="l00516"></a>00516   {
<a name="l00517"></a>00517     <span class="keywordflow">return</span> length_sum_;
<a name="l00518"></a>00518   }
<a name="l00519"></a>00519 
<a name="l00520"></a>00520 
<a name="l00522"></a>00522 
<a name="l00525"></a><a class="code" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a5bdd2a3d741868fc687c2e62388dd348">00525</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00526"></a>00526   <span class="keyword">inline</span> <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, MallocAlloc&lt;int&gt;</a> &gt;&amp;
<a name="l00527"></a>00527   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;
<a name="l00528"></a>00528   ::GetCollectionIndex() <span class="keyword">const</span>
<a name="l00529"></a>00529   {
<a name="l00530"></a>00530     <span class="keywordflow">return</span> collection_;
<a name="l00531"></a>00531   }
<a name="l00532"></a>00532 
<a name="l00533"></a>00533 
<a name="l00535"></a>00535 
<a name="l00539"></a><a class="code" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a6872e6bac0e998a5b2e7dbf2d9b1807f">00539</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00540"></a>00540   <span class="keyword">inline</span> <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, MallocAlloc&lt;int&gt;</a> &gt;&amp;
<a name="l00541"></a>00541   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;
<a name="l00542"></a>00542   ::GetSubvectorIndex() <span class="keyword">const</span>
<a name="l00543"></a>00543   {
<a name="l00544"></a>00544     <span class="keywordflow">return</span> length_sum_;
<a name="l00545"></a>00545   }
<a name="l00546"></a>00546 
<a name="l00547"></a>00547 
<a name="l00549"></a>00549 
<a name="l00552"></a><a class="code" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#addef6ea1f486b2b836b2ca54f8273ccb">00552</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00553"></a>00553   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;<a class="code" href="class_seldon_1_1_vector.php"></a>
<a name="l00554"></a>00554 <a class="code" href="class_seldon_1_1_vector.php">  ::float_dense_c</a>&amp;
<a name="l00555"></a>00555   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;::GetFloatDense()
<a name="l00556"></a>00556   {
<a name="l00557"></a>00557     <span class="keywordflow">return</span> float_dense_c_;
<a name="l00558"></a>00558   }
<a name="l00559"></a>00559 
<a name="l00560"></a>00560 
<a name="l00562"></a>00562 
<a name="l00565"></a>00565   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00566"></a>00566   <span class="keyword">inline</span> <span class="keyword">const</span>
<a name="l00567"></a><a class="code" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#aace5627f0d92e61ec0e99a513f4570c1">00567</a>   <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;<a class="code" href="class_seldon_1_1_vector.php"></a>
<a name="l00568"></a>00568 <a class="code" href="class_seldon_1_1_vector.php">  ::float_dense_c</a>&amp;
<a name="l00569"></a>00569   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;
<a name="l00570"></a>00570   ::GetFloatDense() <span class="keyword">const</span>
<a name="l00571"></a>00571   {
<a name="l00572"></a>00572     <span class="keywordflow">return</span> float_dense_c_;
<a name="l00573"></a>00573   }
<a name="l00574"></a>00574 
<a name="l00575"></a>00575 
<a name="l00577"></a>00577 
<a name="l00580"></a><a class="code" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a919fc21cc5754175f976df5e1c62f1aa">00580</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00581"></a>00581   <span class="keyword">inline</span> <span class="keyword">typename</span>
<a name="l00582"></a>00582   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;<a class="code" href="class_seldon_1_1_vector.php">::float_sparse_c</a>&amp;
<a name="l00583"></a>00583   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;::GetFloatSparse()
<a name="l00584"></a>00584   {
<a name="l00585"></a>00585     <span class="keywordflow">return</span> float_sparse_c_;
<a name="l00586"></a>00586   }
<a name="l00587"></a>00587 
<a name="l00588"></a>00588 
<a name="l00590"></a>00590 
<a name="l00593"></a>00593   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00594"></a>00594   <span class="keyword">inline</span> <span class="keyword">const</span>
<a name="l00595"></a><a class="code" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a93b4ed54472467eeb6529334ee4c90d2">00595</a>   <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;<a class="code" href="class_seldon_1_1_vector.php"></a>
<a name="l00596"></a>00596 <a class="code" href="class_seldon_1_1_vector.php">  ::float_sparse_c</a>&amp;
<a name="l00597"></a>00597   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;
<a name="l00598"></a>00598   ::GetFloatSparse() <span class="keyword">const</span>
<a name="l00599"></a>00599   {
<a name="l00600"></a>00600     <span class="keywordflow">return</span> float_sparse_c_;
<a name="l00601"></a>00601   }
<a name="l00602"></a>00602 
<a name="l00603"></a>00603 
<a name="l00605"></a>00605 
<a name="l00608"></a><a class="code" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a48e3d61c043a065c53b41470e706dfee">00608</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00609"></a>00609   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;<a class="code" href="class_seldon_1_1_vector.php"></a>
<a name="l00610"></a>00610 <a class="code" href="class_seldon_1_1_vector.php">  ::double_dense_c</a>&amp;
<a name="l00611"></a>00611   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;::GetDoubleDense()
<a name="l00612"></a>00612   {
<a name="l00613"></a>00613     <span class="keywordflow">return</span> double_dense_c_;
<a name="l00614"></a>00614   }
<a name="l00615"></a>00615 
<a name="l00616"></a>00616 
<a name="l00618"></a>00618 
<a name="l00621"></a>00621   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00622"></a>00622   <span class="keyword">inline</span>
<a name="l00623"></a><a class="code" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#abfef62ba1ded1d080a8d384eef37e85f">00623</a>   <span class="keyword">const</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;<a class="code" href="class_seldon_1_1_vector.php"></a>
<a name="l00624"></a>00624 <a class="code" href="class_seldon_1_1_vector.php">  ::double_dense_c</a>&amp;
<a name="l00625"></a>00625   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;
<a name="l00626"></a>00626   ::GetDoubleDense() <span class="keyword">const</span>
<a name="l00627"></a>00627   {
<a name="l00628"></a>00628     <span class="keywordflow">return</span> double_dense_c_;
<a name="l00629"></a>00629   }
<a name="l00630"></a>00630 
<a name="l00631"></a>00631 
<a name="l00633"></a>00633 
<a name="l00636"></a><a class="code" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a1add5fecb61b4984003fc903f5e4a71b">00636</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00637"></a>00637   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;<a class="code" href="class_seldon_1_1_vector.php"></a>
<a name="l00638"></a>00638 <a class="code" href="class_seldon_1_1_vector.php">  ::double_sparse_c</a>&amp;
<a name="l00639"></a>00639   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;::GetDoubleSparse()
<a name="l00640"></a>00640   {
<a name="l00641"></a>00641     <span class="keywordflow">return</span> double_sparse_c_;
<a name="l00642"></a>00642   }
<a name="l00643"></a>00643 
<a name="l00644"></a>00644 
<a name="l00646"></a>00646 
<a name="l00649"></a>00649   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00650"></a>00650   <span class="keyword">inline</span>
<a name="l00651"></a><a class="code" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ac499cedccfb785f37d1e1aec3869300d">00651</a>   <span class="keyword">const</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;<a class="code" href="class_seldon_1_1_vector.php"></a>
<a name="l00652"></a>00652 <a class="code" href="class_seldon_1_1_vector.php">  ::double_sparse_c</a>&amp;
<a name="l00653"></a>00653   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;
<a name="l00654"></a>00654   ::GetDoubleSparse() <span class="keyword">const</span>
<a name="l00655"></a>00655   {
<a name="l00656"></a>00656     <span class="keywordflow">return</span> double_sparse_c_;
<a name="l00657"></a>00657   }
<a name="l00658"></a>00658 
<a name="l00659"></a>00659 
<a name="l00661"></a>00661 
<a name="l00665"></a>00665   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00666"></a>00666   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;
<a name="l00667"></a>00667   ::GetVector(<span class="keywordtype">int</span> i, <span class="keyword">typename</span>
<a name="l00668"></a>00668               <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;
<a name="l00669"></a>00669               ::float_dense_v&amp; vector) <span class="keyword">const</span>
<a name="l00670"></a>00670   {
<a name="l00671"></a>00671 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00672"></a>00672 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Nvector_)
<a name="l00673"></a>00673       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00674"></a>00674                        <span class="stringliteral">&quot;::GetVector(int i, Vector&lt;float, VectFull&gt;&amp;)&quot;</span>,
<a name="l00675"></a>00675                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00676"></a>00676                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nvector_ - 1)
<a name="l00677"></a>00677                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00678"></a>00678 <span class="preprocessor">#endif</span>
<a name="l00679"></a>00679 <span class="preprocessor"></span>
<a name="l00680"></a>00680     <span class="keywordflow">if</span> (collection_(i) != 0)
<a name="l00681"></a>00681       <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00682"></a>00682                     <span class="stringliteral">&quot;::GetVector(int i, Vector&lt;float, VectFull&gt;&amp;)&quot;</span>,
<a name="l00683"></a>00683                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;-th inner vector &quot;</span>
<a name="l00684"></a>00684                     <span class="stringliteral">&quot;is of type &quot;</span> + GetType(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00685"></a>00685 
<a name="l00686"></a>00686     vector.SetData(float_dense_c_.GetVector(subvector_(i)));
<a name="l00687"></a>00687   }
<a name="l00688"></a>00688 
<a name="l00689"></a>00689 
<a name="l00691"></a>00691 
<a name="l00695"></a>00695   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00696"></a>00696   <span class="keyword">inline</span> <span class="keywordtype">void</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l00697"></a>00697   ::GetVector(<span class="keywordtype">int</span> i, <span class="keyword">typename</span>
<a name="l00698"></a>00698               Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l00699"></a>00699               ::float_sparse_v&amp; vector) <span class="keyword">const</span>
<a name="l00700"></a>00700   {
<a name="l00701"></a>00701 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00702"></a>00702 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Nvector_)
<a name="l00703"></a>00703       <span class="keywordflow">throw</span> WrongIndex(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00704"></a>00704                        <span class="stringliteral">&quot;::GetVector(int i, Vector&lt;float, VectSparse&gt;&amp;)&quot;</span>,
<a name="l00705"></a>00705                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00706"></a>00706                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nvector_ - 1)
<a name="l00707"></a>00707                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00708"></a>00708 <span class="preprocessor">#endif</span>
<a name="l00709"></a>00709 <span class="preprocessor"></span>
<a name="l00710"></a>00710     <span class="keywordflow">if</span> (collection_(i) != 1)
<a name="l00711"></a>00711       WrongArgument(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00712"></a>00712                     <span class="stringliteral">&quot;::GetVector(int i, Vector&lt;float, VectSparse&gt;&amp;)&quot;</span>,
<a name="l00713"></a>00713                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;-th inner vector &quot;</span>
<a name="l00714"></a>00714                     <span class="stringliteral">&quot;is of type &quot;</span> + GetType(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00715"></a>00715 
<a name="l00716"></a>00716     vector.SetData(float_sparse_c_.GetVector(subvector_(i)));
<a name="l00717"></a>00717   }
<a name="l00718"></a>00718 
<a name="l00719"></a>00719 
<a name="l00721"></a>00721 
<a name="l00725"></a>00725   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00726"></a>00726   <span class="keyword">inline</span> <span class="keywordtype">void</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l00727"></a>00727   ::GetVector(<span class="keywordtype">int</span> i, <span class="keyword">typename</span>
<a name="l00728"></a>00728               Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l00729"></a>00729               ::double_dense_v&amp; vector) <span class="keyword">const</span>
<a name="l00730"></a>00730   {
<a name="l00731"></a>00731 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00732"></a>00732 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Nvector_)
<a name="l00733"></a>00733       <span class="keywordflow">throw</span> WrongIndex(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00734"></a>00734                        <span class="stringliteral">&quot;::GetVector(int i, Vector&lt;double, VectDense&gt;&amp;)&quot;</span>,
<a name="l00735"></a>00735                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00736"></a>00736                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nvector_ - 1)
<a name="l00737"></a>00737                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00738"></a>00738 <span class="preprocessor">#endif</span>
<a name="l00739"></a>00739 <span class="preprocessor"></span>
<a name="l00740"></a>00740     <span class="keywordflow">if</span> (collection_(i) != 2)
<a name="l00741"></a>00741       WrongArgument(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00742"></a>00742                     <span class="stringliteral">&quot;::GetVector(int i, Vector&lt;double, VectDense&gt;&amp;)&quot;</span>,
<a name="l00743"></a>00743                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;-th inner vector &quot;</span>
<a name="l00744"></a>00744                     <span class="stringliteral">&quot;is of type &quot;</span> + GetType(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00745"></a>00745 
<a name="l00746"></a>00746     vector.SetData(double_dense_c_.GetVector(subvector_(i)));
<a name="l00747"></a>00747   }
<a name="l00748"></a>00748 
<a name="l00749"></a>00749 
<a name="l00751"></a>00751 
<a name="l00755"></a>00755   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00756"></a>00756   <span class="keyword">inline</span> <span class="keywordtype">void</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l00757"></a>00757   ::GetVector(<span class="keywordtype">int</span> i, <span class="keyword">typename</span>
<a name="l00758"></a>00758               Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l00759"></a>00759               ::double_sparse_v&amp; vector) <span class="keyword">const</span>
<a name="l00760"></a>00760   {
<a name="l00761"></a>00761 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00762"></a>00762 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Nvector_)
<a name="l00763"></a>00763       <span class="keywordflow">throw</span> WrongIndex(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00764"></a>00764                        <span class="stringliteral">&quot;::GetVector(int i, Vector&lt;double, VectSparse&gt;&amp;)&quot;</span>,
<a name="l00765"></a>00765                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00766"></a>00766                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nvector_ - 1)
<a name="l00767"></a>00767                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00768"></a>00768 <span class="preprocessor">#endif</span>
<a name="l00769"></a>00769 <span class="preprocessor"></span>
<a name="l00770"></a>00770     <span class="keywordflow">if</span> (collection_(i) != 3)
<a name="l00771"></a>00771       WrongArgument(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00772"></a>00772                     <span class="stringliteral">&quot;::GetVector(int i, Vector&lt;double, VectSparse&gt;&amp;)&quot;</span>,
<a name="l00773"></a>00773                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;-th inner vector &quot;</span>
<a name="l00774"></a>00774                     <span class="stringliteral">&quot;is of type &quot;</span> + GetType(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00775"></a>00775 
<a name="l00776"></a>00776     vector.SetData(double_sparse_c_.GetVector(subvector_(i)));
<a name="l00777"></a>00777   }
<a name="l00778"></a>00778 
<a name="l00779"></a>00779 
<a name="l00781"></a>00781 
<a name="l00785"></a><a class="code" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ae15d81d9568c3d4383af9e4cf81427cd">00785</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00786"></a>00786   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Storage0, <span class="keyword">class</span> Allocator0&gt;
<a name="l00787"></a>00787   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;
<a name="l00788"></a>00788   ::GetVector(<span class="keywordtype">string</span> name,<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T0, Storage0, Allocator0&gt;</a>&amp; vector) <span class="keyword">const</span>
<a name="l00789"></a>00789   {
<a name="l00790"></a>00790     map&lt;string,int&gt;::const_iterator label_iterator;
<a name="l00791"></a>00791     label_iterator = label_map_.find(name);
<a name="l00792"></a>00792     <span class="keywordflow">if</span> (label_iterator == label_map_.end())
<a name="l00793"></a>00793       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00794"></a>00794                           <span class="stringliteral">&quot;::SetVector(string name)&quot;</span>,
<a name="l00795"></a>00795                           <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unknown vector name &quot;</span>) + name + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00796"></a>00796     GetVector(label_iterator-&gt;second, vector);
<a name="l00797"></a>00797   }
<a name="l00798"></a>00798 
<a name="l00799"></a>00799 
<a name="l00800"></a>00800   <span class="comment">/*********************************</span>
<a name="l00801"></a>00801 <span class="comment">   * ELEMENT ACCESS AND ASSIGNMENT *</span>
<a name="l00802"></a>00802 <span class="comment">   *********************************/</span>
<a name="l00803"></a>00803 
<a name="l00804"></a>00804 
<a name="l00806"></a>00806 
<a name="l00810"></a><a class="code" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a2bed95a4398789fdbdc0abd96b233c5e">00810</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00811"></a>00811   <span class="keyword">inline</span> <span class="keywordtype">double</span>
<a name="l00812"></a>00812   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;
<a name="l00813"></a>00813   ::operator() (<span class="keywordtype">int</span> i) <span class="keyword">const</span>
<a name="l00814"></a>00814   {
<a name="l00815"></a>00815 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00816"></a>00816 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00817"></a>00817       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparse&gt;::operator()&quot;</span>,
<a name="l00818"></a>00818                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00819"></a>00819                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_ - 1)
<a name="l00820"></a>00820                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00821"></a>00821 <span class="preprocessor">#endif</span>
<a name="l00822"></a>00822 <span class="preprocessor"></span>
<a name="l00823"></a>00823     <span class="keywordtype">int</span> j = 0;
<a name="l00824"></a>00824     <span class="keywordflow">while</span> (i &gt;= length_sum_(j))
<a name="l00825"></a>00825       j++;
<a name="l00826"></a>00826 
<a name="l00827"></a>00827     <span class="keywordflow">switch</span> (collection_(j))
<a name="l00828"></a>00828       {
<a name="l00829"></a>00829       <span class="keywordflow">case</span> 0:
<a name="l00830"></a>00830         <span class="keywordflow">return</span> (j == 0) ? double(float_dense_c_.GetVector(subvector_(j))(i)) :
<a name="l00831"></a>00831           double(float_dense_c_.
<a name="l00832"></a>00832                  GetVector(subvector_(j))(i - length_sum_(j - 1)));
<a name="l00833"></a>00833       <span class="keywordflow">case</span> 1:
<a name="l00834"></a>00834         <span class="keywordflow">return</span> (j == 0) ? double(float_sparse_c_.GetVector(subvector_(j))(i)):
<a name="l00835"></a>00835           double(float_sparse_c_.
<a name="l00836"></a>00836                  GetVector(subvector_(j))(i - length_sum_(j - 1)));
<a name="l00837"></a>00837       <span class="keywordflow">case</span> 2:
<a name="l00838"></a>00838         <span class="keywordflow">return</span> j == 0 ? double_dense_c_.GetVector(subvector_(j))(i) :
<a name="l00839"></a>00839           double_dense_c_.GetVector(subvector_(j))(i - length_sum_(j - 1));
<a name="l00840"></a>00840       <span class="keywordflow">case</span> 3:
<a name="l00841"></a>00841         <span class="keywordflow">return</span> j == 0 ? double_sparse_c_.GetVector(subvector_(j))(i) :
<a name="l00842"></a>00842           double_sparse_c_.GetVector(subvector_(j))(i - length_sum_(j - 1));
<a name="l00843"></a>00843       <span class="keywordflow">default</span>:
<a name="l00844"></a>00844         <span class="keywordflow">return</span> 0.;
<a name="l00845"></a>00845       }
<a name="l00846"></a>00846   }
<a name="l00847"></a>00847 
<a name="l00848"></a>00848 
<a name="l00850"></a>00850 
<a name="l00855"></a><a class="code" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a5a45815f1db11248f8e963611f41d0b1">00855</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00856"></a>00856   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;&amp;
<a name="l00857"></a>00857   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;::operator=
<a name="l00858"></a>00858   (<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;&amp; X)
<a name="l00859"></a>00859   {
<a name="l00860"></a>00860     this-&gt;Copy(X);
<a name="l00861"></a>00861     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00862"></a>00862   }
<a name="l00863"></a>00863 
<a name="l00864"></a>00864 
<a name="l00866"></a>00866 
<a name="l00871"></a>00871   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00872"></a>00872   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;
<a name="l00873"></a>00873   ::Copy(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;&amp; X)
<a name="l00874"></a>00874   {
<a name="l00875"></a>00875     Clear();
<a name="l00876"></a>00876     this-&gt;m_ = X.GetM();
<a name="l00877"></a>00877     collection_.Copy(X.collection_);
<a name="l00878"></a>00878     subvector_.Copy(X.subvector_);
<a name="l00879"></a>00879     length_.Copy(X.length_);
<a name="l00880"></a>00880     length_sum_.Copy(X.length_sum_);
<a name="l00881"></a>00881 
<a name="l00882"></a>00882     float_dense_c_.Copy(X.float_dense_c_);
<a name="l00883"></a>00883     float_sparse_c_.Copy(X.float_sparse_c_);
<a name="l00884"></a>00884     double_dense_c_.Copy(X.double_dense_c_);
<a name="l00885"></a>00885     double_sparse_c_.Copy(X.double_sparse_c_);
<a name="l00886"></a>00886 
<a name="l00887"></a>00887     label_map_.insert(X.label_map_.begin(), X.label_map_.end());
<a name="l00888"></a>00888     label_vector_.assign(X.label_vector_.begin(), X.label_vector_.end());
<a name="l00889"></a>00889   }
<a name="l00890"></a>00890 
<a name="l00891"></a>00891 
<a name="l00893"></a>00893 
<a name="l00896"></a>00896   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00897"></a><a class="code" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a429e20e3d4e359ccbdca47fd89e87d4b">00897</a>   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l00898"></a>00898   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;&amp;
<a name="l00899"></a>00899   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;
<a name="l00900"></a>00900   ::operator*= (<span class="keyword">const</span> T0&amp; alpha)
<a name="l00901"></a>00901   {
<a name="l00902"></a>00902     float_dense_c_ *= alpha;
<a name="l00903"></a>00903     float_sparse_c_ *= alpha;
<a name="l00904"></a>00904     double_dense_c_ *= alpha;
<a name="l00905"></a>00905     double_sparse_c_ *= alpha;
<a name="l00906"></a>00906     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00907"></a>00907   }
<a name="l00908"></a>00908 
<a name="l00909"></a>00909 
<a name="l00911"></a>00911   <span class="comment">// CONVENIENT METHODS //</span>
<a name="l00913"></a>00913 <span class="comment"></span>
<a name="l00914"></a>00914 
<a name="l00916"></a>00916   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00917"></a>00917   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt;</a> &gt;
<a name="l00918"></a>00918   ::Print() <span class="keyword">const</span>
<a name="l00919"></a>00919   {
<a name="l00920"></a>00920     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Nvector_; i++)
<a name="l00921"></a>00921       {
<a name="l00922"></a>00922         <span class="keywordflow">if</span> (i &lt; <span class="keywordtype">int</span>(label_vector_.size()) &amp;&amp; label_vector_[i] != <span class="stringliteral">&quot;&quot;</span>)
<a name="l00923"></a>00923           cout &lt;&lt; label_vector_[i] &lt;&lt; <span class="stringliteral">&quot;:&quot;</span> &lt;&lt; endl;
<a name="l00924"></a>00924         <span class="keywordflow">else</span>
<a name="l00925"></a>00925           cout &lt;&lt; <span class="stringliteral">&quot;(noname):&quot;</span> &lt;&lt; endl;
<a name="l00926"></a>00926 
<a name="l00927"></a>00927         <span class="keywordflow">switch</span>(collection_(i))
<a name="l00928"></a>00928           {
<a name="l00929"></a>00929           <span class="keywordflow">case</span> 0:
<a name="l00930"></a>00930             float_dense_c_.GetVector(subvector_(i)).Print();
<a name="l00931"></a>00931             <span class="keywordflow">break</span>;
<a name="l00932"></a>00932           <span class="keywordflow">case</span> 1:
<a name="l00933"></a>00933             float_sparse_c_.GetVector(subvector_(i)).Print();
<a name="l00934"></a>00934             <span class="keywordflow">break</span>;
<a name="l00935"></a>00935           <span class="keywordflow">case</span> 2:
<a name="l00936"></a>00936             double_dense_c_.GetVector(subvector_(i)).Print();
<a name="l00937"></a>00937             <span class="keywordflow">break</span>;
<a name="l00938"></a>00938           <span class="keywordflow">case</span> 3:
<a name="l00939"></a>00939             double_sparse_c_.GetVector(subvector_(i)).Print();
<a name="l00940"></a>00940             <span class="keywordflow">break</span>;
<a name="l00941"></a>00941           }
<a name="l00942"></a>00942       }
<a name="l00943"></a>00943   }
<a name="l00944"></a>00944 
<a name="l00945"></a>00945 
<a name="l00947"></a>00947 
<a name="l00953"></a>00953   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00954"></a>00954   <span class="keywordtype">void</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l00955"></a>00955   ::Write(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size = <span class="keyword">true</span>) <span class="keyword">const</span>
<a name="l00956"></a>00956   {
<a name="l00957"></a>00957     ofstream FileStream;
<a name="l00958"></a>00958     FileStream.open(FileName.c_str());
<a name="l00959"></a>00959 
<a name="l00960"></a>00960 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00961"></a>00961 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00962"></a>00962     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00963"></a>00963       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00964"></a>00964                     <span class="stringliteral">&quot;::Write(string FileName)&quot;</span>,
<a name="l00965"></a>00965                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00966"></a>00966 <span class="preprocessor">#endif</span>
<a name="l00967"></a>00967 <span class="preprocessor"></span>
<a name="l00968"></a>00968     this-&gt;Write(FileStream, with_size);
<a name="l00969"></a>00969 
<a name="l00970"></a>00970     FileStream.close();
<a name="l00971"></a>00971   }
<a name="l00972"></a>00972 
<a name="l00973"></a>00973 
<a name="l00975"></a>00975 
<a name="l00981"></a>00981   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l00982"></a>00982   <span class="keywordtype">void</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l00983"></a>00983   ::Write(ostream&amp; FileStream, <span class="keywordtype">bool</span> with_size = <span class="keyword">true</span>) <span class="keyword">const</span>
<a name="l00984"></a>00984   {
<a name="l00985"></a>00985 
<a name="l00986"></a>00986 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00987"></a>00987 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00988"></a>00988     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00989"></a>00989       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00990"></a>00990                     <span class="stringliteral">&quot;::Write(ostream&amp; FileStream)&quot;</span>,
<a name="l00991"></a>00991                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00992"></a>00992 <span class="preprocessor">#endif</span>
<a name="l00993"></a>00993 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (with_size)
<a name="l00994"></a>00994       FileStream
<a name="l00995"></a>00995         .write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;Nvector_)),
<a name="l00996"></a>00996                <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00997"></a>00997 
<a name="l00998"></a>00998     collection_.Write(FileStream, with_size);
<a name="l00999"></a>00999 
<a name="l01000"></a>01000     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Nvector_; i++)
<a name="l01001"></a>01001       {
<a name="l01002"></a>01002         <span class="keywordflow">switch</span> (collection_(i))
<a name="l01003"></a>01003           {
<a name="l01004"></a>01004           <span class="keywordflow">case</span> 0:
<a name="l01005"></a>01005             float_dense_c_.GetVector(subvector_(i)).Write(FileStream);
<a name="l01006"></a>01006             <span class="keywordflow">break</span>;
<a name="l01007"></a>01007           <span class="keywordflow">case</span> 1:
<a name="l01008"></a>01008             float_sparse_c_.GetVector(subvector_(i)).Write(FileStream);
<a name="l01009"></a>01009             <span class="keywordflow">break</span>;
<a name="l01010"></a>01010           <span class="keywordflow">case</span> 2:
<a name="l01011"></a>01011             double_dense_c_.GetVector(subvector_(i)).Write(FileStream);
<a name="l01012"></a>01012             <span class="keywordflow">break</span>;
<a name="l01013"></a>01013           <span class="keywordflow">case</span> 3:
<a name="l01014"></a>01014             double_sparse_c_.GetVector(subvector_(i)).Write(FileStream);
<a name="l01015"></a>01015             <span class="keywordflow">break</span>;
<a name="l01016"></a>01016           }
<a name="l01017"></a>01017       }
<a name="l01018"></a>01018 
<a name="l01019"></a>01019 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01020"></a>01020 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l01021"></a>01021     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01022"></a>01022       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l01023"></a>01023                     <span class="stringliteral">&quot;::Write(ostream&amp; FileStream)&quot;</span>,
<a name="l01024"></a>01024                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l01025"></a>01025 <span class="preprocessor">#endif</span>
<a name="l01026"></a>01026 <span class="preprocessor"></span>  }
<a name="l01027"></a>01027 
<a name="l01028"></a>01028 
<a name="l01030"></a>01030 
<a name="l01035"></a>01035   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l01036"></a>01036   <span class="keywordtype">void</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l01037"></a>01037   ::WriteText(<span class="keywordtype">string</span> FileName) <span class="keyword">const</span>
<a name="l01038"></a>01038   {
<a name="l01039"></a>01039     ofstream FileStream;
<a name="l01040"></a>01040     FileStream.precision(cout.precision());
<a name="l01041"></a>01041     FileStream.flags(cout.flags());
<a name="l01042"></a>01042     FileStream.open(FileName.c_str());
<a name="l01043"></a>01043 
<a name="l01044"></a>01044 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01045"></a>01045 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01046"></a>01046     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01047"></a>01047       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l01048"></a>01048                     <span class="stringliteral">&quot;::WriteText(string FileName)&quot;</span>,
<a name="l01049"></a>01049                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01050"></a>01050 <span class="preprocessor">#endif</span>
<a name="l01051"></a>01051 <span class="preprocessor"></span>
<a name="l01052"></a>01052     this-&gt;WriteText(FileStream);
<a name="l01053"></a>01053 
<a name="l01054"></a>01054     FileStream.close();
<a name="l01055"></a>01055   }
<a name="l01056"></a>01056 
<a name="l01057"></a>01057 
<a name="l01059"></a>01059 
<a name="l01064"></a>01064   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l01065"></a>01065   <span class="keywordtype">void</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l01066"></a>01066   ::WriteText(ostream&amp; FileStream) <span class="keyword">const</span>
<a name="l01067"></a>01067   {
<a name="l01068"></a>01068 
<a name="l01069"></a>01069 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01070"></a>01070 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01071"></a>01071     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01072"></a>01072       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l01073"></a>01073                     <span class="stringliteral">&quot;::Write(ostream&amp; FileStream)&quot;</span>,
<a name="l01074"></a>01074                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l01075"></a>01075 <span class="preprocessor">#endif</span>
<a name="l01076"></a>01076 <span class="preprocessor"></span>
<a name="l01077"></a>01077     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Nvector_; i++)
<a name="l01078"></a>01078       {
<a name="l01079"></a>01079         <span class="keywordflow">switch</span> (collection_(i))
<a name="l01080"></a>01080           {
<a name="l01081"></a>01081           <span class="keywordflow">case</span> 0:
<a name="l01082"></a>01082             float_dense_c_.GetVector(subvector_(i)).WriteText(FileStream);
<a name="l01083"></a>01083             <span class="keywordflow">break</span>;
<a name="l01084"></a>01084           <span class="keywordflow">case</span> 1:
<a name="l01085"></a>01085             float_sparse_c_.GetVector(subvector_(i)).WriteText(FileStream);
<a name="l01086"></a>01086             <span class="keywordflow">break</span>;
<a name="l01087"></a>01087           <span class="keywordflow">case</span> 2:
<a name="l01088"></a>01088             double_dense_c_.GetVector(subvector_(i)).WriteText(FileStream);
<a name="l01089"></a>01089             <span class="keywordflow">break</span>;
<a name="l01090"></a>01090           <span class="keywordflow">case</span> 3:
<a name="l01091"></a>01091             double_sparse_c_.GetVector(subvector_(i)).WriteText(FileStream);
<a name="l01092"></a>01092             <span class="keywordflow">break</span>;
<a name="l01093"></a>01093           }
<a name="l01094"></a>01094       }
<a name="l01095"></a>01095 
<a name="l01096"></a>01096 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01097"></a>01097 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l01098"></a>01098     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01099"></a>01099       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l01100"></a>01100                     <span class="stringliteral">&quot;::Write(ostream&amp; FileStream)&quot;</span>,
<a name="l01101"></a>01101                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l01102"></a>01102 <span class="preprocessor">#endif</span>
<a name="l01103"></a>01103 <span class="preprocessor"></span>  }
<a name="l01104"></a>01104 
<a name="l01105"></a>01105 
<a name="l01107"></a>01107 
<a name="l01112"></a>01112   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l01113"></a>01113   <span class="keywordtype">void</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l01114"></a>01114   ::Read(<span class="keywordtype">string</span> FileName)
<a name="l01115"></a>01115   {
<a name="l01116"></a>01116     ifstream FileStream;
<a name="l01117"></a>01117     FileStream.open(FileName.c_str());
<a name="l01118"></a>01118 
<a name="l01119"></a>01119 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01120"></a>01120 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01121"></a>01121     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01122"></a>01122       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l01123"></a>01123                     <span class="stringliteral">&quot;::Read(string FileName)&quot;</span>,
<a name="l01124"></a>01124                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01125"></a>01125 <span class="preprocessor">#endif</span>
<a name="l01126"></a>01126 <span class="preprocessor"></span>
<a name="l01127"></a>01127     this-&gt;Read(FileStream);
<a name="l01128"></a>01128 
<a name="l01129"></a>01129     FileStream.close();
<a name="l01130"></a>01130   }
<a name="l01131"></a>01131 
<a name="l01132"></a>01132 
<a name="l01134"></a>01134 
<a name="l01139"></a>01139   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l01140"></a>01140   <span class="keywordtype">void</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l01141"></a>01141   ::Read(istream&amp; FileStream)
<a name="l01142"></a>01142   {
<a name="l01143"></a>01143 
<a name="l01144"></a>01144 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01145"></a>01145 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01146"></a>01146     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01147"></a>01147       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l01148"></a>01148                     <span class="stringliteral">&quot;::Read(istream&amp; FileStream)&quot;</span>,
<a name="l01149"></a>01149                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l01150"></a>01150 <span class="preprocessor">#endif</span>
<a name="l01151"></a>01151 <span class="preprocessor"></span>
<a name="l01152"></a>01152     <span class="keywordtype">int</span>* Nvector = <span class="keyword">new</span> int;
<a name="l01153"></a>01153     FileStream.read(reinterpret_cast&lt;char*&gt;(Nvector), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01154"></a>01154 
<a name="l01155"></a>01155     Vector&lt;int, VectFull, MallocAlloc&lt;int&gt; &gt; collection(*Nvector);
<a name="l01156"></a>01156     collection.Read(FileStream);
<a name="l01157"></a>01157 
<a name="l01158"></a>01158     Clear();
<a name="l01159"></a>01159 
<a name="l01160"></a>01160     float_dense_v v0;
<a name="l01161"></a>01161     float_sparse_v v1;
<a name="l01162"></a>01162     double_dense_v v2;
<a name="l01163"></a>01163     double_sparse_v v3;
<a name="l01164"></a>01164 
<a name="l01165"></a>01165     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; *Nvector; i++)
<a name="l01166"></a>01166       {
<a name="l01167"></a>01167         <span class="keywordflow">switch</span> (collection(i))
<a name="l01168"></a>01168           {
<a name="l01169"></a>01169           <span class="keywordflow">case</span> 0:
<a name="l01170"></a>01170             v0.Read(FileStream);
<a name="l01171"></a>01171             AddVector(v0);
<a name="l01172"></a>01172             v0.Nullify();
<a name="l01173"></a>01173             <span class="keywordflow">break</span>;
<a name="l01174"></a>01174           <span class="keywordflow">case</span> 1:
<a name="l01175"></a>01175             v1.Read(FileStream);
<a name="l01176"></a>01176             AddVector(v1);
<a name="l01177"></a>01177             v1.Nullify();
<a name="l01178"></a>01178             <span class="keywordflow">break</span>;
<a name="l01179"></a>01179           <span class="keywordflow">case</span> 2:
<a name="l01180"></a>01180             v2.Read(FileStream);
<a name="l01181"></a>01181             AddVector(v2);
<a name="l01182"></a>01182             v2.Nullify();
<a name="l01183"></a>01183             <span class="keywordflow">break</span>;
<a name="l01184"></a>01184           <span class="keywordflow">case</span> 3:
<a name="l01185"></a>01185             v3.Read(FileStream);
<a name="l01186"></a>01186             AddVector(v3);
<a name="l01187"></a>01187             v3.Nullify();
<a name="l01188"></a>01188             <span class="keywordflow">break</span>;
<a name="l01189"></a>01189           }
<a name="l01190"></a>01190       }
<a name="l01191"></a>01191 
<a name="l01192"></a>01192     <span class="keyword">delete</span> Nvector;
<a name="l01193"></a>01193 
<a name="l01194"></a>01194 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01195"></a>01195 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l01196"></a>01196     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01197"></a>01197       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Vector&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l01198"></a>01198                     <span class="stringliteral">&quot;::Read(istream&amp; FileStream)&quot;</span>,
<a name="l01199"></a>01199                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l01200"></a>01200 <span class="preprocessor">#endif</span>
<a name="l01201"></a>01201 <span class="preprocessor"></span>
<a name="l01202"></a>01202   }
<a name="l01203"></a>01203 
<a name="l01204"></a>01204 
<a name="l01206"></a>01206 
<a name="l01211"></a>01211   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l01212"></a>01212   ostream&amp;  <span class="keyword">operator</span> &lt;&lt;
<a name="l01213"></a>01213   (ostream&amp; out,
<a name="l01214"></a>01214    <span class="keyword">const</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;&amp; V)
<a name="l01215"></a>01215   {
<a name="l01216"></a>01216     Vector&lt;float, VectFull, Allocator&lt;float&gt; &gt; v0;
<a name="l01217"></a>01217     Vector&lt;float, VectSparse, Allocator&lt;float&gt; &gt; v1;
<a name="l01218"></a>01218     Vector&lt;double, VectFull, Allocator&lt;double&gt; &gt; v2;
<a name="l01219"></a>01219     Vector&lt;double, VectSparse, Allocator&lt;double&gt; &gt; v3;
<a name="l01220"></a>01220     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; V.GetNvector(); i++)
<a name="l01221"></a>01221       {
<a name="l01222"></a>01222         <span class="keywordflow">switch</span> (V.GetCollectionIndex()(i))
<a name="l01223"></a>01223           {
<a name="l01224"></a>01224           <span class="keywordflow">case</span> 0:
<a name="l01225"></a>01225             V.GetVector(i, v0);
<a name="l01226"></a>01226             out &lt;&lt; v0 &lt;&lt; <span class="charliteral">&#39;\t&#39;</span>;
<a name="l01227"></a>01227             v0.Nullify();
<a name="l01228"></a>01228             <span class="keywordflow">break</span>;
<a name="l01229"></a>01229           <span class="keywordflow">case</span> 1:
<a name="l01230"></a>01230             V.GetVector(i, v1);
<a name="l01231"></a>01231             out &lt;&lt; v1 &lt;&lt; <span class="charliteral">&#39;\t&#39;</span>;
<a name="l01232"></a>01232             v1.Nullify();
<a name="l01233"></a>01233             <span class="keywordflow">break</span>;
<a name="l01234"></a>01234           <span class="keywordflow">case</span> 2:
<a name="l01235"></a>01235             V.GetVector(i, v2);
<a name="l01236"></a>01236             out &lt;&lt; v2 &lt;&lt; <span class="charliteral">&#39;\t&#39;</span>;
<a name="l01237"></a>01237             v2.Nullify();
<a name="l01238"></a>01238             <span class="keywordflow">break</span>;
<a name="l01239"></a>01239           <span class="keywordflow">case</span> 3:
<a name="l01240"></a>01240             V.GetVector(i, v3);
<a name="l01241"></a>01241             out &lt;&lt; v3 &lt;&lt; <span class="charliteral">&#39;\t&#39;</span>;
<a name="l01242"></a>01242             v3.Nullify();
<a name="l01243"></a>01243             <span class="keywordflow">break</span>;
<a name="l01244"></a>01244           }
<a name="l01245"></a>01245       }
<a name="l01246"></a>01246     <span class="keywordflow">return</span> out;
<a name="l01247"></a>01247   }
<a name="l01248"></a>01248 
<a name="l01249"></a>01249 
<a name="l01251"></a>01251   <span class="comment">// PROTECTED METHOD //</span>
<a name="l01253"></a>01253 <span class="comment"></span>
<a name="l01254"></a>01254 
<a name="l01256"></a>01256 
<a name="l01261"></a>01261   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator &gt;
<a name="l01262"></a>01262   <span class="keywordtype">string</span> Vector&lt;FloatDouble, DenseSparseCollection, Allocator&lt;T&gt; &gt;
<a name="l01263"></a>01263   ::GetType(<span class="keywordtype">int</span> i) <span class="keyword">const</span>
<a name="l01264"></a>01264   {
<a name="l01265"></a>01265     <span class="keywordflow">if</span> (collection_(i) == 0)
<a name="l01266"></a>01266       <span class="keywordflow">return</span> <span class="stringliteral">&quot;Vector&lt;float, VectDense&gt;&quot;</span>;
<a name="l01267"></a>01267     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (collection_(i) == 1)
<a name="l01268"></a>01268       <span class="keywordflow">return</span> <span class="stringliteral">&quot;Vector&lt;float, VectSparse&gt;&quot;</span>;
<a name="l01269"></a>01269     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (collection_(i) == 2)
<a name="l01270"></a>01270       <span class="keywordflow">return</span> <span class="stringliteral">&quot;Vector&lt;double, VectDense&gt;&quot;</span>;
<a name="l01271"></a>01271     <span class="keywordflow">else</span>
<a name="l01272"></a>01272       <span class="keywordflow">return</span> <span class="stringliteral">&quot;Vector&lt;double, VectSparse&gt;&quot;</span>;
<a name="l01273"></a>01273   }
<a name="l01274"></a>01274 
<a name="l01275"></a>01275 
<a name="l01276"></a>01276 } <span class="comment">// namespace Seldon.</span>
<a name="l01277"></a>01277 
<a name="l01278"></a>01278 
<a name="l01279"></a>01279 <span class="preprocessor">#define SELDON_FILE_VECTOR_HETEROGENEOUSCOLLECTION_CXX</span>
<a name="l01280"></a>01280 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
