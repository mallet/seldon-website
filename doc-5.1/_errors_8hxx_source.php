<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>share/Errors.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_ERRORS_HXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 <span class="preprocessor">#include &lt;string&gt;</span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &lt;iostream&gt;</span>
<a name="l00024"></a>00024 <span class="preprocessor">#include &quot;Common.hxx&quot;</span>
<a name="l00025"></a>00025 
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 <span class="keyword">namespace </span>Seldon
<a name="l00028"></a>00028 {
<a name="l00029"></a>00029 
<a name="l00030"></a>00030 
<a name="l00031"></a>00031   <span class="keyword">using namespace </span>std;
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00035"></a>00035   <span class="comment">// ERROR //</span>
<a name="l00037"></a>00037 <span class="comment"></span>
<a name="l00038"></a><a class="code" href="class_seldon_1_1_error.php">00038</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_error.php">Error</a>
<a name="l00039"></a>00039   {
<a name="l00040"></a>00040   <span class="keyword">protected</span>:
<a name="l00042"></a><a class="code" href="class_seldon_1_1_error.php#a5442a8a1dffc72f0e04dd9f746e1a3f0">00042</a>     <span class="keywordtype">string</span> description_;
<a name="l00044"></a><a class="code" href="class_seldon_1_1_error.php#a45bc7e61221de461fb4b92964b7b9058">00044</a>     <span class="keywordtype">string</span> function_;
<a name="l00046"></a><a class="code" href="class_seldon_1_1_error.php#a00e98cbd016cc12b92ee48e59a492275">00046</a>     <span class="keywordtype">string</span> comment_;
<a name="l00047"></a>00047 
<a name="l00048"></a>00048   <span class="keyword">public</span>:
<a name="l00049"></a>00049     <a class="code" href="class_seldon_1_1_error.php">Error</a>(<span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> comment = <span class="stringliteral">&quot;&quot;</span>)  throw();
<a name="l00050"></a>00050     <a class="code" href="class_seldon_1_1_error.php">Error</a>(<span class="keywordtype">string</span> description, <span class="keywordtype">string</span> function, <span class="keywordtype">string</span> comment)  throw();
<a name="l00051"></a>00051     virtual ~<a class="code" href="class_seldon_1_1_error.php">Error</a>()  throw();
<a name="l00052"></a>00052 
<a name="l00053"></a>00053     virtual <span class="keywordtype">string</span> What();
<a name="l00054"></a>00054     <span class="keywordtype">void</span> CoutWhat();
<a name="l00055"></a>00055   };
<a name="l00056"></a>00056 
<a name="l00057"></a>00057 
<a name="l00059"></a>00059   <span class="comment">// UNDEFINED //</span>
<a name="l00061"></a>00061 <span class="comment"></span>
<a name="l00062"></a><a class="code" href="class_seldon_1_1_undefined.php">00062</a>   class <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>: public <a class="code" href="class_seldon_1_1_error.php">Error</a>
<a name="l00063"></a>00063   {
<a name="l00064"></a>00064   <span class="keyword">public</span>:
<a name="l00065"></a>00065     <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> comment = <span class="stringliteral">&quot;&quot;</span>)  <span class="keywordflow">throw</span>();
<a name="l00066"></a>00066 
<a name="l00067"></a>00067     <span class="keyword">virtual</span> <span class="keywordtype">string</span> What();
<a name="l00068"></a>00068   };
<a name="l00069"></a>00069 
<a name="l00070"></a>00070 
<a name="l00072"></a>00072   <span class="comment">// WRONGARGUMENT //</span>
<a name="l00074"></a>00074 <span class="comment"></span>
<a name="l00075"></a><a class="code" href="class_seldon_1_1_wrong_argument.php">00075</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_error.php">Error</a>
<a name="l00076"></a>00076   {
<a name="l00077"></a>00077   <span class="keyword">public</span>:
<a name="l00078"></a>00078     <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> comment = <span class="stringliteral">&quot;&quot;</span>)  <span class="keywordflow">throw</span>();
<a name="l00079"></a>00079 
<a name="l00080"></a>00080     <span class="keyword">virtual</span> <span class="keywordtype">string</span> What();
<a name="l00081"></a>00081   };
<a name="l00082"></a>00082 
<a name="l00083"></a>00083 
<a name="l00085"></a>00085   <span class="comment">// NOMEMORY //</span>
<a name="l00087"></a>00087 <span class="comment"></span>
<a name="l00088"></a><a class="code" href="class_seldon_1_1_no_memory.php">00088</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_error.php">Error</a>
<a name="l00089"></a>00089   {
<a name="l00090"></a>00090   <span class="keyword">public</span>:
<a name="l00091"></a>00091     <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> comment = <span class="stringliteral">&quot;&quot;</span>)  <span class="keywordflow">throw</span>();
<a name="l00092"></a>00092   };
<a name="l00093"></a>00093 
<a name="l00094"></a>00094 
<a name="l00096"></a>00096   <span class="comment">// WRONGDIM //</span>
<a name="l00098"></a>00098 <span class="comment"></span>
<a name="l00099"></a><a class="code" href="class_seldon_1_1_wrong_dim.php">00099</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_error.php">Error</a>
<a name="l00100"></a>00100   {
<a name="l00101"></a>00101   <span class="keyword">public</span>:
<a name="l00102"></a>00102     <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> comment = <span class="stringliteral">&quot;&quot;</span>)  <span class="keywordflow">throw</span>();
<a name="l00103"></a>00103   };
<a name="l00104"></a>00104 
<a name="l00105"></a>00105 
<a name="l00107"></a>00107   <span class="comment">// WRONGINDEX //</span>
<a name="l00109"></a>00109 <span class="comment"></span>
<a name="l00110"></a><a class="code" href="class_seldon_1_1_wrong_index.php">00110</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_error.php">Error</a>
<a name="l00111"></a>00111   {
<a name="l00112"></a>00112   <span class="keyword">public</span>:
<a name="l00113"></a>00113     <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> comment = <span class="stringliteral">&quot;&quot;</span>)  <span class="keywordflow">throw</span>();
<a name="l00114"></a>00114   };
<a name="l00115"></a>00115 
<a name="l00116"></a>00116 
<a name="l00118"></a>00118   <span class="comment">// WRONGROW //</span>
<a name="l00120"></a>00120 <span class="comment"></span>
<a name="l00121"></a><a class="code" href="class_seldon_1_1_wrong_row.php">00121</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_error.php">Error</a>
<a name="l00122"></a>00122   {
<a name="l00123"></a>00123   <span class="keyword">public</span>:
<a name="l00124"></a>00124     <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> comment = <span class="stringliteral">&quot;&quot;</span>)  <span class="keywordflow">throw</span>();
<a name="l00125"></a>00125   };
<a name="l00126"></a>00126 
<a name="l00127"></a>00127 
<a name="l00129"></a>00129   <span class="comment">// WRONGCOL //</span>
<a name="l00131"></a>00131 <span class="comment"></span>
<a name="l00132"></a><a class="code" href="class_seldon_1_1_wrong_col.php">00132</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_error.php">Error</a>
<a name="l00133"></a>00133   {
<a name="l00134"></a>00134   <span class="keyword">public</span>:
<a name="l00135"></a>00135     <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> comment = <span class="stringliteral">&quot;&quot;</span>)  <span class="keywordflow">throw</span>();
<a name="l00136"></a>00136   };
<a name="l00137"></a>00137 
<a name="l00138"></a>00138 
<a name="l00140"></a>00140   <span class="comment">// IOERROR //</span>
<a name="l00142"></a>00142 <span class="comment"></span>
<a name="l00143"></a><a class="code" href="class_seldon_1_1_i_o_error.php">00143</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_error.php">Error</a>
<a name="l00144"></a>00144   {
<a name="l00145"></a>00145   <span class="keyword">public</span>:
<a name="l00146"></a>00146     <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> comment = <span class="stringliteral">&quot;&quot;</span>)  <span class="keywordflow">throw</span>();
<a name="l00147"></a>00147   };
<a name="l00148"></a>00148 
<a name="l00149"></a>00149 
<a name="l00151"></a>00151   <span class="comment">// LAPACKERROR //</span>
<a name="l00153"></a>00153 <span class="comment"></span>
<a name="l00154"></a><a class="code" href="class_seldon_1_1_lapack_error.php">00154</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_lapack_error.php">LapackError</a>: <span class="keyword">public</span> <a class="code" href="class_seldon_1_1_error.php">Error</a>
<a name="l00155"></a>00155   {
<a name="l00156"></a>00156   <span class="keyword">protected</span>:
<a name="l00157"></a>00157     <span class="keywordtype">int</span> info_;
<a name="l00158"></a>00158 
<a name="l00159"></a>00159   <span class="keyword">public</span>:
<a name="l00160"></a>00160     <a class="code" href="class_seldon_1_1_lapack_error.php">LapackError</a>(<span class="keywordtype">int</span> info, <span class="keywordtype">string</span> function, <span class="keywordtype">string</span> comment)  <span class="keywordflow">throw</span>();
<a name="l00161"></a>00161 
<a name="l00162"></a>00162     <span class="keyword">virtual</span> <span class="keywordtype">string</span> What();
<a name="l00163"></a>00163   };
<a name="l00164"></a>00164 
<a name="l00165"></a>00165 
<a name="l00167"></a>00167   <span class="comment">// LAPACKINFO //</span>
<a name="l00169"></a>00169 <span class="comment"></span>
<a name="l00170"></a>00170 
<a name="l00171"></a><a class="code" href="class_seldon_1_1_lapack_info.php">00171</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_lapack_info.php">LapackInfo</a>
<a name="l00172"></a>00172   {
<a name="l00173"></a>00173   <span class="keyword">private</span>:
<a name="l00174"></a>00174     <span class="keywordtype">int</span> info_;
<a name="l00175"></a>00175 
<a name="l00176"></a>00176   <span class="keyword">public</span>:
<a name="l00177"></a>00177     <a class="code" href="class_seldon_1_1_lapack_info.php">LapackInfo</a>(<span class="keywordtype">int</span> info);
<a name="l00178"></a>00178 
<a name="l00179"></a>00179 <span class="preprocessor">#ifndef SWIG</span>
<a name="l00180"></a>00180 <span class="preprocessor"></span>    operator int ();
<a name="l00181"></a>00181 <span class="preprocessor">#endif</span>
<a name="l00182"></a>00182 <span class="preprocessor"></span>
<a name="l00183"></a>00183     <span class="keywordtype">int</span> GetInfo();
<a name="l00184"></a>00184     <span class="keywordtype">int</span>&amp; GetInfoRef();
<a name="l00185"></a>00185   };
<a name="l00186"></a>00186 
<a name="l00187"></a>00187 
<a name="l00188"></a>00188   <span class="keyword">extern</span> <a class="code" href="class_seldon_1_1_lapack_info.php">LapackInfo</a> lapack_info;
<a name="l00189"></a>00189 
<a name="l00190"></a>00190 
<a name="l00191"></a>00191 } <span class="comment">// namespace Seldon.</span>
<a name="l00192"></a>00192 
<a name="l00193"></a>00193 <span class="preprocessor">#define SELDON_FILE_ERRORS_HXX</span>
<a name="l00194"></a>00194 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
