<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Array3D&lt; T, Allocator &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a>, including all inherited members.<table>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#af53eaf5dca82c2d4ae7762ea532789ef">Array3D</a>()</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#a503b4ac2ccd0b34ecb12946bd5b6f9ce">Array3D</a>(int i, int j, int k)</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#aa3e0b21aaa3c72280962842605687005">Array3D</a>(const Array3D&lt; T, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>array3D_allocator_</b> (defined in <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td><code> [protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#aa9bf1047ace17bd5c2bf2d4863d46838">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#a2f8287363e777b0072017405e182ccc0">Copy</a>(const Array3D&lt; T, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>data_</b> (defined in <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#a3a5ea3c1352767c1ef1f88219660c045">Fill</a>()</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#a33ec62315a11c44f98aed69cc7808bb3">Fill</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#af32714dd9d5bcad70aad958c1e6299e8">FillRand</a>()</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#a41def646fbd18d0a86b2fced6e80faf1">GetData</a>() const </td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#a64e6b9afc92a4c7fe4a99f5017959cb6">GetDataSize</a>() const </td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#ab792b6b72a04b557792b6d5ad3e86afc">GetLength1</a>() const </td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#af8acaef0c21a50f4ab6128710d534f8b">GetLength2</a>() const </td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#afae5f3ffbbcb66b09ec27d1cfc81759a">GetLength3</a>() const </td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#a99dc59f2c62979e78c43d8ec31c02e68">GetSize</a>() const </td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>length1_</b> (defined in <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>length23_</b> (defined in <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>length2_</b> (defined in <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>length3_</b> (defined in <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#aa29212ec08a08bbda48cf91cce043dd2">operator()</a>(int i, int j, int k)</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#ab596d570ea5b72f77f189b8ebbd7796e">operator()</a>(int i, int j, int k) const </td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#a0cb6b0016cddb1a1e4c25c8e538e69be">operator=</a>(const Array3D&lt; T, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#aca40f1ee2914601787fceafab5d8c654">Print</a>() const </td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#a943c1fb9b12c7e781bf4e39887636ab6">Read</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#a9b0f2c088d6b6f06ecefe17f2d616d21">Read</a>(ifstream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#ae37a5c9ebc684062db0c1be3604641e8">Reallocate</a>(int i, int j, int k)</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>value_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#ac4711cf67da3bca2527438ad5b1c2eaa">Write</a>(string FileName) const </td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#ad134dc852a94cf207fe3c33f7393b547">Write</a>(ofstream &amp;FileStream) const </td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#ab965837a5d4a255c217bb33e08b45ead">Zero</a>()</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_array3_d.php#a0acf50b4c510976bdde062f03787f06b">~Array3D</a>()</td><td><a class="el" href="class_seldon_1_1_array3_d.php">Seldon::Array3D&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
