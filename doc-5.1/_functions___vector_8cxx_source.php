<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/basic_functions/Functions_Vector.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00003"></a>00003 <span class="comment">// Copyright (C) 2010 INRIA</span>
<a name="l00004"></a>00004 <span class="comment">// Author(s): Marc Fragu</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00007"></a>00007 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00008"></a>00008 <span class="comment">//</span>
<a name="l00009"></a>00009 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00010"></a>00010 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00011"></a>00011 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00012"></a>00012 <span class="comment">// any later version.</span>
<a name="l00013"></a>00013 <span class="comment">//</span>
<a name="l00014"></a>00014 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00015"></a>00015 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00016"></a>00016 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00017"></a>00017 <span class="comment">// more details.</span>
<a name="l00018"></a>00018 <span class="comment">//</span>
<a name="l00019"></a>00019 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00020"></a>00020 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00021"></a>00021 
<a name="l00022"></a>00022 
<a name="l00023"></a>00023 <span class="preprocessor">#ifndef SELDON_FILE_FUNCTIONS_VECTOR_CXX</span>
<a name="l00024"></a>00024 <span class="preprocessor"></span>
<a name="l00025"></a>00025 <span class="comment">/*</span>
<a name="l00026"></a>00026 <span class="comment">  Functions defined in this file:</span>
<a name="l00027"></a>00027 <span class="comment"></span>
<a name="l00028"></a>00028 <span class="comment">  alpha X -&gt; X</span>
<a name="l00029"></a>00029 <span class="comment">  Mlt(alpha, X)</span>
<a name="l00030"></a>00030 <span class="comment"></span>
<a name="l00031"></a>00031 <span class="comment">  alpha X + Y -&gt; Y</span>
<a name="l00032"></a>00032 <span class="comment">  Add(alpha, X, Y)</span>
<a name="l00033"></a>00033 <span class="comment"></span>
<a name="l00034"></a>00034 <span class="comment">  X -&gt; Y</span>
<a name="l00035"></a>00035 <span class="comment">  Copy(X, Y)</span>
<a name="l00036"></a>00036 <span class="comment"></span>
<a name="l00037"></a>00037 <span class="comment">  X &lt;-&gt; Y</span>
<a name="l00038"></a>00038 <span class="comment">  Swap(X, Y)</span>
<a name="l00039"></a>00039 <span class="comment"></span>
<a name="l00040"></a>00040 <span class="comment">  X.Y</span>
<a name="l00041"></a>00041 <span class="comment">  DotProd(X, Y)</span>
<a name="l00042"></a>00042 <span class="comment">  DotProdConj(X, Y)</span>
<a name="l00043"></a>00043 <span class="comment"></span>
<a name="l00044"></a>00044 <span class="comment">  ||X||</span>
<a name="l00045"></a>00045 <span class="comment">  Norm1(X)</span>
<a name="l00046"></a>00046 <span class="comment">  Norm2(X)</span>
<a name="l00047"></a>00047 <span class="comment">  GetMaxAbsIndex(X)</span>
<a name="l00048"></a>00048 <span class="comment"></span>
<a name="l00049"></a>00049 <span class="comment">  Omega X</span>
<a name="l00050"></a>00050 <span class="comment">  GenRot(x, y, cos, sin)</span>
<a name="l00051"></a>00051 <span class="comment">  ApplyRot(x, y, cos, sin)</span>
<a name="l00052"></a>00052 <span class="comment"></span>
<a name="l00053"></a>00053 <span class="comment">*/</span>
<a name="l00054"></a>00054 
<a name="l00055"></a>00055 <span class="keyword">namespace </span>Seldon
<a name="l00056"></a>00056 {
<a name="l00057"></a>00057 
<a name="l00058"></a>00058 
<a name="l00060"></a>00060   <span class="comment">// MLT //</span>
<a name="l00061"></a>00061 
<a name="l00062"></a>00062 
<a name="l00063"></a>00063   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00064"></a>00064             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00065"></a>00065   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00066"></a>00066            Vector&lt;T1, Storage1, Allocator1&gt;&amp; X)  <span class="keywordflow">throw</span>()
<a name="l00067"></a>00067   {
<a name="l00068"></a>00068     X *= alpha;
<a name="l00069"></a>00069   }
<a name="l00070"></a>00070 
<a name="l00071"></a>00071 
<a name="l00072"></a>00072   <span class="comment">// MLT //</span>
<a name="l00074"></a>00074 <span class="comment"></span>
<a name="l00075"></a>00075 
<a name="l00076"></a>00076 
<a name="l00078"></a>00078   <span class="comment">// ADD //</span>
<a name="l00079"></a>00079 
<a name="l00080"></a>00080 
<a name="l00081"></a>00081   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00082"></a>00082             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00083"></a>00083             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00084"></a>00084   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00085"></a>00085            <span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00086"></a>00086            Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y)  <span class="keywordflow">throw</span>(WrongDim, NoMemory)
<a name="l00087"></a>00087   {
<a name="l00088"></a>00088     <span class="keywordflow">if</span> (alpha != T0(0))
<a name="l00089"></a>00089       {
<a name="l00090"></a>00090         T1 alpha_ = alpha;
<a name="l00091"></a>00091 
<a name="l00092"></a>00092         <span class="keywordtype">int</span> ma = X.GetM();
<a name="l00093"></a>00093 
<a name="l00094"></a>00094 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00095"></a>00095 <span class="preprocessor"></span>        <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(X, Y, <span class="stringliteral">&quot;Add(alpha, X, Y)&quot;</span>);
<a name="l00096"></a>00096 <span class="preprocessor">#endif</span>
<a name="l00097"></a>00097 <span class="preprocessor"></span>
<a name="l00098"></a>00098         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; ma; i++)
<a name="l00099"></a>00099           Y(i) += alpha_ * X(i);
<a name="l00100"></a>00100       }
<a name="l00101"></a>00101   }
<a name="l00102"></a>00102 
<a name="l00103"></a>00103 
<a name="l00104"></a>00104   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00105"></a>00105             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00106"></a>00106             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00107"></a>00107   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00108"></a>00108            <span class="keyword">const</span> Vector&lt;T1, VectSparse, Allocator1&gt;&amp; X,
<a name="l00109"></a>00109            Vector&lt;T2, VectSparse, Allocator2&gt;&amp; Y)  <span class="keywordflow">throw</span>(WrongDim, NoMemory)
<a name="l00110"></a>00110   {
<a name="l00111"></a>00111     <span class="keywordflow">if</span> (alpha != T0(0))
<a name="l00112"></a>00112       {
<a name="l00113"></a>00113         Vector&lt;T1, VectSparse, Allocator1&gt; Xalpha = X;
<a name="l00114"></a>00114         Xalpha *= alpha;
<a name="l00115"></a>00115         Y.AddInteractionRow(Xalpha.GetSize(),
<a name="l00116"></a>00116                             Xalpha.GetIndex(), Xalpha.GetData(), <span class="keyword">true</span>);
<a name="l00117"></a>00117       }
<a name="l00118"></a>00118   }
<a name="l00119"></a>00119 
<a name="l00120"></a>00120 
<a name="l00121"></a>00121   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00122"></a>00122             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00123"></a>00123             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00124"></a>00124   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00125"></a>00125            <span class="keyword">const</span> Vector&lt;T1, Collection, Allocator1&gt;&amp; X,
<a name="l00126"></a>00126            Vector&lt;T2, Collection, Allocator2&gt;&amp; Y)
<a name="l00127"></a>00127   {
<a name="l00128"></a>00128 
<a name="l00129"></a>00129 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00130"></a>00130 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(X, Y, <span class="stringliteral">&quot;Add(X, Y)&quot;</span>, <span class="stringliteral">&quot;X + Y&quot;</span>);
<a name="l00131"></a>00131 <span class="preprocessor">#endif</span>
<a name="l00132"></a>00132 <span class="preprocessor"></span>
<a name="l00133"></a>00133     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetNvector(); i++)
<a name="l00134"></a>00134       <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(alpha, X.GetVector(i), Y.GetVector(i));
<a name="l00135"></a>00135   }
<a name="l00136"></a>00136 
<a name="l00137"></a>00137 
<a name="l00138"></a>00138   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00139"></a>00139             <span class="keyword">class </span>T1, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U1&gt; <span class="keyword">class </span>Allocator1,
<a name="l00140"></a>00140             <span class="keyword">class </span>T2, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U2&gt; <span class="keyword">class </span>Allocator2&gt;
<a name="l00141"></a>00141   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00142"></a>00142            <span class="keyword">const</span>
<a name="l00143"></a>00143            Vector&lt;FloatDouble, DenseSparseCollection, Allocator1&lt;T1&gt; &gt;&amp; X,
<a name="l00144"></a>00144            Vector&lt;FloatDouble, DenseSparseCollection, Allocator2&lt;T2&gt; &gt;&amp; Y)
<a name="l00145"></a>00145   {
<a name="l00146"></a>00146 
<a name="l00147"></a>00147 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00148"></a>00148 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(X, Y, <span class="stringliteral">&quot;Add(X, Y)&quot;</span>);
<a name="l00149"></a>00149 <span class="preprocessor">#endif</span>
<a name="l00150"></a>00150 <span class="preprocessor"></span>
<a name="l00151"></a>00151     <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(alpha, X.GetFloatDense(), Y.GetFloatDense());
<a name="l00152"></a>00152     <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(alpha, X.GetFloatSparse(), Y.GetFloatSparse());
<a name="l00153"></a>00153     <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(alpha, X.GetDoubleDense(), Y.GetDoubleDense());
<a name="l00154"></a>00154     <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(alpha, X.GetDoubleSparse(), Y.GetDoubleSparse());
<a name="l00155"></a>00155   }
<a name="l00156"></a>00156 
<a name="l00157"></a>00157 
<a name="l00158"></a>00158   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00159"></a>00159             <span class="keyword">class </span>T1, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U1&gt; <span class="keyword">class </span>Allocator1,
<a name="l00160"></a>00160             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00161"></a>00161   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00162"></a>00162            <span class="keyword">const</span>
<a name="l00163"></a>00163            Vector&lt;FloatDouble, DenseSparseCollection, Allocator1&lt;T1&gt; &gt;&amp; X,
<a name="l00164"></a>00164            Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y)  <span class="keywordflow">throw</span>(WrongDim, NoMemory)
<a name="l00165"></a>00165   {
<a name="l00166"></a>00166     <span class="keywordflow">if</span> (alpha != T0(0))
<a name="l00167"></a>00167       {
<a name="l00168"></a>00168         <span class="keywordtype">double</span> alpha_ = alpha;
<a name="l00169"></a>00169 
<a name="l00170"></a>00170         <span class="keywordtype">int</span> ma = X.GetM();
<a name="l00171"></a>00171 
<a name="l00172"></a>00172 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00173"></a>00173 <span class="preprocessor"></span>        <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(X, Y, <span class="stringliteral">&quot;Add(alpha, X, Y)&quot;</span>);
<a name="l00174"></a>00174 <span class="preprocessor">#endif</span>
<a name="l00175"></a>00175 <span class="preprocessor"></span>
<a name="l00176"></a>00176         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; ma; i++)
<a name="l00177"></a>00177           Y(i) += alpha_ * X(i);
<a name="l00178"></a>00178 
<a name="l00179"></a>00179       }
<a name="l00180"></a>00180   }
<a name="l00181"></a>00181 
<a name="l00182"></a>00182 
<a name="l00183"></a>00183   <span class="comment">// ADD //</span>
<a name="l00185"></a>00185 <span class="comment"></span>
<a name="l00186"></a>00186 
<a name="l00187"></a>00187 
<a name="l00189"></a>00189   <span class="comment">// COPY //</span>
<a name="l00190"></a>00190 
<a name="l00191"></a>00191 
<a name="l00192"></a>00192   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00193"></a>00193             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00194"></a>00194   <span class="keywordtype">void</span> Copy(<span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00195"></a>00195             Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y)
<a name="l00196"></a>00196   {
<a name="l00197"></a>00197     Y.Copy(X);
<a name="l00198"></a>00198   }
<a name="l00199"></a>00199 
<a name="l00200"></a>00200 
<a name="l00201"></a>00201   <span class="comment">// COPY //</span>
<a name="l00203"></a>00203 <span class="comment"></span>
<a name="l00204"></a>00204 
<a name="l00205"></a>00205 
<a name="l00207"></a>00207   <span class="comment">// SWAP //</span>
<a name="l00208"></a>00208 
<a name="l00209"></a>00209 
<a name="l00210"></a>00210   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00211"></a>00211   <span class="keywordtype">void</span> Swap(Vector&lt;T, Storage, Allocator&gt;&amp; X,
<a name="l00212"></a>00212             Vector&lt;T, Storage, Allocator&gt;&amp; Y)
<a name="l00213"></a>00213   {
<a name="l00214"></a>00214     <span class="keywordtype">int</span> nx = X.GetM();
<a name="l00215"></a>00215     T* data = X.GetData();
<a name="l00216"></a>00216     X.Nullify();
<a name="l00217"></a>00217     X.SetData(Y.GetM(), Y.GetData());
<a name="l00218"></a>00218     Y.Nullify();
<a name="l00219"></a>00219     Y.SetData(nx, data);
<a name="l00220"></a>00220   }
<a name="l00221"></a>00221 
<a name="l00222"></a>00222 
<a name="l00223"></a>00223   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00224"></a>00224   <span class="keywordtype">void</span> Swap(Vector&lt;T, VectSparse, Allocator&gt;&amp; X,
<a name="l00225"></a>00225             Vector&lt;T, VectSparse, Allocator&gt;&amp; Y)
<a name="l00226"></a>00226   {
<a name="l00227"></a>00227     <span class="keywordtype">int</span> nx = X.GetM();
<a name="l00228"></a>00228     T* data = X.GetData();
<a name="l00229"></a>00229     <span class="keywordtype">int</span>* index = X.GetIndex();
<a name="l00230"></a>00230     X.Nullify();
<a name="l00231"></a>00231     X.SetData(Y.GetM(), Y.GetData(), Y.GetIndex());
<a name="l00232"></a>00232     Y.Nullify();
<a name="l00233"></a>00233     Y.SetData(nx, data, index);
<a name="l00234"></a>00234   }
<a name="l00235"></a>00235 
<a name="l00236"></a>00236 
<a name="l00237"></a>00237   <span class="comment">// SWAP //</span>
<a name="l00239"></a>00239 <span class="comment"></span>
<a name="l00240"></a>00240 
<a name="l00241"></a>00241 
<a name="l00243"></a>00243   <span class="comment">// DOTPROD //</span>
<a name="l00244"></a>00244 
<a name="l00245"></a>00245 
<a name="l00247"></a>00247   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00248"></a>00248            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00249"></a><a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40">00249</a>   T1 <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; X,
<a name="l00250"></a>00250              <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; Y)
<a name="l00251"></a>00251   {
<a name="l00252"></a>00252     T1 value;
<a name="l00253"></a>00253     SetComplexZero(value);
<a name="l00254"></a>00254 
<a name="l00255"></a>00255 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00256"></a>00256 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(X, Y, <span class="stringliteral">&quot;DotProd(X, Y)&quot;</span>);
<a name="l00257"></a>00257 <span class="preprocessor">#endif</span>
<a name="l00258"></a>00258 <span class="preprocessor"></span>
<a name="l00259"></a>00259     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetM(); i++)
<a name="l00260"></a>00260       value += X(i) * Y(i);
<a name="l00261"></a>00261 
<a name="l00262"></a>00262     <span class="keywordflow">return</span> value;
<a name="l00263"></a>00263   }
<a name="l00264"></a>00264 
<a name="l00265"></a>00265 
<a name="l00267"></a>00267   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00268"></a>00268            <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00269"></a><a class="code" href="namespace_seldon.php#a18aae52340d415e476c14955593dc9de">00269</a>   <span class="keyword">typename</span> T1::value_type <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Collection, Allocator1&gt;</a>&amp; X,
<a name="l00270"></a>00270                                   <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Collection, Allocator2&gt;</a>&amp; Y)
<a name="l00271"></a>00271   {
<a name="l00272"></a>00272     <span class="keyword">typename</span> T1::value_type value(0);
<a name="l00273"></a>00273 
<a name="l00274"></a>00274 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00275"></a>00275 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(X, Y, <span class="stringliteral">&quot;DotProd(X, Y)&quot;</span>, <span class="stringliteral">&quot;&lt;X, Y&gt;&quot;</span>);
<a name="l00276"></a>00276 <span class="preprocessor">#endif</span>
<a name="l00277"></a>00277 <span class="preprocessor"></span>
<a name="l00278"></a>00278     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetNvector(); i++)
<a name="l00279"></a>00279       value += <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(X.GetVector(i), Y.GetVector(i));
<a name="l00280"></a>00280     <span class="keywordflow">return</span> value;
<a name="l00281"></a>00281   }
<a name="l00282"></a>00282 
<a name="l00283"></a>00283 
<a name="l00285"></a>00285   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U1&gt; <span class="keyword">class </span>Allocator1,
<a name="l00286"></a>00286            <span class="keyword">class </span>T2, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U2&gt; <span class="keyword">class </span>Allocator2&gt;
<a name="l00287"></a>00287   <span class="keywordtype">double</span>
<a name="l00288"></a><a class="code" href="namespace_seldon.php#a19a95c26c0daf25e7069d73cc26d0b09">00288</a>   <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(<span class="keyword">const</span>
<a name="l00289"></a>00289           <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;<a class="code" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="code" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator1&lt;T1&gt; &gt;&amp; X,
<a name="l00290"></a>00290           <span class="keyword">const</span>
<a name="l00291"></a>00291           <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;<a class="code" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="code" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator2&lt;T2&gt; &gt;&amp; Y)
<a name="l00292"></a>00292   {
<a name="l00293"></a>00293 
<a name="l00294"></a>00294 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00295"></a>00295 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(X, Y, <span class="stringliteral">&quot;DotProd(X, Y)&quot;</span>);
<a name="l00296"></a>00296 <span class="preprocessor">#endif</span>
<a name="l00297"></a>00297 <span class="preprocessor"></span>
<a name="l00298"></a>00298     <span class="keywordtype">double</span> value(0.);
<a name="l00299"></a>00299     value += <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(X.GetFloatDense(), Y.GetFloatDense());
<a name="l00300"></a>00300     value += <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(X.GetFloatSparse(), Y.GetFloatSparse());
<a name="l00301"></a>00301     value += <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(X.GetDoubleDense(), Y.GetDoubleDense());
<a name="l00302"></a>00302     value += <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(X.GetDoubleSparse(), Y.GetDoubleSparse());
<a name="l00303"></a>00303     <span class="keywordflow">return</span> value;
<a name="l00304"></a>00304   }
<a name="l00305"></a>00305 
<a name="l00306"></a>00306 
<a name="l00308"></a>00308   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00309"></a>00309            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00310"></a><a class="code" href="namespace_seldon.php#afd8cf3dd207cb4727edacda863a3f055">00310</a>   T1 <a class="code" href="namespace_seldon.php#afd8cf3dd207cb4727edacda863a3f055" title="Scalar product between two vectors.">DotProdConj</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; X,
<a name="l00311"></a>00311                  <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; Y)
<a name="l00312"></a>00312   {
<a name="l00313"></a>00313     <span class="keywordflow">return</span> <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(X, Y);
<a name="l00314"></a>00314   }
<a name="l00315"></a>00315 
<a name="l00316"></a>00316 
<a name="l00318"></a>00318   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00319"></a>00319            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00320"></a><a class="code" href="namespace_seldon.php#a2623bc1728bba64c267bb68617527806">00320</a>   complex&lt;T1&gt; <a class="code" href="namespace_seldon.php#afd8cf3dd207cb4727edacda863a3f055" title="Scalar product between two vectors.">DotProdConj</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T1&gt;, Storage1, Allocator1&gt;&amp; X,
<a name="l00321"></a>00321                           <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; Y)
<a name="l00322"></a>00322   {
<a name="l00323"></a>00323     complex&lt;T1&gt; value;
<a name="l00324"></a>00324     SetComplexZero(value);
<a name="l00325"></a>00325 
<a name="l00326"></a>00326 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00327"></a>00327 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(X, Y, <span class="stringliteral">&quot;DotProdConj(X, Y)&quot;</span>);
<a name="l00328"></a>00328 <span class="preprocessor">#endif</span>
<a name="l00329"></a>00329 <span class="preprocessor"></span>
<a name="l00330"></a>00330     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetM(); i++)
<a name="l00331"></a>00331       value += conj(X(i)) * Y(i);
<a name="l00332"></a>00332 
<a name="l00333"></a>00333     <span class="keywordflow">return</span> value;
<a name="l00334"></a>00334   }
<a name="l00335"></a>00335 
<a name="l00336"></a>00336 
<a name="l00338"></a>00338   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00339"></a>00339            <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00340"></a><a class="code" href="namespace_seldon.php#a5f54fb77d61d175f2f4dcc6010088053">00340</a>   T1 <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, VectSparse, Allocator1&gt;</a>&amp; X,
<a name="l00341"></a>00341              <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, VectSparse, Allocator2&gt;</a>&amp; Y)
<a name="l00342"></a>00342   {
<a name="l00343"></a>00343     T1 value;
<a name="l00344"></a>00344     SetComplexZero(value);
<a name="l00345"></a>00345 
<a name="l00346"></a>00346     <span class="keywordtype">int</span> size_x = X.GetSize();
<a name="l00347"></a>00347     <span class="keywordtype">int</span> size_y = Y.GetSize();
<a name="l00348"></a>00348     <span class="keywordtype">int</span> kx = 0, ky = 0, pos_x;
<a name="l00349"></a>00349     <span class="keywordflow">while</span> (kx &lt; size_x)
<a name="l00350"></a>00350       {
<a name="l00351"></a>00351         pos_x = X.Index(kx);
<a name="l00352"></a>00352         <span class="keywordflow">while</span> (ky &lt; size_y &amp;&amp; Y.Index(ky) &lt; pos_x)
<a name="l00353"></a>00353           ky++;
<a name="l00354"></a>00354 
<a name="l00355"></a>00355         <span class="keywordflow">if</span> (ky &lt; size_y &amp;&amp; Y.Index(ky) == pos_x)
<a name="l00356"></a>00356           value += X.Value(kx) * Y.Value(ky);
<a name="l00357"></a>00357 
<a name="l00358"></a>00358         kx++;
<a name="l00359"></a>00359       }
<a name="l00360"></a>00360 
<a name="l00361"></a>00361     <span class="keywordflow">return</span> value;
<a name="l00362"></a>00362   }
<a name="l00363"></a>00363 
<a name="l00364"></a>00364 
<a name="l00366"></a>00366   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00367"></a>00367            <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00368"></a>00368   complex&lt;T1&gt;
<a name="l00369"></a><a class="code" href="namespace_seldon.php#a17404d5fc966edefe53657f22f0c04b0">00369</a>   <a class="code" href="namespace_seldon.php#afd8cf3dd207cb4727edacda863a3f055" title="Scalar product between two vectors.">DotProdConj</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T1&gt;, <a class="code" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator1&gt;&amp; X,
<a name="l00370"></a>00370               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, VectSparse, Allocator2&gt;</a>&amp; Y)
<a name="l00371"></a>00371   {
<a name="l00372"></a>00372     complex&lt;T1&gt; value(0, 0);
<a name="l00373"></a>00373 
<a name="l00374"></a>00374     <span class="keywordtype">int</span> size_x = X.GetSize();
<a name="l00375"></a>00375     <span class="keywordtype">int</span> size_y = Y.GetSize();
<a name="l00376"></a>00376     <span class="keywordtype">int</span> kx = 0, ky = 0, pos_x;
<a name="l00377"></a>00377     <span class="keywordflow">while</span> (kx &lt; size_x)
<a name="l00378"></a>00378       {
<a name="l00379"></a>00379         pos_x = X.Index(kx);
<a name="l00380"></a>00380         <span class="keywordflow">while</span> (ky &lt; size_y &amp;&amp; Y.Index(ky) &lt; pos_x)
<a name="l00381"></a>00381           ky++;
<a name="l00382"></a>00382 
<a name="l00383"></a>00383         <span class="keywordflow">if</span> (ky &lt; size_y &amp;&amp; Y.Index(ky) == pos_x)
<a name="l00384"></a>00384           value += conj(X.Value(kx)) * Y.Value(ky);
<a name="l00385"></a>00385 
<a name="l00386"></a>00386         kx++;
<a name="l00387"></a>00387       }
<a name="l00388"></a>00388 
<a name="l00389"></a>00389     <span class="keywordflow">return</span> value;
<a name="l00390"></a>00390   }
<a name="l00391"></a>00391 
<a name="l00392"></a>00392 
<a name="l00393"></a>00393   <span class="comment">// DOTPROD //</span>
<a name="l00395"></a>00395 <span class="comment"></span>
<a name="l00396"></a>00396 
<a name="l00397"></a>00397 
<a name="l00399"></a>00399   <span class="comment">// NORM1 //</span>
<a name="l00400"></a>00400 
<a name="l00401"></a>00401 
<a name="l00402"></a>00402   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Storage1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00403"></a>00403   T1 <a class="code" href="namespace_seldon.php#afefce2ae4cd3147f863955efddc4184c" title="Returns the 1-norm of a matrix.">Norm1</a>(<span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X)
<a name="l00404"></a>00404   {
<a name="l00405"></a>00405     T1 value(0);
<a name="l00406"></a>00406 
<a name="l00407"></a>00407     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetM(); i++)
<a name="l00408"></a>00408       value += abs(X(i));
<a name="l00409"></a>00409 
<a name="l00410"></a>00410     <span class="keywordflow">return</span> value;
<a name="l00411"></a>00411   }
<a name="l00412"></a>00412 
<a name="l00413"></a>00413 
<a name="l00414"></a>00414   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Storage1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00415"></a>00415   T1 <a class="code" href="namespace_seldon.php#afefce2ae4cd3147f863955efddc4184c" title="Returns the 1-norm of a matrix.">Norm1</a>(<span class="keyword">const</span> Vector&lt;complex&lt;T1&gt;, Storage1, Allocator1&gt;&amp; X)
<a name="l00416"></a>00416   {
<a name="l00417"></a>00417     T1 value(0);
<a name="l00418"></a>00418 
<a name="l00419"></a>00419     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetM(); i++)
<a name="l00420"></a>00420       value += abs(X(i));
<a name="l00421"></a>00421 
<a name="l00422"></a>00422     <span class="keywordflow">return</span> value;
<a name="l00423"></a>00423   }
<a name="l00424"></a>00424 
<a name="l00425"></a>00425 
<a name="l00426"></a>00426   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00427"></a>00427   T1 <a class="code" href="namespace_seldon.php#afefce2ae4cd3147f863955efddc4184c" title="Returns the 1-norm of a matrix.">Norm1</a>(<span class="keyword">const</span> Vector&lt;T1, VectSparse, Allocator1&gt;&amp; X)
<a name="l00428"></a>00428   {
<a name="l00429"></a>00429     T1 value(0);
<a name="l00430"></a>00430 
<a name="l00431"></a>00431     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetSize(); i++)
<a name="l00432"></a>00432       value += abs(X.Value(i));
<a name="l00433"></a>00433 
<a name="l00434"></a>00434     <span class="keywordflow">return</span> value;
<a name="l00435"></a>00435   }
<a name="l00436"></a>00436 
<a name="l00437"></a>00437 
<a name="l00438"></a>00438   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00439"></a>00439   T1 <a class="code" href="namespace_seldon.php#afefce2ae4cd3147f863955efddc4184c" title="Returns the 1-norm of a matrix.">Norm1</a>(<span class="keyword">const</span> Vector&lt;complex&lt;T1&gt;, VectSparse, Allocator1&gt;&amp; X)
<a name="l00440"></a>00440   {
<a name="l00441"></a>00441     T1 value(0);
<a name="l00442"></a>00442 
<a name="l00443"></a>00443     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetSize(); i++)
<a name="l00444"></a>00444       value += abs(X.Value(i));
<a name="l00445"></a>00445 
<a name="l00446"></a>00446     <span class="keywordflow">return</span> value;
<a name="l00447"></a>00447   }
<a name="l00448"></a>00448 
<a name="l00449"></a>00449 
<a name="l00450"></a>00450   <span class="comment">// NORM1 //</span>
<a name="l00452"></a>00452 <span class="comment"></span>
<a name="l00453"></a>00453 
<a name="l00454"></a>00454 
<a name="l00456"></a>00456   <span class="comment">// NORM2 //</span>
<a name="l00457"></a>00457 
<a name="l00458"></a>00458 
<a name="l00459"></a>00459   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Storage1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00460"></a>00460   T1 Norm2(<span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X)
<a name="l00461"></a>00461   {
<a name="l00462"></a>00462     T1 value(0);
<a name="l00463"></a>00463 
<a name="l00464"></a>00464     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetM(); i++)
<a name="l00465"></a>00465       value += X(i) * X(i);
<a name="l00466"></a>00466 
<a name="l00467"></a>00467     <span class="keywordflow">return</span> sqrt(value);
<a name="l00468"></a>00468   }
<a name="l00469"></a>00469 
<a name="l00470"></a>00470 
<a name="l00471"></a>00471   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Storage1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00472"></a>00472   T1 Norm2(<span class="keyword">const</span> Vector&lt;complex&lt;T1&gt;, Storage1, Allocator1&gt;&amp; X)
<a name="l00473"></a>00473   {
<a name="l00474"></a>00474     T1 value(0);
<a name="l00475"></a>00475 
<a name="l00476"></a>00476     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetM(); i++)
<a name="l00477"></a>00477       value += real(X(i) * conj(X(i)));
<a name="l00478"></a>00478 
<a name="l00479"></a>00479     <span class="keywordflow">return</span> sqrt(value);
<a name="l00480"></a>00480   }
<a name="l00481"></a>00481 
<a name="l00482"></a>00482 
<a name="l00483"></a>00483   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00484"></a>00484   T1 Norm2(<span class="keyword">const</span> Vector&lt;T1, VectSparse, Allocator1&gt;&amp; X)
<a name="l00485"></a>00485   {
<a name="l00486"></a>00486     T1 value(0);
<a name="l00487"></a>00487 
<a name="l00488"></a>00488     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetSize(); i++)
<a name="l00489"></a>00489       value += X.Value(i) * X.Value(i);
<a name="l00490"></a>00490 
<a name="l00491"></a>00491     <span class="keywordflow">return</span> sqrt(value);
<a name="l00492"></a>00492   }
<a name="l00493"></a>00493 
<a name="l00494"></a>00494 
<a name="l00495"></a>00495   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T1, <span class="keyword">class</span> Allocator1&gt;
<a name="l00496"></a>00496   T1 Norm2(<span class="keyword">const</span> Vector&lt;complex&lt;T1&gt;, VectSparse, Allocator1&gt;&amp; X)
<a name="l00497"></a>00497   {
<a name="l00498"></a>00498     T1 value(0);
<a name="l00499"></a>00499 
<a name="l00500"></a>00500     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetSize(); i++)
<a name="l00501"></a>00501       value += real(X.Value(i) * conj(X.Value(i)));
<a name="l00502"></a>00502 
<a name="l00503"></a>00503     <span class="keywordflow">return</span> sqrt(value);
<a name="l00504"></a>00504   }
<a name="l00505"></a>00505 
<a name="l00506"></a>00506 
<a name="l00507"></a>00507   <span class="comment">// NORM2 //</span>
<a name="l00509"></a>00509 <span class="comment"></span>
<a name="l00510"></a>00510 
<a name="l00511"></a>00511 
<a name="l00513"></a>00513   <span class="comment">// GETMAXABSINDEX //</span>
<a name="l00514"></a>00514 
<a name="l00515"></a>00515 
<a name="l00516"></a>00516   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00517"></a>00517   <span class="keywordtype">int</span> GetMaxAbsIndex(<span class="keyword">const</span> Vector&lt;T, Storage, Allocator&gt;&amp; X)
<a name="l00518"></a>00518   {
<a name="l00519"></a>00519     <span class="keywordflow">return</span> X.GetNormInfIndex();
<a name="l00520"></a>00520   }
<a name="l00521"></a>00521 
<a name="l00522"></a>00522 
<a name="l00523"></a>00523   <span class="comment">// GETMAXABSINDEX //</span>
<a name="l00525"></a>00525 <span class="comment"></span>
<a name="l00526"></a>00526 
<a name="l00527"></a>00527 
<a name="l00529"></a>00529   <span class="comment">// APPLYROT //</span>
<a name="l00530"></a>00530 
<a name="l00531"></a>00531 
<a name="l00533"></a>00533   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00534"></a><a class="code" href="namespace_seldon.php#a280476959b456ad5aa0aab55257f5c03">00534</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a280476959b456ad5aa0aab55257f5c03" title="Computation of rotation between two points.">GenRot</a>(T&amp; a_in, T&amp; b_in, T&amp; c_, T&amp; s_)
<a name="l00535"></a>00535   {
<a name="l00536"></a>00536     <span class="comment">// Old BLAS version.</span>
<a name="l00537"></a>00537     T roe;
<a name="l00538"></a>00538     <span class="keywordflow">if</span> (abs(a_in) &gt; abs(b_in))
<a name="l00539"></a>00539       roe = a_in;
<a name="l00540"></a>00540     <span class="keywordflow">else</span>
<a name="l00541"></a>00541       roe = b_in;
<a name="l00542"></a>00542 
<a name="l00543"></a>00543     T scal = abs(a_in) + abs(b_in);
<a name="l00544"></a>00544     T r, z;
<a name="l00545"></a>00545     <span class="keywordflow">if</span> (scal != T(0))
<a name="l00546"></a>00546       {
<a name="l00547"></a>00547         T a_scl = a_in / scal;
<a name="l00548"></a>00548         T b_scl = b_in / scal;
<a name="l00549"></a>00549         r = scal * sqrt(a_scl * a_scl + b_scl * b_scl);
<a name="l00550"></a>00550         <span class="keywordflow">if</span> (roe &lt; T(0))
<a name="l00551"></a>00551           r *= T(-1);
<a name="l00552"></a>00552 
<a name="l00553"></a>00553         c_ = a_in / r;
<a name="l00554"></a>00554         s_ = b_in / r;
<a name="l00555"></a>00555         z = T(1);
<a name="l00556"></a>00556         <span class="keywordflow">if</span> (abs(a_in) &gt; abs(b_in))
<a name="l00557"></a>00557           z = s_;
<a name="l00558"></a>00558         <span class="keywordflow">else</span> <span class="keywordflow">if</span> (abs(b_in) &gt;= abs(a_in) &amp;&amp; c_ != T(0))
<a name="l00559"></a>00559           z = T(1) / c_;
<a name="l00560"></a>00560       }
<a name="l00561"></a>00561     <span class="keywordflow">else</span>
<a name="l00562"></a>00562       {
<a name="l00563"></a>00563         c_ = T(1);
<a name="l00564"></a>00564         s_ = T(0);
<a name="l00565"></a>00565         r = T(0);
<a name="l00566"></a>00566         z = T(0);
<a name="l00567"></a>00567       }
<a name="l00568"></a>00568     a_in = r;
<a name="l00569"></a>00569     b_in = z;
<a name="l00570"></a>00570   }
<a name="l00571"></a>00571 
<a name="l00572"></a>00572 
<a name="l00574"></a>00574   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00575"></a><a class="code" href="namespace_seldon.php#aa562d261e61e0ebec701800273f4eded">00575</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a280476959b456ad5aa0aab55257f5c03" title="Computation of rotation between two points.">GenRot</a>(complex&lt;T&gt;&amp; a_in, complex&lt;T&gt;&amp; b_in, T&amp; c_, complex&lt;T&gt;&amp; s_)
<a name="l00576"></a>00576   {
<a name="l00577"></a>00577 
<a name="l00578"></a>00578     T a = abs(a_in), b = abs(b_in);
<a name="l00579"></a>00579     <span class="keywordflow">if</span> (a == T(0))
<a name="l00580"></a>00580       {
<a name="l00581"></a>00581         c_ = T(0);
<a name="l00582"></a>00582         s_ = complex&lt;T&gt;(1, 0);
<a name="l00583"></a>00583         a_in = b_in;
<a name="l00584"></a>00584       }
<a name="l00585"></a>00585     <span class="keywordflow">else</span>
<a name="l00586"></a>00586       {
<a name="l00587"></a>00587         T scale = a + b;
<a name="l00588"></a>00588         T a_scal = abs(a_in / scale);
<a name="l00589"></a>00589         T b_scal = abs(b_in / scale);
<a name="l00590"></a>00590         T norm = sqrt(a_scal * a_scal + b_scal * b_scal) * scale;
<a name="l00591"></a>00591 
<a name="l00592"></a>00592         c_ = a / norm;
<a name="l00593"></a>00593         complex&lt;T&gt; alpha = a_in / a;
<a name="l00594"></a>00594         s_ = alpha * conj(b_in) / norm;
<a name="l00595"></a>00595         a_in = alpha * norm;
<a name="l00596"></a>00596       }
<a name="l00597"></a>00597     b_in = complex&lt;T&gt;(0, 0);
<a name="l00598"></a>00598   }
<a name="l00599"></a>00599 
<a name="l00600"></a>00600 
<a name="l00602"></a>00602   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00603"></a><a class="code" href="namespace_seldon.php#a5cb9b0f1114a738c5ecb66e2629a3155">00603</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a5cb9b0f1114a738c5ecb66e2629a3155" title="Rotation of a point in 2-D.">ApplyRot</a>(T&amp; x, T&amp; y, <span class="keyword">const</span> T c_, <span class="keyword">const</span> T s_)
<a name="l00604"></a>00604   {
<a name="l00605"></a>00605     T temp = c_ * x + s_ * y;
<a name="l00606"></a>00606     y = c_ * y - s_ * x;
<a name="l00607"></a>00607     x = temp;
<a name="l00608"></a>00608   }
<a name="l00609"></a>00609 
<a name="l00610"></a>00610 
<a name="l00612"></a>00612   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00613"></a><a class="code" href="namespace_seldon.php#a718682e09681eeb0a72ca19506c59f4a">00613</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a5cb9b0f1114a738c5ecb66e2629a3155" title="Rotation of a point in 2-D.">ApplyRot</a>(complex&lt;T&gt;&amp; x, complex&lt;T&gt;&amp; y,
<a name="l00614"></a>00614                 <span class="keyword">const</span> T&amp; c_, <span class="keyword">const</span> complex&lt;T&gt;&amp; s_)
<a name="l00615"></a>00615   {
<a name="l00616"></a>00616     complex&lt;T&gt; temp = s_ * y + c_ * x;
<a name="l00617"></a>00617     y = -conj(s_) * x + c_ * y;
<a name="l00618"></a>00618     x = temp;
<a name="l00619"></a>00619   }
<a name="l00620"></a>00620 
<a name="l00621"></a>00621 
<a name="l00622"></a>00622   <span class="comment">// APPLYROT //</span>
<a name="l00624"></a>00624 <span class="comment"></span>
<a name="l00625"></a>00625 
<a name="l00626"></a>00626 
<a name="l00628"></a>00628   <span class="comment">// CHECKDIM //</span>
<a name="l00629"></a>00629 
<a name="l00630"></a>00630 
<a name="l00632"></a>00632 
<a name="l00642"></a>00642   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00643"></a>00643             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00644"></a><a class="code" href="namespace_seldon.php#a6372db8f87958ababff2932f9c1e6381">00644</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T0, Storage0, Allocator0&gt;</a>&amp; X,
<a name="l00645"></a>00645                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; Y,
<a name="l00646"></a>00646                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> op = <span class="stringliteral">&quot;X + Y&quot;</span>)
<a name="l00647"></a>00647   {
<a name="l00648"></a>00648     <span class="keywordflow">if</span> (X.GetLength() != Y.GetLength())
<a name="l00649"></a>00649       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation &quot;</span>) + op
<a name="l00650"></a>00650                      + string(<span class="stringliteral">&quot; not permitted:&quot;</span>)
<a name="l00651"></a>00651                      + string(<span class="stringliteral">&quot;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00652"></a>00652                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetLength())
<a name="l00653"></a>00653                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00654"></a>00654                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetLength())
<a name="l00655"></a>00655                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l00656"></a>00656   }
<a name="l00657"></a>00657 
<a name="l00658"></a>00658 
<a name="l00660"></a>00660 
<a name="l00670"></a>00670   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Allocator0,
<a name="l00671"></a>00671             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00672"></a><a class="code" href="namespace_seldon.php#a75dc0d6b7361949b7f502de5eaa732a1">00672</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T0, Vect_Sparse, Allocator0&gt;</a>&amp; X,
<a name="l00673"></a>00673                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Vect_Sparse, Allocator1&gt;</a>&amp; Y,
<a name="l00674"></a>00674                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> op = <span class="stringliteral">&quot;X + Y&quot;</span>)
<a name="l00675"></a>00675   {
<a name="l00676"></a>00676     <span class="comment">// The dimension of a Vector&lt;Vect_Sparse&gt; is infinite,</span>
<a name="l00677"></a>00677     <span class="comment">// so no vector dimension checking has to be done.</span>
<a name="l00678"></a>00678   }
<a name="l00679"></a>00679 
<a name="l00680"></a>00680 
<a name="l00682"></a>00682 
<a name="l00692"></a>00692   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Allocator0,
<a name="l00693"></a>00693             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00694"></a><a class="code" href="namespace_seldon.php#ad26b1228103098f94754459edc495599">00694</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T0, Collection, Allocator0&gt;</a>&amp; X,
<a name="l00695"></a>00695                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Collection, Allocator1&gt;</a>&amp; Y,
<a name="l00696"></a>00696                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> op = <span class="stringliteral">&quot;X + Y&quot;</span>)
<a name="l00697"></a>00697   {
<a name="l00698"></a>00698     <span class="keywordflow">if</span> (X.GetLength() != Y.GetLength())
<a name="l00699"></a>00699       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation &quot;</span>) + op
<a name="l00700"></a>00700                      + string(<span class="stringliteral">&quot; not permitted:&quot;</span>)
<a name="l00701"></a>00701                      + string(<span class="stringliteral">&quot;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00702"></a>00702                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetLength())
<a name="l00703"></a>00703                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00704"></a>00704                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetLength())
<a name="l00705"></a>00705                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l00706"></a>00706 
<a name="l00707"></a>00707     <span class="keywordflow">if</span> (X.GetNvector() != Y.GetNvector())
<a name="l00708"></a>00708       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation &quot;</span>) + op
<a name="l00709"></a>00709                      + string(<span class="stringliteral">&quot; not permitted:&quot;</span>)
<a name="l00710"></a>00710                      + string(<span class="stringliteral">&quot;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00711"></a>00711                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetNvector())
<a name="l00712"></a>00712                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00713"></a>00713                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetNvector())
<a name="l00714"></a>00714                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l00715"></a>00715   }
<a name="l00716"></a>00716 
<a name="l00717"></a>00717 
<a name="l00719"></a>00719 
<a name="l00729"></a>00729   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Allocator0, <span class="keyword">class </span>Allocator00,
<a name="l00730"></a>00730             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator11&gt;
<a name="l00731"></a><a class="code" href="namespace_seldon.php#a280c73fa3bc2108b59e86208518d43b9">00731</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T0, Vect_Sparse, Allocator0&gt;</a>,
<a name="l00732"></a>00732                 <a class="code" href="class_seldon_1_1_collection.php">Collection</a>, Allocator00&gt;&amp; X,
<a name="l00733"></a>00733                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Vect_Sparse, Allocator1&gt;</a>,
<a name="l00734"></a>00734                 <a class="code" href="class_seldon_1_1_collection.php">Collection</a>, Allocator11&gt;&amp; Y,
<a name="l00735"></a>00735                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> op = <span class="stringliteral">&quot;X + Y&quot;</span>)
<a name="l00736"></a>00736   {
<a name="l00737"></a>00737     <span class="keywordflow">if</span> (X.GetNvector() != Y.GetNvector())
<a name="l00738"></a>00738       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation &quot;</span>) + op
<a name="l00739"></a>00739                      + string(<span class="stringliteral">&quot; not permitted:&quot;</span>)
<a name="l00740"></a>00740                      + string(<span class="stringliteral">&quot;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00741"></a>00741                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetNvector())
<a name="l00742"></a>00742                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00743"></a>00743                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetNvector())
<a name="l00744"></a>00744                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l00745"></a>00745   }
<a name="l00746"></a>00746 
<a name="l00747"></a>00747 
<a name="l00749"></a>00749 
<a name="l00759"></a>00759   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Allocator0,
<a name="l00760"></a>00760             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00761"></a><a class="code" href="namespace_seldon.php#a1a2647f39a380a4c7599fed605e80583">00761</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T0, VectFull, Allocator0&gt;</a>&amp; X,
<a name="l00762"></a>00762                 <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Collection, Allocator1&gt;</a>&amp; Y,
<a name="l00763"></a>00763                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> op = <span class="stringliteral">&quot;X + Y&quot;</span>)
<a name="l00764"></a>00764   {
<a name="l00765"></a>00765     <span class="keywordflow">if</span> (X.GetLength() != Y.GetM())
<a name="l00766"></a>00766       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation &quot;</span>) + op
<a name="l00767"></a>00767                      + string(<span class="stringliteral">&quot; not permitted:&quot;</span>)
<a name="l00768"></a>00768                      + string(<span class="stringliteral">&quot;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00769"></a>00769                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetLength())
<a name="l00770"></a>00770                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00771"></a>00771                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetM())
<a name="l00772"></a>00772                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l00773"></a>00773   }
<a name="l00774"></a>00774 
<a name="l00775"></a>00775 
<a name="l00777"></a>00777 
<a name="l00787"></a>00787   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U0&gt; <span class="keyword">class </span>Allocator0,
<a name="l00788"></a>00788             <span class="keyword">class </span>T1, <span class="keyword">template</span> &lt;<span class="keyword">class</span> U1&gt; <span class="keyword">class </span>Allocator1&gt;
<a name="l00789"></a>00789   <span class="keywordtype">void</span>
<a name="l00790"></a><a class="code" href="namespace_seldon.php#a35648550e43382b2430fe2282b34566f">00790</a>   <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span>
<a name="l00791"></a>00791            <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;<a class="code" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="code" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator0&lt;T0&gt; &gt;&amp; X,
<a name="l00792"></a>00792            <span class="keyword">const</span>
<a name="l00793"></a>00793            <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;<a class="code" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="code" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator1&lt;T1&gt; &gt;&amp; Y,
<a name="l00794"></a>00794            <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> op = <span class="stringliteral">&quot;X + Y&quot;</span>)
<a name="l00795"></a>00795   {
<a name="l00796"></a>00796     <span class="keywordflow">if</span> (X.GetNvector() != Y.GetNvector())
<a name="l00797"></a>00797       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation &quot;</span>) + op
<a name="l00798"></a>00798                      + string(<span class="stringliteral">&quot; not permitted:&quot;</span>)
<a name="l00799"></a>00799                      + string(<span class="stringliteral">&quot;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00800"></a>00800                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetNvector())
<a name="l00801"></a>00801                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l00802"></a>00802                      + string(<span class="stringliteral">&quot;vector of length &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetNvector())
<a name="l00803"></a>00803                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l00804"></a>00804   }
<a name="l00805"></a>00805 
<a name="l00806"></a>00806 
<a name="l00807"></a>00807   <span class="comment">// CHECKDIM //</span>
<a name="l00809"></a>00809 <span class="comment"></span>
<a name="l00810"></a>00810 
<a name="l00811"></a>00811 
<a name="l00813"></a>00813   <span class="comment">// CONJUGATE //</span>
<a name="l00814"></a>00814 
<a name="l00815"></a>00815 
<a name="l00817"></a>00817   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00818"></a><a class="code" href="namespace_seldon.php#a90119cfc81b2e157c522a426a0167ccb">00818</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a90119cfc81b2e157c522a426a0167ccb" title="Sets a vector to its conjugate.">Conjugate</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Prop, Allocator&gt;</a>&amp; X)
<a name="l00819"></a>00819   {
<a name="l00820"></a>00820     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetM(); i++)
<a name="l00821"></a>00821       X(i) = conj(X(i));
<a name="l00822"></a>00822   }
<a name="l00823"></a>00823 
<a name="l00824"></a>00824 
<a name="l00826"></a>00826   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00827"></a><a class="code" href="namespace_seldon.php#a15696c108619b2aa5eb3a80bb0efbe87">00827</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a90119cfc81b2e157c522a426a0167ccb" title="Sets a vector to its conjugate.">Conjugate</a>(<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php" title="Sparse vector class.">Vector&lt;T, VectSparse, Allocator&gt;</a>&amp; X)
<a name="l00828"></a>00828   {
<a name="l00829"></a>00829     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.<a class="code" href="class_seldon_1_1_vector___base.php#a839880a3113c1885ead70d79fade0294" title="Returns the number of elements stored.">GetSize</a>(); i++)
<a name="l00830"></a>00830       X.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a51c6d9be9bfc1234e17d997f9bcdff07" title="Access operator.">Value</a>(i) = conj(X.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a51c6d9be9bfc1234e17d997f9bcdff07" title="Access operator.">Value</a>(i));
<a name="l00831"></a>00831   }
<a name="l00832"></a>00832 
<a name="l00833"></a>00833 
<a name="l00834"></a>00834   <span class="comment">// CONJUGATE //</span>
<a name="l00836"></a>00836 <span class="comment"></span>
<a name="l00837"></a>00837 
<a name="l00838"></a>00838 } <span class="comment">// namespace Seldon.</span>
<a name="l00839"></a>00839 
<a name="l00840"></a>00840 <span class="preprocessor">#define SELDON_FILE_FUNCTIONS_VECTOR_CXX</span>
<a name="l00841"></a>00841 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
