<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Matrix&lt; T, Prop, RowUpTriang, Allocator &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowUpTriang, Allocator &gt;</a>, including all inherited members.<table>
  <tr bgcolor="#f0f0f0"><td><b>access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>allocator</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowUpTriang, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>allocator_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#a01cf3952c69288e68376db61cf322c77">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#a22a6a81034ad25df2123e33d1fb9052d">Copy</a>(const Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>data_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>entry_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#a12f60cdce0a9a1dc924cfab291af4178">Fill</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#abddc5c5f65873a308e5f6919f5dc4023">Fill</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#ad9f5e4d6ffc7dff60d8946e26102f71b">FillRand</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#a812d8809554264f3b8f64429231b4e39">GetDataSize</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">GetM</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">GetN</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">GetSize</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>m_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php#a5d54235702d1a8347394a5b63846568a">Matrix</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php#a1899bdb8a6a9b11589b14150837c011a">Matrix</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a47988d7972247286332e853c13bbc00b">Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ad2ea11b0880dc32ba9472da9310c332b">Matrix_Base</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline, explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a127e0229931486cea3e6007988b446a0">Matrix_Base</a>(const Matrix_Base&lt; T, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#a60a394e341c7e0425b5c51f89e93dd14">Matrix_Triangular</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#a3ecbb30f1d5aee888ef396bbfde1f61b">Matrix_Triangular</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#ae2727885c2d249d8f386e55403133a7d">Matrix_Triangular</a>(const Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>me_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>n_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#a5380ef67c34e203bc186a8ca351abbbb">Nullify</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#a5f07919d9f3d4d8ac483247e4d9d752f">operator()</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#a6cc7b33b27d2a8d51293614d176ebfc0">operator()</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php#a19b9f7624485f210146c6255c6835c1a">operator*=</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php#a1c1975aa196d966318a70af7ccfa177b">operator=</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#ab379631da575bbb4b2404f1fc48c37a4">Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;::operator=</a>(const Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#a30661e664fd84702ea900809e4c7764c">operator[]</a>(int i)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#ab4f150251c1afd88a7a59a382ac4f623">operator[]</a>(int i) const</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#ad1b8f26eb7bfda6d3eee2a70707dea73">Print</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#aae8a9e344c29a49dac68392d5bb076be">Print</a>(int a, int b, int m, int n) const</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#a9071a92d72aec94dc95549701a404db8">Print</a>(int l) const</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>property</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowUpTriang, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#afd699e58126e4a75b734c72a7b07652a">Read</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#a1fad405d3c4a66ef2f1aab8ad0d2c054">Read</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#aa1064e91f58256717c26641aff9f8dbb">ReadText</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#a16abbe61e55e1a6a1f0a173f32457eed">ReadText</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#a472fa39bd3faee779cae713a5bd1aeb3">Reallocate</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#a30b10bb11499845df9d647c3e0ef8915">Resize</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SetData</b>(int i, int j, pointer data) (defined in <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#a17b39f20590f545d3dce529aeaab9d5f">SetIdentity</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>storage</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowUpTriang, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#aca8b82d5764a1943b2ec9a36d502816f">Val</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#ab2e953becbaab6664f72752932e14f5c">Val</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>value_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowUpTriang, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#a9696365a66c66029580049b9f282a4c5">Write</a>(string FileName) const</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#a5415533e02cb7b5445e3005ce1f54bda">Write</a>(ostream &amp;FileStream) const</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#ac192355d2d110149854ca8bb9406b076">WriteText</a>(string FileName) const</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#abc80799bd9b45574598b3350187715da">WriteText</a>(ostream &amp;FileStream) const</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#af668e8676a93c1a99ebee4794994cc6e">Zero</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af34a03c0cc56f757a83cd56f97c74ed8">~Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___triangular.php#a1bf20e38fe0a64a4db93991819fba679">~Matrix_Triangular</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular&lt; T, Prop, RowUpTriang, Allocator &gt;</a></td><td></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
